//
//  AppDelegate.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AppDelegate.h"
#import "PDTestViewController.h"
#import "RESideMenu.h"
#import "PDSliderleftViewController.h"
#import "PDHomeViewController.h"
#import "PDStupSQLiteManager.h"             // SQLite
#import "PDLaunchViewController.h"          // Launch
#import "PDTabBarController.h"
#import "PDThirdSharedTool.h"               // 三方登录
#import "PDAliPayHandle.h"                  // 支付宝
#import "WXApi.h"
#import "payRequsestHandler.h"
#import "PDNavigationController.h"
#import <ALBBSDK/ALBBSDK.h>
#import "PDApnsTool.h"
#import "PDChallengeViewController.h"
#import "PDLoginViewController.h"
#import "MobClick.h"
#import "PDFetchModel.h"
@interface AppDelegate ()<RESideMenuDelegate,WXApiDelegate,launchBackDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [MobClick setLogEnabled:YES];  // 打开友盟sdk调试，注意Release发布时需要注释掉此行,减少io消耗
    [MobClick setAppVersion:XcodeAppVersion]; //参数为NSString * 类型,自定义app版本信息，如果不设置，默认从CFBundleVersion里取
    [MobClick startWithAppkey:@"572050f3e0f55a426a0017a5" reportPolicy:BATCH   channelId:nil];
//    [MobClick profileSignInWithPUID:@"playerID"];
    [MobClick profileSignInWithPUID:@"playerID" provider:@"WB"];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    [PDStupSQLiteManager setupSqlite];                              // 1. 安装数据库
    [PDThirdSharedTool setUpThirdLogin];                            // 三方注册
    [WXApi registerApp:WX_APP_ID withDescription:WX_APP_DESC];
    [[PDApnsTool shareInstance] registWithPUSHWithApplication:application launchOptions:launchOptions deviceIdBlock:NULL];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    

    [PDLaunchTool launchJudge]; //判断Launch
    if ([USERDEFAULTS boolForKey:LaunchFirst]) {
        PDLaunchViewController * launchViewController = [[PDLaunchViewController alloc]init];
        launchViewController.delegate = self;
        self.window.rootViewController = launchViewController;
        self.window.backgroundColor = [UIColor whiteColor];
        [self.window makeKeyAndVisible];
    } else {
        [PDLaunchTool createmainSlider:self.window];
    }
    
    [NSThread sleepForTimeInterval:1.5];                          //lanch页面延时1.5s
    return YES;
}

#pragma mark - launch回调
- (void)launchBackDelegate:(UIButton *)button {
    [button buttonWithBlock:^(UIButton *button) {
        [PDLaunchTool createmainSlider:self.window];
        [RESideMenu shareInstance].delegate = self;
    }];
}

#pragma maek - 推送
#pragma mark ---------------------------------------------------
// 注册deviceToken
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    [[PDApnsTool shareInstance] registerWithDeviceId:deviceToken];
}

/// iOS8下申请DeviceToken
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}
#endif

// 通知统计回调
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
    [[PDApnsTool shareInstance] registerWithReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    NSLog(@"didFailToRegisterForRemoteNotificationsWithError %@", error);
}


#pragma mark - 推送end
#pragma mark ----------------------------------------------


- (void)applicationWillResignActive:(UIApplication *)application {

}

- (void)applicationDidEnterBackground:(UIApplication *)application {

}

- (void)applicationWillEnterForeground:(UIApplication *)application {

}

- (void)applicationDidBecomeActive:(UIApplication *)application {

}

- (void)applicationWillTerminate:(UIApplication *)application {
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    [fetchModel clearCookiesForBaseURL];
}

#ifdef IS_IOS9_LATER
-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
    return [self openURLManagerWithURL:url];
}
#else
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    return [self openURLManagerWithURL:url];
}
#endif


#pragma mark - OpenURL Manager
-(BOOL)openURLManagerWithURL:(NSURL *)url{
    NSLog(@"%@",url);
    if ([url.scheme isEqualToString:@"panda-alipay"]) {
        if ([url.host isEqualToString:@"safepay"]) {
            [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@", resultDic);
            }];
        }
        else if ([url.host isEqualToString:@"platformapi"]) {//支付宝钱包快登授权返回 authCode
            [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
                NSLog(@"result = %@", resultDic);
            }];
        }
    }
    else if ([url.scheme isEqualToString:WX_APP_ID]) {
         return [WXApi handleOpenURL:url delegate:self];
    }
    return YES;
}

#pragma mark - 微信支付回调
- (void)onResp:(BaseResp *)resp {
    if ([resp isKindOfClass:[PayResp class]]) {
        PayResp *response = (PayResp *)resp;
        switch (response.errCode) {
            case WXSuccess: {
                [NOTIFICENTER postNotificationName:WX_PAY_NOTIFICATION object:nil userInfo:@{@"result":@"success"}];
                break;
            }
                
            default: {
                [NOTIFICENTER postNotificationName:WX_PAY_NOTIFICATION object:nil userInfo:@{@"result":@"fail"}];
                break;
            }
        }
    }
}

#pragma mark RESideMenuDelegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController {
    //    NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController {
    //    NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController {
    //    NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController {
    //    NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}


@end
