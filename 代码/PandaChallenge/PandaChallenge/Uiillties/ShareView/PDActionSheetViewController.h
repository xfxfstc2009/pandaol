//
//  PDActionSheetViewController.h
//  PandaChallenge
//
//  Created by panda on 16/3/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDActionSheetViewController : AbstractViewController
/**
 *  是否需要手势点击，用于隐藏分享界面
 */
@property (nonatomic,assign) BOOL isHasGesture;
/**
 *  用于显示分享页面
 *
 *  @param viewController
 */
- (void)showInView:(UIViewController *)viewController;
/**
 *  用于隐藏分享页面
 *
 *  @param viewController
 */
- (void)dismissFromeView:(UIViewController *)viewController;
/**
 *  毛玻璃效果
 *
 *  @param viewController
 */
//- (void)lightEffectForView:(UIViewController *)viewController;
@end
