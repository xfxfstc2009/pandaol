//
//  PDActionSheetViewController.m
//  PandaChallenge
//
//  Created by panda on 16/3/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDActionSheetViewController.h"
#import "PDInviteModel.h"
#import "PDImageView.h"
#import "PDShareButton.h"
#import "PDThirdSharedTool.h"
#define PDSHARE_VIEW_HEIGHT (kScreenBounds.size.height*0.3)
#define PDSHARE_VIEW_WIDTH  kScreenBounds.size.width
#define PDSHARE_BUTTON_W   LCFloat(55)
#define PDSHARE_BUTTON_H   (LCFloat(55) + [NSString contentofHeightWithFont:[UIFont systemFontOfSize:10]])
@interface PDActionSheetViewController ()
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIView *backgroundView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong)NSMutableArray *shareButtonArray;
@property (nonatomic,strong)UIImage *backgroudImage;
@property (nonatomic,assign)CGFloat shareViewHeight;
@property (nonatomic,assign)CGFloat numberOfLine;
@property (nonatomic,strong)PDInviteModel *inviteModel;
@property (nonatomic,strong)UIImage *shareImage;
@end

@implementation PDActionSheetViewController
- (NSMutableArray *)shareButtonArray
{
    if (!_shareButtonArray) {
        _shareButtonArray = [NSMutableArray array];
    }
    return _shareButtonArray;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self removeBackgoundView];
    [self arrayWithInit];
    [self creatSheetView];
    NSLog(@"%f",PDSHARE_BUTTON_H);
}
#pragma mark - removeBackgoundView
- (void)removeBackgoundView
{
    for (id tempView in self.view.subviews) {
        if ([tempView isKindOfClass:[PDImageView class]]) {
            [tempView removeFromSuperview];
        }
    }
}
#pragma  mark - arrayWithInit
- (void)arrayWithInit
{
    self.shareButtonArray = [PDThirdSharedTool shareForInstall];
}
#pragma mark - creatSheetView
- (void)creatSheetView
{
    self.view.backgroundColor = [UIColor clearColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.modalPresentationCapturesStatusBarAppearance = YES;
    self.backgroundView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.65f];
//    self.backgroundView.backgroundColor = [UIColor colorWithPatternImage:self.backgroudImage];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    if (self.isHasGesture) {
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissFromeView:)];
        [self.backgroundView addGestureRecognizer:tap];
    }
    [self.view addSubview:self.backgroundView];
    
    [self creatShareView];
}
#pragma mark - creatShareView
- (void)creatShareView
{
    //判断行数，根据行数设置分享页面高度
    self.numberOfLine = (self.shareButtonArray.count-1)/4 == 1?2:1;
    if (self.numberOfLine == 1) {
        self.shareViewHeight = LCFloat(20) + PDSHARE_BUTTON_H * 2 + LCFloat(20);
    }else if (self.numberOfLine ==2){
        self.shareViewHeight = LCFloat(20) + PDSHARE_BUTTON_H *3 +LCFloat(20) + LCFloat(20);
    }
    
    self.shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, kScreenBounds.size.height, PDSHARE_VIEW_WIDTH, self.shareViewHeight)];
    self.shareView.backgroundColor = [UIColor colorWithRed:1. green:1. blue:1. alpha:1.];
    self.shareView.contentMode = UIViewContentModeBottom;
    self.shareView.clipsToBounds = YES;
    self.shareView.userInteractionEnabled = YES;
    [self.view addSubview:self.shareView];
    [self createShareButton];
}
#pragma mark - createShareButton
- (void)createShareButton
{
    //左右边距
    CGFloat horMargin = LCFloat(50);
    //左右中间间距
    CGFloat horCenterMargin = (PDSHARE_VIEW_WIDTH - 2*horMargin - 4*PDSHARE_BUTTON_W)/3.0;
    //上下边距
    CGFloat verMargin = LCFloat(20);
    //上下中间间距
    CGFloat verCenterMargin = LCFloat(20);
    for (int i =0; i <self.shareButtonArray.count; i++) {
        PDShareButton *shareButton = [[PDShareButton alloc]initWithFrame:CGRectMake(horMargin+(PDSHARE_BUTTON_W+horCenterMargin)*(i%4),verMargin+(PDSHARE_BUTTON_H+verCenterMargin)*(i/4) , PDSHARE_BUTTON_W, PDSHARE_BUTTON_H)];
        shareButton.stringTag = self.shareButtonArray[i][2];
        shareButton.imageView.image = [UIImage imageNamed:self.shareButtonArray[i][1]];
        [shareButton setBackgroundColor:[UIColor clearColor]];
        [shareButton setTitle:self.shareButtonArray[i][0] forState:UIControlStateNormal];
        [shareButton addTarget:self action:@selector(shareButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.shareView addSubview:shareButton];
    }
    //线
    CGFloat lineY = self.numberOfLine == 1?2 *verMargin + PDSHARE_BUTTON_H : 2 *verMargin + PDSHARE_BUTTON_H *2 + verCenterMargin;
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0,lineY, kScreenBounds.size.width, 1)];
    line.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"icon_share_line"]];
    [self.shareView addSubview:line];
    //取消按钮
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(0, CGRectGetMaxY(line.frame), kScreenBounds.size.width, self.shareViewHeight-CGRectGetMaxY(line.frame));
    [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancelButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont systemFontOfCustomeSize:20];
    cancelButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    cancelButton.backgroundColor = [UIColor colorWithRed:1. green:1. blue:1. alpha:.8];
    [cancelButton addTarget:self action:@selector(cancelButtonClick) forControlEvents:UIControlEventTouchUpInside];
    [self.shareView addSubview:cancelButton];
 }
#pragma mark - indexClick
- (void)shareButtonClick:(PDShareButton *)sender
{
    [self buttonUnenableClick];
     NSString *shareType = sender.stringTag;
    self.inviteModel = [[PDInviteModel alloc]init];
    __weak typeof(self) weakSelf = self;
    [PDHUD showWithStatus:@"分享加载中..." delay:0];
    [self.inviteModel fetchWithPath:KShare completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf buttonEnableClick];
        if (isSucceeded) {
            NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:strongSelf.inviteModel.imageUrl]];
            strongSelf.shareImage = [UIImage imageWithData:data];
            NSLog(@"%@",strongSelf.shareImage);
            
            [PDThirdSharedTool shareWithShareType:shareType content:strongSelf.inviteModel.content image:strongSelf.shareImage imageUrl:strongSelf.inviteModel.imageUrl urlString:strongSelf.inviteModel.url];
        }else{
            NSLog(@"%@",error);
        }
        [strongSelf dismissFromeView:strongSelf];
    }];
    NSLog(@"%@",sender.stringTag);
}

- (void)buttonUnenableClick
{
    for (int i =0; i <self.shareButtonArray.count; i++) {
        NSString *stringTag = self.shareButtonArray[i][2];
        PDShareButton *shareButton = (PDShareButton *)[self.shareView viewWithStringTag:stringTag];
        shareButton.enabled = NO;
    }
}

- (void)buttonEnableClick
{
    for (int i =0; i <self.shareButtonArray.count; i++) {
        NSString *stringTag = self.shareButtonArray[i][2];
        PDShareButton *shareButton = (PDShareButton *)[self.shareView viewWithStringTag:stringTag];
        shareButton.enabled = YES;
    }

}

- (void)cancelButtonClick
{
    NSLog(@"取消");
    [self dismissFromeView:self];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue
{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    animation.duration = 1.1;
    animation.fromValue = [NSNumber numberWithFloat:fromValue];
    animation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgroundView.layer addAnimation:animation forKey:@"anumateOpacity"];
}
#pragma mark - 显示隐藏View
- (void)dismissFromeView:(UIViewController *)viewController
{
    __weak PDActionSheetViewController *weakVC = self;
    [UIView animateWithDuration:0.3f animations:^{
        self.shareView.frame = CGRectMake(0, kScreenBounds.size.height, PDSHARE_VIEW_WIDTH, self.shareViewHeight);
    }completion:^(BOOL finished) {
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}
- (void)showInView:(UIViewController *)viewController
{
    __weak PDActionSheetViewController *weakVC = self;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [UIView animateWithDuration:0.3f animations:^{
        weakVC.shareView.frame = CGRectMake(0, kScreenBounds.size.height-self.shareViewHeight, PDSHARE_VIEW_WIDTH, self.shareViewHeight);
    }];
}
#pragma mark - 毛玻璃
- (void)lightEffectForView:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroudImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}


@end
