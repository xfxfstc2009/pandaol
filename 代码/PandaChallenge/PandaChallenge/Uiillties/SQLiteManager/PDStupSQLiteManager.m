//
//  PDStupSQLiteManager.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDStupSQLiteManager.h"
#import <sqlite3.h>

#define sqliteName @"pandaDataBase.sqlite"

@implementation PDStupSQLiteManager

#pragma mark - 安装Sqlite
+ (void)setupSqlite{
    NSFileManager *fileManager=[NSFileManager defaultManager];
    bool ifFind=[fileManager fileExistsAtPath:[self pathForDatabase]];
    if(!ifFind) {           // 把包里面的数据库文件 复制到沙盒的Documents 文件夹
        NSString * srcPath= [[NSBundle mainBundle]pathForResource:sqliteName ofType:nil];
        [fileManager copyItemAtPath:srcPath toPath:[self pathForDatabase] error:nil];
    }
}

+ (NSString *)pathForDatabase {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath=[paths objectAtIndex:0];
    NSString *path=[documentsPath stringByAppendingPathComponent:sqliteName];
    return path;
}

+ (int)lastedTime {
    int date;
    sqlite3 *database;
    const char *query = "select max(date) from history";
    sqlite3_stmt *stmt;
    NSFileManager *manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:[self pathForDatabase]]) {
        if (sqlite3_open([[self pathForDatabase] UTF8String], &database) == SQLITE_OK) {
            if (sqlite3_prepare_v2(database, query, -1, &stmt, NULL) == SQLITE_OK) {
                sqlite3_step(stmt);
                date = (int)sqlite3_column_int(stmt, 0);
                sqlite3_finalize(stmt);
            }
            sqlite3_close(database);
            return date;
        }
        else
            NSLog(@"打开数据库失败");
    }
    else {
        NSLog(@"该路径下不存在对应的sqlite");
    }
    return 0;
}

+ (BOOL)insertWithModel:(PDHistory *)model {
    if (![self canNotInsertMoreData]) {
        [self removeNotesBeforeDay:5];
    }
    char *errmsg = NULL;
    sqlite3 *database;
// 将传进来的model与数据库中最新的那一条进行比较时间大于才存进去
    if (sqlite3_open([[self pathForDatabase] UTF8String], &database) == SQLITE_OK) {
        NSString *sql = [NSString stringWithFormat:@"insert into history(date,note) values(%ld,'%@')",(long)model.date.integerValue,model.msg];
        if (SQLITE_OK != sqlite3_exec(database, [sql UTF8String], NULL, NULL, &errmsg)) {
            NSLog(@"插入字段失败 %s",errmsg);
            sqlite3_close(database);
            return NO;
        }
        return YES;
    }
    else {
        sqlite3_close(database);
        NSLog(@"打开数据库失败");
    }
    return NO;
}

// 限制数据库中的数据条数为500条
+ (BOOL)canNotInsertMoreData {
    int count = 0;
    sqlite3 *database;
    const char *query = "select count(*) from history";
    sqlite3_stmt *stmt;
    if (sqlite3_open([[self pathForDatabase] UTF8String], &database) == SQLITE_OK) {
        if (sqlite3_prepare_v2(database, query, -1, &stmt, NULL) == SQLITE_OK) {
            sqlite3_step(stmt);
            count = sqlite3_column_int(stmt, 0);
            if (count > 500) {
                sqlite3_finalize(stmt);
                sqlite3_close(database);
                NSLog(@"插入数据多于500条了,删除5天前的数据");
                return NO;
            }
            sqlite3_finalize(stmt);
        }
        sqlite3_close(database);
        return YES;
    }
    else {
        sqlite3_close(database);
        NSLog(@"打开数据库失败");
        return NO;
    }
}

// 删除几天前的数据 默认5天
+ (void)removeNotesBeforeDay:(int)day {
    if (!day) {
        day = 5;
    }
    int lastTime = [self lastedTime];
    sqlite3 *database;
    char *errmsg = NULL;
    const char *query= [[NSString stringWithFormat:@"delete from history where date<(%d-%d*3600*24)",lastTime,day] UTF8String];
    if (sqlite3_open([[self pathForDatabase] UTF8String], &database) == SQLITE_OK) {
        if (sqlite3_exec(database, query, NULL, NULL, &errmsg) != SQLITE_OK) {
            sqlite3_close(database);
            NSLog(@"删除数据失败 %s",errmsg);
        }
    }
    else {
        sqlite3_close(database);
        NSLog(@"打开数据库失败");
    }
}

+ (NSMutableArray *)getAllData {
    NSMutableArray *mutableArray = [NSMutableArray array];
    sqlite3 *database;
    const char *query = "select *from history";
    sqlite3_stmt *stmt;
// 先判断该路径下db是否存在
    NSFileManager *manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:[self pathForDatabase]]) {
        if (SQLITE_OK == sqlite3_open([[self pathForDatabase] UTF8String], &database)) {
            if (SQLITE_OK == sqlite3_prepare_v2(database, query, -1, &stmt, NULL)) {
                while (sqlite3_step(stmt) == SQLITE_ROW) {
                    //返回SQLITE_ROW，表示有下一条
                    int date = (int)sqlite3_column_int(stmt, 1);
                    char *note = (char *)sqlite3_column_text(stmt, 2);
                    PDHistory *history = [[PDHistory alloc] init];
                    history.date = [NSString stringWithFormat:@"%d",date];
                    history.msg = [NSString stringWithUTF8String:note];
                    [mutableArray addObject:history];
                }
                sqlite3_finalize(stmt);
            }
            sqlite3_close(database);
        }
        else {
            sqlite3_close(database);
            NSLog(@"打开数据库失败");
        }
    }
    else
        NSLog(@"该路径下不存在对应的sqlite");
    return [mutableArray copy];
}

+ (BOOL)removeAllData {
    sqlite3 *database;
    char *errmsg = NULL;
    const char *query= "delete from history";
    NSFileManager *manager = [NSFileManager defaultManager];
    if ([manager fileExistsAtPath:[self pathForDatabase]]) {
        if (sqlite3_open([[self pathForDatabase] UTF8String], &database) == SQLITE_OK) {
            if (sqlite3_exec(database, query, NULL, NULL, &errmsg) != SQLITE_OK) {
                NSLog(@"删除数据失败 %s",errmsg);
                sqlite3_close(database);
            }
        }
        else {
            sqlite3_close(database);
            NSLog(@"打开数据库失败");
        }
    }
    else
        NSLog(@"该路径下不存在对应的sqlite");
    return NO;
}

@end
