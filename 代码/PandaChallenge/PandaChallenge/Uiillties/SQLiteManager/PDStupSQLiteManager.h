//
//  PDStupSQLiteManager.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDHistory.h"

@interface PDStupSQLiteManager : NSObject

+ (void)setupSqlite;

+ (int)lastedTime;

+ (BOOL)insertWithModel:(PDHistory *)model;

+ (NSMutableArray *)getAllData;

+ (BOOL)removeAllData;
@end
