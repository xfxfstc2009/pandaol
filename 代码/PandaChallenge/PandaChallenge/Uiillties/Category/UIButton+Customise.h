//
//  UIButton+Customise.h
//  LaiCai
//
//  Created by SmartMin on 15-8-10.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//
// 自定义按钮
#import <UIKit/UIKit.h>

@interface UIButton (Customise)

- (void) buttonWithBlock:(void(^)(UIButton *button))buttonClickBlock;

@end
