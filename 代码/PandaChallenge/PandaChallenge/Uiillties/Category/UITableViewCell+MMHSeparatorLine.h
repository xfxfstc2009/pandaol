//
//  UITableViewCell+MMHSeparatorLine.h
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger,SeparatorType)
{
    SeparatorTypeHead    =   1,
    SeparatorTypeMiddle      ,
    SeparatorTypeBottom      ,
    SeparatorTypeSingle      ,
};


@interface UITableViewCell (MMHSeparatorLine)
- (void)addSeparatorLineWithType:(SeparatorType)separatorType;
- (void)addSeparatorLineWithTypeWithAres:(SeparatorType)separatorType andUseing:(NSString *)usingVC;

@end
