//
//  SDWebImageManager+Memory.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIImageView+WebCache.h>

@interface SDWebImageManager (Memory)
- (BOOL)memoryCachedImageExistsForURL:(NSURL *)url;


@end
