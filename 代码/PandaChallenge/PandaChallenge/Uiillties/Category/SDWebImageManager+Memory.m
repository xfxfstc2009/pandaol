//
//  SDWebImageManager+Memory.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "SDWebImageManager+Memory.h"

@implementation SDWebImageManager (Memory)

- (BOOL)memoryCachedImageExistsForURL:(NSURL *)url {
    NSString *key = [self cacheKeyForURL:url];
    return ([self.imageCache imageFromMemoryCacheForKey:key] != nil) ?  YES : NO;
}


@end
