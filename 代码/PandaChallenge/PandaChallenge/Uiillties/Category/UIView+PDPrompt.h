//
//  UIView+PDPrompt.h
//  PandaChallenge
//
//  Created by 盼达 on 16/3/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PDPromptImagePosition) {
    PDPromptImagePositionNone,
    PDPromptImagePositionLeft,
    PDPromptImagePositionTop,
};

@interface UIView (PDPrompt)

- (void)showPrompt:(NSString *)promptString withImage:(UIImage *)promptImage andImagePosition:(PDPromptImagePosition)position tapBlock:(void(^)())tapBlock;

- (void)dismissPrompt;

@end
