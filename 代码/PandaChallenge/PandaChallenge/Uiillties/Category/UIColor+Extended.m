//
//  UIColor+Extended.m
//  LaiCai
//
//  Created by SmartMin on 15-8-10.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import "UIColor+Extended.h"

@implementation UIColor (Extended)

+ (UIColor *)hexChangeFloat:(NSString *)hexColor{
    if (hexColor.length < 6){
        return nil;
    }
    unsigned int red_,green_,blue_;
    NSRange exceptionRange;
    exceptionRange.length = 2;
    
    // red
    exceptionRange.location = 0;
    [[NSScanner scannerWithString:[hexColor substringWithRange:exceptionRange]]scanHexInt:&red_];
    
    //green
    exceptionRange.location = 2;
    [[NSScanner scannerWithString:[hexColor substringWithRange:exceptionRange]]scanHexInt:&green_];
    
    //blue
    exceptionRange.location = 4;
    [[NSScanner scannerWithString:[hexColor substringWithRange:exceptionRange]]scanHexInt:&blue_];
    
    UIColor *resultColor = [UIColor colorWithRed:(CGFloat)red_/255. green:(CGFloat)green_/255. blue:(CGFloat)blue_/255. alpha:1.0];
    return resultColor;
}

#pragma mark 自定义颜色
+(UIColor *)colorWithCustomerName:(NSString *)colorName{
    UIColor *customerColor;
    if ([colorName isEqualToString:@"黑"]){
        customerColor = [UIColor hexChangeFloat:@"282828"];
    } else if ([colorName isEqualToString:@"灰"]){
        customerColor = [UIColor hexChangeFloat:@"666666"];
    } else if ([colorName isEqualToString:@"浅灰"]){
        customerColor = [UIColor hexChangeFloat:@"989898"];
    } else if ([colorName isEqualToString:@"白灰"]){
        customerColor = [UIColor hexChangeFloat:@"b8b8b8"];
    } else if ([colorName isEqualToString:@"红"]){
        customerColor = [UIColor hexChangeFloat:@"ff4d61"];
    } else if ([colorName isEqualToString:@"白"]){
        customerColor = [UIColor hexChangeFloat:@"ffffff"];
    } else if ([colorName isEqualToString:@"粉"]){
        customerColor = [UIColor hexChangeFloat:@"fc687c"];
    } else if ([colorName isEqualToString:@"淡灰"]){
        customerColor = [UIColor hexChangeFloat:@"BDBDBD"];
    } else if ([colorName isEqualToString:@"分割线"]){
        customerColor = [UIColor hexChangeFloat:@"e8e8e8"];
    } else if ([colorName isEqualToString:@"背景"]){
        customerColor = [UIColor hexChangeFloat:@"f6f6f6"];
    } else if ([colorName isEqualToString:@"红高亮"]){
        customerColor = [UIColor hexChangeFloat:@"c95362"];
    } else if ([colorName isEqualToString:@"白高亮"]){
        customerColor = [UIColor hexChangeFloat:@"f2f2f2"];
    } else if ([colorName isEqualToString:@"失效"]){
        customerColor = [UIColor hexChangeFloat:@"bfbebe"];
    } else if ([colorName isEqualToString:@"快递"]){
        customerColor = [UIColor hexChangeFloat:@"5ccccc"];
    } else if ([colorName isEqualToString:@"蓝"]){
        customerColor = [UIColor hexChangeFloat:@"447ed8"];
    } else if ([colorName isEqualToString:@"绿"]){
        customerColor = [UIColor hexChangeFloat:@"63ba06"];
    } else if ([colorName isEqualToString:@"灰色3"]){
        customerColor = [UIColor hexChangeFloat:@"eeeeee"];
    } else if ([colorName isEqualToString:@"紫"]){
        customerColor = [UIColor colorWithRed:115/256. green:135/256. blue:198/256. alpha:1];
    } else if ([colorName isEqualToString:@"浅蓝"]){
        customerColor = [UIColor colorWithRed:40/255. green:139/255. blue:250/255. alpha:1];
    } else if ([colorName isEqualToString:@"黄"]){
        customerColor = [UIColor colorWithRed:255/255. green:214/255. blue:79/255. alpha:1];
    } else if ([colorName isEqualToString:@"深绿"]){
        customerColor = [UIColor colorWithRed:51/255. green:177/255. blue:92/255. alpha:1];
    } else if ([colorName isEqualToString:@"浅黑"]){      //协议字体颜色
        customerColor = [UIColor colorWithRed:40/255. green:40/255. blue:40/255. alpha:1];
    }
    
    return customerColor;
}

@end
