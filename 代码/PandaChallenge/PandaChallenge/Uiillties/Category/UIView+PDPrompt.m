//
//  UIView+PDPrompt.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UIView+PDPrompt.h"
#import <objc/runtime.h>

#define PADDING 5.

static char promptKey;
static char tapBlockKey;
@implementation UIView (PDPrompt)

- (void)showPrompt:(NSString *)promptString withImage:(UIImage *)promptImage andImagePosition:(PDPromptImagePosition)position tapBlock:(void(^)())tapBlock{
    objc_setAssociatedObject(self, &tapBlockKey, tapBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    UIView *containerView = objc_getAssociatedObject(self, &promptKey);
    if (containerView) {
        [containerView removeFromSuperview];
    }
    containerView = [[UIView alloc] initWithFrame:self.bounds];
    containerView.userInteractionEnabled = NO;
    containerView.backgroundColor = self.backgroundColor;
    
    objc_setAssociatedObject(self, &promptKey, containerView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    UIImageView *promptImageView = nil;
    if (!promptImageView) {
        promptImageView = [[UIImageView alloc] initWithImage:promptImage];
        [containerView addSubview:promptImageView];
    }
    UILabel *promptLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    promptLabel.backgroundColor = [UIColor clearColor];
    promptLabel.textColor = [UIColor lightGrayColor];
    promptLabel.text = promptString;
    promptLabel.font = [UIFont systemFontOfCustomeSize:13.];
    [promptLabel sizeToFit];
    [containerView addSubview:promptLabel];
    
    CGSize imageSize = promptImageView.bounds.size;
    CGSize labelSize = promptLabel.bounds.size;
    
    if (!promptImage) {
        promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .5f);
        if ([self isKindOfClass:[UITableView class]]) {
            promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .5f - ((UITableView *) self).contentInset.bottom / 2.);
        }
    } else {
        if (position == PDPromptImagePositionLeft) {
            CGFloat comboWidth = imageSize.width + labelSize.width + PADDING;
            CGFloat minX = ceilf(self.bounds.size.width *.5f - comboWidth * .5f);
            promptImageView.center = CGPointMake(minX + imageSize.width * .5f, self.center.y);
            promptLabel.center = CGPointMake(minX + comboWidth - labelSize.width * .5f, self.center.y);
        } else {
            promptImageView.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .3f);
            promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .35f + imageSize.height * .5f + labelSize.height * .5f + PADDING);
        }
    }
    
    [self addSubview:containerView];
    containerView.userInteractionEnabled = YES;
    // 添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [containerView addGestureRecognizer:tap];
}

-(void)tapManager{
    void(^tapBlock)() = objc_getAssociatedObject(self, &tapBlockKey);
    if (tapBlock){
        tapBlock();
    }
}

- (void)dismissPrompt {
    UIView *containerView = objc_getAssociatedObject(self, &promptKey);
    if (containerView) {
        [UIView animateWithDuration:.3f animations:^{
            containerView.alpha = 0;
        } completion:^(BOOL finished) {
            [containerView removeFromSuperview];
        }];
    }
}

@end
