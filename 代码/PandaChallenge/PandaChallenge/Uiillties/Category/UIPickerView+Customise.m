//
//  UIPickerView+Customise.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UIPickerView+Customise.h"
#import <objc/runtime.h>

//static char *pickerSelectedBlock;           /**< blockKey*/
static char *dataSourceArrKey;

@interface UIPickerView()<UIPickerViewDelegate,UIPickerViewDataSource>

@end

@implementation UIPickerView (Customise)

+(instancetype)initWithPickerDataSource:(NSArray *)dataSource{
    UIPickerView *pickerView = [[UIPickerView alloc]init];
    pickerView.delegate = pickerView;
    pickerView.dataSource = pickerView;
    pickerView.backgroundColor = [UIColor whiteColor];
    // 绑定数据源
    objc_setAssociatedObject(self, &dataSourceArrKey, dataSource, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    
    return pickerView;
}





#pragma mark - UIPickerViewDelegate
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel * pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.minimumScaleFactor = 8.;
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
        pickerLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    }
    pickerLabel.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

// 返回 picker 宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
    CGFloat componentWidth = 0.0;
    componentWidth = kScreenBounds.size.width;
    return componentWidth;
}

// 返回picker 高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 30.0;
}

// 2列


//-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
//    NSArray *dataSourceArr = objc_getAssociatedObject(self, &dataSourceArrKey);
//    return dataSourceArr.count;
//}
//
////返回当前列显示的行数
//-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
//    if (pickerView == self.refundSelfPickerView){
//        return self.refundSelfArr.count;
//    } else {
//        return  self.reasonPickerMutableArray.count;
//    }
//}
//
//-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//    if (pickerView == self.refundSelfPickerView){
//        return [self.refundSelfArr objectAtIndex:row];
//    } else {
//        return [self.reasonPickerMutableArray objectAtIndex:row];
//    }
//}
//
//-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
//    if (pickerView == self.refundSelfPickerView){
//        self.inputRefundTypeTextField.text = [self.refundSelfArr objectAtIndex:row];
//    } else {
//        self.inputReasonTextField.text = [self.reasonPickerMutableArray objectAtIndex:row];
//    }
//}



@end
