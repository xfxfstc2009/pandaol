//
//  UIScrollView+ZSPullToRefresh.m
//  RegenerationPlan
//
//  Created by SmartMin on 15/11/7.
//  Copyright © 2015年 baimifan. All rights reserved.
//

#import "UIScrollView+ZSPullToRefresh.h"


@implementation UIScrollView (ZSPullToRefresh)

- (void)addQSPullToRefreshWithActionHandler:(void (^)(void))actionHandler{
    [self addPullToRefreshWithActionHandler:actionHandler];
    
    SVPullToRefreshView *refreshView = self.pullToRefreshView;
    CGRect rect = refreshView.frame;
    rect.origin.x = 30.;
    refreshView.frame = rect;
    refreshView.arrowColor = [UIColor colorWithCustomerName:@"蓝"];
    refreshView.textColor = RGB(197, 197, 197,1);
    [refreshView setTitle:@"下拉刷新数据" forState:SVPullToRefreshStateStopped];
    [refreshView setTitle:@"释放进行刷新" forState:SVPullToRefreshStateTriggered];
    [refreshView setTitle:@"正在加载..." forState:SVPullToRefreshStateLoading];
}

- (void)addQSInfiniteScrollingWithActionHandler:(void (^)(void))actionHandler{
    [self addInfiniteScrollingWithActionHandler:actionHandler];
}

@end
