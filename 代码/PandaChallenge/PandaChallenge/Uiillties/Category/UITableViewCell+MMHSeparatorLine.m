//
//  UITableViewCell+MMHSeparatorLine.m
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "UITableViewCell+MMHSeparatorLine.h"

#define kTopTag      @"topSeparatorTag"
#define kBottomTag   @"bottomSeparatorTag"

#define hltColor     @"e3e0de"

@implementation UITableViewCell (MMHSeparatorLine)

- (void)addSeparatorLineWithType:(SeparatorType)separatorType
{
    UIImageView *topImageView = (UIImageView *)[self viewWithStringTag:kTopTag];
    UIImageView *bottomImageView = (UIImageView *)[self viewWithStringTag:kBottomTag];
    
    CGFloat separatorInset   = (separatorType == SeparatorTypeBottom || separatorType == SeparatorTypeSingle)?0:15;
    BOOL    showTopSeparator = (separatorType == SeparatorTypeHead   || separatorType == SeparatorTypeSingle)?YES:NO;
    
    if (showTopSeparator)
    {
        if (!topImageView)
        {
            topImageView = [[UIImageView alloc] init];
            [topImageView setStringTag:kTopTag];
            [topImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
            
            [topImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        }
        [topImageView setFrame:CGRectMake(0, 0,CGRectGetWidth([self frame]), 0.5)];
        [self addSubview:topImageView];
    }
    else
    {
        if (topImageView)
            [topImageView removeFromSuperview];
    }
    
    if (!bottomImageView)
    {
        bottomImageView = [[UIImageView alloc] init];
        [bottomImageView setStringTag:kBottomTag];
        [bottomImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        [bottomImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
    }
    [bottomImageView setFrame:CGRectMake( separatorInset, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]), 0.5)];
    [self addSubview:bottomImageView];
}




#pragma mark -ares
- (void)addSeparatorLineWithTypeWithAres:(SeparatorType)separatorType andUseing:(NSString *)usingVC {
    UIImageView *topImageView = (UIImageView *)[self viewWithStringTag:kTopTag];
    UIImageView *bottomImageView = (UIImageView *)[self viewWithStringTag:kBottomTag];
    
    CGFloat separatorInset   = (separatorType == SeparatorTypeBottom || separatorType == SeparatorTypeSingle)?0:LCFloat(56);
    BOOL    showTopSeparator = (separatorType == SeparatorTypeHead   || separatorType == SeparatorTypeSingle)?YES:NO;
    
    if (showTopSeparator) {
        if (!topImageView) {
            topImageView = [[UIImageView alloc] init];
            [topImageView setStringTag:kTopTag];
            [topImageView setImage:[UIImage imageWithRenderColor:[UIColor lightGrayColor] renderSize:CGSizeMake(10., 10.)]];
            [topImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor grayColor] renderSize:CGSizeMake(10., 10.)]];
        }
        [topImageView setFrame:CGRectMake(0, 0,CGRectGetWidth([self frame]), 0.5)];
        [self addSubview:topImageView];
    } else {
        if (topImageView)
            [topImageView removeFromSuperview];
    }
    
    if (!bottomImageView) {
        bottomImageView = [[UIImageView alloc] init];
        [bottomImageView setStringTag:kBottomTag];
        [bottomImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithRed:213/256. green:213/256. blue:213/256. alpha:1] renderSize:CGSizeMake(10., 10.)]];
        [bottomImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor hexChangeFloat:hltColor] renderSize:CGSizeMake(10., 10.)]];
    }
    if([usingVC isEqualToString:@"login"]){
        [bottomImageView setFrame:CGRectMake(0, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]), 0.5)];
    } else if([usingVC isEqualToString:@"leaveMessageDetail"]){
        [bottomImageView setFrame:CGRectMake( separatorInset, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame])-30, 0.5)];
    } else if ([usingVC isEqualToString:@"expressViewController"]){
        [bottomImageView setFrame:CGRectMake( separatorInset, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]) -LCFloat(56) - LCFloat(11), 0.5)];
    } else if ([usingVC isEqualToString:@"examineResult"]){         // 审核结果
        [bottomImageView setFrame:CGRectMake( 10, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]) -2 * 10, 0.5)];
    } else if ([usingVC isEqualToString:@"expressDetail"]){
        bottomImageView.frame = CGRectMake(0, CGRectGetHeight([self frame])-0.5, kScreenBounds.size.width, .5f);
    } else if ([usingVC isEqualToString:@"expressData"]){
        bottomImageView.frame = CGRectMake(LCFloat(56), CGRectGetHeight([self frame])-0.5, kScreenBounds.size.width, .5f);
    }
    
    else {
        [bottomImageView setFrame:CGRectMake( separatorInset, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame])-30, 0.5)];
    }
    
    [self addSubview:bottomImageView];
}

@end
