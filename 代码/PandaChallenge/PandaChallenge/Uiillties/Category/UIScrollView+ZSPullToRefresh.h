//
//  UIScrollView+ZSPullToRefresh.h
//  RegenerationPlan
//
//  Created by SmartMin on 15/11/7.
//  Copyright © 2015年 baimifan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVPullToRefresh.h>
@interface UIScrollView (ZSPullToRefresh)

- (void)addQSPullToRefreshWithActionHandler:(void (^)(void))actionHandler;
- (void)addQSInfiniteScrollingWithActionHandler:(void (^)(void))actionHandler;
//- (void)addQSInfiniteScrollingWithisEnd:(BOOL)isEnd ActionHandler:(void (^)(void))actionHandler;
@end
