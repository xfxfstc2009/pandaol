//
//  PDAssetsLibraryViewController.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 选择相册控制器
#import "AbstractViewController.h"

@interface PDAssetsLibraryViewController : AbstractViewController

// 选择相片方法
-(void)selectImageArrayFromImagePickerWithMaxSelected:(NSInteger)maxSelected andBlock:(void(^)(NSArray *selectedImgArr))block;
@end
