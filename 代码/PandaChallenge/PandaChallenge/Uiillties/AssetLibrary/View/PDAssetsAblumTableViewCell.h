//
//  PDAssetsAblumTableViewCell.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
@interface PDAssetsAblumTableViewCell : UITableViewCell

@property (nonatomic,strong)ALAssetsGroup *transferGroup;           /**< 传递相册*/
@property (nonatomic,assign)CGFloat transferCellHeight;
@end
