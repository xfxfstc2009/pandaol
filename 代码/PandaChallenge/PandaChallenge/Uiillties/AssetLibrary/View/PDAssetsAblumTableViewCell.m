//
//  PDAssetsAblumTableViewCell.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAssetsAblumTableViewCell.h"

@interface PDAssetsAblumTableViewCell()
@property (nonatomic,strong)UIImageView *ablumImageView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *numberLabel;

@end

@implementation PDAssetsAblumTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建图片
    self.ablumImageView = [[UIImageView alloc]init];
    self.ablumImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.ablumImageView];
    
    // 2. 创建label
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [[UIFont systemFontOfCustomeSize:15.]boldFont];
    [self addSubview:self.nameLabel];
    
    // 3. 创建数量
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.font = [UIFont systemFontOfCustomeSize:15.];
    self.numberLabel.textColor = [UIColor lightGrayColor];
    [self addSubview:self.numberLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferGroup:(ALAssetsGroup *)transferGroup{
    _transferGroup = transferGroup;
    // 1. 图片
    self.ablumImageView.image = [UIImage imageWithCGImage:transferGroup.posterImage];
    self.ablumImageView.frame = CGRectMake(LCFloat(11), LCFloat(5), (self.transferCellHeight - 2 * LCFloat(5)), (self.transferCellHeight - 2 * LCFloat(5)));
    // 2. 创建文字
    self.nameLabel.text = [transferGroup valueForProperty:ALAssetsGroupPropertyName];
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.ablumImageView.frame) + LCFloat(11), 0, nameSize.width, self.transferCellHeight);
    // 3. 创建数字
    self.numberLabel.text = [NSString stringWithFormat:@"%li",(long)[transferGroup numberOfAssets]];
    CGSize numberSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellHeight)];
    self.numberLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame), 0, numberSize.width, self.transferCellHeight);
}


@end
