//
//  PDAssetsCollectionViewCell.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/AssetsLibrary.h>

typedef void(^assetSelectedBlock)();

@interface PDAssetsCollectionViewCell : UICollectionViewCell

@property (nonatomic,assign)NSInteger fixedTransferMaxSelected;
@property (nonatomic,strong)UIView *containerView;
@property (nonatomic,strong)UIImageView *assetsImageView;
@property (nonatomic,strong)ALAsset *transferSingleAsset;                      /**< 传入的相册内容*/
@property (nonatomic,strong)NSMutableArray *transferSelectedAssets;            /**< 传过来选择的Assets*/
@property (nonatomic,copy)assetSelectedBlock assetSelectedBlock;

@end
