//
//  PDAssetsCameraViewController.m
//  PandaChallenge
//
//  Created by panda on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAssetsCameraViewController.h"
#import "PDAssetsPhotoEditViewController.h"
#import <AVFoundation/AVFoundation.h>
@interface PDAssetsCameraViewController ()<UINavigationControllerDelegate,UIImagePickerControllerDelegate>
@property (nonatomic,copy)void(^imageBlock)(UIImage *image);
@property (nonatomic,assign)BOOL isUsed;
@end

@implementation PDAssetsCameraViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    self.isUsed = [self cameraIsUsed];
    [self createCameraView];
    self.modalPresentationStyle = UIModalPresentationCurrentContext;
    if (!self.isUsed) {
        [[UIAlertView alertViewWithTitle:@"相机访问受限" message:@"请在设置中允许使用相机" buttonTitles:@[@"确定"] callBlock:NULL] show];
    }
}
/**< 返回相机访问是否受限*/
- (BOOL)cameraIsUsed
{
    AVAuthorizationStatus authStatus = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
    if (authStatus == AVAuthorizationStatusRestricted || authStatus ==AVAuthorizationStatusDenied){
        return NO;
    }else if (authStatus == AVAuthorizationStatusAuthorized){
        return YES;
    }
    return YES;
}
/**< 创建相机的View*/
- (void)createCameraView
{
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        UIImagePickerController *imagePickerController = [[UIImagePickerController alloc]init];
        imagePickerController.view.frame = self.view.bounds;
        imagePickerController.delegate = self;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        
        imagePickerController.allowsEditing = NO;
        [self.view addSubview:imagePickerController.view];
        [self addChildViewController:imagePickerController];
    }else{
        [self dismissViewControllerAnimated:YES completion:^{
            [[UIAlertView alertViewWithTitle:@"相机不可用" message:nil buttonTitles:@[@"确定"] callBlock:NULL] show];
        }];
    }
    
}
/**< 相机代理返回照片*/
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *imageOrigial = [info objectForKey:UIImagePickerControllerOriginalImage];
    __weak typeof(self) weakSelf = self;
    if (!self.isEdit) {
        [self.navigationController dismissViewControllerAnimated:YES completion:^{
            __strong typeof(weakSelf) strongSelf = weakSelf;
            strongSelf.imageBlock(imageOrigial);
            UIImageWriteToSavedPhotosAlbum(imageOrigial, nil, nil, nil);
        }];
    }else{   /**< 判断编辑状态，跳转照片编辑控制器*/
        PDAssetsPhotoEditViewController *photoEditViewController = [[PDAssetsPhotoEditViewController alloc]init];
        photoEditViewController.imageOriginal = imageOrigial;
        [self.navigationController pushViewController:photoEditViewController animated:NO];
        [photoEditViewController callBackWithEditImageBlock:^(UIImage *imageEdit) {
            __strong typeof(weakSelf) strongSelf = weakSelf;
            strongSelf.imageBlock(imageEdit);
            UIImageWriteToSavedPhotosAlbum(imageEdit, nil, nil, nil);
        }];
    }
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)callBackWithImageBlock:(void (^)(UIImage *))imageBlock
{
    self.imageBlock = imageBlock;
}
/**< 隐藏状态栏*/
- (BOOL)prefersStatusBarHidden
{
    return YES;
}
@end
