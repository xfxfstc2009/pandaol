//
//  PDAssetsPhotoEditViewController.h
//  PandaChallenge
//
//  Created by panda on 16/3/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDAssetsPhotoEditViewController : AbstractViewController
/**< 1.原始图片, 必须设置*/
@property (nonatomic, strong) UIImage *imageOriginal;
/**< 2.图片的尺寸,剪切框，最好是需求图片的2x, 默认是CGSizeMake(ScreenWidth, ScreenWidth); */
@property (nonatomic, assign) CGSize sizeClip;
/**< 返回已编辑好的照片(调PDAssetsCameraViewController的image回调方法)*/
- (void)callBackWithEditImageBlock:(void(^)(UIImage *imageEdit))imageBlock;
@end
