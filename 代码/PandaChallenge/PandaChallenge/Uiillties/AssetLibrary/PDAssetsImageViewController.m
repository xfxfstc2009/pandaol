//
//  PDAssetsImageViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAssetsImageViewController.h"
#import "PDReuseScrollView.h"
#import "PDScrollViewAssetView.h"
@interface PDAssetsImageViewController()<PDReuseScrollViewDataSource,PDReuseScrollViewDelegate>
@property (nonatomic,strong)PDReuseScrollView *mainScrollView;
@property (nonatomic,strong)NSMutableArray *scrollViewMutableArr;
@property (nonatomic,strong)UIToolbar *topToolBar;
@property (nonatomic,strong)UIToolbar *bottomBar;
@property (nonatomic,assign)BOOL isShow;                /**< 判断是否显示*/
@property (nonatomic,strong)UIImageView *animationImgView;  /**< 用作动画的view */

@end

@implementation PDAssetsImageViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self createView];
    [self createTooBar];
    [self createAnimationView];
    [self animationWithShow];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.view.backgroundColor = [UIColor blackColor];
    self.isShow = NO;
}

#pragma mark - createView
-(void)createView{
    self.mainScrollView = [[PDReuseScrollView alloc]initWithFrame:self.view.bounds];
    self.mainScrollView.reuseDelegate = self;
    self.mainScrollView.reuseDataSource = self;
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.backgroundColor = [UIColor blackColor];
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    self.mainScrollView.showsVerticalScrollIndicator = NO;
    self.mainScrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    // 添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self.mainScrollView addGestureRecognizer:tap];
}

#pragma mark -
- (PDReuseView *)scrollView:(PDReuseScrollView *)scrollView viewForItemAtIndex:(NSInteger)index {
    static NSString *scrollViewIdentify = @"scrollViewIdentify";
    PDScrollViewAssetView *view = [scrollView dequeueReusableViewWithIdentifier:scrollViewIdentify];
    if (!view) {
        view = [[PDScrollViewAssetView alloc]initWithReuseIdentifier:scrollViewIdentify];
    }
    if (index < self.transferAssetsArr.count){
        view.transferAsset = [self.transferAssetsArr objectAtIndex:index];
    }

    return view;
}

- (NSInteger)numberOfItemsInScrollView:(PDReuseScrollView *)scrollView {
    return self.transferAssetsArr.count;
}

- (UIEdgeInsets)scrollView:(PDReuseScrollView *)scrollView insetForForItemAtIndex:(NSInteger)index {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

#pragma mark - createTooBar
-(void)createTooBar{
    self.topToolBar = [[UIToolbar alloc]init];
    self.topToolBar.backgroundColor = [UIColor clearColor];
    self.topToolBar.barStyle = UIBarStyleBlackOpaque;
    self.topToolBar.frame = CGRectMake(0, -64 , kScreenBounds.size.width, 64);
    [self.view addSubview:self.topToolBar];
    
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    __weak typeof(self)weakSelf = self;
    [leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf animationWithPop];
    }];
    leftButton.frame = CGRectMake(LCFloat(11), 0, 64, 64);
    [self.topToolBar addSubview:leftButton];
    
    // 2. 创建bottomBar
    self.bottomBar = [[UIToolbar alloc]init];
    self.bottomBar.backgroundColor = [UIColor clearColor];
    self.bottomBar.barStyle = UIBarStyleBlackOpaque;
    self.bottomBar.frame = CGRectMake(0, kScreenBounds.size.height, kScreenBounds.size.width, 44);
    [self.view addSubview:self.bottomBar];
}

#pragma mark - tapManagerw
-(void)tapManager{
    [UIView animateWithDuration:.3f animations:^{
        self.isShow = !self.isShow;
        self.topToolBar.orgin_y = (self.isShow ? 0 : - self.topToolBar.size_height);
        self.bottomBar.orgin_y = (self.isShow ? kScreenBounds.size.height - self.bottomBar.size_height : kScreenBounds.size.height);
    }];
}

#pragma mark - Animation
-(void)animationWithShow{
    [UIView animateWithDuration:.4f delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.animationImgView.frame = self.view.bounds;
    } completion:^(BOOL finished) {
        [self.mainScrollView setContentOffset:CGPointMake(self.indexAsset * kScreenBounds.size.width, 0)];
        [self.mainScrollView reloadData];
        [self.view addSubview:self.mainScrollView];
        [self.view sendSubviewToBack:self.mainScrollView];
        [self.animationImgView removeFromSuperview];
        self.animationImgView = nil;
        [self tapManager];
    }];
}

-(void)animationWithPop{

    [UIView animateWithDuration:.4f animations:^{
//        self.mainScrollView.frame = self.transferRect;

    } completion:^(BOOL finished) {
        [self.navigationController popViewControllerAnimated:NO];
    }];
}

-(void)createAnimationView{
    self.animationImgView = [[UIImageView alloc]init];
    self.animationImgView.backgroundColor = [UIColor clearColor];
    self.animationImgView.frame = self.transferRect;
    UIImage *originalImage = [UIImage imageWithCGImage:[[self.transferAsset defaultRepresentation] fullScreenImage]];
    originalImage = [UIImage scaleDown:originalImage withSize:CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.width * originalImage.size.height/originalImage.size.width)];
    self.animationImgView.image = originalImage;
    self.animationImgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:self.animationImgView];
}



@end
