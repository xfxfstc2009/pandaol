//
//  PDAssetsAblumsViewController.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

// 相册列表
#import "AbstractViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

typedef void(^ablumSelectedBlock)(ALAssetsGroup *ablum);
@interface PDAssetsAblumsViewController : AbstractViewController

@property (nonatomic,strong)NSArray *ablumsArr;
@property (nonatomic,copy)ablumSelectedBlock ablumSelectrdBlock;

@end
