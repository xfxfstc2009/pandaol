//
//  PDAssetsLibraryViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAssetsLibraryViewController.h"
#import <objc/runtime.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "PDAssetsCollectionViewCell.h"
#import "PDAssetsAblumsViewController.h"
#import "PDAssetsImageViewController.h"
#define k_assets_margin LCFloat(10)

static char selectedImageArrManagerKey;        // 选择照片方法

@interface PDAssetsLibraryViewController()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)NSMutableArray *ablumsMutableArr;                          /**< 相册数量*/
@property (nonatomic,strong)NSMutableArray *assetsMutableArr;                          /**< 照片数量*/
@property (nonatomic,strong)NSMutableArray *selectedAssetsMutableArr;                  /**< 选中的照片内容*/
@property (nonatomic,strong)ALAssetsLibrary *assetsLibrary;                            /**< 所有的相册内容*/
@property (nonatomic,strong)UICollectionView *assetsCollectionView;                    /**< 相册collectionView*/
@property (nonatomic,strong)UITableView *ablumTableView;
@property (nonatomic,strong)UIImage *blurImage;

// pageSetting
@property (nonatomic,strong)UIButton *navRightButton;
@property (nonatomic,assign)NSInteger maxSelected;
@end

@implementation PDAssetsLibraryViewController
-(void)selectImageArrayFromImagePickerWithMaxSelected:(NSInteger)maxSelected andBlock:(void(^)(NSArray *selectedImgArr))block{
    self.maxSelected = maxSelected;
    objc_setAssociatedObject(self, &selectedImageArrManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#ifdef DEBUG
-(void)dealloc{
    NSLog(@"释放了");
}
#endif

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                                 // 页面初始胡
    [self arrayWithInit];                               // 数据初始化
    [self judgmentAuthorityWithAssetLibrary];           // 权限访问
    [self createCollectionView];                        // 创建一个collectionView
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"相册";
    __weak typeof(self)weakSelf = self;
    self.navRightButton = [weakSelf rightBarButtonWithTitle:@"确定" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf assetsChangeImageManager];
    }];
    [weakSelf leftBarButtonWithTitle:@"取消" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.navigationController.childViewControllers.count == 1){
            [strongSelf dismissViewControllerAnimated:YES completion:NULL];
        } else {
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    
    [self createNavigationBar];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.ablumsMutableArr = [NSMutableArray array];
    self.selectedAssetsMutableArr = [NSMutableArray array];
    self.assetsLibrary = [[ALAssetsLibrary alloc]init];
}

#pragma mark - 【1】获取当前相册权限
-(void)judgmentAuthorityWithAssetLibrary{
    if ([ALAssetsLibrary authorizationStatus] == ALAuthorizationStatusDenied){       // 用户已经明确否认了这一照片数据的应用程序访问.
        [[UIAlertView alertViewWithTitle:@"提示" message:@"请在iPhone的\"设置－隐私－照片\"选项中，允许访问你的手机相册" buttonTitles:@[@"确定"] callBlock:NULL]show];
    }  else {
        [self searchAllAlassetsLibrary];
    }
}

#pragma mark - 【2】搜索并获取所有相册内容
-(void)searchAllAlassetsLibrary{
    __weak typeof(self)weakSelf = self;
    // 搜索保存的照片
    [weakSelf searchAlassetLibraryWithType:ALAssetsGroupSavedPhotos withSuccessBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 过滤相片
        if (group){
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
            // 相册增加
            if (group.numberOfAssets > 0){
                [strongSelf.ablumsMutableArr addObject:group];
            }
            // 如果当前的相册是有相片的，就加入数组
            if (strongSelf.ablumsMutableArr.count == 1){
                [strongSelf searchAllAssetsWithGroup:group];
            }
        }
    }];
    
    // 搜索相册内容
    [weakSelf searchAlassetLibraryWithType:ALAssetsGroupAlbum withSuccessBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (group) {
            [group setAssetsFilter:[ALAssetsFilter allPhotos]];
        }
        if (group.numberOfAssets > 0) {
            [strongSelf.ablumsMutableArr addObject:group];
        }
    }];
}
#pragma mark - 【搜索相册方法】
-(void)searchAlassetLibraryWithType:(ALAssetsGroupType)type withSuccessBlock:(void(^)(ALAssetsGroup *group, BOOL *stop))searchBlock{
    __weak typeof(self)weakSelf = self;
    [weakSelf.assetsLibrary enumerateGroupsWithTypes:type usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        if (!weakSelf){
            return ;
        }
        return searchBlock(group, stop);
    } failureBlock:^(NSError *error) {
        [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
    }];
}

#pragma mark - 【3】搜索当前group内的照片数量
-(void)searchAllAssetsWithGroup:(ALAssetsGroup *)group{
    if (!self.assetsMutableArr) {
        self.assetsMutableArr = [NSMutableArray array];
    } else {
        [self.assetsMutableArr removeAllObjects];
    }
    __weak typeof(self)weakSelf = self;
    // 遍历相册内容
    [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (result){
            [strongSelf.assetsMutableArr addObject:result];
        }
    }];
    self.barMainTitle = [group valueForProperty:ALAssetsGroupPropertyName];
    [self.assetsCollectionView reloadData];
    [self.assetsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:[self.assetsCollectionView numberOfItemsInSection:0] - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionTop animated:NO];
}

#pragma mark - 【UICollectionView】
-(void)createCollectionView{
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    self.assetsCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    self.assetsCollectionView.backgroundColor = [UIColor clearColor];
    self.assetsCollectionView.showsVerticalScrollIndicator = NO;
    [self.assetsCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
    self.assetsCollectionView.delegate = self;
    self.assetsCollectionView.dataSource = self;
    self.assetsCollectionView.showsHorizontalScrollIndicator = NO;
    self.assetsCollectionView.scrollsToTop = YES;
    self.assetsCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.assetsCollectionView];
    
    [self.assetsCollectionView registerClass:[PDAssetsCollectionViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.assetsMutableArr.count;
}

#pragma mark - UICollectionViewDelegate
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PDAssetsCollectionViewCell *cell = (PDAssetsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify"  forIndexPath:indexPath];
    cell.transferSingleAsset = [self.assetsMutableArr objectAtIndex:indexPath.row];
    cell.transferSelectedAssets = self.selectedAssetsMutableArr;
    cell.fixedTransferMaxSelected = self.maxSelected;
    __weak typeof(self)weakSelf = self;
    cell.assetSelectedBlock = ^(){
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf selectedAssetsManager];
    };
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((kScreenBounds.size.width - 4 * k_assets_margin) / 3., (kScreenBounds.size.width - 4 * k_assets_margin) / 3.);
}

#pragma mark - UICollectionViewDelegate
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,k_assets_margin,0,k_assets_margin);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    CGSize size = {kScreenBounds.size.width,LCFloat(20)};
    return size;
}

#pragma mark cell的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ALAsset *selectedAsset = [self.assetsMutableArr objectAtIndex:indexPath.row];
    PDAssetsCollectionViewCell *assetCell = (PDAssetsCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    // 获取frame
    CGRect convertItemFrame = [assetCell convertRect:assetCell.assetsImageView.frame toView:self.view.window];
    
    PDAssetsImageViewController *imgDetailViewController = [[PDAssetsImageViewController alloc]init];
    imgDetailViewController.transferAssetsArr = self.assetsMutableArr;
    imgDetailViewController.transferRect = convertItemFrame;
    imgDetailViewController.transferAsset = selectedAsset;
    imgDetailViewController.indexAsset = indexPath.row;
    [self.navigationController pushViewController:imgDetailViewController animated:NO];
}

#pragma mark - createNavigationBar
-(void)createNavigationBar{
    self.barMainTitle = @"添加图片";
    self.barSubTitle = @"相机胶卷";
    
    // 创建button
    UIButton *showAblumsButton = [UIButton buttonWithType:UIButtonTypeCustom];
    showAblumsButton.frame = self.navigationItem.titleView.bounds;
    [showAblumsButton setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateNormal];
    [showAblumsButton setImage:[UIImage imageNamed:@"arrow_down"] forState:UIControlStateHighlighted];
    CGFloat verticalInset = (CGRectGetHeight(showAblumsButton.frame)-8)/2.;
    CGFloat horizontalInset = CGRectGetWidth(showAblumsButton.frame)-12;
    showAblumsButton.imageEdgeInsets = UIEdgeInsetsMake(verticalInset, horizontalInset-48, verticalInset, 48);
    [self.navigationItem.titleView addSubview:showAblumsButton];
    __weak typeof(self)weakSelf = self;
    [showAblumsButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDAssetsAblumsViewController *assetsAblumsViewController = [[PDAssetsAblumsViewController alloc]init];
        assetsAblumsViewController.ablumsArr = strongSelf.ablumsMutableArr;
        assetsAblumsViewController.ablumSelectrdBlock = ^(ALAssetsGroup *ablum){
            [strongSelf searchAllAssetsWithGroup:ablum];
            [strongSelf actionShowAblums];
            [strongSelf.assetsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:[self.assetsCollectionView numberOfItemsInSection:0] - 1 inSection:0] atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
        };
        [self.navigationController pushViewController:assetsAblumsViewController animated:YES];
    }];
}

#pragma mark -actionShowAblums
- (void)actionShowAblums {
    if (self.ablumsMutableArr.count == 0) {
        if (self.ablumsMutableArr.count == 0) {
            [[UIAlertView alertViewWithTitle:@"相册为空" message:@"相册为空" buttonTitles:@[@"确定"] callBlock:NULL] show];
            return ;
        }
    }
    BOOL hadShowAblumTableView = (CGRectGetHeight(self.ablumTableView.frame) == CGRectGetHeight(self.view.bounds)-64);
    UIImageView *blurImageView = (UIImageView *)[self.navigationController.view viewWithStringTag:@"blurImageView"];
    if (!blurImageView) {
        blurImageView = [[UIImageView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        blurImageView.userInteractionEnabled = YES;
        blurImageView.stringTag = @"blurImageView";
        blurImageView.image =  [[UIImage screenShoot:self.navigationController.view] applyExtraWithAlAssetLibrary];
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDismissAblums)];
        [blurImageView addGestureRecognizer:gesture];
    }
    if (!hadShowAblumTableView) {
        [self.navigationController.view insertSubview:blurImageView belowSubview:self.ablumTableView];
    }
    
    if (hadShowAblumTableView) {
        self.ablumTableView.frame = CGRectMake(10, 64, CGRectGetWidth(self.view.bounds)-20, 0);
        [blurImageView removeFromSuperview];
    } else {
        [self.ablumTableView reloadData];
        self.ablumTableView.frame = CGRectMake(10, 64, CGRectGetWidth(self.view.bounds)-20, CGRectGetHeight(self.view.bounds)-64);
    }
}

#pragma mark -actionDismissAblums
- (void)actionDismissAblums{
    //    [self actionShowAblums];
    
    PDAssetsAblumsViewController *assetsAblumsViewController = [[PDAssetsAblumsViewController alloc]init];
    assetsAblumsViewController.ablumsArr = self.ablumsMutableArr;
    [self.navigationController pushViewController:assetsAblumsViewController animated:YES];
}

#pragma mark 判断nav右侧按钮是否可以点击
-(void)selectedAssetsManager{
    if (self.selectedAssetsMutableArr.count){
        self.navRightButton.enabled = YES;
        [self.navRightButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    } else {
        self.navRightButton.enabled = NO;
        [self.navRightButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    }
}

#pragma mark 选中的asset进行转换成img
-(void)assetsChangeImageManager{
    NSMutableArray *selectedImageMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < self.selectedAssetsMutableArr.count;i++){
        ALAsset *singleAsset = [self.selectedAssetsMutableArr objectAtIndex:i];
        ALAssetRepresentation* representation = [singleAsset defaultRepresentation];
        UIImage *singleImage = [UIImage imageWithCGImage:[representation fullScreenImage]];
        [selectedImageMutableArr addObject:singleImage];
    }
    void(^selectedImgArrBlock)(NSArray *selectedImgArr) = objc_getAssociatedObject(self, &selectedImageArrManagerKey);
    if (selectedImgArrBlock){
        selectedImgArrBlock(selectedImageMutableArr);
        if (self.navigationController.viewControllers.count == 1){
            [self dismissViewControllerAnimated:YES completion:NULL];
        } else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}



@end
