//
//  PDAssetsImageViewController.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>

@interface PDAssetsImageViewController : AbstractViewController

@property (nonatomic,strong)NSArray *transferAssetsArr;         /**< 传递过来的图片数组*/
@property (nonatomic,assign)NSInteger indexAsset;              /**< 传递过来的index*/
@property (nonatomic,assign)CGRect transferRect;                    /**< 上个页面传递过来的rect*/
@property (nonatomic,strong)ALAsset *transferAsset;             /**< 传递过去的asset*/

@end
