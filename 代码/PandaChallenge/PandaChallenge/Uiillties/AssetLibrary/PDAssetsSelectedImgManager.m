//
//  PDAssetsSelectedImgManager.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAssetsSelectedImgManager.h"
#import "PDAssetsLibraryViewController.h"
@implementation PDAssetsSelectedImgManager

-(void)selectAssetsInController:(UIViewController *)controller WithMaxSelected:(NSInteger)maxSelected andBlock:(void(^)(NSArray *selectedImgArr))block{
    [[UIActionSheet actionSheetWithTitle:nil buttonTitles:@[@"取消",@"拍照",@"从手机相册选择"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        if (buttonIndex == 2){      // 取消
        
        } else if (buttonIndex == 0){       // 拍照
        
        } else if (buttonIndex == 1){       // 相册筛选
            PDAssetsLibraryViewController *assetlibraryViewController = [[PDAssetsLibraryViewController alloc]init];
            [assetlibraryViewController selectImageArrayFromImagePickerWithMaxSelected:maxSelected andBlock:^(NSArray *selectedImgArr) {     
                if (block){
                    block(selectedImgArr);
                }
            }];
            UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:assetlibraryViewController];
            [controller presentViewController:navController animated:YES completion:NULL];
        }
    }]showInView:controller.view];
}

@end
