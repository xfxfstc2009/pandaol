//
//  PDAssetsCameraViewController.h
//  PandaChallenge
//
//  Created by panda on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDAssetsCameraViewController : AbstractViewController
/**
 *  设置是否需要编辑
 */
@property (nonatomic,assign)BOOL isEdit;
/**
 *  返回拍摄的照片
 *
 *  @param imageBlock 
 */
- (void)callBackWithImageBlock:(void(^)(UIImage *image))imageBlock;
@end
