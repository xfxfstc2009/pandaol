//
//  PDAssetsAblumsViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAssetsAblumsViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "PDAssetsAblumTableViewCell.h"
@interface PDAssetsAblumsViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)ALAssetsLibrary *assetsLibrary;                            /**< 所有的相册内容*/
@property (nonatomic,strong)UITableView *ablumTableView;

@end

@implementation PDAssetsAblumsViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"相册";
}


#pragma mark - UITableView
-(void)createTableView{
    if (!self.ablumTableView){
        self.ablumTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.ablumTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.ablumTableView.delegate = self;
        self.ablumTableView.dataSource = self;
        self.ablumTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.ablumTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.ablumTableView];
    }
}

#pragma makr - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.ablumsArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentify = @"cellIdentify";
    PDAssetsAblumTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentify];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDAssetsAblumTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentify];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferGroup = (ALAssetsGroup *)[self.ablumsArr objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    ALAssetsGroup *selectedAblum = [self.ablumsArr objectAtIndex:indexPath.row];
    if (self.ablumSelectrdBlock){
        self.ablumSelectrdBlock(selectedAblum);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

@end
