//
//  PDAssetsSelectedImgManager.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 选择照片方法
#import <Foundation/Foundation.h>

@interface PDAssetsSelectedImgManager : NSObject

-(void)selectAssetsInController:(UIViewController *)controller WithMaxSelected:(NSInteger)maxSelected andBlock:(void(^)(NSArray *selectedImgArr))block;
@end
