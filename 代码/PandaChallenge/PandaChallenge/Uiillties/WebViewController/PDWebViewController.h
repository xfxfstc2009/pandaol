//
//  PDWebViewController.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDWebViewController : AbstractViewController<UIWebViewDelegate>
/** 传递的网页*/
@property (nonatomic, copy) NSString *urlStr;
@property (nonatomic, strong) UIWebView *webView;

@end
