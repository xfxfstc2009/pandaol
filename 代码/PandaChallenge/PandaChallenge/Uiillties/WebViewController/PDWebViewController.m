//
//  PDWebViewController.m
//  PandaChallenge
//
//  Created by 盼达 on 16/4/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDWebViewController.h"

#define BarButtonItemColor  
#define Bar_Item_Width      LCFloat(60)

@interface PDWebViewController ()
@property (nonatomic, strong) UIButton *backItem;
@property (nonatomic, strong) UIButton *closeItem;
@end
@implementation PDWebViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configWebView];
//    [self configNavigationBar];
    [PDHUD showWithStatus:@"加载中..." maskType:WSProgressHUDMaskTypeDefault delay:0];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [PDHUD dismiss];
}

- (void)configWebView {
    UIWebView *webView = [[UIWebView alloc] initWithFrame:kScreenBounds];
    self.webView = webView;
    for (id subview in webView.subviews) {
        if ([[subview class] isSubclassOfClass: [UIScrollView class]])
            ((UIScrollView *)subview).bounces = NO;
    }
    webView.scrollView.bounces = NO;
    webView.delegate = self;
    [self.view addSubview:webView];
}

- (void)configNavigationBar {
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, Bar_Item_Width, 44)];
    self.backItem = backButton;
    [backButton setImage:[UIImage imageNamed:@"login_back"] forState:UIControlStateNormal];
    [backButton setTitle:@"返回" forState:UIControlStateNormal];
    [backButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(clickedBackItem:) forControlEvents:UIControlEventTouchUpInside];
/*
    UIButton * closeButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, Bar_Item_Width, 44)];
    self.closeItem = closeButton;
    [closeButton setTitle:@"关闭" forState:UIControlStateNormal];
    [closeButton addTarget:self action:@selector(clickedCloseItem:) forControlEvents:UIControlEventTouchUpInside];
    closeButton.hidden = YES;
*/
    UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithCustomView:backButton];
 //   UIBarButtonItem *closeItem = [[UIBarButtonItem alloc] initWithCustomView:closeButton];

    self.navigationItem.leftBarButtonItems = @[backItem];
    
}

- (void)setUrlStr:(NSString *)urlStr {
    _urlStr = urlStr;
    //清除UIWebView的缓存
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:_urlStr] cachePolicy:NSURLRequestReloadIgnoringCacheData timeoutInterval:10.0]];
}

#pragma mark - button methods
- (void)clickedBackItem:(UIButton *)button {
    if (self.webView.canGoBack) {
        [self.webView goBack];
//        self.closeItem.hidden = NO;
    } else {
        [self clickedCloseItem:nil];
    }
}

- (void)clickedCloseItem:(UIButton *)button {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [PDHUD dismiss];
    
    [self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitUserSelect='none';"];
    [self.webView stringByEvaluatingJavaScriptFromString:@"document.documentElement.style.webkitTouchCallout='none';"];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    [PDHUD dismiss];
}


@end
