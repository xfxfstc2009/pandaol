//
//  PDConstants.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef PDConstants_h
#define PDConstants_h

// System Default Sets
#define USERDEFAULTS     [NSUserDefaults standardUserDefaults]
#define NOTIFICENTER     [NSNotificationCenter defaultCenter]


// System
#define IS_IOS7_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 6.99)
#define IS_IOS8_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 7.99)
#define IS_IOS9_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 8.99)

#define iphone5  ([UIScreen mainScreen].bounds.size.height == 568)
#define iphone6  ([UIScreen mainScreen].bounds.size.height == 667)
#define iphone6Plus  ([UIScreen mainScreen].bounds.size.height == 736)
#define iphone4  ([UIScreen mainScreen].bounds.size.height == 480)
#define ipadMini2  ([UIScreen mainScreen].bounds.size.height == 1024)

// Frame
#define kScreenBounds               [[UIScreen mainScreen] bounds]
#define kScreenWidth                kScreenBounds.size.width
#define kScreenHeight               kScreenBounds.size.height

//rightMenuCellHeight
#define RIGHTMENU_CELL_HEIGHT 40

// System Style
#define RGB(r, g, b,a)    [UIColor colorWithRed:(r)/255. green:(g)/255. blue:(b)/255. alpha:a]
#define BACKGROUND_VIEW_COLOR       [UIColor hexChangeFloat:@"F2F2F2"]
#define NAVBAR_COLOR RGB(255, 255, 255,1)

// log
#define debugMethod() PDOLLog(@"%s", __func__)
#ifdef DEBUG
#define PDOLLog(...)  NSLog(__VA_ARGS__)
#else
#define PDOLLog(...)
#endif


// Notification_Name
#define WX_PAY_NOTIFICATION   @"WXPayNotification"
#define ALI_PAY_NOTIFICATION  @"AliPayNotification"

// alipay Notification
static NSString *const PDAlipayNotification = @"PDAlipayNotification";/**< 支付宝支付通知*/

// NSUserDefaults constants
#define UD_CLUB_CAN_SHOW      @"clubProtocolIsShow"

// Cell Margins
#define ConfigCellMargin(value) \
- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexpath { \
    cell.separatorInset = UIEdgeInsetsMake(0, value, 0, value); \
    cell.layoutMargins = UIEdgeInsetsMake(0, value, 0, value); \
    cell.preservesSuperviewLayoutMargins = NO; \
}


// deviCeId
#define DeviceId @"device"

#endif /* PDConstants_h */
