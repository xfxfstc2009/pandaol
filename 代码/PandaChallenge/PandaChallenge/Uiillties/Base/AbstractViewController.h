//
//  AbstractViewController.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCloudView.h"

@interface AbstractViewController : UIViewController
@property (nonatomic,strong)PDCloudView *cloudView;             /**< 背景view*/

@property (nonatomic,strong)UILabel *barMainTitleLabel;

@property (nonatomic,copy)NSString *barMainTitle;
@property (nonatomic,copy)NSString *barSubTitle;

@property (nonatomic,copy)NSString *controllerTitle;

-(void)authorizeWithCompletionHandler:(void (^)(BOOL isSuccess))handler;

- (void)hidesBackButton;

- (UIButton *)leftBarButtonWithTitle:(NSString *)title barNorImage:(UIImage *)norImage barHltImage:(UIImage *)hltImage action:(void(^)(void))actionBlock;

- (UIButton *)rightBarButtonWithTitle:(NSString *)title barNorImage:(UIImage *)norImage barHltImage:(UIImage *)hltImage action:(void(^)(void))actionBlock;

- (void)hidesTabBarWhenPushed;

- (void)hidesTabBar:(BOOL)hidden animated:(BOOL)animated;

// 两个item的时候改变titleLabel的位置居中
- (void)changeTitleLabelToCenterWithItemWidth:(CGFloat)itemWidth;

@end
