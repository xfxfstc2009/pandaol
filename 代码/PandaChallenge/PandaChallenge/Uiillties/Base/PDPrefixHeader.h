//
//  PDPrefixHeader.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef PDPrefixHeader_h
#define PDPrefixHeader_h

#define GWREQUEST_API_HOST  @"www.giganticwhale.top/MobileAPI"
#define GWREQUEST_API_PORT  @""

#define GWREQUEST_API_VER   @"MobileAPI"
#define APP_VER        [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleShortVersionString"]
#define BUILD_VER      [[NSBundle mainBundle].infoDictionary objectForKey:(NSString *)kCFBundleVersionKey]

/// USERDEFAULT
#define CURRENT_USER_NAME_KEY   @"CURRENT_USER_NAME_KEY"
#define CURRENT_USER_PASS_KEY   @"CURRENT_USER_PASS_KEY"

#endif /* LHZC_Prefix_h */

#ifdef __OBJC__
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import "PDConstants.h"
#import "PDTool.h"

#import "PDFetchModel.h"
#import <AFNetworking.h>
#import "PDUploadFileModel.h"

////// Category/////
#import "UIActionSheet+Customise.h"
#import "UIAlertView+Customise.h"
#import "UIButton+Customise.h"
#import "UITextField+CustomShowBar.h"
#import "UITextView+Customise.h"
#import "NSString+LCCalcSize.h"
#import "NSDate+Utilities.h"
#import "UIView+LCGeometry.h"
#import "UIImage+ImageEffects.h"
#import "UIView+StringTag.h"
#import "UIColor+Extended.h"
#import "UIFont+Customise.h"


#endif /* PDPrefixHeader_h */
