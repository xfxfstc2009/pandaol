//
//  AbstractViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import <objc/runtime.h>
#import "PDChallengeViewController.h"
#import "PDChallengeDirectManager.h"
#define BAR_BUTTON_FONT       [UIFont systemFontOfSize:14.]
#define BAR_MAIN_TITLE_FONT   [UIFont boldSystemFontOfSize:16.]
#define BAR_SUB_TITLE_FONT    [UIFont systemFontOfSize:12.]
#define BAR_TITLE_PADDING_TOP -3.
#define BAR_TITLE_MAX_WIDTH   200

static char buttonActionBlockKey;
@interface AbstractViewController()<RESideMenuDelegate>
@property (nonatomic,strong)UIView *barTitleView;
@property (nonatomic,strong)UILabel *barSubTitleLabel;

@property (nonatomic,strong)UIButton *openDrawerButton;
@property (nonatomic,strong)UILabel *barTitleLabel;
@end

@implementation AbstractViewController

-(void)viewDidLoad {
    [super viewDidLoad];
    [self createCloudView];
    [self createBarTitleView];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;             // 顶部偏移
    self.modalPresentationCapturesStatusBarAppearance = NO; //
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    self.view.autoresizesSubviews = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view.backgroundColor = BACKGROUND_VIEW_COLOR;
    //去掉navigationBar底部线
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageWithRenderColor:BACKGROUND_VIEW_COLOR renderSize:CGSizeMake(kScreenBounds.size.width, 64)] forBarPosition:UIBarPositionAny barMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc]init]];

    __weak typeof(self)weakSelf = self;
    if (self != [self.navigationController.viewControllers firstObject]) {
        __weak typeof(self) weakSelf = self;
        [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"basc_nav_back"] barHltImage:[UIImage imageNamed:@"basc_nav_back"] action:^{
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    } else {
        if ([weakSelf isKindOfClass:[PDChallengeViewController class]]){
            [weakSelf leftBarButtonWithTitle:nil barNorImage: [UIImage imageNamed:@"icon_home_slider"] barHltImage:nil action:^{
                if (!weakSelf){
                    return ;
                }
                
            }];
            
        } else {[weakSelf leftBarButtonWithTitle:nil barNorImage: [UIImage imageNamed:@"basc_nav_back"] barHltImage:nil action:^{
            if (!weakSelf){
                return ;
            }
            if ([weakSelf isKindOfClass:[PDChallengeViewController class]]){
                
            } else {
                [PDChallengeDirectManager directToHomeViewController];
            }
        }];
            
        }
    }
}

#pragma mark - 自动登录
-(void)authorizeWithCompletionHandler:(void (^)(BOOL isSuccess))handler{
    if ([[PDLoginModel shareInstance] hasLoggedIn]){        // 登录
        handler(YES);
    } else {
        // 1. 判断以前是否有登陆过
        NSString *userName = [PDTool userDefaultGetWithKey:CURRENT_USER_NAME_KEY];
        NSString *password = [PDTool userDefaultGetWithKey:CURRENT_USER_PASS_KEY];
        __weak typeof(self)weakSelf = self;
        if (userName.length && password.length){
            [[PDLoginModel shareInstance] loginWithType:PDLoginModelCurrent user:userName password:password completionHandler:^(BOOL isSucceeded, NSError *error) {
                if (!weakSelf){
                    return ;
                }
                if (isSucceeded){
                    handler(YES);
                } else {
                    handler(NO);
                    [PDChallengeDirectManager directToLoginViewController];
                }
            }];
        } else {
            handler(NO);
            [PDChallengeDirectManager directToLoginViewController];
        }
    }
}



- (void)createBarTitleView {
    _barTitleView = [[UIView alloc] initWithFrame:CGRectMake((kScreenBounds.size.width - LCFloat(140)) / 2., 0, LCFloat(140), 44)];
    _barTitleView.backgroundColor = [UIColor clearColor];
    _barTitleView.clipsToBounds = YES;
    
    _barMainTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _barMainTitleLabel.backgroundColor = [UIColor clearColor];
    _barMainTitleLabel.font = [UIFont fontWithName:@"Georgia" size:16.];
    _barMainTitleLabel.textColor = [UIColor blackColor];
    
    [_barTitleView addSubview:_barMainTitleLabel];
    
    _barSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _barSubTitleLabel.backgroundColor = [UIColor clearColor];
    _barSubTitleLabel.font = BAR_SUB_TITLE_FONT;
    _barSubTitleLabel.textColor = [UIColor blackColor];
    [_barTitleView addSubview:_barSubTitleLabel];
    
    
    self.navigationItem.titleView = _barTitleView;
}

- (void)setBarMainTitle:(NSString *)barMainTitle {
    _barMainTitle = [barMainTitle copy];
    _barMainTitleLabel.text = _barMainTitle;
    
    [_barMainTitleLabel sizeToFit];
    CGRect rect = _barMainTitleLabel.bounds;
    rect.size.width = MIN(rect.size.width, BAR_TITLE_MAX_WIDTH);
    _barMainTitleLabel.bounds = rect;
    
    [self resetBarTitleView];
}

- (void)setBarSubTitle:(NSString *)barSubTitle {
    _barSubTitle = [barSubTitle copy];
    _barSubTitleLabel.text = _barSubTitle;
    
    [_barSubTitleLabel sizeToFit];
    CGRect rect = _barSubTitleLabel.bounds;
    rect.size.width = MIN(rect.size.width, BAR_TITLE_MAX_WIDTH);
    _barSubTitleLabel.bounds = rect;
    
    [self resetBarTitleView];
}

- (void)resetBarTitleView {
    CGSize mainTitleSize = _barMainTitleLabel.bounds.size;
    CGSize subTitleSize = _barSubTitleLabel.bounds.size;
    
    CGFloat titleViewHeight = mainTitleSize.height + subTitleSize.height;
    if (_barMainTitle.length && _barSubTitle.length) {
        titleViewHeight += BAR_TITLE_PADDING_TOP;
    }
    
    _barTitleView.bounds = CGRectMake(0., 0., BAR_TITLE_MAX_WIDTH, titleViewHeight);
    if ([PDTool isEmpty:_barSubTitle]) {
        if (titleViewHeight < 44) {
            titleViewHeight = 44;
        }
        _barTitleView.bounds = CGRectMake(0., 0., BAR_TITLE_MAX_WIDTH, titleViewHeight);
        _barMainTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, 22);
    } else {
        _barMainTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, _barMainTitleLabel.bounds.size.height*0.5);
        _barSubTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, titleViewHeight - _barSubTitleLabel.bounds.size.height*0.5);
    }
}

#pragma mark - BarButton Configuration

- (void)hidesBackButton {
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
}

- (UIButton *)leftBarButtonWithTitle:(NSString *)title
                         barNorImage:(UIImage *)norImage
                         barHltImage:(UIImage *)hltImage
                              action:(void(^)(void))actionBlock
{
    UIButton *button = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(button, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    return button;
}

- (UIButton *)rightBarButtonWithTitle:(NSString *)title
                          barNorImage:(UIImage *)norImage
                          barHltImage:(UIImage *)hltImage
                               action:(void(^)(void))actionBlock
{
    UIButton *button = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(button, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
    return button;
}

- (UIButton *)buttonWithTitle:(NSString *)title buttonNorImage:(UIImage *)norImage buttonHltImage:(UIImage *)hltImage {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:norImage forState:UIControlStateNormal];
    [button setImage:hltImage forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateDisabled];
    [button addTarget:self action:@selector(actionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font = BAR_BUTTON_FONT;
    [button sizeToFit];
    
    return button;
}

- (void)actionButtonClicked:(UIButton *)sender
{
    void (^actionBlock) (void) = objc_getAssociatedObject(sender, &buttonActionBlockKey);
    actionBlock();
}

#pragma mark - TabBar Configuration

- (void)hidesTabBarWhenPushed {
    [self setHidesBottomBarWhenPushed:YES];
}

- (void)hidesTabBar:(BOOL)hidden animated:(BOOL)animated {
    UITabBarController *tabBarController = self.tabBarController;
    UITabBar *tabBar = tabBarController.tabBar;
    if (!tabBarController || (tabBar.hidden == hidden)) {
        return;
    }
    
    CGFloat tabBarHeight = tabBar.bounds.size.height;
    CGFloat adjustY = hidden ? tabBarHeight : -tabBarHeight;
    
    if (!hidden) {
        tabBar.hidden = NO;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animated ? 0.3 : 0. animations:^{
        
        // adjust TabBar
        CGRect rect = tabBar.frame;
        rect.origin.y += adjustY;
        tabBar.frame = rect;
        
        // adjust TransitionView
        for (UIView *view in tabBarController.view.subviews) {
            if ([NSStringFromClass([view class]) hasSuffix:@"TransitionView"]) {
                if (!IS_IOS7_LATER) {
                    CGRect rect = view.frame;
                    rect.size.height += adjustY;
                    view.frame = rect;
                }
                view.backgroundColor = weakSelf.view.backgroundColor;
            }
        }
    } completion:^(BOOL finished) {
        tabBar.hidden = hidden;
        
        // adjust self.view
        if (IS_IOS7_LATER) {
            CGRect rect = weakSelf.view.frame;
            rect.size.height += adjustY;
            weakSelf.view.frame = rect;
        }
    }];
}


#pragma mark - 创建cloudView
-(void)createCloudView{
    PDImageView *bgImageView = [[PDImageView alloc]init];
    bgImageView.image = [UIImage imageNamed:@"main_backgroundView"];
    bgImageView.frame = kScreenBounds;
    [self.view addSubview:bgImageView];
    
//    PDCloudView *cloudView = [[PDCloudView alloc] initWithFrame:kScreenBounds];
//    NSMutableArray *cloudTagArr = [[NSMutableArray alloc] initWithCapacity:0];
//    for (NSInteger i = 0; i < 11; i ++) {
//        UIView *pathShapeView = [[UIView alloc]init];
//        pathShapeView.backgroundColor = [UIColor clearColor];
//        pathShapeView.opaque = NO;
//        pathShapeView.translatesAutoresizingMaskIntoConstraints = NO;
//        pathShapeView.frame = CGRectMake(0, 0, 10, 10);
//        [cloudTagArr addObject:pathShapeView];
//        [cloudView addSubview:pathShapeView];
//    }
//    [cloudView setCloudTags:cloudTagArr];
//    cloudView.backgroundColor = [UIColor clearColor];
//    [self.view addSubview:cloudView];
//    self.cloudView = cloudView;
}




- (void)setControllerTitle:(NSString *)controllerTitle
{
    UILabel *controllerTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, LCFloat(33), self.view.frame.size.width, LCFloat(31))];
    controllerTitleLabel.text = controllerTitle;
    controllerTitleLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:controllerTitleLabel];
}


- (void)presentLoginFormWithMessage:(NSString *)msg completionHandler:(void (^)())handler{
    __weak typeof(self)weakSelf = self;
    UIAlertView *alertView = [UIAlertView alertViewWithTitle:@"用户登录" message:msg buttonTitles:@[@"取消",@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex){
            PDLoginModel *sharedModel = [PDLoginModel shareInstance];
            NSString *userName = alertView.nameTextField.text;
            NSString *userPass = alertView.passwordTextField.text;
            NSString *loginType = [PDTool userDefaultGetWithKey:CURRENT_USER_LOGIN_TYPE];       // 当前登录类型
            [sharedModel loginWithType:[loginType integerValue] user:userName password:userPass completionHandler:^(BOOL isSucceeded, NSError *error) {
                if (!weakSelf){
                    return ;
                }
                if (isSucceeded){
                    handler();
                } else {
                    NSString *msg = @"";
                    if ([error.domain isEqualToString:PDBizErrorDomain]) {
                        msg = ACCOUNT_ERR_PROMPT;
                    } else {
                        msg = NETWORK_ERR_PROMPT;
                    }
                    [weakSelf presentLoginFormWithMessage:msg completionHandler:handler];
                }
            }];
        }
    }];
    alertView.alertViewStyle = UIAlertViewStyleLoginAndPasswordInput;
    alertView.nameTextField.keyboardType = UIKeyboardTypeNumberPad;
    alertView.nameTextField.placeholder = @"用户名";
    alertView.nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    if ([USERDEFAULTS objectForKey:CURRENT_USER_NAME_KEY]) {
        alertView.nameTextField.text = [USERDEFAULTS objectForKey:CURRENT_USER_NAME_KEY];
    }
    alertView.passwordTextField.placeholder = @"密码";
    alertView.passwordTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [alertView show];
}

- (void)changeTitleLabelToCenterWithItemWidth:(CGFloat)itemWidth {
    _barTitleView.frame = CGRectMake(60, 0, kScreenBounds.size.width - 2 * 60, 44);
}

@end
