//
//  PDImageView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDImageView.h"
#import "SDWebImageManager+Memory.h"

@implementation PDImageView

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:urlString];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"placeholder"];
    }
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

#pragma mark 对战
// 更新对战头像
-(void)uploadChallengeImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:urlString];
    if (placeholder == nil){
        placeholder = [PDTool imageByComposingImage:[UIImage imageNamed:@"icon_challenger_header_normal"] withMaskImage:[UIImage imageNamed:@"round"]];

    }
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            image = [PDTool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
            self.image = image;
        }
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
    
}

@end
