//
//  PDTool.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JCAlertView.h"

extern CGFloat LCFloat(CGFloat floatValue);
extern CGFloat LCFloatWithPadding(CGFloat floatValue, CGFloat padding);

extern CGFloat HSFloat(CGFloat height);

@interface PDTool : NSObject

// 判断是否为空
+ (BOOL)isEmpty:(NSString *)string;
+(NSString *)replaceSpace:(NSString *)string;
// 验证号码
+ (BOOL)validateMobile:(NSString *)mobile;
+ (BOOL)isPureNumandCharacters:(NSString *)string;
+ (BOOL)validatePassword:(NSString *)password;
+ (BOOL)validateUserName:(NSString *)userName;

// 拉伸图片
+ (UIImage *)stretchImageWithName:(NSString *)name;
// 返回imageViewBackGround
+(UIImage *)addBackgroundImageViewWithCellWithDataArray:(NSArray *)array indexPath:(NSIndexPath *)indexPath;
+(UIImage *)addBackgroundImageViewWithRefundCellWithDataArray:(NSArray *)dataArray indexPath:(NSIndexPath *)indexPath;
// 抖动
+(void)animationClickShowWithView:(UIButton *)button block:(void(^)())block;
+ (void)animationWithCollectionWithButton:(UIImageView *)collectionButton collection:(BOOL)isCollection callback:(void(^)())callbackBlock;

// 拨打客服电话
+ (void)callCustomerServer:(UIView *)view phoneNumber:(NSString *)phoneNumber;
// 打开网址
+(void)openURLWithURL:(NSString *)url;

// 获取目标时间
+ (NSTimeInterval)getTargetDateTimeWithTimeInterval:(NSTimeInterval)time;
// 计算倒计时时间
+ (NSString *)getCountdownWithTargetDate:(NSTimeInterval)targetDateWithTimeInterval;
// 统计文字
+ (NSUInteger)calculateCharacterLengthForAres:(NSString *)str;
// 获取当前时间戳
+ (NSString *)getCurrentTimeInterval;

// rangeLabel
+ (NSMutableAttributedString *)rangeLabelWithContent:(NSString *)content hltContentArr:(NSArray *)hltContentArr hltColor:(UIColor *)hltColor normolColor:(UIColor *)normolColor;

+ (NSMutableAttributedString *)rangeLabelWithContent:(NSString *)content hltContentArr:(NSArray *)hltContentArr hltColor:(UIColor *)hltColor hltFont:(UIFont *)hltFont normolColor:(UIColor *)normolColor normalFont:(UIFont *)normalFont;


#pragma mark - userDefault
+(void)userDefaulteWithKey:(NSString *)key Obj:(NSString *)objString;
+(NSString *)userDefaultGetWithKey:(NSString *)key;
+(void)userDefaultDelegtaeWithKey:(NSString *)key;


#pragma mark - 号码裁剪
+(NSString *)numberCutting:(NSString *)number;
+(NSString *)numberCuttingWithBankCard:(NSString *)number;

+(NSString *)appName;
+(NSString *)appVersion;

// 复制
+(void)copyWithString:(NSString *)text callback:(void(^)())callBack;

+(CGFloat)smartFloat:(CGFloat)floatValue;
+ (UIImage*) createImageWithColor: (UIColor*) color frame:(CGRect)rect ;



+(void)clickZanWithView:(UIView *)button block:(void(^)())block;


#pragma mark-- 转化时间为相应格式
//+ (NSString *)getTimeGap:(NSTimeInterval)updateTime;

+ (UIImage *)imageByComposingImage:(UIImage *)image withMaskImage:(UIImage *)maskImage ;
+(CGSize)makeSizeWithLabel:(UILabel *)label;

#pragma mark - md5加密
+ (NSString *)md5:(NSString *)inPutText;

#pragma mark - 显示网络错误
+ (JCAlertView *)showNetErrorString:(NSString *)errorStr;

#pragma mark - 分享成功
+ (JCAlertView *)showShare;
@end
