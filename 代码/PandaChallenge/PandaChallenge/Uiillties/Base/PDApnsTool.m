//
//  PDApnsTool.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDApnsTool.h"
#import <ALBBSDK/ALBBSDK.h>
#import "SBJSON.h"
#import "PDChallengerStatusModel.h"
#import "PDChallengeDirectManager.h"
@implementation PDApnsTool

+ (instancetype)shareInstance{
    
    static PDApnsTool *pushManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pushManager = [[PDApnsTool alloc] init];
    });
    
    return pushManager;
}

-(void)registWithPUSHWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions deviceIdBlock:(void(^)())block{
    // 注册苹果推送
    [self registerAPNSWithApplication:application launchOptions:launchOptions];
    // 1. push sdk 初始化 获取DeviceId
    __weak typeof(self)weakSelf = self;
    [weakSelf init_taeBlock:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];

    // 2. 监听网络消息
    [self listenerOnChannelOpened];
    // 3. 监听推送消息内容
    [self registerMsgReceive];
}


// push sdk 初始化
- (void)init_taeBlock:(void(^)())block{                   //sdk初始化
    [[ALBBSDK sharedInstance] setDebugLogOpen:NO];// 测试时打开
    [[ALBBSDK sharedInstance] asyncInit:^{
        [PDApnsTool shareInstance].deviceToken = [CloudPushSDK getDeviceId];
        [PDTool userDefaulteWithKey:DeviceId Obj:[CloudPushSDK getDeviceId]];
        if (block){
            block();
        }
    }failure:^(NSError *error) {
        
    }];
    
}

#pragma mark - 监听 网络消息
- (void) listenerOnChannelOpened {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onChannelOpened:) name:@"CCPDidChannelConnectedSuccess" object:nil]; // 注册
}

#pragma mark 推送下来的消息抵达的处理示例
- (void)onChannelOpened:(NSNotification *)notification {
    
}


#pragma mark 注册接收CloudChannel推送下来的消息
- (void) registerMsgReceive {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessageReceived:) name:@"CCPDidReceiveMessageNotification" object:nil]; // 注册
}

// 推送下来的消息抵达的处理示例
- (void)onMessageReceived:(NSNotification *)notification {
    NSData *data = [notification object];
    NSString *str =  [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    
    SBJSON *json = [[SBJSON alloc] init];
    NSDictionary *dicWithRequestJson = [json objectWithString:str error:nil];
    PDChallengerStatusModel *messageModel = [[PDChallengerStatusModel alloc]initWithJSONDict:[dicWithRequestJson objectForKey:@"data"]];
    if (messageModel.code == 180) {
        NSString *phone = [PDTool userDefaultGetWithKey:CURRENT_USER_NAME_KEY];
         NSLog(@"%@",phone);
        if ([[PDLoginModel shareInstance] hasLoggedIn] && [phone isEqualToString:messageModel.phone]
            ){            // 退出登录
            [PDChallengeDirectManager directToLoginPageCleanCookie];
        }

    }
}


#pragma mark 注册苹果的推送
-(void) registerAPNSWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions{
    //    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {         // iOS 8 Notifications
    //        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
    //        [application registerForRemoteNotifications];
    //    } else {        // iOS < 8 Notifications
    //        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
    //    }
    //    [CloudPushSDK handleLaunching:launchOptions]; // 作为 apns 消息统计
    
    /// 需要区分iOS SDK版本和iOS版本。
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge|UIUserNotificationTypeAlert|UIUserNotificationTypeSound) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        
    } else
#endif
    {
        /// 去除warning
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
         (UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
#pragma clang diagnostic pop
    }
}

#pragma mark - 注册DeviceId
// 苹果推送服务回调，注册 deviceToken
-(void)registerWithDeviceId:(NSData *)deviceToken{
    [CloudPushSDK registerDevice:deviceToken];
}

// 通知统计回调handleReceiveRemoteNotification
-(void)registerWithReceiveRemoteNotification:(NSDictionary*)userInfo {
    [CloudPushSDK handleReceiveRemoteNotification:userInfo];
}




@end
