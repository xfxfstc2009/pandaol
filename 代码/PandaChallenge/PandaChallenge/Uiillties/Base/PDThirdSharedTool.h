//
//  PDThirdSharedTool.h
//  PandaChallenge
//
//  Created by panda on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ShareSDK/ShareSDK.h>
#import <ShareSDKConnector/ShareSDKConnector.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <ShareSDKUI/ShareSDKUI.h>
#import "WeiboSDK.h"
#import "WXApi.h"

@interface PDThirdSharedTool : NSObject
//三方登录的注册
+ (void)setUpThirdLogin;
//QQ登录返回
+ (void)loginByQQSuccess:(void(^)(SSDKUser *user))success failure:(void (^) (NSError *error))failure;
//微信登录返回
+ (void)loginByWeChatSuccess:(void(^)(SSDKUser *user))success failure:(void (^) (NSError *error))failure;
//分享
+ (void)shareWithShareType:(NSString *)shareType content:(NSString *)content image:(UIImage *)image imageUrl:(NSString *)imageUrl urlString:(NSString *)urlString;

//分享判断是否安装设备
+ (NSMutableArray *)shareForInstall;

@end
