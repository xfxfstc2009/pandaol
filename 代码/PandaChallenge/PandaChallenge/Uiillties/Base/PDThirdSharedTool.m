//
//  PDThirdSharedTool.m
//  PandaChallenge
//
//  Created by panda on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDThirdSharedTool.h"
#import "PDInviteModel.h"
#import "GTMBase64.h"
#define WeChatAppID         @"wxafbc4f7bdaf0156d"
#define WeChatAppSecret     @"fa06c091a292b082b4e9bc5393c0e23a"
#define QQAppID             @"1105244637"
#define QQAppAppSecret      @"mS8wLIan8p4pRc5P"
#define WeiBoAPPID          @"1938399542"
#define WeiBoAppSecret      @"b888b2779298f00b98aa6276f8cb3bdc"
#define WeiBoRedirectUri    @"http://www.pandaol.com"
@interface PDThirdSharedTool ()
@property (nonatomic,strong)JCAlertView *alertView;
@end
@implementation PDThirdSharedTool
#pragma mark - 注册
+(void)setUpThirdLogin{
    //第三方分享登录(微信、QQ)
    [PDThirdSharedTool thirdLoginByTypeArray:@[@(SSDKPlatformTypeWechat),@(SSDKPlatformTypeQQ),@(SSDKPlatformTypeSinaWeibo)]];
}


+ (void)thirdLoginByTypeArray:(NSArray *)logTypeArray
{
    [ShareSDK registerApp:@"1073ba32a005a" activePlatforms:logTypeArray onImport:^(SSDKPlatformType platformType) {
        switch (platformType) {
            case SSDKPlatformTypeWechat:
                [ShareSDKConnector connectWeChat:[WXApi class]];
                break;
            case SSDKPlatformTypeQQ:
                [ShareSDKConnector connectQQ:[QQApiInterface class] tencentOAuthClass:[TencentOAuth class]];
                break;
            case SSDKPlatformTypeSinaWeibo:
                [ShareSDKConnector connectWeibo:[WeiboSDK class]];
                break;
            default:
                break;
        }
    } onConfiguration:^(SSDKPlatformType platformType, NSMutableDictionary *appInfo) {
        switch (platformType) {
            case SSDKPlatformTypeWechat:
                [appInfo SSDKSetupWeChatByAppId:WeChatAppID appSecret:WeChatAppSecret];
                break;
            case SSDKPlatformTypeQQ:
                [appInfo SSDKSetupQQByAppId:QQAppID appKey:QQAppAppSecret authType:SSDKAuthTypeBoth];
                break;
                case SSDKPlatformTypeSinaWeibo:
                [appInfo SSDKSetupSinaWeiboByAppKey:WeiBoAPPID
                                          appSecret:WeiBoAppSecret
                                        redirectUri:WeiBoRedirectUri
                                           authType:SSDKAuthTypeSSO];
                break;
            default:
                break;
        }
        
    }];
}
#pragma mark - 登录返回
+ (void)loginByQQSuccess:(void(^)(SSDKUser *user))success failure:(void (^) (NSError *error))failure
{
    [ShareSDK getUserInfo:SSDKPlatformTypeQQ onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
        if (state == SSDKResponseStateSuccess) {
            success(user);
        }else if(state == SSDKResponseStateFail){
            failure(error);
        }
    }];
}

+ (void)loginByWeChatSuccess:(void(^)(SSDKUser *user))success failure:(void (^) (NSError *error))failure
{
    [ShareSDK getUserInfo:SSDKPlatformTypeWechat onStateChanged:^(SSDKResponseState state, SSDKUser *user, NSError *error) {
        if (state == SSDKResponseStateSuccess) {
            success(user);
        }else if(state == SSDKResponseStateFail){
            failure(error);
        }
    }];
}
#pragma mark - 设备安装判断
+ (NSMutableArray *)shareForInstall
{
    NSMutableArray *installMutabelArray = [NSMutableArray array];
    if ([WXApi isWXAppInstalled]) {
        [installMutabelArray addObject:@[@"微信好友",@"icon_share_weChat",@"wechat"]];
        [installMutabelArray addObject:@[@"朋友圈",@"icon_share_weChatFriend",@"wechatFriend"]];
    }
    if([QQApiInterface isQQInstalled]){
        [installMutabelArray addObject:@[@"QQ好友",@"icon_share_qq",@"qq"]];
        [installMutabelArray addObject:@[@"QQ空间",@"icon_share_qqZone",@"qqzone"]];
    }
    if([WeiboSDK isWeiboAppInstalled]){
        [installMutabelArray addObject:@[@"新浪微博",@"icon_share_sina",@"sina"]];
    }
    NSLog(@"%li",(long)installMutabelArray.count);
    return installMutabelArray;
}

#pragma mark - 分享
+ (void)shareWithShareType:(NSString *)shareType content:(NSString *)content image:(UIImage *)image imageUrl:(NSString *)imageUrl urlString:(NSString *)urlString
{
    //创建分享参数
     NSMutableDictionary *shareParams = [NSMutableDictionary dictionary];
    if ([shareType isEqualToString:@"wechat"]) {                /**< 微信分享*/
        NSString *partUrl = [NSString stringWithFormat:@"&recommend_path=%@&imageUrl=%@&content=%@",@"3",imageUrl,content];
        NSString *partEncodeUrl = [GTMBase64 encodeBase64String:partUrl];
        NSString *shareUrlStr = [NSString stringWithFormat:@"%@?%@&invitationCode=%@",urlString,partEncodeUrl,[PDLoginModel shareInstance].invitationCode];
        [shareParams SSDKSetupShareParamsByText:content images:image url:[NSURL URLWithString:shareUrlStr] title:@"盼达在线" type:SSDKContentTypeAuto];
        [ShareSDK share:SSDKPlatformSubTypeWechatSession parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            if (state == SSDKResponseStateSuccess) {
                [PDTool showShare];
            }else if (state == SSDKResponseStateFail){
                [PDHUD showShimmeringString:error.localizedDescription afterDelay:2];
            }
        }];
    }else if ([shareType isEqualToString:@"wechatFriend"]) {     /**< 微信朋友圈分享*/
        NSString *partUrl = [NSString stringWithFormat:@"&recommend_path=%@&imageUrl=%@&content=%@",@"4",imageUrl,content];
        NSString *partEncodeUrl = [GTMBase64 encodeBase64String:partUrl];
        NSString *shareUrlStr = [NSString stringWithFormat:@"%@?%@&invitationCode=%@",urlString,partEncodeUrl,[PDLoginModel shareInstance].invitationCode];
         [shareParams SSDKSetupShareParamsByText:nil images:image url:[NSURL URLWithString:shareUrlStr] title:content type:SSDKContentTypeAuto];
        [ShareSDK share:SSDKPlatformSubTypeWechatTimeline parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            if (state == SSDKResponseStateSuccess) {
                [PDTool showShare];
            }else if (state == SSDKResponseStateFail){
                [PDHUD showShimmeringString:error.localizedDescription afterDelay:2];
            }
        }];
    }else if ([shareType isEqualToString:@"qq"]) {               /**< QQ分享*/
        NSString *partUrl = [NSString stringWithFormat:@"&recommend_path=%@&imageUrl=%@&content=%@",@"1",imageUrl,content];
        NSString *partEncodeUrl = [GTMBase64 encodeBase64String:partUrl];
        NSString *shareUrlStr = [NSString stringWithFormat:@"%@?%@&invitationCode=%@",urlString,partEncodeUrl,[PDLoginModel shareInstance].invitationCode];
        [shareParams SSDKSetupShareParamsByText:content images:image url:[NSURL URLWithString:shareUrlStr] title:@"盼达在线" type:SSDKContentTypeAuto];
        [ShareSDK share:SSDKPlatformSubTypeQQFriend parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            if (state == SSDKResponseStateSuccess) {
                [PDTool showShare];
            }else if (state == SSDKResponseStateFail){
                [PDHUD showShimmeringString:error.localizedDescription afterDelay:2];
            }
        }];
    }else if ([shareType isEqualToString:@"qqzone"]) {           /**< QQ空间分享*/
        NSString *partUrl = [NSString stringWithFormat:@"&recommend_path=%@&imageUrl=%@&content=%@",@"2",imageUrl,content];
        NSString *partEncodeUrl = [GTMBase64 encodeBase64String:partUrl];
        NSString *shareUrlStr = [NSString stringWithFormat:@"%@?%@&invitationCode=%@",urlString,partEncodeUrl,[PDLoginModel shareInstance].invitationCode];
        [shareParams SSDKSetupShareParamsByText:content images:image url:[NSURL URLWithString:shareUrlStr] title:@"盼达在线" type:SSDKContentTypeAuto];
        [ShareSDK share:SSDKPlatformSubTypeQZone parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            if (state == SSDKResponseStateSuccess) {
                [PDTool showShare];
            }else if (state == SSDKResponseStateFail){
                [PDHUD showShimmeringString:error.localizedDescription afterDelay:2];
            }
        }];
    }else if ([shareType isEqualToString:@"sina"]) {             /**< 新浪微博分享*/
        NSString *partUrl = [NSString stringWithFormat:@"&recommend_path=%@&imageUrl=%@",@"5",imageUrl];
        NSString *partEncodeUrl = [GTMBase64 encodeBase64String:partUrl];
        NSString *shareUrlStr = [NSString stringWithFormat:@"%@?%@",urlString,partEncodeUrl];
        
        
        
        NSString *shareContent = [NSString stringWithFormat:@"%@%@&invitationCode=%@",content,shareUrlStr,[PDLoginModel shareInstance].invitationCode];
        NSLog(@"%@",shareContent);
        [shareParams SSDKEnableUseClientShare];
        [shareParams SSDKSetupShareParamsByText:shareContent images:nil url:nil title:@"盼达在线" type:SSDKContentTypeAuto];
        [ShareSDK share:SSDKPlatformTypeSinaWeibo parameters:shareParams onStateChanged:^(SSDKResponseState state, NSDictionary *userData, SSDKContentEntity *contentEntity, NSError *error) {
            if (state == SSDKResponseStateSuccess) {
                [PDTool showShare];
            }else if (state == SSDKResponseStateFail){
                [PDHUD showShimmeringString:error.localizedDescription afterDelay:2];
            }
        }];
    }
}




- (void)alertShow
{
    PDAlertShowView *alertShowView = [[PDAlertShowView alloc]init];
    __weak typeof(self) weakSelf = self;
    [alertShowView alertWithTitle:@"" headerImage:[UIImage imageNamed:@"icon_report_success"] titleDesc:@"待您的好友成功体验一场比赛后，您可获得一次额外参赛资格" buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.alertView dismissWithCompletion:NULL];
    }];
    self.alertView = [[JCAlertView alloc]init];
    [self.alertView show];
}


@end
