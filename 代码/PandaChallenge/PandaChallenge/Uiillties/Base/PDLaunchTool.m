//
//  PDLaunchTool.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/4/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLaunchTool.h"
#import "PDSliderleftViewController.h"
#import "PDNavigationController.h"
#import "PDChallengeViewController.h"
#import "PDLoginViewController.h"
@implementation PDLaunchTool


+ (void)launchJudge
{
    if (![USERDEFAULTS boolForKey:LaunchEver]) {
        [USERDEFAULTS setBool:YES forKey:LaunchEver];
        [USERDEFAULTS setBool:YES forKey:LaunchFirst];
    } else {
        [USERDEFAULTS setBool:NO forKey:LaunchFirst];
    }
    
}


+ (void)createmainSlider:(UIWindow *)window {
    
    PDSliderleftViewController *leftMenuViewController = [[PDSliderleftViewController alloc] init];
        
        PDNavigationController *navigationController;
        if ([PDLoginModel shareInstance].hasEverLoggedIn) {
            PDChallengeViewController *challengeViewController = [[PDChallengeViewController alloc]init];
            navigationController = [[PDNavigationController alloc]initWithRootViewController:challengeViewController];
        }else{
            PDLoginViewController *loginViewCintroller = [[PDLoginViewController alloc]init];
            navigationController = [[PDNavigationController alloc]initWithRootViewController:loginViewCintroller];
        }
        
        
        RESideMenu *sideMenuViewController = [[RESideMenu shareInstance] initWithContentViewController:navigationController leftMenuViewController:leftMenuViewController rightMenuViewController:nil];
        sideMenuViewController.backgroundImage = [UIImage imageNamed:@"bg"];
        sideMenuViewController.menuPreferredStatusBarStyle = 1; // UIStatusBarStyleLightContent
        sideMenuViewController.contentViewShadowColor = [UIColor blackColor];
        sideMenuViewController.contentViewShadowOffset = CGSizeMake(0, 0);
        sideMenuViewController.contentViewShadowOpacity = 8;
        sideMenuViewController.contentViewShadowRadius = 12;
        sideMenuViewController.contentViewScaleValue = 1;
        sideMenuViewController.contentViewInPortraitOffsetCenterX = kScreenBounds.size.width / 4.;
        sideMenuViewController.contentViewShadowEnabled = YES;
        sideMenuViewController.parallaxEnabled = NO;
        sideMenuViewController.menuPrefersStatusBarHidden = YES;
        window.rootViewController = sideMenuViewController;
        
        window.backgroundColor = [UIColor whiteColor];
        [window makeKeyAndVisible];
}



@end
