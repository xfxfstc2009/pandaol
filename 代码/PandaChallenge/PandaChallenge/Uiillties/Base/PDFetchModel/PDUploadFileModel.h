//
//  PDUploadFileModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDUploadFileModel : PDFetchModel

@property (nonatomic,strong)UIImage *file;
@property (nonatomic,copy)NSString *keyName;

@end
