//
//  PDFetchModelProperty.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
typedef NS_ENUM(NSUInteger, GWFetchModelPropertyValueType) {
    GWClassPropertyValueTypeNone = 0,
    GWClassPropertyTypeChar,
    GWClassPropertyTypeInt,
    GWClassPropertyTypeShort,
    GWClassPropertyTypeLong,
    GWClassPropertyTypeLongLong,
    GWClassPropertyTypeUnsignedChar,
    GWClassPropertyTypeUnsignedInt,
    GWClassPropertyTypeUnsignedShort,
    GWClassPropertyTypeUnsignedLong,
    GWClassPropertyTypeUnsignedLongLong,
    GWClassPropertyTypeFloat,
    GWClassPropertyTypeDouble,
    GWClassPropertyTypeBool,
    GWClassPropertyTypeVoid,
    GWClassPropertyTypeCharString,
    GWClassPropertyTypeObject,
    GWClassPropertyTypeClassObject,
    GWClassPropertyTypeSelector,
    GWClassPropertyTypeArray,
    GWClassPropertyTypeStruct,
    GWClassPropertyTypeUnion,
    GWClassPropertyTypeBitField,
    GWClassPropertyTypePointer,
    GWClassPropertyTypeUnknow
};

@interface PDFetchModelProperty : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *typeName;
@property (nonatomic, assign) Class objectClass;
@property (nonatomic, strong) NSArray *objectProtocols;
@property (nonatomic, assign) BOOL isReadonly;
@property (nonatomic, assign) GWFetchModelPropertyValueType valueType;

- (id)initWithName:(NSString *)name typeString:(NSString *)typeString;

@end
