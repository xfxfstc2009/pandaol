//
//  PDFetchModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

#define PDBizErrorDomain @"PDBizErrorDomain"

typedef void (^FetchCompletionHandler) (BOOL isSucceeded, NSError *error);

@interface PDFetchModel : NSObject

@property (nonatomic, assign) BOOL bizDataIsNull;
@property (nonatomic, assign) BOOL bizResult;

// 请求参数
@property (nonatomic, strong) NSDictionary *requestParams;
@property (nonatomic, strong) NSArray *requestFileDataArr;

// 自定义的UserAgent信息
+ (NSString *)customUserAgent;

// 请求接口
-(void)fetchWithPath:(NSString *)path completionHandler:(FetchCompletionHandler)handler;


- (BOOL)isSessionValid;
- (void)clearCookiesForBaseURL;

// 仅供运行时解析JSON使用，子类不要调用此方法初始化对象
- (instancetype)initWithJSONDict:(NSDictionary *)dict;

// 子类需要覆盖此方法，提供model和JSON无法对应到的成员
- (NSDictionary *)modelKeyJSONKeyMapper;


@end
