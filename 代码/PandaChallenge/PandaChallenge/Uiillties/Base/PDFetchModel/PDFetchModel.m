//
//  PDFetchModel.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import <objc/runtime.h>
#import "PDFetchModelProperty.h"
#import "JSON.h"
#import "PDLoginModel.h"

@interface NSArray (PDFetchModel)
- (NSArray *)modelArrayWithClass:(Class)modeGWlass;
@end

@implementation NSArray (PDFetchModel)

- (NSArray *)modelArrayWithClass:(Class)modeGWlass{
    NSMutableArray *modelArray = [NSMutableArray array];
    for (id object in self) {
        if ([object isKindOfClass:[NSArray class]]) {
            [modelArray addObject:[object modelArrayWithClass:modeGWlass]];
        } else if ([object isKindOfClass:[NSDictionary class]]){
            [modelArray addObject:[[modeGWlass alloc] initWithJSONDict:object]];
        } else {
            [modelArray addObject:object];
        }
    }
    return modelArray;
}

@end

#pragma mark - NSDictionary+GWFetchModel

@interface NSDictionary (PDFetchModel)

- (NSDictionary *)modelDictionaryWithClass:(Class)modeGWlass;

@end

@implementation NSDictionary (PDFetchModel)

- (NSDictionary *)modelDictionaryWithClass:(Class)modeGWlass{
    NSMutableDictionary *modelDictionary = [NSMutableDictionary dictionary];
    for (NSString *key in self) {
        id object = [self objectForKey:key];
        if ([object isKindOfClass:[NSDictionary class]]) {
            [modelDictionary setObject:[[modeGWlass alloc] initWithJSONDict:object] forKey:key];
        }else if ([object isKindOfClass:[NSArray class]]){
            [modelDictionary setObject:[object modelArrayWithClass:modeGWlass] forKey:key];
        }else{
            [modelDictionary setObject:object forKey:key];
        }
    }
    return modelDictionary;
}

@end

#pragma mark - PDFetchModel

static const char *PDFecthModelKeyMapperKey;
static const char *PDFetchModelPropertiesKey;

@interface PDFetchModel() {
    NSURLSessionDataTask *requestOperation;
}

@property(nonatomic, strong) AFHTTPSessionManager *operationManager;

- (void)setupCachedKeyMapper;
- (void)setupCachedProperties;

@end

@implementation PDFetchModel

+ (NSString *)customUserAgent{
    
    NSString *channel = ([BUILD_VER intValue] % 2) ? @"ourtech" : @"appStore";
    PDLoginModel *loginModel = [PDLoginModel shareInstance];
    NSString *userId = @"";
    if (loginModel.bizResult) {
        userId = [NSString stringWithFormat:@"UID/%@ ", loginModel.userId];
    }
    
    NSString *deviceName = @"iPhone";
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        deviceName = @"iPad";
    }
    
    return [NSString stringWithFormat:@"( HotShop; Client/%@%@ V/%@|%@ channel/%@ %@)"
            ,deviceName ,[UIDevice currentDevice].systemVersion , BUILD_VER, APP_VER, channel, userId];
}

- (instancetype)init{
    
    self = [super init];
    if (self) {
        
        [self setupCachedKeyMapper];
        [self setupCachedProperties];
    }
    return self;
}

- (instancetype)initWithJSONDict:(NSDictionary *)dict{
    
    self = [self init];
    if (self) {
        [self injectJSONData:dict];
    }
    return self;
}

- (void)dealloc{
    if (requestOperation) {
        [requestOperation cancel];
    }
}

// manager
- (AFHTTPSessionManager *)operationManager {
    if (!_operationManager) {
        NSURL *baseURL ;
        baseURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@/", GWREQUEST_API_HOST, GWREQUEST_API_VER]];
        _operationManager = [[AFHTTPSessionManager alloc] initWithBaseURL:baseURL];
        _operationManager.requestSerializer.timeoutInterval = 10;
        _operationManager.requestSerializer = [AFHTTPRequestSerializer serializer];
        _operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
        
    }
    
    return _operationManager;
}

// 判断session
- (BOOL)isSessionValid{
  
    NSURL *url = self.operationManager.baseURL;
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:url];
    
    for (NSHTTPCookie *cookie in cookies) {
        if ([cookie.name isEqualToString:@"ci_session"] && (cookie.expiresDate.timeIntervalSinceNow > 0)) {
            return YES;
        }
    }
    
    return NO;
}

// 清除cookies
- (void)clearCookiesForBaseURL{
    NSURL *url = self.operationManager.baseURL;
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:url];
    
    for (NSHTTPCookie *cookie in cookies) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
}

// 请求体
-(void)fetchWithPath:(NSString *)path completionHandler:(FetchCompletionHandler)handler{
    NSMutableDictionary *requestParamsDic = [NSMutableDictionary dictionaryWithDictionary:_requestParams];
    if (![[requestParamsDic allKeys] containsObject:@"token"]) {
        if ((![path hasSuffix:kRegister])||(![path hasSuffix:kLoginPath])) {
            NSString *phone = [PDLoginModel shareInstance].phone;
            if (phone.length != 0) {
                requestParamsDic[@"token"] = [PDRSAManager decodeWithPublicKeyWithString:phone];
            }
        }
    }
    
    __weak typeof(self) weakSelf = self;
    [requestOperation cancel];
    [self.operationManager POST:path parameters:requestParamsDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSDate *date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyyMMddHHmmss"];
        NSString *dateStr = [formatter stringFromDate:date];
        if (_requestFileDataArr.count){
            for (int i = 0 ;i < _requestFileDataArr.count;i++){
                PDUploadFileModel *fileModel = [_requestFileDataArr objectAtIndex:i];
                UIImage *valueImage = fileModel.file;
                NSData *data = UIImageJPEGRepresentation(valueImage,0.7);
                NSString *keyName = fileModel.keyName;
                NSString *fileName = [NSString stringWithFormat:@"%@%d.png", dateStr,i + 1];
                
                // 1. 查找当前的数据里面的key
                NSString *name = [_requestParams objectForKey:keyName];
                if (!name.length){
                    name = [NSString stringWithFormat:@"%li",(long)arc4random()];
                }
                [formData appendPartWithFileData:data name:name fileName:fileName mimeType:@"image/png"];
            }
        }
        
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (!weakSelf){
            return ;
        }
        // 重置相关字段为默认
        weakSelf.bizResult = NO;
        weakSelf.bizDataIsNull = NO;
        if (responseObject) {
            __strong typeof(weakSelf)strongSelf = weakSelf;
            dispatch_async(dispatch_get_main_queue(), ^{
                // 返回data
                NSData *responsData = responseObject;
                NSString *str  =  [[NSString alloc]initWithData:responsData encoding:NSUTF8StringEncoding];
//                [[UIAlertView alertViewWithTitle:nil message:str buttonTitles:@[@"确定"] callBlock:NULL]show];
                NSLog(@"%@",str);
                SBJSON *json = [[SBJSON alloc] init];
                NSDictionary *dicWithRequestJson = [json objectWithString:str error:nil];
#ifdef DEBUG
                NSLog(@"%@",dicWithRequestJson);
#endif
                // 获取请求是否成功
                NSNumber *errorCode = [dicWithRequestJson objectForKey:@"code"];
                // 获取请求data 数据
                id data = [dicWithRequestJson objectForKey:@"data"];
                if (errorCode.integerValue == 200 || (errorCode.integerValue > 131 && errorCode.integerValue < 140)){              // 【成功】
                    if ([data isKindOfClass:[NSArray class]] || [data isKindOfClass:[NSDictionary class]]){
                        strongSelf.bizResult = YES;
                        [strongSelf injectJSONData:data];
                    } else if ([data isKindOfClass:[NSNull class]]){
                        _bizDataIsNull = YES;
                    } else if ([data isKindOfClass:[NSNumber class]]){
                        if ([NSStringFromClass([data class]) hasSuffix:@"CFBoolean"]){
                            _bizResult = [data boolValue];
                        }
                    }
                    handler(YES,nil);
                } else {                                        // 【失败】
                    NSString *errorInfo = [dicWithRequestJson objectForKey:@"msg"];
                    if (!errorInfo){
                        errorInfo = @"系统错误请联系程序员";
//                        errorInfo = str;
                    }
                    NSDictionary *dict = @{NSLocalizedDescriptionKey: errorInfo};
                    NSError *bizError = [NSError errorWithDomain:PDBizErrorDomain code:errorCode.integerValue userInfo:dict];
                    handler(NO,bizError);
                }
            });
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (!weakSelf) {
            return;
        }
#ifdef DEBUG
        NSString *requestURL = [task.currentRequest.URL absoluteString];
        NSString *params = [[NSString alloc]initWithData:task.currentRequest.HTTPBody encoding:NSUTF8StringEncoding];
        NSLog(@"FAILURE URL:%@ \nPARAMS:%@ \nAND RESPONSE:%@", requestURL, params, task.response);
#endif
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf showResponseCode:task.response WithBlock:^(NSInteger statusCode) {
            if (statusCode == 401){
                
            } else {
                handler(NO,error);
            }
        }];
    }];
}



- (void)showResponseCode:(NSURLResponse *)response WithBlock:(void (^)(NSInteger statusCode))block{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    NSInteger responseStatusCode = [httpResponse statusCode];
    return block(responseStatusCode);
}



#pragma mark - GWFetchModel Configuration

- (void)setupCachedKeyMapper{
    
    if (objc_getAssociatedObject(self.class, &PDFecthModelKeyMapperKey) == nil) {
        
        NSDictionary *dict = [self modelKeyJSONKeyMapper];
        if (dict.count) {
            objc_setAssociatedObject(self.class, &PDFecthModelKeyMapperKey, dict, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
    }
}

- (void)setupCachedProperties{
    
    if (objc_getAssociatedObject(self.class, &PDFetchModelPropertiesKey) == nil) {
        
        NSMutableDictionary *propertyMap = [NSMutableDictionary dictionary];
        Class class = [self class];
        
        while (class != [PDFetchModel class]) {
            unsigned int propertyCount;
            objc_property_t *properties = class_copyPropertyList(class, &propertyCount);
            for (unsigned int i = 0; i < propertyCount; i++) {
                
                objc_property_t property = properties[i];
                const char *propertyName = property_getName(property);
                NSString *name = [NSString stringWithUTF8String:propertyName];
                const char *propertyAttrs = property_getAttributes(property);
                NSString *typeString = [NSString stringWithUTF8String:propertyAttrs];
                PDFetchModelProperty *modelProperty = [[PDFetchModelProperty alloc] initWithName:name typeString:typeString];
                if (!modelProperty.isReadonly) {
                    [propertyMap setValue:modelProperty forKey:modelProperty.name];
                }
            }
            free(properties);
            
            class = [class superclass];
        }
        objc_setAssociatedObject(self.class, &PDFetchModelPropertiesKey, propertyMap, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{};
}

#pragma mark - GWFetchModel Runtime Injection

- (void)injectJSONData:(id)dataObject{
    
    NSDictionary *keyMapper = objc_getAssociatedObject(self.class, &PDFecthModelKeyMapperKey);
    NSDictionary *properties = objc_getAssociatedObject(self.class, &PDFetchModelPropertiesKey);
    
    if ([dataObject isKindOfClass:[NSArray class]]) {
        
        PDFetchModelProperty *arrayProperty = nil;
        Class class = NULL;
        for (PDFetchModelProperty *property in [properties allValues]) {
            
            NSString *valueProtocol = [property.objectProtocols firstObject];
            class = NSClassFromString(valueProtocol);
            if ([valueProtocol isKindOfClass:[NSString class]] && [class isSubclassOfClass:[PDFetchModel class]]) {
                arrayProperty = property;
                break;
            }
        }
        
        if (arrayProperty && class) {
            id value = [(NSArray *)dataObject modelArrayWithClass:class];
            [self setValue:value forKey:arrayProperty.name];
        }
    }else if ([dataObject isKindOfClass:[NSDictionary class]]){
        
        for (PDFetchModelProperty *property in [properties allValues]) {
            
            NSString *jsonKey = property.name;
            NSString *mapperKey = [keyMapper objectForKey:jsonKey];
            jsonKey = mapperKey ?: jsonKey;
            
            id jsonValue = [dataObject objectForKey:jsonKey];
            id propertyValue = [self valueForProperty:property withJSONValue:jsonValue];
            
            if (propertyValue) {
                
                [self setValue:propertyValue forKey:property.name];
            }else{
                
                id resetValue = (property.valueType == GWClassPropertyTypeObject) ? nil : @(0);
                [self setValue:resetValue forKey:property.name];
            }
        }
    }
}

- (id)valueForProperty:(PDFetchModelProperty *)property withJSONValue:(id)value{
    
    id resultValue = value;
    if (value == nil || [value isKindOfClass:[NSNull class]]) {
        resultValue = nil;
    }else{
        if (property.valueType != GWClassPropertyTypeObject) {
            
            if ([value isKindOfClass:[NSString class]]) {
                if (property.valueType == GWClassPropertyTypeInt ||
                    property.valueType == GWClassPropertyTypeUnsignedInt||
                    property.valueType == GWClassPropertyTypeShort||
                    property.valueType == GWClassPropertyTypeUnsignedShort) {
                    resultValue = [NSNumber numberWithInt:[(NSString *)value intValue]];
                }
                if (property.valueType == GWClassPropertyTypeLong ||
                    property.valueType == GWClassPropertyTypeUnsignedLong ||
                    property.valueType == GWClassPropertyTypeLongLong ||
                    property.valueType == GWClassPropertyTypeUnsignedLongLong){
                    resultValue = [NSNumber numberWithLongLong:[(NSString *)value longLongValue]];
                }
                if (property.valueType == GWClassPropertyTypeFloat) {
                    resultValue = [NSNumber numberWithFloat:[(NSString *)value floatValue]];
                }
                if (property.valueType == GWClassPropertyTypeDouble) {
                    resultValue = [NSNumber numberWithDouble:[(NSString *)value doubleValue]];
                }
                if (property.valueType == GWClassPropertyTypeChar) {
                    //对于BOOL而言，@encode(BOOL) 为 c 也就是signed char
                    resultValue = [NSNumber numberWithBool:[(NSString *)value boolValue]];
                }
            }
        }else{
            Class valueClass = property.objectClass;
            if ([valueClass isSubclassOfClass:[PDFetchModel class]] &&
                [value isKindOfClass:[NSDictionary class]]) {
                resultValue = [[valueClass alloc] initWithJSONDict:value];
            }
            
            if ([valueClass isSubclassOfClass:[NSString class]] &&
                ![value isKindOfClass:[NSString class]]) {
                resultValue = [NSString stringWithFormat:@"%@",value];
            }
            
            if ([valueClass isSubclassOfClass:[NSNumber class]] &&
                [value isKindOfClass:[NSString class]]) {
                NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                resultValue = [numberFormatter numberFromString:value];
            }
            
            NSString *valueProtocol = [property.objectProtocols lastObject];
            if ([valueProtocol isKindOfClass:[NSString class]]) {
                
                Class valueProtocoGWlass = NSClassFromString(valueProtocol);
                if (valueProtocoGWlass != nil) {
                    if ([valueProtocoGWlass isSubclassOfClass:[PDFetchModel class]]) {
                        //array of models
                        if ([value isKindOfClass:[NSArray class]]) {
                            resultValue = [(NSArray *)value modelArrayWithClass:valueProtocoGWlass];
                        }
                        //dictionary of models
                        if ([value isKindOfClass:[NSDictionary class]]) {
                            resultValue = [(NSDictionary *)value modelDictionaryWithClass:valueProtocoGWlass];
                        }
                    }
                }
            }
        }
    }
    return resultValue;
}



@end
