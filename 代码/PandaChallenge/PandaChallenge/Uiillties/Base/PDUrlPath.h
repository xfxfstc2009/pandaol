//
//  PDUrlPath.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef PDUrlPath_h
#define PDUrlPath_h

// Login
#define kLoginPath @"Login/login"                 // 登录
#define kRegister @"Register/add"                 // 注册
#define KRet_Password @"Register/resetPassword"   // 重置密码
#define KChange_Password @"userCenter/updatePass" // 修改密码
#define KLogin_Out @"Login/login_out"             // 退出登录
#define KPhone_Register @"Register/ajaxPhoneFind" // 判断手机号是否被注册
#define KVCode          @"Register/ajaxVcode"     // 判断重置密码验证码

#define kSMS_Register_VerifyPath      @"Register/createcode"  // 注册获取验证码
#define kSMS_ReSetPassword_VerifyPath @"Register/verifyPhone" // 找回密码获取验证码
// 分享
#define KShare    @"UserCenter/share"
// RSA test
#define kTestPath @"logintest/rsa"

// History
#define kHistoryPath @"history/getUserHistory"                        // 历程

/** 俱乐部 web页面*/
#define kClubUrl_Club    @"http://m.pandaol.net/panda/club/club.html"
#define KClubUrl_AddGame @"http://m.pandaol.net/panda/club/clubIndex.html"
#define kClubUrl_Pay     @"http://m.pandaol.net/panda/club/pay.html"

// 积分兑换
#define kIntegral_getUserCard       @"reword/getUserCard"               //获取用户卡组
#define kIntegral_getReward         @"reword/getReword"                 //换积
#define kIntegral_getMycoupon       @"Exchange/getMycoupon"             //获取个人所有积分和可兑换的RMB
#define kIntegral_getCybercafeInfo  @"Exchange/getCybercafeInfo"        //获取网吧信息
#define kIntegral_cvcode            @"Register/cvcode"                  //换网费发验证码
#define kIntegral_account           @"Exchange/account"                 //结算兑换
#define kIntegral_help              @"http://m.pandaol.net/panda/cafe/other.html"//兑换帮助

// Order
#define kOrderRequest    @"pay/assemble"                                // 向服务器发送订单 获取订单号
#define kWXOrderRequest  @"weiPay/createorder"                          // 创建微信订单

// 绑定召唤师
#define kBindingRoleGetList @"Lol_server/get"                           /**< 绑定召唤师*/
#define kDeleteRole @"Game_user/relieve"                                /**< 删除召唤师*/
#define kBindingRole @"Game_user/findUser"                              /**< 绑定召唤师*/
#define kSpecialInfo @"UserCenter/special"                              /**< 特殊字符*/

// 对战
#define kChallengerKCard @"Matching/card"                               /**< 获取当前的卡券*/
#define kChallengerLoLInfo @"Matching/lolInfo"                          /**< 获取当前对战信息*/
#define kChallengerValideData @"matching/valideData"                    /**< 出征验证*/
#define kChallengerChuzheng @"matching/matching"                        /**< 出征*/
#define kChallengerChuzhengAgain @"matching/matching"                   /**< 再次进行出征*/
#define kChallengerReady @"fight/getReady"                              /**< 进行出征*/
#define kChallengeWithCancel @"fight/abandonPreparation"                /**< 放弃比赛*/
#define kChallengerGetNormalHeader @"Register/avatarDefault"            /**< 获取当前默认头像*/
#define kChallengerCancelGameWithNotNotif @"fight/isAbandonGame"        /**< 放弃比赛，因为20秒内没有获取到通知*/
#define kChallengerCombatSettlement @"fight/combatSettlement"           /**< 请求战斗结束*/
#define kCombatSettlement @"fight/checkUserStatus"                      /**< 比赛入口检测*/
#define kChallengerReport @"fight/ReportList"                           /**< 举报列表*/
#define kChallengerReportSubmit @"fight/ReportSubmit"                   /**< 提交举报*/
#define kChallengerApplyCustomer @"fight/applyCustomer"                 /**< 申请客服介入*/
#define kChallengerConfirm @"fight/manualSettlementConfirm"             /**< 结算确认*/
#define kChallengerKing @"fight/keepKing"                               /**< 保留王座*/
#define kChallengerIsMatting @"Matching/is_matching"                       /**< 判断是否匹配上*/

// center
#define kCenterHeaderlist @"UserCenter/avatarDefault"                   /**< 选择头像*/
#define KCenterCertificateSubmit @"userCenter/RealNameSubmit"           /**< 实名认证提交*/
#define KCenterGetUserInfo @"userCenter/getUserInfo"                    /**< 获取客人信息*/
#define KCenterEditUserInfo @"userCenter/editUserInfo"                  /**> 修改个人信息*/
#define KCenterFeedback @"clientHelp/feedback"                          /**> 意见反馈*/
#define KCenterWelfare @"UserCenter/welfare"                            /**> 领福利 */
#define KCenterCustomerService @"ClientHelp/customerService"            /**> 客服中心*/
#define KCenterHistoryNotice @"history/historyNotice"                   /**> 回去新消息通知*/
#define KCenterHistoryUpdateCheck @"history/updateCheck"                /**> 历程消息更新*/
#endif /* PDUrlPath_h */
