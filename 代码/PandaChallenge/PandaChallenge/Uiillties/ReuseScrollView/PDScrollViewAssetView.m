//
//  PDScrollViewAssetView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDScrollViewAssetView.h"

@interface PDScrollViewAssetView()

@end

@implementation PDScrollViewAssetView

- (id)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.originalImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.originalImageView.frame = kScreenBounds;
    self.originalImageView.contentMode = UIViewContentModeScaleAspectFit;
    [self addSubview:self.originalImageView];
}

-(void)setTransferAsset:(ALAsset *)transferAsset{
    _transferAsset = transferAsset;
    UIImage *originalImage = [UIImage imageWithCGImage:[[self.transferAsset defaultRepresentation] fullScreenImage]];
    originalImage = [UIImage scaleDown:originalImage withSize:CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.width * originalImage.size.height/originalImage.size.width)];
    self.originalImageView.image = originalImage;
}

@end
