//
//  PDScrollViewAssetView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDReuseScrollView.h"
#import <AssetsLibrary/AssetsLibrary.h>

@class PDReuseScrollView;
@interface PDScrollViewAssetView : PDReuseView
@property (nonatomic,strong)ALAsset *transferAsset;
@property (nonatomic,strong)UIImageView *originalImageView;
@end
