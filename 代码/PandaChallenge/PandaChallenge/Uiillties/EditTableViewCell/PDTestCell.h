//
//  PDTestCell.h
//  PandaChallenge
//
//  Created by panda on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDEditTableViewCell.h"
/**
 *  继承PDEditTableViewCell
 *  自定义view(PDTestLabel)添加到cellContentView
 */
@interface PDTestCell : PDEditTableViewCell
@property (nonatomic,strong)UILabel *PDTestLabel;
@end
