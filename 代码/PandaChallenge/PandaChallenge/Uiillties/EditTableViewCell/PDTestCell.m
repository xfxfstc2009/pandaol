//
//  PDTestCell.m
//  PandaChallenge
//
//  Created by panda on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTestCell.h"

@implementation PDTestCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier delegate:(id<PDEditTableViewCellDelegate>)delegate tableView:(UITableView *)tableView rowHeight:(CGFloat)rowHeight rightButtonTitles:(NSArray *)titles rightButtonColors:(NSArray *)colors
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier delegate:delegate tableView:tableView rowHeight:rowHeight rightButtonTitles:titles rightButtonColors:colors]) {
        
        self.PDTestLabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 0, 200, rowHeight)];
        [self.cellContentView addSubview:self.PDTestLabel];
    }
    return self;
}
@end
