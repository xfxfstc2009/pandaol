//
//  PDEditTestViewController.m
//  PandaChallenge
//
//  Created by panda on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDEditTestViewController.h"
#import "PDTestCell.h"
@interface PDEditTestViewController ()<UITableViewDelegate,UITableViewDataSource,PDEditTableViewCellDelegate>
@property (nonatomic,strong)NSMutableArray *testMutableArray;
@property (nonatomic,strong)UITableView *PDTableView;
@end

@implementation PDEditTestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    [self createData];
    [self createTableView];
}
- (void)createData
{
    self.testMutableArray = [NSMutableArray array];
    for (int i=0; i <20; i++) {
        NSString *testStr = [NSString stringWithFormat:@"testCellIndex:%d",i];
        [self.testMutableArray addObject:testStr];
    }
}
- (void)createTableView
{
    self.PDTableView = [[UITableView alloc]initWithFrame:self.view.bounds];
    self.PDTableView.dataSource = self;
    self.PDTableView.delegate = self;
    [self.view addSubview:self.PDTableView];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.testMutableArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PDTestCell *testCell = [tableView dequeueReusableCellWithIdentifier:@"ID"];
    if (!testCell) {
        testCell = [[PDTestCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ID" delegate:self tableView:self.PDTableView rowHeight:60 rightButtonTitles:@[@"确定",@"取消"] rightButtonColors:@[[UIColor greenColor],[UIColor redColor]]];
    }
    testCell.backgroundColor = [UIColor yellowColor];
    testCell.PDTestLabel.text = self.testMutableArray[indexPath.row];
    return testCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"cell for IndexPath =%li",(long)indexPath.row);
}
#pragma mark - 点击按钮 返回按钮的index 和 cellIndexPath
- (void)buttonClickOnCell:(PDEditTableViewCell *)editCell atIndexPath:(NSIndexPath *)indexPath atIndex:(NSInteger)index
{
    NSLog(@"button for indexPath.row =%li,buttonIndex = %li",(long)indexPath.row,(long)index);
}
@end
