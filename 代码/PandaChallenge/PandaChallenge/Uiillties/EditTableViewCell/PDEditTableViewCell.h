//
//  PDEditTableViewCell.h
//  PandaChallenge
//
//  Created by panda on 16/3/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,PDEditTableViewCellState){
    PDEditTableViewCellStateUnexpanded = 0,    /**< cell未编辑模式*/
    PDEditTableViewCellStateExpanded   = 1,    /**< cell编辑模式*/
};
/**< 代理方法返回按钮点击的index和按钮所在的indexPath*/
@class PDEditTableViewCell;
@protocol PDEditTableViewCellDelegate <NSObject>
- (void)buttonClickOnCell:(PDEditTableViewCell *)editCell atIndexPath:(NSIndexPath *)indexPath atIndex:(NSInteger)index;
@end
@interface PDEditTableViewCell : UITableViewCell
/**< 当前cell的状态*/
@property (nonatomic,assign)PDEditTableViewCellState state;
/**< tableView*/
@property (nonatomic,strong)UITableView *tableView;
/**< 每个cell上右滑的ScrollView*/
@property (nonatomic,strong)UIScrollView *scrollView;
/**< buttons的背景视图*/
@property (nonatomic,strong)UIView *buttonsBackgroundView;
/**< cell的contentView*/
@property (nonatomic,strong)UIView *cellContentView;
/**< 点击代理*/
@property (nonatomic,weak)id<PDEditTableViewCellDelegate>delegate;
/**
 *  cell创建实例方法
 *
 *  @param style           UITableViewCellStyleDefault
 *  @param reuseIdentifier reuseIdentifier
 *  @param delegate        当前控制器
 *  @param tableView       当前控制器的tabView
 *  @param rowHeight       cell的高度
 *  @param titles          button标题的数组
 *  @param colors          button背景颜色的数组
 *
 *  @return 返回cell实例
 */
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier delegate:(id<PDEditTableViewCellDelegate>)delegate tableView:(UITableView *)tableView rowHeight:(CGFloat)rowHeight rightButtonTitles:(NSArray *)titles rightButtonColors:(NSArray *)colors;
@end
