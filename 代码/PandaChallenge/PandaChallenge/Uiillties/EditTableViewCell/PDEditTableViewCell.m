 //
//  PDEditTableViewCell.m
//  PandaChallenge
//
//  Created by panda on 16/3/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDEditTableViewCell.h"
#define PDRIGHTBUTTON_WIDTH 60
#define PDCELL_CHANGETOUNEXPANDED   @"PDEditCellNotificationChangeToUnexpanded"
#define PDTABLEVIEW_ENABLE_SCROLL   @"PDTableViewCellNotificationEnableScroll"
#define PDTABLEVIEW_UNENABLE_SCROLL @"PDTableViewCellNotificationUnEnableScroll"
@interface PDEditTableViewCell ()<UIScrollViewDelegate>
@property (nonatomic,strong)UIGestureRecognizer *panGestureRecognizer;

@end
static PDEditTableViewCell *_editingCell;
@implementation PDEditTableViewCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier delegate:(id<PDEditTableViewCellDelegate>)delegate tableView:(UITableView *)tableView rowHeight:(CGFloat)rowHeight rightButtonTitles:(NSArray *)titles rightButtonColors:(NSArray *)colors
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        CGFloat buttonsBackgroundView_W = titles.count * PDRIGHTBUTTON_WIDTH;
        /**< tableView*/
        self.tableView = tableView;
        self.delegate = delegate;
        /**< scrollView*/
        self.scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, rowHeight)];
        self.scrollView.contentSize = CGSizeMake(kScreenBounds.size.width + buttonsBackgroundView_W, rowHeight);
        self.scrollView.showsHorizontalScrollIndicator = NO;
        self.scrollView.showsVerticalScrollIndicator = NO;
        self.scrollView.bounces = NO;
        self.scrollView.delegate = self;
        self.scrollView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.scrollView];
        /**< buttonsBackgroundView*/
        self.buttonsBackgroundView = [[UIView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - buttonsBackgroundView_W, 0, buttonsBackgroundView_W, rowHeight)];
        [self.scrollView addSubview:self.buttonsBackgroundView];
        /**< button*/
        for (int index = 0; index <titles.count; index ++) {
            UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
            rightButton.tag = index;
            rightButton.frame = CGRectMake(index * PDRIGHTBUTTON_WIDTH, 0, PDRIGHTBUTTON_WIDTH, rowHeight);
            [rightButton setTitle:titles[index] forState:UIControlStateNormal];
            [rightButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            rightButton.backgroundColor = colors[index];
            [rightButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
            [self.buttonsBackgroundView addSubview:rightButton];
        }
        /**< cellContentView*/
        CGRect cellContentViewFrame = self.bounds;
        if ([self.tableView respondsToSelector:@selector(separatorInset)]){
            cellContentViewFrame = CGRectMake(0, 0, kScreenBounds.size.width, rowHeight);
        }
        self.cellContentView = [[UIView alloc]initWithFrame:cellContentViewFrame];
        self.cellContentView.backgroundColor = [UIColor whiteColor];
        [self.scrollView addSubview:self.cellContentView];
        /**<cellContentView点击手势*/
        UITapGestureRecognizer* tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(onTapGesture:)];
        [self.cellContentView addGestureRecognizer:tapGesture];
        /**< notification*/
        ///内部通知所有的cell可以滚动scrollView了
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationEnableScroll:) name:PDTABLEVIEW_ENABLE_SCROLL object:nil];
        ///内部通知所有的cell不可以滚动scrollView(除当前编辑的这个外)
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationUnenableScroll:) name:PDTABLEVIEW_UNENABLE_SCROLL object:nil];
    }
    return self;
}
/**< 移除通知*/
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
/**< 重用设置偏移量为zero*/
- (void)prepareForReuse
{
    [super prepareForReuse];
    [self.scrollView setContentOffset:CGPointZero];
}
#pragma mark - setState
/**< 重写set方法*/
/**< 1.展开状态， 1)tabview和cell的scrollView不能滑动 2)添加pan手势,用于收起展开状态*/
/**< 2.未展开状态 1)相反处理*/
- (void)setState:(PDEditTableViewCellState)state
{
    _state = state;
    if (state == PDEditTableViewCellStateUnexpanded) {
        _editingCell = nil;
        self.tableView.scrollEnabled = YES;
        self.tableView.allowsSelection = YES;
        //恢复到未展开状态
        [self.scrollView setContentOffset:CGPointZero animated:YES];
        //通知cell不能滚动
        [[NSNotificationCenter defaultCenter] postNotificationName:PDTABLEVIEW_ENABLE_SCROLL object:nil];
        //移除手势
        if (self.panGestureRecognizer) {
            [self.tableView removeGestureRecognizer:self.panGestureRecognizer];
            self.panGestureRecognizer = nil;
        }
        //防止scrollView展开未复原时点击cell
        self.tableView.userInteractionEnabled = NO;
        double delayInSeconds = 0.3;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            self.tableView.userInteractionEnabled = YES;
        });
    }else if(state == PDEditTableViewCellStateExpanded){
        _editingCell = self;
        self.tableView.scrollEnabled = NO;
        self.tableView.allowsSelection = NO;
        //恢复到展开状态
        [self.scrollView setContentOffset:CGPointMake(self.buttonsBackgroundView.size_width,0) animated:YES];
        //通知cell不能滚动
        [[NSNotificationCenter defaultCenter] postNotificationName:PDTABLEVIEW_UNENABLE_SCROLL object:nil];
        //往tableView上添加一个手势处理,使得在tableView上的拖动也只是影响当前这个cell的scrollView
        if (!self.panGestureRecognizer){
            self.panGestureRecognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(onPanGesture:)];
            [self.tableView addGestureRecognizer:self.panGestureRecognizer];
        }

    }
}
#pragma mark - notification
- (void)notificationEnableScroll:(NSNotification *)notification
{
    self.scrollView.scrollEnabled = YES;
}
- (void)notificationUnenableScroll:(NSNotification *)notification
{
    self.scrollView.scrollEnabled = NO;
}
#pragma mark - gestureRecognizer
- (void)onPanGesture:(UIPanGestureRecognizer *)pangestureRecognizer
{
    if (!_editingCell)
        return;
    if (pangestureRecognizer.state == UIGestureRecognizerStateChanged){
        CGFloat translate_x = [pangestureRecognizer translationInView:_editingCell.tableView].x;
        CGFloat offset_x = self.buttonsBackgroundView.frame.size.width;
        CGFloat move_offset_x = offset_x-translate_x;
        if (move_offset_x <=0) {
            move_offset_x = 0;
        }
        [_editingCell.scrollView setContentOffset:CGPointMake(move_offset_x, 0)];
    }
    else if (pangestureRecognizer.state == UIGestureRecognizerStateEnded||
             pangestureRecognizer.state == UIGestureRecognizerStateCancelled ||
             pangestureRecognizer.state == UIGestureRecognizerStateFailed){
        _editingCell.state = PDEditTableViewCellStateUnexpanded;
    }
}
- (void)onTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer
{
    if (_editingCell){
        _editingCell.state = PDEditTableViewCellStateUnexpanded;
    }
    else{
        if ([self.tableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]){
            NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:self];
            [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:cellIndexPath];
        }
    }

}
#pragma mark - buttonClick
- (void)buttonClick:(UIButton *)sender
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:self];
    if ([self.delegate respondsToSelector:@selector(buttonClickOnCell:atIndexPath:atIndex:)]) {
        [self.delegate buttonClickOnCell:self atIndexPath:indexPath atIndex:sender.tag];
    }
    self.state = PDEditTableViewCellStateUnexpanded;
}
#pragma mark - scrollViewDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    self.buttonsBackgroundView.transform = CGAffineTransformMakeTranslation(scrollView.contentOffset.x, 0.0f);
}

-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    if (scrollView.contentOffset.x >= self.buttonsBackgroundView.frame.size.width/2){
        self.state = PDEditTableViewCellStateExpanded;
    }
    else{
        self.state = PDEditTableViewCellStateUnexpanded;
    }
}

@end
