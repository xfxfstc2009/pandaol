//
//  PDProgressView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 进度条
#import <UIKit/UIKit.h>

@interface PDProgressView : UIView

@property (nonatomic,assign)CGFloat lineWidth;          /**< 线的宽度*/

-(void)setPercent:(CGFloat)percent;
@end
