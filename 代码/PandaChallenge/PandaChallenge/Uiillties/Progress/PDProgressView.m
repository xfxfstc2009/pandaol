//
//  PDProgressView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProgressView.h"

@interface PDProgressView(){
    CAShapeLayer *shapLayer;
    CAShapeLayer *progressLayer;
}

@end

@implementation PDProgressView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self pageSetting];
    }
    return self;
}

-(void)pageSetting{
    self.lineWidth = 10.;
    [self create1];
}

-(void)drawRect:(CGRect)rect{

}

#pragma mark - 1. 画一个容器
-(void)create1{
    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint:CGPointMake(LCFloat(11), (self.size_height - self.lineWidth) / 2.)];
    [bezierPath addLineToPoint:CGPointMake(self.size_width - LCFloat(11), (self.size_height - self.lineWidth) / 2.)];
    bezierPath.lineWidth = self.lineWidth;
    // 设置线条的圆角
    bezierPath.lineCapStyle = kCGLineCapRound;
    bezierPath.lineJoinStyle = kCGLineJoinRound;
    UIColor *fillColor = [UIColor lightGrayColor];
    [fillColor setStroke];
    [bezierPath stroke];
    
    shapLayer = [CAShapeLayer layer];
    shapLayer.frame = self.bounds;
    [self.layer addSublayer:shapLayer];
    shapLayer.fillColor = [[UIColor yellowColor] CGColor];
    shapLayer.strokeColor = [[UIColor yellowColor] CGColor];//指定path的渲染颜色
    shapLayer.opacity = 0;
    shapLayer.strokeStart = 0;
    shapLayer.strokeEnd = 1.0;
    shapLayer.lineCap = kCALineCapRound;//指定线的边缘是圆的
    shapLayer.lineWidth = self.lineWidth;//线的宽度
    
    shapLayer.path = [bezierPath CGPath];
    
    
    //【2.0】
    // 生产出一个圆形路径的Layer
    
    progressLayer = [CAShapeLayer layer];
    progressLayer.path = bezierPath.CGPath;
    progressLayer.strokeColor = [UIColor lightGrayColor].CGColor;
    progressLayer.fillColor = [[UIColor lightGrayColor] CGColor];
    progressLayer.lineWidth = self.lineWidth;
    progressLayer.lineCap = kCALineCapRound;
    
    // 可以设置出圆的完整性
    progressLayer.strokeStart = 0;
    progressLayer.strokeEnd = 1.0;
    
    CALayer *gradientLayer = [CALayer layer];
    
    CAGradientLayer *gradientLayer1 =  [CAGradientLayer layer];
    //    gradientLayer1.frame = self.bounds;
    gradientLayer1.frame = CGRectMake(-(MAX(self.size_height, self.size_width)) / 2., -(MAX(self.size_height, self.size_width)) / 2., 2 * MAX(self.size_width, self.size_height),2 * MAX(self.size_width, self.size_height));
    // 创建颜色数组
    NSMutableArray *colors = [NSMutableArray array];
    [colors addObject:(id)[UIColor colorWithRed:87 / 255. green:198/255. blue:196/255. alpha:1].CGColor];
    [colors addObject:(id)[UIColor colorWithRed:136 / 255. green:132/255. blue:246/255. alpha:1].CGColor];
    [gradientLayer1 setColors:colors];
    [gradientLayer1 setLocations:@[@0.5,@0.9,@1 ]];
    [gradientLayer1 setStartPoint:CGPointMake(0.5, 1)];
    [gradientLayer1 setEndPoint:CGPointMake(0.5, 0)];
    [gradientLayer addSublayer:gradientLayer1];
    gradientLayer.mask = progressLayer;
    [self.layer addSublayer:gradientLayer];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.duration = 5;
    animation.repeatCount = MAXFLOAT;
    animation.fromValue = [NSNumber numberWithDouble:0];
    animation.toValue = [NSNumber numberWithDouble:M_PI*2];
    [gradientLayer1 addAnimation:animation forKey:@"transform"];
    

}

#pragma mark - 2.执行动画
-(void)setPercent:(CGFloat)percent{
    [CATransaction begin];
    [CATransaction setDisableActions:YES];
    [CATransaction setAnimationTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn]];
    [CATransaction setAnimationDuration:50];
    progressLayer.strokeEnd = percent;
    [CATransaction commit];
    
}

@end
