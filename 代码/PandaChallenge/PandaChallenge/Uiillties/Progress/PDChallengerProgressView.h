//
//  PDChallengerProgressView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "WhaleView.h"

@interface PDChallengerProgressView : UIView

// 修改progress
-(void)animationWithProgress:(CGFloat)progress;
@property (nonatomic,strong)WhaleView *sharkView;
@end
