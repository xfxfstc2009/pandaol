//
//  PDRSAManager.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// RSA 的方法
#import <Foundation/Foundation.h>

@interface PDRSAManager : NSObject

+(NSString *)decodeWithPublicKeyWithString:(NSString *)code;

@end
