//
//  PDRSAManager.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRSAManager.h"
#import "GTMBase64.h"
#import "RSA.h"

#define kBase64Key @"LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0NCk1JR2ZNQTBHQ1NxR1NJYjNEUUVCQVFVQUE0R05BRENCaVFLQmdRQzMvL3NSMnRYdzB3ckMyRHlTeDh2TkdscXQNCjNZN2xkVTkrTEJMSTZlMUtTNWxmYzVqbFRHRjdLQlRTa0NIQk0zb3VFSFdxcDFaSjg1aUplNTlhRjVnSUIya2wNCkJkNmg0d3JiYkhBMlhFMXNxMjF5a2phL0dxeDcvSVJpYTN6UWZ4R3YvcUVreUdPeCtYQUxWb09sWnFEd2g3Nm8NCjJuMXZQMUQrdEQzYW1Ic0s3UUlEQVFBQg0KLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0t"


@implementation PDRSAManager


#pragma mark - 1.base64解密获取当前的PublicKey
+(NSString *)decodeWithPublicKeyWithString:(NSString *)code{
    // 1. 解密
    NSString *publicKey = [GTMBase64 decodeBase64String:kBase64Key];
    NSString *encWithPubKey;
    encWithPubKey = [RSA encryptString:code publicKey:publicKey];
    return  encWithPubKey;
}

@end
