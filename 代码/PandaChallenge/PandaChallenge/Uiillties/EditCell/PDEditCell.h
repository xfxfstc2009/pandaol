//
//  PDEditCell.h
//  可以侧滑编辑的单元格
//
//  Created by 盼达 on 16/4/19.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, EditTableViewCellState) {
    EditTableViewCellStateNormal,   /** 普通状态*/
    EditTableViewCellStateEdited,   /** 可编辑状态*/
};

@interface PDEditCell : UITableViewCell
@property (strong, nonatomic) UIView *subContentView;
@property (strong, nonatomic) UIImageView *backgroundImageView;
@property (assign, nonatomic) CGSize customCellSize;
@property (assign, nonatomic) EditTableViewCellState state;

@property (nonatomic,strong)UIButton *button1;
@property (nonatomic,strong)UIButton *button2;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier tableView:(UITableView *)tableView buttonNameArray:(NSArray *)buttonNameArray buttonCompletion:(void(^)(NSInteger buttonIndex,UIButton *button))buttonCompletion;
@end
