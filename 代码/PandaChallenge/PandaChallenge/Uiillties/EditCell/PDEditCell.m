//
//  PDEditCell.m
//  可以侧滑编辑的单元格
//
//  Created by 盼达 on 16/4/19.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import "PDEditCell.h"

static const CGFloat button_width = 44;
static const NSTimeInterval animationDuration = .2;

/** 通知*/
static NSString *NotificationEditCellNormal = @"NotificationEditCellNormal";
static NSString *NotificationEditCellEdited = @"NotificationEditCellEdited";

/** 记录某一个滑动单元格*/
static PDEditCell *editCell;

@interface PDEditCell ()<UIScrollViewDelegate>
@property (strong, nonatomic) UIView *buttonView;
@property (strong, nonatomic) UIScrollView *basicView;
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIPanGestureRecognizer *pan;
@property (strong, nonatomic) UITapGestureRecognizer *tap;
@property (strong, nonatomic) NSArray *nameArray;
@property (assign, nonatomic) CGFloat button_height;
@property (strong, nonatomic) NSMutableArray *buttonArray;
@end

@implementation PDEditCell

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self removeObserver:self forKeyPath:@"state"];
    editCell = nil;
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier tableView:(UITableView *)tableView buttonNameArray:(NSArray *)buttonNameArray buttonCompletion:(void (^)(NSInteger, UIButton *))buttonCompletion{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.tableView = tableView;
        self.nameArray = buttonNameArray;
        [self constantForInit];
        [self addObserver:self forKeyPath:@"state" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
        [self customCellWithButtonNames:buttonNameArray buttonCompletion:buttonCompletion];
    }
    return self;
}

- (void)constantForInit {
    self.buttonArray = [NSMutableArray array];
}

- (void)customCellWithButtonNames:(NSArray *)buttonNames buttonCompletion:(void(^)(NSInteger, UIButton *))buttonCompletion {
    self.backgroundColor = [UIColor clearColor];
    
    UIScrollView *basicView = [[UIScrollView alloc] init];
    self.basicView = basicView;
    basicView.delegate = self;
    basicView.frame = self.bounds;
    basicView.contentSize = CGSizeMake(basicView.bounds.size.width + buttonNames.count * button_width, basicView.bounds.size.height);
    basicView.showsHorizontalScrollIndicator = NO;
    basicView.showsVerticalScrollIndicator   = NO;
    basicView.bounces = NO;
    [self.contentView addSubview:basicView];
    
    UIView *buttonView = [[UIView alloc] init];
    self.buttonView = buttonView;
    buttonView.frame = CGRectMake(basicView.bounds.size.width-button_width * self.nameArray.count, 0, buttonNames.count * button_width, basicView.bounds.size.height);
    buttonView.backgroundColor = [UIColor clearColor];
    [basicView addSubview:buttonView];
    
    for (int i = 0; i < buttonNames.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        if (i == 0) {
            [button setBackgroundImage:[UIImage imageNamed:@"icon_challenge_button_bg_black"] forState:UIControlStateNormal];
            self.button1 = button;
        } else {
            [button setBackgroundImage:[UIImage imageNamed:@"icon_challenge_button_bg_red"] forState:UIControlStateNormal];
            self.button2 = button;
        }
        button.frame = CGRectMake(i*button_width, 0, button_width, buttonView.frame.size.height);
        [button setTitle:[buttonNames objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [buttonView addSubview:button];
        [self.buttonArray addObject:button];
        [button buttonWithBlock:^(UIButton *button) {
            if (buttonCompletion) {
                buttonCompletion(i,button);
                self.state = EditTableViewCellStateNormal;
            }
        }];
        button.hidden = YES;
    }
    
    self.subContentView = [[UIView alloc] init];
    self.subContentView.backgroundColor = [UIColor clearColor];
    self.subContentView.frame = basicView.bounds;
    [basicView addSubview:self.subContentView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSubContentView:)];
    [self.subContentView addGestureRecognizer:tap];
}

- (void)setSubContentView:(UIView *)subContentView {
    _subContentView = subContentView;
    self.state = EditTableViewCellStateNormal;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editCellForNormalNoti:) name:NotificationEditCellNormal object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(editCellForEditedNoti:) name:NotificationEditCellEdited object:nil];
}

- (void)setBackgroundImageView:(UIImageView *)backgroundImageView {
    _backgroundImageView = backgroundImageView;
    NSAssert(_backgroundImageView, @"必须创建背景图实例");
    _backgroundImageView.frame = self.bounds;
    [self.subContentView addSubview:_backgroundImageView];
}

#pragma mark - 当外界有自定义单元格宽高限定的时候，重新设置frame
- (void)setCustomCellSize:(CGSize)customCellSize {
    _customCellSize = customCellSize;
    CGFloat bgSpace = HSFloat(5.5); // 背景图透明部分
    self.basicView.frame = CGRectMake(0, 0, customCellSize.width, customCellSize.height);
    self.basicView.contentSize = CGSizeMake(self.basicView.bounds.size.width + self.nameArray.count * button_width, self.basicView.bounds.size.height);
    self.buttonView.frame = CGRectMake(self.basicView.bounds.size.width-button_width * self.nameArray.count, bgSpace, self.nameArray.count * button_width, self.basicView.bounds.size.height-2*bgSpace);
    for (UIButton *button in self.buttonArray) {
        button.frame = CGRectMake(button.frame.origin.x, button.frame.origin.y, button_width, CGRectGetHeight(self.buttonView.frame));
    }
    self.subContentView.frame = self.basicView.bounds;
    self.backgroundImageView.frame = self.basicView.bounds;
}

- (void)editCellForNormalNoti:(NSNotification *)noti {
    self.basicView.scrollEnabled = YES;
}

- (void)editCellForEditedNoti:(NSNotification *)noti {
    self.basicView.scrollEnabled = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    editCell = self;
    CGPoint point = scrollView.contentOffset;
    self.buttonView.transform = CGAffineTransformMakeTranslation(point.x, point.y);
    if (scrollView.contentOffset.x >= 5){
        self.button1.hidden = NO;
        self.button2.hidden = NO;
    }
    if (scrollView.contentOffset.x <= 5){
        self.button1.hidden = YES;
        self.button2.hidden = YES;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    CGPoint point = scrollView.contentOffset;
    if (point.x >= .5 * self.buttonView.frame.size.width) {
        [UIView animateWithDuration:animationDuration animations:^{
            scrollView.contentOffset = CGPointMake(self.buttonView.frame.size.width, 0);
        }];
        self.state = EditTableViewCellStateEdited;
    } else {
        [UIView animateWithDuration:animationDuration animations:^{
            scrollView.contentOffset = CGPointMake(0, 0);
            self.buttonView.transform = CGAffineTransformMakeTranslation(-scrollView.contentOffset.x, 0);
        }];
        self.state = EditTableViewCellStateNormal;
    }
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"state"]) {
        if (self.state == EditTableViewCellStateNormal) {
            if (self.pan) {
                [editCell removeGestureRecognizer:self.pan];
                self.pan = nil;
            }
            if (self.tap) {
                [self.tableView removeGestureRecognizer:self.tap];
                self.tap = nil;
            }
            editCell = nil;
            [self.basicView setContentOffset:CGPointZero animated:YES];
            self.tableView.scrollEnabled = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationEditCellNormal object:nil];
        } else {
            editCell = self;
            self.tableView.scrollEnabled = NO;
            [[NSNotificationCenter defaultCenter] postNotificationName:NotificationEditCellEdited object:nil];
            UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panOnCell:)];
            self.pan = pan;
            [editCell addGestureRecognizer:pan];
            
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTableView:)];
            self.tap = tap;
            [self.tableView addGestureRecognizer:tap];
            
        }
    }
}

#pragma mark - 手势
- (void)panOnCell:(UIPanGestureRecognizer *)pan {
    // 限制有一个单元格滑开的时候，不能再滑开其他的单元格
    if (self != editCell) {
        return;
    }
    if (pan.state == UIGestureRecognizerStateChanged) {
        CGPoint translatePoint = [pan translationInView:self.tableView];
        NSLog(@"translate %@",NSStringFromCGPoint(translatePoint));
        CGFloat max_x = self.buttonView.frame.size.width;
        if (translatePoint.x - max_x >= 0) {
            translatePoint.x = max_x;
        }
        if (translatePoint.x <= 0) {
            translatePoint.x = 0;
        }
        [self.basicView setContentOffset:CGPointMake(max_x - translatePoint.x, 0)];
    } else if ((pan.state == UIGestureRecognizerStateCancelled) || (pan.state == UIGestureRecognizerStateEnded) || (pan.state == UIGestureRecognizerStateFailed)) {
        self.state = EditTableViewCellStateNormal;
    }
}

- (void)tapOnTableView:(UITapGestureRecognizer *)tap {
    [editCell.basicView setContentOffset:CGPointZero animated:YES];
    self.state = EditTableViewCellStateNormal;
}

- (void)tapOnSubContentView:(UITapGestureRecognizer *)tap {
    if (self.state == EditTableViewCellStateNormal && editCell == nil) {
        NSIndexPath *cellIndexPath = [self.tableView indexPathForCell:self];
        if ([self.tableView.delegate respondsToSelector:@selector(tableView:didSelectRowAtIndexPath:)]) {
            [self.tableView.delegate tableView:self.tableView didSelectRowAtIndexPath:cellIndexPath];
        }
    } else {
        [editCell.basicView setContentOffset:CGPointZero animated:YES];
        self.state = EditTableViewCellStateNormal;
    }
}

@end
