//
//  MMHAliPayHandle.m
//  MamHao
//
//  Created by SmartMin on 15/5/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "PDAliPayHandle.h"
#import "DataSigner.h"
#import <objc/runtime.h>
#import "payRequsestHandler.h"
#import "WXApi.h"
#import "PDPrepayidModel.h"

//AliPay
#define PrivateKey @"MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAMTVd7H5cnbC/4N9hQlBNhbsksT8JbLtO/xemTk/lSEP1gqNnX4k89eHzu8zto/WrcQlBHONVluPh/OxxBWIJIFTYPqUekLEZz+AA4qcbLN8hDTSPlbe5DP3NFzJljUhMIP/Nc50ffXSaFu6/RMFqJBIA6dkqkbAlEWG/bl41LXfAgMBAAECgYB1uWr+gkAosdYasc8Iyvzr1xCtSlXN3z/aYEXqTJIIFS2iYDLLCJTi2rI0tMxC2VZSkwVHi0gUORNJ+I9bhXK27LluiELxdFfMbFikDJdlLMiMRGFHoIkaIRUHyJV4Peg10d5xktOkfEvmb5/bafp7TtT57CeL6Q2bk8bhuN10SQJBAPm2qk0aN93i8Rhinf0Wtvx8HhksoSTo3bOFnKNvpjwxPKvf/N9R6RT6wDk/8HCtwv9typz8aEQYcghL8vQkNHUCQQDJygHk98Fxc5aQ7KZkT7LJ8IEsis2cqlceZujDEm4w1wx6zQ+TAkSo5E49hPlLGAowzcOJUSqC0nAJX4Gd4qaDAkEArh8jDPRVNFFEkB5jz9CA8/mP+znVe6ksvjtSh9wYbCxhA/ABoa65+jkGxGTDQa7II9foyiJuid0J1qMu2/JK6QJBAJDYi2GTIm1QnlyrMolA2EKye9bAT/VMJLry/dPA8A3o39FqTuqkrypYr3zjbZskx3Pez6RK+evsKHXh84WkwwcCQQCTKJ8bN2lK9fEkWZ9WAskwK7ULOIOCAfNYTcYBVSI7Txv8AvpUt1GMo+xE48xps18jS2u/wuad0N0qww6z3/iC"

#define ALIPAY_NOTIFY_URL @"http://www.pandaol.com/mobile/Pay/app_notify"
#define PARTNER    @"2088122519632733"
#define SELLER     @"pandaol@aliyun.com"

static char alipayCallBackKey;

@implementation PDAliPayHandle

#pragma mark - go pay
+ (void)payWithOrder:(PDPayOrderModel *)payOrder block:(void(^)(NSInteger code,NSString *errMsg))block{
    objc_setAssociatedObject(self, &alipayCallBackKey, block, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    NSString *privateKey = PrivateKey;
    payOrder.notifyURL = ALIPAY_NOTIFY_URL;
    payOrder.partner = PARTNER;
    payOrder.seller = SELLER;
    payOrder.service = @"mobile.securitypay.pay";
    payOrder.paymentType = @"1";
    payOrder.inputCharset = @"utf-8";
    payOrder.itBPay = @"30m";
    payOrder.showUrl = @"m.alipay.com";
    
    //订单ID
    //payOrder.tradeNO = [TenpayUtil md5:[NSString stringWithFormat:@"%@",[NSDate date]]];
    
    NSString *fileMessage = @"";
    if ((!payOrder.partner) || (!payOrder.seller) || (!privateKey.length)){
        fileMessage = @"主要参数缺省";
    }
    
    if (!payOrder.tradeNO){
        fileMessage = @"没有单号";
    } else if (!payOrder.productName){
        fileMessage = @"没有商品名称";
    } else if (!payOrder.productDescription){
        fileMessage = @"没有商品描述";
    } else if (!payOrder.amount){
        fileMessage = @"没有商品价格";
    }
    
    if (fileMessage.length){
        [[UIAlertView alertViewWithTitle:@"错误" message:fileMessage buttonTitles:@[@"确定"] callBlock:nil]show];
    }
    
    NSString *appScheme = @"panda-alipay";
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [payOrder description];
    NSLog(@"orderSpec = %@",orderSpec);
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(privateKey);
    NSString *signedString = [signer signString:orderSpec];
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"", orderSpec, signedString, @"RSA"];
        
        /*
         9000 订单支付成功
         8000 正在处理中
         4000 订单支付失败
         6001 用户中途取消
         6002 网络连接出错
         */
        __weak typeof(self)weakSelf = self;
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSInteger code,NSString *errMsg) = objc_getAssociatedObject(strongSelf, &alipayCallBackKey);
            if (block){
                NSNumber *errorCode = [resultDic objectForKey:@"resultStatus"];
                NSString *errMsg = @"";
                if (errorCode.intValue == 9000){
                    errMsg = @"订单支付成功";
                } else if (errorCode.intValue == 8000){
                    errMsg = @"正在处理中";
                } else if (errorCode.intValue == 4000){
                    errMsg = @"订单处理失败";
                } else if (errorCode.intValue == 6001){
                    errMsg = @"用户中途取消";
                } else if (errorCode.intValue == 6002){
                    errMsg = @"网络连接出错";
                }
                if (block){
                    block(errorCode.intValue,errMsg);
                }
            }
        }];
    }
}

@end


