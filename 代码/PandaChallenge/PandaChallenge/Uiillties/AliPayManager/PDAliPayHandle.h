//
//  MMHAliPayHandle.h
//  MamHao
//
//  Created by SmartMin on 15/5/26.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDPayOrderModel.h"
#import <AlipaySDK/AlipaySDK.h>


@interface PDAliPayHandle : NSObject

+ (void)payWithOrder:(PDPayOrderModel *)payOrder block:(void(^)(NSInteger code,NSString *errMsg))block;

@end
