//
//  PDInviteModel.m
//  PandaChallenge
//
//  Created by panda on 16/4/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInviteModel.h"

@implementation PDInviteModel
- (NSDictionary *)modelKeyJSONKeyMapper
{
    return @{@"content":@"template",@"imageUrl":@"img"};
}
@end
