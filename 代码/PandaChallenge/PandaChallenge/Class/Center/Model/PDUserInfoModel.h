//
//  PDUserInfoModel.h
//  PandaChallenge
//
//  Created by 巨鲸 on 16/4/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDUserInfoModel : PDFetchModel
@property (nonatomic,strong)NSDictionary *userinfo;
@end
