//
//  PDPortraitDefaultModel.h
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDPortraitDefaultModel : PDFetchModel
@property (nonatomic,strong)NSArray *avatarList;   //头像数组
@end
