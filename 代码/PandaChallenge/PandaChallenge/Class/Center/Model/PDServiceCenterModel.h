//
//  PDServiceCenterModel.h
//  PandaChallenge
//
//  Created by 巨鲸 on 16/4/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDServiceCenterModel : PDFetchModel
@property (nonatomic,strong)NSArray *info;
@end
