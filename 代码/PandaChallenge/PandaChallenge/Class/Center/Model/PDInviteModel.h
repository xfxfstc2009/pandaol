//
//  PDInviteModel.h
//  PandaChallenge
//
//  Created by panda on 16/4/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDInviteModel : PDFetchModel
@property (nonatomic,copy)NSString *content;        /**< 分享内容*/
@property (nonatomic,copy)NSString *url;            /**< 回调网址*/
@property (nonatomic,copy)NSString *imageUrl;       /**< 分享图片*/
@end
