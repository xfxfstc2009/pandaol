//
//  PDPortraitDefaultCollectionViewCell.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPortraitDefaultCollectionViewCell.h"

@interface PDPortraitDefaultCollectionViewCell ()

@end

@implementation PDPortraitDefaultCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.portraitImage = [[PDImageView alloc] initWithFrame:CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height)];
        self.portraitImage.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:self.portraitImage];
        
        
        
        self.selectImage = [[UIImageView alloc] initWithFrame:CGRectMake(self.portraitImage.frame.size.width-20, self.portraitImage.frame.size.height-20, 20, 20)];
        

        [self.contentView addSubview:self.selectImage];
        

    }
    return self;
}


- (void)setSelected:(BOOL)selected{
    if (selected) {
        self.selectImage.image = [UIImage imageNamed:@"icon_center_gou"];
        
    }else{
        self.selectImage.image = [UIImage imageNamed:@"round1"];
    }
}
@end
