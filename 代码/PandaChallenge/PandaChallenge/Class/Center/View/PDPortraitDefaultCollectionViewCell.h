//
//  PDPortraitDefaultCollectionViewCell.h
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDPortraitDefaultCollectionViewCell : UICollectionViewCell


@property (nonatomic,strong)PDImageView *portraitImage;
@property (nonatomic,strong)UIImageView *selectImage;

@end
