//
//  PDFastReportViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/4/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFastReportViewController.h"

@interface PDFastReportViewController ()
@property (nonatomic,strong)UIView *popView; /**> 弹出view <**/


@end

@implementation PDFastReportViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
}

- (void)pageSetting
{
    
    
}


- (void)createView
{
    self.popView = [[UIView alloc] initWithFrame:CGRectMake(LCFloat(30), self.view.frame.size.height/2 - LCFloat(100), self.view.frame.size.width - LCFloat(60), LCFloat(200))];
    self.popView.backgroundColor = [UIColor whiteColor];
    self.popView.layer.cornerRadius = 5;
    [self.view addSubview:self.popView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
