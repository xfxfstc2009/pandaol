//
//  PDChangePasswordViewController.m
//  PandaChallenge
//
//  Created by panda on 16/4/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChangePasswordViewController.h"
#import "PDSureButton.h"
#define PDLeftRight_Space 53
@interface PDChangePasswordViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong)UITableView *changeTableView;
@property (nonatomic,strong)UITextField *currentPwdTextField;
@property (nonatomic,strong)UITextField *changePwdTextField;
@property (nonatomic,strong)UITextField *changePwdAgainTextField;
@property (nonatomic,strong)NSArray     *changeArray;
@property (nonatomic,strong)PDSureButton *commitButton;
@property (nonatomic,strong)JCAlertView *alertView;
@end
@implementation PDChangePasswordViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self creatChangeTableView];
    [self createkeyboardNotification];
}

#pragma mark - pageSetting
- (void)pageSetting
{
    self.barMainTitle = @"修改密码";
    __weak typeof(self) weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"login_back"] barHltImage:nil action:^{
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
}
#pragma mark - arrayWithInit
- (void)arrayWithInit
{
    self.changeArray = @[@[@"当前密码",@"新密码",@"再次输入新密码"],@[@"提示",@"提示"],@[@"提交按钮"]];
}
#pragma mark - UITableView
- (void)creatChangeTableView
{
    if (!self.changeTableView) {
        self.changeTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height) style:UITableViewStylePlain];
        self.changeTableView.delegate = self;
        self.changeTableView.dataSource = self;
        self.changeTableView.scrollEnabled = NO;
        self.changeTableView.backgroundColor = [UIColor clearColor];
        self.changeTableView.showsVerticalScrollIndicator = NO;
        self.changeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.changeTableView];
    }
}
#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.changeArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sectionArray = self.changeArray[section];
    return sectionArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0) {       //输入框
        static NSString *cellForIdentifierRowOne = @"cellForIdentifierRowOne";
        UITableViewCell *cellForRowOne = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierRowOne];
        if (!cellForRowOne) {
            cellForRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierRowOne];
            cellForRowOne.backgroundColor = [UIColor clearColor];
            cellForRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            //1.iconButton
            UIButton *iconButton = [UIButton buttonWithType:UIButtonTypeCustom];
            iconButton.stringTag = @"iconButton";
            [cellForRowOne addSubview:iconButton];
            //2.textField
            UITextField *textField = [[UITextField alloc]init];
            textField.stringTag = @"textField";
            textField.delegate = self;
            textField.font = [UIFont systemFontOfCustomeSize:13];
            textField.secureTextEntry = YES;
            textField.borderStyle = UITextBorderStyleNone;
            textField.backgroundColor = [UIColor clearColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            [textField addTarget:self action:@selector(textFieldChangeManager:) forControlEvents:UIControlEventEditingChanged];
            [cellForRowOne addSubview:textField];
            //3.bottomLine
            UIView *line = [[UIView alloc]init];
            line.stringTag = @"line";
            line.backgroundColor = [UIColor lightGrayColor];
            [cellForRowOne addSubview:line];
        }
        UIButton *iconButton = (UIButton *)[cellForRowOne viewWithStringTag:@"iconButton"];
        UITextField *textField = (UITextField *)[cellForRowOne viewWithStringTag:@"textField"];
        UIView *line = (UIView *)[cellForRowOne viewWithStringTag:@"line"];
        //frame
        iconButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), LCFloat(15), cellHeight - LCFloat(22), cellHeight -LCFloat(22));
        textField.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(20), LCFloat(12), kScreenBounds.size.width - (LCFloat(PDLeftRight_Space) + LCFloat(20) +CGRectGetMaxX(iconButton.frame)),cellHeight - LCFloat(12));
        line.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(10), CGRectGetMaxY(textField.frame), CGRectGetWidth(textField.frame) + LCFloat(10), 1);
        //赋值
        [iconButton setBackgroundImage:[UIImage imageNamed:@"login_lock"] forState:UIControlStateNormal];
        if (indexPath.row == 0) {
            textField.placeholder = @"当前密码";
            textField.returnKeyType = UIReturnKeyNext;
            self.currentPwdTextField = textField;
        }else if(indexPath.row == 1){
            textField.placeholder = @"新密码";
            textField.returnKeyType = UIReturnKeyNext;
            self.changePwdTextField = textField;
        }else{
            textField.placeholder = @"再次输入新密码";
            textField.returnKeyType = UIReturnKeyDone;
            self.changePwdAgainTextField = textField;
        }
        return cellForRowOne;
    }else if (indexPath.section == 1){      //提示
        static NSString *cellForIdentifierRowTwo = @"cellForIdentifierRowTwo";
        UITableViewCell *cellForRowTwo = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierRowTwo];
        if (!cellForRowTwo) {
            cellForRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierRowTwo];
            cellForRowTwo.backgroundColor = [UIColor clearColor];
            cellForRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            //1.iconImageView
            UIImageView *iconImageView = [[UIImageView alloc]init];
            iconImageView.stringTag = @"iconImageView";
            [cellForRowTwo addSubview:iconImageView];
            //2.tipLabel
            UILabel *tipLabel = [[UILabel alloc]init];
            tipLabel.font = [UIFont systemFontOfCustomeSize:12];
            tipLabel.textColor = [UIColor grayColor];
            tipLabel.stringTag = @"tipLabel";
            [cellForRowTwo addSubview:tipLabel];
        }
        UIImageView *iconImageView = (UIImageView *)[cellForRowTwo viewWithStringTag:@"iconImageView"];
        UILabel *tipLabel = (UILabel *)[cellForRowTwo viewWithStringTag:@"tipLabel"];
        //frame
        iconImageView.frame = CGRectMake(CGRectGetMinX(self.changePwdAgainTextField.frame)-LCFloat(20) , LCFloat(4), cellHeight - 2 * LCFloat(4), cellHeight - 2 * LCFloat(4));
        tipLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame) + LCFloat(6), 0, kScreenBounds.size.width -(CGRectGetMaxX(iconImageView.frame) + LCFloat(6)), cellHeight);
        //赋值
        iconImageView.image = [UIImage imageNamed:@"login_tip"];
        if (indexPath.row == 0) {
            tipLabel.text = @"密码长度为6-16位字母和数字";
        }else {
            tipLabel.text = @"密码不能包含空格";
        }
        return cellForRowTwo;
    }else{      //确认按钮
        static NSString *cellForIdentifierRowThree = @"cellForIdentifierRowThree";
        UITableViewCell *cellForRowThree = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierRowThree];
        if (!cellForRowThree) {
            cellForRowThree = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierRowThree];
            cellForRowThree.backgroundColor = [UIColor clearColor];
            cellForRowThree.selectionStyle = UITableViewCellSelectionStyleNone;
            __weak typeof(self) weakSelf = self;
            self.commitButton = [[PDSureButton alloc]initWithTitle:@"提交" WithActionBlock:^{
                if (!weakSelf) {
                    return;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf commitButonClick];
            }];
            self.commitButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), 0, kScreenBounds.size.width - 2 * LCFloat(PDLeftRight_Space), cellHeight);
            self.commitButton.isEnable = NO;
            [cellForRowThree addSubview:self.commitButton];
        }
        return cellForRowThree;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return LCFloat(44);
    }else if (indexPath.section == 1){
        return [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:12]] + LCFloat(8);
    }else{
        return LCFloat(44);
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [footerView addGestureRecognizer:tap];
    return footerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 2) {
        return LCFloat(150);
    }else{
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [headerView addGestureRecognizer:tap];
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }else if (section == 1){
        return LCFloat(11);
    }else{
        return LCFloat(37);
    }
}
#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (toBeString.length > 16) {
        textField.text = [toBeString substringToIndex:16];
        return NO;
    }
    return YES;
}
- (void)textFieldChangeManager:(UITextField *)textField
{
    if (self.currentPwdTextField.text.length >=6 && self.changePwdTextField.text.length >=6 &&self.changePwdAgainTextField.text.length >=6) {
        self.commitButton.isEnable = YES;
    }else{
        self.commitButton.isEnable = NO;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.currentPwdTextField && textField.returnKeyType == UIReturnKeyNext) {
        [self.changePwdTextField becomeFirstResponder];
    }else if (textField == self.changePwdTextField && textField.returnKeyType == UIReturnKeyNext){
        [self.changePwdAgainTextField becomeFirstResponder];
    }else if (textField == self.changePwdAgainTextField && textField.returnKeyType == UIReturnKeyDone){
        [self.changePwdAgainTextField resignFirstResponder];
        [self commitButonClick];
    }
    return YES;
}
#pragma mark - tapManager
- (void)tapManager
{
    [self.view endEditing:YES];
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}
#pragma mark - buttonClick
- (void)commitButonClick
{
    if (![self checkTextFieldInput]) {
        return;
    }
    [self changePasswordManager];
}
#pragma mark - 接口
- (void)changePasswordManager
{
    PDFetchModel *changePasswordModel = [[PDFetchModel alloc]init];
    self.commitButton.isEnable = NO;
    changePasswordModel.requestParams = @{@"opassword":self.currentPwdTextField.text,@"npassword":self.changePwdTextField.text,@"rpassword":self.changePwdAgainTextField.text};
    __weak typeof(self) weakSelf = self;
    [changePasswordModel fetchWithPath:KChange_Password completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (isSucceeded) {
            strongSelf.commitButton.isEnable = YES;
            [PDTool userDefaulteWithKey:CURRENT_USER_PASS_KEY Obj:self.changePwdTextField.text];
            [strongSelf alertShow];
            
        }else{
            strongSelf.commitButton.isEnable = YES;
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];
}

- (void)alertShow
{
    PDAlertShowView *alertShowView = [[PDAlertShowView alloc]init];
    __weak typeof(self) weakSelf = self;
    [alertShowView alertWithTitle:@"密码修改成功" headerImage:[UIImage imageNamed:@"icon_report_success"] titleDesc:@"请妥善保管您的密码" buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf popToLastViewController];
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alertShowView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

- (void)popToLastViewController
{
    [self.alertView dismissWithCompletion:NULL];
    [self.navigationController popViewControllerAnimated:YES];
}
#pragma mark - 键盘通知
- (void)createkeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [value CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    CGFloat keyboard_Y = keyboardRect.origin.y;
    CGRect newTableViewFrame = self.view.bounds;
    newTableViewFrame.size.height = keyboard_Y - self.view.bounds.origin.y;
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.changeTableView.frame = newTableViewFrame;
    [UIView commitAnimations];
    
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *animationDurationVale = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationVale getValue:&animationDuration];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.changeTableView.frame = self.view.bounds;
    [UIView commitAnimations];
}

- (void)dealloc
{
    NSLog(@"...");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.currentPwdTextField becomeFirstResponder];
}
#pragma mark - error
- (BOOL)checkTextFieldInput
{
    if ([PDTool isEmpty:self.changePwdTextField.text] || [PDTool isEmpty:self.changePwdAgainTextField.text]|| [PDTool isEmpty:self.currentPwdTextField.text]) {
        [PDHUD showShimmeringString:@"密码不能为空" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else if (![PDTool validatePassword:self.currentPwdTextField.text] || ![PDTool validatePassword:self.changePwdTextField.text] || ![PDTool validatePassword:self.changePwdAgainTextField.text]){
        [PDHUD showShimmeringString:@"密码只能为数字和字母" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else if (![self.changePwdAgainTextField.text isEqualToString:self.changePwdTextField.text]){
        [PDHUD showShimmeringString:@"两次新密码不相同" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else{
        return YES;
    }
}


@end
