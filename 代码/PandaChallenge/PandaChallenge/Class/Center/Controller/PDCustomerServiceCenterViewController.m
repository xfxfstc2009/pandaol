//
//  PDCustomerServiceCenterViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCustomerServiceCenterViewController.h"
#import "PDServiceCenterModel.h"
#import "PDChallengeDirectManager.h"
@interface PDCustomerServiceCenterViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSArray *tableImageArray;   // 照片
@property (nonatomic,strong)NSArray *tableTitleArray;        // 标题
@property (nonatomic,strong)NSMutableArray *tableInformationArray;   // 信息
@property (nonatomic,strong)UITableView *tableView ;

@end
@implementation PDCustomerServiceCenterViewController



- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {

        self.tableTitleArray = [NSArray arrayWithObjects:@"客服QQ",@"客服微信",@"客服邮箱",@"商务合作", nil];
        self.tableImageArray = [NSArray arrayWithObjects:@"icon_center_serviceqq",@"icon_center_servicewechat",@"icon_center_serviceemail",@"icon_center_servicecollaboration", nil];
        self.tableInformationArray = [NSMutableArray array];
        }
    return self;

}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.barMainTitle = @"客服中心";
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self createTableView];
    [self sendRequestToGetServiceCenter];
    [self pageSetting];
}


-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
    if (self == [self.navigationController.viewControllers firstObject]){
        [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_home_slider"] barHltImage:nil action:^{
            if (!weakSelf){
                return ;
            }
            [PDChallengeDirectManager directToHomeViewController];
        }];
    } else {
        [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"basc_nav_back"] barHltImage:nil action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf.popBlock){
                strongSelf.popBlock();
            }
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }];
    }
}


- (void)createTableView
{
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.scrollEnabled = NO;
    self.tableView.rowHeight = 100;
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:self.tableView];
    
}

- (NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableInformationArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *str = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:str];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:str];
        PDImageView *tableImage = [[PDImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-20, 10, 40, 40)];
        [tableImage makeRoundedRectangleShape];
        [cell addSubview:tableImage];
        
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(tableImage.frame)+10, self.view.frame.size.width, 20)];
        titleLabel.font = [UIFont systemFontOfCustomeSize:15];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:titleLabel];
        
        UILabel *informationLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(titleLabel.frame), self.view.frame.size.width, 20)];
        informationLabel.font = [UIFont systemFontOfCustomeSize:13];
        informationLabel.textAlignment = NSTextAlignmentCenter;
        [cell addSubview:informationLabel];
        
        tableImage.image = [UIImage imageNamed:self.tableImageArray[indexPath.row]];
        titleLabel.text = self.tableTitleArray[indexPath.row];
        NSDictionary *dic = self.tableInformationArray[indexPath.row];
        informationLabel.text = dic[@"email"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    return cell;
}

#pragma mark - 客服中心数据请求
- (void)sendRequestToGetServiceCenter
{
    PDServiceCenterModel *fetchModel = [[PDServiceCenterModel alloc] init];
    [fetchModel fetchWithPath:KCenterCustomerService completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (isSucceeded){
            [self.tableInformationArray removeAllObjects];
            [self.tableInformationArray addObjectsFromArray:fetchModel.info];
            [self.tableView reloadData];
            
        } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
