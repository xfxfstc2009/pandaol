//
//  PDQrCodeViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDQrCodeViewController.h"
#import <AVFoundation/AVFoundation.h>
#import "PDExchangeViewController.h"
@interface PDQrCodeViewController ()<AVCaptureMetadataOutputObjectsDelegate,UIAlertViewDelegate>
@property (nonatomic, strong) AVCaptureSession *session;

@property (nonatomic, strong) AVCaptureVideoPreviewLayer *previewLayer;
@end

@implementation PDQrCodeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view setBackgroundColor:[UIColor whiteColor]];
    self.barMainTitle = @"网吧活动";
    
    [self createDevice];
    [self setupScanWindowView];
    [self setOverView];
    
    
    
}

- (void)createDevice
{
    // 1. 实例化拍摄设备
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    
    // 2. 设置输入设备
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:device error:nil];
    
    if (!input) return;
    
    
    // 3. 设置元数据输出
    // 3.1 实例化拍摄元数据输出
    
    AVCaptureMetadataOutput *output = [[AVCaptureMetadataOutput alloc] init];
    
    
    // x.y颠倒  最大(0, 0, 1, 1);
    [output setRectOfInterest:CGRectMake(60/self.view.frame.size.height,15/self.view.frame.size.width, (self.view.size_width - 15 * 2)/(self.view.frame.size.height), (self.view.size_width - 15 * 2)/(self.view.frame.size.width))]; //貌似 中间的感觉！！！
    // 3.3 设置输出数据代理
    [output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    
    // 4. 添加拍摄会话
    // 4.1 实例化拍摄会话
    
    self.session = [[AVCaptureSession alloc] init];
    // 高质量采集率
    [self.session setSessionPreset:AVCaptureSessionPresetHigh];
    // 4.2 添加会话输入
    
    [self.session addInput:input];
    
    // 4.3 添加会话输出
    
    [self.session addOutput:output];
    
    // 4.3 设置输出数据类型，需要将元数据输出添加到会话后，才能指定元数据类型，否则会报错
    
    [output setMetadataObjectTypes:@[AVMetadataObjectTypeQRCode,AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeCode128Code]];
    
    //    self.session = self.session;
    
    // 5. 视频预览图层
    // 5.1 实例化预览图层, 传递_session是为了告诉图层将来显示什么内容
    
    AVCaptureVideoPreviewLayer *preview = [AVCaptureVideoPreviewLayer layerWithSession:_session];
    
    preview.videoGravity = AVLayerVideoGravityResizeAspectFill;
    
    preview.frame = self.view.bounds;
    // 5.2 将图层插入当前视图
    [self.view.layer insertSublayer:preview atIndex:100];
    
    self.previewLayer = preview;
    
    // 6. 启动会话
    [_session startRunning];

}


- (void)setOverView {
    CGFloat width = CGRectGetWidth(self.view.frame);
    CGFloat height = CGRectGetHeight(self.view.frame);
    
    CGFloat x = 15;
    CGFloat y = 60;
    CGFloat w = self.view.size_width - 15 * 2;
    CGFloat h = self.view.size_width - 15 * 2;
    [self creatView:CGRectMake(0, 0, width, y)];
    [self creatView:CGRectMake(0, y, x, h)];
    [self creatView:CGRectMake(0, y + h, width, height - y - h)];
    [self creatView:CGRectMake(x + w, y, width - x - w, h)];
    
    UILabel *promptLabel = [[UILabel alloc] initWithFrame:CGRectMake(0,h+10+60, self.view.frame.size.width, 20)];
    promptLabel.text = @"将二维码放入框内,即可自动扫描";
    promptLabel.font = [UIFont systemFontOfCustomeSize:12];
    promptLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    promptLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:promptLabel];

}

- (void)creatView:(CGRect)rect {
//    CGFloat alpha = 0.5;
    UIColor *backColor = [UIColor blackColor];
    UIView *view = [[UIView alloc] initWithFrame:rect];
    view.backgroundColor = backColor;
//    view.alpha = alpha;
    [self.view addSubview:view];
}


- (void)setupScanWindowView
{
//    CGFloat scanWindowH = self.view.size_height * 0.9 - 60 * 2;
    CGFloat scanWindowW = self.view.size_width - 15 * 2;
    UIView *scanWindow = [[UIView alloc] initWithFrame:CGRectMake(15, 60, scanWindowW, scanWindowW)];
    scanWindow.clipsToBounds = YES;
    [self.view addSubview:scanWindow];
    
    CGFloat scanNetImageViewH = 241;
    CGFloat scanNetImageViewW = scanWindow.size_width;
    UIImageView *scanNetImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"scan_net"]];
    scanNetImageView.frame = CGRectMake(0, -scanNetImageViewH, scanNetImageViewW, scanNetImageViewH);
    CABasicAnimation *scanNetAnimation = [CABasicAnimation animation];
    scanNetAnimation.keyPath = @"transform.translation.y";
    scanNetAnimation.byValue = @(scanWindowW);
    scanNetAnimation.duration = 2.0;
    scanNetAnimation.repeatCount = MAXFLOAT;
    [scanNetImageView.layer addAnimation:scanNetAnimation forKey:nil];
    [scanWindow addSubview:scanNetImageView];
    
    CGFloat buttonWH = 18;
    
    
    UIButton *topLeft = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, buttonWH, buttonWH)];
    [topLeft setImage:[UIImage imageNamed:@"scan_1"] forState:UIControlStateNormal];
    [scanWindow addSubview:topLeft];
    
    UIButton *topRight = [[UIButton alloc] initWithFrame:CGRectMake(scanWindowW - buttonWH, 0, buttonWH, buttonWH)];
    [topRight setImage:[UIImage imageNamed:@"scan_2"] forState:UIControlStateNormal];
    [scanWindow addSubview:topRight];
    
    UIButton *bottomLeft = [[UIButton alloc] initWithFrame:CGRectMake(0, scanWindowW - buttonWH, buttonWH, buttonWH)];
    [bottomLeft setImage:[UIImage imageNamed:@"scan_3"] forState:UIControlStateNormal];
    [scanWindow addSubview:bottomLeft];
    
    UIButton *bottomRight = [[UIButton alloc] initWithFrame:CGRectMake(topRight.orgin_x, bottomLeft.orgin_y, buttonWH, buttonWH)];
    [bottomRight setImage:[UIImage imageNamed:@"scan_4"] forState:UIControlStateNormal];
    [scanWindow addSubview:bottomRight];
    
    
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    
    // 会频繁的扫描，调用代理方法
    // 1. 如果扫描完成，停止会话
    [self.session stopRunning];
    
//    // 2. 删除预览图层
//    [self.previewLayer removeFromSuperlayer];
    
    NSLog(@"%@", metadataObjects);
    
    // 3. 设置界面显示扫描结果
    
    if (metadataObjects.count > 0)
        
    {
        
        AVMetadataMachineReadableCodeObject *obj = metadataObjects[0];
        // 提示：如果需要对url或者名片等信息进行扫描，可以在此进行扩展！
        NSLog(@"%@", obj.stringValue);
        
        
        NSArray *strArray =[obj.stringValue componentsSeparatedByString:@"?"];
        if (strArray.count && strArray.count == 2){
            NSString *lastString = [strArray lastObject];
            NSString *netId = [lastString substringWithRange:NSMakeRange(3, 6)];
            NSLog(@"%@",netId);
           NSString *newNetId = [NSString stringWithFormat:@"%@%@",netId,@"D#@$dhf342#$"];
            newNetId = [PDTool md5:newNetId];
            
            if ([lastString rangeOfString:newNetId].location != NSNotFound) {
                PDExchangeViewController *exchangeViewController =  [[PDExchangeViewController alloc] init];
                            exchangeViewController.netId = netId.integerValue;
                            [self.navigationController pushViewController:exchangeViewController animated:YES];
            }else
            {
                UIAlertView *alertView= [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"此二维码无法识别,请确立来源或联系客服" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                [alertView show];

            }
            


        }else
        {
            UIAlertView *alertView= [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"此二维码无法识别,请确立来源或联系客服" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
                    [alertView show];
            
            
            
        }
        
        
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    
    
    
    [self createDevice];
    [self setupScanWindowView];
    [self setOverView];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
