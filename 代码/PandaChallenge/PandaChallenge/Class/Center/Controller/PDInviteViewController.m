//
//  PDInviteViewController.m
//  PandaChallenge
//
//  Created by panda on 16/4/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInviteViewController.h"
#import "PDSureButton.h"
#import "PDActionSheetViewController.h"
#import "PDLoginModel.h"
#define PDLeftRight_Space 53
@implementation PDInviteViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
}
#pragma mark - pageSetting
- (void)pageSetting
{
    self.barMainTitle = @"邀请好友";
}
#pragma mark - createView
- (void)createView
{
    //图标
    UIImageView *envelopeImageView = [[UIImageView alloc]init];
    envelopeImageView.image = [UIImage imageNamed:@"icon_share_envelope"];
    envelopeImageView.frame = CGRectMake((kScreenWidth - LCFloat(100))/2., LCFloat(30), LCFloat(100), LCFloat(75));
    [self.view addSubview:envelopeImageView];
    //标题
    UILabel *titleLabel = [self labelWithFont:[UIFont systemFontOfSize:13] title:@"分享您的福利码"];
    titleLabel.frame = CGRectMake(0, CGRectGetMaxY(envelopeImageView.frame), kScreenWidth, LCFloat(44));
    
    //福利码
    UILabel *welfareLabel = [self labelWithFont:[UIFont systemFontOfCustomeSize:22] title:[PDLoginModel shareInstance].invitationCode];
    welfareLabel.backgroundColor = [UIColor colorWithRed:229/255. green:229/255. blue:229/255. alpha:1];
    welfareLabel.textColor = [UIColor colorWithRed:40/255. green:40/255. blue:40/255. alpha:1];
    welfareLabel.frame = CGRectMake(LCFloat(PDLeftRight_Space) + LCFloat(25), CGRectGetMaxY(titleLabel.frame), kScreenWidth - 2 * LCFloat(PDLeftRight_Space) - LCFloat(50), LCFloat(43));
    welfareLabel.layer.cornerRadius = 5.;
    welfareLabel.layer.masksToBounds = YES;
    
    //描述
    UILabel *desLabel = [self labelWithFont:[UIFont systemFontOfSize:12] title:@"当有好友使用您的福利码注册成功并参与赛事后您将可以获得一次赛事资格"];
    desLabel.textColor = [UIColor grayColor];
    desLabel.numberOfLines = 2;
    CGFloat heightForLine = [NSString contentofHeightWithFont:[UIFont systemFontOfSize:12]];
    desLabel.frame = CGRectMake(LCFloat(PDLeftRight_Space), LCFloat(10) + CGRectGetMaxY(welfareLabel.frame), kScreenWidth - 2 * LCFloat(PDLeftRight_Space), 2 * heightForLine);
    
    //邀请按钮
    __weak typeof(self) weakSelf = self;
    PDSureButton *inviteButton = [[PDSureButton alloc]initWithTitle:@"立即邀请" WithActionBlock:^{
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        PDActionSheetViewController *actionViewController = [[PDActionSheetViewController alloc]init];
        actionViewController.isHasGesture = YES;
        [actionViewController showInView:strongSelf];
    }];
    inviteButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), LCFloat(44) + CGRectGetMaxY(desLabel.frame), kScreenWidth - 2 * (LCFloat(PDLeftRight_Space)), LCFloat(44));
    [self.view addSubview:inviteButton];
}

#pragma mark - Label的创建
- (UILabel *)labelWithFont:(UIFont *)font title:(NSString *)title
{
    UILabel *label = [[UILabel alloc]init];
    label.textAlignment = NSTextAlignmentCenter;
    label.font = font;
    label.text = title;
    [self.view addSubview:label];
    return label;
}
@end
