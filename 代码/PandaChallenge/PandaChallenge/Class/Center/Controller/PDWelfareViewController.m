//
//  PDWelfareViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/4/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDWelfareViewController.h"
#import "PDChallengeViewController.h"
#import "PDNavigationController.h"
@interface PDWelfareViewController ()<UITextFieldDelegate>
@property (nonatomic,strong)UITextField *welfareTextfield;
@property (nonatomic,strong)UIButton *submitBtn;
@end

@implementation PDWelfareViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self pageSetting];
    self.barMainTitle = @"福利";
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    [self createView];
}
- (void)pageSetting
{
    __weak typeof(self) weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"login_back"] barHltImage:nil action:^{
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if ([strongSelf.welfareTextfield isFirstResponder])
            [strongSelf.welfareTextfield resignFirstResponder];
        [strongSelf directToHomeViewController];
    }];
    
    if (self.isFirstBind) {
        // 隐藏返回按钮
        [self hidesBackButton];
        // 设置导航栏右侧按钮
        __weak typeof(self) weakSelf = self;
        [self rightBarButtonWithTitle:@"跳过" barNorImage:nil barHltImage:nil action:^{
            if (!weakSelf) return;
            __strong typeof(weakSelf) strongSelf = weakSelf;
            if ([strongSelf.welfareTextfield isFirstResponder])
                [strongSelf.welfareTextfield resignFirstResponder];
            PDChallengeViewController *challengeViewController = [[PDChallengeViewController alloc]init];
            [strongSelf.navigationController pushViewController:challengeViewController animated:YES];
        }];
    }
}


- (void)createView
{
    PDImageView *welfare = [[PDImageView alloc] init];
    welfare.size_height = 20;
    welfare.size_width = 20;
    welfare.image = [UIImage imageNamed:@"icon_center_welfare"];
    
    self.welfareTextfield = [[UITextField alloc] initWithFrame:CGRectMake(30, 20, self.view.frame.size.width-60, 30)];
    self.welfareTextfield.delegate = self;
    self.welfareTextfield.placeholder = @"请输入推荐人福利码";
    self.welfareTextfield.leftView = welfare;
    self.welfareTextfield.textAlignment = NSTextAlignmentCenter;
    self.welfareTextfield.leftViewMode = UITextFieldViewModeAlways;
    self.welfareTextfield.keyboardType = UIKeyboardTypeDefault;
    self.welfareTextfield.returnKeyType = UIReturnKeyDone;
    [self.welfareTextfield setValue:[UIFont boldSystemFontOfSize:15] forKeyPath:@"_placeholderLabel.font"];
    [self.welfareTextfield setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.color"];
    [self.welfareTextfield addTarget:self action:@selector(welfareTextfieldBegainAction:) forControlEvents:(UIControlEventEditingChanged)];
    self.welfareTextfield.font = [UIFont systemFontOfCustomeSize:15];
    [self.view addSubview:self.welfareTextfield];
    
    
    
    // 线
    UIView *xian = [[UIView alloc] initWithFrame:CGRectMake(50, CGRectGetMaxY(self.welfareTextfield.frame), self.view.frame.size.width-80, 1)];
    xian.backgroundColor = [UIColor groupTableViewBackgroundColor];
    [self.view addSubview:xian];
    
    
    
    self.submitBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, CGRectGetMaxY(xian.frame)+30, self.view.frame.size.width-60, 40)];
    [self.submitBtn setTitle:@"提交" forState:(UIControlStateNormal)];
    [self.submitBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.submitBtn.backgroundColor = [UIColor grayColor];
    self.submitBtn.layer.cornerRadius = 5;
    [self.submitBtn addTarget:self action:@selector(submitBtnAction) forControlEvents:(UIControlEventTouchUpInside)];
    self.submitBtn.enabled = NO;
    self.submitBtn.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    [self.view addSubview:self.submitBtn];
    
    
    // 提示
    UILabel *promptLabel = [[UILabel alloc] init];
    promptLabel.text = @"填写福利码，您将获得一次初级赛事资格";
    promptLabel.textAlignment = NSTextAlignmentCenter;
    promptLabel.font = [UIFont systemFontOfCustomeSize:13];
    promptLabel.textColor = [UIColor grayColor];
    promptLabel.frame =  CGRectMake(0, CGRectGetMaxY(self.submitBtn.frame)+LCFloat(5), self.view.frame.size.width, [NSString  contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13]]);
    [self.view addSubview:promptLabel];
    
}




#pragma mark - 监听文本的改变
- (void)welfareTextfieldBegainAction:(UITextField *)change
{
    if (self.welfareTextfield.text.length > 0) {
        self.submitBtn.backgroundColor = [UIColor blackColor];
        self.submitBtn.enabled = YES;
    }else
    {
        self.submitBtn.backgroundColor = [UIColor grayColor];
        self.submitBtn.enabled = NO;
        self.welfareTextfield.placeholder = @"请输入推荐人福利码";
        [self.welfareTextfield setValue:[UIColor grayColor] forKeyPath:@"_placeholderLabel.color"];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.welfareTextfield && textField.returnKeyType == UIReturnKeyDone) {
        [textField resignFirstResponder];
        [self submitBtnAction];
    }
    return YES;
}

#pragma mark - 提交
- (void)submitBtnAction
{
//    self.welfareTextfield.text = @"";
//    self.welfareTextfield.placeholder = @"你输入的福利码不正确,请重新输入";
//    [self.welfareTextfield setValue:[UIColor redColor] forKeyPath:@"_placeholderLabel.color"];
    
    [self.welfareTextfield resignFirstResponder];
    PDFetchModel *fetchModel = [[PDFetchModel alloc] init];
    fetchModel.requestParams = @{@"invitationCode":self.welfareTextfield.text};
    __weak typeof(self) weakSelf = self;
    [fetchModel fetchWithPath:KCenterWelfare completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf) return;
        if (isSucceeded){
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf.welfareTextfield resignFirstResponder];
            PDAlertShowView *showView = [[PDAlertShowView alloc] init];
            [showView alertForAlertViewType:PDAlertShowViewTypeSuccess withTitle:@"领取成功" desc:@"成功获得一次初级级别联赛资格"];
            JCAlertView *alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:YES];
            [alertView show];

            if (strongSelf.isFirstBind) {
                PDChallengeViewController *challengeViewController = [[PDChallengeViewController alloc]init];
                [strongSelf.navigationController pushViewController:challengeViewController animated:YES];
            } else {
                 PDChallengeViewController *challengeViewController = [[PDChallengeViewController alloc]init];
                PDNavigationController *navigationController = [[PDNavigationController alloc]initWithRootViewController:challengeViewController];
                [[RESideMenu shareInstance] setContentViewController:navigationController animated:YES];
            }
        } else {
           [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [self.view endEditing:YES];
}

- (void)directToHomeViewController{
    PDChallengeViewController *inviteViewController = [[PDChallengeViewController alloc]init];
    [[RESideMenu shareInstance] setContentViewController:[[UINavigationController alloc]initWithRootViewController:inviteViewController] animated:YES];
    [[RESideMenu shareInstance] hideMenuViewController];
}


#pragma mark - 以后再说跳转其他页面
- (void)laterBtnAction:(UIButton *)sender
{
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.welfareTextfield becomeFirstResponder];
}

//- (void)viewDidDisappear:(BOOL)animated
//{
//    [super viewDidDisappear:animated];
//    if ([self.welfareTextfield isFirstResponder]) {
//        [self.welfareTextfield resignFirstResponder];
//    }
//}
@end
