//
//  PDPortraitDefaultViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPortraitDefaultViewController.h"
#import "PDPortraitDefaultModel.h"
#import "PDPortraitDefaultCollectionViewCell.h"
@interface PDPortraitDefaultViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic,strong)NSMutableArray *portraitListMutableArr;  // 接数组
@property (nonatomic,strong)UICollectionView *portraitCollectionView;
@property (nonatomic,strong)NSMutableArray *portaritArray;  //接图片
@property (nonatomic,strong)NSString *portraitStr;

@end

@implementation PDPortraitDefaultViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.portraitListMutableArr = [NSMutableArray array];
        self.portaritArray = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    
    [self createCollectionView];
    
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetportraitList];
}


-(void)pageSetting{

    self.barMainTitle = @"更换头像";
    
    
    
    UIButton *rightBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    [rightBtn setTitle:@"确定" forState:(UIControlStateNormal)];
    rightBtn.titleLabel.font = [UIFont systemFontOfCustomeSize:17];
    [rightBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    [rightBtn addTarget:self action:@selector(rightBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    self.navigationItem.rightBarButtonItem = rightItem;
}


- (void)createCollectionView
{
    // 创建collectionview
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc]init];
    [flowLayout setItemSize:CGSizeMake(self.view.frame.size.width/4-2, self.view.frame.size.width/4)];//设置cell的尺寸
    //    [flowLayout setScrollDirection:    UICollectionViewScrollDirectionHorizontal
    //];//设置其布局方向
    
    // 1.2 设置每列最小间距
    flowLayout.minimumInteritemSpacing = 1;
    // 1.3 设置每行最小间距
    flowLayout.minimumLineSpacing = 2;
    flowLayout.sectionInset = UIEdgeInsetsMake(0, 0, 0, 0);//设置其边界
    
    
    self.portraitCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-64) collectionViewLayout:flowLayout];
    
    self.portraitCollectionView.backgroundColor = [UIColor whiteColor];
    self.portraitCollectionView.dataSource = self;
    self.portraitCollectionView.delegate = self;
    [self.view addSubview:self.portraitCollectionView];
    [self.portraitCollectionView registerClass:[PDPortraitDefaultCollectionViewCell class] forCellWithReuseIdentifier:@"reuse"];
    
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.portaritArray.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    PDPortraitDefaultCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"reuse" forIndexPath:indexPath];

    [cell.portraitImage uploadImageWithURL:self.portaritArray[indexPath.item] placeholder:nil callback:^(UIImage *image) {
        
    }];

    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    self.portraitStr = self.portraitListMutableArr[indexPath.item][@"url"];
    NSLog(@"1221221%@",self.portraitStr);
    
   
}



#pragma mark - 确定
- (void)rightBtnAction:(UIButton *)sender
{
    
    if (self.portraitStr.length == 0) {
        [PDHUD showShimmeringString:@"请选择头像" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return;
    }
    

    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    fetchModel.requestParams = @{@"avatar":self.portraitStr};
    [fetchModel fetchWithPath:KCenterEditUserInfo completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (isSucceeded){
            
            if ([self.delegate respondsToSelector:@selector(sendPortraitStr:)]) {
                [self.delegate sendPortraitStr:self.portraitStr];
            }
            
            NSString *porStr = [self.portraitStr stringByReplacingOccurrencesOfString:@"\\" withString:@""];
            [PDLoginModel shareInstance].avatar = porStr;
            [self.navigationController popViewControllerAnimated:YES];

            
        } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];

}


#pragma mark - 头像列表数据请求
-(void)sendRequestToGetportraitList{
    PDPortraitDefaultModel *fetchModel = [[PDPortraitDefaultModel alloc]init];
    [fetchModel fetchWithPath:kCenterHeaderlist completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (isSucceeded){
            self.portraitListMutableArr = [NSMutableArray arrayWithArray:fetchModel.avatarList];
            for (NSDictionary *dic in self.portraitListMutableArr) {
                
                NSString *portraitStr = dic[@"url"];
                
                
                [self.portaritArray addObject:portraitStr];
            }
            
            [self.portraitCollectionView reloadData];
        } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];
}
@end
