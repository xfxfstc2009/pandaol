//
//  PDLeagueRulesViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/4/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLeagueRulesViewController.h"

@interface PDLeagueRulesViewController ()


@property (nonatomic,strong)UIButton *confimBtn;
@end

@implementation PDLeagueRulesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.navigationController.navigationBarHidden = NO;
}
- (void)pageSetting
{
  
    self.navigationController.navigationBar.tintColor=[UIColor blackColor];
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithTitle:@"" style:(UIBarButtonItemStylePlain) target:self action:nil];
    self.navigationItem.leftBarButtonItem= backButton;
}

- (void)createView
{
    self.webView.frame = CGRectMake(0, LCFloat(44), self.view.frame.size.width, self.view.frame.size.height- LCFloat(144));

    self.urlStr = @"http://m.pandaol.net/panda/cafe/rules.html";
    if (self.isFirstChallage ==YES) {
        
    
   self.confimBtn = [[UIButton alloc] initWithFrame:CGRectMake(LCFloat(30), self.view.frame.size.height-LCFloat(100), self.view.frame.size.width-2 * LCFloat(30), LCFloat(40))];
    [self.confimBtn setTitle:@"确定(15s)" forState:(UIControlStateNormal)];
    [self.confimBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    self.confimBtn.backgroundColor = [UIColor grayColor];
    [self.confimBtn addTarget:self action:@selector(confirmBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    self.confimBtn.enabled = NO;
    self.confimBtn.layer.cornerRadius = 5;
    [self.view addSubview:self.confimBtn];
    
    
    
    __block int timeout=15; //倒计时时间
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    dispatch_source_t _timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0,queue);
    dispatch_source_set_timer(_timer,dispatch_walltime(NULL, 0),1.0*NSEC_PER_SEC, 0); //每秒执行
    dispatch_source_set_event_handler(_timer, ^{
        if(timeout<=0){ //倒计时结束，关闭
            dispatch_source_cancel(_timer);
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                [self.confimBtn setTitle:@"确定" forState:UIControlStateNormal];
                [self.confimBtn setBackgroundColor:[UIColor blackColor]];
                self.confimBtn.enabled = YES;
            });
        }else{
            //            int minutes = timeout / 60;
            int seconds = timeout % 61;
            NSString *strTime = [NSString stringWithFormat:@"%.2d", seconds];
            dispatch_async(dispatch_get_main_queue(), ^{
                //设置界面的按钮显示 根据自己需求设置
                //                NSLog(@"____%@",strTime);
                [self.confimBtn setTitle:[NSString stringWithFormat:@"确定(%@s)",strTime] forState:UIControlStateNormal];
                self.confimBtn.enabled = NO;
                
            });
            timeout--;
        }
    });
    dispatch_resume(_timer);
    
        
    } else {
        
        self.confimBtn = [[UIButton alloc] initWithFrame:CGRectMake(LCFloat(30), self.view.frame.size.height-LCFloat(100), self.view.frame.size.width-2 * LCFloat(30), LCFloat(40))];
        [self.confimBtn setTitle:@"确定(15s)" forState:(UIControlStateNormal)];
        [self.confimBtn setTitle:@"确定" forState:(UIControlStateNormal)];
        [self.confimBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        self.confimBtn.backgroundColor = [UIColor blackColor];
        [self.confimBtn addTarget:self action:@selector(confirmBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        self.confimBtn.layer.cornerRadius = 5;
        [self.view addSubview:self.confimBtn];

        
    }
    
    
    
    // 提示
    UILabel *promptLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.confimBtn.frame)+LCFloat(11), self.view.frame.size.width, LCFloat(20))];
    promptLabel.numberOfLines = 0;
    promptLabel.textAlignment = NSTextAlignmentCenter;
    promptLabel.font = [UIFont systemFontOfCustomeSize:12];
    promptLabel.text = @"点击确定即视作同意该规则,最终解释权归平台所有";
    [self.view addSubview:promptLabel];

    
}


- (void)confirmBtnAction:(UIButton *)sender
{
    NSLog(@"确定");
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
