//
//  PDHelpInformationViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/4/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDHelpInformationViewController.h"
#import "PDCustomerServiceCenterViewController.h"
#import "PDSuggestionViewController.h"
#import "PDWebViewController.h"
#import "MobClick.h"
#import "PDChallengeDirectManager.h"
@interface PDHelpInformationViewController ()
@end

@implementation PDHelpInformationViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    self.webView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-89-64);
    [self createView];
    [self loadUrlStr];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"帮助信息";
    
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"basc_nav_back"] barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.webView.canGoBack) {
            [strongSelf.webView goBack];
        }else{
            [PDChallengeDirectManager directToHomeViewController];
        }
    }];
}

- (void)loadUrlStr {
    self.urlStr = @"http://m.pandaol.net/panda/system/help.html";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [MobClick beginLogPageView:@"TwoPage"];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"TwoPage"];
}


- (void)createView
{
    // 联系客服
    UIButton *customerBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 89-64, self.view.frame.size.width, 44)];
    [customerBtn setTitle:@"联系客服" forState:(UIControlStateNormal)];
    [customerBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    customerBtn.backgroundColor = [UIColor whiteColor];
    customerBtn.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    [customerBtn addTarget:self action:@selector(customerBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:customerBtn];
    // 意见反馈
    
    UIButton *feedBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height - 44-64, self.view.frame.size.width, 44)];
    [feedBtn setTitle:@"意见反馈" forState:(UIControlStateNormal)];
    [feedBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
    feedBtn.backgroundColor = [UIColor whiteColor];
    feedBtn.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    [feedBtn addTarget:self action:@selector(feedBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:feedBtn];
}



#pragma mark - 意见反馈
- (void)feedBtnAction:(UIButton *)sender
{
    
    NSDictionary *dict = @{@"type" : @"book", @"quantity" : @"3"};
    [MobClick event:@"challenger" attributes:dict];
    PDSuggestionViewController *suggestionController = [[PDSuggestionViewController alloc] init];
    [self.navigationController pushViewController:suggestionController animated:YES];
}


#pragma mark - 联系客服
- (void)customerBtnAction:(UIButton *)sender
{
    PDCustomerServiceCenterViewController *customerServixeCenter = [[PDCustomerServiceCenterViewController alloc] init];
    [self.navigationController pushViewController:customerServixeCenter animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
