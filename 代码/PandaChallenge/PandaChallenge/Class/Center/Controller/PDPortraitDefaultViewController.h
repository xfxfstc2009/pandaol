//
//  PDPortraitDefaultViewController.h
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@protocol sendPortraitStrDelegate <NSObject>

- (void)sendPortraitStr:(NSString *)portraitStr;

@end
@interface PDPortraitDefaultViewController : AbstractViewController

@property (nonatomic,assign)id<sendPortraitStrDelegate>delegate;
@end
