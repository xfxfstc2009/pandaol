//
//  PDPersonalCenterViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPersonalCenterViewController.h"
#import "PDCertificationViewController.h"
#import "PDPortraitDefaultViewController.h"
#import "PDUserInfoModel.h"
@interface PDPersonalCenterViewController ()<UITableViewDataSource,UITableViewDelegate,UIAlertViewDelegate,UITextFieldDelegate,sendPortraitStrDelegate>

@property (nonatomic,strong)NSArray *tableArray;
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)UILabel *userNameLabel;
@property (nonatomic,strong)NSArray *imageArray;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *QQLabel;
@property (nonatomic,strong)UILabel *sexLabel;
@property (nonatomic,strong)UIButton *manBtn;
@property (nonatomic,strong)UIButton *womanBtn;
@property (nonatomic,strong)NSString *sexTemp;                  /**> 接收性别值 <**/
@property (nonatomic,strong) PDImageView *portraitImageView;    /**> 头像 <**/


@property (nonatomic,strong)UIView *popupView;                  /**> 弹出view填资料 <**/
@property (nonatomic,strong)UILabel *promptLabel;               /**> 红色提示不能为空 <**/
@property (nonatomic,strong)UITextField *nickTextField;



@property (nonatomic,strong)UILabel *lifeTimeIDLabel;           /**> 终生id <**/
@property (nonatomic,strong)PDImageView *sexImage;              /**> 性别图片 <**/
@property (nonatomic,strong)UIView *portraitBottomView;         /**> 最下面的view <**/
@end

@implementation PDPersonalCenterViewController

-(instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.tableArray = [NSArray arrayWithObjects:@"昵称",@"性别",@"QQ",@"实名认证", nil];
        self.imageArray = [NSArray arrayWithObjects:@"icon_center_nick",@"icon_center_sex",@"icon_center_qq",@"icon_center_certification", nil];
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.barMainTitle = @"个人信息";
    [self createView];
    [self sendRequestToGetUserInfo];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
   
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
//    self.navigationController.navigationBarHidden = NO;

}

- (void)createView
{
 
    
    self.portraitBottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.size_width, LCFloat(200))];
    self.portraitBottomView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.view addSubview:self.portraitBottomView];
    
    
    
    // 头像
    self.portraitImageView = [[PDImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-LCFloat(50), LCFloat(10), LCFloat(100), LCFloat(100))];
    self.portraitImageView.image = [UIImage imageNamed:@"icon_center_avatar"];
    [self.portraitImageView makeRoundedRectangleShape];
    self.portraitImageView.userInteractionEnabled = YES;
    [self.portraitBottomView addSubview:self.portraitImageView];
    
    
    
    // 头像点击手势
    UITapGestureRecognizer *portraitTapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(portraitTapGesAction:)];
    [self.portraitImageView addGestureRecognizer:portraitTapGes];
    
    
    // 昵称
    self.userNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.portraitImageView.frame)+LCFloat(5), self.view.size_width, 20)];
    self.userNameLabel.font = [UIFont systemFontOfCustomeSize:17];
    self.userNameLabel.textAlignment = NSTextAlignmentCenter;
    self.userNameLabel.text = [PDLoginModel shareInstance].name;
    self.userNameLabel.textColor = [UIColor blackColor];
    [self.portraitBottomView addSubview: self.userNameLabel];
    
    
    
    
    self.sexImage = [[PDImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-10, CGRectGetMaxY(self.userNameLabel.frame)+LCFloat(5), 15, 15)];
    self.sexImage.image = [UIImage imageNamed:@"icon_center_man"];
    [self.portraitBottomView addSubview:self.sexImage];
    
    
    // 终生ID
    self.lifeTimeIDLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.sexImage.frame), self.view.size_width, 20)];
    self.lifeTimeIDLabel.font = [UIFont systemFontOfCustomeSize:11];
    self.lifeTimeIDLabel.textAlignment = NSTextAlignmentCenter;
    self.lifeTimeIDLabel.text = [NSString stringWithFormat:@"ID:%@",[PDLoginModel shareInstance].invitationCode];
    self.lifeTimeIDLabel.textColor = [UIColor blackColor];
    [self.portraitBottomView addSubview: self.lifeTimeIDLabel];

    
    
    
  
    
    
    
    
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.portraitBottomView.frame), self.view.frame.size.width, self.view.frame.size.height-CGRectGetMaxY(self.portraitBottomView.frame)) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableFooterView = [[UIView alloc] init];
    [self.view addSubview:self.tableView];

    
    
    
}


- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexpath {
    cell.separatorInset = UIEdgeInsetsMake(0, 10, 0, 0);
//    cell.layoutMargins = UIEdgeInsetsMake(0, 10, 0, 10);
//    cell.preservesSuperviewLayoutMargins = NO;

}

- (NSInteger )tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.tableArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    
    cell.backgroundColor = [UIColor whiteColor];
    cell.textLabel.text = self.tableArray[indexPath.row];
    cell.imageView.image = [UIImage imageNamed:self.imageArray[indexPath.row]];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.textLabel.font = [UIFont systemFontOfCustomeSize:15];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        PDUserInfoModel *fechModel = [[PDUserInfoModel alloc] init];
        
    if (indexPath.row == 0) {
        // 昵称
        self.nickNameLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-200, 0, 170, 44)];
        self.nickNameLabel.textColor = [UIColor grayColor];
        self.nickNameLabel.font = [UIFont systemFontOfCustomeSize:15];
        self.nickNameLabel.textAlignment = NSTextAlignmentRight;
        if ([fechModel.userinfo[@"username"] isKindOfClass:[NSNull class]]) {
            self.nickNameLabel.text = fechModel.userinfo[@"phone"];
        }else
        {
            self.nickNameLabel.text = fechModel.userinfo[@"username"];
        }
        
        [cell addSubview:self.nickNameLabel];
    }else if (indexPath.row ==1){
        // 性别
        self.sexLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-200, 0, 170, 44)];
        self.sexLabel.textColor = [UIColor grayColor];
        self.sexLabel.font = [UIFont systemFontOfCustomeSize:15];
        self.sexLabel.textAlignment = NSTextAlignmentRight;
        [cell addSubview:self.sexLabel];
    }else if (indexPath.row ==2){
        // QQ
        self.QQLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-200, 0, 170, 44)];
        self.QQLabel.textColor = [UIColor grayColor];
        self.QQLabel.font = [UIFont systemFontOfCustomeSize:15];
        self.QQLabel.textAlignment = NSTextAlignmentRight;
        [cell addSubview:self.QQLabel];
    }
        
    }
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
//        self.nickNameAlert =[[UIAlertView alloc] initWithTitle:@"昵称" message:nil delegate:self cancelButtonTitle:@"确定" otherButtonTitles:@"取消", nil];
//        self.nickNameAlert.delegate = self;
//        self.nickNameAlert.stringTag = @"nickNameAlert";
//        self.nickNameAlert.alertViewStyle = UIAlertViewStylePlainTextInput;
//        [self.nickNameAlert textFieldAtIndex:0].placeholder = @"请输入昵称";
//        [self.nickNameAlert show];
        
        
        
        // 昵称更改
        if (self.nickNameLabel.text.length == 0) {
            [self popupView:@"修改昵称" placeholder:@"3-8位字符,支持中英文、数字" prompt:@"昵称不能为空" tag:0];
        } else {
        [self popupView:@"修改昵称" placeholder:@"3-8位字符,支持中英文、数字" prompt:@"" tag:0];
        }
        
       
    }else if (indexPath.row == 1){
        // 性别更改
        
        self.popupView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
        self.popupView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        [self.view.window addSubview:self.popupView];
        
        
        // 底部view
        UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(20, self.view.frame.size.height/2-70, self.view.frame.size.width-40, 140)];
        backgroundView.layer.cornerRadius = 5;
        backgroundView.backgroundColor = [UIColor whiteColor];
        [self.popupView addSubview:backgroundView];
        
        
        
        // 标题
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, backgroundView.frame.size.width, 20)];
        titleLabel.text = @"更改性别";
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [backgroundView addSubview:titleLabel];
        
        
        
        
        self.manBtn = [[UIButton alloc] initWithFrame:CGRectMake(backgroundView.frame.size.width/2-50, CGRectGetMaxY(titleLabel.frame)+10, 30, 30)];
        [self.manBtn setTitle:@"男" forState:(UIControlStateNormal)];
        [self.manBtn setImage:[UIImage imageNamed:@"icon_center_unchecked"] forState:UIControlStateNormal];
        [self.manBtn setImage:[UIImage imageNamed:@"icon_center_checked"] forState:UIControlStateSelected];
        [self.manBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 15)];
        [self.manBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
        [self.manBtn addTarget:self action:@selector(manBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [backgroundView addSubview:self.manBtn];

        
        self.womanBtn = [[UIButton alloc] initWithFrame:CGRectMake(backgroundView.frame.size.width/2+20, CGRectGetMaxY(titleLabel.frame)+10, 30, 30)];
        [self.womanBtn setTitle:@"女" forState:(UIControlStateNormal)];
        [self.womanBtn setImage:[UIImage imageNamed:@"icon_center_unchecked"] forState:UIControlStateNormal];
        [self.womanBtn setImage:[UIImage imageNamed:@"icon_center_checked"] forState:UIControlStateSelected];
        [self.womanBtn setImageEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 15)];
        [self.womanBtn setTitleColor:[UIColor blackColor] forState:(UIControlStateNormal)];
        [self.womanBtn addTarget:self action:@selector(womanBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];

        [backgroundView addSubview:self.womanBtn];
        
    
        // 线
        UIView *xianView = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.womanBtn.frame), backgroundView.frame.size.width-20, 1)];
        xianView.backgroundColor = [UIColor groupTableViewBackgroundColor];
        [backgroundView addSubview:xianView];
        

        // 确定
        UIButton *confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(backgroundView.frame.size.width/2-100, CGRectGetMaxY(xianView.frame)+20, 80, 30)];
        [confirmButton setTitle:@"确定" forState:(UIControlStateNormal)];
        confirmButton.layer.cornerRadius = 5;
        confirmButton.layer.borderWidth = 1.0f;
        confirmButton.layer.borderColor = [UIColor grayColor].CGColor;
        [confirmButton setTitleColor:[UIColor grayColor] forState:(UIControlStateNormal)];
        [confirmButton addTarget:self action:@selector(sexConfirmButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [backgroundView addSubview:confirmButton];
        
        // 取消
        UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(backgroundView.frame.size.width/2+20, CGRectGetMaxY(xianView.frame)+20, 80, 30)];
        [cancelButton setTitle:@"取消" forState:(UIControlStateNormal)];
        cancelButton.layer.cornerRadius = 5;
        cancelButton.layer.borderWidth = 1.0f;
        cancelButton.layer.borderColor = [UIColor grayColor].CGColor;
        [cancelButton setTitleColor:[UIColor grayColor] forState:(UIControlStateNormal)];
        [cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
        [backgroundView addSubview:cancelButton];
        
        
        if ([self.sexLabel.text isEqualToString:@"女"]) {
            self.womanBtn.selected = YES;
            self.sexTemp = @"女";
        }else
        {
            self.manBtn.selected = YES;
            self.sexTemp = @"男";
        }
        
    }
    else if (indexPath.row == 2) {
        // qq账号
        if (self.QQLabel.text.length == 0) {
            [self popupView:@"QQ账号" placeholder:@"请输入您的QQ账号" prompt:@"账号不能为空" tag:2];
        } else {
            [self popupView:@"QQ账号" placeholder:@"请输入您的QQ账号" prompt:@"" tag:2];
        }
        
        
        
        
        
    }else if (indexPath.row == 3){
        
        
        // 认证
        PDCertificationViewController *certification =[[PDCertificationViewController alloc] init];
        if([PDLoginModel shareInstance].cn_idcard.length == 18 ){
            certification.isChecked = YES;
        }else
        {
            certification.isChecked = NO;
        }
        
        [self.navigationController pushViewController:certification animated:YES];
    }



    [tableView deselectRowAtIndexPath:[tableView indexPathForSelectedRow] animated:YES];//取消选中项
    
    
}


- (UIView *)popupView:(NSString *)title placeholder:(NSString *)placeholder prompt:(NSString *)prompt tag:(NSInteger)tag
{
    self.popupView = [[UIView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.popupView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
    [self.view.window addSubview:self.popupView];
    
    
    // 底部view
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(20, self.view.frame.size.height -271-160, self.view.frame.size.width-40, 140)];
    backgroundView.layer.cornerRadius = 5;
    backgroundView.backgroundColor = [UIColor whiteColor];
    [self.popupView addSubview:backgroundView];
    
    
    
    // 标题
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, backgroundView.frame.size.width, 20)];
    titleLabel.text = title;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [backgroundView addSubview:titleLabel];
    
    
    self.nickTextField = [[UITextField alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(titleLabel.frame)+10, backgroundView.frame.size.width-20, 20)];
    self.nickTextField.placeholder = placeholder;
    self.nickTextField.textAlignment = NSTextAlignmentCenter;
    [self.nickTextField addTarget:self action:@selector(nickTextField:) forControlEvents:(UIControlEventEditingChanged)];
    if (tag == 0) {
        self.nickTextField.text = self.nickNameLabel.text;
    } else if (tag == 2){
        self.nickTextField.text = self.QQLabel.text;
        self.nickTextField.keyboardType = UIKeyboardTypeNumberPad;
    }
    
    self.nickTextField.tag = tag;
    [self.nickTextField setValue:[UIFont boldSystemFontOfSize:14] forKeyPath:@"_placeholderLabel.font"];

    [backgroundView addSubview:self.nickTextField];
    [self.nickTextField becomeFirstResponder];
    
    // 线
    UIView *xianView = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(self.nickTextField.frame), backgroundView.frame.size.width-20, 1)];
    xianView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [backgroundView addSubview:xianView];
    
    
    
    // 提示label
    self.promptLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(xianView.frame), backgroundView.frame.size.width, 20)];
    self.promptLabel.text = prompt;
    self.promptLabel.textAlignment = NSTextAlignmentCenter;
    self.promptLabel.textColor = [UIColor redColor];
    self.promptLabel.font = [UIFont systemFontOfCustomeSize:14];
    [backgroundView addSubview:self.promptLabel];
    
    // 确定
    UIButton *confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(backgroundView.frame.size.width/2-100, CGRectGetMaxY(self.promptLabel.frame)+10, 80, 30)];
    [confirmButton setTitle:@"确定" forState:(UIControlStateNormal)];
    confirmButton.layer.cornerRadius = 5;
    confirmButton.layer.borderWidth = 1.0f;
    confirmButton.layer.borderColor = [UIColor grayColor].CGColor;
    confirmButton.tag = tag;
    [confirmButton setTitleColor:[UIColor grayColor] forState:(UIControlStateNormal)];
    [confirmButton addTarget:self action:@selector(confirmButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [backgroundView addSubview:confirmButton];
    
    // 取消
    UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(backgroundView.frame.size.width/2+20, CGRectGetMaxY(self.promptLabel.frame)+10, 80, 30)];
    [cancelButton setTitle:@"取消" forState:(UIControlStateNormal)];
    cancelButton.layer.cornerRadius = 5;
    cancelButton.layer.borderWidth = 1.0f;
    cancelButton.layer.borderColor = [UIColor grayColor].CGColor;
    [cancelButton setTitleColor:[UIColor grayColor] forState:(UIControlStateNormal)];
    [cancelButton addTarget:self action:@selector(cancelButtonAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [backgroundView addSubview:cancelButton];

    
    return self.popupView;
}

#pragma mark - 确定
- (void)confirmButtonAction:(UIButton *)sender
{

    [self.popupView removeFromSuperview];
    
    
    if (self.nickTextField.text.length == 0) {
         [PDHUD showShimmeringString:@"资料不能为空" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return;
    }
    
    
    if (sender.tag == 0) {
        PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
        fetchModel.requestParams = @{@"username":self.nickTextField.text};
        [fetchModel fetchWithPath:KCenterEditUserInfo completionHandler:^(BOOL isSucceeded, NSError *error) {
            if (isSucceeded){
                
                self.nickNameLabel.text = self.nickTextField.text;
                self.userNameLabel.text = self.nickTextField.text;
                [PDLoginModel shareInstance].name = self.nickTextField.text;

            } else {
                [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
            }
        }];


    }else if (sender.tag == 2){
        
        PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
        fetchModel.requestParams = @{@"qq":self.nickTextField.text};
        [fetchModel fetchWithPath:KCenterEditUserInfo completionHandler:^(BOOL isSucceeded, NSError *error) {
            if (isSucceeded){
                self.QQLabel.text = self.nickTextField.text;

                
            } else {
                [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
            }
        }];

    }
}



#pragma mark - 取消
- (void)cancelButtonAction:(UIButton *)sender
{
    [self.popupView removeFromSuperview];
}

#pragma mark - sex确定
- (void)sexConfirmButtonAction:(UIButton *)sender
{
    [self.popupView removeFromSuperview];
   
    NSString *sexStr = [[NSString alloc] init];
    if ([self.sexTemp isEqualToString:@"男"]) {
        sexStr = @"1";
    }else
    {
        sexStr = @"2";
    }
    
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    fetchModel.requestParams = @{@"sex":sexStr};
    [fetchModel fetchWithPath:KCenterEditUserInfo completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (isSucceeded){
             self.sexLabel.text = self.sexTemp;
            if ([self.sexTemp isEqualToString:@"男"]) {
                self.sexImage.image = [UIImage imageNamed:@"icon_center_man"];
                
                
            } else{
                
                self.sexImage.image = [UIImage imageNamed:@"icon_center_woman"];
                
            }
            
        } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];

    
}

#pragma mark - 监听textfield的变化
- (void)nickTextField:(UITextField *)sender
{

    
    if (sender.tag == 0) {
        if (sender.text.length > 0) {
            self.promptLabel.text = @"";
        }else
        {
            self.promptLabel.text = @"昵称不能为空";
        }

    }else if (sender.tag ==2){
        if (sender.text.length > 0) {
            self.promptLabel.text = @"";
        }else
        {
            self.promptLabel.text = @"账号不能为空";
        }

        
    }
    
}

// 男
- (void)manBtnAction:(UIButton *)sender
{
    self.manBtn.selected = YES;
    self.womanBtn.selected = NO;
    
    self.sexTemp = sender.currentTitle;
}

// 女
- (void)womanBtnAction:(UIButton *)sender
{
    self.womanBtn.selected = YES;
    self.manBtn.selected = NO;
    self.sexTemp = sender.currentTitle;

}

#pragma mark - 获取个人信息网络请求
- (void)sendRequestToGetUserInfo
{
    PDUserInfoModel *fetchModel = [[PDUserInfoModel alloc]init];
    [fetchModel fetchWithPath:KCenterGetUserInfo completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (isSucceeded){

            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([fetchModel.userinfo[@"username"]isKindOfClass:[NSNull class]]) {
                    self.userNameLabel.text = fetchModel.userinfo[@"phone"];
                    self.nickNameLabel.text = fetchModel.userinfo[@"phone"];
                }else
                {
                    self.userNameLabel.text = fetchModel.userinfo[@"username"];
                    self.nickNameLabel.text = fetchModel.userinfo[@"username"];
                }
                
                
//                self.lifeTimeIDLabel.text = [NSString stringWithFormat:@"ID:%@",fetchModel.userinfo[@"userid"]];
                if ([fetchModel.userinfo[@"sex"] isKindOfClass:[NSNull class]]) {
                    self.sexImage.image = [UIImage imageNamed:@"icon_center_man"];
                    self.sexLabel.text = @"男";

                }else
                {
                    if ([fetchModel.userinfo[@"sex"] isEqualToString:@"1"]) {
                        self.sexLabel.text = @"男";
                        self.sexImage.image = [UIImage imageNamed:@"icon_center_man"];


                    } else{
                        self.sexLabel.text = @"女";
                        self.sexImage.image = [UIImage imageNamed:@"icon_center_woman"];

                    }
                }
                
                if ([fetchModel.userinfo[@"qq"]isKindOfClass:[NSNull class]]) {
                    self.QQLabel.text = @"";

                }else {
                    self.QQLabel.text = fetchModel.userinfo[@"qq"];
                }
                
                
                NSString *porStr = fetchModel.userinfo[@"avatar"];
                NSString *newStr = [porStr stringByReplacingOccurrencesOfString:@"\\" withString:@""];
                [self.portraitImageView uploadChallengeImageWithURL:newStr placeholder:[UIImage imageNamed:@"icon_center_avatar"] callback:^(UIImage *image) {
                    
                }];
                
            });
            
            
        } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];

}

#pragma mark - 更换头像
- (void)portraitTapGesAction:(UITapGestureRecognizer *)sender
{
    
    PDPortraitDefaultViewController *portraitDefault = [[PDPortraitDefaultViewController alloc] init];
    portraitDefault.delegate = self;
    [self.navigationController pushViewController:portraitDefault animated:YES];
    
    
}

# pragma mark - 头像返回
- (void)sendPortraitStr:(NSString *)portraitStr
{
    
    [self.portraitImageView uploadImageWithURL:portraitStr placeholder:nil callback:^(UIImage *image) {
        
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
