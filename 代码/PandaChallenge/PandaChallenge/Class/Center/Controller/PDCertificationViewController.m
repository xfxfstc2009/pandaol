//
//  PDCertificationViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCertificationViewController.h"
#import "PDLoginAlertView.h"
@interface PDCertificationViewController ()<UITextFieldDelegate>


@property(nonatomic,strong)UITextField *realNameFiled;
@property (nonatomic,strong)UITextField *idNumberField;
@property (nonatomic,strong)UIButton *submitBtn;
@property (nonatomic,strong)UILabel *warningSignLabel;
@property (nonatomic,strong)PDImageView *certificationbg;
@property (nonatomic,strong)PDImageView *certificationImage;
@property (nonatomic,strong)UILabel *certificationStateLabel;
@property (nonatomic,strong)UILabel *certificateRealName;   // 认证之后的名字
@property (nonatomic,strong)UILabel *certificateIdNumber;   // 认证之后的身份证号
@property (nonatomic,assign)BOOL ispopUp;                   // 判断是否弹出键盘
@property (nonatomic,strong)JCAlertView *alertView;

@end

@implementation PDCertificationViewController
- (void)dealloc {
    [NOTIFICENTER removeObserver:self];
    
    NSLog(@"%s 被释放啦",__func__);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.barMainTitle = @"实名认证";
    [self createView];
    [self createNotifi];
    
}
#pragma mark KeyBoard Notification
-(void)createNotifi{
    [NOTIFICENTER addObserver:self selector:@selector(openKeyboard:) name:@"UIKeyboardWillShowNotification" object:nil];
    [NOTIFICENTER addObserver:self selector:@selector(closeKeyboard:) name:@"UIKeyboardWillHideNotification" object:nil];
}

- (void)openKeyboard:(NSNotification *)notification {
//    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (self.ispopUp) {
        return;
    }
    NSTimeInterval duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    int option = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
    [UIView animateKeyframesWithDuration:duration delay:0 options:option animations:^{
        self.view.orgin_y = self.view.orgin_y - LCFloat(194);
        self.ispopUp = YES;
    } completion:nil];
    
}

- (void)closeKeyboard:(NSNotification *)notification {
//    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (!self.ispopUp) {
        return;
    }
    NSTimeInterval duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    int option = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
    [UIView animateKeyframesWithDuration:duration delay:0 options:option animations:^{
        
        self.view.orgin_y = self.view.orgin_y + LCFloat(194);
        self.ispopUp = NO;
    } completion:nil];
}


- (void)createView
{
    
    NSString *realNameStr = [PDLoginModel shareInstance].fullName;
    NSString *idNumberStr = [PDLoginModel shareInstance].cn_idcard;
    
    // 背景
    self.certificationbg= [[PDImageView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, LCFloat(194))];
    
    if (self.isChecked == YES) {
        self.certificationbg.image = [UIImage imageNamed:@"icon_center_nocertificationbg"];

    }else{
        self.certificationbg.image = [UIImage imageNamed:@"icon_center_certificationbg"];
    }
    self.certificationbg.userInteractionEnabled = YES;
    [self.view addSubview:self.certificationbg];
    
    
    // 背景上面的image
    self.certificationImage = [[PDImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2-LCFloat(40), self.certificationbg.frame.size.height/2-LCFloat(20), LCFloat(80), LCFloat(40))];
    if (self.isChecked == YES) {
        self.certificationImage.image = [UIImage imageNamed:@"icon_center_nocertificationimage"];
    }else{
        self.certificationImage.image = [UIImage imageNamed:@"icon_center_certificationimage"];
    }
    self.certificationImage.userInteractionEnabled = YES;
    [self.certificationbg addSubview:self.certificationImage];
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [self.view addGestureRecognizer:tap];
    
    
    
    self.certificationStateLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.certificationImage.frame)+LCFloat(5), self.view.frame.size.width, 20)];
    if (self.isChecked == YES) {
        self.certificationStateLabel.text = @"实名认证已完成";
        self.certificationStateLabel.textColor = [UIColor hexChangeFloat:@"fc5453"];

    }else
    {
        
        self.certificationStateLabel.text = @"你尚未进行实名认证";
        self.certificationStateLabel.textColor = [UIColor whiteColor];
    }
    
    
    self.certificationStateLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.certificationStateLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.certificationStateLabel];
    
    
    
    
    
    //名字图标
    PDImageView *realNameImage =[[PDImageView alloc] init];
    realNameImage.frame = CGRectMake(LCFloat(10), CGRectGetMaxY(self.certificationbg.frame)+LCFloat(10), LCFloat(20), LCFloat(20));
    realNameImage.image = [UIImage imageNamed:@"icon_center_realname"];
    [self.view addSubview:realNameImage];
    // 真实姓名
    UILabel *realName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(realNameImage.frame)+LCFloat(10), CGRectGetMaxY(self.certificationbg.frame)+LCFloat(10), LCFloat(70), LCFloat(20))];
    realName.text = @"真实姓名";
    realName.font = [UIFont systemFontOfCustomeSize:14];
    [self.view addSubview:realName];
    
    // 竖线
    UIView *verticalLine = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(realName.frame), CGRectGetMaxY(self.certificationbg.frame)+LCFloat(10), 1, LCFloat(20))];
    verticalLine.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [self.view addSubview:verticalLine];
    
    self.certificateRealName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(verticalLine.frame)+LCFloat(10), CGRectGetMaxY(self.certificationbg.frame)+LCFloat(10), LCFloat(220), LCFloat(20))];
    self.certificateRealName.font = [UIFont systemFontOfCustomeSize:14];
    if (realNameStr.length == 0) {
        self.certificateRealName.text = @"";
    }else
    {
         self.certificateRealName.text = [realNameStr stringByReplacingCharactersInRange:NSMakeRange(0, 1) withString:@"*"];
    }
   
    [self.view addSubview:self.certificateRealName];
    
    if (self.isChecked == NO) {
        
    
    self.realNameFiled = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width-LCFloat(240), CGRectGetMaxY(self.certificationbg.frame)+LCFloat(10), LCFloat(220), LCFloat(20))];
    self.realNameFiled.placeholder = @"请填写你的真实名字";
    [self.realNameFiled setValue:[UIFont systemFontOfCustomeSize:14] forKeyPath:@"_placeholderLabel.font"];

    self.realNameFiled.textAlignment = NSTextAlignmentRight;
    [self.view addSubview:self.realNameFiled];
    
    }
    // 分割线
    UIView *lineView1 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(realName.frame)+LCFloat(10), self.view.frame.size.width, 1)];
    lineView1.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [self.view addSubview:lineView1];
    
    // 身份证图片
    PDImageView *idnumberImage =[[PDImageView alloc] init];
    idnumberImage.frame = CGRectMake(LCFloat(10), CGRectGetMaxY(lineView1.frame)+LCFloat(10), LCFloat(20), LCFloat(20));
    idnumberImage.image = [UIImage imageNamed:@"icon_center_idnumber"];
    [self.view addSubview:idnumberImage];
    // 身份证号
    UILabel *idNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(idnumberImage.frame)+LCFloat(10), CGRectGetMaxY(lineView1.frame)+LCFloat(10), LCFloat(70), LCFloat(20))];
    idNumber.text = @"身份证号";
    idNumber.font = [UIFont systemFontOfCustomeSize:14];
    [self.view addSubview:idNumber];
    
    
    // 竖线
    UIView *verticalLine1 = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(idNumber.frame), CGRectGetMaxY(lineView1.frame)+LCFloat(10), 1, LCFloat(20))];
    verticalLine1.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [self.view addSubview:verticalLine1];
    
    self.certificateIdNumber = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(verticalLine1.frame)+LCFloat(10), CGRectGetMaxY(lineView1.frame)+LCFloat(10), LCFloat(220), LCFloat(20))];
    self.certificateIdNumber.font = [UIFont systemFontOfCustomeSize:14];
    if (idNumberStr.length == 18) {
        self.certificateIdNumber.text = [idNumberStr stringByReplacingCharactersInRange:NSMakeRange(10, 4) withString:@"****"];

    }else{
        self.certificateIdNumber.text =@"";
    }
        [self.view addSubview:self.certificateIdNumber];

    if (self.isChecked == NO) {
        
        self.idNumberField = [[UITextField alloc] initWithFrame:CGRectMake(self.view.frame.size.width-LCFloat(240), CGRectGetMaxY(lineView1.frame)+LCFloat(10), LCFloat(220), LCFloat(20))];
        self.idNumberField.placeholder = @"请填写你的身份证号";
        [self.idNumberField setValue:[UIFont systemFontOfCustomeSize:14] forKeyPath:@"_placeholderLabel.font"];
        self.idNumberField.delegate = self;
        self.idNumberField.textAlignment = NSTextAlignmentRight;
        [self.view addSubview:self.idNumberField];
    }

    
    
    // 分割线
    UIView *lineView2 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(idNumber .frame)+LCFloat(10), self.view.frame.size.width, 1)];
    lineView2.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [self.view addSubview:lineView2];
    
    
    
    // 提示认证的用途
    UILabel *useLabel = [[UILabel alloc] init];
    useLabel.font = [UIFont systemFontOfCustomeSize:12];
    useLabel.text = @"实名认证信息将会用于网费兑换、线下赛事邀请等后续活动的唯一凭证,请务必保证信息的真实准确";
    useLabel.frame = CGRectMake(LCFloat(20), CGRectGetMaxY(lineView2.frame)+LCFloat(10), self.view.frame.size.width-LCFloat(40), [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:14]]);
    
    useLabel.numberOfLines = 0;
    CGSize contentOfSize = [useLabel.text sizeWithCalcFont:useLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(10), CGFLOAT_MAX)];
    useLabel.size_height = contentOfSize.height;
    [self.view addSubview:useLabel];
    
    
    
    
    if (self.isChecked == NO) {
        
        // 提交
        self.submitBtn = [[UIButton alloc] initWithFrame:CGRectMake(LCFloat(30) , CGRectGetMaxY(useLabel.frame)+LCFloat(44), self.view.frame.size.width-LCFloat(60), LCFloat(40))];
        [self.submitBtn setTitle:@"提交" forState:(UIControlStateNormal)];
        [self.submitBtn addTarget:self action:@selector(submitBtnAcyion:) forControlEvents:(UIControlEventTouchUpInside)];
        self.submitBtn.backgroundColor = [UIColor blackColor];
        
        self.submitBtn.layer.cornerRadius = 5;
        self.submitBtn.layer.borderColor = [UIColor grayColor].CGColor;
        self.submitBtn.layer.borderWidth = 1.0f;
        [self.submitBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
        [self.view addSubview:self.submitBtn];
        
        
        
        
        
        
        
        // 告示
        self.warningSignLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.submitBtn.frame)+10, self.view.frame.size.width, 20)];
        self.warningSignLabel.textAlignment = NSTextAlignmentCenter;
        self.warningSignLabel.text = @"实名认证成功后,无法修改、无法清空,请慎重考虑";
        self.warningSignLabel.textColor = [UIColor hexChangeFloat:@"fc5453"];
        self.warningSignLabel.font = [UIFont systemFontOfCustomeSize:12];
        [self.view addSubview:self.warningSignLabel];
    }
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.idNumberField) {
        if (toBeString.length > 18) return NO;
    }
    
    return YES;
}


- (void)tapAction:(UITapGestureRecognizer *)sender
{
    [self.realNameFiled resignFirstResponder];
    [self.idNumberField resignFirstResponder];
}


- (void)submitBtnAcyion:(UIButton *)sender
{
    
    [self.idNumberField resignFirstResponder];
    [self.realNameFiled resignFirstResponder];
    
    if (self.realNameFiled.text.length == 0) {
       [PDHUD showShimmeringString:@"请填写真实名字" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return;
    }else if (self.idNumberField.text.length == 0){
        [PDHUD showShimmeringString:@"请填写身份证号" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return;
    }
    
    
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    fetchModel.requestParams = @{@"real_name":self.realNameFiled.text,@"idcard":self.idNumberField.text};
    
    [fetchModel fetchWithPath:KCenterCertificateSubmit completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (isSucceeded){
            
//            [self.submitBtn removeFromSuperview];
//            [self.warningSignLabel removeFromSuperview];
//            [self.idNumberField removeFromSuperview];
//            [self.realNameFiled removeFromSuperview];
//            self.certificationbg.image = [UIImage imageNamed:@"icon_center_nocertificationbg"];
//            self.certificationImage.image = [UIImage imageNamed:@"icon_center_nocertificationimage"];
//            self.certificationStateLabel.text = @"实名认证已完成";
//            self.certificationStateLabel.textColor = [UIColor redColor];
//            self.certificateIdNumber.text = self.idNumberField.text;
//            self.certificateRealName.text = self.realNameFiled.text;
            
            [PDLoginModel shareInstance].cn_idcard = self.idNumberField.text;
            [PDLoginModel shareInstance].fullName = self.realNameFiled.text;
            
            __weak typeof (self)weakSelf = self;
            PDAlertShowView *showView = [[PDAlertShowView alloc] init];
            [showView alertWithTitle:@"实名认证已提交" headerImage:[UIImage imageNamed:@"icon_report_success"] titleDesc:@"成功提交实名认证信息，请审核通过后查看" buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
                if (!weakSelf) return;
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf.alertView dismissWithCompletion:NULL];
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }];
            
            self.alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:NO];
            [self.alertView show];
        
        } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];

}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
