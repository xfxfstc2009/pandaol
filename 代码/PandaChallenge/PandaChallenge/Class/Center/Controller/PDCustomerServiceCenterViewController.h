//
//  PDCustomerServiceCenterViewController.h
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

typedef void(^popBlock)();

@interface PDCustomerServiceCenterViewController : AbstractViewController

@property (nonatomic,copy)popBlock popBlock;

@end
