//
//  PDHistoryCell.h
//  PandaChallenge
//
//  Created by 盼达 on 16/3/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDHistory.h"

@interface PDHistoryCell : UITableViewCell
@property (nonatomic, strong) PDHistory *history;

@property (nonatomic,assign)CGFloat transferCellHeiht;

+(CGFloat)calculationCellHeightWithModel:(PDHistory *)historyModel;
@end
