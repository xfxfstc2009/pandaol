//
//  PDHistoryCell.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDHistoryCell.h"

@interface PDHistoryCell ()
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *roundImageView;
@property (nonatomic, strong) UIView *lineView;
@property (nonatomic,strong)UIImageView *bgView;

@end

@implementation PDHistoryCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

+(CGFloat)calculationCellHeightWithModel:(PDHistory *)historyModel{
    CGFloat cellHeihgt = 0;
    cellHeihgt += 35+7;
    CGSize contentOfSize = [historyModel.msg sizeWithCalcFont:[UIFont systemFontOfCustomeSize:15.] constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(35) * 2 - (LCFloat(15) + LCFloat(75) + LCFloat(15)), CGFLOAT_MAX)];
    cellHeihgt += contentOfSize.height;
    
    return cellHeihgt;
}

-(void)createView{
    // 1. createLabel
    self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(LCFloat(15), 16, LCFloat(75), 30)];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.textAlignment = NSTextAlignmentRight;
    self.timeLabel.textColor = [UIColor lightGrayColor];
    self.timeLabel.numberOfLines = 2;
    self.timeLabel.font = [UIFont systemFontOfCustomeSize:11];
    [self.contentView addSubview:self.timeLabel];
    
    // line
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.lineView];
    
    // round
    self.roundImageView = [[UIImageView alloc]init];
    self.roundImageView.backgroundColor = [UIColor whiteColor];
    self.roundImageView.image = [UIImage imageNamed:@"icon_history_round.png"];
    [self.contentView insertSubview:self.roundImageView aboveSubview:self.lineView];
    
    // content
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.font = [UIFont systemFontOfCustomeSize:15];
    [self.contentView addSubview:self.contentLabel];

    // bgView
    self.bgView = [[UIImageView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.image = [UIImage imageNamed:@"icon_history_bg"];
    [self.contentView insertSubview:self.bgView belowSubview:self.contentLabel];
}

-(void)setTransferCellHeiht:(CGFloat)transferCellHeiht{
    _transferCellHeiht = transferCellHeiht;
    
}

- (void)setHistory:(PDHistory *)history {
    _history = history;
    
    // 1. time
    
    NSLog(@"时间戳 %@",history.date);
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy.MM.dd hh:mm";
    NSString *dateStr = [dateFormatter stringFromDate:[NSDate dateWithTimeIntervalSince1970:_history.date.integerValue]];
    self.timeLabel.text = dateStr;
    
    self.lineView.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame) + LCFloat(15), 0, .5f, self.transferCellHeiht);
    
    self.roundImageView.frame = CGRectMake(CGRectGetMaxX(self.lineView.frame) - 8, 20, 16, 16);
    
    self.contentLabel.text = history.msg;
    CGSize contentOfSize = [history.msg sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(35) * 2 - CGRectGetMaxX(self.lineView.frame), CGFLOAT_MAX)];
    self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.lineView.frame) + LCFloat(35), self.roundImageView.orgin_y, kScreenWidth - CGRectGetMaxX(self.lineView.frame) - LCFloat(15) * 2 - LCFloat(20) - LCFloat(10), contentOfSize.height);
    
    self.bgView.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame) - LCFloat(15)-LCFloat(10), CGRectGetMinY(self.contentLabel.frame) - 10, CGRectGetWidth(self.contentLabel.frame) + 2 * LCFloat(15), CGRectGetHeight(self.contentLabel.frame)+15+7);
}

@end
