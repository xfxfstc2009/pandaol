//
//  PDNotes.h
//  PandaChallenge
//
//  Created by 盼达 on 16/3/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDHistory.h"

@interface PDNotes : PDFetchModel
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSArray<PDHistory> *history;
@end
