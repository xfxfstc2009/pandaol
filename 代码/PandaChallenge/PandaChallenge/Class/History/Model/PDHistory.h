//
//  PDHistory.h
//  PandaChallenge
//
//  Created by 盼达 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PDHistory <NSObject>

@end

@interface PDHistory : PDFetchModel
@property (nonatomic, strong) NSString *date;
@property (nonatomic, strong) NSString *msg;
@end
