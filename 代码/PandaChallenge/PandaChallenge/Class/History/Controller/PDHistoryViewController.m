//
//  PDHistoryViewController.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDHistoryViewController.h"
#import <SVPullToRefresh.h>
#import "PDHistory.h"
#import "PDNotes.h"
#import "PDStupSQLiteManager.h"
#import "UIScrollView+ZSPullToRefresh.h"
#import "UIView+PDPrompt.h"
#import "PDHistoryCell.h"

@interface PDHistoryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray<PDHistory *> *historyCount;
@property (assign, nonatomic) NSInteger page;
@end

@implementation PDHistoryViewController
- (NSMutableArray *)historyCount {
    if (!_historyCount) {
        _historyCount = [NSMutableArray array];
    }
    return _historyCount;
}

- (void)dealloc {
    NSLog(@"%s 被释放啦",__func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createTableView];
}

- (void)pageSetting {
    self.barMainTitle = @"历程";
}

- (void)constantSetting {
    self.page = 1;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView triggerPullToRefresh];// 一显示界面就加载
}

- (void)createTableView {
    self.tableView = [[UITableView alloc] initWithFrame:kScreenBounds];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor whiteColor];
//    self.tableView.tableFooterView = [UIView new];
    self.tableView.separatorStyle = UITableViewCellSelectionStyleNone;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 80;
    [self.view addSubview:self.tableView];
    [self fetchLastHistory];
    [self fetchMoreHistory];
}

#pragma mark - HistoryFetch
/**
 *  下拉刷新 获取最新数据
 */
- (void)fetchLastHistory {
    __weak typeof(self) weakSelf = self;
    [self.tableView addQSPullToRefreshWithActionHandler:^{
        if (!weakSelf) return ;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.page = 1;
        [strongSelf.tableView.infiniteScrollingView stopAnimating];
        PDNotes *notes =  [[PDNotes alloc] init];
        // note
        notes.requestParams = @{@"page":@(strongSelf.page)};
        [notes fetchWithPath:kHistoryPath completionHandler:^(BOOL isSucceeded, NSError *error) {
            [strongSelf.tableView.pullToRefreshView stopAnimating];
            if (isSucceeded == YES) {
                [strongSelf.historyCount removeAllObjects];// 清空数组后放最新的数据
                // 先判断用户有没有历程数据
                if (notes.history.count == 0) {
                    [strongSelf showNoData];
                } else {
                    for (PDHistory *history in notes.history) {
                        [strongSelf.historyCount addObject:history];
                        if ([PDStupSQLiteManager lastedTime] < history.date.integerValue)
                            [PDStupSQLiteManager insertWithModel:history];
                    }
                }
                NSLog(@"lastedTime - %d",[PDStupSQLiteManager lastedTime]);
                [strongSelf.tableView reloadData];
            }
            else {
                [strongSelf showError:error];
            }
        }];
    }];
}

#pragma mark - 获取数据库中的历史数据
/**
 *  上拉刷新 获取请求服务器中的下一页
 */
- (void)fetchMoreHistory {
    __weak typeof(self) weakSelf = self;
    [self.tableView  addQSInfiniteScrollingWithActionHandler:^{
        if (!weakSelf)
            return ;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.page++;
        [strongSelf.tableView.pullToRefreshView stopAnimating];
        PDNotes *notes = [[PDNotes alloc] init];
        notes.requestParams = @{@"page":@(strongSelf.page)};
        [notes fetchWithPath:kHistoryPath completionHandler:^(BOOL isSucceeded, NSError *error) {
            [strongSelf.tableView.infiniteScrollingView stopAnimating];
            if (isSucceeded) {
                if (notes.history.count == 0) {
                    [PDHUD dismiss];
                    [PDHUD showShimmeringString:@"没有更多数据了" maskType:WSProgressHUDMaskTypeDefault delay:1.5];
                    return ;
                }
                 for (PDHistory *history in notes.history) {
                    [strongSelf.historyCount addObject:history];
                }
                
                [strongSelf.tableView reloadData];
            }
            else {
                strongSelf.page--;
                [strongSelf showError:error];
            }
        }];
    }];
}

#pragma mark - 错误提示
- (void)showError:(NSError *)error {
    self.historyCount = [PDStupSQLiteManager getAllData];
    [self.tableView reloadData];
    if (self.historyCount.count == 0) {
        // 暂无数据
        [self showNoData];
    }
    [PDTool showNetErrorString:nil];
}

#pragma mark - 暂无数据
- (void)showNoData {
    UIView *view = [[UIView alloc] initWithFrame:kScreenBounds];
    view.backgroundColor = [UIColor whiteColor];
    [view showPrompt:@"暂无数据" withImage:[UIImage imageNamed:@"icon_history_none"]  andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
    [self.view addSubview:view];
}

#pragma mark -  UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.historyCount.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *identifier = @"HistoryCell";
    PDHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[PDHistoryCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.backgroundColor = [UIColor clearColor];
    cell.transferCellHeiht = cellHeight;
    PDHistory *history = [self.historyCount objectAtIndex:indexPath.row];
    cell.history = history;
    return cell;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    PDHistory *history = [self.historyCount objectAtIndex:indexPath.row];
    return [PDHistoryCell calculationCellHeightWithModel:history];
}

@end
