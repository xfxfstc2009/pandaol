//
//  PDSuggestionViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSuggestionViewController.h"
@interface PDSuggestionViewController()<UITextViewDelegate>
@property (nonatomic,strong)UITextView *adviceTextView;                 /**< 建议textView*/
@property (nonatomic,strong)UILabel *textlimitLabel;                    /**< 控制文字长度的Label*/
@property (nonatomic,strong)UIButton *sendButton;                       /**< 提交按钮*/
@property (nonatomic,strong)JCAlertView *alertView;

@end

@implementation PDSuggestionViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createView];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"意见反馈";
}

#pragma mark - createView
-(void)createView{
    // 建议textView
    self.adviceTextView = [[UITextView alloc]init];
    self.adviceTextView.delegate = self;
    self.adviceTextView.textColor = [UIColor blackColor];
    self.adviceTextView.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.adviceTextView.returnKeyType = UIReturnKeyDefault;
    self.adviceTextView.keyboardType = UIKeyboardTypeDefault;
    self.adviceTextView.backgroundColor = [UIColor whiteColor];
    self.adviceTextView.scrollEnabled = YES;
    self.adviceTextView.limitMax = 300;
    self.adviceTextView.frame = CGRectMake(LCFloat(16),LCFloat(20), kScreenBounds.size.width - 2 * LCFloat(16), LCFloat(203));
    [self.view addSubview:self.adviceTextView];
    self.adviceTextView.placeholder = @"您的宝贵意见,是我们进步的源泉!";
    __weak typeof(self)weakSelf = self;
    [weakSelf.adviceTextView textViewDidChangeWithBlock:^(NSInteger currentCount) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf adjustWithButtonAndPlaceholderWithCount:currentCount];
    }];
    
    
    // 提示
    UILabel *promptLabel = [[UILabel alloc] initWithFrame:CGRectMake(LCFloat(16), CGRectGetMaxY(self.adviceTextView.frame), self.view.frame.size.width-LCFloat(32), 50)];
    promptLabel.text = @"请详细描述您遇到的问题,有助于我们快速定位并解决问题,或留下宝贵的建议或意见,我们会认真进行评估";
    promptLabel.textColor = [UIColor grayColor];
    promptLabel.numberOfLines = 0;
    promptLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.view addSubview:promptLabel];
    
    
    // 确定发送按钮
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.sendButton setTitle:@"提交" forState:UIControlStateNormal];
    self.sendButton.titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
    self.sendButton.layer.cornerRadius = LCFloat(5);
    self.sendButton.frame = CGRectMake(LCFloat(16), CGRectGetMaxY(promptLabel.frame) + LCFloat(25), kScreenBounds.size.width - 2 * LCFloat(16), LCFloat(44));
    self.sendButton.enabled = NO;
    [self.sendButton buttonWithBlock:^(UIButton *button) {
        // 执行接口
        PDFetchModel *fetchModel = [[PDFetchModel alloc] init];
        fetchModel.requestParams = @{@"content":self.adviceTextView.text};
        [fetchModel fetchWithPath:KCenterFeedback completionHandler:^(BOOL isSucceeded, NSError *error) {
            if (isSucceeded){
                
                [self.adviceTextView resignFirstResponder];
                
                
                __weak typeof (self)weakSelf = self;
                PDAlertShowView *showView = [[PDAlertShowView alloc] init];
                [weakSelf.navigationController popViewControllerAnimated:YES];

                [showView alertWithTitle:@"提交成功" headerImage:[UIImage imageNamed:@"icon_report_success"] titleDesc:@"感谢您的反馈,我们将会努力改进" buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
                    if (!weakSelf) return;
                    __strong typeof(weakSelf) strongSelf = weakSelf;
                    [strongSelf.alertView dismissWithCompletion:NULL];
                    [strongSelf.navigationController popViewControllerAnimated:YES];

                }];
                
                self.alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:NO];
                [self.alertView show];
                
                
            
            } else {
                [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
            }
        }];

        
    }];
    [self.view addSubview: self.sendButton];
    
    
    
    
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backTapAction:)];
    [self.view addGestureRecognizer:tap];
}

#pragma mark - 键盘回收
- (void)backTapAction:(UITapGestureRecognizer *)sender
{
    [self.adviceTextView resignFirstResponder];
}


#pragma mark 
-(void)adjustWithButtonAndPlaceholderWithCount:(NSInteger)count{
    if (count == 0){
        self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
        self.sendButton.enabled = NO;
    } else {
        self.sendButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        self.sendButton.enabled = YES;
    }
}

@end
