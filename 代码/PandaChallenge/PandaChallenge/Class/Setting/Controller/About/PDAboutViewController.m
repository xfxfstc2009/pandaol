//
//  PDAboutViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAboutViewController.h"

@implementation PDAboutViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"关于我们";
}


- (void)createView
{
    
    
    self.webView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-40-64);

    self.urlStr = @"http://m.pandaol.net/panda/cafe/about.html";
    
    
    // 声明
    UILabel *declaration = [[UILabel alloc] initWithFrame:CGRectMake(0, self.view.frame.size.height-64 - 40, self.view.frame.size.width, 30)];
    declaration.text = @"Copyright©2013-2016\n杭州盼达网络科技有限公司";
    declaration.numberOfLines = 0;
    declaration.textColor = [UIColor hexChangeFloat:@"989898"];
    declaration.textAlignment = NSTextAlignmentCenter;
    declaration.font = [UIFont systemFontOfCustomeSize:11];
    [self.view addSubview:declaration];
    
    
    
}
@end
