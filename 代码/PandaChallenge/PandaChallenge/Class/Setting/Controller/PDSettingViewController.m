//
//  PDSettingViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSettingViewController.h"
#import "PDLoginViewController.h"
#import "PDSuggestionViewController.h"              // 意见建议
#import "PDAboutViewController.h"                   // 关于我们
#import "PDChangePasswordViewController.h"          // 修改密码
#import "PDNavigationController.h"
#import "UIImageView+WebCache.h"
@interface PDSettingViewController()<UITableViewDelegate,UITableViewDataSource,UIAlertViewDelegate>
@property (nonatomic,strong)UITableView *settingTableView;
@property (nonatomic,strong)NSArray *settingArr;
@property (nonatomic,strong)NSArray *settingImageArr;
@property (nonatomic,strong)UILabel *deleteLabel;
@property (nonatomic,strong)UIButton *logoutBtn;

@end

@implementation PDSettingViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithinit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"设置";
}

-(void)arrayWithinit{
    self.settingArr = @[@"修改密码",@"删除缓存",@"关于我们"];
    self.settingImageArr = [NSArray arrayWithObjects:@"icon_center_modify",@"icon_center_delete",@"icon_center_about", nil];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.settingTableView){
        self.settingTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.settingTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.settingTableView.delegate = self;
        self.settingTableView.dataSource = self;
        self.settingTableView.bounces = NO;
        self.settingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.settingTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.settingTableView];
    }
    
    
    
    // 退出
    self.logoutBtn = [[UIButton alloc] initWithFrame:CGRectMake(30, self.view.frame.size.height-150, self.view.frame.size.width-60, 40)];
    [self.logoutBtn setImage:[UIImage imageNamed:@"icon_center_logout"] forState:(UIControlStateNormal)];
    [self.logoutBtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 5, 15)];
    self.logoutBtn.backgroundColor = [UIColor colorWithRed:231/255.0 green:62/255.0 blue:67/255.0 alpha:1.0];
    self.logoutBtn.layer.cornerRadius = 5;
    [self.logoutBtn setTitle:@"退出登录" forState:(UIControlStateNormal)];
    [self.logoutBtn setTitleColor:[UIColor whiteColor] forState:(UIControlStateNormal)];
    [self.logoutBtn addTarget:self action:@selector(logoutBtnAction:) forControlEvents:(UIControlEventTouchUpInside)];
    [self.view addSubview:self.logoutBtn];
    
}


#pragma mark - 退出登录
- (void)logoutBtnAction:(UIButton *)sender {
    self.logoutBtn.enabled = NO;
    if ([PDLoginModel shareInstance].hasLoggedIn) {
        __weak typeof(self) weakSelf = self;
        [[PDLoginModel shareInstance] logoutWithBlock:^(BOOL isSucceeded, NSError *error) {
            if (!weakSelf) return;
            __strong typeof(weakSelf) strongSelf = weakSelf;
            strongSelf.logoutBtn.enabled = YES;
            if (isSucceeded) {
                [PDHUD showShimmeringString:@"用户已退出" maskType:WSProgressHUDMaskTypeGradient delay:2];
                PDLoginViewController *loginViewCintroller = [[PDLoginViewController alloc]init];
                PDNavigationController *navigationController = [[PDNavigationController alloc]initWithRootViewController:loginViewCintroller];
                [[RESideMenu shareInstance] setContentViewController:navigationController];
            }else{
               [PDHUD showShimmeringString:@"用户未登录" maskType:WSProgressHUDMaskTypeDefault delay:2];
            }
        }];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.settingArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cellWithRowOne.textLabel.text = self.settingArr[indexPath.row];
        cellWithRowOne.textLabel.font = [UIFont systemFontOfCustomeSize:17];
        cellWithRowOne.imageView.image = [UIImage imageNamed:self.settingImageArr[indexPath.row]];
        
        if (indexPath.row == 1) {
            float size = [[SDImageCache sharedImageCache] getSize];
            self.deleteLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.view.frame.size.width-200, 0, 170, 44)];
            self.deleteLabel.text = [NSString stringWithFormat:@"%.2fkb",size/1024];
            self.deleteLabel.textAlignment = NSTextAlignmentRight;
            self.deleteLabel.textColor = [UIColor grayColor];
            self.deleteLabel.font = [UIFont systemFontOfCustomeSize:15];
            [cellWithRowOne addSubview:self.deleteLabel];
            cellWithRowOne.accessoryType = UITableViewCellAccessoryNone;

        }
    }
   
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0){            // 修改密码
        PDChangePasswordViewController *passwordViewController = [[PDChangePasswordViewController alloc]init];
        [self.navigationController pushViewController:passwordViewController animated:YES];
    } else if (indexPath.row == 1){     // 删除缓存
        // 清除缓存
        float size = [[SDImageCache sharedImageCache] getSize];
        NSString *clean = [NSString stringWithFormat:@"是否清理该应用的缓存文件?\n(%.2fkb)", size/1024];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:clean delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
        alert.stringTag = @"alertView";
        alert.delegate = self;
        [alert show];

    } else if (indexPath.row == 2){     // 关于我们
        PDAboutViewController *aboutMeViewController = [[PDAboutViewController alloc]init];
        [self.navigationController pushViewController:aboutMeViewController animated:YES];
    }
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView.stringTag isEqualToString:@"alertView"]) {
        if (buttonIndex == 1) {
            [[SDImageCache sharedImageCache] clearDisk];
            self.deleteLabel.text = @"0.00kb";
        }
    }
}
@end
