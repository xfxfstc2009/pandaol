//
//  PDExchangeHelpViewController.m
//  PandaChallenge
//
//  Created by 盼达 on 16/4/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDExchangeHelpViewController.h"

@interface PDExchangeHelpViewController ()

@end

@implementation PDExchangeHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self loadUrlStr];
}

- (void)pageSetting {
    self.barMainTitle = @"";
}

- (void)loadUrlStr {
    self.urlStr = kIntegral_help;
}

@end
