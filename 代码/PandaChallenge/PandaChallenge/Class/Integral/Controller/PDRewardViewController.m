//
//  PDRewardViewController.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//


#import "PDRewardViewController.h"
#import "PDExchangeViewController.h"
#import "PDUserCardModel.h"
#import "PDUsedCard.h"
#import "PDGetRewardModel.h"
#import "PDQrCodeViewController.h"
#import "PDCertificationViewController.h"
#import "PDExchangeHelpViewController.h"

#define SPACE           HSFloat(20) // 最上边_卡的行_高顶边距离竹子视图下边沿的高度
#define CARD_WIDTH      HSFloat(61)
#define CARD_HEIGHT     HSFloat(72)
// 卡上下的空隙
#define CARD_MARGIN     (((kScreenHeight-100-44-HSFloat(60)-SPACE*2)/5-CARD_HEIGHT)/2)
// 卡的行高
#define CARD_ROW_HEIGHT (CARD_HEIGHT+CARD_MARGIN*2)
// 卡与卡的上下间隙
#define CARD_SAPCE      (CARD_MARGIN*2)
// 卡与卡水平间距
#define CARD_HORI_SPACE ((kScreenWidth-4*CARD_WIDTH-2*10)/3)

#define Bottom_ButtonView_Height  80

// 标签的背景色（绿）
#define customGreenColor colorWithRed:41/255.0 green:149/255.0 blue:64/255.0 alpha:1.0

// 为不与底层其它视图起冲突，将tag值变大
static const NSInteger tagNormal      = 500; //卡图
static const NSInteger tagLabel       = 11;  //label相对于卡图
static const NSInteger tagAmountBg    = 22;  //label背景想对于卡图
/**王卡的等级*/
static const NSInteger kKingCardLevel = 999;

@interface PDRewardViewController ()<UIGestureRecognizerDelegate>
@property (nonatomic, assign) NSInteger integral;
@property (nonatomic, strong) UILabel *myBambooLabel;
@property (nonatomic, strong) NSMutableArray<PDKCard *> *kCards;
// 此数组中存的卡的等级从10～0
// 对应的数组的下表是    0~10
// (10-level) 就是数组中的对应的卡的原点坐标
@property (nonatomic, strong) NSMutableArray *pointArray; // 存放卡（x，y）坐标的数组
@property (nonatomic, strong) NSMutableArray *useableImageViewArray; // 显示在界面上的一组图片
@property (nonatomic, strong) NSMutableArray *reuseImageViewArray; // 复用的图片数组
@property (nonatomic, strong) NSMutableArray *amountArray;
@property (nonatomic, strong) NSMutableArray *amountBgImageViewArray;
// 记录下用掉卡的名称
@property (nonatomic, strong) NSMutableArray *usedCardArray;
// 用掉的卡转换成的模型
@property (nonatomic, strong) NSMutableArray *usedCardModelArray;
/** 要上传用户用掉k卡的数组*/
@property (nonatomic, strong) NSMutableArray *updateUsedCardArray;
@property (nonatomic, strong) UIImageView *reuseImageView;
/** 领赏按钮*/
@property (nonatomic, strong) UIButton *rewardButton;
/** 点击一键领赏是否有效果*/
@property (nonatomic, assign) BOOL canClickAllReward;
/** 不同卡对应积分*/
@property (nonatomic, strong) NSMutableDictionary *integralDic;
/** 存放卡可变字典*/
@property (nonatomic, strong) NSMutableDictionary *pointDic;
/**是否是推广员*/
@property (nonatomic, assign) BOOL isPromoter;
@property (nonatomic, strong) JCAlertView *alertView;
/**存放label的字典*/
@property (nonatomic, strong) NSMutableDictionary *labelDic;
@end

@implementation PDRewardViewController
- (void)dealloc {
    NSLog(@"%s 被释放啦",__func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrrayWithInit];
    [self judgeIsPromoter];
    // 获取用户的K卡
    [self getkCards];
    [self pageSetting];
    [self rightButtonSetting];
    [self createMyBambooView];
    [self createBottomView];
    [self kCardPlaceSetting];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [PDHUD dismiss];
}

- (void)arrrayWithInit {
    // 用户卡模型数组
    self.kCards = [NSMutableArray array];
    // 卡坐标数组
    self.pointArray = [NSMutableArray array];
    // 用户卡图数组
    self.useableImageViewArray = [NSMutableArray array];
    // 复用图片数组
    self.reuseImageViewArray = [NSMutableArray array];
    // 卡数量Label的数组
    self.amountArray = [NSMutableArray array];
    // 拖入领赏池的卡等级数组
    self.usedCardArray = [NSMutableArray array];
    // 由领赏池中卡等级转换成的卡模型数组
    self.usedCardModelArray = [NSMutableArray array];
    // 不同的卡对应不同的积分 根据健支队来取
    self.integralDic = [NSMutableDictionary dictionary];
    // 卡数量label背景图数组
    self.amountBgImageViewArray = [NSMutableArray array];
    // 上传用掉的卡组
    self.updateUsedCardArray = [NSMutableArray array];
    // 不同卡组的坐标字典 根据卡的等级取取坐标
    self.pointDic = [NSMutableDictionary dictionary];
    // label根据卡的level来取
    self.labelDic = [NSMutableDictionary dictionary];
}

//*******
- (void)getkCards {
    [PDHUD showWithStatus:@"正在获取用户卡组,请稍后..." maskType:WSProgressHUDMaskTypeDefault delay:0];
    PDUserCardModel *fetchModel = [[PDUserCardModel alloc] init];
    [fetchModel fetchWithPath:kIntegral_getUserCard completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        if (isSucceeded) {
            for (PDKCard *kCard in fetchModel.userCard) {
                [self.kCards addObject:kCard];
                [self.integralDic setObject:kCard.exchange_point forKey:kCard.card_level];
            }
            
            // 获取积分
            self.integral = fetchModel.points.integerValue;
            self.myBambooLabel.text = [NSString stringWithFormat:@"我的竹子：%ld B",(long)self.integral];
            
            
            // 生成用户卡组
            [self kCardForUserWithAnimation:YES];
        } else {
            [PDTool showNetErrorString:nil];
        }
    }];
}

#pragma mark - 推广员
// 判断是否是推广员
- (void)judgeIsPromoter {
    self.isPromoter = [PDLoginModel shareInstance].isSpreader;
    if (self.isPromoter) {
        NSLog(@"是推广员");
    } else {
        NSLog(@"不是推广员");
    }
}

- (void)pageSetting {
    self.barMainTitle = @"领赏";
    [self.cloudView removeGestureRecognizer:self.cloudView.gesture];
}

#pragma mark - 跳到兑换网费
- (void)rightButtonSetting {
    __weak typeof(self) weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_net_activity"] barHltImage:nil action:^{
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        NSLog(@"cn_idCard %@",[PDLoginModel shareInstance].cn_idcard);
        // 进行实名认证判断
        if ([PDLoginModel shareInstance].cn_idcard) {
//             打开扫一扫
            PDQrCodeViewController *qrCodeVC = [[PDQrCodeViewController alloc] init];
            [strongSelf.navigationController pushViewController:qrCodeVC animated:YES];
        } else {
            // 跳出是否咬跳转实名认证
            PDAlertShowView *showView = [[PDAlertShowView alloc] init];
            [showView alertWithTitle:@"您尚未实名认证" headerImage:[UIImage imageNamed:@"icon_report_warn"] titleDesc:@"实名认证后方可参加网吧活动" buttonNameArray:@[@"前往实名认证"] clickBlock:^(NSInteger index) {
                // 进入实名认证
                PDCertificationViewController *certificationVC = [[PDCertificationViewController alloc] init];
                [strongSelf.navigationController pushViewController:certificationVC animated:YES];
                [strongSelf.alertView dismissWithCompletion:nil];
            }];
            strongSelf.alertView = [[JCAlertView alloc] initWithCustomView:showView dismissWhenTouchedBackground:NO];
            [strongSelf.alertView show];
        }
    }];
}

#pragma mark - 顶部竹子

- (void)createMyBambooView {
    UIView *myBambooView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, HSFloat(60))];
    myBambooView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:myBambooView];
    
     // 我的竹子
    UILabel *myBambooLabel = [[UILabel alloc] init];
    self.myBambooLabel = myBambooLabel;
    myBambooLabel.font = [UIFont systemFontOfCustomeSize:16];
    myBambooLabel.frame = CGRectMake(0, myBambooView.center.y-([NSString contentofHeightWithFont:myBambooLabel.font]/2), kScreenWidth, [NSString contentofHeightWithFont:myBambooLabel.font]);
    myBambooLabel.backgroundColor = [UIColor whiteColor];
    myBambooLabel.textAlignment = NSTextAlignmentCenter;
    myBambooLabel.text = [NSString stringWithFormat:@"我的竹子：0 B"];
    [myBambooView addSubview:myBambooLabel];
    
    // 帮助按钮
    CGFloat spaceValue = LCFloat(10); // 上右
    CGFloat buttonWidth = LCFloat(30);
    UIButton *helpButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [helpButton setImage:[UIImage imageNamed:@"icon_reward_help"] forState:UIControlStateNormal];
    helpButton.frame = CGRectMake(kScreenWidth - spaceValue - buttonWidth, CGRectGetMaxY(myBambooView.frame) + spaceValue, buttonWidth, buttonWidth);
    [self.view addSubview:helpButton];
    __weak typeof(self) weakSelf = self;
    [helpButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        PDExchangeHelpViewController *exchangeHelpVC = [[PDExchangeHelpViewController alloc] init];
        [strongSelf.navigationController pushViewController:exchangeHelpVC animated:YES];
    }];
}

#pragma mark - 底部按钮

// 底部领赏按钮
- (void)createBottomView {
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenHeight-(80+64), kScreenWidth, Bottom_ButtonView_Height)];
    bottomView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:bottomView];
    
    UIButton *rewardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rewardButton = rewardButton;
    [rewardButton setBackgroundImage:[UIImage imageNamed:@"icon_rewardBtn"] forState:UIControlStateNormal];
    [rewardButton setTitle:@"领赏" forState:UIControlStateNormal];
    rewardButton.tintColor = [UIColor whiteColor];
    rewardButton.titleLabel.font = [UIFont systemFontOfCustomeSize:16];
    [bottomView addSubview:rewardButton];
    [rewardButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.mas_equalTo(0);
        make.size.mas_equalTo(CGSizeMake(60, 60));
    }];
    [rewardButton addTarget:self action:@selector(reward:) forControlEvents:UIControlEventTouchUpInside];
    
    // 一键领取
    UIButton *allButton = [UIButton buttonWithType:UIButtonTypeCustom];
    allButton.backgroundColor = [UIColor whiteColor];
    [allButton setTitle:@"一键领取" forState:UIControlStateNormal];
    [allButton setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    allButton.layer.cornerRadius = LCFloat(5);
    allButton.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
    allButton.layer.borderWidth = .5f;
    allButton.titleLabel.font = [UIFont systemFontOfCustomeSize:11];
    [bottomView addSubview:allButton];
    [allButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(rewardButton);
        make.centerX.mas_equalTo(LCFloat(-100));
        make.size.mas_equalTo(CGSizeMake(LCFloat(80), 20));
    }];
    [allButton addTarget:self action:@selector(allReward:) forControlEvents:UIControlEventTouchUpInside];
    self.canClickAllReward = YES; //一开始，代表可以点
    
    // 初始化
    UIButton *initButton = [UIButton buttonWithType:UIButtonTypeCustom];
    initButton.backgroundColor = [UIColor whiteColor];
    [initButton setTitle:@"初始化" forState:UIControlStateNormal];
    [initButton setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    initButton.layer.cornerRadius = LCFloat(5);
    initButton.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
    initButton.layer.borderWidth = .5f;
    initButton.titleLabel.font = [UIFont systemFontOfCustomeSize:11];
    [bottomView addSubview:initButton];
    [initButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.mas_equalTo(rewardButton);
        make.centerX.mas_equalTo(LCFloat(100));
        make.size.mas_equalTo(allButton);
    }];
    [initButton addTarget:self action:@selector(buttonForInit:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - button methods
// 问题1：初始化之后曾经用掉的图片仍存在
// 原因 生成的复用图片存在
// 解决 删除复用的图片

// 问题2:拖动之后直接点一键领取 复用图片仍然存在

- (void)buttonForInit:(UIButton *)button {
    [self cardsForInitWithAnimation:YES];
}

- (void)reward:(UIButton *)button {
    // 当数组中元素个数增加的时候
    if (self.usedCardModelArray.count == 0) {
        if (self.usedCardArray.count == 0) {
            PDAlertShowView *showView = [[PDAlertShowView alloc] init];
            [showView alertForAlertViewType:PDAlertShowViewTypeWarn withTitle:@"兑换失败" desc:@"您尚未选择任何K卡"];
            JCAlertView *alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:YES];
            [alertView show];
            return ;
        }
        [self changCardToModel];
    }
    // 将一键领赏按钮开启
    self.canClickAllReward = YES;
    
    // 计算换算后的总积分
    NSInteger totalIntegral = 0;
    for (NSString *point in self.usedCardArray) {
        totalIntegral += [[self.integralDic objectForKey:point] integerValue];
    }
    
    
// 这边写兑换成功后跳出的提示框
// K0卡不能兑换
    
    [self showTotalIntegralTableViewWithCardArray:self.usedCardModelArray totalIntegral:totalIntegral];
}

- (void)allReward:(UIButton *)button {
    //self.usedCardArray.count 此数组是存放丢掉的卡的数量的
    // 有两种情况：1、先有拖拽，那么此时self.usedCardArray.count > 0；再一键领赏
    //           2、直接一键领赏self.usedCardArray.count == 0
    // 解决：设置一个bool值。
    if (self.usedCardModelArray.count == 0 && self.canClickAllReward) {
        self.canClickAllReward = NO;
        // 这样可以防止kCards数组变的时候 i 会变
        NSInteger allCardCount = 11;
        for (int i = 0; i < allCardCount; i++) {
            // 其中第0，1，10张不取
            if (self.isPromoter ? (i == 1 || i == 10) : (i == 0 || i == 1 || i == 10)) {
                continue;
            }
            // 采取按顺序取图片 存在界面的图片
            UIImageView *existImageView = [self.view viewWithTag:(i+tagNormal)];
            UILabel *existLabel = [self.view viewWithTag:(i+tagLabel+tagNormal)];
            NSString *pointNum = [existLabel.text stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"x"]];
            UIImageView *existBgImageView = [self.view viewWithTag:(i+tagAmountBg+tagNormal)];
            if (existImageView) {
                [UIView animateWithDuration:1.0 delay:i*.1 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    existImageView.center = CGPointMake(self.view.center.x, kScreenHeight-40-64);
                    existImageView.transform = CGAffineTransformMakeScale(0.3, 0.3);
                    existLabel.alpha = 0;
                    existBgImageView.alpha = 0;
                } completion:^(BOOL finished) {
                    [existImageView removeFromSuperview];
                    [existLabel removeFromSuperview];
                    [existBgImageView removeFromSuperview];
                    NSString *point = [self.integralDic objectForKey:[NSString stringWithFormat:@"%ld",(long)i]];
                    [self showIntegralLabelForKCardPoint:[NSString stringWithFormat:@"%ld",(long)point.integerValue * pointNum.integerValue]];
                }];
            }
        }
        
        // 所有的数据都在kCards里面
        // self.usedCardArray 是记录等级的不重复的一个数组
        [self.usedCardArray removeAllObjects]; // 先保证这个数组是空的
        for (PDKCard *kCard in self.kCards) {
            if (self.isPromoter ? (kCard.card_level.integerValue == 1 || kCard.card_level.integerValue == 10) : (kCard.card_level.integerValue == 0 || kCard.card_level.integerValue == 1 || kCard.card_level.integerValue == kKingCardLevel)) {
                continue;
            }
            for (int i = 0; i < kCard.card_num.integerValue; i++) {
                [self.usedCardArray addObject:kCard.card_level];
            }
        }
    }
}

#pragma mark - 卡组恢复初始状态
- (void)cardsForInitWithAnimation:(BOOL)animation {
    // 把原来的图和数据label都删除掉
    for (UIImageView *usedImageView in self.useableImageViewArray) {
        [usedImageView removeFromSuperview];
    }
    [self.useableImageViewArray removeAllObjects];
    for (UILabel *amountLabel in self.amountArray) {
        [amountLabel removeFromSuperview];
    }
    [self.amountArray removeAllObjects];
    [self.labelDic removeAllObjects];
    
    for (UIImageView *amountBgImageView in self.amountBgImageViewArray) {
        [amountBgImageView removeFromSuperview];
    }
    [self.amountBgImageViewArray removeAllObjects];
    
    // 删除复用的图片
    for (UIImageView *reuseImageView in self.reuseImageViewArray) {
        [reuseImageView removeFromSuperview];
    }
    [self.reuseImageViewArray removeAllObjects];
    
    // 删除model
    [self.usedCardArray removeAllObjects];
    [self.usedCardModelArray removeAllObjects];
    [self kCardForUserWithAnimation:animation];
    
    // 复位一键领赏的限制参数
    self.canClickAllReward = YES;
}

#pragma mark - 将一张张卡转换成模型类 *** self.usedCardArray-->self.usedCardModelArray
- (void)changCardToModel {
    NSInteger count = 1; // 计数 没多一张相同的卡 计数＋1
    if (self.usedCardArray.count == 1) {
        PDUsedCard *usedCard = [[PDUsedCard alloc] init];
        usedCard.card_level = self.usedCardArray[0];
        usedCard.card_num = [NSString stringWithFormat:@"%ld",(long)count];
        usedCard.exchange_point = [self.integralDic objectForKey:usedCard.card_level];
        [self.usedCardModelArray addObject:usedCard];
    } else {
        for (int i = 0;i < self.usedCardArray.count;i++) {
            if (i == self.usedCardArray.count - 1) {// 代表最后一位
                PDUsedCard *usedCard = [[PDUsedCard alloc] init];
                usedCard.card_level = self.usedCardArray[i];
                usedCard.card_num = [NSString stringWithFormat:@"%ld",(long)count];
                usedCard.exchange_point = [self.integralDic objectForKey:usedCard.card_level];

                [self.usedCardModelArray addObject:usedCard];
                break;
            }
            if (self.usedCardArray[i+1] == self.usedCardArray[i]) {
                count++;
            } else {
                PDUsedCard *usedCard = [[PDUsedCard alloc] init];
                usedCard.card_level = self.usedCardArray[i];
                usedCard.card_num = [NSString stringWithFormat:@"%ld",(long)count];
                usedCard.exchange_point = [self.integralDic objectForKey:usedCard.card_level];
                
                [self.usedCardModelArray addObject:usedCard];
                count = 1;
            }
        }
    }
}

#pragma mark - 显示总兑换积分表
- (void)showTotalIntegralTableViewWithCardArray:(NSArray *)cardArray totalIntegral:(NSInteger)totalIntegral {
    __weak typeof(self) weakSelf = self;
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    [showView alertListWithTitle:@"换竹子" cardModelArray:cardArray totalintegral:totalIntegral buttonblock:^(NSInteger buttonIndex) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.alertView dismissWithCompletion:nil];
        if (buttonIndex == 0) { // 确认
            // 拼接描述
            NSString *descString = [NSString stringWithFormat:@"提示：您总共兑换了%ld B竹子",(long)totalIntegral];
            
            // 清空上传usedCard的数组
            [self.updateUsedCardArray removeAllObjects];
            
            // 根据usedCardModelArray数组中的数据改变 修改kCard数组中的元素
            for (PDUsedCard *usedCard in self.usedCardModelArray) {
                NSInteger usedCardNum = usedCard.card_num.integerValue;
                NSMutableDictionary *usedCardDic = [NSMutableDictionary dictionary];
                [usedCardDic setObject:[NSNumber numberWithInteger:usedCard.card_level.integerValue] forKey:@"card_level"];
                [usedCardDic setObject:[NSString stringWithFormat:@"%ld",(long)usedCardNum] forKey:@"card_num"];
                [self.updateUsedCardArray addObject:usedCardDic];
                
                for (PDKCard *kCard in self.kCards) {
                    NSInteger kCardNum = kCard.card_num.integerValue;
                    if ([kCard.card_level isEqualToString:usedCard.card_level]) {
                        if (kCardNum - usedCardNum == 0) {
                            [self.kCards removeObject:kCard];
                            break;
                        }
                        else {
                            kCardNum -= usedCardNum;
                            kCard.card_num = [NSString stringWithFormat:@"%ld",(long)kCardNum];
                        }
                    }
                }
//                if (self.usedCardModelArray.count == 1) {
//                    descString = [descString stringByAppendingFormat:@"%@张K%@卡共兑换了%ld根竹子",usedCard.card_num,usedCard.card_level,(long)totalIntegral];
//                } else {
//                    if ([[self.usedCardModelArray firstObject] isEqual:usedCard]) {
//                        descString = [descString stringByAppendingFormat:@"%@张K%@卡",usedCard.card_num,usedCard.card_level];
//                    } else if ([[self.usedCardModelArray lastObject] isEqual:usedCard]){
//                        descString = [descString stringByAppendingFormat:@"与%@张K%@卡共兑换了%ld根竹子",usedCard.card_num,usedCard.card_level,(long)totalIntegral];
//                    } else {
//                        descString = [descString stringByAppendingFormat:@"与%@张K%@卡",usedCard.card_num,usedCard.card_level];
//                    }
//                }
            }
            
            
            // 修改服务器端的数据
            PDGetRewardModel *fetchModel = [[PDGetRewardModel alloc] init];
            NSArray *data = [self.updateUsedCardArray copy];
            NSError *error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:data options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            fetchModel.requestParams = @{@"data":jsonString};
            [fetchModel fetchWithPath:kIntegral_getReward completionHandler:^(BOOL isSucceeded, NSError *error) {
                if (isSucceeded) {
                    
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [PDHUD dismiss];
                        PDAlertShowView *showView = [[PDAlertShowView alloc]init];
                        [showView alertForAlertViewType:PDAlertShowViewTypeSuccess withTitle:@"兑换成功" desc:descString];
                        
                        strongSelf.alertView  = [[JCAlertView alloc] initWithCustomView:showView dismissWhenTouchedBackground:YES];
                        [strongSelf.alertView show];
                        strongSelf.myBambooLabel.text = [NSString stringWithFormat:@"我的竹子：%@ B",fetchModel.points];
                    });

                } else {
                    [PDHUD dismiss];
                    [PDTool showNetErrorString:nil];
                }
            }];
            
            [PDHUD showWithStatus:@"兑换中..." maskType:WSProgressHUDMaskTypeDefault delay:0];
        } else {// 取消
            [self cardsForInitWithAnimation:NO];
        }
        [strongSelf.usedCardModelArray removeAllObjects];
        // 清空数组
        [strongSelf.usedCardArray removeAllObjects];
    }];
    self.alertView = [[JCAlertView alloc] initWithCustomView:showView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

#pragma mark - K卡排布
- (void)kCardPlaceSetting {
    for (int y = 0; y < 5; y++) {
        for (int x = 0; x <= y; x++) {
            if (y == 4) {
                UIImageView *cardImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth-CARD_WIDTH)/2, HSFloat(60)+SPACE+CARD_MARGIN+y*CARD_ROW_HEIGHT, CARD_WIDTH, CARD_HEIGHT)];
                cardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_kCard_bg%i%i",y,x]];
                [self.view addSubview:cardImageView];
                [self.pointArray addObject:[NSValue valueWithCGPoint:cardImageView.frame.origin]];
                break;
            }
            UIImageView *cardImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenWidth-(CARD_WIDTH+(CARD_HORI_SPACE+CARD_WIDTH)*y))/2+x*(CARD_WIDTH+CARD_HORI_SPACE),HSFloat(60)+SPACE+CARD_MARGIN+y*CARD_ROW_HEIGHT, CARD_WIDTH, CARD_HEIGHT)];
            cardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_kCard_bg%i%i",y,x]];
            [self.view addSubview:cardImageView];
            [self.pointArray addObject:[NSValue valueWithCGPoint:cardImageView.frame.origin]];
        }
    }
    // 999 8 9 5 6 7 1 2 3 4 0
    // 999 9 8 7 6 5 4 3 2 1 0
    NSValue *value = [[NSValue alloc] init];
    // 9 8 对换
    value = self.pointArray[1];
    self.pointArray[1] = self.pointArray[2];
    self.pointArray[2] = value;
    // 5 6 对换
    value = self.pointArray[5];
    self.pointArray[5] = self.pointArray[3];
    self.pointArray[3] = value;
    // 4 1，3 2 兑换
    value = self.pointArray[9];
    self.pointArray[9] = self.pointArray[6];
    self.pointArray[6] = value;
    value = self.pointArray[8];
    self.pointArray[8] = self.pointArray[7];
    self.pointArray[7] = value;
    
    
    for (int i = 0; i < self.pointArray.count; i++) {
        if (i == 0) {
            [self.pointDic setObject:self.pointArray[i] forKey:[NSString stringWithFormat:@"%ld",(long)kKingCardLevel]]; //这张是王卡
        } else {
            [self.pointDic setObject:self.pointArray[i] forKey:[NSString stringWithFormat:@"%i",10-i]];
        }
    }
}

#pragma mark - 生成用户卡组的位置
- (void)kCardForUserWithAnimation:(BOOL)animation {
    for (int i = 0;i < self.kCards.count;i++) {
        PDKCard *kCard = self.kCards[i];
        NSInteger level = kCard.card_level.integerValue;
        NSInteger amount = kCard.card_num.integerValue;
        NSValue *pointValue = [self.pointDic objectForKey:[NSString stringWithFormat:@"%ld",(long)level]];
        CGPoint origin = [pointValue CGPointValue];
        
        UIImageView *useableImageView = [[UIImageView alloc] initWithFrame:CGRectMake(origin.x, origin.y, CARD_WIDTH, CARD_HEIGHT)];
        useableImageView.alpha = animation ? 0 : 1; // 有动画就是0
        // level 可以是0～9，999
        useableImageView.tag = level+tagNormal;
        useableImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%ld_nor",(long)level]];
        [self.view addSubview:useableImageView];
        [self.useableImageViewArray addObject:useableImageView];
        useableImageView.userInteractionEnabled = YES;
        UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panWithCard:)];
        [useableImageView addGestureRecognizer:pan];
        
        UIImageView *amountBgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(origin.x+CARD_WIDTH-LCFloat(14), origin.y+CARD_HEIGHT-LCFloat(18), LCFloat(20), LCFloat(20))];
        amountBgImageView.tag = useableImageView.tag+tagAmountBg;
        amountBgImageView.transform = animation ? CGAffineTransformMakeScale(0, 0) :  CGAffineTransformMakeScale(1, 1);
        amountBgImageView.image = [UIImage imageNamed:@"challenge_kCard_number"];
         [self.view addSubview:amountBgImageView];// *
        [self.amountBgImageViewArray addObject:amountBgImageView];
        
        UILabel *amountLabel = [[UILabel alloc] initWithFrame:amountBgImageView.bounds];
        amountLabel.transform = animation ? CGAffineTransformMakeScale(0, 0) :  CGAffineTransformMakeScale(1, 1);
        amountLabel.font = [UIFont systemFontOfCustomeSize:10];
        amountLabel.tag = useableImageView.tag+tagLabel;
        amountLabel.text = [NSString stringWithFormat:@"x%ld",(long)amount];
        amountLabel.textAlignment = NSTextAlignmentCenter;
        amountLabel.textColor = [UIColor whiteColor];
        amountLabel.backgroundColor = [UIColor clearColor];
        amountLabel.adjustsFontSizeToFitWidth = YES;
        [amountBgImageView addSubview:amountLabel];
        [self.labelDic setObject:amountLabel forKey:kCard.card_level];
        [self numForLabel:amountLabel];
        [self.amountArray addObject:amountLabel];
        if (animation) {
            [UIView animateWithDuration:.6 delay:i*.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
                useableImageView.alpha = 1;
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:.5 delay:0 usingSpringWithDamping:.6 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                    amountBgImageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
                    amountLabel.transform = CGAffineTransformMakeScale(1.0, 1.0);
                } completion:^(BOOL finished) {
                    
                }];
            }];
        }
    }
}

#pragma mark - gestureRecongnizer
- (void)panWithCard:(UIPanGestureRecognizer *)pan {
    UIImageView *useableImageView = (UIImageView *)pan.view;
    useableImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_selected",(long)(useableImageView.tag-tagNormal)]];
    UILabel *label = [self.labelDic objectForKey:[NSString stringWithFormat:@"%ld",(long)(useableImageView.tag-tagNormal)]];
    [self.view bringSubviewToFront:label.superview];
    [self.view bringSubviewToFront:useableImageView]; // *
    
    
    if (pan.state == UIGestureRecognizerStateBegan) {
        // 这边K0、K1、K10卡在移动的时候按钮 不要变色。并且它不能被拿来更换
        if (self.isPromoter ? (useableImageView.tag != 1+tagNormal && useableImageView.tag != tagNormal+kKingCardLevel): (useableImageView.tag != tagNormal && useableImageView.tag != 1+tagNormal && useableImageView.tag != tagNormal+kKingCardLevel)) {
            // 按钮变色变大
            [self.rewardButton setBackgroundImage:[UIImage imageNamed:@"icon_rewardBtn_selected"] forState:UIControlStateNormal];
            self.rewardButton.transform = CGAffineTransformMakeScale(1.1, 1.1);
        }
        
        
        // 获取触电的位置
        CGPoint beginPoint;
        beginPoint = [pan locationInView:self.view];
        [self textForLabel:label isRestore:NO];
 //此处要做一个判断 判断卡片剩余数量 > 0?
        if ([self numForLabel:label] > 0) {
            // 复用图片
            UIImageView *reuseImageView = [[UIImageView alloc] initWithFrame:useableImageView.frame];
            reuseImageView.tag = useableImageView.tag;
            reuseImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)(reuseImageView.tag-tagNormal)]];
            reuseImageView.userInteractionEnabled = YES;
            [self.view insertSubview:reuseImageView belowSubview:useableImageView];// *
            [self.reuseImageViewArray addObject:reuseImageView];
            
        }
        // 拖拽的时候放大
        useableImageView.transform = CGAffineTransformMakeScale(1.2, 1.2);
    }
    if (pan.state == UIGestureRecognizerStateEnded) {
        [self.view bringSubviewToFront:label.superview];// *
        // 如果说是特别卡 直接回去
        if (self.isPromoter ? (useableImageView.tag == 1+tagNormal || useableImageView.tag == tagNormal+kKingCardLevel) : (useableImageView.tag == tagNormal || useableImageView.tag == 1+tagNormal || useableImageView.tag == tagNormal+kKingCardLevel)) {
            // 否则就把卡放回原处
            useableImageView.transform = CGAffineTransformIdentity;
            useableImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)(useableImageView.tag-tagNormal)]];
            // 并且把标签值改回来
            [self textForLabel:label isRestore:YES];
            // 释放复用图片
            if ([self numForLabel:label] > 1) {
                UIImageView *reuseImageView = (UIImageView *)[self.view viewWithTag:useableImageView.tag];
                [self.reuseImageViewArray removeObject:reuseImageView];
                [reuseImageView removeFromSuperview];
            }
            return;
        }
        
        
        // 取到手势放开的时候的那个点 判断卡的中心店的y是否>=领赏按钮中心店的y
        CGPoint endPoint = [pan locationInView:self.view];
        
        // 当卡片的底边y与底部按钮视图的上边想接触的时候
        CGFloat kCardMaxY = CGRectGetMaxY(useableImageView.frame);
        if (kCardMaxY >= kScreenHeight - 64 - Bottom_ButtonView_Height) {
            NSInteger level = useableImageView.tag-tagNormal; // 取到卡的等级
// 准从从小到大排序的原则
            NSInteger count = self.usedCardArray.count;
            if (count == 0) {
                // 记录用掉了那一张卡
                [self.usedCardArray addObject:[NSString stringWithFormat:@"%ld",(long)level]];
            } else {
                for (int i = 0 ; i < self.usedCardArray.count; i++) {
                    if (i == self.usedCardArray.count - 1) { // 最后一位
                        if ([self.usedCardArray[i] integerValue] <= level) {
                            [self.usedCardArray addObject:[NSString stringWithFormat:@"%ld",(long)level]];
                        } else {
                            [self.usedCardArray insertObject:[NSString stringWithFormat:@"%ld",(long)level] atIndex:i];
                        }
                        break;
                    }
                    if ([self.usedCardArray[i] integerValue] >= level) {
                        [self.usedCardArray insertObject:[NSString stringWithFormat:@"%ld",(long)level] atIndex:i];
                        break;
                    }
                }
            }
            useableImageView.center = CGPointMake(endPoint.x, endPoint.y-20);
            [UIView animateWithDuration:.6 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
                useableImageView.center = CGPointMake(self.view.center.x, kScreenHeight-40-64);
                useableImageView.transform = CGAffineTransformMakeScale(0.3, 0.3);
                [useableImageView removeGestureRecognizer:pan];
            } completion:^(BOOL finished) {
                if (finished) {
                    [useableImageView removeFromSuperview];
                    [self.useableImageViewArray removeObject:useableImageView];
                    
                    UIImageView *reuseImageView = [self.view viewWithTag:tagNormal+level];
                    UIPanGestureRecognizer *pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panWithCard:)];
                    [reuseImageView addGestureRecognizer:pan];
                    
                    // 按钮便会原装
                    [self.rewardButton setBackgroundImage:[UIImage imageNamed:@"icon_rewardBtn"] forState:UIControlStateNormal];
                    self.rewardButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
                    
                    [self showIntegralLabelForKCardPoint:[self.integralDic objectForKey:[NSString stringWithFormat:@"%li",(long)(useableImageView.tag-tagNormal)]]];
                }
            }];
        } else {
            // 按钮变回原状
            [self.rewardButton setBackgroundImage:[UIImage imageNamed:@"icon_rewardBtn"] forState:UIControlStateNormal];
            self.rewardButton.transform = CGAffineTransformMakeScale(1.0, 1.0);
            
            // 否则就把卡放回原处
            useableImageView.transform = CGAffineTransformIdentity;
            useableImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)(useableImageView.tag-tagNormal)]];
            // 并且把标签值改回来
            [self textForLabel:label isRestore:YES];
            // 释放复用图片
            if ([self numForLabel:label] > 1) {
                UIImageView *reuseImageView = (UIImageView *)[self.view viewWithTag:useableImageView.tag];
                [reuseImageView removeFromSuperview];
            }
        }
    }
    CGPoint point = [pan translationInView:useableImageView];
    CGFloat x_space = point.x;
    CGFloat y_space = point.y;
    useableImageView.transform = CGAffineTransformTranslate(useableImageView.transform, x_space, y_space);
    [pan setTranslation:CGPointZero inView:self.view];
}

#pragma mark - 弹起积分显示的Label
- (void)showIntegralLabelForKCardPoint:(NSString *)point {
    UILabel *label = [[UILabel alloc] init];
    label.backgroundColor = [UIColor clearColor];
    label.text = [NSString stringWithFormat:@"+%@",point];
    label.textColor = [UIColor colorWithCustomerName:@"绿"];
    label.font = [UIFont systemFontOfCustomeSize:15];
    CGFloat height = [NSString contentofHeightWithFont:label.font];
    CGSize size = [label.text sizeWithCalcFont:label.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, height)];
    label.frame = CGRectMake((kScreenWidth-size.width)/2, kScreenHeight, size.width, height);
    [self.view addSubview:label];
    
    __block CGRect frame = label.frame;
    [UIView animateWithDuration:1.0 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        frame.origin.y -= 250;
        label.frame = frame;
        label.transform = CGAffineTransformMakeScale(2., 2.);
    } completion:^(BOOL finished) {
        [label removeFromSuperview];
    }];
}



#pragma mark - 改变label的状态
// restore 是否是还原状态
- (void)textForLabel:(UILabel *)label isRestore:(BOOL)restore {
    NSInteger num = [self numForLabel:label];
    if (restore) {
        label.text = [NSString stringWithFormat:@"x%li",(long)(num+1)];
        if (num + 1 == 2) {
            label.hidden = NO;
            label.superview.hidden = NO;
        }
    } else {
        label.text = [NSString stringWithFormat:@"x%li",(long)(num-1)];
        if (num - 1 == 1) {
            label.hidden = YES;
            label.superview.hidden = YES;
        }
    }
}

- (NSInteger)numForLabel:(UILabel *)label {
    NSString *text = label.text;
    NSInteger num = [text substringFromIndex:1].integerValue;
    if (num == 1) {
        label.superview.hidden = YES;
        label.hidden = YES;
    }
    return num;
}


@end
