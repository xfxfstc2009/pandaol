//
//  PDExchangeViewController.h
//  PandaChallenge
//
//  Created by 盼达 on 16/3/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDExchangeViewController : AbstractViewController
@property (assign, nonatomic) NSInteger netId;
@end
