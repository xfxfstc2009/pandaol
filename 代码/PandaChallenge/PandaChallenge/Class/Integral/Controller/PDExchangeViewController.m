//
//  PDExchangeViewController.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDExchangeViewController.h"
#import "PDExchangeTableView.h"
#import "PDNetModel.h"
#import "PDMyPointInfoModel.h"
#import "PDExchangeVerificationModel.h"
#import "PDChallengeViewController.h"
#import "PDNavigationController.h"

@interface PDExchangeViewController ()<PDExchangeTableViewDelegate>
@property (nonatomic, assign) NSInteger integral;
@property (nonatomic, assign) NSInteger money;
/**后台返回验证码*/
@property (nonatomic, strong) NSString *vCode;
@property (nonatomic, strong) PDExchangeTableView *changeTableView;
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UILabel *myBambooLabel;
@property (nonatomic, strong) UILabel *changLabel;
@property (nonatomic, strong) PDNetModel *netModel;
@property (nonatomic, strong) JCAlertView *alertView;
@end

@implementation PDExchangeViewController

- (void)dealloc {
    [NOTIFICENTER removeObserver:self];
    
    NSLog(@"%s 被释放啦",__func__);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createHeaderView];
    [self createTableView];
    [self pageSetting];
    
    UIImage *backImage = [UIImage imageNamed:@"basc_nav_back"];
    backImage = [backImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:backImage style:UIBarButtonItemStyleDone target:self action:@selector(backAction)];
    
    
}

- (void)backAction
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [NOTIFICENTER addObserver:self selector:@selector(openKeyboard:) name:@"UIKeyboardWillShowNotification" object:nil];
    [NOTIFICENTER addObserver:self selector:@selector(closeKeyboard:) name:@"UIKeyboardWillHideNotification" object:nil];
    // 获取信息
    [self getData];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [PDHUD dismiss];
}

- (void)pageSetting {
    self.barMainTitle = @"领赏";
}

#pragma mark - 获取用户积分 最大换取人名币 网吧信息
- (void)getData {
    [PDHUD showWithStatus:@"正在获取信息中..." maskType:WSProgressHUDMaskTypeDefault delay:0];
    
    PDNetModel *netModel = [[PDNetModel alloc] init];
    self.netModel = netModel;
    netModel.requestParams = @{@"id":@(self.netId)};
    [netModel fetchWithPath:kIntegral_getCybercafeInfo completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        if (isSucceeded) {
            self.changeTableView.netModel = netModel;
        } else {
            [PDTool showNetErrorString:nil];
        }
    }];
    
    PDMyPointInfoModel *myPointModel = [[PDMyPointInfoModel alloc] init];
    [myPointModel fetchWithPath:kIntegral_getMycoupon completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (isSucceeded) {
            self.myBambooLabel.text = [NSString stringWithFormat:@"我的竹子：%@ B",myPointModel.myPoints];
            self.changLabel.text    = [NSString stringWithFormat:@"最高兑换 %@ R",myPointModel.RMB];
            self.money = myPointModel.RMB.integerValue;
        } else {
            [PDTool showNetErrorString:nil];
        }
    }];
}

#pragma mark - 头部视图

- (void)createHeaderView {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, HSFloat(160))];
    self.headerView = headerView;
    headerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:headerView];
    
// my bamboo
    UILabel *myBambooLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, (CGRectGetHeight(headerView.frame)-HSFloat(50))/2, kScreenWidth, HSFloat(25))];
    self.myBambooLabel = myBambooLabel;
    myBambooLabel.backgroundColor = [UIColor clearColor];
    myBambooLabel.textAlignment = NSTextAlignmentCenter;
    myBambooLabel.font = [[UIFont systemFontOfCustomeSize:20] boldFont];
    myBambooLabel.textColor = [UIColor blackColor];
    myBambooLabel.text = [NSString stringWithFormat:@"我的竹子：0 B"];
    [headerView addSubview:myBambooLabel];
    
// can change how match RMB
    UILabel *changeLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(myBambooLabel.frame), kScreenWidth, HSFloat(25))];
    self.changLabel = changeLabel;
    changeLabel.backgroundColor = [UIColor clearColor];
    changeLabel.textAlignment = NSTextAlignmentCenter;
    changeLabel.font = [[UIFont systemFontOfCustomeSize:14] boldFont];
    changeLabel.textColor = [UIColor lightGrayColor];
    changeLabel.text = [NSString stringWithFormat:@"最高兑换 0 R"];
    [headerView addSubview:changeLabel];
}

#pragma mark - 兑换界面表

- (void)createTableView {
    self.changeTableView = [[PDExchangeTableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.headerView.frame), kScreenWidth, kScreenHeight - 64 - CGRectGetHeight(self.headerView.frame))];
    self.changeTableView.backgroundColor = [UIColor whiteColor];
    self.changeTableView.tableViewDelegate = self;
    [self.view addSubview:self.changeTableView];
    
    [self sendVerification];
}

#pragma mark - 发送验证码
- (void)sendVerification {
    __weak typeof(self) weakSelf = self;
    self.changeTableView.block = ^(){
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [PDHUD showShimmeringString:@"验证码已发送" afterDelay:1.];
        NSString *phone = [PDLoginModel shareInstance].phone;
        PDExchangeVerificationModel *fetchModel = [[PDExchangeVerificationModel alloc] init];
        fetchModel.requestParams = @{@"phone":phone};
        [fetchModel fetchWithPath:kIntegral_cvcode completionHandler:^(BOOL isSucceeded, NSError *error) {
            if (isSucceeded) {
                strongSelf.vCode = fetchModel.vCode;
            } else {
                [PDTool showNetErrorString:nil];
            }
        }];
    };
}

#pragma mark - tableViewDelegate
- (void)tableView:(PDExchangeTableView *)tableView didClickButton:(UIButton *)button {
    if ([button.stringTag isEqualToString:@"确定"]) {
        // 验证码
        // RMB
        
        // 没有兑换金额
        if (self.changeTableView.slider.value == 0) {
            [PDHUD dismiss];
            [PDHUD showShimmeringString:@"您未选择任何金额" afterDelay:1.5];
            return;
        }
        // 先判断验证码是否与后台的相同
        if (![self.changeTableView.verificationTextField.text isEqualToString:self.vCode]) {
            [PDHUD dismiss];
            [PDHUD showShimmeringString:@"验证码错误" afterDelay:1.5];
        } else {
            [PDHUD dismiss];
            [PDHUD showWithStatus:@"正在验证..." maskType:WSProgressHUDMaskTypeDefault delay:0];
            NSInteger vCode = self.changeTableView.verificationTextField.text.integerValue;
            NSInteger price = (NSInteger)self.changeTableView.slider.value;
            PDMyPointInfoModel *fetchModel = [[PDMyPointInfoModel alloc] init];
            fetchModel.requestParams = @{@"vcode":@(vCode),@"price":@(price),@"partner_id":@(self.netId)};
            __weak typeof(self) weakSelf = self;
            [fetchModel fetchWithPath:kIntegral_account completionHandler:^(BOOL isSucceeded, NSError *error) {
                [PDHUD dismiss];
                if (!weakSelf) {
                    return ;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (isSucceeded) {
                    strongSelf.myBambooLabel.text = [NSString stringWithFormat:@"我的竹子：%@ B",fetchModel.myPoints];
                    strongSelf.changLabel.text = [NSString stringWithFormat:@"最高兑换 %@ R",fetchModel.RMB];
                    
                    strongSelf.changeTableView.verificationTextField.text = @"";
            
                    [self showExchangedAlertViewWithPrice:price];
                } else {
                    [PDTool showNetErrorString:nil];
                }
            }];
        }
    }
//    if ([button.stringTag isEqualToString:@"重置"]) {
//        // 兑换金额变成0
//        // slider变回原样
//        self.changeTableView.moneyLabel.text = @"0";
//        self.changeTableView.slider.value = 0.f;
//        self.changeTableView.verificationTextField.text = @"";
//    }
}

- (void)tableView:(PDExchangeTableView *)tableView sliderChange:(UISlider *)slider {
    if (slider.value != 0) {
        self.changeTableView.timerButton.enabled = YES;
    } else {
        self.changeTableView.timerButton.enabled = NO;
    }
    if (slider.value >= self.money) {
        slider.value = self.money;
        [PDHUD showShimmeringString:@"您不能更换更多网费" afterDelay:1.5];
    }
}

- (void)tableView:(PDExchangeTableView *)tableView didEditTextField:(UITextField *)textField {
    if (textField.text.length >= 5) {
        textField.text = [textField.text substringToIndex:5];
    }
}


#pragma mark - 键盘通知方法
- (void)openKeyboard:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    int option = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
    __block CGPoint center = self.changeTableView.center;
    [UIView animateKeyframesWithDuration:duration delay:0 options:option animations:^{
        center.y -= keyboardRect.size.height;
        self.changeTableView.center = center;
    } completion:nil];
}

- (void)closeKeyboard:(NSNotification *)notification {
    CGRect keyboardRect = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    NSTimeInterval duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    int option = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
    __block CGPoint center = self.changeTableView.center;
    [UIView animateKeyframesWithDuration:duration delay:0 options:option animations:^{
        center.y += keyboardRect.size.height;
        self.changeTableView.center = center;
    } completion:nil];
}

#pragma mark - alertView
- (void)showExchangedAlertViewWithPrice:(NSInteger)price {
    __weak typeof(self) weakSelf = self;
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    [showView alertWithTitle:@"兑换成功" headerImage:[UIImage imageNamed:@"icon_report_success"] changeRMB:price netInfo:self.netModel desc:[NSString stringWithFormat:@"提示：您在“NO.%@”网咖兑换了%ld元网费，请及时与网吧人员联系",self.netModel.netId,(long)price] buttonNameArray:@[@"我的赛事",@"继续兑换"] clickBlock:^(NSInteger buttonIndex) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.alertView dismissWithCompletion:nil];
        if (buttonIndex == 0) {
            PDChallengeViewController *challengeVC = [[PDChallengeViewController alloc] init];
            PDNavigationController *navigationController = [[PDNavigationController alloc]initWithRootViewController:challengeVC];
            [strongSelf.sideMenuViewController setContentViewController:navigationController animated:YES];
        } else {
            
        }
    }];
    self.alertView = [[JCAlertView alloc] initWithCustomView:showView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}
@end
