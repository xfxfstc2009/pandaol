//
//  PDExchangeTableView.h
//  PandaChallenge
//
//  Created by 盼达 on 16/3/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDNetModel.h"
#import "PDTimerButton.h"

@class PDExchangeTableView;
@protocol PDExchangeTableViewDelegate <NSObject>
- (void)tableView:(PDExchangeTableView *)tableView didClickButton:(UIButton *)button;
- (void)tableView:(PDExchangeTableView *)tableView sliderChange:(UISlider *)slider;
- (void)tableView:(PDExchangeTableView *)tableView didEditTextField:(UITextField *)textField;
@end

typedef void(^buttonClickBlock)();

@interface PDExchangeTableView : UITableView
@property (nonatomic, strong) PDNetModel *netModel;
@property (nonatomic, strong) buttonClickBlock block;
@property (nonatomic, weak)   id<PDExchangeTableViewDelegate> tableViewDelegate;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, strong) UILabel *moneyLabel;
@property (nonatomic, strong) UITextField *verificationTextField;
@property (nonatomic, strong) PDTimerButton *timerButton;
@end
