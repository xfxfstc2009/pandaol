//
//  PDExchangeInfoView.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDExchangeInfoView : UIView
@property (nonatomic, strong) UILabel *moneyLabel;
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UILabel *numLabel;
@end
