//
//  PDExchangeInfoView.m
//  PandaChallenge
//
//  Created by 盼达 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDExchangeInfoView.h"

@implementation PDExchangeInfoView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self createMainViewWithFrame:(CGRect)frame];
    }
    return self;
}

- (void)createMainViewWithFrame:(CGRect)frame {
    // 底层
    UIImageView *bottomImageView = [[UIImageView alloc] init];
    bottomImageView.frame = self.bounds;
    bottomImageView.image = [UIImage imageNamed:@"icon_change_bg"];
    [self addSubview:bottomImageView];
    
    // 左边
    UIView *ticketImageView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, LCFloat(100), CGRectGetHeight(frame))];
    ticketImageView.backgroundColor = [UIColor clearColor];
    [bottomImageView addSubview:ticketImageView];
    CGFloat right_x = ticketImageView.frame.size.width;
    CGFloat right_width = frame.size.width - ticketImageView.frame.size.width;
    
    // 兑换券上的数字
    UILabel *moneyLabel = [[UILabel alloc] init];
    self.moneyLabel = moneyLabel;
    moneyLabel.backgroundColor = [UIColor clearColor];
    moneyLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    moneyLabel.frame = CGRectMake(0, HSFloat(28), CGRectGetWidth(ticketImageView.frame), [NSString contentofHeightWithFont:moneyLabel.font]);
    moneyLabel.text = @"0";
    moneyLabel.textColor = [UIColor whiteColor];
    moneyLabel.textAlignment = NSTextAlignmentCenter;
    [ticketImageView addSubview:moneyLabel];
    
    // 右边
    CGFloat headerImageViewWidth = LCFloat(40); // 图片边
    PDImageView *headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake((right_width - headerImageViewWidth)/2 + right_x, HSFloat(10), headerImageViewWidth, headerImageViewWidth)];
    self.headerImageView = headerImageView;
    headerImageView.image = [UIImage imageNamed:@"icon_net_header"];
    headerImageView.layer.cornerRadius = CGRectGetWidth(headerImageView.frame)/2;
    headerImageView.clipsToBounds = YES;
    [bottomImageView addSubview:headerImageView];
    
    // name
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(right_x, CGRectGetMaxY(headerImageView.frame), right_width, HSFloat(30))];
    self.nameLabel = nameLabel;
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.textAlignment = NSTextAlignmentCenter;
    nameLabel.textColor = [UIColor redColor];
    nameLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
    [bottomImageView addSubview:nameLabel];
    
    // address
    UILabel *addressLabel = [[UILabel alloc] initWithFrame:CGRectMake(right_x, CGRectGetMaxY(nameLabel.frame)+HSFloat(8), right_width, HSFloat(20))];
    self.addressLabel = addressLabel;
    addressLabel.backgroundColor = [UIColor clearColor];
    addressLabel.textAlignment = NSTextAlignmentCenter;
    addressLabel.textColor = [UIColor redColor];
    addressLabel.font = [[UIFont systemFontOfCustomeSize:12] boldFont];
    [bottomImageView addSubview:addressLabel];
    
    // phone
    UILabel *phoneLabel = [[UILabel alloc] initWithFrame:CGRectMake(right_x, CGRectGetMaxY(addressLabel.frame), right_width, HSFloat(20))];
    self.phoneLabel = phoneLabel;
    phoneLabel.backgroundColor = [UIColor clearColor];
    phoneLabel.textAlignment = NSTextAlignmentCenter;
    phoneLabel.textColor = [UIColor redColor];
    phoneLabel.font = [[UIFont systemFontOfCustomeSize:12] boldFont];
    [bottomImageView addSubview:phoneLabel];
    
    // NO.
    UILabel *numLabel = [[UILabel alloc] initWithFrame:CGRectMake(right_x, 0, right_width-HSFloat(2), HSFloat(20))];
    self.numLabel = numLabel;
    numLabel.backgroundColor = [UIColor clearColor];
    numLabel.textAlignment = NSTextAlignmentRight;
    numLabel.textColor = [UIColor redColor];
    numLabel.font = [[UIFont systemFontOfCustomeSize:12] boldFont];
    [bottomImageView addSubview:numLabel];
}

@end
