//
//  PDExchangeTableView.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDExchangeTableView.h"
#import "PDExchangeInfoView.h"

#define Fix_Button_Width  LCFloat(280) // 确定按钮的宽
#define Fix_Button_Height HSFloat(44)
#define Fix_Space  ((kScreenWidth-Fix_Button_Width)/2) // 确定按钮左定点距离左边的距离
#define Verification_Button_Width LCFloat(80)
#define Slider_Button_Width   HSFloat(20)

#define Table_Row_Num     5 // 表的层数

@interface PDExchangeTableView ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *addressLabel;
@property (nonatomic, strong) UILabel *phoneLabel;
@property (nonatomic, strong) UILabel *numLabel;
// 兑换
@property (nonatomic, strong) NSArray *changeImages;

@end

@implementation PDExchangeTableView
- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        self.dataSource = self;
        self.backgroundColor = [UIColor whiteColor];
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
    return self;
}

#pragma mark - tableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return Table_Row_Num;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"changeCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.backgroundColor = [UIColor clearColor];
    switch (indexPath.row) {
        case 0:
        {
            PDExchangeInfoView *exchangeInfoView = [[PDExchangeInfoView alloc] initWithFrame:CGRectMake(Fix_Space, HSFloat(20), Fix_Button_Width, HSFloat(140))];
            [cell.contentView addSubview:exchangeInfoView];
            self.moneyLabel = exchangeInfoView.moneyLabel;
            self.headerImageView = exchangeInfoView.headerImageView;
            self.nameLabel = exchangeInfoView.nameLabel;
            self.addressLabel = exchangeInfoView.addressLabel;
            self.phoneLabel = exchangeInfoView.phoneLabel;
            self.numLabel = exchangeInfoView.numLabel;
            
            break;
        }
        case 1:
        {
            // 左减按钮
            UIButton *reduceButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [reduceButton setBackgroundImage:[UIImage imageNamed:@"icon_button_reduce"] forState:UIControlStateNormal];
//            [reduceButton setImage:[UIImage imageNamed:@"icon_button_reduce"] forState:UIControlStateNormal];
            reduceButton.frame = CGRectMake(Fix_Space, HSFloat(10), Slider_Button_Width, Slider_Button_Width);
            [cell.contentView addSubview:reduceButton];
            __weak typeof(self) weakSelf = self;
            [reduceButton buttonWithBlock:^(UIButton *button) {
                if (weakSelf == nil) {
                    return ;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                strongSelf.slider.value -= 1.f;
                [strongSelf.slider setValue:strongSelf.slider.value animated:YES];
                if ([self.tableViewDelegate respondsToSelector:@selector(tableView:sliderChange:)]) {
                    [self.tableViewDelegate tableView:self sliderChange:strongSelf.slider];
                }
            }];
            
            // 滑动杆
            UISlider *slider = [[UISlider alloc] initWithFrame:CGRectMake(CGRectGetMaxX(reduceButton.frame)+LCFloat(5), HSFloat(8), Fix_Button_Width-(CGRectGetWidth(reduceButton.frame)+HSFloat(5))*2, HSFloat(25))];
            self.slider = slider;
            slider.value = 0.f;
            slider.minimumValue = 0.f;
            slider.maximumValue = 100.f;
            [slider setMinimumTrackImage:[UIImage imageNamed:@"icon_slider_front"] forState:UIControlStateNormal];
            [slider setMaximumTrackImage:[UIImage imageNamed:@"icon_slider_bg"] forState:UIControlStateNormal];
//            [slider setThumbImage:[UIImage imageNamed:@"icon_slider_thumb"] forState:UIControlStateNormal];// 设置滑轮的图片
            slider.continuous = YES;
            [cell.contentView addSubview:slider];
            [slider addTarget:self action:@selector(sliderValueChange:) forControlEvents:UIControlEventValueChanged];
            // 添加观察者
            [slider addObserver:self forKeyPath:@"value" options:NSKeyValueObservingOptionNew context:nil];
            
            // 右边加按钮
            UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [addButton setBackgroundImage:[UIImage imageNamed:@"icon_button_add"] forState:UIControlStateNormal];
//            [addButton setImage:[UIImage imageNamed:@"icon_button_add"] forState:UIControlStateNormal];
            addButton.frame = CGRectMake(CGRectGetMaxX(slider.frame)+LCFloat(5), HSFloat(10), Slider_Button_Width, Slider_Button_Width);
            [cell.contentView addSubview:addButton];
            [addButton buttonWithBlock:^(UIButton *button) {
                if (weakSelf == nil) {
                    return ;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                strongSelf.slider.value += 1.f;
                [strongSelf.slider setValue:strongSelf.slider.value animated:YES];
                if ([self.tableViewDelegate respondsToSelector:@selector(tableView:sliderChange:)]) {
                    [self.tableViewDelegate tableView:self sliderChange:strongSelf.slider];
                }
            }];
            
            break;
        }
        case 2:
        {
            //验证码
            UITextField *verificationTextField = [[UITextField alloc] initWithFrame:CGRectMake(Fix_Space+LCFloat(5), HSFloat(60-30), Fix_Button_Width-LCFloat(5)*2-Verification_Button_Width, HSFloat(30))];
            self.verificationTextField = verificationTextField;
            verificationTextField.placeholder = @"请输入验证码";
            verificationTextField.tintColor = [UIColor blackColor];
            verificationTextField.font = [UIFont systemFontOfCustomeSize:13];
            verificationTextField.textAlignment = NSTextAlignmentCenter;
            verificationTextField.borderStyle = UITextBorderStyleNone;
            verificationTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
            verificationTextField.keyboardType = UIKeyboardTypeNumberPad;
            [cell.contentView addSubview:verificationTextField];
            [verificationTextField addTarget:self action:@selector(editTextField:) forControlEvents:UIControlEventEditingChanged];
            
            // 线
            UIView *bottomLine = [[UIView alloc] initWithFrame:CGRectMake(verificationTextField.frame.origin.x, CGRectGetMaxY(verificationTextField.frame), verificationTextField.frame.size.width, .5)];
            bottomLine.backgroundColor = [UIColor lightGrayColor];
            [cell.contentView addSubview:bottomLine];
            
            // 验证码按钮
            PDTimerButton *button = [[PDTimerButton alloc] initWithTimerType:exchangeTimer actionBlock:self.block];
            self.timerButton = button;
            button.enabled = NO;
            button.isCanClickByPhone = YES;
            button.frame = CGRectMake(Fix_Button_Width+Fix_Space-Verification_Button_Width, verificationTextField.frame.origin.y, Verification_Button_Width, HSFloat(30));
            [cell.contentView addSubview:button];
            
            break;
        }
        case 4:
        {
            // button
            UIButton *sureButton = [self createButtonWithTitle:@"确定"];
            sureButton.stringTag = @"确定";
            sureButton.frame = CGRectMake(Fix_Space, 0, Fix_Button_Width, Fix_Button_Height);
            [cell.contentView addSubview:sureButton];
            sureButton.backgroundColor = [UIColor blackColor];
            [sureButton addTarget:self action:@selector(clickSure:) forControlEvents:UIControlEventTouchUpInside];
            break;
        }
        case 3:
        {
//            UIButton *cancleButton = [self createButtonWithTitle:@"重置"];
//            cancleButton.stringTag = @"重置";
//            cancleButton.frame = CGRectMake(Fix_Space, 0, Fix_Button_Width, Fix_Button_Height);
//            [cell.contentView addSubview:cancleButton];
//            cancleButton.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6];
//            [cancleButton addTarget:self action:@selector(clickCancle:) forControlEvents:UIControlEventTouchUpInside];
            break;
        }
    }
    }
    return cell;
}

#pragma mark UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            return HSFloat(170);
        case 1:
            return HSFloat(40);
        case 2:
            return HSFloat(60);
        case 3:
            return HSFloat(44);
        case 4:
            return HSFloat(50);
    }
    return 0;
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"value"] && [object isKindOfClass:[UISlider class]]) {
        self.moneyLabel.text = [NSString stringWithFormat:@"%.f",self.slider.value];
    }
}

#pragma mark - Delegate

- (void)sliderValueChange:(UISlider *)slider {
    self.moneyLabel.text = [NSString stringWithFormat:@"%.f",slider.value];
    if ([self.tableViewDelegate respondsToSelector:@selector(tableView:sliderChange:)]) {
        [self.tableViewDelegate tableView:self sliderChange:slider];
    }
}

- (void)clickSure:(UIButton *)button {
    [self endEditing:YES];
    if ([self.tableViewDelegate respondsToSelector:@selector(tableView:didClickButton:)]) {
        [self.tableViewDelegate tableView:self didClickButton:button];
    }
}

//- (void)clickCancle:(UIButton *)button {
//    [self endEditing:YES];
//    if ([self.tableViewDelegate respondsToSelector:@selector(tableView:didClickButton:)]) {
//        [self.tableViewDelegate tableView:self didClickButton:button];
//    }
//}

- (void)editTextField:(UITextField *)textField {
    if ([self.tableViewDelegate respondsToSelector:@selector(tableView:didEditTextField:)]) {
        [self.tableViewDelegate tableView:self didEditTextField:textField];
    }
}

#pragma mark - button and methods
- (UIButton *)createButtonWithTitle:(NSString *)title {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTintColor:[UIColor whiteColor]];
    button.layer.cornerRadius = 5;
    button.clipsToBounds = YES;
    [button setTitle:title forState:UIControlStateNormal];
    return button;
}

#pragma mark - 屏幕触控
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self endEditing:YES];
}

#pragma mark - model赋值
- (void)setNetModel:(PDNetModel *)netModel {
    _netModel = netModel;
    self.numLabel.text = [NSString stringWithFormat:@"NO.%@",_netModel.netId];
    self.nameLabel.text = _netModel.shop_name;
    self.addressLabel.text = _netModel.address;
    self.phoneLabel.text = _netModel.mobile;
    [self.headerImageView uploadImageWithURL:_netModel.avatar placeholder:[UIImage imageNamed:@"icon_net_header"] callback:nil];
}

- (void)dealloc {
    [self.slider removeObserver:self forKeyPath:@"value"];
}

@end
