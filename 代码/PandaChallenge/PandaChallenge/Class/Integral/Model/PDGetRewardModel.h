//
//  PDGetRewardModel.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDGetRewardModel : PDFetchModel
@property (nonatomic, strong) NSString *points;
@end
