//
//  PDNetModel.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNetModel.h"

@implementation PDNetModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"netId": @"id"};
}

@end
