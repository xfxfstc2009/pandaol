//
//  PDKCard.h
//  PandaChallenge
//
//  Created by 盼达 on 16/3/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PDKCard <NSObject>

@end

@interface PDKCard : PDFetchModel
/**卡数量*/
@property (nonatomic, strong) NSString *card_num;
/**卡等级*/
@property (nonatomic, strong) NSString *card_level;
/**卡兑换积分数*/
@property (nonatomic, strong) NSString *exchange_point;

@end
