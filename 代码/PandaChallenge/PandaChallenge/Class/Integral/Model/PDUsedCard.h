//
//  PDUsedCard.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDKCard.h"

// 记录用掉的卡的模型
@interface PDUsedCard : PDKCard

@end
