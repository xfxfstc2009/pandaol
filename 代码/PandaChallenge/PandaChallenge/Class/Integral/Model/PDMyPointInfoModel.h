//
//  PDMyPointInfoModel.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDMyPointInfoModel : PDFetchModel
@property (nonatomic, strong) NSString *myPoints;
@property (nonatomic, strong) NSString *RMB;
@end
