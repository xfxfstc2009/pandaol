//
//  PDExchangeVerificationModel.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDExchangeVerificationModel : PDFetchModel
@property (nonatomic, strong) NSString *vCode;
@end
