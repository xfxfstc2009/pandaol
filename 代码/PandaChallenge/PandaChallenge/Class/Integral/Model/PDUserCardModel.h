//
//  PDUserCardModel.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDKCard.h"

@interface PDUserCardModel : PDFetchModel
@property (nonatomic, strong) NSArray<PDKCard> *userCard;
/**积分*/
@property (nonatomic, strong) NSString *points;
@end
