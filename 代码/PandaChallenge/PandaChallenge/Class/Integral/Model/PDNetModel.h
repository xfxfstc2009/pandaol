//
//  PDNetModel.h
//  PandaChallenge
//
//  Created by 盼达 on 16/3/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDNetModel : PDFetchModel
@property (nonatomic, strong) NSString *avatar;
/**网吧ID*/
@property (nonatomic, strong) NSString *netId;
/**网吧名字*/
@property (nonatomic, strong) NSString *shop_name;
/**网吧地址*/
@property (nonatomic, strong) NSString *address;
/**网吧联系方式*/
@property (nonatomic, strong) NSString *mobile;
@end
