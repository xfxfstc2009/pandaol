//
//  PDProduct.h
//  PandaChallenge
//
//  Created by 盼达 on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDProduct : PDFetchModel
@property (nonatomic, strong) NSString *orderNum;

@property (nonatomic, strong) NSString *price;

@property (nonatomic, strong) NSString *total;

@end
