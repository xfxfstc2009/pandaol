//
//  PDPrepayidModel.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDPrepayidModel : PDFetchModel
@property (nonatomic, strong) NSString *trade_type;
@property (nonatomic, strong) NSString *prepay_id;
@end
