//
//  PDClubViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDClubViewController.h"
#import "PDAddGameViewController.h"

@interface PDClubViewController ()

@end

@implementation PDClubViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self loadUrlStr];
}

- (void)loadUrlStr {
    self.urlStr = kClubUrl_Club;
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([request.URL.absoluteString isEqualToString:KClubUrl_AddGame]) {
        if ([USERDEFAULTS boolForKey:UD_CLUB_CAN_SHOW]) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [USERDEFAULTS setBool:YES forKey:UD_CLUB_CAN_SHOW];
            PDAddGameViewController *addGameVC = [[PDAddGameViewController alloc] init];
            [self.navigationController pushViewController:addGameVC animated:YES];
        }
        return NO;
    } else
        return YES;
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [super webViewDidFinishLoad:webView];
    NSString *title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.barMainTitle = title;
}

@end
