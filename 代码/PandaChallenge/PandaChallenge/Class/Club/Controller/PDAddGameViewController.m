//
//  PDAddGameViewController.m
//  PandaChallenge
//
//  Created by 盼达 on 16/4/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAddGameViewController.h"
#import "PDPayViewController.h"
#import "PDClubViewController.h"
#import "PDChallengeDirectManager.h"

@interface PDAddGameViewController ()

@end

@implementation PDAddGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self configLeftButton];
    [self configRightButton];
    [self loadUrlStr];
}

- (void)pageSetting {
   
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [super webViewDidFinishLoad:webView];
    NSString *title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.barMainTitle = title;
}


- (void)configLeftButton {
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"basc_nav_back"] barHltImage:[UIImage imageNamed:@"basc_nav_back"] action:^{
        [PDChallengeDirectManager directToHomeViewController];
    }];
}

- (void)loadUrlStr {
    self.urlStr = KClubUrl_AddGame;
}

#pragma mark - 右上角协议按钮
// 刚进来的时候没有
// 第二次进来有
- (void)configRightButton {
    if ([USERDEFAULTS boolForKey:UD_CLUB_CAN_SHOW]) {
        __weak typeof(self) weakSelf = self;
        [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_club_rule"] barHltImage:[UIImage imageNamed:@"icon_club_rule"] action:^{
            if (!weakSelf) {
                return ;
            }
            __strong typeof(weakSelf) strongSelf = weakSelf;
            PDClubViewController *clubVC = [[PDClubViewController alloc] init];
            [strongSelf.navigationController pushViewController:clubVC animated:YES];
        }];
    }
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([request.URL.absoluteString isEqualToString:kClubUrl_Pay]) {
        PDPayViewController *payViewController = [[PDPayViewController alloc] init];
        [self.navigationController pushViewController:payViewController animated:YES];
        return NO;
    } else
        return YES;
}

@end
