//
//  PDPayViewController.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPayViewController.h"
#import "PDAliPayHandle.h"
#import "PDPayOrderModel.h"
#import "WXApi.h"
#import "PDProduct.h"
#import "payRequsestHandler.h"

#define PayConnectStr  @"http://www.baidu.com"
/**与后台约定的一个固定参数*/
static const NSInteger product_id = 4;

@interface PDPayViewController ()
@property (nonatomic, strong) JCAlertView *alertView;
/** 商品数量*/
@property (assign, nonatomic) NSInteger num;
//@property (copy,   nonatomic) NSString *msg;
@property (strong, nonatomic) PDPayOrderModel *order;
@property (copy,   nonatomic) NSString *payId;
/** 流水单号，微信支付的回调有时候会回调两次，用流水单号来控制*/
@property (copy,   nonatomic) NSString *tradeNO;
@end

@implementation PDPayViewController
- (void)dealloc {
    NSLog(@"%s 释放了",__func__);
    [NOTIFICENTER removeObserver:self name:WX_PAY_NOTIFICATION object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self addNotiFication];
    [self loadUrlStr];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [super webViewDidFinishLoad:webView];
    NSString *title = [self.webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.barMainTitle = title;
}


- (void)pageSetting {
    
}

- (void)loadUrlStr {
    self.urlStr = kClubUrl_Pay;
}

- (void)addNotiFication {
    [NOTIFICENTER addObserver:self selector:@selector(WXPayResult:) name:WX_PAY_NOTIFICATION object:nil];
}

#pragma mark - 通知
- (void)WXPayResult:(NSNotification *)notification {
    // 防止多次回调
    if ([self.tradeNO isEqualToString:@"result"]) {
        return;
    }
    self.tradeNO = @"result";
    if ([[notification.userInfo objectForKey:@"result"] isEqualToString:@"success"]) {
        // 支付成功
        [self paySuccessWithNum:self.num];
    } else {
        // 支付失败
        [self payFailWithMsg:nil order:self.order payId:self.payId];
    }
}

#pragma mark - 收到web跳转

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    NSString *htmlStr = [[[request URL] absoluteString] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"html %@",htmlStr);
    // 跳到百度界面执行了两次该方法
    // http://www.baidu.com/?id=0&pay=49&name=5
    // 截取http:开头的字符串
    if ([htmlStr rangeOfString:PayConnectStr].location != NSNotFound) {
        [self.webView stopLoading];
        
        [self showPayState];
        
        NSArray *strArray = [htmlStr componentsSeparatedByString:@"?"];
        NSString *mainString = [strArray objectAtIndex:1];//id=0&pay=49
        
        NSArray *payInfoArray = [mainString componentsSeparatedByString:@"&"];
        NSString *payId = [[payInfoArray objectAtIndex:0] substringFromIndex:3];
        self.payId = payId;
        NSString *pay = [[payInfoArray objectAtIndex:1] substringFromIndex:4];
        NSUInteger name = [[[payInfoArray objectAtIndex:2] substringFromIndex:5] integerValue];
        self.num = name;
        
        // 吊起支付订单
        PDPayOrderModel *order = [[PDPayOrderModel alloc] init];
        order.productName = [self nameForProductWithAmount:name];
        order.amount = pay;
        order.productDescription = [NSString stringWithFormat:@"您本次购买的是价值%@的%@",order.amount,order.productName];
        
        // 把order纪录下来 用在服务器返回的通知中
        self.order = order;
        
        if ([payId isEqualToString:@"0"]) { //支付宝
            [self aliPayWithOrder:order];
        } else {//WX
            // 判断用户有没有安装微信
            if ([WXApi isWXAppInstalled]) {
                if ([WXApi isWXAppSupportApi]) {
                    [self WXPayWithOrder:order];
                } else {
                    [[UIAlertView alertViewWithTitle:@"提示" message:@"您当前微信版本不支持微信支付，请先升级" buttonTitles:@[@"确定"] callBlock:nil] show];
                }
            } else {
                [[UIAlertView alertViewWithTitle:@"提示" message:@"您未安装微信，请先安装微信" buttonTitles:@[@"确定"] callBlock:nil] show];
            }
        }
    }
    return YES;
}

- (NSString *)nameForProductWithAmount:(NSUInteger)name {
    switch (name) {
        case 1:
            return @"K级联赛补赛一次";
//        case 2:
//            return @"K级联赛补赛二次";
//        case 3:
//            return @"K级联赛补赛三次";
//        case 4:
//            return @"K级联赛补赛四次";
        case 5:
            return @"K级联赛补赛五次";
//        case 6:
//            return @"K级联赛补赛六次";
//        case 7:
//            return @"K级联赛补赛七次";
//        case 8:
//            return @"K级联赛补赛八次";
//        case 9:
//            return @"K级联赛补赛九次";
        case 10:
            return @"K级联赛补赛十次";
    }
    return nil;
}

#pragma mark - 支付宝
//支付宝支付
/*
 9000 订单支付成功
 8000 正在处理中
 4000 订单支付失败
 6001 用户中途取消
 6002 网络连接出错
 */

- (void)aliPayWithOrder:(PDPayOrderModel *)order {
    // 1.给服务器传递商品id,数量,金额
    PDProduct *fectchModel = [[PDProduct alloc] init];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(product_id) forKey:@"product_id"];
    [params setObject:@(self.num) forKey:@"product_num"];
    [params setObject:@"panda" forKey:@"ali_token"];
    [params setObject:[PDLoginModel shareInstance].userId forKey:@"user_id"];
    fectchModel.requestParams = params;
    __weak typeof(self) weakSelf = self;
    [fectchModel fetchWithPath:kOrderRequest completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        //__strong typeof(weakSelf) strongSelf = weakSelf;
        [PDHUD dismiss];
        if (isSucceeded) {
            // 2.从服务器接收到订单后再吊起支付宝支付
            order.tradeNO = fectchModel.orderNum;
            [PDAliPayHandle payWithOrder:order block:^(NSInteger code, NSString *errMsg) {
                NSLog(@"pay result %li,%@",(long)code,errMsg);
                switch (code) {
                    case 9000:
                        [self paySuccessWithNum:self.num];
                        break;
                    default:
                        [self payFailWithMsg:nil order:order payId:@"0"];
                        break;
                }
            }];
        } else {
            [self payFailWithMsg:error.localizedDescription order:order payId:@"0"];
        }
    }];
}

#pragma mark - WXPay
- (void)WXPayWithOrder:(PDPayOrderModel *)order {
    // 向后台发消息请求订单号
    PDProduct *product = [[PDProduct alloc] init];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@(product_id) forKey:@"product_id"];
    [params setObject:@(self.num) forKey:@"product_num"];
    product.requestParams = params;
    __weak typeof(self) weakSelf = self;
    [product fetchWithPath:kWXOrderRequest completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (weakSelf == nil) {
            return;
        }
       // __strong typeof(weakSelf) strongSelf = self;
        [PDHUD dismiss];
        if (isSucceeded) {
            // 拿到流水单号，在回调的时候用来控制
            self.tradeNO = product.orderNum;
            
            order.tradeNO = product.orderNum;
            // 微信的金额是／100
            CGFloat wxAmount = order.amount.floatValue;
            order.amount = [NSString stringWithFormat:@"%.0f",wxAmount * 100];
            payRequsestHandler *payReq = [[payRequsestHandler alloc] init];
            [payReq init:WX_APP_ID mch_id:WX_MCH_ID];
            [payReq setKey:WX_PARTNER_ID];
            [payReq sendPayWithOrder:order];
        } else {
            [self payFailWithMsg:error.localizedDescription order:order payId:@"1"];
        }
    }];
}

#pragma mark 支付结果
- (void)paySuccessWithNum:(NSInteger)num {
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    [showView alertWithTitle:@"支付成功" headerImage:[UIImage   imageNamed:@"icon_report_success"] desc:[NSString stringWithFormat:@"您已获得%ld次初级级别赛事资格",(long)num]];
    JCAlertView *alertView = [[JCAlertView alloc] initWithCustomView:showView dismissWhenTouchedBackground:YES];
    [alertView show];
}

- (void)payFailWithMsg:(NSString *)msg order:(PDPayOrderModel *)order payId:(NSString *)payId {
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    __weak typeof(self) weakSelf = self;
    [showView alertHorizontalButtonWithTitle:@"支付失败" headerImage:[UIImage imageNamed:@"icon_report_warn"] desc:msg ? msg : @"请重试或选择其它他支付方式" buttonNameArray:@[@"重试",@"取消"] clickBlock:^(NSInteger index) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (index == 0) {
            [self showPayState];
            if ([payId isEqualToString:@"0"]) {
                [strongSelf aliPayWithOrder:order];
            } else {
                [strongSelf WXPayWithOrder:order];
            }
        } else {
            
        }
        [strongSelf.alertView dismissWithCompletion:nil];
    }];
    self.alertView = [[JCAlertView alloc] initWithCustomView:showView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

#pragma mark 支付HUD提示
- (void)showPayState {
    [PDHUD showWithStatus:@"正在下单，请稍候..." maskType:WSProgressHUDMaskTypeDefault delay:0];
}

@end
