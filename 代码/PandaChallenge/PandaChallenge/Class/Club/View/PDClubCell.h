//
//  PDClubCell.h
//  PandaChallenge
//
//  Created by 盼达 on 16/3/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDClubCell : UITableViewCell
@property (nonatomic, strong) NSString *imageName;

@property (nonatomic, strong) NSString *content;

@end
