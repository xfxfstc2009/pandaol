//
//  PDClubCell.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDClubCell.h"

@interface PDClubCell ()
@property (nonatomic, strong) UIImageView *iconView;

@property (nonatomic, strong) UILabel *label;
@end

@implementation PDClubCell

- (UIImageView *)iconView {
	if(_iconView == nil) {
		_iconView = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconView];
        [_iconView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.right.mas_equalTo(LCFloat(40));
            make.size.mas_equalTo(CGSizeMake(30, 30));
        }];
	}
	return _iconView;
}

- (UILabel *)label {
	if(_label == nil) {
		_label = [[UILabel alloc] init];
        _label.textColor = [UIColor grayColor];
        _label.font = [UIFont systemFontOfCustomeSize:15];
        _label.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_label];
        [_label mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.label.mas_right).mas_equalTo(10);
            make.centerY.mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(200, [NSString contentofHeightWithFont:_label.font]));
        }];
	}
	return _label;
}

- (void)setImageName:(NSString *)imageName {
    _imageName = imageName;
    self.iconView.image = [UIImage imageNamed:_imageName];
}

- (void)setContent:(NSString *)content {
    _content = content;
    self.label.text = _content;
}

@end
