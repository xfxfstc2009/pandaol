//
//  MMHPayOrderModel.h
//  MamHao
//
//  Created by SmartMin on 15/5/27.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDPayOrderModel : NSObject

@property(nonatomic, copy) NSString * partner;                      /**< 合作商*/
@property(nonatomic, copy) NSString * seller;                       /**< 销售方*/
@property(nonatomic, copy) NSString * tradeNO;                      /**< 订单号*/
@property(nonatomic, copy) NSString * productName;                  /**< 商品名称*/
@property(nonatomic, copy) NSString * productDescription;           /**< 商品详情*/
@property(nonatomic, copy) NSString * amount;                       /**< 商品金额*/
@property(nonatomic, copy) NSString * notifyURL;                    /**< 后端回调地址*/
//123
// 不用填写
@property(nonatomic, copy) NSString * service;                      //
@property(nonatomic, copy) NSString * paymentType;                  //
@property(nonatomic, copy) NSString * inputCharset;                 //
@property(nonatomic, copy) NSString * itBPay;                       //
@property(nonatomic, copy) NSString * showUrl;                      // 


@property(nonatomic, copy) NSString * rsaDate;//可选
@property(nonatomic, copy) NSString * appID;//可选

@property(nonatomic, readonly) NSMutableDictionary * extraParams;


@end
