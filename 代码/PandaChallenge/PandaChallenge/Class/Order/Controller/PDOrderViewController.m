//
//  PDOrderViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDOrderViewController.h"

@implementation PDOrderViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"订单";
}

@end
