//
//  PDRightMenuCell.h
//  PandaChallenge
//
//  Created by panda on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDRightMenuCell : UITableViewCell
@property (nonatomic,copy)NSString *imageViewStr;
@property (nonatomic,copy)NSString *titleStr;

@end
