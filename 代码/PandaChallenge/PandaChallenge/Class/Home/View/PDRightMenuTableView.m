//
//  PDRightMenuTableView.m
//  PandaChallenge
//
//  Created by panda on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRightMenuTableView.h"
#import "PDLoginModel.h"
@implementation PDRightMenuTableView
- (instancetype)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame]) {
        self.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.backgroundColor = [UIColor grayColor];
        
    }
    return self;
}

- (NSArray *)cellImages
{
    return [PDLoginModel shareInstance].hasLoggedIn?@[@"icon_data",@"icon_data",@"icon_data",@"icon_data"] : @[@"icon_data",@"icon_data"];
}

- (NSArray *)cellTitles
{
    return [PDLoginModel shareInstance].hasLoggedIn?@[@"赛事",@"个人",@"帮助",@"退出"] : @[@"登陆",@"帮助"];
}
@end
