//
//  PDRightMenuTableView.h
//  PandaChallenge
//
//  Created by panda on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDRightMenuTableView : UITableView
@property (nonatomic,strong)NSArray *cellTitles;

@property (nonatomic,strong)NSArray *cellImages;


@end
