//
//  PDRightMenuCell.m
//  PandaChallenge
//
//  Created by panda on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRightMenuCell.h"
#define PDIMAGE_WH 20
#define PDMARGIN 5
@interface PDRightMenuCell()
@property (nonatomic,strong)UIImageView *PDimageView;
@property (nonatomic,strong)UILabel *PDtextLabel;
@end
@implementation PDRightMenuCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.backgroundColor = [UIColor clearColor];
        [self createCellSubView];
    }
    return self;
}
- (void)createCellSubView
{
    //imageView
    self.PDimageView = [[UIImageView alloc]init];
    [self addSubview:self.PDimageView];
    [self.PDimageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.equalTo(self).offset(PDMARGIN);
        make.width.height.equalTo(@(PDIMAGE_WH));
    }];
    //title
    self.PDtextLabel = [[UILabel alloc]init];
    self.PDtextLabel.textColor = [UIColor blackColor];
    self.PDtextLabel.textAlignment = NSTextAlignmentCenter;
    self.PDtextLabel.font = [UIFont systemFontOfCustomeSize:18];
    [self addSubview:self.PDtextLabel];
    [self.PDtextLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.PDimageView.mas_right).offset(PDMARGIN);
        make.top.right.equalTo(self).offset(PDMARGIN);
    }];
}
 - (void)setTitleStr:(NSString *)titleStr
{
    _titleStr = titleStr;
    self.PDtextLabel.text = titleStr;
}
- (void)setImageViewStr:(NSString *)imageViewStr
{
    _imageViewStr = imageViewStr;
    self.PDimageView.image = [UIImage imageNamed:imageViewStr];
}
@end
