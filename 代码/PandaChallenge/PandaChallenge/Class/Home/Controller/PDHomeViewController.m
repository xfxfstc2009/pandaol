//
//  PDHomeViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDHomeViewController.h"
#import "PDLoginModel.h"
//rightMenu
#import "PDRightMenuCell.h"
#import "PDRightMenuTableView.h"
// test
#import "PDTestViewController.h"

@interface PDHomeViewController ()<UITableViewDataSource,UITableViewDelegate>
{
    UIButton *button1;
}
@property (nonatomic,strong)UIButton *smartButton;
//菜单
@property (nonatomic,strong)PDRightMenuCell *rightMenuCell;
@property (nonatomic,strong)PDRightMenuTableView *rightMenuTableView;
@property (nonatomic,assign)BOOL rightMenuIsOpen;
@end

@implementation PDHomeViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [PDHUD showShimmeringString:@"132" afterDelay:1];
    [self createMenuButton];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"首页";
    
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:@"左侧按钮" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        [[RESideMenu shareInstance] presentLeftMenuViewController];
    }]; 
    
    
    
    [weakSelf rightBarButtonWithTitle:@"右侧按钮" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDTestViewController *testViewController = [[PDTestViewController alloc]init];
        [strongSelf.navigationController pushViewController:testViewController animated:YES];
    }];
}
#pragma mark - clickButtonToShowMenu
- (void)createMenuButton
{
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(100, 100, 100, 100);
    [btn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [btn setTitle:@"菜单按钮" forState:UIControlStateNormal];
    [self.view addSubview:btn];
    __weak typeof(self)weakSelf = self;
    [btn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [weakSelf showCloseRightMenu:weakSelf.rightMenuIsOpen];
    }];
}
#pragma mark - showCloseRightMenu
- (void)showCloseRightMenu:(BOOL)isOpen
{
    CGFloat x = kScreenBounds.size.width *0.7;
    CGFloat y = [PDLoginModel shareInstance].hasLoggedIn? RIGHTMENU_CELL_HEIGHT * 4 : RIGHTMENU_CELL_HEIGHT * 2;
    CGFloat w = kScreenBounds.size.width - x;
    CGFloat h = y;
    //创建推出
    if (!isOpen) {
        self.rightMenuTableView = [[PDRightMenuTableView alloc]initWithFrame:CGRectMake(x, - y , w, h)];
        self.rightMenuTableView.backgroundColor = [UIColor grayColor];
        self.rightMenuTableView.delegate = self;
        self.rightMenuTableView.dataSource = self;
        [self.view addSubview:self.rightMenuTableView];
        self.rightMenuIsOpen = YES;
        [UIView animateWithDuration:0.3 animations:^{
            self.rightMenuTableView.frame = CGRectMake(x, 0, w, h);
        }];
    }else{
        //回收移除
        [UIView animateWithDuration:0.3 animations:^{
            self.rightMenuTableView.frame = CGRectMake(x, -y, w, h);
            self.rightMenuIsOpen = NO;
            
        }completion:^(BOOL finished) {
            __weak typeof(self) weakSelf = self;
            [weakSelf.rightMenuTableView removeFromSuperview];
        }];
    }
    
    
}
#pragma mark - tabViewDelegate,tabViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return self.rightMenuTableView.cellTitles.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *ID = @"RightMenuCell";
    self.rightMenuCell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!self.rightMenuCell) {
        self.rightMenuCell = [[PDRightMenuCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
    }
    self.rightMenuCell.imageViewStr = self.rightMenuTableView.cellImages[indexPath.row];
    self.rightMenuCell.titleStr = self.rightMenuTableView.cellTitles[indexPath.row];
    return self.rightMenuCell;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return RIGHTMENU_CELL_HEIGHT;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}




@end
