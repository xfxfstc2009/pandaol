//
//  PDLaunchViewController.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@protocol launchBackDelegate <NSObject>

- (void)launchBackDelegate:(UIButton *)button;

@end
@interface PDLaunchViewController : AbstractViewController
@property (nonatomic,assign) id<launchBackDelegate>delegate;
@end
