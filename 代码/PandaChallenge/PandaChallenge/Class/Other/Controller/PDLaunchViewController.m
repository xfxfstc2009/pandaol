//
//  PDLaunchViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLaunchViewController.h"
#import "PDSliderleftViewController.h"
#import "PDNavigationController.h"
#import "PDChallengeViewController.h"
#import "PDLoginViewController.h"
@interface PDLaunchViewController()<UIScrollViewDelegate,RESideMenuDelegate>
@property (nonatomic, strong) NSArray *images;

@property (nonatomic, strong) UIPageControl *pageControl;
@end

@implementation PDLaunchViewController

- (NSArray *)images {
    if (iphone4) {
        return @[@"guide1_4",@"guide2_4",@"guide3_4",@"guide4_4"];
    } else if (iphone5) {
        return @[@"guide1_5",@"guide2_5",@"guide3_5",@"guide4_5"];
    } else if (iphone6) {
        return @[@"guide1_6",@"guide2_6",@"guide3_6",@"guide4_6"];
    } else { //6P
        return @[@"guide1_6p",@"guide2_6p",@"guide3_6p",@"guide4_6p"];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self lunchScrollView];
//    [self setPageView];
}

- (void)lunchScrollView {
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:kScreenBounds];
    scrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.images.count, kScreenBounds.size.height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.bounces = NO;
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    for (int i = 0; i < self.images.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width * i, 0, kScreenBounds.size.width, kScreenBounds.size.height)];
        imageView.image = [UIImage imageNamed:[self.images objectAtIndex:i]];
        [scrollView addSubview:imageView];
        if (i == (self.images.count - 1)) {
            imageView.userInteractionEnabled = YES;
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.backgroundColor = [UIColor clearColor];
            if (iphone4) {
                button.frame = CGRectMake(self.view.frame.size.width/2- LCFloat(50), self.view.frame.size.height - LCFloat(75), LCFloat(100), LCFloat(30));
            } else if (iphone5) {
                button.frame = CGRectMake(self.view.frame.size.width/2- LCFloat(50), self.view.frame.size.height - LCFloat(80), LCFloat(100), LCFloat(30));
            } else if (iphone6) {
                button.frame = CGRectMake(self.view.frame.size.width/2- LCFloat(50), self.view.frame.size.height - LCFloat(80), LCFloat(100), LCFloat(30));
            } else if (iphone6Plus) {
                button.frame = CGRectMake(self.view.frame.size.width/2- LCFloat(50), self.view.frame.size.height - LCFloat(80), LCFloat(100), LCFloat(30));
            }
            
            
            
            if ([self.delegate respondsToSelector:@selector(launchBackDelegate:)]) {
                [self.delegate launchBackDelegate:button];
            }
            [imageView addSubview:button];
                       
        }
    }
}

- (void)setPageView {
    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width * .4f, 30)];
    self.pageControl = pageControl;
    pageControl.center = CGPointMake(self.view.center.x, kScreenBounds.size.height - 20);
    pageControl.numberOfPages = self.images.count;
    pageControl.currentPageIndicatorTintColor = [UIColor redColor];
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.userInteractionEnabled = NO;
    [self.view addSubview:pageControl];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSInteger index = round(scrollView.contentOffset.x / kScreenBounds.size.width);
    self.pageControl.currentPage = index;
    if (index == self.images.count - 1) {
        self.pageControl.hidden = YES;
    } else {
        self.pageControl.hidden = NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
@end
