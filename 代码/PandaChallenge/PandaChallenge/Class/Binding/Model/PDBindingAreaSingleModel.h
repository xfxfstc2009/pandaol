//
//  PDBindingAreaSingleModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@protocol PDBindingAreaSingleModel <NSObject>

@end

@interface PDBindingAreaSingleModel : PDFetchModel

@property (nonatomic,copy)NSString *other_name;         /**< */
@property (nonatomic,copy)NSString *server_id;          /**< id*/
@property (nonatomic,copy)NSString *server_name;        /**< */
@property (nonatomic,copy)NSString *telecom_or_netcom;  /***/
@property (nonatomic,assign)NSInteger status;
@end
