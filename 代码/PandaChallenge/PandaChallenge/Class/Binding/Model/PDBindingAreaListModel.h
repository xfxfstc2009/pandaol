//
//  PDBindingAreaListModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDBindingAreaSingleModel.h"

@protocol PDBindingAreaListModel <NSObject>

@end

@interface PDBindingAreaListModel : PDFetchModel

@property (nonatomic,copy)NSString *area;                                   /**< 区名*/
@property (nonatomic,strong)NSArray<PDBindingAreaSingleModel> *areaList;    /**< 区信息*/

@end
