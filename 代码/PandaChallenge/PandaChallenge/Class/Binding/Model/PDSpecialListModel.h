//
//  PDSpecialListModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDSpecialListModel : PDFetchModel

@property (nonatomic,copy)NSString *content;

@end
