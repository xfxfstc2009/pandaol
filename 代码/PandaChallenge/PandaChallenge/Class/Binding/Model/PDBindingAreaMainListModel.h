//
//  PDBindingAreaMainListModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDBindingAreaListModel.h"

@interface PDBindingAreaMainListModel : PDFetchModel

@property (nonatomic,strong)NSArray<PDBindingAreaListModel> *areaList;

@end
