//
//  PDRoleBingdingLocationTableViewCell.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBindingAreaSingleModel.h"
#import "PDChallengeRoleSingleModel.h"

@interface PDRoleBingdingLocationTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;         /**< 传递过来的cell高度*/
@property (nonatomic,strong)NSArray *transferAreaArr;
@property (nonatomic,strong)PDChallengeRoleSingleModel *transferModel;


+(CGFloat)calculationCellHeight;                                /**< 传出cell高度*/

-(void)pickerViewDidSelectedWithBlock:(void(^)(PDBindingAreaSingleModel *areaSingleModel))block;

@end
