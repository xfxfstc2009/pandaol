//
//  PDRoleBindingNetWorkTableViewCell.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRoleBindingNetWorkTableViewCell.h"
#import <objc/runtime.h>

#define kBingdingButton_Margin LCFloat(30)

@interface PDRoleBindingNetWorkTableViewCell()
@property (nonatomic,strong)NSMutableArray *btnMutableArr;          /**< 按钮数组*/
@property (nonatomic,strong)UIView *bgView;
@end

@implementation PDRoleBindingNetWorkTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.btnMutableArr = [NSMutableArray array];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, transferCellHeight);
}


-(void)setTransferModel:(PDChallengeRoleSingleModel *)transferModel{
    _transferModel = transferModel;
}

-(void)setTransferAreaListModel:(PDBindingAreaMainListModel *)transferAreaListModel{
    NSInteger buttonIndex = -1;
    if (self.transferModel){            // 进行修改
        for (int i = 0 ; i < transferAreaListModel.areaList.count;i++){
            PDBindingAreaListModel *bindingAreaSingleModel = [transferAreaListModel.areaList objectAtIndex:i];
            if ([self.transferModel.other_name hasPrefix:bindingAreaSingleModel.area]){
                buttonIndex = i;
            }
        }
    }
    
    if (self.bgView.subviews.count < transferAreaListModel.areaList.count){
        // 进行创建按钮
        for (int i = 0 ;i < transferAreaListModel.areaList.count;i++){
            PDBindingAreaListModel *bindingAreaListSingleModel = [transferAreaListModel.areaList objectAtIndex:i];
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            [button setTitle:bindingAreaListSingleModel.area forState:UIControlStateNormal];
            CGFloat buttonWidth = (kScreenBounds.size.width - 4 * kBingdingButton_Margin) / 3.;
            CGFloat origin_x = kBingdingButton_Margin + i * (kBingdingButton_Margin + buttonWidth);
            button.frame = CGRectMake(origin_x, LCFloat(30), buttonWidth, LCFloat(40));
            button.layer.cornerRadius = LCFloat(2);
            button.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
            button.layer.borderWidth = LCFloat(1);
            [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            [self.btnMutableArr addObject:button];
            [self.bgView addSubview:button];
            __weak typeof(self)weakSelf = self;
            [button buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf.delegate netWorkClickWith:bindingAreaListSingleModel.areaList];
                // 2. 按钮变颜色
                [strongSelf buttonClickManagerWithIndex:i];
            }];
            
            if (!self.transferModel){
                if ([bindingAreaListSingleModel.area isEqualToString:@"电信"]){
                    buttonIndex = i;
                }
            }
        }
        
        for (int i = 0 ; i < self.btnMutableArr.count;i++){
            UIButton *singleButton = (UIButton *)[self.btnMutableArr objectAtIndex:i];
            if (i == buttonIndex && (buttonIndex != -1)){
                [singleButton setBackgroundColor:[UIColor blackColor]];
                [singleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            } else {
                [singleButton setBackgroundColor:[UIColor whiteColor]];
                [singleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            }
        }
    }
}

#pragma mark - 按钮
-(void)buttonClickManagerWithIndex:(NSInteger)index{
    UIButton *button = (UIButton *)[self.btnMutableArr objectAtIndex:index];
    for (int i = 0 ; i < self.btnMutableArr.count;i++){
        UIButton *singleButton = (UIButton *)[self.btnMutableArr objectAtIndex:i];
        [singleButton setBackgroundColor:[UIColor whiteColor]];
        [singleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    [button setBackgroundColor:[UIColor blackColor]];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}


#pragma mark - calucationCellHeihgt
+(CGFloat)calculationCellHeight{
    return LCFloat(100);
}
@end
