//
//  PDRoleBindingNetWorkTableViewCell.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 1. 创建网络状态按钮
#import <UIKit/UIKit.h>
#import "PDBindingAreaMainListModel.h"              // 传入的信息
#import "PDChallengeRoleSingleModel.h"
//#import "PDRoleBindingViewController.h"

//@class PDRoleBindingViewController;

@protocol PDRoleBindingNetWorkTableViewCellDelegate <NSObject>
-(void)netWorkClickWith:(NSArray *)areaList;
@end

@interface PDRoleBindingNetWorkTableViewCell : UITableViewCell

@property (nonatomic,strong)PDChallengeRoleSingleModel *transferModel;          /**< 上个页面如果有修改，进行修改*/
@property (nonatomic,strong)PDBindingAreaMainListModel *transferAreaListModel;  /**< 上个页面传递过来的区域列表*/
@property (nonatomic,copy)NSString *transferServerName;                         /**< 传递过来的服务器名称*/

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,weak)id<PDRoleBindingNetWorkTableViewCellDelegate> delegate;

+(CGFloat)calculationCellHeight;


@end
