//
//  PDSpecialSingleView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDSpecialSingleView : UIView

-(instancetype)initWithText:(NSString *)text buttonClick:(void (^)(NSString *text))block;
@end
