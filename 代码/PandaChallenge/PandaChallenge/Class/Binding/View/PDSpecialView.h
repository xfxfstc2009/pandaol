//
//  PDSpecialView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDSpecialView : UIView

@property (nonatomic,strong)NSArray *transferArr;

-(void)chooseBackBlock:(void(^)(NSString *string))block;

@end
