//
//  PDSpecialView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSpecialView.h"
#import "PDSpecialSingleView.h"
#import <objc/runtime.h>
#import "PDSpecialListModel.h"

static char chooseKey;
@interface PDSpecialView ()<UIScrollViewDelegate>
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)NSMutableArray *mainMutableArr;
@property (nonatomic,strong)UIScrollView *keyScrollView;
@property (nonatomic,strong)UIPageControl *pageControl;         /**< pageControl*/
@end

@implementation PDSpecialView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        self.mainMutableArr = [NSMutableArray array];
        [self createView];
    }
    return self;
}

-(void)createView{
    if (!self.keyScrollView){
        self.keyScrollView = [[UIScrollView alloc]init];
        self.keyScrollView.backgroundColor = [UIColor clearColor];
        self.keyScrollView.showsHorizontalScrollIndicator = NO;
        self.keyScrollView.showsVerticalScrollIndicator = NO;
        self.keyScrollView.pagingEnabled = YES;
        self.keyScrollView.bounces = NO;
        self.keyScrollView.scrollEnabled = YES;
        self.keyScrollView.delegate = self;
        self.keyScrollView.userInteractionEnabled = YES;
        self.keyScrollView.frame = CGRectMake(0, 0,kScreenBounds.size.width,LCFloat(200));
        [self addSubview:self.keyScrollView];
        
        self.pageControl = [[UIPageControl alloc]init];
        self.pageControl.frame = CGRectMake(0, CGRectGetMaxY(self.keyScrollView.frame) , kScreenBounds.size.width, self.size_height - CGRectGetMaxY(self.keyScrollView.frame));
        self.pageControl.currentPage = 0;
        self.pageControl.backgroundColor = [UIColor clearColor];
        self.pageControl.hidesForSinglePage = YES;
        self.pageControl.currentPageIndicatorTintColor = [UIColor grayColor];
        self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        [self addSubview:self.pageControl]; 
    }
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetSepialList];
}

-(void)chooseBackBlock:(void(^)(NSString *string))block{
    objc_setAssociatedObject(self, &chooseKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)sendRequestToGetSepialList{
    PDSpecialListModel *fetchModel = [[PDSpecialListModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:kSpecialInfo completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            NSArray *tempArr = [fetchModel.content componentsSeparatedByString:@","];
            // 2. 去除空格
            for (NSString *flag in tempArr){
                NSString *x = [flag stringByReplacingOccurrencesOfString:@" " withString:@""];
                [strongSelf.mainMutableArr addObject:x];
            }
            
            if ([strongSelf.mainMutableArr containsObject:@""]){
                [strongSelf.mainMutableArr removeObject:@""];
            }
            
            // 1. 拿到了数组
            CGFloat size_width = kScreenBounds.size.width / 9.;
            CGFloat size_height = LCFloat(200) / 4.;
            NSInteger tempCount = self.mainMutableArr.count;
            
            // 2. 设置分页 4 * 9
            NSInteger numberOfPage = self.mainMutableArr.count % 36 == 0 ? (self.mainMutableArr.count / 36) : (self.mainMutableArr.count / 36 + 1);
            self.pageControl.numberOfPages = numberOfPage;
            self.keyScrollView.contentSize = CGSizeMake(numberOfPage * kScreenBounds.size.width, self.keyScrollView.size_height);
            for (int j = 0 ; j < numberOfPage;j++){
                UIView *mainView = [[UIView alloc]init];
                mainView.backgroundColor = [UIColor clearColor];
                mainView.frame = CGRectMake(j * kScreenBounds.size.width, 0, kScreenBounds.size.width, LCFloat(200));
                mainView.userInteractionEnabled = YES;
                [self.keyScrollView addSubview:mainView];
                
                NSInteger temp = (tempCount > 36 ? 36 : tempCount % 36);
                for (int i = 0 ; i < temp ;i++){
                    PDSpecialSingleView *view = [[PDSpecialSingleView alloc]initWithText:[strongSelf.mainMutableArr objectAtIndex:(j * 36 + i)] buttonClick:^(NSString *text) {
                        if (!weakSelf){
                            return ;
                        }
                        __strong typeof(weakSelf)strongSelf = weakSelf;
                        void(^block)(NSString *string) = objc_getAssociatedObject(strongSelf, &chooseKey);
                        if (block){
                            return block(text);
                        }
                    }];
                    // 1. origin_x
                    CGFloat origin_x = (i % 9) * size_width + (i % 9) * 0;
                    // 2. origin_y
                    CGFloat origin_y =( i / 9 ) * size_height;
                    // 3 .frame
                    CGRect tempRect = CGRectMake(origin_x, origin_y, size_width,size_height);
                    view.frame = tempRect;
                    [mainView addSubview:view];
                    if (i == temp - 1){
                        tempCount -= 36;
                    }
                }
            }
        } else {
        
        }
    }];
}


#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSLog(@"123");
    NSInteger currentIndex = scrollView.contentOffset.x / kScreenBounds.size.width;
    self.pageControl.currentPage = currentIndex;
}
@end
