//
//  PDRoleBingdingLocationTableViewCell.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRoleBingdingLocationTableViewCell.h"
#import <objc/runtime.h>

static char pickerViewDidSelectedKey;
@interface PDRoleBingdingLocationTableViewCell()<UIPickerViewDataSource,UIPickerViewDelegate>
@property (nonatomic,strong)UIPickerView *locationPicker;       /**< picker*/
@property (nonatomic,strong)NSMutableArray *dataSourceMutableArr;

@end

@implementation PDRoleBingdingLocationTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建一个picker
    self.locationPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(LCFloat(25), 0 , kScreenBounds.size.width - 2 * LCFloat(25), LCFloat(150))];
    self.locationPicker.delegate = self;
    self.locationPicker.showsSelectionIndicator = YES;
    self.locationPicker.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self addSubview:self.locationPicker];
    // 2. 数据源初始化
    self.dataSourceMutableArr = [NSMutableArray array];
}

#pragma mark - UIPickerViewDataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.dataSourceMutableArr.count;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return LCFloat(30);
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel * pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.minimumScaleFactor = 8.;
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    pickerLabel.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    PDBindingAreaSingleModel *areaSingleModel = [self.dataSourceMutableArr objectAtIndex:row];
    return areaSingleModel.server_name;
}

#pragma mark - UIPickerViewDelegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    PDBindingAreaSingleModel *areaSingleModel = [self.dataSourceMutableArr objectAtIndex:row];
    void(^block)(PDBindingAreaSingleModel *areaSingleModel) = objc_getAssociatedObject(self, &pickerViewDidSelectedKey);
    if (block){
        block(areaSingleModel);
    }
}


-(void)setTransferAreaArr:(NSArray *)transferAreaArr{
    [self.dataSourceMutableArr removeAllObjects];
    [self.dataSourceMutableArr addObjectsFromArray:transferAreaArr];
    [self.locationPicker reloadAllComponents];
    
    // 1.
    if (self.transferModel){    // 如果是修改
        for (int i = 0 ; i < transferAreaArr.count;i++){
            PDBindingAreaSingleModel *areaSingleModel = [transferAreaArr objectAtIndex:i];
            if ([areaSingleModel.server_name isEqualToString:self.transferModel.server_name]){
                [self.locationPicker selectRow:i inComponent:0 animated:YES];
            }
        }
    }
    
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.locationPicker.frame = CGRectMake(0, 0, kScreenBounds.size.width, transferCellHeight);
}

-(void)setTransferModel:(PDChallengeRoleSingleModel *)transferModel{
    _transferModel = transferModel;
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeihgt = 0 ;
    cellHeihgt += LCFloat(5);
    cellHeihgt += LCFloat(150);
    cellHeihgt += LCFloat(5);
    return cellHeihgt;
}

-(void)pickerViewDidSelectedWithBlock:(void(^)(PDBindingAreaSingleModel *areaSingleModel))block{
    objc_setAssociatedObject(self, &pickerViewDidSelectedKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
