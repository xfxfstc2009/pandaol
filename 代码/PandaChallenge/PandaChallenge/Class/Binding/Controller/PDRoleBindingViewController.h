//
//  PDRoleBindingViewController.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 绑定
#import "AbstractViewController.h"
#import "PDChallengeRoleSingleModel.h"
typedef NS_ENUM(NSInteger,bindingType) {            // 绑定类型
    bindingTypeCreate,                  /**< 创建*/
    bindingTypeUpdate,                  /**< 修改*/
    
};

@interface PDRoleBindingViewController : AbstractViewController

@property (nonatomic,assign)bindingType bindingType;            /**< 绑定类型*/
@property (nonatomic,strong)PDChallengeRoleSingleModel *transferModel;
@property (nonatomic,assign)BOOL isFirstBinding;                /**< 注册后跳转*/

-(void)addRoleWithBlock:(void(^)(PDChallengeRoleSingleModel *model))block;
@end
