//
//  PDRoleBindingViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRoleBindingViewController.h"
#import "PDRoleBindingNetWorkTableViewCell.h"           // 绑定网络
#import "PDRoleBingdingLocationTableViewCell.h"         // 绑定区域
#import <objc/runtime.h>
#import "PDBindingAreaMainListModel.h"
#import "PDSpecialView.h"
#import "PDCustomerServiceCenterViewController.h"
#import "PDWelfareViewController.h"

static char addRoleKey;
@interface PDRoleBindingViewController()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,PDRoleBindingNetWorkTableViewCellDelegate>{
    NSRange tempRange;
}
@property (nonatomic,strong)NSMutableArray *dataSourceMutableArr;                                   /**< 数据源*/
@property (nonatomic,strong)NSMutableArray *pickerMutableArr;                       /**< 区选择数组*/
@property (nonatomic,strong)UITextField *roleNameTextField;                         /**< 输入内容*/
@property (nonatomic,strong)UIButton *bindingButton;                                /**< 绑定按钮*/
@property (nonatomic,strong)NSMutableArray *cellMutableArr;
@property (nonatomic,strong)UITableView *bindingTableView;                          /**< 绑定tableView*/
@property (nonatomic,strong)PDBindingAreaMainListModel *areaListModel;
@property (nonatomic,strong)PDBindingAreaSingleModel *verificationAreaSingleModel;  /**< 验证地区model*/
@property (nonatomic,strong)PDSpecialView *specialView;                             /**< 特殊字符键盘*/
@property (nonatomic,strong)UIButton *serverButton;
@property (nonatomic,strong)JCAlertView *alertView;

@end

@implementation PDRoleBindingViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createNotifi];
    [self createKey];
    [self createCustomerServerButton];
    __weak typeof(self)weakSelf = self;
    [weakSelf getGameServer];               // 获取当前的信息
}

#pragma mark - KeepingStatus
-(void)KeepingStatus {
    if (self.bindingType == bindingTypeUpdate){
        NSLog(@"%@",self.transferModel);
        NSString *serverName = self.transferModel.server_name;
        // 1. 遍历获取电信区
        for (int i = 0 ; i < self.areaListModel.areaList.count;i++){
            PDBindingAreaListModel *areaListModel = [self.areaListModel.areaList objectAtIndex:i];
            for (int j = 0 ; j < areaListModel.areaList.count;j++){
                PDBindingAreaSingleModel *areaSingleMoel = [areaListModel.areaList objectAtIndex:j];
                if ([areaSingleMoel.server_name isEqualToString:serverName]){
                    self.verificationAreaSingleModel = areaSingleMoel;
                    if (self.pickerMutableArr){
                        [self.pickerMutableArr removeAllObjects];
                        [self.pickerMutableArr addObjectsFromArray:areaListModel.areaList];
                    }
                    break ;
                }
            }
        }
    } else {
        // 获取当前的model
        for (int i = 0 ; i < self.areaListModel.areaList.count;i++){
            PDBindingAreaListModel *areaList = [self.areaListModel.areaList objectAtIndex:i];
            if ([areaList.area isEqualToString:@"电信"]){
                self.verificationAreaSingleModel = (PDBindingAreaSingleModel *)[areaList.areaList objectAtIndex:0];
                [self.pickerMutableArr addObjectsFromArray:areaList.areaList];
            }
        }
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"召唤师绑定";
    if (self.isFirstBinding)    
        [self hidesBackButton];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.pickerMutableArr = [NSMutableArray array];
    self.dataSourceMutableArr = [NSMutableArray array];
    self.cellMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.bindingTableView){
        self.bindingTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.bindingTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.bindingTableView.delegate = self;
        self.bindingTableView.dataSource = self;
        self.bindingTableView.scrollEnabled = NO;
        self.bindingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.bindingTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.bindingTableView];
    }
}

#pragma mark - 创建客服中心
-(void)createCustomerServerButton{
    self.serverButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.serverButton setTitle:@"客服中心" forState:UIControlStateNormal];
    [self.serverButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    self.serverButton.titleLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.serverButton.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(11) - LCFloat(30) - 64, kScreenBounds.size.width, LCFloat(30));
    __weak typeof(self)weakSelf = self;
    [self.serverButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDCustomerServiceCenterViewController *server = [[PDCustomerServiceCenterViewController alloc]init];
        [strongSelf.navigationController pushViewController:server animated:YES];
    }];
    [self.view addSubview:self.serverButton];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.cellMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.cellMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"网络"]){            // 【网络】
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDRoleBindingNetWorkTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDRoleBindingNetWorkTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
            cellWithRowOne.delegate = self;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferModel = self.transferModel;
        cellWithRowOne.transferAreaListModel = self.areaListModel;
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"选择"]){     // 【picker】
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDRoleBingdingLocationTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDRoleBingdingLocationTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellStyleDefault;
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo pickerViewDidSelectedWithBlock:^(PDBindingAreaSingleModel *areaSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.verificationAreaSingleModel = areaSingleModel;
            [strongSelf verificationCurrentData];
        }];
        cellWithRowTwo.transferModel = self.transferModel;
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferAreaArr = self.pickerMutableArr;
        
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入"]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowThr.backgroundColor = [UIColor clearColor];
            
            self.roleNameTextField = [[UITextField alloc]init];
            self.roleNameTextField.backgroundColor = [UIColor clearColor];
            self.roleNameTextField.placeholder = @"请输入召唤师姓名";
            self.roleNameTextField.textAlignment = NSTextAlignmentCenter;
            self.roleNameTextField.frame = CGRectMake(LCFloat(16),0, kScreenBounds.size.width - 2 * LCFloat(16), LCFloat(44));
            self.roleNameTextField.font = [UIFont systemFontOfCustomeSize:16.];
            self.roleNameTextField.textColor = [UIColor blackColor];
            self.roleNameTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
            self.roleNameTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
            self.roleNameTextField.returnKeyType = UIReturnKeyDone;
            self.roleNameTextField.delegate = self;
            [self.roleNameTextField addTarget:self action:@selector(textChageManager) forControlEvents:UIControlEventEditingChanged];
            self.roleNameTextField.keyboardType = UIKeyboardTypeDefault;
            self.roleNameTextField.backgroundColor = [UIColor clearColor];
            __weak typeof(self)weakSelf = self;
            [self.roleNameTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex, UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (button == nil){
                    [textField resignFirstResponder];
                    return;
                }
                if (buttonIndex == 0){
                    strongSelf->tempRange = [strongSelf.roleNameTextField selectedRange];
                    [strongSelf changeKeyBoard];
                    if ([button.titleLabel.text isEqualToString:@"特殊字符"]){
                        [button setTitle:@"返回键盘" forState:UIControlStateNormal];
                    } else {
                        [button setTitle:@"特殊字符" forState:UIControlStateNormal];
                    }
                } else if (buttonIndex == 1){
                    [textField resignFirstResponder];
                }
            }];
            [cellWithRowThr addSubview:self.roleNameTextField];
            if (self.bindingType == bindingTypeUpdate){
                self.roleNameTextField.text = self.transferModel.lol_name.length?self.transferModel.lol_name:@"";
            }
            // 1.1 创建线
            UIImageView *lineView = [[UIImageView alloc]init];
            lineView.image = [UIImage imageNamed:@"icon_line"];
            lineView.frame = CGRectMake(LCFloat(60), CGRectGetMaxY(self.roleNameTextField.frame) + LCFloat(2), kScreenBounds.size.width - 2 * LCFloat(60), .5f);
            [cellWithRowThr addSubview:lineView];
            
            
            // 2. 创建label
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.backgroundColor = [UIColor clearColor];
            fixedLabel.text = @"召唤师等级如果未达到30级，则无法绑定。如有问题，请联系客服。";
            fixedLabel.font = [UIFont systemFontOfCustomeSize:12.];
            fixedLabel.textColor = [UIColor grayColor];
            fixedLabel.numberOfLines = 0;
            CGSize contentOfSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(lineView.size_width, CGFLOAT_MAX)];
            fixedLabel.frame = CGRectMake(lineView.orgin_x+LCFloat(15), CGRectGetMaxY(lineView.frame) + LCFloat(2), lineView.size_width, contentOfSize.height);
            fixedLabel.font = [UIFont systemFontOfCustomeSize:12.];
            [cellWithRowThr addSubview:fixedLabel];
            
            
            UIImageView *attentionImageView = [[UIImageView alloc] init];
            attentionImageView.image = [UIImage imageNamed:@"icon_attention"];
            attentionImageView.frame = CGRectMake(lineView.orgin_x, CGRectGetMaxY(lineView.frame) + LCFloat(3), LCFloat(14), LCFloat(14));
            [cellWithRowThr addSubview:attentionImageView];

        }
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮"]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        UITableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowFour.backgroundColor = [UIColor clearColor];
            
            self.bindingButton = [UIButton buttonWithType:UIButtonTypeCustom];
            self.bindingButton.titleLabel.font = [UIFont systemFontOfCustomeSize:18];
            self.bindingButton.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
            self.bindingButton.enabled = NO;
            self.bindingButton.layer.cornerRadius = LCFloat(5);
            self.bindingButton.frame = CGRectMake(LCFloat(16), 0, kScreenBounds.size.width - 2 * LCFloat(16), cellHeight);
            __weak typeof(self)weakSelf = self;
            [self.bindingButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (strongSelf.bindingType == bindingTypeCreate){         // 创建
                    [strongSelf bindingRoleInfoWithServerId:strongSelf.verificationAreaSingleModel.server_id roleName:strongSelf.roleNameTextField.text];
                } else if (strongSelf.bindingType == bindingTypeUpdate){  // 修改
                    [strongSelf updateRoleInfoWithServerId:strongSelf.verificationAreaSingleModel.server_id roleName:strongSelf.roleNameTextField.text match_id:strongSelf.transferModel.roleId];
                }
            }];
            [cellWithRowFour addSubview: self.bindingButton];
            if (self.bindingType == bindingTypeCreate){
                [self.bindingButton setTitle:@"绑定" forState:UIControlStateNormal];
            } else if (self.bindingType == bindingTypeUpdate){
                [self.bindingButton setTitle:@"修改绑定" forState:UIControlStateNormal];
            }
        }
        return cellWithRowFour;
    } else {
        static NSString *cellIdengifyWithRowHHH = @"cellIdengifyWithRowHHH";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowHHH];
        if (!cell){
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowHHH];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"网络"]){
        return [PDRoleBindingNetWorkTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"选择"]){
        return [PDRoleBingdingLocationTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入"]){
        CGFloat cellHeight = 0;
        cellHeight += LCFloat(44);
        cellHeight += LCFloat(2);
        cellHeight += .5f;
        cellHeight += LCFloat(2);
        NSString *content = @"召唤师等级如果未达到30级，则无法绑定。如有问题，请联系客服。";
        CGSize contentOfSize = [content sizeWithCalcFont:[UIFont systemFontOfCustomeSize:12.] constrainedToSize:CGSizeMake(self.roleNameTextField.size_width, CGFLOAT_MAX)];
        cellHeight += contentOfSize.height;
        cellHeight += LCFloat(5);
        return cellHeight;
    } else {
        return 44;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapMagaer)];
    [headerView addGestureRecognizer:tap];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else if (section == 1){
        return 0;
    } else if (section == 2){
        return LCFloat(20);
    } else if (section == 3){
        return LCFloat(20);
    } else {
        return 0;
    }
}



#pragma mark - tapMagaer
-(void)tapMagaer{
    if ([self.roleNameTextField isFirstResponder]){
        [self.roleNameTextField resignFirstResponder];
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if ([self.roleNameTextField isFirstResponder]){
        [self.roleNameTextField resignFirstResponder];
    }
    return YES;
}

-(void)addRoleWithBlock:(void(^)(PDChallengeRoleSingleModel *model))block{
    objc_setAssociatedObject(self, &addRoleKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark 数据验证
-(void)verificationCurrentData{
    if (self.verificationAreaSingleModel && self.roleNameTextField.text.length){
        self.bindingButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        self.bindingButton.enabled = YES;
    } else {
        self.bindingButton.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
        self.bindingButton.enabled = NO;
    }
}

#pragma mark - UITextFieldDelegate
-(void)textChageManager{
    [self verificationCurrentData];
}


#pragma mark - 接口
#pragma mark 获取游戏大区
-(void)getGameServer{
    self.areaListModel = [[PDBindingAreaMainListModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [PDHUD showShimmeringString:@"正在努力获取游戏大区" maskType:WSProgressHUDMaskTypeGradient maskWithout:WSProgressHUDMaskWithoutDefault delay:0];
    [weakSelf.areaListModel fetchWithPath:kBindingRoleGetList completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.bindingTableView dismissPrompt];
            NSArray *cellFixedArr = @[@[@"网络"],@[@"选择"],@[@"输入"],@[@"按钮"]];
            [strongSelf.cellMutableArr addObjectsFromArray:cellFixedArr];

            if (strongSelf.dataSourceMutableArr.count){
                [strongSelf.dataSourceMutableArr removeAllObjects];
            }
            [strongSelf.dataSourceMutableArr addObjectsFromArray:strongSelf.areaListModel.areaList];
//            // 保持修改状态信息
            [strongSelf KeepingStatus];
            [strongSelf.bindingTableView reloadData];
        } else {
            [strongSelf getGameServerTempManagerWithErr:error.localizedDescription];
        }
    }];
}

-(void)getGameServerTempManagerWithErr:(NSString *)err{
    __weak typeof(self)weakSelf = self;
    [weakSelf.bindingTableView showPrompt:[NSString stringWithFormat:@"%@",err] withImage:[UIImage imageNamed:@"icon_prompt_error"] andImagePosition:PDPromptImagePositionTop tapBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf getGameServer];
    }];
}

#pragma mark 修改绑定召唤师
-(void)updateRoleInfoWithServerId:(NSString *)serverId roleName:(NSString *)roleName match_id:(NSString *)matchId{
    self.bindingButton.enabled = NO;
    [PDHUD showShimmeringString:@"正在努力修改召唤师" maskType:WSProgressHUDMaskTypeGradient maskWithout:WSProgressHUDMaskWithoutDefault delay:0];
    PDChallengeRoleSingleModel *fetchModel = [[PDChallengeRoleSingleModel alloc]init];
    fetchModel.requestParams = @{@"server_id":serverId,@"play_name":roleName,@"match_id":matchId};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:kDeleteRole completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        self.bindingButton.enabled = YES;
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(isSucceeded){            // 成功
            fetchModel.lol_name = @"召唤师名称";
            fetchModel.other_name = @"召唤师区";
            fetchModel.lol_max_rank = @"等级";
            
            [strongSelf.navigationController popViewControllerAnimated:YES];
        } else {
            strongSelf.bindingButton.enabled = YES;
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}

#pragma mark 绑定召唤师
-(void)bindingRoleInfoWithServerId:(NSString *)serverId roleName:(NSString *)roleName{
    self.bindingButton.enabled = NO;
    [PDHUD showShimmeringString:@"正在努力绑定召唤师" maskType:WSProgressHUDMaskTypeGradient maskWithout:WSProgressHUDMaskWithoutDefault delay:0];
    PDChallengeRoleSingleModel *fetchModel = [[PDChallengeRoleSingleModel alloc]init];
    fetchModel.requestParams = @{@"server_id":serverId,@"play_name":roleName};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:kBindingRole completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        self.bindingButton.enabled = YES;
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(isSucceeded){            // 成功
            if (strongSelf.isFirstBinding) {
                PDWelfareViewController *welfareViewController = [[PDWelfareViewController alloc]init];
                welfareViewController.isFirstBind = YES;
                [strongSelf.navigationController pushViewController:welfareViewController animated:YES];
            }else{
                void(^block)(PDChallengeRoleSingleModel *model) = objc_getAssociatedObject(strongSelf, &addRoleKey);
                
                fetchModel.lol_name = @"召唤师名称";
                fetchModel.other_name = @"召唤师区";
                fetchModel.lol_max_rank = @"等级";
                
                if (block){
                    block(fetchModel);
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }
            }
        } else {
            strongSelf.bindingButton.enabled = YES;
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if ([self.roleNameTextField isFirstResponder]){
        [self.roleNameTextField resignFirstResponder];
    }
}

#pragma mark - add KeyBoard Notific
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    _bindingTableView.frame = newTextViewFrame;
    _bindingTableView.scrollEnabled = YES;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    _bindingTableView.scrollEnabled = NO;
    _bindingTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
    NSLog(@"释放");
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


#pragma mark - Other Manager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.cellMutableArr.count ; i++){
        NSArray *dataTempArr = [self.cellMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(void)showSelectedPickerManagerWithArr:(NSArray *)arr{
    if (self.pickerMutableArr.count){
        [self.pickerMutableArr removeAllObjects];
    }
    [self.pickerMutableArr addObjectsFromArray:arr];
    
    NSArray *chooseArr = @[@"选择"];
    NSArray *inputArr = @[@"输入"];
    NSInteger cellOfSection = [self cellIndexPathSectionWithcellData:@"网络"];
    if ([self cellIndexPathSectionWithcellData:@"选择"] == -1){   // 没有选择
        [self.cellMutableArr insertObject:chooseArr atIndex:(cellOfSection + 1)];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:(cellOfSection + 1)];
        [self.bindingTableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationBottom];
        
        // 2. 增加输入框
        [self.cellMutableArr insertObject:inputArr atIndex:(cellOfSection + 2)];
        NSIndexSet *indexSet2 = [NSIndexSet indexSetWithIndex:(cellOfSection + 2)];
        [self.bindingTableView insertSections:indexSet2 withRowAnimation:UITableViewRowAnimationBottom];
    } else {
        NSIndexPath *pickerIndexpath = [NSIndexPath indexPathForRow:0 inSection:(cellOfSection + 1)];
        [self.bindingTableView reloadRowsAtIndexPaths:@[pickerIndexpath] withRowAnimation:UITableViewRowAnimationNone];
    }
}

#pragma mark - 修改键盘输入
-(void)changeKeyBoard{
    [UIView animateWithDuration:.3f animations:^{
        if ([self.roleNameTextField.inputView isKindOfClass:[UIView class]]){
            self.roleNameTextField.inputView = nil;
        } else {
            self.roleNameTextField.inputView = self.specialView;
        }
        if ([self.roleNameTextField isFirstResponder]){
            [self.roleNameTextField resignFirstResponder];
            [self.roleNameTextField becomeFirstResponder];
        } else {
            
        }
        [self.roleNameTextField setSelectedRange:tempRange];
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - 创建键盘
-(void)createKey{
    self.specialView = [[PDSpecialView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(220))];
    self.specialView.backgroundColor = [UIColor whiteColor];
    __weak typeof(self)weakSelf = self;
    [self.specialView chooseBackBlock:^(NSString *string) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSRange range = [strongSelf.roleNameTextField selectedRange];
        
        if (strongSelf.roleNameTextField){
            // 1. 获取当前的字符串
            NSString *tempString = strongSelf.roleNameTextField.text;
            // 2. 截取字符串
            // 2.1 0 - location
            NSString *tempString1 = [tempString substringToIndex:range.location];
            NSString *tempString2 = [tempString substringFromIndex:range.location];
            NSString *newTempString = [tempString1 stringByAppendingString:string];
            NSString *newString = [newTempString stringByAppendingString:tempString2];
            strongSelf.roleNameTextField.text = newString;
            NSRange newRange = NSMakeRange(range.location + 1, 0);
            strongSelf->tempRange = newRange;
            [strongSelf.roleNameTextField setSelectedRange:newRange];
        }
        [strongSelf verificationCurrentData];
    }];
}

-(void)netWorkClickWith:(NSArray<PDBindingAreaSingleModel> *)areaList{
    // 刷新indexPath
    // 1. 判断是否有选择
    [self showSelectedPickerManagerWithArr:areaList];
    // 2. 直接塞入
    if (areaList.count){
        if ([[areaList objectAtIndex:0] isKindOfClass:[PDBindingAreaSingleModel class]]){
            self.verificationAreaSingleModel = [areaList objectAtIndex:0];
        }
    }
}

-(void)showErrWithWarn:(NSString *)warn{
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    __weak typeof(self)weakSelf = self;
    [showView alertWithTitle:@"绑定失败" headerImage:[UIImage imageNamed:@"icon_report_warn"] titleDesc:warn buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
    }];
    
    self.alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

@end

