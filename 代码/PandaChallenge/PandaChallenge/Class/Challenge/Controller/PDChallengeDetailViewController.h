//
//  PDChallengeDetailViewController.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
#import "AbstractViewController.h"
#import "PDChallengeViewController.h"
#import "PDChallengerStatusModel.h"

@class PDChallengeViewController;

#define kCard_width LCFloat(42)                     // k卡的宽
#define kCard_height LCFloat(54)                    // k卡的高

typedef void(^backLastPageBlock)(alertType type,NSString *errInfo);

@interface PDChallengeDetailViewController : AbstractViewController

@property (nonatomic,assign)CGRect transferkCardRect;                   /**< 获取到当前k卡的rect*/
@property (nonatomic,assign)CGRect transferHeaderRect;                  /**< 获取到当前头像rect */
@property (nonatomic,assign)CGRect transferNameRect;                    /**< 获取到当前名字的rect*/
@property (nonatomic,assign)CGRect transferLeaveRect;                   /**< 获取到当前等级的rect*/
@property (nonatomic,strong)UIImage *transferImage;                     /**< 获取到上个页面传递的头像*/

@property (nonatomic,strong)NSArray *transferChallengerSelectedMutableArr;       /**< 用户角色的数组*/
@property (nonatomic,strong)NSArray *transferVoucherSelectedMutableArr;          /**< 用户选择k卡的数组*/

// 上一个页面报错，返回并且弹出
@property (nonatomic,copy)backLastPageBlock backLastPageBlock;

#pragma mark - 比赛如果没有开始执行方法
@property (nonatomic,strong)PDChallengerStatusModel *transferChallengerStatusModel;         /**< 传递过来的状态信息*/

#pragma mark - 更换出征条件按钮
-(void)changeChallengeManager:(void(^)())block;


@end
