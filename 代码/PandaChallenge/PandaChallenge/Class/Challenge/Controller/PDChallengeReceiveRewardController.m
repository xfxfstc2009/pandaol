//
//  PDChallengeReceiveRewardController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeReceiveRewardController.h"
#import "PDChallengeUserInfoView.h"             // 头部内容

#define margin_in LCFloat(20)
#define kCard_width LCFloat(70)

@interface PDChallengeReceiveRewardController()
@property (nonatomic,strong)PDChallengeUserInfoView *headerView;        /**< 领奖按钮*/
@property (nonatomic,strong)UIView *kCardView;                          /**< k卡的背景*/
@property (nonatomic,strong)UIButton *getRewardButton;                  /**< 领奖按钮*/


@end

@implementation PDChallengeReceiveRewardController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
//    [self createView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"panda 在线";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"确定" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf.navigationController pushViewController:[[PDChallengeReceiveRewardConfirmController alloc]init] animated:YES];
    }];
}

#pragma mark - createView
-(void)createView{
    // 1. 创建头部内容
    self.headerView = [[PDChallengeUserInfoView alloc]init];
    self.headerView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(100));
    self.headerView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.headerView];

    // 2. 创建k卡
    self.kCardView = [[UIView alloc]init];
    self.kCardView.backgroundColor = [UIColor yellowColor];
    self.kCardView.frame = CGRectMake(0, CGRectGetMaxY(self.headerView.frame), kScreenBounds.size.width, LCFloat(100));
    [self.view addSubview:self.kCardView];
    
    for (int i = 0 ; i < 9 ;i++){
        CGFloat origin_Left = (kScreenBounds.size.width - 3 * kCard_width - 2 * margin_in) / 2.;
        CGFloat origin_x = origin_Left + (i % 3) * kCard_width + (i % 3) * margin_in;
        // 2. origin_y
        CGFloat origin_y = LCFloat(5) + ( i / 3 ) * (kCard_width + margin_in);
        // 3 .frame
        CGRect rect = CGRectMake(origin_x, origin_y, kCard_width, kCard_width);
        NSLog(@"%.2f",rect.origin.x);
    }
    
    // 3. 创建领赏按钮
    self.getRewardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.getRewardButton.backgroundColor = [UIColor clearColor];
    self.getRewardButton.frame = CGRectMake(LCFloat(40), CGRectGetMaxY(self.kCardView.frame) + LCFloat(20), kScreenBounds.size.width - 2 * LCFloat(40), LCFloat(50));
    __weak typeof(self)weakSelf = self;
    [weakSelf.getRewardButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        NSLog(@"领取奖励");
    }];
    [self.getRewardButton setTitle:@"领取奖励" forState:UIControlStateNormal];
    self.getRewardButton.backgroundColor = [UIColor redColor];
    [self.getRewardButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.view addSubview:self.getRewardButton];
    
    
    
}

@end
