//
//  PDChallengeDirectManager.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeDirectManager.h"
#import "PDRewardViewController.h"          // 领赏
#import "PDPayViewController.h"
#import "PDInviteViewController.h"
#import "PDChallengeViewController.h"
#import "PDCustomerServiceCenterViewController.h"           // 客服中心
#import "PDLoginViewController.h"

@implementation PDChallengeDirectManager

#pragma mark 跳转到领赏页面
+(void)directToRewardViewController{
    PDRewardViewController *rewardViewController = [[PDRewardViewController alloc]init];
    [[RESideMenu shareInstance] setContentViewController:[[UINavigationController alloc]initWithRootViewController:rewardViewController] animated:YES];
    [[RESideMenu shareInstance] hideMenuViewController];
}

#pragma mark - 跳转到补赛
+(void)directToPayViewController{
    PDPayViewController *payViewController = [[PDPayViewController alloc]init];
    [[RESideMenu shareInstance] setContentViewController:[[UINavigationController alloc]initWithRootViewController:payViewController] animated:YES];
    [[RESideMenu shareInstance] hideMenuViewController];
}

#pragma mark - 跳转到邀请好友
+(void)directToInviteViewController{
    PDInviteViewController *inviteViewController = [[PDInviteViewController alloc]init];
    [[RESideMenu shareInstance] setContentViewController:[[UINavigationController alloc]initWithRootViewController:inviteViewController] animated:YES];
    [[RESideMenu shareInstance] hideMenuViewController];
}


#pragma mark - 跳转到首页
+(void)directToHomeViewController{
    PDChallengeViewController *inviteViewController = [[PDChallengeViewController alloc]init];
    [[RESideMenu shareInstance] setContentViewController:[[UINavigationController alloc]initWithRootViewController:inviteViewController] animated:YES];
    [[RESideMenu shareInstance] hideMenuViewController];
}


#pragma mark - 跳转到客服中心
+(void)directToCenterViewController{
    PDCustomerServiceCenterViewController *serviceCenterViewController = [[PDCustomerServiceCenterViewController alloc]init];
    [[RESideMenu shareInstance] setContentViewController:[[UINavigationController alloc]initWithRootViewController:serviceCenterViewController] animated:YES];
    [[RESideMenu shareInstance] hideMenuViewController];
}

#pragma mark - 跳转到登录页面，并且清除cookie
+(void)directToLoginPageCleanCookie{
    [[PDLoginModel shareInstance] logoutWithBlock:NULL];
    
    [JCAlertView showOneButtonWithTitle:@"账号异地登录" Message:@"您的账号由于在其他设备登录，已被踢下线。" ButtonType:JCAlertViewButtonTypeDefault ButtonTitle:@"确定" Click:^{
        PDLoginViewController *loginViewController = [[PDLoginViewController alloc]init];
        [[RESideMenu shareInstance] setContentViewController:[[UINavigationController alloc]initWithRootViewController:loginViewController] animated:YES];
        [[RESideMenu shareInstance] hideMenuViewController];
    }];
}

+ (void)directToLoginViewController {
    PDLoginViewController *loginViewController = [[PDLoginViewController alloc]init];
    [[RESideMenu shareInstance] setContentViewController:[[UINavigationController alloc]initWithRootViewController:loginViewController] animated:YES];
    [[RESideMenu shareInstance] hideMenuViewController];
}
@end
