//
//  PDChallengeViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeViewController.h"
#import "PDChallengeUserInfoView.h"                                 // 头部内容
// cell
#import "PDChallengeChooseRoleCell.h"                               // 召唤师cell
#import "PDChallengeChooseCardCell.h"                               // 卡片cell
#import "PDChallengeChooseRoleCell.h"                               // 选择角色

#import "PDChallengeDetailViewController.h"                         // 战斗详情控制器
#import "PDRoleBindingViewController.h"                             // 角色绑定
#import "PDChallengerGameEndAlertView.h"                            // alert
#import "PDLeagueRulesViewController.h"                             // 联赛规则
#import "PDChallengeDirectManager.h"                                // 页面跳转
#import "PDChallengerStatusModel.h"

#import "PDChallengerCheckStatusModel.h"
#import "SBJSON.h"
#import "MobClick.h"
@interface PDChallengeViewController()<UITableViewDataSource,UITableViewDelegate,UIGestureRecognizerDelegate>
@property (nonatomic,strong)PDChallengeUserInfoView *headerView;
@property (nonatomic,strong)UITableView *challengeTableView;            /**< 召唤师tableView*/
@property (nonatomic,strong)NSMutableArray *challengerMutableArr;       /**< 挑战者数组*/
@property (nonatomic,strong)UITableView *voucherTableView;              /**< 凭证tableView*/
@property (nonatomic,strong)NSMutableArray *voucherMutableArr;          /**< 凭证数组*/
@property (nonatomic,strong)UIButton *challengeButton;                  /**< 出征按钮*/
@property (nonatomic,strong)UIButton *guizeButotn;                      /**< 规则按钮*/

// 用户选中的内容
@property (nonatomic,strong)NSMutableArray *challengerSelectedMutableArr;       /**< 用户角色的数组*/
@property (nonatomic,strong)NSMutableArray *voucherSelectedMutableArr;          /**< 用户选择k卡的数组*/
@property (nonatomic,strong)UILabel *titleLabel;

// 手势
@property (nonatomic, strong) UIPanGestureRecognizer *panGest;
@property (nonatomic,strong)JCAlertView *alertView;
@property (nonatomic,strong)UIButton *leftButton;

// 判断是否已经请求失败
@property (nonatomic,assign)BOOL isRequestFail;         /**< 判断是否请求失败*/
@end

@implementation PDChallengeViewController

-(void)dealloc{
    NSLog(@"释放了");
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [MobClick beginLogPageView:@"OnePage"];
    

}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [MobClick endLogPageView:@"OnePage"];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                 // 页面初始化
    [self createMainView];              // 创建主要视图
    [self arrayWithInit];               // 数组初始化
    [self createTableView];             // 创建tableView
    [self addTapGest];                  // 添加手势
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    __weak typeof(self)weakSelf = self;
    [weakSelf authorizeWithCompletionHandler:^(BOOL isSuccess) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccess){
            [strongSelf.headerView animationWithInfo];                  // 头部信息
            [strongSelf sendRequestToGetChallenger];                    // 获取当前角色
            [strongSelf sendRequestToGetKCard];                         // 获取k卡
            if ([PDLoginModel shareInstance].status){                   //
                [strongSelf sendRequestTocheckUserStatus];              // 获取当前比赛状态
            }
        } else {
            [strongSelf dataInfoGetFail];
        }
    }];
}

-(void)addTapGest{    // 增加手势
    self.panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizedWithMap:)];
    self.panGest.delegate = self;
    [self.view addGestureRecognizer:self.panGest];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.isRequestFail = YES;
    self.barMainTitle = @"PANDAOL";
    self.barMainTitleLabel.font = [self.barMainTitleLabel.font boldFont];
    
    __weak typeof(self)weakSelf = self;
    self.leftButton = [weakSelf leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_home_slider"] barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        [[RESideMenu shareInstance] presentLeftMenuViewController];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.challengerMutableArr = [NSMutableArray array];             // 角色卡
    self.voucherMutableArr = [NSMutableArray array];                // k卡
    self.challengerSelectedMutableArr = [NSMutableArray array];     // 选择的角色
    self.voucherSelectedMutableArr = [NSMutableArray array];        // 选中的k卡
    
    [self addNormalButtonWithRole];                                 // 1. 添加当前的信息
    [self addNormalButtonWithKCard];                                // 2. 添加默认k卡
}

#pragma mark createMainView
-(void)createMainView{
    // 2. 创建头部信息
    self.headerView = [[PDChallengeUserInfoView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(177))];
    self.headerView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.headerView];
    
    //3. 创建联赛规则
    self.guizeButotn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.guizeButotn.titleLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.guizeButotn.frame = CGRectMake(LCFloat(105) / 2., self.view.size_height - LCFloat(14) - ([NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:12.]] + 2 * LCFloat(7)) - 64, kScreenBounds.size.width - LCFloat(105) , [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:12.]] + 2 * LCFloat(7));
    [self.guizeButotn setTitle:@"联赛规则" forState:UIControlStateNormal];
    [self.guizeButotn setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self; 
    [self.guizeButotn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDLeagueRulesViewController *leagueRulesViewController = [[PDLeagueRulesViewController alloc]init];
        leagueRulesViewController.isFirstChallage = NO;
        [strongSelf.navigationController pushViewController:leagueRulesViewController animated:NO];
    }];
    [self.guizeButotn setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.guizeButotn];
    
    // 添加出征按钮
    self.challengeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.challengeButton setTitle:@"出征" forState:UIControlStateNormal];
    self.challengeButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.challengeButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.challengeButton.layer.cornerRadius = LCFloat(5);
    self.challengeButton.frame = CGRectMake(LCFloat(105) / 2., self.guizeButotn.orgin_y - LCFloat(25) - LCFloat(44), kScreenBounds.size.width - LCFloat(105), LCFloat(44));
    [self.challengeButton buttonWithBlock:^(UIButton *button) {                 // 执行接口
        if (!weakSelf){
            return ;
        }
        
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (![USERDEFAULTS boolForKey:firstChallenge]) {
            [USERDEFAULTS setBool:YES forKey:firstChallenge];
            PDLeagueRulesViewController *leagueRulesViewController = [[PDLeagueRulesViewController alloc] init];
            leagueRulesViewController.isFirstChallage = YES;
            [strongSelf.navigationController pushViewController:leagueRulesViewController animated:YES];
        } else {
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToValideData];
        }
    }];
    [self.view addSubview: self.challengeButton];
}

#pragma mark - UITableView 
-(void)createTableView{
    if (!self.challengeTableView){
        self.challengeTableView = [[UITableView alloc] initWithFrame:CGRectMake(LCFloat(12), CGRectGetMaxY(self.headerView.frame) + LCFloat(10), LCFloat(260), self.challengeButton.orgin_y - CGRectGetMaxY(self.headerView.frame) + 64 - LCFloat(20)) style:UITableViewStylePlain];
        self.challengeTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.challengeTableView.delegate = self;
        self.challengeTableView.dataSource = self;
        self.challengeTableView.showsHorizontalScrollIndicator = NO;
        self.challengeTableView.showsVerticalScrollIndicator = NO;
        self.challengeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.challengeTableView.allowsMultipleSelection = YES;
        self.challengeTableView.allowsSelectionDuringEditing = YES;
        self.challengeTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.challengeTableView];
    }
    
    if (!self.voucherTableView){
        self.voucherTableView = [[UITableView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(12) - LCFloat(65), CGRectGetMaxY(self.headerView.frame), LCFloat(55) + LCFloat(10), self.challengeTableView.size_height) style:UITableViewStylePlain];
        self.voucherTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.voucherTableView.delegate = self;
        self.voucherTableView.dataSource = self;
        self.voucherTableView.showsHorizontalScrollIndicator = NO;
        self.voucherTableView.showsVerticalScrollIndicator = NO;
        self.voucherTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.voucherTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.voucherTableView];
    }
    
    self.challengeTableView.size_width = self.voucherTableView.orgin_x - LCFloat(20) - LCFloat(12);
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.challengeTableView){
        return self.challengerMutableArr.count;
    } else {
        return self.voucherMutableArr.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGSize cellSize = [tableView rectForRowAtIndexPath:indexPath].size;
    if (tableView == self.challengeTableView){
        if (indexPath.row == [self cellIndexPathSectionWithcellData:@"添加召唤师"]){     // 添加
            static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
            UITableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
            if (!cellWithRowZero){
                cellWithRowZero = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
                cellWithRowZero.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowZero.backgroundColor = [UIColor clearColor];
                // 1.
                UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
                addButton.userInteractionEnabled = NO;
                [addButton setImage:[UIImage imageNamed:@"challenge_add"] forState:UIControlStateNormal];
                addButton.frame = CGRectMake(0, 0, tableView.size_width, cellSize.height);
                [cellWithRowZero addSubview:addButton];
            }
            return cellWithRowZero;
        } else {
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            PDChallengeChooseRoleCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[PDChallengeChooseRoleCell alloc]initWithStyle:UITableViewCellStyleValue1  reuseIdentifier:cellIdentifyWithRowOne tableView:tableView buttonNameArray:@[@"更改",@"解除"] buttonCompletion:NULL];

                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowOne.transferCellSize = cellSize;
            }
            PDChallengeRoleSingleModel *challengeCurrentModel = [self.challengerMutableArr objectAtIndex:indexPath.row];
            cellWithRowOne.transferChallengeRoleSingleModel = challengeCurrentModel;
            
            // 添加cell信息
            UIButton *button1 = cellWithRowOne.button1;
            __weak typeof(self)weakSelf = self;
            [button1 buttonWithBlock:^(UIButton *button) {              // 修改
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf sendRequestToUpdateRoleWithModel:challengeCurrentModel];
                [strongSelf pushManager];
            }];
            
            UIButton *button2 = cellWithRowOne.button2;
            [button2 buttonWithBlock:^(UIButton *button) {              // 解绑
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf unbundlingRoleManager:challengeCurrentModel];
            }];
            
            // 1. 判断当前是否选中当前的model
            if ([self.challengerSelectedMutableArr containsObject:challengeCurrentModel]){       // 如果包含
                [cellWithRowOne setChecked:YES];
            } else {
                [cellWithRowOne setChecked:NO];
            }
            
            return cellWithRowOne;
        }
    } else if (tableView == self.voucherTableView){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDChallengeChooseCardCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDChallengeChooseCardCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellSize = cellSize;
        PDChallengeCardSingleModel *cardSingleModel = [self.voucherMutableArr objectAtIndex:indexPath.row];
        cellWithRowOne.transferCardSingleModel = cardSingleModel;
        
        // 1. 判断当前是否选中当前的k卡model
        if ([self.voucherSelectedMutableArr containsObject:cardSingleModel]){       // 如果包含
            [cellWithRowOne setChecked:YES];
        } else {
            [cellWithRowOne setChecked:NO];
        }
        
        return cellWithRowOne;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.challengeTableView){
        return [PDChallengeChooseRoleCell calculationCellHeight];
    } else if (tableView == self.voucherTableView){
        return [PDChallengeChooseCardCell calculationCellHeight];
    } else {
        return 44;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return LCFloat(10);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (tableView == self.challengeTableView){
        PDChallengeRoleSingleModel *challengeRoleSingleModel = [self.challengerMutableArr objectAtIndex:indexPath.row];
        if ((indexPath.row == self.challengerMutableArr.count - 1) && [challengeRoleSingleModel.lol_name isEqualToString:@"添加召唤师"]){  // 添加挑战者
            PDRoleBindingViewController *roleBindingViewController = [[PDRoleBindingViewController alloc]init];
            roleBindingViewController.bindingType = bindingTypeCreate;
            __weak typeof(self)weakSelf = self;
            [roleBindingViewController addRoleWithBlock:^(PDChallengeRoleSingleModel *model) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf addRoleInChallengeArrayWithModel:model];
            }];
            [self pushManager];
            [self.navigationController pushViewController:roleBindingViewController animated:YES];
        } else {
            [self challengerSelectedManagerWithIndexRow:indexPath.row];
            [self selectedSuccessManager];
        }
    } else if (tableView == self.voucherTableView){
        PDChallengeCardSingleModel *currentCardModel = [self.voucherMutableArr objectAtIndex:indexPath.row];
        if (currentCardModel.card_level != kCardTypeBuy){
            [self kCardSelectedmanagerWithIndexRow:indexPath.row];
            [self selectedSuccessManager];
        } else {    // 购买k卡
            [PDChallengeDirectManager directToPayViewController];
        }
    }
}

#pragma mark - Show Alert
// 房间满了，弹出弹框
-(void)homeHotAlert{
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    [showView alertWithTitle:@"提示" headerImage:[UIImage imageNamed:@"icon_report_warn"] titleDesc:@"当前对战比较多，请稍后再试。" buttonNameArray:nil clickBlock:NULL];
    
    self.alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:YES];
    [self.alertView show];
}

// 没有k卡
-(void)noKCardShow{
    __weak typeof(self)weakSelf = self;
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    [showView alertHorizontalButtonWithTitle:@"无法出征" headerImage:[UIImage imageNamed:@"icon_report_warn"] desc:@"您尚未获得赛事资格" buttonNameArray:@[@"获取赛事资格",@"免费补偿"] clickBlock:^(NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        if (index == 0) {
            [PDChallengeDirectManager directToPayViewController];
        } else {
            [PDChallengeDirectManager directToInviteViewController];
        }
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

// 没有角色
-(void)noChallengerShow{
    __weak typeof(self)weakSelf = self;
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    [showView alertHorizontalButtonWithTitle:@"请绑定召唤师" headerImage:nil desc:@"您还没有绑定召唤师，请先进行绑定" buttonNameArray:@[@"确定",@"取消"] clickBlock:^(NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        if (index == 0) {
            PDRoleBindingViewController *roleBindingViewController = [[PDRoleBindingViewController alloc]init];
            roleBindingViewController.bindingType = bindingTypeCreate;
            [strongSelf.navigationController pushViewController:roleBindingViewController animated:YES];
        }
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

#pragma mark - Interface
#pragma mark 获取战斗角色
-(void)sendRequestToGetChallenger{
    PDChallengeRoleListModel *roleListModel = [[PDChallengeRoleListModel alloc]init];
    __weak typeof(self)weakSelf = self;
    roleListModel.requestParams = @{@"api":@"true"};
    [roleListModel fetchWithPath:kChallengerLoLInfo completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf dataInfoGetSuccess];
            if (strongSelf.challengerSelectedMutableArr.count){
                [strongSelf.challengerSelectedMutableArr removeAllObjects];
            }
            
            if (roleListModel.roleList.count){
                [strongSelf.challengerMutableArr removeAllObjects];
                [strongSelf.challengerMutableArr addObjectsFromArray:roleListModel.roleList];
                [strongSelf addNormalButtonWithRole];
            }
            [strongSelf.challengeTableView reloadData];
        } else {
            [strongSelf dataInfoGetFail];
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        }
    }];
}

#pragma mark  删除召唤师
-(void)sendRequestToDeleteRoleWithModel:(PDChallengeRoleSingleModel *)challengeCurrentModel{
    PDFetchModel *deleteFetchModel = [[PDFetchModel alloc]init];
    deleteFetchModel.requestParams = @{@"server_id":challengeCurrentModel.lol_server_id};
    __weak typeof(self)weakSelf = self;
    [deleteFetchModel fetchWithPath:kDeleteRole completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            __weak typeof(self)weakSelf = self;
            NSString *msg = [NSString stringWithFormat:@"已顺利解绑您的角色【%@】",challengeCurrentModel.lol_name];
            PDAlertShowView *showView = [[PDAlertShowView alloc]init];
            [showView alertWithTitle:@"成功" headerImage:[UIImage imageNamed:@"icon_report_success"] titleDesc:msg buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                // 1. 移除当前的model
                NSInteger modelIndex;
                if([strongSelf.challengerMutableArr containsObject:challengeCurrentModel]){
                    modelIndex = [strongSelf.challengerMutableArr indexOfObject:challengeCurrentModel];
                    [strongSelf.challengerMutableArr removeObject:challengeCurrentModel];
                    // 2. 移除当前的cell
                    [strongSelf.challengeTableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:modelIndex inSection:0]] withRowAnimation:UITableViewRowAnimationLeft];
                }
                [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
            }];
            strongSelf.alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:NO];
            [strongSelf.alertView show];
        } else {
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}

#pragma mark 获取当前卡券
-(void)sendRequestToGetKCard{
    PDChallengeCardListModel *challengeCardListModel = [[PDChallengeCardListModel alloc]init];
    __weak typeof(self)weakSelf = self;
    challengeCardListModel.requestParams = @{@"api":@"true"};
    [challengeCardListModel fetchWithPath:kChallengerKCard completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf dataInfoGetSuccess];
            if (strongSelf.voucherSelectedMutableArr.count){
                [strongSelf.voucherSelectedMutableArr removeAllObjects];
            }
            
            if (challengeCardListModel.cardList.count){
                [strongSelf.voucherMutableArr removeAllObjects];
                [strongSelf.voucherMutableArr addObjectsFromArray:challengeCardListModel.cardList];
                [strongSelf addNormalButtonWithKCard];
            }
            [strongSelf.voucherTableView reloadData];
        } else {
            if (!weakSelf){
                return;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf dataInfoGetFail];
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        }
    }];
}

#pragma mark 进行出征验证
-(void)sendRequestToValideData{
    if (self.voucherMutableArr.count <= 1){
        [self noKCardShow];
        return;
    }
    if (self.challengerMutableArr.count <= 1){
        [self noChallengerShow];
        return;
    }
    
    [PDHUD showWithStatus:@"出征准备中..." maskType:WSProgressHUDMaskTypeGradient maskWithout:WSProgressHUDMaskWithoutDefault delay:0];
    self.challengeButton.enabled = NO;
    PDFetchModel *valideDataModel = [[PDFetchModel alloc]init];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    // 1. 用户角色
    if (self.challengerSelectedMutableArr.count){
        PDChallengeRoleSingleModel *currentModel = [self.challengerSelectedMutableArr lastObject];
        [dic setObject:currentModel.roleId forKey:@"lol_name"];
    }
    // 2. 用户k卡
    if (self.voucherSelectedMutableArr.count){
        PDChallengeCardSingleModel *currentCardModel = [self.voucherSelectedMutableArr lastObject];
        [dic setObject:currentCardModel.card_id forKey:@"card_id"];
    }
    
    valideDataModel.requestParams = dic;
    __weak typeof(self)weakSelf = self;
    [valideDataModel fetchWithPath:kChallengerValideData completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        self.challengeButton.enabled = YES;
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf chuzhengButtonClickManager];
        } else {
            if (error.code == 141){             // 房间已满
                [strongSelf homeHotAlert];
            }  else {
                [strongSelf showErrWithWarn:error.localizedDescription];
            }
        }
    }];
}

#pragma mark 获取当前比赛状态
-(void)sendRequestTocheckUserStatus{
    PDChallengerStatusModel *challengerStatusModel = [[PDChallengerStatusModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [challengerStatusModel fetchWithPath:kCombatSettlement completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
            if (challengerStatusModel.status == 0){
                return;
            }
            UIView *bgView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeGameNotEnd block:^(NSInteger buttonIndex) {
                [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                PDChallengeDetailViewController *challengeDetailViewController = [[PDChallengeDetailViewController alloc]init];
                challengeDetailViewController.transferChallengerStatusModel = challengerStatusModel;
                [strongSelf challengeDetailErrorWithContrtoller:challengeDetailViewController];
                [strongSelf.navigationController pushViewController:challengeDetailViewController animated:NO];
            }];
            strongSelf.alertView = [[JCAlertView alloc]initWithCustomView:bgView dismissWhenTouchedBackground:NO];
            [strongSelf.alertView show];
        } else{
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}

#pragma mark - OtherManager
// 用户进行选择角色
-(void)challengerSelectedManagerWithIndexRow:(NSInteger)index{
    PDChallengeRoleSingleModel *currentModel = [self.challengerMutableArr objectAtIndex:index];
    if (!self.challengerSelectedMutableArr.count){           // 当前没有选择
        [self.challengerSelectedMutableArr addObject:currentModel];
        PDChallengeChooseRoleCell *lastCell = [self.challengeTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        [lastCell setChecked:YES];
    } else {                                                // 当前已有选择
        // 1. 删除上一个model
        PDChallengeRoleSingleModel *lastModel = [self.challengerSelectedMutableArr lastObject];
        NSInteger lastIndex = [self.challengerMutableArr indexOfObject:lastModel];
        
        if ([self.challengerSelectedMutableArr containsObject:currentModel]){
            // 2. 修改上一个view
            PDChallengeChooseRoleCell *lastCell = [self.challengeTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:lastIndex inSection:0]];
            // 3. 执行删除
            [self.challengerSelectedMutableArr removeAllObjects];
            // 4. 修改view
            [lastCell setChecked:NO];
        } else {
            // 2. 修改上一个view
            PDChallengeChooseRoleCell *lastCell = [self.challengeTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:lastIndex inSection:0]];
            // 3. 执行删除
            [self.challengerSelectedMutableArr removeAllObjects];
            // 4. 修改view
            [lastCell setChecked:NO];
            // 5. 增加本model
            [self.challengerSelectedMutableArr addObject:currentModel];
            // 6. 找到当前cell
            PDChallengeChooseRoleCell *currentCell = [self.challengeTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            [currentCell setChecked:YES];
        }
    }
}

// 用户进行选择k卡
-(void)kCardSelectedmanagerWithIndexRow:(NSInteger)index{
    PDChallengeCardSingleModel *currentModel = [self.voucherMutableArr objectAtIndex:index];
    
    if (!self.voucherSelectedMutableArr.count){
        [self.voucherSelectedMutableArr addObject:currentModel];
        PDChallengeChooseCardCell *lastCardCell = [self.voucherTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
        [lastCardCell setChecked:YES];
    } else {
        // 1. 删除上一个model
        PDChallengeCardSingleModel *lastModel = [self.voucherSelectedMutableArr lastObject];
        NSInteger lastIndex = [self.voucherMutableArr indexOfObject:lastModel];
        // 2. 修改上一个view
        PDChallengeChooseCardCell *lastCell = [self.voucherTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:lastIndex inSection:0]];
        if ([self.voucherSelectedMutableArr containsObject:currentModel]){
            [self.voucherSelectedMutableArr removeAllObjects];
            // 4. 修改view
            [lastCell setChecked:NO];
        } else {
            // 3. 执行删除
            [self.voucherSelectedMutableArr removeAllObjects];
            // 4. 修改view
            [lastCell setChecked:NO];
            // 5. 增加本model
            [self.voucherSelectedMutableArr addObject:currentModel];
            // 6. 找到当前cell
            PDChallengeChooseCardCell *currentCell = [self.voucherTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
            [currentCell setChecked:YES];
        }
    }
}

-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfRow = -1;
    for (int i = 0 ; i < self.challengerMutableArr.count ; i++){
        PDChallengeRoleSingleModel *challengeCurrentModel = [self.challengerMutableArr objectAtIndex:i];
        if ([challengeCurrentModel.lol_name isEqualToString:string] && [challengeCurrentModel.other_name isEqualToString:@"游戏大区"]){
            return cellIndexPathOfRow = i;
        }
    }
    return cellIndexPathOfRow;
}

-(void)noDataInfoWithView:(UIView *)view desc:(NSString *)desc manager:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [view showPrompt:desc withImage: [UIImage imageNamed:@"icon_prompt_error"] andImagePosition:PDPromptImagePositionTop tapBlock:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}

// 解绑召唤师
-(void)unbundlingRoleManager:(PDChallengeRoleSingleModel *)challengeCurrentModel{
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    __weak typeof(self)weakSelf = self;
    [showView alertHorizontalButtonWithTitle:@"提示" headerImage:nil desc:[NSString stringWithFormat:@"确定删除召唤师“%@”吗？",challengeCurrentModel.lol_name] buttonNameArray:@[@"确定",@"取消"] clickBlock:^(NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (index == 0) {
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
            [strongSelf sendRequestToDeleteRoleWithModel:challengeCurrentModel];
        }
        [strongSelf pushManager];
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:NO];
    [self.alertView show];

}

#pragma mark - Model Handel
// 添加添加角色按钮
-(void)addNormalButtonWithRole{
    PDChallengeRoleSingleModel *singleModel = [[PDChallengeRoleSingleModel alloc]init];
    singleModel.lol_name = @"添加召唤师";
    singleModel.other_name = @"游戏大区";
    singleModel.lol_max_rank = @"段位";
    [self.challengerMutableArr addObject:singleModel];
}

// 添加卡片按钮
-(void)addNormalButtonWithKCard{
    PDChallengeCardSingleModel *challengeCardSingleModel = [[PDChallengeCardSingleModel alloc]init];
    challengeCardSingleModel.card_level = kCardTypeBuy;
    [self.voucherMutableArr addObject:challengeCardSingleModel];
}

// 修改召唤师
-(void)sendRequestToUpdateRoleWithModel:(PDChallengeRoleSingleModel *)challengeCurrentModel{
    PDRoleBindingViewController *roleBindingViewController = [[PDRoleBindingViewController alloc]init];
    roleBindingViewController.transferModel = challengeCurrentModel;
    roleBindingViewController.bindingType = bindingTypeUpdate;
    [self.navigationController pushViewController:roleBindingViewController animated:YES];
}

// 添加召唤师，将召唤师插入到【添加召唤师按钮之前】
-(void)addRoleInChallengeArrayWithModel:(PDChallengeRoleSingleModel *)model{
    // 1. 查找当前的index
    NSInteger addButtonIndex;
    for (int i = 0 ; i < self.challengerMutableArr.count;i++){
        PDChallengeRoleSingleModel *roleSingleModel = [self.challengerMutableArr objectAtIndex:i];
        if ([roleSingleModel.lol_name isEqualToString:@"添加召唤师"] && [roleSingleModel.other_name isEqualToString:@"游戏大区"]){
            addButtonIndex = i;
            break;
        }
    }
    
    // 2. 插入数据到按钮之前
    [self.challengerMutableArr insertObject:model atIndex:addButtonIndex];
    [self.challengeTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:addButtonIndex inSection:0]] withRowAnimation:UITableViewRowAnimationTop];
}

-(void)selectedSuccessManager{
    if (self.challengerSelectedMutableArr.count && self.voucherSelectedMutableArr.count){
        [self.challengeButton setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
    } else {
        [self.challengeButton setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
    }
}

// 出征按钮点击
-(void)chuzhengButtonClickManager{
    [self.alertView dismissAllAlertWithCompletion:NULL];
    
    PDChallengeDetailViewController *challengeDetailViewController = [[PDChallengeDetailViewController alloc]init];
    // 0. 获取到当前头像图片
    challengeDetailViewController.transferImage = self.headerView.headerImageView.image;
    
    // 1. 获取用户头像的frame
    CGRect convertFrameHeader = [self.headerView convertRect:self.headerView.headerImageView.frame toView:self.view.window];
    challengeDetailViewController.transferHeaderRect = convertFrameHeader;
    // 2. 获取当前用户名字的frame
    // 2.1 获取当前的cell
    PDChallengeRoleSingleModel *challengeCurrentModel = (PDChallengeRoleSingleModel *)[self.challengerSelectedMutableArr lastObject];
    NSInteger cellIndex = [self.challengerMutableArr indexOfObject:challengeCurrentModel];
    PDChallengeChooseRoleCell *challengeChooseRoleCell = [self.challengeTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:cellIndex inSection:0]];
    
    CGRect convertFrameName = [challengeChooseRoleCell.contentView convertRect:challengeChooseRoleCell.roleNameLabel.frame toView:self.view.window];
    challengeDetailViewController.transferNameRect = convertFrameName;
    // 3. 获取当前等级的frame
    CGRect convertFrameLeave = [challengeChooseRoleCell.contentView convertRect:challengeChooseRoleCell.leaveLabel.frame toView:self.view.window];
    challengeDetailViewController.transferLeaveRect = convertFrameLeave;
    
    // 4. 获取到当前的k卡的frame
    // 4.1 获取到当前的cell
    PDChallengeCardSingleModel *cardSingleModel = (PDChallengeCardSingleModel *)[self.voucherSelectedMutableArr lastObject];
    NSInteger cardIndex = [self.voucherMutableArr indexOfObject:cardSingleModel];
    PDChallengeChooseCardCell *cardCell = [self.voucherTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:cardIndex inSection:0]];
    CGRect convertFrameKCard = [cardCell convertRect:cardCell.bgView.frame toView:self.view.window];
    challengeDetailViewController.transferkCardRect = convertFrameKCard;
    
    // 5.传递当前的角色
    challengeDetailViewController.transferChallengerSelectedMutableArr = self.challengerSelectedMutableArr;
    // 6.传递当前的k卡
    challengeDetailViewController.transferVoucherSelectedMutableArr = self.voucherSelectedMutableArr;
    
    
    // 修改出征条件按钮
    __weak typeof(self)weakSelf = self;
    [challengeDetailViewController changeChallengeManager:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.challengerSelectedMutableArr removeAllObjects];
        [strongSelf.voucherSelectedMutableArr removeAllObjects];
        [strongSelf.challengeTableView reloadData];
        [strongSelf.voucherTableView reloadData];
    }];
    [self challengeDetailErrorWithContrtoller:challengeDetailViewController];
    [self.navigationController pushViewController:challengeDetailViewController animated:NO];
}

#pragma mark - UIGestureRecognizer
- (void)panGestureRecognizedWithMap:(UIPanGestureRecognizer *)recognizer{
    [[RESideMenu shareInstance] panGestureRecognized:recognizer];
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch{
    if ([touch.view isKindOfClass:[UIControl class]]){
        return NO;
    } else if ([touch.view isKindOfClass:[UITableView class]] || [touch.view isKindOfClass:[UITableViewCell class]]){
        return NO;
    }
    return YES;
}


-(void)pushManager{
    if (self.alertView){
        [self.alertView dismissAllAlertWithCompletion:NULL];
    }
    NSArray *cellArr = [self.challengeTableView visibleCells];
    for (UITableViewCell *cell in cellArr){
        if ([cell isKindOfClass:[PDChallengeChooseRoleCell class]]){
            PDChallengeChooseRoleCell *challengeChooseRoleCell  = (PDChallengeChooseRoleCell *)cell;
            challengeChooseRoleCell.state = EditTableViewCellStateNormal;
        }
    }
}

#pragma mark 信息获取异常
-(void)dataInfoGetFail{
    if (self.isRequestFail == YES){
        self.leftButton.hidden = YES;
        self.challengeButton.enabled = NO;
        self.guizeButotn.hidden = YES;
        [self.view removeGestureRecognizer:self.panGest];
        
        __weak typeof(self)weakSelf = self;
        [weakSelf noDataInfoWithView:weakSelf.view desc:@"与服务器连接失败" manager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf errFailBtnManager];
        }];
    }
}

-(void)errFailBtnManager{
    self.challengeTableView.hidden = YES;
    self.voucherTableView.hidden = YES;
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetChallenger];
    [weakSelf sendRequestToGetKCard];
    self.isRequestFail = NO;
}

-(void)dataInfoGetSuccess{
    self.isRequestFail = YES;
    self.challengeButton.enabled = YES;
    self.challengeTableView.hidden = NO;
    self.voucherTableView.hidden = NO;
    self.guizeButotn.hidden = NO;
    self.leftButton.hidden = NO;
    [self.view addGestureRecognizer:self.panGest];
    [self.view dismissPrompt];
}

-(void)challengeDetailErrorWithContrtoller:(PDChallengeDetailViewController *)challengeDetailViewController{
    __weak typeof(self)weakSelf = self;
    challengeDetailViewController.backLastPageBlock = ^(alertType type,NSString *errInfo){
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        if (type == alertTypeNoHome){
            PDChallengerGameEndAlertView *alertView = [[PDChallengerGameEndAlertView alloc] initWithFrame:kScreenBounds alertWithType:alertTypeNoHome block:NULL];
            strongSelf.alertView = [[JCAlertView alloc]initWithCustomView:alertView dismissWhenTouchedBackground:YES];
            [strongSelf.alertView show];
        } else if (type == alertTypeValideData){
            [strongSelf showErrWithWarn:errInfo];
        }
    };
}

-(void)showErrWithWarn:(NSString *)warn{
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    __weak typeof(self)weakSelf = self;
    [showView alertWithTitle:@"错误" headerImage:[UIImage imageNamed:@"icon_report_warn"] titleDesc:warn buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
    }];
    
    self.alertView = [[JCAlertView alloc]initWithCustomView:showView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}
@end
