//
//  PDChallengeViewController.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 对战
#import "AbstractViewController.h"
#import "PDChallengeRoleListModel.h"        // 角色
#import "PDChallengeCardListModel.h"        // k卡

@interface PDChallengeViewController : AbstractViewController

@end

