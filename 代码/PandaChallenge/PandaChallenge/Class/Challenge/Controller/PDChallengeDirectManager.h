//
//  PDChallengeDirectManager.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDChallengeDirectManager : NSObject

#pragma mark - 跳转到领赏页面
+(void)directToRewardViewController;    /**< 跳转到领赏页面*/
#pragma mark - 跳转到补赛
+(void)directToPayViewController;       /**< 跳转到补赛页面*/
#pragma mark - 跳转到邀请好友
+(void)directToInviteViewController;    /**< 跳转到邀请好友页面*/

#pragma mark - 跳转到首页
+(void)directToHomeViewController;
#pragma mark - 跳转到客服中心
+(void)directToCenterViewController;    /**< 跳转到客服中心*/

#pragma mark - 跳转到登录页面，并且清除cookie
+(void)directToLoginPageCleanCookie;

#pragma mark - 跳转到登录页面
+ (void)directToLoginViewController;    /**< 只跳转到登录*/
@end
