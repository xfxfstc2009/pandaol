//
//  PDChallengeDetailViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "SBJSON.h"
#import <objc/runtime.h>
#import "PandaChallenge-Swift.h"                    // swift
#import "PDChallengeDetailViewController.h"

// view
#import "PDChallengerProgress.h"
#import "PDChallengerRoleView.h"                    // 角色view
#import "PDChallengeBottomView.h"                   // 底部信息
#import "PDChallengeHeaderView.h"                   // 头部信息

// model
#import "PDChallengerStatusModel.h"
#import "PDChallengerReadyModel.h"                  // 是否准备
#import "PDChallengerGameEndAlertView.h"            // alert
#import "PDChallengerReportView.h"
#import "PDChallengerKingView.h"

#import "PDisAbandonGameModel.h"                    // 接口判断对方是否准备
#import "PDChallengeDirectManager.h"                // 页面跳转
#import "PDChallengeStatusMainModel.h"              // 比赛结果
#import "PDChallengerCheckStatusModel.h"
#import "PDCustomerServiceCenterViewController.h"   // 客服中心

#define kMargin_HeaderWidth LCFloat(122)
static char changeChallengeManagerKey;
@interface PDChallengeDetailViewController()<PDChallengeBottomViewDelegate>
@property (nonatomic,strong)PDChallengeHeaderView *challengeHeaderView;             /**< 头部信息*/
@property (nonatomic,strong)PDImageView *roleImageView;                             /**< 角色头像*/
@property (nonatomic,strong)PDImageView *myKCardImageView;                          /**< 我的k卡*/
@property (nonatomic,strong)UILabel *roleNameLabel;                                 /**< 角色*/
@property (nonatomic,strong)UILabel *leaveNameLabel;                                /**< 等级label*/
@property (nonatomic,strong)PDChallengerRoleView *challengerRoleView;               /**< 对方召唤师view*/
@property (nonatomic,strong)LTMorphingLabel *vsFixedLabel;                          /**< vs文字*/
@property (nonatomic,strong)PDChallengeBottomView *bottomView;                      /**< 底部信息条*/
@property (nonatomic,assign)CGRect bodyRect;                                        /**< bodyView*/
@property (nonatomic,strong)PDImageView *windowCardView;                            /**< 卡片动画过度*/
@property (nonatomic,strong)PDImageView *progress;                                  /**< progress*/
@property (nonatomic,strong)PDChallengerProgress *imageUploadProgress;              /**< 图片加载*/
@property (nonatomic,strong)JCAlertView *alertView;                                 /**< AlertView*/
// temp
@property (nonatomic,strong)PDChallengerStatusModel *tempChallengerStatusModel;     /**< 临时调用*/
@property (nonatomic,strong)UIActivityIndicatorView *activityNameView;
@property (nonatomic,strong)UIActivityIndicatorView *activityLevelView;
@property (nonatomic,strong)UIActivityIndicatorView *cardView;

@end

@implementation PDChallengeDetailViewController

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
    NSLog(@"释放了");
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
    if (self.alertView){
        [self.alertView dismissAllAlertWithCompletion:NULL];
    }
    if (self.bottomView){
        [self.bottomView cleanTimer];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createView];                          // 创建页面
    [self addChallengerNotification];           // 添加通知
    [self statusWithInit];                      // 初始状态
    [self createNotifi];                        // 创建键盘代理
    [self keepingStatusMainManager];            // 保持状态
}

#pragma mark - Create View
-(void)createView{
    [self createNavView];       // 1. 创建头部信息
    [self createBottomView];    // 2. 创建底部信息
    [self createBodyView];      // 3. 创建body信息
}

#pragma mark Create View Header
-(void)createNavView{
    self.challengeHeaderView = [[PDChallengeHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(100))];
    self.challengeHeaderView.backgroundColor = [UIColor clearColor];
//    __weak typeof(self)weakSelf = self;
    [self.challengeHeaderView.popButton buttonWithBlock:^(UIButton *button) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf.navigationController popToRootViewControllerAnimated:YES];
    }];
    [self.view addSubview:self.challengeHeaderView];
}

#pragma mark Create View Bottom
-(void)createBottomView{
    // 1. 创建底部信息
    self.bottomView = [[PDChallengeBottomView alloc]initWithFrame:CGRectMake(0, kScreenBounds.size.height, kScreenBounds.size.width, LCFloat(250))];
    self.bottomView.delegate = self;
    self.bottomView.orgin_y = kScreenBounds.size.height;
    self.bottomView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.bottomView];
}

#pragma mark Create View Body
-(void)createBodyView{
    self.bodyRect = CGRectMake(0, self.challengeHeaderView.size_height, kScreenBounds.size.width, kScreenBounds.size.height - self.challengeHeaderView.size_height - self.bottomView.size_height);
    
    // 2.创建图片
    self.roleImageView = [[PDImageView alloc]init];
    self.roleImageView.backgroundColor = [UIColor clearColor];
    self.roleImageView.frame = self.transferHeaderRect;
    self.roleImageView.image = self.transferImage;
    [self.view addSubview:self.roleImageView];
    
    // 3. 创建卡片等级
    self.myKCardImageView = [[PDImageView alloc]init];
    self.myKCardImageView.backgroundColor = [UIColor clearColor];
    self.myKCardImageView.frame = self.transferHeaderRect;
    if ([self isTransferKCard]){
        PDChallengeCardSingleModel *currentCardModel = [self.transferVoucherSelectedMutableArr lastObject];
        self.myKCardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)currentCardModel.card_level]];
    } else {
        self.myKCardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_-2_nor"]];
    }
    [self.view addSubview:self.myKCardImageView];
    
    // 4.创建名称
    self.roleNameLabel = [[UILabel alloc]init];
    self.roleNameLabel.backgroundColor = [UIColor clearColor];
    self.roleNameLabel.font = [UIFont systemFontOfCustomeSize:16.];
    self.roleNameLabel.textAlignment = NSTextAlignmentLeft;
    self.roleNameLabel.frame = self.transferNameRect;
    [self.view addSubview:self.roleNameLabel];
    
    // 5.创建等级
    self.leaveNameLabel = [[UILabel alloc]init];
    self.leaveNameLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.leaveNameLabel.frame = self.transferLeaveRect;
    self.leaveNameLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.leaveNameLabel];
    
    // 1. 用户角色
    if (self.transferChallengerSelectedMutableArr.count){
        PDChallengeRoleSingleModel *currentModel = [self.transferChallengerSelectedMutableArr lastObject];
        self.roleNameLabel.text = currentModel.lol_name;
        self.leaveNameLabel.text = currentModel.lol_max_rank;
    }
    
    // 4. 创建对方召唤师
    CGFloat kMargin_top = (self.bodyRect.size.height - kMargin_HeaderWidth - LCFloat(11) - [NSString contentofHeightWithFont:self.roleNameLabel.font] - LCFloat(11) - [NSString contentofHeightWithFont:self.leaveNameLabel.font]) / 2.;
    
    CGRect enemyRect = CGRectMake(kScreenBounds.size.width - LCFloat(28) - kMargin_HeaderWidth, self.challengeHeaderView.size_height + kMargin_top, kMargin_HeaderWidth, LCFloat(200));
    
    self.challengerRoleView = [[PDChallengerRoleView alloc]initWithFrame:enemyRect];
    self.challengerRoleView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.challengerRoleView];
    // 设置当前的状态
    [self.challengerRoleView viewShowManagerWithStatus:challengerStatusNormal];
    
    // 5. 创建VS
    self.vsFixedLabel = [[LTMorphingLabel alloc]init];
    self.vsFixedLabel.backgroundColor = [UIColor clearColor];
    self.vsFixedLabel.textAlignment = NSTextAlignmentCenter;
    self.vsFixedLabel.font = [[UIFont systemFontOfCustomeSize:24]boldFont];
    self.vsFixedLabel.text = @"";
    self.vsFixedLabel.hidden = YES;
    self.vsFixedLabel.frame = CGRectMake(self.roleImageView.orgin_x, self.challengerRoleView.orgin_y + (self.challengerRoleView.roleImageView.size_height - [NSString contentofHeightWithFont:self.vsFixedLabel.font]) / 2., kScreenBounds.size.width - (LCFloat(28) + kMargin_HeaderWidth) * 2, [NSString contentofHeightWithFont:self.vsFixedLabel.font]);
    [self.view addSubview:self.vsFixedLabel];
    
    // 5. 创建菊花1
    self.activityNameView = [[UIActivityIndicatorView alloc]init];
    [self.activityNameView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];//设置进度轮显示类型
    [self.view addSubview:self.activityNameView];
    
    // 6. 创建菊花2
    self.activityLevelView = [[UIActivityIndicatorView alloc]init];
    [self.activityLevelView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];//设置进度轮显示类型
    [self.view addSubview:self.activityLevelView];
    
    self.cardView = [[UIActivityIndicatorView alloc]init];
    [self.cardView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];//设置进度轮显示类型
    self.cardView.frame = CGRectMake((kCard_width - LCFloat(20)) / 2.,( kCard_height - LCFloat(20)) / 2., LCFloat(20), LCFloat(20));
    self.cardView.backgroundColor = [UIColor clearColor];
    [self.cardView startAnimating];
    [self.myKCardImageView addSubview:self.cardView];
}

#pragma mark - Other Manager 
-(void)statusWithInit{      // 1. 底部信息
    if ([self isTransferRole]){         // 如果选中挑战者，显示当前大区
        PDChallengeRoleSingleModel *currentModel = [self.transferChallengerSelectedMutableArr lastObject];
        [self.challengeHeaderView showAnimationWithText:currentModel.server_name];
    }
}

-(BOOL)isTransferKCard{
    if (self.transferVoucherSelectedMutableArr.count){
        return YES;
    } else {
        return NO;
    }
}

-(BOOL)isTransferRole{
    if (self.transferChallengerSelectedMutableArr.count){
        return YES;
    } else {
        return NO;
    }
}

#pragma mark Notification
-(void)addChallengerNotification{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onMessageReceived:) name:@"CCPDidReceiveMessageNotification" object:nil]; // 注册
}

// 推送下来的消息抵达的处理示例
- (void)onMessageReceived:(NSNotification *)notification {
    NSData *data = [notification object];
    NSString *str =  [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"【推送】%@",str);
    
    SBJSON *json = [[SBJSON alloc] init];
    NSDictionary *dicWithRequestJson = [json objectWithString:str error:nil];
    PDChallengerStatusModel *statusModel = [[PDChallengerStatusModel alloc]initWithJSONDict:[dicWithRequestJson objectForKey:@"data"]];
    if (statusModel.status == 1){                       // 进行匹配 匹配成功
        self.tempChallengerStatusModel = statusModel;
        [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
        [self pushChallengePipeiManagerWithModel:statusModel];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self confirmShowSliderWithStatus1];        // 比赛状态更新
        });
    } else if (statusModel.status == 2){                // 比赛开始
        self.tempChallengerStatusModel.room_name = statusModel.room_name;
        self.tempChallengerStatusModel.room_pass = statusModel.room_pass;
        self.tempChallengerStatusModel.battle_end_time = statusModel.battle_end_time;
        self.tempChallengerStatusModel.status = 2;
        
        [self pushChallengingManagerWithModel:self.tempChallengerStatusModel];
        [self.challengerRoleView showManagerWithChallengerSingleModel:self.tempChallengerStatusModel.emenge];

        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self confirmShowSliderWithStatus2];        // 比赛状态更新
        });
    } else if (statusModel.status == 3){                // 推送
        [PDLoginModel shareInstance].challengeStatus = challengerStatusCanEndGame;
        [self pushChallengingEndManagerWithModel:statusModel];
    } else if (statusModel.status == 7){                // 比赛强制结束
        [self pushChallengingCancelEndManagerWithModel:statusModel];
        [PDLoginModel shareInstance].status = 0;
    } else if (statusModel.status == 8){                // 比赛失效
        [self pushChallengingCancelManagerWithModel:statusModel];
        [PDLoginModel shareInstance].status = 0;
    }
}

#pragma mark KeyBoard Notification
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.alertView.orgin_y = newTextViewFrame.origin.y;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    [UIView commitAnimations];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - Interface
#pragma mark 1.进行出征
-(void)sendRequestToChallenger{
    [PDHUD showImage:[UIImage imageNamed:@"login_logo"] status:@"准备出征" delay:0];
    PDFetchModel *challengerModel = [[PDFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    
    // 1. 用户角色
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (self.transferChallengerSelectedMutableArr.count){
        PDChallengeRoleSingleModel *currentModel = [self.transferChallengerSelectedMutableArr lastObject];
        NSArray *userInfoArr = @[currentModel.roleId];
        [dic setObject:userInfoArr forKey:@"lol_info"];
        
    } else {
        [dic setObject:@[@"true"] forKey:@"lol_info"];
    }
    // 2. 用户k卡
    if (self.transferVoucherSelectedMutableArr.count){
        PDChallengeCardSingleModel *currentCardModel = [self.transferVoucherSelectedMutableArr lastObject];
        NSArray *kCardInfoArr = @[currentCardModel.card_id];
        [dic setObject:kCardInfoArr forKey:@"card_id"];
        // 3.修改当前的k卡
        self.myKCardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",currentCardModel.card_level]];
    } else {
        [dic setObject:@[@"true"] forKey:@"card_id"];
    }
    
    challengerModel.requestParams = dic;
    
    [challengerModel fetchWithPath:kChallengerChuzheng completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){           // 进行准备状态
            [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
            [strongSelf.challengerRoleView viewShowManagerWithStatus:[PDLoginModel shareInstance].challengeStatus];
            [strongSelf.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
        } else {
            [strongSelf showError:error.localizedDescription];
        }
    }];
}

#pragma mark  2.进行准备
-(void)sendRequestToReady{
    [PDHUD showWithafterDelay:0];
    PDChallengerReadyModel *readyFetchModel = [[PDChallengerReadyModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [readyFetchModel fetchWithPath:kChallengerReady completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (readyFetchModel.is_ready == YES){        // 对方准备
                [PDLoginModel shareInstance].challengeStatus = challengerStatusReady;
            } else {    // 对方未准备
                if ([PDLoginModel shareInstance].challengeStatus < challengerStatusReady){
                    [PDLoginModel shareInstance].challengeStatus = challengerStatusNotReadyTo;
                }
            }
            [strongSelf.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
        } else {
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}

#pragma mark  点击结算失败点击确定后执行接口
-(void)sendRequestToComfirmWithCalculationEnd{
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:kChallengerConfirm completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
            [strongSelf dismissAnimation];
            [strongSelf.bottomView cleanTimer];
        } else {
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}

#pragma mark 申请客服介入接口
-(void)sendRequestToApplyCustomer{
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:kChallengerApplyCustomer completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDChallengerGameEndAlertView *alertBgView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeWatingGameEnd block:^(NSInteger buttonIndex) {
                if (buttonIndex == 0){  // 客服中心
                    [PDChallengeDirectManager directToCenterViewController];
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                }
            }];
            strongSelf.alertView = [[JCAlertView alloc]initWithCustomView:alertBgView dismissWhenTouchedBackground:NO];
            [strongSelf.alertView show];
        } else {
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}

#pragma mark 再次进行匹配
-(void)sendRequestToChallengingAgain{
    [PDHUD showWithStatus:@"正在再次匹配..." maskType:WSProgressHUDMaskTypeGradient maskWithout:WSProgressHUDMaskWithoutDefault delay:0];
    PDFetchModel *challengeAgainModel = [[PDFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [challengeAgainModel fetchWithPath:kChallengerChuzhengAgain completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = self;
        if (isSucceeded){
            [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
            // 重新修改当前的对战者状态
            [strongSelf.challengerRoleView viewShowManagerWithStatus:[PDLoginModel shareInstance].challengeStatus];
            // 2. 修改底部信息
            [strongSelf.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
            // 3. alert消失
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        } else {
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}


#pragma mark 放弃比赛
-(void)sendRequestTocancelBisai{
    [PDHUD showWithStatus:@"正在放弃比赛..." maskType:WSProgressHUDMaskTypeGradient maskWithout:WSProgressHUDMaskWithoutDefault delay:0];
    PDFetchModel *cancelChallengingModel = [[PDFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [cancelChallengingModel fetchWithPath:kChallengeWithCancel completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            // 1. 消失
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
            // 2. pop
            [strongSelf dismissAnimation];
        } else {
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}

#pragma mark 战斗结束
-(void)sendRequestToEndChallenge{
    [PDHUD showWithafterDelay:0];
    // 1. 重置当前状态
    [PDLoginModel shareInstance].challengeStatus = challengerStatusCalculationing;
    // 2. 修改底部状态为结算中
    [self.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
    
    PDChallengerStatusModel *statusModel= [[PDChallengerStatusModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [statusModel fetchWithPath:kChallengerCombatSettlement completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf calculationEndWithStatusModel:statusModel];
        } else {
            // 1. 重置当前状态
            [PDLoginModel shareInstance].challengeStatus = challengerStatusChallenging;
            // 2. 修改底部状态为结算中
            [self.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
            
            if (error.code == 313){         // 结算失败
                [strongSelf keepingStatus6];
            } else if (error.code == 314){      // 再次结算失败
                [strongSelf keepingStatus6];
            } else {
                [strongSelf showErrWithWarn:error.localizedDescription];
            }
        }
    }];
}

#pragma mark 没有接受到通知，自动放弃比赛
-(void)sendRequestToCancelGameWithNotNotificationWithBlock:(void(^)(BOOL isReady,PDChallengerStatusModel *fetchModel))block{
    PDChallengerStatusModel *fetchModel = [[PDChallengerStatusModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:kChallengerCancelGameWithNotNotif completionHandler:^(BOOL isSucceeded, NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if(fetchModel.gameArea.length && fetchModel.emenge){
                block(YES,fetchModel);
            } else {
                if (fetchModel.other_status == YES){        // 对方准备了
                    if (block){
                        block(YES,nil);
                    }
                } else {                                    // 对方没有准备
                    if (block){
                        block(NO,nil);
                    }
                }
            }
        } else {
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}

#pragma mark 进行出征验证
-(void)sendRequestToValideDataWithIsNext:(BOOL)isNext{
    PDFetchModel *valideDataModel = [[PDFetchModel alloc]init];
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    // 1. 用户角色
    if (self.transferChallengerSelectedMutableArr.count){
        PDChallengeRoleSingleModel *currentModel = [self.transferChallengerSelectedMutableArr lastObject];
        [dic setObject:currentModel.roleId forKey:@"lol_info"];
    }
    // 2. 用户k卡
    if (self.transferVoucherSelectedMutableArr.count){
        PDChallengeCardSingleModel *currentCardModel = [self.transferVoucherSelectedMutableArr lastObject];
        [dic setObject:currentCardModel.card_id forKey:@"card_id"];
    }
    
    valideDataModel.requestParams = dic;
    __weak typeof(self)weakSelf = self;
    [valideDataModel fetchWithPath:kChallengerValideData completionHandler:^(BOOL isSucceeded, NSError *error) {
        [PDHUD dismiss];
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (isNext){
                [strongSelf sendRequestToChallengerWithNext];
            } else {
                [strongSelf sendRequestToChallenger];
            }
        } else {
            if (error.code == 141){     // 房间已满
                [strongSelf homeHotAlert];
            } else {
                if (strongSelf.backLastPageBlock){
                    strongSelf.backLastPageBlock(alertTypeValideData,error.localizedDescription);
                }
                [strongSelf showError:error.localizedDescription];
            }
        }
    }];
}

#pragma mark 请求检测当前是否匹配到
-(void)sendRequestToAdjustIsMating{
    PDChallengerCheckStatusModel *statusModel = [[PDChallengerCheckStatusModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [statusModel fetchWithPath:kChallengerIsMatting completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (statusModel.status == 1){
                PDChallengerStatusModel *challengerStatusModel = [[PDChallengerStatusModel alloc]init];
                challengerStatusModel.status = statusModel.status;
                
                if (statusModel.gameName.length && (![PDTool isEmpty:statusModel.gameName])){
                    challengerStatusModel.gameName = statusModel.gameName;
                    challengerStatusModel.gameArea = statusModel.gameArea;
                    
                    if (statusModel.matchingData.emenge){
                        challengerStatusModel.room_name = statusModel.matchingData.emenge.room_name;
                        challengerStatusModel.room_pass = statusModel.matchingData.emenge.room_pass;
                    }
                    
                    if (statusModel.matchingData.myChallenge){
                        challengerStatusModel.myChallenge = [[PDChallengerSingleModel alloc]init];
                        challengerStatusModel.myChallenge.avatar = statusModel.matchingData.myChallenge.msg.avatar;
                        challengerStatusModel.myChallenge.name = statusModel.matchingData.myChallenge.msg.lol_name;
                        challengerStatusModel.myChallenge.lol_name = statusModel.matchingData.myChallenge.msg.lol_name;
                        challengerStatusModel.card_level = statusModel.matchingData.myChallenge.msg.card_leavel;
                        challengerStatusModel.myChallenge.card_leavel = statusModel.matchingData.myChallenge.msg.card_leavel;
                        challengerStatusModel.myChallenge.level = statusModel.matchingData.myChallenge.msg.lol_max_rank;
                        challengerStatusModel.myChallenge.lol_max_rank = statusModel.matchingData.myChallenge.msg.lol_max_rank;
                    }
                    if (statusModel.matchingData.emenge){
                        challengerStatusModel.emenge = [[PDChallengerSingleModel alloc]init];
                        challengerStatusModel.emenge.avatar = statusModel.matchingData.emenge.msg.avatar;
                        challengerStatusModel.emenge.name = statusModel.matchingData.emenge.msg.lol_name;
                        challengerStatusModel.emenge.lol_name = statusModel.matchingData.emenge.msg.lol_name;
                        challengerStatusModel.card_level = statusModel.matchingData.emenge.msg.card_leavel;
                        challengerStatusModel.emenge.card_leavel = statusModel.matchingData.emenge.msg.card_leavel;
                        challengerStatusModel.emenge.level = statusModel.matchingData.emenge.msg.lol_max_rank;
                        challengerStatusModel.emenge.lol_max_rank = statusModel.matchingData.emenge.msg.lol_max_rank;
                    }
                    if (statusModel && challengerStatusModel.status == 1){        // 匹配到
                        self.tempChallengerStatusModel = challengerStatusModel;
                        [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
                        [strongSelf pushChallengePipeiManagerWithModel:challengerStatusModel];
                    } else {                                            // 没有匹配到
                        [strongSelf challengeStatusNotFindedManager];
                    }
                } else {
                    [strongSelf challengeStatusNotFindedManager];
                }
            }
        } else {
            [strongSelf showErrWithWarn:error.localizedDescription];
        }
    }];
}

#pragma mark - Show Animation
-(void)showAnimation{
    self.myKCardImageView.hidden = YES;
    if ([self isTransferKCard]){
        [self.cardView stopAnimating];
        [self cardAnimation];
    }
    [UIView animateWithDuration:.4f animations:^{
        // 【0.获取当前的间距】
        CGFloat kMargin_top = (self.bodyRect.size.height - kMargin_HeaderWidth - LCFloat(11) - [NSString contentofHeightWithFont:self.roleNameLabel.font] - LCFloat(11) - [NSString contentofHeightWithFont:self.leaveNameLabel.font]) / 2.;
        
        // 【1.获取当前我的头像的位置】
        self.roleImageView.frame = CGRectMake(LCFloat(28), self.challengeHeaderView.size_height + kMargin_top, kMargin_HeaderWidth ,kMargin_HeaderWidth);
        
        // 【1.1获取我当前kCard位置】
        self.myKCardImageView.frame = CGRectMake(CGRectGetMaxX(self.roleImageView.frame) + LCFloat(11) - kCard_width, CGRectGetMaxY(self.roleImageView.frame) - kCard_height, kCard_width, kCard_height);
        
        // 【2.获取当前我的召唤师名字】
        CGSize roleNameSize = [self.roleNameLabel.text sizeWithCalcFont:self.roleNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.roleNameLabel.font])];
        CGFloat roleName_origin_x = self.roleImageView.orgin_x + (self.roleImageView.size_width - roleNameSize.width) / 2.;
        self.roleNameLabel.frame = CGRectMake(roleName_origin_x, CGRectGetMaxY(self.myKCardImageView.frame) + LCFloat(11), roleNameSize.width, [NSString contentofHeightWithFont:self.roleNameLabel.font]);
        
        // 菊花
        self.activityNameView.frame = self.roleNameLabel.frame;
        if (![self isTransferRole]){
            [self.activityNameView startAnimating];
        } else {
            [self.activityNameView stopAnimating];
        }
        
        // 【3.获取当前等级名称】
        CGSize leaveSize = [self.leaveNameLabel.text sizeWithCalcFont:self.leaveNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leaveNameLabel.font])];
        CGFloat leave_origin_x = self.roleImageView.orgin_x + (self.roleImageView.size_width - leaveSize.width) / 2.;
        self.leaveNameLabel.frame = CGRectMake(leave_origin_x, CGRectGetMaxY(self.roleNameLabel.frame) + LCFloat(11), leaveSize.width, [NSString contentofHeightWithFont:self.leaveNameLabel.font]);
        
        // 菊花
        self.activityLevelView.frame = self.leaveNameLabel.frame;
        if (![self isTransferRole]){
            [self.activityLevelView startAnimating];
        } else {
            [self.activityLevelView stopAnimating];
        }
        
        // 【4.头部】
        self.challengeHeaderView.orgin_y = 0;
        // 5. 底部
        self.bottomView.orgin_y = kScreenBounds.size.height - self.bottomView.size_height;
        
        // 6. 卡片
        self.windowCardView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(84)) / 2., (kScreenBounds.size.height - LCFloat(108)) / 2., LCFloat(84), LCFloat(108));
        
    } completion:^(BOOL finished) {
        [self cardDismissAnimation];
    }];
}

// k卡动画
-(void)cardAnimation{           // 1. 创建一个card
    self.windowCardView = [[PDImageView alloc]init];
    self.windowCardView.backgroundColor = [UIColor clearColor];
    self.windowCardView.clipsToBounds = YES;
    PDChallengeCardSingleModel *currentCardModel = [self.transferVoucherSelectedMutableArr lastObject];
    self.windowCardView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)currentCardModel.card_level]];
    self.windowCardView.frame = self.transferkCardRect;
    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
    [window addSubview:self.windowCardView];
}

// k卡消失动画
-(void)cardDismissAnimation{
    [UIView animateWithDuration:.5f animations:^{
        self.windowCardView.frame = CGRectMake(CGRectGetMaxX(self.roleImageView.frame) + LCFloat(11) - kCard_width, CGRectGetMaxY(self.roleImageView.frame) - kCard_height, kCard_width, kCard_height);
    } completion:^(BOOL finished) {
        self.myKCardImageView.hidden = NO;
        
        self.myKCardImageView.frame = CGRectMake(CGRectGetMaxX(self.roleImageView.frame) + LCFloat(11) - kCard_width, CGRectGetMaxY(self.roleImageView.frame) - kCard_height, kCard_width, kCard_height);
        [self.windowCardView removeFromSuperview];
        self.windowCardView = nil;
    }];
}

// 消失动画
-(void)dismissAnimation{
    [UIView animateWithDuration:.4f animations:^{
        // 1. 头像位置
        self.roleImageView.frame = self.transferHeaderRect;
        // 2. 当前召唤师名字
        self.roleNameLabel.frame = self.transferNameRect;
        // 3. 当前名称
        self.leaveNameLabel.frame = self.transferLeaveRect;
        // 4. 修改当前头部frame
        self.challengeHeaderView.orgin_y = -self.challengeHeaderView.size_height;
        // 5. 底部
        self.bottomView.orgin_y = kScreenBounds.size.height;
        // 6. k 卡
        self.myKCardImageView.alpha = 0;
    } completion:^(BOOL finished) {
        self.myKCardImageView.alpha = 1;
        self.myKCardImageView.hidden = YES;
        [self.navigationController popToRootViewControllerAnimated:NO];
    }];
}

#pragma mark - Delegate
#pragma mark 0.点击进行准备
-(void)challengeButtonClickToReady{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToReady];
}

#pragma mark 1.正在为您寻找对手……【20秒自动放弃】
// 1. 在寻找对手中，20秒没有进行匹配到，然后进行放弃【没有接口。弹出弹框】
-(void)challengeStatusFindingStatusTimerEndManager{                 // 如果没有匹配到就去请求接口
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToAdjustIsMating];
}

-(void)challengeStatusNotFindedManager{
    __weak typeof(self)weakSelf = self;
    PDChallengerGameEndAlertView *alertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:self.view.bounds alertWithType:alertTypeNoChallenger block:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){              // 继续寻找对手
            [strongSelf sendRequestToValideDataWithIsNext:NO];
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        } else if (buttonIndex == 1){       // 更换出征条件
            [strongSelf sendRequestTocancelBisai];
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        }
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alertView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}


#pragma mark 2.匹配成功后，30秒后如果没有进行下一步执行操作
// 比赛30秒内没有进行准备，弹出的alert 4.6.3
-(void)daojishiEndManagerWithStatus:(challengerStatus)challengerNotReady{          // 提示，你没有准备
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToCancelGameWithNotNotificationWithBlock:^(BOOL isReady,PDChallengerStatusModel *statusModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (statusModel){
            [strongSelf pushChallengingManagerWithModel:statusModel];
            [strongSelf.challengerRoleView showManagerWithChallengerSingleModel:strongSelf.tempChallengerStatusModel.emenge];
            
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [strongSelf confirmShowSliderWithStatus2];        // 比赛状态更新
            });
        } else {
            if (isReady){
                if (challengerNotReady == challengerStatusNotReadyMe){      // 如果。我没有准备
                    [PDLoginModel shareInstance].challengeStatus = challengerStatusNotReadyMe;
                    [strongSelf showAlertWithDaojishiEndManagerStatus:[PDLoginModel shareInstance].challengeStatus];
                } else {    // 对方准备，我没有准备
                    [strongSelf showAlertWithDaojishiEndManagerStatus:challengerStatusNotReadyMe];
                }
            } else {    // 对方没有准备
                if (challengerNotReady == challengerStatusNotReadyTo){      // 对方没有准备
                    [PDLoginModel shareInstance].challengeStatus = challengerStatusNotReadyTo;
                    [strongSelf showAlertWithDaojishiEndManagerStatus:[PDLoginModel shareInstance].challengeStatus];
                } else if (challengerNotReady == challengerStatusNotReadyMe){
                    [PDLoginModel shareInstance].challengeStatus = challengerStatusNotReadyMe;
                    [strongSelf showAlertWithDaojishiEndManagerStatus:[PDLoginModel shareInstance].challengeStatus];
                }
            }
        }
    }];
}

#pragma mark 3. 匹配成功后，比赛将要开始，有3秒停留时间
-(void)challengeWillBeginWithTime:(NSInteger)beginWithTime{
    if (self.alertView){
        [self.alertView dismissAllAlertWithCompletion:NULL];
    }
    
    PDChallengerGameEndAlertView *numberAlertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:self.view.bounds alertWithTime:beginWithTime];
    self.alertView = [[PDAlertView alloc]initWithCustomView:numberAlertView dismissWhenTouchedBackground:YES];
    [self.alertView show];
}

#pragma mark 4. 3秒停留时间之后开始执行的代理方法
-(void)challengeStartManager{
    [self.alertView dismissAllAlertWithCompletion:NULL];
    // 比赛已经开始
    [PDLoginModel shareInstance].challengeStatus = challengerStatusChallenging;
    [self.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
}

#pragma mark 5. 游戏开始之后每秒进行执行
-(void)challengingManagerWithTime:(NSInteger)time{
    //    if (time > 10){
    //        if (self.alertView){
    //            [self.alertView dismissAllAlertWithCompletion:NULL];
    //        }
    //    }
}


#pragma mark 6. 游戏开始之后点击结算方法
-(void)challengingJiesuanManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToEndChallenge];
}

#pragma mark 7.结算
-(void)challengeButtonClickEnd{
    NSLog(@"123");
}

#pragma mark 举报按钮
-(void)quicklyReportButtonClickManager{
    __weak typeof(self)weakSelf = self;
    PDChallengerReportView *alert = [[PDChallengerReportView alloc]initWithFrame:CGRectMake(0, 0, 280, 300) successBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf quicklyReportButtonClickManagerSuccess];
    } failBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alert dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

#pragma mark - Show Alert
#pragma mark 举报
-(void)quicklyReportButtonClickManagerSuccess{
    if (self.alertView){
        [self.alertView dismissAllAlertWithCompletion:NULL];
    }
    __weak typeof(self)weakSelf = self;
    PDChallengerReportView *alert = [[PDChallengerReportView alloc]initWithFrame:CGRectMake(0, 0, 280, 300) reportSuccessBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alert dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

#pragma mark 倒计时结束 判断谁没有准备弹框
-(void)showAlertWithDaojishiEndManagerStatus:(challengerStatus)challengerNotReady{          // 提示，你没有准备
    __weak typeof(self)weakSelf = self;
    alertType alertType;
    if (challengerNotReady == challengerStatusNotReadyMe){
        alertType = alertTypeNotReadyMe;
    } else if (challengerNotReady == challengerStatusNotReadyTo){
        alertType = alertTypeNotReadyTo;
    }
    
    PDChallengerGameEndAlertView *alertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:self.view.bounds alertWithType:alertType block:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){              // 继续寻找对手
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
            // 1. 验证一下
            [strongSelf sendRequestToValideDataWithIsNext:NO];
        } else if (buttonIndex == 1){       // 更换出征条件
            [strongSelf sendRequestTocancelBisai];
        }
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alertView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

#pragma mark k8 状态弹框
-(void)showWinAlertK8Manager{
    [self showKingView];
}

#pragma mark 显示胜利弹框
-(void)showWinAlertOtherManagerWithKCard:(kCardType)card buttonClock:(void(^)(NSInteger buttonIndex))block{
    __weak typeof(self)weakSelf = self;
    PDChallengerGameEndAlertView *alertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:self.view.bounds alertWithChallengeEndType:challengeEndVictory currentKCard:card integral:0 buttonArr:@[@"继续下一轮",@"领赏",@"我的赛事"] block:^(NSInteger buttonIndex) {
        if(!weakSelf){
            return ;
        }
        if (block){
            block(buttonIndex);
        }
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alertView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

#pragma mark 显示失败弹框
-(void)showFailAlertOtherManagerWithModel:(PDChallengerStatusModel *)model buttonClock:(void(^)(NSInteger buttonIndex))block{
    PDChallengerGameEndAlertView *alertView;
    __weak typeof(self)weakSelf = self;
    if (model.result.isNext){       // 有同级k卡
        alertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:self.view.bounds alertWithChallengeEndType:challengeEndFail currentKCard:model.result.current_level integral:model.result.points buttonArr:@[@"再次出征",@"我的赛事",@"获取赛事资格",@"免费补赛"] block:^(NSInteger buttonIndex) {
            if (!weakSelf){
                return ;
            }
            if (block){
                block(buttonIndex);
            }
        }];
    } else {
        alertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:self.view.bounds alertWithChallengeEndType:challengeEndFail currentKCard:model.result.current_level integral:model.result.points buttonArr:@[@"我的赛事",@"获取赛事资格",@"免费补赛"] block:^(NSInteger buttonIndex) {
            if (!weakSelf){
                return ;
            }
            if (block){
                block(buttonIndex);
            }
        }];
    }
    self.alertView = [[JCAlertView alloc]initWithCustomView:alertView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

#pragma mark 更换出征条件按钮
-(void)changeChallengeManager:(void(^)())block{
    objc_setAssociatedObject(self, &changeChallengeManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

// 房间满了，弹出弹框
-(void)homeHotAlert{
    if (self.backLastPageBlock){
        self.backLastPageBlock(alertTypeNoHome,@"");
    }
    [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
    [self dismissAnimation];
    [self.bottomView cleanTimer];
    [self.alertView dismissAllAlertWithCompletion:NULL];
    
    
    PDChallengerGameEndAlertView *alertView = [[PDChallengerGameEndAlertView alloc] initWithFrame:self.view.bounds alertWithType:alertTypeNoHome block:NULL];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alertView dismissWhenTouchedBackground:YES];
    [self.alertView show];
}

// 弹出弹框
-(void)showErrWithWarn:(NSString *)warn{
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    __weak typeof(self)weakSelf = self;
    [showView alertWithTitle:@"错误" headerImage:[UIImage imageNamed:@"icon_report_warn"] titleDesc:warn buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
    }];
    self.alertView = [[JCAlertView alloc] initWithCustomView:showView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

-(void)showError:(NSString *)err{
    __weak typeof(self)weakSelf = self;
    PDAlertShowView *showView = [[PDAlertShowView alloc] init];
    [showView alertWithTitle:@"错误" headerImage:[UIImage imageNamed:@"icon_report_fail"] titleDesc:err buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
        [strongSelf.bottomView cleanTimer];
        [strongSelf dismissAnimation];
    }];
    self.alertView = [[JCAlertView alloc] initWithCustomView:showView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

#pragma mark - Push Manager
// 匹配成功
-(void)pushChallengePipeiManagerWithModel:(PDChallengerStatusModel *)statusModel{
    [self.activityLevelView stopAnimating];
    [self.activityNameView stopAnimating];
    [self.cardView stopAnimating];
    
    // 2. 修改当前我的数据
    [self.challengeHeaderView showAnimationWithText:statusModel.gameArea];
    // 3. 我方角色
    [self.roleImageView uploadChallengeImageWithURL:statusModel.myChallenge.avatar placeholder:nil callback:NULL];
    
    self.myKCardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)statusModel.myChallenge.card_leavel]];
    
    // 角色信息
    NSString *name = @"";
    if (statusModel.myChallenge.name.length){
        name = statusModel.myChallenge.name;
    } else if (statusModel.myChallenge.lol_name.length){
        name = statusModel.myChallenge.lol_name;
    } else {
        name = @"获取用户信息失败";
    }
    
    self.roleNameLabel.text = name;
    
    // 等级
    NSString *level = @"";
    if (statusModel.myChallenge.level.length){
        level = statusModel.myChallenge.level;
    } else if (statusModel.myChallenge.lol_max_rank.length){
        level = statusModel.myChallenge.lol_max_rank;
    } else {
        level = @"获取用户信息失败";
    }
    self.leaveNameLabel.text = level;
    // 【2.获取当前我的召唤师名字】
    CGSize roleNameSize = [self.roleNameLabel.text sizeWithCalcFont:self.roleNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.roleNameLabel.font])];
    CGFloat roleName_origin_x = self.roleImageView.orgin_x + (self.roleImageView.size_width - roleNameSize.width) / 2.;
    self.roleNameLabel.frame = CGRectMake(roleName_origin_x, CGRectGetMaxY(self.myKCardImageView.frame) + LCFloat(11), roleNameSize.width, [NSString contentofHeightWithFont:self.roleNameLabel.font]);
    
    // 【3.获取当前等级名称】
    CGSize leaveSize = [self.leaveNameLabel.text sizeWithCalcFont:self.leaveNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leaveNameLabel.font])];
    CGFloat leave_origin_x = self.roleImageView.orgin_x + (self.roleImageView.size_width - leaveSize.width) / 2.;
    self.leaveNameLabel.frame = CGRectMake(leave_origin_x, CGRectGetMaxY(self.roleNameLabel.frame) + LCFloat(11), leaveSize.width, [NSString contentofHeightWithFont:self.leaveNameLabel.font]);
    // 4. 敌方角色
    __weak typeof(self)weakSelf = self;
    [self.challengerRoleView.roleImageView uploadChallengeImageWithURL:statusModel.emenge.avatar placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (!strongSelf.imageUploadProgress){
            strongSelf.imageUploadProgress = [[PDChallengerProgress alloc] init];
            strongSelf.imageUploadProgress.radius = LCFloat(100);
            strongSelf.imageUploadProgress.progressBorderThickness = -10;
            strongSelf.imageUploadProgress.trackColor = [UIColor whiteColor];
            strongSelf.imageUploadProgress.progressColor = [UIColor whiteColor];
            strongSelf.imageUploadProgress.imageToUpload = image;
            [strongSelf.imageUploadProgress show];
        } else {
            strongSelf.imageUploadProgress.imageToUpload = image;
            [strongSelf.imageUploadProgress show];
        }
        [self performSelector:@selector(showSliderWithAnimationWithModel:) withObject:statusModel.emenge afterDelay:1];
    }];
}

#pragma mark 确认战斗
-(void)pushChallengingManagerWithModel:(PDChallengerStatusModel *)statusModel{
    // 1. 保存当前时间
    [PDTool userDefaulteWithKey:CURRENT_CHALLENGING_TIME_KEY Obj:[NSString stringWithFormat:@"%li",(long)statusModel.battle_end_time     ]];
    
    //1. vs
    self.vsFixedLabel.morphingEffect = LTMorphingEffectAnvil;
    self.vsFixedLabel.text = @"VS";
    self.vsFixedLabel.hidden = NO;
    
    // 2. 文字
    [PDLoginModel shareInstance].challengeStatus = challengerStatusReady;
    [self.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
    [self performSelector:@selector(pushChallengingManager1WithModel:) withObject:statusModel afterDelay:.8f];
}

#pragma mark 比赛结算完成
-(void)pushChallengingEndManagerWithModel:(PDChallengerStatusModel *)model{     // 比赛结算中
     [self.alertView dismissAllAlertWithCompletion:NULL];
    [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
    __weak typeof(self)weakSelf = self;
    if (model.result.win == YES){       // 胜利
        if (model.result.current_level == kCardType8){          // k8卡
            [self showWinAlertK8Manager];
        } else {
            [self showWinAlertOtherManagerWithKCard:model.result.current_level buttonClock:^(NSInteger buttonIndex) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (buttonIndex == 0){              // 【继续下一轮】
                    [strongSelf sendRequestToValideDataWithIsNext:YES];
                } else if (buttonIndex == 1){       // 领赏
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                    [PDChallengeDirectManager directToRewardViewController];
                } else if (buttonIndex == 2){       // 我的赛事
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                    [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
                    [strongSelf dismissAnimation];
                    [strongSelf.bottomView cleanTimer];
                }
                [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
            }];
        }
    } else {                                        // 失败
        [self showFailAlertOtherManagerWithModel:model buttonClock:^(NSInteger buttonIndex) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (model.result.isNext){               // 有同级k卡
                if (buttonIndex == 0){
                    [strongSelf sendRequestToValideDataWithIsNext:NO];
                } else if (buttonIndex == 1) {      // dismiss
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                    [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
                    [strongSelf dismissAnimation];
                    [strongSelf.bottomView cleanTimer];
                } else if (buttonIndex == 2){       // 获取赛事资格
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                    [PDChallengeDirectManager directToPayViewController];
                } else if (buttonIndex == 3){       // 免费补赛
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                    [PDChallengeDirectManager directToInviteViewController];
                }
                [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
            } else {
                if (buttonIndex == 0){          //  NSLog(@"我的赛事");
                    [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
                    [strongSelf dismissAnimation];
                    [strongSelf.bottomView cleanTimer];
                } else if (buttonIndex == 1){   // NSLog(@"获取赛事资格");
                    [PDChallengeDirectManager directToPayViewController];
                } else if (buttonIndex == 2){   // NSLog(@"免费补赛");
                    [PDChallengeDirectManager directToInviteViewController];
                }
                [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
            }
        }];
    }
}

#pragma mark 比赛失效
-(void)pushChallengeInvalidManager:(PDChallengerStatusModel *)model{
    __weak typeof(self)weakSelf = self;
    PDChallengerGameEndAlertView *challengerGameEndAlertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeGameCancelToFail block:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf= weakSelf;
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
        [strongSelf dismissAnimation];
        [strongSelf.bottomView cleanTimer];
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:challengerGameEndAlertView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

#pragma mark 比赛失效
-(void)pushChallengingCancelManagerWithModel:(PDChallengerStatusModel *)model{
    if (model.invalid == YES){          // 比赛失效
        [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
        __weak typeof(self)weakSelf = self;
        [weakSelf pushChallengeInvalidManager:model];
    } else {                            // 比赛胜利失败
        [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
        __weak typeof(self)weakSelf =self;
        [weakSelf pushChallengingEndManagerWithModel:model];
    }
}

#pragma mark 比赛强制结束
-(void)pushChallengingCancelEndManagerWithModel:(PDChallengerStatusModel *)model{
    __weak typeof(self)weakSelf = self;
    if (self.alertView){
        [self.alertView dismissAllAlertWithCompletion:NULL];
    }
    PDChallengerGameEndAlertView *alertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeGameCustomerFailEnd desc:model.desc isServer:YES isAgain:YES block:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
        [strongSelf dismissAnimation];
        [strongSelf.bottomView cleanTimer];
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alertView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

-(void)challengeWinNextGame{
    // 1. 获取当前比赛的卡
    
}


#pragma mark 确认战斗后修改房间名称和房间密码
-(void)pushChallengingManager1WithModel:(PDChallengerStatusModel *)statusModel{
    [self.bottomView animationWithChangeHomeInfo:statusModel.room_name homePwd:statusModel.room_pass];
}


-(void)showSliderWithAnimationWithModel:(PDChallengerSingleModel *)singleModel{
    // 1.找到对应的位置
    CGRect convertFrame = [self.challengerRoleView convertRect:self.challengerRoleView.roleImageView.frame toView:self.view.window];
    // 2. 创建一个副本
    UIImageView *animationImageView = [[UIImageView alloc]init];
    animationImageView.image = self.imageUploadProgress.imageToUpload;
    animationImageView.frame = self.imageUploadProgress.imageView.frame;
    [self.view.window addSubview:animationImageView];
    
    // 3. 执行消失方法
    [self.imageUploadProgress outroAnimation];
    // 4. 跳转
    [UIView animateWithDuration:.9  delay:.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        animationImageView.frame = convertFrame;
    } completion:^(BOOL finished) {
        animationImageView.hidden = YES;
        [animationImageView removeFromSuperview];
        // 找到选手
        [self.challengerRoleView showManagerWithPipeiSuccess:self.tempChallengerStatusModel.emenge];
    }];
    // 5. 修改当前的状态我方未准备
    [PDLoginModel shareInstance].challengeStatus = challengerStatusNotReadyMe;
    [self.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
}

-(void)confirmShowSliderWithStatus1{        // 5. 修改当前的状态我方未准备
    [PDLoginModel shareInstance].challengeStatus = challengerStatusNotReadyMe;
    [self.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
}

-(void)confirmShowSliderWithStatus2{        // 5. 修改当前的状态我方未准备
    [PDLoginModel shareInstance].challengeStatus = challengerStatusReady;
    [self.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
    [self.bottomView animationWithChangeHomeInfo:self.tempChallengerStatusModel.room_name homePwd:self.tempChallengerStatusModel.room_pass];
    [self.challengerRoleView showManagerWithChallengerSingleModel:self.tempChallengerStatusModel.emenge];
}

-(void)showKingView{
    __weak typeof(self)weakSelf = self;
    PDChallengerKingView *challenger = [[PDChallengerKingView alloc]initWithFrame:kScreenBounds lingshangBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [PDChallengeDirectManager directToRewardViewController];
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
    } myChallengerBlock:^{                                          // 返回到我的首页
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [PDLoginModel shareInstance].challengeStatus = challengerStatusNormal;
        [strongSelf dismissAnimation];
        [strongSelf.bottomView cleanTimer];
        [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:challenger dismissWhenTouchedBackground:YES];
    [self.alertView show];
}

#pragma mark - 游戏保持状态
-(void)keepingStatus{
    PDChallengerGameEndAlertView *alertView;
    __weak typeof(self)weakSelf = self;
    if (self.transferChallengerStatusModel.fail){
        if (self.transferChallengerStatusModel.times == 1){
            alertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeNoData desc:self.transferChallengerStatusModel.desc isServer:self.transferChallengerStatusModel.isServer isAgain:NO block:^(NSInteger buttonIndex) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (buttonIndex == 0){          // 再次结算
                    [strongSelf challengingJiesuanManager];
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                } else if (buttonIndex == 1){   // 申请客服介入
                    [strongSelf sendRequestToApplyCustomer];
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                } else if (buttonIndex == 2){   // 跳转到客服中心
                    PDCustomerServiceCenterViewController *serviceCenterViewController = [[PDCustomerServiceCenterViewController alloc]init];
                    serviceCenterViewController.popBlock = ^(){
                        [strongSelf.alertView show];
                    };
                    [strongSelf.navigationController pushViewController:serviceCenterViewController animated:YES];
                }
            }];
        } else {
            alertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeNoData desc:self.transferChallengerStatusModel.desc isServer:self.transferChallengerStatusModel.isServer isAgain:YES block:^(NSInteger buttonIndex) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (strongSelf.transferChallengerStatusModel.isServer){
                    [strongSelf sendRequestToComfirmWithCalculationEnd];
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                } else {        // 跳转到客服中心
                    PDCustomerServiceCenterViewController *serviceCenterViewController = [[PDCustomerServiceCenterViewController alloc]init];
                    serviceCenterViewController.popBlock = ^(){
                        [strongSelf.alertView show];
                    };
                    [strongSelf.navigationController pushViewController:serviceCenterViewController animated:YES];
                }
            }];
        }
        self.alertView = [[JCAlertView alloc]initWithCustomView:alertView dismissWhenTouchedBackground:NO];
        [self.alertView show];
    } else {
        if (self.transferChallengerStatusModel.status == 6){            // 1. 结算失败。等待客服处理
            [self keepingStatus6];
        } else if (self.transferChallengerStatusModel.status == 2){     // 2. 战斗中
            [self keepingStatus2];
        }
    }
    
    NSLog(@"%@",self.transferChallengerStatusModel);
}

-(void)keepingStatus6{
    [PDLoginModel shareInstance].challengeStatus = challengerStatusCalculationFail;     // 结算失败
    [self.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
    // 2 . 显示alertView
    __weak typeof(self)weakSelf = self;
    PDChallengerGameEndAlertView *alertBgView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeNoDataWithComputer block:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){          // 再次结算
            [strongSelf challengingJiesuanManager];
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        } else if (buttonIndex == 1){   // 申请客服介入
            [strongSelf sendRequestToApplyCustomer];
            [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
        } else if (buttonIndex == 2){   // 跳转到客服中心
            PDCustomerServiceCenterViewController *serviceCenterViewController = [[PDCustomerServiceCenterViewController alloc]init];
            serviceCenterViewController.popBlock = ^(){
                [strongSelf.alertView show];
            };
            [strongSelf.navigationController pushViewController:serviceCenterViewController animated:YES];
        }
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alertBgView dismissWhenTouchedBackground:YES];
    [self.alertView show];
}

-(void)calculationGameFail313{
    __weak typeof(self)weakSelf = self;
    PDChallengerGameEndAlertView *alertBgView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeCustomerAgain block:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){      // 客服中心
            PDCustomerServiceCenterViewController *serviceCenterViewController = [[PDCustomerServiceCenterViewController alloc]init];
            serviceCenterViewController.popBlock = ^(){
                [strongSelf.alertView show];
            };
            [strongSelf.navigationController pushViewController:serviceCenterViewController animated:YES];
        }
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alertBgView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

-(void)calculationGameFail314{
    __weak typeof(self)weakSelf = self;
    PDChallengerGameEndAlertView *alertBgView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeGameCustomerFailEnd block:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (buttonIndex == 0){      // 客服中心
            PDCustomerServiceCenterViewController *serviceCenterViewController = [[PDCustomerServiceCenterViewController alloc]init];
            [strongSelf.navigationController pushViewController:serviceCenterViewController animated:YES];
        }
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:alertBgView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

// 战斗中
-(void)keepingStatus2{
    self.transferChallengerStatusModel.emenge.card_leavel = self.transferChallengerStatusModel.card_level;
    self.transferChallengerStatusModel.myChallenge.card_leavel = self.transferChallengerStatusModel.card_level;
    // 1. 头部信息
    [self.challengeHeaderView showAnimationWithText:self.transferChallengerStatusModel.gameArea];
    // 2. 我的角色
    [self.roleImageView uploadChallengeImageWithURL:self.transferChallengerStatusModel.myChallenge.avatar placeholder:nil callback:NULL];
    self.myKCardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)self.transferChallengerStatusModel.myChallenge.card_leavel]];
    
    // 角色信息
    NSString *name = @"";
    if (self.transferChallengerStatusModel.myChallenge.name.length){
        name = self.transferChallengerStatusModel.myChallenge.name;
    } else if (self.transferChallengerStatusModel.myChallenge.lol_name){
        name = self.transferChallengerStatusModel.myChallenge.lol_name;
    } else {
        name = @"获取用户信息失败";
    }
    
    self.roleNameLabel.text = name;
    
    // 等级
    NSString *level = @"";
    if (self.transferChallengerStatusModel.myChallenge.level.length){
        level = self.transferChallengerStatusModel.myChallenge.level;
    } else if (self.transferChallengerStatusModel.myChallenge.lol_max_rank.length){
        level = self.transferChallengerStatusModel.myChallenge.lol_max_rank;
    } else {
        level = @"获取用户信息失败";
    }
    self.leaveNameLabel.text = level;
    
    // 【4.对方信息】
    self.tempChallengerStatusModel = self.transferChallengerStatusModel;
    [self.challengerRoleView showManagerWithChallengerSingleModel:self.tempChallengerStatusModel.emenge];
    // 5. 房间名称
    [PDLoginModel shareInstance].challengeStatus = challengerStatusChallenging;
    [self.bottomView bottomViewStatusManager:[PDLoginModel shareInstance].challengeStatus];
    [self.bottomView animationWithChangeHomeInfo:self.transferChallengerStatusModel.room_name homePwd:self.transferChallengerStatusModel.room_pass];
    
    // 4. 创建对方召唤师
    CGFloat kMargin_top = (self.bodyRect.size.height - kMargin_HeaderWidth - LCFloat(11) - [NSString contentofHeightWithFont:self.roleNameLabel.font] - LCFloat(11) - [NSString contentofHeightWithFont:self.leaveNameLabel.font]) / 2.;
    
    // 【1.获取当前我的头像的位置】
    self.roleImageView.frame = CGRectMake(LCFloat(28), self.challengeHeaderView.size_height + kMargin_top, kMargin_HeaderWidth ,kMargin_HeaderWidth);
    // 【1.1获取我当前kCard位置】
    self.myKCardImageView.frame = CGRectMake(CGRectGetMaxX(self.roleImageView.frame) + LCFloat(11) - kCard_width, CGRectGetMaxY(self.roleImageView.frame) - kCard_height, kCard_width, kCard_height);
    
    // 【2.获取当前我的召唤师名字】
    CGSize roleNameSize = [self.roleNameLabel.text sizeWithCalcFont:self.roleNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.roleNameLabel.font])];
    CGFloat roleName_origin_x = self.roleImageView.orgin_x + (self.roleImageView.size_width - roleNameSize.width) / 2.;
    self.roleNameLabel.frame = CGRectMake(roleName_origin_x, CGRectGetMaxY(self.myKCardImageView.frame) + LCFloat(11), roleNameSize.width, [NSString contentofHeightWithFont:self.roleNameLabel.font]);
    
    // 【3.获取当前等级名称】
    CGSize leaveSize = [self.leaveNameLabel.text sizeWithCalcFont:self.leaveNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leaveNameLabel.font])];
    CGFloat leave_origin_x = self.roleImageView.orgin_x + (self.roleImageView.size_width - leaveSize.width) / 2.;
    self.leaveNameLabel.frame = CGRectMake(leave_origin_x, CGRectGetMaxY(self.roleNameLabel.frame) + LCFloat(11), leaveSize.width, [NSString contentofHeightWithFont:self.leaveNameLabel.font]);
    
    self.roleNameLabel.frame = CGRectMake(roleName_origin_x, CGRectGetMaxY(self.myKCardImageView.frame) + LCFloat(11), roleNameSize.width, [NSString contentofHeightWithFont:self.roleNameLabel.font]);
    
    self.leaveNameLabel.frame = CGRectMake(leave_origin_x, CGRectGetMaxY(self.roleNameLabel.frame) + LCFloat(11), leaveSize.width, [NSString contentofHeightWithFont:self.leaveNameLabel.font]);
    
    // 【4.头部】
    self.challengeHeaderView.orgin_y = 0;
    // 5. 底部
    self.bottomView.orgin_y = kScreenBounds.size.height - self.bottomView.size_height;
    
    // 6. 卡片
    self.windowCardView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(84)) / 2., (kScreenBounds.size.height - LCFloat(108)) / 2., LCFloat(84), LCFloat(108));
}

-(void)calculationEndWithStatusModel:(PDChallengerStatusModel *)statusModel{
    PDChallengerGameEndAlertView *alertView;
    __weak typeof(self)weakSelf  = self;
    if (statusModel.fail){
        if (statusModel.times == 1){
            alertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeNoData desc:statusModel.desc isServer:statusModel.isServer isAgain:NO block:^(NSInteger buttonIndex) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (buttonIndex == 0){          // 再次结算
                    [strongSelf challengingJiesuanManager];
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                } else if (buttonIndex == 1){   // 申请客服介入
                    [strongSelf sendRequestToApplyCustomer];
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                } else if (buttonIndex == 2){   // 跳转到客服中心
                    PDCustomerServiceCenterViewController *serviceCenterViewController = [[PDCustomerServiceCenterViewController alloc]init];
                    serviceCenterViewController.popBlock = ^(){
                        [strongSelf.alertView show];
                    };
                    [strongSelf.navigationController pushViewController:serviceCenterViewController animated:YES];
                }
            }];
        } else {
            alertView = [[PDChallengerGameEndAlertView alloc]initWithFrame:kScreenBounds alertWithType:alertTypeNoData desc:statusModel.desc isServer:statusModel.isServer isAgain:YES block:^(NSInteger buttonIndex) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (statusModel.isServer){
                    [strongSelf sendRequestToComfirmWithCalculationEnd];
                    [strongSelf.alertView dismissAllAlertWithCompletion:NULL];
                } else {        // 跳转到客服中心
                    PDCustomerServiceCenterViewController *serviceCenterViewController = [[PDCustomerServiceCenterViewController alloc]init];
                    serviceCenterViewController.popBlock = ^(){
                        [strongSelf.alertView show];
                    };
                    [strongSelf.navigationController pushViewController:serviceCenterViewController animated:YES];
                }
            }];
        }
        self.alertView = [[JCAlertView alloc]initWithCustomView:alertView dismissWhenTouchedBackground:NO];
        [self.alertView show];
    } else {
        [PDLoginModel shareInstance].challengeStatus = challengerStatusCanEndGame;
        [self pushChallengingEndManagerWithModel:statusModel];
    }
}

-(void)keepingStatusMainManager{
    if (self.transferChallengerStatusModel){        // 保持状态
        self.tempChallengerStatusModel = self.transferChallengerStatusModel;
        __weak typeof(self)weakSelf = self;
        // 保存当前k卡
        [weakSelf adjustKCardWithCardLevel:weakSelf.transferChallengerStatusModel.myChallenge.card_leavel block:^(PDChallengeCardSingleModel *cardSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (cardSingleModel){
                if (!strongSelf.transferVoucherSelectedMutableArr.count){
                    NSArray *tempArr = @[cardSingleModel];
                    strongSelf.transferVoucherSelectedMutableArr = tempArr;
                }
            }
        }];
        // 保存当前角色
        NSString *lolName = @"";
        if (self.transferChallengerStatusModel.myChallenge.name.length){
            lolName = self.transferChallengerStatusModel.myChallenge.name;
        } else if (self.transferChallengerStatusModel.myChallenge.lol_name.length){
            lolName = self.transferChallengerStatusModel.myChallenge.lol_name;
        }
        [weakSelf adjustChallengerWithName:lolName block:^(PDChallengeRoleSingleModel *challengeSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (challengeSingleModel){
                if (!strongSelf.transferChallengerSelectedMutableArr.count){
                    NSArray *tempArr = @[challengeSingleModel];
                    strongSelf.transferChallengerSelectedMutableArr = tempArr;
                }
            }
        }];
        
        // 1. 保存上一个k卡状态
        [self keepingStatus];
        [self.cardView stopAnimating];
    } else {
        __weak typeof(self)weakSelf = self;
        [weakSelf showAnimation];                       // 开始动画
        [weakSelf sendRequestToChallenger];             // 开启战斗
    }
}

#pragma mark 通过k卡等级判断当前k卡
-(void)adjustKCardWithCardLevel:(kCardType)cardLevel block:(void(^)(PDChallengeCardSingleModel *cardSingleModel))block{
    PDChallengeCardListModel *challengeCardListModel = [[PDChallengeCardListModel alloc]init];
    __weak typeof(self)weakSelf = self;
    challengeCardListModel.requestParams = @{@"api":@"true"};
    [challengeCardListModel fetchWithPath:kChallengerKCard completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDChallengeCardSingleModel *cardTempSingleModel;
            for (PDChallengeCardSingleModel *cardSingleModel in challengeCardListModel.cardList){
                if (cardSingleModel.card_level == cardLevel){
                    cardTempSingleModel = cardSingleModel;
                }
            }
            if (cardTempSingleModel){
                block(cardTempSingleModel);
            } else {
                block(nil);
            }
        } else {

        }
    }];
}

#pragma mark 通过用户名字判断当前角色
-(void)adjustChallengerWithName:(NSString *)challengeName block:(void(^)(PDChallengeRoleSingleModel *challengeSingleModel))block{
    PDChallengeRoleListModel *roleListModel = [[PDChallengeRoleListModel alloc]init];
    __weak typeof(self)weakSelf = self;
    roleListModel.requestParams = @{@"api":@"true"};
    [roleListModel fetchWithPath:kChallengerLoLInfo completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            PDChallengeRoleSingleModel *challengeTempSingleModel;
            for (PDChallengeRoleSingleModel *roleSingleModel in roleListModel.roleList){
                if ([roleSingleModel.lol_name isEqualToString:challengeName]){
                    challengeTempSingleModel = roleSingleModel;
                }
            }
            if (challengeTempSingleModel){
                block(challengeTempSingleModel);
            } else {
                block(nil);
            }
        } else {
            
        }
    }];
}

-(void)sendRequestToChallengerWithNext{             // 获取下一张k卡
    __weak typeof(self)weakSelf = self;
    [weakSelf adjustKCardWithCardLevel:(self.transferChallengerStatusModel.myChallenge.card_leavel + 1) block:^(PDChallengeCardSingleModel *cardSingleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (cardSingleModel){
            NSArray *tempArr = @[cardSingleModel];
            strongSelf.transferVoucherSelectedMutableArr = tempArr;
            
            [strongSelf sendRequestToChallenger];
        }
    }];
}





@end
