//
//  PDChallengeChooseCardCell.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDChallengeCardSingleModel.h"
@interface PDChallengeChooseCardCell : UITableViewCell{
    BOOL isChecked;             /**< 判断是否选中*/
}

@property (nonatomic,assign)CGSize transferCellSize;                                    /**< 传递过来的size*/
@property (nonatomic,strong)PDChallengeCardSingleModel *transferCardSingleModel;        /**< 传递过来的卡片model*/
@property (nonatomic,strong)UIView *bgView;                                             /**< 背景图片*/

// 选中方法
-(void)setChecked:(BOOL)checked;

+(CGFloat)calculationCellHeight;
@end
