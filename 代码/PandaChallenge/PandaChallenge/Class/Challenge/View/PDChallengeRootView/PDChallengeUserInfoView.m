//
//  PDChallengeUserInfoView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeUserInfoView.h"
#import "PDChallengerProgressView.h"

@interface PDChallengeUserInfoView()
@property (nonatomic,strong)UILabel *leaveLabel;                /**< 等级名称*/
@property (nonatomic,strong)UILabel *probability;               /**< 胜率label*/
@property (nonatomic,strong)UILabel *expLabel;
@property (nonatomic,strong)PDChallengerProgressView *mainProgress; /**< 进度条*/

@end

@implementation PDChallengeUserInfoView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 头像
    self.headerImageView = [[PDImageView alloc]init];
    self.headerImageView.backgroundColor = [UIColor clearColor];
    self.headerImageView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(80)) / 2., LCFloat(18), LCFloat(80),LCFloat(80));
    [self addSubview:self.headerImageView];
    
    // 2. 创建name
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.font = [[UIFont systemFontOfCustomeSize:15.]boldFont];
    self.nameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.headerImageView.frame) + LCFloat(6), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.nameLabel];
    
    // 3. 创建expLabel
    self.expLabel = [[UILabel alloc]init];
    self.expLabel.font = [UIFont systemFontOfCustomeSize:11];
    self.expLabel.text = @"EXP";
    self.expLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.expLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.expLabel];
    CGSize contentOfExpSize = [self.expLabel.text sizeWithCalcFont:self.expLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.expLabel.font])];

    // 4. 创建进度条
    self.mainProgress = [[PDChallengerProgressView alloc]initWithFrame:CGRectMake(100, 100, 98, 7)];
    
    [self.mainProgress animationWithProgress:[PDLoginModel shareInstance].fight_times / 300.];
    [self addSubview:self.mainProgress];
    
    // 4.1 重置frame
    CGFloat kMarginX = (kScreenBounds.size.width - contentOfExpSize.width - LCFloat(11) - LCFloat(98)) / 2.;
    self.expLabel.frame = CGRectMake(kMarginX, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(10), contentOfExpSize.width, [NSString contentofHeightWithFont:self.expLabel.font]);
    self.mainProgress.frame = CGRectMake(CGRectGetMaxX(self.expLabel.frame) + LCFloat(11), self.expLabel.orgin_y + ([NSString contentofHeightWithFont:self.expLabel.font] - LCFloat(7)) / 2., LCFloat(98), LCFloat(7));
    
    // 3. 创建场
    self.probability = [[UILabel alloc]init];
    self.probability.textAlignment = NSTextAlignmentCenter;
    self.probability.backgroundColor = [UIColor clearColor];
    self.probability.font = [UIFont systemFontOfCustomeSize:14.];
    self.probability.frame = CGRectMake(0, CGRectGetMaxY(self.expLabel.frame) + LCFloat(6), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.probability.font]);
    [self addSubview:self.probability];
    
    // 4. 创建线
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, self.bounds.size.height - .5f, kScreenBounds.size.width, .5f);
    [self addSubview:lineView];
}

#pragma mark - 进行更新数据操作
-(void)animationWithInfo{
    // 1. 头像
    [self.headerImageView uploadChallengeImageWithURL:[PDLoginModel shareInstance].avatar placeholder:nil callback:NULL];
    // 2. 名字
    self.nameLabel.text = [PDLoginModel shareInstance].name;
    // 3. 进度条
    CGFloat progress = [PDLoginModel shareInstance].fight_times / 300.;
    [self.mainProgress animationWithProgress: progress];
    // 4. 胜场
    self.probability.text = [NSString stringWithFormat:@"胜场:%li",(long)[PDLoginModel shareInstance].wins];
}

@end
