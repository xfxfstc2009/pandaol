//
//  PDChallengeChooseRoleCell.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 选择角色cell
#import <UIKit/UIKit.h>
#import "PDEditCell.h"
#import "PDChallengeRoleSingleModel.h"              // 当前的召唤师model
@interface PDChallengeChooseRoleCell : PDEditCell{
    BOOL isChecked;             /**< 判断是否选中*/
}

@property (nonatomic,strong)UILabel *roleNameLabel;          /**< 角色名字*/
@property (nonatomic,strong)UILabel *locationLabel;          /**< 地址名称*/
@property (nonatomic,strong)UILabel *leaveLabel;             /**< 等级名称*/
@property (nonatomic,strong)UIImageView *bgView;             /**< 显示背景的view */

@property (nonatomic,assign)CGSize transferCellSize;         /**< 传递的cell 高度*/

@property (nonatomic,assign)EditTableViewCellState normalStatus;

@property (nonatomic,strong)PDChallengeRoleSingleModel *transferChallengeRoleSingleModel;       /**< 传递过来的model */
+(CGFloat)calculationCellHeight;            //  cell 高度
-(void)tapManagerBlock:(void(^)())block;
-(void)animationToEditingMode:(BOOL)isEditing;

// 选中方法
-(void)setChecked:(BOOL)checked;

@end
