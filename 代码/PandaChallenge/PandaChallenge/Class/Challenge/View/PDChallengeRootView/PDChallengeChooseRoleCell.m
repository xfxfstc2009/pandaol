//
//  PDChallengeChooseRoleCell.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeChooseRoleCell.h"
#import <objc/runtime.h>

static char longPresskey;
typedef void(^longPressBlock)();

@interface PDChallengeChooseRoleCell()
@property (nonatomic,strong)UILongPressGestureRecognizer *longPressGesture;
@property (nonatomic,assign)BOOL isFirstTap;                      /**< 判断是否长按*/

@property (nonatomic,copy)longPressBlock longPressBlock;
@property (nonatomic,strong)UIButton *addButton;                /**< 添加按钮*/
@end

@implementation PDChallengeChooseRoleCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier tableView:(UITableView *)tableView buttonNameArray:(NSArray *)buttonNameArray buttonCompletion:(void (^)(NSInteger buttonIndex,UIButton *button))buttonCompletion {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier tableView:tableView buttonNameArray:buttonNameArray buttonCompletion:buttonCompletion];
    
    if (self){
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
        [self createView];
    }
    return self;
}

#pragma mark -  createView
-(void)createView{
    // 1. 创建背景
    self.bgView = [[UIImageView alloc]init];
    self.bgView.image = [PDTool stretchImageWithName:@"common_card_background_nor"];
    self.backgroundImageView = self.bgView;
    
    // 1. 创建名称
    self.roleNameLabel = [[UILabel alloc]init];
    self.roleNameLabel.backgroundColor = [UIColor clearColor];
    self.roleNameLabel.font = [UIFont systemFontOfCustomeSize:14.];
    [self.subContentView addSubview:self.roleNameLabel];
    
    // 2. 创建区名称
    self.locationLabel = [[UILabel alloc]init];
    self.locationLabel.backgroundColor = [UIColor clearColor];
    self.locationLabel.font = [UIFont systemFontOfCustomeSize:13.];
    self.locationLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self.subContentView addSubview:self.locationLabel];
    
    // 3. 创建等级名称
    self.leaveLabel = [[UILabel alloc]init];
    self.leaveLabel.backgroundColor = [UIColor clearColor];
    self.leaveLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.leaveLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self.subContentView addSubview:self.leaveLabel];
    
    // 4. 添加长按事件
    self.longPressGesture = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
    self.longPressGesture.minimumPressDuration = 1.0;
    [self.subContentView addGestureRecognizer:self.longPressGesture];
    
    // 5. 创建addImg
    self.addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.addButton.userInteractionEnabled = NO;
    [self.addButton setImage:[UIImage imageNamed:@"challenge_add"] forState:UIControlStateNormal];
    [self addSubview:self.addButton];
}

-(void)setTransferCellSize:(CGSize)transferCellSize{
    _transferCellSize = transferCellSize;
    self.customCellSize = _transferCellSize;
}

-(void)setTransferChallengeRoleSingleModel:(PDChallengeRoleSingleModel *)transferChallengeRoleSingleModel{
    _transferChallengeRoleSingleModel = transferChallengeRoleSingleModel;
    // 1. 等级
    self.leaveLabel.text = transferChallengeRoleSingleModel.lol_max_rank.length?transferChallengeRoleSingleModel.lol_max_rank:@"30级";
    CGSize contentOfLeaver = [self.leaveLabel.text sizeWithCalcFont:self.leaveLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leaveLabel.font])];
    self.leaveLabel.frame = CGRectMake(self.transferCellSize.width - LCFloat(11) - contentOfLeaver.width, LCFloat(11), contentOfLeaver.width, [NSString contentofHeightWithFont:self.leaveLabel.font]);
    
    // 1. 名称
    self.roleNameLabel.text = transferChallengeRoleSingleModel.lol_name;
    self.roleNameLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), self.leaveLabel.orgin_x - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.roleNameLabel.font]);
    
    // 2. 区
    self.locationLabel.text = transferChallengeRoleSingleModel.server_name;
    self.locationLabel.frame = CGRectMake(self.roleNameLabel.orgin_x, CGRectGetMaxY(self.roleNameLabel.frame) + LCFloat(11), self.roleNameLabel.size_width, [NSString contentofHeightWithFont:self.locationLabel.font]);
    
    // 3. 创建按钮
    self.addButton.frame = CGRectMake(0, 0, self.transferCellSize.width, self.transferCellSize.height);
    
    if ([transferChallengeRoleSingleModel.lol_name isEqualToString:@"添加召唤师"] && [transferChallengeRoleSingleModel.other_name isEqualToString:@"游戏大区"]){
        self.bgView.hidden = YES;
        self.leaveLabel.hidden = YES;
        self.roleNameLabel.hidden = YES;
        self.locationLabel.hidden = YES;
        self.addButton.hidden = NO;
    } else {
        self.bgView.hidden = NO;
        self.leaveLabel.hidden = NO;
        self.roleNameLabel.hidden = NO;
        self.locationLabel.hidden = NO;
        self.addButton.hidden = YES;
    }
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:14.]];
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13.]];
    cellHeight += LCFloat(11);
    return cellHeight;
}


#pragma mark - 长按事件
-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer{
    if(gestureRecognizer.state == UIGestureRecognizerStateBegan)    {
        self.isFirstTap = YES;
    } else if(gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        self.isFirstTap = YES;
    } else if(gestureRecognizer.state == UIGestureRecognizerStateChanged) {
        if (self.isFirstTap == YES){
            self.isFirstTap = NO;
            self.longPressBlock = objc_getAssociatedObject(self, &longPresskey);
            if (self.longPressBlock){
                self.longPressBlock();
            }
        }
    }
}

-(void)tapManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &longPresskey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 拉伸图片
+ (UIImage *)stretchImageWithName:(NSString *)name {
    UIImage *image = [UIImage imageNamed:name];
    return [image stretchableImageWithLeftCapWidth:image.size.width * 0.5 topCapHeight:image.size.height * 0.5];
}

#pragma mark - 进行编辑模式
-(void)animationToEditingMode:(BOOL)isEditing{
    if (isEditing){         // 进行编辑模式
        self.leaveLabel.hidden = YES;
    } else {                // 退出编辑模式
        self.leaveLabel.hidden = NO;
    }
}

#pragma mark - 选中方法
- (void)setChecked:(BOOL)checked{
    if (checked) {
        self.backgroundColor = [UIColor clearColor];
        self.bgView.image = [PDTool stretchImageWithName:@"common_card_background_hlt"];
    } else {
        self.backgroundColor = [UIColor clearColor];
        self.bgView.image = [PDTool stretchImageWithName:@"common_card_background_nor"];
    }
    isChecked = checked;
}


@end
