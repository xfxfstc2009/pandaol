//
//  PDChallengeUserInfoView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 挑战信息头像
#import <UIKit/UIKit.h>
@interface PDChallengeUserInfoView : UIView

-(void)animationWithInfo;
@property (nonatomic,strong)PDImageView *headerImageView;       /**< 头像*/
@property (nonatomic,strong)UILabel *nameLabel;                 /**< 名称*/
@end
