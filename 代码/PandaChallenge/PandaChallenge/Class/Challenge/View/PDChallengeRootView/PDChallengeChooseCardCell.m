//
//  PDChallengeChooseCardCell.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeChooseCardCell.h"

@interface PDChallengeChooseCardCell ()
@property (nonatomic,strong)PDImageView *kCardImageView;
@property (nonatomic,strong)UIImageView *numberView;
@property (nonatomic,strong)UILabel *numberLabel;
@end

@implementation PDChallengeChooseCardCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1.背景图
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgView];
    
    // 2. 创建卡片
    self.kCardImageView = [[PDImageView alloc]init];
    self.kCardImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.kCardImageView];
    
    // 3. 创建数量
    self.numberView = [[UIImageView alloc]init];
    self.numberView.image = [UIImage imageNamed:@"challenge_kCard_number"];
    self.numberView.hidden = YES;
    [self addSubview:self.numberView];
    
    // 4. 创建k卡数量
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.numberLabel.textColor = [UIColor whiteColor];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.numberView addSubview:self.numberLabel];
}

-(void)setTransferCellSize:(CGSize)transferCellSize{
    _transferCellSize = transferCellSize;
}

-(void)setTransferCardSingleModel:(PDChallengeCardSingleModel *)transferCardSingleModel{
    _transferCardSingleModel = transferCardSingleModel;
    // 1.k卡
    self.kCardImageView.frame = CGRectMake(LCFloat(5),LCFloat(5),self.transferCellSize.width - LCFloat(10),self.transferCellSize.height - LCFloat(10));
    self.kCardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)transferCardSingleModel.card_level]];
    // 2.创建数量
    self.numberView.frame = CGRectMake(self.transferCellSize.width - LCFloat(10) - LCFloat(13), CGRectGetMaxY(self.kCardImageView.frame) - LCFloat(15), LCFloat(20), LCFloat(20));
    if ((transferCardSingleModel.card_num != 0) && (transferCardSingleModel.card_num != 1)){
        self.numberView.hidden = NO;
        self.numberLabel.text = [NSString stringWithFormat:@"X%li",(long)transferCardSingleModel.card_num];
        self.numberLabel.frame = self.numberView.bounds;
        self.numberLabel.adjustsFontSizeToFitWidth = YES;
    } else {
        self.numberView.hidden = YES;
    }
}

#pragma mark - 选中方法
- (void)setChecked:(BOOL)checked{
    if (checked) {
        self.kCardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_selected",(long)self.transferCardSingleModel.card_level]];
    } else {
     self.kCardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)self.transferCardSingleModel.card_level]];
    }
    isChecked = checked;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(65) + LCFloat(17);
}

@end
