//
//  PDChallengeHeaderView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeHeaderView.h"
#import "PandaChallenge-Swift.h"

@interface PDChallengeHeaderView()      // 头部信息

@property (nonatomic,strong)UILabel *titleNameLabel;            /**< 比赛名称*/
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *fixedLabel1;
@property (nonatomic,strong)LTMorphingLabel *dymicLabel;
@property (nonatomic,strong)UIActivityIndicatorView *activityView;   /**< 菊花*/

@end

@implementation PDChallengeHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createNavView];
    }
    return self;
}


#pragma mark - 创建头部信息
-(void)createNavView{
    // 1. 创建比赛名称
    self.titleNameLabel = [[UILabel alloc]init];
    self.titleNameLabel.font = [[UIFont systemFontOfCustomeSize:24] boldFont];
    self.titleNameLabel.frame = CGRectMake(0, LCFloat(44), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleNameLabel.font]);
    self.titleNameLabel.textAlignment = NSTextAlignmentCenter;
    self.titleNameLabel.text = @"PDK 英雄联盟 极地王";
    [self addSubview:self.titleNameLabel];
    
    // 2.0 创建fixedLabel
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.text = @"(";
    self.fixedLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.fixedLabel.textAlignment = NSTextAlignmentLeft;
    [self addSubview:self.fixedLabel];
    
    // 2.1 创建菊花
    self.activityView = [[UIActivityIndicatorView alloc]init];
    self.activityView.frame = CGRectMake(0, 0, LCFloat(30), LCFloat(30));
    [self.activityView startAnimating];
    [self.activityView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];//设置进度轮显示类型
    [self addSubview:self.activityView];
    
    // 2.2 创建dymicLabel
    self.dymicLabel = [[LTMorphingLabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.dymicLabel.textColor = [UIColor grayColor];
    self.dymicLabel.text = @"";
    [self addSubview:self.dymicLabel];
    
    // 2.创建比赛类别信息
    self.fixedLabel1 = [[UILabel alloc]init];
    self.fixedLabel1.backgroundColor = [UIColor clearColor];
    self.fixedLabel1.font = [UIFont systemFontOfCustomeSize:12.];
    self.fixedLabel1.frame = CGRectMake(0, CGRectGetMaxY(self.titleNameLabel.frame) + LCFloat(15), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.fixedLabel1.font]);
    self.fixedLabel1.textAlignment = NSTextAlignmentLeft;
    self.fixedLabel1.text = @"-自定义-自选模式)";
    [self addSubview:self.fixedLabel1];
    
    // 3. 重新计算
    [self auCalculationWidth];
    
    // 3. 添加pop
    self.popButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.popButton.backgroundColor = [UIColor clearColor];
    self.popButton.frame = CGRectMake(LCFloat(11), LCFloat(30), LCFloat(30), LCFloat(30));
    [self addSubview:self.popButton];
    
    // 4. 创建rightButton
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.backgroundColor = [UIColor clearColor];
    self.rightButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(30), LCFloat(30), LCFloat(30), LCFloat(30));
    [self addSubview:self.rightButton];
    
    // 4. 修改头部高度
    self.size_height = CGRectGetMaxY(self.fixedLabel1.frame);
}

#pragma mark - 计算宽度
-(void)auCalculationWidth{
    // 1. 计算fixed宽度
    CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    // 2. 计算dymic宽度
    CGSize fixedSize1 = [self.fixedLabel1.text sizeWithCalcFont:self.fixedLabel1.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel1.font])];
    
    // 3. 计算dymic
    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLabel.font])];
    
    if (self.activityView.isAnimating){
        self.activityView.size_width = LCFloat(30);
    } else {
        self.activityView.size_width = 0;
    }
    
    CGFloat kMargin_x = (kScreenBounds.size.width - fixedSize.width - fixedSize1.width - dymicSize.width - self.activityView.size_width - 2 * LCFloat(15)) / 2.;
    
    self.fixedLabel.frame = CGRectMake(kMargin_x, CGRectGetMaxY(self.titleNameLabel.frame) + LCFloat(15), fixedSize.width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    //3. 计算当前菊花位置
    self.activityView.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(15), self.fixedLabel.orgin_y + ([NSString contentofHeightWithFont:self.fixedLabel.font] - self.activityView.size_height) / 2., self.activityView.size_width, self.activityView.size_height);
    
    if (self.activityView.isAnimating){
        self.activityView.size_width = LCFloat(30);
    } else {
        self.activityView.size_width = 0;
    }
    
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.activityView.frame), self.fixedLabel.orgin_y, dymicSize.width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    
    // 4. 计算fixed
    self.fixedLabel1.frame = CGRectMake(CGRectGetMaxX(self.dymicLabel.frame) + LCFloat(15), self.fixedLabel.orgin_y, fixedSize1.width, [NSString contentofHeightWithFont:self.fixedLabel1.font]);
}


#pragma mark 显示动画
-(void)showAnimationWithText:(NSString *)text{
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
    
    if (![self.dymicLabel.text isEqualToString:text]){
        self.dymicLabel.text = text;
        self.dymicLabel.morphingEffect = LTMorphingEffectBurn;
    }
    
    [self auCalculationWidth];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.dymicLabel.text = [NSString stringWithFormat:@" %@",text];
    });
}

#pragma mark 隐藏动画
-(void)hidenAnimation{
    [self.activityView startAnimating];
    self.activityView.hidden = NO;

    
    [self auCalculationWidth];
}
@end
