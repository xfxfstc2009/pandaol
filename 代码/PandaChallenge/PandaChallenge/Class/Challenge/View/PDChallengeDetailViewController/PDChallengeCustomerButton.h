//
//  PDChallengeCustomerButton.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 自定义按钮
#import <UIKit/UIKit.h>
#import "PDChallengerStatusEnum.h"
@protocol PDChallengeCustomerButtonDelegate <NSObject>

-(void)buttonClickWithStatus:(challengerStatus)status;

@end

@interface PDChallengeCustomerButton : UIView
@property (nonatomic,strong)UITapGestureRecognizer *tapGesture;      /**< 手势点击方法*/
@property (nonatomic,strong)UIScrollView *mainScrollView;            /**< scrollView*/
@property (nonatomic,strong)UIView *pageOneView;                     /**< 第一个页面的view*/
@property (nonatomic,strong)UIActivityIndicatorView *activityView;   /**< 菊花*/
@property (nonatomic,strong)UILabel *contentLabel;                   /**< 显示的菊花*/
@property (nonatomic,strong)UIView *pageTwoView;                     /**< 第二个页面的view*/
@property (nonatomic,strong)UILabel *contentLabelTwo;                /**< 第二个页面的content*/

@property (nonatomic,assign)BOOL isEnable;
// Delegate
@property (nonatomic,weak)id<PDChallengeCustomerButtonDelegate> delegate;

#pragma mark 修改第一个文案
-(void)changeOneContent:(NSString *)content;

-(void)showLoadingAnimationWithText:(NSString *)text;                               // 显示加载动画
-(void)hidenAnimation;                                      // 隐藏动画
-(void)cleanTimer;

@end
