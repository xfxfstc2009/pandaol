//
//  PDChallengeBottomView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 底部的信息
#import <UIKit/UIKit.h>
#import "PDChallengerStatusEnum.h"

@protocol PDChallengeBottomViewDelegate <NSObject>

#pragma mark 0.点击进行准备
-(void)challengeButtonClickToReady;
#pragma mark 1.正在为您寻找对手……【20秒自动放弃】
-(void)challengeStatusFindingStatusTimerEndManager;
#pragma mark 2.匹配成功后，30秒后如果没有进行下一步执行操作
-(void)daojishiEndManagerWithStatus:(challengerStatus)challengerNotReady;
#pragma mark 3. 匹配成功后，比赛将要开始，有3秒停留时间
-(void)challengeWillBeginWithTime:(NSInteger)beginWithTime;                     // 将要开始战斗
#pragma mark 4. 3秒停留时间之后开始执行的代理方法
-(void)challengeStartManager;
#pragma mark 5. 游戏开始之后每秒进行执行
-(void)challengingManagerWithTime:(NSInteger)time;
#pragma mark 6. 游戏开始之后点击结算方法
-(void)challengingJiesuanManager;


-(void)challengeButtonClickEnd;                     // 当按钮点击之后进行结束

#pragma mark other 举报按钮
-(void)quicklyReportButtonClickManager;
@end



@interface PDChallengeBottomView : UIView
@property (nonatomic, weak) id<PDChallengeBottomViewDelegate> delegate;
@property (nonatomic,assign)challengerStatus status;                         /**< 当前状态*/

-(void)bottomViewStatusManager:(challengerStatus)status;
-(void)cleanTimer;

#pragma mark 修改当前的房间名字和房间密码
-(void)animationWithChangeHomeInfo:(NSString *)homeName homePwd:(NSString *)homePwd;

@end
