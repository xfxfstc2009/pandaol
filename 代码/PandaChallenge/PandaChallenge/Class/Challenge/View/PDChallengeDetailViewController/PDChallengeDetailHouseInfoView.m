//
//  PDChallengeDetailHouseInfoView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeDetailHouseInfoView.h"

@interface PDChallengeDetailHouseInfoView()
@property (nonatomic,strong)PDImageView *headerImageView;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UIView *lineView;

@end

@implementation PDChallengeDetailHouseInfoView

-(instancetype)initWithFrame:(CGRect)frame fixedString:(NSString *)fixedString {
    self = [super initWithFrame:frame];
    if (self){
        [self createViewWithfixedString:fixedString];
    }
    return self;
}

#pragma mark - 创建view
-(void)createViewWithfixedString:(NSString *)fixedString {
    // 1. 创建背景
    self.headerImageView = [[PDImageView alloc]init];
    self.headerImageView.backgroundColor = [UIColor clearColor];
    self.headerImageView.image = [PDTool stretchImageWithName:@"icon_challengerDetail_houseBg"];
    self.headerImageView.frame = self.bounds;
    [self addSubview:self.headerImageView];
    
    // 2. 创建label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont systemFontOfCustomeSize:15.];
    self.fixedLabel.text = fixedString;
    [self addSubview:self.fixedLabel];
    
    // 2.1计算文字长度
    CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, fixedSize.width, self.bounds.size.height);
    
    // 3. 创建line
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor lightGrayColor];
    self.lineView.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(11), LCFloat(7), LCFloat(1), self.bounds.size.height - 2 * LCFloat(7));
    [self addSubview:self.lineView];
    
    // 4. 创建dymicLabel
    self.dymicLabel = [[LTMorphingLabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.lineView.frame) + LCFloat(11), 0, self.bounds.size.width - CGRectGetMaxX(self.lineView.frame) - LCFloat(11) * 2, self.bounds.size.height);
    self.dymicLabel.textColor = [UIColor grayColor];
    self.dymicLabel.text = @"**********";
    self.dymicLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.dymicLabel];
}

-(void)changeFramge{
    self.headerImageView.frame = self.bounds;
    
    // 2.1计算文字长度
    CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, fixedSize.width, self.bounds.size.height);
    
    self.lineView.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(11), LCFloat(7), LCFloat(1), self.bounds.size.height - 2 * LCFloat(7));
    
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.lineView.frame) + LCFloat(11), 0, self.bounds.size.width - CGRectGetMaxX(self.lineView.frame) - 2 * LCFloat(11), self.bounds.size.height);
}

-(void)showAnimationWithStirng:(NSString *)animationString{
    self.dymicLabel.adjustsFontSizeToFitWidth = YES;
    self.dymicLabel.font = [[UIFont systemFontOfCustomeSize:20] boldFont];
    
    if (![self.dymicLabel.text isEqualToString:animationString]){
        self.dymicLabel.text = animationString;
        self.dymicLabel.morphingEffect = LTMorphingEffectBurn;
    }
}

-(void)stopAnimation{
    if (![self.dymicLabel.text isEqualToString:@"**********"]){
        self.dymicLabel.morphingEffect = LTMorphingEffectScale;
        self.dymicLabel.text = @"**********";
    }
    
    self.dymicLabel.font = [UIFont systemFontOfCustomeSize:14.];
}


@end
