//
//  PDChallengerRoleView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengerRoleView.h"
#import "PandaChallenge-Swift.h"                    // swift

@interface PDChallengerRoleView()
@property (nonatomic,strong)UIView *roleBgView;                 /**< role背景颜色*/
@property (nonatomic,strong)PDImageView *myKCardImageView;      /**< 我的k卡图*/
@property (nonatomic,strong)UIActivityIndicatorView *headerActivity;    /**< 头像菊花*/
@property (nonatomic,strong)LTMorphingLabel *roleNameLabel;             /**< 召唤师名称*/
@property (nonatomic,strong)LTMorphingLabel *leaveNameLabel;            /**< 召唤师当前等级*/

@end


@implementation PDChallengerRoleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - 创建view
-(void)createView{
    // 2.创建图片
    self.roleImageView = [[PDImageView alloc]init];
    self.roleImageView.backgroundColor = [UIColor clearColor];
    self.roleImageView.frame = CGRectMake(0, 0, LCFloat(122), LCFloat(122));
    self.roleImageView.clipsToBounds = YES;
    self.roleImageView.hidden = YES;
    self.roleImageView.layer.cornerRadius = self.roleImageView.size_height / 2.;
    [self addSubview:self.roleImageView];
    
    // 2.1 创建菊花
    self.headerActivity = [[UIActivityIndicatorView alloc]init];
    self.headerActivity.frame = self.roleImageView.frame;
    [self.headerActivity setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleGray];//设置进度轮显示类型
    [self addSubview:self.headerActivity];

    // 3. 创建卡片等级
    self.myKCardImageView = [[PDImageView alloc]init];
    self.myKCardImageView.backgroundColor = [UIColor clearColor];
    self.myKCardImageView.frame = CGRectMake(self.roleImageView.center_x - LCFloat(21), CGRectGetMaxY(self.roleImageView.frame) + LCFloat(11), kCard_width,kCard_height);
    self.myKCardImageView.frame = CGRectMake(CGRectGetMaxX(self.roleImageView.frame) + LCFloat(11) - kCard_width, CGRectGetMaxY(self.roleImageView.frame) - kCard_height, kCard_width, kCard_height);
    self.myKCardImageView.image = [UIImage imageNamed:@"challenge_kCard_3"];
    [self addSubview:self.myKCardImageView];
    
    self.roleNameLabel = [[LTMorphingLabel alloc]init];
    self.roleNameLabel.backgroundColor = [UIColor clearColor];
    self.roleNameLabel.font = [UIFont systemFontOfCustomeSize:16.];
    self.roleNameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.myKCardImageView.frame) + LCFloat(11), self.bounds.size.width, [NSString contentofHeightWithFont:self.roleNameLabel.font]);
    self.roleNameLabel.text = @"";
    self.roleNameLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.roleNameLabel];
    
    // 5.创建等级
    self.leaveNameLabel = [[LTMorphingLabel alloc]init];
    self.leaveNameLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.leaveNameLabel.text = @"";
    self.leaveNameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.roleNameLabel.frame) + LCFloat(11), self.bounds.size.width, [NSString contentofHeightWithFont:self.leaveNameLabel.font]);;
    self.leaveNameLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.leaveNameLabel];
    
    // 6. 修改当前的frame
    self.size_height = CGRectGetMaxY(self.leaveNameLabel.frame);
}


#pragma mark - 页面显示状态
-(void)viewShowManagerWithStatus:(challengerStatus)status{
    if (status == challengerStatusNormal){          // 正在寻找对手
        self.headerActivity.hidden = NO;
        [self.headerActivity startAnimating];
        
        self.roleImageView.hidden = YES;
        self.myKCardImageView.hidden = YES;
        self.leaveNameLabel.hidden = YES;
        self.roleNameLabel.hidden = YES;
        
    } else if (status == challengerStatusNotReadyMe){    // 找到对手
        self.headerActivity.hidden = YES;
        [self.headerActivity stopAnimating];
        self.roleImageView.image = [UIImage imageNamed:@"clouds"];
    }
}

#pragma mark 游戏开始
-(void)showManagerWithChallengerSingleModel:(PDChallengerSingleModel *)transferChallengerSingleModel{
    // 0.菊花隐藏
    self.headerActivity.hidden = YES;
    [self.headerActivity stopAnimating];
    
    // 1.加载头像
    self.roleImageView.hidden = NO;
    [self.roleImageView uploadChallengeImageWithURL:transferChallengerSingleModel.avatar placeholder:nil callback:NULL];

    // 2.角色名
    self.roleNameLabel.hidden = NO;
    
    // 3. 等级
    self.leaveNameLabel.hidden = NO;
    
    // 角色信息
    NSString *name = @"";
    if (transferChallengerSingleModel.name.length){
        name = transferChallengerSingleModel.name;
    } else if (transferChallengerSingleModel.lol_name.length){
        name = transferChallengerSingleModel.lol_name;
    } else {
        name = @"获取用户信息失败";
    }
    
    if (![self.roleNameLabel.text isEqualToString:name]){
        self.roleNameLabel.morphingEffect = LTMorphingEffectPixelate;
        self.roleNameLabel.text = name;
    }
    if (self.roleNameLabel.morphingEffect == LTMorphingEffectPixelate){
        self.roleNameLabel.morphingEffect = LTMorphingEffectSparkle;
        self.roleNameLabel.text = [NSString stringWithFormat:@"%@ ",name];
    }
    
    // 等级
    NSString *level = @"";
    if (transferChallengerSingleModel.level.length){
        level = transferChallengerSingleModel.level;
    } else if (transferChallengerSingleModel.lol_max_rank.length){
        level = transferChallengerSingleModel.lol_max_rank;
    } else {
        level = @"获取用户信息失败";
    }
    
    if (![self.leaveNameLabel.text isEqualToString:level]){
        self.leaveNameLabel.morphingEffect = LTMorphingEffectPixelate;
        self.leaveNameLabel.text = level;
    }
    if (self.leaveNameLabel.morphingEffect == LTMorphingEffectPixelate){
        self.leaveNameLabel.morphingEffect = LTMorphingEffectSparkle;
        self.leaveNameLabel.text = [NSString stringWithFormat:@"%@ ",level];
    }
    
    // 4. 卡片
    self.myKCardImageView.hidden = NO;
    self.myKCardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)transferChallengerSingleModel.card_leavel]];
}

#pragma mark 匹配成功
-(void)showManagerWithPipeiSuccess:(PDChallengerSingleModel *)transferChallengerSingleModel{
    // 0.菊花隐藏
    self.headerActivity.hidden = YES;
    [self.headerActivity stopAnimating];
    
    // 1.加载头像
    self.roleImageView.hidden = NO;
    [self.roleImageView uploadChallengeImageWithURL:transferChallengerSingleModel.avatar placeholder:nil callback:NULL];

    // 2.角色名
    self.roleNameLabel.hidden = NO;
    if (![self.roleNameLabel.text isEqualToString:@"*****"]){
        self.roleNameLabel.morphingEffect = LTMorphingEffectScale;
        self.roleNameLabel.text = @"*****";
    }
    
    // 3. 等级
    self.leaveNameLabel.hidden = NO;
    if (![self.leaveNameLabel.text isEqualToString:@"*****"]){
        self.leaveNameLabel.morphingEffect = LTMorphingEffectScale;
        self.leaveNameLabel.text = @"*****";
    }
    
    // 4. 卡片
    self.myKCardImageView.hidden = NO;
    self.myKCardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)transferChallengerSingleModel.card_leavel]];

}

@end
