//
//  PDChallengerProgress.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString * const IMAGE_UPLOAD_COMPLETED;

@interface PDChallengerProgress : UIView

@property (nonatomic) CGFloat progress;
@property (nonatomic) NSUInteger radius;
@property (nonatomic) NSUInteger progressBorderThickness;

@property (nonatomic, strong) UIImage *imageToUpload;
@property (nonatomic, strong) UIImageView *imageView;

@property (nonatomic, strong) UIColor *trackColor;
@property (nonatomic, strong) UIColor *progressColor;

@property (nonatomic) BOOL showOverlay;

- (void)show;
- (void)outroAnimation ;

@end
