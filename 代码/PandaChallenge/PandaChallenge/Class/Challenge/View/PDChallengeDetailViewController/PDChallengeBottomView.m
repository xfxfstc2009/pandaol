//
//  PDChallengeBottomView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeBottomView.h"
#import "PDChallengeDetailHouseInfoView.h"          // 房间名 & 房间密码
#import "PDChallengeCustomerButton.h"               // 按钮1

@interface PDChallengeBottomView()<PDChallengeCustomerButtonDelegate>
@property (nonatomic,strong)PDChallengeCustomerButton *button1;                 /**< 按钮1*/
@property (nonatomic,strong)UIButton *button2;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UIButton *guizeButton;
@property (nonatomic,strong)PDChallengeDetailHouseInfoView *housNameView;       /**< 房间view*/
@property (nonatomic,strong)PDChallengeDetailHouseInfoView *passwordView;       /**< 密码view*/

@property (nonatomic,strong)UILabel *daojishiFixedLabel1;   /**< 倒计时*/
@property (nonatomic,strong)UILabel *daojishiFixedLabel2;   /**< 倒计时2*/
@property (nonatomic,strong)UILabel *daojishiDymicLabel;    /**< 倒计时3*/
@property (nonatomic,strong)UIButton *quicklyReportButton;  /**< 快速举报*/
@property (nonatomic,assign)NSInteger daojishiTime;
@property (nonatomic,strong)NSTimer *daojishiTimer;
@end

@implementation PDChallengeBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建
    self.button1 = [[PDChallengeCustomerButton alloc]initWithFrame:CGRectMake(LCFloat(45), 0, kScreenBounds.size.width - 2 * LCFloat(45), LCFloat(44))];
    self.button1.delegate = self;
    self.button1.hidden = YES;
    [self addSubview:self.button1];
    
    // 2. 创建按钮2
    self.button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button2.frame = CGRectMake(self.button1.orgin_x, CGRectGetMaxY(self.button1.frame) + LCFloat(15), self.button1.size_width, self.button1.size_height);
    [self.button2 setTitle:@"按钮2" forState:UIControlStateNormal];
    self.button2.titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    self.button2.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
    self.button2.layer.cornerRadius = LCFloat(5);
    self.button2.hidden = YES;
    [self.button2 buttonWithBlock:^(UIButton *button) {
        NSLog(@"按钮2");
    }];
    [self addSubview:self.button2];
    
    // 3. 创建房间密码
    self.housNameView = [[PDChallengeDetailHouseInfoView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.button2.frame) + LCFloat(10), kScreenBounds.size.width - 2 * LCFloat(70), LCFloat(44)) fixedString:@"房间"];
    [self addSubview:self.housNameView];
    self.housNameView.hidden = YES;
    
    // 4. 创建密码
    self.passwordView = [[PDChallengeDetailHouseInfoView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.housNameView.frame) + LCFloat(10), self.housNameView.size_width, self.housNameView.size_height) fixedString:@"密码"];
    self.passwordView.hidden = YES;
    [self addSubview:self.passwordView];
    
    // 5. fixedLabel
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.fixedLabel.text = @"提示,确认战斗后15分钟内无法再次出征，战斗结束后请点击“战斗结束”按钮，以便进行战斗结算。";
    self.fixedLabel.numberOfLines = 0;
    self.fixedLabel.hidden = YES;
    [self addSubview:self.fixedLabel];
    
    // 6. 创建倒计时
    self.daojishiFixedLabel1 = [[UILabel alloc]init];
    self.daojishiFixedLabel1.backgroundColor = [UIColor clearColor];
    self.daojishiFixedLabel1.font = [UIFont systemFontOfCustomeSize:13.];
    self.daojishiFixedLabel1.text = @"请在";
    self.daojishiFixedLabel1.hidden = YES;
    [self addSubview:self.daojishiFixedLabel1];
    
    // 7. 创建倒计时fixed
    self.daojishiFixedLabel2 = [[UILabel alloc]init];
    self.daojishiFixedLabel2.backgroundColor = [UIColor clearColor];
    self.daojishiFixedLabel2.font = [UIFont systemFontOfCustomeSize:13.];
    self.daojishiFixedLabel2.text = @"秒内确认战斗";
    self.daojishiFixedLabel2.hidden = YES;
    [self addSubview:self.daojishiFixedLabel2];
    
    // 8.self.dymic
    self.daojishiDymicLabel = [[UILabel alloc]init];
    self.daojishiDymicLabel.textColor = [UIColor redColor];
    self.daojishiDymicLabel.backgroundColor = [UIColor clearColor];
    self.daojishiDymicLabel.font = [UIFont systemFontOfCustomeSize:13.];
    self.daojishiDymicLabel.hidden = YES;
    [self addSubview:self.daojishiDymicLabel];
    
    // 9. 快速举报
    self.quicklyReportButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.quicklyReportButton.backgroundColor = [UIColor clearColor];
    self.quicklyReportButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(130)) / 2., self.size_height - LCFloat(11) - LCFloat(30) , LCFloat(130), LCFloat(30));
    self.quicklyReportButton.titleLabel.font = [UIFont systemFontOfCustomeSize:12.];
    [self.quicklyReportButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.quicklyReportButton setTitle:@"快速举报" forState:UIControlStateNormal];
    [self addSubview:self.quicklyReportButton];
    __weak typeof(self)weakSelf = self;
    [weakSelf.quicklyReportButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.delegate quicklyReportButtonClickManager];
    }];
    self.quicklyReportButton.hidden = YES;
}

-(void)bottomViewStatusManager:(challengerStatus)status{
    // 1. 初始化
    if (self.daojishiTimer){
        [self.daojishiTimer invalidate];
        self.daojishiTimer = nil;
    }
    
    if (status == challengerStatusNormal){                          // 1. 显示正在寻找对手
        NSLog(@" // 1. 显示正在寻找对手");
        [self challengerStatusNormalManager];
    } else if (status == challengerStatusNotReadyMe){               // 2. 寻找到对手，本方没有准备
        NSLog(@" // 2. 寻找到对手，本方没有准备");
        [self challengerStatusNotReadyMeManager];
    } else if (status == challengerStatusNotReadyTo){               // 3. 寻找到对手，对方没有准备
        NSLog(@" // 3. 寻找到对手，对方没有准备");
        [self challengerStatusNotReadyToManager];
    } else if (status == challengerStatusReady){                    // 4. 双方都准备好了，变成确认战斗即将开始
        NSLog(@" // 4. 双方都准备好了，变成确认战斗即将开始");
        [self challengerStatusReadyManager];
    } else if (status == challengerStatusChallenging){              // 5. 对战开始,战斗中
        NSLog(@" // 5. 对战开始,战斗中");
        [self challengerStatusChallengingManager];
    } else if (status == challengerStatusCanEndGame){               // 6.比赛可以进行关闭状态
        NSLog(@" // 6.比赛可以进行关闭状态");
        [self challengerStatusCanEndGameManager];
    } else if (status == challengerStatusCalculationing){           // 7.结算中
        [self challengerStatusCalculationingManager];
    } else if (status == challengerStatusCalculationSuccess){       // 8.结算成功
    
    } else if (status == challengerStatusCalculationFail){          // 9.结算失败
        
    } else if (status == challengerStatusNotFind){                  // 10.比赛没有寻找到对手
    
    }
}

#pragma mark - 【1.正在寻找对手】
-(void)challengerStatusNormalManager{
    self.fixedLabel.hidden = YES;
    self.button2.hidden = YES;
    self.button1.hidden = NO;
    [self.button1 cleanTimer];
    self.housNameView.hidden = NO;
    self.passwordView.hidden = NO;
    self.housNameView.alpha = 1;
    self.passwordView.alpha = 1;
    self.daojishiDymicLabel.hidden = YES;
    self.daojishiFixedLabel1.hidden = YES;
    self.daojishiFixedLabel2.hidden = YES;
    self.quicklyReportButton.hidden = YES;
    
    // 1. 房间名显示
    self.housNameView.frame = CGRectMake(LCFloat(60), 0, kScreenBounds.size.width - 2 * LCFloat(60), LCFloat(44));
    self.housNameView.dymicLabel.text = @"********";
    [self.housNameView changeFramge];
    // 2. 密码
    self.passwordView.frame = CGRectMake(self.housNameView.orgin_x, CGRectGetMaxY(self.housNameView.frame) + LCFloat(20), self.housNameView.size_width, self.housNameView.size_height);
    self.passwordView.dymicLabel.text = @"********";
    [self.passwordView changeFramge];
    // 3. 寻找对手
    self.button1.orgin_y = CGRectGetMaxY(self.passwordView.frame) + LCFloat(30);
    // 4. 按钮修改文字，正在为您寻找对手
    [self.button1 changeOneContent:@"正在为您寻找对手"];
    self.button1.isEnable = NO;
    [self.button1 showLoadingAnimationWithText:@"正在为您寻找对手"];
    
    // 5. 开启计时器
    self.daojishiTime = 15;
    if (self.daojishiTimer){
        [self.daojishiTimer invalidate];
        self.daojishiTimer = nil;
    }
    self.daojishiTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(challengeStatusFindingStatusTimer) userInfo:nil repeats:YES];
}

#pragma mark 1.1正在为您寻找对手……【20秒自动放弃计时器方法】
-(void)challengeStatusFindingStatusTimer{
    NSLog(@"【20秒自动放弃计时器方法】%li",(long)self.daojishiTime);
    self.daojishiDymicLabel.text = [NSString stringWithFormat:@"%li",(long)self.daojishiTime];
    self.daojishiTime--;
    if (self.daojishiTime <= 0){
        if (self.daojishiTimer){
            [self.daojishiTimer invalidate];
            self.daojishiTimer = nil;
        }
        [self.delegate challengeStatusFindingStatusTimerEndManager];            // 20秒自动放弃计时器方法
    }
}

#pragma mark - 2.【我没有进行准备】
-(void)challengerStatusNotReadyMeManager{
    self.fixedLabel.hidden = YES;
    self.button2.hidden = YES;
    self.button1.hidden = NO;
    [self.button1 cleanTimer];
    self.housNameView.hidden = NO;
    self.passwordView.hidden = NO;
    self.housNameView.alpha = 1;
    self.passwordView.alpha = 1;
    self.daojishiDymicLabel.hidden = NO;
    self.daojishiFixedLabel1.hidden = NO;
    self.daojishiFixedLabel2.hidden = NO;
    self.quicklyReportButton.hidden = YES;
    
    // 1. 房间名显示
    self.housNameView.frame = CGRectMake(LCFloat(60), 0, kScreenBounds.size.width - 2 * LCFloat(60), LCFloat(44));
    [self.housNameView changeFramge];
    // 2. 密码
    self.passwordView.frame = CGRectMake(self.housNameView.orgin_x, CGRectGetMaxY(self.housNameView.frame) + LCFloat(20), self.housNameView.size_width, self.housNameView.size_height);
    [self.passwordView changeFramge];
    // 3. 寻找对手
    self.button1.orgin_y = CGRectGetMaxY(self.passwordView.frame) + LCFloat(30);
    // 4. 按钮修改文字，正在为您寻找对手
    [self.button1 hidenAnimation];
    [self.button1 changeOneContent:@"确认战斗"];
    self.button1.isEnable = YES;
    
    // 5 增加固定内容
    self.daojishiFixedLabel1.text = @"请在";
    self.daojishiFixedLabel2.text = @"秒内确认战斗";
    CGSize contentOfSize = [@"请在30秒内确认战斗" sizeWithCalcFont:self.daojishiFixedLabel1.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.daojishiFixedLabel1.font])];
    CGFloat daojishiMarginX = (kScreenBounds.size.width - contentOfSize.width - LCFloat(3) * 2) / 2.;
    CGSize daojishiSize1 = [self.daojishiFixedLabel1.text sizeWithCalcFont:self.daojishiFixedLabel1.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.daojishiFixedLabel1.font])];
    self.daojishiFixedLabel1.frame = CGRectMake(daojishiMarginX, CGRectGetMaxY(self.button1.frame) + LCFloat(10), daojishiSize1.width, [NSString contentofHeightWithFont:self.daojishiFixedLabel1.font]);
    CGSize daojishiSize2 = [@"30" sizeWithCalcFont:self.daojishiDymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.daojishiDymicLabel.font])];
    self.daojishiDymicLabel.frame = CGRectMake(CGRectGetMaxX(self.daojishiFixedLabel1.frame) + LCFloat(3), self.daojishiFixedLabel1.orgin_y, daojishiSize2.width, [NSString contentofHeightWithFont:self.daojishiDymicLabel.font]);
    CGSize daojishiSize3 = [self.daojishiFixedLabel2.text sizeWithCalcFont:self.daojishiFixedLabel2.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.daojishiFixedLabel2.font])];
    self.daojishiFixedLabel2.frame = CGRectMake(CGRectGetMaxX(self.daojishiDymicLabel.frame) + LCFloat(3), self.daojishiDymicLabel.orgin_y, daojishiSize3.width, [NSString contentofHeightWithFont:self.daojishiFixedLabel2.font]);
    // 5. 开启计时器
    self.daojishiTime = 30;
    NSLog(@"计时器前");
    if (self.daojishiTimer){
        [self.daojishiTimer invalidate];
        self.daojishiTimer = nil;
    }
    NSLog(@"计时器后");
    self.daojishiTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(challengerStatusNotReadyMeManagerTime) userInfo:nil repeats:YES];
}

// 没有进行准备 20秒准备时间
-(void)challengerStatusNotReadyMeManagerTime{
    NSLog(@"没有进行准备 20秒准备时间%li",(long)self.daojishiTime);
    // 1. 获取当前结束时间
    self.daojishiDymicLabel.text = [NSString stringWithFormat:@"%li",(long)self.daojishiTime];
    self.daojishiTime--;
    if (self.daojishiTime <= 0){
        if (self.daojishiTimer){
            [self.daojishiTimer invalidate];
            self.daojishiTimer = nil;
        }
        if ([PDLoginModel shareInstance].challengeStatus == challengerStatusNotReadyMe){      // 我没有准备
            [self.delegate daojishiEndManagerWithStatus:challengerStatusNotReadyMe];
        } else if ([PDLoginModel shareInstance].challengeStatus == challengerStatusNotReadyTo){   // 对方没有准备
            [self.delegate daojishiEndManagerWithStatus:challengerStatusNotReadyTo];
        }
    }
}

#pragma mark - 【3.对方未准备】
-(void)challengerStatusNotReadyToManager{
    self.fixedLabel.hidden = YES;
    self.button2.hidden = YES;
    self.button1.hidden = NO;
    [self.button1 cleanTimer];
    self.housNameView.hidden = NO;
    self.passwordView.hidden = NO;
    self.housNameView.alpha = 1;
    self.passwordView.alpha = 1;
    self.daojishiDymicLabel.hidden = NO;
    self.daojishiFixedLabel1.hidden = NO;
    self.daojishiFixedLabel2.hidden = NO;
    self.quicklyReportButton.hidden = YES;
    
    // 1. 房间名显示
    self.housNameView.frame = CGRectMake(LCFloat(60), 0, kScreenBounds.size.width - 2 * LCFloat(60), LCFloat(44));
    [self.housNameView changeFramge];
    // 2. 密码
    self.passwordView.frame = CGRectMake(self.housNameView.orgin_x, CGRectGetMaxY(self.housNameView.frame) + LCFloat(20), self.housNameView.size_width, self.housNameView.size_height);
    [self.passwordView changeFramge];
    // 3. 寻找对手
    self.button1.orgin_y = CGRectGetMaxY(self.passwordView.frame) + LCFloat(30);
    // 4. 按钮修改文字，正在为您寻找对手
    [self.button1 changeOneContent:@"确认(等待对手确认)"];
    self.button1.isEnable = NO;
    [self.button1 hidenAnimation];
    
    // 4.1
    // 6. 创建倒计时
    self.daojishiFixedLabel1.text = @"请等待对手在";
    // 6.1 重新计算文字宽度高度
    
    // 5 增加固定内容
    CGSize contentOfSize = [@"请等待对手在30秒内确认战斗" sizeWithCalcFont:self.daojishiFixedLabel1.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.daojishiFixedLabel1.font])];
    CGFloat daojishiMarginX = (kScreenBounds.size.width - contentOfSize.width - LCFloat(3) * 2) / 2.;
    CGSize daojishiSize1 = [self.daojishiFixedLabel1.text sizeWithCalcFont:self.daojishiFixedLabel1.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.daojishiFixedLabel1.font])];
    self.daojishiFixedLabel1.frame = CGRectMake(daojishiMarginX, CGRectGetMaxY(self.button1.frame) + LCFloat(10), daojishiSize1.width, [NSString contentofHeightWithFont:self.daojishiFixedLabel1.font]);
    CGSize daojishiSize2 = [@"30" sizeWithCalcFont:self.daojishiDymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.daojishiDymicLabel.font])];
    self.daojishiDymicLabel.frame = CGRectMake(CGRectGetMaxX(self.daojishiFixedLabel1.frame) + LCFloat(3), self.daojishiFixedLabel1.orgin_y, daojishiSize2.width, [NSString contentofHeightWithFont:self.daojishiDymicLabel.font]);
    CGSize daojishiSize3 = [self.daojishiFixedLabel2.text sizeWithCalcFont:self.daojishiFixedLabel2.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.daojishiFixedLabel2.font])];
    self.daojishiFixedLabel2.frame = CGRectMake(CGRectGetMaxX(self.daojishiDymicLabel.frame) + LCFloat(3), self.daojishiDymicLabel.orgin_y, daojishiSize3.width, [NSString contentofHeightWithFont:self.daojishiFixedLabel2.font]);
    
    // 5. 准备开启计时器
    if (self.daojishiTimer){
        [self.daojishiTimer invalidate];
        self.daojishiTimer = nil;
    }
    self.daojishiTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(challengeStatusNotReady1ManagerTimer) userInfo:nil repeats:YES];
}

// 我已经准备，对方在30秒内没有进行准备执行方法
-(void)challengeStatusNotReady1ManagerTimer{        // 1. 获取当前结束时间
    NSLog(@"我已经准备，对方在30秒内没有进行准备执行方法%li",(long)self.daojishiTime);
    self.daojishiDymicLabel.text = [NSString stringWithFormat:@"%li",(long)self.daojishiTime];
    self.daojishiTime--;
    if (self.daojishiTime <= 0){
        if (self.daojishiTimer){
            [self.daojishiTimer invalidate];
            self.daojishiTimer = nil;
        }
        if ([PDLoginModel shareInstance].challengeStatus == challengerStatusNotReadyMe){      // 我没有准备
            [self.delegate daojishiEndManagerWithStatus:challengerStatusNotReadyMe];
        } else if ([PDLoginModel shareInstance].challengeStatus == challengerStatusNotReadyTo){   // 对方没有准备
            [self.delegate daojishiEndManagerWithStatus:challengerStatusNotReadyTo];
        }
    }
}

#pragma mark - 【4.战斗即将开始 3. 2. 1.读秒】
-(void)challengerStatusReadyManager{
    self.fixedLabel.hidden = YES;
    self.button2.hidden = YES;
    self.button1.hidden = NO;
    [self.button1 cleanTimer];
    self.housNameView.hidden = NO;
    self.passwordView.hidden = NO;
    self.housNameView.alpha = 1;
    self.passwordView.alpha = 1;
    self.daojishiDymicLabel.hidden = YES;
    self.daojishiFixedLabel1.hidden = YES;
    self.daojishiFixedLabel2.hidden = YES;
    self.quicklyReportButton.hidden = YES;
    
    // 1. 房间名显示
    self.housNameView.frame = CGRectMake(LCFloat(60), 0, kScreenBounds.size.width - 2 * LCFloat(60), LCFloat(44));
    [self.housNameView changeFramge];
    // 2. 密码
    self.passwordView.frame = CGRectMake(self.housNameView.orgin_x, CGRectGetMaxY(self.housNameView.frame) + LCFloat(20), self.housNameView.size_width, self.housNameView.size_height);
    [self.passwordView changeFramge];
    // 3. 寻找对手
    self.button1.orgin_y = CGRectGetMaxY(self.passwordView.frame) + LCFloat(30);
    // 4. 按钮修改文字，正在为您寻找对手
    [self.button1 changeOneContent:@"战斗即将开始..."];
    self.button1.isEnable = NO;
    [self.button1 hidenAnimation];
    
    // 5. 开启倒计时
    self.daojishiTime = 3;
    if (self.daojishiTimer){
        [self.daojishiTimer invalidate];
        self.daojishiTimer = nil;
    }
    self.daojishiTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(challengeWillBeginManager) userInfo:nil repeats:YES];
}

// 比赛将要开始，剩下3秒倒计时
-(void)challengeWillBeginManager{
    NSLog(@"比赛将要开始，剩下3秒倒计时%li",(long)self.daojishiTime);
    [self.delegate challengeWillBeginWithTime:self.daojishiTime];
    self.daojishiTime--;
    if (self.daojishiTime <= 0){
        if (self.daojishiTimer){
            [self.daojishiTimer invalidate];
            self.daojishiTimer = nil;
        }
        [self.delegate challengeStartManager];
    }
}


#pragma mark - 【5.战斗中方法】
-(void)challengerStatusChallengingManager{
    self.fixedLabel.hidden = NO;
    self.button2.hidden = YES;
    self.button1.hidden = NO;
    [self.button1 cleanTimer];
    self.housNameView.hidden = NO;
    self.passwordView.hidden = NO;
    self.housNameView.alpha = 1;
    self.passwordView.alpha = 1;
    self.daojishiDymicLabel.hidden = YES;
    self.daojishiFixedLabel1.hidden = YES;
    self.daojishiFixedLabel2.hidden = YES;
    self.quicklyReportButton.hidden = NO;
    
    // 1. 房间名显示
    self.housNameView.frame = CGRectMake(LCFloat(60), 0, kScreenBounds.size.width - 2 * LCFloat(60), LCFloat(44));
    [self.housNameView changeFramge];

    // 2. 密码
    self.passwordView.frame = CGRectMake(self.housNameView.orgin_x, CGRectGetMaxY(self.housNameView.frame) + LCFloat(20), self.housNameView.size_width, self.housNameView.size_height);
    [self.passwordView changeFramge];
    // 3. 寻找对手
    self.button1.orgin_y = CGRectGetMaxY(self.passwordView.frame) + LCFloat(30);
    // 4. 按钮修改文字，正在为您寻找对手
    [self.button1 changeOneContent:@"战斗即将开始..."];
    self.button1.isEnable = NO;
    [self.button1 hidenAnimation];
    
    // 5. 创建固定文字
    CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(self.button1.size_width, CGFLOAT_MAX)];
    self.fixedLabel.frame = CGRectMake(self.button1.orgin_x, CGRectGetMaxY(self.button1.frame), self.button1.size_width, fixedSize.height);
    
    // 6. 计算宽高
    CGFloat kMargin = (self.size_height - LCFloat(11) - self.quicklyReportButton.size_height - self.fixedLabel.size_height - self.button1.size_height - self.housNameView.size_height - self.passwordView.size_height) / 4.;
    // 7. 重新排版
    [UIView animateWithDuration:.5f animations:^{
        self.housNameView.orgin_y = 0;
        self.passwordView.orgin_y = CGRectGetMaxY(self.housNameView.frame) + kMargin;
        self.button1.orgin_y = CGRectGetMaxY(self.passwordView.frame) + kMargin;
        self.fixedLabel.orgin_y = CGRectGetMaxY(self.button1.frame) + kMargin;
        self.quicklyReportButton.orgin_y = CGRectGetMaxY(self.fixedLabel.frame) + kMargin;
    } completion:NULL];
    
    // 5. 开启倒计时
    self.daojishiTime = 0;
    if (self.daojishiTimer){
        [self.daojishiTimer invalidate];
        self.daojishiTimer = nil;
    }
    self.daojishiTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(challengingManager) userInfo:nil repeats:YES];
}

//  比赛战斗中
-(void)challengingManager{
    // 1. 获取截止时间
    NSTimeInterval timeInterval = [[PDTool userDefaultGetWithKey:CURRENT_CHALLENGING_TIME_KEY] integerValue];
    // 2. 获取当前时间
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval currentInterval=[dat timeIntervalSince1970];
    // 3
    if (currentInterval >= timeInterval){           // 已经截止
        if ([PDLoginModel shareInstance].challengeStatus != challengerStatusCanEndGame){
            [PDLoginModel shareInstance].challengeStatus = challengerStatusCanEndGame;
            [self challengerStatusCanEndGameManager];
        } else {
            [self.button1 changeOneContent:[NSDate gamePass:(timeInterval - 60 * 15)]];
        }
    } else {                                        // 没有截止
        NSString *endTime = [PDTool getCountdownWithTargetDate:timeInterval];
        [self.button1 changeOneContent:endTime];
    }
    [self.delegate challengingManagerWithTime:timeInterval - currentInterval];
}

#pragma mark - 【6.比赛将要结束】
-(void)challengerStatusCanEndGameManager{
    self.fixedLabel.hidden = NO;
    self.button2.hidden = YES;
    self.button1.hidden = NO;
    [self.button1 cleanTimer];
    self.housNameView.hidden = NO;
    self.passwordView.hidden = NO;
    self.housNameView.alpha = 1;
    self.passwordView.alpha = 1;
    self.daojishiDymicLabel.hidden = YES;
    self.daojishiFixedLabel1.hidden = YES;
    self.daojishiFixedLabel2.hidden = YES;
    self.quicklyReportButton.hidden = NO;
    
    // 1. 房间名显示
    self.housNameView.frame = CGRectMake(LCFloat(60), 0, kScreenBounds.size.width - 2 * LCFloat(60), LCFloat(44));
    [self.housNameView changeFramge];
    
    // 2. 密码
    self.passwordView.frame = CGRectMake(self.housNameView.orgin_x, CGRectGetMaxY(self.housNameView.frame) + LCFloat(20), self.housNameView.size_width, self.housNameView.size_height);
    [self.passwordView changeFramge];
    // 3. 寻找对手
    self.button1.orgin_y = CGRectGetMaxY(self.passwordView.frame) + LCFloat(30);
    // 4. 按钮修改文字，正在为您寻找对手
    [self.button1 changeOneContent:@"战斗结束"];
    self.button1.isEnable = YES;
    [self.button1 hidenAnimation];
    
    // 5. 创建固定文字
    CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(self.button1.size_width, CGFLOAT_MAX)];
    self.fixedLabel.frame = CGRectMake(self.button1.orgin_x, CGRectGetMaxY(self.button1.frame), self.button1.size_width, fixedSize.height);
    
    
    // 6. 计算宽高
    CGFloat kMargin = (self.size_height - LCFloat(11) - self.quicklyReportButton.size_height - self.fixedLabel.size_height - self.button1.size_height - self.housNameView.size_height - self.passwordView.size_height) / 4.;
    // 7. 重新排版
//    [UIView animateWithDuration:.5f animations:^{
        self.housNameView.orgin_y = 0;
        self.passwordView.orgin_y = CGRectGetMaxY(self.housNameView.frame) + kMargin;
        self.button1.orgin_y = CGRectGetMaxY(self.passwordView.frame) + kMargin;
        self.fixedLabel.orgin_y = CGRectGetMaxY(self.button1.frame) + kMargin;
        self.quicklyReportButton.orgin_y = CGRectGetMaxY(self.fixedLabel.frame) + kMargin;
//    } completion:NULL];

    // 5. 开启倒计时
    self.daojishiTime = 0;
    if (self.daojishiTimer){
        [self.daojishiTimer invalidate];
        self.daojishiTimer = nil;
    }
    self.daojishiTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(challengeStatusWillEndTimerManager) userInfo:nil repeats:YES];
    
}

-(void)challengeStatusWillEndTimerManager{
    // 1. 获取截止时间
    NSTimeInterval timeInterval = [[PDTool userDefaultGetWithKey:CURRENT_CHALLENGING_TIME_KEY] integerValue];
    // 2. 获取当前时间
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval currentInterval=[dat timeIntervalSince1970];
    // 3
    if (currentInterval >= timeInterval){           // 已经截止
        if ([PDLoginModel shareInstance].challengeStatus != challengerStatusCanEndGame){
            [PDLoginModel shareInstance].challengeStatus = challengerStatusCanEndGame;
//            [self challengerStatusCanEndGameManager];
        } else {
            [self.button1 changeOneContent:[NSDate gamePass:(timeInterval - 60 * 15)]];
        }
    } else {                                        // 没有截止
        NSString *endTime = [PDTool getCountdownWithTargetDate:timeInterval];
        [self.button1 changeOneContent:endTime];
    }
    [self.delegate challengingManagerWithTime:timeInterval];
}

#pragma mark 【7.比赛结算】
-(void)challengerStatusCalculationingManager{
    if (self.daojishiTimer){
        [self.daojishiTimer invalidate];
        self.daojishiTimer = nil;
    }
    
    // 6. 计算宽高
    CGFloat kMargin = (self.size_height - LCFloat(11) - self.quicklyReportButton.size_height - self.fixedLabel.size_height - self.button1.size_height - self.housNameView.size_height - self.passwordView.size_height) / 4.;
    // 7. 重新排版
    self.housNameView.orgin_y = 0;
    self.passwordView.orgin_y = CGRectGetMaxY(self.housNameView.frame) + kMargin;
    
    [UIView animateWithDuration:.5f animations:^{
        self.housNameView.alpha = 0;
        self.passwordView.alpha = 0;
    } completion:^(BOOL finished) {
        self.housNameView.hidden = YES;
        self.passwordView.hidden = YES;
         self.button1.orgin_y = CGRectGetMaxY(self.passwordView.frame) + kMargin;
        self.fixedLabel.orgin_y = CGRectGetMaxY(self.button1.frame) + kMargin;
        self.quicklyReportButton.orgin_y = CGRectGetMaxY(self.fixedLabel.frame) + kMargin;

        [self.button1 changeOneContent:@"结算中，请稍后"];
        [self.button1 showLoadingAnimationWithText:@"结算中,请稍后"];
        
    }];
}

#pragma mark - 8.【结算失败】
-(void)challengerStatusCalculationFailManager{
    if (self.daojishiTimer){
        [self.daojishiTimer invalidate];
        self.daojishiTimer = nil;
    }
    
    // 6. 计算宽高
    CGFloat kMargin = (self.size_height - LCFloat(11) - self.quicklyReportButton.size_height - self.fixedLabel.size_height - self.button1.size_height - self.housNameView.size_height - self.passwordView.size_height) / 4.;
    // 7. 重新排版
    self.housNameView.orgin_y = 0;
    self.passwordView.orgin_y = CGRectGetMaxY(self.housNameView.frame) + kMargin;
    
    [UIView animateWithDuration:.5f animations:^{
        self.housNameView.alpha = 0;
        self.passwordView.alpha = 0;
    } completion:^(BOOL finished) {
        self.housNameView.hidden = YES;
        self.passwordView.hidden = YES;
        self.button1.orgin_y = CGRectGetMaxY(self.passwordView.frame) + kMargin;
        self.fixedLabel.orgin_y = CGRectGetMaxY(self.button1.frame) + kMargin;
        self.quicklyReportButton.orgin_y = CGRectGetMaxY(self.fixedLabel.frame) + kMargin;
        
        [self.button1 changeOneContent:@"结算中，请稍后"];
        [self.button1 showLoadingAnimationWithText:@"结算中,请稍后"];
        
    }];
}

#pragma mark ButtonDelegate
-(void)buttonClickWithStatus:(challengerStatus)status{
    if (status == challengerStatusNotReadyMe){          // 1. 我方未准备进行点击，进行准备
        [self.delegate challengeButtonClickToReady];
    } else if (status == challengerStatusNotFind){      // 2. 比赛没有找到对手点击进行重新寻找
    
    } else if (status == challengerStatusCanEndGame){   // 3. 比赛可以结束，点击执行结束接口
        [self.delegate challengingJiesuanManager];
    }
}


#pragma mark - OtherManager
-(void)animationWithChangeHomeInfo:(NSString *)homeName homePwd:(NSString *)homePwd{        // 修改房间动画
    [self.housNameView showAnimationWithStirng:homeName];
    [self.passwordView showAnimationWithStirng:homePwd];
}


#pragma mark - 没有找到对手
-(void)challengeStatusNotFindManager{
    self.fixedLabel.hidden = YES;
    self.button2.hidden = NO;
    self.button1.hidden = NO;
    self.housNameView.hidden = YES;
    self.passwordView.hidden = YES;
    self.housNameView.alpha = 1;
    self.passwordView.alpha = 1;
    self.daojishiDymicLabel.hidden = YES;
    self.daojishiFixedLabel1.hidden = YES;
    self.daojishiFixedLabel2.hidden = YES;
    
    [UIView animateWithDuration:.5f animations:^{
        self.button1.orgin_y = 0;
        self.button2.orgin_y = CGRectGetMaxY(self.button1.frame) + LCFloat(15);
        [self.button2 setTitle:@"更换出征条件" forState:UIControlStateNormal];
    }];
    
}

#pragma mark 时间
-(void)cleanTimer{
    if (self.daojishiTimer){
        [self.daojishiTimer invalidate];
        self.daojishiTimer = nil;
    }
}
@end
