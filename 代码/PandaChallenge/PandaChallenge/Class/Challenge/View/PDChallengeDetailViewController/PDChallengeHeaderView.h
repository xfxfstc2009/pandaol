//
//  PDChallengeHeaderView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 进行匹配头部信息
#import <UIKit/UIKit.h>

@interface PDChallengeHeaderView : UIView
@property (nonatomic,strong)UIButton *popButton;                /**< 返回按钮*/
@property (nonatomic,strong)UIButton *rightButton;

-(void)showAnimationWithText:(NSString *)text;
-(void)hidenAnimation;

@end


