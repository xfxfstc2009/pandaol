//
//  PDChallengeDetailHouseInfoView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PandaChallenge-Swift.h"

@interface PDChallengeDetailHouseInfoView : UIView
@property (nonatomic,strong)LTMorphingLabel *dymicLabel;

-(instancetype)initWithFrame:(CGRect)frame fixedString:(NSString *)fixedString ;

-(void)changeFramge;
-(void)showAnimationWithStirng:(NSString *)animationString;
-(void)stopAnimation;
@end
