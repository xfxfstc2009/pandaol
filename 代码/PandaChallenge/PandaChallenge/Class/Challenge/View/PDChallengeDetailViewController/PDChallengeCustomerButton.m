//
//  PDChallengeCustomerButton.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeCustomerButton.h"
#import <objc/runtime.h>
@interface PDChallengeCustomerButton()
@property (nonatomic,strong)NSTimer *buttonTimer;           /**< 按钮计时器*/
@property (nonatomic,assign)NSInteger timerInteger;         /**< 倒数的时间*/
@property (nonatomic,copy)NSString *contentTempString;      /**< 临时调用内容*/
@property (nonatomic,strong)UIButton *button1;
@property (nonatomic,strong)UIButton *button2;
@end

@implementation PDChallengeCustomerButton

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
        self.clipsToBounds = YES;
        self.layer.cornerRadius = LCFloat(5);
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建scrollView
    self.mainScrollView = [[UIScrollView alloc]init];
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    self.mainScrollView.showsVerticalScrollIndicator = NO;
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.bounces = NO;
    self.mainScrollView.scrollEnabled = NO;
    self.mainScrollView.contentSize = CGSizeMake(2 * self.mainScrollView.size_width, self.bounds.size.height);
    self.mainScrollView.frame = self.bounds;
    [self addSubview:self.mainScrollView];
    
    // 2. 创建第一个页面
    self.pageOneView = [[UIView alloc]init];
    self.pageOneView.backgroundColor = [UIColor blackColor];
    self.pageOneView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    [self.mainScrollView addSubview:self.pageOneView];
    
    // 4. 创建菊花
    self.activityView = [[UIActivityIndicatorView alloc]init];
    [self.activityView setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];//设置进度轮显示类型
    [self.pageOneView addSubview:self.activityView];
    
    // 5. 创建label
    self.contentLabel = [[UILabel alloc]init];
    self.contentLabel.backgroundColor = [UIColor clearColor];
    self.contentLabel.textAlignment = NSTextAlignmentLeft;
    self.contentLabel.font = [UIFont systemFontOfCustomeSize:18.];
    self.contentLabel.textColor = [UIColor whiteColor];
    [self.pageOneView addSubview:self.contentLabel];
    
    // 6. 创建透明按钮
    self.button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button1.frame = self.pageOneView.bounds;
    self.button1.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [self.button1 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.delegate buttonClickWithStatus:[PDLoginModel shareInstance].challengeStatus];
    }];
    [self.pageOneView addSubview:self.button1];
    
    
    // 3.创建第二个页面
    self.pageTwoView = [[UIView alloc]init];
    self.pageTwoView.backgroundColor = [UIColor clearColor];
    self.pageTwoView.frame = CGRectMake(self.bounds.size.width, 0, self.bounds.size.width, self.bounds.size.height);
    [self.mainScrollView addSubview:self.pageTwoView];
    
    self.contentLabelTwo = [[UILabel alloc]init];
    self.contentLabelTwo.frame = self.bounds;
    self.contentLabelTwo.textAlignment = NSTextAlignmentCenter;
    self.contentLabelTwo.textColor = [UIColor whiteColor];
    self.contentLabelTwo.font = self.contentLabel.font;
    [self.pageTwoView addSubview:self.contentLabelTwo];
    
    // 5. 修改frame
    [self updateFrame];
}

#pragma mark 重置frame
-(void)updateFrame{
    CGSize contentOfSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.contentLabel.font])];
    
    CGFloat kmargin;
    if (self.activityView.isAnimating == YES){
        kmargin = (self.bounds.size.width - contentOfSize.width - LCFloat(30) - LCFloat(10)) / 2.;
        self.activityView.frame = CGRectMake(kmargin, (self.bounds.size.height - LCFloat(30)) / 2., LCFloat(30), LCFloat(30));
        self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.activityView.frame), 0, self.bounds.size.width - CGRectGetMaxX(self.activityView.frame), self.bounds.size.height);
    } else {
        kmargin = (self.bounds.size.width - contentOfSize.width ) / 2.;
        self.contentLabel.frame = CGRectMake(kmargin, 0, self.bounds.size.width - kmargin, self.bounds.size.height);
    }
}

#pragma mark 修改当前按钮内容
-(void)changeOneContent:(NSString *)content{
    _contentTempString = content;
    self.contentLabel.text = content;
    
    [self updateFrame];
}



#pragma mark 显示动画
-(void)showLoadingAnimationWithText:(NSString *)text{
    self.contentLabel.text = text;
    // 0.菊花
    [self.activityView startAnimating];
    [self updateFrame];
    
    // 1. 开启计时器
    if (!self.buttonTimer){
        self.buttonTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerManager) userInfo:nil repeats:YES];
        self.timerInteger = 0;
    }
    
}

// 计时器方法
-(void)timerManager{
    self.timerInteger++;
    NSInteger timeC = self.timerInteger % 4;
    NSString *dotString = @"";
    for (int i = 0 ;i < timeC ; i++){
        dotString = [dotString stringByAppendingString:@"."];
    }
    
    self.contentLabel.text = [NSString stringWithFormat:@"%@%@",self.contentTempString,dotString];
}

#pragma mark - 隐藏动画
-(void)hidenAnimation{
    [self.activityView stopAnimating];
    
    if (self.buttonTimer){
        [self.buttonTimer invalidate];
        self.buttonTimer = nil;
    }
}

-(void)managerSuccessWithBlock:(void(^)())block{
    if (self.buttonTimer){
        [self.buttonTimer invalidate];
        self.buttonTimer = nil;
    }
    [self.mainScrollView setContentOffset:CGPointMake(self.bounds.size.width, 0) animated:YES];
    if (block){
        block();
    }
}

#pragma mark 设置是否可以选中
-(void)setIsEnable:(BOOL)isEnable{
    _isEnable = isEnable;
    if (isEnable == YES){       // 可以选中
        self.button1.enabled = YES;
        self.pageOneView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    } else {                    // 不可以选中
        self.button1.enabled = NO;
        self.pageOneView.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
    }
}

-(void)cleanTimer{
    if (self.buttonTimer){
        [self.buttonTimer invalidate];
        self.buttonTimer = nil;
    }
}

@end
