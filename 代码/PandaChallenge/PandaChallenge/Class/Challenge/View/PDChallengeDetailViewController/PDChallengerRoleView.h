//
//  PDChallengerRoleView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDChallengeDetailViewController.h"
#import "PDChallengerStatusEnum.h"
#import "PDChallengerSingleModel.h"
@class PDChallengeDetailViewController;
@interface PDChallengerRoleView : UIView
@property (nonatomic,strong)PDImageView *roleImageView;         /**< 角色头像*/

@property (nonatomic,strong)PDChallengerSingleModel *transferChallengerSingleModel;     /**< 传递过来内容信息*/

// 页面显示状态
-(void)viewShowManagerWithStatus:(challengerStatus)status;
-(void)showManagerWithChallengerSingleModel:(PDChallengerSingleModel *)transferChallengerSingleModel;   /**< 传递过去的model*/
-(void)showManagerWithPipeiSuccess:(PDChallengerSingleModel *)transferChallengerSingleModel;
@end
