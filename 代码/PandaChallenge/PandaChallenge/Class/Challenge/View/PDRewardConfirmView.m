//
//  PDRewardConfirmView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRewardConfirmView.h"
#import <objc/runtime.h>
static char rewardButtonKey;    /**< 领奖按钮*/
static char cancelKey;          /**< 取消按钮*/

@interface PDRewardConfirmView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *confirmRewardTableView;
@property (nonatomic,strong)NSMutableArray *confirmRewardMutableArr;        /**< 数组*/
@property (nonatomic,strong)UILabel *countIntegrallabel;                    /**< 积分总数*/
@property (nonatomic,strong)UIButton *rewardButton;                         /**< 领奖按钮*/
@property (nonatomic,strong)UIButton *cancelButton;                         /**< 取消按钮*/

@end

@implementation PDRewardConfirmView

-(instancetype)initWithCardArr:(NSArray *)cardArr rewardBlock:(void(^)())rewardBlock cancelBlock:(void(^)())cancelBlock{
    self = [super init];
    if (self){
        objc_setAssociatedObject(self, &rewardButtonKey, rewardBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
        objc_setAssociatedObject(self, &cancelKey, cancelBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [self createView];
    }
    return self;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.confirmRewardMutableArr = [NSMutableArray array];
    
}


-(void)createView{
    // 1. 创建tableView
    if (!self.confirmRewardTableView){
        self.confirmRewardTableView = [[UITableView alloc] initWithFrame:self.bounds style:UITableViewStylePlain];
        self.confirmRewardTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.confirmRewardTableView.delegate = self;
        self.confirmRewardTableView.dataSource = self;
        self.confirmRewardTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.confirmRewardTableView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.confirmRewardTableView];
    }

    // 2. 创建
    self.countIntegrallabel = [[UILabel alloc]init];
    self.countIntegrallabel.backgroundColor = [UIColor clearColor];
    self.countIntegrallabel.font = [UIFont systemFontOfCustomeSize:15.];
    self.countIntegrallabel.frame = CGRectMake(0, CGRectGetMaxY(self.confirmRewardTableView.frame), self.bounds.size.width, [NSString contentofHeightWithFont:self.countIntegrallabel.font]);
    self.countIntegrallabel.text = @"23100P";
    [self addSubview:self.countIntegrallabel];
    
    // 3. 创建按钮
    self.rewardButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rewardButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(30)) / 2., CGRectGetMaxY(self.countIntegrallabel.frame), LCFloat(100), LCFloat(100));
    __weak typeof(self)weakSelf = self;
    [weakSelf.rewardButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf =  weakSelf;
        void(^rewardBlock)() = objc_getAssociatedObject(strongSelf, &rewardButtonKey);
        if (rewardBlock){
            rewardBlock();
        }
    }];
    [self.rewardButton setBackgroundColor:[UIColor yellowColor]];
    [self.rewardButton setTitle:@"领赏" forState:UIControlStateNormal];
    [self addSubview:self.rewardButton];
    
    // 4. 创建取消按钮
    self.cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cancelButton setTitle:@"取消" forState:UIControlStateNormal];
    self.cancelButton.backgroundColor = [UIColor clearColor];
    [self.cancelButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^cancelBlock)() = objc_getAssociatedObject(strongSelf, &cancelBlock);
        if (cancelBlock){
            cancelBlock();
        }
    }];
    self.cancelButton.frame = CGRectMake(CGRectGetMaxX(self.rewardButton.frame), self.rewardButton.orgin_y, 100, 100);
    [self addSubview:self.cancelButton];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.confirmRewardMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cellWithRowOne;
}


@end
