//
//  PDRewardConfirmView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDRewardConfirmView : UIView

-(instancetype)initWithCardArr:(NSArray *)cardArr rewardBlock:(void(^)())rewardBlock cancelBlock:(void(^)())cancelBlock;

@end
