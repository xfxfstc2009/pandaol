//
//  PDChallengeCheckChallengerInfo.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDChallengeCheckChallengerMsgInfo.h"
@interface PDChallengeCheckChallengerInfo : PDFetchModel

@property (nonatomic,copy)NSString *gameName;
@property (nonatomic,copy)NSString *ganemArea;
@property (nonatomic,assign)NSTimeInterval reg_time;
@property (nonatomic,copy)NSString *room_id;
@property (nonatomic,copy)NSString *userid;             
@property (nonatomic,strong) PDChallengeCheckChallengerMsgInfo *msg;
@property (nonatomic,copy)NSString *room_name;
@property (nonatomic,copy)NSString *room_pass;


@end
