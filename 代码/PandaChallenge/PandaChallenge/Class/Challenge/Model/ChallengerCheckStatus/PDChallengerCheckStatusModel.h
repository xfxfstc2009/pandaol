//
//  PDChallengerCheckStatusModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDChallengeCHeckChallengerTopModel.h"
@interface PDChallengerCheckStatusModel : PDFetchModel

@property (nonatomic,copy)NSString *gameName;           /**< 游戏名字*/
@property (nonatomic,copy)NSString *gameArea;
@property (nonatomic,strong)PDChallengeCHeckChallengerTopModel *matchingData;
@property (nonatomic,assign)NSInteger status;
@end
