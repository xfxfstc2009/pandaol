//
//  PDChallengeCheckChallengerMsgInfo.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDChallengeCheckChallengerMsgInfo : PDFetchModel

@property (nonatomic,copy)NSString *userid;
@property (nonatomic,copy)NSString *lol_name;
@property (nonatomic,assign)kCardType card_leavel;
@property (nonatomic,copy)NSString *lol_server_id;
@property (nonatomic,copy)NSString *fighting_capacity;
@property (nonatomic,copy)NSString *lol_max_rank;
@property (nonatomic,copy)NSString *match_id;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *server_name;
@end
