//
//  PDChallengeCHeckChallengerTopModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDChallengeCheckChallengerInfo.h"
@interface PDChallengeCHeckChallengerTopModel : PDFetchModel

@property (nonatomic,strong)PDChallengeCheckChallengerInfo *emenge;
@property (nonatomic,strong)PDChallengeCheckChallengerInfo *myChallenge;
@end
