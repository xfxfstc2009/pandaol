//
//  PDChallengerStatusEnum.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef PDChallengerStatusEnum_h
#define PDChallengerStatusEnum_h


typedef NS_ENUM(NSInteger,challengerStatus) {
    challengerStatusNormal = 0,                  /**< 1. 寻找对手状态*/
    challengerStatusNotReadyMe,                  /**< 2. 本方没有准备*/
    challengerStatusNotReadyTo,                  /**< 3. 对方没有准备*/
    challengerStatusReady,                       /**< 4. 双方都准备了*/
    challengerStatusChallenging,                 /**< 5. 对战中*/
    challengerStatusCanEndGame,                  /**< 6. 15分钟后比赛可以结束*/
    challengerStatusCalculationing,              /**< 7. 结算中*/
    challengerStatusCalculationSuccess,          /**< 8. 结算成功*/
    challengerStatusCalculationFail,             /**< 9. 结算失败*/
    challengerStatusNotFind,                     /**< 10.比赛没有寻找到对手*/
    challengerStatusCalculationFailAgain = 314   /**< 10.比赛再次结算失败*/
};


#endif /* PDChallengerStatusEnum_h */
