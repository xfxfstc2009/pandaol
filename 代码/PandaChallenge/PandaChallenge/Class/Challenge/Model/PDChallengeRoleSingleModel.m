//
//  PDChallengeRoleSingleModel.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeRoleSingleModel.h"

@implementation PDChallengeRoleSingleModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"roleId": @"id"};
}


@end
