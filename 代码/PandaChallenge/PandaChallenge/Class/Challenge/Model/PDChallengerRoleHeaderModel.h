//
//  PDChallengerRoleHeaderModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDChallengerRoleHeaderModel : PDFetchModel

@property (nonatomic,copy)NSString *headerImg;          /**< 头像*/
@property (nonatomic,copy)NSString *userName;           /**< 名字*/
@property (nonatomic,assign)CGFloat progress;           /**< 进度*/
@property (nonatomic,assign)CGFloat shenglv;            /**< 胜率*/

@end
