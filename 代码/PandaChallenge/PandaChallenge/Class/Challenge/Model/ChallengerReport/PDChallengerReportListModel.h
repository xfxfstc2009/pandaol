//
//  PDChallengerReportListModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDChallengerReportSingleModel.h"

@interface PDChallengerReportListModel : PDFetchModel

@property (nonatomic,strong)NSArray <PDChallengerReportSingleModel> *reportList;

@end
