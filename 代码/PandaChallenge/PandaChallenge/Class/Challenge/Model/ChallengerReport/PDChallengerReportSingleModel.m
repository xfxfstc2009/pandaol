//
//  PDChallengerReportSingleModel.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengerReportSingleModel.h"

@implementation PDChallengerReportSingleModel
- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"reportId": @"id"};
}

@end
