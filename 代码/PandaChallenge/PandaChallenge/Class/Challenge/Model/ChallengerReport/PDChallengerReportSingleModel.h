//
//  PDChallengerReportSingleModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@protocol PDChallengerReportSingleModel <NSObject>

@end

@interface PDChallengerReportSingleModel : PDFetchModel

@property (nonatomic,copy)NSString *content;
@property (nonatomic,copy)NSString *reportId;

@end
