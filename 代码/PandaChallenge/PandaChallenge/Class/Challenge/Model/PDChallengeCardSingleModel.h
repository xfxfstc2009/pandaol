//
//  PDChallengeCardSingleModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 召唤师卡片
#import "PDFetchModel.h"
#import "PDChallengerCardEnum.h"

@protocol PDChallengeCardSingleModel <NSObject>

@end

@interface PDChallengeCardSingleModel : PDFetchModel

@property (nonatomic,copy)NSString *card_id;                /**< 卡的编号*/
@property (nonatomic,assign)kCardType card_level;           /**< 卡的等级*/
@property (nonatomic,assign)NSInteger card_num;             /**< 卡片数量*/
@property (nonatomic,assign)NSInteger product_id;           /**< 商品编号*/
@property (nonatomic,copy)NSString *user_id;                /**< 用户id*/


@end
