//
//  PDChallengeRoleSingleModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 单一的召唤师名称
#import "PDFetchModel.h"

@protocol  PDChallengeRoleSingleModel<NSObject>

@end

@interface PDChallengeRoleSingleModel : PDFetchModel

@property (nonatomic,copy)NSString *fighting_capacity;          /**< 战斗力*/
@property (nonatomic,copy)NSString *roleId;                     /**< 角色id*/
@property (nonatomic,copy)NSString *lol_max_rank;               /**< 最高等级*/
@property (nonatomic,copy)NSString *lol_name;                   /**< 名称*/
@property (nonatomic,copy)NSString *lol_server_id;              /**< 服务器编号*/
@property (nonatomic,copy)NSString *other_name;                 /**< 其他名称*/
@property (nonatomic,copy)NSString *server_name;                /**< 服务器名称*/

@end
