//
//  PDChallengeCardListModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDChallengeCardSingleModel.h"

@interface PDChallengeCardListModel : PDFetchModel

@property (nonatomic,strong)NSArray <PDChallengeCardSingleModel> *cardList;

@end
