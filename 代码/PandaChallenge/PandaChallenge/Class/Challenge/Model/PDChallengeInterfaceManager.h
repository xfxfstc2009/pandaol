//
//  PDChallengeInterfaceManager.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDChallengeInterfaceManager : NSObject

-(void)sendRequestToReadyWithSuccessBlock:(void(^)())successBlock failBlock:(void(^)())block;

@end
