//
//  PDisAbandonGameModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDisAbandonGameModel : PDFetchModel

@property (nonatomic,assign)BOOL other_status;          // 用来判断对方是否准备

@end
