//
//  PDChallengerStatusModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDChallengerStatusEnum.h"
//

@interface PDChallengerStatusModel : PDFetchModel
@property (nonatomic,assign)challengeStatus status;
@end
