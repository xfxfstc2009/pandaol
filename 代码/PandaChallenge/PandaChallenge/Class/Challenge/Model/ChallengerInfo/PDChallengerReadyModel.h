//
//  PDChallengerReadyModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 用户进行准备，获取对方当前是否准备
#import "PDFetchModel.h"

typedef NS_ENUM(NSInteger,isReadyType) {
    isReadyTypeNo = 0,                      /**< 未准备*/
    isReadyTypeYes = 1                      /**< 准备*/
};

@interface PDChallengerReadyModel : PDFetchModel

@property (nonatomic,assign)isReadyType is_ready;              /**< 当前准备的类型*/

@end
