//
//  PDChallengerEndStatus.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDChallengerCardEnum.h"
@interface PDChallengerEndStatus : PDFetchModel


@property (nonatomic,assign)BOOL win;
@property (nonatomic,assign)kCardType win_level;            /**< 赢得后的卡等级*/
@property (nonatomic,assign)kCardType current_level;        /**< 当前的卡等级*/
@property (nonatomic,copy)NSString *server_id;              /**< 区的id*/
@property (nonatomic,assign)NSInteger points;               /**< 返回的积分*/
@property (nonatomic,assign)BOOL isNext;                    /**< 是否可以下一轮*/
@end
