//
//  PDChallengerStatusModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDChallengerStatusEnum.h"
#import "PDChallengerSingleModel.h"
#import "PDChallengerEndStatus.h"

@interface PDChallengerStatusModel : PDFetchModel
@property (nonatomic,assign)challengerStatus status;
@property (nonatomic,copy)NSString *gameName;                           /**< 游戏名称*/
@property (nonatomic,copy)NSString *gameArea;                           /**< 游戏区*/
@property (nonatomic,strong)PDChallengerSingleModel *myChallenge;       /**< 我的角色*/
@property (nonatomic,strong)PDChallengerSingleModel *emenge;            /**< 对方角色*/

// 2. 游戏开始
@property (nonatomic,assign)NSTimeInterval battle_end_time;             /**< 游戏开始时间*/
@property (nonatomic,copy)NSString *room_name;                          /**< 房间名*/
@property (nonatomic,copy)NSString *room_pass;                          /**< 房间密码*/

@property (nonatomic,assign)kCardType card_level;

// 3. 游戏结束
@property (nonatomic,strong)PDChallengerEndStatus *result;

// 结算
@property (nonatomic,assign)NSInteger times;    /**< 结算次数*/
@property (nonatomic,assign)BOOL isServer;      /**< 是否人工返回判定*/
@property (nonatomic,copy)NSString *desc;       /**< 返回信息*/

@property (nonatomic,assign)BOOL fail;          /**< 是否失败*/

// 踢人下线
@property (nonatomic,assign)NSInteger code;
@property (nonatomic,copy)NSString *room_id;
@property (nonatomic,copy)NSString *userId;
@property (nonatomic,copy)NSString *phone;
// 判断是否状态
@property (nonatomic,assign)BOOL other_status;          // 用来判断对方是否准备

@property (nonatomic,assign)BOOL invalid;

@end
