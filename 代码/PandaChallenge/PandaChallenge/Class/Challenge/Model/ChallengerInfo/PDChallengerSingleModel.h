//
//  PDChallengerSingleModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDChallengerCardEnum.h"

@protocol PDChallengerSingleModel <NSObject>

@end

@interface PDChallengerSingleModel : PDFetchModel

@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,assign)kCardType card_leavel;
@property (nonatomic,copy)NSString *level;

@property (nonatomic,copy)NSString *lol_name;   // name
@property (nonatomic,copy)NSString *lol_max_rank;

@end
