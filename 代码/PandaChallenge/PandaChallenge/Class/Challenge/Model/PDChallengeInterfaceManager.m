//
//  PDChallengeInterfaceManager.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeInterfaceManager.h"

@implementation PDChallengeInterfaceManager

#pragma mark - Interface
#pragma mark 准备战斗
-(void)sendRequestToReadyWithSuccessBlock:(void(^)())successBlock failBlock:(void(^)())block{
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:@"room_fight/getReady" completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            
        } else {
            if (error.code == 401){
                NSLog(@"未登录");
            } else if (error.code == 302){
                NSLog(@"未进行匹配");
            } else if (error.code == 303){
                NSLog(@"为准备");
            } else if (error.code == 304){
                NSLog(@"已准备");
            } else if (error.code == 305){
                NSLog(@"战斗中");
            } else if (error.code == 306){
                
            }
        }
    }];
}

@end
