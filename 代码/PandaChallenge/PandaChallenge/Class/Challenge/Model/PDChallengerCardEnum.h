//
//  PDChallengerCardEnum.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef PDChallengerCardEnum_h
#define PDChallengerCardEnum_h

typedef NS_ENUM(NSInteger,kCardType) {
    kCardType1 = 1,                       /**< K1*/
    kCardType2,
    kCardType3,
    kCardType4,
    kCardType5,
    kCardType6,
    kCardType7,
    kCardType8,
    kCardType9,
    kCardType10 = 999,
    kCardType0 = 0,
    kCardTypeBuy = -1,
};

#endif /* PDChallengerCardEnum_h */
