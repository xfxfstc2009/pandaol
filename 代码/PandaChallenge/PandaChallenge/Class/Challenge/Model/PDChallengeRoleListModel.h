//
//  PDChallengeRoleListModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
#import "PDChallengeRoleSingleModel.h"
@interface PDChallengeRoleListModel : PDFetchModel

@property (nonatomic,strong)NSArray<PDChallengeRoleSingleModel> *roleList;

@end
