//
//  PDChallengeStatusMainModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"

@interface PDChallengeStatusMainModel : PDFetchModel


@property (nonatomic,assign)NSInteger status;           /**< 313 结算失败,314 再次结算失败*/

@end
