//
//  PDPoint.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef PDPoint_h
#define PDPoint_h

struct PDPoint {
    CGFloat x;
    CGFloat y;
    CGFloat z;
};

typedef struct PDPoint PDPoint;


PDPoint PDPointMake(CGFloat x, CGFloat y, CGFloat z) {
    PDPoint point;
    point.x = x;
    point.y = y;
    point.z = z;
    return point;
}

#endif /* PDPoint_h */
