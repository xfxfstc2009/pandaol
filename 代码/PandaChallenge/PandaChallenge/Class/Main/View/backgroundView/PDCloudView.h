//
//  PDCloudView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDCloudView : UIView

@property (nonatomic,strong)UIPanGestureRecognizer *gesture;

- (void)setCloudTags:(NSArray *)array;
- (void)timerStart;
- (void)timerStop;

@end
