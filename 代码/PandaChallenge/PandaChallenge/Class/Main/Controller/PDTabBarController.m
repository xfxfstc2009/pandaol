//
//  PDTabBarController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTabBarController.h"
#import "PDNavigationController.h"
#import "PDTestViewController.h"                // 测试
#import "PDChallengeViewController.h"           // 挑战
#import "PDOrderViewController.h"               // 订单
@interface PDTabBarController()

@end

@implementation PDTabBarController

-(void)viewDidLoad{
    [super viewDidLoad];
    // 1. 测试
    PDTestViewController *testViewController = [[PDTestViewController alloc] init];
    PDNavigationController *testNavigationVC = [[PDNavigationController alloc]initWithRootViewController:testViewController];
    testViewController.tabBarItem.tag = 0;
    testViewController.tabBarItem.image = [UIImage imageNamed:@"tabbar_competition"];
    testViewController.tabBarItem.title = @"测试";

    
    // 2. 挑战
    PDChallengeViewController *challengeViewController = [[PDChallengeViewController alloc] init];
    PDNavigationController *challengeNavigationController = [[PDNavigationController alloc] initWithRootViewController:challengeViewController];
    challengeNavigationController.tabBarItem.tag = 1;
    challengeViewController.tabBarItem.image = [UIImage imageNamed:@"tabbar_competition"];
    challengeViewController.tabBarItem.title = @"挑战";
    
    // 3. 登录
   
    
    // 4.订单
    PDOrderViewController *orderViewController = [[PDOrderViewController alloc] init];
    PDNavigationController *orderNavigationController = [[PDNavigationController alloc] initWithRootViewController:orderViewController];
    orderNavigationController.tabBarItem.tag = 3;
    orderViewController.tabBarItem.image = [UIImage imageNamed:@"tabbar_competition"];
    orderViewController.tabBarItem.title = @"订单";
    
    self.viewControllers = @[testNavigationVC,challengeNavigationController,orderNavigationController];
    
    // customsie UINavigationBar UI Effect
    UIImage *backgroundImage = [UIImage imageWithRenderColor:NAVBAR_COLOR renderSize:CGSizeMake(10., 10.)];
    [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];


}

#pragma mark 


@end
