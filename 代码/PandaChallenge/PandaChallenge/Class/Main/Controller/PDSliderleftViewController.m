//
//  PDSliderleftViewController.m
//  PandaChallenge
//
//  Created by 巨鲸 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSliderleftViewController.h"

#import "PDChallengeViewController.h"               // 对战
#import "PDClubViewController.h"                    // 俱乐部系统
#import "PDAddGameViewController.h"
#import "PDSettingViewController.h"                 // 设置
#import "PDPersonalCenterViewController.h"          // 个人信息
#import "PDHistoryViewController.h"                 // 历程
#import "PDRewardViewController.h"                  // 领赏
#import "PDHelpInformationViewController.h"         // 帮助
#import "PDWelfareViewController.h"                 // 福利
#import "PDInviteViewController.h"                  // 邀请好友
#import "PDTestViewController.h"
#import "PDChallengerProgressView.h"                // 进度条
#import "BFPaperTableViewCell.h"
#import "PDHistoryNoticeModel.h"
@interface PDSliderleftViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *sliderTableView;       /**< list*/
@property (nonatomic,strong)NSArray *sliderArray;
@property (nonatomic,strong)NSArray *sliderImageArray;
@property (nonatomic,strong)PDChallengerProgressView *mainProgress; /**< 进度条*/
@property (nonatomic,strong)PDImageView *portraitImage;
@property (nonatomic,strong)UILabel *nicknameLabel;
@property (nonatomic,strong)UILabel *winLabel;
@property (nonatomic,strong) UIView *redView;

@end

@implementation PDSliderleftViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    
    
    NSString *porStr = [[PDLoginModel shareInstance].avatar stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    [self.portraitImage uploadChallengeImageWithURL:porStr placeholder:nil callback:^(UIImage *image) {
        
    }];
    self.nicknameLabel.text = [PDLoginModel shareInstance].name;
    self.winLabel.text = [NSString stringWithFormat:@"胜场:%ld",[PDLoginModel shareInstance].wins];
    
    [self.mainProgress animationWithProgress:[PDLoginModel shareInstance].fight_times/300.];
    [self sendRequestToGetHistoryNoticeData];

}


- (void)sendRequestToGetHistoryNoticeData
{
    PDHistoryNoticeModel *featchModel = [[PDHistoryNoticeModel alloc] init];
    [featchModel fetchWithPath:KCenterHistoryNotice completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (isSucceeded) {
            
            
            if ([featchModel.status isEqualToString:@"0"]) {
                self.redView.hidden = YES;
            }else
            {
                self.redView.hidden = NO;
            }
            [self.sliderTableView reloadData];
        }else
        {
            
        }
    }];
}


- (void)viewWillDisappear:(BOOL)animated
{
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleDefault;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}



#pragma mark -pageSetting
-(void)pageSetting{
    self.view.backgroundColor = [UIColor clearColor];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.sliderArray = @[@"俱乐部",@"历程",@"领赏",@"邀请好友",@"领福利",@"帮助",@"设置"];
    self.sliderImageArray = @[@"icon_center_club",@"icon_center_history",@"icon_center_reward",@"icon_center_invite",@"icon_center_claimwelfare",@"icon_center_help",@"icon_center_setting"];
    
}

#pragma mark - UITableView
-(void)createTableView{
    
    
    
    // 控制顶部view是否一起滑动
    CGFloat topViewHeight = LCFloat(10) + LCFloat(100) +LCFloat(10) +[NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:17]] + LCFloat(18) + [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:11]] + LCFloat(12) ;
    UIView *topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width/4*3, topViewHeight)];
    topView.backgroundColor = RGB(30, 30, 30, 1);
    [self.view addSubview:topView];
    
    self.portraitImage = [[PDImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width/4*3/2-LCFloat(40), LCFloat(20), LCFloat(80), LCFloat(80))];
    self.portraitImage.userInteractionEnabled = YES;
    [self.portraitImage makeRoundedRectangleShape];
    [topView addSubview:self.portraitImage];
    
    
    UITapGestureRecognizer *portraitTapGes = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(portraitTapAction:)];
    [self.portraitImage addGestureRecognizer:portraitTapGes];
    
    
    
    
    // 昵称
    CGFloat nicknameLabelHeight = [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:17]];
    self.nicknameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.portraitImage.frame)+LCFloat(5), self.view.frame.size.width/4*3, nicknameLabelHeight)];
    self.nicknameLabel.font = [UIFont systemFontOfCustomeSize:17];
    self.nicknameLabel.textAlignment = NSTextAlignmentCenter;
    self.nicknameLabel.text = [PDLoginModel shareInstance].name;
    self.nicknameLabel.textColor = [UIColor whiteColor];
    [topView addSubview: self.nicknameLabel];
    
    
    // 4. 创建进度条
    self.mainProgress = [[PDChallengerProgressView alloc]initWithFrame:CGRectMake(self.view.frame.size.width/4*3/2-(LCFloat(98)/2), CGRectGetMaxY(self.nicknameLabel.frame)+ LCFloat(10),  LCFloat(98), LCFloat(8))];
    [self.mainProgress animationWithProgress:0];
    [topView addSubview: self.mainProgress];
    
    // 4.1 重置frame
    
    self.mainProgress.frame = CGRectMake(self.view.frame.size.width/4*3/2-(LCFloat(98)/2), CGRectGetMaxY(self.nicknameLabel.frame)+ LCFloat(10), LCFloat(98), LCFloat(8));
    [self.mainProgress animationWithProgress:0];
    self.mainProgress.sharkView.progressTintColor = [UIColor whiteColor];
    
    UILabel *expLabel = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMinX(self.mainProgress.frame)-40, self.mainProgress.orgin_y, 30, 10)];
    expLabel.font = [UIFont systemFontOfCustomeSize:11];
    expLabel.textAlignment = NSTextAlignmentCenter;
    expLabel.text = @"EXP";
    expLabel.textColor = [UIColor whiteColor];
    [topView addSubview:expLabel];
    
    
    CGFloat winLabelHeight = [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:11]];
    self.winLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.mainProgress.frame) + LCFloat(5), self.view.frame.size.width/4*3, winLabelHeight)];
    self.winLabel.font = [UIFont systemFontOfCustomeSize:11];
    self.winLabel.textAlignment = NSTextAlignmentCenter;
    self.winLabel.text = @"胜场:168";
    self.winLabel.textColor = [UIColor whiteColor];
    [topView addSubview:self.winLabel];
   

    // 线
    
    UIView *xianView = [[UIView alloc] initWithFrame:CGRectMake(LCFloat(10), CGRectGetMaxY(expLabel.frame)+LCFloat(30), self.view.frame.size.width/4*3-LCFloat(20), 0.5f)];
    xianView.backgroundColor = [UIColor colorWithCustomerName:@"灰色3"];
    xianView.alpha = 0.1;
    [topView addSubview:xianView];
    
    
    
    if (!self.sliderTableView){
        self.sliderTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, topViewHeight -LCFloat(2), self.view.frame.size.width/4*3, self.view.frame.size.height -(topViewHeight) +LCFloat(2)) style:UITableViewStylePlain];
        //        self.sliderTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.sliderTableView.delegate = self;
        self.sliderTableView.dataSource = self;
        self.sliderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.sliderTableView.backgroundColor = RGB(30, 30, 30, 1.0);
        self.sliderTableView.backgroundView = nil;
        self.sliderTableView.bounces = NO;
        //        self.sliderTableView.contentInset = UIEdgeInsetsMake(LCFloat(70), 0, 0, 0);
        self.sliderTableView.opaque = NO;
        [self.view addSubview:self.sliderTableView];
    }
    
    

    
}


#pragma mark - 头像点击进入
- (void)portraitTapAction:(UITapGestureRecognizer *)sender
{
    PDPersonalCenterViewController *personalCenter = [[PDPersonalCenterViewController alloc] init];
    [self.sideMenuViewController setContentViewController:[[UINavigationController alloc]initWithRootViewController:personalCenter] animated:YES];
    [self.sideMenuViewController hideMenuViewController];
}



#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.sliderArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    BFPaperTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[BFPaperTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.textLabel.text = [self.sliderArray objectAtIndex:indexPath.row];
        cellWithRowOne.imageView.image = [UIImage imageNamed:self.sliderImageArray[indexPath.row]];
        
        cellWithRowOne.selectedBackgroundView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_center_selectbg"]];
        cellWithRowOne.textLabel.font = [UIFont systemFontOfCustomeSize:15];
        cellWithRowOne.textLabel.textColor = [UIColor whiteColor];
        cellWithRowOne.backgroundColor = RGB(30, 30, 30, 1.0);
        cellWithRowOne.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        cellWithRowOne.tintColor = [UIColor whiteColor];
        
        if (indexPath.row == 1) {
            
            CGSize size = [cellWithRowOne.textLabel.text sizeWithCalcFont:cellWithRowOne.textLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, CGFLOAT_MAX)];
            self.redView = [[UIView alloc] initWithFrame:CGRectMake(size.width + 50, 15, 7, 7)];
            self.redView.backgroundColor = [UIColor hexChangeFloat:@"fc5453"];
            [self.redView makeRoundedRectangleShape];
            [cellWithRowOne addSubview:self.redView];
            self.redView.hidden = YES;
        }
    }
   
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     if (indexPath.row == 0){                // 俱乐部
        if ([USERDEFAULTS boolForKey:UD_CLUB_CAN_SHOW]) {
            PDAddGameViewController *addGameViewController = [[PDAddGameViewController alloc] init];
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc]initWithRootViewController:addGameViewController] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        } else {
            PDClubViewController *clubViewController = [[PDClubViewController alloc]init];
            [self.sideMenuViewController setContentViewController:[[UINavigationController alloc]initWithRootViewController:clubViewController] animated:YES];
            [self.sideMenuViewController hideMenuViewController];
        }
    } else if (indexPath.row == 1){        // 历程
        
        PDFetchModel *fetchModel = [[PDFetchModel alloc] init];
        [fetchModel fetchWithPath:KCenterHistoryUpdateCheck completionHandler:^(BOOL isSucceeded, NSError *error) {
            if (isSucceeded) {

            }else{
                
            }
        }];
        
        
                PDHistoryViewController *historyViewController = [[PDHistoryViewController alloc]init];
                [self.sideMenuViewController setContentViewController:[[UINavigationController alloc]initWithRootViewController:historyViewController] animated:YES];
                [self.sideMenuViewController hideMenuViewController];
    } else if (indexPath.row == 2){         // 领赏
        PDRewardViewController *rewardViewController = [[PDRewardViewController alloc]init];
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc]initWithRootViewController:rewardViewController] animated:YES];
        [self.sideMenuViewController hideMenuViewController];

        
    }else if (indexPath.row == 3){
        // 邀请好友
        PDInviteViewController *inviteViewController = [[PDInviteViewController alloc]init];
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc]initWithRootViewController:inviteViewController] animated:YES];
        [self.sideMenuViewController hideMenuViewController];
        
    }else if (indexPath.row == 4){
        // 领福利
        PDWelfareViewController *welfareViewController = [[PDWelfareViewController alloc]init];
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc]initWithRootViewController:welfareViewController] animated:YES];
        [self.sideMenuViewController hideMenuViewController];
        
    }else if (indexPath.row == 5){
        // 帮助
        PDHelpInformationViewController *helpInformationViewController = [[PDHelpInformationViewController alloc] init];
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc] initWithRootViewController:helpInformationViewController] animated:YES];
        [self.sideMenuViewController hideMenuViewController];
    }else if (indexPath.row == 6){
        // 设置
        PDSettingViewController *settingViewController = [[PDSettingViewController alloc]init];
        [self.sideMenuViewController setContentViewController:[[UINavigationController alloc]initWithRootViewController:settingViewController] animated:YES];
        [self.sideMenuViewController hideMenuViewController];
        
    }
}
@end
