//
//  PDNavigationController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNavigationController.h"
#import "AbstractViewController.h"

@implementation PDNavigationController

- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    if ([viewController isKindOfClass:[AbstractViewController class]]) {
        AbstractViewController *abstractViewController = (AbstractViewController *)viewController;
        if (self.viewControllers.count == 0) {
            abstractViewController.hidesBottomBarWhenPushed = NO;
        } else {
            abstractViewController.hidesBottomBarWhenPushed = YES;
        }
    }
    [super pushViewController:viewController animated:animated];
}

@end
