//
//  PDTimerButton.m
//  PandaChallenge
//
//  Created by panda on 16/3/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTimerButton.h"
@interface PDTimerButton ()
@property (nonatomic,assign)NSInteger seconds;
@property (nonatomic,assign)TimerType timerType;
@end
@implementation PDTimerButton
- (instancetype)initWithTimerType:(TimerType)type actionBlock:(void(^)())actionBlock
{
    if (self = [super init]) {
        /**< 按钮的配置*/
        self.layer.cornerRadius = self.cornerRadius?self.cornerRadius : 5.;
        [self setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
        [self setTitle:@"验证码" forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.timerType = type;
        
        /**< 判断是否初始化定时器*/
        self.isTimeOut = [self isTimeOut];
        if (!self.isTimeOut) {
            NSInteger currentSeconds = 60 - [self getGapInterval];
            [self startTimer:currentSeconds];
//            NSLog(@"currentSeconds==%ld",(long)currentSeconds);
        }
        
        __weak typeof(self) weakSelf = self;
        [self buttonWithBlock:^(UIButton *button) {
            if (!weakSelf) return;
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf saveTimerInterval];
            [strongSelf startTimer:60];
            if (actionBlock) {
                actionBlock();
            }
        }];
    }
    return self;
}

#pragma mark - 判断是否超过60s
/**< 1.初次进入与每次60s结束取值为空或者超过60s,返回timeOut 2.判断超过60s返回timeOut*/
- (BOOL)isTimeOut
{
    if ([PDTool isEmpty:[self getTimerInterval]]) {
        return YES;
    }else{
        NSInteger gap = [self getGapInterval];
        if (gap >= 60) {
            return YES;
        } else {
            return NO;
        }
    }
}
/**< 计算当前时间戳与存储时间戳的差值*/
- (NSInteger)getGapInterval
{
    NSString *lastIntervalString = [self getTimerInterval];
    NSInteger lastInterVal = [lastIntervalString integerValue];
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval currentInterval = [date timeIntervalSince1970];
    double gapInterval = currentInterval - lastInterVal;
    NSInteger gap = (NSInteger)(gapInterval);
//    NSLog(@"lastInterval==%ld,currentInterval==%f",(long)lastInterVal            ,currentInterval);
    return gap;
}
#pragma mark - 在UserDefault存取时间戳
- (NSString *)getTimerInterval
{
    NSString *intervalString;
//    NSLog(@"%ld",(long)self.timerType);
    switch (self.timerType) {
        case registerTimer:
            intervalString = [PDTool userDefaultGetWithKey:CURRENT_TIMEINTERVAL_REGISTER_KEY];
            break;
        case passwordTimer:
            intervalString = [PDTool userDefaultGetWithKey:CURRENT_TIMEINTERVAL_PASSWORD_KEY];
            break;
        case exchangeTimer:
            intervalString = [PDTool userDefaultGetWithKey:CURRENT_TIMEINTERVAL_EXCHANGE_KEY];
            break;
        default:
            break;
    }
//    NSLog(@"getInterVal=%@",intervalString);
    return intervalString;
}

- (void)saveTimerInterval
{
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval timeInterval = [date timeIntervalSince1970];
    NSString *timerString = [NSString stringWithFormat:@"%f",timeInterval];
//    NSLog(@"saveInterVal=%f",timeInterval);
    switch (self.timerType) {
        case registerTimer:
            [PDTool userDefaulteWithKey:CURRENT_TIMEINTERVAL_REGISTER_KEY Obj:timerString];
            break;
        case passwordTimer:
            [PDTool userDefaulteWithKey:CURRENT_TIMEINTERVAL_PASSWORD_KEY Obj:timerString];
            break;
        case exchangeTimer:
            [PDTool userDefaulteWithKey:CURRENT_TIMEINTERVAL_EXCHANGE_KEY Obj:timerString];
            break;
        default:
            break;
    }
}

#pragma mark - 定时器
- (void)startTimer:(NSInteger)gap
{
    self.seconds = gap;
    if (!self.timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(buttonCountDown) userInfo:nil repeats:YES];
        self.isEnable = NO;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:10];
        [self setTitle:[NSString stringWithFormat:@"重新发送%lis",(long)self.seconds] forState:UIControlStateNormal];
    }
}
- (void)buttonCountDown
{
    self.seconds--;
    [self setTitle:[NSString stringWithFormat:@"重新发送%lis",(long)self.seconds] forState:UIControlStateNormal];
    if (self.seconds == 0) {
        [self.timer invalidate];
        self.timer = nil;
        [self setTitle:@"验证码" forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        if (self.isCanClickByPhone) {
            self.isEnable = YES;
        }
        switch (self.timerType) {
            case registerTimer:
                [PDTool userDefaultDelegtaeWithKey:CURRENT_TIMEINTERVAL_REGISTER_KEY];
                break;
            case passwordTimer:
                [PDTool userDefaultDelegtaeWithKey:CURRENT_TIMEINTERVAL_PASSWORD_KEY];
                break;
            case exchangeTimer:
                [PDTool userDefaultDelegtaeWithKey:CURRENT_TIMEINTERVAL_EXCHANGE_KEY];
                break;
            default:
                break;
        }
    }
}

- (void)setIsEnable:(BOOL)isEnable
{
    _isEnable = isEnable;
    self.enabled = isEnable;
    if (!isEnable) {
        self.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    }else{
        self.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    }
    
}
@end
