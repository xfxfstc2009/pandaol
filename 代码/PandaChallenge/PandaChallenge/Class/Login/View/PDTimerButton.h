//
//  PDTimerButton.h
//  PandaChallenge
//
//  Created by panda on 16/3/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,TimerType){
    registerTimer,              /**< 注册*/
    passwordTimer,              /**< 忘记密码*/
    exchangeTimer,              /**< 兑换*/
};
@interface PDTimerButton : UIButton
@property (nonatomic,assign)CGFloat cornerRadius;
/**< 按钮是否可以点击*/
@property (nonatomic,assign)BOOL isEnable;
/**< 是否超过60s*/
@property (nonatomic,assign)BOOL isTimeOut;
/**< 当textField中的手机号有效是为YES*/
@property (nonatomic,assign)BOOL isCanClickByPhone;

@property (nonatomic,strong)NSTimer *timer;
/**< 初始化实例*/
- (instancetype)initWithTimerType:(TimerType)type actionBlock:(void(^)())actionBlock;
@end
