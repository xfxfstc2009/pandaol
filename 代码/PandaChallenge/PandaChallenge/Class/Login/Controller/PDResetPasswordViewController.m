//
//  PDResetPasswordViewController.m
//  PandaChallenge
//
//  Created by panda on 16/3/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDResetPasswordViewController.h"
#import "PDLoginViewController.h"
#import "PDSureButton.h"
#import "PDLoginAlertView.h"
#define PDLeftRight_Space (105/2.)
@interface PDResetPasswordViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (nonatomic,strong)JCAlertView *alertView;
@property (nonatomic,strong)UITextField *passwordTextField;
@property (nonatomic,strong)UITextField *passwordAgainTextField;
@property (nonatomic,strong)PDSureButton *commitButton;
@property (nonatomic,strong)NSArray *resetArray;
@property (nonatomic,strong)UITableView *resetTableView;
//@property (nonatomic,copy) NSString *phone;
@end

@implementation PDResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createResetTableView];
}
#pragma mark - PageSetting
- (void)pageSetting
{
    self.barMainTitle = @"修改密码";
    __weak typeof(self) weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"login_back"] barHltImage:nil action:^{
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
}
#pragma mark - Array With Init
- (void)arrayWithInit
{
    self.resetArray = @[@[@"密码",@"确定密码"],@[@"提示",@"提示",@"提示"],@[@"提交按钮"]];
}
#pragma mark - UITableView
- (void)createResetTableView
{
    if (!self.resetTableView) {
        self.resetTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height) style:UITableViewStylePlain];
        self.resetTableView.delegate = self;
        self.resetTableView.dataSource = self;
        self.resetTableView.backgroundView = nil;
        self.resetTableView.backgroundColor = [UIColor clearColor];
        self.resetTableView.showsVerticalScrollIndicator = NO;
        self.resetTableView.scrollEnabled = YES;
        self.resetTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.resetTableView];
    }
}
#pragma mark - UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.resetArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sectionArray = [self.resetArray objectAtIndex:section];
    return sectionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0) {       //输入框
        static NSString *cellForIdentifierRowOne = @"cellForIdentifierRowOne";
        UITableViewCell *cellForRowOne = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierRowOne];
        if (!cellForRowOne) {
            cellForRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierRowOne];
            cellForRowOne.backgroundColor = [UIColor clearColor];
            cellForRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            //1.iconButton
            UIButton *iconButton = [UIButton buttonWithType:UIButtonTypeCustom];
            iconButton.stringTag = @"iconButton";
            [cellForRowOne addSubview:iconButton];
            //2.textField
            UITextField *textField = [[UITextField alloc]init];
            textField.stringTag = @"textField";
            textField.delegate = self;
            textField.font = [UIFont systemFontOfCustomeSize:13];
            textField.borderStyle = UITextBorderStyleNone;
            textField.backgroundColor = [UIColor clearColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            [textField addTarget:self action:@selector(textFieldChangeManager:) forControlEvents:UIControlEventEditingChanged];
            [cellForRowOne addSubview:textField];
            //3.bottomLine
            UIView *line = [[UIView alloc]init];
            line.stringTag = @"line";
            line.backgroundColor = [UIColor lightGrayColor];
            [cellForRowOne addSubview:line];
        }
        UIButton *iconButton = (UIButton *)[cellForRowOne viewWithStringTag:@"iconButton"];
        UITextField *textField = (UITextField *)[cellForRowOne viewWithStringTag:@"textField"];
        UIView *line = (UIView *)[cellForRowOne viewWithStringTag:@"line"];
        //frame
        iconButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), LCFloat(15), cellHeight - LCFloat(22), cellHeight -LCFloat(22));
        textField.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(20), LCFloat(12), kScreenBounds.size.width - (LCFloat(PDLeftRight_Space) + LCFloat(20) +CGRectGetMaxX(iconButton.frame)),cellHeight - LCFloat(12));
        line.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(10), CGRectGetMaxY(textField.frame), CGRectGetWidth(textField.frame) + LCFloat(10), 1);
        //赋值
        if (indexPath.row == 0) {
            [iconButton setBackgroundImage:[UIImage imageNamed:@"login_lock"] forState:UIControlStateNormal];
            textField.placeholder = @"新密码";
            textField.returnKeyType = UIReturnKeyNext;
            textField.secureTextEntry = YES;
            self.passwordTextField = textField;
        }else{
            [iconButton setBackgroundImage:[UIImage imageNamed:@"login_lock"] forState:UIControlStateNormal];
            textField.placeholder = @"再次输入密码";
            textField.returnKeyType = UIReturnKeyDone;
            textField.secureTextEntry = YES;
            self.passwordAgainTextField = textField;
        }
        return cellForRowOne;
    }else if (indexPath.section == 1){      //提示
        static NSString *cellForIdentifierRowTwo = @"cellForIdentifierRowTwo";
        UITableViewCell *cellForRowTwo = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierRowTwo];
        if (!cellForRowTwo) {
            cellForRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierRowTwo];
            cellForRowTwo.backgroundColor = [UIColor clearColor];
            cellForRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            //1.iconImageView
            UIImageView *iconImageView = [[UIImageView alloc]init];
            iconImageView.stringTag = @"iconImageView";
            [cellForRowTwo addSubview:iconImageView];
            //2.tipLabel
            UILabel *tipLabel = [[UILabel alloc]init];
            tipLabel.font = [UIFont systemFontOfCustomeSize:12];
            tipLabel.textColor = [UIColor grayColor];
            tipLabel.stringTag = @"tipLabel";
            [cellForRowTwo addSubview:tipLabel];
        }
        UIImageView *iconImageView = (UIImageView *)[cellForRowTwo viewWithStringTag:@"iconImageView"];
        UILabel *tipLabel = (UILabel *)[cellForRowTwo viewWithStringTag:@"tipLabel"];
        //frame
        iconImageView.frame = CGRectMake(CGRectGetMinX(self.passwordTextField.frame)-LCFloat(20) , LCFloat(4), cellHeight - 2 * LCFloat(4), cellHeight - 2 * LCFloat(4));
        tipLabel.frame = CGRectMake(CGRectGetMaxX(iconImageView.frame) + LCFloat(6), 0, kScreenBounds.size.width -(CGRectGetMaxX(iconImageView.frame) + LCFloat(6)), cellHeight);
        //赋值
        iconImageView.image = [UIImage imageNamed:@"login_tip"];
        if (indexPath.row == 0) {
            tipLabel.text = @"密码长度为6-16位字母和数字";
        }else if (indexPath.row == 1){
            tipLabel.text = @"密码不能包含空格";
        }else if (indexPath.row == 2){
            tipLabel.text = @"密码必须相同";
        }
        return cellForRowTwo;
    }else{      //确认按钮
        static NSString *cellForIdentifierRowThree = @"cellForIdentifierRowThree";
        UITableViewCell *cellForRowThree = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierRowThree];
        if (!cellForRowThree) {
            cellForRowThree = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierRowThree];
            cellForRowThree.backgroundColor = [UIColor clearColor];
            cellForRowThree.selectionStyle = UITableViewCellSelectionStyleNone;
            __weak typeof(self) weakSelf = self;
            self.commitButton = [[PDSureButton alloc]initWithTitle:@"提交" WithActionBlock:^{
                if (!weakSelf) {
                    return;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf.view endEditing:YES];
                [strongSelf reSetPasswordManager];
            }];
            self.commitButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), 0, kScreenBounds.size.width - 2 * LCFloat(PDLeftRight_Space), cellHeight);
            self.commitButton.isEnable = NO;
            [cellForRowThree addSubview:self.commitButton];
        }
        return cellForRowThree;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return LCFloat(44);
    }else if (indexPath.section == 1){
        return [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:12]] + LCFloat(8);
    }else{
        return LCFloat(44);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager:)];
    [footerView addGestureRecognizer:tap];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 2) {
        return LCFloat(150);
    }else{
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager:)];
    [headerView addGestureRecognizer:tap];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return LCFloat(22);
    }else if (section == 1){
        return LCFloat(11);
    }else{
        return LCFloat(37);
    }
}
#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (toBeString.length > 16) {
        return NO;
    }
    return YES;
}
- (void)textFieldChangeManager:(UITextField *)textField
{
    if (self.passwordTextField.text.length >=6 && self.passwordAgainTextField.text.length >=6) {
        self.commitButton.isEnable = YES;
    }else{
        self.commitButton.isEnable = NO;
    }
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.passwordTextField && textField.returnKeyType == UIReturnKeyNext) {
        [self.passwordAgainTextField becomeFirstResponder];
    }else if (textField == self.passwordAgainTextField && textField.returnKeyType == UIReturnKeyDone){
        [self.passwordAgainTextField resignFirstResponder];
    }
    return YES;
}
#pragma mark - UIGestureRecognizer
- (void)tapManager:(UITapGestureRecognizer *)tap
{
    [self.view endEditing:YES];
}
#pragma mark - 键盘通知
- (void)createkeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [value CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    CGFloat keyboard_Y = keyboardRect.origin.y;
    CGRect newTableViewFrame = self.view.bounds;
    newTableViewFrame.size.height = keyboard_Y - self.view.bounds.origin.y;
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.resetTableView.frame = newTableViewFrame;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *animationDurationVale = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationVale getValue:&animationDuration];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.resetTableView.frame = self.view.bounds;
    [UIView commitAnimations];
}

- (void)dealloc
{
    NSLog(@"...");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - 接口
#pragma mark reSetPasswordManager
- (void)reSetPasswordManager
{
    if (![self checkTextFieldInput]) {
        return;
    }
    self.commitButton.isEnable = NO;
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    fetchModel.requestParams = @{@"phone":self.phone,@"password":self.passwordTextField.text};
    __weak typeof(self) weakSelf = self;
    [fetchModel fetchWithPath:KRet_Password completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf) return;
         __strong typeof(weakSelf) strongSelf = weakSelf;
         strongSelf.commitButton.isEnable = YES;
        if (isSucceeded) {
            [strongSelf alertShow];
            } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];
}

#pragma mark alert
- (void)alertShow
{
    __weak typeof(self) weakSelf = self;
    PDAlertShowView *passwordAlertView = [[PDAlertShowView alloc]init];
    [passwordAlertView alertWithTitle:@"重置成功" headerImage:[UIImage imageNamed:@"icon_report_success"] titleDesc:@"请妥善保管您的密码" buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (index == 0) {
            [strongSelf popToLoginViewController];
        }
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:passwordAlertView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}

- (void)popToLoginViewController
{
    [self.alertView dismissWithCompletion:NULL];
    [self.navigationController popToRootViewControllerAnimated:YES];
}
#pragma mark - error
- (BOOL)checkTextFieldInput
{
    if ([PDTool isEmpty:self.passwordTextField.text] || [PDTool isEmpty:self.passwordAgainTextField.text]) {
        [PDHUD showShimmeringString:@"密码不能为空" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else if (![PDTool validatePassword:self.passwordTextField.text] || ![PDTool validatePassword:self.passwordAgainTextField.text]){
        [PDHUD showShimmeringString:@"密码只能为数字和字母" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else if (![self.passwordTextField.text isEqualToString:self.passwordAgainTextField.text]){
         [PDHUD showShimmeringString:@"两次密码不相同" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else{
        return YES;
    }
}
#pragma mark - srollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([self.passwordTextField isFirstResponder] || [self.passwordAgainTextField isFirstResponder]) {
        [self.view endEditing:YES];
    }
}

@end
