//
//  PDRegistViewController.m
//  PandaChallenge
//
//  Created by panda on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRegistViewController.h"
#import "PDProtocolViewController.h"
#import "PDRoleBindingViewController.h"
#import "PDWelfareViewController.h"
#import "PDLoginAlertView.h"
#import "PDTimerButton.h"
#import "PDSureButton.h"
#import "PDAlertView.h"
#import "PDSMSModel.h"
#import "PDChallengerGameEndAlertView.h"
#define PDLeftRight_Space (105/2.)
@interface PDRegistViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong)UITableView *registerTableView;
@property (nonatomic,strong)NSArray *registerArray;
@property (nonatomic,strong)JCAlertView *alertView;
@property (nonatomic,strong)UITextField *userTextField;
@property (nonatomic,strong)UITextField *passWordTextField;
@property (nonatomic,strong)UITextField *passWordAgainTextField;
@property (nonatomic,strong)UITextField *phoneCodeTextField;
@property (nonatomic,strong)PDTimerButton *getCodeButton;
@property (nonatomic,strong)PDSureButton *registerButton;
@property (nonatomic,strong)UIButton *backButton;
@property (nonatomic,strong)PDSMSModel *smsModel;
@end

@implementation PDRegistViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createRegistTableView];
    [self createKeyBoardNotification];
}
#pragma mark - PageSetting
- (void)pageSetting
{
    self.barMainTitle = @"注册";
    __weak typeof(self) weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"login_back"] barHltImage:nil action:^{
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
}
#pragma mark - Array With Init
- (void)arrayWithInit
{
    self.registerArray = @[@[@"手机账号",@"密码",@"确认密码",@"验证码和短信按钮"],@[@"提交按钮"],@[@"协议按钮"]];
}
#pragma mark - UITableView
- (void)createRegistTableView
{
    if (!self.registerTableView) {
        self.registerTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height) style:UITableViewStylePlain];
        self.registerTableView.delegate = self;
        self.registerTableView.dataSource = self;
        self.registerTableView.backgroundView = nil;
        self.registerTableView.backgroundColor = [UIColor clearColor];
        self.registerTableView.showsVerticalScrollIndicator = NO;
        self.registerTableView.scrollEnabled = YES;
        self.registerTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.registerTableView];
    }
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.registerArray.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sectionArray = [self.registerArray objectAtIndex:section];
    return sectionArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0) {       //输入框
        static NSString *cellForIdentifierWithRowOne = @"cellForIdentifierWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierWithRowOne];
        
        if (!cellWithRowOne) {
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
            
            //1.iconButton
            UIButton *iconButton = [UIButton buttonWithType:UIButtonTypeCustom];
            iconButton.stringTag = @"iconButton";
            iconButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), LCFloat(15), cellHeight - LCFloat(24), cellHeight -LCFloat(24));
            [cellWithRowOne addSubview:iconButton];
            //2.textField
            UITextField *textField = [[UITextField alloc]init];
            textField.stringTag = @"textField";
            textField.delegate = self;
            textField.backgroundColor = [UIColor clearColor];
            textField.borderStyle = UITextBorderStyleNone;
            textField.textColor = [UIColor blackColor];
            textField.font = [UIFont systemFontOfCustomeSize:13];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            [textField addTarget:self action:@selector(textFieldChangeManager:) forControlEvents:UIControlEventEditingChanged];
            [cellWithRowOne addSubview:textField];
            //3.timerButton
            __weak typeof(self) weakSelf = self;
            PDTimerButton *timerButton = [[PDTimerButton alloc]initWithTimerType:registerTimer actionBlock:^{
                 if (!weakSelf) return;
                 __strong typeof(weakSelf) strongSelf = weakSelf;
                 [strongSelf registerSmsCode];
             }];
            timerButton.isEnable = NO;
            timerButton.stringTag = @"timerButton";
            [cellWithRowOne addSubview:timerButton];
            //4.line
            UIView *line = [[UIView alloc]init];
            line.backgroundColor = [UIColor grayColor];
            line.stringTag = @"line";
            [cellWithRowOne addSubview:line];
        }
        UIButton *iconButton = (UIButton *)[cellWithRowOne viewWithStringTag:@"iconButton"];
        UITextField *textField = (UITextField *)[cellWithRowOne viewWithStringTag:@"textField"];
        PDTimerButton *timerButton = (PDTimerButton *)[cellWithRowOne viewWithStringTag:@"timerButton"];
        UIView *line = (UIView *)[cellWithRowOne viewWithStringTag:@"line"];
        //设置frame
        if (indexPath.row == 3) {
            timerButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(PDLeftRight_Space) - LCFloat(80) - LCFloat(5), LCFloat(14), LCFloat(80), cellHeight - LCFloat(14));
            textField.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(20), LCFloat(12), CGRectGetMinX(timerButton.frame) - CGRectGetMaxX(iconButton.frame) - LCFloat(20) - LCFloat(5),cellHeight - LCFloat(12));
            line.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(10), CGRectGetMaxY(textField.frame), CGRectGetWidth(textField.frame) + LCFloat(10), 1);
            timerButton.hidden = NO;
        }else{
            textField.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(20), LCFloat(12), kScreenBounds.size.width - (LCFloat(PDLeftRight_Space) + LCFloat(20) +CGRectGetMaxX(iconButton.frame)),cellHeight - LCFloat(12));
             line.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(10), CGRectGetMaxY(textField.frame), CGRectGetWidth(textField.frame) + LCFloat(10), 1);
            timerButton.hidden = YES;
        }
        //赋值
        if (indexPath.row == 0) {
            [iconButton setBackgroundImage:[UIImage imageNamed:@"login_phone"] forState:UIControlStateNormal];
            textField.placeholder = @"手机号账号";
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.returnKeyType = UIReturnKeyNext;
            self.userTextField = textField;
        }else if (indexPath.row == 1){
            [iconButton setBackgroundImage:[UIImage imageNamed:@"login_lock"] forState:UIControlStateNormal];
            textField.placeholder = @"请输入6-16位字母或数字";
            textField.returnKeyType = UIReturnKeyNext;
            textField.secureTextEntry = YES;
            self.passWordTextField = textField;
        }else if (indexPath.row == 2){
            [iconButton setBackgroundImage:[UIImage imageNamed:@"login_lock"] forState:UIControlStateNormal];
            textField.placeholder = @"请确认您的密码";
            textField.returnKeyType = UIReturnKeyDone;
            textField.secureTextEntry = YES;
            self.passWordAgainTextField = textField;
        }else{
            [iconButton setBackgroundImage:[UIImage imageNamed:@"login_code"] forState:UIControlStateNormal];
            textField.placeholder = @"验证码";
            textField.returnKeyType = UIReturnKeyDone;
            textField.keyboardType = UIKeyboardTypeNumberPad;
            self.phoneCodeTextField = textField;
            self.getCodeButton = timerButton;
        }
        return cellWithRowOne;
    }else if(indexPath.section == 1){       //提交按钮
        static NSString *cellForIdentifierWithRowTwo = @"cellForIdentifierWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierWithRowTwo];
        if (!cellWithRowTwo) {
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowTwo.backgroundColor = [UIColor clearColor];
            __weak typeof(self) weakSelf = self;
           self.registerButton = [[PDSureButton alloc]initWithTitle:@"提交注册" WithActionBlock:^{
               if (!weakSelf) return;
               __strong typeof(weakSelf) strongSelf = weakSelf;
               [strongSelf registerManager];
            }];
            self.registerButton.isEnable = NO;
            self.registerButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), 0, kScreenBounds.size.width - 2 * LCFloat(PDLeftRight_Space), cellHeight);
            [cellWithRowTwo addSubview:self.registerButton];
        }
        return cellWithRowTwo;
    }else{      //协议按钮
        static NSString *cellForIdentifierWithRowThree = @"cellForIdentifierWithRowThree";
        UITableViewCell *cellWithRowThree = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierWithRowThree];
        
        if (!cellWithRowThree) {
            cellWithRowThree = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierWithRowThree];
            cellWithRowThree.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowThree.backgroundColor = [UIColor clearColor];
            //protocalButton
            NSString *protocalString = @"点击\"提交注册\"按钮，即表示您同意《用户使用协议》";
            CGSize protocalButtonSize = [protocalString sizeWithCalcFont:[UIFont systemFontOfCustomeSize:11]];
            __weak typeof(self) weakSelf = self;
            UIButton *protocalButton = [self createCustomerButtonViewWithTitle:protocalString font:[UIFont systemFontOfCustomeSize:11] backgroundImage:nil Block:^{
                if (!weakSelf) return;
                __strong typeof(weakSelf) strongSelf = weakSelf;
                PDProtocolViewController *protocolViewController = [[PDProtocolViewController alloc]init];
                [strongSelf.navigationController pushViewController:protocolViewController animated:NO];
            }];
            protocalButton.center = CGPointMake(kScreenBounds.size.width/2, cellHeight/2);
            protocalButton.bounds = CGRectMake(0, 0, protocalButtonSize.width, protocalButtonSize.height);
            [cellWithRowThree addSubview:protocalButton];
            
            UIView *protocalLine = [[UIView alloc]init];
            protocalLine.backgroundColor = [UIColor lightGrayColor];
            protocalLine.frame = CGRectMake(CGRectGetMinX(protocalButton.frame), CGRectGetMaxY(protocalButton.frame), CGRectGetWidth(protocalButton.frame), 1);
            [cellWithRowThree addSubview:protocalLine];
        }
        return cellWithRowThree;
    }
}
#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return LCFloat(44);
    }else if (indexPath.section == 1){
        return LCFloat(44);
    }else{
        return [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:11]];
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager:)];
    [footerView addGestureRecognizer:tap];
    return footerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 2) {
        return LCFloat(150);
    }else{
        return 0;
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager:)];
    [headerView addGestureRecognizer:tap];
    return headerView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return LCFloat(22);
    }else if (section == 1){
        return LCFloat(37);
    }else{
        return LCFloat(15);
    }
}
#pragma mark - UITetxFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.userTextField && textField.returnKeyType ==  UIReturnKeyNext ) {
        [self.passWordTextField becomeFirstResponder];
    }else if (textField == self.passWordTextField && textField.returnKeyType == UIReturnKeyNext){
        [self.passWordAgainTextField becomeFirstResponder];
    }else if (textField == self.passWordAgainTextField && textField.returnKeyType == UIReturnKeyDone){
        [textField resignFirstResponder];
    }else if(textField == self.phoneCodeTextField && textField.returnKeyType == UIReturnKeyDone){
        [textField resignFirstResponder];
    }
    return YES;
}

- (void)textFieldChangeManager:(UITextField *)textField
{
    if (textField == self.userTextField) {
        if ([PDTool validateMobile:self.userTextField.text] && self.getCodeButton.isTimeOut) {
            __weak typeof(self) weakSelf = self;
            [self phoneHasRegisterCompletion:^(BOOL isHasRegister) {
                if (!weakSelf) return;
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (isHasRegister) {
                    strongSelf.getCodeButton.isEnable = YES;
                    strongSelf.getCodeButton.isCanClickByPhone = YES;
                }
            }];
        }else if (self.getCodeButton.isTimeOut){
            self.getCodeButton.isEnable = NO;
            self.getCodeButton.isCanClickByPhone = NO;
        }else if (!self.getCodeButton.isTimeOut && [PDTool validateMobile:self.userTextField.text]){
            __weak typeof(self) weakSelf = self;
            [self phoneHasRegisterCompletion:^(BOOL isHasRegister) {
                if (!weakSelf) return;
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (isHasRegister) {
                    strongSelf.getCodeButton.isCanClickByPhone = YES;
                }
            }];
        }else if (!self.getCodeButton.isTimeOut){
            self.getCodeButton.isCanClickByPhone = NO;
        }
    }
    if (self.userTextField.text.length == 11 && self.passWordTextField.text.length >=6 && self.passWordAgainTextField.text.length >=6 && self.phoneCodeTextField.text.length == 5) {
        self.registerButton.isEnable = YES;
    }else{
        self.registerButton.isEnable = NO;
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *toBeString =[textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.userTextField) {
        if (toBeString.length>11) {
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
    }else if (textField == self.passWordTextField){
        if (toBeString.length>16) {
            return NO;
        }
    }else if (textField == self.passWordAgainTextField){
        if (toBeString.length>16) {
            return NO;
        }
    }else if(textField == self.phoneCodeTextField){
        if (toBeString.length>5) {
            textField.text = [toBeString substringToIndex:5];
            return NO;
        }
    }
    return YES;
}

#pragma mark - 键盘通知
- (void)createKeyBoardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [value CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    CGFloat keyboard_Y = keyboardRect.origin.y;
    CGRect newTableViewFrame = self.view.bounds;
    newTableViewFrame.size.height = keyboard_Y - self.view.bounds.origin.y;
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.registerTableView.frame = newTableViewFrame;
    [UIView commitAnimations];

}
- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *animationDurationVale = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationVale getValue:&animationDuration];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.registerTableView.frame = self.view.bounds;
    [UIView commitAnimations];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - UITapGestureRecognizer
- (void)tapManager:(UITapGestureRecognizer *)tap
{
    [self.view endEditing:YES];
}
#pragma mark - CustomButton创建
-(UIButton *)createCustomerButtonViewWithTitle:(NSString *)title font:(UIFont *)font backgroundImage:(UIImage *)backgroundImage Block:(void(^)())block{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = font;
    if (backgroundImage) {
        [button setBackgroundImage:backgroundImage forState:UIControlStateNormal];
    }
    __weak typeof(self)weakSelf = self;
    [button buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    return button;
}
#pragma mark - 接口
#pragma mark 短信验证码
-(void)registerSmsCode{
    PDFetchModel *smsModel = [[PDFetchModel alloc]init];
    smsModel.requestParams = @{@"phone":self.userTextField.text};
    __weak typeof(self)weakSelf = self;
    [smsModel fetchWithPath:kSMS_Register_VerifyPath completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            [PDHUD showShimmeringString:@"短信已发送" maskType:WSProgressHUDMaskTypeDefault delay:2];
        } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];
}
#pragma mark 账号注册
- (void)registerManager
{
    [self.view endEditing:YES];
    if (![self checkRegister]) {
        return;
    }
    self.registerButton.isEnable = NO;
    [self registerByInput];;
}

-(void)registerByInput{
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    fetchModel.requestParams = @{@"phone":self.userTextField.text,@"password":self.passWordTextField.text,@"password_again":self.passWordAgainTextField.text,@"phone_code":self.phoneCodeTextField.text};
    __weak typeof(self) weakSelf = self;
    [fetchModel fetchWithPath:kRegister completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.registerButton.isEnable = YES;
            if (isSucceeded){
                [strongSelf alertShow];
            } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];
}
#pragma mark alert
- (void)alertShow
{
    __weak typeof(self) weakSelf = self;
    PDAlertShowView *registerAlertView = [[PDAlertShowView alloc]init];
    [registerAlertView alertWithTitle:@"注册成功" headerImage:[UIImage imageNamed:@"icon_report_success"] titleDesc:@"欢迎使用PANDAOL" buttonNameArray:@[@"确定"] clickBlock:^(NSInteger index) {
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (index == 0) {
            [strongSelf authorizeLogin];
        }
    }];
    self.alertView = [[JCAlertView alloc]initWithCustomView:registerAlertView dismissWhenTouchedBackground:NO];
    [self.alertView show];
}
#pragma mark 自动登录
-(void)authorizeLogin {
    [self.alertView dismissWithCompletion:NULL];
    __weak typeof(self)weakSelf = self;
    [[PDLoginModel shareInstance] loginWithType:PDLoginModelCurrent user:self.userTextField.text password:self.passWordTextField.text completionHandler:^(BOOL isSucceeded, NSError *error) {
            if (!weakSelf){
                return;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (isSucceeded){
                PDRoleBindingViewController *roleBindingViewController = [[PDRoleBindingViewController alloc]init];
                roleBindingViewController.bindingType = bindingTypeCreate;
                roleBindingViewController.isFirstBinding = YES;
                [strongSelf.getCodeButton.timer invalidate];
                strongSelf.getCodeButton.timer = nil;
                [strongSelf.navigationController pushViewController:roleBindingViewController animated:YES];
            } else {
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }
        }];
    }
#pragma mark - 手机号验证
- (void)phoneHasRegisterCompletion:(void(^)(BOOL isHasRegister))completion
{
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    fetchModel.requestParams = @{@"phone":self.userTextField.text};
    __weak typeof(self) weakSelf = self;
    [fetchModel fetchWithPath:KPhone_Register completionHandler:^(BOOL isSucceeded, NSError *error) {
        if(!weakSelf) return;
        if (isSucceeded) {
            if (completion) {
                completion(NO);
            }
            [PDHUD showShimmeringString:@"手机号已经被注册" maskType:WSProgressHUDMaskTypeGradient delay:2];
        }else{
            if (error.code == 121) {
                if (completion) {
                    completion(YES);
                }
            }else{
                [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
                return;
            }
        }
    }];
}
#pragma mark - error
- (BOOL)checkRegister
{
    if (![PDTool validateMobile:self.userTextField.text]){
        [PDHUD showShimmeringString:@"用户名为无效手机号" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else if (![self.passWordTextField.text isEqualToString:self.passWordAgainTextField.text]){
        [PDHUD showShimmeringString:@"两次密码不相同" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else if (!([PDTool validatePassword:self.passWordTextField.text]&&[PDTool validatePassword:self.passWordAgainTextField.text])){
        [PDHUD showShimmeringString:@"密码必须为数字和字母" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else if ([PDTool isEmpty:self.phoneCodeTextField.text]){
        [PDHUD showShimmeringString:@"验证码不能为空" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else if(![PDTool isPureNumandCharacters:self.phoneCodeTextField.text]){
        [PDHUD showShimmeringString:@"验证码错误请重新输入" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    } else {
        return YES;
    }
}
#pragma mark - srollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([self.userTextField isFirstResponder] || [self.passWordTextField isFirstResponder]||[self.passWordAgainTextField isFirstResponder] || [self.phoneCodeTextField isFirstResponder]) {
        [self.view endEditing:YES];
    }
}

@end
