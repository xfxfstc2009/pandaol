//
//  PDResetPasswordViewController.h
//  PandaChallenge
//
//  Created by panda on 16/3/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDSMSModel.h"
@interface PDResetPasswordViewController : AbstractViewController
@property (nonatomic,copy)NSString *phone;
@end
