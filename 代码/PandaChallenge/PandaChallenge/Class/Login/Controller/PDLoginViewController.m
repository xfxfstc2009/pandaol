//
//  PDLoginViewController.m
//  PandaChallenge
//
//  Created by panda on 16/3/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginViewController.h"
#import "PDThirdSharedTool.h"
#import "PDRegistViewController.h"
#import "PDPasswordViewController.h"
#import "PDSureButton.h"
#import "PDChallengeViewController.h"

#define PDLeftRight_Space (105/2.)
@interface PDLoginViewController ()<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
@property (nonatomic,strong)UITextField *userTextField;
@property (nonatomic,strong)UITextField *passWordTextField;
@property (nonatomic,strong)NSArray *loginArray;
@property (nonatomic,strong)UITableView *loginTableView;
@property (nonatomic,strong)PDImageView *logImageView;
@property (nonatomic,strong)PDSureButton *loginButton;
@property (nonatomic,assign)BOOL isLogoUp;
@end

@implementation PDLoginViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayInit];
    [self creatLoginTableView];
    [self createKeyBoardNotification];
}

#pragma mark - PageSetting
- (void)pageSetting {
    self.barMainTitle = @"登录";
    [RESideMenu shareInstance].panGestureEnabled = NO;
    [self hidesBackButton];
}

#pragma mark - Array With Init
- (void)arrayInit {
    self.loginArray = @[@[@"logo"],@[@"用户名",@"密码"],@[@"登录按钮"]];
}
#pragma mark - UITableView
- (void)creatLoginTableView {
    if (!self.loginTableView) {
        self.loginTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height) style:UITableViewStylePlain];
        self.loginTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.loginTableView.delegate = self;
        self.loginTableView.dataSource = self;
        self.loginTableView.backgroundColor = [UIColor clearColor];
        self.loginTableView.showsVerticalScrollIndicator = NO;
        self.loginTableView.scrollEnabled = YES;
        self.loginTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.loginTableView];
    }
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.loginArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSArray *sectionOfArray = [self.loginArray objectAtIndex:section];
    return sectionOfArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0) {
       static NSString *cellForIdentifierWithRowOne = @"cellForIdentifierWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierWithRowOne];
        if (!cellWithRowOne) {
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
            
            // 1.logo
            PDImageView *logoImageView = [[PDImageView alloc]init];
            self.logImageView = logoImageView;
            logoImageView.image = [UIImage imageNamed:@"login_logoWithText"];
            logoImageView.stringTag = @"logo";
            logoImageView.frame = CGRectMake((kScreenWidth - LCFloat(169.5))/2, 0, LCFloat(169.5), cellHeight);
            [cellWithRowOne addSubview:logoImageView];
        }
        return cellWithRowOne;
    }else if (indexPath.section == 1){
        static NSString *cellForIdentifierWithRowTwo = @"cellForIdentifierWithRowTwo";
        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierWithRowTwo];
        if (!cellWithRowTwo) {
            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowTwo.backgroundColor = [UIColor clearColor];
            
            //1.iconButton
            UIButton *iconButton = [UIButton buttonWithType:UIButtonTypeCustom];
            iconButton.stringTag = @"iconButton";
            [cellWithRowTwo addSubview:iconButton];
            
            //2.textField
            UITextField *textField = [[UITextField alloc]init];
            textField.stringTag = @"textField";
            textField.delegate = self;
            textField.font = [UIFont systemFontOfCustomeSize:13];
            textField.borderStyle = UITextBorderStyleNone;
            textField.backgroundColor = [UIColor clearColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            [textField addTarget:self action:@selector(textFieldChangeManager:) forControlEvents:UIControlEventEditingChanged];
            [cellWithRowTwo addSubview:textField];
            
            //3.bottomLine
            UIView *line = [[UIView alloc]init];
            line.stringTag = @"line";
            line.backgroundColor = [UIColor lightGrayColor];
            [cellWithRowTwo addSubview:line];
        }
        UIButton *iconButton = (UIButton *)[cellWithRowTwo viewWithStringTag:@"iconButton"];
        UITextField *textField = (UITextField *)[cellWithRowTwo viewWithStringTag:@"textField"];
        UIView *line = (UIView *)[cellWithRowTwo viewWithStringTag:@"line"];
        //frame
        iconButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), LCFloat(15), cellHeight - LCFloat(22), cellHeight -LCFloat(22));
        textField.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(20), LCFloat(12), kScreenBounds.size.width - (LCFloat(PDLeftRight_Space) + LCFloat(20) +CGRectGetMaxX(iconButton.frame)),cellHeight - LCFloat(12));
        line.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(10), CGRectGetMaxY(textField.frame), CGRectGetWidth(textField.frame) + LCFloat(10), 1);
        //赋值
        if (indexPath.row == 0) {
            [iconButton setBackgroundImage:[UIImage imageNamed:@"login_user"] forState:UIControlStateNormal];
            textField.placeholder = @"手机号/账号";
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.returnKeyType = UIReturnKeyNext;
            self.userTextField = textField;
            if ([PDTool userDefaultGetWithKey:CURRENT_USER_NAME_KEY].length){
                self.userTextField.text = [PDTool userDefaultGetWithKey:CURRENT_USER_NAME_KEY];
            }
        } else {
            [iconButton setBackgroundImage:[UIImage imageNamed:@"login_lock"] forState:UIControlStateNormal];
            textField.placeholder = @"请输入密码";
            textField.returnKeyType = UIReturnKeyDone;
            textField.secureTextEntry = YES;
            self.passWordTextField = textField;
        }
        return cellWithRowTwo;
    } else {
        static NSString *cellForIdentifierWithRowThree = @"cellForIdentifierWithRowThree";
        UITableViewCell *cellWithRowThree = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierWithRowThree];
        if (!cellWithRowThree) {
            cellWithRowThree = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierWithRowThree];
            cellWithRowThree.backgroundColor = [UIColor clearColor];
            cellWithRowThree.selectionStyle = UITableViewCellSelectionStyleNone;
            // 1.登录
            __weak typeof(self) weakSelf = self;
            self.loginButton = [[PDSureButton alloc]initWithTitle:@"登录" WithActionBlock:^{
                if (!weakSelf) {
                    return;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf.view endEditing:YES];
                [strongSelf loginManager];
            }];
            self.loginButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), 0, kScreenBounds.size.width - 2 * LCFloat(PDLeftRight_Space), LCFloat(44));
            self.loginButton.isEnable = NO;
            [cellWithRowThree addSubview:self.loginButton];
            // 2.忘记密码
            NSString *title = @"忘记密码";
            CGSize size = [title sizeWithCalcFont:[UIFont systemFontOfCustomeSize:12] constrainedToSize:CGSizeMake(MAXFLOAT, [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:12]])];
            UIButton *passwordButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [passwordButton setBackgroundColor:[UIColor clearColor]];
            passwordButton.titleLabel.textAlignment = NSTextAlignmentLeft;
            passwordButton.titleLabel.font = [UIFont systemFontOfCustomeSize:12];
            [passwordButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            [passwordButton setTitle:title forState:UIControlStateNormal];
            passwordButton.frame = CGRectMake(LCFloat(PDLeftRight_Space),  LCFloat(44) + LCFloat(22), size.width + LCFloat(11), size.height + LCFloat(11));
            [passwordButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf) return;
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf.view endEditing:YES];
                PDPasswordViewController  *passwordViewCintroller = [[PDPasswordViewController alloc]init];
                [strongSelf.navigationController pushViewController:passwordViewCintroller animated:YES];
            }];
            [cellWithRowThree addSubview:passwordButton];
            // 3.立即注册
            UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
            registerButton.titleLabel.textAlignment = NSTextAlignmentRight;
//            [registerButton setBackgroundColor:[UIColor redColor]];
            registerButton.titleLabel.font = [UIFont systemFontOfCustomeSize:12];
            [registerButton setTitle:@"立即注册" forState:UIControlStateNormal];
            [registerButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
            registerButton.frame = CGRectMake(kScreenWidth - LCFloat(PDLeftRight_Space) - size.width - LCFloat(11),  LCFloat(44) + LCFloat(22), size.width + LCFloat(11), size.height + LCFloat(11));
            [registerButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf) return;
                __strong typeof(weakSelf) strongSelf = weakSelf;
                 [strongSelf.view endEditing:YES];
                PDRegistViewController  *registerViewCintroller = [[PDRegistViewController alloc]init];
                [strongSelf.navigationController pushViewController:registerViewCintroller animated:YES];
            }];
            [cellWithRowThree addSubview:registerButton];
        }
        return cellWithRowThree;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0) {
        return LCFloat(129);
    }else if (indexPath.section == 1) {
        return LCFloat(44);
    }else {
        return LCFloat(44) + LCFloat(33) + [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:12]];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager:)];
    [headerView addGestureRecognizer:tap];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        return LCFloat(33);
    }else if (section == 1) {
        return LCFloat(55);
    }else {
        return LCFloat(45);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager:)];
    [footerView addGestureRecognizer:tap];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 2) {
        return [self calculateFooterViewHeight];
    }else{
        return 0;
    }
    
}
- (CGFloat)calculateFooterViewHeight
{
    return self.view.size_height - LCFloat(33) - LCFloat(55) - LCFloat(45) - LCFloat(129) - 2 * LCFloat(44) - (LCFloat(44) + LCFloat(33) + [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:12]]);
}

#pragma mark - UIGestureRecognizer
- (void)tapManager:(UITapGestureRecognizer *)tap {
    [self.view endEditing:YES];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.userTextField) {
        if (toBeString.length > 11) {
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
    }else if (textField == self.passWordTextField){
        if (toBeString.length > 16) {
            return NO;
        }
    }
    return YES;
}

- (void)textFieldChangeManager:(UITextField *)textField{
    if (self.userTextField.text.length == 11 && self.passWordTextField.text.length >= 6) {
        self.loginButton.isEnable = YES;
    }else{
        self.loginButton.isEnable = NO;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.userTextField && textField.returnKeyType == UIReturnKeyNext) {
        [self.passWordTextField becomeFirstResponder];
    }else if (textField == self.passWordTextField && textField.returnKeyType == UIReturnKeyDone){
        [self.passWordTextField resignFirstResponder];
    }
    return YES;
}


#pragma mark - loginManager
-(void)loginManager{
    if (![self checkLogin]) {
        return;
    }
    self.loginButton.isEnable = NO;
    [PDHUD showWithStatus:@"登录中..." delay:0];
    __weak typeof(self) weakSelf = self;
    [[PDLoginModel shareInstance]loginWithType:PDLoginModelCurrent user:self.userTextField.text password:self.passWordTextField.text completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.loginButton.isEnable = YES;
        if (isSucceeded){
            [PDHUD showShimmeringString:@"登录成功" maskType:WSProgressHUDMaskTypeDefault delay:2];
            [RESideMenu shareInstance].panGestureEnabled = YES;
            [strongSelf.navigationController pushViewController:[[PDChallengeViewController alloc]init] animated:YES];
        } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];
}
#pragma mark - error
- (BOOL)checkLogin {
    if (![PDTool validateMobile:self.userTextField.text]){
        [PDHUD showShimmeringString:@"用户名为无效手机号" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else if(![PDTool validatePassword:self.passWordTextField.text]){
        [PDHUD showShimmeringString:@"密码必须为数字和字母" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else{
        return YES;
    }
}

#pragma mark - 键盘通知
- (void)createKeyBoardNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    NSTimeInterval duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    int option = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
    if (self.isLogoUp) return;
    __block CGPoint center = self.loginTableView.center;
    [UIView animateKeyframesWithDuration:duration delay:0 options:option animations:^{
        center.y -= LCFloat(175);
        self.loginTableView.center = center;
        self.isLogoUp = YES;
    } completion:nil];
}

- (void)keyboardWillHide:(NSNotification *)notification {
    NSTimeInterval duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    int option = [notification.userInfo[UIKeyboardAnimationCurveUserInfoKey] intValue];
    if (!self.isLogoUp) return;
    __block CGPoint center = self.loginTableView.center;
    [UIView animateKeyframesWithDuration:duration delay:0 options:option animations:^{
        center.y += LCFloat(175);
        self.loginTableView.center = center;
        self.isLogoUp = NO;
    } completion:nil];

}
- (void)dealloc {
    NSLog(@"...");
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - srollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if ([self.userTextField isFirstResponder] || [self.passWordTextField isFirstResponder]) {
        [self.view endEditing:YES];
    }
}

@end
