//
//  PDProtocolViewController.m
//  PandaChallenge
//
//  Created by panda on 16/4/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProtocolViewController.h"
#import "PDSureButton.h"
@implementation PDProtocolViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createView];
}
#pragma mark - PageSetting
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setHidden:NO];
}

#pragma mark - CreateView
- (void)createView
{
    self.webView.frame = CGRectMake(0, LCFloat(30), self.view.frame.size.width, self.view.frame.size.height-LCFloat(99)-LCFloat(30));
    self.urlStr = @"http://m.pandaol.net/panda/cafe/peo.html";

    
   
    
    __weak typeof(self) weakSelf = self;
    PDSureButton *sureButton = [[PDSureButton alloc]initWithTitle:@"确定" WithActionBlock:^{
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    sureButton.frame = CGRectMake(LCFloat(105/2.), self.view.frame.size.height - LCFloat(88), self.view.frame.size.width - 2 * LCFloat(105/2), LCFloat(44));
    [self.view addSubview:sureButton];
}
@end
