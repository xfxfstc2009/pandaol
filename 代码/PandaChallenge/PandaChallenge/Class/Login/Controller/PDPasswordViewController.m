//
//  PDPasswordViewController.m
//  PandaChallenge
//
//  Created by panda on 16/3/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPasswordViewController.h"
#import "PDResetPasswordViewController.h"
#import "PDTimerButton.h"
#import "PDSureButton.h"
#import "PDSMSModel.h"
#define PDLeftRight_Space (105/2.)
@interface PDPasswordViewController ()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
@property (nonatomic,strong)UITextField *userTextField;
@property (nonatomic,strong)UITextField *phoneCodeTextField;
@property (nonatomic,strong)PDTimerButton *getCodeButton;
@property (nonatomic,strong)PDSureButton *passwordButton;
@property (nonatomic,strong)UITableView *passwordTableView;
@property (nonatomic,strong)NSArray *passwordArray;
@property (nonatomic,strong)PDSMSModel *smsModel;
@end

@implementation PDPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createPasswordTableView];
    [self createkeyboardNotification];
}
#pragma mark - PageSetting
- (void)pageSetting
{
    self.barMainTitle = @"找回密码";
    __weak typeof(self) weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"login_back"] barHltImage:nil action:^{
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
}
#pragma mark - Array With Init
- (void)arrayWithInit
{
    self.passwordArray = @[@[@"手机号",@"验证码和短信按钮"],@[@"找回密码按钮"]];
}
#pragma mark - UITableView
- (void)createPasswordTableView
{
    if (!self.passwordTableView) {
        self.passwordTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height) style:UITableViewStylePlain];
        self.passwordTableView.delegate = self;
        self.passwordTableView.dataSource = self;
        self.passwordTableView.backgroundView = nil;
        self.passwordTableView.backgroundColor = [UIColor clearColor];
        self.passwordTableView.showsVerticalScrollIndicator = NO;
        self.passwordTableView.scrollEnabled = YES;
        self.passwordTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.passwordTableView];
    }
}
#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.passwordArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sectionArray = [self.passwordArray objectAtIndex:section];
    return sectionArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0) {       //输入框
        static NSString *cellForIdentifierRowOne = @"cellForIdentifierRowOne";
        UITableViewCell *cellForRowOne = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierRowOne];
        if (!cellForRowOne) {
            cellForRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierRowOne];
            cellForRowOne.backgroundColor = [UIColor clearColor];
            cellForRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            //1.iconButton
            UIButton *iconButton = [UIButton buttonWithType:UIButtonTypeCustom];
            iconButton.stringTag = @"iconButton";
            [cellForRowOne addSubview:iconButton];
            //2.textField
            UITextField *textField = [[UITextField alloc]init];
            textField.stringTag = @"textField";
            textField.delegate = self;
            textField.font = [UIFont systemFontOfCustomeSize:13];
            textField.keyboardType = UIKeyboardTypeNumberPad;
            textField.borderStyle = UITextBorderStyleNone;
            textField.backgroundColor = [UIColor clearColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            [textField addTarget:self action:@selector(textFieldChangeManager:) forControlEvents:UIControlEventEditingChanged];
            [cellForRowOne addSubview:textField];
            //3.bottomLine
            UIView *line = [[UIView alloc]init];
            line.stringTag = @"line";
            line.backgroundColor = [UIColor lightGrayColor];
            [cellForRowOne addSubview:line];
            //4.timerButton
            __weak typeof(self) weakSelf = self;
            PDTimerButton *timerButton = [[PDTimerButton alloc]initWithTimerType:passwordTimer actionBlock:^{
                if (!weakSelf) {
                    return;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf registerSmsCode];
            }];
            timerButton.isEnable = NO;
            timerButton.stringTag = @"timerButton";
            [cellForRowOne addSubview:timerButton];
        }
        UIButton *iconButton = (UIButton *)[cellForRowOne viewWithStringTag:@"iconButton"];
        UITextField *textField = (UITextField *)[cellForRowOne viewWithStringTag:@"textField"];
        UIView *line = (UIView *)[cellForRowOne viewWithStringTag:@"line"];
        PDTimerButton *timerButton = (PDTimerButton *)[cellForRowOne viewWithStringTag:@"timerButton"];
        //frame
        iconButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), LCFloat(15), cellHeight - LCFloat(24), cellHeight - LCFloat(24));
        if (indexPath.row == 0) {
            textField.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(20), LCFloat(12), kScreenBounds.size.width - (LCFloat(PDLeftRight_Space) + LCFloat(20) +CGRectGetMaxX(iconButton.frame)),cellHeight - LCFloat(12));
            line.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(10), CGRectGetMaxY(textField.frame), CGRectGetWidth(textField.frame) + LCFloat(10), 1);
            timerButton.hidden = YES;
        }else{
            timerButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(PDLeftRight_Space) - LCFloat(80) - LCFloat(5), LCFloat(14), LCFloat(80), cellHeight - LCFloat(14));
            textField.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(20), LCFloat(12), CGRectGetMinX(timerButton.frame) - CGRectGetMaxX(iconButton.frame) - LCFloat(20) - LCFloat(5),cellHeight - LCFloat(12));
            line.frame = CGRectMake(CGRectGetMaxX(iconButton.frame) + LCFloat(10), CGRectGetMaxY(textField.frame), CGRectGetWidth(textField.frame)+ LCFloat(10), 1);
            timerButton.hidden = NO;
        }
        //赋值
        if (indexPath.row == 0) {
            [iconButton setBackgroundImage:[UIImage imageNamed:@"login_phone"] forState:UIControlStateNormal];
            textField.placeholder = @"手机账号";
            textField.returnKeyType = UIReturnKeyNext;
            self.userTextField = textField;
        }else{
            [iconButton setBackgroundImage:[UIImage imageNamed:@"login_code"] forState:UIControlStateNormal];
            textField.placeholder = @"验证码";
            textField.returnKeyType = UIReturnKeyDone;
            self.phoneCodeTextField = textField;
            self.getCodeButton = timerButton;
        }
    return cellForRowOne;
    }else{      //找回密码按钮
        static NSString *cellForIdentifierRowTwo = @"cellForIdentifierRowTwo";
        UITableViewCell *cellForRowTwo = [tableView dequeueReusableCellWithIdentifier:cellForIdentifierRowTwo];
        if (!cellForRowTwo) {
            cellForRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellForIdentifierRowTwo];
            cellForRowTwo.backgroundColor = [UIColor clearColor];
            cellForRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            //1.找回密码按钮
            __weak typeof(self) weakSelf = self;
            self.passwordButton = [[PDSureButton alloc]initWithTitle:@"找回密码" WithActionBlock:^{
                if (!weakSelf) {
                    return;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                [strongSelf passwordManager];
            }];
            self.passwordButton.frame = CGRectMake(LCFloat(PDLeftRight_Space), 0, kScreenBounds.size.width - 2 * LCFloat(PDLeftRight_Space), cellHeight);
            self.passwordButton.isEnable = NO;
            [cellForRowTwo addSubview:self.passwordButton];
        }
        return cellForRowTwo;
    }
}

#pragma mark - UITableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return LCFloat(44);
    }else{
        return LCFloat(44);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc]init];
    footerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager:)];
    [footerView addGestureRecognizer:tap];
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1) {
        return LCFloat(150);
    }else{
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager:)];
    [headerView addGestureRecognizer:tap];
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return LCFloat(22);
    }else{
        return LCFloat(37);
    }
}
#pragma mark - 接口
#pragma mark passwordManager
- (void)passwordManager
{
    [self.view endEditing:YES];
    if (![self checkTextFieldInput]) {
        return;
    }
    self.passwordButton.isEnable = NO;
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    fetchModel.requestParams = @{@"vcode":self.phoneCodeTextField.text};
    __weak typeof(self) weakSelf = self;
    [fetchModel fetchWithPath:KVCode completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        strongSelf.passwordButton.isEnable = YES;
        if (isSucceeded) {
            PDResetPasswordViewController *resetViewController = [[PDResetPasswordViewController alloc]init];
            resetViewController.phone = strongSelf.userTextField.text;
            [self.navigationController pushViewController:resetViewController animated:YES];
        }else{
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeDefault delay:2];
        }
    }];
   
}
#pragma mark 手机号验证
- (void)phoneHasRegisterCompletion:(void(^)(BOOL isHasRegister))completion
{
    
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    fetchModel.requestParams = @{@"phone":self.userTextField.text};
    __weak typeof(self) weakSelf = self;
    [fetchModel fetchWithPath:KPhone_Register completionHandler:^(BOOL isSucceeded, NSError *error) {
        if(!weakSelf) return;
        if (isSucceeded) {
            if (completion) {
                completion(YES);
            }
        }else{
            if (error.code == 121) {
                 [PDHUD showShimmeringString:@"未注册的手机号" maskType:WSProgressHUDMaskTypeGradient delay:2];
                if (completion) {
                    completion(NO);
                }
            }else{
                [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
                return;
            }
        }
    }];
    
}

#pragma mark 接口短信验证码
- (void)registerSmsCode{
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    fetchModel.requestParams = @{@"phone":self.userTextField.text};
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:kSMS_ReSetPassword_VerifyPath completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return;
        }
        if (isSucceeded){
            [PDHUD showShimmeringString:@"短信已发送" maskType:WSProgressHUDMaskTypeDefault delay:2];
        } else {
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
        }
    }];
}

#pragma mark - textFieldDelegate
- (void)textFieldChangeManager:(UITextField *)textField
{
    if (textField == self.userTextField) {
        if ([PDTool validateMobile:self.userTextField.text] && self.getCodeButton.isTimeOut) {
            __weak typeof(self) weakSelf = self;
            [self phoneHasRegisterCompletion:^(BOOL isHasRegister) {
                if (!weakSelf) return;
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (isHasRegister) {
                    strongSelf.getCodeButton.isEnable = YES;
                    strongSelf.getCodeButton.isCanClickByPhone = YES;
                }
            }];
        }else if (self.getCodeButton.isTimeOut){
            self.getCodeButton.isEnable = NO;
            self.getCodeButton.isCanClickByPhone = NO;
        }else if (!self.getCodeButton.isTimeOut && [PDTool validateMobile:self.userTextField.text]){
            __weak typeof(self) weakSelf = self;
            [self phoneHasRegisterCompletion:^(BOOL isHasRegister) {
                if (!weakSelf) return;
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (isHasRegister) {
                    strongSelf.getCodeButton.isCanClickByPhone = YES;
                }
            }];
        }else if (!self.getCodeButton.isTimeOut){
            self.getCodeButton.isCanClickByPhone = NO;
        }
    }
    if (self.userTextField.text.length == 11 && self.phoneCodeTextField.text.length == 5) {
        self.passwordButton.isEnable = YES;
    }else{
        self.passwordButton.isEnable = NO;
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
    if (textField == self.userTextField) {
        if (toBeString.length >11) {
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
    }else if (textField == self.phoneCodeTextField){
        if (toBeString.length >5) {
            textField.text = [toBeString substringToIndex:5];
            return NO;
        }
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.userTextField && textField.returnKeyType == UIReturnKeyNext) {
        [self.userTextField resignFirstResponder];
    }else{
        [self.phoneCodeTextField resignFirstResponder];
    }
    return YES;
}

#pragma mark - UIGestureRecognizer
- (void)tapManager:(UITapGestureRecognizer *)tap
{
    [self.view endEditing:YES];
}

#pragma mark - 键盘通知
- (void)createkeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *value = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    CGRect keyboardRect = [value CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    CGFloat keyboard_Y = keyboardRect.origin.y;
    CGRect newTableViewFrame = self.view.bounds;
    newTableViewFrame.size.height = keyboard_Y - self.view.bounds.origin.y;
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    self.passwordTableView.frame = newTableViewFrame;
    [UIView commitAnimations];

}
- (void)keyboardWillHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    NSValue *animationDurationVale = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationVale getValue:&animationDuration];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    self.passwordTableView.frame = self.view.bounds;
    [UIView commitAnimations];
}
- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
#pragma mark - error
- (BOOL)checkTextFieldInput
{
    if (![PDTool validateMobile:self.userTextField.text]){
        [PDHUD showShimmeringString:@"无效的手机号" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else if (![PDTool isPureNumandCharacters:self.phoneCodeTextField.text]){
        [PDHUD showShimmeringString:@"验证码错误请重新输入" maskType:WSProgressHUDMaskTypeGradient delay:2];
        return NO;
    }else{
        return YES;
    }
}
#pragma mark - srollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    if ([self.userTextField isFirstResponder] || [self.phoneCodeTextField isFirstResponder]) {
        [self.view endEditing:YES];
    }
}
@end
