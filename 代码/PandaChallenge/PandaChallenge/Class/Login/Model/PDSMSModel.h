//
//  PDSMSModel.h
//  PandaChallenge
//
//  Created by panda on 16/4/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFetchModel.h"
typedef  NS_ENUM(NSInteger,SMSType){
    smsInRegister = 0,              /**< 注册验证码*/
    smsInResetPassword,             /**< 忘记密码验证码*/
};

@interface PDSMSModel : PDFetchModel
@property (nonatomic,copy)NSString *phone;      /**< 手机号*/
@property (nonatomic,copy)NSString *vCode;      /**< 手机验证码*/

- (instancetype)initWithSMSType:(SMSType)type;
@end
