//
//  PDLoginModel.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginModel.h"
#import "PDApnsTool.h"

@implementation PDLoginModel

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"userId": @"userid",@"fullName":@"fullname",@"isSpreader":@"kind",@"nickName":@"name"};
}


+ (instancetype)shareInstance{
    static PDLoginModel *loginModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        loginModel = [[PDLoginModel alloc] init];
    });
    return loginModel;
}

- (BOOL)hasLoggedIn{
    return (self.isSessionValid && ![PDTool isEmpty:[PDLoginModel shareInstance].phone]);
}

- (BOOL)hasEverLoggedIn
{
    return (![PDTool isEmpty:[USERDEFAULTS objectForKey:CURRENT_USER_PASS_KEY]] && ![PDTool isEmpty:[USERDEFAULTS objectForKey:CURRENT_USER_NAME_KEY]]);
}
#pragma mark 登录
-(void)loginWithType:(PDLoginType)loginType user:(NSString *)user password:(NSString *)password completionHandler:(void(^)(BOOL isSucceeded, NSError *error))handler{
    __weak typeof(self)weakSelf = self;
    NSString *deviceToken = [PDApnsTool shareInstance].deviceToken.length?[PDApnsTool shareInstance].deviceToken:[PDTool userDefaultGetWithKey:DeviceId];
    NSLog(@"deviceToken=%@",deviceToken);
    if (!deviceToken) return;
    self.requestParams = @{@"phone":user,@"password":password,@"login_type":@"ios",@"login_device":deviceToken};
    [self fetchWithPath:kLoginPath completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf.bizResult = YES;
            [strongSelf saveCurrentUserName:user];                                                  // 2. 保存登录名
            [strongSelf saveCurrentUserPass:password];                                              // 3. 保存密码
            [strongSelf saveCurrentCurrentLoginType:loginType];                                     // 4. 保存当前登录方式
            [strongSelf setupFromLocalData:strongSelf andPhone:user];                               // 5. 保存登录信息
            handler(YES,nil);
        } else if ([error.domain isEqualToString:PDBizErrorDomain]){
            handler(NO,error);
        } else {
            handler(NO,error);
        }
    }];
}


#pragma mark - 保存信息
- (void)setupFromLocalData:(PDLoginModel *)model andPhone:(NSString *)phone{
    [PDLoginModel shareInstance].userId = model.userId;
    [PDLoginModel shareInstance].phone = phone;
    NSString *newAvatar = [model.avatar stringByReplacingOccurrencesOfString:@"\\" withString:@""];
    [PDLoginModel shareInstance].avatar = newAvatar;
    [PDLoginModel shareInstance].fight_times = model.fight_times;
    [PDLoginModel shareInstance].name = model.name;
    [PDLoginModel shareInstance].wins = model.wins;
    [PDLoginModel shareInstance].avatar = model.avatar;
    [PDLoginModel shareInstance].nickName = model.nickName;
    [PDLoginModel shareInstance].status = model.status;
    [PDLoginModel shareInstance].invitationCode = model.invitationCode;
}

- (void)saveCurrentUserName:(NSString *)userName{           // 保存账号
    [PDTool userDefaulteWithKey:CURRENT_USER_NAME_KEY Obj:userName];
}

- (void)saveCurrentUserPass:(NSString *)userPass{           // 保存密码
    [PDTool userDefaulteWithKey:CURRENT_USER_PASS_KEY Obj:userPass];
}

-(void)saveCurrentCurrentLoginType:(PDLoginType)loginType{    // 保存登录类型
    [PDTool userDefaulteWithKey:CURRENT_USER_LOGIN_TYPE Obj:[NSString stringWithFormat:@"%li",(long)loginType]];
}

#pragma mark - 登出
-(void)logoutWithBlock:(void (^)(BOOL isisSucceeded, NSError *error))block{
    __weak typeof(self) weakSelf = self;
    [self fetchWithPath:KLogin_Out completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf) return;
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (isSucceeded) {
            [strongSelf clearCookiesForBaseURL];        // 清空cookie
            [strongSelf clearUserInfoInUserDefault];    // 清空用户信息
            [strongSelf clearLocalData];                // 清空loginModel属性值
            [PDHUD showShimmeringString:@"用户已退出" maskType:WSProgressHUDMaskTypeGradient delay:2];
            if (block){
                block(YES,nil);
            }
        }else{
            [PDHUD showShimmeringString:error.localizedDescription maskType:WSProgressHUDMaskTypeGradient delay:2];
            if (block){
                block(NO,error);
            }
        }
    }];
}
#pragma mark - 清空信息
- (void)clearUserInfoInUserDefault
{
    [PDTool userDefaultDelegtaeWithKey:CURRENT_USER_PASS_KEY];    // 密码
    
    [PDTool userDefaultDelegtaeWithKey:CURRENT_USER_LOGIN_TYPE];  // 登录类型
    
}

- (void)clearLocalData
{
    [PDLoginModel shareInstance].userId = nil;
    [PDLoginModel shareInstance].phone = nil;
    [PDLoginModel shareInstance].avatar = nil;
    [PDLoginModel shareInstance].fight_times = 0;
    [PDLoginModel shareInstance].name = nil;
    [PDLoginModel shareInstance].wins = 0;
    [PDLoginModel shareInstance].avatar = nil;
    [PDLoginModel shareInstance].nickName = nil;
    [PDLoginModel shareInstance].isSpreader = nil;
    [PDLoginModel shareInstance].cn_idcard = nil;
}
@end
