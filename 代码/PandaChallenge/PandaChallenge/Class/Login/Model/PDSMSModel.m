//
//  PDSMSModel.m
//  PandaChallenge
//
//  Created by panda on 16/4/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSMSModel.h"
@interface PDSMSModel ()
@property (nonatomic,assign)SMSType type;
@end
@implementation PDSMSModel

- (instancetype)initWithSMSType:(SMSType)type
{
    if (self = [super init]) {
        [self saveType:type];
    }
    return self;
}

- (void)saveType:(SMSType)type
{
    [USERDEFAULTS setInteger:type forKey:SMS_TYPE_KEY];
    [USERDEFAULTS synchronize];
}
- (void)setPhone:(NSString *)phone
{
    _phone = phone;
    self.type = (SMSType)[USERDEFAULTS integerForKey:SMS_TYPE_KEY];
    if (self.type == smsInRegister) {
        [USERDEFAULTS setObject:phone forKey:SMS_PHONE_REGISTER_KEY];
        [USERDEFAULTS synchronize];
    }else if (self.type == smsInResetPassword){
        [USERDEFAULTS setObject:phone forKey:SMS_PHONE_RESET_KEY];
        [USERDEFAULTS synchronize];
    }
}
- (void)setVCode:(NSString *)vCode
{
    _vCode = vCode;
    self.type = (SMSType)[USERDEFAULTS integerForKey:SMS_TYPE_KEY];
    if (self.type == smsInRegister) {
        [USERDEFAULTS setObject:vCode forKey:SMS_CODE_REGISTER_KEY];
        [USERDEFAULTS synchronize];
    }else if (self.type == smsInResetPassword){
        [USERDEFAULTS setObject:vCode forKey:SMS_CODE_RESET_KEY];
        [USERDEFAULTS synchronize];
    }
}
@end
