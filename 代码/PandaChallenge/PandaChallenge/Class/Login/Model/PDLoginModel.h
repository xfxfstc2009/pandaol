//
//  PDLoginModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 登录
#import "PDFetchModel.h"
#import "PDChallengerStatusEnum.h"
typedef NS_ENUM(NSInteger,PDLoginType) {    // 登录方式
    PDLoginModelWeChat,         /**< 微信登录*/
    PDLoginModelQQ,             /**< QQ 登录*/
    PDLoginModelCurrent,        /**< 当前登录*/
};


@interface PDLoginModel : PDFetchModel

@property (nonatomic,copy)NSString *userId;                     /**< 用户identify*/
@property (nonatomic,copy)NSString *phone;                      /**< 手机号码*/
@property (nonatomic,assign)challengerStatus challengeStatus;   /**< 当前的游戏状态*/
@property (nonatomic,copy)NSString *avatar;                     /**< 用户头像*/
@property (nonatomic,copy)NSString *name;                       /**< 用户名字*/
@property (nonatomic,assign)NSInteger fight_times;              /**< 对战场次*/
@property (nonatomic,assign)NSInteger wins;                     /**< 胜利场次*/

@property (nonatomic,copy)NSString *cn_idcard;                  /**< 身份证号*/
@property (nonatomic,copy)NSString *fullName;                   /**< 真实姓名*/
@property (nonatomic,copy)NSString *nickName;                   /**< 昵称*/
@property (nonatomic,assign)BOOL isSpreader;                    /**< 是否是推广者*/
@property (nonatomic,copy)NSString *status_msg;                 /**< 状态详情*/
@property (nonatomic,assign)BOOL status;                        /**< 用来判断是否要显示状态*/
@property (nonatomic,copy)NSString *invitationCode;             /**< 邀请码*/

+ (instancetype)shareInstance;

// 登录
-(void)loginWithType:(PDLoginType)loginType user:(NSString *)user password:(NSString *)password completionHandler:(void(^)(BOOL isSucceeded, NSError *error))handler;

- (BOOL)hasLoggedIn;                                            /**< 判断当前是否登录*/
- (BOOL)hasEverLoggedIn;                                        /**< 判断以前是否登录过*/
- (void)logoutWithBlock:(void(^)(BOOL isSucceeded, NSError *error))block;                                              /**< 退出登录*/

@end
