//
//  PDLunchViewController.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLunchViewController.h"

@interface PDLunchViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) NSArray *images;

@property (nonatomic, strong) UIPageControl *pageControl;
@end

@implementation PDLunchViewController
- (NSArray *)images {
    return @[@"login_logo",@"login_logo",@"login_logo"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self lunchScrollView];
    [self setPageView];
}

- (void)lunchScrollView {
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:kScreenBounds];
    scrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.images.count, kScreenBounds.size.height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.bounces = NO;
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    for (int i = 0; i < self.images.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width * i, 0, kScreenBounds.size.width, kScreenBounds.size.height)];
        imageView.image = [UIImage imageNamed:[self.images objectAtIndex:i]];
        [scrollView addSubview:imageView];
        if (i == (self.images.count - 1)) {
            imageView.userInteractionEnabled = YES;
            UIButton *button = [UIButton buttonWithType:1];
            button.backgroundColor = [UIColor redColor];
            button.frame = CGRectMake(0, 0, LCFloat(100), LCFloat(30));
            button.center = self.view.center;
            [imageView addSubview:button];
            [button buttonWithBlock:^(UIButton *button) {
                NSLog(@"跳转");
            }];
        }
    }
}

- (void)setPageView {
    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width * .4f, 30)];
    self.pageControl = pageControl;
    pageControl.center = CGPointMake(self.view.center.x, kScreenBounds.size.height - 50);
    pageControl.numberOfPages = self.images.count;
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.userInteractionEnabled = NO;
    [self.view addSubview:pageControl];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSInteger index = round(scrollView.contentOffset.x / kScreenBounds.size.width);
    self.pageControl.currentPage = index;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

@end
