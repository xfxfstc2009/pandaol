//
//  PDTestViewController.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTestViewController.h"
#import "PDProgressView.h"
#import "PDHomeViewController.h"
#import "PDChallengeViewController.h"
#import "PDPayOrderModel.h"
#import "PDAliPayHandle.h"
#import "WXApi.h"
#import "PDAssetsSelectedImgManager.h"
#import "PDRewardViewController.h"
#import "PDLoginViewController.h"
#import "PDRoleBindingViewController.h"
#import "PDHistoryViewController.h"
#import "PDAssetsCameraViewController.h"
#import "PDAssetsPhotoEditViewController.h"
#import "PDChallengerProgress.h"
#import "PDPayViewController.h"
#import "PDQrCodeViewController.h"
#import "PDLaunchViewController.h"
#import "PDEditTestViewController.h"
#import "PDClubViewController.h"
#import "PDChallengeCustomerButton.h"
#import "PDChallengeDetailHouseInfoView.h"
#import "PDAlertView.h"
#import "PDInviteViewController.h"
#import "PDChangePasswordViewController.h"
@interface PDTestViewController()<UITableViewDataSource,UITableViewDelegate>{
    PDChallengeDetailHouseInfoView *smart;
}

@property (nonatomic,strong)UITableView *testTableView;
@property (nonatomic,strong)NSArray *testArr;
@property (nonatomic,strong)UIButton *openDrawerButton;
@property (nonatomic,strong)PDChallengerProgress *imageUploadProgress;

@property (nonatomic,strong)PDChallengeCustomerButton *challengeButton;
@end

@implementation PDTestViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
    
//    -(void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc{
    
//    PDProgressView *pro = [[PDProgressView alloc]initWithFrame:CGRectMake(100, 100, 100, 10)];
//    pro.backgroundColor = [UIColor clearColor];
//    [self.view addSubview:pro];
//    
//    
//    UIPickerView *picker = [UIPickerView pickerViewWithDataSource:@[@[@"1",@"2",@"3",@"2",@"3",@"2",@"3",@"2",@"3"],@[@"3",@"3",@"3",@"2",@"3",@"2",@"3",@"2",@"3",@"2",@"3",@"2",@"3"]] block:^(NSInteger comment, NSInteger row) {
//        NSLog(@"%li,%li",comment,row);
//    }];
//    picker.frame = CGRectMake(0, 0, kScreenBounds.size.width, 500);
//    [self.view addSubview:picker];
    
//    
//    UIPickerView *picker = [UIPickerView pickerViewWithDataSource:@[@[@"1",@"2",@"3",@"2",@"3",@"2",@"3",@"2",@"3"],@[@"3",@"3",@"3",@"2",@"3",@"2",@"3",@"2",@"3",@"2",@"3",@"2",@"3"]] block:^(NSInteger comment, NSInteger row) {
//        NSLog(@"%li,%li",comment,row);
//    }];
//    picker.frame = CGRectMake(0, 0, kScreenBounds.size.width, 500);
//    [self.view addSubview:picker];
    
    
    self.challengeButton = [[PDChallengeCustomerButton alloc]initWithFrame:CGRectMake(50, 50, kScreenBounds.size.width - 2 * 50, 60)];
    self.challengeButton.backgroundColor = [UIColor blackColor];
//    [self.challengeButton buttonClickWithBlock:^{
//        NSLog(@"123");
//    }];
    [self.view addSubview:self.challengeButton];
    
    
    smart = [[PDChallengeDetailHouseInfoView alloc]initWithFrame:CGRectMake(LCFloat(50), 150, 300, 50) fixedString:@"房间"];
    [self.view addSubview:smart];

}

#pragma mark - test
-(void)pageSetting{
    self.barMainTitle = @"测试";
    
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"右侧按钮" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        PDTestViewController *testVC = [[PDTestViewController alloc]init];
//        [strongSelf.navigationController pushViewController:testVC animated:YES];
//        [strongSelf.challengeButton managerSuccessWithBlock:NULL];
        [smart showAnimationWithStirng:@"123"];
    
    }];
}

-(void)arrayWithInit{
    self.testArr = @[@"1.测试",@"2RSA测试",@"3.home",@"4.出征",@"5,支付宝",@"6.相册",@"7.WXPay",@"8.登录界面",@"9.绑定",@"10.换积分",@"11PDRoleBindingViewController",@"12.相机",@"13.历程",@"14.ceshi ",@"15.可编辑cell",@"16.Lunch",@"17.俱乐部",@"18.分享界面",@"19.邀请好友",@"20.修改密码"];


}

#pragma mark - UITableView
-(void)createTableView{
    self.testTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.testTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.testTableView.delegate = self;
    self.testTableView.dataSource = self;
    self.testTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.testTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.testTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.testArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;

    }
    
    cellWithRowOne.textLabel.text = [self.testArr objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){        // 测试
        
    } else if (indexPath.row == 1){     // RSA 测试
        [self rsaTest];
    } else if (indexPath.row == 2){     //右侧菜单
        PDHomeViewController *home = [[PDHomeViewController alloc]init];
        [self.navigationController pushViewController:home animated:YES];
    } else if (indexPath.row == 3){     // 出征
        PDChallengeViewController *challengeViewController = [[PDChallengeViewController alloc]init];
        [self.navigationController pushViewController:challengeViewController animated:YES];
    } else if (indexPath.row == 4){     //
        PDPayViewController *payVC = [[PDPayViewController alloc] init];
        [self.navigationController pushViewController:payVC animated:YES];
    } else if (indexPath.row == 6) {//WXPay
        if (![WXApi isWXAppInstalled]) {
            [[[UIAlertView alloc] initWithTitle:@"提示" message:@"您并没有安装微信客服端，请换个支付方式！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil] show];
        }
        
    } else if (indexPath.row == 5){
        PDAssetsSelectedImgManager *asset = [[PDAssetsSelectedImgManager alloc]init];
        [asset selectAssetsInController:self WithMaxSelected:3 andBlock:^(NSArray *selectedImgArr) {
            NSLog(@"%li",(unsigned long)selectedImgArr.count);
        }];
    } else if (indexPath.row == 9) {
        PDRewardViewController *rewardVC = [[PDRewardViewController alloc] init];
        [self.navigationController pushViewController:rewardVC animated:YES];

    }else if (indexPath.row==7){
        PDLoginViewController *loginVc = [PDLoginViewController new];
        [self.navigationController pushViewController:loginVc animated:YES];
    } else if (indexPath.row == 8){
        PDRoleBindingViewController *bindingVC = [[PDRoleBindingViewController alloc]init];
        [self.navigationController pushViewController:bindingVC animated:YES];
    }else if (indexPath.row == 12) {
        PDHistoryViewController *historyVC = [[PDHistoryViewController alloc] init];
        [self.navigationController pushViewController:historyVC animated:YES];
    }else if (indexPath.row == 11){
        PDAssetsCameraViewController *cameraVC = [[PDAssetsCameraViewController alloc]init];
        cameraVC.isEdit = YES;
        UINavigationController *navi = [[UINavigationController alloc]initWithRootViewController:cameraVC];
        [cameraVC callBackWithImageBlock:^(UIImage *image) {
            NSLog(@"%@",image);
        }];
        [self presentViewController:navi animated:YES completion:nil];
    } else if (indexPath.row == 10){
        PDRoleBindingViewController *roleBindingVC = [[PDRoleBindingViewController alloc]init];
        [self.navigationController pushViewController:roleBindingVC animated:YES];
    } else if (indexPath.row == 13){
//        self.imageUploadProgress = [[PDChallengerProgress alloc] init];
//        self.imageUploadProgress.radius = 100;
//        self.imageUploadProgress.progressBorderThickness = -10;
//        self.imageUploadProgress.trackColor = [UIColor clearColor];
//        self.imageUploadProgress.progressColor = [UIColor whiteColor];
//        self.imageUploadProgress.imageToUpload = [PDTool imageByComposingImage:[UIImage imageNamed:@"login_logo"] withMaskImage:[UIImage imageNamed:@"round"]];
//        [self.imageUploadProgress show];
//        [self performSelector:@selector(smart) withObject:nil afterDelay:1];
        PDQrCodeViewController *test = [[PDQrCodeViewController alloc]init];
        [self.navigationController pushViewController:test animated:YES];

    } else if (indexPath.row == 15) {
        PDLaunchViewController *lunchVC = [[PDLaunchViewController alloc] init];
        [self presentViewController:lunchVC animated:YES completion:nil];

    }else if (indexPath.row == 14){
        PDEditTestViewController *vc = [[PDEditTestViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    } else if (indexPath.row == 16){
//        [self sendInfo];
    } else if (indexPath.row == 16) {
        PDClubViewController *clubVC = [[PDClubViewController alloc] init];
        [self.navigationController pushViewController:clubVC animated:YES];
    }else if (indexPath.row == 17){
       
    }else if (indexPath.row == 18){
        PDInviteViewController *vc = [[PDInviteViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }else if (indexPath.row == 19){
        PDChangePasswordViewController *vc = [[PDChangePasswordViewController alloc]init];
        [self.navigationController pushViewController:vc animated:YES];
    }
}




-(void)smart{
        [self.imageUploadProgress outroAnimation];
}

#pragma mark
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.testTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == self.testArr.count - 1 ) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}


#pragma mark - row 1
-(void)rsaTest{
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:kTestPath completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [[UIAlertView alertViewWithTitle:@"成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}



@end
