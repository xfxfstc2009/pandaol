//
//  PDChallengerKingView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengerKingView.h"
#import <objc/runtime.h>
#import "PDFetchModel.h"

@interface PDChallengerKingView()
@property (nonatomic,strong)PDImageView *kingkCard;
@property (nonatomic,strong)PDImageView *dengdingImageView;
@property (nonatomic,strong)UIButton *keepKingButton;
@property (nonatomic,strong)UIButton *lingshangButton;
@property (nonatomic,strong)UIView *myChallengerView;

@end

@implementation PDChallengerKingView

-(instancetype)initWithFrame:(CGRect)frame lingshangBlock:(void(^)())lingshangBlock myChallengerBlock:(void(^)())challengeBlock{
    self = [super initWithFrame:frame];
    if (self){

        [self createViewWithLingshangBlock:lingshangBlock myChallengerBlock:challengeBlock];
    }
    return self;
}


#pragma mark - king Manager
-(void)createViewWithLingshangBlock:(void(^)())lingshangBlock myChallengerBlock:(void(^)())challengeBlock{
    CGSize cardSize = CGSizeMake(LCFloat(291), LCFloat(263));
    CGSize dengdingSize = CGSizeMake(LCFloat(375), LCFloat(145));
    
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor clearColor];
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height);
    [self addSubview:bgView];
    
    // 1. 创建view
    self.kingkCard = [[PDImageView alloc]init];
    self.kingkCard.backgroundColor = [UIColor clearColor];
    self.kingkCard.frame = CGRectMake((kScreenBounds.size.width - LCFloat(291)) / 2., 0 , LCFloat(291), LCFloat(263));
    self.kingkCard.orgin_y = - self.kingkCard.size_height;
    self.kingkCard.image = [UIImage imageNamed:@"icon_challengeDetail_king_9"];
    [bgView addSubview:self.kingkCard];
    
    // 2. 创建您已登顶
    self.dengdingImageView = [[PDImageView alloc]init];
    self.dengdingImageView.backgroundColor = [UIColor clearColor];
    self.dengdingImageView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(375) / 10.) / 2., cardSize.height + LCFloat(20), dengdingSize.width/ 10., dengdingSize.height / 10.);
    self.dengdingImageView.image = [UIImage imageNamed:@"icon_challengeDetail_King_dengding"];
    [bgView addSubview:self.dengdingImageView];
    
    // 1. 创建按钮保留王座
    self.keepKingButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.keepKingButton setTitle:@"保留王座" forState:UIControlStateNormal];
    self.keepKingButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.keepKingButton.titleLabel.textColor = [UIColor blackColor];
    [self.keepKingButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.keepKingButton.backgroundColor = [UIColor colorWithCustomerName:@"白"];
    self.keepKingButton.layer.cornerRadius = LCFloat(5);
    self.keepKingButton.frame = CGRectMake(LCFloat(105) / 2., kScreenBounds.size.height, kScreenBounds.size.width - LCFloat(105), LCFloat(44));
    __weak typeof(self)weakSelf = self;
    [self.keepKingButton buttonWithBlock:^(UIButton *button) {                 // 执行接口
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestTokeepingKing];
    }];
    [bgView addSubview: self.keepKingButton];
    
    // 2. 创建领赏按钮
    self.lingshangButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.lingshangButton setTitle:@"领赏" forState:UIControlStateNormal];
    self.lingshangButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.lingshangButton.titleLabel.textColor = [UIColor blackColor];
    [self.lingshangButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.lingshangButton.backgroundColor = [UIColor colorWithCustomerName:@"白"];
    self.lingshangButton.layer.cornerRadius = LCFloat(5);
    self.lingshangButton.frame = CGRectMake(LCFloat(105) / 2., CGRectGetMaxY(self.keepKingButton.frame) + LCFloat(20), kScreenBounds.size.width - LCFloat(105), LCFloat(44));
    [self.lingshangButton buttonWithBlock:^(UIButton *button) {                 // 执行接口
        if (!weakSelf){
            return ;
        }
        if (lingshangBlock){
            lingshangBlock();
        }
    }];
    [bgView addSubview: self.lingshangButton];
    
    // 3. 创建我的赛事按钮
    self.myChallengerView = [weakSelf createFixedViewWithMyChallengeBlock:^{
        if (!weakSelf){
            return ;
        }
        if (challengeBlock){
            challengeBlock();
        }
    }];
    self.myChallengerView.orgin_y = kScreenBounds.size.height;
    self.myChallengerView.hidden = YES;
    [bgView addSubview:self.myChallengerView];
    
    
    
    // 3. 重新定义高度
    [UIView animateWithDuration:.5f animations:^{
        CGFloat kTopMargin = (kScreenBounds.size.height - (cardSize.height + dengdingSize.height + LCFloat(20))) / 2.;
        self.kingkCard.orgin_y = kTopMargin;
        self.dengdingImageView.orgin_y = CGRectGetMaxY(self.kingkCard.frame) + LCFloat(40);
    } completion:^(BOOL finished) {
        CGAffineTransform  transform;
        transform = CGAffineTransformScale(self.dengdingImageView.transform,10,10);
        [UIView beginAnimations:@"scale" context:nil];
        [UIView setAnimationDuration:1.];
        [UIView setAnimationDelegate:self];
        [self.dengdingImageView setTransform:transform];
        [UIView commitAnimations];
    }];
    
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 3. 重新定义高度
        [UIView animateWithDuration:.5f animations:^{
            self.dengdingImageView.alpha = 0;
            CGAffineTransform  transform = CGAffineTransformScale(self.dengdingImageView.transform,.1,.1);
            [self.dengdingImageView setTransform:transform];
            
            self.keepKingButton.orgin_y = CGRectGetMaxY(self.kingkCard.frame) + LCFloat(20);
            self.lingshangButton.orgin_y = CGRectGetMaxY(self.keepKingButton.frame) + LCFloat(20);
        } completion:^(BOOL finished) {
            self.dengdingImageView.hidden = YES;
        }];
    });
}

#pragma mark 选择保留王座按钮
-(void)chooseKingTypeButtonClickManager{
    self.keepKingButton.enabled = NO;
    self.dengdingImageView.hidden = NO;
    self.dengdingImageView.alpha = 1;
    self.dengdingImageView.image = [UIImage imageNamed:@"icon_challengeDetail_king_wangzuo"];
    [UIView animateWithDuration:.5f animations:^{
        self.keepKingButton.alpha = 0;
        self.lingshangButton.alpha = 0;
    } completion:^(BOOL finished) {
        self.keepKingButton.hidden = YES;
        self.lingshangButton.hidden = YES;
    }];
    
    // 1.王卡隐藏
    CGAffineTransform  transform;
    transform = CGAffineTransformScale(self.kingkCard.transform,.1,.1);
    
    CGAffineTransform  transform1;
    transform1 = CGAffineTransformScale(self.dengdingImageView.transform,10,10);
    

    [UIView beginAnimations:@"scale" context:nil];
    [UIView setAnimationDuration:1.];
    [UIView setAnimationDelegate:self];
    [self.kingkCard setTransform:transform];
    [self.dengdingImageView setTransform:transform1];
    [UIView commitAnimations];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        // 3. 重新定义高度
        self.kingkCard.image = [UIImage imageNamed:@"icon_challengeDetail_card_king"];
        CGAffineTransform  transform;
        transform = CGAffineTransformScale(self.kingkCard.transform,10,10);
        [UIView beginAnimations:@"scale" context:nil];
        [UIView setAnimationDuration:1.];
        [UIView setAnimationDelegate:self];
        [self.kingkCard setTransform:transform];
        [UIView commitAnimations];
    });
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.9 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.myChallengerView.hidden = NO;
        [UIView animateWithDuration:.5f animations:^{
            self.myChallengerView.orgin_y = CGRectGetMaxY(self.dengdingImageView.frame) + LCFloat(20);
        } completion:^(BOOL finished) {
            self.myChallengerView.orgin_y = CGRectGetMaxY(self.dengdingImageView.frame) + LCFloat(20);
        }];
    });
}

#pragma mark - 创建view
-(UIView *)createFixedViewWithMyChallengeBlock:(void(^)())block{
    UIView *myView = [[UIView alloc]init];
    myView.frame = CGRectMake(LCFloat(115) / 2., 0, kScreenBounds.size.width - LCFloat(115), LCFloat(30));
    myView.clipsToBounds = YES;
    myView.layer.cornerRadius = LCFloat(3);
    myView.backgroundColor = [UIColor whiteColor];
    
    
    // 1. 创建label
    UILabel *fixedLabel1 = [[UILabel alloc]init];
    fixedLabel1.backgroundColor = [UIColor clearColor];
    fixedLabel1.font = [UIFont systemFontOfCustomeSize:14.];
    fixedLabel1.text = @"您已经选择保留王座";
    fixedLabel1.textColor = [UIColor colorWithCustomerName:@"红"];
    fixedLabel1.textAlignment = NSTextAlignmentCenter;
    fixedLabel1.frame = CGRectMake(0, LCFloat(20), myView.size_width, [NSString contentofHeightWithFont:fixedLabel1.font]);
    [myView addSubview:fixedLabel1];
    
    // 2. 创建label2
    UILabel *fixedLabel2 = [[UILabel alloc]init];
    fixedLabel2.backgroundColor = [UIColor clearColor];
    fixedLabel2.font = [UIFont systemFontOfCustomeSize:14.];
    fixedLabel2.text = @"请耐心等候客服人员联系";
    fixedLabel2.textColor = [UIColor colorWithCustomerName:@"红"];
    fixedLabel2.textAlignment = NSTextAlignmentCenter;
    fixedLabel2.frame = CGRectMake(0, CGRectGetMaxY(fixedLabel1.frame), myView.size_width, [NSString contentofHeightWithFont:fixedLabel2.font]);
    [myView addSubview:fixedLabel2];
    
    // 3. 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, CGRectGetMaxY(fixedLabel2.frame) + LCFloat(20), myView.size_width, .5f);
    [myView addSubview:lineView];
    
    // 2. 创建领赏按钮
    UIButton *myChallengeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [myChallengeButton setTitle:@"我的赛事" forState:UIControlStateNormal];
    myChallengeButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    [myChallengeButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    myChallengeButton.backgroundColor = [UIColor colorWithCustomerName:@"白"];
    myChallengeButton.layer.cornerRadius = LCFloat(5);
    myChallengeButton.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
    myChallengeButton.layer.borderWidth = .5f;
    myChallengeButton.frame = CGRectMake((myView.size_width - LCFloat(150)) / 2., CGRectGetMaxY(lineView.frame) + LCFloat(20), LCFloat(150), LCFloat(30));
    __weak typeof(self)weakSelf = self;
    [myChallengeButton buttonWithBlock:^(UIButton *button) {                 // 执行接口
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    [myView addSubview: myChallengeButton];
    myView.size_height = CGRectGetMaxY(myChallengeButton.frame) + LCFloat(20);
    
    return myView;
}


#pragma mark 保留王座
-(void)sendRequestTokeepingKing{
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [fetchModel fetchWithPath:kChallengerKing completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf chooseKingTypeButtonClickManager];
        }
    }];
}

@end
