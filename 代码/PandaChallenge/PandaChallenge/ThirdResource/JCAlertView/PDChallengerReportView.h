//
//  PDChallengerReportView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDChallengerReportView : UIView

-(instancetype)initWithFrame:(CGRect)frame successBlock:(void(^)())successBlock failBlock:(void(^)())block;

-(instancetype)initWithFrame:(CGRect)frame reportSuccessBlock:(void(^)())confirmBlock;

@end
