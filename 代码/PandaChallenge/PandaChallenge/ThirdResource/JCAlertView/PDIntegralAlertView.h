//
//  PDIntegralAlertView.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PDIntegralAlertViewType) {
    PDIntegralAlertViewTypeSuccess,     /**成功*/
    PDIntegralAlertViewTypeFail,        /**失败*/
    PDIntegralAlertViewTypeWarn,        /**警告*/
};

@interface PDIntegralAlertView : UIView
- (void)alertForAlertViewType:(PDIntegralAlertViewType)alertViewType withTitle:(NSString *)title desc:(NSString *)desc;
/** 描述在横线上变*/
- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage titleDesc:(NSString *)titleDesc buttonNameArray:(NSArray *)nameArray clickBlock:(void(^)(NSInteger index))block;
/** 描述在横下上变*/
- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc;
- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void(^)(NSInteger index))block;
/**显示积分列表*/
- (void)alertListWithTitle:(NSString *)title cardModelArray:(NSArray *)cardModelArray totalintegral:(NSInteger)totalIntegral buttonblock:(void (^)(NSInteger buttonIndex))block;
/**兑换人名币成功结果*/
- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage changeRMB:(NSInteger)rmb netInfo:(PDNetModel *)netModel desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void (^)(NSInteger buttonIndex))block;
@end
