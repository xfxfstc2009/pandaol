//
//  PDLoginAlertView.m
//  PandaChallenge
//
//  Created by panda on 16/4/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginAlertView.h"
#import <objc/runtime.h>
@implementation PDLoginAlertView
- (instancetype)initWithFrame:(CGRect)frame alertWithType:(loginWithAlertType)type block:(void (^)(NSInteger))block
{
    if (self = [super initWithFrame:frame]) {
        UIView *bgView;
        if (type == alertWithReSetPasswordSuccess) {
            bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"icon_report_success"] title:@"重置成功" desc:@"请妥善保管您的密码" subDesc:nil kCardImg:nil buttonArr:@[@"确定"] subIconImg:nil challengeEnd:challengeEndNormal isCenter:NO buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
                if (block) {
                    block(buttonIndex);
                }
            }];
        } else if (type == alertWithRegisterSuccess) {
            bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"icon_report_success"] title:@"注册成功" desc:@"欢迎使用PANDAOL" subDesc:nil kCardImg:nil buttonArr:@[@"确定"] subIconImg:nil challengeEnd:challengeEndNormal isCenter:NO buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
                if (block) {
                    block(buttonIndex);
                }
            }];
        }
        self.frame = bgView.bounds;
        [self addSubview:bgView];
    }
    return self;
}

-(UIView *)alertWithHeaderImgView:(UIImage *)errImg title:(NSString *)title desc:(NSString *)desc subDesc:(NSString *)subDesc kCardImg:(UIImage *)kCardImg buttonArr:(NSArray *)buttonArr subIconImg:(UIImage *)subIconImg challengeEnd:(challengeEnd)challengeEndType isCenter:(BOOL)isCenter buttonTitleArrbuttonClick:(void (^)(NSInteger buttonIndex))block{
    
    // 1.背景
    UIImageView *bgView = [[UIImageView alloc]init];
    bgView.image = [PDTool stretchImageWithName:@"bg_report_half"];
    bgView.userInteractionEnabled = YES;
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(50), 300);
    
    // 2. 创建胜利图片
    UIImageView *iconImageView = [[UIImageView alloc]init];
    iconImageView.backgroundColor = [UIColor clearColor];
    iconImageView.image = errImg;
    iconImageView.frame = CGRectMake((bgView.size_width - LCFloat(50)) / 2., LCFloat(15), LCFloat(50), LCFloat(50));
    [bgView addSubview:iconImageView];
    
    // 3. 创建提示
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    titleLabel.frame = CGRectMake(0, CGRectGetMaxY(iconImageView.frame) + LCFloat(15), bgView.size_width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [bgView addSubview:titleLabel];
    
    // 4. 创建desc
    UILabel *descLabel = [[UILabel alloc]init];
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.font = [UIFont systemFontOfCustomeSize:14.];
    descLabel.numberOfLines = 0;
    descLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.text = desc;
    CGSize contentOfDescSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(bgView.size_width - 2 * LCFloat(11), CGFLOAT_MAX)];
    descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(titleLabel.frame) + LCFloat(20), bgView.size_width - 2 * LCFloat(11), contentOfDescSize.height);
    [bgView addSubview:descLabel];
    
    // 4. 创建钱袋Image
    PDImageView *moneyImageView = [[PDImageView alloc]init];
    moneyImageView.backgroundColor = [UIColor clearColor];
    moneyImageView.image = subIconImg;
    [bgView addSubview:moneyImageView];
    
    // 6. 进行计算
    CGFloat iconWidth = LCFloat(40);
    if (contentOfDescSize.height == [NSString contentofHeightWithFont:descLabel.font]){ // 一行
        CGSize contentOfDescLabelSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:descLabel.font])];
        if (subIconImg){
            CGFloat origin_X = (bgView.size_width - contentOfDescLabelSize.width - LCFloat(10) - iconWidth) / 2.;
            descLabel.frame = CGRectMake(origin_X, CGRectGetMaxY(titleLabel.frame) + LCFloat(25), contentOfDescLabelSize.width, [NSString contentofHeightWithFont:descLabel.font]);
            moneyImageView.frame = CGRectMake(CGRectGetMaxX(descLabel.frame) + LCFloat(10), CGRectGetMaxY(descLabel.frame) - iconWidth, iconWidth, iconWidth);
        }
    } else {                   // 多行
        
    }
    
    
    // 5. 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(descLabel.frame) + LCFloat(10), bgView.size_width - 2 * LCFloat(11), .5f);
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [bgView addSubview:lineView];
    if (!desc.length){
        lineView.orgin_y = CGRectGetMaxY(titleLabel.frame) + LCFloat(10);
    }
    
    // 6. 创建subDesc
    UILabel *subDescLabel = [[UILabel alloc]init];
    subDescLabel.backgroundColor = [UIColor clearColor];
    subDescLabel.font = [UIFont systemFontOfCustomeSize:13.];
    subDescLabel.numberOfLines = 0;
    subDescLabel.textColor = [UIColor colorWithCustomerName:@"黑http://m.pandaol.net/panda/club/pay.html"];
    subDescLabel.textAlignment = NSTextAlignmentLeft;
    subDescLabel.text = subDesc;
    CGSize subDescSize = [subDescLabel.text sizeWithCalcFont:subDescLabel.font constrainedToSize:CGSizeMake(descLabel.size_width, CGFLOAT_MAX)];
    subDescLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(lineView.frame) + LCFloat(10), bgView.size_width - 2 * LCFloat(11), subDescSize.height);
    [bgView addSubview:subDescLabel];
    if (!desc.length){
        subDescLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    // 6. 创建k卡
    UIImageView *kCardImageView = [[UIImageView alloc]init];
    kCardImageView.backgroundColor = [UIColor clearColor];
    kCardImageView.image = kCardImg;
    kCardImageView.frame = CGRectMake((bgView.size_width - kCardImg.size.width) / 2., CGRectGetMaxY(lineView.frame) + LCFloat(10), kCardImg.size.width, kCardImg.size.height);
    [bgView addSubview:kCardImageView];
    
    // 7. 创建按钮
    for (int i = 0 ; i < (isCenter ? buttonArr.count + 1: buttonArr.count);i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        
        CGFloat origin_x = LCFloat(20);
        CGFloat origin_y = CGRectGetMaxY(lineView.frame) + LCFloat(20) + (LCFloat(35) + LCFloat(20)) * i;
        button.frame = CGRectMake(origin_x, origin_y, bgView.size_width - 2 * LCFloat(20), LCFloat(35));
        [bgView addSubview:button];
        
        if (isCenter){
            if (i == buttonArr.count){
                button.layer.borderColor = [UIColor clearColor].CGColor;
                [button setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
                [button setTitle:@"客服中心" forState:UIControlStateNormal];
                button.titleLabel.font = [UIFont systemFontOfCustomeSize:12.];
                [button setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
                if (subDesc.length){
                    button.frame = CGRectMake(origin_x, CGRectGetMaxY(subDescLabel.frame) + LCFloat(10),bgView.size_width - 2 * LCFloat(20), LCFloat(35));
                }
            } else {
                [button setTitle:[buttonArr objectAtIndex:i] forState:UIControlStateNormal];
            }
        } else {
            [button setTitle:[buttonArr objectAtIndex:i] forState:UIControlStateNormal];
        }
        
        [button buttonWithBlock:^(UIButton *button) {
            if (block){
                block(i);
            }
        }];
    }
    
    CGFloat bgViewHeight = 0;
    bgViewHeight += CGRectGetMaxY(lineView.frame);
    
    // 1. 提示状态《按钮》
    if (subDesc.length){            // 判断是否有底部的文字
        subDescLabel.hidden = NO;
        kCardImageView.hidden = YES;
        bgViewHeight += LCFloat(10);
        bgViewHeight += subDescSize.height;
        bgViewHeight += LCFloat(10);
    } else if (kCardImg){
        subDescLabel.hidden = YES;
        kCardImageView.hidden = NO;
        bgViewHeight += LCFloat(10);
        bgViewHeight += kCardImg.size.height;
        bgViewHeight += LCFloat(10);
    } else if (buttonArr.count){
        subDescLabel.hidden = YES;
        kCardImageView.hidden = YES;
        bgViewHeight += LCFloat(20) + (LCFloat(35) + LCFloat(20)) * buttonArr.count;
    } else {
        subDescLabel.hidden = YES;
        kCardImageView.hidden = YES;
        lineView.hidden = YES;
    }
    if (isCenter){
        bgViewHeight += (LCFloat(35) + LCFloat(10));
    }
    bgView.size_height = bgViewHeight;
    return bgView;
}


- (instancetype)initWithFrame:(CGRect)frame buttonTitleArr:(NSArray *)buttonTitleArr block:(void (^)(NSInteger))block
{
    UIView *bgView;
    if (self = [super initWithFrame:frame]) {
        bgView = [self alertByBindingWithHeaderImgView:nil title:@"111" desc:@"555" buttonArr:buttonTitleArr buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            
        }];
        self.frame = bgView.bounds;
        [self addSubview:bgView];
    }
    
    return self;
}

- (UIView *)alertByBindingWithHeaderImgView:(UIImage *)errImg title:(NSString *)title desc:(NSString *)desc  buttonArr:(NSArray *)buttonArr buttonTitleArrbuttonClick:(void (^)(NSInteger buttonIndex))block{
    
    // 1.背景
    UIImageView *bgView = [[UIImageView alloc]init];
    bgView.image = [PDTool stretchImageWithName:@"bg_report_half"];
    bgView.userInteractionEnabled = YES;
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(50), 300);
    
    // 2. 创建胜利图片
    UIImageView *iconImageView = [[UIImageView alloc]init];
    iconImageView.backgroundColor = [UIColor clearColor];
    iconImageView.image = errImg;
    iconImageView.frame = CGRectMake((bgView.size_width - LCFloat(50)) / 2., LCFloat(15), LCFloat(50), LCFloat(50));
    [bgView addSubview:iconImageView];
    
    // 3. 创建提示
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    titleLabel.frame = CGRectMake(0, CGRectGetMaxY(iconImageView.frame) + LCFloat(15), bgView.size_width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [bgView addSubview:titleLabel];
    
    // 4. 创建desc
    UILabel *descLabel = [[UILabel alloc]init];
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.font = [UIFont systemFontOfCustomeSize:14.];
    descLabel.numberOfLines = 0;
    descLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.text = desc;
    CGSize contentOfDescSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(bgView.size_width - 2 * LCFloat(11), CGFLOAT_MAX)];
    descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(titleLabel.frame) + LCFloat(20), bgView.size_width - 2 * LCFloat(11), contentOfDescSize.height);
    [bgView addSubview:descLabel];
    
    
    // 6. 进行计算
//    CGFloat iconWidth = LCFloat(40);
    if (contentOfDescSize.height == [NSString contentofHeightWithFont:descLabel.font]){ // 一行
//        CGSize contentOfDescLabelSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:descLabel.font])];
    } else {                   // 多行
        
    }
    
    
    // 5. 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(descLabel.frame) + LCFloat(10), bgView.size_width - 2 * LCFloat(11), .5f);
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [bgView addSubview:lineView];
    if (!desc.length){
        lineView.orgin_y = CGRectGetMaxY(titleLabel.frame) + LCFloat(10);
    }
    
    
    // 7. 创建按钮
    CGFloat button_w = (bgView.size_width - (buttonArr.count + 1) * LCFloat(11)) / buttonArr.count;
    CGFloat button_h = LCFloat(40);
    for (int i = 0 ; i < (buttonArr.count);i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        button.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
        [button setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        
        CGFloat origin_x = (button_w + LCFloat(11)) * i;
        CGFloat origin_y = CGRectGetMaxY(lineView.frame) + LCFloat(20);
        button.frame = CGRectMake(origin_x + LCFloat(11), origin_y, button_w, button_h);
        [bgView addSubview:button];
        
        [button setTitle:[buttonArr objectAtIndex:i] forState:UIControlStateNormal];
        [button buttonWithBlock:^(UIButton *button) {
            if (block){
                block(i);
            }
        }];
    }
    
    CGFloat bgViewHeight = 0;
    bgViewHeight += CGRectGetMaxY(lineView.frame);
    bgViewHeight += LCFloat(20) + LCFloat(40) + LCFloat(20);
    bgView.size_height = bgViewHeight;
    return bgView;
}


@end
