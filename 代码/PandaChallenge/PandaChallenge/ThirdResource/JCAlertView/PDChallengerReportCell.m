//
//  PDChallengerReportCell.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengerReportCell.h"

@interface PDChallengerReportCell()
@property (nonatomic,strong)PDImageView *iconImageView;
@property (nonatomic,strong)UILabel *dymicLabel;

@end

@implementation PDChallengerReportCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView {
    self.iconImageView = [[PDImageView alloc]init];
    self.iconImageView.frame = CGRectMake(LCFloat(11), LCFloat(5), LCFloat(11), LCFloat(11));
    self.iconImageView.image = [UIImage imageNamed:@""];
    [self addSubview:self.iconImageView];
    
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.dymicLabel.numberOfLines = 0;
    [self addSubview:self.dymicLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.iconImageView.frame = CGRectMake(LCFloat(11), (transferCellHeight - LCFloat(11)) / 2., LCFloat(11), LCFloat(11));
}

-(void)setTransferReportSingleModel:(PDChallengerReportSingleModel *)transferReportSingleModel{
    _transferReportSingleModel = transferReportSingleModel;
    self.dymicLabel.text = transferReportSingleModel.content;
    CGSize contentOfSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11) - LCFloat(11) - LCFloat(11), CGFLOAT_MAX)];
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + LCFloat(11), LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(11) - LCFloat(11) - LCFloat(11), contentOfSize.height);
}


- (void)setChecked:(BOOL)checked{
    if (checked) {
        self.iconImageView.image = [UIImage imageNamed:@"icon_challenge_report_selected"];
    } else {
        self.iconImageView.image = [UIImage imageNamed:@"icon_challenge_report_normal"];
    }
    isChecked = checked;
}


#pragma mark - 计算高度
+(CGFloat)calculationCellHeihgtWithText:(NSString *)text{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(5);
    CGSize contentOfSize = [text sizeWithCalcFont:[UIFont systemFontOfCustomeSize:14.] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11) - LCFloat(11) - LCFloat(11), CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    cellHeight += LCFloat(5);
    return cellHeight;
}

@end
