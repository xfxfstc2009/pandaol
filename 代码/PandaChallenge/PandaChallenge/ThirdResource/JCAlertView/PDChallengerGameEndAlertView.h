//
//  PDChallengerGameEndAlertView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 比赛结果view

#import <UIKit/UIKit.h>
#import "PDChallengerCardEnum.h"                    // k 卡枚举
#import "PDAlertView.h"

@interface PDChallengerGameEndAlertView : UIView

#pragma mark - 倒计时
-(instancetype)initWithFrame:(CGRect)frame alertWithTime:(NSInteger)timeInteger;

#pragma mark - 基础弹框
-(instancetype)initWithFrame:(CGRect)frame alertWithType:(alertType)type block:(void (^)(NSInteger buttonIndex))block;

#pragma mark - 基础弹框(动态数据)
-(instancetype)initWithFrame:(CGRect)frame alertWithType:(alertType)type desc:(NSString *)desc isServer:(BOOL)isServer isAgain:(BOOL)again block:(void (^)(NSInteger buttonIndex))block;

#pragma mark - 比赛结果获取方式
-(instancetype)initWithFrame:(CGRect)frame alertWithChallengeEndType:(challengeEnd)endType currentKCard:(kCardType)kCard integral:(NSInteger)integral buttonArr:(NSArray *)btnArr block:(void (^)(NSInteger buttonIndex))block;

@end
