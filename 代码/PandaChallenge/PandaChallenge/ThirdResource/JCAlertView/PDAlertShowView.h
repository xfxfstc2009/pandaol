//
//  PDAlertShowView.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>


typedef NS_ENUM(NSUInteger, PDAlertShowViewType) {
    PDAlertShowViewTypeSuccess,     /**成功*/
    PDAlertShowViewTypeFail,        /**失败*/
    PDAlertShowViewTypeWarn,        /**警告*/
};

@interface PDAlertShowView : UIView

- (void)alertForAlertViewType:(PDAlertShowViewType)alertViewType withTitle:(NSString *)title desc:(NSString *)desc;
/** 描述竖排*/
- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage titleDesc:(NSString *)titleDesc buttonNameArray:(NSArray *)nameArray clickBlock:(void(^)(NSInteger index))block;
/** 有客户中心的竖排*/
- (void)alertHaveClientCenterWithTitle:(NSString *)title headerImage:(UIImage *)headerImage titleDesc:(NSString *)titleDesc buttonNameArray:(NSArray *)nameArray clickBlock:(void(^)(NSInteger index))block;
/** 描述在横线下边*/
- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc;
/** 按钮竖排*/
//- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void(^)(NSInteger index))block;
/** 按钮横排*/
- (void)alertHorizontalButtonWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void(^)(NSInteger index))block;
#pragma mark - 积分部分
/**显示积分列表*/
- (void)alertListWithTitle:(NSString *)title cardModelArray:(NSArray *)cardModelArray totalintegral:(NSInteger)totalIntegral buttonblock:(void (^)(NSInteger buttonIndex))block;
/**兑换人名币成功结果*/
- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage changeRMB:(NSInteger)rmb netInfo:(PDNetModel *)netModel desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void (^)(NSInteger buttonIndex))block;

#pragma mark - 网络失败弹框
- (void)showErrorAlertViewWithErrorString:(NSString *)errorStr;
@end
