//
//  PDIntegralChangeCell.m
//  PandaChallenge
//
//  Created by 盼达 on 16/4/5.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDIntegralChangeCell.h"

@implementation PDIntegralChangeCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        self.cardImageView.hidden = NO;
    }
    return self;
}

- (UIImageView *)cardImageView {
    if (!_cardImageView) {
        _cardImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_cardImageView];
        [_cardImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.left.mas_equalTo(10);
            make.size.mas_equalTo(CGSizeMake(30, 40));
        }];
    }
    return _cardImageView;
}

- (UILabel *)numLabel {
    if (!_numLabel) {
        _numLabel = [[UILabel alloc] init];
        _numLabel.font = [[UIFont systemFontOfCustomeSize:18] boldFont];
        _numLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_numLabel];
        [_numLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(0);
            make.left.mas_equalTo(self.cardImageView.mas_right).mas_equalTo(10);
            make.size.mas_equalTo(CGSizeMake(60, 30));
        }];
    }
    return _numLabel;
}

- (UILabel *)integralLabel {
    if (!_integralLabel) {
        _integralLabel = [[UILabel alloc] init];
        _integralLabel.font = [[UIFont systemFontOfCustomeSize:18] boldFont];
        _integralLabel.backgroundColor = [UIColor clearColor];
        _integralLabel.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:_integralLabel];
        [_integralLabel mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.mas_equalTo(-20);
            make.centerY.mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(80, 30));
        }];
    }
    return _integralLabel;
}

- (UIImageView *)iconImageView {
    if (!_iconImageView) {
        _iconImageView = [[UIImageView alloc] init];
        [self.contentView addSubview:_iconImageView];
        [_iconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.integralLabel.mas_right).mas_equalTo(10);
            make.centerY.mas_equalTo(0);
            make.size.mas_equalTo(CGSizeMake(25, 25));
        }];
    }
    return _iconImageView;
}


@end
