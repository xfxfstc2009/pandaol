//
//  PDIntegralChangeCell.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/5.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDIntegralChangeCell : UITableViewCell
@property (nonatomic, strong) UIImageView *cardImageView;
@property (nonatomic, strong) UILabel     *numLabel;
@property (nonatomic, strong) UILabel     *integralLabel;
@property (nonatomic, strong) UIImageView *iconImageView;
@end
