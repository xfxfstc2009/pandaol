//
//  PDIntegralBackgroundView.h
//  PandaChallenge
//
//  Created by 盼达 on 16/4/5.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDIntegralBackgroundView : UIView
- (UIView *)customBackgroundViewWithTitle:(NSString *)title modelArray:(NSArray *)modelArray totalIntegral:(NSInteger)totalIntegral buttonArray:(NSArray *)buttonArray buttonClick:(void (^)(NSInteger buttonIndex))block;
@end
