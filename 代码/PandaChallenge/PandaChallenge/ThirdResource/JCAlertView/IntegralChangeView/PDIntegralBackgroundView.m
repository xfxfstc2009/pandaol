//
//  PDIntegralBackgroundView.m
//  PandaChallenge
//
//  Created by 盼达 on 16/4/5.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDIntegralBackgroundView.h"
#import "PDIntegralChangeCell.h"
#import "PDUsedCard.h"

#define IntegralCellHeight      50   // 显示积分兑换表的行高
#define ButtonWidth             200
#define ButtonHeight            30
#define ButtonSpace             20   // 按钮间距
#define LineSpace_width         (bgSize.width-LCFloat(15)*2)
#define LineSpace_x             LCFloat(15)


@interface PDIntegralBackgroundView ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSArray *modelArray;
@end

@implementation PDIntegralBackgroundView
- (instancetype)init {
    self = [super initWithFrame:kScreenBounds];
    if (self) {
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.6];
    }
    return self;
}

- (UIView *)customBackgroundViewWithTitle:(NSString *)title modelArray:(NSArray *)modelArray totalIntegral:(NSInteger)totalIntegral buttonArray:(NSArray *)buttonArray buttonClick:(void (^)(NSInteger))block {
    self.modelArray = modelArray;
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    [window addSubview:self];
    
    UIView *backgroundView = [[UIView alloc] init];
    backgroundView.backgroundColor = [UIColor whiteColor];
    CGFloat bgViewHeight = 286;
    NSUInteger maxRow = 3; // 最大的行数
    CGFloat tabelViewheight;
    if (modelArray.count >= maxRow) {
        tabelViewheight = maxRow * IntegralCellHeight;
        bgViewHeight += tabelViewheight;
    } else {
        tabelViewheight = modelArray.count * IntegralCellHeight;
        bgViewHeight += tabelViewheight;
    }
    backgroundView.frame = CGRectMake(0, 0, kScreenWidth - 2 * LCFloat(50), bgViewHeight);
    backgroundView.center = self.center;
    backgroundView.layer.cornerRadius = 5;
    backgroundView.clipsToBounds = YES;
    [self addSubview:backgroundView];
    
    CGSize bgSize = backgroundView.frame.size;
    
    // 金币图
    UIImageView *headerImageView = [[UIImageView alloc] init];
    headerImageView.image = [UIImage imageNamed:@"icon_integral_change"];
    headerImageView.frame = CGRectMake((bgSize.width-30)/2, 15, 30, 30);
    [backgroundView addSubview:headerImageView];
    
    // 表头
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(headerImageView.frame)+5, bgSize.width, 39)];
    titleLabel.text = title;
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [[UIFont systemFontOfCustomeSize:20] boldFont];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [backgroundView addSubview:titleLabel];
    
    // line
    UIImageView *line = [[UIImageView alloc] initWithFrame:CGRectMake(LineSpace_x, CGRectGetMaxY(titleLabel.frame)+10, LineSpace_width, 1)];
    line.image = [UIImage imageNamed:@"icon_integral_line"];
    [backgroundView addSubview:line];
    
    // 积分表
    UITableView *integralTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(line.frame), bgSize.width, tabelViewheight)];
    integralTableView.backgroundColor = [UIColor clearColor];
    integralTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    integralTableView.delegate = self;
    integralTableView.dataSource = self;
    [backgroundView addSubview:integralTableView];
    
    // 总积分
    UIView *totalView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(integralTableView.frame), bgSize.width, 45)];
    totalView.backgroundColor = [UIColor whiteColor];
    [backgroundView addSubview:totalView];
    
    UILabel *totalLabel = [[UILabel alloc] init];
    totalLabel.backgroundColor = [UIColor clearColor];
    totalLabel.font = [[UIFont systemFontOfCustomeSize:20] boldFont];
    totalLabel.text = [NSString stringWithFormat:@"%ld B",(long)totalIntegral];
    totalLabel.textAlignment = NSTextAlignmentCenter;
    CGSize labelSize = [totalLabel.text sizeWithCalcFont:totalLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, 45)];
    totalLabel.frame = CGRectMake((bgSize.width-labelSize.width)/2, 0, labelSize.width, 45);
    [totalView addSubview:totalLabel];
    
    // 总积分下方的横线
    UIImageView *bottomLine = [[UIImageView alloc] init];
    bottomLine.frame = CGRectMake(LineSpace_x, CGRectGetMaxY(totalView.frame)+5, LineSpace_width, 1);
    bottomLine.image = [UIImage imageNamed:@"icon_integral_line"];
    [backgroundView addSubview:bottomLine];
    
    UILabel *warnLabel = [[UILabel alloc] init];
    warnLabel.font = [[UIFont systemFontOfCustomeSize:12] boldFont];
    warnLabel.frame = CGRectMake(0, CGRectGetMaxY(bottomLine.frame)+5, bgSize.width, [NSString contentofHeightWithFont:warnLabel.font]);
    warnLabel.textAlignment = NSTextAlignmentCenter;
    warnLabel.textColor = [UIColor redColor];
    warnLabel.text = @"兑换后，您将失去该级别的晋级资格";
    [backgroundView addSubview:warnLabel];
    
    // 按钮
    for (int i = 0;i < buttonArray.count;i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.tag = i;
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:[buttonArray objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        button.titleLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        
        CGFloat button_y = CGRectGetMaxY(warnLabel.frame)+15+i*(ButtonHeight+ButtonSpace);
        CGFloat button_x = (bgSize.width - ButtonWidth)/2;
        button.frame = CGRectMake(button_x,  button_y, ButtonWidth, ButtonHeight);
        [backgroundView addSubview:button];
        [button buttonWithBlock:^(UIButton *button) {
            block(i);
        }];
    }
    return backgroundView;
}

#pragma mark - UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.modelArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *ID = @"changeIntegralCell";
    PDIntegralChangeCell *cell = [tableView dequeueReusableCellWithIdentifier:ID];
    if (!cell) {
        cell = [[PDIntegralChangeCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ID];
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    PDUsedCard *usedCard = [self.modelArray objectAtIndex:indexPath.row];
    cell.cardImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_kCard_result_%@",usedCard.card_level]];
    cell.numLabel.text = [NSString stringWithFormat:@"X%@",usedCard.card_num];
    cell.integralLabel.text = [NSString stringWithFormat:@"%ld B",(long)(usedCard.exchange_point.integerValue * usedCard.card_num.integerValue)];
    return cell;
} 

#pragma mark - TableViewDelegate
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return IntegralCellHeight;
}


@end
