//
//  PDLoginAlertView.h
//  PandaChallenge
//
//  Created by panda on 16/4/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef NS_ENUM(NSInteger,loginWithAlertType) {
    alertWithReSetPasswordSuccess,          /**< 修改密码成功*/
    alertWithRegisterSuccess,               /**< 注册成功*/
};

@interface PDLoginAlertView : UIView
-(instancetype)initWithFrame:(CGRect)frame alertWithType:(loginWithAlertType)type block:(void (^)(NSInteger buttonIndex))block;

- (instancetype)initWithFrame:(CGRect)frame buttonTitleArr:(NSArray *)buttonTitleArr block:(void (^)(NSInteger buttonIndex))block;
@end
