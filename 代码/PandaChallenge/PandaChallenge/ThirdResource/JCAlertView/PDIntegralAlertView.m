//
//  PDIntegralAlertView.m
//  PandaChallenge
//
//  Created by 盼达 on 16/4/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDIntegralAlertView.h"

@implementation PDIntegralAlertView
- (void)alertForAlertViewType:(PDIntegralAlertViewType)alertViewType withTitle:(NSString *)title desc:(NSString *)desc {
    UIView *bgView;
    if (alertViewType == PDIntegralAlertViewTypeFail) {
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"bg_challenge_report_fail"] title:title desc:nil subDesc:desc kCardImg:nil buttonArr:nil subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:nil];
    } else if (alertViewType == PDIntegralAlertViewTypeSuccess) {
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"bg_challenge_report_success"] title:title desc:nil subDesc:desc kCardImg:nil buttonArr:nil subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:nil];
    } else if (alertViewType == PDIntegralAlertViewTypeWarn) {
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"bg_challenge_report_warn"] title:title desc:nil subDesc:desc kCardImg:nil buttonArr:nil subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:nil];
    }
    self.frame = bgView.frame;
    [self addSubview:bgView];
}

- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage titleDesc:(NSString *)titleDesc buttonNameArray:(NSArray *)nameArray clickBlock:(void (^)(NSInteger))block {
    UIView *bgView = [self alertWithHeaderImgView:headerImage title:title desc:titleDesc subDesc:nil kCardImg:nil buttonArr:nameArray subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
        if (block) {
            block(buttonIndex);
        }
    }];
    self.frame = bgView.frame;
    [self addSubview:bgView];
}

- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc {
    UIView *bgView = [self alertWithHeaderImgView:headerImage title:title desc:nil subDesc:desc kCardImg:nil buttonArr:nil subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:nil];
    self.frame = bgView.frame;
    [self addSubview:bgView];
}

- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void (^)(NSInteger))block {
    UIView *bgView = [self alertWithHeaderImgView:headerImage title:title desc:nil subDesc:desc kCardImg:nil buttonArr:nameArray subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
        if (block) {
            block(buttonIndex);
        }
    }];
    self.frame = bgView.frame;
    [self addSubview:bgView];
}

- (void)alertListWithTitle:(NSString *)title cardModelArray:(NSArray *)cardModelArray totalintegral:(NSInteger)totalIntegral buttonblock:(void (^)(NSInteger))block {
    PDIntegralBackgroundView *bgView = [[PDIntegralBackgroundView alloc] init];
    [bgView customBackgroundViewWithTitle:title modelArray:cardModelArray totalIntegral:totalIntegral buttonArray:@[@"确定",@"取消"] buttonClick:block];
    self.frame = bgView.frame;
    [self addSubview:bgView];
}

- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage changeRMB:(NSInteger)rmb netInfo:(PDNetModel *)netModel desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void (^)(NSInteger))block {
    UIView *bgView = [self customViewWithTitle:title headerImage:headerImage changeRMB:rmb netInfo:netModel desc:desc buttonNameArray:nameArray clickBlock:^(NSInteger buttonIndex) {
        if (block) {
            block(buttonIndex);
        }
    }];
    self.frame = bgView.frame;
    [self addSubview:bgView];
}

#pragma mark 创建自定义View
-(UIView *)alertWithHeaderImgView:(UIImage *)errImg title:(NSString *)title desc:(NSString *)desc subDesc:(NSString *)subDesc kCardImg:(UIImage *)kCardImg buttonArr:(NSArray *)buttonArr subIconImg:(UIImage *)subIconImg challengeEnd:(challengeEnd)challengeEndType buttonTitleArrbuttonClick:(void (^)(NSInteger buttonIndex))block{
    
    // 1.背景
    UIImageView *bgView = [[UIImageView alloc] initWithImage:errImg];
    bgView.backgroundColor = [UIColor clearColor];
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(50), 330);
    bgView.layer.cornerRadius = 5;
    
    // 2. 创建胜利图片
//    UIImageView *iconImageView = [[UIImageView alloc]init];
//    iconImageView.backgroundColor = [UIColor clearColor];
//    iconImageView.image = errImg;
//    iconImageView.frame = CGRectMake((bgView.size_width - LCFloat(50)) / 2., LCFloat(15), LCFloat(50), LCFloat(50));
//    [bgView addSubview:iconImageView];
    CGFloat spaceHeight = LCFloat(80);
    
    // 3. 创建提示
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    titleLabel.frame = CGRectMake(0, spaceHeight + LCFloat(15), bgView.size_width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [bgView addSubview:titleLabel];
    
    // 4. 创建desc
    UILabel *descLabel = [[UILabel alloc]init];
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.font = [UIFont systemFontOfCustomeSize:14.];
    descLabel.numberOfLines = 0;
    descLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.text = desc;
    CGSize contentOfDescSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(bgView.size_width - 2 * LCFloat(11), CGFLOAT_MAX)];
    descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(titleLabel.frame) + LCFloat(20), bgView.size_width - 2 * LCFloat(11), contentOfDescSize.height);
    [bgView addSubview:descLabel];
    
    // 4. 创建钱袋Image
    PDImageView *moneyImageView = [[PDImageView alloc]init];
    moneyImageView.backgroundColor = [UIColor clearColor];
    moneyImageView.image = subIconImg;
    [bgView addSubview:moneyImageView];
    
    // 6. 进行计算
    CGFloat iconWidth = LCFloat(40);
    if (contentOfDescSize.height == [NSString contentofHeightWithFont:descLabel.font]){ // 一行
        CGSize contentOfDescLabelSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:descLabel.font])];
        if (subIconImg){
            CGFloat origin_X = (bgView.size_width - contentOfDescLabelSize.width - LCFloat(10) - iconWidth) / 2.;
            descLabel.frame = CGRectMake(origin_X, CGRectGetMaxY(titleLabel.frame) + LCFloat(25), contentOfDescLabelSize.width, [NSString contentofHeightWithFont:descLabel.font]);
            moneyImageView.frame = CGRectMake(CGRectGetMaxX(descLabel.frame) + LCFloat(10), CGRectGetMaxY(descLabel.frame) - iconWidth, iconWidth, iconWidth);
        }
    } else {                   // 多行
        
    }
    
    
    // 5. 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(descLabel.frame) + LCFloat(10), bgView.size_width - 2 * LCFloat(11), .5f);
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [bgView addSubview:lineView];
    if (!desc.length){
        lineView.orgin_y = CGRectGetMaxY(titleLabel.frame) + LCFloat(10);
    }
    
    // 6. 创建subDesc
    UILabel *subDescLabel = [[UILabel alloc]init];
    subDescLabel.backgroundColor = [UIColor clearColor];
    subDescLabel.font = [UIFont systemFontOfCustomeSize:13.];
    subDescLabel.numberOfLines = 0;
    subDescLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    subDescLabel.textAlignment = NSTextAlignmentLeft;
    subDescLabel.text = subDesc;
    CGSize subDescSize = [subDescLabel.text sizeWithCalcFont:subDescLabel.font constrainedToSize:CGSizeMake(descLabel.size_width, CGFLOAT_MAX)];
    subDescLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(lineView.frame) + LCFloat(10), bgView.size_width - 2 * LCFloat(11), subDescSize.height);
    [bgView addSubview:subDescLabel];
    if (!desc.length){
        subDescLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    // 6. 创建k卡
    UIImageView *kCardImageView = [[UIImageView alloc]init];
    kCardImageView.backgroundColor = [UIColor clearColor];
    kCardImageView.image = kCardImg;
    kCardImageView.frame = CGRectMake((bgView.size_width - kCardImg.size.width) / 2., CGRectGetMaxY(lineView.frame) + LCFloat(10), kCardImg.size.width, kCardImg.size.height);
    [bgView addSubview:kCardImageView];
    
    // 7. 创建按钮
    for (int i = 0 ; i < buttonArr.count;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:[buttonArr objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        
        CGFloat origin_x = LCFloat(20);
        CGFloat origin_y = CGRectGetMaxY(lineView.frame) + LCFloat(10) + (LCFloat(35) + LCFloat(10)) * i;
        button.frame = CGRectMake(origin_x, origin_y, bgView.size_width - 2 * LCFloat(20), LCFloat(35));
        [bgView addSubview:button];
        
        [button buttonWithBlock:^(UIButton *button) {
            if (block){
                block(i);
            }
        }];
    }
    
    CGFloat bgViewHeight = 0;
    bgViewHeight += CGRectGetMaxY(lineView.frame);
    
    // 1. 提示状态《按钮》
    if (subDesc.length){            // 判断是否有底部的文字
        subDescLabel.hidden = NO;
        kCardImageView.hidden = YES;
        bgViewHeight += LCFloat(10);
        bgViewHeight += subDescSize.height;
        bgViewHeight += LCFloat(10);
    } else if (kCardImg){
        subDescLabel.hidden = YES;
        kCardImageView.hidden = NO;
        bgViewHeight += LCFloat(10);
        bgViewHeight += kCardImg.size.height;
        bgViewHeight += LCFloat(10);
    } else if (buttonArr.count){
        subDescLabel.hidden = YES;
        kCardImageView.hidden = YES;
        bgViewHeight += LCFloat(10) + (LCFloat(35) + LCFloat(10)) * buttonArr.count;
    } else {
        subDescLabel.hidden = YES;
        kCardImageView.hidden = YES;
        lineView.hidden = YES;
    }
    bgView.size_height = bgViewHeight;
    return bgView;
}

#pragma mark - 兑换网费的alertView
- (UIView *)customViewWithTitle:(NSString *)title headerImage:(UIImage *)headerImage changeRMB:(NSInteger)rmb netInfo:(PDNetModel *)netModel desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void (^)(NSInteger buttonIndex))block {
    // 1.背景
    UIImageView *bgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_challenge_report_success"]];
    bgView.frame = CGRectMake(0, 0, LCFloat(280) + LCFloat(15) * 2, 2+LCFloat(50)+HSFloat(430));
    bgView.backgroundColor = [UIColor clearColor];
    bgView.layer.cornerRadius = 5;
    [self addSubview:bgView];
    
    // 2. 创建胜利图片
//    UIImageView *iconImageView = [[UIImageView alloc]init];
//    iconImageView.backgroundColor = [UIColor clearColor];
//    iconImageView.image = headerImage;
//    iconImageView.frame = CGRectMake((bgView.size_width - LCFloat(50)) / 2., HSFloat(15), LCFloat(50), LCFloat(50));
//    [bgView addSubview:iconImageView];
    CGFloat spaceHeight = LCFloat(80);
    
    // 3. 创建提示
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    titleLabel.frame = CGRectMake(0, spaceHeight + HSFloat(15), bgView.size_width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.textColor = [UIColor colorWithCustomerName:@"绿"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [bgView addSubview:titleLabel];
    
    CGFloat line_x = LCFloat(15);
    CGFloat line_width = (CGRectGetWidth(bgView.frame) - line_x*2);
    // 上分割线
    UIImageView *topLine = [[UIImageView alloc] init];
    topLine.frame = CGRectMake(line_x, CGRectGetMaxY(titleLabel.frame)+HSFloat(15), line_width, 1);
    topLine.image = [UIImage imageNamed:@"icon_integral_line"];
    [bgView addSubview:topLine];
    
    // 兑换信息
    PDExchangeInfoView *exchangeInfoView = [[PDExchangeInfoView alloc] initWithFrame:CGRectMake((bgView.size_width - LCFloat(280))/2, CGRectGetMaxY(topLine.frame) + HSFloat(20), LCFloat(280), HSFloat(140))];
    exchangeInfoView.moneyLabel.text = [NSString stringWithFormat:@"%ld",(long)rmb];
    [exchangeInfoView.headerImageView uploadImageWithURL:netModel.avatar placeholder:[UIImage imageNamed:@"icon_net_header"] callback:nil];
    exchangeInfoView.nameLabel.text = netModel.shop_name;
    exchangeInfoView.addressLabel.text = netModel.address;
    exchangeInfoView.phoneLabel.text = netModel.mobile;
    exchangeInfoView.numLabel.text = [NSString stringWithFormat:@"NO.%@",netModel.netId];
    [bgView addSubview:exchangeInfoView];
    
    // line
    UIImageView *lineView = [[UIImageView alloc] init];
    lineView.frame = CGRectMake(line_x, CGRectGetMaxY(exchangeInfoView.frame) + HSFloat(20), line_width, 1);
    lineView.image = [UIImage imageNamed:@"icon_integral_line"];
    [bgView addSubview:lineView];
    
    // 提示框
    UILabel *msgLabel = [[UILabel alloc] init];
    msgLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
    msgLabel.numberOfLines = 0;
    CGFloat msgLabel_x = 5;
    msgLabel.frame = CGRectMake(msgLabel_x, CGRectGetMaxY(lineView.frame) + HSFloat(15), CGRectGetWidth(bgView.frame) - msgLabel_x * 2, [NSString contentofHeightWithFont:msgLabel.font]*2);
    msgLabel.text = desc;

    NSMutableAttributedString *atr1 = [[NSMutableAttributedString alloc] initWithString:@"提示："attributes:@{NSForegroundColorAttributeName:[UIColor blackColor]}];
    NSMutableAttributedString *atr2 = [[NSMutableAttributedString alloc] initWithString:[desc substringFromIndex:3] attributes:@{NSForegroundColorAttributeName:[UIColor redColor]}];
    [atr1 appendAttributedString:atr2];
    msgLabel.attributedText = atr1;
    
    msgLabel.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:msgLabel];
    
    
    // buttons
    for (int i = 0 ; i <nameArray.count;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:[nameArray objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        button.titleLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        
        CGFloat button_width = LCFloat(125);
        CGFloat button_x = LCFloat(20) + i * (button_width + (CGRectGetWidth(bgView.frame) - button_width*2 - LCFloat(20)*2));
        button.frame = CGRectMake(button_x, CGRectGetMaxY(msgLabel.frame) + HSFloat(25), button_width, HSFloat(30));
        [button buttonWithBlock:^(UIButton *button) {
            if (block) {
                block(i);
            }
        }];
        [bgView addSubview:button];
    }
    
    return bgView;
}

@end
