//
//  PDAlertView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "JCAlertView.h"
#import "JCAlertView.h"                             // alert
#import "PDChallengerCardEnum.h"                    // k 卡枚举
#import "PDIntegralBackgroundView.h"
#import "PDNetModel.h"
#import "PDExchangeInfoView.h"

typedef NS_ENUM(NSInteger,alertType) {
    alertTypeNoHome,                        /**< 比赛没有房间*/
    alertTypeNoChallenger,                  /**< 进行匹配没有配对的人*/
    alertTypePrompt,                        /**< 提示*/
    alertTypeNoData,                        /**< 比赛记录获取失败*/
    alertTypeNotReadyTo,                    /**< 匹配失败，由于对手未能准备*/
    alertTypeNotReadyMe,                    /**< 匹配失败，由于自己未能准备*/
    alertTypeNotReadyALL,                   /**< 匹配失败，双方都没有准备*/
    alertTypeNoDataWithComputer,            /**< 比赛记录获取失败*/
    alertTypeWatingGameEnd,                 /**< 人工判定，等待处理结果*/
    alertTypeNoDataWithCustomer,            /**< 人工数据获取失败*/
    alertTypeCustomerAgain,                 /**< 比赛击落再次失败*/
    alertTypeNoKCard,                       /**< 没有k卡*/
    alertTypeRewardFail,                     /**< 领赏失败 */
    alertTypeRewardSuccess,                 /**< 领赏成功*/
    alertTypeGameNotEnd,                    /**< 比赛没有结束*/
    alertTypeGameRevoke,                    /**< 比赛作废*/
    alertTypeGameCustomerFailEnd,           /**< 游戏客服结算失败*/
    alertTypeValideData,                    /**< 游戏验证失败*/
    alertTypeGameCancelToFail,              /**<*/
};

typedef NS_ENUM(NSInteger,challengeEnd) {           // 比赛结果
    challengeEndVictory,                /**< 胜利*/
    challengeEndFail,                   /**< 失败*/
    challengeEndNormal,                 /**< 其他*/
};

typedef NS_ENUM(NSInteger,loginAlertType) {
    alertReSetPasswordSuccess,          /**< 修改密码成功*/
    alertRegisterSuccess,               /**< 注册成功*/
};

@interface PDAlertView : JCAlertView

// 报错误提示
-(void)alertWithType:(alertType)type block:(void (^)(NSInteger buttonIndex))block;

// 比赛结果获取方式
-(void)alertWithChallengeEndType:(challengeEnd)endType integral:(NSInteger)integral buttonArr:(NSArray *)btnArr block:(void (^)(NSInteger buttonIndex))block;

// 弹出2层的
-(void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc;
- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void(^)(NSInteger index))block;
// 兑换积分结果
+ (PDIntegralBackgroundView *)alertListWithTitle:(NSString *)title cardModelArray:(NSArray *)cardModelArray totalintegral:(NSInteger)totalIntegral buttonblock:(void (^)(NSInteger buttonIndex))block;
// 登录注册提示
- (void)alterWithLoginType:(loginAlertType)loginType completion:(void(^)())completion;

// 兑换人名币成功结果
- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage changeRMB:(NSInteger)rmb netInfo:(PDNetModel *)netModel desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void (^)(NSInteger buttonIndex))block;
@end
