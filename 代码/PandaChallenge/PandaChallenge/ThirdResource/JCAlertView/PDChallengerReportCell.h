//
//  PDChallengerReportCell.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDChallengerReportSingleModel.h"

@interface PDChallengerReportCell : UITableViewCell{
    BOOL isChecked;             /**< 判断是否选中*/
}

@property (nonatomic,strong)PDChallengerReportSingleModel *transferReportSingleModel;
@property (nonatomic,assign)CGFloat transferCellHeight;


-(void)setChecked:(BOOL)checked;

+(CGFloat)calculationCellHeihgtWithText:(NSString *)text;
@end
