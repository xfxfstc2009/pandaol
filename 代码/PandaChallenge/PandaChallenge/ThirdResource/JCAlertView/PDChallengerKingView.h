//
//  PDChallengerKingView.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDChallengerKingView : UIView

-(instancetype)initWithFrame:(CGRect)frame lingshangBlock:(void(^)())lingshangBlock myChallengerBlock:(void(^)())challengeBlock;

@end
