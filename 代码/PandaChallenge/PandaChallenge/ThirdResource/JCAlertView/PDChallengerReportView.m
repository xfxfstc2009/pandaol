//
//  PDChallengerReportView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengerReportView.h"
#import "PDChallengerReportCell.h"
#import <objc/runtime.h>
#import "PDChallengerReportListModel.h"

@interface PDChallengerReportView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *mainTableView;
@property (nonatomic,strong)NSMutableArray *mainMutableArr;
@property (nonatomic,strong)UITextView *inputView;
@property (nonatomic,strong)NSMutableArray *selectedMutableArr;         /**< 选中的内容*/
@property (nonatomic,strong)UIButton *feedbackButton;
@property (nonatomic,strong)UIButton *backButton;

@end


static char successBlockKey;
static char failBlockKey;
static char confirmBlockKey;
@implementation PDChallengerReportView

-(instancetype)initWithFrame:(CGRect)frame successBlock:(void(^)())successBlock failBlock:(void(^)())failBlock{
    self = [super initWithFrame:frame];
    if (self){
        objc_setAssociatedObject(self, &successBlockKey, successBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
        objc_setAssociatedObject(self, &failBlockKey, failBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
        
        self.frame = CGRectMake(0, 0, LCFloat(283), LCFloat(300));
        [self showReportView];
    }
    return self;
}

#pragma mark 
-(void)showReportView{
    PDImageView *bgView = [[PDImageView alloc]init];
    bgView.image = [self stretchImageWithName:@"bg_challenge_report"];
    bgView.frame = self.bounds;
    bgView.userInteractionEnabled = YES;
    [self addSubview:bgView];
    
    // 1. 创建label
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.backgroundColor = [UIColor clearColor];
    fixedLabel.font = [UIFont systemFontOfCustomeSize:14.];
    fixedLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    fixedLabel.text = @"举报玩家选择其违规行为:";
    fixedLabel.textAlignment = NSTextAlignmentCenter;
    fixedLabel.numberOfLines = 1;
    fixedLabel.frame = CGRectMake(0, LCFloat(75), bgView.frame.size.width, [NSString contentofHeightWithFont:fixedLabel.font]);
    [bgView addSubview:fixedLabel];
    
    // 2. 创建线条
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(fixedLabel.frame) + LCFloat(11), bgView.size_width - 2 * LCFloat(11), .5f);
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [bgView addSubview:lineView];
    
    // 3. 创建按钮
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.backButton setTitle:@"取消" forState:UIControlStateNormal];
    self.backButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.backButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.backButton.layer.cornerRadius = LCFloat(5);
    self.backButton.frame = CGRectMake(lineView.orgin_x, bgView.size_height - LCFloat(15) - LCFloat(35), (lineView.size_width - LCFloat(15)) / 2., LCFloat(35));
    __weak typeof(self)weakSelf = self;
    [self.backButton buttonWithBlock:^(UIButton *button) {                 // 执行接口
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^failBlock)() = objc_getAssociatedObject(strongSelf, &failBlockKey);
        if (failBlock){
            failBlock();
        }
    }];
    [bgView addSubview: self.backButton];
    
    // 3. 创建按钮
    self.feedbackButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.feedbackButton setTitle:@"提交" forState:UIControlStateNormal];
    self.feedbackButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.feedbackButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.feedbackButton.layer.cornerRadius = LCFloat(5);
    self.feedbackButton.frame = CGRectMake(CGRectGetMaxX(self.backButton.frame) + LCFloat(15), bgView.size_height - LCFloat(15) - LCFloat(35), self.backButton.size_width, LCFloat(35));
    [self.feedbackButton buttonWithBlock:^(UIButton *button) {                 // 执行接口
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf yanzhengSendRequest];
    }];
    [bgView addSubview: self.feedbackButton];
    
    // 4. 创建line2
    UIView *lineView2 = [[UIView alloc]init];
    lineView2.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView2.frame = CGRectMake(lineView.orgin_x, self.feedbackButton.orgin_y - LCFloat(15), lineView.size_width, lineView.size_height);
    [bgView addSubview:lineView2];
    
    [self arrayWithInit];
    // 2. 创建tableView
    if (!self.mainTableView){
        self.mainTableView = [[UITableView alloc] initWithFrame:bgView.bounds style:UITableViewStylePlain];
        self.mainTableView.orgin_y = CGRectGetMaxY(lineView.frame);
        self.mainTableView.size_height = lineView2.orgin_y - lineView.orgin_y - .5f;
        self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.mainTableView.delegate = self;
        self.mainTableView.dataSource = self;
        self.mainTableView.showsHorizontalScrollIndicator = NO;
        self.mainTableView.showsVerticalScrollIndicator = NO;
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.mainTableView.allowsMultipleSelection = YES;
        self.mainTableView.allowsSelectionDuringEditing = YES;
        self.mainTableView.backgroundColor = [UIColor clearColor];
        [self.mainTableView reloadData];
        [bgView addSubview:self.mainTableView];
    }
    [weakSelf sendRequestToGetReportList];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.selectedMutableArr = [NSMutableArray array];
    self.mainMutableArr = [NSMutableArray array];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.mainMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.mainMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入"] && indexPath.row == [self cellIndexPathRowWithcellData:@"输入"]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            
            self.inputView = [[UITextView alloc]init];
            self.inputView.frame = CGRectMake(LCFloat(11), LCFloat(5), tableView.size_width - 2 * LCFloat(11), LCFloat(80));
            self.inputView.placeholder = @"请输入举报内容";
            self.inputView.layer.cornerRadius = LCFloat(3);
            self.inputView.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
            self.inputView.layer.borderWidth = 1;
            self.inputView.limitMax = 200;
            [cellWithRowOne addSubview:self.inputView];
        }
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PDChallengerReportCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[PDChallengerReportCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            
            //
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        
        PDChallengerReportSingleModel *singleModel = [[self.mainMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        cellWithRowThr.transferReportSingleModel = singleModel;
        
        // 是 isselectArray 里面的，就显示yes else 显示no
        if([self.selectedMutableArr containsObject:singleModel]){
            [cellWithRowThr setChecked:YES];
        } else {
            [cellWithRowThr setChecked:NO];
        }
        
        return cellWithRowThr;
    }
}



#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入"] && indexPath.row == [self cellIndexPathRowWithcellData:@"输入"]){
        if (self.inputView){
            [self.inputView becomeFirstResponder];
        }
    } else {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"其他"] && indexPath.row == [self cellIndexPathRowWithcellData:@"其他"]){
            [self insertInfo];
        } else {
            [self deleteInfo];
        }
        [self.selectedMutableArr removeAllObjects];
        PDChallengerReportSingleModel *singleModel = [[self.mainMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

        [self.selectedMutableArr addObject:singleModel];
        [self.mainTableView reloadData];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"输入"] && indexPath.row == [self cellIndexPathRowWithcellData:@"输入"]){
        return LCFloat(90);
    } else {
        return [PDChallengerReportCell calculationCellHeihgtWithText:@"1"];
    }
}

#pragma mark - otherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.mainMutableArr.count ; i++){
        NSArray *dataTempArr = [self.mainMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[PDChallengerReportSingleModel class]]){
                PDChallengerReportSingleModel *singleModel = (PDChallengerReportSingleModel *)item;
                NSString *itemString = singleModel.content;
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            } else if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

#pragma mark 返回indexPath
-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.mainMutableArr.count ; i++){
        NSArray *dataTempArr = [self.mainMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[PDChallengerReportSingleModel class]]){
                PDChallengerReportSingleModel *singleModel = (PDChallengerReportSingleModel *)item;
                NSString *itemString = singleModel.content;
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            } else if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}

-(void)insertInfo{
    if ([self cellIndexPathRowWithcellData:@"输入"] == -1){       // 不存在
        if ([self cellIndexPathSectionWithcellData:@"其他"] != -1){           //1.找到其他的位置
            NSInteger otherIndexSection = [self cellIndexPathSectionWithcellData:@"其他"];
            NSInteger otherIndexRow = [self cellIndexPathRowWithcellData:@"其他"];
            // 2. 增加输入
            NSMutableArray *tempMutableArr = [self.mainMutableArr objectAtIndex:otherIndexSection];
            [tempMutableArr insertObject:@"输入" atIndex:otherIndexRow + 1];
            NSIndexPath *insertIndexPath = [NSIndexPath indexPathForRow:otherIndexRow + 1 inSection:otherIndexSection];
            [self.mainTableView insertRowsAtIndexPaths:@[insertIndexPath] withRowAnimation:UITableViewRowAnimationNone];
            
            // 滚动到输入框
            NSInteger inputSec = [self cellIndexPathSectionWithcellData:@"输入"];
            NSInteger inputRow = [self cellIndexPathRowWithcellData:@"输入"];
            [self.mainTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:inputRow inSection:inputSec] atScrollPosition:UITableViewScrollPositionTop animated:YES];
        }
    }
}

-(void)deleteInfo{
    if ([self cellIndexPathRowWithcellData:@"输入"] != -1){       // 存在        // 删除
        NSInteger inputSec = [self cellIndexPathSectionWithcellData:@"输入"];
        NSInteger inputRow = [self cellIndexPathRowWithcellData:@"输入"];
        NSMutableArray *tempMutableArr = [self.mainMutableArr objectAtIndex:inputSec];
        if ([tempMutableArr containsObject:@"输入"]){
            [tempMutableArr removeObject:@"输入"];
            [self.mainTableView deleteRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:inputRow inSection:inputSec]] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

#pragma mark - 拉伸图片
- (UIImage *)stretchImageWithName:(NSString *)name {
    UIImage *image = [UIImage imageNamed:name];
    return [image stretchableImageWithLeftCapWidth:0 topCapHeight:image.size.height * .8f];
}


#pragma mark - Interface
-(void)sendRequestToGetReportList{
    PDChallengerReportListModel *reportListModel = [[PDChallengerReportListModel alloc]init];
    __weak typeof(self)weakSelf = self;
    [reportListModel fetchWithPath:kChallengerReport completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            NSMutableArray *mainArr = [NSMutableArray array];
            [mainArr addObjectsFromArray:reportListModel.reportList];
            [strongSelf.mainMutableArr addObject:mainArr];
            [strongSelf.mainTableView reloadData];
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

#pragma 提交
-(void)sendRequestToReportSubmitWithId:(NSString *)reportId content:(NSString *)content{
    PDFetchModel *fetchModel = [[PDFetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    if (reportId.length){
        [dic setObject:reportId forKey:@"report_id"];
    }
    if (content.length){
        [dic setObject:content forKey:@"content"];
    }
    
    fetchModel.requestParams = dic;
    [fetchModel fetchWithPath:kChallengerReportSubmit completionHandler:^(BOOL isSucceeded, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            void(^successBlock)() = objc_getAssociatedObject(strongSelf, &successBlockKey);
            if (successBlock){
                successBlock();
            }
        } else {
            [[UIAlertView alertViewWithTitle:@"错误" message:error.localizedDescription buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}

#pragma mark 验证提交
-(void)yanzhengSendRequest{
    __weak typeof(self)weakSelf = self;
    
    if (self.selectedMutableArr.count){
        PDChallengerReportSingleModel *singleModel = [self.selectedMutableArr lastObject];
        if ([singleModel.content isEqualToString:@"其他"]){
            if (!self.inputView.text.length){
                [[UIAlertView alertViewWithTitle:@"错误" message:@"请输入举报内容" buttonTitles:@[@"确定"] callBlock:NULL]show];
            } else {
                [weakSelf sendRequestToReportSubmitWithId:singleModel.reportId content:weakSelf.inputView.text];
            }
        } else {
            [weakSelf sendRequestToReportSubmitWithId:singleModel.reportId content:nil];
        }
    } else {
        [[UIAlertView alertViewWithTitle:@"错误" message:@"请选择举报内容" buttonTitles:@[@"确定"] callBlock:NULL]show];
    }
}

#pragma mark 举报成功方法
-(instancetype)initWithFrame:(CGRect)frame reportSuccessBlock:(void(^)())confirmBlock{
    self = [super initWithFrame:frame];
    if (self){
        objc_setAssociatedObject(self, &confirmBlockKey, confirmBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [self createSuccessView];
    }
    return self;
}

#pragma mark - createSuccessView
-(void)createSuccessView{
    PDImageView *bgView = [[PDImageView alloc]init];
    bgView.image = [self stretchImageWithName:@"bg_challenge_report_success"];
    bgView.frame = self.bounds;
    bgView.userInteractionEnabled = YES;
    [self addSubview:bgView];
    
    // 1. 创建label
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.backgroundColor = [UIColor clearColor];
    fixedLabel.font = [UIFont systemFontOfCustomeSize:14.];
    fixedLabel.textColor = [UIColor colorWithCustomerName:@"绿"];
    fixedLabel.text = @"举报成功";
    fixedLabel.textAlignment = NSTextAlignmentCenter;
    fixedLabel.numberOfLines = 1;
    fixedLabel.frame = CGRectMake(0, LCFloat(75), bgView.frame.size.width, [NSString contentofHeightWithFont:fixedLabel.font]);
    [bgView addSubview:fixedLabel];
    
    // 2. 创建线条
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(fixedLabel.frame) + LCFloat(11), bgView.size_width - 2 * LCFloat(11), .5f);
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [bgView addSubview:lineView];
    
    // 3. 创建dymicLabel
    UILabel *dymicLabel = [[UILabel alloc]init];
    dymicLabel.backgroundColor = [UIColor clearColor];
    dymicLabel.font = [UIFont systemFontOfCustomeSize:14.];
    dymicLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    dymicLabel.text = @"客服人员将会跟进，请耐心等待";
    dymicLabel.textAlignment = NSTextAlignmentCenter;
    dymicLabel.numberOfLines = 1;
    dymicLabel.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame) + LCFloat(20), bgView.frame.size.width, [NSString contentofHeightWithFont:dymicLabel.font]);
    [bgView addSubview:dymicLabel];
    
    // 4. 创建line
    UIView *lineView2 = [[UIView alloc]init];
    lineView2.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView2.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(dymicLabel.frame) + LCFloat(20), bgView.size_width - 2 * LCFloat(11), .5f);
    [bgView addSubview:lineView2];
    
    // 3. 创建按钮
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [confirmButton setTitle:@"确定" forState:UIControlStateNormal];
    confirmButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    confirmButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    confirmButton.layer.cornerRadius = LCFloat(5);
    confirmButton.frame = CGRectMake((bgView.size_width - LCFloat(150)) / 2., CGRectGetMaxY(lineView2.frame) + LCFloat(20), LCFloat(150), LCFloat(30));
    __weak typeof(self)weakSelf = self;
    [confirmButton buttonWithBlock:^(UIButton *button) {                 // 执行接口
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^confirmBlock)() = objc_getAssociatedObject(strongSelf, &confirmBlockKey);
        if (confirmBlock){
            confirmBlock();
        }
    }];
    [bgView addSubview: confirmButton];
    bgView.size_height = CGRectGetMaxY(confirmButton.frame) + LCFloat(20);
    
}

@end
