//
//  PDChallengerAlertViewManager.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengerAlertViewManager.h"

@implementation PDChallengerAlertViewManager

-(UIView *)alertWithTime:(NSInteger)timeInteger{
    PDImageView *numberImageView = [[PDImageView alloc]init];
    numberImageView.backgroundColor = [UIColor clearColor];
    numberImageView.frame = CGRectMake(0, 0, LCFloat(229), LCFloat(151));
    numberImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_alert_download_%li",(long)timeInteger]];
    return numberImageView;
}


#pragma mark - 【1】主方法
-(UIView *)alertWithType:(alertType)type block:(void (^)(NSInteger buttonIndex))block{
    UIView *bgView;
    if (type == alertTypeNoChallenger){     // 1. 没有匹配到对手
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"icon_alert_fail"]  title:@"提示" desc:@"匹配失败，未能找到符合条件的对手" subDesc:nil kCardImg:nil buttonArr:@[@"继续寻找对手",@"更换出征条件"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            if (block){
                block(buttonIndex);
            }
        }];
    } else if (type == alertTypeNoHome){   // 2. 比赛没有房间
      bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"icon_alert_fail"] title:@"提示" desc:nil subDesc:@"当前对战比较多，请稍后再试。" kCardImg:nil buttonArr:nil subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:NULL];
    } else if (type == alertTypeNotReadyTo){    // 3. 对手没有准备
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"icon_alert_fail"] title:@"提示" desc:@"匹配失败，由于您的对手未能准备" subDesc:nil kCardImg:nil buttonArr:@[@"重新寻找对手",@"返回首页"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            if (block){
                block(buttonIndex);
            }
        }];
    } else if (type == alertTypeNotReadyMe){   // 4. 我没有准备
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"icon_alert_fail"] title:@"提示" desc:@"匹配失败，您未能确认战斗" subDesc:nil kCardImg:nil buttonArr:@[@"继续寻找对手",@"更换出征条件"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            if(block){
                block(buttonIndex);
            }
        }];
    } else if (type == alertTypeNoKCard){       // 5. 我没有k卡
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"icon_alert_fail"] title:@"无法出征" desc:@"您尚未获得赛事资格" subDesc:nil kCardImg:nil buttonArr:@[@"获取赛事资格",@"免费补偿"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            if(block){
                block(buttonIndex);
            }
        }];
    }
    
    
    
    
//    else if (type == alertTypeNoData){                       // 1.【比赛记录获取失败】
//        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"]  title:@"比赛记录获取失败" desc:nil subDesc:@"比赛将由人工判定，请耐心等待处理结果" kCardImg:nil buttonArr:@[@"再次结算",@"申请客服"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
//            if (block){
//                block(buttonIndex);
//            }
//        }];
//    } else if (type == alertTypeNoDataWithCustomer){    // 3.【客服校验失败】
//        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"] title:@"比赛记录获取失败" desc:@"TGP 无法查询到本场战役的数据，您将可以继续征战" subDesc:nil kCardImg:nil buttonArr:@[@"确定"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
//            if (block){
//                block(buttonIndex);
//            }
//        }];
//    } else if (type == alertTypeCustomerAgain){         // 【客服再次校验失败】
//        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"] title:@"比赛记录获取失败" desc:nil subDesc:@"比赛将由人工判定，请耐心等待处理结果" kCardImg:nil buttonArr:nil subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:NULL];
//    } else if (type == alertTypeNotReady){              // 【没有准备好】
//        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"] title:@"提示" desc:@"匹配失败，由于您的对手未准备" subDesc:nil kCardImg:nil buttonArr:@[@"重新寻找对手",@"返回首页"] subIconImg:nil challengeEnd:challengeEndFail buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
//            if (block){
//                block(buttonIndex);
//            }
//        }];
//    } else if(type == alertTypeNotReadyWithMe){
//        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"] title:@"提示" desc:@"匹配失败，您未能确认战斗" subDesc:nil kCardImg:nil buttonArr:@[@"继续寻找对手",@"更换出征条件"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
//            if (block){
//                block(buttonIndex);
//            }
//        }];
//    }
    return bgView;
}



#pragma mark 比赛结果获取方式
-(UIView *)alertWithChallengeEndType:(challengeEnd)endType currentKCard:(kCardType)kCard integral:(NSInteger)integral buttonArr:(NSArray *)btnArr block:(void (^)(NSInteger buttonIndex))block{
    UIView *bgView;
    if (endType == challengeEndVictory){            // 比赛成功
        bgView = [self alertWithChallengerImgView:[UIImage imageNamed:@"icon_alert_success"] challengeEndType:challengeEndVictory currentKCard:kCard title:@"胜利" desc:nil buttonArr:btnArr buttonClick:block];
    } else {                                        // 比赛失败
        bgView = [self alertWithChallengerImgView:[UIImage imageNamed:@"icon_alert_fail"] challengeEndType:challengeEndFail currentKCard:kCard title:@"失败" desc:nil buttonArr:btnArr buttonClick:block];
    }
    return bgView;
}


#pragma mark - 创建自定义战斗成功失败view
-(UIView *)alertWithChallengerImgView:(UIImage *)errImg challengeEndType:(challengeEnd)endType currentKCard:(kCardType)kCard title:(NSString *)title desc:(NSString *)desc buttonArr:(NSArray *)buttonArr buttonClick:(void(^)(NSInteger buttonIndex))block{
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.clipsToBounds = YES;
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(50), 300);
    bgView.layer.cornerRadius = 5;
    
    // 2. 创建胜利图片
    UIImageView *iconImageView = [[UIImageView alloc]init];
    iconImageView.backgroundColor = [UIColor clearColor];
    iconImageView.image = errImg;
    iconImageView.frame = CGRectMake((bgView.size_width - LCFloat(50)) / 2., LCFloat(27), LCFloat(53), LCFloat(53));
    [bgView addSubview:iconImageView];
    
    // 3. 创建提示
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    titleLabel.frame = CGRectMake(0, CGRectGetMaxY(iconImageView.frame) + LCFloat(15), bgView.size_width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [bgView addSubview:titleLabel];
    
    // 4. 创建desc
    UILabel *descLabel = [[UILabel alloc]init];
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.font = [UIFont systemFontOfCustomeSize:19.];
    descLabel.numberOfLines = 1;
    descLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.text = desc;
    CGSize contentOfDescSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:descLabel.font])];
    [bgView addSubview:descLabel];
    
    // 5. 创建imgView
    UIImageView *subIconImageView = [[UIImageView alloc]init];
    subIconImageView.backgroundColor = [UIColor clearColor];
    [bgView addSubview:subIconImageView];
    
    // 6. 进行计算
    CGFloat iconWidth = LCFloat(40);
    CGFloat origin_X = (bgView.size_width - contentOfDescSize.width - LCFloat(10) - iconWidth) / 2.;
    descLabel.frame = CGRectMake(origin_X, CGRectGetMaxY(titleLabel.frame) + LCFloat(25), contentOfDescSize.width, [NSString contentofHeightWithFont:descLabel.font]);
    subIconImageView.frame = CGRectMake(CGRectGetMaxX(descLabel.frame) + LCFloat(10), CGRectGetMaxY(descLabel.frame) - iconWidth, iconWidth, iconWidth);
    
    
    // 5. 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(descLabel.frame) + LCFloat(9), bgView.size_width - 2 * LCFloat(11), .5f);
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [bgView addSubview:lineView];
    if (!desc.length){
        lineView.orgin_y = CGRectGetMaxY(titleLabel.frame) + LCFloat(10);
    }
    
    
    // 6.1 计算bgView 高度
    CGFloat bgViewHeight = 0;
    bgViewHeight += CGRectGetMaxY(lineView.frame);
    bgViewHeight += LCFloat(28);
    bgViewHeight += (LCFloat(55/2.) + LCFloat(10)) * buttonArr.count;
    bgViewHeight += LCFloat(28);
    bgView.size_height = bgViewHeight;
    
    
    // 6. 创建scrollView
    UIScrollView *mainScrollView = [[UIScrollView alloc]init];
    mainScrollView.backgroundColor = [UIColor clearColor];
    mainScrollView.showsHorizontalScrollIndicator = NO;
    mainScrollView.showsVerticalScrollIndicator = NO;
    mainScrollView.pagingEnabled = YES;
    mainScrollView.bounces = NO;
    mainScrollView.scrollEnabled = NO;
    mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame), bgView.size_width, bgView.size_height - CGRectGetMaxY(lineView.frame));
    mainScrollView.contentSize = CGSizeMake(3 * mainScrollView.size_width, mainScrollView.size_height);
    [bgView addSubview:mainScrollView];
    
    // 1. 创建第一个view
    UIView *firstView = [[UIView alloc]init];
    firstView.backgroundColor = [UIColor clearColor];
    firstView.frame = CGRectMake(0, 0, bgView.size_width, bgView.size_height - CGRectGetMaxY(lineView.frame));
    [mainScrollView addSubview:firstView];
    
    // 1.1
    PDImageView *kCardImageViewWithV = [[PDImageView alloc]init];
    kCardImageViewWithV.backgroundColor = [UIColor clearColor];
    UIImage *kCardImg = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)kCard]];
    kCardImageViewWithV.image = kCardImg;
    CGFloat imgBili = kCardImg.size.height * 1.f / kCardImg.size.width;
    CGFloat imgWidth = (firstView.size_height - 2 * LCFloat(11)) / imgBili;
    kCardImageViewWithV.frame = CGRectMake((firstView.size_width - imgWidth) / 2., LCFloat(11), imgWidth, firstView.size_height - 2 * LCFloat(11));
    [firstView addSubview:kCardImageViewWithV];
    
    // 1. 创建第一个view
    UIView *twoView = [[UIView alloc]init];
    twoView.backgroundColor = [UIColor clearColor];
    twoView.frame = CGRectMake(bgView.size_width, firstView.orgin_y, firstView.size_width, firstView.size_height);
    [mainScrollView addSubview:twoView];
    
    // 1.1
    PDImageView *kCardImageViewWithV2 = [[PDImageView alloc]init];
    kCardImageViewWithV2.backgroundColor = [UIColor clearColor];
    UIImage *kCardImg2 = [UIImage imageNamed:[NSString stringWithFormat:@"challenge_kCard_%li_nor",(long)(kCard + 1)]];
    kCardImageViewWithV2.image = kCardImg2;
    kCardImageViewWithV2.frame = CGRectMake((firstView.size_width - imgWidth) / 2., LCFloat(11), imgWidth, firstView.size_height - 2 * LCFloat(11));
    [twoView addSubview:kCardImageViewWithV2];
    
    
    // 2. 创建第二个view
    UIView *thrView = [[UIView alloc]init];
    thrView.backgroundColor = [UIColor clearColor];
    thrView.frame = CGRectMake(2 * bgView.size_width, firstView.orgin_y, firstView.size_width, firstView.size_height);
    [mainScrollView addSubview:thrView];
    
    // 7. 创建按钮
    for (int i = 0 ; i < buttonArr.count;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:[buttonArr objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        
        CGFloat origin_x = LCFloat(44);
        CGFloat origin_y = LCFloat(28) + (LCFloat(55/2.) + LCFloat(10)) * i;
        button.frame = CGRectMake(origin_x, origin_y, bgView.size_width - 2 * LCFloat(44), LCFloat(55 / 2.));
        [thrView addSubview:button];
        
        [button buttonWithBlock:^(UIButton *button) {
            if (block){
                block(i);
            }
        }];
    }
    
    if (endType == challengeEndFail){
    
    } else if (endType == challengeEndVictory){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [mainScrollView setContentOffset:CGPointMake(bgView.size_width, 0) animated:YES];
        });
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [mainScrollView setContentOffset:CGPointMake( 2 * bgView.size_width, 0) animated:YES];
        });
   
    }

    return bgView;
}




#pragma mark - 创建自定义View
-(UIView *)alertWithHeaderImgView:(UIImage *)errImg title:(NSString *)title desc:(NSString *)desc subDesc:(NSString *)subDesc kCardImg:(UIImage *)kCardImg buttonArr:(NSArray *)buttonArr subIconImg:(UIImage *)subIconImg challengeEnd:(challengeEnd)challengeEndType buttonTitleArrbuttonClick:(void (^)(NSInteger buttonIndex))block{
    
    // 1.背景
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(50), 300);
    bgView.layer.cornerRadius = 5;
    
    // 2. 创建胜利图片
    UIImageView *iconImageView = [[UIImageView alloc]init];
    iconImageView.backgroundColor = [UIColor clearColor];
    iconImageView.image = errImg;
    iconImageView.frame = CGRectMake((bgView.size_width - LCFloat(50)) / 2., LCFloat(15), LCFloat(50), LCFloat(50));
    [bgView addSubview:iconImageView];
    
    // 3. 创建提示
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    titleLabel.frame = CGRectMake(0, CGRectGetMaxY(iconImageView.frame) + LCFloat(15), bgView.size_width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [bgView addSubview:titleLabel];
    
    // 4. 创建desc
    UILabel *descLabel = [[UILabel alloc]init];
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.font = [UIFont systemFontOfCustomeSize:14.];
    descLabel.numberOfLines = 0;
    descLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.text = desc;
    CGSize contentOfDescSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(bgView.size_width - 2 * LCFloat(11), CGFLOAT_MAX)];
    descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(titleLabel.frame) + LCFloat(20), bgView.size_width - 2 * LCFloat(11), contentOfDescSize.height);
    [bgView addSubview:descLabel];
    
    // 4. 创建钱袋Image
    PDImageView *moneyImageView = [[PDImageView alloc]init];
    moneyImageView.backgroundColor = [UIColor clearColor];
    moneyImageView.image = subIconImg;
    [bgView addSubview:moneyImageView];
    
    // 6. 进行计算
    CGFloat iconWidth = LCFloat(40);
    if (contentOfDescSize.height == [NSString contentofHeightWithFont:descLabel.font]){ // 一行
        CGSize contentOfDescLabelSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:descLabel.font])];
        if (subIconImg){
            CGFloat origin_X = (bgView.size_width - contentOfDescLabelSize.width - LCFloat(10) - iconWidth) / 2.;
            descLabel.frame = CGRectMake(origin_X, CGRectGetMaxY(titleLabel.frame) + LCFloat(25), contentOfDescLabelSize.width, [NSString contentofHeightWithFont:descLabel.font]);
            moneyImageView.frame = CGRectMake(CGRectGetMaxX(descLabel.frame) + LCFloat(10), CGRectGetMaxY(descLabel.frame) - iconWidth, iconWidth, iconWidth);
        }
    } else {                   // 多行
        
    }
    
    
    // 5. 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(descLabel.frame) + LCFloat(10), bgView.size_width - 2 * LCFloat(11), .5f);
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [bgView addSubview:lineView];
    if (!desc.length){
        lineView.orgin_y = CGRectGetMaxY(titleLabel.frame) + LCFloat(10);
    }
    
    // 6. 创建subDesc
    UILabel *subDescLabel = [[UILabel alloc]init];
    subDescLabel.backgroundColor = [UIColor clearColor];
    subDescLabel.font = [UIFont systemFontOfCustomeSize:13.];
    subDescLabel.numberOfLines = 0;
    subDescLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    subDescLabel.textAlignment = NSTextAlignmentLeft;
    subDescLabel.text = subDesc;
    CGSize subDescSize = [subDescLabel.text sizeWithCalcFont:subDescLabel.font constrainedToSize:CGSizeMake(descLabel.size_width, CGFLOAT_MAX)];
    subDescLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(lineView.frame) + LCFloat(10), bgView.size_width - 2 * LCFloat(11), subDescSize.height);
    [bgView addSubview:subDescLabel];
    if (!desc.length){
        subDescLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    // 6. 创建k卡
    UIImageView *kCardImageView = [[UIImageView alloc]init];
    kCardImageView.backgroundColor = [UIColor clearColor];
    kCardImageView.image = kCardImg;
    kCardImageView.frame = CGRectMake((bgView.size_width - kCardImg.size.width) / 2., CGRectGetMaxY(lineView.frame) + LCFloat(10), kCardImg.size.width, kCardImg.size.height);
    [bgView addSubview:kCardImageView];
    
    // 7. 创建按钮
    for (int i = 0 ; i < buttonArr.count;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:[buttonArr objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        
        CGFloat origin_x = LCFloat(20);
        CGFloat origin_y = CGRectGetMaxY(lineView.frame) + LCFloat(10) + (LCFloat(35) + LCFloat(10)) * i;
        button.frame = CGRectMake(origin_x, origin_y, bgView.size_width - 2 * LCFloat(20), LCFloat(35));
        [bgView addSubview:button];
        
        [button buttonWithBlock:^(UIButton *button) {
            if (block){
                block(i);
            }
        }];
    }
    
    CGFloat bgViewHeight = 0;
    bgViewHeight += CGRectGetMaxY(lineView.frame);
    
    // 1. 提示状态《按钮》
    if (subDesc.length){            // 判断是否有底部的文字
        subDescLabel.hidden = NO;
        kCardImageView.hidden = YES;
        bgViewHeight += LCFloat(10);
        bgViewHeight += subDescSize.height;
        bgViewHeight += LCFloat(10);
    } else if (kCardImg){
        subDescLabel.hidden = YES;
        kCardImageView.hidden = NO;
        bgViewHeight += LCFloat(10);
        bgViewHeight += kCardImg.size.height;
        bgViewHeight += LCFloat(10);
    } else if (buttonArr.count){
        subDescLabel.hidden = YES;
        kCardImageView.hidden = YES;
        bgViewHeight += LCFloat(10) + (LCFloat(35) + LCFloat(10)) * buttonArr.count;
    } else {
        subDescLabel.hidden = YES;
        kCardImageView.hidden = YES;
        lineView.hidden = YES;
    }
    bgView.size_height = bgViewHeight;
    return bgView;
}




@end
