//
//  PDAlertView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/3/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAlertView.h"
#import <objc/runtime.h>


@interface PDAlertView(){
    JCAlertView *alert;
}
@end

@implementation PDAlertView

#pragma mark - 【1】主方法
-(void)alertWithType:(alertType)type block:(void (^)(NSInteger buttonIndex))block{
    UIView *bgView;
    if (type == alertTypeNoData){                       // 1.【比赛记录获取失败】
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"]  title:@"比赛记录获取失败" desc:nil subDesc:@"比赛将由人工判定，请耐心等待处理结果" kCardImg:nil buttonArr:@[@"再次结算",@"申请客服"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            if (block){
                block(buttonIndex);
                [alert dismissWithCompletion:NULL];
            }
        }];
    } else if (type == alertTypeNoHome){                // 2.【比赛满房间】
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"] title:@"提示" desc:nil subDesc:@"当前对战比较多，请稍后再试。" kCardImg:nil buttonArr:nil subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:NULL];
    } else if (type == alertTypeNoDataWithCustomer){    // 3.【客服校验失败】
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"] title:@"比赛记录获取失败" desc:@"TGP 无法查询到本场战役的数据，您将可以继续征战" subDesc:nil kCardImg:nil buttonArr:@[@"确定"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            if (block){
                block(buttonIndex);
                [alert dismissWithCompletion:NULL];
            }
        }];
    } else if (type == alertTypeCustomerAgain){         // 【客服再次校验失败】
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"] title:@"比赛记录获取失败" desc:nil subDesc:@"比赛将由人工判定，请耐心等待处理结果" kCardImg:nil buttonArr:nil subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:NULL];
    } else if (type == alertTypeNotReadyTo){              // 【没有准备好】
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"] title:@"提示" desc:@"匹配失败，由于您的对手未准备" subDesc:nil kCardImg:nil buttonArr:@[@"重新寻找对手",@"返回首页"] subIconImg:nil challengeEnd:challengeEndFail buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            if (block){
                block(buttonIndex);
                [alert dismissWithCompletion:NULL];
            }
        }];
    } else if(type == alertTypeNotReadyMe){
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"login_logo"] title:@"提示" desc:@"匹配失败，您未能确认战斗" subDesc:nil kCardImg:nil buttonArr:@[@"继续寻找对手",@"更换出征条件"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            if (block){
                block(buttonIndex);
                [alert dismissWithCompletion:NULL];
            }
        }];
    }
    alert = [[JCAlertView alloc]initWithCustomView:bgView dismissWhenTouchedBackground:YES];
    [alert show];
}
#pragma mark - 注册登录
- (void)alterWithLoginType:(loginAlertType)loginType completion:(void(^)())completion
{
    UIView *bgView;
    if (loginType == alertReSetPasswordSuccess) {
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"icon_alert_success"] title:@"重置成功" desc:@"请妥善保管自己的密码" subDesc:nil kCardImg:nil buttonArr:@[@"确定"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            [alert dismissWithCompletion:NULL];
            if (completion) {
                completion();
            }
        }];
    }else if (loginType == alertRegisterSuccess) {
        bgView = [self alertWithHeaderImgView:[UIImage imageNamed:@"icon_alert_success"] title:@"注册成功" desc:@"欢迎使用PANDAOL" subDesc:nil kCardImg:nil buttonArr:@[@"确定"] subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
            [alert dismissWithCompletion:NULL];
            if (completion) {
                completion();
            }
        }];
    }
    alert = [[JCAlertView alloc]initWithCustomView:bgView dismissWhenTouchedBackground:NO];
    [alert show];
}
#pragma mark - 战斗胜利和失败
-(void)alertWithChallengeEndType:(challengeEnd)endType integral:(NSInteger)integral buttonArr:(NSArray *)btnArr block:(void (^)(NSInteger buttonIndex))block{
    UIView *bgView;
    if (endType == challengeEndFail){               // 失败
        bgView = [self alertWithChallengerImgView:[UIImage imageNamed:@"login_logo"] challengeEndType:challengeEndFail title:@"失败" desc:[NSString stringWithFormat:@"%li",(long)integral] buttonArr:btnArr buttonClick:^(NSInteger buttonIndex) {
            if (block){
                block(buttonIndex);
            }
        }];
    } else if (endType == challengeEndVictory){     // 胜利
        
    }
    [[[JCAlertView alloc]initWithCustomView:bgView dismissWhenTouchedBackground:YES]show];
}


#pragma mark 积分兑换成功和失败
-(void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc{
    UIView *bgView = [self alertWithHeaderImgView:headerImage title:title desc:nil subDesc:desc kCardImg:nil buttonArr:nil subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:NULL];
    alert = [[JCAlertView alloc]initWithCustomView:bgView dismissWhenTouchedBackground:YES];
    [alert show];
}

- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void (^)(NSInteger))block {
    UIView *bgView = [self alertWithHeaderImgView:headerImage title:title desc:desc subDesc:nil kCardImg:nil buttonArr:nameArray subIconImg:nil challengeEnd:challengeEndNormal buttonTitleArrbuttonClick:^(NSInteger buttonIndex) {
        [alert dismissWithCompletion:nil];
        if (block) {
            block(buttonIndex);
        }
    }];
    alert = [[JCAlertView alloc] initWithCustomView:bgView dismissWhenTouchedBackground:NO];
    [alert show];
}

#pragma mark -  兑换积分卡列表
+ (PDIntegralBackgroundView *)alertListWithTitle:(NSString *)title cardModelArray:(NSArray *)cardModelArray totalintegral:(NSInteger)totalIntegral buttonblock:(void (^)(NSInteger))block {
    PDIntegralBackgroundView *bgView = [[PDIntegralBackgroundView alloc] init];
    [bgView customBackgroundViewWithTitle:title modelArray:cardModelArray totalIntegral:totalIntegral buttonArray:@[@"确定",@"取消"] buttonClick:block];
    return bgView;
}

#pragma mark - 创建自定义战斗成功失败view
-(UIView *)alertWithChallengerImgView:(UIImage *)errImg challengeEndType:(challengeEnd)endType title:(NSString *)title desc:(NSString *)desc buttonArr:(NSArray *)buttonArr buttonClick:(void(^)(NSInteger buttonIndex))block{
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.clipsToBounds = YES;
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(50), 300);
    bgView.layer.cornerRadius = 5;
    [self addSubview:bgView];
    
    // 2. 创建胜利图片
    UIImageView *iconImageView = [[UIImageView alloc]init];
    iconImageView.backgroundColor = [UIColor clearColor];
    iconImageView.image = errImg;
    iconImageView.frame = CGRectMake((bgView.size_width - LCFloat(50)) / 2., LCFloat(27), LCFloat(53), LCFloat(53));
    [bgView addSubview:iconImageView];
    
    // 3. 创建提示
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    titleLabel.frame = CGRectMake(0, CGRectGetMaxY(iconImageView.frame) + LCFloat(15), bgView.size_width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [bgView addSubview:titleLabel];
    
    // 4. 创建desc
    UILabel *descLabel = [[UILabel alloc]init];
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.font = [UIFont systemFontOfCustomeSize:19.];
    descLabel.numberOfLines = 1;
    descLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.text = desc;
    CGSize contentOfDescSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:descLabel.font])];
    [bgView addSubview:descLabel];
    
    // 5. 创建imgView
    UIImageView *subIconImageView = [[UIImageView alloc]init];
    subIconImageView.backgroundColor = [UIColor clearColor];
    [bgView addSubview:subIconImageView];
    
    // 6. 进行计算
    CGFloat iconWidth = LCFloat(40);
    CGFloat origin_X = (bgView.size_width - contentOfDescSize.width - LCFloat(10) - iconWidth) / 2.;
    descLabel.frame = CGRectMake(origin_X, CGRectGetMaxY(titleLabel.frame) + LCFloat(25), contentOfDescSize.width, [NSString contentofHeightWithFont:descLabel.font]);
    subIconImageView.frame = CGRectMake(CGRectGetMaxX(descLabel.frame) + LCFloat(10), CGRectGetMaxY(descLabel.frame) - iconWidth, iconWidth, iconWidth);
    
    
    // 5. 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(descLabel.frame) + LCFloat(9), bgView.size_width - 2 * LCFloat(11), .5f);
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [bgView addSubview:lineView];
    if (!desc.length){
        lineView.orgin_y = CGRectGetMaxY(titleLabel.frame) + LCFloat(10);
    }
    
    
    // 6.1 计算bgView 高度
    CGFloat bgViewHeight = 0;
    bgViewHeight += CGRectGetMaxY(lineView.frame);
    bgViewHeight += LCFloat(28);
    bgViewHeight += (LCFloat(55/2.) + LCFloat(10)) * buttonArr.count;
    bgViewHeight += LCFloat(28);
    bgView.size_height = bgViewHeight;
    
    
    // 6. 创建scrollView
    UIScrollView *mainScrollView = [[UIScrollView alloc]init];
    mainScrollView.backgroundColor = [UIColor clearColor];
    mainScrollView.showsHorizontalScrollIndicator = NO;
    mainScrollView.showsVerticalScrollIndicator = NO;
    mainScrollView.pagingEnabled = YES;
    mainScrollView.bounces = NO;
    mainScrollView.scrollEnabled = NO;
    mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame), bgView.size_width, bgView.size_height - CGRectGetMaxY(lineView.frame));
    mainScrollView.contentSize = CGSizeMake(2 * mainScrollView.size_width, mainScrollView.size_height);
    [bgView addSubview:mainScrollView];
    
    // 1. 创建第一个view
    UIView *firstView = [[UIView alloc]init];
    firstView.backgroundColor = [UIColor clearColor];
    firstView.frame = CGRectMake(0, 0, bgView.size_width, bgView.size_height - CGRectGetMaxY(lineView.frame));
    [mainScrollView addSubview:firstView];
    
    // 1.1
    PDImageView *kCardImageViewWithV = [[PDImageView alloc]init];
    kCardImageViewWithV.backgroundColor = [UIColor clearColor];
    UIImage *kCardImg = [UIImage imageNamed:@"challenge_kCard_3"];
    kCardImageViewWithV.image = kCardImg;
    CGFloat imgBili = kCardImg.size.height * 1.f / kCardImg.size.width;
    CGFloat imgWidth = (firstView.size_height - 2 * LCFloat(11)) / imgBili;
    kCardImageViewWithV.frame = CGRectMake((firstView.size_width - imgWidth) / 2., LCFloat(11), imgWidth, firstView.size_height - 2 * LCFloat(11));
    [firstView addSubview:kCardImageViewWithV];

    // 2. 创建第二个view
    UIView *twoView = [[UIView alloc]init];
    twoView.backgroundColor = [UIColor clearColor];
    twoView.frame = CGRectMake(bgView.size_width, firstView.orgin_y, firstView.size_width, firstView.size_height);
    [mainScrollView addSubview:twoView];
    
    // 7. 创建按钮
    for (int i = 0 ; i < buttonArr.count;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:[buttonArr objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        
        CGFloat origin_x = LCFloat(44);
        CGFloat origin_y = LCFloat(28) + (LCFloat(55/2.) + LCFloat(10)) * i;
        button.frame = CGRectMake(origin_x, origin_y, bgView.size_width - 2 * LCFloat(44), LCFloat(55 / 2.));
        [twoView addSubview:button];
        
        [button buttonWithBlock:^(UIButton *button) {
            if (block){
                block(i);
            }
        }];
    }
    

    
    
    
    

    
    return bgView;
}




#pragma mark - 创建自定义View
-(UIView *)alertWithHeaderImgView:(UIImage *)errImg title:(NSString *)title desc:(NSString *)desc subDesc:(NSString *)subDesc kCardImg:(UIImage *)kCardImg buttonArr:(NSArray *)buttonArr subIconImg:(UIImage *)subIconImg challengeEnd:(challengeEnd)challengeEndType buttonTitleArrbuttonClick:(void (^)(NSInteger buttonIndex))block{
    
    // 1.背景
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(50), 300);
    bgView.layer.cornerRadius = 5;
    [self addSubview:bgView];
    
    // 2. 创建胜利图片
    UIImageView *iconImageView = [[UIImageView alloc]init];
    iconImageView.backgroundColor = [UIColor clearColor];
    iconImageView.image = errImg;
    iconImageView.frame = CGRectMake((bgView.size_width - LCFloat(50)) / 2., LCFloat(15), LCFloat(50), LCFloat(50));
    [bgView addSubview:iconImageView];
    
    // 3. 创建提示
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    titleLabel.frame = CGRectMake(0, CGRectGetMaxY(iconImageView.frame) + LCFloat(15), bgView.size_width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [bgView addSubview:titleLabel];
    
    // 4. 创建desc
    UILabel *descLabel = [[UILabel alloc]init];
    descLabel.backgroundColor = [UIColor clearColor];
    descLabel.font = [UIFont systemFontOfCustomeSize:14.];
    descLabel.numberOfLines = 0;
    descLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    descLabel.textAlignment = NSTextAlignmentCenter;
    descLabel.text = desc;
    CGSize contentOfDescSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(bgView.size_width - 2 * LCFloat(11), CGFLOAT_MAX)];
    descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(titleLabel.frame) + LCFloat(20), bgView.size_width - 2 * LCFloat(11), contentOfDescSize.height);
    [bgView addSubview:descLabel];
    
    // 4. 创建钱袋Image
    PDImageView *moneyImageView = [[PDImageView alloc]init];
    moneyImageView.backgroundColor = [UIColor clearColor];
    moneyImageView.image = subIconImg;
    [bgView addSubview:moneyImageView];
    
    // 6. 进行计算
    CGFloat iconWidth = LCFloat(40);
    if (contentOfDescSize.height == [NSString contentofHeightWithFont:descLabel.font]){ // 一行
        CGSize contentOfDescLabelSize = [descLabel.text sizeWithCalcFont:descLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:descLabel.font])];
        if (subIconImg){
            CGFloat origin_X = (bgView.size_width - contentOfDescLabelSize.width - LCFloat(10) - iconWidth) / 2.;
            descLabel.frame = CGRectMake(origin_X, CGRectGetMaxY(titleLabel.frame) + LCFloat(25), contentOfDescLabelSize.width, [NSString contentofHeightWithFont:descLabel.font]);
            moneyImageView.frame = CGRectMake(CGRectGetMaxX(descLabel.frame) + LCFloat(10), CGRectGetMaxY(descLabel.frame) - iconWidth, iconWidth, iconWidth);
        }
    } else {                   // 多行
        
    }
    
    
    // 5. 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(descLabel.frame) + LCFloat(10), bgView.size_width - 2 * LCFloat(11), .5f);
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [bgView addSubview:lineView];
    if (!desc.length){
        lineView.orgin_y = CGRectGetMaxY(titleLabel.frame) + LCFloat(10);
    }
    
    // 6. 创建subDesc
    UILabel *subDescLabel = [[UILabel alloc]init];
    subDescLabel.backgroundColor = [UIColor clearColor];
    subDescLabel.font = [UIFont systemFontOfCustomeSize:13.];
    subDescLabel.numberOfLines = 0;
    subDescLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    subDescLabel.textAlignment = NSTextAlignmentLeft;
    subDescLabel.text = subDesc;
    CGSize subDescSize = [subDescLabel.text sizeWithCalcFont:subDescLabel.font constrainedToSize:CGSizeMake(descLabel.size_width, CGFLOAT_MAX)];
    subDescLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(lineView.frame) + LCFloat(10), bgView.size_width - 2 * LCFloat(11), subDescSize.height);
    [bgView addSubview:subDescLabel];
    if (!desc.length){
        subDescLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    // 6. 创建k卡
    UIImageView *kCardImageView = [[UIImageView alloc]init];
    kCardImageView.backgroundColor = [UIColor clearColor];
    kCardImageView.image = kCardImg;
    kCardImageView.frame = CGRectMake((bgView.size_width - kCardImg.size.width) / 2., CGRectGetMaxY(lineView.frame) + LCFloat(10), kCardImg.size.width, kCardImg.size.height);
    [bgView addSubview:kCardImageView];

    // 7. 创建按钮
    for (int i = 0 ; i < buttonArr.count;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:[buttonArr objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        
        CGFloat origin_x = LCFloat(20);
        CGFloat origin_y = CGRectGetMaxY(lineView.frame) + LCFloat(10) + (LCFloat(35) + LCFloat(10)) * i;
        button.frame = CGRectMake(origin_x, origin_y, bgView.size_width - 2 * LCFloat(20), LCFloat(35));
        [bgView addSubview:button];
        
        [button buttonWithBlock:^(UIButton *button) {
            if (block){
                block(i);
            }
        }];
    }
    
    CGFloat bgViewHeight = 0;
    bgViewHeight += CGRectGetMaxY(lineView.frame);
    
    // 1. 提示状态《按钮》
    if (subDesc.length){            // 判断是否有底部的文字
        subDescLabel.hidden = NO;
        kCardImageView.hidden = YES;
        bgViewHeight += LCFloat(10);
        bgViewHeight += subDescSize.height;
        bgViewHeight += LCFloat(10);
    } else if (kCardImg){
        subDescLabel.hidden = YES;
        kCardImageView.hidden = NO;
        bgViewHeight += LCFloat(10);
        bgViewHeight += kCardImg.size.height;
        bgViewHeight += LCFloat(10);
    } else if (buttonArr.count){
        subDescLabel.hidden = YES;
        kCardImageView.hidden = YES;
        bgViewHeight += LCFloat(10) + (LCFloat(35) + LCFloat(10)) * buttonArr.count;
    } else {
        subDescLabel.hidden = YES;
        kCardImageView.hidden = YES;
        lineView.hidden = YES;
    }
    bgView.size_height = bgViewHeight;
    return bgView;
}

#pragma mark - 兑换网费结果
- (void)alertWithTitle:(NSString *)title headerImage:(UIImage *)headerImage changeRMB:(NSInteger)rmb netInfo:(PDNetModel *)netModel desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void (^)(NSInteger))block {
    UIView *bgView = [self customViewWithTitle:title headerImage:headerImage changeRMB:rmb netInfo:netModel desc:desc buttonNameArray:nameArray clickBlock:^(NSInteger buttonIndex) {
        [alert dismissWithCompletion:nil];
        if (block) {
            block(buttonIndex);
        }
    }];
    alert = [[JCAlertView alloc] initWithCustomView:bgView dismissWhenTouchedBackground:NO];
    [alert show];
}

- (UIView *)customViewWithTitle:(NSString *)title headerImage:(UIImage *)headerImage changeRMB:(NSInteger)rmb netInfo:(PDNetModel *)netModel desc:(NSString *)desc buttonNameArray:(NSArray *)nameArray clickBlock:(void (^)(NSInteger))block {
    // 1.背景
    UIView *bgView = [[UIView alloc]init];
    bgView.frame = CGRectMake(0, 0, LCFloat(280) + LCFloat(15) * 2, 2+LCFloat(50)+HSFloat(360));
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.layer.cornerRadius = 5;
    [self addSubview:bgView];
    
    // 2. 创建胜利图片
    UIImageView *iconImageView = [[UIImageView alloc]init];
    iconImageView.backgroundColor = [UIColor clearColor];
    iconImageView.image = headerImage;
    iconImageView.frame = CGRectMake((bgView.size_width - LCFloat(50)) / 2., HSFloat(15), LCFloat(50), LCFloat(50));
    [bgView addSubview:iconImageView];
    
    // 3. 创建提示
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    titleLabel.frame = CGRectMake(0, CGRectGetMaxY(iconImageView.frame) + HSFloat(15), bgView.size_width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.textColor = [UIColor colorWithCustomerName:@"绿"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [bgView addSubview:titleLabel];
    
    // 兑换信息
    PDExchangeInfoView *exchangeInfoView = [[PDExchangeInfoView alloc] initWithFrame:CGRectMake((bgView.size_width - LCFloat(280))/2, CGRectGetMaxY(titleLabel.frame) + HSFloat(15), LCFloat(280), HSFloat(140))];
    exchangeInfoView.moneyLabel.text = [NSString stringWithFormat:@"%ld",(long)rmb];
    [exchangeInfoView.headerImageView uploadImageWithURL:netModel.avatar placeholder:nil callback:nil];
    exchangeInfoView.nameLabel.text = netModel.shop_name;
    exchangeInfoView.addressLabel.text = netModel.address;
    exchangeInfoView.phoneLabel.text = netModel.mobile;
    exchangeInfoView.numLabel.text = [NSString stringWithFormat:@"NO.%@",netModel.netId];
    [bgView addSubview:exchangeInfoView];
    
    // line
    UIView *lineView = [[UIView alloc] init];
    lineView.frame = CGRectMake(0, CGRectGetMaxY(exchangeInfoView.frame) + HSFloat(20), CGRectGetWidth(bgView.frame), 2);
    lineView.backgroundColor = [UIColor lightGrayColor];
    [bgView addSubview:lineView];
    
    // 提示框
    UILabel *msgLabel = [[UILabel alloc] init];
    msgLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
    msgLabel.numberOfLines = 0;
    msgLabel.frame = CGRectMake(0, CGRectGetMaxY(lineView.frame) + HSFloat(15), CGRectGetWidth(bgView.frame), [NSString contentofHeightWithFont:msgLabel.font]*2);
    msgLabel.text = desc;
    msgLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    msgLabel.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:msgLabel];
    
    
    // buttons
    for (int i = 0 ; i <nameArray.count;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:[nameArray objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        
        CGFloat button_width = LCFloat(125);
        CGFloat button_x = LCFloat(20) + i * (button_width + (CGRectGetWidth(bgView.frame) - button_width*2 - LCFloat(20)*2));
        button.frame = CGRectMake(button_x, CGRectGetMaxY(msgLabel.frame) + HSFloat(15), button_width, HSFloat(30));
        [button buttonWithBlock:^(UIButton *button) {
            if (block) {
                block(i);
            }
        }];
        [bgView addSubview:button];
    }
    
    return bgView;
}
@end
