//
//  PDChallengerAlertViewManager.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDChallengerCardEnum.h"                    // k 卡枚举
#import "PDAlertView.h"
@class PDAlertView;


@interface PDChallengerAlertViewManager : NSObject

// 报错误提示
-(UIView *)alertWithTime:(NSInteger)timeInteger;

#pragma mark 基础弹框
-(UIView *)alertWithType:(alertType)type block:(void (^)(NSInteger buttonIndex))block;

#pragma mark 比赛结果获取方式
-(UIView *)alertWithChallengeEndType:(challengeEnd)endType currentKCard:(kCardType)kCard integral:(NSInteger)integral buttonArr:(NSArray *)btnArr block:(void (^)(NSInteger buttonIndex))block;

#pragma mark king Manager
//-(UIView *)kingManager;

@end
