//
//  WSProgressHUD.h
//  WSProgressHUD
//
//  Created by Wilson-Yuan on 15/7/17.
//  Copyright (c) 2015年 wilson-yuan. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, WSProgressHUDMaskType) {
    WSProgressHUDMaskTypeDefault, /*< 默认*/
    WSProgressHUDMaskTypeClear, /*< 没有效果*/
    WSProgressHUDMaskTypeBlack, /*< 背景黑色*/
    WSProgressHUDMaskTypeGradient /*< 背景有梯度差*/
};

typedef NS_ENUM(NSInteger, WSProgressHUDMaskWithoutType) { //
    WSProgressHUDMaskWithoutDefault,                    // default mask all
    WSProgressHUDMaskWithoutNavigation,                 //show mask without navigation
    WSProgressHUDMaskWithoutTabbar,                     //show mask without tabbar
    WSProgressHUDMaskWithoutNavAndTabbar,               //show mask without tabbar and navigation
};

typedef NS_ENUM(NSInteger, WSProgressHUDIndicatorStyle) {
    WSProgressHUDIndicatorCustom,
    WSProgressHUDIndicatorMMSpinner,
    WSProgressHUDIndicatorSmallLight,
};


@interface WSProgressHUD : UIView

/*----------------------Show On the Window------------------------------*/
+ (void)showWithafterDelay:(CGFloat)delay;/**< 显示12*/
+ (void)showWithMaskType: (WSProgressHUDMaskType)maskType delay:(CGFloat)delay;;
+ (void)showWithMaskType: (WSProgressHUDMaskType)maskType maskWithout: (WSProgressHUDMaskWithoutType)withoutType;
+ (void)showWithMaskType: (WSProgressHUDMaskType)maskType maskWithout: (WSProgressHUDMaskWithoutType)withoutType delay:(CGFloat)delay;

/**@brief 带整数参数的方法. */
+ (void)showWithStatus: (NSString *)string delay:(CGFloat)delay;;;
+ (void)showWithStatus: (NSString *)string maskType: (WSProgressHUDMaskType)maskType delay:(CGFloat)delay;;;
+ (void)showWithStatus: (NSString *)string maskType: (WSProgressHUDMaskType)maskType maskWithout: (WSProgressHUDMaskWithoutType)withoutType delay:(CGFloat)delay;

+ (void)showShimmeringString: (NSString *)string afterDelay:(CGFloat)delay ;
+ (void)showShimmeringString: (NSString *)string maskType: (WSProgressHUDMaskType)maskType delay:(CGFloat)delay;
+ (void)showShimmeringString: (NSString *)string maskType: (WSProgressHUDMaskType)maskType maskWithout: (WSProgressHUDMaskWithoutType)withoutType delay:(CGFloat)delay;

+ (void)showProgress:(CGFloat)progress delay:(CGFloat)delay;
+ (void)showProgress:(CGFloat)progress maskType:(WSProgressHUDMaskType)maskType delay:(CGFloat)delay;
+ (void)showProgress:(CGFloat)progress maskType:(WSProgressHUDMaskType)maskType maskWithout: (WSProgressHUDMaskWithoutType)withoutType delay:(CGFloat)delay;

+ (void)showProgress:(CGFloat)progress status:(NSString*)string delay:(CGFloat)delay;
+ (void)showProgress:(CGFloat)progress status:(NSString*)string maskType:(WSProgressHUDMaskType)maskType delay:(CGFloat)delay;
+ (void)showProgress:(CGFloat)progress status:(NSString*)string maskType:(WSProgressHUDMaskType)maskType maskWithout: (WSProgressHUDMaskWithoutType)withoutType delay:(CGFloat)delay;

+ (void)showImage:(UIImage *)image status:(NSString *)title delay:(CGFloat)delay;
+ (void)showImage:(UIImage *)image status:(NSString *)title maskType: (WSProgressHUDMaskType)maskType delay:(CGFloat)delay;
+ (void)showImage:(UIImage *)image status:(NSString *)title maskType: (WSProgressHUDMaskType)maskType maskWithout: (WSProgressHUDMaskWithoutType)withoutType delay:(CGFloat)delay;

+ (void)showSuccessWithStatus: (NSString *)string delay:(CGFloat)delay;
+ (void)showErrorWithStatus: (NSString *)string delay:(CGFloat)delay;

+ (void)dismiss;

/*----------------------------Custom---------------------------------*/

+ (void)setProgressHUDIndicatorStyle: (WSProgressHUDIndicatorStyle)style;


/*----------------------Show On the view------------------------------*/

- (instancetype)initWithView: (UIView *)view;
- (instancetype)initWithFrame:(CGRect)frame;

- (void)show;
- (void)showWithMaskType: (WSProgressHUDMaskType)maskType;

- (void)showWithString: (NSString *)string;
- (void)showWithString: (NSString *)string maskType: (WSProgressHUDMaskType)maskType;


- (void)showShimmeringString: (NSString *)string;
- (void)showShimmeringString: (NSString *)string maskType: (WSProgressHUDMaskType)maskType;

- (void)showProgress:(CGFloat)progress;
- (void)showProgress:(CGFloat)progress maskType:(WSProgressHUDMaskType)maskType;

- (void)showProgress:(CGFloat)progress status:(NSString*)status;
- (void)showProgress:(CGFloat)progress status:(NSString*)status maskType:(WSProgressHUDMaskType)maskType;



- (void)showImage:(UIImage *)image status:(NSString *)title;
- (void)showImage:(UIImage *)image status:(NSString *)title maskType: (WSProgressHUDMaskType)maskType;

- (void)showSuccessWithString: (NSString *)string;
- (void)showErrorWithString: (NSString *)string;


- (void)dismiss;

@end
