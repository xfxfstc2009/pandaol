//
//  PDChallengerProgressView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengerProgressView.h"
@interface PDChallengerProgressView(){
    CAShapeLayer *shapLayer;
}

@end

@implementation PDChallengerProgressView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.sharkView = [[WhaleView alloc]init];
    self.sharkView.trackTintColor = [UIColor clearColor];
    self.sharkView.layer.borderColor = c15.CGColor;
    self.sharkView.layer.borderWidth = 1;
    self.sharkView.progressTintColor = c15;
    self.sharkView.frame = CGRectMake(0, 0, self.size_width, self.size_height);
    [self addSubview:self.sharkView];
    
    [self.sharkView setShape:[self createView1].CGPath];
    
    [self.sharkView setProgress:0];
    [self.sharkView startGravity];
}

-(void)animationWithProgress:(CGFloat)progress{
    [self.sharkView setProgress:progress animated:YES];
}


#pragma mark - createView
// 1. 画出一个容器
-(UIBezierPath *)createView1{
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];
    [bezierPath moveToPoint: CGPointMake(0, 0)];
    [bezierPath addLineToPoint: CGPointMake(self.bounds.size.width, 0)];
    [bezierPath addLineToPoint: CGPointMake(self.bounds.size.width, self.bounds.size.height)];
    [bezierPath addLineToPoint: CGPointMake(0, self.bounds.size.height)];
    [bezierPath addLineToPoint: CGPointMake(0, 0)];
    [bezierPath closePath];
    [UIColor.grayColor setFill];
    [bezierPath fill];
    [UIColor.blackColor setStroke];
    bezierPath.lineWidth = 1;
    [bezierPath stroke];
    
    return bezierPath;
}

@end
