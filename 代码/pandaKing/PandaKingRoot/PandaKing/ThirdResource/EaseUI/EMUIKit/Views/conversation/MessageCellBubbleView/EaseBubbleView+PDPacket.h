//
//  EaseBubbleView+PDPacket.h
//  PandaKing
//
//  Created by Cranz on 17/3/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "EaseBubbleView.h"

#define BUBBLE_PACKET_WIDTH  260.f
#define BUBBLE_PACKET_HEIGHT 65

/// 红包类型
@interface EaseBubbleView (PDPacket)
- (void)setupPacketBubbleView;
- (void)updatePacketMargin:(UIEdgeInsets)margin;
@end
