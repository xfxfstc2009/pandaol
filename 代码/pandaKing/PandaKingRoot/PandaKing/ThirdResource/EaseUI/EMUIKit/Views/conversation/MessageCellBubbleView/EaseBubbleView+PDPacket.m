//
//  EaseBubbleView+PDPacket.m
//  PandaKing
//
//  Created by Cranz on 17/3/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "EaseBubbleView+PDPacket.h"

// icon 72 x 90
#define PACKET_ICON_WIDTH  36
#define PACKET_ICON_HEIGHT 45

@implementation EaseBubbleView (PDPacket)

- (void)_setupPacketMarginConstraints {
    NSLayoutConstraint *packetTopConstraint = [NSLayoutConstraint constraintWithItem:self.packetIcon attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeTop multiplier:1.0 constant:EaseMessageCellPadding];
    NSLayoutConstraint *packetLeftConstraint = [NSLayoutConstraint constraintWithItem:self.packetIcon attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:EaseMessageCellPadding];
    NSLayoutConstraint *packetWidthConstraint = [NSLayoutConstraint constraintWithItem:self.packetIcon attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:0.0 constant:PACKET_ICON_WIDTH];
    NSLayoutConstraint *packetHeightConstraint = [NSLayoutConstraint constraintWithItem:self.packetIcon attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:PACKET_ICON_HEIGHT];
    NSLayoutConstraint *packetRightConstraint = [NSLayoutConstraint constraintWithItem:self.packetIcon attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-158];
    NSLayoutConstraint *packetBottomConstraint = [NSLayoutConstraint constraintWithItem:self.packetIcon attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-EaseMessageCellPadding];
    
    [self.marginConstraints removeAllObjects];
    [self.marginConstraints addObject:packetTopConstraint];
    [self.marginConstraints addObject:packetLeftConstraint];
    [self.marginConstraints addObject:packetWidthConstraint];
    [self.marginConstraints addObject:packetHeightConstraint];
    [self.marginConstraints addObject:packetRightConstraint];
    [self.marginConstraints addObject:packetBottomConstraint];
    
    // title
    NSLayoutConstraint *titleTopConstraint = [NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.packetIcon attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    NSLayoutConstraint *titleLeftConsatraint = [NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.packetIcon attribute:NSLayoutAttributeRight multiplier:1.0 constant:EaseMessageCellPadding];
    NSLayoutConstraint *titleRightConstraint = [NSLayoutConstraint constraintWithItem:self.titleLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-EaseMessageCellPadding];
    
    [self.marginConstraints addObjectsFromArray:@[titleLeftConsatraint, titleTopConstraint, titleRightConstraint]];
    
    // sub title
    NSLayoutConstraint *subtitleBottomConstraint = [NSLayoutConstraint constraintWithItem:self.subTitleLabel attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.packetIcon attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *subtitleLeftConsatraint = [NSLayoutConstraint constraintWithItem:self.subTitleLabel attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.packetIcon attribute:NSLayoutAttributeRight multiplier:1.0 constant:EaseMessageCellPadding];
    NSLayoutConstraint *subtitleRightConstraint = [NSLayoutConstraint constraintWithItem:self.subTitleLabel attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:-EaseMessageCellPadding];
    
    [self.marginConstraints addObjectsFromArray:@[subtitleBottomConstraint, subtitleLeftConsatraint,subtitleRightConstraint]];
    
    // 红包背景图
    NSLayoutConstraint *packetBgTopConstraint = [NSLayoutConstraint constraintWithItem:self.packetBackgroundImageView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    NSLayoutConstraint *packetBgLeftConstraint = [NSLayoutConstraint constraintWithItem:self.packetBackgroundImageView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
    NSLayoutConstraint *packetBgBottomConstraint = [NSLayoutConstraint constraintWithItem:self.packetBackgroundImageView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0];
    NSLayoutConstraint *packetBgRightConstraint = [NSLayoutConstraint constraintWithItem:self.packetBackgroundImageView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:self.backgroundImageView attribute:NSLayoutAttributeRight multiplier:1.0 constant:0];
    [self.marginConstraints addObjectsFromArray:@[packetBgTopConstraint, packetBgLeftConstraint, packetBgBottomConstraint, packetBgRightConstraint]];
    
    [self addConstraints:self.marginConstraints];
}

#pragma mark - Public

- (void)setupPacketBubbleView {
    self.backgroundImageView.layer.cornerRadius = 2;
    self.backgroundImageView.clipsToBounds = YES;
    
    self.packetBackgroundImageView = [[UIImageView alloc] init];
    self.packetBackgroundImageView.translatesAutoresizingMaskIntoConstraints = NO;
    self.packetBackgroundImageView.backgroundColor = [UIColor hexChangeFloat:@"fb9d3a"];
    [self.backgroundImageView addSubview:self.packetBackgroundImageView];
    
    self.packetIcon = [[UIImageView alloc] init];
    self.packetIcon.translatesAutoresizingMaskIntoConstraints = NO;
    [self.backgroundImageView addSubview:self.packetIcon];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.titleLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
    self.titleLabel.textColor = [UIColor whiteColor];
    [self.backgroundImageView addSubview:self.titleLabel];
    
    self.subTitleLabel = [[UILabel alloc] init];
    self.subTitleLabel.translatesAutoresizingMaskIntoConstraints = NO;
    self.subTitleLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.subTitleLabel.textColor = [UIColor whiteColor];
    [self.backgroundImageView addSubview:self.subTitleLabel];
    
    [self _setupPacketMarginConstraints];
}

- (void)updatePacketMargin:(UIEdgeInsets)margin {
    if (_margin.top == margin.top && _margin.bottom == margin.bottom && _margin.left == margin.left && _margin.right == margin.right) {
        return;
    }
    _margin = margin;
    
    [self removeConstraints:self.marginConstraints];
    [self _setupPacketMarginConstraints];
}

@end
