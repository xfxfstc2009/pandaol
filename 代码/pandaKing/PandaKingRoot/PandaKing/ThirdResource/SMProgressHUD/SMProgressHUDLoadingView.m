//
//  SMProgressHUDLoadingView.m
//  SMProgressHUD
//
//  Created by OrangeLife on 15/10/10.
//  Copyright (c) 2015年 Shenme Studio. All rights reserved.
//

#import "SMProgressHUDLoadingView.h"
#import "SMProgressHUDConfigure.h"
#import "PDPandaHUDView.h"

static NSString *const kName = @"animationName";    /**< 动画的key*/
@interface SMProgressHUDLoadingView()
@property (strong, nonatomic) UILabel *tipLabel;
@property (strong, nonatomic) NSMutableArray *circles;
@property (nonatomic,strong)PDPandaHUDView *pandaHUD;
@property (nonatomic,strong)CALayer *cirlce1;
@property (nonatomic,strong)CALayer *cirlce2;
@property (nonatomic,strong)CALayer *cirlce3;
@property (nonatomic,strong)CALayer *cirlce4;
@property (nonatomic,strong)CALayer *cirlce5;
@property (nonatomic,strong)CALayer *cirlce6;
@end

@implementation SMProgressHUDLoadingView

#pragma mark 创建
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _circles = [NSMutableArray array];
        [self.layer setCornerRadius:kSMProgressHUDCornerRadius];
        [self setBackgroundColor:[UIColor whiteColor]];
        [self.layer setShadowOffset:CGSizeMake(0, 2)];
        [self.layer setShadowOpacity:0.2];
        
        self.pandaHUD = [[PDPandaHUDView alloc]init];
        self.pandaHUD.frame = CGRectMake((self.bounds.size.width - 140) / 2., LCFloat(11), 140, 140);
        self.pandaHUD.backgroundColor = [UIColor redColor];
        [self.pandaHUD startAnimationWithBlock:NULL];
        [self addSubview:self.pandaHUD];
        
        // 创建label
        self.tipLabel = [[UILabel alloc]init];
        self.tipLabel.backgroundColor = [UIColor clearColor];
        self.tipLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
        self.tipLabel.frame = CGRectMake(0, CGRectGetMaxY(self.pandaHUD.frame) + LCFloat(11), self.size_width, [NSString contentofHeightWithFont:self.tipLabel.font]);
        self.tipLabel.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:self.tipLabel];
        
        
        self.size_height = CGRectGetMaxY(self.pandaHUD.frame) + LCFloat(11) + 10.f + LCFloat(11);
        
    }
    return self;
}

#pragma mark 添加动画
- (void)addAnimation {
    CGFloat width = 10.f;
    CGFloat x = (self.frame.size.width - width * 6 -50)/2;
    CGFloat y;
    if (self.tipLabel.text.length){
       y = CGRectGetMaxY(self.tipLabel.frame) + LCFloat(11);
    } else {
       y = CGRectGetMaxY(self.pandaHUD.frame) + LCFloat(11);
    }
    
    for (NSInteger index = 0; index < 6; ++index) {
        CALayer *cirlce = [CALayer layer];
        [cirlce setFrame:CGRectMake(x + (width+10)*index, y, width, width)];
//        [cirlce setBackgroundColor:[UIColor colorWithRed:0.1+0.2*index green:0.1+0.1*index blue:0.1+0.1*index alpha:1].CGColor];
        [cirlce setBackgroundColor:[UIColor blackColor].CGColor];
        
        [cirlce setCornerRadius:width/2];
        [self.layer addSublayer:cirlce];
        
        CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        [anim setFromValue:@0];
        [anim setToValue:@1];
        [anim setAutoreverses:YES];
        [anim setDuration:0.8];
        [anim setRemovedOnCompletion:YES];
        [anim setBeginTime:CACurrentMediaTime()+0.2*index-1];
        [anim setRepeatCount:INFINITY];
        [anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [cirlce addAnimation:anim forKey:nil];
        [_circles addObject:cirlce];
        
    }
}

#pragma mark 动画1
-(void)addAnimation1{
    CGFloat width = 10.f;
    CGFloat x = (self.frame.size.width - width * 6 - 50)/2;
    CGFloat y;
    if (self.tipLabel.text.length){
        y = CGRectGetMaxY(self.tipLabel.frame) + LCFloat(11);
    } else {
        y = CGRectGetMaxY(self.pandaHUD.frame) + LCFloat(11);
    }
    
    for (NSInteger index = 0; index < 6; ++index) {
        CALayer *cirlce = [CALayer layer];
        [cirlce setFrame:CGRectMake(x + (width+10)*index, y, width, width)];
        [cirlce setBackgroundColor:[UIColor blackColor].CGColor];
        [cirlce setCornerRadius:width/2];
        cirlce.hidden = YES;
        [self.layer addSublayer:cirlce];
        
        [_circles addObject:cirlce];
    }
    
    [self doanimation:0];
}

-(void)doanimation:(NSInteger)index{
    if (index == 6){
        for (CALayer *circle1 in _circles){
            circle1.hidden = YES;
        }
        [self doanimation:0];
        return;
    }

    if (_circles.count){
        CALayer *cirlce = [_circles objectAtIndex:index];
        cirlce.hidden = NO;
        
        CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
        [anim setFromValue:@0];
        [anim setToValue:@1];
        [anim setDuration:.5f];
        anim.delegate = self;
        [anim setRemovedOnCompletion:YES];
        NSString *key = [NSString stringWithFormat:@"do%li",(long)index];
        [anim setValue:key forKey:kName];
        [anim setRepeatCount:1];
        [anim setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
        [cirlce addAnimation:anim forKey:nil];
    }
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if ([[anim valueForKey:kName] isEqualToString:@"do0"]){
        [self doanimation:1];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do1"]){
        [self doanimation:2];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do2"]){
        [self doanimation:3];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do3"]){
        [self doanimation:4];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do4"]){
        [self doanimation:5];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do5"]){
        [self doanimation:6];
    }

}


#pragma mark 重置提示语
- (void)setAlpha:(CGFloat)alpha {
    [super setAlpha:alpha];
    if (alpha == 0) {
        [_tipLabel setText:SMProgressHUDLoadingTip];
    }
}

#pragma mark 开始动画
- (void)willMoveToSuperview:(UIView *)newSuperview {
    [super willMoveToSuperview:newSuperview];
    if (_circles.count == 0) {
        [self addAnimation1];
    }
}

#pragma mark 停止动画
- (void)removeFromSuperview {
    [super removeFromSuperview];
    for (CALayer *circle in _circles) {
        [circle removeFromSuperlayer];
    }
    [_circles removeAllObjects];
}

#pragma mark 设置提示文字
-(void)setTipText:(NSString *)tip {
    _tipLabel.text = tip;
    
    if (tip.length){
        // 重新修改frame
        self.tipLabel.orgin_y = CGRectGetMaxY(self.pandaHUD.frame) + LCFloat(11);

        for (CALayer *cirlce in _circles){
            cirlce.frame = CGRectMake(cirlce.frame.origin.x,CGRectGetMaxY(self.tipLabel.frame) + LCFloat(11) , cirlce.frame.size.width, cirlce.frame.size.height);
        }
        self.size_height = CGRectGetMaxY(self.pandaHUD.frame) + LCFloat(11) + 10.f + LCFloat(11) + LCFloat(11) + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
    } else {
        for (CALayer *cirlce in _circles){
            cirlce.frame = CGRectMake(cirlce.frame.origin.x,CGRectGetMaxY(self.pandaHUD.frame) + LCFloat(11) , cirlce.frame.size.width, cirlce.frame.size.height);
        }
        self.size_height = CGRectGetMaxY(self.pandaHUD.frame) + LCFloat(11) + 10.f + LCFloat(11);
    }
}
@end
