//
//  PDTestViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTestViewController.h"
#import "PDAccountAvatarView.h"
@interface PDTestViewController()
@property (nonatomic,retain)CABasicAnimation *basicAnimation;       /**< 动画*/
@property (nonatomic,retain)CABasicAnimation *basicAnimation1;       /**< 动画*/
@property (nonatomic,strong)UIImageView *img1;
@property (nonatomic,strong)UIImageView *img2;
@property (nonatomic,strong)PDImageView *avatarImageView;
@property (nonatomic,strong)PDAccountAvatarView *accountView;
@end

@implementation PDTestViewController

-(void)viewDidLoad{
    [super viewDidLoad];
//    [self createView];
    [self rightBarButtonWithTitle:@"123" barNorImage:nil barHltImage:nil action:^{
        [self.accountView startAnimationDidEnd:NULL];
    }];
    self.view.backgroundColor = [UIColor blackColor];
    [self smart];

}


-(void)smart{
    self.accountView = [[PDAccountAvatarView alloc]init];
    self.accountView.backgroundColor = [UIColor clearColor];
    self.accountView.avatarFrame = CGRectMake(100, 100, 100, 100);
    self.accountView.avatarUrl = @"4.jpg";
    [self.view addSubview:self.accountView];
}

#pragma mark - createView;

-(void)createView{
    self.view.backgroundColor = [UIColor blackColor];

    self.img1 = [[UIImageView alloc]init];
    self.img1.backgroundColor = [UIColor clearColor];
    self.img1.frame = CGRectMake(100, 100, 100, 100);
    self.img1.image = [UIImage imageNamed:@"img_in"];
    [self.view addSubview:self.img1];

    self.img2 = [[UIImageView alloc]init];
    self.img2.backgroundColor = [UIColor clearColor];
    self.img2.frame = CGRectMake(100, 100, 100, 100);
    self.img2.image = [UIImage imageNamed:@"img_out"];
    [self.view addSubview:self.img2];
    
    // 1. 创建头像
    self.avatarImageView = [[PDImageView alloc]init];
    self.avatarImageView.backgroundColor = [UIColor clearColor];
    self.avatarImageView.frame = CGRectMake(50, 50, 70, 70);
    self.avatarImageView.center = self.img1.center;
    self.avatarImageView.clipsToBounds = YES;
    [self.avatarImageView uploadImageWithURL:@"4.jpg" placeholder:nil callback:NULL];
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.size_height / 2.;
    [self.view addSubview:self.avatarImageView];
}



- (CABasicAnimation *)basicAnimation {
    if (_basicAnimation == nil) {
        self.basicAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        //旋转一圈时长
        self.basicAnimation.duration = 8;
        //开始动画的起始位置
        self.basicAnimation.fromValue = [NSNumber numberWithInt:0];
        self.basicAnimation.byValue = [NSNumber numberWithInt:2];
        //M_PI是180度
        self.basicAnimation.toValue = [NSNumber numberWithInt:M_PI * 2];
        //动画重复次数
        [self.basicAnimation setRepeatCount:NSIntegerMax];
        //播放完毕之后是否逆向回到原来位置
        [self.basicAnimation setAutoreverses:NO];
        //是否叠加（追加）动画效果
        [self.basicAnimation setCumulative:YES];
        //停止动画，速度设置为0
        self.img1.layer.speed = 1;
        //    self.ImageView.layer.speed = 0;
        [self.img1.layer addAnimation:self.basicAnimation forKey:@"basicAnimation"];
    }
    return _basicAnimation;
}

- (CABasicAnimation *)basicAnimation1 {
    if (_basicAnimation1 == nil) {
        self.basicAnimation1 = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        //旋转一圈时长
        self.basicAnimation1.duration = 10;
        //开始动画的起始位置
        self.basicAnimation1.fromValue = [NSNumber numberWithInt:0];
        self.basicAnimation1.byValue = [NSNumber numberWithInt:2];
        //M_PI是180度
        self.basicAnimation1.toValue = [NSNumber numberWithInt:M_PI * 2];
        //动画重复次数
        [self.basicAnimation1 setRepeatCount:NSIntegerMax];
        //播放完毕之后是否逆向回到原来位置
        [self.basicAnimation1 setAutoreverses:NO];
        //是否叠加（追加）动画效果
        [self.basicAnimation1 setCumulative:YES];
        //停止动画，速度设置为0
        self.img2.layer.speed = 3;
        //    self.ImageView.layer.speed = 0;
        [self.img2.layer addAnimation:self.basicAnimation forKey:@"basicAnimation"];
    }
    return _basicAnimation1;
}

@end
