//
//  Constants.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

// 【USER Detault】
#define USERDEFAULTS     [NSUserDefaults standardUserDefaults]
#define NOTIFICENTER     [NSNotificationCenter defaultCenter]


// System
#define IS_IOS7_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 6.99)
#define IS_IOS8_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 7.99)
#define IS_IOS9_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 8.99)


#define kScreenBounds               [[UIScreen mainScreen] bounds]


// 【style】
#define RGB(r, g, b,a)    [UIColor colorWithRed:(r)/255. green:(g)/255. blue:(b)/255. alpha:a]

#define BACKGROUND_VIEW_COLOR        RGB(239, 239, 244,1)

#define NAVBAR_COLOR RGB(255, 255, 255,1)

// 【高德地图】
#define mapIdentify @"9e84d17f3fb0b260bce18de87c9dc95d"

// 【阿里云】
#define aliyunOSS_BaseURL @"http://smartmin.img-cn-shanghai.aliyuncs.com/Panda/Home/"

// 【百川聊天】
#define AliChattingIdentify @"23330943"                                     // 百川chattingAPI

// 【首页图片】
#define homeBannerbgImg  @"http://pic.baike.soso.com/p/20131204/20131204122431-1808867473.jpg"

// 【注册协议】
#define registerDelegate @"http://192.168.0.133:81/po/html/panda/userAgreement.html"

// Page
#import "PDNavigationController.h"
#import "RESideMenu.h"
#import "FBGlowLabel.h"
#import "PDLabel.h"

//Enum
#import "EnumGroup.h"

#import "UIView+ZYDraggable.h"
#import "PDWebViewController.h"
#import "FetchModel.h"


//transition
#import "UIViewController+XWTransition.h"
#import "XWFilterAnimator+XWRipple.h"
#import "UINavigationController+XWTransition.h"

// 刷新
#import "PDCenterTool.h"
#import "UIScrollView+PDPullToRefresh.h"

#import "PDMainTabbarViewController.h"
#import "PDTabBarController.h"


#define kKEYBOARD_SHOW  @"UIKeyboardWillShowNotification"
#define kKEYBOARD_HIDE  @"UIKeyboardWillHideNotification"

// 支付通知
#define kALIPAY_NOTIFACTION @"PDAlipayNotification"
#define kWXPAY_NOTIFACTION  @"PDWXPayNotification"

/// center / 表头部露出的空隙高度
#define kTableViewHeader_height     10
/// section header 的label距离左侧的间距
#define kTableViewSectionHeader_left    11
/// section 有label的时候的高度
#define kTableViewSectionHeader_height  30
/// 较大的空隙
#define kTableViewSectionHeader_leftMaxtter    18

#define kAPP_STORE_ID @"1079756043"


// 【资讯】
#define InformationDetail(catid,id)               [NSString stringWithFormat:@"http://www.pandaol.com/index.php?m=content&c=index&a=show&catid=%@&id=%@",catid,id]

// 【token】
#define CustomerToken @"CustomerToken"                              // token
#define CustomeSocketAddress @"CustomeSocketAddress"                // 地址
#define CustomeSocketPort @"CustomeSocketPort"                      // 当前的端口号
#define CustomerType    @"CustomerType"                             // 游客类型
#define CustomerMemberId @"CustomerMemberId"

// 本地用户数据
#define CustomerNickname @"CustomerNickname"
#define CustomerAvatar   @"CustomerAvatar"

// 第一次引导页控制,显示之后设置为yes
#define PDGUIDE_STATE @"pdFirstGuide"

// 【新特性】
#define NewFeature_Binding @"NewFeature_Binding"                        /**< 绑定*/
#define NewFeature_HomePage @"NewFeature_HomePage"                      /**< 首页*/
#define NewFeature_Message @"NewFeature_Message"                        /**< 消息*/
#define NewFeature_NetBar @"NewFeature_NetBar"                          /**< 网吧*/
#define NewFeature_Personal @"NewFeature_Personal"                      /**< 个人中心*/
#define NewFeature_Center @"NewFeature_Center"                          /**< centert*/

#define FirstLogin @"FirstLogin"                                        /**< 第一次登陆*/
#define FirstChooseGame @"FirstChooseGame"                              /**< 第一次选择游戏*/
#import "PDViewTool.h"
#import "PDSideMenu.h"


#define  championGuess1 @"championGuess1"
#define  championGuess2 @"championGuess2"
#endif /* Constants_h */
