//
//  AppDelegate.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

