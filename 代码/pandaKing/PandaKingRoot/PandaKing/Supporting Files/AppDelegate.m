//
//  AppDelegate.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AppDelegate.h"

#import "PDStupSQLiteManager.h"
#import "PDLaungchViewController.h"                 /**> LaungchView*/
#import "PDNewFeatureViewController.h"

// 【Test】
#import "PDTabBarController.h"
#import "PDSideMenu.h"

#import "APNSTool.h"
#import "PDOAuthLoginAndShareTool.h"
#import "PDMapViewSetupManager.h"                       // 地图

// 支付
#import <AlipaySDK/AlipaySDK.h>
#import "PDWXPayHandle.h"

// 友盟
#import <Bugly/Bugly.h>
#import "UMMobClick/MobClick.h"

#import "PDPandaRoleDetailViewController.h"
#import "PDGradientNavBar.h"

#import "PDWelcomeViewController.h"

// 推送
#import "PDApnsControllerManager.h"

#import "Growing.h"

@interface AppDelegate ()<WXApiDelegate,WeiboSDKDelegate,UITabBarControllerDelegate, QQApiInterfaceDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [self stupSQLiteManager];                                                                       // 1. 安装数据库
    [PDOAuthLoginAndShareTool regist];                                                              // 5.注册三方登录
    [PDMapViewSetupManager authorizineMap];                                                         /**< 创建地图*/
    [[APNSTool shareInstance] registWithPUSHWithApplication:application launchOptions:launchOptions deviceIdBlock:NULL];                                                                        // 3.推送

    [[PDEMManager sharedInstance] initial];
    [self mainSetup];                                                                               // 6.创建主视图

    // 注册微信支付
    [PDWXPayHandle regiset];

    [self createStartAnimation];
    
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    [self becomeFirstResponder];
    
    [Bugly startWithAppId:@"900056439"];
    [self umengTrack];
    
    [PDApnsControllerManager apnsWithLaunchOptions:launchOptions];
    [self addGrowingIOManager];

    return YES;
}

#pragma mark - createMainView
-(void)mainSetup{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    UIImage *backgroundImage = [Tool createImageWithColor:[UIColor blackColor] frame:CGRectMake(0, 0, kScreenBounds.size.width, 44)];
    [[UINavigationBar appearance] setBackgroundImage:backgroundImage forBarMetrics:UIBarMetricsDefault];
    
    self.window.backgroundColor = [UIColor clearColor];
    [self.window makeKeyAndVisible];
    
//    PDLaungchViewController *launchVC = [[PDLaungchViewController alloc] init];
//    [self.window addSubview:launchVC.view];
}



- (void)applicationWillResignActive:(UIApplication *)application {

}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[PDEMManager sharedInstance] applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[PDEMManager sharedInstance] applicationWillEnterForeground:application];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    
}

- (void)applicationWillTerminate:(UIApplication *)application {
    
}

#pragma mark - 数据库
-(void)stupSQLiteManager{
    [PDStupSQLiteManager steupSqlite];
}



#pragma maek - 推送
#pragma mark ---------------------------------------------------
// 注册deviceToken
- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        [[EMClient sharedClient] bindDeviceToken:deviceToken];
    });
    [[APNSTool shareInstance] registerWithDeviceId:deviceToken];
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings {
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

// 通知统计回调
- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
    [[APNSTool shareInstance] registerWithReceiveRemoteNotification:userInfo];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [[PDEMManager sharedInstance] application:application didReceiveRemoteNotification:userInfo];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error {
    DLog(@"didFailToRegisterForRemoteNotificationsWithError %@", error);
}


#pragma mark - 推送end
#pragma mark ----------------------------------------------

#ifdef IS_IOS9_LATER
-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
    return [self openURLManagerWithURL:url];
}
#else
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    if ([Growing handleUrl:url]){
        return YES;
    } else {
        return [self openURLManagerWithURL:url];
    }
    return NO;

}
#endif

-(BOOL)openURLManagerWithURL:(NSURL *)url{
    DLog(@"%@",url);
    
    if ([url.host isEqualToString:@"safepay"]) {
        //这个是进程KILL掉之后也会调用，这个只是第一次授权回调，同时也会返回支付信息
        [[AlipaySDK defaultService] processAuth_V2Result:url standbyCallback:^(NSDictionary *resultDic) {
            NSString * str = resultDic[@"result"];
            DLog(@"result = %@",str);
        }];
        //跳转支付宝钱包进行支付，处理支付结果，这个只是辅佐订单支付结果回调
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) {
            DLog(@"result = %@",resultDic);
            NSString * str = resultDic[@"memo"];
            DLog(@"memo = %@",str);
            
        }];
    }else if ([url.host isEqualToString:@"platformapi"]){
        //授权返回码
        [[AlipaySDK defaultService] processAuthResult:url standbyCallback:^(NSDictionary *resultDic) {
            
        }];
    }
    
    if ([url.scheme isEqualToString:kWXPay_AppID]) {
        return [WXApi handleOpenURL:url delegate:self];
    }else if ([url.scheme isEqualToString:@"tencent1105265453"]) {
        return [QQApiInterface  handleOpenURL:url delegate:self];
    }else if ([url.scheme isEqualToString:@"wb1938399542"]) {
        return [WeiboSDK handleOpenURL:url delegate:self];
    }
    return YES;
}

#pragma mark - 微信 & 微博 delegate
/** 微信回调*/
- (void)onResp:(BaseResp *)resp {
    [PDOAuthLoginAndShareTool wxResp:resp];
    
    if ([resp isKindOfClass:[PayResp class]]){
        PayResp *response = (PayResp*)resp;
        
        /*
         0	成功
         -1	错误
         -2	用户取消
         */
        switch(response.errCode){
            case WXSuccess:
                //服务器端查询支付通知或查询API返回的结果再提示成功
                [[NSNotificationCenter defaultCenter] postNotificationName:kWXPAY_NOTIFACTION object:nil userInfo:@{@"result":@"success"}];
                break;
            default:
                [[NSNotificationCenter defaultCenter] postNotificationName:kWXPAY_NOTIFACTION object:nil userInfo:@{@"result":@"fail"}];
                break;
        }
    } else if ([resp isKindOfClass:[SendMessageToWXResp class]]) { // 微信分享的回调
        SendMessageToWXResp *response = (SendMessageToWXResp *)resp;
        switch (response.errCode) {
            case WXSuccess:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postShareNotification" object:nil userInfo:@{@"result":@"success"}];
                break;
            default:
                [[NSNotificationCenter defaultCenter] postNotificationName:@"postShareNotification" object:nil userInfo:@{@"result":@"fail"}];
                break;
        }
    } else if ([resp isKindOfClass:[SendMessageToQQResp class]]) {
        SendMessageToQQResp *response = (SendMessageToQQResp *)resp;
        if (response.result.intValue == 0) { // 分享成功
            [[NSNotificationCenter defaultCenter] postNotificationName:@"postShareNotification" object:nil userInfo:@{@"result":@"success"}];
        } else {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"postShareNotification" object:nil userInfo:@{@"result":@"fail"}];
        }
    }
}

/** 微博回调*/
- (void)didReceiveWeiboResponse:(WBBaseResponse *)response {
    [PDOAuthLoginAndShareTool wbResp:response];
}

- (void)didReceiveWeiboRequest:(WBBaseRequest *)request {
    
}

#pragma mark - New Feature
-(void)createStartAnimation{
    // 1. 判断是否第一次使用此版本
    NSString *versionKey = @"CFBundleShortVersionString";
    NSString *lastVersionCode = [[NSUserDefaults standardUserDefaults] objectForKey:versionKey];
    NSString *currentVersionCode = [[NSBundle mainBundle].infoDictionary objectForKey:versionKey];
    if ([lastVersionCode isEqualToString:currentVersionCode]) {                 // 非第一次使用软件
        [UIApplication sharedApplication].statusBarHidden = NO ;
        [self startAnimtion:NO];
    } else {                                                                    // 第一次使用软件
        [[NSUserDefaults standardUserDefaults] setObject:currentVersionCode forKey:versionKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
        // 跳转开场动画
//        PDNewFeatureViewController *newFeatureViewController = [[PDNewFeatureViewController alloc] init];
//        newFeatureViewController.startBlock = ^(){
//            [self startAnimtion:NO];
//        };

//        PDWelcomeViewController *welcomeViewController = [[PDWelcomeViewController alloc] init];
//        welcomeViewController.startBlock = ^(){
            [self startAnimtion:NO];
//        };
        
//        self.window.rootViewController = welcomeViewController;
    }
}

#pragma mark 跳转主界面
-(void)startAnimtion:(BOOL)isBunding{
    [UIApplication sharedApplication].statusBarHidden = NO;

    self.window.rootViewController = [PDSideMenu setupSlider];
    [PDTabBarController sharedController].delegate = self;
    
    PDLaungchViewController *launchVC = [[PDLaungchViewController alloc] init];
    [self.window addSubview:launchVC.view];
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
//    if ([viewController.tabBarItem.title isEqualToString:@"竞猜"]){
//        [[PDTabBarController sharedController] showMenu];
//        return NO;
//    }

    if ([AccountModel sharedAccountModel].isShenhe && [viewController.tabBarItem.title isEqualToString:@"娱乐"]){
        if ([AccountModel sharedAccountModel].hasMemberLoggedIn){
            PDPandaRoleDetailViewController *pandaRolDetailViewController = [[PDPandaRoleDetailViewController alloc]init];
            pandaRolDetailViewController.transferLolGameUserId =             [AccountModel sharedAccountModel].lolGameUser;
            PDNavigationController *lotteryRootNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
            [lotteryRootNav setViewControllers:@[pandaRolDetailViewController]];
            
            lotteryRootNav.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
            lotteryRootNav.navigationBar.layer.shadowOpacity = 2.0;
            lotteryRootNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
            
            [[PDTabBarController sharedController] presentViewController:lotteryRootNav animated:YES completion:NULL];
            return NO;
        } else {
            [[PDAlertView sharedAlertView]showAlertWithTitle:@"请先登录" conten:@"登录后可查看召唤师信息哦" isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
            }];
            return NO;
        }
    }

    return YES;
}




#pragma mark 禁止横屏
//- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window {
//    return UIInterfaceOrientationMaskPortrait;
//}



#pragma mark - 友盟埋点
- (void)umengTrack {
    [MobClick setLogEnabled:YES];
    UMConfigInstance.appKey = @"5805b84867e58e91eb00251d";
    UMConfigInstance.secret = @"secretstringaldfkals";
    [MobClick startWithConfigure:UMConfigInstance];
}

#pragma mark - 添加买点
-(void)addGrowingIOManager{
    [Growing startWithAccountId:@"90adcfc54f46b3fb"];
}
@end
