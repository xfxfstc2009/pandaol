//
//  PDGuideSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

typedef NS_ENUM(NSInteger,GuideGuesterType) {
    GuideGuesterTypeNormal,
    GuideGuesterTypeTap,                    /**< 点击手势*/
    GuideGuesterTypeDrag,                   /**< 拖动手势*/
    GuideGuesterTypeDrop,                   /**< 下拉*/
    GuideGuesterTypeSwipe,                  /**< 滑动*/
};

typedef NS_ENUM(NSInteger,foundationType){
    foundationTypeRect        = 1 << 2,   /*! 方形 */
    foundationTypeCircle      = 1 << 3,   /*! 圆形 */
};

typedef NS_ENUM(NSInteger,GuideType) {
    GuideTypeLotteryNormal = 0,
    GuideTypeLotteryGame = 1,
};

@interface PDGuideSingleModel : FetchModel

@property (nonatomic,strong)UIView *transferView;                   /**< 传递过去的view*/
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,copy)NSString *text;                           /**< 传递的内容*/
@property (nonatomic,assign)BOOL isAction;                          /**< 判断是否可以真实点击*/
@property (nonatomic,assign)GuideGuesterType guideType;             /**< 显示的手势类型*/
@property (nonatomic,assign)CGRect transferShowFrame;               /**< 传入的frame*/
@property (nonatomic,assign)foundationType foundationType;          /**< 边框的frame*/
@property (nonatomic,assign)CGRect transferPositionFrame;           /**< 焦点的frame*/
@property (nonatomic,assign)GuideType transferGuideType;            /**< 引导类型*/

// temp
@property (nonatomic,strong)UISlider *tempSlider;
@property (nonatomic,strong)UIButton *tempActionButton;
-(void)actionClickWithBlock:(void(^)())block;
@end
