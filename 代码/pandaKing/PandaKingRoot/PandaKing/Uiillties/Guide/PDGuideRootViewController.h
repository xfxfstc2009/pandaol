//
//  PDGuideRootViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDGuideSingleModel.h"


@protocol PDGuideRootViewControllerDelegate <NSObject>

-(void)actionClickWithGuide:(PDGuideSingleModel *)guideSingleModel;
-(void)actionDismissGuide;                                              /**< 停止引导*/
@end


@interface PDGuideRootViewController : AbstractViewController

// 相关属性

@property (nonatomic,weak)id<PDGuideRootViewControllerDelegate> delegate;


- (void)showInView:(UIViewController *)viewController withViewActionArr:(NSArray *)viewArr;
- (void)dismissFromView:(UIViewController *)viewController;
-(void)sheetViewDismiss;

#pragma mark - 点击气泡绑定召唤师
-(void)actionToClickPaopaoBindingRoleBlock:(void(^)())block;
#pragma mark - 赛事猜方法
-(void)actionClicDismisWithGameLotteryManager:(void(^)())block;
#pragma mark - 下拉方法结束后
-(void)bindingEndDropManager:(void(^)())block;
#pragma mark - 赛事tabbar
-(void)lotteryTabBarManager:(void(^)())block;
#pragma mark - 绑定召唤师
-(void)bindingGameUserManager:(void(^)())block;
#pragma mark - 赛事猜主页
-(void)lotteryRootManagerBlock:(void(^)())block;
#pragma mark - 赛事详情
-(void)lotteryDetailManagerBlock:(void(^)())block;
#pragma mark - 赛事投注
-(void)lotteryDetailTouzhuManagerBlock:(void(^)())block;
#pragma mark - 赛事投注 - 点击确定
-(void)lotteryDetailTouzhuActionManagerBlock:(void(^)())block;

@end
