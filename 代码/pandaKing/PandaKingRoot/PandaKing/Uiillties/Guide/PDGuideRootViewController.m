//
//  PDGuideRootViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDGuideRootViewController.h"

#define sWidth  CGRectGetWidth(self.view.frame)
#define sHeigth CGRectGetHeight(self.view.frame)
#define WINSIZE [UIScreen mainScreen].bounds.size
#define arrowsImageViewW 60
#define arrowsImageViewH 46
#define titleLabW (kScreenBounds.size.width / 2.)

/*! 引导描述距离 guideView 边框大小 */
#define Margin 10.0

static char actionToClickPaopaoBindingRoleKey;
static char lotteryGameListKey;
static char bindingEndDropKey;
static char tabBarLotteryKey;
static char bindingGameUserKey;
static char boundGameUserKey;
static char matchGuessKey;
static char lotteryDetailManagerBlockkey;
static char lotteryDetailTouzhuManagerBlockKey;
static char lotteryDetailTouzhuActionManagerBlockKey;               /**< 赛事猜-点击确定*/
typedef enum{
    Left  = 1,
    Right = 1 << 1,
    Upper = 1 << 2,
    Down  = 1 << 3
}InfoLocation;

@interface PDGuideRootViewController ()
@property (nonatomic,strong)NSMutableArray *guidesArr;

// temp
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIView *backgroundView;
@property (nonatomic,strong)UIImageView *showView;
@property (nonatomic,strong)UIImage *backgroundImage;
@property (nonatomic,assign)NSInteger currentIndex;                 // 当前的指向
@property (nonatomic, assign) InfoLocation location;
@property (nonatomic, assign) CGFloat space;                        /*! 引导描述与引导框距离，默认为 10 */
@property (nonatomic,strong)UILabel *infoLabel;
@property (nonatomic,strong)PDImageView *guideImgView;              /**< 引导View*/
@property (nonatomic,strong)PDImageView *topImgView;
@property (nonatomic,strong)PDImageView * bottomImgView;


// customer
@property (nonatomic, assign) CGFloat titleLabH;
@property (nonatomic,strong)PDImageView *arrowsImageView;
@end

@implementation PDGuideRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createSheetView];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self showGuide];
}

#pragma mark - createActionSheetView
-(void)createSheetView{
    self.view.backgroundColor = [UIColor clearColor];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.modalPresentationCapturesStatusBarAppearance = YES;
    
    // 创建背景色
    self.backgroundView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.85f];
    [self.view addSubview:self.backgroundView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
    tapGestureRecognizer.numberOfTapsRequired = 1;
    [self.backgroundView addGestureRecognizer:tapGestureRecognizer];

    self.space = 10.;
    
    [self createTopView];
}

-(void)createTopView{
    // 引导描述
    self.infoLabel = [[UILabel alloc] init];
    self.infoLabel.textColor = [UIColor whiteColor];
    self.infoLabel.numberOfLines = 0;
    self.infoLabel.font = [UIFont systemFontOfCustomeSize:15.];
    [self.backgroundView addSubview:self.infoLabel];
    
    // 2. 创建引导图片
    self.guideImgView = [[PDImageView alloc]init];
    self.guideImgView.backgroundColor = [UIColor clearColor];
    UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;
    [keywindow addSubview:self.guideImgView];
    
    // 3. 创建top
    self.topImgView = [[PDImageView alloc]init];
    self.topImgView.backgroundColor = [UIColor clearColor];
    self.topImgView.image = [UIImage imageNamed:@"top_gesture"];
    self.topImgView.hidden = YES;
    [keywindow addSubview:self.topImgView];
    
    // 4. bottomView
    self.bottomImgView = [[PDImageView alloc]init];
    self.bottomImgView.backgroundColor = [UIColor clearColor];
    self.bottomImgView.image = [UIImage imageNamed:@"bottom_gesture"];
    self.bottomImgView.hidden = YES;
    [keywindow addSubview:self.bottomImgView];
}


#pragma mark- Normal Manager
#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgroundView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak PDGuideRootViewController *weakVC = self;
    [self.guideImgView removeFromSuperview];
    [self.bottomImgView removeFromSuperview];
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
        
        // 赛事猜退出 dismiss
        void(^lotteryGameListBlock)() = objc_getAssociatedObject(self, &lotteryGameListKey);
        if (lotteryGameListBlock){
            lotteryGameListBlock();
        }
        
        // tabbar点击事件
        void(^tabbarLotteryBlock)() = objc_getAssociatedObject(self, &tabBarLotteryKey);
        if (tabbarLotteryBlock){
            tabbarLotteryBlock();
        }
        
        // 去绑定召唤师
        void (^actionToClickPaopaoBindingRoleBlock)() = objc_getAssociatedObject(self, &actionToClickPaopaoBindingRoleKey);
        if (actionToClickPaopaoBindingRoleBlock){
            // 1. 保存当前
            [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeToBindGameUser block:NULL];
            actionToClickPaopaoBindingRoleBlock();
        }
        
        // 绑定召唤师
        void(^bindingGameUserKeyBlock)() = objc_getAssociatedObject(self, &bindingGameUserKey);
        if (bindingGameUserKeyBlock){
            // 1. 保存当前
            [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeBindingGameUser block:NULL];
            bindingGameUserKeyBlock();
        }
        
        // 绑定结束
        void(^bindingEndDropBlock)() = objc_getAssociatedObject(self, &bindingEndDropKey);
        // 1. 保存当前
        [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeBindGameUser block:NULL];
        if (bindingEndDropBlock){
            bindingEndDropBlock();
        }
        
        // 下拉气泡
        void(^boundGameUserKeyBlock)() = objc_getAssociatedObject(self, &boundGameUserKey);
        if (boundGameUserKeyBlock){
            // 1. 保存当前
            [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeBindGameUser block:NULL];
            boundGameUserKeyBlock();
        }
        
        // 赛事猜
        void(^matchGuessKeyBlock)() = objc_getAssociatedObject(self, &matchGuessKey);
        if (matchGuessKeyBlock){
            // 1. 保存当前
            [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeMatchGuess block:NULL];
            matchGuessKeyBlock();
        }
        
        // 赛事猜详情
        void(^lotteryDetailManagerBlock)() = objc_getAssociatedObject(self, &lotteryDetailManagerBlockkey);
        if (lotteryDetailManagerBlock){
            // 1. 保存当前
            [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeMatchGuess2 block:NULL];
            lotteryDetailManagerBlock();
        }
        
        // 赛事猜点击确定
        void(^lotteryDetailTouzhuActionManagerBlock)() = objc_getAssociatedObject(self, &lotteryDetailTouzhuActionManagerBlockKey);
        if (lotteryDetailTouzhuActionManagerBlock){
            // 1. 保存当前
            [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeMatchGuess3 block:NULL];
            lotteryDetailTouzhuActionManagerBlock();
        }
    });
}

#pragma mark - actionClick
-(void)viewDismiss{
    [self dismissFromView:_showViewController];
}

// 显示view
- (void)showInView:(UIViewController *)viewController withViewActionArr:(NSArray *)viewArr{
    self.guidesArr = [viewArr copy];
    
//    __weak PDGuideRootViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
}

- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}

#pragma mark - actionClick
-(void)sheetViewDismiss{

    if (self.guidesArr.count && (self.guidesArr.count > self.currentIndex)){
        PDGuideSingleModel *guideModel = [self.guidesArr objectAtIndex:self.currentIndex];
        if (guideModel.transferGuideType == GuideTypeLotteryGame){
            return;
        }
    }
    
    [self nextGuide:nil];
}




-(void)showGuide{
    self.currentIndex = 0;
    [self guideWithIndex:self.currentIndex];
}

- (void)guideWithIndex:(NSInteger)index {
    if (self.guidesArr.count == 0){
        [self nextGuide:nil];
        return;
    }
    // 1. 获取model
    PDGuideSingleModel *guideModel = [self.guidesArr objectAtIndex:index];
    
    // 2. 获取最新的frame
    CGRect newRect;
    NSValue *value = [NSValue valueWithCGRect:guideModel.transferShowFrame];
    if (guideModel.transferShowFrame.size.height != 0){
        newRect = [value CGRectValue];
    } else {
        newRect = guideModel.transferView.frame;
    }
//    
//    if (guideModel.transferPositionFrame.size.width){
//        newRect = guideModel.transferPositionFrame;
//    }
    
    // 【设置新的frame】
    [self resetSubViewsFrameWithBtnFrame:newRect text:guideModel.text];
    self.guideImgView.hidden = YES;
    if (guideModel.guideType == GuideGuesterTypeTap){
        [self tapGuestureManagerWithModel:guideModel];
    } else if (guideModel.guideType == GuideGuesterTypeDrag){
        [self dragGuestureManagerWithModel:guideModel];
    } else if (guideModel.guideType == GuideGuesterTypeDrop){
        [self dropGuestureManagerWithModel:guideModel];
    } else if (guideModel.guideType == GuideGuesterTypeSwipe){      // 滑动
        [self swipeGuestureManagerWithModel:guideModel];
    }

    // 添加view
    if (guideModel.transferView){
        guideModel.transferView.frame = guideModel.transferShowFrame;
        [self.view addSubview:guideModel.transferView];
        if (guideModel.tempSlider){
            [guideModel.tempSlider addTarget:self action:@selector(sliderChangedManager) forControlEvents:UIControlEventTouchUpInside];
        }
    }
    
    UIBezierPath *shapePath;
    CGFloat lineWidth = 0.0;
    shapePath = [UIBezierPath bezierPathWithOvalInRect:newRect];
    if (guideModel.foundationType == foundationTypeRect){
        shapePath = [UIBezierPath bezierPathWithRect:newRect];
    } else if (guideModel.foundationType == foundationTypeCircle){
        shapePath = [UIBezierPath bezierPathWithOvalInRect:newRect];
    }
    
    // 添加圆形空白处
    CAShapeLayer *layer = [CAShapeLayer layer];
    UIBezierPath *bezier = [UIBezierPath bezierPathWithRect:self.backgroundView.bounds];
    [bezier appendPath:[shapePath bezierPathByReversingPath]];
    layer.path = bezier.CGPath;
    layer.lineWidth = lineWidth;
    layer.lineDashPattern = @[@5,@5];
    layer.strokeColor = [UIColor redColor].CGColor;
    layer.fillColor = [UIColor redColor].CGColor;
    self.backgroundView.layer.mask = layer;

}

-(void)nextGuide:(UIButton *)sender{
    self.currentIndex ++;
    if (self.currentIndex < self.guidesArr.count){
        self.topImgView.hidden = YES;
        self.bottomImgView.hidden = YES;
        [self guideWithIndex:self.currentIndex];
        return;
    }
    
    // 进行消失
    [self dismissFromView:_showViewController];
    if (self.delegate && [self.delegate respondsToSelector:@selector(actionDismissGuide)]){
        [self.delegate actionDismissGuide];
    }
}

#pragma mark 引导描述放于中心上下的一个边
- (void)addMessage0:(NSString *)message nearRect:(CGRect)rect{
    CGPoint center = CGPointMake(CGRectGetMidX(rect),
                                 CGRectGetMidY(rect));
    self.location = center.x > sWidth - center.x ? Left : Right;
    self.location |= (sHeigth - center.y) > sHeigth/2 ? Down : Upper;
    
    // 文字最大显示区域
    CGSize size = CGSizeMake(self.location & Left ?  center.x : sWidth - center.x - Margin,
                             self.location & Upper ? CGRectGetMinY(rect) - self.space : sHeigth - (self.space + CGRectGetMaxY(rect)));
    // 文字长宽
    CGRect msgRect = [message boundingRectWithSize:size
                                           options:NSStringDrawingUsesLineFragmentOrigin
                                        attributes:@{NSFontAttributeName : self.infoLabel.font}
                                           context:nil];
    CGFloat labY = self.location & Upper ? CGRectGetMinY(rect) - self.space - CGRectGetHeight(msgRect) : CGRectGetMaxY(rect) + self.space;
    CGFloat labX = self.location & Left ? center.x - CGRectGetWidth(msgRect) + Margin : center.x;
    CGRect labRect = CGRectMake(labX, labY, msgRect.size.width, msgRect.size.height);
    self.infoLabel.frame = labRect;
    self.infoLabel.text = message;
}


#pragma mark -Animation
#pragma mark - 点击手势
-(void)tapGuestureManagerWithModel:(PDGuideSingleModel *)guideModel{
    // 2. 获取最新的frame
    CGRect newRect;
    NSValue *value = [NSValue valueWithCGRect:guideModel.transferShowFrame];
    if (guideModel.transferShowFrame.size.height != 0){
        newRect = [value CGRectValue];
    } else {
        newRect = guideModel.transferView.frame;
    }
    
    if (guideModel.transferPositionFrame.size.width){
        newRect = guideModel.transferPositionFrame;
    }
    
    self.guideImgView.hidden = NO;
    self.guideImgView.image = [UIImage imageNamed:@"tap_gesture"];
    self.guideImgView.frame = CGRectMake(newRect.size.width / 3 * 2., newRect.size.height / 2. + newRect.size.height / 4., LCFloat(55), LCFloat(68));
    // 1. 手势
    CGPoint fromPoint = CGPointMake(newRect.origin.x + newRect.size.width / 3 * 2. + 1.5, newRect.origin.y + newRect.size.height / 2. + newRect.size.height / 4. + 1.5);
    id fromValue = [NSValue valueWithCGPoint:fromPoint];
    CGPoint toPoint = CGPointMake(newRect.origin.x + newRect.size.width / 3 * 2. - 1.5, newRect.origin.y + newRect.size.height / 2. + newRect.size.height / 4. - 1.5);
    id toValue = [NSValue valueWithCGPoint:toPoint];
    [self animationWithView:self.guideImgView keyPath:@"position" fromValue:fromValue toValue:toValue duration:.6f];
}

#pragma mark - 点击手势
-(void)dragGuestureManagerWithModel:(PDGuideSingleModel *)guideModel{
    // 2. 获取最新的frame
    CGRect newRect;
    NSValue *value = [NSValue valueWithCGRect:guideModel.transferShowFrame];
    if (guideModel.transferShowFrame.size.height != 0){
        newRect = [value CGRectValue];
    } else {
        newRect = guideModel.transferView.frame;
    }
    
    self.guideImgView.hidden = NO;
    self.guideImgView.image = [UIImage imageNamed:@"drag_gresture"];
    self.guideImgView.frame = CGRectMake(newRect.size.width / 2., newRect.size.height / 2. + newRect.size.height / 4., LCFloat(55), LCFloat(68));
    // 1. 手势
    CGPoint fromPoint = CGPointMake(newRect.origin.x + newRect.size.width / 4 * 3., newRect.origin.y + newRect.size.height / 2. + newRect.size.height / 16.);
    id fromValue = [NSValue valueWithCGPoint:fromPoint];
    CGPoint toPoint = CGPointMake(newRect.origin.x + newRect.size.width / 4 * 3, newRect.origin.y + newRect.size.height / 2. - newRect.size.height / 16. );
    id toValue = [NSValue valueWithCGPoint:toPoint];
    
    self.topImgView.frame = CGRectMake(toPoint.x, newRect.origin.y, LCFloat(34), LCFloat(83));
    self.bottomImgView.frame = CGRectMake(toPoint.x,newRect.origin.y + newRect.size.height - LCFloat(83), LCFloat(34), LCFloat(83));
    self.topImgView.hidden = NO;
    self.bottomImgView.hidden = NO;
    [self animationWithView:self.guideImgView keyPath:@"position" fromValue:fromValue toValue:toValue duration:.6f];
}

#pragma mark - 下拉手势
-(void)dropGuestureManagerWithModel:(PDGuideSingleModel *)guideModel{
    // 2. 获取最新的frame
    CGRect newRect;
    NSValue *value = [NSValue valueWithCGRect:guideModel.transferShowFrame];
    if (guideModel.transferShowFrame.size.height != 0){
        newRect = [value CGRectValue];
    } else {
        newRect = guideModel.transferView.frame;
    }
    
    self.guideImgView.hidden = NO;
    self.guideImgView.image = [UIImage imageNamed:@"drag_gresture"];
    self.guideImgView.frame = CGRectMake(newRect.size.width / 2., newRect.size.height / 2. + newRect.size.height / 4., LCFloat(55), LCFloat(68));
    // 1. 手势
    CGPoint fromPoint = CGPointMake(newRect.origin.x + newRect.size.width / 4 * 3., newRect.origin.y + newRect.size.height / 2. + newRect.size.height / 16.);
    id fromValue = [NSValue valueWithCGPoint:fromPoint];
    CGPoint toPoint = CGPointMake(newRect.origin.x + newRect.size.width / 4 * 3, newRect.origin.y + newRect.size.height / 2. - newRect.size.height / 16. );
    id toValue = [NSValue valueWithCGPoint:toPoint];
    
    self.bottomImgView.frame = CGRectMake(newRect.origin.x + newRect.size.width / 2. - LCFloat(17),newRect.origin.y + newRect.size.height - LCFloat(83), LCFloat(34), LCFloat(83));
    self.bottomImgView.hidden = NO;
    [self animationWithView:self.guideImgView keyPath:@"position" fromValue:fromValue toValue:toValue duration:.6f];
}

#pragma mark 滑动手势
-(void)swipeGuestureManagerWithModel:(PDGuideSingleModel *)guideModel{
    // 2. 获取最新的frame
    CGRect newRect;
    NSValue *value = [NSValue valueWithCGRect:guideModel.transferShowFrame];
    if (guideModel.transferShowFrame.size.height != 0){
        newRect = [value CGRectValue];
    } else {
        newRect = guideModel.transferView.frame;
    }
    
    if (guideModel.transferPositionFrame.size.width){
        newRect = guideModel.transferPositionFrame;
    }
    
    self.guideImgView.hidden = NO;
    UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;
    [keywindow addSubview:self.guideImgView];
    self.guideImgView.image = [UIImage imageNamed:@"drag_gresture"];
    self.guideImgView.frame = CGRectMake(newRect.origin.x, newRect.origin.y + newRect.size.height, LCFloat(55), LCFloat(68));
    // 1. 手势
    CGPoint fromPoint = CGPointMake(newRect.origin.x , newRect.origin.y + newRect.size.height);
    id fromValue = [NSValue valueWithCGPoint:fromPoint];
    CGPoint toPoint = CGPointMake(newRect.origin.x + 100, newRect.origin.y + newRect.size.height);
    id toValue = [NSValue valueWithCGPoint:toPoint];

    self.bottomImgView.hidden = YES;
    [self animationWithView:self.guideImgView keyPath:@"position" fromValue:fromValue toValue:toValue duration:.6f];
}


- (void)animationWithView:(UIView *)view keyPath:(NSString *)keyPath fromValue:(id)fromValue toValue:(id)toValue{
    CAAnimation *animation = [self createAnimationWithKeyPath:keyPath fromValue:fromValue toValue:toValue];
    [view.layer addAnimation:animation forKey:nil];
}

- (void)animationWithView:(UIView *)view keyPath:(NSString *)keyPath fromValue:(id)fromValue toValue:(id)toValue duration:(CGFloat)duration{
    CAAnimation *animation = [self createAnimationWithKeyPath:keyPath fromValue:fromValue toValue:toValue];
    animation.duration = duration;
    [view.layer addAnimation:animation forKey:nil];
}

- (CAAnimation *)createSAnimationWithKeyPath:(NSString *)keyPath fromValue:(id)fromValue toValue:(id)toValue{
    CAMediaTimingFunction *mediaTiming = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:keyPath];
    animation.timingFunction = mediaTiming;
    animation.duration = 0.2;
    animation.repeatCount = 1;
    animation.fromValue =  fromValue;// 起始角度
    animation.toValue = toValue; // 终止角度
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    return animation;
}

- (CAAnimation *)createKAnimationWithKeyPath:(NSString *)keyPath fromValue:(id)fromValue toValue:(id)toValue{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:keyPath];
    animation.duration = 2;
    animation.calculationMode = kCAAnimationCubic;
    animation.repeatCount = HUGE_VALF;
    animation.values = @[fromValue, fromValue, @(-[toValue floatValue]/ 2.0), toValue, fromValue, fromValue];
    animation.keyTimes = @[@(0), @(0.075), @(0.09), @(0.13), @(0.16), @(1)];
    return animation;
}

- (CAAnimation *)createAnimationWithKeyPath:(NSString *)keyPath fromValue:(id)fromValue toValue:(id)toValue{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:keyPath];
    animation.duration = 1.5; // 持续时间
    
    CAMediaTimingFunction *mediaTiming = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    animation.timingFunction = mediaTiming;
    animation.repeatCount = HUGE_VALF; // 重复次数
    animation.fromValue =  fromValue;// 起始角度
    animation.toValue = toValue; // 终止角度
    animation.autoreverses = YES;
    return animation;
}


#pragma mark - 赛事猜方法
-(void)actionClicDismisWithGameLotteryManager:(void(^)())block{
    objc_setAssociatedObject(self, &lotteryGameListKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
#pragma mark - 下拉方法结束后
-(void)bindingEndDropManager:(void(^)())block{
    objc_setAssociatedObject(self, &bindingEndDropKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
#pragma mark - 赛事tabbar
-(void)lotteryTabBarManager:(void(^)())block{
    objc_setAssociatedObject(self, &tabBarLotteryKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 点击气泡绑定召唤师
-(void)actionToClickPaopaoBindingRoleBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionToClickPaopaoBindingRoleKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 下拉气泡
-(void)actionToboundGameUser:(void(^)())block{
    objc_setAssociatedObject(self, &boundGameUserKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 绑定召唤师
-(void)bindingGameUserManager:(void(^)())block{
    objc_setAssociatedObject(self, &bindingGameUserKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
#pragma mark - 赛事猜主页
-(void)lotteryRootManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &matchGuessKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
#pragma mark - 赛事详情
-(void)lotteryDetailManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &lotteryDetailManagerBlockkey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 赛事投注- 滑块
-(void)lotteryDetailTouzhuManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &lotteryDetailTouzhuManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 赛事投注 - 点击确定
-(void)lotteryDetailTouzhuActionManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &lotteryDetailTouzhuActionManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark

#pragma mark - 滑块进行滑动方法
-(void)sliderChangedManager{
    void (^lotteryDetailTouzhuManagerBlock)() = objc_getAssociatedObject(self, &lotteryDetailTouzhuManagerBlockKey);
    if (lotteryDetailTouzhuManagerBlock){
        lotteryDetailTouzhuManagerBlock();
    }
    
    // 进行下一步
    [self nextGuide:nil];
}










#pragma mark - Customer Manager
- (void)resetSubViewsFrameWithBtnFrame:(CGRect)frame text:(NSString *)text{
    
    CGFloat centerX = WINSIZE.width/2;
    CGFloat centerY = WINSIZE.height/2;
    CGFloat x = frame.origin.x;
    CGFloat y = frame.origin.y;
    CGFloat btnW = frame.size.width;
    CGFloat btnH = frame.size.height;
    
    CGFloat arrowsX;
    CGFloat arrowsY;
    CGFloat titleLabX;
    CGFloat titleLabY;
    self.infoLabel.text = text;
    CGSize titleHeight = [self.infoLabel.text sizeWithCalcFont:self.infoLabel.font constrainedToSize:CGSizeMake(titleLabW, CGFLOAT_MAX)];
    self.titleLabH = titleHeight.height;
    
    CGFloat angle;
    if (x<centerX && y<centerY) {
        //左上
        //箭头旋转180‘
        angle = M_PI;
        arrowsX = x+btnW/2;
        arrowsY = y+btnH;
//        titleLabX = arrowsX+arrowsImageViewW;
        titleLabX = arrowsX;
        titleLabY = arrowsY + 70;
    }else if(x>=centerX && y<=centerY){
        //右上
        //箭头旋转-90‘
        angle = -M_PI_2;
        arrowsX = x-arrowsImageViewW/2;
        arrowsY = y+btnH;
        titleLabX = arrowsX - titleLabW / 2. - LCFloat(22);
        titleLabY = arrowsY+arrowsImageViewH;
        
        
    }else if(x>=centerX && y>=centerY){
        //右下
        //箭头不旋转
        angle = 0;
        arrowsX = x;
        arrowsY = y-arrowsImageViewH;
        titleLabX = arrowsX-titleLabW+20;
        titleLabY = arrowsY-self.titleLabH+20;
        
        
    }else if(x<centerX && y>centerY){
        //左下
        angle = M_PI_2;
        arrowsX = x + frame.size.width / 2.;
        arrowsY = y - 60;
        titleLabX = arrowsX;
        titleLabY = arrowsY-self.titleLabH;
    }
    
    self.arrowsImageView.transform = CGAffineTransformMakeRotation(angle);
    self.arrowsImageView.frame = CGRectMake(arrowsX, arrowsY, arrowsImageViewW, arrowsImageViewH);
    self.infoLabel.frame = CGRectMake(titleLabX, titleLabY, titleLabW, self.titleLabH);
    [self.view addSubview:self.arrowsImageView];
    [self startArrowsAnimation];
    [self.view addSubview:self.infoLabel];
    //[self.view layoutSubviews];
    [self.view layoutIfNeeded];
    
}

- (void)startArrowsAnimation{
    if (self.arrowsImageView.animationImages == nil) {
        NSMutableArray *images = [[NSMutableArray alloc]init];
        for (int i = 1; i<=8; i++) {
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat: @"userGuide_arrors%d",i]];
            [images addObject:image];
        }
        self.arrowsImageView.animationImages = images;
        self.arrowsImageView.animationDuration = 0.6;
        self.arrowsImageView.animationRepeatCount = 0;
    }
    
    [self.arrowsImageView startAnimating];
}

- (void)stopArrowsAnimation{
    if ([self.arrowsImageView isAnimating]) [self.arrowsImageView stopAnimating];
}

-(PDImageView *)arrowsImageView {
    if (!_arrowsImageView) {
        _arrowsImageView = [[PDImageView alloc]init];
    }
    return _arrowsImageView;
    
}
@end
