//
//  PDSQLBindingSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/5.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDSQLBindingSingleModel : FetchModel

@property (nonatomic,assign)NSInteger djlsh;
@property (nonatomic,copy)NSString *server_id;
@property (nonatomic,copy)NSString *server_name;
@property (nonatomic,copy)NSString *other_name;
@property (nonatomic,copy)NSString *telecom_or_netcom;
@property (nonatomic,assign)PDBindStatus type;
@property (nonatomic,copy)NSString *img;
@property (nonatomic,copy)NSString *role_name;
@property (nonatomic,copy)NSString *user_id;
@property (nonatomic,copy)NSString *update_time;
@property (nonatomic,assign)BOOL isActivity;                    /**< 是否活跃*/

@end
