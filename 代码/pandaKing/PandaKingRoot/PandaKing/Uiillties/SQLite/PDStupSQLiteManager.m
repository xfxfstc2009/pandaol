//
//  PDStupSQLiteManager.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDStupSQLiteManager.h"
#import <sqlite3.h>

#define sqliteName @"mydatabase.sqlite"
#define binding @"Binding"

@implementation PDStupSQLiteManager

+(void)steupSqlite{
    // 1. 寻找当前程序锁在沙盒下的Document
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    // 2.取得数组里面的第一个路径值
    NSString *documentsPath=[paths objectAtIndex:0];
    // 3.附加数据库文件名称
    NSString *path=[documentsPath stringByAppendingPathComponent:sqliteName];
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(!ifFind) {
        NSString * srcPath= [[NSBundle mainBundle]pathForResource:sqliteName ofType:nil];
        
        [fm copyItemAtPath:srcPath toPath:path error:nil];
    }
}


#pragma mark - 插入数据
+(BOOL)insetIntoInfoWithSqlStr:(NSString *)sqlString{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath=[paths objectAtIndex:0];
    NSString *path=[documentsPath stringByAppendingPathComponent:sqliteName];
    NSFileManager *fm=[NSFileManager defaultManager];
    BOOL ifFind=[fm fileExistsAtPath:path];
    
    BOOL result=false;
    if (ifFind){
        sqlite3 *database;
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK) {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
        } else {
            char *errorMsg;
            if(sqlite3_exec(database, [sqlString UTF8String], NULL, NULL, &errorMsg)!=SQLITE_OK) {
                result = false;
            } else {
                result = true;
            }
            // 关闭数据库
            
            sqlite3_close(database);
        }
    }
    return result;
}

#pragma mark - 删除数据
+(BOOL)deleteInfoWithSqlStr:(NSString *)sqlString{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath=[paths objectAtIndex:0];
    NSString *path=[documentsPath stringByAppendingPathComponent:sqliteName];
    NSFileManager *fm=[NSFileManager defaultManager];
    bool ifFind=[fm fileExistsAtPath:path];
    if(ifFind) {
        sqlite3 *database;
        if(sqlite3_open([path UTF8String], &database)!=SQLITE_OK) {// 尝试去关闭一次，不管是什么失败
            sqlite3_close(database);
        } else {
            sqlite3_stmt *statement;
            
            if(sqlite3_prepare_v2(database, [sqlString UTF8String], -1, &statement, nil) == SQLITE_OK) {
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    sqlite3_finalize(statement);
                    sqlite3_close(database);
                    return YES;
                }
            }
            sqlite3_finalize(statement);
            sqlite3_close(database);
        }
    }
    return NO;
}




#pragma mark - 绑定
+(BOOL)bindingWithInsertInfo:(PDSQLBindingSingleModel *)model{
    NSString *insertSql = @"";
    insertSql = [NSString stringWithFormat:@"insert into Binding(server_id,server_name,other_name,telecom_or_netcom,type,img,role_name,user_id,update_time)values('%@','%@','%@','%@','%li','%@','%@','%@','%@')",model.server_id,model.server_name,model.other_name,model.telecom_or_netcom,(long)model.type,model.img,model.role_name,model.user_id,model.update_time];
    return [PDStupSQLiteManager insetIntoInfoWithSqlStr:insertSql];
}


#pragma mark - 获取绑定信息
+(NSMutableArray *)getErrBindingList{
    NSMutableArray *list = [NSMutableArray array];          // 返回值
    
    // 1.获取iPhone上sqlite3的数据库文件的地址(应用程序沙盒里面的Documents文件夹)
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:sqliteName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL find = [fileManager fileExistsAtPath:path];
    if (find) {
        // 2.打开iPhone上的sqlite3的数据库文件
        sqlite3 *database;
        // 判断数据库是否打开
        if(sqlite3_open([path UTF8String], &database) != SQLITE_OK) {
            sqlite3_close(database);
        } else {
            // 3.准备sql语句
            sqlite3_stmt *statement;
            
            NSString *sqlStr = @"select * from binding";
            
            // NSLog(@"query = %@",query);
            if(sqlite3_prepare_v2(database, [sqlStr UTF8String], -1, &statement, nil) == SQLITE_OK) {
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    // 返回值
                    PDSQLBindingSingleModel *item = [[PDSQLBindingSingleModel alloc] init];
                    
                    // id
                    item.djlsh = (int)sqlite3_column_int(statement ,0);
                    // server_id
                    item.server_id = [PDStupSQLiteManager smartChange:(char *) sqlite3_column_text(statement,  1)];
                    // server_name
                    item.server_name = [PDStupSQLiteManager smartChange:(char *) sqlite3_column_text(statement,  2)];
                    // other_name
                    item.other_name = [PDStupSQLiteManager smartChange:(char *) sqlite3_column_text(statement,  3)];
                    // telecom_or_netcom
                    item.telecom_or_netcom = [PDStupSQLiteManager smartChange:(char *) sqlite3_column_text(statement, 4)];
                    // type
                    item.type = (int)sqlite3_column_int(statement, 5);
                    // 6.img
                    item.img = [PDStupSQLiteManager smartChange:(char *) sqlite3_column_text(statement, 6)];
                    // 7. role_name
                    item.role_name = [PDStupSQLiteManager smartChange:(char *) sqlite3_column_text(statement, 7)];
                    // 8.user_id
                    item.user_id = [PDStupSQLiteManager smartChange:(char *) sqlite3_column_text(statement, 8)];
                    // 9.update_time
                    item.update_time = [PDStupSQLiteManager smartChange:(char *) sqlite3_column_text(statement, 9)];
                    
                    // 时间没处理
                    [list addObject:item];
                }
            }
            
            // 7.释放sql文资源
            sqlite3_finalize(statement);
            
            // 8.关闭iPhone上的sqlite3的数据库
            sqlite3_close(database);
        }
    }
    return list;
}


+(NSString *)smartChange:(char *)info{
    NSString *tempStr = @"";
    if (info != NULL){
        tempStr = [NSString stringWithUTF8String:info];
    } else {
        tempStr = @"";
    }
    return tempStr;
}


+(NSArray *)pubgSearchListInfo{
    NSMutableArray *list = [NSMutableArray array];          // 返回值
    
    // 1.获取iPhone上sqlite3的数据库文件的地址(应用程序沙盒里面的Documents文件夹)
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *path = [documentsDirectory stringByAppendingPathComponent:sqliteName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    BOOL find = [fileManager fileExistsAtPath:path];
    if (find) {
        // 2.打开iPhone上的sqlite3的数据库文件
        sqlite3 *database;
        // 判断数据库是否打开
        if(sqlite3_open([path UTF8String], &database) != SQLITE_OK) {
            sqlite3_close(database);
        } else {
            // 3.准备sql语句
            sqlite3_stmt *statement;
            
            NSString *sqlStr = @"select * from pubgSearch order by id desc";
            
            // NSLog(@"query = %@",query);
            if(sqlite3_prepare_v2(database, [sqlStr UTF8String], -1, &statement, nil) == SQLITE_OK) {
                while(sqlite3_step(statement) == SQLITE_ROW) {
                    NSString *info = [PDStupSQLiteManager smartChange:(char *) sqlite3_column_text(statement,  1)];
                    [list addObject:info];
                }
            }
            
            // 7.释放sql文资源
            sqlite3_finalize(statement);
            
            // 8.关闭iPhone上的sqlite3的数据库
            sqlite3_close(database);
        }
    }
    return list;

}

#pragma mark - 绑定
+(BOOL)pubgInsertStr:(NSString *)user{
    NSString *insertSql = @"";
    insertSql = [NSString stringWithFormat:@"insert into pubgSearch(user)values('%@')",user];
    return [PDStupSQLiteManager insetIntoInfoWithSqlStr:insertSql];
}

#pragma mark - 删除
+(BOOL)pubgDeleteStr:(NSString *)user{
    NSString *deleteSql = [NSString stringWithFormat:@"delete from pubgSearch where user = '%@'",user];
    return [PDStupSQLiteManager deleteInfoWithSqlStr:deleteSql];
}

#pragma mark - 删除全部
+(BOOL)pubgDeleteAll{
    NSString *deleteSql = [NSString stringWithFormat:@"delete from pubgSearch"];
    return [PDStupSQLiteManager deleteInfoWithSqlStr:deleteSql];
}
@end
