//
//  PDStupSQLiteManager.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDSQLBindingSingleModel.h"

@interface PDStupSQLiteManager : NSObject

+(void)steupSqlite;



#pragma mark - 绑定
+(BOOL)bindingWithInsertInfo:(PDSQLBindingSingleModel *)model;
// 获取绑定信息列表
+(NSMutableArray *)getErrBindingList;



#pragma mark - 搜索历史
+(NSArray *)pubgSearchListInfo;
#pragma mark - 绑定
+(BOOL)pubgInsertStr:(NSString *)user;
#pragma mark - 删除
+(BOOL)pubgDeleteStr:(NSString *)user;
+(BOOL)pubgDeleteAll;


@end
