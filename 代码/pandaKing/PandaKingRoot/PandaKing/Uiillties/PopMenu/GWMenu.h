//
//  GWMenuItem.h
//  魔境家居
//
//  Created by 裴烨烽 on 16/2/23.
//  Copyright © 2016年 mojing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GWMenuItem : NSObject

@property (readwrite, nonatomic, strong) UIImage *image;
@property (readwrite, nonatomic, strong) NSString *title;
@property (readwrite, nonatomic, weak) id target;
@property (readwrite, nonatomic) SEL action;
@property (readwrite, nonatomic, strong) UIColor *foreColor;
@property (readwrite, nonatomic) NSTextAlignment alignment;

+ (instancetype) menuItem:(NSString *) title image:(UIImage *) image target:(id)target  action:(SEL) action;

@end

@protocol GWMenuDelegate <NSObject>

-(void)imgBtnClick:(NSInteger)btnIndex;

@end

@interface GWMenu : NSObject

@property (nonatomic,weak)id<GWMenuDelegate> delegate;

+ (void) showMenuInView:(UIView *)view fromRect:(CGRect)rect  menuItems:(NSArray *)menuItems;
+ (void)showMenuInView:(UIView *)view fromRect:(CGRect)rect imgArr:(NSArray *)imgArr imgBlock:(void(^)(NSInteger imgIndex))imgIndexBlock addImgBlock:(void(^)())addImgBlock;

+ (void) dismissMenu;

+ (UIColor *) tintColor;
+ (void) setTintColor: (UIColor *) tintColor;

+ (UIFont *) titleFont;
+ (void) setTitleFont: (UIFont *) titleFont;

@end
