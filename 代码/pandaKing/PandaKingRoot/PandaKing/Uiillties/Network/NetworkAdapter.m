//
//  NetworkAdapter.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

// 网络适配器
#import "NetworkAdapter.h"
#import "NetworkEngine.h"


@implementation NetworkAdapter

+(instancetype)sharedAdapter{
    static NetworkAdapter *_sharedAdapter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAdapter = [[NetworkAdapter alloc] init];
    });
    return _sharedAdapter;
}

#pragma mark - 短链接
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block{
    __weak typeof(self)weakSelf = self;
    
    NetworkEngine *networkEngine;
    if ([path isEqualToString:pingguoshenhe]){
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:@"http://www.giganticwhale.top"]];
    } else if ([path hasPrefix:@"api"]){
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@/",@"baobab.wandoujia.com"]]];
    } else if ([path hasPrefix:postMessage]){
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@",Jungle_Host,Jungle_Port]]];
    } else {
#ifdef DEBUG
        NSString *address = [Tool userDefaultGetWithKey:testNet_address].length?[Tool userDefaultGetWithKey:testNet_address]:JAVA_Host;
        NSString *port = [Tool userDefaultGetWithKey:testNet_port].length?[Tool userDefaultGetWithKey:testNet_port]:JAVA_Port;
        if (port.length){
            networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@",address,port]]];
        } else {
            networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",address]]];
        }
#else
        if (JAVA_Port.length){
            networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@",JAVA_Host,JAVA_Port]]];
        } else {
            networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",JAVA_Host]]];
        }
#endif
    }
 
    [networkEngine fetchWithPath:path requestParams:requestParams responseObjectClass:responseObjectClass succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            [PDHUD dismissManager];
            if ([responseObject isKindOfClass:[NSDictionary class]]){           // 字典类别
                // 判断是否请求成功
                NSNumber *errorNumber = [responseObject objectForKey:@"code"];
                if (errorNumber.integerValue == 200){           // 请求成功
                    if ([path isEqualToString:thirdLogin] || [path isEqualToString:userBind] || [path isEqualToString:userContinue] || [path isEqualToString:@""] || [path isEqualToString:tokenRegister]){      // 【当返回token 就可以进行保存】
                        if ([responseObject isKindOfClass:[NSDictionary class]]){
                            NSString *token = [[responseObject objectForKey:@"data"] objectForKey:@"token"];
                            if (token.length){
                                [AccountModel sharedAccountModel].token = token;
                            }
                        }
                    }
                    
                    if (responseObjectClass == nil){
                        NSDictionary *dic = [responseObject objectForKey:@"data"];
                        block(YES,dic,nil);
                        return;
                    }
                    
                    FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:[responseObject objectForKey:@"data"]];
                    block(YES,responseModelObject,nil);
                    
                } else if (errorNumber.integerValue == 401){            // 重新登录
                    if ([path isEqualToString:messageState] || [path isEqualToString:slidermemberinfo] || [path isEqualToString:sliderList]){
                        return;
                    }
                    // 删除tokem
//                    [Tool userDefaultDelegtaeWithKey:CustomerToken];
                    [Tool userDefaulteWithKey:CustomerType Obj:@"guest"];
                    
                    
                    if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(pandaNotLoginNeedLoginManager)]){
                        [[NetworkAdapter sharedAdapter].delegate pandaNotLoginNeedLoginManager];
                    }
                } else {                                        // 业务错误
                    
                    id errInfo = [responseObject objectForKey:@"msg"];
                    if ([errInfo isKindOfClass:[NSString class]]){
                        NSString *errorInfo = [responseObject objectForKey:@"msg"];
                        if (!errorInfo.length){
                            errorInfo = @"系统错误，请联系管理员";
                        }
                        NSDictionary *dict = @{NSLocalizedDescriptionKey: errorInfo};
                        NSError *bizError = [NSError errorWithDomain:PDBizErrorDomain code:errorNumber.integerValue userInfo:dict];
                        //                    if (errorNumber.integerValue != 301){
                        [PDHUD showConnectionErr:errorInfo];
                        //                    }
                        block(NO,responseObject,bizError);
                        return;
                    } else {
                        return;
                    }
                }
            }
        }
    }];
}

#pragma mark - 长链接
#pragma mark - WebSocket 
// 【1.webSocket 连接】
-(void)webSocketConnection:(WebSocketConnection *)webSocketConnection host:(NSString *)host port:(NSInteger)port{
    if (webSocketConnection){
        [self webSocketCloseConnectionWithSoceket:webSocketConnection];
    }
    webSocketConnection = [[WebSocketConnection alloc]init];
    webSocketConnection.delegate = self;
    [webSocketConnection webSocketConnectWithHost:host port:port];
}

// 【2.断开长链接【WebSocket】】
-(void)webSocketCloseConnectionWithSoceket:(WebSocketConnection *)webSocketConnection{
    if (webSocketConnection){
        webSocketConnection.delegate = nil;
        [webSocketConnection webSocketDisconnect];
        webSocketConnection = nil;
    }
}

 // 【3.填写信息WebSocket】
-(void)webSocketFetchModelWithPHPRequestParams:(NSDictionary *)requestParams socket:(WebSocketConnection *)webSocketConnection{
    NSString *jsonParams = [Tool dictionaryToJson:requestParams];
    NSString *appendingParams = [NSString stringWithFormat:@"\r\n"];
    NSString *dataStr = [NSString stringWithFormat:@"%@%@",jsonParams,appendingParams];
    
    [webSocketConnection webSocketWriteData:dataStr];
}

//【4.WebSocket 内容信息返回】
//-(void)webSocket:(SRWebSocket *)webSocket didReceiveData:(id)message{
//#ifdef DEBUG
//    NSLog(@"RESPONSE 【WebSocket】 :%@", message );
//    if (isSocketLogAlert){
//        [[UIAlertView alertViewWithTitle:@"socket Log" message:message buttonTitles:@[@"确定"] callBlock:NULL]show];
//    }
//#endif
//    
//    if (_delegate && [_delegate respondsToSelector:@selector(webSocketDidBackData:)]){
//        [_delegate webSocketDidBackData:message];
//    }
//}




#pragma mark - 【SOCKET】
-(void)socketCloseConnectionWithSocket:(SocketConnection *)socketConnection{
    if (socketConnection){
        socketConnection.delegate = nil;
        [socketConnection disconnect];
        socketConnection = nil;
    }
}

#pragma mark 【写信息】
-(void)socketFetchModelWithJavaRequestParams:(NSDictionary *)requestParams socket:(SocketConnection *)socketConnection{
    NSString *jsonParams = [Tool dictionaryToJson:requestParams];
    
    NSData *data1 = [jsonParams dataUsingEncoding:NSUTF8StringEncoding];
    
    NSInteger jsonParamsLength = jsonParams.length;
    
    char str[4];
    str[0] = (char) ((jsonParamsLength >> 24) & 0xFF);
    str[1] = (char) ((jsonParamsLength >> 16)& 0xFF);
    str[2] = (char) ((jsonParamsLength >> 8)&0xFF);
    str[3] = (char) (jsonParamsLength & 0xFF);
    
    NSData *data2 = [NSData dataWithBytes:str length:4];
    NSMutableData *data3 = [NSMutableData dataWithData:data2];
    [data3 appendData:data1];
    
    [socketConnection writeData:data3 timeout:4 tag:socketConnection.port];
}



#pragma mark 写信息JAVA
-(void)didReceiveData:(NSData *)data tag:(long)tag{
#ifdef DEBUG
    NSString *result = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"RESPONSE Socket :%@", result);
    if (isSocketLogAlert){
        [[UIAlertView alertViewWithTitle:@"socket Log" message:result buttonTitles:@[@"确定"] callBlock:NULL]show];
    }
#endif
//    (unsigned long)data.length
    
    if (_delegate && [_delegate respondsToSelector:@selector(socketDidBackData:)]){
        [_delegate socketDidBackData:data];
    }
}

-(void)socketConnection:(SocketConnection *)socket host:(NSString *)host port:(NSInteger)port{
    
}


@end

