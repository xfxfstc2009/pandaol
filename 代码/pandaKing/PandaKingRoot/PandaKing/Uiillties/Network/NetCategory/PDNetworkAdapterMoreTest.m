//
//  PDNetworkAdapterMoreTest.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDNetworkAdapterMoreTest.h"
#import "PDMoreSocketSingleModel.h"

@interface PDNetworkAdapterMoreTest()<PDSocketConnectionDelegate>

@end

@implementation PDNetworkAdapterMoreTest

#pragma mark - 1.连接socket
-(void)socketConnectionWithSocket:(PDMoreSocketSingleModel *)model LoginHost:(NSString *)host port:(NSInteger)port{
    self.singleModel = model;
    if (self.singleModel.socketConnect){
        [[NetworkAdapter sharedAdapter] socketCloseConnectionWithSocket:self.singleModel.socketConnect];
    }
    model.socketConnect = [[SocketConnection alloc]init];
    [model.socketConnect connectWithHost:host port:port];
    model.socketConnect.delegate = self;
    
    self.socketTempLength = 0;
    self.socketTempData = [NSMutableData data];
}

#pragma mark - 2.连接成功后执行发送消息
-(void)socketConnectionWithLoginStartMore{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"login" forKey:@"type"];
    [dic setValue:self.singleModel.token forKey:@"token"];
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:dic socket:self.singleModel.socketConnect];
    self.socketTempLength = 0;
    self.socketTempData = [NSMutableData data];
}

#pragma mark - webSocket 连接成功后执行代理方法
-(void)didConnectToHost:(NSString *)host port:(UInt16)port{
    [self socketConnectionWithLoginStartMore];
}

#pragma mark - 断开当前登录永久长连接
-(void)socketCloseConnectionWithLogin{
    [[NetworkAdapter sharedAdapter] socketCloseConnectionWithSocket:self.singleModel.socketConnect];
}

#pragma mark - 数据返回
-(void)didReceiveData:(NSData *)data tag:(long)tag{
    [self adjustReceiveManagerDataTemp:data];
}

-(void)adjustReceiveManagerDataTemp:(NSData *)data{
    NSMutableData *mutableData = [NSMutableData data];
    
    if (self.socketWillTempData.length){
        [mutableData appendData:self.socketWillTempData];
        self.socketWillTempData = nil;
    }
    
    [mutableData appendData:data];
    
    if ([self.socketTempLength longValue] == 0){                // 第一次
        // 1. 获取4字节作为长度
        NSData *lengthData = [mutableData subdataWithRange:NSMakeRange(0, 4)];
        Byte *bytes = (Byte *)[lengthData bytes];
        int backLength = (bytes[0]<<24) + (bytes[1]<<16) + (bytes[2]<<8) + bytes[3];
        // 1. 过滤4字节
        [mutableData replaceBytesInRange:NSMakeRange(0, 4) withBytes:NULL length:0];
        self.socketTempLength = [NSNumber numberWithLong:backLength];
        self.socketTempWillLength = [NSNumber numberWithLong:backLength];                   // 需要接收的长度
    }
    // 如果需要接收的长度大于当前接收到的长度就接收
    if ([self.socketTempWillLength longValue] >= mutableData.length){
        if (self.socketTempData.length < [self.socketTempLength longValue]){                    // 需要粘包
            [self.socketTempData appendData:mutableData];
            self.socketTempWillLength = [NSNumber numberWithLong:([self.socketTempWillLength longValue] - mutableData.length)]; // 需要接收的长度
        }
    } else {
        NSRange range = NSMakeRange(0, [self.socketTempWillLength longValue]);
        NSData *jiequhouData = [mutableData subdataWithRange:range];
        [self.socketTempData appendData:jiequhouData];
        self.socketTempWillLength = [NSNumber numberWithLong:([self.socketTempWillLength longValue] - jiequhouData.length)]; // 需要接收的长度
        
        // 存储需要保存下来的data
        NSRange willSaveRange = NSMakeRange(jiequhouData.length, (mutableData.length - jiequhouData.length));
        self.socketWillTempData = [mutableData subdataWithRange: willSaveRange];
    }
    
    if (self.socketTempData.length == [self.socketTempLength longValue]){
        NSString *result = [[NSString alloc] initWithData:self.socketTempData encoding:NSUTF8StringEncoding];
        SBJSON *json = [[SBJSON alloc] init];
        NSDictionary *dicWithRequestJson = [json objectWithString:result error:nil];
        if (!dicWithRequestJson){
            [[UIAlertView alertViewWithTitle:@"socket Log" message:result buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
#ifdef DEBUG
        NSLog(@"RESPONSE Socket :%@", result);
        if (isSocketLogAlert){
            [[UIAlertView alertViewWithTitle:@"socket Log" message:result buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
#endif
        // 【代理】
        if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(socketDidBackData:)]){
            [[NetworkAdapter sharedAdapter].delegate socketDidBackData:dicWithRequestJson];
        }
        self.socketTempLength = 0;
        self.socketTempData = [NSMutableData data];
        if (self.socketWillTempData.length){
            [self adjustReceiveManagerDataTemp:nil];
        }
    }
}



@end
