//
//  NetworkAdapter+PDLogin.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 【PHP】
#import "NetworkAdapter.h"

@interface NetworkAdapter (PDLogin)

-(void)webSocketConnectionWithLoginAddress:(NSString *)socketAddress port:(NSInteger)port;   // 登录以后进行永久长连接
-(void)webSocketCloseConnectionWithLogin;                                   // 断开登录永久长连接

-(void)webSocketOrganizeTeamWhthParams:(PDChallengeTeamBackModel300 *)organizeTeamModel;            // 发起组队邀请

@end
