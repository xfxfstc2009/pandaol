//
//  NetworkAdapter+Binding.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 绑定召唤师
#import "NetworkAdapter.h"
#import "WebSocketConnection.h"
@interface NetworkAdapter (Binding)<WebSocketConnectionDelegate>

-(void)webSocketConnectionWithBindingWithKey:(NSString *)key;


@end
