//
//  NetworkAdapter+Binding.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "NetworkAdapter+Binding.h"
#import <objc/runtime.h>

static char bindingTokenKey;
@implementation NetworkAdapter (Binding)

#pragma mark - 添加绑定召唤师
-(void)webSocketConnectionWithBindingWithKey:(NSString *)key{
    // 1. 绑定token
    objc_setAssociatedObject(self, &bindingTokenKey, key, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    if (self.bindingWebSocketConnection){
        [self webSocketCloseConnectionWithSoceket:self.bindingWebSocketConnection];
    }
    self.bindingWebSocketConnection = [[WebSocketConnection alloc]init];
    self.bindingWebSocketConnection.delegate = self;
    [self.bindingWebSocketConnection webSocketConnectWithHost:@"121.40.194.113" port:9081];
}

#pragma mark - 绑定召唤师进行发起数据
-(void)webSocketConnectionBindingStartWithToken:(NSString *)token{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"1" forKey:@"type"];
    [dic setValue:token forKey:@"key"];
//    [[NetworkAdapter sharedAdapter] socketFetchModelWithRequestParams:dic webSocket:[NetworkAdapter sharedAdapter].bindingSocketConnection];
}


#pragma mark - 确定socket 链接成功，然后进行发送信息
-(void)webSocketDidOpenWithSocket:(SRWebSocket *)webSocket{
    NSString *token = objc_getAssociatedObject(self, &bindingTokenKey);
    
    [self webSocketConnectionBindingStartWithToken:token];
}



@end
