//
//  NetworkAdapter+PDBinding.h
//  PandaKing
//
//  Created by Cranz on 16/7/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "NetworkAdapter.h"
#import "PDBindRoleModel.h"

@interface NetworkAdapter (PDBinding)
+ (void)connectWebSocketWithModel:(PDBindRoleModel *)bindRoleModel;
+ (void)closeWebSocket;
@end
