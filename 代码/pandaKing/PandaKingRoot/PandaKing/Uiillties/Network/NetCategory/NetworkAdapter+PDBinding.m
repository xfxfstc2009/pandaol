//
//  NetworkAdapter+PDBinding.m
//  PandaKing
//
//  Created by Cranz on 16/7/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "NetworkAdapter+PDBinding.h"
#import <objc/runtime.h>

static const char *bindRoleModelKey = "bindRoleModelKey";

@implementation NetworkAdapter (PDBinding)

+ (void)connectWebSocketWithModel:(PDBindRoleModel *)bindRoleModel {
    if ([NetworkAdapter sharedAdapter].bindingSocketConnection) {
        [[NetworkAdapter sharedAdapter] webSocketCloseConnectionWithSoceket:[NetworkAdapter sharedAdapter].bindingSocketConnection];
    }
    
    [NetworkAdapter sharedAdapter].bindingSocketConnection = [[WebSocketConnection alloc] init];
    [NetworkAdapter sharedAdapter].bindingSocketConnection.delegate = [NetworkAdapter sharedAdapter];
    [[NetworkAdapter sharedAdapter].bindingSocketConnection webSocketConnectWithHost:@"121.40.194.113" port:9081];

    objc_setAssociatedObject([[NetworkAdapter sharedAdapter] class], bindRoleModelKey, bindRoleModel, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

+ (void)closeWebSocket {
    [[NetworkAdapter sharedAdapter] webSocketCloseConnectionWithSoceket:[NetworkAdapter sharedAdapter].bindingSocketConnection];
}

#pragma mark -
#pragma mark -- WebSocketConnectionDelegate

- (void)webSocketDidConnectedWithSocket:(SRWebSocket *)webSocket {
    PDBindRoleModel *bindRoleModel = objc_getAssociatedObject([[NetworkAdapter sharedAdapter] class], bindRoleModelKey);
    if (bindRoleModel) {
        NSDictionary *params = @{@"type":@1,@"key":bindRoleModel.key};
        [[NetworkAdapter sharedAdapter] socketFetchModelWithRequestParams:params webSocket:[NetworkAdapter sharedAdapter].bindingSocketConnection];
    }
}

- (void)webSocket:(SRWebSocket *)webSocket didReceiveData:(id)message {
    
}

@end
