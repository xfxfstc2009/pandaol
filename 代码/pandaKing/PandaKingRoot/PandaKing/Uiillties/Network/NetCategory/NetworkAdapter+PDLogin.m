//
//  NetworkAdapter+PDLogin.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "NetworkAdapter+PDLogin.h"
#import "PDOrganizeTeamModel.h"                 // 发起组队邀请的model
#import "PDCloseTeamModel.h"                    // 离开组队
#import "PDChallengeStartParamsModel.h"               // 开始出征model
#import "PDChallengeBeginParamsModel.h"               // 6.开始战斗model
#import "PDChallengeStatusParamsModel.h"            // 出征状态model

#import "PDResposeModel1.h"



@implementation NetworkAdapter (PDLogin)

#pragma mark - 登录永久长连
-(void)webSocketConnectionWithLoginAddress:(NSString *)socketAddress port:(NSInteger)port{
    if ([NetworkAdapter sharedAdapter].loginWebSocketConnection){
        [[NetworkAdapter sharedAdapter] webSocketCloseConnectionWithSoceket:[NetworkAdapter sharedAdapter].loginWebSocketConnection];
    }
    [NetworkAdapter sharedAdapter].loginWebSocketConnection = [[WebSocketConnection alloc]init];
    [[NetworkAdapter sharedAdapter].loginWebSocketConnection webSocketConnectWithHost:socketAddress port:port];
    [NetworkAdapter sharedAdapter].loginWebSocketConnection.delegate = self;
}

#pragma mark - 连接成功后执行发送消息
-(void)webSocketConnectionWithLoginStart{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"100" forKey:@"type"];
    [dic setValue:[AccountModel sharedAccountModel].token forKey:@"token"];
    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithPHPRequestParams:dic socket:[NetworkAdapter sharedAdapter].loginWebSocketConnection];
}

#pragma mark - webSocket 连接成功后执行代理方法
-(void)webSocketDidConnectedWithSocket:(SRWebSocket *)webSocket{
    if (webSocket == [NetworkAdapter sharedAdapter].loginWebSocketConnection.webSocket){         // 连接成功后马上发送信息
        [self webSocketConnectionWithLoginStart];
    }
}


#pragma mark - 数据返回
-(void)webSocket:(SRWebSocket *)webSocket didReceiveData:(id)message{
    NSString *str = @"";
    if ([message isKindOfClass:[NSString class]]){
        str = (NSString *)message;
    }
    NSDictionary *dataSource = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
    
#ifdef DEBUG
    NSLog(@"RESPONSE 【WebSocket】 :%@", message );
    if (isSocketLogAlert){
        [[UIAlertView alertViewWithTitle:@"socket Log" message:message buttonTitles:@[@"确定"] callBlock:NULL]show];
    }
#endif
    
    PDResposeModel1 *resposeModel = [[PDResposeModel1 alloc]initWithJSONDict:dataSource];
    if (resposeModel.type == 101){              // 登录成功返回
        if (self.loginSocketDelegate && [self.loginSocketDelegate respondsToSelector:@selector(loginConnectedWithStatus:)]){
            [self.loginSocketDelegate loginConnectedWithStatus:YES];
        }
    } else if (resposeModel.type == 102){       // 登录失败token 失效
        if (self.loginSocketDelegate && [self.loginSocketDelegate respondsToSelector:@selector(loginConnectedWithStatus:)]){
            [self.loginSocketDelegate loginConnectedWithStatus:NO];
        }
    } else if (resposeModel.type == 300){       // 绑定成功
        PDChallengeTeamBackModel300 *teamBackModel = [[PDChallengeTeamBackModel300 alloc]initWithJSONDict:dataSource];
        
        if (self.loginSocketDelegate && [self.loginSocketDelegate respondsToSelector:@selector(teamInvitedManager:)]){
            [self.loginSocketDelegate teamInvitedManager:teamBackModel];
        }
    }
}





#pragma mark - 断开当前登录永久长连接
-(void)webSocketCloseConnectionWithLogin{
    [[NetworkAdapter sharedAdapter] webSocketCloseConnectionWithSoceket:[NetworkAdapter sharedAdapter].loginWebSocketConnection];
}



#pragma mark -发送组队&拒绝邀请
-(void)webSocketOrganizeTeamWhthParams:(PDChallengeTeamBackModel300 *)organizeTeamModel{
    NSDictionary *paramsDic = [organizeTeamModel dicDecrypt];
    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithPHPRequestParams:paramsDic socket:[NetworkAdapter sharedAdapter].loginWebSocketConnection];
}


#pragma mark - 发送离队请求
-(void)webSocketCloseOrgaizeTeamWithParams:(PDCloseTeamModel *)closeTeamModel{
    NSDictionary *paramsDic = [closeTeamModel dicDecrypt];
    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithPHPRequestParams:paramsDic socket:[NetworkAdapter sharedAdapter].loginWebSocketConnection];
}


#pragma mark - 出征
-(void)webSocketChallengingWithParams:(PDChallengeStartParamsModel *)challengeStartModel{
    NSDictionary *paramsDic = [challengeStartModel dicDecrypt];
    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithPHPRequestParams:paramsDic socket:[NetworkAdapter sharedAdapter].loginWebSocketConnection];
}

#pragma mark - 开始战斗
-(void)webSocketChallengingBeginWithParams:(PDChallengeBeginParamsModel *)challengeBeginModel{
    NSDictionary *paramsDic = [challengeBeginModel dicDecrypt];
    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithPHPRequestParams:paramsDic socket:[NetworkAdapter sharedAdapter].loginWebSocketConnection];
 
}

#pragma mark - 获取当前的队伍状态
-(void)webSocketGetCurrentTeamStatus:(PDChallengeStatusParamsModel *)teamStatus{
    NSDictionary *paramsDic = [teamStatus dicDecrypt];
    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithPHPRequestParams:paramsDic socket:[NetworkAdapter sharedAdapter].loginWebSocketConnection];
}

#pragma mark - 再来一局
-(void)webSocketChallengeAgain{

}



@end
