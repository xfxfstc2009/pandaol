//
//  NetworkAdapter+PDJavaLogin.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "NetworkAdapter+PDJavaLogin.h"
#import "SBJSON.h"

static char socketTempDataKey;
static char socketTempLengthKey;
static char socketTempWillLengthKey;
static char socketWillTempDataKey;
@implementation NetworkAdapter (PDJavaLogin)

#pragma mark - 1.连接socket
-(void)socketConnectionWithLoginHost:(NSString *)host port:(NSInteger)port{
    if ([NetworkAdapter sharedAdapter].loginSocket){
        [[NetworkAdapter sharedAdapter] socketCloseConnectionWithSocket:[NetworkAdapter sharedAdapter].loginSocket];
    }
    [NetworkAdapter sharedAdapter].loginSocket = [[SocketConnection alloc]init];
    [[NetworkAdapter sharedAdapter].loginSocket connectWithHost:host port:port];
    [NetworkAdapter sharedAdapter].loginSocket.delegate = self;
    
    self.socketTempLength = 0;
    self.socketTempData = [NSMutableData data];
}

#pragma mark - 2.连接成功后执行发送消息
-(void)socketConnectionWithLoginStart{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:@"login" forKey:@"type"];
    [dic setValue:[AccountModel sharedAccountModel].token forKey:@"token"];
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:dic socket:[NetworkAdapter sharedAdapter].loginSocket];
    self.socketTempLength = 0;
    self.socketTempData = [NSMutableData data];
}

#pragma mark - webSocket 连接成功后执行代理方法
-(void)didConnectToHost:(NSString *)host port:(UInt16)port{
    //    if ([host isEqualToString:[NetworkAdapter sharedAdapter].loginSocket.host]){            // 连接成功后马上发送信息
    [self socketConnectionWithLoginStart];
    //    }
    
    // 【返回代理】
    if (self.loginSocketDelegate && [self.loginSocketDelegate respondsToSelector:@selector(loginConnectedWithStatus:)]){
        [self.loginSocketDelegate loginConnectedWithStatus:YES];
    }
}

#pragma mark - 断开当前登录永久长连接
-(void)socketCloseConnectionWithLogin{
    [[NetworkAdapter sharedAdapter] socketCloseConnectionWithSocket:[NetworkAdapter sharedAdapter].loginSocket];
}

#pragma mark - 数据返回
-(void)didReceiveData:(NSData *)data tag:(long)tag{
    [self adjustReceiveManagerData:data];
}

-(void)adjustReceiveManagerData:(NSData *)data{
    NSMutableData *mutableData = [NSMutableData data];
    
    if (self.socketWillTempData.length){
        [mutableData appendData:self.socketWillTempData];
        self.socketWillTempData = nil;
    }
    
    [mutableData appendData:data];
    
    if ([self.socketTempLength longValue] == 0){                // 第一次
        // 1. 获取4字节作为长度
        NSData *lengthData = [mutableData subdataWithRange:NSMakeRange(0, 4)];
        Byte *bytes = (Byte *)[lengthData bytes];
        int backLength = (bytes[0]<<24) + (bytes[1]<<16) + (bytes[2]<<8) + bytes[3];
        // 1. 过滤4字节
        [mutableData replaceBytesInRange:NSMakeRange(0, 4) withBytes:NULL length:0];
        self.socketTempLength = [NSNumber numberWithLong:backLength];
        self.socketTempWillLength = [NSNumber numberWithLong:backLength];                   // 需要接收的长度
    }
    // 如果需要接收的长度大于当前接收到的长度就接收
    if ([self.socketTempWillLength longValue] >= mutableData.length){
        if (self.socketTempData.length < [self.socketTempLength longValue]){                    // 需要粘包
            [self.socketTempData appendData:mutableData];
            self.socketTempWillLength = [NSNumber numberWithLong:([self.socketTempWillLength longValue] - mutableData.length)]; // 需要接收的长度
        }
    } else {
        NSRange range = NSMakeRange(0, [self.socketTempWillLength longValue]);
        NSData *jiequhouData = [mutableData subdataWithRange:range];
        [self.socketTempData appendData:jiequhouData];
        self.socketTempWillLength = [NSNumber numberWithLong:([self.socketTempWillLength longValue] - jiequhouData.length)]; // 需要接收的长度
        
        // 存储需要保存下来的data
        NSRange willSaveRange = NSMakeRange(jiequhouData.length, (mutableData.length - jiequhouData.length));
        self.socketWillTempData = [mutableData subdataWithRange: willSaveRange];
    }
    
    if (self.socketTempData.length == [self.socketTempLength longValue]){
        NSString *result = [[NSString alloc] initWithData:self.socketTempData encoding:NSUTF8StringEncoding];
        SBJSON *json = [[SBJSON alloc] init];
        NSDictionary *dicWithRequestJson = [json objectWithString:result error:nil];
#ifdef DEBUG
        NSLog(@"RESPONSE Socket :%@", result);
        if (isSocketLogAlert){
            [[UIAlertView alertViewWithTitle:@"socket Log" message:result buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
#endif
        // 【代理】
        if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(socketDidBackData:)]){
            [[NetworkAdapter sharedAdapter].delegate socketDidBackData:dicWithRequestJson];
        }
        self.socketTempLength = 0;
        self.socketTempData = [NSMutableData data];
        if (self.socketWillTempData.length){
            [self adjustReceiveManagerData:nil];
        }
    }
}


#pragma mark - DATA
-(void)setSocketTempData:(NSMutableData *)socketTempData{
    objc_setAssociatedObject(self, &socketTempDataKey, socketTempData, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
-(NSMutableData *)socketTempData{
    return  objc_getAssociatedObject(self, &socketTempDataKey);
}

-(void)setSocketTempLength:(NSNumber *)socketTempLength{
    objc_setAssociatedObject(self, &socketTempLengthKey, socketTempLength, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSNumber *)socketTempLength{
    return  objc_getAssociatedObject(self, &socketTempLengthKey);
}

// socket 将要接收的数据
-(void)setSocketTempWillLength:(NSNumber *)socketTempWillLength{
    objc_setAssociatedObject(self, &socketTempWillLengthKey, socketTempWillLength, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

-(NSNumber *)socketTempWillLength{
    return objc_getAssociatedObject(self, &socketTempWillLengthKey);
}

-(NSData *)socketWillTempData{
    return  objc_getAssociatedObject(self, &socketWillTempDataKey);
}

-(void)setSocketWillTempData:(NSData *)socketWillTempData{
    objc_setAssociatedObject(self, &socketWillTempDataKey, socketWillTempData, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}
@end
