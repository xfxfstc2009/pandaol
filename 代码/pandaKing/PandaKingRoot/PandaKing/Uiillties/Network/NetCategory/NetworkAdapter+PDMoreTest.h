//
//  NetworkAdapter+PDMoreTest.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "NetworkAdapter.h"
#import "PDMoreSocketSingleModel.h"

@interface NetworkAdapter (PDMoreTest)

@property (nonatomic,strong)NSNumber *socketTempLength;
@property (nonatomic,strong)NSMutableData *socketTempData;
@property (nonatomic,strong)NSNumber *socketTempWillLength;                 /**< socket 将要接收的数据*/
@property (nonatomic,strong)NSData *socketWillTempData;              /**< socket 已经过滤掉的数据*/


@property (nonatomic,strong)SocketConnection *socketConnect;
@property (nonatomic,strong)PDMoreSocketSingleModel *singleModel;


#pragma mark - 1. 连接socket
-(void)socketConnectionWithSocket:(PDMoreSocketSingleModel *)model LoginHost:(NSString *)host port:(NSInteger)port;

#pragma mark - 2.连接成功后执行发送消息
-(void)socketConnectionWithLoginStartMore;

#pragma mark - 3.断开连接
-(void)socketCloseConnectionWithLogin;                                   // 断开登录永久长连接

@end
