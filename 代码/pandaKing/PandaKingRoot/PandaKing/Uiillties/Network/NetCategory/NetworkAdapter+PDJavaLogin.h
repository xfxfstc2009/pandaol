//
//  NetworkAdapter+PDJavaLogin.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "NetworkAdapter.h"

@interface NetworkAdapter (PDJavaLogin)

@property (nonatomic,strong)NSNumber *socketTempLength;
@property (nonatomic,strong)NSMutableData *socketTempData;
@property (nonatomic,strong)NSNumber *socketTempWillLength;                 /**< socket 将要接收的数据*/
@property (nonatomic,strong)NSData *socketWillTempData;              /**< socket 已经过滤掉的数据*/

#pragma mark - 1. 连接socket
-(void)socketConnectionWithLoginHost:(NSString *)host port:(NSInteger)port;

#pragma mark - 2.连接成功后执行发送消息
-(void)socketConnectionWithLoginStart;

#pragma mark - 3.断开连接
-(void)socketCloseConnectionWithLogin;                                   // 断开登录永久长连接


@end
