//
//  NetworkAdapter.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//
// 网络适配器
#import <Foundation/Foundation.h>
#import "BaseNetworkConstants.h"
#import "WebSocketConnection.h"

#define PDBizErrorDomain @"PDBizErrorDomain"

@protocol PDLoginSocketDelegate <NSObject>

@optional
-(void)loginConnectedWithStatus:(BOOL)status;                                                         // 连接成功后执行方法
@end

typedef void (^FetchCompletionHandler) (BOOL isSucceeded,id responseObject, NSError *error);

@protocol PDNetworkAdapterDelegate <NSObject>

@optional
-(void)pandaNotLoginNeedLoginManager;                           // 没有登录，需要登录
-(void)socketDidBackData:(id)responseObject;

@end


@interface NetworkAdapter : NSObject<PDSocketConnectionDelegate,WebSocketConnectionDelegate>

+(instancetype)sharedAdapter;                                                               /**< 单例*/

#pragma mark - 短链接
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block;                                                          /**< 短链接*/

#pragma mark - 长链接
@property (nonatomic,weak)id<PDNetworkAdapterDelegate> delegate;                        /**< */
@property (nonatomic,weak)id<PDLoginSocketDelegate> loginSocketDelegate;                // 登录回调

#pragma mark - 【WebSocket】
@property (nonatomic,strong)WebSocketConnection *bindingWebSocketConnection;                   /**< 绑定的长连接*/
@property (nonatomic,strong)WebSocketConnection *loginWebSocketConnection;                     /**< 登录永久长连接*/

-(void)webSocketConnection:(WebSocketConnection *)webSocketConnection host:(NSString *)host port:(NSInteger)port;
-(void)webSocketCloseConnectionWithSoceket:(WebSocketConnection *)webSocketConnection;
-(void)webSocketFetchModelWithPHPRequestParams:(NSDictionary *)requestParams socket:(WebSocketConnection *)webSocketConnection;
//-(void)webSocket:(SRWebSocket *)webSocket didReceiveData:(id)message;                       /**< 数据返回*/

#pragma mark - 【Socket】
@property (nonatomic,strong)SocketConnection *loginSocket;                                              /**< 登录永久长连接*/



-(void)socketConnection:(SocketConnection *)socket host:(NSString *)host port:(NSInteger)port;          /**< socket连接*/
-(void)socketCloseConnectionWithSocket:(SocketConnection *)socketConnection;
-(void)socketFetchModelWithJavaRequestParams:(NSDictionary *)requestParams socket:(SocketConnection *)socketConnection;


// temp
@property (nonatomic,strong)SocketConnection *tempSocket1;
@property (nonatomic,strong)SocketConnection *tempSocket2;
@property (nonatomic,strong)SocketConnection *tempSocket3;
@property (nonatomic,strong)SocketConnection *tempSocket4;
@property (nonatomic,strong)SocketConnection *tempSocket5;
@property (nonatomic,strong)SocketConnection *tempSocket6;
@property (nonatomic,strong)SocketConnection *tempSocket7;
@property (nonatomic,strong)SocketConnection *tempSocket8;
@property (nonatomic,strong)SocketConnection *tempSocket9;
@property (nonatomic,strong)SocketConnection *tempSocket10;
@property (nonatomic,strong)SocketConnection *tempSocket11;

@end
