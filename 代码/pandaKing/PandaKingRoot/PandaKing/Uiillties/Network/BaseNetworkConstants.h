//
//  BaseNetworkConstants.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

#ifndef BaseNetworkConstants_h
#define BaseNetworkConstants_h

// 【接口环境】
#ifdef DEBUG        // 测试           // 【JAVA】
#define JAVA_Host @"192.168.0.149"  // 192.168.0.133
#define JAVA_Port @"81"

#else               // 线上
#define JAVA_Host @"panda-e.com"  // 121.199.35.108
#define JAVA_Port @"81"
#endif

// 【接口环境】
#ifdef DEBUG        // 测试           // 【JAVA】
#define Jungle_Host @"192.168.0.136"  // 192.168.0.138
#define Jungle_Port @"82"               // 82

#else               // 线上
#define Jungle_Host @"pandaol.picp.io"  // pandaol.picp.io
#define Jungle_Port @"85"
#endif


// 【版本号】
#define API_VER @""
#define APP_VER        [[NSBundle mainBundle].infoDictionary objectForKey:@"CFBundleShortVersionString"]
#define BUILD_VER      [[NSBundle mainBundle].infoDictionary objectForKey:(NSString *) kCFBundleVersionKey]

// 【Socket】
#ifdef DEBUG
#define Socket_API_HOST     @"192.168.0.220" //192.168.0.220
#define Socket_API_PORT     8889
#else
#define Socket_API_HOST     @"121.199.35.108"
#define Socket_API_PORT     8889
#endif


static NSString *const ErrorDomain = @"ErrorDomain";
static BOOL netLogAlert = YES;
static NSInteger timeoutInterval = 15;                                          /**< 短链接失效时间*/

// 【长链接】
static BOOL isHeartbeatPacket = NO;                                            /**< 是否心跳包*/
static BOOL isSocketLogAlert = NO;                                             /**< 是否socket*/

#import "FetchModelProperty.h"

// Base
#import "FetchModel.h"
#import "FetchFileModel.h"
#import "AFNetworking.h"
#import "SocketConnection.h"

#import "NetworkEngine.h"
#import "RequestSerializer.h"
#import "ResponseSerializer.h"
#import "NetworkAdapter.h"
#import "URLConstance.h"
#import "PDImageView.h"
#import "AccountModel.h"             // 账户信息
#import "JSON.h"


// 【Login】
#define kLoginPath                  @"token/login"                              /**< 登录 */
#define kRegister                   @"Register/add"                             /**< 注册 */


#ifdef DEBUG
#define PDSocketDebug
#endif

#ifdef PDSocketDebug
#define PDSocketLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define PDSocketLog(format, ...)
#endif


#endif /* BaseNetworkConstants_h */
