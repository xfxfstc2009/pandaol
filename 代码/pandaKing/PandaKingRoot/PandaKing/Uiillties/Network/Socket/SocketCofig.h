//
//  SocketCofig.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/24.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,codeType) {
    codeType1 = 100,                    /**< 进入匹配队列等待进行匹配*/
    codeType2 = 101,                    /**< 已匹配到用户请等待邀请进入房间*/
    codeType3 = 200,                    /**< 战斗开始（连接断开）*/
    codeType4 = 400,                    /**< 匹配不到战斗，连接断开（重复匹配N次，每次间隔N秒）*/
    codeType5 = 401,                    /**< 没有可用的房间，连接断开*/
    codeType6 = 402,                    /**< 对手放弃战斗，比赛失败，重新进入匹配队列*/
    codeType7 = 403,                    /**< 自己放弃战斗，比赛失败，连接断开*/
    codeType8 = 404,                    /**< 用户被锁定暂时无法进行匹配（已匹配到对手，等待裁判开始战斗阶段），连接断开*/
    codeType9 = 501,                    /**< 参数错误无法进行匹配，连接断开*/
    codeType10 = 620,                   /**< 无法退出匹配队列（已经匹配到对手）*/
    codeType11 = 621,                   /**< 成功退出匹配队列，连接断开*/
};

@interface SocketCofig : NSObject
-(NSString *)socketResponseMappingWithCode:(codeType)type;
@end
