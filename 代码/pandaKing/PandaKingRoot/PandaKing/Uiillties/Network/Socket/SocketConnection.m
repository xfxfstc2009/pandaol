//
//  SocketConnection.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/23.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

#import "SocketConnection.h"
#import <AsyncSocket.h>

@interface SocketConnection()<PDSocketConnectionDelegate,AsyncSocketDelegate>
@property (nonatomic,strong) AsyncSocket *socket;                 /**< socket*/
@property (nonatomic,strong) NSTimer *connectTimer;               /**< 计时器*/
@property (nonatomic,strong)NSData *receiveData;
@end

@implementation SocketConnection

-(instancetype)init{
    if (self = [super init]){
        self.socket = [[AsyncSocket alloc]initWithDelegate:self];
    }
    return self;
}

-(void)dealloc{
    self.socket.delegate = nil;
    self.socket = nil;
}

#pragma mark - AsyncSocketDelegate
// 链接
-(void)connectWithHost:(NSString *)hostName port:(NSInteger)port{
    self.host = hostName;
    self.port = port;
    
    NSError *error = nil;
    if (![self.socket connectToHost:hostName onPort:port error:&error]){
        if (_delegate && [_delegate respondsToSelector:@selector(didDisconnectWithError:)]) {
            [_delegate didDisconnectWithError:error];
        }
        if (!error){
            PDSocketLog(@"链接成功");
        }
    }
}

#pragma mark 放弃链接
-(void)disconnect{
    self.socket.userData = SocketOfflineByUser;              /**< 用户主动断开连接*/
    if (self.connectTimer){
        [self.connectTimer invalidate];                          // 计时器断开
    }
    
    [self.socket disconnect];
    self.socket.delegate = nil;
    self.socket = nil;
}

#pragma mark 判断是否链接
-(BOOL)isConnected{
    return [self.socket isConnected];
}

-(void)readDataWithTimeout:(NSTimeInterval)timeout tag:(long)tag{
    [self.socket readDataWithTimeout:timeout tag:tag];
}

- (void)readDataWithTimeout:(NSTimeInterval)timeout buffer:(NSMutableData *)buffer bufferOffset:(NSUInteger)offset tag:(long)tag{
    PDSocketLog(@"123");
}

-(void)writeData:(NSData *)data timeout:(NSTimeInterval)timeout tag:(long)tag{
    [self.socket writeData:data withTimeout:timeout tag:tag];
}



#pragma mark - 重链
-(void)onSocketDidDisconnect:(AsyncSocket *)sock{
    if (sock.userData == SocketOfflineByServer){                // 服务器断线，重连
        PDSocketLog(@"【断线断开连接】");
        [self socketConnectHost];
    } else if (sock.userData == SocketOfflineByUser){           // 用户主动断开，不重连
        PDSocketLog(@"【用户主动断开连接】");
        return;
    }
    if (_delegate && [_delegate respondsToSelector:@selector(didDisconnectWithError:)]) {
        [_delegate didDisconnectWithError:nil];
    }
}

#pragma mark 链接成功回调
-(void)onSocket:(AsyncSocket *)sock didConnectToHost:(NSString *)host port:(UInt16)port{
    PDSocketLog(@"【已经链接上】获取链接的ip和端口 didConnectToHost: %@, port: %d", host, port);
    if (isHeartbeatPacket){     // 是否需要心跳包
        self.connectTimer = [NSTimer scheduledTimerWithTimeInterval:3 target:self selector:@selector(longConnectToSocket) userInfo:nil repeats:YES];
        [self.connectTimer fire];
    }
    
    if (_delegate && [_delegate respondsToSelector:@selector(didConnectToHost:port:)]) {
        [_delegate didConnectToHost:host port:port];
    }
}

-(void)onSocket:(AsyncSocket *)sock didReadData:(NSData *)data withTag:(long)tag{
    PDSocketLog(@"【读取数据】 didReadData length: %lu, tag: %ld", (unsigned long)data.length, tag);
    
    if (_delegate && [_delegate respondsToSelector:@selector(didReceiveData:tag:)]) {
        [_delegate didReceiveData:data tag:tag];
    }
    [sock readDataWithTimeout:-1 tag:tag];
}

//当socket完成写数据请求的时候。
-(void)onSocket:(AsyncSocket *)sock didWriteDataWithTag:(long)tag{
    PDSocketLog(@"【数据发送成功】 didWriteDataWithTag: %ld", tag);
    [sock readDataWithTimeout:-1 tag:tag];
}

/**
 //
 //在断开之前你可以读取断开之前最后的bit数据
 //当连接的时候，这个代理的方法可能被执行。
 */
-(void)onSocket:(AsyncSocket *)sock willDisconnectWithError:(NSError *)err{
    NSLog(@"在socket连结发生错误的时候，socket被关闭。");
}

#pragma mark - 心跳包
-(void)longConnectToSocket{
    PDSocketLog(@"心跳");
    NSString *longConnect = @"0";
    NSData   *dataStream  = [longConnect dataUsingEncoding:NSUTF8StringEncoding];
    [self.socket writeData:dataStream withTimeout:1 tag:1];
}

#pragma mark - 断线重连
-(void)socketConnectHost{
    NSError *error = nil;
    [self.socket connectToHost:[AccountModel sharedAccountModel].lifeSocketHost onPort:[AccountModel sharedAccountModel].lifeSocketPort error:&error];
}

@end
