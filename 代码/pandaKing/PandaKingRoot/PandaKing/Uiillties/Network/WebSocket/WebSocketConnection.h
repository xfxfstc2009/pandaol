//
//  WebSocketConnection.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <SocketRocket/SRWebSocket.h>

@protocol WebSocketConnectionDelegate <NSObject>

@optional
-(void)webSocket:(SRWebSocket *)webSocket didReceiveData:(id)message;
-(void)webSocketDidConnectedWithSocket:(SRWebSocket *)webSocket;                /**< webSocket 连接成功后操作*/
@end

@interface WebSocketConnection : NSObject

@property (nonatomic,weak)id<WebSocketConnectionDelegate> delegate;
@property (nonatomic,strong)SRWebSocket *webSocket;

- (void)webSocketConnectWithHost:(NSString *)hostName port:(NSInteger)port;                            /**< 连接到服务器*/
- (void)webSocketDisconnect;                                                                           /**< 断开服务*/

#pragma mark - 发送信息
- (void)webSocketWriteData:(NSString *)info;

@end
