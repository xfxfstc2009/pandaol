//
//  RequestSerializer.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//
// 设置请求头
#import "RequestSerializer.h"
#import "GTMBase64.h"
#import "PDRSAManager.h"
@implementation RequestSerializer

-(NSMutableURLRequest *)requestWithMethod:(NSString *)method URLString:(NSString *)URLString parameters:(id)parameters error:(NSError *__autoreleasing *)error{
    NSMutableURLRequest *request = [super requestWithMethod:method URLString:URLString parameters:parameters error:error];
    if ([URLString hasSuffix:tokenLogin]){              // 判断是否是这个结尾
        NSString *userName = [parameters objectForKey:@"username"];
        NSString *password = [parameters objectForKey:@"password"];
        NSString *password1 = password.length?[PDRSAManager encryptStringWithString:password]:@"";
        
        NSString *authorization = @"";
        if ((userName.length) && (password1.length)){
            authorization = [GTMBase64 encodeBase64String:[NSString stringWithFormat:@"%@:%@",userName,password1]];
            authorization = [NSString stringWithFormat:@"Basic %@",authorization];
            [request setValue:authorization forHTTPHeaderField:@"Authorization"];
        }
    }
    return request;
}


@end
