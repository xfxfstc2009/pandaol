//
//  FetchModel.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseNetworkConstants.h"
#define BizErrorDomain @"BizErrorDomain"

@class FetchModel;

typedef void (^FetchCompletionHandler) (BOOL isSucceeded,id responseObject, NSError *error);
typedef void(^FetchProgress)(NSProgress * uploadProgress);

@interface FetchModel : NSObject

@property (nonatomic, assign) BOOL bizDataIsNull;                   /**< 业务数据无效*/
@property (nonatomic, assign) BOOL bizResult;

@property (nonatomic,strong)NSDictionary *requestParams;            /**< 数据请求*/
@property (nonatomic, strong) NSArray *requestFileDataArr;          /**< 上传文件参数*/

// 请求接口
-(void)fetchWithPath:(NSString *)path completionHandler:(FetchCompletionHandler)handler;

-(BOOL)isSessionValid;                                              /**< 验证session是否有效*/
-(void)clearCookies;                                                /**< 清除cookie*/

// 仅供运行时解析JSON使用，子类不要调用此方法初始化对象
- (instancetype)initWithJSONDict:(NSDictionary *)dict;

// 子类需要覆盖此方法，提供model和JSON无法对应到的成员
- (NSDictionary *)modelKeyJSONKeyMapper;


// timeout 时间 默认 20秒
@property (nonatomic,assign)NSInteger timeOutTime;
// 是否抛出日志 默认不抛出
@property (nonatomic,assign)BOOL isReturnLog;


// 将model 转换为字典
-(NSDictionary *)dicDecrypt;
@end
