//
//  FetchModel.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

#import "FetchModel.h"
#import <objc/runtime.h>

#import "UIAlertView+Customise.h"               // category
#import "MD5Manager.h"
#import "APNSTool.h"


#pragma mark - NSArray + FetchModel
@interface NSArray (FetchModel)
- (NSArray *)modelArrayWithClass:(Class)modelClass;

@end

@implementation NSArray (FetchModel)

- (NSArray *)modelArrayWithClass:(Class)modelClass{
    NSMutableArray *modelArray = [NSMutableArray array];
    for (id object in self) {
        if ([object isKindOfClass:[NSArray class]]) {
            [modelArray addObject:[object modelArrayWithClass:modelClass]];
        } else if ([object isKindOfClass:[NSDictionary class]]){
            [modelArray addObject:[[modelClass alloc] initWithJSONDict:object]];
        } else {
            [modelArray addObject:object];
        }
    }
    return modelArray;
}
@end

#pragma mark - NSDictionary + FetchModel
@interface NSDictionary (FetchModel)
- (NSDictionary *)modelDictionaryWithClass:(Class)modelClass;

@end

@implementation NSDictionary (FetchModel)

- (NSDictionary *)modelDictionaryWithClass:(Class)modelClass{
    NSMutableDictionary *modelDictionary = [NSMutableDictionary dictionary];
    for (NSString *key in self) {
        id object = [self objectForKey:key];
        if ([object isKindOfClass:[NSDictionary class]]) {
            [modelDictionary setObject:[[modelClass alloc] initWithJSONDict:object] forKey:key];
        }else if ([object isKindOfClass:[NSArray class]]){
            [modelDictionary setObject:[object modelArrayWithClass:modelClass] forKey:key];
        }else{
            [modelDictionary setObject:object forKey:key];
        }
    }
    return modelDictionary;
}

@end

#pragma mark - FetchModel

static const char *FecthModelKeyMapperKey;
static const char *FetchModelPropertiesKey;
static const char *propertiesKey;

@interface FetchModel(){
    NSURLSessionDataTask *requestOperation;
}
@property (nonatomic,strong)AFHTTPSessionManager *operationManager;
- (void)setupCachedKeyMapper;
- (void)setupCachedProperties;

@end


@implementation FetchModel

// 1.

-(void)dealloc{
    if (requestOperation){
        [requestOperation cancel];
    }
}

-(void)setUP{
    if (!self.timeOutTime){
        self.timeOutTime = 10;
    }
}

-(void)setIsReturnLog:(BOOL)isReturnLog{
    if (isReturnLog){
        _isReturnLog = YES;
    } else {
        _isReturnLog = NO;
    }
}

-(instancetype)init{
    self = [super init];
    if (self){
        [self setUP];
        [self setupCachedKeyMapper];
        [self setupCachedProperties];
    }
    return self;
}

- (instancetype)initWithJSONDict:(NSDictionary *)dict{
    self = [self init];
    if (self) {
        [self injectJSONData:dict];
    }
    return self;
}

#pragma mark - 
-(AFHTTPSessionManager *)operationManager{
    if (!_operationManager){
        NSString *baseURLString = @"";
       
#ifdef DEBUG
        NSString *address = [Tool userDefaultGetWithKey:testNet_address].length?[Tool userDefaultGetWithKey:testNet_address]:JAVA_Host;
        NSString *port = [Tool userDefaultGetWithKey:testNet_port].length?[Tool userDefaultGetWithKey:testNet_port]:JAVA_Port;
        if (port.length){
            baseURLString =[NSString stringWithFormat:@"http://%@:%@",address,port];
        } else {
            baseURLString = [NSString stringWithFormat:@"http://%@",address];
        }
#else
        if (JAVA_Port.length){
            baseURLString =[NSString stringWithFormat:@"http://%@:%@",JAVA_Host,JAVA_Port];
        } else {
            baseURLString =[NSString stringWithFormat:@"http://%@",JAVA_Host];
        }
#endif
        
        NSURL *baseURL = [NSURL URLWithString:baseURLString];
        
        _operationManager = [[AFHTTPSessionManager alloc]initWithBaseURL:baseURL];
        _operationManager.requestSerializer = [RequestSerializer serializer];
        _operationManager.requestSerializer.timeoutInterval = _timeOutTime;
        _operationManager.responseSerializer = [AFHTTPResponseSerializer serializer];
    }
    return _operationManager;
}




-(BOOL)isSessionValid{
    NSURL *url = self.operationManager.baseURL;
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:url];
    
    for (NSHTTPCookie *cookie in cookies) {
        if ([cookie.name isEqualToString:@"kdAuthToken"] && (cookie.expiresDate.timeIntervalSinceNow < 0)) {
            return NO;
        }
    }
    
    return YES;
}

- (void)clearCookies{
    NSURL *url = self.operationManager.baseURL;
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:url];
    
    for (NSHTTPCookie *cookie in cookies) {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
}

-(void)fetchWithPath:(NSString *)path completionHandler:(FetchCompletionHandler)handler{
    NSMutableDictionary *requestParamsDic = [NSMutableDictionary dictionaryWithDictionary:_requestParams];
    
    [requestParamsDic setObject:@"ios" forKey:@"deviceType"];
    [requestParamsDic setObject:[Tool appVersion] forKey:@"version"];
    [requestParamsDic setObject:[CloudPushSDK getDeviceId] forKey:@"deviceId"];
    // token
    if ([AccountModel sharedAccountModel].token.length){
        [requestParamsDic setObject:[AccountModel sharedAccountModel].token forKey:@"token"];
    }
    
    NSString *sign = [MD5Manager md5:[self sortModelManagerWithDic:requestParamsDic]];
    NSMutableDictionary *smartDic = [NSMutableDictionary dictionaryWithDictionary:requestParamsDic];
    [smartDic setObject:sign forKey:@"sign"];
    
    
    

    __weak typeof(self) weakSelf = self;
    [requestOperation cancel];
    [self.operationManager POST:path parameters:smartDic constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSDate *date = [NSDate date];
        NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
        [formatter setDateFormat:@"yyyyMMddHHmmss"];
        NSString *dateStr = [formatter stringFromDate:date];
        if (_requestFileDataArr.count){
            for (int i = 0 ;i < _requestFileDataArr.count;i++){
                FetchFileModel *fileModel = [_requestFileDataArr objectAtIndex:i];
                UIImage *valueImage = fileModel.uploadImage;
                NSData *data = UIImageJPEGRepresentation(valueImage,0.7);
                NSString *keyName = fileModel.keyName;
                NSString *fileName = [NSString stringWithFormat:@"%@%d.png", dateStr,i + 1];
                
                // 1. 查找当前的数据里面的key
                NSString *name = [_requestParams objectForKey:keyName];
                if (!name.length){
                    name = [NSString stringWithFormat:@"%li",(long)arc4random()];
                }
                [formData appendPartWithFileData:data name:name fileName:fileName mimeType:@"image/png"];
            }
        }
    } progress:^(NSProgress * _Nonnull uploadProgress) {
//        DLog(@"上传进度: %@",uploadProgress);
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        if (!weakSelf) {
//            return;
//        }
        weakSelf.bizResult = NO;
        weakSelf.bizDataIsNull = NO;
        __strong typeof(weakSelf)strongSelf = weakSelf;
        id responseObjectWithJson = [NSJSONSerialization JSONObjectWithData:responseObject options:0 error:nil];
#ifdef DEBUG
        NSLog(@"RESPONSE JSON:%@", responseObjectWithJson );
        if (strongSelf.isReturnLog){            // 输出log
            [[UIAlertView alertViewWithTitle:                 @"测试log" message:responseObjectWithJson buttonTitles:@[@"复制Log",@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 0){
                    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
                    pasteboard.string = responseObjectWithJson;
                }
            }]show];
        }
#endif
        // 【抛出解析】
        NSNumber *errorCode = [responseObjectWithJson objectForKey:@"code"];
        id data = [responseObjectWithJson  objectForKey:@"data"];
        if (errorCode.integerValue == 200) {
            if ([data isKindOfClass:[NSArray class]] || [data isKindOfClass:[NSDictionary class]]) {
                strongSelf.bizResult = YES;
                [strongSelf injectJSONData:data];
            }else if ([data isKindOfClass:[NSNull class]]) {
                strongSelf.bizDataIsNull = YES;
            }else if ([data isKindOfClass:[NSNumber class]]) {
                if ([NSStringFromClass([data class]) hasSuffix:@"CFBoolean"]) {
                    strongSelf.bizResult = [data boolValue];
                }
            }
            
            handler(YES, data,nil);
        } else {                // 解析出数据
            NSString *errorInfo = [responseObjectWithJson objectForKey:@"msg"];
            if (!errorInfo.length){
                errorInfo = @"错误";
            }
            NSDictionary *dict = @{NSLocalizedDescriptionKey: errorInfo};
            NSError *bizError = [NSError errorWithDomain:BizErrorDomain code:errorCode.integerValue userInfo:dict];
            handler(NO,nil,bizError);
        }
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(self)strongSelf = weakSelf;
        
#ifdef DEBUG
        NSString *requestURL = [task.currentRequest.URL absoluteString];
        NSString *params = [[NSString alloc]initWithData:task.currentRequest.HTTPBody encoding:NSUTF8StringEncoding];
        NSLog(@"FAILURE URL:%@ \nPARAMS:%@ \nAND RESPONSE:%@", requestURL, params, task.response);
#endif
        [strongSelf showResponseCode:task.response WithBlock:^(NSInteger statusCode) {
            handler(NO,nil,error);
        }];
    }];
}

- (void)showResponseCode:(NSURLResponse *)response WithBlock:(void (^)(NSInteger statusCode))block{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    NSInteger responseStatusCode = [httpResponse statusCode];
    return block(responseStatusCode);
}

- (void)setupCachedKeyMapper{
    
    if (objc_getAssociatedObject(self.class, &FecthModelKeyMapperKey) == nil) {
        
        NSDictionary *dict = [self modelKeyJSONKeyMapper];
        if (dict.count) {
            objc_setAssociatedObject(self.class, &FecthModelKeyMapperKey, dict, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
        }
    }
}

- (void)setupCachedProperties{
    
    if (objc_getAssociatedObject(self.class, &FetchModelPropertiesKey) == nil) {
        
        NSMutableDictionary *propertyMap = [NSMutableDictionary dictionary];
        Class class = [self class];
        
        while (class != [FetchModel class]) {
            unsigned int propertyCount;
            objc_property_t *properties = class_copyPropertyList(class, &propertyCount);
            for (unsigned int i = 0; i < propertyCount; i++) {
                
                objc_property_t property = properties[i];
                const char *propertyName = property_getName(property);
                NSString *name = [NSString stringWithUTF8String:propertyName];
                const char *propertyAttrs = property_getAttributes(property);
                NSString *typeString = [NSString stringWithUTF8String:propertyAttrs];
                FetchModelProperty *modelProperty = [[FetchModelProperty alloc] initWithName:name typeString:typeString];
                if (!modelProperty.isReadonly) {
                    [propertyMap setValue:modelProperty forKey:modelProperty.name];
                }
            }
            free(properties);
            
            class = [class superclass];
        }
        objc_setAssociatedObject(self.class, &FetchModelPropertiesKey, propertyMap, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    }
}

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{};
}

#pragma mark - FetchModel Runtime Injection

- (void)injectJSONData:(id)dataObject{
    
    NSDictionary *keyMapper = objc_getAssociatedObject(self.class, &FecthModelKeyMapperKey);
    NSDictionary *properties = objc_getAssociatedObject(self.class, &FetchModelPropertiesKey);
    
    if ([dataObject isKindOfClass:[NSArray class]]) {
        
        FetchModelProperty *arrayProperty = nil;
        Class class = NULL;
        for (FetchModelProperty *property in [properties allValues]) {
            
            NSString *valueProtocol = [property.objectProtocols firstObject];
            class = NSClassFromString(valueProtocol);
            if ([valueProtocol isKindOfClass:[NSString class]] && [class isSubclassOfClass:[FetchModel class]]) {
                arrayProperty = property;
                break;
            }
        }
        
        if (arrayProperty && class) {
            id value = [(NSArray *)dataObject modelArrayWithClass:class];
            [self setValue:value forKey:arrayProperty.name];
        }
    }else if ([dataObject isKindOfClass:[NSDictionary class]]){
        
        for (FetchModelProperty *property in [properties allValues]) {
            
            NSString *jsonKey = property.name;
            NSString *mapperKey = [keyMapper objectForKey:jsonKey];
            jsonKey = mapperKey ?: jsonKey;
            
            id jsonValue = [dataObject objectForKey:jsonKey];
            id propertyValue = [self valueForProperty:property withJSONValue:jsonValue];
            
            if (propertyValue) {
                
                [self setValue:propertyValue forKey:property.name];
            }else{
                
                id resetValue = (property.valueType == ClassPropertyTypeObject) ? nil : @(0);
                [self setValue:resetValue forKey:property.name];
            }
        }
    }
}

- (id)valueForProperty:(FetchModelProperty *)property withJSONValue:(id)value{
    
    id resultValue = value;
    if (value == nil || [value isKindOfClass:[NSNull class]]) {
        resultValue = nil;
    }else{
        if (property.valueType != ClassPropertyTypeObject) {
            
            if ([value isKindOfClass:[NSString class]]) {
                if (property.valueType == ClassPropertyTypeInt ||
                    property.valueType == ClassPropertyTypeUnsignedInt||
                    property.valueType == ClassPropertyTypeShort||
                    property.valueType == ClassPropertyTypeUnsignedShort) {
                    resultValue = [NSNumber numberWithInt:[(NSString *)value intValue]];
                }
                if (property.valueType == ClassPropertyTypeLong ||
                    property.valueType == ClassPropertyTypeUnsignedLong ||
                    property.valueType == ClassPropertyTypeLongLong ||
                    property.valueType == ClassPropertyTypeUnsignedLongLong){
                    resultValue = [NSNumber numberWithLongLong:[(NSString *)value longLongValue]];
                }
                if (property.valueType == ClassPropertyTypeFloat) {
                    resultValue = [NSNumber numberWithFloat:[(NSString *)value floatValue]];
                }
                if (property.valueType == ClassPropertyTypeDouble) {
                    resultValue = [NSNumber numberWithDouble:[(NSString *)value doubleValue]];
                }
                if (property.valueType == ClassPropertyTypeChar) {
                    //对于BOOL而言，@encode(BOOL) 为 c 也就是signed char
                    resultValue = [NSNumber numberWithBool:[(NSString *)value boolValue]];
                }
            }
        }else{
            Class valueClass = property.objectClass;
            if ([valueClass isSubclassOfClass:[FetchModel class]] &&
                [value isKindOfClass:[NSDictionary class]]) {
                resultValue = [[valueClass alloc] initWithJSONDict:value];
            }
            
            if ([valueClass isSubclassOfClass:[NSString class]] &&
                ![value isKindOfClass:[NSString class]]) {
                resultValue = [NSString stringWithFormat:@"%@",value];
            }
            
            if ([valueClass isSubclassOfClass:[NSNumber class]] &&
                [value isKindOfClass:[NSString class]]) {
                NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc] init];
                [numberFormatter setNumberStyle:NSNumberFormatterDecimalStyle];
                resultValue = [numberFormatter numberFromString:value];
            }
            
            NSString *valueProtocol = [property.objectProtocols lastObject];
            if ([valueProtocol isKindOfClass:[NSString class]]) {
                
                Class valueProtocolass = NSClassFromString(valueProtocol);
                if (valueProtocolass != nil) {
                    if ([valueProtocolass isSubclassOfClass:[FetchModel class]]) {
                        //array of models
                        if ([value isKindOfClass:[NSArray class]]) {
                            resultValue = [(NSArray *)value modelArrayWithClass:valueProtocolass];
                        }
                        //dictionary of models
                        if ([value isKindOfClass:[NSDictionary class]]) {
                            resultValue = [(NSDictionary *)value modelDictionaryWithClass:valueProtocolass];
                        }
                    }
                }
            }
        }
    }
    return resultValue;
}

-(void)dicWithModel:(FetchModel *)fetchModel{
    
}


- (NSArray *)propertyList {
    NSArray *pList = objc_getAssociatedObject(self, propertiesKey);
    if (pList != nil) {
        return pList;
    }

    unsigned int count = 0;
    objc_property_t *list = class_copyPropertyList([self class], &count);
    NSMutableArray *arrayM = [NSMutableArray arrayWithCapacity:count];
    // 遍历数组
    for (unsigned int i = 0; i < count; ++i) {        // 获取到属性
        objc_property_t pty = list[i];                // 获取属性的名称
        const char *cname = property_getName(pty);
        [arrayM addObject:[NSString stringWithUTF8String:cname]];
    }
    free(list);
    objc_setAssociatedObject(self, propertiesKey, arrayM, OBJC_ASSOCIATION_COPY_NONATOMIC);
    return arrayM.copy;
}


-(NSDictionary *)dicDecrypt{
    NSArray *properties = [self propertyList];
    NSMutableDictionary *propertyDic = [NSMutableDictionary dictionary];
    for (NSString *property in properties){
        id value = [self valueForKeyPath:property];
        if ([value isKindOfClass:[NSNumber class]]){
            [propertyDic setValue:value forKey:property];
        } else {
           [propertyDic setValue:value forKey:property];
        }
    }
    return propertyDic;
}

-(NSString *)sortModelManagerWithDic:(NSDictionary *)dict{
    NSString *sortString = @"";
    NSArray *keysArray = [dict allKeys];
    NSArray *resultArray = [keysArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    for (NSString *categoryId in resultArray) {
        NSString *keyValueString = [dict objectForKey:categoryId];
        
        NSString *key = categoryId;
        NSString *keyValue = keyValueString;
        sortString = [sortString stringByAppendingString:[NSString stringWithFormat:@"%@=%@&",key,keyValue]];
    }
    sortString = [sortString stringByAppendingString:@"pandaolWR@#!DFS"];
    return sortString;
    
}


@end
