//
//  URLConstance.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef URLConstance_h
#define URLConstance_h

#pragma mark - APPURL
static NSString *const appUrl = @"https://itunes.apple.com/us/app/%E7%9B%BC%E8%BE%BE%E7%94%B5%E7%AB%9E/id1079756043?l=zh&ls=1&mt=8";

// 【A1】
static NSString *const authmemberAuth = @"/po/auth/member/auth";
static NSString *const tokenRegister = @"/po/auth/member/reg";                          /**< 注册*/
static NSString *const tokenLogin = @"/po/auth/member/login";                           /**< 用户普通登录接口*/
static NSString *const userRegSend = @"/po/auth/member/regcode";                        /**< 注册短信*/
static NSString *const userVcode = @"/po/auth/member/regcodecheck";                     /**< 注册验证code */
static NSString *const resetMsm = @"/po/auth/member/resetcode";                         /**< 重置密码找回密码发送验证码*/
static NSString *const resetValidate = @"/po/auth/member/resetcodecheck";               /**< 密码找回验证*/
static NSString *const resetPassword = @"/po/auth/member/resetpassword";                /**< 重置密码*/
// 三方登录
static NSString *const thirdLogin = @"/po/auth/third/login";                            /**< 三方登录*/
static NSString *const userContinue = @"/po/auth/third/skipbind";                       /**< 跳过*/
static NSString *const userBindCheck = @"/po/auth/third/validatebind";                  /**< 验证三方是否能绑定*/
static NSString *const thirdBindCode = @"/po/auth/third/bindcode";              /**< 获取绑定验证码*/
static NSString *const userBind = @"/po/auth/third/bindwithpwd";                             /**< 绑定*/
static NSString *const usetOutBind = @"/po/auth/third/bindwithoutpwd";                          /**< 绑定已注册的手机*/
static NSString *const userLogout = @"/po/member/logout";                         /**< 登出*/
static NSString *const memberThirdbind = @"/po/member/thirdbind";                       /**< */

static NSString *const checkBindStatus = @"/a1/api/user/check-bind-status";         /**< 检测用户qq或微信绑定状态*/
static NSString *const authorMember = @"/po/auth/member/guestlogin";                       /**< 游客登录*/
static NSString *const loginUpdatePwd = @"/po/member/resetpassword";                   /**< 修改密码*/

static NSString *const bindThird = @"/po/member/bindthird";                          /**< 绑定*/
static NSString *const unbindindThird = @"/po/member/unbindthird";                  /**< 解绑定*/
// 【A3 匹配对战系统】
static NSString *const getRoleList = @"/a3/api/match/get-role";            /**< 获取用户召唤师列表*/
static NSString *const matchActiveHero = @"a3/api/match/active-hero";       /**< 设置活跃召唤师*/
static NSString *const matchInactiveHero = @"a3/api/match/inactive-hero";   /**< 设为非活跃召唤师*/
static NSString *const matchUnbindHero = @"a3/api/match/unbind-hero";       /**< 解绑召唤师*/



// 【绑定】

//////////////////////////// A1 //////////////////////////////
static NSString *const thirdBind = @"/a1/api/user/login-bind-third";    /**<  手机号|邮箱 已登录 绑定第三方*/
static NSString *const unBind = @"/a1/api/user/unbind"; /**< 解除绑定*/
static NSString *const findactivatedtimes = @"/po/member/lol/findactivatedtimes";           // 


#pragma mark - iOS聊天头像昵称
static NSString *const imUserInfo = @"/po/member/other/im"; /**< 获取头像昵称*/


//////////////////////////// 我的 ////////////////////////
#pragma mark - 我的
static NSString *const  myInfo = @"/po/member/myinfo"; /**< 我的信息*/
static NSString *const memberInfo = @"/po/member/memberinfo";    /** 个人更详细信息*/
static NSString *const editGender = @"/po/member/gender";   /** 编辑性别*/
static NSString *const editNickName = @"/po/member/nickname";   /**< 编辑昵称*/
static NSString *const editQQ = @"/po/member/qq";   /**< 编辑qq*/
static NSString *const editWeChat = @"/po/member/wechat";   /**< 编辑微信*/
static NSString *const editAge = @"/po/member/age"; /**< 编辑年龄*/
static NSString *const editSign = @"/po/member/signature";  /**< 编辑个性签名*/
static NSString *const editAddress = @"/po/member/address"; /**< 编辑地址*/
static NSString *const editAvatar = @"/po/member/avatar";   /**< 上传头像*/
static NSString *const resetpasswordinfo = @"/po/member/resetpasswordinfo";         // 设置密码

// 我的竞猜
#pragma mark - 我的竞猜
//// lol
static NSString *const centerLottery = @"/po/member/guessrecord/guessstatistic"; /**< 统计*/
static NSString *const centerGameLottery = @"/po/member/guessrecord/matchstakelist"; /**< 赛事猜*/
static NSString *const centerHeroLottery = @"/po/member/guessrecord/championstakelist";  /**< 英雄猜*/
static NSString *const centerAnchorLottery = @"/po/member/guessrecord/anchorplaylist"; /**< 主播猜*/
///// dota2
static NSString *const centerHeroDota2Lottery = @"/po/member/dota2championguessrecord/stakelist";
//// 王者荣耀
static NSString *const centerHeroKingLottery = @"/po/member/kingchampionguessrecord/stakelist";

// 背包
#pragma mark - 背包
static NSString *const backpackHad = @"/po/member/cer/membercommoditycer";   /**< 已拥有*/
static NSString *const backpackUsed = @"/po/member/cer/membercommodityorder";   /**< 已使用*/
static NSString *const exchange = @"/po/member/cer/usecertificate"; /**< 兑换商品*/
static NSString *const present = @"/po/member/cer/present"; /**< 赠送*/
static NSString *const exchangeDetail = @"/po/member/cer/commodityorder"; /**< 兑换详情*/
static NSString *const backpackGameServers = @"/po/member/cer/gameservers"; /**< lol 大区*/
static NSString *const backpackExchangeRule = @"/po/html/cer/conversionrules.html";    /**< 兑换规则*/

// 钱包
#pragma mark - 钱包
static NSString *const centerMemberWallet = @"/po/member/wallet/membergold";  /**< 我的钱包页*/
static NSString *const centerMemberGold = @"/po/member/wallet/memberaccountlogforgold";  /**< 查看会员金币流水*/
static NSString *const centerMemberBamboo = @"/po/member/wallet/memberaccountlogforbamboo"; /** 查看会员竹子流水*/
static NSString *const centerOrder = @"/po/member/wallet/order"; /**< 购买竹子 ，订单*/
static NSString *const memberaccountlogforxfegold = @"/po/member/other/xfegoldrecord";    /**<转账记录流水*/
//// 新
static NSString *const walletPayList = @"/po/member/wallet/paylist"; /**< 充值列表*/
static NSString *const walletExchange = @"/po/member/wallet/exchange"; /**< 竹子兑换金币*/
static NSString *const walletChangeList = @"/po/member/wallet/exchangelist"; /**< 兑换列表*/


// 我的撸友
#pragma mark - 我的撸友
static NSString *const centerMyfollowlist = @"/po/member/follow/myfollowlist"; /**< 我的关注*/
static NSString *const centerMyfanslist = @"/po/member/follow/myfanslist";  /**< 我的粉丝*/
static NSString *const centerAddFollow = @"/po/member/follow/follow";   /**< 添加关注*/
static NSString *const cancenFollow = @"/po/member/follow/unfollow";
// 推广
#pragma mark - 推广／邀请好友
static NSString *const centerInvited = @"/po/member/invite/invited";    /**< 填写邀请码*/
static NSString *const centerMemberInvite = @"/po/member/invite/memberinvite"; /**< 邀请好友页信息*/
static NSString *const centerInviteDetails = @"/po/member/invite/invitelog"; /**< 邀请明细*/
static NSString *const centerInviteCodeUrl = @"/po/auth/web/member/invitepage?code="; // 二维码链接

#pragma mark - 查询玩家
static NSString *const zoneSearchPlayer = @"/po/member/follow/search"; /**< 查询*/

// 我的夺宝消息
#pragma mark - 我的夺宝
static NSString *const centerSnatchProgress = @"/po/member/tlarecord/notdrawphases"; /**< 夺宝消息进行中*/
static NSString *const centerSnacthEnd = @"/po/member/tlarecord/drawphases"; /**< 夺宝消息已结束*/
static NSString *const centerSnatchWins = @"/po/member/tlarecord/wins"; /**< 夺宝消息已中奖*/
#pragma mark - 社交配置
/**
 * 社交－黑科技
 */
static NSString *const appFunctionZoneFind = @"/po/member/pandatopic/appfunction/find";

#pragma mark - 我的任务
static NSString *const centerMyTask = @"/po/member/task/list"; /** 我的任务列表*/
static NSString *const centerTaskAward = @"/po/member/task/receiveaward"; /**< 领取奖励*/
static NSString *const centerTaskReceived = @"/po/member/task/countunreceivedtask"; /**< 是否有未领取的奖励的任务*/
/**
 * 分享任务
 */
static NSString *const centerTaskShareSuccess = @"/po/member/task/sharepost";

////////////////////////////////////  商城 /////////////////////////////
#pragma mark - 商城
static NSString *const storeList = @"/po/member/bambooredeemactivity/activities"; /**< 商城列表*/
static NSString *const storeDetail = @"/po/member/bambooredeemactivity/detail"; /**< 商城详情*/
static NSString *const storeExchangeRecord = @"/po/member/bambooredeemactivity/redeemlist"; /** 兑换记录*/
static NSString *const storeGraphicDetails = @"/po/member/bambooredeemactivity/commoditydetail"; ///< 图文详情
static NSString *const storeRedeem = @"/po/member/bambooredeemactivity/redeempoints"; /** 兑换*/


///////////////////////////////////// 发现 ////////////////////////
#pragma mark - TA的夺宝
static NSString *const findTaSnatchRecord = @"/po/member/other/tlarecord";  /**< TA的夺宝记录*/
static NSString *const findTaWinerRecord = @"/po/member/other/winrecord";   /**< TA的中奖纪录*/
static NSString *const findTaMemberInfo = @"/po/member/other/info"; /**< 用户信息*/

#pragma mark - 聊天配置
static NSString *const findChattingConfig = @"/po/member/other/imdetail"; /** 聊天配置*/

// 消息盒子
#pragma mark - 消息盒子
static NSString *const messageState = @"/po/member/msgbox/msgsstate";
static NSString *const messageLottery = @"/po/member/msgbox/guessmsgs"; /**< 竞猜消息*/
static NSString *const messageSystem = @"/po/member/msgbox/pandamsgs"; /**< 系统消息*/
static NSString *const messageSnatch = @"/po/member/msgbox/treasuremsgs"; /**< 夺宝消息*/
static NSString *const messageSocial = @"/po/member/msgbox/socialMsgs"; /**< 社交消息*/
static NSString *const messageSystemRead = @"/po/member/msgbox/pandamsg/read";  /**< 查看系统消息*/
static NSString *const messageSnatchRead = @"/po/member/msgbox/treasuremsg/read"; /**< 查看夺宝消息*/
static NSString *const messageSocialRead = @"/po/member/msgbox/socialmsg/read"; /**< 查看社交消息*/
static NSString *const messageLotteryRead = @"/po/member/msgbox/guessmsg/read"; /**< 查看竞猜消息*/
static NSString *const messageDeleteSystem = @"/po/member/msgbox/pandamsg/delete"; /**< 删除系统消息*/
static NSString *const messageDeleteSnatch = @"/po/member/msgbox/treasuremsg/delete"; /**< 删除夺宝消息*/
static NSString *const messageDeleteSocial = @"/po/member/msgbox/socialmsg/delete"; /**< 删除社交消息*/
static NSString *const messageDeleteLottery = @"/po/member/msgbox/guessmsg/delete"; /**< 删除竞猜消息*/
/**
 * 野区
 */
static NSString *const messagePost = @"/po/member/msgbox/postMsgs";
/**
 * 读野区信息
 */
static NSString *const messagePostRead = @"/po/member/msgbox/postmsg/read";
/**
 * 删除野区信息
 */
static NSString *const messagePostDelete = @"/po/member/msgbox/postmsg/delete";
//【】

//////////////////////////////////// 英雄猜 ////////////////////////////////////
#pragma mark - 英雄猜
//////// lol
static NSString *const lotteryHeroLolLastedDetail = @"/po/member/guess/latestchampiondayguess"; /**< 最新英雄猜详情*/
static NSString *const lotteryHeroLolGetGuessOdds = @"/po/member/guess/championguessodds"; /**< 获取赔率*/
static NSString *const lotteryHeroLolLotteriesRecord = @"/po/member/guess/championdayguesslotteries";  /**< 开奖记录*/
static NSString *const lotteryHeroLolInputRecord = @"/po/member/guess/championguessstakes"; /**< 投注记录*/
static NSString *const lotteryHeroLolInput = @"/po/member/guess/stakechampionguess"; /**< 投注*/
static NSString *const lotteryHeroLolRule = @"/po/html/lolchampionguess/gzsm.html";
static NSString *const lotteryHeroLolDailyHistory = @"/po/member/guessrecord/todaylolchampiondraw"; /**< 历史分析-今日中奖列表*/
static NSString *const lotteryHeroLolDailyStatistics = @"/po/member/guessrecord/todaylolchampionstatistic"; /**< 历史分析-今日统计*/
static NSString *const lotteryHeroLolRankList = @"/po/member/guess/championdayguessstaketoplist"; /**< 英雄猜排行榜*/
///////// dota2
/**
 * 最近一期英雄猜
 */
static NSString *const lotteryHeroDota2LastedDetail = @"/po/member/dota2championguess/latestdayguess";
/**
 * 开奖纪录
 */
static NSString *const lotteryHeroDota2LotteriesRecord = @"/po/member/dota2championguess/daylotteries";
/**
 * 获取peilv
 */
static NSString *const lotteryHeroDota2GetGuessOdds = @"/po/member/dota2championguess/odds";

/**
 * 投注纪录
 */
static NSString *const lotteryHeroDota2InputRecord = @"/po/member/dota2championguess/stakes";
/**
 * 投注排行
 */
static NSString *const lotteryHeroDota2RankList = @"/po/member/dota2championguess/daystaketoplist";
/**
 * 投注
 */
static NSString *const lotteryHeroDota2Input = @"/po/member/dota2championguess/stake";
/**
 * 今日中奖列表
 */
static NSString *const lotteryHeroDota2HistoryList = @"/po/member/dota2championguessrecord/todaydraw";
/**
 * 今日统计
 */
static NSString *const lotteryHeroDota2HistoryStatistics = @"/po/member/dota2championguessrecord/todaystatistic";

//////// 王者荣耀
/**
 * 最近一期英雄猜
 */
static NSString *const lotteryHeroPvpLastedDetail = @"/po/member/kingchampionguess/latestdayguess";
/**
 * 开奖纪录
 */
static NSString *const lotteryHeroPvpLotteriesRecord = @"/po/member/kingchampionguess/daylotteries";
/**
 * 获取peilv
 */
static NSString *const lotteryHeroPvpGetGuessOdds = @"/po/member/kingchampionguess/odds";
/**
 * 投注纪录
 */
static NSString *const lotteryHeroPvpInputRecord = @"/po/member/kingchampionguess/stakes";
/**
 * 投注排行
 */
static NSString *const lotteryHeroPvpRankList = @"/po/member/kingchampionguess/daystaketoplist";
/**
 * 投注
 */
static NSString *const lotteryHeroPvpInput = @"/po/member/kingchampionguess/stake";
/**
 * 今日中奖列表
 */
static NSString *const lotteryHeroPvpHistoryList = @"/po/member/kingchampionguessrecord/todaydraw";
/**
 * 今日统计
 */
static NSString *const lotteryHeroPvpHistoryStatistics = @"/po/member/kingchampionguessrecord/todaystatistic";

/////////////////////////////////// 夺宝 ////////////////////////////////////
#pragma mark - 夺宝
static NSString *const snatchWinerList = @"/po/member/tla/winlist"; /**< 往期中奖名单*/
static NSString *const snatchBanner = @"/po/member/tla/banners"; /**< 夺宝banner*/
static NSString *const snatchAllProduct = @"/po/member/tla/alllist"; /**< 所有商品*/
static NSString *const snatchNewList = @"/po/member/tla/recentlist";    /**< 最新揭晓*/
static NSString *const snatchProductImagetextDetail = @"/po/member/tla/commoditydetail";    /**< 奖品图文详情*/
static NSString *const snatchJoinRecord = @"/po/member/tla/joinlist"; /**< 参与记录*/
static NSString *const snatchProductDetail = @"/po/member/tla/detail";   /**< 夺宝详情*/
static NSString *const snatchStake = @"/po/member/tla/stake";   /**< 投入竹子*/
static NSString *const snatchNextId = @"/po/member/tla/nextid"; /**< 下一期活动id*/
static NSString *const snatchExit = @"/po/member/tla/quitdetail";   /**< 退出查看夺宝活动详情*/
static NSString *const snatchRule = @"/po/html/activity/dbgz.html";    /**< 夺宝规则*/
static NSString *const snatchGetNewId = @"/po/member/tla/latestlotteryid";  /**< 最新活动期号*/
/**
 * 分享夺宝
 */
static NSString *const snatchShareHelp = @"/po/member/tla/sharehelp";


#pragma mark - 娱乐
static NSString *const amusementNotice = @"/po/member/entertainment/notifynoticelist"; /// 公告
static NSString *const amusementBanner = @"/po/member/entertainment/banners"; /// 娱乐轮播
/**
 * 娱乐模块详情
 */
static NSString *const amusementPartList = @"/po/member/entertainment/posterlist";

static NSString *const amusementRankTh = @"/po/member/top/goldbalance"; /// 土豪榜
static NSString *const amusementRankCz = @"/po/member/top/recharge"; /// 充值榜
static NSString *const amusementRankYl = @"/po/member/top/profit";  /// 盈利榜
static NSString *const amusementTheGuide = @"/po/html/panda/help.html"; ///  新手教程



// 【发现】
static NSString *const findSegmentList = @"po/news/categories";            /**< 获取发现里面的segmentList*/
static NSString *const findInformation = @"po/news/newslist";              /**< 获取新闻详情列表*/

// 【首页】
static NSString *const neearBars = @"po/member/near/limitbars";            /**< 获取战场网吧*/
static NSString *const homeTreasures = @"po/member/tla/treasures";               /**< 获取当前的最新夺宝*/
static NSString *const membergameuserstat = @"po/member/lol/membergameuserstat";    /**< 获取首页信息*/
static NSString *const memberLolSettle = @"po/member/lol/settle";                   /**< 手动获取首页信息*/
static NSString *const memberGuessMatchguess = @"po/member/guess/matchguess";       /**< 首页获取竞猜*/

// 【附近】
static NSString *const nearbyCustomer = @"po/member/near/members";          /**< 获取附近的会员*/
static NSString *const personInfo = @"po/member/other/info";                /**< 获取用户信息*/
static NSString *const changePosition = @"po/member/near/changeposition";   /**< 修改用户当前的经纬度*/


// 【网吧】
static NSString *const myNetbarCollection = @"po/member/bars/followedbars"; /**< 我收藏的网吧*/
static NSString *const myNearNetbarList = @"po/member/near/bars";           /**< 我附近的网吧列表*/
static NSString *const barCollection = @"po/member/bars/followbar";         /**< 关注网吧*/
static NSString *const barNotCollection = @"po/member/bars/unfollowbar";    /**< 取消关注网吧*/
static NSString *const barDetail = @"po/member/near/bar/detail";            /**< 网吧详情*/
static NSString *const barActivity = @"po/member/near/bar/activities";      /**< 网吧活动*/

// 【绑定】
static NSString *const unbindGameUser = @"po/member/lol/unbindgameuser";     /**< 解绑召唤师*/
static NSString *const gameUser = @"po/member/lol/gameuser";                 /**< 获取角色详情*/
static NSString *const bindingGameserver = @"/po/member/lol/gameserver";    /**< 获取当前绑定大区*/
static NSString *const bindingRole = @"/po/member/lol/bindgameuser";        /**< 绑定用户*/
static NSString *const activateGameUser = @"/po/member/lol/activategameuser";/**< 激活召唤师 ---- */
static NSString *const dealactivategameuser = @"/po/member/lol/dealactivategameuser";           /**< 我已认证*/
static NSString *const gamerecorddetail = @"/po/member/lol/gamerecorddetail";   /**< 获取战绩详情*/


static NSString *const memberGameUser = @"/po/member/lol/membergameuser";   /**< 我的召唤师*/
static NSString *const bindSpecial = @"/po/character/special";              /**< 获取特殊字符*/
static NSString *const gamerUserRecord = @"/po/member/lol/gameuserrecord";  /**< 获取当前战绩*/
static NSString *const refusetoactivate = @"/po/member/lol/refusetoactivate";  /**< 稍后认证*/

// 【个人中心】
static NSString *const thirdbindinfo = @"/po/member/thirdbindinfo";         /**< 获取账号绑定信息*/

// 【赛事列表】
static NSString *const matchguesslist = @"/po/member/guess/matchguesslist"; /**< 赛事列表*/
static NSString *const matchDetail = @"/po/member/guess/matchguessdetail";  /**< 赛事详情*/
static NSString *const matchguessstakelist = @"/po/member/guess/matchguessstakelist";   /**< 投注详情*/
static NSString *const drawremind = @"/po/member/guess/matchstartremind";                     /**< 添加竞猜*/
static NSString *const removedrawremind = @"/po/member/guess/removematchstartremind";         /**< 取消竞猜*/
static NSString *const gamelist = @"/po/member/guess/gamelist";                     /**< 竞猜列表*/
static NSString *const requeststakematchguess = @"/po/member/guess/requeststakematchguess";     /**< 将要去竞猜*/
static NSString *const stakematchguess = @"/po/member/guess/stakematchguess";               /**< 进行投注*/
static NSString *const matchguessinfo = @"/po/member/guess/matchguessinfo";             /***/

// cons
// 【主播猜】
static NSString *const latestanchoractivity = @"/po/member/guess/latestanchoractivity";
static NSString *const anchorguesslist = @"/po/member/guess/anchorguesslist";               /**< 获取主播竞猜信息*/
static NSString *const guessQuitlatestanchoractivity = @"/po/member/guess/quitlatestanchoractivity";    /**< 退出主播猜*/
static NSString *const guessQuitanchorguesslist = @"/po/member/guess/quitanchorguesslist";              /**< 退出查看竞猜列表*/
static NSString *const guessStakeanchorguess = @"/po/member/guess/stakeanchorguess";                    /**< 投注*/

static NSString *const test = @"locationBeta/stores/facilities/lite";
static NSString *const pingguoshenhe = @"/class/company/panda/pandaShenhe.ashx";
static NSString *const shenhe = @"/po/ios/version";

// 【新手引导】
static NSString *const memberguideline = @"/po/member/guideline/memberguideline";
static NSString *const guidelineComplete = @"/po/member/guideline/complete";


// 【HTML】
static NSString *const bindgameuserrules = @"/po/html/member/lol/bindgameuserrules.html";           /**< 绑定规则*/
static NSString *const dbgz = @"/po/html/activity/dbgz.html";                                       /**< 夺宝规则*/
static NSString *const conversionrules = @"/po/html/cer/conversionrules.html";                      /**< 兑换规则*/
static NSString *const aboutus = @"/po/html/panda/aboutus.html";                                    /**< 关于我们*/
static NSString *const gzgz = @"/po/html/gamematchguess/gzgz.html";                                 /**< 竞猜规则*/
static NSString *const gzsm = @"/po/html/lolchampionguess/gzsm.html";                               /**< 竞猜说明*/
static NSString *const loginandsign = @"/po/member/task/loginandsign";                              /**< 签到*/

// 【转账】
static NSString *const requesttransfergoldtomem = @"/po/member/other/requesttransfergoldtomem";     /**< 请求金币转账*/
static NSString *const transfergoldtomem = @"/po/member/other/transfergoldtomem";                   /**< 进行金币转账*/

// 【个人资产】
static NSString *const entertainmentMemasset = @"/po/member/entertainment/memasset";                /**< 获取个人资产*/
static NSString *const winList = @"/po/member/tla/wininfolist";

static NSString *const gameuserdetail = @"/po/member/lol/gameuserdetail";
static NSString *const matchlist = @"/po/member/lol/matchlist";

#pragma mark - 获取用户信息
static NSString *const slidermemberinfo = @"/po/member/slider/slidermemberinfo";
static NSString *const sliderList = @"/po/member/slider/sliderlist";
static NSString *const sliderotherinfo = @"/po/member/slider/sliderotherinfo";


#pragma mark - 野区
static NSString *const postMessage = @"/bbs/post/addPost";
static NSString *const postImages = @"/bbs/upload/uploadimg";
/**
 * 删除帖子
 */
static NSString *const postDelete = @"/bbs/post/deletePost";
/**
 * 获取举报列表
 */
static NSString *const postReportList = @"/bbs/report/listReportReason";
/**
 * 举报这个帖子
 */
static NSString *const postReport = @"/bbs/report/addReportReason";
/**
 * 野区列表 web
 */
static NSString *const postList = @"/bbs/post_bar/home.html";
static NSString *const postWebDetail = @"/bbs/post_bar/detail.html";
static NSString *const postJungle = @"/bbs/post_bar/jungle.html";
/**
 * 野区分享详情
 */
static NSString *const postShareInfo = @"/bbs/post/sharePost";
/**
 * 野区列表
 */
static NSString *const postBarLists = @"/bbs/post/listAllPostInfo";
/**
 * 点赞和取消赞
 */
static NSString *const postPraise = @"/bbs/upvote/good";
/**
 * 帖子详情
 */
static NSString *const postDetail = @"/bbs/post/findPost";
/**
 * 点赞列表
 */
static NSString *const postPriseList = @"/bbs/upvote/listGoodByPostId";
/**
 * 帖子回复列表
 */
static NSString *const postReplyList = @"/bbs/post/postAndReply";
/**
 * 只看楼主
 */
static NSString *const postHostReplyList = @"/bbs/post/onlyLookHost";
/**
 * 发表回复
 */
static NSString *const postReplyHost = @"/bbs/post/addReply";
/**
 * 回复评论
 */
static NSString *const postReplyComment = @"/bbs/post/addReplyReply";
/**
 * 删除评论
 */
static NSString *const postDeleteReply = @"/bbs/post/deleteReply";

#pragma mark - 红包相关
/// 获取红包id
static NSString *const packetGetId = @"po/member/chat/createredenvelope";
/// 查看红包
static NSString *const packetCheckInfo = @"po/member/chat/lookredenvelope";
/// 打开红包 显示详情
static NSString *const packetOpen = @"po/member/chat/snatchredenvelope";
/// 显示红包详情
static NSString *const packetDetail = @"po/member/chat/redenvelopedetail";


// 打野
static NSString *const todayloljunglestatistic = @"/po/member/guessrecord/todayloljunglestatistic";
static NSString *const todaydota2junglestatistic = @"/po/member/guessrecord/todaydota2junglestatistic";
static NSString *const todaykingjunglestatistic = @"/po/member/guessrecord/todaykingjunglestatistic";

static NSString *const todayloljungledraw = @"/po/member/guessrecord/todayloljungledraw";
static NSString *const todaydota2jungledraw = @"/po/member/guessrecord/todaydota2jungledraw";
static NSString *const todaykingjungledraw = @"/po/member/guessrecord/todaykingjungledraw";





#pragma mark - LOL英雄杀
/**
 * 入口金币场列表
 */
static NSString *const scuffleEnterList = @"/po/member/cardsgame/groundlist";
/**
 * 开始匹配
 */
static NSString *const scuffleStartMatching = @"/po/member/cardsgame/enterground";
/**
 * 取消匹配、逃跑
 */
static NSString *const scuffleStopMatching = @"/po/member/cardsgame/quit";
/**
 * 我方卡牌信息
 */
static NSString *const scuffleMineCardsInfo = @"/po/member/cardsgame/cards";
/**
 * 根据id获取用户信息
 */
static NSString *const getMemberDetailInfo = @"/po/member/other/oneim";
/**
 * 准备好了
 */
static NSString *const scufflePlayCards = @"/po/member/cardsgame/decidestrategy";
/**
 * 继续
 */
static NSString *const scuffleContinue = @"/po/member/cardsgame/continuemate";

#pragma mark - 上架信息
// 上架信息
static NSString *const appconfigNewmoduleFind = @"/po/appconfig/newmodule/find";

static NSString *const appversionGet = @"/po/appconfig/appversion/get";






// 【约战】
static NSString *const combatgame = @"/po/member/combatgame/index";
static NSString *const allgameuser = @"/po/member/combatgame/allgameuser";
static NSString *const gameserverlist = @"/po/member/combatgame/gameserverlist";
static NSString *const addgameuser = @"/po/member/combatgame/addgameuser";
static NSString *const deletegameuser =@"/po/member/combatgame/deletegameuser";
static NSString *const editgameuser = @"/po/member/combatgame/editgameuser";                /**< 编辑召唤师*/
static NSString *const waitconfig = @"/po/member/combatgame/waitconfig";                    /**< 等待时间配置*/
static NSString *const createcombat = @"/po/member/combatgame/createcombat";                /**< 创建*/
static NSString *const combatdetail = @"/po/member/combatgame/combatdetail";                /**< 详情*/
static NSString *const cancelcombat = @"/po/member/combatgame/cancelcombat";                /**< 解散*/
static NSString *const requestsettle = @"/po/member/combatgame/requestsettle";              /**< 请求结算*/
static NSString *const readycombat = @"/po/member/combatgame/readycombat";                  /**< 准备约战*/
static NSString *const judgePassword = @"/po/member/combatgame/judgePassword";              /**< 判断密码*/
static NSString *const combatlist = @"/po/member/combatgame/combatlist";                    /**< 约战历史*/
static NSString *const joincombat = @"/po/member/combatgame/joincombat";                    /**< 加入*/
static NSString *const joinencryptcombat = @"/po/member/combatgame/joinencryptcombat";      /**< 加密*/
static NSString *const gameuserlist = @"/po/member/combatgame/gameuserlist";                /**< 获取大区召唤师*/

// 【新约战】
static NSString *const pubgcombatgame = @"/po/member/pubgcombatgame/index";                 /**< 新约战主页*/
static NSString *const creategameuser = @"/po/member/pubgcombatgame/creategameuser";        /**< 绑定游戏ID*/
static NSString *const pubgcombatgameJoin = @"/po/member/pubgcombatgame/join";              /**< 加入约战*/
static NSString *const pubgcombatgameDetail = @"/po/member/pubgcombatgame/detail";          /**< 约战详情*/
static NSString *const pubgcombatgameList = @"/po/member/pubgcombatgame/list";              /**< 历史列表*/
static NSString *const pubgcombatgameGameuser = @"/po/member/pubgcombatgame/gameuser";      /**< user*/


// 2017 - 10.10 获取图形验证码
static NSString *const memberverifycode  = @"/po/auth/member/verifycode";

/**
 * 修改绑定的手机号
 */
static NSString *const changeBindPhoneNumber = @"/po/member/changephone";


#endif /* URLConstance_h */




