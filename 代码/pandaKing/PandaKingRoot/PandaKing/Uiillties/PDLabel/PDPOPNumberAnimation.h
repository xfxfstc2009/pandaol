//
//  PDPOPNumberAnimation.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 动画的数字
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <pop/POP.h>

@class PDPOPNumberAnimation;

@protocol PDPOPNumberAnimationDelegate <NSObject>

@required
- (void)POPNumberAnimation:(PDPOPNumberAnimation *)numberAnimation currentValue:(CGFloat)currentValue;

@end

@interface PDPOPNumberAnimation : NSObject

@property (nonatomic, weak) id <PDPOPNumberAnimationDelegate>  delegate;
@property (nonatomic,assign)CGFloat fromValue;
@property (nonatomic,assign)CGFloat toValue;
@property (nonatomic,assign)CGFloat currentValue;
@property (nonatomic,assign)NSTimeInterval duration;
@property (nonatomic, strong)CAMediaTimingFunction *timingFunction;

- (void)saveValues;

- (void)startAnimation;

- (void)stopAnimation;

@end
