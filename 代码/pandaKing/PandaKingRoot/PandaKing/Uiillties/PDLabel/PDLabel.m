//
//  PDLabel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLabel.h"

@implementation PDLabel

-(instancetype) initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self setupManager];
    }
    return self;
}

#pragma mark - setup
-(void)setupManager{
    self.glowInSize = 0.0f;
    self.glowInColor = [UIColor clearColor];
    self.glowOutSize = 0.0;
    self.glowOutColor = [UIColor clearColor];
}


#pragma mark - 光晕
- (void)drawTextInRect:(CGRect)rect {
    if (self.text == nil || self.text.length == 0) {
        return;
    }
    if (self.glowShow) {
        [self drawTextInRectForGlow:rect];
    }
}


#pragma mark - 放光
- (void)drawTextInRectForGlow:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    
    UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);
    
    [super drawTextInRect:rect];
    UIImage *textImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    CGContextSaveGState(ctx);
    if (_glowOutSize > 0) {
        CGContextSetShadow(ctx, CGSizeZero, _glowOutSize);
        CGContextSetShadowWithColor(ctx, CGSizeZero, _glowOutSize, _glowOutColor.CGColor);
    }
    [textImage drawAtPoint:rect.origin];
    CGContextRestoreGState(ctx);
    
    if (_glowInSize > 0) {
        
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, 0.0);
        CGContextRef ctx2 = UIGraphicsGetCurrentContext();
        CGContextSaveGState(ctx2);
        CGContextSetFillColorWithColor(ctx2, [UIColor blackColor].CGColor);
        CGContextFillRect(ctx2, rect);
        CGContextTranslateCTM(ctx2, 0.0, rect.size.height);
        CGContextScaleCTM(ctx2, 1.0, -1.0);
        CGContextClipToMask(ctx2, rect, textImage.CGImage);
        CGContextClearRect(ctx2, rect);
        CGContextRestoreGState(ctx2);
        
        UIImage *inverted = UIGraphicsGetImageFromCurrentImageContext();
        CGContextClearRect(ctx2, rect);
        
        CGContextSaveGState(ctx2);
        CGContextSetFillColorWithColor(ctx2, _glowInColor.CGColor);
        CGContextSetShadowWithColor(ctx2, CGSizeZero, _glowInSize, _glowInColor.CGColor);
        [inverted drawAtPoint:CGPointZero];
        CGContextTranslateCTM(ctx2, 0.0, rect.size.height);
        CGContextScaleCTM(ctx2, 1.0, -1.0);
        CGContextClipToMask(ctx2, rect, inverted.CGImage);
        CGContextClearRect(ctx2, rect);
        CGContextRestoreGState(ctx2);
        
        UIImage *innerShadow = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        [innerShadow drawAtPoint:rect.origin];
    }
}


@end
