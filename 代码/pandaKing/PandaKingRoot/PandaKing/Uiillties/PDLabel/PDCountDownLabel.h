//
//  PDCountDownLabel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDCountDownLabel : UILabel

@property (nonatomic,assign)NSInteger countDown;

@end
