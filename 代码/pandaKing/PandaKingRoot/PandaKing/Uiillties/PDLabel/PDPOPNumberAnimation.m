//
//  PDPOPNumberAnimation.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPOPNumberAnimation.h"

@interface PDPOPNumberAnimation()
@property (nonatomic, strong) POPBasicAnimation  *conutAnimation;

@end

@implementation PDPOPNumberAnimation

- (instancetype)init {
    
    if (self = [super init]) {
        
        self.conutAnimation = [POPBasicAnimation animation];
    }
    
    return self;
}

- (void)saveValues {
    
    self.conutAnimation.fromValue = @(self.fromValue);
    self.conutAnimation.toValue   = @(self.toValue);
    self.conutAnimation.duration  = (self.duration <= 0 ? 0.4f : self.duration);
    
    if (self.timingFunction) {
        
        self.conutAnimation.timingFunction = self.timingFunction;
    }
}

- (void)startAnimation {
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(POPNumberAnimation:currentValue:)]) {
        
        __weak PDPOPNumberAnimation *weakSelf = self;
        
        // 将计算出来的值通过writeBlock动态给控件设定
        self.conutAnimation.property = \
        [POPMutableAnimatableProperty propertyWithName:@"conutAnimation" initializer:^(POPMutableAnimatableProperty *prop) {
            
            prop.writeBlock = ^(id obj, const CGFloat values[]) {
                
                weakSelf.currentValue = values[0];
                [weakSelf.delegate POPNumberAnimation:weakSelf currentValue:values[0]];
            };
        }];
        
        // 添加动画
        [self pop_addAnimation:self.conutAnimation forKey:nil];
    }
}

- (void)stopAnimation {
    
    [self pop_removeAllAnimations];
}


@end
