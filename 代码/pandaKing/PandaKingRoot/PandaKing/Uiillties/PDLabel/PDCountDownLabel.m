//
//  PDCountDownLabel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCountDownLabel.h"

@interface PDCountDownLabel()
@property (nonatomic,strong)NSTimer *timer;         /**< 计时器*/
@end

@implementation PDCountDownLabel

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    
}

-(void)setCountDown:(NSInteger)countDown{
    _countDown = countDown;
    
}
@end
