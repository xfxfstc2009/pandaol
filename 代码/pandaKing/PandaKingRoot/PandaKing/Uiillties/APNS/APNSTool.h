//
//  APNSTool.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CloudPushSDK/CloudPushSDK.h>

@interface APNSTool : NSObject

+ (instancetype)shareInstance;                                  /**< 单利*/
-(void)registWithPUSHWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions deviceIdBlock:(void(^)())block;
#pragma mark - 注册DeviceId
-(void)registerWithDeviceId:(NSData *)deviceToken;
#pragma mark - 通知统计回调
-(void)registerWithReceiveRemoteNotification:(NSDictionary*)userInfo ;

@property (nonatomic,copy)NSString *deviceToken;

@end
