//
//  PDApnsControllerManager.h
//  PandaKing
//
//  Created by Cranz on 16/10/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

// 9eadd3b2aa4947d9aedac2374de3157e  SE 设备ID
// fd260a3d8a2f4789a5589ed808e99394  Cranz's iPhone6
// 当APP关闭的时候需要在Appdelegate中主动接受远程推送
// [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey] 

typedef NS_ENUM(NSUInteger, ApnsType) {
    ApnsTypeSnatch = 851,   /**< 夺宝推送*/
    ApnsTypeMessage = 854,  /**< 消息盒子推送*/
    ApnsTypeFans = 852,   /**< 关注推送，跳粉丝*/
    ApnsTypePresent = 853,  /**< 赠送，背包已拥有*/
    ApnsTypeMatchWin = 855, /**< 赛事猜中奖*/
    ApnsTypeMatchStop = 856, /**< 赛事猜投注截止*/
    ApnsTypeMatchStart = 862, /**< 赛事猜比赛开始*/
    ApnsTypeMatchCancle = 867, /**< 赛事猜竞猜取消*/
    ApnsTypeHeroWin = 858, /**< 英雄猜中奖*/
};

@interface PDApnsControllerManager : NSObject
/** 根据字典的内容条转*/
+ (void)apnsWithLaunchOptions:(NSDictionary *)launchOptions;
+ (void)apnsJumpDetailWithUserInfo:(NSDictionary *)userInfo;

#pragma mark - 跳转条目
+ (void)apnsManagerToSnatchDetailWithId:(NSString *)ID;
/** 跳粉丝*/
+ (void)apnsManagerToFansList;
+ (void)apnsManagerToBackpack;
+ (void)apnsManagerToMessageDetailWithType:(NSString *)type contentUrl:(NSString *)url title:(NSString *)title text:(NSString *)text msgTime:(NSInteger)msgTime;
+ (void)apnsManagerToMatchWin;
+ (void)apnsManagerToMatchStopWithGameId:(NSString *)gameId;
+ (void)apnsManagerToMatchCancleWithTitle:(NSString *)title text:(NSString *)text msgTime:(NSInteger)msgTime;
+ (void)apnsManagerToMatchStartWithGameId:(NSString *)gameId;
+ (void)apnsManagerToHeroWinWithType:(int)type;

#pragma mark - 跳到聊天
+ (void)apnsJumpToChatWithId:(NSDictionary *)dic;
@end
