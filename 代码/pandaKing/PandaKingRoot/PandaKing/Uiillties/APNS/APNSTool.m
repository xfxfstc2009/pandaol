//
//  APNSTool.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "APNSTool.h"
#import "PDApnsControllerManager.h"

#define kAPNS_KEY    @"23330943" // 23330943
#define kAPNS_SECRET @"1a87d709667be873ead5f738df4e1c7c" // 1a87d709667be873ead5f738df4e1c7c

@implementation APNSTool

+ (instancetype)shareInstance{
    static APNSTool *pushManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        pushManager = [[APNSTool alloc] init];
    });
    return pushManager;
}

- (void)registWithPUSHWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions deviceIdBlock:(void(^)())block{
    
    // 注册苹果推送
    [self registerAPNSWithApplication:application launchOptions:launchOptions];
    
    // 初始化SDK
    [self initSdkWithBlock:block];
    
    // 监听推送通道打开动作
    [self listenerOnChannelOpened];
    // 监听推送消息到达
    [self registerMessageReceive];
    
    // 点击通知将App从关闭状态启动时，将通知打开回执上报
    [CloudPushSDK sendNotificationAck:launchOptions];
    
    DLog(@"SDK 版本 %@",[CloudPushSDK getVersion]);
}


// push sdk 初始化
- (void)initSdkWithBlock:(void(^)())block{                   //sdk初始化
    [CloudPushSDK asyncInit:kAPNS_KEY appSecret:kAPNS_SECRET callback:^(CloudPushCallbackResult *res) {
        if (res.success) {
            [APNSTool shareInstance].deviceToken = [CloudPushSDK getDeviceId];
            DLog(@"deviceToken ====== %@",[APNSTool shareInstance].deviceToken);
            if (block){
                block();
            }
        } else {
            DLog(@"Push SDK init failed, error: %@",res.error);
        }
    }];
}

/**
 *	注册推送通道打开监听
 */
- (void) listenerOnChannelOpened {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onChannelOpened:)
                                                 name:@"CCPDidChannelConnectedSuccess"
                                               object:nil];
}

/**
 *    注册推送消息到来监听
 */
- (void)registerMessageReceive {
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onMessageReceived:)
                                                 name:@"CCPDidReceiveMessageNotification"
                                               object:nil];
}

#pragma mark 注册苹果的推送
- (void)registerAPNSWithApplication:(UIApplication *)application launchOptions:(NSDictionary *)launchOptions{
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
//        // iOS 8 Notifications
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored"-Wdeprecated-declarations"
//        [application registerUserNotificationSettings:
//         [UIUserNotificationSettings settingsForTypes:
//          (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge)
//                                           categories:nil]];
//        [application registerForRemoteNotifications];
//#pragma clang diagnostic pop
//    }
//    else {
//        // iOS < 8 Notifications
//#pragma clang diagnostic push
//#pragma clang diagnostic ignored"-Wdeprecated-declarations"
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:
//         (UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)];
//#pragma clang diagnostic pop
//    }
    
    /**
     注册APNS离线推送  iOS8 注册APNS
     */
    
    if ([application respondsToSelector:@selector(registerForRemoteNotifications)]) {
        [application registerForRemoteNotifications];
        UIUserNotificationType notificationTypes = UIUserNotificationTypeBadge |
        UIUserNotificationTypeSound |
        UIUserNotificationTypeAlert;
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:notificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
    }
    else{
        UIRemoteNotificationType notificationTypes = UIRemoteNotificationTypeBadge |
        UIRemoteNotificationTypeSound |
        UIRemoteNotificationTypeAlert;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:notificationTypes];
    }
}

/*
 *  苹果推送注册成功回调，将苹果返回的deviceToken上传到CloudPush服务器
 */
-(void)registerWithDeviceId:(NSData *)deviceToken{
    [CloudPushSDK registerDevice:deviceToken withCallback:nil];
}

/*
 *  App处于启动状态时，通知打开回调
 */
-(void)registerWithReceiveRemoteNotification:(NSDictionary*)userInfo {
    [CloudPushSDK sendNotificationAck:userInfo];
    
    if ([UIApplication sharedApplication].applicationState == UIApplicationStateActive) {
        return;
    }
    
    [PDApnsControllerManager apnsJumpDetailWithUserInfo:userInfo];
}

/**
 *    处理到来推送消息
 *
 *    @param     notification
 */
- (void)onMessageReceived:(NSNotification *)notification {
//    CCPSysMessage *message = [notification object];
//    NSString *title = [[NSString alloc] initWithData:message.title encoding:NSUTF8StringEncoding];
//    NSString *body = [[NSString alloc] initWithData:message.body encoding:NSUTF8StringEncoding];
//    NSLog(@"Receive message title: %@, content: %@.", title, body);
    
}

- (void)onChannelOpened:(NSNotification *)notification {
    
}

@end
