//
//  PDBaseMapView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 1. 基础mapView

#import <UIKit/UIKit.h>
#import <MAMapKit/MAMapKit.h>

@interface PDBaseMapView : UIView

@property (nonatomic,assign)CLLocationCoordinate2D transferCoordinate;          /**< 当前需要展示的经纬度*/

@end
