//
//  MMHLocationNavigationManager.m
//  MamHao
//
//  Created by SmartMin on 15/6/18.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHLocationNavigationManager.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "JZLocationConverter.h"
@implementation MMHLocationNavigationManager

#pragma mark 1. 苹果自带导航
-(void)locationNavigationSelfWithTargetLoation:(CGFloat)latitude longitude:(CGFloat)longitude targetLocationName:(NSString *)targetLocationName{
    CLLocationCoordinate2D targetLocation;
    
    targetLocation.latitude = latitude;
    targetLocation.longitude = longitude;
    MKMapItem *currentLocation = [MKMapItem mapItemForCurrentLocation];
    MKMapItem *toLocation = [[MKMapItem alloc] initWithPlacemark:[[MKPlacemark alloc] initWithCoordinate:targetLocation addressDictionary:nil]];
    
    toLocation.name = targetLocationName;
    [MKMapItem openMapsWithItems:[NSArray arrayWithObjects:currentLocation, toLocation, nil] launchOptions:[NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:MKLaunchOptionsDirectionsModeDriving, [NSNumber numberWithBool:YES], nil] forKeys:[NSArray arrayWithObjects:MKLaunchOptionsDirectionsModeKey, MKLaunchOptionsShowsTrafficKey, nil]]];
}

#pragma mark 2. 百度导航
-(void)locationNavigationBaiduWithCurrentLocation:(CGFloat)currentLatitude longitude:(CGFloat)currentLongitude targetLocation:(CGFloat)targetLatitude longitude:(CGFloat)targetLongitude targetLocationName:(NSString *)targetLocationName{
    
    CLLocationCoordinate2D currentLocation;
    currentLocation.latitude = currentLatitude;
    currentLocation.longitude = currentLongitude;
    CLLocationCoordinate2D currentLocation1 = [JZLocationConverter wgs84ToBd09:currentLocation];
    
    CLLocationCoordinate2D targetLocation;
    targetLocation.latitude = targetLatitude;
    targetLocation.longitude = targetLongitude;
    CLLocationCoordinate2D targetLocation1 = [JZLocationConverter gcj02ToBd09:targetLocation];
    
    
    
    NSString *baiduUrlString = [NSString stringWithFormat:@"baidumap://map/direction?origin=latlng:%f,%f|name:sdaf&destination=latlng:%f,%f|name:%@&mode=driving",currentLocation1.latitude , currentLocation1.longitude ,targetLocation1.latitude ,targetLocation1.longitude  ,targetLocationName] ;
    NSString *urlString = [baiduUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] ;
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlString]];
}

#pragma mark 3. 高德导航
-(void)locationNavigationGaodeWithCurrentLocation:(CGFloat)currentLatitude longitude:(CGFloat)currentLongitude targetLocation:(CGFloat)targetLatitude longitude:(CGFloat)targetLongitude targetLocationName:(NSString *)targetLocationName{
    CLLocationCoordinate2D targetLocation;
    targetLocation.latitude = targetLatitude;
    targetLocation.longitude = targetLongitude;
    
    CLLocationCoordinate2D netLocation = [JZLocationConverter gcj02ToWgs84:targetLocation];
    
    NSString *gaodeUrlString = [NSString stringWithFormat:@"iosamap://navi?sourceApplication=%@&backScheme=%@&poiname=%@&lat=%f&lon=%f&dev=1&style=2",targetLocationName, @"GiganticWhale-FCJJR", @"终点", netLocation.latitude, netLocation.longitude] ;
    NSString *urlString = [gaodeUrlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:urlString]];
    
}




#pragma mark - actionSheet
- (void)showActionSheetWithView:(UIView *)view shopName:(NSString *)shopName shopLocationLat:(CGFloat)shopLat shopLocationLon:(CGFloat)shopLon{
    
    NSMutableArray *buttonTitleMutableArr = [NSMutableArray array];
    [buttonTitleMutableArr addObject:@"取消"];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]){
        [buttonTitleMutableArr addObject:@"百度地图"];
    }
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]){
        [buttonTitleMutableArr addObject:@"高德地图"];
    }
    [buttonTitleMutableArr addObject:@"苹果地图"];
    
    UIActionSheet *sheet = [UIActionSheet actionSheetWithTitle:[NSString stringWithFormat:@"是否打开三方导航软件导航到 %@",shopName] buttonTitles:buttonTitleMutableArr callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        [buttonTitleMutableArr removeObject:@"取消"];
        [buttonTitleMutableArr addObject:@"取消"];
        NSInteger cancelIndex = [buttonTitleMutableArr indexOfObject:@"取消"] ;
        NSInteger baiduIndex = [buttonTitleMutableArr indexOfObject:@"百度地图"];
        NSInteger gaodeIndex = [buttonTitleMutableArr indexOfObject:@"高德地图"];
        NSInteger appleIndex = [buttonTitleMutableArr indexOfObject:@"苹果地图"];
        
        if (buttonIndex == cancelIndex){
            // 取消
        } else if (buttonIndex == baiduIndex){
            [self locationNavigationBaiduWithCurrentLocation:[PDCurrentLocationManager sharedLocation].lat longitude:[PDCurrentLocationManager sharedLocation].lng targetLocation:shopLat longitude:shopLon targetLocationName:shopName];
        } else if (buttonIndex == gaodeIndex){
            [self locationNavigationGaodeWithCurrentLocation:[PDCurrentLocationManager sharedLocation].lat longitude:[PDCurrentLocationManager sharedLocation].lng targetLocation:shopLat longitude:shopLon targetLocationName:shopName];
        } else if (buttonIndex == appleIndex){
            [self locationNavigationSelfWithTargetLoation:shopLat longitude:shopLon targetLocationName:shopName];
        }
    }];
    [sheet showInView:view];
}

#pragma mark - actionSheet
- (void)showActionSheetWithTabBar:(UITabBar *)tabBar shopName:(NSString *)shopName shopLocationLat:(CGFloat)shopLat shopLocationLon:(CGFloat)shopLon{
    
    NSMutableArray *buttonTitleMutableArr = [NSMutableArray array];
    [buttonTitleMutableArr addObject:@"取消"];
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"baidumap://"]]){
        [buttonTitleMutableArr addObject:@"百度地图"];
    }
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"iosamap://"]]){
        [buttonTitleMutableArr addObject:@"高德地图"];
    }
    [buttonTitleMutableArr addObject:@"苹果地图"];
    
    UIActionSheet *sheet = [UIActionSheet actionSheetWithTitle:[NSString stringWithFormat:@"是否打开三方导航软件导航到 %@",shopName] buttonTitles:buttonTitleMutableArr callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
        [buttonTitleMutableArr removeObject:@"取消"];
        [buttonTitleMutableArr addObject:@"取消"];
        NSInteger cancelIndex = [buttonTitleMutableArr indexOfObject:@"取消"] ;
        NSInteger baiduIndex = [buttonTitleMutableArr indexOfObject:@"百度地图"];
        NSInteger gaodeIndex = [buttonTitleMutableArr indexOfObject:@"高德地图"];
        NSInteger appleIndex = [buttonTitleMutableArr indexOfObject:@"苹果地图"];
        
        if (buttonIndex == cancelIndex){
            // 取消
        } else if (buttonIndex == baiduIndex){
            [self locationNavigationBaiduWithCurrentLocation:[PDCurrentLocationManager sharedLocation].lat longitude:[PDCurrentLocationManager sharedLocation].lng targetLocation:shopLat longitude:shopLon targetLocationName:shopName];
        } else if (buttonIndex == gaodeIndex){
            [self locationNavigationGaodeWithCurrentLocation:[PDCurrentLocationManager sharedLocation].lat longitude:[PDCurrentLocationManager sharedLocation].lng targetLocation:shopLat longitude:shopLon targetLocationName:shopName];
        } else if (buttonIndex == appleIndex){
            [self locationNavigationSelfWithTargetLoation:shopLat longitude:shopLon targetLocationName:shopName];
        }
    }];
    [sheet showFromTabBar:tabBar];
}


+(CLLocationCoordinate2D)transferLocation{
    CLLocationCoordinate2D targetLocation;
    targetLocation.latitude = [PDCurrentLocationManager sharedLocation].lat;
    targetLocation.longitude = [PDCurrentLocationManager sharedLocation].lng;
    
    CLLocationCoordinate2D currentLocation1 = [JZLocationConverter wgs84ToGcj02:targetLocation];
    return currentLocation1;
}
@end
