//
//  PDVitamioManager.h
//  CCZMediaPlayer
//
//  Created by Cranz on 16/11/24.
//  Copyright © 2016年 Cranz. All rights reserved.
//

/*
 /////////////////////   1.导入以下库   ///////////////
 
 libz.tbd
 libstdc++.tbd
 libiconv.tbd
 libbz2.tbd
 
 AVFoundation.framework
 AudioToolbox.framework
 CoreMedia.framework
 CoreVideo.framework
 MediaPlayer.framework
 OpenGLES.framework
 QuartzCore.framework
 
 libffmpeg.a
 libopenssl.a
 libVitamio.a
 */

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Vitamio.h"

@interface PDVitamioManager : NSObject<VMediaPlayerDelegate>
@property (nonatomic, copy, readonly) NSString *absluteURL;
@property (nonatomic, assign) BOOL isPlaying;                 /** 是否在播放中*/
@property (nonatomic, assign, readonly) NSInteger mediaDuration;        /**< 获得媒体文件的播放总时间,毫秒级别;-1: error*/
@property (nonatomic, assign, readonly) NSInteger currentPosition;      /** 当前播放到的位置*/
@property (nonatomic, assign) float speed;                              /** 播放速度,default to 1.0, range in [0.5-2]*/
@property (nonatomic, assign) float volume;                             /**< 音量,default to 0.5,range in [0.0-1.0]*/
@property (nonatomic, assign, readonly) CGSize videoSize;               /**< 获得视频播放尺寸Size*/
@property (nonatomic, assign) float videoFillScale;                     /**< video的比例,default to 1.0, range in [0.2-...]*/

+ (id)sharedInstance;

/** 初始配置*/
- (void)setupPlayView:(UIView *)playView;
- (void)setupPlayUrl:(NSString *)url;

/** 初始化完成的回调*/
- (void)setupComplicationHandle:(void(^)(BOOL isSetuped, NSError *error))complication;
/** 播放结束的回调*/
- (void)didEndPlayComplicationHandle:(void(^)(BOOL isEnd, NSError *error))complication;

/** 播放，暂停*/
- (void)playMedia;
- (void)pauseMedia;

/** 重置*/
- (void)resetMedia;

/** 退出播放*/
- (BOOL)stopMedia;

/** 
 跳到某个特定的时刻
 mesc: 毫秒
 sec:  秒
 */
- (void)seekToMesc:(NSInteger)mesc;
- (void)seekToSec:(NSInteger)sec;

/**
 设置视频的播放质量
 default is VIDEOQUALITY_LOW
 */
- (void)setupVideoQuality:(emVMVideoQuality)quality;

/** 获得当前时刻的视频截图*/
- (UIImage *)getCurrentVideoScreenshot;

@end

