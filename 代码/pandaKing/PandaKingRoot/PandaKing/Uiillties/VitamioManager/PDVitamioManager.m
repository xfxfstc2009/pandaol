//
//  PDVitamioManager.m
//  CCZMediaPlayer
//
//  Created by Cranz on 16/11/24.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import "PDVitamioManager.h"

typedef void(^setupComplication)(BOOL, NSError *);
@interface PDVitamioManager ()
@property (nonatomic, strong) NSURL *url;
@property (nonatomic, copy)   setupComplication setupCom;
@property (nonatomic, copy)   setupComplication endCom;
@end

@implementation PDVitamioManager

- (void)setupConfig {
    self.speed = 1.0;
    self.volume = 0.3;
    self.videoFillScale = 1.0;
}

+ (id)sharedInstance {
    static PDVitamioManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PDVitamioManager alloc] init];
    });
    return instance;
}

- (void)setupPlayView:(UIView *)playView {
    [[VMediaPlayer sharedInstance] setupPlayerWithCarrierView:playView withDelegate:self];
    [self setupConfig];
}

- (void)setupPlayUrl:(NSString *)url {
    if (!url.length) {
        return;
    }
    self.url = [NSURL URLWithString:url];
    [UIApplication sharedApplication].idleTimerDisabled = YES;// 播放时不锁屏
    [[VMediaPlayer sharedInstance] setDataSource:self.url];
    [[VMediaPlayer sharedInstance] prepareAsync];
}

- (void)playMedia {
    [[VMediaPlayer sharedInstance] start];
}

- (void)pauseMedia {
    [[VMediaPlayer sharedInstance] pause];
}

- (void)resetMedia {
    [[VMediaPlayer sharedInstance] reset];
    [self setupPlayUrl:self.absluteURL];
}

- (NSString *)absluteURL {
    return self.url.absoluteString;
}

- (BOOL)stopMedia {
    [[VMediaPlayer sharedInstance] reset];
    return [[VMediaPlayer sharedInstance] unSetupPlayer];
}

- (void)setupComplicationHandle:(void(^)(BOOL isSetuped, NSError *error))complication {
    if (complication) {
        self.setupCom = complication;
    }
}

- (void)didEndPlayComplicationHandle:(void (^)(BOOL, NSError *))complication {
    if (complication) {
        self.endCom = complication;
    }
}

- (BOOL)isPlaying {
    return [[VMediaPlayer sharedInstance] isPlaying];
}

- (NSInteger)mediaDuration {
    return [[VMediaPlayer sharedInstance] getDuration];
}

- (NSInteger)currentPosition {
    return [[VMediaPlayer sharedInstance] getCurrentPosition];
}

- (void)seekToMesc:(NSInteger)mesc {
    [[VMediaPlayer sharedInstance] seekTo:mesc];
}

- (void)seekToSec:(NSInteger)sec {
    [[VMediaPlayer sharedInstance] seekTo:sec * 1000];
}

- (void)setSpeed:(float)speed {
    if (speed > 2.0) {
        _speed = 2.0;
    }
    else if (speed < 0.5) {
        _speed = 0.5;
    }
    else {
       _speed = speed;
    }
    
    [[VMediaPlayer sharedInstance] setPlaybackSpeed:speed];
}

- (void)setupVideoQuality:(emVMVideoQuality)quality {
    [[VMediaPlayer sharedInstance] setVideoQuality:quality];
}

- (UIImage *)getCurrentVideoScreenshot {
    return [[VMediaPlayer sharedInstance] getCurrentFrame];
}

- (CGSize)videoSize {
    return CGSizeMake([[VMediaPlayer sharedInstance] getVideoWidth], [[VMediaPlayer sharedInstance] getVideoHeight]);
}

- (void)setVolume:(float)volume {
    if (volume < 0.0) {
        volume = 0.0;
    }
    if (volume > 1) {
        volume = 1.0;
    }
    
    [[VMediaPlayer sharedInstance] setVolume:volume];
}

- (float)volume {
    return [[VMediaPlayer sharedInstance] getVolume];
}

- (void)clearCache {
    [[VMediaPlayer sharedInstance] clearCache];
}

- (long long)diskSize {
    return [[VMediaPlayer sharedInstance] getDiskSize];
}

- (void)setVideoFillScale:(float)videoFillScale {
    if (videoFillScale < 0.2) {
        videoFillScale = 0.2;
    }
    
    [[VMediaPlayer sharedInstance] setVideoFillScale:videoFillScale];
}

- (float)videoFillScale {
    return [[VMediaPlayer sharedInstance] getVideoFillScale];
}

#pragma mark -- delegate

- (void)mediaPlayer:(VMediaPlayer *)player didPrepared:(id)arg {
    if (self.setupCom) {
       self.setupCom(YES, nil);
    }
}

- (void)mediaPlayer:(VMediaPlayer *)player playbackComplete:(id)arg {
    if (self.endCom) {
        self.endCom(YES, nil);
    }
}

- (void)mediaPlayer:(VMediaPlayer *)player error:(id)arg; {
    if (self.setupCom) {
        self.setupCom(NO, (NSError *)arg);
    }
}

@end
