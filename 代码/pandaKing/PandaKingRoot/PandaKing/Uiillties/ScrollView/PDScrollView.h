//
//  PDScrollView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDScrollViewSingleModel.h"

@interface PDScrollView : UIView

@property (nonatomic,assign)BOOL isPageControl;         /**< 是否page*/
@property (nonatomic,strong)NSArray *transferImArr;     /**< 传入数组*/

-(void)startTimerWithTime:(NSInteger)time;
-(void)removeTimer;

-(void)bannerImgTapManagerWithInfoblock:(void(^)(PDScrollViewSingleModel *))block;


@end
