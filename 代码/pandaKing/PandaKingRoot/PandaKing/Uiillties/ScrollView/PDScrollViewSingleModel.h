//
//  PDScrollViewSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDScrollViewSingleModel : FetchModel

@property (nonatomic,copy)NSString *title;           /**< 数据源标题*/
@property (nonatomic,copy)NSString *img;             /**< 数据源图片*/
@property (nonatomic,copy)NSString *actionType;                 /**< 类型*/
@property (nonatomic,copy)NSString *actionEntityId;             /**< id*/


@end
