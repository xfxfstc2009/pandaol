//
//  PDWXPayHandle.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDAlipayOrderInfo.h"

#define kWXPay_AppID      @"wxafbc4f7bdaf0156d"
#define kWXPay_AppDesc    @"PandaOL"

@interface PDWXPayHandle : NSObject
@property (nonatomic, copy) NSString *notifyUrl;

+ (void)regiset;

+ (id)payHandle;
- (void)sendPayRequestWithOrder:(Order *)order;
@end
