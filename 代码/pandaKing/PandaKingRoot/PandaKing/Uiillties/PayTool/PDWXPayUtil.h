//
//  PDWXPayUtil.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDWXPayUtil : NSObject
/*
 加密实现MD5和SHA1
 */
+ (NSString *)md5:(NSString *)str;
+ (NSString*)sha1:(NSString *)str;

+ (NSData *)httpSend:(NSString *)url method:(NSString *)method data:(NSString *)data;
@end

@interface PDWXXmlUtil : NSObject<NSXMLParserDelegate> {
    //解析器
    NSXMLParser *xmlParser;
    //解析元素
    NSMutableArray *xmlElements;
    //解析结果
    NSMutableDictionary *dictionary;
    //临时串变量
    NSMutableString *contentString;
}
//输入参数为xml格式串，初始化解析器
- (void)startParse:(NSData *)data;
//获取解析后的字典
- (NSMutableDictionary*)getDict;

@end