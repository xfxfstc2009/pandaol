//
//  PDWXPayHandle.m
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDWXPayHandle.h"
#import "PDWXPayUtil.h"
#import <UIKit/UIKit.h>
#import "WXApi.h"

#define kWXPay_AppSecret   @"fa06c091a292b082b4e9bc5393c0e23a" //appsecret
#define kWXPay_Partner     @"1333184501" // 商户号
#define kWXPay_NotifyUrl   @""
#define kWXPay_PartnerKey  @"pdk955401acranz5222016year4month"

@implementation PDWXPayHandle {
    //debug信息
    NSMutableString *debugInfo;
}


+ (void)regiset {
    [WXApi registerApp:kWXPay_AppID];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        debugInfo = [NSMutableString string];
    }
    return self;
}

//获取debug信息
-(NSString*) getDebugifo {
    NSString    *res = [NSString stringWithString:debugInfo];
    [debugInfo setString:@""];
    return res;
}

+ (id)payHandle {
    return [[self alloc] init];
}

- (void)sendPayRequestWithOrder:(Order *)order {
    
    
    NSString *appId = kWXPay_AppID;
    NSString *partner = kWXPay_Partner;
    
    NSString *notifyUrl = self.notifyUrl.length? self.notifyUrl : kWXPay_NotifyUrl;
    
    //订单金额,单位（分）
    NSString *order_price   = [NSString stringWithFormat:@"%.0f",([order.biz_content.total_amount floatValue]) * 100];//金额作处理 分为单位

    srand( (unsigned)time(0) );
    NSString *noncestr  = [NSString stringWithFormat:@"%d", rand()];
    
    NSDateFormatter* formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyyMMddHHmmss"];
    NSString *time_stamp = [formatter stringFromDate:[NSDate date]];
    
    //////// 发起预支付请求 -> 获取预支付订单ID
    // 统一下单参数：appid、mch_id、nonce_str、body、out_trade_no、total_fee、spbill_create_ip、trade_type、notify_url、sign
    NSMutableDictionary *packageParams = [NSMutableDictionary dictionary];
    
    [packageParams setObject:appId forKey:@"appid"];       //开放平台appid
    [packageParams setObject:partner forKey:@"mch_id"];      //商户号
    [packageParams setObject:noncestr forKey:@"nonce_str"];   //随机串
    [packageParams setObject:order.biz_content.body forKey:@"body"];        //订单描述，展示给用户
    [packageParams setObject:order.biz_content.out_trade_no forKey:@"out_trade_no"];//商户订单号
    [packageParams setObject:order_price forKey:@"total_fee"];       //订单金额，单位为分
    [packageParams setObject:@"192.168.0.153" forKey:@"spbill_create_ip"];//发器支付的机器ip
    [packageParams setObject:notifyUrl forKey:@"notify_url"];  //支付结果异步通知
    [packageParams setObject:@"APP" forKey:@"trade_type"];  //支付类型，固定为APP
    
    // 获得预支付订单ID
    NSString *prePayid = [self sendPrepay:packageParams];
    if ( prePayid != nil) {
        //获取到prepayid后进行第二次签名
        
        NSString    *package, *nonce_str;
        //设置支付参数
        nonce_str	= [PDWXPayUtil md5:time_stamp];
        //重新按提交格式组包，微信客户端暂只支持package=Sign=WXPay格式，须考虑升级后支持携带package具体参数的情况
        package         = @"Sign=WXPay";
        //第二次签名参数列表
        NSMutableDictionary *signParams = [NSMutableDictionary dictionary];
        [signParams setObject: kWXPay_AppID forKey:@"appid"];
        [signParams setObject: nonce_str    forKey:@"noncestr"];
        [signParams setObject: package      forKey:@"package"];
        [signParams setObject: kWXPay_Partner forKey:@"partnerid"];
        [signParams setObject: [NSString stringWithFormat:@"%ld", (long)[NSDate date].timeIntervalSince1970]   forKey:@"timestamp"];
        [signParams setObject: prePayid     forKey:@"prepayid"];
        //生成签名
        NSString *sign  = [self createMd5Sign:signParams];
        
        //添加签名
        [signParams setObject:sign forKey:@"sign"];
        
        [debugInfo appendFormat:@"第二步签名成功，sign＝%@\n",sign];
        
        //返回参数列表
        if(signParams != nil){
            NSMutableString *retcode = [signParams objectForKey:@"retcode"];
            if (retcode.intValue == 0){
                NSMutableString *stamp  = [signParams objectForKey:@"timestamp"];
                
                //调起微信支付
                PayReq* req             = [[PayReq alloc] init];
                req.openID              = [signParams objectForKey:@"appid"];
                req.partnerId           = [signParams objectForKey:@"partnerid"];
                req.prepayId            = [signParams objectForKey:@"prepayid"];
                req.nonceStr            = [signParams objectForKey:@"noncestr"];
                req.timeStamp           = stamp.intValue;
                req.package             = [signParams objectForKey:@"package"];
                req.sign                = [signParams objectForKey:@"sign"];
                [WXApi sendReq:req];
                //日志输出
            }else{
                [self alert:@"提示信息" msg:[signParams objectForKey:@"retmsg"]];
            }
        }else{
            [self alert:@"提示信息" msg:@"服务器返回错误，未获取到json对象"];
        }
        
    }else{
        [debugInfo appendFormat:@"获取prepayid失败！\n"];
    }
}

//提交预支付
- (NSString *)sendPrepay:(NSMutableDictionary *)prePayParams {
    
    // 预支付Url
    NSString *rePayUrl = @"https://api.mch.weixin.qq.com/pay/unifiedorder";
    NSString *prepayid = nil;
    
    //获取提交支付
    NSString *send = [self genPackage:prePayParams];
    
    //输出Debug Info
    [debugInfo appendFormat:@"API链接:%@\n", rePayUrl];
    [debugInfo appendFormat:@"发送的xml:%@\n", send];
    
    //发送请求post xml数据
    NSData *res = [PDWXPayUtil httpSend:rePayUrl method:@"POST" data:send];
    
    //输出Debug Info
    [debugInfo appendFormat:@"服务器返回：\n%@\n\n",[[NSString alloc] initWithData:res encoding:NSUTF8StringEncoding]];
    
    PDWXXmlUtil *xml  = [[PDWXXmlUtil alloc] init];
    
    //开始解析
    [xml startParse:res];
    
    NSMutableDictionary *resParams = [xml getDict];
    
    //判断返回
    NSString *return_code   = [resParams objectForKey:@"return_code"];
    NSString *result_code   = [resParams objectForKey:@"result_code"];
    if ( [return_code isEqualToString:@"SUCCESS"] )
    {
        //生成返回数据的签名
        NSString *sign      = [self createMd5Sign:resParams ];
        NSString *send_sign =[resParams objectForKey:@"sign"] ;
        
        //验证签名正确性
        if( [sign isEqualToString:send_sign]){
            if( [result_code isEqualToString:@"SUCCESS"]) {
                //验证业务处理状态
                prepayid    = [resParams objectForKey:@"prepay_id"];
                return_code = 0;
                
                [debugInfo appendFormat:@"获取预支付交易标示成功！\n"];
            }
        }else{
            [debugInfo appendFormat:@"gen_sign=%@\n   _sign=%@\n",sign,send_sign];
            [debugInfo appendFormat:@"服务器返回签名验证错误！！！\n"];
        }
    }else{
        [debugInfo appendFormat:@"接口返回错误！！！\n"];
    }
    DLog(@"%@",debugInfo);
    
    return prepayid;
}

//获取package带参数的签名包
- (NSString *)genPackage:(NSMutableDictionary*)packageParams {
    NSString *sign;
    NSMutableString *reqPars=[NSMutableString string];
    //生成签名
    sign        = [self createMd5Sign:packageParams];
    //生成xml的package
    NSArray *keys = [packageParams allKeys];
    [reqPars appendString:@"<xml>\n"];
    for (NSString *categoryId in keys) {
        [reqPars appendFormat:@"<%@>%@</%@>\n", categoryId, [packageParams objectForKey:categoryId],categoryId];
    }
    [reqPars appendFormat:@"<sign>%@</sign>\n</xml>", sign];
    return [NSString stringWithString:reqPars];
}

//创建package签名
- (NSString*) createMd5Sign:(NSMutableDictionary*)dict {
    NSMutableString *contentString  =[NSMutableString string];
    NSArray *keys = [dict allKeys];
    //按字母顺序排序
    NSArray *sortedArray = [keys sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        return [obj1 compare:obj2 options:NSNumericSearch];
    }];
    //拼接字符串
    for (NSString *categoryId in sortedArray) {
        if (   ![[dict objectForKey:categoryId] isEqualToString:@""]
            && ![categoryId isEqualToString:@"sign"]
            && ![categoryId isEqualToString:@"key"]
            )
        {
            [contentString appendFormat:@"%@=%@&", categoryId, [dict objectForKey:categoryId]];
        }
        
    }
    //添加key字段
    [contentString appendFormat:@"key=%@", kWXPay_PartnerKey];
    //得到MD5 sign签名
    NSString *md5Sign =[PDWXPayUtil md5:contentString];
    
    //输出Debug Info
    [debugInfo appendFormat:@"MD5签名字符串：\n%@\n\n",contentString];
    
    return md5Sign;
}

//客户端提示信息
- (void)alert:(NSString *)title msg:(NSString *)msg {
    [[[UIAlertView alloc] initWithTitle:title message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

@end
