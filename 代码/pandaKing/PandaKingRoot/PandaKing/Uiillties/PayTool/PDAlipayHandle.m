//
//  PDAlipayHandle.m
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAlipayHandle.h"
#import "DataSigner.h"
#import <AlipaySDK/AlipaySDK.h>

#define kAlipay_PrivateKey @"MIICeAIBADANBgkqhkiG9w0BAQEFAASCAmIwggJeAgEAAoGBAMTVd7H5cnbC/4N9hQlBNhbsksT8JbLtO/xemTk/lSEP1gqNnX4k89eHzu8zto/WrcQlBHONVluPh/OxxBWIJIFTYPqUekLEZz+AA4qcbLN8hDTSPlbe5DP3NFzJljUhMIP/Nc50ffXSaFu6/RMFqJBIA6dkqkbAlEWG/bl41LXfAgMBAAECgYB1uWr+gkAosdYasc8Iyvzr1xCtSlXN3z/aYEXqTJIIFS2iYDLLCJTi2rI0tMxC2VZSkwVHi0gUORNJ+I9bhXK27LluiELxdFfMbFikDJdlLMiMRGFHoIkaIRUHyJV4Peg10d5xktOkfEvmb5/bafp7TtT57CeL6Q2bk8bhuN10SQJBAPm2qk0aN93i8Rhinf0Wtvx8HhksoSTo3bOFnKNvpjwxPKvf/N9R6RT6wDk/8HCtwv9typz8aEQYcghL8vQkNHUCQQDJygHk98Fxc5aQ7KZkT7LJ8IEsis2cqlceZujDEm4w1wx6zQ+TAkSo5E49hPlLGAowzcOJUSqC0nAJX4Gd4qaDAkEArh8jDPRVNFFEkB5jz9CA8/mP+znVe6ksvjtSh9wYbCxhA/ABoa65+jkGxGTDQa7II9foyiJuid0J1qMu2/JK6QJBAJDYi2GTIm1QnlyrMolA2EKye9bAT/VMJLry/dPA8A3o39FqTuqkrypYr3zjbZskx3Pez6RK+evsKHXh84WkwwcCQQCTKJ8bN2lK9fEkWZ9WAskwK7ULOIOCAfNYTcYBVSI7Txv8AvpUt1GMo+xE48xps18jS2u/wuad0N0qww6z3/iC"
#define kAlipay_NotifyUrl  @""
#define kAlipay_Partner    @"2088122519632733"
#define kAlipay_Seller     @"pandaol@aliyun.com"

@interface PDAlipayHandle ()

@end

@implementation PDAlipayHandle

+ (instancetype)payHandle {
    return [[self alloc] init];
}

- (void)payWithOrder:(Order *)order payResultComplication:(void (^)(NSDictionary *))complication {
    
    NSString *privateKey = self.privateKey.length? self.privateKey : kAlipay_PrivateKey;
    
    order.app_id = kAlipay_AppID;
    
    order.method = @"alipay.trade.app.pay";
    
    order.charset = @"utf-8";
    
    NSDateFormatter* formatter = [NSDateFormatter new];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    order.timestamp = [formatter stringFromDate:[NSDate date]];
    
    order.version = @"1.0";
    
    order.sign_type = @"RSA";
    
    order.notify_url = self.notifyUrl.length? self.notifyUrl : kAlipay_NotifyUrl;
    
    order.biz_content.timeout_express = @"30m";
    
    order.biz_content.subject = order.biz_content.body;
    
    order.biz_content.product_code = @"QUICK_MSECURITY_PAY";
    
    order.biz_content.seller_id = kAlipay_Partner;
    
    //将商品信息拼接成字符串
    NSString *orderInfo = [order orderInfoEncoded:NO];
    NSString *orderInfoEncoded = [order orderInfoEncoded:YES];
    
    // NOTE: 获取私钥并将商户信息签名，外部商户的加签过程请务必放在服务端，防止公私钥数据泄露；
    //       需要遵循RSA签名规范，并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(privateKey);
    NSString *signedString = [signer signString:orderInfo];
    
    if (signedString != nil) {
        // 这个换成自己的scheme
        NSString *appScheme = @"pandaking-alipay";
        
        NSString *orderString = [NSString stringWithFormat:@"%@&sign=%@",
                                 orderInfoEncoded, signedString];
        
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            if (complication) {
                complication(resultDic);
            }
        }];
    }
}

@end

@interface PDAlipayHandle1 ()

@end

@implementation PDAlipayHandle1

+ (instancetype)payHandle {
    return [[self alloc] init];
}

- (void)payWithOrder:(PDPayOrderModel *)order payResultComplication:(void (^)(NSInteger,NSString *))complication {
    NSString *privateKey = self.privateKey.length? self.privateKey : kAlipay_PrivateKey;
    order.notifyURL = self.notifyUrl.length? self.notifyUrl : kAlipay_NotifyUrl;
    order.partner = kAlipay_Partner;
    order.seller = kAlipay_Seller;
    order.service = @"mobile.securitypay.pay";
    order.paymentType = @"1";
    order.inputCharset = @"utf-8";
    order.itBPay = @"30m";
    order.showUrl = @"m.alipay.com";
    
    NSString *appScheme = @"pandaol-alipay";
    
    //将商品信息拼接成字符串
    NSString *orderSpec = [order description];
    
    //获取私钥并将商户信息签名,外部商户可以根据情况存放私钥和签名,只需要遵循RSA签名规范,并将签名字符串base64编码和UrlEncode
    id<DataSigner> signer = CreateRSADataSigner(privateKey);
    NSString *signedString = [signer signString:orderSpec];
    
    //将签名成功字符串格式化为订单字符串,请严格按照该格式
    NSString *orderString = nil;
    if (signedString != nil) {
        orderString = [NSString stringWithFormat:@"%@&sign=\"%@\"&sign_type=\"%@\"", orderSpec, signedString, @"RSA"];
        
        /*
         9000 订单支付成功
         8000 正在处理中
         4000 订单支付失败
         6001 用户中途取消
         6002 网络连接出错
         */
        [[AlipaySDK defaultService] payOrder:orderString fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            
            if (complication){
                NSNumber *errorCode = [resultDic objectForKey:@"resultStatus"];
                NSString *errMsg = @"";
                if (errorCode.intValue == 9000){
                    errMsg = @"订单支付成功";
                } else if (errorCode.intValue == 8000){
                    errMsg = @"正在处理中";
                } else if (errorCode.intValue == 4000){
                    errMsg = @"订单处理失败";
                } else if (errorCode.intValue == 6001){
                    errMsg = @"用户中途取消";
                } else if (errorCode.intValue == 6002){
                    errMsg = @"网络连接出错";
                }
                
                complication(errorCode.intValue, errMsg);
            }
        }];
    }
}

@end