//
//  PDAlipayHandle.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDAlipayOrderInfo.h"
#import "PDPayOrderModel.h"

#define kAlipay_AppID @"2016041201290145"
/// 这个是新版的
@interface PDAlipayHandle : NSObject
@property (nonatomic, copy) NSString *privateKey;
@property (nonatomic, copy) NSString *notifyUrl;

+ (instancetype)payHandle;
- (void)payWithOrder:(Order *)order payResultComplication:(void(^)(NSDictionary *result))complication;
@end

/// 这个是旧版1.0的方法
@interface PDAlipayHandle1 : NSObject
@property (nonatomic, copy) NSString *privateKey;
@property (nonatomic, copy) NSString *notifyUrl;

+ (instancetype)payHandle;
- (void)payWithOrder:(PDPayOrderModel *)order payResultComplication:(void(^)(NSInteger code,NSString *errMsg))complication;
@end