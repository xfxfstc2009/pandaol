//
//  PDImageView.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <UIImageView+WebCache.h>

@interface PDImageView : UIImageView

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;

// 更新头像
-(void)uploadImageWithAvatarURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;

// 走阿里云
-(void)uploadAliYunHomeCardImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;

#pragma mark - 英雄猜
-(void)uploadHeroGuessImgWithUrl:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
#pragma mark - LOL英雄杀
- (void)uploadScuffleImageUrl:(NSString *)url callback:(void(^)(UIImage *image))callbackBlock;
#pragma mark - 英雄猜上根据游戏类型选择图片路径
- (void)uploadHeroLotteryWithGameType:(GameType)type
                            imageCode:(NSString *)code
                             callback:(void(^)(UIImage *image))callbackBlock;
#pragma mark - 娱乐底部图片路径
- (void)uploadAmusementPartWithImageCode:(NSString *)code
                                callback:(void(^)(UIImage *image))callbackBlock;
#pragma mark - slider
- (void)uploadSliderWithImageCode:(NSString *)code callback:(void (^)(UIImage *))callbackBlock ;

#pragma mark - 清除缓存
+(void)cleanImgCacheWithBlock:(void(^)())block;
+(NSString *)diskCount;
+(CGFloat)diskCountFloat;

-(void)uploadImageWithVCodeURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
@end
