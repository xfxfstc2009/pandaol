//
//  PDImageView.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDImageView.h"

@implementation PDImageView

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:urlString];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"icon_nordata"];
    }
    if ([urlString hasPrefix:@"/po"]){
        if (JAVA_Port.length){
            imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@%@",JAVA_Host,JAVA_Port,urlString]];
        } else {
            imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@%@",JAVA_Host,urlString]];
        }
    } else {
        imgURL = [NSURL URLWithString:urlString];
    }
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.image = image;
        } else {
            self.image = [UIImage imageNamed:@"icon_nordata"];
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}


// 更新图片
-(void)uploadImageWithAvatarURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if ((![urlString isKindOfClass:[NSString class]]) ||(!urlString.length)){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:urlString];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"icon_nordata"];
    }
    if ([urlString hasPrefix:@"/po"]){
        if (JAVA_Port.length){
            imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@%@",JAVA_Host,JAVA_Port,urlString]];
        } else {
           imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@%@",JAVA_Host,urlString]];
        }
    } else if ([urlString hasPrefix:@"po"]){
        if (JAVA_Port.length){
            imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@/%@",JAVA_Host,JAVA_Port,urlString]];
        } else {
            imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",JAVA_Host,urlString]];
        }
    } else {
        imgURL = [NSURL URLWithString:urlString];
    }
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.image = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
        } else {
            self.image = [Tool imageByComposingImage:[UIImage imageNamed:@"icon_nordata"] withMaskImage:[UIImage imageNamed:@"round"]];
        }

        if (callbackBlock){
            callbackBlock(self.image);
        }
    }];
}


#pragma mark - 清除缓存
+(void)cleanImgCacheWithBlock:(void(^)())block{
    [[SDWebImageManager sharedManager].imageCache clearMemory];
    __weak typeof(self)weakSelf = self;
    [[SDWebImageManager sharedManager].imageCache clearDiskOnCompletion:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}

+(NSString *)diskCount{
    float tmpSize = [[SDImageCache sharedImageCache] getSize];
    CGFloat tempSizeWithM = tmpSize / 1024. / 1024.;
    
    NSString *clearCacheName = tempSizeWithM >= 1 ? [NSString stringWithFormat:@"%.2fM",tempSizeWithM] : [NSString stringWithFormat:@"%.2fK",tempSizeWithM * 1024];
    return clearCacheName;
}

+(CGFloat)diskCountFloat{
    float tmpSize = [[SDImageCache sharedImageCache] getSize];
    CGFloat tempSizeWithM = tmpSize / 1024. / 1024.;

    return tempSizeWithM;
}


// 首页的卡片
-(void)uploadAliYunHomeCardImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"icon_nordata"];
    }
    
    // 1. 获取最后文件名
    NSArray *imgs = [urlString componentsSeparatedByString:@"/"];
    NSString *imgsName = [imgs lastObject];
    
     NSURL *mainImgURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@@1e_1c_0o_0l_%lih_%liw_90q.png",aliyunOSS_BaseURL,imgsName,(long)self.size_height,(long)self.size_width]];
    
    [self sd_setImageWithURL:mainImgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.image = image;
        } else {
            self.image = [UIImage imageNamed:@"icon_nordata"];
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

#pragma mark - 英雄猜
-(void)uploadHeroGuessImgWithUrl:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if (![urlString isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"icon_nordata"];
    }
    
    NSURL *imgURL;
    if (JAVA_Host.length){
        imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@/po/dynimg/png?name=cg.heros.%@&width=%li&height=%li",JAVA_Host,JAVA_Port,urlString,(long)self.size_width,(long)self.size_height]];
    } else {
        imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@%@",JAVA_Host,urlString]];
    }
    
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.image = image;
        } else {
            self.image = [UIImage imageNamed:@"icon_nordata"];
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

- (void)uploadScuffleImageUrl:(NSString *)url callback:(void (^)(UIImage *))callbackBlock {
    if (![url isKindOfClass:[NSString class]]){
        return;
    }
    // 加载图片
    UIImage *placeholder = [UIImage imageNamed:@"icon_nordata"];
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@/po/dynimg/png?name=cag.skins.%@&width=%li&height=%li",JAVA_Host,JAVA_Port,url,(long)(self.size_width * 2),(long)(self.size_height * 2)]];
    
    [self sd_setImageWithURL:imageURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.image = image;
        } else {
            self.image = [UIImage imageNamed:@"icon_nordata"];
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

- (void)uploadHeroLotteryWithGameType:(GameType)type imageCode:(NSString *)code callback:(void (^)(UIImage *))callbackBlock {
    NSString *imageUrl = @"";
    if (type == GameTypeLoL) {
        imageUrl = [NSString stringWithFormat:@"http://%@:%@/po/dynimg/png?name=cg.heros.%@&width=%li&height=%li",JAVA_Host,JAVA_Port,code,(long)(self.size_width * 2),(long)(self.size_height * 2)];
    } else if (type == GameTypeDota2) {
        imageUrl = [NSString stringWithFormat:@"http://%@:%@/po/dynimg/png?name=dota2cg.heros.%@&width=%li&height=%li" ,JAVA_Host,JAVA_Port,code,(long)(self.size_width * 2),(long)(self.size_height * 2)];
    } else if (type == GameTypePVP) {
        imageUrl = [NSString stringWithFormat:@"http://%@:%@/po/dynimg/png?name=kingcg.heros.%@&width=%li&height=%li",JAVA_Host,JAVA_Port,code,(long)(self.size_width * 2),(long)(self.size_height * 2)];
    }
    
    NSURL *imageURL = [NSURL URLWithString:imageUrl];
    // 加载图片
    UIImage *placeholder = [UIImage imageNamed:@"icon_nordata"];
    [self sd_setImageWithURL:imageURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.image = image;
        } else {
            self.image = [UIImage imageNamed:@"icon_nordata"];
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

- (void)uploadSliderWithImageCode:(NSString *)code callback:(void (^)(UIImage *))callbackBlock {
    NSString *imageUrl = @"";
    imageUrl = [NSString stringWithFormat:@"http://%@:%@/po/dynimg/png?name=si.icon.%@&width=0&height=0",JAVA_Host,JAVA_Port,code];
   
    
    NSURL *imageURL = [NSURL URLWithString:imageUrl];
    // 加载图片
    UIImage *placeholder = [UIImage imageNamed:@"icon_nordata"];
    [self sd_setImageWithURL:imageURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.image = image;
        } else {
            self.image = [UIImage imageNamed:@"icon_nordata"];
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}


- (void)uploadAmusementPartWithImageCode:(NSString *)code callback:(void (^)(UIImage *))callbackBlock {
    // 加载图片
    UIImage *placeholder = [UIImage imageNamed:@"icon_nordata"];
    NSURL *imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@/po/dynimg/png?name=ep.covers.%@&width=%li&height=%li",JAVA_Host,JAVA_Port,code,(long)(self.size_width * 2),(long)(self.size_height * 2)]];
    
    [self sd_setImageWithURL:imageURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (image){
            self.image = image;
        } else {
            self.image = [UIImage imageNamed:@"icon_nordata"];
        }
        
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}




// 更新图片
-(void)uploadImageWithVCodeURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock{
    if ((![urlString isKindOfClass:[NSString class]]) ||(!urlString.length)){
        return;
    }
    // 加载图片
    NSURL *imgURL = [NSURL URLWithString:urlString];
    if (placeholder == nil){
        placeholder = [UIImage imageNamed:@"icon_nordata"];
    }
    if ([urlString hasPrefix:@"/po"]){
        if (JAVA_Port.length){
            imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@%@",JAVA_Host,JAVA_Port,urlString]];
        } else {
            imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@%@",JAVA_Host,urlString]];
        }
    } else if ([urlString hasPrefix:@"po"]){
        if (JAVA_Port.length){
            imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@/%@",JAVA_Host,JAVA_Port,urlString]];
        } else {
            imgURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@/%@",JAVA_Host,urlString]];
        }
    } else {
        imgURL = [NSURL URLWithString:urlString];
    }
    [self sd_setImageWithURL:imgURL placeholderImage:placeholder completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        self.image = image;
        if (callbackBlock){
            callbackBlock(image);
        }
    }];
}

@end
