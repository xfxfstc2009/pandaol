//
//  PDOAuthLoginAndShareTool.h
//  PandaChallenge
//
//  Created by Cranz on 16/7/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "WXApi.h"
#import "WeiboSDK.h"
#import <Foundation/Foundation.h>
#import <TencentOpenAPI/TencentOAuth.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import <TencentOpenAPI/TencentApiInterface.h>
#import <TencentOpenAPI/TencentMessageObject.h>
#import "PDThirdLoginModel.h"                               /**< 三方登录的model */

#define WeChatAppID         @"wxafbc4f7bdaf0156d"
#define WeChatAppSecret     @"a2cb92a0e160c5470b62b9d2d303cc7b"

#define QQAppID             @"1105265453"
#define QQAppAppSecret      @"KEYazhxfBFfyftcsPW8"

#define WeiBoAPPID          @"1938399542"
#define WeiBoAppSecret      @"b888b2779298f00b98aa6276f8cb3bdc"
#define WeiBoRedirectUri    @"http://www.pandaol.com"


typedef NS_ENUM(NSUInteger, PDOAuthLoginType) {
    PDOAuthLoginTypeWX,     /**< 微信登录*/
    PDOAuthLoginTypeQQ,     /**< qq登录*/
    PDOAuthLoginTypeWB,     /**< 微博登录*/
};

typedef NS_ENUM(NSUInteger,PD3RShareType) {
    PD3RShareTypeWXSession,   /**< 微信聊天界面*/
    PD3RShareTypeWXTimeLine,  /**< 微信朋友圈*/
    PD3RShareTypeQQFriend,    /**< QQ好友*/
    PD3RShareTypeQQGroup,     /**< QQ群*/
    PD3RShareTypeQQZone,      /**< QQ空间*/
    PD3RShareTypeWB,          /**< 微博*/
};

@interface PDOAuthLoginAndShareTool : NSObject
/** 注册，在AppDelegate中调用即可*/
+ (void)regist;
/** 微信登录回调，在AppDelegate的微信回调中使用*/
+ (void)wxResp:(BaseResp *)resp;
/** 微博回调，在AppDelegate的微博回调中使用*/
+ (void)wbResp:(WBBaseResponse*)resp;

#pragma mark - 登录分享模块
/** 
 第三方授权登录
 在选择QQ登录的时候，必须将返回的PDOAuthLoginAndShareTool也就是实例对象引用起来，否则接受不到回调
 userinfo 中通过open_id获得openId
 */
+ (PDOAuthLoginAndShareTool *)oAuthLoginWithType:(PDOAuthLoginType)type respBlock:(void(^)(NSDictionary *userinfo))respBlock;
/** 不能同时发送文本和多媒体，当为多媒体时（网页也属于多媒体）text失效*/
+ (void)sharedWithType:(PD3RShareType)type text:(NSString *)text title:(NSString *)title desc:(NSString *)desc thubImageData:(NSData *)thubImageData webUrl:(NSString *)webUrl;
@end
