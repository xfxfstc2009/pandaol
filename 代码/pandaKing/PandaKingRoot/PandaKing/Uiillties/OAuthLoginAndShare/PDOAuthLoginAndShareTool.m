//
//  PDOAuthLoginAndShareTool.m
//  PandaChallenge
//
//  Created by Cranz on 16/7/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDOAuthLoginAndShareTool.h"
#import <objc/runtime.h>
#import "WeiboUser.h"


// url
static NSString *const wxBaseUrl = @"https://api.weixin.qq.com/sns";

// 运行绑定
static const char *wxRespKey = "wxRespKey";
static const char *qqRespKey = "qqRespKey";
static const char *wbRespKey = "wbRespKey";

// 类型常量

//*qq
static NSString *const qqAccessToken = @"qqAccesstoken";
static NSString *const qqOpenid = @"qqOpenId";
//weibo


typedef void(^respBlock)(NSDictionary *dic);

@interface PDOAuthLoginAndShareTool ()<TencentSessionDelegate>
@property (nonatomic, strong) SendAuthReq *sendAuthReq;
@property (nonatomic, strong) TencentOAuth *tencentOAuth;

@end

@implementation PDOAuthLoginAndShareTool

#pragma mark - http

+ (NSData *)httpSend:(NSString *)url method:(NSString *)method data:(NSString *)data {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5];
    //设置提交方式
    [request setHTTPMethod:method];
    //设置数据类型
    [request addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
    //设置编码
    [request setValue:@"UTF-8" forHTTPHeaderField:@"charset"];
    //如果是POST
    [request setHTTPBody:[data dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error;
    //将请求的url数据放到NSData对象中
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    return response;
}

+ (void)httpSend:(NSString *)url method:(NSString *)method params:(NSString *)params complication:(void(^)(NSData *response))complication {
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:5];
    //设置提交方式
    [request setHTTPMethod:method];
    //设置数据类型
    [request addValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
    //设置编码
    [request setValue:@"UTF-8" forHTTPHeaderField:@"charset"];
    //如果是POST
    [request setHTTPBody:[params dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSError *error;
    //将请求的url数据放到NSData对象中
    NSData *response = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:&error];
    if (complication) {
        complication(response);
    }
}

#pragma mark - 微信登录

+ (void)wxLogin {
    NSMutableString *dataStrForUser = [NSMutableString string];
    [dataStrForUser appendFormat:@"access_token=%@&openid=%@",[AccountModel sharedAccountModel].wx_accessToken,[AccountModel sharedAccountModel].wx_openId];
    
    __weak typeof(self) weakSelf = self;
    [PDOAuthLoginAndShareTool httpSend:[NSString stringWithFormat:@"%@/userinfo",wxBaseUrl] method:@"POST" params:dataStrForUser complication:^(NSData *response) {
        if (!weakSelf) {
            return ;
        }
        if (response) {
            NSDictionary *userInfo = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
            NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:userInfo];
            [mutableDic setObject:[AccountModel sharedAccountModel].wx_openId forKey:@"open_id"];
            respBlock respBlock = objc_getAssociatedObject([weakSelf class], wxRespKey);
            if (respBlock) {
                respBlock([mutableDic copy]);
            }
        }
    }];
}



#pragma mark - 在AppDelegate中传回的回调处理
#pragma mark - 微信
+ (void)wxResp:(BaseResp *)resp {
    if ([resp isKindOfClass:[SendAuthResp class]]) {
        SendAuthResp *resp_ = (SendAuthResp *)resp;
        if (resp_.errCode == 0) {
//            NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//            NSBlockOperation *getTokenOperation = [NSBlockOperation blockOperationWithBlock:^{
//                NSMutableString *dataStr = [NSMutableString string];
//                [dataStr appendFormat:@"appid=%@&secret=%@&code=%@&grant_type=authorization_code",WeChatAppID,WeChatAppSecret,resp_.code];
//                NSData *data = [PDOAuthLoginAndShareTool httpSend:[NSString stringWithFormat:@"%@/oauth2/access_token",wxBaseUrl] method:@"POST" data:dataStr];
//                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
//                [AccountModel sharedAccountModel].wx_accessToken = dic[@"access_token"];
//                [AccountModel sharedAccountModel].wx_refreshToken = dic[@"refresh_token"];
//                [AccountModel sharedAccountModel].wx_openId = dic[@"openid"];
//            }];
//            
//            NSBlockOperation *getUserInfoOperation = [NSBlockOperation blockOperationWithBlock:^{
//                [self wxLogin];
//            }];
//            [getUserInfoOperation addDependency:getTokenOperation];
//            [queue addOperations:@[getTokenOperation,getUserInfoOperation] waitUntilFinished:YES];
            
            
            
            NSMutableString *dataStr = [NSMutableString string];
            [dataStr appendFormat:@"appid=%@&secret=%@&code=%@&grant_type=authorization_code",WeChatAppID,WeChatAppSecret,resp_.code];
            __weak typeof(self) weakSelf = self;
            [PDOAuthLoginAndShareTool httpSend:[NSString stringWithFormat:@"%@/oauth2/access_token",wxBaseUrl] method:@"POST" params:dataStr complication:^(NSData *response) {
                if (!weakSelf) {
                    return ;
                }
                __strong typeof(weakSelf) strongSelf = weakSelf;
                if (response) {
                    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
                    [AccountModel sharedAccountModel].wx_accessToken = dic[@"access_token"];
                    [AccountModel sharedAccountModel].wx_refreshToken = dic[@"refresh_token"];
                    [AccountModel sharedAccountModel].wx_openId = dic[@"openid"];
                    [strongSelf wxLogin];
                }
            }];
        }
    }
}

#pragma mark - 微博回调

+ (void)wbResp:(WBBaseResponse *)resp {
    if ([resp isKindOfClass:[WBAuthorizeResponse class]]) {
        WBAuthorizeResponse *resp_ = (WBAuthorizeResponse *)resp;
        [WBHttpRequest requestForUserProfile:resp_.userID withAccessToken:resp_.accessToken andOtherProperties:nil queue:nil withCompletionHandler:^(WBHttpRequest *httpRequest, id result, NSError *error) {
            if (!error) {
                WeiboUser *user = (WeiboUser *)result;
                NSMutableDictionary *userInfo = [NSMutableDictionary dictionary];

                unsigned int propertyCount;
                objc_property_t *propertys = class_copyPropertyList(user.class, &propertyCount);
                for (unsigned int i = 0; i < propertyCount; i++) {
                    objc_property_t property = propertys[i];
                    NSString *property_name = [NSString stringWithUTF8String:property_getName(property)];
                    id obj = [user valueForKey:property_name];
                    if (!obj || ![obj isKindOfClass:[NSString class]]) {
                        continue;
                    }
                    [userInfo setObject:obj forKey:property_name];
                }
                
                [userInfo setObject:resp_.userID forKey:@"open_id"];
                
                respBlock respBlock = objc_getAssociatedObject([self class], wbRespKey);
                if (respBlock) {
                    respBlock([userInfo copy]);
                }
            }
        }];
    }
}

#pragma mark - 注册

+ (void)regist {
    [WXApi registerApp:WeChatAppID withDescription:@"PandaOL"];
    [WeiboSDK enableDebugMode:NO];
    [WeiboSDK registerApp:WeiBoAPPID];
}

#pragma mark - 授权三方登录

- (instancetype)initWithOAuthLoginWithType:(PDOAuthLoginType)type respBlock:(void (^)(NSDictionary *))respBlock {
    if (self = [super init]) {
        if (type == PDOAuthLoginTypeWX) {
            if ([WXApi isWXAppInstalled]) {
//                NSString *accessToken = [AccountModel sharedAccountModel].wx_accessToken;
//                NSString *refreshToken = [AccountModel sharedAccountModel].wx_refreshToken;
//                NSString *openId = [AccountModel sharedAccountModel].wx_openId;
//                
//                if (accessToken.length && refreshToken.length && openId.length) {
//                    // 刷新refreshToken
//                    NSMutableString *dataStrForUser = [NSMutableString string];
//                    
//                    [dataStrForUser appendFormat:@"appid=%@&grant_type=refresh_token&refresh_token=%@",WeChatAppID,refreshToken];
//                    
//                    __weak typeof(self) weakSelf = self;
//                    [PDOAuthLoginAndShareTool httpSend:[NSString stringWithFormat:@"%@/oauth2/refresh_token",wxBaseUrl] method:@"POST" params:dataStrForUser complication:^(NSData *response) {
//                        if (!weakSelf) {
//                            return ;
//                        }
//                        
//                        __strong typeof(weakSelf) strongSelf = weakSelf;
//                        if (response) {
//                            NSDictionary *userInfo = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
//                            if ([userInfo objectForKey:@"refresh_token"]) {
//                                [AccountModel sharedAccountModel].wx_accessToken = [userInfo objectForKey:@"access_token"];
//                                [AccountModel sharedAccountModel].wx_refreshToken = [userInfo objectForKey:@"refresh_token"];
//                                [AccountModel sharedAccountModel].wx_openId = [userInfo objectForKey:@"openid"];
//                                objc_setAssociatedObject(strongSelf.class, wxRespKey, respBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
//                                
//                                // 判断token是否失效
//                                NSMutableString *dataStrForUser = [NSMutableString string];
//                                [dataStrForUser appendFormat:@"access_token=%@&openid=%@",accessToken,openId];
//                                NSData *dataForUser = [PDOAuthLoginAndShareTool httpSend:[NSString stringWithFormat:@"%@/auth",wxBaseUrl] method:@"POST" data:dataStrForUser];
//                                NSDictionary *userInfo = [NSJSONSerialization JSONObjectWithData:dataForUser options:NSJSONReadingAllowFragments error:nil];
//                                int errcode = [[userInfo objectForKey:@"errcode"] intValue];
//                                if (errcode == 0) {
//                                    [PDOAuthLoginAndShareTool wxLogin];
//                                } else {
//                                    strongSelf.sendAuthReq = [[SendAuthReq alloc] init];
//                                    strongSelf.sendAuthReq.scope = @"snsapi_userinfo";
//                                    strongSelf.sendAuthReq.state = @"panda_ios";
//                                    [WXApi sendReq:strongSelf.sendAuthReq];
//                                    if (respBlock) {
//                                        objc_setAssociatedObject(strongSelf.class, wxRespKey, respBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
//                                    }
//                                }
//                            }
//                        }
//                    }];
//                } else {
//                    self.sendAuthReq = [[SendAuthReq alloc] init];
//                    self.sendAuthReq.scope = @"snsapi_userinfo";
//                    self.sendAuthReq.state = @"panda_ios";
//                    [WXApi sendReq:self.sendAuthReq];
//                    if (respBlock) {
//                        objc_setAssociatedObject(self.class, wxRespKey, respBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
//                    }
//                }
                
                self.sendAuthReq = [[SendAuthReq alloc] init];
                self.sendAuthReq.scope = @"snsapi_userinfo";
                self.sendAuthReq.state = @"panda_ios";
                [WXApi sendReq:self.sendAuthReq];
                if (respBlock) {
                    objc_setAssociatedObject(self.class, wxRespKey, respBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
                }
            }
        } else if (type == PDOAuthLoginTypeQQ) {
            if ([TencentOAuth iphoneQQInstalled]) {
                self.tencentOAuth = [[TencentOAuth alloc] initWithAppId:QQAppID andDelegate:self];
                NSArray *permissions= [NSArray arrayWithObjects:@"get_user_info", @"get_simple_userinfo", @"add_t",nil];
                [self.tencentOAuth authorize:permissions inSafari:NO];
                if (respBlock) {
                    objc_setAssociatedObject(self.class, qqRespKey, respBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
                }
            }
        } else {
            if ([WeiboSDK isWeiboAppInstalled]) {
                WBAuthorizeRequest *request = [WBAuthorizeRequest request];
                request.redirectURI = WeiBoRedirectUri;
                request.scope = @"all";
                [WeiboSDK sendRequest:request];
                if (respBlock) {
                    objc_setAssociatedObject(self.class, wbRespKey, respBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
                }
            }
        }
    }
    return self;
}


+ (PDOAuthLoginAndShareTool *)oAuthLoginWithType:(PDOAuthLoginType)type respBlock:(void (^)(NSDictionary *))respBlock {
    PDOAuthLoginAndShareTool *tool = [[self alloc] initWithOAuthLoginWithType:type respBlock:respBlock];
    return tool;
}

#pragma mark - 三方分享

+ (void)sharedWithType:(PD3RShareType)type text:(NSString *)text title:(NSString *)title desc:(NSString *)desc thubImageData:(NSData *)thubImageData webUrl:(NSString *)webUrl {
    [[self alloc] sharedWithType:type text:text title:title desc:desc thubImageData:thubImageData webUrl:webUrl];
}

- (void)sharedWithType:(PD3RShareType)type text:(NSString *)text title:(NSString *)title desc:(NSString *)desc thubImageData:(NSData *)thubImageData webUrl:(NSString *)webUrl {
    if (type == PD3RShareTypeWXTimeLine || type == PD3RShareTypeWXSession) {
        if ([WXApi isWXAppInstalled]) {
            WXWebpageObject *webpage = [WXWebpageObject object];
            webpage.webpageUrl = webUrl;
            WXMediaMessage *mediaMessage = [WXMediaMessage message];
            mediaMessage.title = title;
            mediaMessage.description = desc;
            mediaMessage.thumbData = thubImageData;
            mediaMessage.mediaObject = webpage;
            SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
            req.text = text;
            req.bText = webUrl.length ? NO : YES;
            req.scene = type == PD3RShareTypeWXSession ? WXSceneSession : WXSceneTimeline;
            req.message = mediaMessage;
            [WXApi sendReq:req];
        }
    } else if (type == PD3RShareTypeQQFriend || type == PD3RShareTypeQQZone || type == PD3RShareTypeQQGroup) {
        if ([TencentOAuth iphoneQQInstalled]) {
            self.tencentOAuth = [[TencentOAuth alloc] initWithAppId:QQAppID andDelegate:self];
            QQApiNewsObject *news = [QQApiNewsObject objectWithURL:[NSURL URLWithString:webUrl] title:title description:desc previewImageData:thubImageData];
            SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:news];
            if (type == PD3RShareTypeQQFriend) {
                [QQApiInterface sendReq:req];
            } else if (type == PD3RShareTypeQQGroup) {
                [QQApiInterface SendReqToQQGroupTribe:req];
            } else {
                [QQApiInterface SendReqToQZone:req];
            }
        }
    } else {
        if ([WeiboSDK isWeiboAppInstalled]) {
            WBWebpageObject *media = [WBWebpageObject object];
            media.objectID = [NSString stringWithFormat:@"%li%@",(long)type,title];
            media.title = title;
            media.description = desc;
            media.thumbnailData = thubImageData;
            media.webpageUrl = webUrl;
            WBMessageObject *message = [WBMessageObject message];
            message.text = text;
            message.mediaObject = media;
            WBSendMessageToWeiboRequest *req = [WBSendMessageToWeiboRequest requestWithMessage:message];
            [WeiboSDK sendRequest:req];
        }
    }
}

#pragma mark - QQ授权登录 Delegate

- (void)tencentDidLogin {
    if (self.tencentOAuth.accessToken && self.tencentOAuth.accessToken.length != 0) {
        [self.tencentOAuth getUserInfo];
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled {
    
}

- (void)tencentDidNotNetWork {
    
}

- (void)getUserInfoResponse:(APIResponse *)response {
    respBlock respBlock = objc_getAssociatedObject([self class], qqRespKey);
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithDictionary:response.jsonResponse];
    [userInfo setObject:self.tencentOAuth.openId forKey:@"open_id"];
    if (respBlock) {
        respBlock([userInfo copy]);
    }
}

- (void)responseDidReceived:(APIResponse*)response forMessage:(NSString *)message {
}

- (BOOL)onTencentReq:(TencentApiReq *)req
{
    return YES;
}

- (BOOL)onTencentResp:(TencentApiResp *)resp {
    return YES;
}

@end
