//
//  PDSortManager.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDSortManager : NSObject

+(NSArray *)sortWithArr:(NSArray *)array;

@end
