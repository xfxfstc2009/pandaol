//
//  PDGravityImageView.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/7/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDGravityImageView : UIView

@property (nonatomic,strong) UIImage *image;
@property (nonatomic,strong,readonly) UIImageView * myImageView;

-(void)startAnimate;
-(void)stopAnimate;

@end
