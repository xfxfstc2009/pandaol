//
//  GWVideoCollectionViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/10.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWVideoCollectionViewCell.h"

@interface GWVideoCollectionViewCell(){
    dispatch_queue_t videoSessionQueue;             //  线程
}
@property (nonatomic,strong)AVCaptureVideoPreviewLayer *captureVideoPreviewLayer;                // 摄像取景Layer
@property (nonatomic,strong)PDImageView *takePhotoImg;
@end

@implementation GWVideoCollectionViewCell
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    if (!self.captureSession){
        NSArray *camerasArr = [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
        AVCaptureDevice *backCaptureDevice;
        for (AVCaptureDevice *device in camerasArr){
            if (device.position == AVCaptureDevicePositionBack){
                backCaptureDevice = device;
                break;
            }
        }
        if (backCaptureDevice){
            NSError *error = nil;
            AVCaptureDeviceInput *videoInput = [AVCaptureDeviceInput deviceInputWithDevice:backCaptureDevice error:&error];
            if (!videoInput || error){
                return;
            }
            
            self.captureSession = [[AVCaptureSession alloc]init];
            self.captureSession.sessionPreset = AVCaptureSessionPresetLow;
            [self.captureSession addInput:videoInput];
            
            AVCaptureVideoDataOutput *avCaptureVideoDataOutput = [[AVCaptureVideoDataOutput alloc]init];
            NSDictionary *settings = [[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithUnsignedInt:kCVPixelFormatType_32BGRA],kCVPixelBufferPixelFormatTypeKey, nil];
            avCaptureVideoDataOutput.videoSettings = settings;
            self.captureVideoPreviewLayer = [AVCaptureVideoPreviewLayer layerWithSession:self.captureSession];
            self.captureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
            self.captureVideoPreviewLayer.frame = self.bounds;
            [self.layer addSublayer:self.captureVideoPreviewLayer];
        }
    }
    
    //take photo
    self.takePhotoImg = [[PDImageView alloc]init];
    self.takePhotoImg.backgroundColor = [UIColor clearColor];
    self.takePhotoImg.frame = CGRectMake((self.size_width - LCFloat(52)) / 2., (self.size_height - LCFloat(42)) / 2., LCFloat(52), LCFloat(42));
    self.takePhotoImg.image = [UIImage imageNamed:@"icon_asset_takePhoto"];
    [self addSubview:self.takePhotoImg];
    [self actionRunningCapture];
}


-(void)actionRunningCamera{
    [self actionRunningCapture];
}



-(void)actionRunningCapture{
    if (!videoSessionQueue) {
        videoSessionQueue = dispatch_queue_create("smartmin", NULL);
    }
    __weak typeof(self) weakSelf = self;
    dispatch_async(videoSessionQueue, ^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.captureSession startRunning];
    });
}

@end
