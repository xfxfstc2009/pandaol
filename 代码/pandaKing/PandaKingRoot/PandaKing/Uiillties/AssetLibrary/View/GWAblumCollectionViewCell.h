//
//  GWAblumCollectionViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/18.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWAblumCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)UIImage *transferImage;             /**< 传递过来的图片*/
@property (nonatomic,copy)NSString *ablumName;                  /**< 相册名*/
@property (nonatomic,copy)NSString *numberOfAblum;              /**< 照片数量*/
+(CGSize)calculationCellSize ;
@end
