//
//  UIView+PDPrompt.m
//  PandaChallenge
//
//  Created by 盼达 on 16/3/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UIView+PDPrompt.h"
#import <objc/runtime.h>

#define PADDING 5.

static char promptKey;
static char tapBlockKey;
@implementation UIView (PDPrompt)

- (void)showPrompt:(NSString *)promptString withImage:(UIImage *)promptImage andImagePosition:(PDPromptImagePosition)position tapBlock:(void(^)())tapBlock{
    objc_setAssociatedObject(self, &tapBlockKey, tapBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    UIView *containerView = objc_getAssociatedObject(self, &promptKey);
    if (containerView) {
        [containerView removeFromSuperview];
    }
    containerView = [[UIView alloc] initWithFrame:self.bounds];
    containerView.userInteractionEnabled = NO;
    containerView.backgroundColor = self.backgroundColor;
    
    objc_setAssociatedObject(self, &promptKey, containerView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    UIImageView *promptImageView = nil;
    if (!promptImageView) {
        promptImageView = [[UIImageView alloc] initWithImage:promptImage];
        [containerView addSubview:promptImageView];
    }
    UILabel *promptLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    promptLabel.backgroundColor = [UIColor clearColor];
    promptLabel.textColor = [UIColor lightGrayColor];
    promptLabel.text = promptString;
    promptLabel.font = [UIFont systemFontOfCustomeSize:14.];
    [promptLabel sizeToFit];
    [containerView addSubview:promptLabel];
    
    CGSize imageSize = promptImageView.bounds.size;
    CGSize labelSize = promptLabel.bounds.size;
    
    if (!promptImage) {
        promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .5f);
        if ([self isKindOfClass:[UITableView class]]) {
            promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .5f - ((UITableView *) self).contentInset.bottom / 2.);
        }
    } else {
        if (position == PDPromptImagePositionLeft) {
            CGFloat comboWidth = imageSize.width + labelSize.width + PADDING;
            CGFloat minX = ceilf(self.bounds.size.width *.5f - comboWidth * .5f);
            promptImageView.center = CGPointMake(minX + imageSize.width * .5f, self.center.y);
            promptLabel.center = CGPointMake(minX + comboWidth - labelSize.width * .5f, self.center.y);
        } else {
            promptImageView.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .3f);
            promptLabel.center = CGPointMake(self.bounds.size.width * .5f, self.bounds.size.height * .35f + imageSize.height * .5f + labelSize.height * .5f + PADDING);
        }
    }
    
    [self addSubview:containerView];
    containerView.userInteractionEnabled = YES;
    // 添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [containerView addGestureRecognizer:tap];
    
}

-(void)tapManager{
    void(^tapBlock)() = objc_getAssociatedObject(self, &tapBlockKey);
    if (tapBlock){
        tapBlock();
    }
}

- (void)dismissPrompt {
    UIView *containerView = objc_getAssociatedObject(self, &promptKey);
    if (containerView) {
        [UIView animateWithDuration:.3f animations:^{
            containerView.alpha = 0;
        } completion:^(BOOL finished) {
            [containerView removeFromSuperview];
        }];
    }
}

- (void)showPrompt:(NSString *)promptString withImage:(UIImage *)promptImage buttonTitle:(NSString *)title clickBlock:(void (^)())block {
    objc_setAssociatedObject(self, &tapBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    UIView *containerView = objc_getAssociatedObject(self, &promptKey);
    if (containerView) {
        [containerView removeFromSuperview];
    }
    containerView = [[UIView alloc] initWithFrame:self.bounds];
    containerView.userInteractionEnabled = NO;
    containerView.backgroundColor = self.backgroundColor;
    containerView.userInteractionEnabled = YES;
    [self addSubview:containerView];
    
    objc_setAssociatedObject(self, &promptKey, containerView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
    
    CGSize size = containerView.frame.size;
    UIImageView *promptImageView = [[UIImageView alloc] initWithImage:promptImage];
    promptImageView.frame = CGRectMake((size.width - promptImage.size.width) / 2, (size.height - promptImage.size.height) / 2 - LCFloat(110), promptImage.size.width, promptImage.size.height);
    [containerView addSubview:promptImageView];
    
    UILabel *textLabel = [[UILabel alloc] init];
    textLabel.numberOfLines = 0;
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.text = promptString;
    textLabel.font = [UIFont systemFontOfCustomeSize:14];
    textLabel.textColor = [UIColor lightGrayColor];
    CGSize textSize = [textLabel.text sizeWithCalcFont:textLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2, CGFLOAT_MAX)];
    textLabel.frame = CGRectMake((size.width - textSize.width) / 2, CGRectGetMaxY(promptImageView.frame) + LCFloat(10), textSize.width, textSize.height);
    [containerView addSubview:textLabel];
    
    UIButton *actButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [actButton setTitle:title forState:UIControlStateNormal];
    [actButton setTitleColor:c14 forState:UIControlStateNormal];
    actButton.titleLabel.font = [UIFont systemFontOfCustomeSize:16];
    CGSize titleSize = [title sizeWithCalcFont:actButton.titleLabel.font];
    CGSize buttonSize = CGSizeMake(titleSize.width + LCFloat(25) * 2, LCFloat(20) * 2);
    actButton.frame = CGRectMake((size.width - buttonSize.width) / 2, CGRectGetMaxY(textLabel.frame) + LCFloat(26), buttonSize.width, buttonSize.height);
    actButton.layer.cornerRadius = buttonSize.height / 2;
    actButton.clipsToBounds = YES;
    actButton.layer.borderColor = c14.CGColor;
    actButton.layer.borderWidth = 1;
    [actButton addTarget:self action:@selector(tapManager) forControlEvents:UIControlEventTouchUpInside];
    [containerView addSubview:actButton];
}

@end
