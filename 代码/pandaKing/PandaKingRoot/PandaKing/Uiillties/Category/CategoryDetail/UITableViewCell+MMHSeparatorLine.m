//
//  UITableViewCell+MMHSeparatorLine.m
//  MamHao
//
//  Created by SmartMin on 15/4/8.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "UITableViewCell+MMHSeparatorLine.h"

#define kTopTag      @"topSeparatorTag"
#define kBottomTag   @"bottomSeparatorTag"

#define hltColor     @"e3e0de"

@implementation UITableViewCell (MMHSeparatorLine)

- (void)addSeparatorLineWithType:(SeparatorType)separatorType
{
    UIImageView *topImageView = (UIImageView *)[self viewWithStringTag:kTopTag];
    UIImageView *bottomImageView = (UIImageView *)[self viewWithStringTag:kBottomTag];
    
    CGFloat separatorInset   = (separatorType == SeparatorTypeBottom || separatorType == SeparatorTypeSingle)?0:15;
    BOOL    showTopSeparator = (separatorType == SeparatorTypeHead   || separatorType == SeparatorTypeSingle)?YES:NO;
    
    if (showTopSeparator)
    {
        if (!topImageView)
        {
            topImageView = [[UIImageView alloc] init];
            [topImageView setStringTag:kTopTag];
            [topImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
            
            [topImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        }
        [topImageView setFrame:CGRectMake(0, 0,CGRectGetWidth([self frame]), 0.5)];
        [self addSubview:topImageView];
    }
    else
    {
        if (topImageView)
            [topImageView removeFromSuperview];
    }
    
    if (!bottomImageView)
    {
        bottomImageView = [[UIImageView alloc] init];
        [bottomImageView setStringTag:kBottomTag];
        [bottomImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        [bottomImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
    }
    [bottomImageView setFrame:CGRectMake( separatorInset, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]), 0.5)];
    [self addSubview:bottomImageView];
}




#pragma mark -ares
- (void)addSeparatorLineWithTypeWithAres:(SeparatorType)separatorType andUseing:(NSString *)usingVC {
    UIImageView *topImageView = (UIImageView *)[self viewWithStringTag:kTopTag];
    UIImageView *bottomImageView = (UIImageView *)[self viewWithStringTag:kBottomTag];
    
//    CGFloat separatorInset   = (separatorType == SeparatorTypeBottom || separatorType == SeparatorTypeSingle)?0:LCFloat(56);
    BOOL    showTopSeparator = (separatorType == SeparatorTypeHead   || separatorType == SeparatorTypeSingle)?YES:NO;
    
    if (showTopSeparator) {
        if (!topImageView) {
            topImageView = [[UIImageView alloc] init];
            [topImageView setStringTag:kTopTag];
            [topImageView setImage:[UIImage imageWithRenderColor:[UIColor lightGrayColor] renderSize:CGSizeMake(10., 10.)]];
            [topImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor grayColor] renderSize:CGSizeMake(10., 10.)]];
        }
        [topImageView setFrame:CGRectMake(0, 0,CGRectGetWidth([self frame]), 0.5)];
        [self addSubview:topImageView];
    } else {
        if (topImageView)
            [topImageView removeFromSuperview];
    }
    
    if (!bottomImageView) {
        bottomImageView = [[UIImageView alloc] init];
        [bottomImageView setStringTag:kBottomTag];
        [bottomImageView setImage:[UIImage imageWithRenderColor:[UIColor colorWithRed:213/256. green:213/256. blue:213/256. alpha:1] renderSize:CGSizeMake(10., 10.)]];
        [bottomImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor hexChangeFloat:hltColor] renderSize:CGSizeMake(10., 10.)]];
    }
    if([usingVC isEqualToString:@"login"]){
        [bottomImageView setFrame:CGRectMake(LCFloat(20), CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]) - 2 * LCFloat(20), 0.5)];
    } else if ([usingVC isEqualToString:@"record"]){
        [bottomImageView setFrame:CGRectMake(0, CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]), 0.5)];
    } else if ([usingVC isEqualToString:@"slider"]){
        [bottomImageView setImage:[UIImage imageWithRenderColor:[UIColor hexChangeFloat:@"212223"] renderSize:CGSizeMake(10., 10.)]];
        [bottomImageView setFrame:CGRectMake(LCFloat(10), CGRectGetHeight([self frame])-0.5, CGRectGetWidth([self frame]) - 2 * LCFloat(10), 0.5)];
    }
    [self addSubview:bottomImageView];
}

@end
