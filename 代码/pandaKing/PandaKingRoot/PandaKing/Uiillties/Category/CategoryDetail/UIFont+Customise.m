//
//  UIFont+Customise.m
//  LaiCai
//
//  Created by SmartMin on 15/8/13.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import "UIFont+Customise.h"

@implementation UIFont (Customise)

+ (UIFont *)fontWithCustomerSizeName:(NSString *)fontName{
    CGFloat customerFontSize = 0.0;
    if ([fontName isEqualToString:@"标题"]){
        customerFontSize = 18;
    } else if ([fontName isEqualToString:@"副标题"]){
        customerFontSize = 16.;
    } else if ([fontName isEqualToString:@"正文"]){
        customerFontSize = 15.;
    } else if ([fontName isEqualToString:@"小正文"]){
        customerFontSize = 14.;
    } else if ([fontName isEqualToString:@"提示"]){
        customerFontSize = 13.;
    } else if ([fontName isEqualToString:@"小提示"]){
        customerFontSize = 12.;
    }
    
    if (kScreenBounds.size.width == 320) {
        return [UIFont systemFontOfSize:LCFloat(customerFontSize + 2)];
    }
    return [UIFont systemFontOfSize:LCFloat(customerFontSize)];
}

+ (UIFont *)fontWithGameCustomerSizeName:(NSString *)fontName{
    CGFloat customerFontSize = 0.0;
    if ([fontName isEqualToString:@"标题"]){
        customerFontSize = 18;
    } else if ([fontName isEqualToString:@"副标题"]){
        customerFontSize = 16.;
    } else if ([fontName isEqualToString:@"正文"]){
        customerFontSize = 15.;
    } else if ([fontName isEqualToString:@"小正文"]){
        customerFontSize = 14.;
    } else if ([fontName isEqualToString:@"提示"]){
        customerFontSize = 13.;
    } else if ([fontName isEqualToString:@"小提示"]){
        customerFontSize = 12.;
    }
    
    if (kScreenBounds.size.width == 320) {
        return [UIFont systemFontOfSize:customerFontSize + 2];
    }
    return [UIFont systemFontOfSize:customerFontSize];
}

+ (UIFont *)systemFontOfCustomeSize:(CGFloat)fontSize {
    return [self systemFontOfSize:LCFontSize(fontSize)];
}

extern CGFloat LCFontSize(CGFloat pointSize) {
    if (kScreenBounds.size.width == 320) {
        return LCFloat(pointSize + 2);
    }
    return LCFloat(pointSize);
}

- (UIFont *)boldFont {
    CGFloat pointSize = [self pointSize];
    return [UIFont boldSystemFontOfSize:pointSize];
}

+ (UIFont *)HelveticaNeueBoldFontSize:(CGFloat)size {
//    Verdana-Bold
    return [UIFont fontWithName:@"Verdana-Bold" size:LCFontSize(size)];
}


@end
