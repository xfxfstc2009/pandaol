

#import <UIKit/UIKit.h>

@interface UIImage (ImageEffects)

- (UIImage *)applyLightEffect;
- (UIImage *)applyExtraLightEffectforAres;
- (UIImage *)applyExtraLightEffect;
- (UIImage *)applyDarkEffect;
- (UIImage *)applyTintEffectWithColor:(UIColor *)tintColor;

- (UIImage *)applyBlurWithRadius:(CGFloat)blurRadius tintColor:(UIColor *)tintColor saturationDeltaFactor:(CGFloat)saturationDeltaFactor maskImage:(UIImage *)maskImage;

+(UIImage *)fullScreenImageWithName:(NSString *)name;

-(UIImage *)applyExtraWithAlAssetLibrary;

- (UIImage *)applyExtraLightEffectforDarkAres;
-(UIImage *)boxblurImageWithBlur:(CGFloat)blur;

- (UIImage *)bulrNaviBarWithBlur:(CGFloat)blur;
@end
