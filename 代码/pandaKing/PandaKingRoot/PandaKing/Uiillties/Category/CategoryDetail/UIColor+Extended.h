//
//  UIColor+Extended.h
//  LaiCai
//
//  Created by SmartMin on 15-8-10.
//  Copyright (c) 2015年 LaiCai. All rights reserved.
//

#import <UIKit/UIKit.h>

//#define C1
//c1：c2：＃222222
//c3：＃333333
//c4：＃999999
//c5：＃858585
//c6：＃d7d7d7
//


#define UUGrey         [UIColor colorWithRed:246.0/255.0 green:246.0/255.0 blue:246.0/255.0 alpha:1.0f]
#define UULightBlue    [UIColor colorWithRed:94.0/255.0 green:147.0/255.0 blue:196.0/255.0 alpha:1.0f]
#define UUGreen        [UIColor colorWithRed:77.0/255.0 green:186.0/255.0 blue:122.0/255.0 alpha:1.0f]
#define UUTitleColor   [UIColor colorWithRed:0.0/255.0 green:189.0/255.0 blue:113.0/255.0 alpha:1.0f]
#define UUButtonGrey   [UIColor colorWithRed:141.0/255.0 green:141.0/255.0 blue:141.0/255.0 alpha:1.0f]
#define UUFreshGreen   [UIColor colorWithRed:77.0/255.0 green:196.0/255.0 blue:122.0/255.0 alpha:1.0f]
#define UURed          [UIColor colorWithRed:245.0/255.0 green:94.0/255.0 blue:78.0/255.0 alpha:1.0f]
#define UUMauve        [UIColor colorWithRed:88.0/255.0 green:75.0/255.0 blue:103.0/255.0 alpha:1.0f]
#define UUBrown        [UIColor colorWithRed:119.0/255.0 green:107.0/255.0 blue:95.0/255.0 alpha:1.0f]
#define UUBlue         [UIColor colorWithRed:82.0/255.0 green:116.0/255.0 blue:188.0/255.0 alpha:1.0f]
#define UUDarkBlue     [UIColor colorWithRed:121.0/255.0 green:134.0/255.0 blue:142.0/255.0 alpha:1.0f]
#define UUYellow       [UIColor colorWithRed:242.0/255.0 green:197.0/255.0 blue:117.0/255.0 alpha:1.0f]
#define UUWhite        [UIColor colorWithRed:255.0/255.0 green:255.0/255.0 blue:255.0/255.0 alpha:1.0f]
#define UUDeepGrey     [UIColor colorWithRed:99.0/255.0 green:99.0/255.0 blue:99.0/255.0 alpha:1.0f]
#define UUPinkGrey     [UIColor colorWithRed:200.0/255.0 green:193.0/255.0 blue:193.0/255.0 alpha:1.0f]
#define UUHealYellow   [UIColor colorWithRed:245.0/255.0 green:242.0/255.0 blue:238.0/255.0 alpha:1.0f]
#define UULightGrey    [UIColor colorWithRed:225.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1.0f]
#define UUCleanGrey    [UIColor colorWithRed:251.0/255.0 green:251.0/255.0 blue:251.0/255.0 alpha:1.0f]
#define UULightYellow  [UIColor colorWithRed:241.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0f]
#define UUDarkYellow   [UIColor colorWithRed:152.0/255.0 green:150.0/255.0 blue:159.0/255.0 alpha:1.0f]
#define UUPinkDark     [UIColor colorWithRed:170.0/255.0 green:165.0/255.0 blue:165.0/255.0 alpha:1.0f]
#define UUCloudWhite   [UIColor colorWithRed:244.0/255.0 green:244.0/255.0 blue:244.0/255.0 alpha:1.0f]
#define UUBlack        [UIColor colorWithRed:45.0/255.0 green:45.0/255.0 blue:45.0/255.0 alpha:1.0f]
#define UUStarYellow   [UIColor colorWithRed:252.0/255.0 green:223.0/255.0 blue:101.0/255.0 alpha:1.0f]
#define UUTwitterColor [UIColor colorWithRed:0.0/255.0 green:171.0/255.0 blue:243.0/255.0 alpha:1.0]
#define UUWeiboColor   [UIColor colorWithRed:250.0/255.0 green:0.0/255.0 blue:33.0/255.0 alpha:1.0]
#define UUiOSGreenColor [UIColor colorWithRed:98.0/255.0 green:247.0/255.0 blue:77.0/255.0 alpha:1.0]
#define UURandomColor   [UIColor colorWithRed:arc4random()%255/255.0 green:arc4random()%255/255.0 blue:arc4random()%255/255.0 alpha:1.0f]
#define UUSmartRed  [UIColor colorWithRed:255./255.0 green:31.0/255.0 blue:66./255.0 alpha:1]


// Style
#define c1 [UIColor hexChangeFloat:@"ffffff"]                   /**< 白色*/
#define c2 [UIColor hexChangeFloat:@"222222"]                   /**< 深黑色*/
#define c3 [UIColor hexChangeFloat:@"333333"]                   /**< 中黑色*/
#define c4 [UIColor hexChangeFloat:@"999999"]                   /**< 深灰色*/
#define c5 [UIColor hexChangeFloat:@"858585"]                   /**< 浅黑色*/
#define c6 [UIColor hexChangeFloat:@"d7d7d7"]                   /**< 淡灰色 （线条色）*/
#define c7 [UIColor hexChangeFloat:@"000000"]                   /**< 黑色*/
#define c8 [UIColor hexChangeFloat:@"f2f2f2"]                   /**< 中灰色（背景）*/
#define c9 [UIColor hexChangeFloat:@"e4393c"]                   /**< 红色*/
#define c10 [UIColor hexChangeFloat:@"bdbdbd"]                  /**< 线条色*/
#define c11 [UIColor hexChangeFloat:@"e5d3c2"]                  /**< 金色*/
#define c12 [UIColor hexChangeFloat:@"ff5000"]                  /**< 橙色*/
#define c13 [UIColor hexChangeFloat:@"222332"]                  /**< 深蓝*/
#define c14 [UIColor hexChangeFloat:@"e7b975"]                  /**< 淡金色     上*/
#define c15 [UIColor hexChangeFloat:@"d3a25e"]                  /**< 亮金色            中     匹配中渐变色值*/
#define c16 [UIColor hexChangeFloat:@"b68241"]                  /**< 暗金色            下*/
#define c17 [UIColor hexChangeFloat:@"ffae00"]                  /**< 暗黄色            上*/
#define c18 [UIColor hexChangeFloat:@"ffc925"]                  /**< 淡黄色            中       倒数渐变色值*/
#define c19 [UIColor hexChangeFloat:@"ffe54b"]                  /**< 亮黄色              下*/
#define c20 [UIColor hexChangeFloat:@"b07e00"]                  /**< 深橙色               id名和网吧*/
#define c21 [UIColor hexChangeFloat:@"9c846d"]                  /**< 淡褐色                  裁判进入房间邀请信息等等*/
#define c22 [UIColor hexChangeFloat:@"7c7b78"]                  /**< 明灰色                  战斗中*/
#define c23 [UIColor hexChangeFloat:@"caa787"]                  /**< 淡橙色                  房间信息*/
#define c24 [UIColor hexChangeFloat:@"87725e"]                  /**< 浅橙色                   比赛规则*/
#define c25 [UIColor hexChangeFloat:@"603913"]                  /**< 深褐色*/
#define c26 [UIColor hexChangeFloat:@"d0ac66"]                  /**< 流金色*/
#define c27 [UIColor hexChangeFloat:@"ededed"]                  /**< 轻灰色*/
#define c28 [UIColor hexChangeFloat:@"666666"]                  /**< 表灰色*/
#define c29 [UIColor hexChangeFloat:@"007565"]                  /**< 表灰色*/
#define c30 [UIColor hexChangeFloat:@"a4000d"]                  /**< 表灰色*/
#define c31 [UIColor hexChangeFloat:@"f8f8f8"]                  /**< 表灰色*/
#define c32 [UIColor hexChangeFloat:@"afa084"]                  /**< 💩黄色*/
#define c33 [UIColor hexChangeFloat:@"f8f8f8"]                  /**< 大💩灰*/
#define c34 [UIColor hexChangeFloat:@"ffe3a3"]
#define c35 [UIColor hexChangeFloat:@"e8c88a"]
#define c36 [UIColor hexChangeFloat:@"00c5aa"]                  /**< 大💩灰*/
#define c37 [UIColor hexChangeFloat:@"f02c3b"]                  /**< 大💩灰*/
#define c38 [UIColor hexChangeFloat:@"86621b"]
#define c39 [UIColor hexChangeFloat:@"896a2d"]                  // 灰金色

#define c40 [UIColor hexChangeFloat:@"fbdeb0"]                  /**< 红包金*/
#define c41_ [UIColor hexChangeFloat:@"d3533d"]                  /**< 红包红*/
#define c42 [UIColor hexChangeFloat:@"fdf9f4"]                  /**< 红包背景粉*/
#define c43 [UIColor hexChangeFloat:@"db5443"]                  /**< 红包盖子红*/

#define c44 [UIColor hexChangeFloat:@"7d492a"]                  /**< 淡棕色*/
#define c45 [UIColor hexChangeFloat:@"de6c2b"]                  /**< 深橙色*/
#define c46 [UIColor hexChangeFloat:@"412714"]                  /**< 深棕色*/

@interface UIColor (Extended)
+ (UIColor *)hexChangeFloat:(NSString *)hexColor;
+ (UIColor *)colorWithCustomerName:(NSString *)colorName;
+ (UIColor *)colorWithHex:(NSInteger)intColor;
- (NSInteger)getHexColor;

@end
