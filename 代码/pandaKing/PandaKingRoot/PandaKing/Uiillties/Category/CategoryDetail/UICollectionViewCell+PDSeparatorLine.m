//
//  UICollectionViewCell+PDSeparatorLine.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "UICollectionViewCell+PDSeparatorLine.h"

#define kTopTag      @"topSeparatorTag1"
#define kBottomTag   @"bottomSeparatorTag1"

@implementation UICollectionViewCell (PDSeparatorLine)

- (void)addSeparatorLineWithType:(CollectionSeparatorType)separatorType{
    PDImageView *topImageView = (PDImageView *)[self viewWithStringTag:kTopTag];
    PDImageView *bottomImageView = (PDImageView *)[self viewWithStringTag:kBottomTag];
    
    BOOL    showTopSeparator = (separatorType == SeparatorTypeHead   || separatorType == SeparatorTypeSingle)?YES:NO;
    
    if (showTopSeparator)
    {
        if (!topImageView)
        {
            topImageView = [[PDImageView alloc] init];
            [topImageView setStringTag:kTopTag];
            topImageView.image = [UIImage imageNamed:@"icon_line_silver"];
            
            [topImageView setHighlightedImage:[UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"分割线"] renderSize:CGSizeMake(10., 10.)]];
        }
        [topImageView setFrame:CGRectMake(0, 0,CGRectGetWidth([self frame]), 0.5)];
        [self addSubview:topImageView];
    }
    else
    {
        if (topImageView)
            [topImageView removeFromSuperview];
    }
}

@end
