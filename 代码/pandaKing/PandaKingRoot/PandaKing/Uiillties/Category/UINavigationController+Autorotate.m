//
//  UINavigationController+Autorotate.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "UINavigationController+Autorotate.h"

@implementation UINavigationController (Autorotate)

- (BOOL)shouldAutorotate {
    return [self.topViewController shouldAutorotate];
    
}


- (NSUInteger)supportedInterfaceOrientations

{
    
    return [self.topViewController supportedInterfaceOrientations];
    
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation

{
    
    return [self.topViewController preferredInterfaceOrientationForPresentation];
    
}

@end
