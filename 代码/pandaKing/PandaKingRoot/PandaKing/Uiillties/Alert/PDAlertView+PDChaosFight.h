//
//  PDAlertView+PDChaosFight.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAlertView.h"
#import "PDAlertChaosFightBtnView.h"
#import "PDWebView.h"
#import "PDChaosFightTodayCountRootModel.h"

@interface PDAlertView (PDChaosFight)<UITableViewDataSource,UITableViewDelegate,PDWebViewDelegate>{
    
}

@property (nonatomic,strong)PDImageView *backgroundImageView;                       /**< 背景图片*/
@property (nonatomic,strong)UITableView *countTableView;                            /**< 统计tableView*/
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)NSMutableArray *segmentMutableArr;
@property (nonatomic,strong)PDWebView *webView;
@property (nonatomic,strong)PDChaosFightTodayCountRootModel *transferSingleModel;
@property (nonatomic,strong)NSMutableArray *historyMutableArr;
@property (nonatomic,strong)UITableView *rightTableView;
-(void)showChaosFightAlertManager;                                  // 显示规则

// 显示充值
-(void)showNoYueToChongzhiWithBlock:(void(^)())block;
#pragma mark - 显示到达上线方法
-(void)showShangxianWithBlock:(void(^)())block;
#pragma mark - 被踢出
-(void)showTichuWithBlock:(void(^)())block;
@end
