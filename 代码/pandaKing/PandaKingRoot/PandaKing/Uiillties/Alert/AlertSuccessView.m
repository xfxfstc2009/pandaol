//
//  AlertSuccessView.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AlertSuccessView.h"
#import "AlertLogoView.h"

@interface AlertSuccessView()
@property (nonatomic,strong)UIView *alphaBgView;
@property (nonatomic,strong)UIView *writeBgView;
@property (nonatomic,strong)AlertLogoView *logoView;


@end

@implementation AlertSuccessView

-(instancetype)initWithFrame:(CGRect)frame logoType:(alertLogoType)type{
    self = [super initWithFrame:frame];
    if (self){
        [self createViewWithLogoType:type];
    }
    return self;
}

-(void)createViewWithLogoType:(alertLogoType)type{
    // 1. 创建透明背景图片
    self.alphaBgView = [[UIView alloc]init];
    self.alphaBgView.backgroundColor = [UIColor clearColor];
    self.alphaBgView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(50), 100);
    [self addSubview:self.alphaBgView];
    
    // 2. 创建logo
    self.logoView = [[AlertLogoView alloc]initWithFrame:CGRectMake(100, 100, 100, 100) logoType:type];
    [self addSubview:self.logoView];
    
    // 3. 创建view
    self.writeBgView = [[UIView alloc]init];
    self.writeBgView.backgroundColor = [UIColor whiteColor];
    self.writeBgView.layer.cornerRadius = LCFloat(4);
    self.writeBgView.clipsToBounds = YES;
    [self addSubview:self.writeBgView];
    
    
}

@end
