//
//  PDAlertChaosFightBtnView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,PDAlertChaosFightBtnViewType) {
    PDAlertChaosFightBtnViewType1,
    PDAlertChaosFightBtnViewType2,
};

@interface PDAlertChaosFightBtnView : UIView

@property (nonatomic,assign)PDAlertChaosFightBtnViewType transferType;

@property (nonatomic,assign)BOOL isSelected;

@end
