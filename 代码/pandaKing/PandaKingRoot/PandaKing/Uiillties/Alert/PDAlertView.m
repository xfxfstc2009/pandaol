//
//  PDAlertView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAlertView.h"
#import <objc/runtime.h>
#import "PDAlertGenderSingleView.h"
#import "PDScrollGuideView.h" // 主业轮播

static char countDownKey;                   // 倒计时key
@interface PDAlertView()
@property (nonatomic,strong)JCAlertView *alert;
@property (nonatomic,strong)UITextField *inputTextField;
@property (nonatomic,assign)NSInteger countDown;
@property (nonatomic,strong)UILabel *countDownLabel;
@property (nonatomic,strong)PDImageView *caipanImageView;                   /**< 裁判图片*/
@property (nonatomic,strong)UIView *genderView;

// 这个是用在我的钱包兑换竹子的
@property (nonatomic, strong) UIButton *selButton;
@end

@implementation PDAlertView

+(instancetype)sharedAlertView{
    static PDAlertView *_sharedAlertView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAlertView = [[PDAlertView alloc] init];
    });
    return _sharedAlertView;
}

-(void)dismissWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [JCAlertView dismissWithCompletion:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}

-(void)dismissAllWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [JCAlertView dismissAllCompletion:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}

-(NSMutableArray *)alertArr{
    return [jCSingleTon shareSingleTon].alertStack;
}

#pragma mark - 弹出正常弹框
-(void)showNormalAlertTitle:(NSString *)title content:(NSString *)content isClose:(BOOL)isClose btnArr:(NSArray *)btnArr btnCliock:(void(^)(NSInteger buttonIndex,NSString *inputInfo ))actionBlock{
    [self showBlackAlertWithAlertType:alertBackgroundTypeNormal title:title content:content isClose:isClose house:nil couponModel:nil inputPlaceholader:nil isGender:NO btnArr:btnArr countDown:NO countDownBlock:NULL caipan:NO btnCliock:actionBlock];
}

#pragma mark - 显示男女
-(void)showGenderAlertWithEdit:(void (^)(alertGenderType))block{
    __weak typeof(self)weakSelf = self;
    [[PDAlertView sharedAlertView]showBlackAlertWithAlertType:alertBackgroundTypeNormal title:@"你是什么样的司机" content:nil isClose:YES house:nil couponModel:nil inputPlaceholader:nil isGender:YES btnArr:nil countDown:NO countDownBlock:NULL caipan:NO btnCliock:^(NSInteger buttonIndex, NSString *inputInfo) {
        if (!weakSelf){
            return ;
        }
        if (buttonIndex == alertGenderTypeBoy){
            block(alertGenderTypeBoy);
        } else {
            block(alertGenderTypeGirl);
        }
        [JCAlertView dismissWithCompletion:NULL];
    }];
}

-(void)showGenderAlertWithChoose:(void (^)(alertGenderType))block{
    __weak typeof(self)weakSelf = self;
    [[PDAlertView sharedAlertView]showBlackAlertWithAlertType:alertBackgroundTypeNormal title:@"你想要认识…" content:nil isClose:YES house:nil couponModel:nil inputPlaceholader:nil isGender:YES btnArr:nil countDown:NO countDownBlock:NULL caipan:NO btnCliock:^(NSInteger buttonIndex, NSString *inputInfo) {
        if (!weakSelf){
            return ;
        }
        if (buttonIndex == alertGenderTypeBoy){
            block(alertGenderTypeBoy);
        } else {
            block(alertGenderTypeGirl);
        }
        [JCAlertView dismissWithCompletion:NULL];
    }];
}

-(void)showAlertWithTitle:(NSString *)title conten:(NSString *)content isClose:(BOOL)isClose btnArr:(NSArray *)btnArr buttonClick:(void(^)(NSInteger buttonIndex))actionBlock{
    
    UIImageView *imgView = [self setupBackgroundViewWithType:alertBackgroundTypeNormal Close:isClose];
    
    // 1. 创建title
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = c2;
    titleLabel.text = title;
    [imgView addSubview:titleLabel];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:titleLabel.font])];
    titleLabel.frame = CGRectMake((imgView.size_width - titleSize.width) / 2., LCFloat(55), titleSize.width, [NSString contentofHeightWithFont:titleLabel.font]);
    
    // 1. 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = c4;
    lineView.frame = CGRectMake(titleLabel.orgin_x - LCFloat(16) -LCFloat(30), titleLabel.center.y, LCFloat(30), .5f);
    [imgView addSubview:lineView];
    
    // 2. 创建line2
    UIView *lineView2 = [[UIView alloc]init];
    lineView2.backgroundColor = c4;
    lineView2.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame)+LCFloat(16),lineView.orgin_y, LCFloat(30), .5f);
    [imgView addSubview:lineView2];
    
    // 3. 创建line2
    UIView *lineView3 = [[UIView alloc]init];
    lineView3.backgroundColor = c7;
    lineView3.frame = CGRectMake((imgView.size_width -LCFloat(33)) / 2., CGRectGetMaxY(titleLabel.frame)+LCFloat(27), LCFloat(33), LCFloat(2));
    [imgView addSubview:lineView3];
    
    // 4. 创建title2
    UILabel *titleLabel2 = [[UILabel alloc]init];
    titleLabel2.backgroundColor = [UIColor clearColor];
    titleLabel2.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    titleLabel2.text = content;
    titleLabel2.textAlignment = NSTextAlignmentCenter;
    [imgView addSubview:titleLabel2];
    titleLabel2.numberOfLines = 0;
    CGSize titleSize2 = [titleLabel2.text sizeWithCalcFont:titleLabel2.font constrainedToSize:CGSizeMake(imgView.size_width - 2 * LCFloat(25), CGFLOAT_MAX)];
    titleLabel2.frame = CGRectMake(LCFloat(25), CGRectGetMaxY(lineView3.frame) + LCFloat(27), imgView.size_width - 2 * LCFloat(25), titleSize2.height);
    
    
    
    // 5. 创建按钮
    CGFloat kmargin_left = LCFloat(24);
    CGFloat kmargin_y = CGRectGetMaxY(titleLabel2.frame) + LCFloat(91);
    CGFloat size_width = (imgView.size_width - 2 *kmargin_left - LCFloat(9)) / 2.;
    CGFloat size_height = LCFloat(30);
    
    for (int i = 0;i<btnArr.count;i++){             // 5. 创建按钮
        NSString *title=btnArr [i];
        
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        button1.backgroundColor = [UIColor blackColor];
        button1.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        if(btnArr.count == 1){
            button1.frame = CGRectMake(LCFloat(34), kmargin_y, imgView.size_width - 2 * LCFloat(34), size_height);
        } else{
 
            if(i == 0){
                button1.frame = CGRectMake(kmargin_left, kmargin_y, size_width, size_height);
                [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                button1.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
            } else if(i == 1){
                button1.frame = CGRectMake(imgView.size_width - kmargin_left - size_width, kmargin_y, size_width, size_height);
                button1.backgroundColor = [UIColor clearColor];
                button1.layer.borderColor = c4.CGColor;
                button1.layer.borderWidth = 1;
                [button1 setTitleColor:c4 forState:UIControlStateNormal];
            }
        }
        
        [button1 setTitle:title forState:UIControlStateNormal];
        [imgView addSubview:button1];
        __weak typeof(self)weakSelf = self;
        [button1 buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            if(actionBlock){
                actionBlock(i);
            }
        }];
    }
    
    // 重新修改frame
    imgView.size_height = kmargin_y + size_height + LCFloat(47);
    
    // 创建按钮
    
    UIButton *xBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    xBtn.backgroundColor = [UIColor clearColor];
    xBtn.frame = CGRectMake(imgView.size_width - LCFloat(30), 0, LCFloat(30), LCFloat(30));
    __weak typeof(self)weakSelf = self;
    [xBtn buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        [JCAlertView dismissWithCompletion:NULL];
    }];
    [imgView addSubview:xBtn];
    
    if (isClose){             // 可以关闭
        xBtn.hidden = NO;
    } else {                // 不可以关闭
        xBtn.hidden = YES;
    }
    
    [JCAlertView initWithCustomView:imgView dismissWhenTouchedBackground:NO];
}



-(void)showAlertWithType:(AlertType)type block:(void(^)())block{
    
}

#pragma mark - 创建背景
#pragma mark 白色
-(UIImageView *)setupBackgroundViewWithType:(alertBackgroundType)bgType Close:(BOOL)close{
    UIImageView *backgroundImageView = [[UIImageView alloc]init];
    UIImage *img ;

    if (bgType == alertBackgroundTypeNormal){
        if (close == YES){
            img = [UIImage imageNamed:@"bg_alert"];
        } else {
            img = [UIImage imageNamed:@"bg_alert_nor"];
        }
    } else if (bgType == alertBackgroundTypeBlack_Short){
        if (close == YES){
            img = [UIImage imageNamed:@"bg_alert_black"];
        } else {
            img = [UIImage imageNamed:@"bg_alert_black"];
        }
    } else if (bgType == alertBackgroundTypeBlack_Long){
        if (close == YES){
            img = [UIImage imageNamed:@"bg_alert_black"];
        } else {
            img = [UIImage imageNamed:@"bg_alert_black"];
        }
    } else if (bgType == alertBackgroundTypeBinding){
        if (close == YES){
            img = [UIImage imageNamed:@"icon_alert_binding"];
        } else {
            img = [UIImage imageNamed:@"icon_alert_binding"];
        }
    }
    
    if (bgType == alertBackgroundTypeNormal){
        backgroundImageView.image = [img stretchableImageWithLeftCapWidth:img.size.width * .34f topCapHeight:img.size.height * .6f];
        backgroundImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(70), 400);
    } else if ((bgType == alertBackgroundTypeBlack_Long) || (bgType == alertBackgroundTypeBlack_Short)){
        backgroundImageView.image = img;
        backgroundImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(57), 400);
    } else if (bgType == alertBackgroundTypeBinding){
        backgroundImageView.image = img;
        backgroundImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(30), LCFloat(300));
    }
    
    return backgroundImageView;
}


#pragma mark - 错误
-(void)showError:(NSString *)error{
    [self showAlertWithTitle:@"出现错误" conten:error isClose:YES btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissWithCompletion:NULL];
    }];
}



#pragma mark -  显示黑色弹框
-(void)showBlackAlertWithAlertType:(alertBackgroundType)alertType title:(NSString *)title content:(NSString *)content isClose:(BOOL)isClose house:(PDChallengeHouseModel *)houseModel couponModel:(PDCouponSingleModel *)couponModel inputPlaceholader:(NSString *)placeholder isGender:(BOOL)isGender btnArr:(NSArray *)btnArr countDown:(BOOL)countDown countDownBlock:(void(^)())countDownBlock caipan:(BOOL)caipan btnCliock:(void(^)(NSInteger buttonIndex,NSString *inputInfo ))actionBlock{
    
    // 1. 创建背景
    UIImageView *imgView = [self setupBackgroundViewWithType:alertType Close:isClose];
    
    if (isGender){
        imgView.size_width = kScreenBounds.size.width - 2 * LCFloat(57);
    }
    
    // 2.创建标题
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = (alertType == alertBackgroundTypeNormal? c2 : c20);
    titleLabel.text = title;
    [imgView addSubview:titleLabel];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:titleLabel.font])];
    titleLabel.frame = CGRectMake((imgView.size_width - titleSize.width) / 2., LCFloat(55), titleSize.width, [NSString contentofHeightWithFont:titleLabel.font]);
    if ((alertType == alertBackgroundTypeBlack_Short) || (alertType == alertBackgroundTypeBlack_Long)){
        titleLabel.orgin_y = LCFloat(64);
    } else {
        titleLabel.orgin_y = LCFloat(55);
    }
    
    // 3. 创建左边的view
    PDImageView *lineView = [[PDImageView alloc]init];
    lineView.frame = CGRectMake(titleLabel.orgin_x - LCFloat(16) -LCFloat(30), titleLabel.center.y, LCFloat(30), .5f);
    [imgView addSubview:lineView];
    
    // 3.1 创建右边的view
    PDImageView *lineView2 = [[PDImageView alloc]init];
    lineView2.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame)+LCFloat(16),lineView.orgin_y, LCFloat(30), .5f);
    [imgView addSubview:lineView2];
    
    // 3.2 创建下面的线
    PDImageView *lineView3 = [[PDImageView alloc]init];
    lineView3.frame = CGRectMake((imgView.size_width -LCFloat(33)) / 2., CGRectGetMaxY(titleLabel.frame)+LCFloat(27), LCFloat(33), LCFloat(2));
    [imgView addSubview:lineView3];
    
    if (alertType == alertBackgroundTypeNormal){
        lineView.backgroundColor = c4;
        lineView2.backgroundColor = c4;
        lineView3.backgroundColor = c7;
    } else {
        lineView.image = [UIImage imageNamed:@"icon_alert_black_line"];
        lineView2.image = [UIImage imageNamed:@"icon_alert_black_line"];
        lineView3.image = [UIImage imageNamed:@"icon_alert_black_bottomLine"];
        lineView.backgroundColor = [UIColor clearColor];
        lineView2.backgroundColor = [UIColor clearColor];
        lineView3.backgroundColor = [UIColor clearColor];
        lineView.frame = CGRectMake(titleLabel.orgin_x - LCFloat(18) - LCFloat(30), titleLabel.center.y - (LCFloat(13) / 2.), LCFloat(30), LCFloat(13));
        lineView2.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame) + LCFloat(18), lineView.orgin_y, lineView.size_width, lineView.size_height);
        lineView3.frame = CGRectMake((imgView.size_width - LCFloat(44)) / 2., CGRectGetMaxY(titleLabel.frame) + LCFloat(26), LCFloat(44), LCFloat(14));
    }
    
    // 4. 创建内容
    UILabel *titleLabel2 = [[UILabel alloc]init];
    titleLabel2.backgroundColor = [UIColor clearColor];
    titleLabel2.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    titleLabel2.text = content;
    titleLabel2.textAlignment = NSTextAlignmentLeft;
    [imgView addSubview:titleLabel2];
    titleLabel2.numberOfLines = 0;
    if (alertType == alertBackgroundTypeNormal){           
        CGSize titleSize2 = [titleLabel2.text sizeWithCalcFont:titleLabel2.font constrainedToSize:CGSizeMake(imgView.size_width - 2 * LCFloat(25), CGFLOAT_MAX)];
        titleLabel2.frame = CGRectMake(LCFloat(25), CGRectGetMaxY(lineView3.frame) + LCFloat(27), imgView.size_width - 2 * LCFloat(25), titleSize2.height);
        titleLabel2.textColor = [UIColor colorWithCustomerName:@"黑"];
        if (titleSize2.height <= [NSString contentofHeightWithFont:titleLabel2.font]){
            titleLabel2.textAlignment = NSTextAlignmentCenter;
        } else {
            titleLabel2.textAlignment = NSTextAlignmentLeft;
        }
        
        
    } else if ((alertType == alertBackgroundTypeBlack_Short) ||(alertType == alertBackgroundTypeBlack_Long)){
        titleLabel2.textColor = c21;
        CGSize titleSize2 = [titleLabel2.text sizeWithCalcFont:titleLabel2.font constrainedToSize:CGSizeMake(imgView.size_width - 2 * LCFloat(43), CGFLOAT_MAX)];
        titleLabel2.frame = CGRectMake(LCFloat(43), CGRectGetMaxY(lineView3.frame) + LCFloat(26), imgView.size_width - 2 * LCFloat(43), titleSize2.height);
    }
    
    
    
    UIView *houseView;
    // 6. 创建房间信息
    if (houseModel){
        houseView = [self createHouseViewWithWidth:imgView.size_width houseName:houseModel.houseName housePwd:houseModel.housePwd];
        houseView.frame = CGRectMake(0, CGRectGetMaxY(lineView3.frame) + LCFloat(27), imgView.size_width, houseView.size_height);
        [imgView addSubview:houseView];
        
        // 隐藏content
        titleLabel2.hidden = YES;
    }
    
    // 7. 创建优惠券信息
    UIView *couponView;
    if (couponModel){
        couponView = [self createCouponViewWithWidth:imgView.size_width - 2 * LCFloat(39) couponModel:couponModel];
        couponView.frame = CGRectMake(LCFloat(39), CGRectGetMaxY(titleLabel2.frame) + LCFloat(11), imgView.size_width - 2 * LCFloat(39), couponView.size_height);
        [imgView addSubview:couponView];
    }
    
    // 8. 创建inputView
    UITextField *textField;
    if (placeholder.length){
        textField = [self inputTextFieldWithPlaceholder:placeholder];
        textField.frame = CGRectMake(LCFloat(33), CGRectGetMaxY(titleLabel2.frame) + LCFloat(22), imgView.size_width - 2 * LCFloat(33), LCFloat(34));
        [imgView addSubview:textField];
        // 增加手势
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
        [imgView addGestureRecognizer:tap];
    }
    
    // 5. 创建按钮
    CGFloat kmargin_left = LCFloat(24);
    CGFloat kmargin_y = CGRectGetMaxY(titleLabel2.frame) + LCFloat(91);
    CGFloat size_width = (imgView.size_width - 2 *kmargin_left - LCFloat(9)) / 2.;
    CGFloat size_height = LCFloat(30);
    if ((alertType == alertBackgroundTypeBlack_Long) || (alertType == alertBackgroundTypeBlack_Short)){
        kmargin_left = LCFloat(20);
        kmargin_y = CGRectGetMaxY(titleLabel2.frame) + LCFloat(57);
        size_width = (imgView.size_width - 2 * kmargin_left - LCFloat(9)) / 2.;
        size_height = LCFloat(44);
        
        if (couponModel){               // 【判断是否有优惠券】
            kmargin_left = LCFloat(20);
            kmargin_y = CGRectGetMaxY(couponView.frame) + LCFloat(14);
            size_width = imgView.size_width - 2 * LCFloat(20);
            size_height = LCFloat(44);
        }
    }
    
    // 是否是输入框
    if (placeholder.length){
        kmargin_left = LCFloat(33);
        kmargin_y = CGRectGetMaxY(textField.frame) + LCFloat(16);
        size_width = imgView.size_width - 2 * LCFloat(33);
        size_height = LCFloat(34);
        
    }
    
    for (int i = 0 ; i < btnArr.count;i++){
        NSString *title = [btnArr objectAtIndex:i];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        button1.backgroundColor = [UIColor blackColor];
        button1.stringTag = [NSString stringWithFormat:@"button%li",(long)i];
        button1.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        
        if (alertType == alertBackgroundTypeNormal){
            if(btnArr.count == 1){
                button1.frame = CGRectMake(LCFloat(34), kmargin_y, imgView.size_width - 2 * LCFloat(34), size_height);
                if (placeholder){           // 输入框
                    
                }
            } else{
                if(i == 0){
                    button1.frame = CGRectMake(kmargin_left, kmargin_y, size_width, size_height);
                    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                    button1.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
                } else if(i == 1){
                    button1.frame = CGRectMake(imgView.size_width - kmargin_left - size_width, kmargin_y, size_width, size_height);
                    button1.backgroundColor = [UIColor clearColor];
                    button1.layer.borderColor = c4.CGColor;
                    button1.layer.borderWidth = 1;
                    [button1 setTitleColor:c4 forState:UIControlStateNormal];
                }
            }
        } else if ((alertType == alertBackgroundTypeBlack_Long) || (alertType == alertBackgroundTypeBlack_Short)){
            if (btnArr.count == 1){
                button1.frame = CGRectMake(kmargin_left, kmargin_y, imgView.size_width - 2 * kmargin_left, size_height);
                [button1 setBackgroundImage:[UIImage imageNamed:@"icon_alert_btn_black"] forState:UIControlStateNormal];
                [button1 setTitleColor:c20 forState:UIControlStateNormal];
                button1.backgroundColor = [UIColor clearColor];
            } else {
                CGFloat origin_y = CGRectGetMaxY(couponView.frame) + LCFloat(14) + (LCFloat(44) + LCFloat(5)) * i;
                button1.frame = CGRectMake(kmargin_left, origin_y, size_width, LCFloat(44));
                [button1 setBackgroundImage:[UIImage imageNamed:@"icon_alert_bg_goldenBtn"] forState:UIControlStateNormal];
                [button1 setTitleColor:c20 forState:UIControlStateNormal];
                button1.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
                button1.backgroundColor = [UIColor clearColor];
            }
        }
        
        [button1 setTitle:title forState:UIControlStateNormal];
        [imgView addSubview:button1];
        __weak typeof(self)weakSelf = self;
        [button1 buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            if(actionBlock){
                NSString *inputInfo = @"";
                if (placeholder.length){
                    inputInfo = textField.text;
                } else {
                    inputInfo = @"";
                }
                actionBlock(i,inputInfo);
            }
        }];
    }
    
    // max
    if (houseModel){
        imgView.size_height = CGRectGetMaxY(houseView.frame) + LCFloat(62);
    }
    if (couponModel){
        UIButton *button = (UIButton *)[imgView viewWithStringTag:@"button1"];
        imgView.size_height = CGRectGetMaxY(button.frame) + LCFloat(46);
    }
    if (placeholder){
        UIButton *button = (UIButton *)[imgView viewWithStringTag:@"button0"];
        imgView.size_height = CGRectGetMaxY(button.frame) + LCFloat(45);
    }
    
    
    // 创建倒计时
    if (countDown){         // 创建倒计时
        objc_setAssociatedObject(self, &countDownKey, countDownBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [self createCountDownLabelWithWidth:imgView.size_width];
        self.countDownLabel.orgin_y = CGRectGetMaxY(titleLabel2.frame) + LCFloat(35);
        [imgView addSubview:self.countDownLabel];
    }
    if (countDown){
        imgView.size_height = CGRectGetMaxY(self.countDownLabel.frame) + LCFloat(50);
    }
    
    if (caipan){
        self.caipanImageView = [[PDImageView alloc]init];
        self.caipanImageView.backgroundColor = [UIColor clearColor];
        self.caipanImageView.image = [UIImage imageNamed:@"img_binding_caipan"];
        self.caipanImageView.frame = CGRectMake(titleLabel2.orgin_x, CGRectGetMaxY(titleLabel2.frame) + LCFloat(4), titleLabel2.size_width, LCFloat(100));
        [imgView addSubview:self.caipanImageView];
        
        UIButton *button1 = (UIButton *)[imgView viewWithStringTag:@"button0"];
        button1.orgin_y = CGRectGetMaxY(self.caipanImageView.frame) + LCFloat(20);
        
        imgView.size_height = CGRectGetMaxY(button1.frame) + LCFloat(35);
    }
    
    if (isGender){
        __weak typeof(self)weakSelf = self;
        self.genderView = [self createGenderViewWithWidth:imgView.size_width block:^(alertGenderType type) {
            if (!weakSelf){
                return ;
            }
            if (actionBlock){
                if (type == alertGenderTypeBoy){
                    actionBlock(alertGenderTypeBoy,@"");
                } else {
                    actionBlock(alertGenderTypeGirl,@"");
                }
            }
        }];
        self.genderView.frame = CGRectMake(0, CGRectGetMaxY(lineView3.frame ) + LCFloat(27), imgView.size_width, self.genderView.size_height);
        [imgView addSubview:self.genderView];
    }
    if (isGender){
        imgView.size_height = CGRectGetMaxY(self.genderView.frame) + LCFloat(50);
    }
    [JCAlertView initWithCustomView:imgView dismissWhenTouchedBackground:isClose];
}

#pragma mark - 创建房间view
-(UIView *)createHouseViewWithWidth:(CGFloat)width houseName:(NSString *)houseName housePwd:(NSString *)pwd{
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor clearColor];
    
    // 1. 创建房间名
    UILabel *houseFixedLabel = [self houseLabel];
    houseFixedLabel.text = @"房间名:";
    [bgView addSubview:houseFixedLabel];
    CGSize houseFixedSize = [houseFixedLabel.text sizeWithCalcFont:houseFixedLabel.font constrainedToSize:CGSizeMake((width -  2 * LCFloat(42)), [NSString contentofHeightWithFont:houseFixedLabel.font])];
    houseFixedLabel.frame = CGRectMake(LCFloat(42), 0, houseFixedSize.width, [NSString contentofHeightWithFont:houseFixedLabel.font]);
    
    // 2. 创建房间名字
    UILabel *houseDymicLabel = [self houseLabel];
    houseDymicLabel.text = houseName;
    houseDymicLabel.numberOfLines = 0;
    [bgView addSubview:houseDymicLabel];
    CGSize houseDymicSize = [houseDymicLabel.text sizeWithCalcFont:houseDymicLabel.font constrainedToSize:CGSizeMake((width - CGRectGetMaxX(houseFixedLabel.frame) - LCFloat(42)), CGFLOAT_MAX)];
    houseDymicLabel.frame = CGRectMake(CGRectGetMaxX(houseFixedLabel.frame), houseFixedLabel.orgin_y, width - CGRectGetMaxX(houseFixedLabel.frame) - LCFloat(42), houseDymicSize.height);
    
    // 3. 创建房间密码
    UILabel *housePwdFixedLabel = [self houseLabel];
    housePwdFixedLabel.text = @"密码:";
    [bgView addSubview:housePwdFixedLabel];
    housePwdFixedLabel.textAlignment = NSTextAlignmentRight;
    housePwdFixedLabel.frame = CGRectMake(houseFixedLabel.orgin_x, CGRectGetMaxY(houseDymicLabel.frame) + LCFloat(14), houseFixedLabel.size_width, [NSString contentofHeightWithFont:housePwdFixedLabel.font]);
    
    // 4. 创建密码
    UILabel *housePwdDymicLabel = [self houseLabel];
    housePwdDymicLabel.text = pwd;
    housePwdDymicLabel.numberOfLines = 0;
    [bgView addSubview:housePwdDymicLabel];
    CGSize pwdDymicLabelSize = [housePwdDymicLabel.text sizeWithCalcFont:houseDymicLabel.font constrainedToSize:CGSizeMake(houseDymicLabel.size_width, CGFLOAT_MAX)];
    housePwdDymicLabel.frame = CGRectMake(houseDymicLabel.orgin_x, housePwdFixedLabel.orgin_y, houseDymicLabel.size_width, pwdDymicLabelSize.height);
    
    // 5. 计算bgview高度
    bgView.frame = CGRectMake(0, 0, width, CGRectGetMaxY(housePwdDymicLabel.frame));
    return bgView;
}

-(UILabel *)houseLabel{
    UILabel *houseInfoLaebl = [[UILabel alloc]init];
    houseInfoLaebl.backgroundColor = [UIColor clearColor];
    houseInfoLaebl.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    houseInfoLaebl.textColor = c21;
    return houseInfoLaebl;
}

#pragma mark - 创建优惠券view
-(PDCouponView *)createCouponViewWithWidth:(CGFloat)width couponModel:(PDCouponSingleModel *)couponModel{
    PDCouponView *couponView = [[PDCouponView alloc]initWithFrame:CGRectMake(0, 0, width, 40) couponModel:couponModel];
    return couponView;
}

#pragma makr - createInputFiled
-(UITextField *)inputTextFieldWithPlaceholder:(NSString *)placeholder{
    UITextField *inputTextField = [[UITextField alloc]init];
    inputTextField.backgroundColor = [UIColor clearColor];
    inputTextField.placeholder = placeholder;
    inputTextField.textColor = c4;
    inputTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    inputTextField.textAlignment = NSTextAlignmentCenter;
    self.inputTextField = inputTextField;
    return inputTextField;
}

-(void)tapManager{
    if (self.inputTextField && [self.inputTextField isFirstResponder]){
        [self.inputTextField resignFirstResponder];
    }
}

#pragma mark 创建倒计时
-(UILabel *)createCountDownLabelWithWidth:(CGFloat)width{
    self.countDownLabel = [[UILabel alloc]init];
    self.countDownLabel.backgroundColor = [UIColor clearColor];
    self.countDownLabel.font = [[UIFont systemFontOfCustomeSize:35.]boldFont];
    self.countDownLabel.textAlignment = NSTextAlignmentCenter;
    self.countDownLabel.frame = CGRectMake(0, 0, width, [NSString contentofHeightWithFont:self.countDownLabel.font]);
    
    // 2. 开启timer
    self.countDown = 30;
    if (![PDAlertView sharedAlertView].countDownTimer){
        [PDAlertView sharedAlertView].countDownTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
    }
    self.countDownLabel.text = [NSString stringWithFormat:@"%li",(long)self.countDown];
    return self.countDownLabel;
}

#pragma mark - 停止计时器
-(void)stopTomer{
    if ([PDAlertView sharedAlertView].countDownTimer){
        [[PDAlertView sharedAlertView].countDownTimer invalidate];
        [PDAlertView sharedAlertView].countDownTimer = nil;
    }
}

-(void)daojishiManager{
    self.countDown--;
    if (self.countDown <= 0){
        // 1. 关闭倒计时
        if ([PDAlertView sharedAlertView].countDownTimer){
            [[PDAlertView sharedAlertView].countDownTimer invalidate];
            [PDAlertView sharedAlertView].countDownTimer = nil;
        }
         [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];

        void(^countDownBlock)() = objc_getAssociatedObject(self, &countDownKey);
        if (countDownBlock){
            countDownBlock();
        }
       
    } else {
        self.countDownLabel.text = [NSString stringWithFormat:@"%li",(long)self.countDown];
    }
}

#pragma mark - 创建性别view
-(UIView *)createGenderViewWithWidth:(CGFloat)width block:(void(^)(alertGenderType type))block{
    UIView *genderView = [[UIView alloc]init];
    genderView.frame = CGRectMake(0, 0, width, LCFloat(145));
    
    //1 . 创建左侧按钮
    __weak typeof(self)weakSelf = self;
    PDAlertGenderSingleView *leftView = [[PDAlertGenderSingleView alloc]initWithFrame:CGRectMake(0, 0, genderView.size_width / 2., genderView.size_height) withSex:@"男司机" block:^{
        if (!weakSelf){
            return ;
        }
        block(alertGenderTypeBoy);
    }];
    leftView.backgroundColor = [UIColor clearColor];
    [genderView addSubview:leftView];
    
    PDAlertGenderSingleView *rightView = [[PDAlertGenderSingleView alloc]initWithFrame:CGRectMake(genderView.size_width / 2., 0, genderView.size_width / 2., genderView.size_height) withSex:@"女司机" block:^{
        if (!weakSelf){
            return ;
        }
        block(alertGenderTypeGirl);
    }];
    rightView.backgroundColor = [UIColor clearColor];
    genderView.size_height = leftView.size_height;
    [genderView addSubview:rightView];
    
    return genderView;
}

#pragma mark - 绑定

-(void)showAlertWithBindingCurrentAva:(NSString *)currentAva afterAva:(NSString *)afterAva WithBlock:(void(^)(NSInteger btnIndex))block{
    UIView *bgView = [self showBindingAlertCustomerViewWithAva:currentAva afterAva:afterAva block:^(NSInteger btnIndex) {
        block(btnIndex);
    }];
    
    [JCAlertView initWithCustomView:bgView dismissWhenTouchedBackground:NO];
}

-(UIView *)showBindingAlertCustomerViewWithAva:(NSString *)ava afterAva:(NSString *)afterAva block:(void(^)(NSInteger btnIndex))block{
    UIImageView *bgView = [self setupBackgroundViewWithType:alertBackgroundTypeBinding Close:NO];
    bgView.backgroundColor = [UIColor whiteColor];
    // 2. 添加认证提示文字
    UILabel *renzhengLabel = [[UILabel alloc]init];
    renzhengLabel.backgroundColor = [UIColor whiteColor];
    renzhengLabel.textAlignment = NSTextAlignmentCenter;
    renzhengLabel.text = @"认证提示";
    renzhengLabel.textColor = c26;
    renzhengLabel.font = [[UIFont systemFontOfCustomeSize:20.]boldFont];
    CGSize renzhengSize = [renzhengLabel.text sizeWithCalcFont:renzhengLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:renzhengLabel.font])];
    CGFloat renzhengWidth = renzhengSize.width + 2 * LCFloat(10);
    renzhengLabel.frame = CGRectMake((bgView.size_width - renzhengWidth) / 2., LCFloat(5), renzhengWidth, [NSString contentofHeightWithFont:renzhengLabel.font]);
    [bgView addSubview:renzhengLabel];
    
    // 1. 创建背景
    UILabel *fixedLabel1 = [[UILabel alloc]init];
    fixedLabel1.backgroundColor = [UIColor clearColor];
    fixedLabel1.text = @"经检测你的召唤师头像为:";
    fixedLabel1.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    fixedLabel1.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    fixedLabel1.numberOfLines = 2;
    fixedLabel1.adjustsFontSizeToFitWidth = YES;
    fixedLabel1.frame = CGRectMake(LCFloat(40), CGRectGetMaxY(renzhengLabel.frame) + LCFloat(19), LCFloat(70), 2 * [NSString contentofHeightWithFont:fixedLabel1.font]);
    [bgView addSubview:fixedLabel1];
    
    // 2. 创建fixed2
    UILabel *fixedLabel2 = [[UILabel alloc]init];
    fixedLabel2.backgroundColor = [UIColor clearColor];
    fixedLabel2.text = @"请进入游戏,将头像更改为:";
    fixedLabel2.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    fixedLabel2.numberOfLines = 2;
    fixedLabel2.adjustsFontSizeToFitWidth = YES;
    fixedLabel2.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    fixedLabel2.frame = CGRectMake(bgView.size_width - LCFloat(40) - LCFloat(70), fixedLabel1.orgin_y, fixedLabel1.size_width, fixedLabel1.size_height);
    [bgView addSubview:fixedLabel2];
    
    PDImageView *avatar1 = [[PDImageView alloc]init];
    avatar1.backgroundColor = [UIColor clearColor];
    [avatar1 uploadImageWithURL:ava placeholder:nil callback:NULL];
    avatar1.frame = CGRectMake(fixedLabel1.orgin_x, CGRectGetMaxY(fixedLabel1.frame) + LCFloat(5), LCFloat(70), LCFloat(70));
    [bgView addSubview:avatar1];
    
    PDImageView *arrowImgView = [[PDImageView alloc]init];
    arrowImgView.backgroundColor = [UIColor clearColor];
    UIImage *arrowImg = [UIImage imageNamed:@"icon_alert_binding_arrow"];
    arrowImgView.image = arrowImg;
    arrowImgView.frame = CGRectMake(0, 0, LCFloat(30), LCFloat(15));
    arrowImgView.center_x = bgView.size_width / 2.;
    arrowImgView.center_y = avatar1.orgin_y + avatar1.size_height / 2.;
    [bgView addSubview:arrowImgView];
    
    PDImageView *avatar2 = [[PDImageView alloc]init];
    avatar2.backgroundColor = [UIColor clearColor];
    [avatar2 uploadImageWithURL:afterAva placeholder:nil callback:NULL];
    avatar2.frame = CGRectMake(fixedLabel2.orgin_x, avatar1.orgin_y, avatar1.size_width, avatar1.size_height);
    [bgView addSubview:avatar2];
    
    UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button1 setTitle:@"我已更换" forState:UIControlStateNormal];
    button1.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    button1.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button1.frame = CGRectMake(avatar1.orgin_x, CGRectGetMaxY(avatar1.frame) + LCFloat(11), CGRectGetMaxX(avatar2.frame) - avatar1.orgin_x, LCFloat(40));
    
    __weak typeof(self)weakSelf = self;
    [button1 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block(0);
        }
    }];
    [bgView addSubview:button1];
    
    // 2. 创建稍后再说按钮
    UIButton *button2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [button2 setTitle:@"稍后再说" forState:UIControlStateNormal];
    button2.backgroundColor = [UIColor whiteColor];
    [button2 setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    button2.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    button2.layer.borderColor = [UIColor colorWithCustomerName:@"黑"].CGColor;
    button2.layer.borderWidth = 1;
    [button2 setTitleColor: [UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    [button2 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block(1);
        }
    }];
    button2.frame = CGRectMake(button1.orgin_x, CGRectGetMaxY(button1.frame) + LCFloat(11), button1.size_width, button1.size_height);
    [bgView addSubview:button2];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(LCFloat(25), CGRectGetMaxY(button2.frame) + LCFloat(11), bgView.size_width - 2 * LCFloat(25), .5f);
    [bgView addSubview:lineView];
    
    // 3. fixedLabel
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.backgroundColor = [UIColor clearColor];
    fixedLabel.font = [[UIFont fontWithCustomerSizeName:@"小提示"]boldFont];
    fixedLabel.text = @"请完成以下认证步骤";
    CGSize fixedSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:fixedLabel.font])];
    fixedLabel.frame = CGRectMake(avatar1.orgin_x, CGRectGetMaxY(lineView.frame) + LCFloat(11), fixedSize.width , [NSString contentofHeightWithFont:fixedLabel.font]);
    [bgView addSubview:fixedLabel];
    
    //
    NSArray *topInfoArr = @[@"1.登录英雄联盟客户端",@"2.点击游戏大厅右上角召唤师头像",@"3.根据提示更换召唤师头像",@"4.返回App点击【我已更换】提交验证"];
    NSMutableArray *labelMutableArr = [NSMutableArray array];
    CGFloat origin_y = CGRectGetMaxY(fixedLabel.frame);
    for (int i = 0 ; i < topInfoArr.count;i++){
        UILabel *fixedLabel = [[UILabel alloc]init];
        fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.textColor = [UIColor colorWithCustomerName:@"淡灰"];
        fixedLabel.text = [topInfoArr objectAtIndex:i];
        CGSize contentOfSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11),CGFLOAT_MAX)];
        fixedLabel.frame = CGRectMake(avatar1.orgin_x,origin_y, kScreenBounds.size.width - 2 * LCFloat(11), contentOfSize.height);
        origin_y += contentOfSize.height;
        
        [bgView addSubview:fixedLabel];
        [labelMutableArr addObject:fixedLabel];
    }
    
    bgView.size_height = origin_y + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]] + LCFloat(25);
    
    
    fixedLabel1.hidden = YES;
    avatar1.hidden = YES;
    arrowImgView.hidden = YES;
    fixedLabel2.frame = CGRectMake(LCFloat(40), fixedLabel2.orgin_y, bgView.size_width - 2 * LCFloat(40), [NSString contentofHeightWithFont:fixedLabel2.font]);
    fixedLabel2.textAlignment = NSTextAlignmentCenter;
    fixedLabel2.center_x = bgView.center_x;
    avatar2.center_x = bgView.center_x;

    return bgView;
}

#pragma mark -
#pragma mark -- 兑换竹子的Alert

- (void)showAlertForWalletWithTitle:(NSString *)title cancleEnable:(BOOL)en exchangeModelArr:(NSArray<PDPropertyItem *> *)itemArr selBlock:(void (^)(NSInteger, PDPropertyItem *))selBlock {
    // 1. 创建背景
    UIImageView *imgView = [self setupBackgroundViewWithType:alertBackgroundTypeNormal Close:en];
    
    if (en) {
        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        closeButton.frame = CGRectMake(imgView.size_width - 25, 0, 25, 25);
        [closeButton buttonWithBlock:^(UIButton *button) {
            [JCAlertView dismissAllCompletion:NULL];
        }];
        [imgView addSubview:closeButton];
    }
    
    // 2.创建标题
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = c2;
    titleLabel.text = title;
    [imgView addSubview:titleLabel];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:titleLabel.font])];
    titleLabel.frame = CGRectMake((imgView.size_width - titleSize.width) / 2., LCFloat(55), titleSize.width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.orgin_y = LCFloat(64);
    
    // 3. 创建左边的view
    PDImageView *lineView = [[PDImageView alloc]init];
    lineView.frame = CGRectMake(titleLabel.orgin_x - LCFloat(16) -LCFloat(30), titleLabel.center.y, LCFloat(30), .5f);
    [imgView addSubview:lineView];
    
    // 3.1 创建右边的view
    PDImageView *lineView2 = [[PDImageView alloc]init];
    lineView2.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame)+LCFloat(16),lineView.orgin_y, LCFloat(30), .5f);
    [imgView addSubview:lineView2];
    
    // 3.2 创建下面的线
    PDImageView *lineView3 = [[PDImageView alloc]init];
    lineView3.frame = CGRectMake((imgView.size_width -LCFloat(33)) / 2., CGRectGetMaxY(titleLabel.frame)+LCFloat(27), LCFloat(33), LCFloat(2));
    [imgView addSubview:lineView3];
    
    lineView.backgroundColor = c4;
    lineView2.backgroundColor = c4;
    lineView3.backgroundColor = c7;
    
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(LCFloat(50), LCFloat(25) + CGRectGetMaxY(lineView3.frame), imgView.size_width - LCFloat(50) * 2, 0)];
    [imgView addSubview:buttonView];
    
    for (int i = 0; i < itemArr.count; i++) {
        PDPropertyItem *item = itemArr[i];
        
        UIButton *button = [[UIButton alloc] init];
        button.tag = i;
        [button setBackgroundImage:[UIImage imageNamed:@"bg_wallet_btn_sel"] forState:UIControlStateSelected];
        [button setBackgroundImage:[UIImage imageNamed:@"bg_wallet_btn_normal"] forState:UIControlStateNormal];
        button.titleLabel.numberOfLines = 3;
        button.titleLabel.font = [UIFont systemFontOfCustomeSize:12];
        button.titleLabel.textAlignment = NSTextAlignmentCenter;
        [button setTitleColor:c26 forState:UIControlStateSelected];
        [button setTitleColor:c4 forState:UIControlStateNormal];
        
        NSString *gold = [PDCenterTool separateStringWithString:[NSString stringWithFormat:@"%ld",item.gold]];
        [button setTitle:[NSString stringWithFormat:@"%@金币\n兑换\n%ld竹子",gold,(long)item.bamboo] forState:UIControlStateNormal];
        CGSize titleSize = [button.titleLabel.text sizeWithCalcFont:button.titleLabel.font];
        CGSize buttonSize = CGSizeMake(buttonView.size_width, titleSize.height + 6);
        button.frame = CGRectMake(0, i * ((LCFloat(20) + buttonSize.height)), CGRectGetWidth(buttonView.frame), buttonSize.height);
        [buttonView addSubview:button];
        
        buttonView.frame = CGRectMake(buttonView.orgin_x, buttonView.orgin_y, buttonView.size_width, CGRectGetMaxY(button.frame));
        
        if (i == 0) {
            button.selected = YES;
            self.selButton = button;
        }
        
        __weak typeof(self) weakSelf = self;
        [button buttonWithBlock:^(UIButton *button) {
            
            if (weakSelf.selButton != button) {
                
                weakSelf.selButton.selected = NO;
                button.selected = YES;
                weakSelf.selButton = button;
                
            }
            
        }];
    }
    
    UIButton *submitButton = [[UIButton alloc] initWithFrame:CGRectMake(LCFloat(30), CGRectGetMaxY(buttonView.frame) + LCFloat(30), imgView.size_width - LCFloat(30) * 2, LCFloat(36))];
    submitButton.backgroundColor = c2;
    submitButton.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    [submitButton setTitle:@"确定兑换" forState:UIControlStateNormal];
    [imgView addSubview:submitButton];
    __weak typeof(self) weakSelf = self;
    
    [submitButton buttonWithBlock:^(UIButton *button) {

        [JCAlertView dismissAllCompletion:NULL];
        
        PDPropertyItem *selItem = itemArr[weakSelf.selButton.tag];
        if (selBlock) {
            selBlock(weakSelf.selButton.tag, selItem);
        }
    }];
    
    imgView.size_height = CGRectGetMaxY(submitButton.frame) + LCFloat(30);
    
    [JCAlertView initWithCustomView:imgView dismissWhenTouchedBackground:NO];
}

#pragma mark - 主页引导弹框

- (void)showScrollGuideViewWithDismissComplication:(void (^)())complication {
    PDScrollGuideView *guideView = [[PDScrollGuideView alloc] init];
    [JCAlertView initWithCustomView:guideView dismissWhenTouchedBackground:NO];
    [guideView didClickCancleButtonWithComplication:^{
        [JCAlertView dismissAllCompletion:nil];
        if (complication) {
            complication();
        }
    }];
}

- (void)showPacketAlertViewWithModel:(PDPacketCheckInfo *)model didClickWithComplication:(void (^)(NSString *, NSString *))complication {
    PDPacketAlertView *packetView = [[PDPacketAlertView alloc] initWithModel:model];
    [JCAlertView initWithCustomView:packetView dismissWhenTouchedBackground:YES];
    [packetView packetViewDidClickCancle:^{
        [JCAlertView dismissAllCompletion:nil];
    }];
    [packetView packetViewDidClickOpen:^{
        !complication ?: complication(model.redEnvelopeId, @"open");
    }];
    [packetView packetViewDidClickLinkOthers:^{
        !complication ?: complication(model.redEnvelopeId, @"other");
    }];
}


#pragma mark - 显示弹框被认证5次
-(void)showAlertBeirenzhengWithModel:(PDFindactivatedtimesSingleModel *)singleModel btnArr:(NSArray *)btnArr btnClick:(void(^)(NSInteger buttonIndex))actionBlock{
    // 1. 创建背景
    UIImageView *imgView = [self setupBackgroundViewWithType:alertBackgroundTypeNormal Close:YES];

    // 2.创建标题
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = c2;
    titleLabel.text = @"认证提示";
    [imgView addSubview:titleLabel];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:titleLabel.font])];
    titleLabel.frame = CGRectMake((imgView.size_width - titleSize.width) / 2., LCFloat(55), titleSize.width, [NSString contentofHeightWithFont:titleLabel.font]);
    titleLabel.orgin_y = LCFloat(64);
    
    // 3. 创建左边的view
    PDImageView *lineView = [[PDImageView alloc]init];
    lineView.frame = CGRectMake(titleLabel.orgin_x - LCFloat(16) -LCFloat(30), titleLabel.center.y, LCFloat(30), .5f);
    [imgView addSubview:lineView];
    
    // 3.1 创建右边的view
    PDImageView *lineView2 = [[PDImageView alloc]init];
    lineView2.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame)+LCFloat(16),lineView.orgin_y, LCFloat(30), .5f);
    [imgView addSubview:lineView2];
    
    // 3.2 创建下面的线
    PDImageView *lineView3 = [[PDImageView alloc]init];
    lineView3.frame = CGRectMake((imgView.size_width -LCFloat(33)) / 2., CGRectGetMaxY(titleLabel.frame)+LCFloat(27), LCFloat(33), LCFloat(2));
    [imgView addSubview:lineView3];
    
    lineView.backgroundColor = c4;
    lineView2.backgroundColor = c4;
    lineView3.backgroundColor = c7;

    // .
    UILabel *zhaohuanshibeirenzhengLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    zhaohuanshibeirenzhengLabel.text = @"该召唤师已被认证：";
    zhaohuanshibeirenzhengLabel.textAlignment = NSTextAlignmentCenter;
    zhaohuanshibeirenzhengLabel.frame = CGRectMake(LCFloat(25), CGRectGetMaxY(lineView3.frame) + 27, imgView.size_width - 2 * LCFloat(25), [NSString contentofHeightWithFont:zhaohuanshibeirenzhengLabel.font]);
    [imgView addSubview:zhaohuanshibeirenzhengLabel];
    
    UILabel *timeDymicLabel = [GWViewTool createLabelFont:@"标题" textColor:@"红"];
    timeDymicLabel.text = [NSString stringWithFormat:@"%li",singleModel.activatedTimes];
    timeDymicLabel.font = [[UIFont systemFontOfCustomeSize:50]boldFont];
    timeDymicLabel.frame = CGRectMake((imgView.size_width - 80) / 2., CGRectGetMaxY(zhaohuanshibeirenzhengLabel.frame) + LCFloat(11), 80, 40);
    timeDymicLabel.textAlignment = NSTextAlignmentCenter;
    timeDymicLabel.adjustsFontSizeToFitWidth = YES;
    [imgView addSubview:timeDymicLabel];
    
    UILabel *timeFixedLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    timeFixedLabel.text = @"次";
    CGSize timeFixedSize = [timeFixedLabel.text sizeWithCalcFont:timeFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:timeFixedLabel.font])];
    timeFixedLabel.frame = CGRectMake(CGRectGetMaxX(timeDymicLabel.frame) + LCFloat(11), 0, timeFixedSize.width, [NSString contentofHeightWithFont:timeFixedLabel.font]);
    timeFixedLabel.center_y = timeDymicLabel.center_y;
    [imgView addSubview:timeFixedLabel];
    
    
    // 4. 创建内容
    UILabel *titleLabel2 = [[UILabel alloc]init];
    titleLabel2.backgroundColor = [UIColor clearColor];
    titleLabel2.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    titleLabel2.text = [NSString stringWithFormat:@"召唤师被认证超过%li次即无法获得任务奖励，您可以：",singleModel.limitTimes];
    titleLabel2.textAlignment = NSTextAlignmentLeft;
    [imgView addSubview:titleLabel2];
    titleLabel2.numberOfLines = 0;
    CGSize titleSize2 = [titleLabel2.text sizeWithCalcFont:titleLabel2.font constrainedToSize:CGSizeMake(imgView.size_width - 2 * LCFloat(25), CGFLOAT_MAX)];
    titleLabel2.frame = CGRectMake(LCFloat(25), CGRectGetMaxY(timeDymicLabel.frame) + LCFloat(27), imgView.size_width - 2 * LCFloat(25), titleSize2.height);
    titleLabel2.textColor = [UIColor colorWithCustomerName:@"黑"];
    if (titleSize2.height <= [NSString contentofHeightWithFont:titleLabel2.font]){
        titleLabel2.textAlignment = NSTextAlignmentCenter;
    } else {
        titleLabel2.textAlignment = NSTextAlignmentLeft;
    }

    // 5. 创建按钮
    CGFloat kmargin_left = LCFloat(24);
    CGFloat kmargin_y = CGRectGetMaxY(titleLabel2.frame) + LCFloat(31);
    CGFloat size_width = (imgView.size_width - 2 *kmargin_left - LCFloat(9)) / 2.;
    CGFloat size_height = LCFloat(30);
    
    for (int i = 0 ; i < btnArr.count;i++){
        NSString *title = [btnArr objectAtIndex:i];
        UIButton *button1 = [UIButton buttonWithType:UIButtonTypeCustom];
        button1.backgroundColor = [UIColor whiteColor];
        button1.stringTag = [NSString stringWithFormat:@"button%li",(long)i];
        button1.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        if(i == 1){
            button1.frame = CGRectMake(kmargin_left, kmargin_y, size_width, size_height);
            [button1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            button1.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        } else if(i == 0){
            button1.frame = CGRectMake(imgView.size_width - kmargin_left - size_width, kmargin_y, size_width, size_height);
            button1.backgroundColor = [UIColor clearColor];
            button1.layer.borderColor = c4.CGColor;
            button1.layer.borderWidth = 1;
            [button1 setTitleColor:c4 forState:UIControlStateNormal];
        }
    [button1 setTitle:title forState:UIControlStateNormal];
    [imgView addSubview:button1];
        __weak typeof(self)weakSelf = self;
        [button1 buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            if(actionBlock){
                actionBlock(i);
            }
        }];
    }
    [JCAlertView initWithCustomView:imgView dismissWhenTouchedBackground:NO];
}

#pragma mark - 签到

- (void)showSignAlertWithDrawComplication:(void (^)())complication {
    PDAlertSignInView *signView = [[PDAlertSignInView alloc] initWithFrame:CGRectZero];
    [JCAlertView initWithCustomView:signView dismissWhenTouchedBackground:YES];
    [signView didClickCancle:^{
        [JCAlertView dismissWithCompletion:nil];
    }];
    
    [signView didClickDraw:^{
        if (complication) {
            complication();
        }
        [JCAlertView dismissWithCompletion:nil];
    }];
}

@end
