//
//  PDAlertSignInView.h
//  PandaKing
//
//  Created by Cranz on 17/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDAlertSignInView : UIView
- (void)didClickDraw:(void(^)())drawBlock;
- (void)didClickCancle:(void(^)())cacleBlock;
@end
