//
//  AlertShowType.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef AlertShowType_h
#define AlertShowType_h

typedef NS_ENUM(NSInteger,alertLogoType) {
    alertLogoTypeSuccess,                    /**< 成功*/
    alertLogoTypeError,                      /**< 错误*/
    alertLogoTypeFail,                       /**< 失败*/
};

#endif /* AlertShowType_h */
