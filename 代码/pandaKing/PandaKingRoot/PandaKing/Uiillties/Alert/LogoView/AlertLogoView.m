//
//  AlertLogoView.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AlertLogoView.h"

NSInteger const Simble_Size = 60;
NSInteger const Simble_Top = 20;


@implementation AlertLogoView

-(instancetype)initWithFrame:(CGRect)frame logoType:(alertLogoType)type{
    self = [super initWithFrame:frame];
    if (self){
        self.frame = CGRectMake((self.frame.size.width - Simble_Size) / 2., Simble_Top, Simble_Size, Simble_Size);
        
        if (type == alertLogoTypeError){
            [self createErrorView];
        } else if (type == alertLogoTypeFail){
            [self createFailView];
        } else if (type == alertLogoTypeSuccess){
            [self createTickView];
        }
    }
    return self;
}

-(void)createErrorView{
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(Simble_Size / 2., Simble_Size / 2.) radius:Simble_Size / 2. startAngle:0 endAngle:M_PI*2 clockwise:YES];
    
    CGPoint p1 =  CGPointMake(Simble_Size / 4., Simble_Size / 4.);
    [path moveToPoint:p1];
    
    CGPoint p2 =  CGPointMake(Simble_Size / 4 * 3, Simble_Size / 4 * 3);
    [path addLineToPoint:p2];
    
    CGPoint p3 =  CGPointMake(Simble_Size / 4 * 3, Simble_Size / 4);
    [path moveToPoint:p3];
    
    CGPoint p4 =  CGPointMake(Simble_Size / 4, Simble_Size / 4 * 3);
    [path addLineToPoint:p4];
    
    
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    layer.lineWidth = 5;
    layer.path = path.CGPath;
    layer.fillColor = [UIColor clearColor].CGColor;
    layer.strokeColor = [UIColor redColor].CGColor;
    
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:NSStringFromSelector(@selector(strokeEnd))];
    animation.fromValue = @0;
    animation.toValue = @1;
    animation.duration = 0.5;
    [layer addAnimation:animation forKey:NSStringFromSelector(@selector(strokeEnd))];
    
    [self.layer addSublayer:layer];
}

-(void)createFailView{
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(Simble_Size / 2, Simble_Size / 2) radius:Simble_Size / 2 startAngle:0 endAngle:M_PI*2 clockwise:YES];
    path.lineCapStyle = kCGLineCapRound;
    path.lineJoinStyle = kCGLineCapRound;
    
    [path moveToPoint:CGPointMake(Simble_Size / 2., Simble_Size / 6.)];
    CGPoint p1 = CGPointMake(Simble_Size / 2., Simble_Size / 6 * 3.8);
    [path addLineToPoint:p1];
    
    [path moveToPoint:CGPointMake(Simble_Size/2, Simble_Size/6*4.5)];
    [path addArcWithCenter:CGPointMake(Simble_Size/2, Simble_Size/6*4.5) radius:2 startAngle:0 endAngle:M_PI*2 clockwise:YES];
    
    
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    layer.fillColor = [UIColor clearColor].CGColor;
    layer.strokeColor = [UIColor orangeColor].CGColor;
    layer.lineWidth = 5;
    layer.path = path.CGPath;
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:NSStringFromSelector(@selector(strokeEnd))];
    animation.fromValue = @0;
    animation.toValue = @1;
    animation.duration = 0.5;
    [layer addAnimation:animation forKey:NSStringFromSelector(@selector(strokeEnd))];
    
    [self.layer addSublayer:layer];
}

- (void)createTickView {
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:CGPointMake(Simble_Size / 2., Simble_Size / 2.) radius:Simble_Size/2 startAngle:0 endAngle:M_PI*2 clockwise:YES];
    
    path.lineCapStyle = kCGLineCapRound;
    path.lineJoinStyle = kCGLineCapRound;
    
    [path moveToPoint:CGPointMake(Simble_Size / 4., Simble_Size / 2.)];
    CGPoint p1 = CGPointMake(Simble_Size / 4 + 10, Simble_Size / 2 + 10);
    [path addLineToPoint:p1];
    
    
    CGPoint p2 = CGPointMake(Simble_Size/4*3, Simble_Size/4);
    [path addLineToPoint:p2];
    
    CAShapeLayer *layer = [[CAShapeLayer alloc] init];
    layer.fillColor = [UIColor clearColor].CGColor;
    layer.strokeColor = [UIColor greenColor].CGColor;
    layer.lineWidth = 5;
    layer.path = path.CGPath;
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:NSStringFromSelector(@selector(strokeEnd))];
    animation.fromValue = @0;
    animation.toValue = @1;
    animation.duration = 0.5;
    [layer addAnimation:animation forKey:NSStringFromSelector(@selector(strokeEnd))];
    
    [self.layer addSublayer:layer];
}


@end
