//
//  PDScrollGuideView.m
//  CCZScrollGuideView
//
//  Created by 金峰 on 2017/3/17.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import "PDScrollGuideView.h"
#import "PDAmusementItem.h"

#define kGuideScreenSize [UIScreen mainScreen].bounds.size
#define kGuideWidth       (kGuideScreenSize.width * 0.7)
#define kGuidePartHeight1 kGuideWidth
#define kGuidePartHeight2 100
#define kGuidePartHeight3 80

// 从左上角开始到右下角
#define kColorPart1_1 @"ff435c"
#define kColorPart1_2 @"ff764e"
#define kColorPart2_1 @"ff8d22"
#define kColorPart2_2 @"ffbf52"
#define kColorPart3_1 @"79e977"
#define kColorPart3_2 @"27c8a1"
#define kColorPart4_1 @"48c5e8"
#define kColorPart4_2 @"54e6d9"
#define kColorPart5_1 @"615edc"
#define kColorPart5_2 @"4994e2"

typedef void(^PDScorllGuideViewBlock)();

@interface PDScrollGuideView ()<UIScrollViewDelegate>
@property (nonatomic, strong) CAGradientLayer *gradientLayer;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIPageControl *pageControl;

@property (nonatomic, strong) NSArray *items;

@property (nonatomic, copy) PDScorllGuideViewBlock block;
@end

@implementation PDScrollGuideView

- (NSArray *)items {
    if (!_items) {
        NSMutableArray *tempArr = [NSMutableArray array];
        for (int i = 0; i < 5; i++) {
            PDAmusementItem *item = [PDAmusementItem new];
            switch (i) {
                case 0:
                {
                    item.image = [UIImage imageNamed:@"img_guide_1"];
                    item.mainTitle = @"赛事竞猜";
                    item.subTitle = @"每天20场热门赛事";
                    item.rangeArr = @[[NSValue valueWithRange:NSMakeRange(2, 2)]];
                    item.renderColor = [UIColor hexChangeFloat:kColorPart1_1];
                    break;
                }
                    
                case 1:
                {
                    item.image = [UIImage imageNamed:@"img_guide_2"];
                    item.mainTitle = @"英雄猜";
                    item.subTitle = @"每天千万金币大放送";
                    item.rangeArr = @[[NSValue valueWithRange:NSMakeRange(2, 2)]];
                    item.renderColor = [UIColor hexChangeFloat:kColorPart2_1];
                    break;
                }
                    
                case 2:
                {
                    item.image = [UIImage imageNamed:@"img_guide_3"];
                    item.mainTitle = @"打野大乱斗";
                    item.subTitle = @"30秒即赚百倍奖励";
                    item.rangeArr = @[[NSValue valueWithRange:NSMakeRange(0, 3)]];
                    item.renderColor = [UIColor hexChangeFloat:kColorPart3_1];
                    break;
                }
                    
                case 3:
                {
                    item.image = [UIImage imageNamed:@"img_guide_4"];
                    item.mainTitle = @"LOL大乱斗";
                    item.subTitle = @"卡牌对战胜者为王";
                    item.rangeArr = @[[NSValue valueWithRange:NSMakeRange(2, 2)]];
                    item.renderColor = [UIColor hexChangeFloat:kColorPart4_1];
                    break;
                }
                    
                case 4:
                {
                    item.image = [UIImage imageNamed:@"img_guide_5"];
                    item.mainTitle = @"福利商城";
                    item.subTitle = @"金币夺宝、实物兑换、玩竞猜赚现金";
                    item.rangeArr = @[[NSValue valueWithRange:NSMakeRange(5, 2)],[NSValue valueWithRange:NSMakeRange(14, 2)]];
                    item.renderColor = [UIColor hexChangeFloat:kColorPart5_1];
                    break;
                }

            }
            
            [tempArr addObject:item];
        }
        _items = [tempArr copy];
    }
    return _items;
}

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:CGRectMake(0, 0, kGuideWidth, kGuidePartHeight1 + kGuidePartHeight2 + kGuidePartHeight3)];
    if (self) {
        self.layer.cornerRadius = 8;
        self.layer.masksToBounds = YES;
        [self gradualLayerSetting];
        [self meteorSetting];
        [self scrollViewSetting];
        [self imageSetting];
        [self waveSetting];
        [self textLabelSetting];
        [self pageSetting];
        [self buttonSetting];
    }
    return self;
}

/**
 * 渐变层设置
 */
- (void)gradualLayerSetting {
    CAGradientLayer *layer = [CAGradientLayer layer];
    layer.frame = CGRectMake(0, 0, kGuideWidth, kGuidePartHeight1);
    layer.colors = @[(id)[UIColor hexChangeFloat:kColorPart1_1].CGColor, (id)[UIColor hexChangeFloat:kColorPart1_2].CGColor];
    layer.locations = @[@(0.5)];
    layer.startPoint = CGPointMake(0, 0);
    layer.endPoint = CGPointMake(1, 1);
    [self.layer addSublayer:layer];
    
    self.gradientLayer = layer;
}

/** 流星图*/
- (void)meteorSetting {
    UIImageView *meteorImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_guide_meteor"]];
    meteorImageView.frame = CGRectMake(0, 0, kGuideWidth, kGuidePartHeight1);
    [self addSubview:meteorImageView];
}

/**
 * 图片滚动容器
 */
- (void)scrollViewSetting {
    self.scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, kGuideWidth, kGuidePartHeight1 + kGuidePartHeight2)];
    self.scrollView.contentSize = CGSizeMake(self.items.count * kGuideWidth, kGuidePartHeight1 + kGuidePartHeight2);
    self.scrollView.delegate = self;
    self.scrollView.pagingEnabled = YES;
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.bounces = NO;
    self.scrollView.layer.cornerRadius = 12;
    self.scrollView.layer.masksToBounds = YES;
    [self addSubview:self.scrollView];
}

/**
 * 图片设置
 */
- (void)imageSetting {
    for (int i = 0; i < self.items.count; i++) {
        PDAmusementItem *item = self.items[i];
        UIImage *image = item.image;
        int space = 25;
        CGSize size = CGSizeMake(kGuideWidth - space * 2, image.size.height / image.size.width * (kGuideWidth - space * 2));
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.frame = CGRectMake((kGuideWidth - size.width) / 2 + i * kGuideWidth, kGuidePartHeight1 - size.height, size.width, size.height);
        [self.scrollView addSubview:imageView];
    }
}

/** 波浪*/
- (void)waveSetting {
    UIImage *waveImage = [UIImage imageNamed:@"icon_guide_wave"];
    CGSize waveSize = CGSizeMake(kGuideWidth, waveImage.size.height / waveImage.size.width * kGuideWidth);
    UIImageView *waveImageView = [[UIImageView alloc] initWithImage:waveImage];
    waveImageView.frame = CGRectMake(0, kGuidePartHeight1 - waveSize.height, waveSize.width, waveSize.height);
    [self addSubview:waveImageView];
}

/**
 * 文字版款
 */
- (void)textLabelSetting {
    for (int i = 0; i < self.items.count; i++) {
        UIView *textView = [[UIView alloc] initWithFrame:CGRectMake(i * kGuideWidth, kGuidePartHeight1, kGuideWidth, kGuidePartHeight2)];
        textView.backgroundColor = [UIColor whiteColor];
        [self.scrollView addSubview:textView];
        
        PDAmusementItem *item = self.items[i];
        
        UILabel *label = [[UILabel alloc] init];
        label.font = [UIFont systemFontOfSize:18];
        label.text = item.mainTitle;
        label.textAlignment = NSTextAlignmentCenter;
        [textView addSubview:label];
        
        UILabel *subLabel = [[UILabel alloc] init];
        subLabel.font = [UIFont systemFontOfCustomeSize:12];
        subLabel.numberOfLines = 0;
        subLabel.textAlignment = NSTextAlignmentCenter;
        [textView addSubview:subLabel];
        NSMutableAttributedString *subAtt = [[NSMutableAttributedString alloc] initWithString:item.subTitle];
        for (NSValue *rangeValue in item.rangeArr) {
            NSRange r = rangeValue.rangeValue;
            [subAtt addAttribute:NSForegroundColorAttributeName value:item.renderColor range:NSMakeRange(r.location, r.length)];
        }
        subLabel.attributedText = [subAtt copy];
        
        CGSize textSize = [label.text sizeWithCalcFont:label.font];
        CGSize subSize = [subLabel.text sizeWithCalcFont:subLabel.font constrainedToSize:CGSizeMake(kGuideWidth, CGFLOAT_MAX)];
        CGFloat y = (kGuidePartHeight2 - 5 - textSize.height - subSize.height - 10) / 2;
        
        label.frame = CGRectMake((kGuideWidth - textSize.width) / 2, y, textSize.width, textSize.height);
        subLabel.frame = CGRectMake((kGuideWidth - subSize.width) / 2, CGRectGetMaxY(label.frame) + 10, subSize.width, subSize.height);
    }
}

/**
 * 分页控制器
 */
- (void)pageSetting {
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, kGuidePartHeight1 + kGuidePartHeight2 - 15, kGuideWidth, 5)];
    self.pageControl.numberOfPages = self.items.count;
    self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    self.pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    [self addSubview:self.pageControl];
}

/**
 * 底下按钮设置
 */
- (void)buttonSetting {
    UIView *buttonImageView = [[UIView alloc] initWithFrame:CGRectMake(0, kGuidePartHeight1 + kGuidePartHeight2, kGuideWidth, kGuidePartHeight3)];
    [self addSubview:buttonImageView];
    
    UITapGestureRecognizer *cancleTap = [[UITapGestureRecognizer alloc] init];
    [cancleTap addTarget:self action:@selector(didClickCancle)];
    [buttonImageView addGestureRecognizer:cancleTap];
    
    UIImage *cancleImage = [UIImage imageNamed:@"icon_guide_cancle"];
    CGSize imageSize = CGSizeMake(kGuidePartHeight3 / cancleImage.size.height * cancleImage.size.width, kGuidePartHeight3);
    UIImageView *cancleImageView = [[UIImageView alloc] initWithImage:cancleImage];
    cancleImageView.frame = CGRectMake((kGuideWidth - imageSize.width) / 2, 0, imageSize.width, imageSize.height);
    [buttonImageView addSubview:cancleImageView];
}

/**
 * 点击取消
 */
- (void)didClickCancle {
    if (self.block) {
        self.block();
    }
}

- (void)didClickCancleButtonWithComplication:(void (^)())complication {
    if (complication) {
        self.block = complication;
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    CGFloat offsetX = scrollView.contentOffset.x;
    
    int index = offsetX / kGuideWidth;
    self.pageControl.currentPage = index;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetX = scrollView.contentOffset.x;
    
    int index = offsetX / kGuideWidth;
    CGFloat x = offsetX - index * kGuideWidth;
    int targetIndex = index + 1;
    
    self.gradientLayer.colors = @[(id)[self colorWithColorStringFrom:[self colorGradientFromIndex:index location:0] to:[self colorGradientFromIndex:targetIndex location:0] offsetX:x].CGColor,(id)[self colorWithColorStringFrom:[self colorGradientFromIndex:index location:1] to:[self colorGradientFromIndex:targetIndex location:1] offsetX:x].CGColor];
}

- (CGFloat)colorPartFrom:(CGFloat)from to:(CGFloat)to offsetX:(CGFloat)x {
    return (from - (from - to) / kGuideWidth * x) / 255.;
}

- (NSArray *)getRGBFromColorString:(NSString *)colorString {
    NSString *cString = colorString;
    if ([colorString hasPrefix:@"0X"])
        cString = [colorString substringFromIndex:2];
    if ([colorString hasPrefix:@"#"])
        cString = [colorString substringFromIndex:1];
    if ([colorString length] < 6)
        return nil;
    
    unsigned int r, g, b;
    NSRange range;
    range.length = 2;
    
    range.location = 0;
    [[NSScanner scannerWithString:[cString substringWithRange:range]] scanHexInt:&r];
    
    range.location = 2;
    [[NSScanner scannerWithString:[cString substringWithRange:range]] scanHexInt:&g];
    
    range.location = 4;
    [[NSScanner scannerWithString:[cString substringWithRange:range]] scanHexInt:&b];
    
    return @[@(r),@(g),@(b)];
}

- (UIColor *)colorWithColorStringFrom:(NSString *)color1 to:(NSString *)color2 offsetX:(CGFloat)x {
    NSArray *rgb1 = [self getRGBFromColorString:color1];
    NSArray *rgb2 = [self getRGBFromColorString:color2];
    
    return [UIColor colorWithRed:[self colorPartFrom:[rgb1[0] floatValue] to:[rgb2[0] floatValue] offsetX:x] green:[self colorPartFrom:[rgb1[1] floatValue] to:[rgb2[1] floatValue] offsetX:x] blue:[self colorPartFrom:[rgb1[2] floatValue] to:[rgb2[2] floatValue] offsetX:x] alpha:1];
}

- (NSString *)colorGradientFromIndex:(NSUInteger)index location:(int)location {
    NSString *colorString;
    switch (index) {
        case 0:
            colorString = location == 0? kColorPart1_1 : kColorPart1_2;
            break;
        case 1:
            colorString = location == 0? kColorPart2_1 : kColorPart2_2;
            break;
        case 2:
            colorString = location == 0? kColorPart3_1 : kColorPart3_2;
            break;
        case 3:
            colorString = location == 0? kColorPart4_1 : kColorPart4_2;
            break;
        case 4:
            colorString = location == 0? kColorPart5_1 : kColorPart5_2;
            break;
    }
    
    return colorString;
}

@end
