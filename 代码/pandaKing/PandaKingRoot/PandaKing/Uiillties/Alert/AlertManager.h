//
//  AlertManager.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AlertLogoView.h"
#import "AlertShowType.h"
@interface AlertManager : NSObject

+(void)alertManagerShowType:(alertLogoType)alert;

@end
