//
//  PDAlertView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JCAlertView.h"
#import "PDChallengeHouseModel.h"               /**< 房间信息*/
#import "PDCouponView.h"
// 兑换竹子的model
#import "PDPropertyItem.h"
/// 红包
#import "PDPacketAlertView.h"
// 认证次数
#import "PDFindactivatedtimesSingleModel.h"
// 签到
#import "PDAlertSignInView.h"

typedef NS_ENUM(NSInteger,AlertType) {
    AlertTypeLogin_RegisterSuccess,             /**< 注册成功*/
};

typedef NS_ENUM(NSInteger,alertBackgroundType) {
    alertBackgroundTypeNormal,                          /**< 白色的*/
    alertBackgroundTypeBlack_Short,                     /**< 黑色短*/
    alertBackgroundTypeBlack_Long,                      /**< 黑色长*/
    alertBackgroundTypeBinding,                         /**< 绑定背景*/
};

typedef NS_ENUM(NSInteger,alertGenderType) {
    alertGenderTypeBoy = 1,
    alertGenderTypeGirl = 0,
    alertGenderTypeNormal = 2,
};


@interface PDAlertView : NSObject

+(instancetype)sharedAlertView;                                                               /**< 单例*/

@property (nonatomic,strong)NSMutableArray *alertArr;


-(void)showAlertWithType:(AlertType)type block:(void(^)())block;

-(void)showAlertWithTitle:(NSString *)title conten:(NSString *)content isClose:(BOOL)isClose btnArr:(NSArray *)btnArr buttonClick:(void(^)(NSInteger buttonIndex))actionBlock;

-(void)showError:(NSString *)error;




#pragma mark - 显示黑色的弹框
-(void)showBlackAlertWithAlertType:(alertBackgroundType)alertType title:(NSString *)title content:(NSString *)content isClose:(BOOL)isClose house:(PDChallengeHouseModel *)houseModel couponModel:(PDCouponSingleModel *)couponModel inputPlaceholader:(NSString *)placeholder isGender:(BOOL)isGender btnArr:(NSArray *)btnArr countDown:(BOOL)countDown countDownBlock:(void(^)())countDownBlock caipan:(BOOL)caipan btnCliock:(void(^)(NSInteger buttonIndex,NSString *inputInfo ))actionBlock;

#pragma mark - 选择男女的框
// 编辑
-(void)showGenderAlertWithEdit:(void (^)(alertGenderType))block;
-(void)showGenderAlertWithChoose:(void (^)(alertGenderType))block;

#pragma mark - 绑定
-(void)showAlertWithBindingCurrentAva:(NSString *)currentAva afterAva:(NSString *)afterAva WithBlock:(void(^)(NSInteger btnIndex))block;

#pragma mark - 弹出正常弹框
-(void)showNormalAlertTitle:(NSString *)title content:(NSString *)content isClose:(BOOL)isClose btnArr:(NSArray *)btnArr btnCliock:(void(^)(NSInteger buttonIndex,NSString *inputInfo ))actionBlock;

#pragma mark - Dismiss
-(void)dismissWithBlock:(void(^)())block;
-(void)dismissAllWithBlock:(void(^)())block;
@property (nonatomic,strong)NSTimer *countDownTimer;                    /**< 倒计时*/
-(void)stopTomer;                       // 停止计时器

/////////// 我的钱包
#pragma mark - 我的钱包

- (void)showAlertForWalletWithTitle:(NSString *)title cancleEnable:(BOOL)en exchangeModelArr:(NSArray <PDPropertyItem *> *)itemArr selBlock:(void(^)(NSInteger index, PDPropertyItem *item))selBlock;

#pragma mark - 主页引导图
- (void)showScrollGuideViewWithDismissComplication:(void(^)())complication;

#pragma mark - 红包弹框
/** type 返回点击的类型:open,other*/
- (void)showPacketAlertViewWithModel:(PDPacketCheckInfo *)model didClickWithComplication:(void(^)(NSString *packetId, NSString *type))complication;

#pragma mark - 显示被认证5次
-(void)showAlertBeirenzhengWithModel:(PDFindactivatedtimesSingleModel *)singleModel btnArr:(NSArray *)btnArr btnClick:(void(^)(NSInteger buttonIndex))actionBlock;


#pragma mark - 签到
- (void)showSignAlertWithDrawComplication:(void(^)())complication;

@end
