//
//  AlertWindowView.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AlertWindowView.h"
#import "AlertLogoView.h"

#define kAlert_Margin LCFloat(15)
#define kLogo_Size LCFloat(50)
@interface AlertWindowView()
@property (nonatomic,strong)PDImageView *bgImageView;           /**< 背景*/
@property (nonatomic,strong)AlertLogoView *alertLogoView;       /**< logo*/
@property (nonatomic,strong)UIView *detailMainView;

@property (nonatomic,assign)ArrangeType arrangeType;            /**< 排列类型*/
@property (nonatomic,strong)NSArray *buttonArr;
@end

@implementation AlertWindowView

-(instancetype)initWithLogoType:(alertLogoType)type arrangeType:(ArrangeType)arrangeType buttonArray:(NSArray *)buttonArray  buttonClick:(void(^)(NSInteger buttonIndex))block{
    self = [super initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(30), LCFloat(400))];
    if (self){
        self.backgroundColor = [UIColor redColor];
        _arrangeType = arrangeType;
        _buttonArr = buttonArray;
        [self createViewWithLogoType:type arrangeType:arrangeType buttonArray:buttonArray buttonClick:^(NSInteger buttonIndex) {
            block(buttonIndex);
        }];
    }
    return self;
}

#pragma mark - createView
-(void)createViewWithLogoType:(alertLogoType)type arrangeType:(ArrangeType)arrangeType buttonArray:(NSArray *)buttonArray  buttonClick:(void(^)(NSInteger buttonIndex))block{
    // 1. 创建背景
    
    self.bgImageView = [[PDImageView alloc]init];
    self.bgImageView.backgroundColor = [UIColor clearColor];
    self.bgImageView.frame = CGRectMake(self.frame.origin.x, kLogo_Size, self.frame.size.width, self.size_height - kLogo_Size);
    self.bgImageView.image = [Tool stretchImageWithName:@"AlertBgView"];
    [self addSubview:self.bgImageView];
    
    // 2. 创建logoView
    self.alertLogoView = [[AlertLogoView alloc]initWithFrame:CGRectMake((self.bounds.size.width - LCFloat(50)) / 2. + self.frame.origin.x, 0, LCFloat(50), LCFloat(50)) logoType:type];
    self.alertLogoView.orgin_x = (self.bounds.size.width - LCFloat(50)) / 2.;
    self.alertLogoView.backgroundColor = [UIColor blackColor];
    [self addSubview:self.alertLogoView];
    
    // 2.1 创建View
    self.detailMainView = [[UIView alloc]init];
    self.detailMainView.backgroundColor = [UIColor greenColor];
    self.detailMainView.frame = CGRectMake(0, CGRectGetMaxY(self.alertLogoView.frame), self.bounds.size.width, LCFloat(150));
    [self addSubview:self.detailMainView];
    
    // 3. 创建line
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(kAlert_Margin, CGRectGetMaxY(self.detailMainView.frame), self.bounds.size.width - 2 * kAlert_Margin, .5f);
    [self.bgImageView addSubview:lineView];
    
    // 3. 创建按钮
    CGFloat button_Size_width = (self.bounds.size.width - (buttonArray.count + 1) * kAlert_Margin) / buttonArray.count;
    for (int i = 0 ; i < buttonArray.count ; i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor clearColor];
        [button setTitle:[buttonArray objectAtIndex:i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
        button.layer.cornerRadius = LCFloat(5);
        button.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
        button.layer.borderWidth = .5f;
        [button buttonWithBlock:^(UIButton *button) {
            if (block){
                block(i);
            }
        }];
        
        CGFloat origin_x = 0;
        CGFloat origin_y = 0;
        
        if (arrangeType == ArrangeTypeTransverse){                      // 【横向】
            origin_x = kAlert_Margin + i * (button_Size_width + kAlert_Margin);
            origin_y = CGRectGetMaxY(lineView.frame) + LCFloat(5);
            button.frame = CGRectMake(origin_x, origin_y, button_Size_width, LCFloat(35));
        } else if (arrangeType == ArrangeTypeLongitudinally){           // 【纵向】
            origin_x = kAlert_Margin;
            origin_y = CGRectGetMaxY(lineView.frame) + LCFloat(10) + (LCFloat(35) + LCFloat(10)) * i;
            button.frame = CGRectMake(origin_x, origin_y, self.bounds.size.width - 2 * kAlert_Margin, LCFloat(35));
        }

        [self.bgImageView addSubview:button];
    }
    [self calculationHeight];
    
}


#pragma mark - calculationHeight
- (void)calculationHeight{
    CGFloat viewHeight = 0;
    if (self.arrangeType == ArrangeTypeLongitudinally){     // 【纵向】
        viewHeight += CGRectGetMaxY(self.detailMainView.frame);
        viewHeight += kAlert_Margin * 2;
        viewHeight += LCFloat(35);
    } else if (self.arrangeType == ArrangeTypeTransverse){      // 【横向】
        viewHeight += CGRectGetMaxY(self.detailMainView.frame);
        viewHeight += kAlert_Margin;
        viewHeight +=  (LCFloat(35) + LCFloat(10)) * self.buttonArr.count;
    }
    self.bgImageView.size_height = viewHeight;
    self.size_height = viewHeight;
}


@end
