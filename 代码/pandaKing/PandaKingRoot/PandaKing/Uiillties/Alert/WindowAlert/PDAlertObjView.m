//
//  PDAlertObjView.m
//  PandaKing
//
//  Created by Cranz on 17/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAlertObjView.h"

@interface PDAlertObjView ()
@property (nonatomic, strong, readwrite) UIView *customView;
@property (nonatomic, strong) UITapGestureRecognizer *tap;
@property (nonatomic) BOOL isAddKeyboardNotification;
@end

@implementation PDAlertObjView

+ (instancetype)alertWithCustomView:(UIView *)customView {
    return [[self alloc] initWithCustomView:customView];
}

- (instancetype)initWithCustomView:(UIView *)customView {
    self = [super initWithFrame:kScreenBounds];
    if (self) {
        self.customView = customView;
        self.customView.frame = CGRectMake((self.frame.size.width - self.customView.frame.size.width) / 2, (self.frame.size.height - self.customView.frame.size.height) / 2, self.customView.frame.size.width, self.customView.frame.size.height);
        self.customView.transform = CGAffineTransformMakeScale(0, 0);
        self.dismissOnTap = YES;
        
        self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickTap)];
        [self addGestureRecognizer:self.tap];
        
        [self addSubview:self.customView];
    }
    return self;
}

- (void)showAlertComplication:(PDAlertP0Block)complication {
    NSEnumerator *windowEnnumtor = [UIApplication sharedApplication].windows.reverseObjectEnumerator;
    for (UIWindow *window in windowEnnumtor) {
        BOOL isOnMainScreen = window.screen == [UIScreen mainScreen];
        BOOL isVisible      = !window.hidden && window.alpha > 0;
        BOOL isLevelNormal  = window.windowLevel == UIWindowLevelNormal;
        
        if (isOnMainScreen && isVisible && isLevelNormal) {
            if (self.superview) {
                [self removeFromSuperview];
            }
            [window addSubview:self];
        }
    }
    
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.5];
        self.customView.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
        if (finished && complication) {
            complication();
        }
    }];
}

- (void)dismissAlertComplication:(PDAlertP0Block)complication {
    if (self.isAddKeyboardNotification) {
        [self endEditing:YES];
        [self removeKeyboardNotification];
    }
    [UIView animateWithDuration:0.3 animations:^{
        self.customView.transform = CGAffineTransformMakeScale(0.01, 0.01);
        self.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        if (finished) {
            [self removeFromSuperview];
            if (complication) {
                [self endEditing:YES];
                complication();
            }
        }
    }];
}

- (void)didClickTap {
    [self dismissAlertComplication:nil];
}

- (void)setDismissOnTap:(BOOL)dismissOnTap {
    self.tap.enabled = dismissOnTap;
}

#pragma mark - 键盘监听

- (void)addKeyboardNotification {
    self.isAddKeyboardNotification = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:kKEYBOARD_SHOW object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:kKEYBOARD_HIDE object:nil];
}

- (void)removeKeyboardNotification {
    self.isAddKeyboardNotification = NO;
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKEYBOARD_HIDE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKEYBOARD_SHOW object:nil];
}

- (void)keyboardShow:(NSNotification *)noti {
    NSTimeInterval duration = [[noti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [[noti.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];

    // 重叠的长度
    CGFloat over = (CGRectGetMaxY(self.customView.frame) + rect.size.height) - kScreenBounds.size.height;
    if (over >= 0) { // 重叠了
        [UIView animateWithDuration:duration animations:^{
            self.customView.frame = CGRectOffset(self.customView.frame, 0, -(over + 10));
        }];
    }
}

- (void)keyboardHide:(NSNotification *)noti {
    NSTimeInterval duration = [[noti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    CGRect rect = [[noti.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat over = (CGRectGetMaxY(self.customView.frame) + rect.size.height) - kScreenBounds.size.height;
    
    if (over >= 0) {
        [UIView animateWithDuration:duration animations:^{
            self.customView.frame = CGRectOffset(self.customView.frame, 0, (over + 10));
        }];
    }
}

@end
