//
//  PDAlertObjView.h
//  PandaKing
//
//  Created by Cranz on 17/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^PDAlertP0Block)();

@interface PDAlertObjView : UIView
@property (nonatomic, strong, readonly) UIView *customView;
/**
 * 默认YES
 */
@property (nonatomic, assign) BOOL dismissOnTap;

+ (instancetype)alertWithCustomView:(UIView *)customView;
- (void)showAlertComplication:(PDAlertP0Block)complication;
- (void)dismissAlertComplication:(PDAlertP0Block)complication;

/**
 * 添加键盘监听，在消失的时候会自动去移除键盘监听
 */
- (void)addKeyboardNotification;
//- (void)removeKeyboardNotification;

@end
