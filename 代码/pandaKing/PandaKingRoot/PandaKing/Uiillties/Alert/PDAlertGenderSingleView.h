//
//  PDAlertGenderSingleView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDAlertGenderSingleView : UIView

-(instancetype)initWithFrame:(CGRect)frame withSex:(NSString *)sex block:(void(^)())block;

-(void)chooseStyle:(BOOL)isChoose;

@end
