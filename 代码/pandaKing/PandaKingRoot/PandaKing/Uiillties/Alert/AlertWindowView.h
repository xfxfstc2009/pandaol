//
//  AlertWindowView.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertShowType.h"

typedef NS_ENUM(NSInteger,ArrangeType) {
    ArrangeTypeTransverse,              /**< 横向*/
    ArrangeTypeLongitudinally,          /**< 纵向*/
};

@interface AlertWindowView : UIView

-(instancetype)initWithLogoType:(alertLogoType)type arrangeType:(ArrangeType)arrangeType buttonArray:(NSArray *)buttonArray  buttonClick:(void(^)(NSInteger buttonIndex))block;

@end
