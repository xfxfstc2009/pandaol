//
//  PDAlertSignInView.m
//  PandaKing
//
//  Created by Cranz on 17/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAlertSignInView.h"

@interface PDAlertSignInView ()
@property (nonatomic, strong) UIImageView *signinImageView;
@property (nonatomic, strong) UIButton *drawButton;
@property (nonatomic, strong) UIImageView *cancleImageView;
@property (nonatomic, strong) UIButton *cancleButton;

@property (nonatomic, strong) UIImage *signinImage;
@property (nonatomic, strong) UIImage *drawImage;
@property (nonatomic, strong) UIImage *cancleImgae;

@property (nonatomic, copy) void(^drawBlock)();
@property (nonatomic, copy) void(^cancleBlock)();
@end

@implementation PDAlertSignInView

- (instancetype)initWithFrame:(CGRect)frame {
    UIImage *signinImage = [UIImage imageNamed:@"icon_signin_scuss"];
    UIImage *drawImage = [UIImage imageNamed:@"icon_signin_draw"];
    UIImage *cancleImgae = [UIImage imageNamed:@"icon_guide_cancle"];
    
    self = [super  initWithFrame:CGRectMake(0, 0, signinImage.size.width, signinImage.size.height + cancleImgae.size.height)];
    if (self) {
        self.signinImage = signinImage;
        self.drawImage = drawImage;
        self.cancleImgae = cancleImgae;
        [self setupView];
    }
    return self;
}

- (void)setupView {
    self.signinImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.signinImage.size.width, self.signinImage.size.height)];
    self.signinImageView.image = self.signinImage;
    [self addSubview:self.signinImageView];
    
    self.cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cancleButton setImage:self.cancleImgae forState:UIControlStateNormal];
    self.cancleButton.frame = CGRectMake(0, CGRectGetMaxY(self.signinImageView.frame) - 8, self.signinImage.size.width, self.cancleImgae.size.height);
    [self addSubview:self.cancleButton];
    [self.cancleButton addTarget:self action:@selector(didClickCancleButton) forControlEvents:UIControlEventTouchUpInside];
    
    self.drawButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.drawButton setBackgroundImage:self.drawImage forState:UIControlStateNormal];
    self.drawButton.frame = CGRectMake((self.signinImage.size.width - self.drawImage.size.width) / 2, self.signinImage.size.height - 30 - self.drawImage.size.height, self.drawImage.size.width, self.drawImage.size.height);
    [self addSubview:self.drawButton];
    [self.drawButton addTarget:self action:@selector(didClickDrawButton) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didClickCancleButton {
    if (self.cancleBlock) {
        self.cancleBlock();
    }
}

- (void)didClickDrawButton {
    if (self.drawBlock) {
        self.drawBlock();
    }
}

- (void)didClickDraw:(void(^)())drawBlock {
    if (drawBlock) {
        self.drawBlock = drawBlock;
    }
}

- (void)didClickCancle:(void(^)())cacleBlock {
    if (cacleBlock) {
        self.cancleBlock = cacleBlock;
    }
}

@end
