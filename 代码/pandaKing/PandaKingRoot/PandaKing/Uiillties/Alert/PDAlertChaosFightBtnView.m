//
//  PDAlertChaosFightBtnView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAlertChaosFightBtnView.h"

@interface PDAlertChaosFightBtnView()
@property (nonatomic,strong)UIButton *btn;

@end

@implementation PDAlertChaosFightBtnView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.btn = [[UIButton alloc]init];
    self.btn.backgroundColor = [UIColor clearColor];
    self.btn.frame = CGRectMake(0, 0, 103, 30);
    [self addSubview:self.btn];
}

-(void)setTransferType:(PDAlertChaosFightBtnViewType)transferType{
    if (transferType == PDAlertChaosFightBtnViewType1){
        [self.btn setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment1_nor"] forState:UIControlStateNormal];
    } else if (transferType == PDAlertChaosFightBtnViewType2){
        [self.btn setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment2_nor"] forState:UIControlStateNormal];
    }
}

-(void)setIsSelected:(BOOL)isSelected{
    _isSelected = isSelected;
    if (self.transferType == PDAlertChaosFightBtnViewType1){
        if (self.isSelected){
            [self.btn setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment1_hlt"] forState:UIControlStateNormal];
        } else {
            [self.btn setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment1_nor"] forState:UIControlStateNormal];
        }
    } else if (self.transferType == PDAlertChaosFightBtnViewType2){
        if (self.isSelected){
            [self.btn setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment2_hlt"] forState:UIControlStateNormal];
        } else {
            [self.btn setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment2_nor"] forState:UIControlStateNormal];
        }
    }
}


@end
