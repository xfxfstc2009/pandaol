//
//  AlertSuccessView.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertShowType.h"

@interface AlertSuccessView : UIView

-(instancetype)initWithFrame:(CGRect)frame logoType:(alertLogoType)type;

@end
