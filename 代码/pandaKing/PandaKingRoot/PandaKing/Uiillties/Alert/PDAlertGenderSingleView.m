//
//  PDAlertGenderSingleView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAlertGenderSingleView.h"
#import <objc/runtime.h>

static char setKey;

@interface PDAlertGenderSingleView()
@property (nonatomic,strong)PDImageView *chooseImgView;
@property (nonatomic,strong)UILabel *sexLabel;
@property (nonatomic,strong)UIButton *chooseButton;
@property (nonatomic,copy)NSString *sex;

@end

@implementation PDAlertGenderSingleView

-(instancetype)initWithFrame:(CGRect)frame withSex:(NSString *)sex block:(void(^)())block{
    self = [super initWithFrame:frame];
    if (self){
        objc_setAssociatedObject(self, &setKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        _sex = sex;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.chooseImgView = [[PDImageView alloc]init];
    self.chooseImgView.backgroundColor = [UIColor clearColor];
    if ([self.sex isEqualToString:@"男司机"]){
        [self.chooseImgView setImage:[UIImage imageNamed:@"gender_boy"]];
    } else {
        [self.chooseImgView setImage:[UIImage imageNamed:@"gender_girl"]];
    }
    self.chooseImgView.frame = CGRectMake((self.size_width - LCFloat(52)) / 2. , 0, LCFloat(52), LCFloat(52));
    [self addSubview:self.chooseImgView];
    
    // 2. 创建label
    self.sexLabel = [[UILabel alloc]init];
    self.sexLabel.backgroundColor = [UIColor clearColor];
    self.sexLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    if ([self.sex isEqualToString:@"男司机"]){
        self.sexLabel.textColor = [UIColor colorWithCustomerName:@"浅蓝"];
    } else {
        self.sexLabel.textColor = [UIColor colorWithCustomerName:@"粉"];
    }
    self.sexLabel.textAlignment = NSTextAlignmentCenter;
    self.sexLabel.text = self.sex;
    self.sexLabel.frame = CGRectMake(0, CGRectGetMaxY(self.chooseImgView.frame) + LCFloat(10), self.size_width , [NSString contentofHeightWithFont:self.sexLabel.font]);
    [self addSubview:self.sexLabel];
    
    // 3. 创建按钮
    self.chooseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.chooseButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [weakSelf.chooseButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &setKey);
        if (block){
            block();
        }
    }];
    self.size_height = CGRectGetMaxY(self.sexLabel.frame);
    self.chooseButton.frame = self.bounds;
    [self addSubview:self.chooseButton];
}


-(void)chooseStyle:(BOOL)isChoose{
    if (isChoose){                          // 选中
        self.chooseImgView.image = [UIImage imageNamed:@"jjr_ic_xuanze_1"];
    } else {
        self.chooseImgView.image = [UIImage imageNamed:@"jjr_ic_xuanze_2"];
    }
}


@end
