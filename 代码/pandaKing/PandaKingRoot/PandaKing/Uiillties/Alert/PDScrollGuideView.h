//
//  PDScrollGuideView.h
//  CCZScrollGuideView
//
//  Created by 金峰 on 2017/3/17.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 主页导航图
@interface PDScrollGuideView : UIView
- (void)didClickCancleButtonWithComplication:(void(^)())complication;
@end
