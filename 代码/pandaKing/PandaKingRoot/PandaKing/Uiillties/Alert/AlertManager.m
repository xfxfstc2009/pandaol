//
//  AlertManager.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AlertManager.h"
#import "AlertWindowView.h"
#import "JCAlertView.h"
#import "AlertLogoView.h"

@class AlertLogoView;
@implementation AlertManager

#pragma mark - 显示一个成功的弹框
+(void)alertManagerShowType:(alertLogoType)alert{
    NSLog(@"%.2f",kScreenBounds.size.width - 2 * LCFloat(30));
        AlertWindowView *alertView = [[AlertWindowView alloc]initWithLogoType:alertLogoTypeSuccess arrangeType:ArrangeTypeLongitudinally buttonArray:@[@"1",@"2",@"3",@"1",@"2",@"3"] buttonClick:^(NSInteger buttonIndex) {
            NSLog(@"%li",(long)buttonIndex);
        }];
    JCAlertView *alertManager = [[JCAlertView alloc]initWithCustomView:alertView dismissWhenTouchedBackground:YES];
    [alertManager show];
}


@end
