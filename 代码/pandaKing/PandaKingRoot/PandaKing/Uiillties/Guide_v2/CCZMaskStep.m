//
//  CCZMaskStep.m
//  新手指引
//
//  Created by Cranz on 16/12/23.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import "CCZMaskStep.h"

@interface CCZMaskStep ()
@property (nonatomic, strong, readwrite) UIBezierPath *path;
@end

@implementation CCZMaskStep

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.style = CCZMaskTextStyleAdaptMargins;
    self.type = CCZMaskStepGestureRecognizerTypeNormal;
    return self;
}

- (UIBezierPath *)path {
    CGRect screenBounds = [UIScreen mainScreen].bounds;
    UIBezierPath *path = [UIBezierPath bezierPathWithRect:screenBounds];
    [path appendPath:[[UIBezierPath bezierPathWithRoundedRect:self.rect cornerRadius:self.radius] bezierPathByReversingPath]];
    return path;
}

- (UIBezierPath *)subPath {
    return [[UIBezierPath bezierPathWithRoundedRect:self.rect cornerRadius:self.radius] bezierPathByReversingPath];
}

@end
