//
//  CCZMaskView.h
//  新手指引
//
//  Created by Cranz on 16/12/23.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCZMaskStep.h"

NS_ASSUME_NONNULL_BEGIN
@interface CCZMaskView : UIView

@property (nonatomic, assign) BOOL shadowAvailable; // 默认NO
@property (nonatomic, assign) BOOL removedWhenFinishGuide; // 默认YES，否则重置后从第一步开始
@property (nonatomic, assign) BOOL isAlreadyShowed; // 已经show，防止重复

/// steps
- (void)addSteps:(NSArray<CCZMaskStep *> *)steps;

/// show
// 开始 从第零步开始
- (void)showMask;
// 指定从第几步开始
- (void)showMaskAtIndex:(NSUInteger)index;
// 消失
- (void)dismissMask;

/// next, isFinished是最后一步结束的时候会返回YES，此时是-1步
- (void)nextComplicationHandle:(void(^)(NSUInteger index,
                                        BOOL isFinished,
                                        __kindof UIGestureRecognizer *gestureRecognizer))handle
     dismissComplicationHandle:( void(^ _Nullable )())dismissHandle;

/// 某些特殊的手势：比如pan会有这个回调
- (void)valueForGestureRecognizer:(void(^ _Nullable)(NSUInteger index, NSValue *grv))valueHandle;

/// 添加subView
- (void)addSubGuideView:(UIView *)subview;

@end
NS_ASSUME_NONNULL_END