//
//  CCZMaskStep.h
//  新手指引
//
//  Created by Cranz on 16/12/23.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, CCZMaskStepGestureRecognizerType) {
    CCZMaskStepGestureRecognizerTypeNormal,
    CCZMaskStepGestureRecognizerTypeTap = CCZMaskStepGestureRecognizerTypeNormal,
    CCZMaskStepkGestureRecognizerTypeSwipe,
    CCZMaskStepGestureRecognizerTypePan,
    CCZMaskStepGestureRecognizerTypeLongPress
};

typedef NS_ENUM(NSUInteger, CCZMaskTextStyle) {
    CCZMaskTextStyleAdaptRect,
    CCZMaskTextStyleAdaptMargins
};

NS_ASSUME_NONNULL_BEGIN
/// 每一步的具体显示
@interface CCZMaskStep : NSObject
@property (nonatomic, assign) CCZMaskStepGestureRecognizerType type; // 默认normal
@property (nonatomic, assign) CCZMaskTextStyle style; // 默认CCZMaskTextStyleAdaptRect
@property (nonatomic, assign) CGRect rect;
@property (nonatomic, assign) CGFloat radius;
@property (nonatomic, copy, nullable)   NSString *text;
@property (nonatomic, strong, readonly) UIBezierPath *path;
@property (nonatomic, strong, readonly) UIBezierPath *subPath;
@end
NS_ASSUME_NONNULL_END