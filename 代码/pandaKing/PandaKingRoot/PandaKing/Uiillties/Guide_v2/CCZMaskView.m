//
//  CCZMaskView.m
//  新手指引
//
//  Created by Cranz on 16/12/23.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import "CCZMaskView.h"

/// 记录rect的中心位置
typedef NS_OPTIONS(NSUInteger, CCZMaskRectPosition) {
    CCZMaskRectPositionLeft        = 1 << 0,
    CCZMaskRectPositionRight       = 1 << 1,
    CCZMaskRectPositionTop         = 1 << 2,
    CCZMaskRectPositionBottom      = 1 << 3,
    CCZMaskRectPositionCenter      = 0UL
};

static const NSTimeInterval showDuration = 0.3;
static const NSTimeInterval dismissDuration = 0.1;
static const CGFloat showAlpha = 0.8;

#define kSCREENBOUNDS [[UIScreen mainScreen] bounds]

typedef void(^CCZMaskComplcaitonHandle)(NSUInteger, BOOL, __kindof UIGestureRecognizer * _Nonnull);
typedef void(^CCZMaskDismissHandle)();
typedef void(^CCZMaskValueHandle)(NSUInteger, NSValue *);
@interface CCZMaskView ()
// 步骤
@property (nonatomic, strong) NSArray *steps;
@property (nonatomic, assign) NSUInteger index;
// mask 路径
@property (nonatomic, strong) CAShapeLayer *shapeLayer;
// 显示形状的路径
//@property (nonatomic, strong) CAShapeLayer *subShapeLayer;
// 文案
@property (nonatomic, strong) UILabel *textLabel;

@property (nonatomic, copy)   CCZMaskComplcaitonHandle nextBlock;
@property (nonatomic, copy)   CCZMaskDismissHandle dismimssBlock;
@property (nonatomic, copy)   CCZMaskValueHandle valueBlock;
// 手势
@property (nonatomic, strong, readwrite) UIGestureRecognizer *currentGestureRecognizer;
@property (nonatomic, strong) UITapGestureRecognizer *tap;
@property (nonatomic, strong) UISwipeGestureRecognizer *swipe;
@property (nonatomic, strong) UIPanGestureRecognizer *pan;
@property (nonatomic, strong) UILongPressGestureRecognizer *longPress;


// subview,需要及时移除
@property (nonatomic, strong) UIView *subGuideView;
@end
@implementation CCZMaskView

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.font = [UIFont systemFontOfSize:14];
        _textLabel.textColor = [UIColor whiteColor];
        _textLabel.numberOfLines = 0;
        [self addSubview:_textLabel];
    }
    return _textLabel;
}

- (CAShapeLayer *)shapeLayer {
    if (!_shapeLayer) {
        _shapeLayer = [CAShapeLayer layer];
        _shapeLayer.lineWidth = 1;
        _shapeLayer.strokeColor = [UIColor clearColor].CGColor;
        _shapeLayer.fillColor = [UIColor blackColor].CGColor;
    }
    return _shapeLayer;
}

- (UITapGestureRecognizer *)tap {
    if (!_tap) {
        _tap = [[UITapGestureRecognizer alloc] init];
        [_tap addTarget:self action:@selector(didActionOnGestureRecognizer:)];
    }
    return _tap;
}

- (UISwipeGestureRecognizer *)swipe {
    if (!_swipe) {
        _swipe = [[UISwipeGestureRecognizer alloc] init];
        [_swipe addTarget:self action:@selector(didActionOnGestureRecognizer:)];
    }
    return _swipe;
}

- (UIPanGestureRecognizer *)pan {
    if (!_pan) {
        _pan = [[UIPanGestureRecognizer alloc] init];
        [_pan addTarget:self action:@selector(didActionOnGestureRecognizer:)];
    }
    return _pan;
}

- (UILongPressGestureRecognizer *)longPress {
    if (!_longPress) {
        _longPress = [[UILongPressGestureRecognizer alloc] init];
        [_longPress addTarget:self action:@selector(didActionOnGestureRecognizer:)];
    }
    return _longPress;
}

#pragma mark - initial

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    if (!self) {
        return nil;
    }
    
    [self pageSetting];
    return self;
}

- (void)pageSetting {
    self.backgroundColor = [UIColor clearColor];
    self.removedWhenFinishGuide = YES;
    self.isAlreadyShowed = NO;
}

#pragma mark - step

- (void)addSteps:(NSArray<CCZMaskStep *> *)steps {
    self.steps = steps;
}

#pragma mark - show

- (void)showMaskLayer {
    if (self.subGuideView) {
        [self.subGuideView removeFromSuperview];
    }
    
    if (self.index > self.steps.count - 1) {
        if (self.removedWhenFinishGuide) {
            self.nextBlock(-1, YES, self.currentGestureRecognizer);
            [self dismissMask];
            return;
        }
        self.index = 0; // 最后一个重置
    }
    
    CCZMaskStep *step = self.steps[self.index];
    [self addTextWithStep:step];
    [self addGestureRecognizerWithType:step.type];
    self.shapeLayer.path = step.path.CGPath;
    self.layer.mask = self.shapeLayer;
    
    if (self.nextBlock) {
        self.nextBlock(self.index, NO, self.currentGestureRecognizer);
    }

    self.index++;
}

- (void)showMask {
    [self showMaskAtIndex:0];
}

- (void)showMaskAtIndex:(NSUInteger)index {
    self.index = index;
    self.layer.mask = nil;
    self.textLabel.text = nil;
    self.isAlreadyShowed = YES;
    
    [[[UIApplication sharedApplication].delegate window] addSubview:self];
    
    [UIView animateWithDuration:showDuration animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:showAlpha];
    } completion:^(BOOL finished) {
        [self showMaskLayer];
    }];
}

- (void)dismissMask {
    [UIView animateWithDuration:dismissDuration animations:^{
        self.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        self.isAlreadyShowed = NO;
        !self.dismimssBlock ?: self.dismimssBlock();
    }];
}

#pragma mark - 添加文字

- (void)addTextWithStep:(CCZMaskStep *)step {

    CGRect rect = step.subPath.bounds;
    CCZMaskRectPosition position = [self rectPosition:rect];
    CGFloat spaceX; // 距离最近边的在x方向上的距离

    if (position == (CCZMaskRectPositionLeft | CCZMaskRectPositionTop)) { // 左上
        spaceX = rect.origin.x;
        [self textRectWithStep:step pathRect:rect spaceX:spaceX isUp:NO];
    } else if (position == CCZMaskRectPositionLeft) { // 左
        spaceX = rect.origin.x;
        [self textRectWithStep:step pathRect:rect spaceX:spaceX isUp:NO];
    } else if (position == (CCZMaskRectPositionLeft | CCZMaskRectPositionBottom)) { // 左下
        spaceX = rect.origin.x;
        [self textRectWithStep:step pathRect:rect spaceX:spaceX isUp:YES];
    } else if (position == CCZMaskRectPositionTop) { // 上
        spaceX = rect.origin.x;
        [self textRectWithStep:step pathRect:rect spaceX:spaceX isUp:NO];
    } else if (position == CCZMaskRectPositionBottom) { // 下
        spaceX = rect.origin.x;
        [self textRectWithStep:step pathRect:rect spaceX:spaceX isUp:YES];
    } else if (position == CCZMaskRectPositionCenter) { // 中心
        spaceX = rect.origin.x;
        [self textRectWithStep:step pathRect:rect spaceX:spaceX isUp:NO];
    } else if (position == (CCZMaskRectPositionRight | CCZMaskRectPositionTop)) { // 右上
        spaceX = kSCREENBOUNDS.size.width - (rect.origin.x + rect.size.width);
        [self textRectWithStep:step pathRect:rect spaceX:spaceX isUp:NO];
    } else if (position == (CCZMaskRectPositionRight)) { // 右
        spaceX = kSCREENBOUNDS.size.width - (rect.origin.x + rect.size.width);
        [self textRectWithStep:step pathRect:rect spaceX:spaceX isUp:NO];
    } else { // 右下
        spaceX = kSCREENBOUNDS.size.width - (rect.origin.x + rect.size.width);
        [self textRectWithStep:step pathRect:rect spaceX:spaceX isUp:YES];
    }
}

- (CCZMaskRectPosition)rectPosition:(CGRect)rect {
    CGSize size = rect.size;
    CGPoint center = CGPointMake(rect.origin.x + size.width / 2, rect.origin.y + size.height / 2);
    CGPoint boundsCenter = CGPointMake(kSCREENBOUNDS.size.width / 2, kSCREENBOUNDS.size.height / 2);
    if (center.x < boundsCenter.x && center.y < boundsCenter.y) {
        return CCZMaskRectPositionLeft | CCZMaskRectPositionTop;
    } else if (center.x < boundsCenter.x && center.y == boundsCenter.y) {
        return CCZMaskRectPositionLeft;
    } else if (center.x < boundsCenter.x && center.y > boundsCenter.y) {
        return CCZMaskRectPositionLeft | CCZMaskRectPositionBottom;
    } else if (center.x == boundsCenter.x && center.y < boundsCenter.y) {
        return CCZMaskRectPositionTop;
    } else if (center.x == boundsCenter.x && center.y > boundsCenter.y) {
        return CCZMaskRectPositionBottom;
    } else if (center.x == boundsCenter.x && center.y == boundsCenter.y) {
        return CCZMaskRectPositionCenter;
    } else if (center.x > boundsCenter.x && center.y < boundsCenter.y) {
        return CCZMaskRectPositionRight | CCZMaskRectPositionTop;
    } else if (center.x > boundsCenter.x && center.y == boundsCenter.y) {
        return CCZMaskRectPositionRight;
    } else {
        return CCZMaskRectPositionRight | CCZMaskRectPositionBottom;
    }
}

- (void)textRectWithStep:(CCZMaskStep *)step pathRect:(CGRect)rect spaceX:(CGFloat)space isUp:(BOOL)up {
    NSString *text = step.text;
    self.textLabel.text = text;
    
    CGSize textSize; // 文案的size
    CGFloat s = 6; // rect和文案之间的空隙
    CGFloat x = 11; // 当text自适应边距时的x
    CGFloat limitedWidth;
    CGPoint origin;
    
    if (step.style == CCZMaskTextStyleAdaptRect) {
        limitedWidth = space * 2 + rect.size.width; // label的限宽
        textSize = [text boundingRectWithSize:CGSizeMake(limitedWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.textLabel.font} context:nil].size;
        origin = CGPointMake(rect.origin.x + rect.size.width / 2 - textSize.width / 2, up? (rect.origin.y - s - textSize.height) : (s + rect.origin.y + rect.size.height));
    } else {
        limitedWidth = kSCREENBOUNDS.size.width - x * 2;
        textSize = [text boundingRectWithSize:CGSizeMake(limitedWidth, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.textLabel.font} context:nil].size;
        origin = CGPointMake(rect.origin.x < (kSCREENBOUNDS.size.width / 2)? x : kSCREENBOUNDS.size.width - x - textSize.width, up? (rect.origin.y - s - textSize.height) : (s + rect.origin.y + rect.size.height));
    }
    
    self.textLabel.frame = CGRectMake(origin.x, origin.y, textSize.width, textSize.height);
}

#pragma mark - 手势

- (void)addGestureRecognizerWithType:(CCZMaskStepGestureRecognizerType)type {

    !self.currentGestureRecognizer ?: [self removeGestureRecognizer:self.currentGestureRecognizer];
    
    if (type == CCZMaskStepGestureRecognizerTypeNormal) {
        self.tap.numberOfTapsRequired = 1;
        self.currentGestureRecognizer = self.tap;
    } else if (type == CCZMaskStepkGestureRecognizerTypeSwipe) {
        self.currentGestureRecognizer = self.swipe;
        self.swipe.direction = UISwipeGestureRecognizerDirectionRight | UISwipeGestureRecognizerDirectionLeft;
    } else if (type == CCZMaskStepGestureRecognizerTypePan) {
        self.currentGestureRecognizer = self.pan;
    } else if (type == CCZMaskStepGestureRecognizerTypeLongPress) {
        self.longPress.minimumPressDuration = 1.5;
        self.currentGestureRecognizer = self.longPress;
    }
    
    [self addGestureRecognizer:self.currentGestureRecognizer];
}

- (void)didActionOnGestureRecognizer:(UIGestureRecognizer *)gestureRecognizer {
    if ([gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]) {
        UIPanGestureRecognizer *pan = (UIPanGestureRecognizer *)gestureRecognizer;
        if (pan.state == UIGestureRecognizerStateEnded || pan.state == UIGestureRecognizerStateCancelled) {
            [self showMaskLayer];
        } else {
            !self.valueBlock ?: self.valueBlock(self.index, [NSValue valueWithCGPoint:[pan translationInView:self]]);
        }
    } else {
        [self showMaskLayer];
    }
}

- (void)valueForGestureRecognizer:(void (^)(NSUInteger, NSValue * _Nonnull))valueHandle {
    if (valueHandle) {
        self.valueBlock = valueHandle;
    }
}

#pragma mark - next

- (void)nextComplicationHandle:(void (^)(NSUInteger, BOOL, __kindof UIGestureRecognizer * _Nonnull))handle dismissComplicationHandle:(void (^)())dismissHandle {
    if (handle) {
        self.nextBlock = handle;
    }
    if (dismissHandle) {
        self.dismimssBlock = dismissHandle;
    }
}

#pragma mark - set

- (void)setShadowAvailable:(BOOL)shadowAvailable {
    _shadowAvailable = shadowAvailable;
    
    self.shapeLayer.shadowColor = [UIColor blackColor].CGColor;
    self.shapeLayer.shadowOpacity = 1;
    self.shapeLayer.shadowOffset = CGSizeMake(0, 0);
    self.shapeLayer.shadowRadius = 10;
}

#pragma mark - add

- (void)addSubGuideView:(UIView *)subview {
    [[[UIApplication sharedApplication].delegate window] addSubview:subview];
    self.subGuideView = subview;
}

@end
