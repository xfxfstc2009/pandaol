//
//  PDHUD.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDHUD.h"
#import "SMProgressHUD.h"
#import "WSProgressHUD.h"
@interface PDHUD()


@end

@implementation PDHUD


#pragma mark - Loading
+(void)showPDLoadingWithText:(NSString *)tip{
    [[SMProgressHUD shareInstancetype] showLoadingWithTip:@"123" delay:5];
}

#pragma mark - Text
+(void)showText:(NSString *)text{
    [WSProgressHUD showShimmeringString:text maskType:WSProgressHUDMaskTypeClear maskWithout:WSProgressHUDMaskWithoutDefault];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [WSProgressHUD dismiss];
    });
}

+(void)showConnectionErr:(NSString *)text{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.4 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [PDHUD showText:text];
    });
}

-(void)showText:(NSString *)text{
    [self showShimmeringString:text maskType:WSProgressHUDMaskTypeClear];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismiss];
    });
}


#pragma mark showProgress
+(void)showHUDProgress:(NSString *)text diary:(NSInteger)diary{
    [self showWithStatus:text];
    if(diary != 0){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(diary * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismiss];
        });
    }
}

+(void)showHUDBindingProgress:(NSString *)text diary:(NSInteger)diary{
    [self showWithStatus:text maskType:WSProgressHUDMaskTypeBlack maskWithout:WSProgressHUDMaskWithoutDefault];
    if(diary != 0){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(diary * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismiss];
        });
    }
}

+(void)showHUDBindingRoleDetailProgress:(NSString *)text diary:(NSInteger)diary{
    [self showWithStatus:text maskType:WSProgressHUDMaskTypeBlack maskWithout:WSProgressHUDMaskWithoutDefault];
    if(diary != 0){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(diary * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismiss];
        });
    }
}


+(void)showHUDSuccess:(NSString *)text{
    [self showSuccessWithStatus:text];
}

+(void)showHUDError:(NSString *)text{
    [self showErrorWithStatus:text];
}

#pragma mark - dismiss
+(void)dismissManager{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self dismiss];
    });
}
@end
