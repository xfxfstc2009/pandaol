//
//  PDQRTool.h
//  PandaKing
//
//  Created by Cranz on 16/8/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDQRTool : NSObject
/** 生成二维码图片，默认黑白二维码*/
+ (UIImage *)showQRCodeImageWithString:(NSString *)string;
+ (UIImage *)showQRCodeImageWithString:(NSString *)string onColor:(UIColor *)oncolor offColor:(UIColor *)offcolor;
@end
