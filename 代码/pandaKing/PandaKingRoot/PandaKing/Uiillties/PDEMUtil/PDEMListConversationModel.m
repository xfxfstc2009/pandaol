//
//  PDEMListConversationModel.m
//  PandaKing
//
//  Created by Cranz on 17/2/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDEMListConversationModel.h"

@implementation PDEMListConversationModel

- (instancetype)initWithConversation:(EMConversation *)conversation
{
    self = [super init];
    if (self) {
        _conversation = conversation;
        _title = _conversation.conversationId;
        if (conversation.type == EMConversationTypeChat) {
            _avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/user"];
        }
        else{
            _avatarImage = [UIImage imageNamed:@"EaseUIResource.bundle/group"];
        }
    }
    
    return self;
}

@end
