//
//  PDEMListViewController.h
//  IM
//
//  Created by 金峰 on 2017/2/4.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import "EaseConversationListViewController.h"

/// 消息列表控制器
@interface PDEMListViewController : EaseConversationListViewController<EaseConversationListViewControllerDataSource>

@end
