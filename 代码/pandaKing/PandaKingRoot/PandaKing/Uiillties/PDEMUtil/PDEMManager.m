//
//  PDEMManager.m
//  IM
//
//  Created by 金峰 on 2017/2/4.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import "PDEMManager.h"

@interface PDEMManager ()
@property (nonatomic, copy) PDEMErrorBlock autoLoginBlock;
@property (nonatomic, copy) PDEMExitBlock exitedBlock;
@property (nonatomic, copy) PDEMChatroomBlock roomInfoChangeBlock;
@property (nonatomic, copy) PDEMUnreadBlock unreadBlock;
@property (nonatomic, copy) PDEMDataArrBlock arrBlock;
@end

@implementation PDEMManager

+ (instancetype)sharedInstance {
    static PDEMManager *instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[PDEMManager alloc] init];
    });
    return instance;
}

#pragma mark - 初始化
- (void)initial {
    EMOptions *options = [EMOptions optionsWithAppkey:PDEM_APP_KEY];
    options.apnsCertName = PDEM_APP_CER;
    EMError *error = [[EMClient sharedClient] initializeSDKWithOptions:options];
    
    if (!error) {
        [EMClient sharedClient].pushOptions.displayStyle = EMPushDisplayStyleMessageSummary;
        [[EMClient sharedClient] addDelegate:self delegateQueue:nil];
        [[EMClient sharedClient].chatManager addDelegate:self delegateQueue:nil];
    }
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[EMClient sharedClient] applicationDidEnterBackground:application];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[EMClient sharedClient] applicationWillEnterForeground:application];
}

- (void)application:(id)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [[EMClient sharedClient] application:application didReceiveRemoteNotification:userInfo];
}

#pragma mark - 登陆登出
- (BOOL)isAutoLogin {
    return [EMClient sharedClient].options.isAutoLogin;
}

- (void)loginWithAccount:(NSString *)account password:(NSString *)password complication:(void (^)(EMError *))complication {
    [[EMClient sharedClient] loginWithUsername:account password:password completion:^(NSString *aUsername, EMError *aError) {
        if (!aError) {
            [[EMClient sharedClient].options setIsAutoLogin:YES];
        }
        if (complication) {
            complication(aError);
        }
    }];
}

- (void)autoLogin:(void (^)(EMError *))complication {
    if (complication) {
        self.autoLoginBlock = complication;
    }
}

- (void)logout:(void (^)(EMError *))complication {
    [[EMClient sharedClient] logout:YES completion:complication];
}

- (void)exitedByEvent:(void (^)(PDEMExitType))complication {
    if (complication) {
        self.exitedBlock = complication;
    }
}

- (NSString *)accountId {
    return [EMClient sharedClient].currentUsername;
}

#pragma mark - EMClientDelegate
- (void)autoLoginDidCompleteWithError:(EMError *)aError {
    if (self.autoLoginBlock) {
        self.autoLoginBlock(aError);
    }
}

- (void)userAccountDidLoginFromOtherDevice {
    if (self.exitedBlock) {
        self.exitedBlock(PDEMExitType1);
    }
}

- (void)userAccountDidRemoveFromServer {
    if (self.exitedBlock) {
        self.exitedBlock(PDEMExitType2);
    }
}

#pragma mark - 单聊
- (PDEMMessageViewController *)conversationWithOnePerson:(NSString *)personId {
    PDEMMessageViewController *messageViewController = [[PDEMMessageViewController alloc] initWithConversationChatter:personId conversationType:EMConversationTypeChat];
    
    return messageViewController;
}

#pragma mark - 群聊

#pragma mark - 聊天室

- (PDEMMessageViewController *)conversationWithChatroom:(NSString *)roomId {
    PDEMMessageViewController *messageViewController;
    messageViewController = [[PDEMMessageViewController alloc] initWithConversationChatter:roomId conversationType:EMConversationTypeChatRoom];
    [[EMClient sharedClient].roomManager addDelegate:self];
    return messageViewController;
}

- (void)exitChatroomWithChatroom:(NSString *)roomId {
    EMError *error;
    [[EMClient sharedClient].roomManager leaveChatroom:roomId error:&error];
    [[EMClient sharedClient].roomManager removeDelegate:self];
}

- (void)chatroomChangeInfo:(void (^)(EMChatroom *))complication {
    if (complication) {
        self.roomInfoChangeBlock = complication;
    }
}

#pragma mark - EMChatroomManagerDelegate

- (void)userDidJoinChatroom:(EMChatroom *)aChatroom user:(NSString *)aUsername {
    if (self.roomInfoChangeBlock) {
        self.roomInfoChangeBlock(aChatroom);
    }
}

- (void)userDidLeaveChatroom:(EMChatroom *)aChatroom user:(NSString *)aUsername {
    if (self.roomInfoChangeBlock) {
        self.roomInfoChangeBlock(aChatroom);
    }
}

#pragma mark - 聊天列表
- (PDEMListViewController *)makeOneListViewCoontroller {
    PDEMListViewController *listViewController = [[PDEMListViewController alloc] init];
    self.listViewController = listViewController;
    return listViewController;
}

- (void)needToUpdateConversationList:(PDEMDataArrBlock)complication {
    if (complication) {
        self.arrBlock = complication;
    }
}

#pragma mark - 黑名单
- (void)addOnePersonToBlackList:(NSString *)personId complication:(PDEMErrorBlock)complication {
    [[EMClient sharedClient].contactManager addUserToBlackList:personId completion:^(NSString *aUsername, EMError *aError) {
        if (complication) {
            complication(aError);
        }
    }];
}

- (void)deleteOnePersonFromBlackList:(NSString *)personId complication:(PDEMErrorBlock)complication {
    [[EMClient sharedClient].contactManager removeUserFromBlackList:personId completion:^(NSString *aUsername, EMError *aError) {
        if (complication) {
            complication(aError);
        }
    }];
}

- (BOOL)isMemberInBlackList:(NSString *)personId {
    NSArray *list = [[EMClient sharedClient].contactManager getBlackList];
    for (NSString *memberId in list) {
        if ([personId isEqualToString:memberId]) {
            return YES;
        }
    }
    return NO;
}

#pragma mark - EMChatManagerDelegate

- (void)conversationListDidUpdate:(NSArray *)aConversationList {
    if (self.arrBlock) {
        self.arrBlock(aConversationList);
    }
}

- (void)messagesDidReceive:(NSArray *)aMessages {
    /// 在收到消息药更新列表
    [self.listViewController tableViewDidTriggerHeaderRefresh];
    
    if (self.unreadBlock) {
        self.unreadBlock([self getNumber]);
    }
}

#pragma mark - 获取未读消息数

- (void)getUnreadNumber:(PDEMUnreadBlock)complication {
    if (complication) {
        self.unreadBlock = complication;
        self.unreadBlock([self getNumber]);
    }
}

- (NSUInteger)getNumber {
    NSUInteger n = 0;
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    for (EMConversation *conversation in conversations) {
        if (conversation.type != EMConversationTypeChat) {
            continue;
        }
        n += conversation.unreadMessagesCount;
    }
    return n;
}

@end
