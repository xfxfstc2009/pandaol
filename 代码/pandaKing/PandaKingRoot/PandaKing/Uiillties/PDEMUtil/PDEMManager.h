//
//  PDEMManager.h
//  IM
//
//  Created by 金峰 on 2017/2/4.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EMSDK.h"
#import "PDEMMessageViewController.h"
#import "PDEMListViewController.h"

// 测试环境用的线上的,正式环境用的测试的key；反一下
//#ifdef DEBUG
//#define PDEM_APP_KEY @"1122170103178884#pandaol"
//#define PDEM_APP_CER @"product"
//#else
#define PDEM_APP_KEY @"1122170103178884#ceshiapp"
#define PDEM_APP_CER @"sandbox"
//#endif

typedef NS_ENUM(NSUInteger, PDEMExitType) {
    /// 当前帐号在其他设备被登录
    PDEMExitType1,
    /// 当前帐号被删除
    PDEMExitType2,
};

typedef void(^PDEMErrorBlock)(EMError *error);
typedef void(^PDEMExitBlock)(PDEMExitType type);
typedef void(^PDEMChatroomBlock)(EMChatroom *room);
typedef void(^PDEMUnreadBlock)(NSUInteger num);
typedef void(^PDEMDataArrBlock)(NSArray *arr);

@interface PDEMManager : NSObject<EMChatroomManagerDelegate, EMClientDelegate, EMChatManagerDelegate>

+ (instancetype)sharedInstance;

#pragma mark - 初始化
/// 程序启动的时候进行初始化
- (void)initial;
- (void)applicationDidEnterBackground:(UIApplication *)application;
- (void)applicationWillEnterForeground:(UIApplication *)application;
- (void)application:(id)application didReceiveRemoteNotification:(NSDictionary *)userInfo;

#pragma mark - 登陆登出
@property (nonatomic, assign, readonly) BOOL isAutoLogin; /// 是否已经设置自动登录，如果YES，则不再需要调用登陆接口
@property (nonatomic, copy, readonly) NSString *accountId;

- (void)loginWithAccount:(NSString *)account
                password:(NSString *)password
            complication:(PDEMErrorBlock)complication;

/// 自动登录成功后的回调
- (void)autoLogin:(PDEMErrorBlock)complication;

/// 登出
- (void)logout:(PDEMErrorBlock)complication;

/// 被动退出时的回调
- (void)exitedByEvent:(PDEMExitBlock)complication;

#pragma mark - 单聊
- (PDEMMessageViewController *)conversationWithOnePerson:(NSString *)personId;

#pragma mark - 群聊

#pragma mark - 聊天室

/// 进入聊天室
- (PDEMMessageViewController *)conversationWithChatroom:(NSString *)roomId;

/// 退出聊天室
- (void)exitChatroomWithChatroom:(NSString *)roomId;

/// 获取聊天室详情
- (void)chatroomChangeInfo:(PDEMChatroomBlock)complication;

#pragma mark - 聊天列表
@property (nonatomic, strong) PDEMListViewController *listViewController;

- (PDEMListViewController *)makeOneListViewCoontroller;
- (void)needToUpdateConversationList:(PDEMDataArrBlock)complication;

#pragma mark - 黑名单
- (void)addOnePersonToBlackList:(NSString *)personId
                   complication:(PDEMErrorBlock)complication;
- (void)deleteOnePersonFromBlackList:(NSString *)personId
                        complication:(PDEMErrorBlock)complication;
- (BOOL)isMemberInBlackList:(NSString *)personId;

#pragma mark - 获取未读消息数

- (void)getUnreadNumber:(PDEMUnreadBlock)complication;

@end

