//
//  PDEMListViewController.m
//  IM
//
//  Created by 金峰 on 2017/2/4.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import "PDEMListViewController.h"
#import "PDEMMessageViewController.h"
#import "PDEMListConversationModel.h"
#import "PDEMUserList.h"
#import "PDZoneViewController.h"

@interface PDEMListViewController ()
@property (nonatomic, strong) PDEMUserList *list;
@end

@implementation PDEMListViewController

- (void)viewDidLoad {
    self.dataSource = self;
    [super viewDidLoad];
    
    __weak typeof(self) weakSelf = self;
    [[PDEMManager sharedInstance] needToUpdateConversationList:^(NSArray *arr) {
        NSMutableString *userIdString = [NSMutableString string];
        for (EMConversation *model in arr) {
            if (userIdString.length) {
                [userIdString appendFormat:@",%@",model.conversationId];
            } else {
                [userIdString appendString:model.conversationId];
            }
        }
        [weakSelf fetchUserInfoWithIds:[userIdString copy]];
        
    }];
    
    NSMutableString *userIdString = [NSMutableString string];
    for (PDEMListConversationModel *model in self.dataArray) {
        if (userIdString.length) {
            [userIdString appendFormat:@",%@",model.conversation.conversationId];
        } else {
            [userIdString appendString:model.conversation.conversationId];
        }
    }
    [self fetchUserInfoWithIds:[userIdString copy]];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    // 防止聊天室没退出主动退出
    NSArray *conversations = [[EMClient sharedClient].chatManager getAllConversations];
    for (EMConversation *conversation in conversations) {
        if (conversation.type == EMConversationTypeChatRoom) {
            [[PDEMManager sharedInstance] exitChatroomWithChatroom:conversation.conversationId];
        }
    }
    
    // 角标的更新
    PDZoneViewController *zoneViewController = (PDZoneViewController *)self.parentViewController;
    [[PDEMManager sharedInstance] getUnreadNumber:^(NSUInteger num) {
        zoneViewController.tabBarItem.badgeValue = num == 0? nil : [NSString stringWithFormat:@"%@",@(num)];
    }];
    
    [self tableViewDidTriggerHeaderRefresh];
}

#pragma mark - EaseConversationListViewControllerDataSource

- (NSString *)conversationListViewController:(EaseConversationListViewController *)conversationListViewController
       latestMessageTimeForConversationModel:(id<IConversationModel>)conversationModel {
    id<IConversationModel> model = conversationModel;
    return [NSDate getTimeGap:model.conversation.latestMessage.timestamp/1000];
}

- (id<IConversationModel>)conversationListViewController:(EaseConversationListViewController *)conversationListViewController
                                    modelForConversation:(EMConversation *)conversation {

//    if ([conversationListViewController isMemberOfClass:[PDEMListViewController class]] && conversation.type == EMConversationTypeChatRoom) {
//        [[PDEMManager sharedInstance] exitChatroomWithChatroom:conversation.conversationId];
//        return nil;
//    }
    PDEMListConversationModel *model = [[PDEMListConversationModel alloc] initWithConversation:conversation];
    model.avatarImage = [UIImage imageNamed:@"icon_nordata"];
    [self getModelWithId:model.conversation.conversationId conversationModel:&model];
    return model;
}

#pragma mark - 网络请求

- (void)fetchUserInfoWithIds:(NSArray *)ids {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:imUserInfo requestParams:@{@"imUserIds[]":ids} responseObjectClass:[PDEMUserList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        if (isSucceeded) {
            weakSelf.list = (PDEMUserList *)responseObject;
            [weakSelf tableViewDidTriggerHeaderRefresh];
        }
    }];
}

- (void)getModelWithId:(NSString *)conversationId conversationModel:(PDEMListConversationModel **)model {
    PDEMListConversationModel *conversationModel = *model;
    for (PDEMUserInfo *info in self.list.items) {
        if ([info.imUserId isEqualToString:conversationId]) {
            conversationModel.title = info.nickname;
            conversationModel.avatarURLPath = info.avatar;
        }
    }
}


#pragma mark - 无数据页

@end
