//
//  PDEMUserList.h
//  PandaKing
//
//  Created by Cranz on 17/2/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDEMUserInfo.h"

@interface PDEMUserList : FetchModel
@property (nonatomic, strong) NSArray<PDEMUserInfo> *items;
@end
