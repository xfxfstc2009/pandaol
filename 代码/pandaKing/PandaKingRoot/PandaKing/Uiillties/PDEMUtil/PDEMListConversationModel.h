//
//  PDEMListConversationModel.h
//  PandaKing
//
//  Created by Cranz on 17/2/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDEMListConversationModel : NSObject<IConversationModel>
/** @brief 会话对象 */
@property (strong, nonatomic, readonly) EMConversation *conversation;
/** @brief 会话的标题(主要用户UI显示) */
@property (strong, nonatomic) NSString *title;
/** @brief conversationId的头像url */
@property (strong, nonatomic) NSString *avatarURLPath;
/** @brief conversationId的头像 */
@property (strong, nonatomic) UIImage *avatarImage;


/*!
 @method
 @brief 初始化会话对象模型
 @param conversation    会话对象
 @return 会话对象模型
 */
- (instancetype)initWithConversation:(EMConversation *)conversation;
@end
