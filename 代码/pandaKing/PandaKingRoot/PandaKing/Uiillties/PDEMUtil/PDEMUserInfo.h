//
//  PDEMUserInfo.h
//  PandaKing
//
//  Created by Cranz on 17/2/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDEMUserInfo <NSObject>

@end

@interface PDEMUserInfo : FetchModel
@property (nonatomic, copy) NSString *imUserId;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *memberId;
@end
