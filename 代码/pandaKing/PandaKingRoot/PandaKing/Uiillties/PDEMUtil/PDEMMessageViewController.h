//
//  PDEMMessageViewController.h
//  IM
//
//  Created by 金峰 on 2017/2/4.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import "EaseMessageViewController.h"

/// 消息控制器
@interface PDEMMessageViewController : EaseMessageViewController<EaseMessageViewControllerDataSource, EaseMessageViewControllerDelegate>
@property (nonatomic, copy)   NSString *targetId; /// 目标id
@property (nonatomic, strong) EMChatroom *transferRoom; /// 传递的聊天室
@property (nonatomic, strong) EMGroup *transferGroup; /// 传递的群
@end
