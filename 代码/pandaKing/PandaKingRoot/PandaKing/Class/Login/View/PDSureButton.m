//
//  PDButton.m
//  PandaChallenge
//
//  Created by Bluedaquiri on 16/3/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSureButton.h"

@implementation PDSureButton
- (instancetype)initWithTitle:(NSString *)title WithActionBlock:(void(^)())actionBlock {
    if (self = [super init]) {
        self.layer.cornerRadius = self.cornerRadius?self.cornerRadius : 5.;
        [self setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
        [self setTitle:title forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:15];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        __weak typeof(self) weakSelf = self;
        [self buttonWithBlock:^(UIButton *button) {
            if (!weakSelf) return;
            if (actionBlock) {
                actionBlock();
            }
        }];
    }
    return self;
}

- (void)setIsEnable:(BOOL)isEnable {
    _isEnable = isEnable;
    self.enabled = isEnable;
    if (!isEnable) {
        self.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    }else{
        self.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    }
}

@end
