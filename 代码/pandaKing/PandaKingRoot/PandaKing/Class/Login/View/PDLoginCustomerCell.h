//
//  PDLoginCustomerCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLoginEnumHeader.h"
#import "PDTimerButton.h"

@interface PDLoginCustomerCell : UITableViewCell

@property (nonatomic,strong)PDImageView *leftView;
@property (nonatomic,strong)UITextField *inputTextField;
@property (nonatomic,strong)PDTimerButton *timerButton;

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier accountType:(AccountCellType)accountType timerType:(PDTimerType)timerType timerButtonAction:(void(^)())buttonAction;

@end
