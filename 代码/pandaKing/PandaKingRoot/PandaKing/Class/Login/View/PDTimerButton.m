//
//  PDTimerButton.m
//  PandaChallenge
//
//  Created by Bluedaquiri on 16/3/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTimerButton.h"
#define ButtonCount 10
@interface PDTimerButton ()
@property (nonatomic,assign)NSInteger seconds;
@property (nonatomic,assign)PDTimerType timerType;
@end
@implementation PDTimerButton

- (instancetype)initWithTimerType:(PDTimerType)type actionBlock:(void(^)())actionBlock {
    if (self = [super init]) {
        /**< 按钮的配置*/
        self.layer.cornerRadius = self.cornerRadius?self.cornerRadius : 5.;
        [self setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
        [self setTitle:@"验证码" forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
        self.timerType = type;
        NSLog(@"%ld",(long)self.timerType);
        /**< 判断是否初始化定时器*/
        self.isTimeOut = [self isTimeOut];
        if (!self.isTimeOut) {
            NSInteger currentSeconds = ButtonCount - [self getGapInterval];
            [self startTimer:currentSeconds];
        }
        __weak typeof(self) weakSelf = self;
        [self buttonWithBlock:^(UIButton *button) {
            if (!weakSelf) return;
            __strong typeof(weakSelf) strongSelf = weakSelf;
            [strongSelf saveIntervalToUserDefault];
            [strongSelf startTimer:ButtonCount];
            if (actionBlock) {
                actionBlock();
            }
        }];
    }
    return self;
}

#pragma mark - 判断是否超过60s
/**< 1.初次进入与每次60s结束取值为空或者超过60s,返回timeOut 2.判断超过60s返回timeOut*/
- (BOOL)isTimeOut {
    if ([Tool isEmpty:[self getIntervalFromUserDefault]]) {
        return YES;
    } else {
        NSInteger gap = [self getGapInterval];
        if (gap >= ButtonCount) {
            return YES;
        } else {
            return NO;
        }
    }
}

/**< 计算当前时间戳与存储时间戳的差值*/
- (NSInteger)getGapInterval {
    NSString *lastIntervalString = [self getIntervalFromUserDefault];
    NSInteger lastInterVal = [lastIntervalString integerValue];
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval currentInterval = [date timeIntervalSince1970];
    double gapInterval = currentInterval - lastInterVal;
    NSInteger gap = (NSInteger)(gapInterval);
    return gap;
}

#pragma mark - NSTimer
- (void)startTimer:(NSInteger)gap {
    self.seconds = gap;
    if (!self.timer) {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(buttonCountDown) userInfo:nil repeats:YES];
        self.isEnable = NO;
        self.titleLabel.font = [UIFont boldSystemFontOfSize:10];
        [self setTitle:[NSString stringWithFormat:@"重新发送%lis",(long)self.seconds] forState:UIControlStateNormal];
    }
}

- (void)buttonCountDown {
    self.seconds--;
    [self setTitle:[NSString stringWithFormat:@"重新发送%lis",(long)self.seconds] forState:UIControlStateNormal];
    if (self.seconds == 0) {
        [self.timer invalidate];
        self.timer = nil;
        [self setTitle:@"验证码" forState:UIControlStateNormal];
        self.titleLabel.font = [UIFont boldSystemFontOfSize:13];
        if (self.isCanClickByPhone) {
            self.isEnable = YES;
        }
        [self deleteIntervalFromUserDefault];
    }
}

#pragma mark - Setter
- (void)setIsEnable:(BOOL)isEnable {
    _isEnable = isEnable;
    self.enabled = isEnable;
    if (!isEnable) {
        self.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    } else {
        self.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    }
}

#pragma mark - Private
- (void)saveIntervalToUserDefault {
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:0];
    NSTimeInterval timeInterval = [date timeIntervalSince1970];
    NSString *timerString = [NSString stringWithFormat:@"%f",timeInterval];
    switch (self.timerType) {
        case PDPhoneRegisterTimer:
            [Tool userDefaulteWithKey:TIMEINTERVAL_PHONE_REGISTER_KEY Obj:timerString];
            break;
        case PDMailRegisterTimer:
            [Tool userDefaulteWithKey:TIMEINTERVAL_MAIL_REGISTER_KEY Obj:timerString];
            break;
        case PDPhonePasswordTimer:
            [Tool userDefaulteWithKey:TIMEINTERVAL_PHONE_PASSWORD_KEY Obj:timerString];
            break;
        case PDMailPasswordTimer:
            [Tool userDefaulteWithKey:TIMEINTERVAL_MAIL_PASSWORD_KEY Obj:timerString];
            break;
        case PDBindingTimer:
            [Tool userDefaulteWithKey:TIMEINTERVAL_BINDING_KEY Obj:timerString];
            break;
        default:
            break;
    }
}

- (void)deleteIntervalFromUserDefault {
    switch (self.timerType) {
        case PDPhoneRegisterTimer:
            [Tool userDefaultDelegtaeWithKey:TIMEINTERVAL_PHONE_REGISTER_KEY];
            break;
        case PDMailRegisterTimer:
            [Tool userDefaultDelegtaeWithKey:TIMEINTERVAL_MAIL_REGISTER_KEY];
            break;
        case PDPhonePasswordTimer:
            [Tool userDefaultDelegtaeWithKey:TIMEINTERVAL_PHONE_PASSWORD_KEY];
            break;
        case PDMailPasswordTimer:
            [Tool userDefaultDelegtaeWithKey:TIMEINTERVAL_MAIL_PASSWORD_KEY];
            break;
        case PDBindingTimer:
            [Tool userDefaultDelegtaeWithKey:TIMEINTERVAL_BINDING_KEY];
            break;
        default:
            break;
    }
}

- (NSString *)getIntervalFromUserDefault {
    NSString *intervalString;
    switch (self.timerType) {
        case PDPhoneRegisterTimer:
            intervalString = [Tool userDefaultGetWithKey:TIMEINTERVAL_PHONE_REGISTER_KEY];
            break;
        case PDMailRegisterTimer:
            intervalString = [Tool userDefaultGetWithKey:TIMEINTERVAL_MAIL_REGISTER_KEY];
            break;
        case PDPhonePasswordTimer:
            intervalString = [Tool userDefaultGetWithKey:TIMEINTERVAL_PHONE_PASSWORD_KEY];
            break;
        case PDMailPasswordTimer:
            intervalString = [Tool userDefaultGetWithKey:TIMEINTERVAL_MAIL_PASSWORD_KEY];
            break;
        case PDBindingTimer:
            intervalString = [Tool userDefaultGetWithKey:TIMEINTERVAL_BINDING_KEY];
            break;
        default:
            break;
    }
    return intervalString;
}
@end
