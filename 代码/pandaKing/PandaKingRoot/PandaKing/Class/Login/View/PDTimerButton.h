//
//  PDTimerButton.h
//  PandaChallenge
//
//  Created by Bluedaquiri on 16/3/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLoginEnumHeader.h"

@interface PDTimerButton : UIButton
@property (nonatomic,assign)CGFloat cornerRadius;
@property (nonatomic,assign)BOOL isEnable; /**< 按钮是否可以点击*/
@property (nonatomic,assign)BOOL isTimeOut; /**< 是否超过60s*/
@property (nonatomic,assign)BOOL isCanClickByPhone; /**< 当textField中的手机号有效是为YES*/
@property (nonatomic,strong)NSTimer *timer;

- (instancetype)initWithTimerType:(PDTimerType)type actionBlock:(void(^)())actionBlock; /**< 初始化实例*/
@end
