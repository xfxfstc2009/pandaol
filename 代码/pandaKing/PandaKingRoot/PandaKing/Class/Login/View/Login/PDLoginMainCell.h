//
//  PDLoginMainCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PDLoginMainDelegate <NSObject>

-(void)segmentDidSelect:(NSInteger)buttonIndex;                 /**< 用户点击segment 方法*/
-(void)touchManager;                                            /**< 用户点击当前view进行执行方法*/

@end

@interface PDLoginMainCell : UITableViewCell

@property (nonatomic,weak)id<PDLoginMainDelegate> delegate;         /**< 代理*/

+(CGFloat)calculationCellHeight;

@end
