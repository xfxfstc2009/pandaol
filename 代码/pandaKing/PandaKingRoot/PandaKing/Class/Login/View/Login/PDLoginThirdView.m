//
//  PDLoginThirdView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginThirdView.h"
#import "PDOAuthLoginAndShareTool.h"

static char accountRegisterKey;
static char accountLoginKey;
static char accountUserDelegateKey;
@interface PDLoginThirdView()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *actionButton;

@property (nonatomic,strong)UIButton *accountLoginButton;               /**< 账号登录*/
@property (nonatomic,strong)UIButton *registerButton;                   /**< 注册按钮*/
@property (nonatomic,strong)UIView *lineView;
@end

@implementation PDLoginThirdView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self addSubview:self.titleLabel];
    self.titleLabel.text = @"登录即表示您同意";
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(12), titleSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    [self.actionButton setTitle:@"用户协议" forState:UIControlStateNormal];
    [self.actionButton setTitleColor:[UIColor colorWithCustomerName:@"橙"] forState:UIControlStateNormal];
    self.actionButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &accountUserDelegateKey);
        if (block){
            block();
        }
    }];
    CGSize actionButtonSize = [self.actionButton.titleLabel.text sizeWithCalcFont:self.actionButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.actionButton.titleLabel.font])];
    self.actionButton.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame), 0, actionButtonSize.width, self.size_height);
    [self addSubview:self.actionButton];
    
    // 注册
    self.registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.registerButton.backgroundColor = [UIColor clearColor];
    self.registerButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self.registerButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    [self.registerButton setTitle:@"注册" forState:UIControlStateNormal];
    [self.registerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf , &accountRegisterKey);
        if (block){
            block();
        }
    }];
    CGSize registeSize = [self.registerButton.titleLabel.text sizeWithCalcFont:self.registerButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.registerButton.titleLabel.font])];
    CGFloat width = registeSize.width + 2 * LCFloat(11);
    CGFloat height = self.size_height;
    self.registerButton.frame = CGRectMake(kScreenBounds.size.width - width, 0, width, height);
    [self addSubview:self.registerButton];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.lineView.frame = CGRectMake(self.registerButton.orgin_x - .5f, LCFloat(12), .5f, self.size_height - 2 * LCFloat(12));
    [self addSubview:self.lineView];
    
    //
    self.accountLoginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.accountLoginButton.backgroundColor = [UIColor clearColor];
    [self.accountLoginButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    [self.accountLoginButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf , &accountLoginKey);
        if (block){
            block();
        }
    }];
    [self.accountLoginButton setTitle:@"账号登录" forState:UIControlStateNormal];
    self.accountLoginButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    
    CGSize accountLoginSize = [self.accountLoginButton.titleLabel.text sizeWithCalcFont:self.accountLoginButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.size_height)];
    CGFloat accountLoginWidth = accountLoginSize.width + 2 * LCFloat(11);
    self.accountLoginButton.frame = CGRectMake(self.lineView.orgin_x - accountLoginWidth, self.registerButton.orgin_y, accountLoginWidth, self.size_height);
    [self addSubview:self.accountLoginButton];
    
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(12);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(12);
    return cellHeight;
}

-(void)accountLogin:(void(^)())block{
    objc_setAssociatedObject(self, &accountLoginKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)accountRegister:(void(^)())block{
    objc_setAssociatedObject(self, &accountRegisterKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)accountUserDelegate:(void(^)())block{
    objc_setAssociatedObject(self, &accountUserDelegateKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
