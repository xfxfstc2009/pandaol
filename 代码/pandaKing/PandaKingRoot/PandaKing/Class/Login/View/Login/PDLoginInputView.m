//
//  PDLoginInputView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginInputView.h"
#import "PDCountDownButton.h"
#import <objc/runtime.h>
#import "PDMineCodeButton.h"

static char *smsBtnKey;
static char vCodeActionClickManagerKey;
@interface PDLoginInputView()<UITextFieldDelegate>
@property (nonatomic,strong)PDImageView *iconImgView;               /*< icon**/
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UIView *lineView2;                      /**< 竖线*/
@property (nonatomic,strong)PDCountDownButton *countDownButton;
@property (nonatomic,strong)UIButton *eyeBtn;                       /**< 眼睛按钮*/

@end

@implementation PDLoginInputView

-(instancetype)initWithFrame:(CGRect)frame type:(PDLoginInputViewType)type{
    self = [super initWithFrame:frame];
    if (self){
        _LoginInputType = type;
        [self createView];
    }
    return self;
}

#pragma mark - UITableView
-(void)createView{
    // 1. 创建icon
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    // 2. 创建输入框
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor clearColor];
    self.inputTextField.userInteractionEnabled = YES;
    self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.inputTextField.delegate = self;
    [self.inputTextField addTarget:self action:@selector(textFieldDidChanged) forControlEvents:UIControlEventEditingChanged];
    if (self.LoginInputType == PDLoginInputViewTypePhone){           // 【手机号码模式】
        self.inputTextField.keyboardType = UIKeyboardTypeURL;
        self.inputTextField.returnKeyType = UIReturnKeyNext;
    } else if (self.LoginInputType == PDLoginInputViewTypePwd){      // 【密码模式】
        self.inputTextField.secureTextEntry = YES;
        self.inputTextField.clearButtonMode = UITextFieldViewModeAlways;
        self.inputTextField.returnKeyType = UIReturnKeySend;
        self.inputTextField.keyboardType = UIKeyboardTypeURL;
    } else if (self.LoginInputType == PDLoginInputViewTypeUser || self.LoginInputType == PDLoginInputViewTypeRegisterUser){
        self.inputTextField.keyboardType = UIKeyboardTypeTwitter;
        self.inputTextField.returnKeyType = UIReturnKeyNext;
    } else if (self.LoginInputType == PDLoginInputViewTypeSmsCode){      // 【短信验证码】
        self.inputTextField.returnKeyType = UIReturnKeySend;
        self.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
    } else if (self.LoginInputType == PDLoginInputViewTypeMineCode){     // 【我们的验证码】
        self.inputTextField.clearButtonMode = UITextFieldViewModeAlways;
        self.inputTextField.returnKeyType = UIReturnKeyNext;
        self.inputTextField.keyboardType = UIKeyboardTypeURL;
    }
    
    [self addSubview:self.inputTextField];
    
    self.iconImgView.frame = CGRectMake(LCFloat(20) + LCFloat(18), (self.size_height - LCFloat(18)) / 2. , LCFloat(18), LCFloat(18));
    self.inputTextField.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(18), 0, kScreenBounds.size.width - CGRectGetMaxX(self.iconImgView.frame) - LCFloat(18) - LCFloat(20), self.size_height);
    
    if (self.LoginInputType == PDLoginInputViewTypeUser || self.LoginInputType == PDLoginInputViewTypeRegisterUser){             // 用户登录状态
        [self userType];
    } else if (self.LoginInputType == PDLoginInputViewTypePwd){       // 密码登录状态
        [self pwdType];
    } else if (self.LoginInputType == PDLoginInputViewTypeSmsCode){   // 验证码
        [self smsType];
    } else if (self.LoginInputType == PDLoginInputViewTypePhone){     //
        [self userType];
    } else if (self.LoginInputType == PDLoginInputViewTypeMineCode){
        [self mineCodeType];
    }
    
    // 创建线条
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    self.lineView.frame = CGRectMake(LCFloat(20), CGRectGetMaxY(self.inputTextField.frame), kScreenBounds.size.width - 2 * LCFloat(20), 1);
    [self addSubview:self.lineView];
}


#pragma mark - type
-(void)userType{
    self.eyeBtn.hidden = YES;
    self.iconImgView.image = [UIImage imageNamed:@"icon_login_user"];
    self.inputTextField.placeholder = @"请输入您的账号/手机";
}

-(void)pwdType{
    self.iconImgView.image = [UIImage imageNamed:@"icon_login_password"];
    self.inputTextField.placeholder = @"请输入6-18位字符或数字";
    
    
    // 5. 添加眼睛
    self.eyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.eyeBtn.backgroundColor = [UIColor clearColor];
    [self.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwd_close"] forState:UIControlStateNormal];
    self.eyeBtn.selected = NO;
    __weak typeof(self)weakSelf = self;
    [self.eyeBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        self.eyeBtn.selected = !self.eyeBtn.selected;
        if (weakSelf.eyeBtn.selected){
            [weakSelf.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwd_open"] forState:UIControlStateNormal];
            self.inputTextField.secureTextEntry = NO;
        } else {
            [weakSelf.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwd_close"] forState:UIControlStateNormal];
            self.inputTextField.secureTextEntry = YES;
        }
    }];
    [self addSubview:self.eyeBtn];
    
    // 2. 增加眼睛
    self.eyeBtn.frame = CGRectMake(kScreenBounds.size.width - LCFloat(18) - self.size_height, 0, self.size_height, self.size_height);
    self.eyeBtn.hidden = NO;
    // 3. 输入框移动
    self.inputTextField.size_width = self.eyeBtn.orgin_x - self.inputTextField.orgin_x;
    
}

-(void)smsType{
    self.eyeBtn.hidden = YES;
    self.iconImgView.image = [UIImage imageNamed:@"icon_login_password"];
    self.inputTextField.placeholder = @"请输入验证码";
    
    // 创建获取验证码按钮
    [self createSmsButton];
    
    self.lineView2 = [[UIView alloc]init];
    self.lineView2.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    self.lineView2.frame = CGRectMake(self.countDownButton.orgin_x - 1, LCFloat(5), .5f, self.size_height - 2 * LCFloat(5));
    [self addSubview:self.lineView2];
}

-(void)mineCodeType{
    self.eyeBtn.hidden = YES;
    self.iconImgView.image = [UIImage imageNamed:@"icon_login_mineCode"];
    self.inputTextField.placeholder = @"请输入验证码";
    
    // 创建获取验证码按钮
    [self createMineCode];
    
    self.lineView2 = [[UIView alloc]init];
    self.lineView2.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    self.lineView2.frame = CGRectMake(self.mineCodeButton.orgin_x - 1, LCFloat(5), .5f, self.size_height - 2 * LCFloat(5));
    [self addSubview:self.lineView2];
}

-(void)createMineCode{
    self.mineCodeButton = [[PDMineCodeButton alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(98), 0, LCFloat(98), 44) ];
    __weak typeof(self)weakSelf = self;
    [self.mineCodeButton mineCodeActionClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &vCodeActionClickManagerKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.mineCodeButton];
    self.inputTextField.size_width = self.mineCodeButton.orgin_x - self.inputTextField.orgin_x - LCFloat(20);
    self.lineView.orgin_x = self.mineCodeButton.orgin_x - 1;
}

-(void)vCodeActionClickManager:(void(^)())block{
    objc_setAssociatedObject(self, &vCodeActionClickManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



-(void)createSmsButton{
    __weak typeof(self)weakSelf = self;
    self.countDownButton = [[PDCountDownButton alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(98), 0, LCFloat(98), 44) daojishi:60 withBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &smsBtnKey);
        if (block){
            block();
        }
    }];
    
    [self addSubview:self.countDownButton];
    
    self.inputTextField.size_width = self.countDownButton.orgin_x - self.inputTextField.orgin_x - LCFloat(20);
    self.lineView.orgin_x = self.countDownButton.orgin_x - 1;
}

-(void)phoneType{
    self.eyeBtn.hidden = YES;
    self.iconImgView.image = [UIImage imageNamed:@"icon_login_user"];
    self.inputTextField.placeholder = @"手机号";
}

+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}




#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (self.LoginInputType == PDLoginInputViewTypeSmsCode || self.LoginInputType == PDLoginInputViewTypeMineCode){                     // 【验证码】
        if (toBeString.length > 4){
            textField.text = [toBeString substringToIndex:4];
            return NO;
        }
    } else if (self.LoginInputType == PDLoginInputViewTypeUser || self.LoginInputType == PDLoginInputViewTypeRegisterUser){                 // 【用户】
        if(toBeString.length > 20){
            textField.text = [toBeString substringToIndex:20];
            return NO;
        }
    } else if (self.LoginInputType == PDLoginInputViewTypePwd){
        if (toBeString.length > 18){
            textField.text = [toBeString substringToIndex:18];
            return NO;
        }
    } else if (self.LoginInputType == PDLoginInputViewTypePhone){
        if(toBeString.length > 11){
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
    }
    
    return YES;
}

-(void)textFieldDidChanged{
    BOOL success = NO;
    if (self.LoginInputType == PDLoginInputViewTypeSmsCode){
        if (self.inputTextField.text.length == 4){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.LoginInputType == PDLoginInputViewTypePhone){
        if (self.inputTextField.text.length == 11){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.LoginInputType == PDLoginInputViewTypeUser || self.LoginInputType == PDLoginInputViewTypeRegisterUser){
        if (self.inputTextField.text.length >= 5){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.LoginInputType == PDLoginInputViewTypePwd){
        if (self.inputTextField.text.length >= 6){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.LoginInputType == PDLoginInputViewTypeMineCode){
        if (self.inputTextField.text.length >= 4){
            success = YES;
            [AccountModel sharedAccountModel].verifyCode = self.inputTextField.text;
        } else {
            success = NO;
        }
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextViewConfirm:textField:status:)]){
        [self.delegate inputTextViewConfirm:self.LoginInputType textField:self.inputTextField status:success];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextFieldShouldReturn:returnType:)]){
        [self.delegate inputTextFieldShouldReturn:textField returnType:self.inputTextField.returnKeyType];
    }
    
    return YES;
}

#pragma mark - Public Manager
-(void)smsTextFieldGetSmsInfo:(void(^)())block{
    objc_setAssociatedObject(self, &smsBtnKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)startCountDownAnimation{
    [self.countDownButton startTimer];
}
@end
