//
//  PDLoginOtherCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLoginInputView.h"
#import "PDLoginButtonView.h"

typedef NS_ENUM(NSInteger,otherDidSelectType) {
    otherDidSelectTypeResetPwd,                 /**< 忘记密码*/
    otherDidSelectTypeLogin,                    /**< 登录*/
    otherDidSelectTypeQQ,                       /**< QQ*/
    otherDidSelectTypeWechat,                   /**< Wechat*/
    otherDidSelectTypeRegister,                 /**< 注册*/
    otherDidSelectTypeDelegate,                 /**< 跳转协议*/
    otherDidSelectTypeThirdLogin,               /**< 三方登录*/
    otherDidSelectTypeThirdRegister,            /**< 三方登录*/
};

@protocol PDLoginOtherCellDelegate <NSObject>

-(void)userTouchMangerWithIndex:(otherDidSelectType)type;               /**< 用户点击进行收拢*/
-(void)textFieldwillShow:(UITextField *)textField;                      /**< textFieldDidBeginEditing*/
-(void)directToRegisterPageWithVcodeIsSuccess:(BOOL)isSuccess account:(NSString *)account smsCode:(NSString *)smsCode; /**< 跳转到注册页面*/
-(void)loginMainViewControllerWillLoginWithUser:(NSString *)user pwd:(NSString *)password;

// 2017-10-10
-(void)vCodeGetManager;
@end

@interface PDLoginOtherCell : UITableViewCell

@property (nonatomic,strong)PDLoginInputView *inputLoginUserView;            /**< 输入*/
@property (nonatomic,strong)PDLoginInputView *inputLoginPwdView;             /**< 密码*/
@property (nonatomic,strong)PDLoginInputView *inputRegisterUserView;           /**< 注册view*/
@property (nonatomic,strong)PDLoginInputView *inputRegisterPhoneSmsView;        /**< 验证码*/
@property (nonatomic,strong)PDLoginInputView *inputMineCodeView;
@property (nonatomic,strong)PDLoginButtonView *registButtonView;                /**< 按钮*/
@property (nonatomic,strong)PDLoginButtonView *buttonView;              /**< 按钮*/

@property (nonatomic,assign)CGFloat transferCellHeight;                 /**< 上个页面传入高度*/
@property (nonatomic,weak)id<PDLoginOtherCellDelegate>  delegate;       /**< 代理*/

-(void)scrollToIndex:(NSInteger)index;                                  /**< 登录&注册页面跳转*/

@end
