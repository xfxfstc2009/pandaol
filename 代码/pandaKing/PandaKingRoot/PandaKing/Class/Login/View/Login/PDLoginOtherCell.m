//
//  PDLoginOtherCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginOtherCell.h"
#import "PDLoginThirdView.h"
#import "PDLoginButtonView.h"
#import "WXApi.h"

@interface PDLoginOtherCell()<UIScrollViewDelegate,PDLoginInputViewDelegate>
@property (nonatomic,strong)UIScrollView *mainScrollView;               /**< 主试图的scrollView*/
@property (nonatomic,strong)UIView *loginView;                          /**< 登录*/
@property (nonatomic,strong)UIView *registerView;                       /**< 注册*/
@property (nonatomic,strong)UIView *wechatLoginView;                    /**< 微信登录*/

@property (nonatomic,strong)PDLoginThirdView *thirdView;                /**< 三方*/

// 注册
@property (nonatomic,strong)PDLoginButtonView *registerButton;          /**< 注册按钮*/
// 微信登录
@property (nonatomic,strong)UIButton *wechatButton;
@property (nonatomic,strong)UITextField *tempLoginUserTextField;        /**< 临时调用账号*/
@property (nonatomic,strong)UITextField *tempLoginPwdTextField;         /**< 密码*/
@property (nonatomic,strong)UITextField *tempRegUserTextField;          /**< 注册账号*/
@property (nonatomic,strong)UITextField *tempRegSmsTextField;           /**< 验证码*/

@property (nonatomic,assign)BOOL tempLoginUserEnable;
@property (nonatomic,assign)BOOL tempLoginPwdEnable;
@property (nonatomic,assign)BOOL tempRegUserEnable;
@property (nonatomic,assign)BOOL tempRegSmsEnable;
@end

@implementation PDLoginOtherCell

-(instancetype) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - UIScrollView
-(void)createView{
    // 1. 创建底部的view
    [self createBottomView];
    // 2.
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 300);
        self.mainScrollView.delegate = self;
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.backgroundColor = [UIColor whiteColor];
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.scrollEnabled = NO;
        [self addSubview:self.mainScrollView];
    }
    // 2. 创建上面的view
    [self createTableView];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.loginView){
        self.loginView = [[UIView alloc]init];
        self.loginView.backgroundColor = [UIColor clearColor];
        [self.mainScrollView addSubview:self.loginView];
    }
    
    if (!self.registerView){
        self.registerView = [[UIView alloc]init];
        self.registerView.backgroundColor = [UIColor clearColor];
        [self.mainScrollView addSubview:self.registerView];
    }
    
    if (!self.wechatLoginView){
        self.wechatLoginView = [[UIView alloc]init];
        self.wechatLoginView.backgroundColor = [UIColor clearColor];
        [self.mainScrollView addSubview:self.wechatLoginView];
    }
}

#pragma mark - SET
-(void)setTransferCellHeight:(CGFloat)transferCellHeight{           // 传入高度
    _transferCellHeight = transferCellHeight;
    
    self.thirdView.orgin_y = self.transferCellHeight - [PDLoginThirdView calculationCellHeight];
    
    self.mainScrollView.size_height = transferCellHeight - self.thirdView.size_height;
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 3, self.mainScrollView.frame.size.height);
    self.loginView.frame = self.mainScrollView.bounds;
    self.registerView.frame = self.mainScrollView.bounds;
    self.wechatLoginView.frame = self.mainScrollView.bounds;
    self.loginView.orgin_x = CGRectGetMaxX(self.wechatLoginView.frame);
    self.registerView.orgin_x = CGRectGetMaxX(self.loginView.frame);
    if ([WXApi isWXAppInstalled]){
        [self wechatLoginManager];
    } else {
        [self.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width, 0)];
    }
    
    [self loginViewManager];

    [self registerViewManager];
}

-(void)createBottomView{
    __weak typeof(self)weakSelf = self;
    // 1. 创建三方
    CGFloat thirdSizeHeight = [PDLoginThirdView calculationCellHeight];
    self.thirdView = [[PDLoginThirdView alloc]initWithFrame:CGRectMake(0, self.transferCellHeight - thirdSizeHeight, kScreenBounds.size.width, thirdSizeHeight)];
    self.thirdView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.thirdView];
    [self.thirdView accountLogin:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(userTouchMangerWithIndex:)]){
            [strongSelf.delegate userTouchMangerWithIndex:otherDidSelectTypeThirdLogin];
        }
    }];
    
    [self.thirdView accountRegister:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(userTouchMangerWithIndex:)]){
            [strongSelf.delegate userTouchMangerWithIndex:otherDidSelectTypeThirdRegister];
        }
    }];
    
    [self.thirdView accountUserDelegate:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(userTouchMangerWithIndex:)]){
            [strongSelf.delegate userTouchMangerWithIndex:otherDidSelectTypeDelegate];
        }
    }];
}


#pragma mark 创建登录页面
-(void)loginViewManager{
    __weak typeof(self)weakSelf = self;
    if (!self.loginView.subviews.count){
        // 2. 创建登录按钮
        CGFloat buttonSizeHeight = [PDLoginButtonView calculationCellHeightWithType:loginButtonTypeLogin];
        self.buttonView = [[PDLoginButtonView alloc]initWithFrame:CGRectMake(0, self.thirdView.orgin_y - LCFloat(20) - buttonSizeHeight, kScreenBounds.size.width, buttonSizeHeight) type:loginButtonTypeLogin];
        self.buttonView.cusomterButton.backgroundColor = c4;
        self.buttonView.btnClickBlock = ^(loginButtonType type){
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(userTouchMangerWithIndex:)]){
                [strongSelf.delegate userTouchMangerWithIndex:otherDidSelectTypeLogin];
            }
        };
        [self.loginView addSubview:self.buttonView];
        
        // 3. 创建输入框
        self.inputLoginUserView = [[PDLoginInputView alloc]initWithFrame:CGRectMake(0, LCFloat(24), kScreenBounds.size.width, LCFloat(44)) type:PDLoginInputViewTypeUser];
        self.inputLoginUserView.inputTextField.placeholder = @"请输入您的手机";
        self.inputLoginUserView.delegate = self;
        [self.loginView addSubview:self.inputLoginUserView];
        
        // 4. 创建密码框
        self.inputLoginPwdView = [[PDLoginInputView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.inputLoginUserView.frame) + LCFloat(16), kScreenBounds.size.width, LCFloat(44)) type:PDLoginInputViewTypePwd];
        self.inputLoginPwdView.delegate = self;
        [self.loginView addSubview:self.inputLoginPwdView];
        
        // 5. 创建忘记密码
        
        CGSize contentOfText = [@"忘记密码" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]])];
        
        CGFloat consHeight = self.buttonView.orgin_y - CGRectGetMaxY(self.inputLoginPwdView.frame);
        // 重新修改内容信息
        CGRect restRect = CGRectMake((kScreenBounds.size.width - contentOfText.width - 2 * LCFloat(15)) / 2., CGRectGetMaxY(self.inputLoginPwdView.frame) + (consHeight - [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]] - LCFloat(10)) / 2., contentOfText.width + 2 * LCFloat(15), [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]] + 2 * LCFloat(10));
        
        self.registButtonView = [[PDLoginButtonView alloc]initWithFrame:restRect type:loginButtonTypeReset];
        self.registButtonView.btnClickBlock = ^(loginButtonType type){
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(userTouchMangerWithIndex:)]){
                [strongSelf.delegate userTouchMangerWithIndex:otherDidSelectTypeResetPwd];
            }
        };
        [self.loginView addSubview:self.registButtonView];
    }
}

#pragma mark 创建注册页面
-(void)registerViewManager{
    __weak typeof(self)weakSelf = self;
    // 3. 创建输入框
    self.inputRegisterUserView = [[PDLoginInputView alloc]initWithFrame:CGRectMake(0, LCFloat(24), kScreenBounds.size.width, LCFloat(44)) type:PDLoginInputViewTypeRegisterUser];
    self.inputRegisterUserView.inputTextField.placeholder = @"请输入您的手机";
    self.inputRegisterUserView.delegate = self;
    [self.registerView addSubview:self.inputRegisterUserView];
 
    // 4. 创建验证码
    self.inputMineCodeView = [[PDLoginInputView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.inputRegisterUserView.frame) + LCFloat(16), kScreenBounds.size.width, LCFloat(44)) type:PDLoginInputViewTypeMineCode];
    self.inputRegisterPhoneSmsView.delegate = self;
    // 我们的验证码
    [self.inputMineCodeView vCodeActionClickManager:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(vCodeGetManager)]){
            [strongSelf.delegate vCodeGetManager];
        }
    }];
    [self.registerView addSubview:self.inputMineCodeView];
    
    
    // 4. 创建验证码
    self.inputRegisterPhoneSmsView = [[PDLoginInputView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.inputMineCodeView.frame) + LCFloat(16), kScreenBounds.size.width, LCFloat(44)) type:PDLoginInputViewTypeSmsCode];
    self.inputRegisterPhoneSmsView.delegate = self;
    [self.inputRegisterPhoneSmsView smsTextFieldGetSmsInfo:^{           // 进行发送验证码
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf verificationSmsCode];
    }];
    [self.registerView addSubview:self.inputRegisterPhoneSmsView];
    
    
    
    // 创建按钮
    self.registerButton = [[PDLoginButtonView alloc ]initWithFrame:self.buttonView.frame type:loginButtonTypeRegister];
    [self.registerView addSubview:self.registerButton];
    [self.registerButton changeBtnEnableType:NO];
    self.registerButton.btnClickBlock = ^(loginButtonType type){
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(userTouchMangerWithIndex:)]){
            [strongSelf.delegate userTouchMangerWithIndex:otherDidSelectTypeRegister];
        }
    };
}

#pragma mark - 创建微信登录接口
-(void)wechatLoginManager{
    if (!self.wechatButton){
        self.wechatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    self.wechatButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self.wechatButton setTitle:@"微信登录" forState:UIControlStateNormal];
    [self.wechatButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.wechatButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.wechatButton.layer.cornerRadius = LCFloat(5);
    self.wechatButton.clipsToBounds = YES;
    __weak typeof(self)weakSelf = self;
    [self.wechatButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (self.delegate && [strongSelf.delegate respondsToSelector:@selector(userTouchMangerWithIndex:)]){
            [strongSelf.delegate userTouchMangerWithIndex:otherDidSelectTypeWechat];
        }
    }];
    self.wechatButton.frame = CGRectMake(2 * LCFloat(11), 3 * LCFloat(11), kScreenBounds.size.width - 4 * LCFloat(11), LCFloat(50));
    [self.wechatLoginView addSubview:self.wechatButton];
}


#pragma mark - Other Manager
-(void)scrollToIndex:(NSInteger)index{
    [self.mainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
}


-(void)textFieldDidBeginEditing:(UITextField *)textField{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldwillShow:)]){
        [self.delegate textFieldwillShow:textField];
    }
}

// return 按钮
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.inputLoginUserView.inputTextField){            // 账号
        [self.inputLoginPwdView.inputTextField becomeFirstResponder];
    } else if (self.inputLoginPwdView.inputTextField){                   // 【准备进行登录】
        if(self.delegate && [self.delegate respondsToSelector:@selector(loginMainViewControllerWillLoginWithUser:pwd:)]){
            [self.delegate loginMainViewControllerWillLoginWithUser:self.inputLoginUserView.inputTextField.text pwd:self.inputLoginPwdView.inputTextField.text];
        }
    } else if (self.inputRegisterUserView.inputTextField){
        [self.inputRegisterPhoneSmsView.inputTextField becomeFirstResponder];
    } else if (self.inputRegisterPhoneSmsView.inputTextField){
        if (self.delegate && [self.delegate respondsToSelector:@selector(userTouchMangerWithIndex:)]){
            [self.delegate userTouchMangerWithIndex:otherDidSelectTypeLogin];
        }
    }
    return YES;
}



-(void)inputTextViewConfirm:(PDLoginInputViewType)cellType textField:(UITextField *)textField status:(BOOL)status{
    if (cellType == PDLoginInputViewTypeRegisterUser){            // 【账号】
        if (status == YES){
            self.tempRegUserTextField.text = textField.text;
            self.tempRegUserEnable = YES;
            if (self.tempRegSmsEnable == YES){
                [self.registerButton changeBtnEnableType:YES];
            } else {
                [self.registerButton changeBtnEnableType:NO];
            }
        } else {
            self.tempRegUserEnable = NO;
            [self.registerButton changeBtnEnableType:NO];
        }
    } else if (cellType == PDLoginInputViewTypePwd){       // 【密码】
        if (status == YES){
            self.tempLoginPwdTextField.text = textField.text;
            self.tempLoginPwdEnable = YES;
            if (self.tempLoginUserEnable == YES){
                [self.buttonView changeBtnEnableType:YES];
            } else {
                [self.buttonView changeBtnEnableType:NO];
            }
        } else {
            self.tempLoginPwdEnable = NO;
            [self.buttonView changeBtnEnableType:NO];
        }
    } else if (cellType == PDLoginInputViewTypeSmsCode){   // 【验证码】
        if (status == YES){
            self.tempRegSmsTextField.text = textField.text;
            self.tempRegSmsEnable = YES;
            if (self.tempRegUserEnable == YES){
                [self.registerButton changeBtnEnableType:YES];
            } else {
                [self.registerButton changeBtnEnableType:NO];
            }
        } else {
            self.tempRegSmsEnable = NO;
            [self.registerButton changeBtnEnableType:NO];
        }
    } else if (cellType == PDLoginInputViewTypeUser){      // 【账号】
        if (status == YES){
            self.tempLoginUserTextField.text = textField.text;
            self.tempLoginUserEnable = YES;
            if (self.tempLoginPwdEnable == YES){
                [self.buttonView changeBtnEnableType:YES];
            } else {
                [self.buttonView changeBtnEnableType:NO];
            }
        } else {
            self.tempLoginUserEnable = NO;
            [self.buttonView changeBtnEnableType:NO];
        }
    }
}

-(void)inputTextFieldShouldReturn:(UITextField *)textField returnType:(UIReturnKeyType)type{
    if (textField == self.inputRegisterUserView.inputTextField && type == UIReturnKeyNext){
        [self.inputRegisterPhoneSmsView.inputTextField becomeFirstResponder];
    } else if (textField == self.inputLoginUserView.inputTextField && type == UIReturnKeyNext){
        [self.inputLoginPwdView.inputTextField becomeFirstResponder];
    } else if (textField == self.inputLoginPwdView.inputTextField && type == UIReturnKeySend){
        if (self.delegate && [self.delegate respondsToSelector:@selector(userTouchMangerWithIndex:)]){
            [self.delegate userTouchMangerWithIndex:otherDidSelectTypeLogin];
        }
    } else if (textField == self.inputRegisterPhoneSmsView.inputTextField && type == UIReturnKeySend){
        if (self.delegate && [self.delegate respondsToSelector:@selector(userTouchMangerWithIndex:)]){
            [self.delegate userTouchMangerWithIndex:otherDidSelectTypeRegister];
        }
    }
}


#pragma mark - Interface
#pragma mark 验证手机&邮箱输入格式
-(void)verificationSmsCode{
    NSString *err = @"";
    if (!self.inputRegisterUserView.inputTextField.text.length){
        err = @"请输入您的账号(手机号)";
    } else {
        if ((![Tool validateMobile:self.inputRegisterUserView.inputTextField.text]) && (![Tool isValidateEmail:self.inputRegisterUserView.inputTextField.text])){
            err = @"请确认您输入的账号为手机号";
        }
    }
    
    if (err.length){
        [[PDAlertView sharedAlertView]showError:err];
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    [self getSmsCodeWithPhoneNumber:self.inputRegisterUserView.inputTextField.text successBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 1.变成第一响应
        [strongSelf.inputRegisterPhoneSmsView.inputTextField becomeFirstResponder];
        // 2. 验证码更新
        [strongSelf.inputRegisterPhoneSmsView startCountDownAnimation];
    }];
}

#pragma mark 获取验证码
-(void)getSmsCodeWithPhoneNumber:(NSString *)phoneNumber successBlock:(void(^)())successBlock{
    NSString *alert = @"正在拼命获取验证码";
    [PDHUD showHUDProgress:alert diary:0];
    __weak typeof(self)weakSelf = self;
    if (![AccountModel sharedAccountModel].verifyCode.length){
        [PDHUD showHUDProgress:@"请输入图形验证码" diary:2];
        return;
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:userRegSend requestParams:@{@"account":phoneNumber,@"type":@(UserLoginTypeCurrent),@"verifyCode":[AccountModel sharedAccountModel].verifyCode} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){           // 进行发送验证码
            // 1. 重置验证码
            [AccountModel sharedAccountModel].verifyCode = @"";
            // 2. 重置输入框
            strongSelf.inputMineCodeView.inputTextField.text = @"";
            // 3. 修改图片
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(vCodeGetManager)]){
                [strongSelf.delegate vCodeGetManager];
            }
            
            [PDHUD showHUDProgress:alert diary:1];
            successBlock();
        } else {
            // 1. 重置验证码
            [AccountModel sharedAccountModel].verifyCode = @"";
            // 2. 重置输入框
            strongSelf.inputMineCodeView.inputTextField.text = @"";
            // 3. 修改图片
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(vCodeGetManager)]){
                [strongSelf.delegate vCodeGetManager];
            }
        }
    }];
}

#pragma mark 进行注册&验证注册信息
-(void)verificationRegisterInfo{
    NSString *err = @"";
    if (!self.inputRegisterUserView.inputTextField.text.length){
        err = @"请输入注册账号";
    } else if (!self.inputRegisterPhoneSmsView.inputTextField.text.length){
        err = @"请输入验证码";
    }
    if (err.length){
        [[PDAlertView sharedAlertView]showError:err];
        return;
    }
    // 进行验证
    [self sendRequestToVcodeManagerWithAccount:self.inputRegisterUserView.inputTextField.text code:self.inputRegisterPhoneSmsView.inputTextField.text];
}

#pragma mark 注册短信或邮件验证
-(void)sendRequestToVcodeManagerWithAccount:(NSString *)account code:(NSString *)code{
    NSString *alert = @"熊猫君正在拼命验证…";
    [PDHUD showHUDProgress:alert diary:0];
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:userVcode requestParams:@{@"account":account,@"code":code} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [PDHUD showHUDProgress:alert diary:2];
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(directToRegisterPageWithVcodeIsSuccess:account:smsCode:)]){
                [strongSelf.delegate directToRegisterPageWithVcodeIsSuccess:YES account:account smsCode:code];
            }
        } else {
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(directToRegisterPageWithVcodeIsSuccess:account:smsCode:)]){
                [strongSelf.delegate directToRegisterPageWithVcodeIsSuccess:NO account:account smsCode:code];
            }
        }
    }];
}

@end
