//
//  PDLoginInputView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMineCodeButton.h"

typedef NS_ENUM(NSInteger ,PDLoginInputViewType) {
    PDLoginInputViewTypeUser,                      /**< 输入用户信息 5 - 20长度*/
    PDLoginInputViewTypeRegisterUser,              /**< 输入用户信息 5-20长度*/
    PDLoginInputViewTypePhone,                     /**< 输入手机 11 长度*/
    PDLoginInputViewTypePwd,                       /**< 输入密码 6 - 16 长度*/
    PDLoginInputViewTypeSmsCode,                   /**< 输入验证码 4 长度*/
    PDLoginInputViewTypeMineCode,                  /**< 输入我方的验证码 4 长度*/

};

@protocol PDLoginInputViewDelegate <NSObject>

-(void)inputTextViewConfirm:(PDLoginInputViewType)cellType textField:(UITextField *)textField status:(BOOL)status;
-(void)inputTextFieldShouldReturn:(UITextField *)textField returnType:(UIReturnKeyType)type;

@end


@interface PDLoginInputView : UIView

@property (nonatomic,assign)PDLoginInputViewType LoginInputType;                     /**< 当前cell类型*/
@property (nonatomic,strong)UITextField *inputTextField;                /**< 输入框*/
@property (nonatomic,strong)PDMineCodeButton *mineCodeButton;       /**< 我们的验证码*/


@property (nonatomic,weak)id<PDLoginInputViewDelegate> delegate;        /**< 代理*/
-(instancetype)initWithFrame:(CGRect)frame type:(PDLoginInputViewType)type;

+(CGFloat)calculationCellHeight;                                        /**< cell 高度*/

-(void)smsTextFieldGetSmsInfo:(void(^)())block;                         /**< 用户验证码点击*/
-(void)startCountDownAnimation;

// 用户我们验证码点击
-(void)vCodeActionClickManager:(void(^)())block;

@end
