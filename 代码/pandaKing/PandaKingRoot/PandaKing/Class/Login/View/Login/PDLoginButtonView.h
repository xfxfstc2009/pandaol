//
//  PDLoginButtonView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,loginButtonType){
    loginButtonTypeReset,                      /**< 忘记密码*/
    loginButtonTypeLogin,                       /**< 登录*/
    loginButtonTypeRegister,                    /**< 注册*/
};

typedef void(^buttonClickManage)(loginButtonType type);

@interface PDLoginButtonView : UIView

@property (nonatomic,assign)loginButtonType type;                       /**< 定义按钮类型*/
@property (nonatomic,copy)buttonClickManage btnClickBlock;              /**< 按钮点击方法 block*/
@property (nonatomic,strong)UIButton *cusomterButton;                   /**< 按钮*/

+(CGFloat)calculationCellHeightWithType:(loginButtonType)type;          /**< 返回按钮高度*/

-(void)changeBtnEnableType:(BOOL)enable;                                  // 修改按钮可点击状态
-(instancetype)initWithFrame:(CGRect)frame type:(loginButtonType)type;


@end
