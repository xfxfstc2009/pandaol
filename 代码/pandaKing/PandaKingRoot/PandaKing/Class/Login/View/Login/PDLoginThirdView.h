//
//  PDLoginThirdView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDLoginThirdView : UIView

+(CGFloat)calculationCellHeight;                        /**< 计算cell 高度*/

-(void)accountLogin:(void(^)())block;
-(void)accountRegister:(void(^)())block;
-(void)accountUserDelegate:(void(^)())block;


@end
