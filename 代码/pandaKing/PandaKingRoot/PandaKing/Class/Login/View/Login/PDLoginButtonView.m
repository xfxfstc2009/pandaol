//
//  PDLoginButtonView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginButtonView.h"

@interface PDLoginButtonView()
@end

@implementation PDLoginButtonView

-(instancetype)initWithFrame:(CGRect)frame type:(loginButtonType)type{
    self = [super initWithFrame:frame];
    if (self){
        _type = type;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.cusomterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cusomterButton setTitleColor:[UIColor colorWithCustomerName:@"深灰"] forState:UIControlStateNormal];
    [self addSubview:self.cusomterButton];
    
    if (self.type == loginButtonTypeReset){         // 忘记密码
        [self.cusomterButton setTitleColor:[UIColor colorWithCustomerName:@"深灰"] forState:UIControlStateNormal];
        [self.cusomterButton setTitle:@"忘记密码?"  forState:UIControlStateNormal];
        self.cusomterButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        [self.cusomterButton setBackgroundColor:[UIColor clearColor]];
        self.cusomterButton.frame = CGRectMake(0, 0, self.size_width, self.size_height);
    } else if (self.type == loginButtonTypeLogin){       // 登录
        [self.cusomterButton setTitle:@"登录" forState:UIControlStateNormal];
        [self.cusomterButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
        [self.cusomterButton setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
        self.cusomterButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
        self.cusomterButton.frame =CGRectMake(LCFloat(20), 0, kScreenBounds.size.width - 2 * LCFloat(20), self.size_height);
    } else if (self.type == loginButtonTypeRegister){
        [self.cusomterButton setTitle:@"下一步" forState:UIControlStateNormal];
        [self.cusomterButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
        [self.cusomterButton setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
        self.cusomterButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
        self.cusomterButton.frame =CGRectMake(LCFloat(20), 0, kScreenBounds.size.width - 2 * LCFloat(20), self.size_height);
    }
    __weak typeof(self)weakSelf = self;
    [self.cusomterButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.btnClickBlock){
            strongSelf.btnClickBlock(strongSelf.type);
        }
    }];
}

#pragma mark 修改按钮可否点击状态
-(void)changeBtnEnableType:(BOOL)type{
    if (type == YES){
        self.cusomterButton.enabled = YES;
        self.cusomterButton.backgroundColor = c7;
    } else {
        self.cusomterButton.enabled = NO;
        self.cusomterButton.backgroundColor = c4;
    }
}


#pragma mark 返回cell 高度
+(CGFloat)calculationCellHeightWithType:(loginButtonType)type{
    if (type == loginButtonTypeReset){              // 忘记密码按钮
        return LCFloat(107);
    } else if (type == loginButtonTypeLogin){     // 登录按钮
        return LCFloat(44);
    }
    return 44;
}


@end
