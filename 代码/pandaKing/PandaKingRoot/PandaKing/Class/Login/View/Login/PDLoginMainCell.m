//
//  PDLoginMainCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginMainCell.h"
#import "PDGradientMainView.h"
#import "HTHorizontalSelectionList.h"

@interface PDLoginMainCell()<HTHorizontalSelectionListDelegate, HTHorizontalSelectionListDataSource>
@property (nonatomic,strong)PDGradientMainView *mainScrollView;                 /**< 底层的view*/
@property (nonatomic,strong)PDImageView *alphaImgView;                          /**< 阴影层内容*/
@property (nonatomic,strong)PDImageView *logoImageView;                         /**< logo*/
@property (nonatomic,strong)PDImageView *lineView;                              /**< 线*/
@property (nonatomic,strong) HTHorizontalSelectionList *segmentList;            /**< segment */
@property (nonatomic,strong) NSArray *segmentArr;
@end

@implementation PDLoginMainCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.backgroundColor = [UIColor blackColor];
    
    CGFloat shipeiFloat = ([UIDevice currentDevice].systemVersion.floatValue <5) ?LCFloat(200):LCFloat(220);
    self.mainScrollView = [[PDGradientMainView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, shipeiFloat)];
    self.mainScrollView.backgroundColor = [UIColor blackColor];
    [self addSubview:self.mainScrollView];
    
    // 2. 创建蒙层
    self.alphaImgView = [[PDImageView alloc]init];
    self.alphaImgView.image = [UIImage imageNamed:@"img_login_alpha"];
    self.alphaImgView.frame = self.mainScrollView.bounds;
    [self addSubview:self.alphaImgView];
    
    // 3. 创建logo
    self.logoImageView = [[PDImageView alloc]init];
    self.logoImageView.backgroundColor = [UIColor clearColor];
    self.logoImageView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(228)) / 2., (self.mainScrollView.size_height - LCFloat(21)) / 2., LCFloat(228), LCFloat(21));
    self.logoImageView.image = [UIImage imageNamed:@"pandaol_logo_r"];
    [self addSubview:self.logoImageView];
    
//    // 4. 创建line
//    self.lineView = [[PDImageView alloc]init];
//    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"深灰"];
//    self.lineView.frame = CGRectMake(LCFloat(20), CGRectGetMaxY(self.mainScrollView.frame), kScreenBounds.size.width - 2 * LCFloat(20),1);
//    self.lineView.alpha = .8f;
//    [self addSubview:self.lineView];
    
    // 1. 创建touch
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(userTapManager)];
    [self.mainScrollView addGestureRecognizer:tap];
    
    
}

#pragma mark - HTHorizontalSelectionList
-(void)createSegment{
    self.segmentList = [[HTHorizontalSelectionList alloc]init];
    self.segmentList.frame = CGRectMake(0, CGRectGetMaxY(self.mainScrollView.frame),kScreenBounds.size.width,LCFloat(50));
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    [self.segmentList setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
    [self.segmentList setTitleFont:[UIFont fontWithCustomerSizeName:@"副标题"] forState:UIControlStateNormal];
    [self.segmentList setTitleFont:[UIFont fontWithCustomerSizeName:@"副标题"] forState:UIControlStateHighlighted];
    self.segmentList.selectionIndicatorColor = [UIColor whiteColor];
    self.segmentList.backgroundColor = [UIColor blackColor];
    self.segmentList.bottomTrimColor = [UIColor colorWithCustomerName:@"分割线"];
    self.segmentList.showsEdgeFadeEffect = YES;
    self.segmentList.selectionIndicatorHeight = 13;
    self.segmentArr = @[@"登录",@"注册"];
    self.segmentList.isNotScroll = YES;
    self.segmentList.arrowImageRect = CGRectMake(0, 0, LCFloat(30), LCFloat(13));
    self.segmentList.snapToCenter = YES;
    self.segmentList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleBottomArrow;
    [self addSubview:self.segmentList];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    if (self.delegate && [self.delegate respondsToSelector:@selector(segmentDidSelect:)]){
        [self.delegate segmentDidSelect:index];
    }
}

#pragma mark - 抛出cell高度
+(CGFloat)calculationCellHeight{
    if (([UIDevice currentDevice].systemVersion.floatValue <5)){
        return LCFloat(200) + LCFloat(50);
    } else {
        return LCFloat(220) + LCFloat(50);
    }
}

-(void)userTapManager{
    if (self.delegate && [self.delegate respondsToSelector:@selector(touchManager)]){
        [self.delegate touchManager];
    }
}

@end
