//
//  PDPasswordInputCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,passwordType) {
    passwordTypeOne,                            /**< 提示输入密码*/
    passwordTypeAgain,                          /**< 提示再次输入密码*/
};

@interface PDPasswordInputCell : UITableViewCell

@property (nonatomic,assign)passwordType transferType;          /**< 外面传入的类型*/
@property (nonatomic,assign)CGFloat transferCellHeight;         /**< 传入cell高度*/
@property (nonatomic,strong)UITextField *inputTextField;        /**< 输入内容*/

+(CGFloat)calculationCellHeight;                                /**<  cell高度*/

@end
