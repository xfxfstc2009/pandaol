//
//  PDLoginClauseCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,PDLoginClauseCellType) {
    PDLoginClauseCellType1,                 /**< 用户注册协议*/
    PDLoginClauseCellType2,                 /**< 盼达电竞将在您绑定手机号之前对您的账号进行限制*/
    PDLoginClauseCellType3,                 /**< 您的账号限制已经全部解除啦，快点参赛吧~*/
    PDLoginClauseCellType4,                 /**< 账号绑定成功，您的手机号已在平台进行注册，使用手机号登陆将会沿用您的原先手机账号密码*/
};

@interface PDLoginClauseCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;         /**< 传入cell高度*/
@property (nonatomic,assign)PDLoginClauseCellType transferType;

+(CGFloat)calculationCellHeightWithType:(PDLoginClauseCellType)type;            /**< 获取当前cell 高度*/
@end
