//
//  PDLoginClauseCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginClauseCell.h"

@interface PDLoginClauseCell()
@property (nonatomic,strong)UILabel *fixedLabel;            /**< 固定文字*/
@property (nonatomic,strong)UIButton *dymicButton;          /**< 动态文字*/

@end

@implementation PDLoginClauseCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.textColor = c4;
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    [self addSubview:self.fixedLabel];
    
    // 2. 创建按钮
    self.dymicButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.dymicButton setTitleColor:c4 forState:UIControlStateNormal];
    [self.dymicButton setTitle:@"《用户注册协议》" forState:UIControlStateNormal];
    self.dymicButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    __weak typeof(self)weakSelf = self;
    [weakSelf.dymicButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        NSLog(@"用户注册协议");
    }];
    [self addSubview:self.dymicButton];
}


-(void)setTransferType:(PDLoginClauseCellType)transferType{
    _transferType = transferType;
}



#pragma mark 设置cell 高度
-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    if (self.transferType == PDLoginClauseCellType1){               // 【注册协议】
        self.fixedLabel.text = @"点击‘注册’按钮，即表示您已同意";
        CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
        self.fixedLabel.hidden = NO;
        self.dymicButton.hidden = NO;
        CGSize dymicSize = [@"《用户注册协议》" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"提示"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
        CGFloat margin = (kScreenBounds.size.width - fixedSize.width - dymicSize.width ) / 2.;
        self.fixedLabel.frame = CGRectMake(margin, 0, fixedSize.width, transferCellHeight);
        self.dymicButton.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame), 0, dymicSize.width, transferCellHeight);
        
    } else if (self.transferType == PDLoginClauseCellType2){        // 【盼达电竞将在您未绑定手机号之前对您的账号进行一定的限制】
        self.fixedLabel.text = @"盼达电竞将在您未绑定手机号之前对您的账号进行一定的限制";
    } else if (self.transferType == PDLoginClauseCellType3){        // 【您的账号限制已经全部解除啦，快点参赛吧】
        self.fixedLabel.text = @"您的账号限制已经全部解除啦，快点参赛吧~";
    } else if (self.transferType == PDLoginClauseCellType4){
        self.fixedLabel.numberOfLines = 0;
        self.fixedLabel.text = @"账号绑定成功，您可以使用手机号登陆此账号";
    }

    // 【手机】
    if (self.transferType == PDLoginClauseCellType1){
        
    } else if (self.transferType == PDLoginClauseCellType2 || self.transferType == PDLoginClauseCellType3){
        self.dymicButton.hidden = YES;
        self.fixedLabel.hidden = NO;
        self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
        self.fixedLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width ,transferCellHeight);
        self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    } else if (self.transferType == PDLoginClauseCellType3){
        self.dymicButton.hidden = YES;
        self.fixedLabel.hidden = NO;
        self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
        CGSize contentOfSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(20), CGFLOAT_MAX)];
        self.fixedLabel.frame = CGRectMake(LCFloat(20), LCFloat(16), kScreenBounds.size.width - 2 * LCFloat(20), contentOfSize.height);
        self.fixedLabel.textAlignment = NSTextAlignmentLeft;
    } else if (self.transferType == PDLoginClauseCellType4){
        self.dymicButton.hidden = YES;
        self.fixedLabel.hidden = NO;
        self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
        self.fixedLabel.textColor = c2;
        CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(20), CGFLOAT_MAX)];
        self.fixedLabel.frame = CGRectMake(LCFloat(20), 16, kScreenBounds.size.width - 2 * LCFloat(20), fixedSize.height);
        self.fixedLabel.textAlignment = NSTextAlignmentLeft;
    }
}

+(CGFloat)calculationCellHeightWithType:(PDLoginClauseCellType)type{
    CGFloat cellHeight = 0;
    NSString *textString = @"";
    UIFont *font = [UIFont fontWithCustomerSizeName:@"小提示"];
    if (type == PDLoginClauseCellType1){
        textString = @"点击‘注册’按钮，即表示您已同意《用户注册协议》";
    } else if (type == PDLoginClauseCellType2){
        textString = @"盼达电竞将在您未绑定手机号之前对您的账号进行一定的限制";
    } else if (type == PDLoginClauseCellType3){
        textString = @"您的账号限制已经全部解除啦，快点参赛吧~";
    } else if (type == PDLoginClauseCellType4){
        textString = @"账号绑定成功，您的手机号已经在平台进行过注册，使用手机号登录将会沿用您原先的手机账号和密码";
    }
    
    CGSize constentSize = [textString sizeWithCalcFont:font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(20), CGFLOAT_MAX)];
    cellHeight += constentSize.height + 2 * LCFloat(11);
    return cellHeight;
}
@end
