//
//  PDPasswordInputCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPasswordInputCell.h"

@interface PDPasswordInputCell()<UITextFieldDelegate>
@property (nonatomic,strong)PDImageView *iconImgView;             /**< 输入iCon*/

@end

@implementation PDPasswordInputCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建icon
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.image = [UIImage imageNamed:@"icon_login_password"];
    [self addSubview:self.iconImgView];
    
    // 2. 创建输入框
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor clearColor];
    self.inputTextField.userInteractionEnabled = YES;
    self.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"提示"];
    [self addSubview:self.inputTextField];
    
    self.iconImgView.frame = CGRectMake(LCFloat(20) + LCFloat(18), (self.size_height - LCFloat(18)) / 2. , LCFloat(18), LCFloat(18));
    self.inputTextField.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(18), 0, kScreenBounds.size.width - CGRectGetMaxX(self.iconImgView.frame) - LCFloat(18) - LCFloat(20), self.size_height);
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferType:(passwordType)transferType{
    if (transferType == passwordTypeOne){
        self.inputTextField.placeholder = @"请输入密码";
    } else if (transferType == passwordTypeAgain){
        self.inputTextField.placeholder = @"请再次输入密码";
    }
}

#pragma mark - 传出高度
+(CGFloat)calculationCellHeight{
    return 44;
}
@end
