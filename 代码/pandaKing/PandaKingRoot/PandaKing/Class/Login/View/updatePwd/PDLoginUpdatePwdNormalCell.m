//
//  PDLoginUpdatePwdNormalCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginUpdatePwdNormalCell.h"

@interface PDLoginUpdatePwdNormalCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;

@end

@implementation PDLoginUpdatePwdNormalCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - cresteView
-(void)createView{
    // 固定
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.fixedLabel.textColor = [UIColor lightGrayColor];
    [self addSubview:self.fixedLabel];
    
    // 动态
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.dymicLabel.textColor = [UIColor lightGrayColor];
    [self addSubview:self.dymicLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransforPhoneNum:(NSString *)transforPhoneNum{
    _transforPhoneNum = transforPhoneNum;
    self.fixedLabel.text = @"账号";
    CGSize contentOfSize = [@"设置新密码" sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, contentOfSize.width, self.transferCellHeight);
    
    // 2. 密码
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(11), 0, kScreenBounds.size.width - CGRectGetMaxX(self.fixedLabel.frame) - LCFloat(11), self.transferCellHeight);
    self.dymicLabel.text = transforPhoneNum;
}



@end
