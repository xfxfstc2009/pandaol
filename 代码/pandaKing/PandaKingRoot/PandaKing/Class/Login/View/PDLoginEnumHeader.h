//
//  PDLoginEnumHeader.h
//  PandaKing
//
//  Created by Bluedaquiri on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#ifndef PDLoginEnumHeader_h
#define PDLoginEnumHeader_h

typedef NS_ENUM(NSInteger,AccountCellType) {
    AccountCellTypeNormal,                      /**< 普通的cell*/
    AccountCellTypeTimer,                       /**< 时间cell*/
};

typedef NS_ENUM(NSInteger,PDTimerType){
    PDPhoneRegisterTimer,             /**< 手机注册*/
    PDMailRegisterTimer,              /**< 邮箱注册*/
    PDPhonePasswordTimer,             /**< 手机找回*/
    PDMailPasswordTimer,              /**< 邮箱找回*/
    PDBindingTimer,                   /**< 三方登录后绑定手机号*/
    PDTimerNone,                      /**< 无定时器*/
};


#endif /* PDLoginEnumHeader_h */
