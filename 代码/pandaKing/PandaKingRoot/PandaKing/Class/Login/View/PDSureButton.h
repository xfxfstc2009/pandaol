//
//  PDButton.h
//  PandaChallenge
//
//  Created by Bluedaquiri on 16/3/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDSureButton : UIButton
@property (nonatomic,assign)CGFloat cornerRadius;
@property (nonatomic,assign)BOOL isEnable;
- (instancetype)initWithTitle:(NSString *)title WithActionBlock:(void(^)())actionBlock;
@end
