//
//  PDMineCodeButton.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/10/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDMineCodeButton : UIView

-(void)mineCodeActionClickBlock:(void(^)())block;

-(void)changeCodeImg:(NSString *)imgUrl;

@end
