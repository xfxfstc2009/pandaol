//
//  PDPhoneBindingHeaderCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLoginMainViewController.h"

@class PDLoginMainViewController;

typedef NS_ENUM(NSInteger,PDPhoneBindingHeaderCellType) {
    PDPhoneBindingHeaderCellTypePhone,                  /**< 手机绑定页面*/
    PDPhoneBindingHeaderCellTypePwd,                    /**< 手机绑定页面密码*/
    PDPhoneBindingHeaderCellTypeSuccess,                /**< 绑定成功页面*/
};


@interface PDPhoneBindingHeaderCell : UITableViewCell

@property (nonatomic,assign)PDPhoneBindingHeaderCellType transferPageType;      /**< 页面类型*/
@property (nonatomic,assign)PDThirdLoginType transferThirdLoginType;            /**< 三方类型*/
@property (nonatomic,assign)CGFloat transferCellHeight;                         /**< 传递过来的cell高度*/
+(CGFloat)calculationCellHeight;

-(void)showAnimationChangeWithType:(PDPhoneBindingHeaderCellType) type;
@end
