//
//  PDPhoneBindingOtherCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPhoneBindingHeaderCell.h"
#import "PDLoginPhoneBindingViewController.h"

@class PDLoginPhoneBindingViewController;
@class PDPhoneBindingHeaderCell;
@protocol PDPhoneBindingOtherCellDelegate <NSObject>

-(void)updatePhoneBindingHeaderType:(PDPhoneBindingHeaderCellType)pageType; /**< 当此处修改绑定类型了以后就修改页面*/
-(void)directToLoginHomePage;                                               /**< 因为无法进行绑定，跳转到登录页面*/
-(void)userMainScrollNotifyManager;
-(void)directToAppHomePage;                                                 /**< 进入首页*/

// 已有账号进行绑定
-(void)hasAccountAndBindingSuccessManager;
@end

@interface PDPhoneBindingOtherCell : UITableViewCell

@property (nonatomic,weak)id<PDPhoneBindingOtherCellDelegate> delegate;         /**< 页面代理*/
@property (nonatomic,assign)PDPhoneBindingHeaderCellType transferPageType;      /**< 页面类型*/
@property (nonatomic,assign)CGFloat transferCellHeight;                         /**< 传入高度*/
@property (nonatomic,assign)PDThirdLoginType transferThirdLoginType;            /**< 三方登录类型*/
@property (nonatomic,copy)NSString *transferOpen_Id;                            /**< 传递过来的id*/
@property (nonatomic,copy)NSString *transferAvatar;                             /**< 传递过来的头像*/
@property (nonatomic,copy)NSString *transferNick;                               /**< 传递过来的昵称*/


// 后来加的，三方账号进行登录
@property (nonatomic,assign)LoginPhoneBindingVCType bindingVCType;              /**< 整体页面*/



-(void)touchManager;

@end
