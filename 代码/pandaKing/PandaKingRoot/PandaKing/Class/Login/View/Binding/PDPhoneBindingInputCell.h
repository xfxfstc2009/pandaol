//
//  PDPhoneBindingInputCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMineCodeButton.h"

typedef NS_ENUM(NSInteger,PhoneBindingInputCellType) {
    PhoneBindingInputCellTypeUser = 0,                      /**< 输入账户*/
    PhoneBindingInputCellTypePhone ,                        /**< 输入手机号*/
    PhoneBindingInputCellTypeSmsCode,                       /**< 输入验证码*/
    PhoneBindingInputCellTypePwd,                           /**< 密码*/
    PhoneBindingInputCellTypePwdAgain,                      /**< 再次密码*/
    PhoneBindingInputCellTypeUpdatePwd,                     /**< 修改密码*/
    PhoneBindingInputCellTypeUpdatePwdAgain,                /**< 修改密码*/
    PhoneBindingInputCellTypeOldPwd,                        /**< 修改密码旧密码*/
    PhoneBindingInputCellTypeMineVCode,                     /**< 我们的验证码*/
};

@protocol PDPhoneBindingInputCellDelegate <NSObject>

-(void)inputTextConfirm:(PhoneBindingInputCellType)type textField:(UITextField *)textField status:(BOOL)status;     /**< 判断当前输入内容是否符合*/
-(void)vCodeClickManager;

@end

typedef void(^smsBtnClickManagerBlock)();

@interface PDPhoneBindingInputCell : UITableViewCell

@property (nonatomic,weak)id<PDPhoneBindingInputCellDelegate> delegate;         /**< 当前代理*/
@property (nonatomic,copy)smsBtnClickManagerBlock block;                        /**< 抛出的block*/
@property (nonatomic,assign)PhoneBindingInputCellType transferType;             /**< 传递过来的页面类型*/
@property (nonatomic,assign)CGFloat transferCellHeight;                         /**< 传递过来的cell高度*/

@property (nonatomic,strong)UITextField *inputTextField;                        /**< 输入信息*/

-(void)startTimer;                                                              /**< 开始计时器方法*/



// 2010-10-10
@property (nonatomic,strong)PDMineCodeButton *vCodebutton;

@end
