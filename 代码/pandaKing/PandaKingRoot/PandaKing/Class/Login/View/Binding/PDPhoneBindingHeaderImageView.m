//
//  PDPhoneBindingHeaderImageView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPhoneBindingHeaderImageView.h"

@interface PDPhoneBindingHeaderImageView(){
    CALayer  *_Layer;
}

@end

@implementation PDPhoneBindingHeaderImageView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        _duration = .4f;
        _Layer = self.layer;
    }
    return self;
}

@synthesize image = _image;

- (void)setImage:(UIImage *)image {
    if (_image != image) {
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"contents"];
        animation.fromValue         = (__bridge id)(_image.CGImage);
        animation.toValue           =  (__bridge id)(image.CGImage);
        animation.duration          = _duration;
        _Layer.contents             = (__bridge id)(image.CGImage);
        [_Layer addAnimation:animation forKey:nil];
        
        _image = image;
    }
}

- (UIImage *)image {
    return _image;
}


@end
