//
//  PDPhoneBindingInputCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPhoneBindingInputCell.h"
#import "PDCountDownButton.h"                                       /**< 倒计时按钮*/
#import "PDMineCodeButton.h"
@interface PDPhoneBindingInputCell()<UITextFieldDelegate>
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)PDImageView *iconImgView;               /*< icon**/
@property (nonatomic,strong)PDCountDownButton *daojishiBtn;         /**< 倒计时按钮*/
@property (nonatomic,strong)UIView *lineView;                       /**< 下划线*/
@property (nonatomic,strong)UIButton *eyeBtn;                       /**< 眼睛按钮*/


@end


@implementation PDPhoneBindingInputCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.userInteractionEnabled = YES;
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建icon
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    // 1.1 创建固定label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    [self addSubview:self.fixedLabel];
    
    
    // 2. 创建输入框
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor clearColor];
    self.inputTextField.userInteractionEnabled = YES;
    self.inputTextField.delegate = self;
    self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"提示"];
    [self.inputTextField addTarget:self action:@selector(textFieldWillChange) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.inputTextField];
    
    // 3. 倒计时
    __weak typeof(self)weakSelf = self;
    self.daojishiBtn = [[PDCountDownButton alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(98), 0, LCFloat(98), 44) daojishi:60 withBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.block){
            strongSelf.block();
        }
    }];
    [self addSubview:self.daojishiBtn];
    
    // 4. line
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [self addSubview:self.lineView];
    
    // 5. 添加眼睛
    self.eyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.eyeBtn.backgroundColor = [UIColor clearColor];
    [self.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwd_close"] forState:UIControlStateNormal];
    self.eyeBtn.selected = NO;
    [self.eyeBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        self.eyeBtn.selected = !self.eyeBtn.selected;
        if (weakSelf.eyeBtn.selected){
            [weakSelf.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwd_open"] forState:UIControlStateNormal];
            self.inputTextField.secureTextEntry = NO;
        } else {
            [weakSelf.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwd_close"] forState:UIControlStateNormal];
            self.inputTextField.secureTextEntry = YES;
        }
    }];
    [self addSubview:self.eyeBtn];
    
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    
    // icon
    self.iconImgView.frame = CGRectMake(LCFloat(20) + LCFloat(18), (transferCellHeight - LCFloat(18)) / 2. , LCFloat(18), LCFloat(18));
    
    CGSize contentOfSize = [@"请输入账号:" sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, transferCellHeight)];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, contentOfSize.width, transferCellHeight);
    
    // textField
    self.inputTextField.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(18), 0, kScreenBounds.size.width - CGRectGetMaxX(self.iconImgView.frame) - LCFloat(18) - LCFloat(20), transferCellHeight);
    
    self.daojishiBtn.hidden = YES;
    self.lineView.hidden = YES;
    
    if (self.transferType == PhoneBindingInputCellTypeUser){            // 【账户】
        self.fixedLabel.hidden = YES;
        self.iconImgView.image = [UIImage imageNamed:@"icon_login_user"];
        self.inputTextField.placeholder = @"请输入您的手机";
        self.inputTextField.keyboardType = UIKeyboardTypeTwitter;
        self.eyeBtn.hidden = YES;
    } if (self.transferType == PhoneBindingInputCellTypePhone){        // 【手机】
        self.fixedLabel.hidden = YES;
        self.iconImgView.image = [UIImage imageNamed:@"icon_login_bindingInutr_phone"];
        self.inputTextField.placeholder = @"手机号";
        self.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
        self.eyeBtn.hidden = YES;
    } else if (self.transferType == PhoneBindingInputCellTypeSmsCode){  // 【验证码】
        self.fixedLabel.hidden = YES;
        self.eyeBtn.hidden = YES;
        self.daojishiBtn.hidden = NO;
        self.lineView.hidden = NO;
        self.daojishiBtn.size_height = transferCellHeight;
        self.lineView.frame = CGRectMake(self.daojishiBtn.orgin_x - 1, LCFloat(5), .5f, transferCellHeight - 2 * LCFloat(5));

        self.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
        self.iconImgView.image = [UIImage imageNamed:@"icon_login_binding_sms"];
        self.inputTextField.placeholder = @"请输入验证码";
    } else if (self.transferType == PhoneBindingInputCellTypePwd || self.transferType == PhoneBindingInputCellTypePwdAgain){      // 【密码】
        self.fixedLabel.hidden = YES;
        self.iconImgView.image = [UIImage imageNamed:@"icon_login_binding_pwdS"];
        self.inputTextField.placeholder = (self.transferType == PhoneBindingInputCellTypePwd?@"请设置密码":@"请再次输入密码");
        self.inputTextField.clearButtonMode = UITextFieldViewModeAlways;
        self.inputTextField.keyboardType = UIKeyboardTypeEmailAddress;
        self.inputTextField.secureTextEntry = YES;
        
        // 2. 增加眼睛
        self.eyeBtn.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(18) - transferCellHeight, 0, transferCellHeight, transferCellHeight);
        self.eyeBtn.hidden = NO;
        // 3. 输入框移动
        self.inputTextField.size_width = self.eyeBtn.orgin_x - self.inputTextField.orgin_x;
    } else if (self.transferType == PhoneBindingInputCellTypeUpdatePwd || self.transferType == PhoneBindingInputCellTypeUpdatePwdAgain || self.transferType == PhoneBindingInputCellTypeOldPwd){
        self.fixedLabel.hidden = NO;
        self.iconImgView.hidden = YES;
        if (self.transferType == PhoneBindingInputCellTypeOldPwd){
            self.inputTextField.placeholder = @"请输入当前账户密码";
            self.fixedLabel.text = @"当前密码:";
        } else if (self.transferType == PhoneBindingInputCellTypeUpdatePwd){
            self.inputTextField.placeholder = @"请输入设置账户新密码";
            self.fixedLabel.text = @"设置新密码:";
        } else if (self.transferType == PhoneBindingInputCellTypeUpdatePwdAgain){
            self.inputTextField.placeholder = @"请再次确认输入设置账户新密码";
            self.fixedLabel.text = @"确认新密码:";
        }
        self.inputTextField.orgin_x = CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(11);
        self.eyeBtn.orgin_x = CGRectGetMaxX(self.inputTextField.frame);
        
        self.inputTextField.clearButtonMode = UITextFieldViewModeAlways;
        self.inputTextField.keyboardType = UIKeyboardTypeEmailAddress;
        self.inputTextField.secureTextEntry = YES;
        
        // 2. 增加眼睛
        self.eyeBtn.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(18) - transferCellHeight, 0, transferCellHeight, transferCellHeight);
        self.eyeBtn.hidden = NO;
        // 3. 输入框移动
        self.inputTextField.size_width = self.eyeBtn.orgin_x - self.inputTextField.orgin_x;
    } else if (self.transferType == PhoneBindingInputCellTypeMineVCode){
        self.fixedLabel.hidden = YES;
        self.eyeBtn.hidden = YES;
        self.daojishiBtn.hidden = YES;
        self.lineView.hidden = NO;
        self.daojishiBtn.size_height = transferCellHeight;
        self.lineView.frame = CGRectMake(self.daojishiBtn.orgin_x - 1, LCFloat(5), .5f, transferCellHeight - 2 * LCFloat(5));
        
        self.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
        self.iconImgView.image = [UIImage imageNamed:@"icon_login_mineCode"];
        self.inputTextField.placeholder = @"请输入图形验证码";
        
        if (!self.vCodebutton){
            self.vCodebutton = [[PDMineCodeButton alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(20) - LCFloat(98), 0, LCFloat(98), 44) ];
            [self addSubview:self.vCodebutton];
            
            __weak typeof(self)weakSelf = self;
            [self.vCodebutton mineCodeActionClickBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(vCodeClickManager)]){
                    [strongSelf.delegate vCodeClickManager];
                }
            }];
            
            self.vCodebutton.backgroundColor = [UIColor clearColor];
        }
    }
}


-(void)setTransferType:(PhoneBindingInputCellType)transferType{
    _transferType = transferType;
}


#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]){
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (self.transferType == PhoneBindingInputCellTypeUser){                // 用户
        if (toBeString.length > 20){
            textField.text = [toBeString substringToIndex:20];
            return NO;
        }
        if (toBeString.length >= 5 && toBeString.length <= 20){
            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
            }
        } else {
            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:NO];
            }
        }
    } else if (self.transferType == PhoneBindingInputCellTypePhone){           // 手机号
        if (toBeString.length > 11){
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
        if (toBeString.length == 11 && [Tool validateMobile:toBeString]){
            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
            }
        } else {
            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:NO];
            }
        }
    } else if (self.transferType == PhoneBindingInputCellTypePwd){      // 密码
        if (toBeString.length > 18){
            textField.text = [toBeString substringToIndex:18];
            return NO;
        } else if (toBeString.length >= 6 && toBeString.length <= 18){
            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
            }
        } else {
            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:NO];
            }
        }
    } else if (self.transferType == PhoneBindingInputCellTypePwdAgain || self.transferType == PhoneBindingInputCellTypeUpdatePwd || self.transferType == PhoneBindingInputCellTypeUpdatePwdAgain || self.transferType == PhoneBindingInputCellTypeOldPwd){ // 再次密码
        if (toBeString.length > 18){
            textField.text = [toBeString substringToIndex:18];
            return NO;
        } else if (toBeString.length >= 6 && toBeString.length <= 18){
            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
            }
        } else {
            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:NO];
            }
        }
    } else if (self.transferType == PhoneBindingInputCellTypeSmsCode || self.transferType == PhoneBindingInputCellTypeMineVCode){  // 验证码
        if (toBeString.length > 4){
            textField.text = [toBeString substringToIndex:4];
            return NO;
        } else if (toBeString.length == 4){
            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
            }
        }
    }
    return YES;
}

-(void)textFieldWillChange{
    BOOL success = NO;
    if (self.transferType == PhoneBindingInputCellTypeUser){
        if (self.inputTextField.text.length >= 5){                               // 【账户】
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.transferType == PhoneBindingInputCellTypePhone){           // 【手机号】
        if (self.inputTextField.text.length == 11 && ([Tool validateMobile:self.inputTextField.text])){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.transferType == PhoneBindingInputCellTypePwd){              // 【密码】
        if (self.inputTextField.text.length >= 6 && self.inputTextField.text.length <= 18){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.transferType == PhoneBindingInputCellTypePwdAgain || self.transferType == PhoneBindingInputCellTypeUpdatePwd || self.transferType == PhoneBindingInputCellTypeUpdatePwdAgain || self.transferType == PhoneBindingInputCellTypeOldPwd){         // 【再次密码】
        if (self.inputTextField.text.length >= 6 && self.inputTextField.text.length <= 18){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.transferType == PhoneBindingInputCellTypeSmsCode || self.transferType == PhoneBindingInputCellTypeMineVCode){          // 【验证码】
        if (self.inputTextField.text.length == 4){
            [AccountModel sharedAccountModel].resetVerifyCode = self.inputTextField.text;
            success = YES;
        } else {
            success = NO;
        }
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
        [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:success];
    }
}


-(void)startTimer{
    [self.daojishiBtn startTimer];
}
@end
