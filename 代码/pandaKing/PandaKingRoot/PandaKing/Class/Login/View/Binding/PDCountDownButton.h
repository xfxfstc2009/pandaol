//
//  PDCountDownButton.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 【倒计时按钮】
#import <UIKit/UIKit.h>

@interface PDCountDownButton : UIView

-(instancetype)initWithFrame:(CGRect)frame daojishi:(NSInteger)daojishi withBlock:(void(^)())block;

-(void)startTimer;                  // 开始动画

@end
