//
//  PDBindingButtonCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingButtonCell.h"

@interface PDBindingButtonCell()

@end

@implementation PDBindingButtonCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.userInteractionEnabled = YES;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.cusomterButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cusomterButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    self.cusomterButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.cusomterButton.frame = CGRectMake(0, 0, kScreenBounds.size.width, 44);
    self.cusomterButton.userInteractionEnabled = YES;
    [self addSubview:self.cusomterButton];
    
}

-(void)setBingdingType:(PDBindingButtonCellType)bingdingType{
    _bingdingType = bingdingType;
    if (bingdingType == PDBindingButtonCellTypeNext){
        [self.cusomterButton setTitle:@"下一步" forState:UIControlStateNormal];
    } else if (bingdingType == PDBindingButtonCellTypeSend){
        [self.cusomterButton setTitle:@"提交" forState:UIControlStateNormal];
    } else if (bingdingType == PDBindingButtonCellInHome){
        [self.cusomterButton setTitle:@"进入首页" forState:UIControlStateNormal];
    } else if (bingdingType == PDBindingButtonCellInRegister){
        [self.cusomterButton setTitle:@"注册" forState:UIControlStateNormal];
    } else if (bingdingType == PDBindingButtonCellFindPwd){
        [self.cusomterButton setTitle:@"找回密码" forState:UIControlStateNormal];
    }
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    
    // button Frame
    self.cusomterButton.frame = CGRectMake(LCFloat(20), 0, kScreenBounds.size.width - 2 * LCFloat(20), transferCellHeight);
}



-(void)showEnableWithStatus:(BOOL)status{
    if (status == YES){
        self.cusomterButton.backgroundColor = c7;
        self.cusomterButton.enabled = YES;
    } else {
        self.cusomterButton.backgroundColor = c4;
        self.cusomterButton.enabled = NO;
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight = 44;
    return cellHeight;
}

@end
