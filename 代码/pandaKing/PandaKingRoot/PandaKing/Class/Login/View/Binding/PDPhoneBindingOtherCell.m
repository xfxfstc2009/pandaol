//
//  PDPhoneBindingOtherCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPhoneBindingOtherCell.h"
#import "PDPhoneBindingInputCell.h"
#import "PDBindingButtonCell.h"
#import "PDLoginClauseCell.h"                   // 信息
#import "PDBindingCheckModel.h"                 // 绑定校验接口
#import "PDThirdLoginModel.h"
#import "PDThirdLoginSessionModel.h"
#import "NetworkAdapter+PDJavaLogin.h"

@interface PDPhoneBindingOtherCell()<UITableViewDataSource,UITableViewDelegate,PDPhoneBindingInputCellDelegate>
@property (nonatomic,strong)UIScrollView *mainScrollView;           /**< 主scrollView*/
@property (nonatomic,strong)UITableView *tableView1;                /**< 列表1*/
@property (nonatomic,strong)UITableView *tableView2;                /**< 列表2*/
@property (nonatomic,strong)UITableView *tableView3;                /**< 列表3*/
@property (nonatomic,strong)NSArray *dataSourceArr1;                /**< 数据1*/
@property (nonatomic,strong)NSArray *dataSourceArr2;                /**< 数据2*/
@property (nonatomic,strong)NSArray *dataSourceArr3;                /**< 数据3*/

// 【temp】
@property (nonatomic,strong)UITextField *tempPhoneTextField;
@property (nonatomic,assign)BOOL tempPhoneEnable;

@property (nonatomic,strong)UITextField *tempSmsTextField;
@property (nonatomic,assign)BOOL tempSmsEnable;

@property (nonatomic,strong)PDBindingButtonCell *tempCellWithRowThr;

@property (nonatomic,strong)UITextField *tempPwdTextField;
@property (nonatomic,assign)BOOL tempPwdEnable;

@property (nonatomic,strong)UITextField *tempPwdAgainTextField;
@property (nonatomic,assign)BOOL tempPwdAgainEnable;

@property (nonatomic,strong)PDBindingButtonCell *tempCellWithTabTwo;

@end

@implementation PDPhoneBindingOtherCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.userInteractionEnabled = YES;
        [self arrayWithInit];
        [self createView];
    }
    return self;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.dataSourceArr1 = @[@[@"手机号"],@[@"验证码"],@[@"下一步"],@[@"手机提示"]];
    self.dataSourceArr2 = @[@[@"密码"],@[@"再次密码"],@[@"提交"]];
    self.dataSourceArr3 = @[@[@"成功提示"],@[@"进入首页"],@[@"成功提示1"]];
}

#pragma mark - createView
-(void)createView{
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        self.mainScrollView.userInteractionEnabled = YES;
        self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 300);
        self.mainScrollView.delegate = self;
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.backgroundColor = [UIColor whiteColor];
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.scrollEnabled = NO;
        [self addSubview:self.mainScrollView];
        
    }
    
    if (!self.tableView1){
        self.tableView1 = [self createTableView];
        self.tableView1.orgin_x = 0;
    }
    if (!self.tableView2){
        self.tableView2 = [self createTableView];
        self.tableView2.orgin_x = kScreenBounds.size.width;
    }
    if (!self.tableView3){
        self.tableView3 = [self createTableView];
        self.tableView3.orgin_x = 2 * kScreenBounds.size.width;
    }
}

#pragma mark - UITableView
-(UITableView *)createTableView{
    UITableView *loginTableView = [[UITableView alloc]initWithFrame:self.mainScrollView.bounds style:UITableViewStylePlain];
    loginTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    loginTableView.userInteractionEnabled = YES;
    loginTableView.delegate = self;
    loginTableView.dataSource = self;
    loginTableView.backgroundColor = [UIColor clearColor];
    loginTableView.showsVerticalScrollIndicator = NO;
    loginTableView.scrollEnabled = YES;
    loginTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.mainScrollView addSubview:loginTableView];
    return loginTableView;
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.tableView1) {
        return self.dataSourceArr1.count;
    } else if (tableView == self.tableView2){
        return self.dataSourceArr2.count;
    } else if (tableView == self.tableView3){
        return self.dataSourceArr3.count;
    }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr ;
    if (tableView == self.tableView1){
        sectionOfArr = [self.dataSourceArr1 objectAtIndex:section];
    } else if (tableView == self.tableView2){
        sectionOfArr = [self.dataSourceArr2 objectAtIndex:section];
    } else if (tableView == self.tableView3){
        sectionOfArr = [self.dataSourceArr3 objectAtIndex:section];
    }
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (tableView == self.tableView1){
        if (indexPath.section == 0 && indexPath.row == 0){                     // 【输入手机号 】
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            PDPhoneBindingInputCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[PDPhoneBindingInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.delegate = self;
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                
            }
            cellWithRowOne.transferType = PhoneBindingInputCellTypePhone;
            cellWithRowOne.transferCellHeight = cellHeight;
            return cellWithRowOne;
        } else if (indexPath.section == 1 && indexPath.row == 0){              // 【输入验证码】
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            PDPhoneBindingInputCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[PDPhoneBindingInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.delegate = self;
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                
                __weak typeof(self)weakSelf = self;
                cellWithRowTwo.block = ^(){
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf verificationPhoneNumber];
                };
            }
            cellWithRowTwo.transferType = PhoneBindingInputCellTypeSmsCode;
            cellWithRowTwo.transferCellHeight = cellHeight;
            
            return cellWithRowTwo;
        } else if (indexPath.section == 2 && indexPath.row == 0){               // 【输入 下一步】
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            PDBindingButtonCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[PDBindingButtonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
                self.tempCellWithRowThr = cellWithRowThr;
                
            }
            cellWithRowThr.bingdingType = PDBindingButtonCellTypeNext;
            cellWithRowThr.transferCellHeight = cellHeight;
            [cellWithRowThr showEnableWithStatus:NO];
            __weak typeof(self)weakSelf = self;
            [cellWithRowThr.cusomterButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf touchManagerToNext];
            }];
            
            return cellWithRowThr;
        } else if (indexPath.section == 3 && indexPath.row == 0){
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            PDLoginClauseCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[PDLoginClauseCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFour.transferType = PDLoginClauseCellType2;
            cellWithRowFour.transferCellHeight = cellHeight;
            
            return cellWithRowFour;
        }
    } else if (tableView == self.tableView2){
        if (indexPath.section == 0 && indexPath.row == 0){
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            PDPhoneBindingInputCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[PDPhoneBindingInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.delegate = self;
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferType = PhoneBindingInputCellTypePwd;
            cellWithRowFiv.transferCellHeight = cellHeight;
            
            return cellWithRowFiv;
        } else if (indexPath.section == 1 && indexPath.row == 0){
            static NSString *cellIdentifyWithRowSix = @"cellIdentifyWithRowSix";
            PDPhoneBindingInputCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
            if (!cellWithRowSix){
                cellWithRowSix = [[PDPhoneBindingInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
                cellWithRowSix.delegate = self;
                cellWithRowSix.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowSix.transferType = PhoneBindingInputCellTypePwdAgain;
            cellWithRowSix.transferCellHeight = cellHeight;
            
            return cellWithRowSix;
        } else if (indexPath.section == 2 && indexPath.row == 0){
            static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
            PDBindingButtonCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
            if (!cellWithRowSev){
                cellWithRowSev = [[PDBindingButtonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
                cellWithRowSev.selectionStyle = UITableViewCellSelectionStyleNone;
                [cellWithRowSev showEnableWithStatus:NO];
            }
            cellWithRowSev.bingdingType = PDBindingButtonCellTypeSend;
            cellWithRowSev.transferCellHeight = cellHeight;
            self.tempCellWithTabTwo = cellWithRowSev;
            __weak typeof(self)weakSelf = self;
            [cellWithRowSev.cusomterButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (self.bindingVCType == LoginPhoneBindingVCTypeBinding){
                    [strongSelf sendRequestToBindingPageType];
                } else {
                    [strongSelf sendRequestToRegisterAndBinding];
                }
            }];
            
            return cellWithRowSev;
        }
    } else if (tableView == self.tableView3){
        if (indexPath.section == 0 && indexPath.row == 0){
            static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
            PDLoginClauseCell *cellWithRowEig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
            if (!cellWithRowEig){
                cellWithRowEig = [[PDLoginClauseCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
                cellWithRowEig.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowEig.transferType = PDLoginClauseCellType4;
            cellWithRowEig.transferCellHeight = cellHeight;
            
            return cellWithRowEig;
        } else if (indexPath.section == 1 && indexPath.row == 0){
            static NSString *cellIdentifyWithRowNig = @"cellIdentifyWithRowNig";
            PDBindingButtonCell *cellWithRowNig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowNig];
            if (!cellWithRowNig){
                cellWithRowNig = [[PDBindingButtonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowNig];
                cellWithRowNig.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowNig.bingdingType = PDBindingButtonCellInHome;
            cellWithRowNig.transferCellHeight = cellHeight;
            [cellWithRowNig showEnableWithStatus:YES];
            __weak typeof(self)weakSelf = self;
            [cellWithRowNig.cusomterButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(directToAppHomePage)]){
                    [strongSelf.delegate directToAppHomePage];
                }
            }];
            
            return cellWithRowNig;
        } else if (indexPath.section == 2 && indexPath.row == 0){
            static NSString *cellIdentifyWithRowTen = @"cellIdentifyWithRowTen";
            PDLoginClauseCell *cellWithRowTen = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTen];
            if (!cellWithRowTen){
                cellWithRowTen = [[PDLoginClauseCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTen];
                cellWithRowTen.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTen.transferType = PDLoginClauseCellType3;
            cellWithRowTen.transferCellHeight = cellHeight;
            
            return cellWithRowTen;
        }
    } else {
        static NSString *cellIdentifyWithRowTen = @"cellIdentifyWithRowTen";
        UITableViewCell *cellWithRowTen = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTen];
        if (!cellWithRowTen){
            cellWithRowTen = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTen];
            cellWithRowTen.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        return cellWithRowTen;
        
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.tableView1) {
        if (indexPath.section == 0 || indexPath.section == 1){
            SeparatorType separatorType = SeparatorTypeBottom;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"login"];
        }
    } else if (tableView == self.tableView2){
        if (indexPath.section == 0 || indexPath.section == 1){
            SeparatorType separatorType = SeparatorTypeBottom;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"login"];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.tableView1){
        if (section == 1 || section == 0){
            return 16;
        } else if (section == 2){
            return 44;
        } else if (section == 3){
            return 18;
        }
    } else if (tableView == self.tableView2){
        if (section == 0){
            return 30;
        } else if (section == 1){
            return 16;
        } else if (section == 2){
            return 33;
        }
    } else if (tableView == self.tableView3){
        if (section == 0){
            return 16;
        } else if (section == 1){
            return 180;
        } else if (section == 2){
            return 18;
        }
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - SET
-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.mainScrollView.size_height = transferCellHeight;
    self.tableView1.size_height = transferCellHeight;
    self.tableView2.size_height = transferCellHeight;
    self.tableView3.size_height = transferCellHeight;
}

-(void)setTransferPageType:(PDPhoneBindingHeaderCellType)transferPageType{
    //    [self typeManagerWithType:transferPageType];
}

// 三方登录类型
-(void)setTransferThirdLoginType:(PDThirdLoginType)transferThirdLoginType{
    _transferThirdLoginType = transferThirdLoginType;
}

#pragma mark - Interface

#pragma mark 绑定判断手机号码
-(void)verificationPhoneNumber{
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [cellWithRowTwo startTimer];                // 开启倒计时
    NSString *err = @"";
    if (!cellWithRowOne.inputTextField.text.length){
        err = @"请输入手机号码";
    } else {
        if (![Tool validateMobile:cellWithRowOne.inputTextField.text]){
            err = @"请确认输入的手机号是否正确";
        }
    }
    
    if (err.length){
        [[PDAlertView sharedAlertView]showError:err];
    }
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetVCodeWithAccount:cellWithRowOne.inputTextField.text];
}

#pragma mark 发送验证码
-(void)sendRequestToGetVCodeWithAccount:(NSString *)account{
    [self touchManager];
    NSString *alert = @"正在发送验证码";
    [PDHUD showHUDProgress:alert diary:0];
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:thirdBindCode requestParams:@{@"account":account,@"type":@(UserLoginTypeThird)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [PDHUD showHUDProgress:@"验证码发送成功" diary:3];
            PDPhoneBindingInputCell *cell = (PDPhoneBindingInputCell *)[strongSelf.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
            [cell.inputTextField becomeFirstResponder];
        } else {
            [[UIAlertView alertViewWithTitle:@"短信发送失败" message:@"是否重新发送" buttonTitles:@[@"取消",@"重新发送"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 1){
                    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[strongSelf.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
                    [strongSelf sendRequestToGetVCodeWithAccount:cellWithRowOne.inputTextField.text];
                }
            }]show];
        }
    }];
}


#pragma mark - touchManager
// 点击进行下一步操作
-(void)touchManagerToNext{
    [self touchManager];
    // 1. 判断是否能进行绑定
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToUserBindCheckWithBlock:^(BindingCheckType type) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (type == BindingCheckTypeNo){
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"账号无法绑定" conten:@"无法绑定，账号已被绑定,请返回登录" isClose:NO btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                if (strongSelf.delegate && [self.delegate respondsToSelector:@selector(directToLoginHomePage)]){
                    [strongSelf.delegate directToLoginHomePage];
                }
                [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
            }];
        } else if (type == BindingCheckTypeHas){                // 【账号存在，直接绑定】
            [strongSelf verificationPhoneNumber1WithDirect:1];
        } else if (type == BindingCheckTypeSuccess){            // 【账号不存在，跳转到注册页面】
            [strongSelf verificationPhoneNumber1WithDirect:2];
        }
    }];
}

#pragma mark 1、手机号码验证是否能绑定
-(void)sendRequestToUserBindCheckWithBlock:(void(^)(BindingCheckType type))block{
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    NSString *err = @"";
    if (!cellWithRowOne.inputTextField.text.length){
        err = @"请输入手机号码";
    } else {
        if (![Tool validateMobile:cellWithRowOne.inputTextField.text]){
            err = @"请确认输入的手机号是否正确";
        }
    }
    
    if (err.length){
        [[PDAlertView sharedAlertView]showError:err];
    }
    __weak typeof(self)weakSelf = self;
    
    NSString *alert = @"正在拼命验证验证码是否正确";
    [PDHUD showHUDProgress:alert diary:0];
    [[NetworkAdapter sharedAdapter] fetchWithPath:userBindCheck requestParams:@{@"type":@(self.transferThirdLoginType),@"account":self.tempPhoneTextField.text,@"code":cellWithRowTwo.inputTextField.text} responseObjectClass:[PDBindingCheckModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [PDHUD showHUDProgress:alert diary:2];
            PDBindingCheckModel *checkModel = (PDBindingCheckModel *)responseObject;
            if (block){
                block(checkModel.type);
            }
        } else {
            
        }
    }];
}

#pragma mark 绑定判断手机号码 && 验证手机号码
-(void)verificationPhoneNumber1WithDirect:(NSInteger)index{
//    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
//    NSString *err = @"";
//    if (!cellWithRowOne.inputTextField.text.length){
//        err = @"请输入手机号码";
//    } else {
//        if (![Tool validateMobile:cellWithRowOne.inputTextField.text]){
//            err = @"请确认输入的手机号是否正确";
//        }
//    }
//    
//    if (err.length){
//        [[PDAlertView sharedAlertView]showError:err];
//    }
//    __weak typeof(self)weakSelf = self;
//    [weakSelf sendRequestToVCodeWithAccount:cellWithRowOne.inputTextField.text code:cellWithRowTwo.inputTextField.text block:^{
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
        [cellWithRowTwo startTimer];                // 开启倒计时
        if (index == 1){                                // 【账号存在，直接绑定】
            [self sendRequestToBinding];
            [self scrollScrollTopage:2];
        } else if (index == 2){                         // 账号不存在，直接绑定
            if (self.delegate && [self.delegate respondsToSelector:@selector(updatePhoneBindingHeaderType:)]){
                [self.delegate updatePhoneBindingHeaderType:PDPhoneBindingHeaderCellTypePwd];
            }
            [self scrollScrollTopage:1];
        }
//    }];
}

#pragma mark 验证手机号码
-(void)sendRequestToVCodeWithAccount:(NSString *)account code:(NSString *)code block:(void(^)())block{
    NSString *alert = @"熊猫君正在拼命验证中…";
    [PDHUD showHUDProgress:alert diary:0];
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]fetchWithPath:userVcode requestParams:@{@"account":account,@"code":code,@"type":@(UserLoginTypeThird)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [PDHUD showHUDProgress:alert diary:2];
            if (block){
                block();
            }
        }
    }];
}

#pragma mark 2. 三方绑定接口
-(void)sendRequestToBinding{
    NSString *alert = @"熊猫君正在拼命绑定…";
    [PDHUD showHUDProgress:alert diary:0];
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    NSString *account = cellWithRowOne.inputTextField.text;
    NSString *code = cellWithRowTwo.inputTextField.text;
    
    NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:@{@"account":account,@"openId":self.transferOpen_Id,@"code":code}];
    if (self.transferThirdLoginType == PDThirdLoginTypeWechat){
        [mutableDic setObject:self.transferOpen_Id forKey:@"openId"];
        [mutableDic setObject:@"2" forKey:@"type"];
    } else if (self.transferThirdLoginType == PDThirdLoginTypeQQ){
        [mutableDic setObject:self.transferOpen_Id forKey:@"openId"];
        [mutableDic setObject:@"1" forKey:@"type"];
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]fetchWithPath:usetOutBind requestParams:mutableDic  responseObjectClass:[PDThirdLoginSessionModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [PDHUD showHUDProgress:@"绑定成功" diary:2];
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(updatePhoneBindingHeaderType:)]){
                [strongSelf.delegate updatePhoneBindingHeaderType:PDPhoneBindingHeaderCellTypeSuccess];
            }
            
            // 长连接
            PDThirdLoginSessionModel *thirdLoginModel = (PDThirdLoginSessionModel *)responseObject;
            [[AccountModel sharedAccountModel] thirdLoginManagerWithModel:thirdLoginModel];
        } else {
            
        }
    }];
}

#pragma mark 进行注册
-(void)sendRequestToRegisterAndBinding{
    [self touchManager];
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    PDPhoneBindingInputCell *cellWithRowThr = (PDPhoneBindingInputCell *)[self.tableView2 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowFour = (PDPhoneBindingInputCell *)[self.tableView2 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    
    NSString *err = @"";
    if (!cellWithRowThr.inputTextField.text.length){
        err = @"请输入密码";
    } else if (!cellWithRowFour.inputTextField.text.length){
        err = @"请再次输入密码";
    } else if (![cellWithRowFour.inputTextField.text isEqualToString:cellWithRowThr.inputTextField.text]){
        err = @"请确认两次密码是否一致";
    }
    if(err.length){
        [[PDAlertView sharedAlertView]showError:err];
        return;
    }
    
    NSString *account = cellWithRowOne.inputTextField.text;
    NSString *code = cellWithRowTwo.inputTextField.text;
    NSString *password = cellWithRowThr.inputTextField.text;
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *mutableDic = [NSMutableDictionary dictionaryWithDictionary:@{@"account":account,@"code":code,@"password":password}];
    if (self.transferThirdLoginType == PDThirdLoginTypeWechat){
        [mutableDic setObject:self.transferOpen_Id forKey:@"openId"];
    } else if (self.transferThirdLoginType == PDThirdLoginTypeQQ){
        [mutableDic setObject:self.transferOpen_Id forKey:@"openId"];
    }
    if (self.transferAvatar.length){
        [mutableDic setObject:self.transferAvatar forKey:@"avatar"];
    }
    if (self.transferNick.length){
        [mutableDic setObject:self.transferNick forKey:@"nickName"];
    }
    
    [mutableDic setObject:@(self.transferThirdLoginType) forKey:@"type"];
    
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:userBind requestParams:mutableDic responseObjectClass:[PDThirdLoginSessionModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf scrollScrollTopage:2];
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(updatePhoneBindingHeaderType:)]){
                [strongSelf.delegate updatePhoneBindingHeaderType:PDPhoneBindingHeaderCellTypeSuccess];
            }
            
            // 连接socket
            PDThirdLoginSessionModel *loginModel = (PDThirdLoginSessionModel *)responseObject;
            
            [[AccountModel sharedAccountModel] thirdLoginManagerWithModel:loginModel];
        } else {
            
        }
    }];
}



#pragma mark - OtherManager
-(void)scrollScrollTopage:(NSInteger)page{
    [self.mainScrollView setContentOffset:CGPointMake(page * kScreenBounds.size.width, 0) animated:YES];
    if (self.delegate && [self.delegate respondsToSelector:@selector(userMainScrollNotifyManager)]){
        [self.delegate userMainScrollNotifyManager];
    }
}

-(void)touchManager{
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    PDPhoneBindingInputCell *cellWithRowThr = (PDPhoneBindingInputCell *)[self.tableView2 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowFour = (PDPhoneBindingInputCell *)[self.tableView2 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if ([cellWithRowOne.inputTextField isFirstResponder] && cellWithRowOne.inputTextField){
        [cellWithRowOne.inputTextField resignFirstResponder];
    } else if ([cellWithRowTwo.inputTextField isFirstResponder] && cellWithRowTwo.inputTextField){
        [cellWithRowTwo.inputTextField resignFirstResponder];
    } else if ([cellWithRowThr.inputTextField isFirstResponder] && cellWithRowThr.inputTextField){
        [cellWithRowThr.inputTextField resignFirstResponder];
    } else if ([cellWithRowFour.inputTextField isFirstResponder] && cellWithRowFour.inputTextField){
        [cellWithRowFour.inputTextField resignFirstResponder];
    }
}

#pragma mark - PDPhoneBindingInputCellDelegate
-(void)inputTextConfirm:(PhoneBindingInputCellType)type textField:(UITextField *)textField status:(BOOL)status{
    if (type == PhoneBindingInputCellTypePhone){        // 手机
        if (status == YES){
            self.tempPhoneTextField = textField;
            self.tempPhoneEnable = YES;
            // 判断短信验证码是否通过
            if (self.tempSmsEnable == YES){
                [self.tempCellWithRowThr showEnableWithStatus:YES];
            } else {
                [self.tempCellWithRowThr showEnableWithStatus:NO];
            }
        } else {
            self.tempPhoneEnable = NO;
            [self.tempCellWithRowThr showEnableWithStatus:NO];
        }
    } else if (type == PhoneBindingInputCellTypeSmsCode){   // 短信验证码
        if (status == YES){
            self.tempSmsTextField = textField;
            self.tempSmsEnable = YES;
            if (self.tempPhoneEnable == YES){
                [self.tempCellWithRowThr showEnableWithStatus:YES];
            } else {
                [self.tempCellWithRowThr showEnableWithStatus:NO];
            }
        } else {
            self.tempSmsEnable = NO;
            [self.tempCellWithRowThr showEnableWithStatus:NO];
        }
    } else if (type == PhoneBindingInputCellTypePwd){   // 密码
        if (status == YES){
            self.tempPwdTextField = textField;
            self.tempPwdEnable = YES;
            if (self.tempPwdAgainEnable == YES && [textField.text isEqualToString:self.tempPwdAgainTextField.text]){
                [self.tempCellWithTabTwo showEnableWithStatus:YES];
            } else {
                [self.tempCellWithTabTwo showEnableWithStatus:NO];
            }
        } else {
            self.tempPwdEnable = NO;
            [self.tempCellWithTabTwo showEnableWithStatus:NO];
        }
    } else if (type == PhoneBindingInputCellTypePwdAgain){  // 再次密码
        if (status == YES){
            self.tempPwdAgainTextField = textField;
            self.tempPwdAgainEnable = YES;
            if (self.tempPwdEnable == YES && [textField.text isEqualToString:self.tempPwdTextField.text]){
                [self.tempCellWithTabTwo showEnableWithStatus:YES];
            } else {
                [self.tempCellWithTabTwo showEnableWithStatus:NO];
            }
        } else {
            self.tempPwdAgainEnable = NO;
            [self.tempCellWithTabTwo showEnableWithStatus:NO];
        }
    }
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    [self touchManager];
}

#pragma mark - linkSocket
-(void)linkSocketWithHost:(NSString *)host port:(NSInteger)port{
    [AccountModel sharedAccountModel].lifeSocketHost = host;
    [AccountModel sharedAccountModel].lifeSocketPort = port;
    
    [[NetworkAdapter sharedAdapter] socketConnectionWithLoginHost:host port:port];
}








#pragma mark - 账号绑定进入
-(void)sendRequestToBindingPageType{
    [self touchManager];
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.tableView1 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    PDPhoneBindingInputCell *cellWithRowThr = (PDPhoneBindingInputCell *)[self.tableView2 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowFour = (PDPhoneBindingInputCell *)[self.tableView2 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    
    NSString *err = @"";
    if (!cellWithRowThr.inputTextField.text.length){
        err = @"请输入密码";
    } else if (!cellWithRowFour.inputTextField.text.length){
        err = @"请再次输入密码";
    } else if (![cellWithRowFour.inputTextField.text isEqualToString:cellWithRowThr.inputTextField.text]){
        err = @"请确认两次密码是否一致";
    }
    if(err.length){
        [[PDAlertView sharedAlertView]showError:err];
        return;
    }
    
    NSString *account = cellWithRowOne.inputTextField.text;
    NSString *code = cellWithRowTwo.inputTextField.text;
    NSString *password = cellWithRowThr.inputTextField.text;
    __weak typeof(self)weakSelf = self;
    
    NSDictionary *params = @{@"account":account,@"password":password,@"code":code};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberThirdbind requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"绑定成功" conten:@"恭喜您绑定成功" isClose:NO btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(hasAccountAndBindingSuccessManager)]){
                    [strongSelf.delegate hasAccountAndBindingSuccessManager];
                }
                [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
            }];
        }
    }];
}

@end
