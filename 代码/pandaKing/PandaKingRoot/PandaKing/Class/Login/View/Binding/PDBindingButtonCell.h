//
//  PDBindingButtonCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,PDBindingButtonCellType) {
    PDBindingButtonCellTypeNext,                    /**< 下一步*/
    PDBindingButtonCellTypeSend,                    /**< 提交*/
    PDBindingButtonCellInHome,                      /**< 进入首页*/
    PDBindingButtonCellInRegister,                  /**< 注册*/
    PDBindingButtonCellFindPwd,                     /**< 找回密码*/
};

@interface PDBindingButtonCell : UITableViewCell

@property (nonatomic,strong)UIButton *cusomterButton;                   /**< 自定义按钮 */
@property (nonatomic,assign)PDBindingButtonCellType bingdingType;       /**< 当前绑定类型*/
@property (nonatomic,assign)CGFloat transferCellHeight;                 /**< 外部传入的cell高度*/

+(CGFloat)calculationCellHeight;
-(void)showEnableWithStatus:(BOOL)status;                               // 显示当前按钮的状态
@end
