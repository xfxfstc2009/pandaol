//
//  PDPhoneBindingHeaderImageView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDPhoneBindingHeaderImageView : UIImageView

@property (nonatomic,assign) CGFloat  duration;

@end
