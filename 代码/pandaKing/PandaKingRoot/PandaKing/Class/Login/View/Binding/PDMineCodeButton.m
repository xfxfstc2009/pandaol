//
//  PDMineCodeButton.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/10/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMineCodeButton.h"

static char mineCodeActionClickBlockKey;
@interface PDMineCodeButton()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UIButton *actionButton;

@end

@implementation PDMineCodeButton

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = UURandomColor;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.frame = self.bounds;
    self.iconImgView.userInteractionEnabled = NO;
    [self addSubview:self.iconImgView];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.frame = self.bounds;
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &mineCodeActionClickBlockKey);
        if(block){
            block();
        }
    }];
    [self addSubview:self.actionButton];
}

-(void)changeCodeImg:(NSString *)imgUrl{
    [self.iconImgView uploadImageWithVCodeURL:imgUrl placeholder:nil callback:NULL];
}

-(void)mineCodeActionClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &mineCodeActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
