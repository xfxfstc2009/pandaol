//
//  PDPhoneBindingHeaderCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPhoneBindingHeaderCell.h"
#import "PDPhoneBindingHeaderImageView.h"

@interface PDPhoneBindingHeaderCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDPhoneBindingHeaderImageView *logoImageView;
@property (nonatomic,strong)UILabel *fixedLabel;

@end

@implementation PDPhoneBindingHeaderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - 创建view
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.bgView];
    
    // 2. 创建logo
    self.logoImageView = [[PDPhoneBindingHeaderImageView alloc]init];
    self.logoImageView.backgroundColor = [UIColor clearColor];
    self.logoImageView.image = [UIImage imageNamed:@"icon_login_binding_phone"];
    [self addSubview:self.logoImageView];
    
    // 3. 创建title
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.fixedLabel.text = @"该功能需要绑定手机才可以使用";
    self.fixedLabel.textColor = c11;
    self.fixedLabel.numberOfLines = 0;
    [self addSubview:self.fixedLabel];
}

#pragma mark - 绑定类型
-(void)setTransferPageType:(PDPhoneBindingHeaderCellType)transferPageType{
    _transferPageType = transferPageType;
}

-(void)setTransferThirdLoginType:(PDThirdLoginType)transferThirdLoginType{
    _transferThirdLoginType = transferThirdLoginType;
}

#pragma mark -传递cell高度
-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;

    // 标题
    NSString *title = @"";
    if (self.transferThirdLoginType == PDThirdLoginTypeQQ){
        title = @"亲爱的QQ平台用户\n为了给您更好的服务，请绑定手机号";
    } else if (self.transferThirdLoginType == PDThirdLoginTypeWechat){
        title = @"亲爱的微信平台用户\r为了给您更好的服务，请绑定手机号";
    } else if (self.transferThirdLoginType == PDThirdLoginTypeNor){
        title = @"亲爱的盼达电竞用户\r为了给您更好的服务，请绑定手机号";
    }
    self.fixedLabel.text = title;
    
    
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, transferCellHeight);

    CGSize contentOfSize = [title sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width, CGFLOAT_MAX)];
    self.fixedLabel.frame = CGRectMake(0, transferCellHeight - LCFloat(12) - contentOfSize.height ,kScreenBounds.size.width, contentOfSize.height);
    
    CGFloat margin = (transferCellHeight - LCFloat(12) - [NSString contentofHeightWithFont:self.fixedLabel.font] - LCFloat(85)) / 2.;
    self.logoImageView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(85)) / 2., margin, LCFloat(85), LCFloat(85));
    [self mainAnimationWithType:self.transferPageType];
}

#pragma mark 修改页面类型
-(void)showAnimationChangeWithType:(PDPhoneBindingHeaderCellType) type{
    self.transferPageType = type;
    if (self.transferPageType == PDPhoneBindingHeaderCellTypePhone){                // 手机号码
        self.logoImageView.image = [UIImage imageNamed:@"icon_login_binding_phone"];
        NSString *title = @"";
        if (self.transferThirdLoginType == PDThirdLoginTypeQQ){
            title = @"亲爱的QQ平台用户\n为了给您更好的服务，请绑定手机号";
        } else if (self.transferThirdLoginType == PDThirdLoginTypeWechat){
            title = @"亲爱的微信平台用户\n为了给您更好的服务，请绑定手机号";
        }
        self.fixedLabel.text = title;
    } else if (self.transferPageType == PDPhoneBindingHeaderCellTypePwd){           // 密码
        self.logoImageView.image = [UIImage imageNamed:@"icon_login_binding_pwd"];
        self.fixedLabel.text = @"设置密码";
    } else if (self.transferPageType == PDPhoneBindingHeaderCellTypeSuccess){       // 成功
        self.logoImageView.image = [UIImage imageNamed:@"icon_login_binding_success"];
        self.fixedLabel.text = @"绑定成功";
    }
    [self mainAnimationWithType:type];
}

#pragma mark 动画
-(void)mainAnimationWithType:(PDPhoneBindingHeaderCellType) type{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect tmpRect         = self.logoImageView.bounds;
        tmpRect.size           = self.logoImageView.image.size;
        self.logoImageView.bounds   = tmpRect;
    }];
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight = 148;
    return cellHeight;
}

@end
