//
//  PDLoginCustomerCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginCustomerCell.h"

@interface PDLoginCustomerCell()<UITextFieldDelegate>
@property (nonatomic,assign)AccountCellType type;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,assign)CGFloat cellHeight;
@end

@implementation PDLoginCustomerCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier accountType:(AccountCellType)type timerType:(PDTimerType)timerType timerButtonAction:(void (^)())buttonAction{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.cellHeight = LCFloat(44);
        self.type = type;
        [self createViewWithTimerType:timerType buttonAction:buttonAction];
    }
    return self;
}

#pragma mark - view
-(void)createViewWithTimerType:(PDTimerType)timerType buttonAction:(void(^)())buttonAction {
    // 1.icon
    self.leftView = [[PDImageView alloc]init];
    self.leftView.backgroundColor = [UIColor redColor];
    [self addSubview:self.leftView];
    
    // 2.textField
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor clearColor];
    self.inputTextField.textColor = [UIColor blackColor];
    self.inputTextField.font = [UIFont systemFontOfCustomeSize:13];
    self.inputTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self addSubview:self.inputTextField];
    
    // 3.Button
    self.timerButton = [[PDTimerButton alloc]initWithTimerType:timerType actionBlock:^{
        if (buttonAction) {
            buttonAction();
        }
    }];
    self.timerButton.isCanClickByPhone = YES;
    [self addSubview:self.timerButton];
    
    // 4.line
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor blackColor];
    [self addSubview:self.lineView];
    
    [self customCellFrame];
}

- (void)customCellFrame {
    if (self.type == AccountCellTypeNormal) {
        self.leftView.frame = CGRectMake(LCFloat(105/2.), LCFloat(15), self.cellHeight - LCFloat(22), self.cellHeight - LCFloat(22));
        self.inputTextField.frame = CGRectMake(CGRectGetMaxX(self.leftView.frame) + LCFloat(20), LCFloat(12), kScreenBounds.size.width - (LCFloat(105/2.) + LCFloat(20) + CGRectGetMaxX(self.leftView.frame)),self.cellHeight - LCFloat(12));
        self.lineView.frame = CGRectMake(CGRectGetMaxX(self.leftView.frame) + LCFloat(10), CGRectGetMaxY(self.inputTextField.frame), CGRectGetWidth(self.inputTextField.frame) + LCFloat(10), 1);
        self.timerButton.hidden = YES;
    } else if (self.type == AccountCellTypeTimer) {
        self.leftView.frame = CGRectMake(LCFloat(105/2.), LCFloat(15), self.cellHeight - LCFloat(22), self.cellHeight - LCFloat(22));
        self.timerButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(105/2.) - LCFloat(80) - LCFloat(5), LCFloat(14), LCFloat(80), self.cellHeight - LCFloat(14));
        self.inputTextField.frame = CGRectMake(CGRectGetMaxX(self.leftView.frame) + LCFloat(20), LCFloat(12), CGRectGetMinX(self.timerButton.frame) - CGRectGetMaxX(self.leftView.frame) - LCFloat(20) - LCFloat(5),LCFloat(44) - LCFloat(12));
        self.lineView.frame = CGRectMake(CGRectGetMaxX(self.leftView.frame) + LCFloat(10), CGRectGetMaxY(self.inputTextField.frame), CGRectGetWidth(self.inputTextField.frame) + LCFloat(10), 1);
        self.timerButton.hidden = NO;
    }
}
@end
