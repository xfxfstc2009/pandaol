//
//  AccountModel.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AccountModel.h"
#import "PDThirdLoginModel.h"                   // 三方登录
#import "NetworkAdapter+PDJavaLogin.h"                      // 登录的socket
#import <objc/runtime.h>
#import "PDThirdLoginSessionModel.h"
#import "PDNewFeatureListModel.h"
#import "PDLoginandsign.h"
#import "PDCenterMyInfo.h"
#import "PDSliderViewController.h"
#import "PDSliderListModel.h"
#import "PDMyTaskViewController.h"
#import "PDAmusementRootViewController.h"
#import "PDAccountSliderOtherInfoModel.h"

static char gestureLoginKey;                        // 游客登录
static char userLoginKey;                           // 用户登录
static char thirdLoginKey;                          // 三方登录
static char contureKey;                             // 用户跳过
@interface AccountModel()<PDLoginSocketDelegate,PDNetworkAdapterDelegate>

@end

@implementation AccountModel

+(instancetype)sharedAccountModel{
    static AccountModel *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[AccountModel alloc] init];
    });
    return _sharedAccountModel;
}

- (NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"uid": @"id"};
}

- (BOOL)hasLoggedIn{
    return ([AccountModel sharedAccountModel].token.length ? YES : NO);
}

#pragma mark - 判断是否是游客登录
-(BOOL)hasMemberLoggedIn{
    if ([[Tool userDefaultGetWithKey:CustomerType] isEqualToString:@"member"] && [Tool userDefaultGetWithKey:CustomerToken].length){
        return YES;
    } else {
        return NO;
    }
}

#pragma mark - 1. 游客登录
-(void)guestLoginManagerWithBlcok:(void(^)(BOOL interfaceSuccess, BOOL socketSuccess))block{
    if ([[Tool userDefaultGetWithKey:CustomerType] isEqualToString:@"member"]){            // 【账号登录用户】
        objc_setAssociatedObject(self, &userLoginKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        
        [AccountModel sharedAccountModel].lifeSocketHost = [Tool userDefaultGetWithKey:CustomeSocketAddress];
        [AccountModel sharedAccountModel].lifeSocketPort = [[Tool userDefaultGetWithKey:CustomeSocketPort]integerValue];
        NSString *token = [Tool userDefaultGetWithKey:CustomerToken];
        [AccountModel sharedAccountModel].token = token;
        if (![AccountModel sharedAccountModel].token.length){
            [Tool userDefaulteWithKey:CustomerType Obj:@"guest"];
        }
        [AccountModel sharedAccountModel].accountType = [[Tool userDefaultGetWithKey:CustomerType] isEqualToString:@"member"]?AccountTypeMember:AccountTypeGesture;
        [AccountModel sharedAccountModel].memberId = [Tool userDefaultGetWithKey:CustomerMemberId];
        
        
        if ([AccountModel sharedAccountModel].accountType == AccountTypeMember){
            [self loginWithIMWithAccountId:[AccountModel sharedAccountModel].memberId];
        }

        // 获取用户信息
        __weak typeof(self)weakSelf = self;
        [self sendRequestToGetUserInfoWithBlock:^(BOOL success) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf linkSocketManager];
        }];

        return;
    }
    // 【游客】
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:authorMember requestParams:nil responseObjectClass:[PDThirdLoginSessionModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){                   // 游客登录成功后进行连接socket
            objc_setAssociatedObject(strongSelf, &gestureLoginKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
            
            PDThirdLoginSessionModel *loginModel = (PDThirdLoginSessionModel *)responseObject;
            
            [AccountModel sharedAccountModel].lifeSocketHost = loginModel.session.socketAddress;
            [AccountModel sharedAccountModel].lifeSocketPort = loginModel.session.socketPort;
            [AccountModel sharedAccountModel].accountType = [loginModel.session.status isEqualToString:@"member"]?AccountTypeMember:AccountTypeGesture;
            // 存入token
            if (loginModel.session.token.length){
                [Tool userDefaulteWithKey:CustomerToken Obj:loginModel.session.token];
            }
            
            [Tool userDefaulteWithKey:CustomeSocketAddress Obj:loginModel.session.socketAddress];
            [Tool userDefaulteWithKey:CustomeSocketPort Obj:[NSString stringWithFormat:@"%li",(long)loginModel.session.socketPort]];
            [Tool userDefaulteWithKey:CustomerType Obj:loginModel.session.status];              // 保存当前的status;
            if (loginModel.session.memberId.length){
                [Tool userDefaulteWithKey:CustomerMemberId Obj:loginModel.session.memberId];
            }
            
            [AccountModel sharedAccountModel].token = loginModel.session.token;
            if (loginModel.avatar.length){
                [AccountModel sharedAccountModel].avatar = loginModel.avatar;
                
            }
            if (loginModel.nickname.length){
                [AccountModel sharedAccountModel].nickname = loginModel.nickname;
                
            }
            [AccountModel sharedAccountModel].uid = loginModel.session.memberId;
            
            
            if ([AccountModel sharedAccountModel].accountType == AccountTypeMember){
                [self loginWithIMWithAccountId:[AccountModel sharedAccountModel].memberId];
            }
            
            __weak typeof(self)weakSelf = self;
            [self sendRequestToGetUserInfoWithBlock:^(BOOL success) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf linkSocketManager];
            }];

        } else {
            if (block){
                block(NO,NO);
            }
        }
    }];
}

#pragma mark 1.1连接socket
-(void)linkSocketManager{
    [[NetworkAdapter sharedAdapter] socketConnectionWithLoginHost:[AccountModel sharedAccountModel].lifeSocketHost port:[AccountModel sharedAccountModel].lifeSocketPort];
//    [NetworkAdapter sharedAdapter].delegate = self;
    [NetworkAdapter sharedAdapter].loginSocketDelegate = self;
}

#pragma mark - 1.1 back - 永久长连接连接成功
-(void)loginConnectedWithStatus:(BOOL)status{
    // 游客登录
    void(^gestureBlock)(BOOL interfaceSuccess, BOOL socketSuccess) = objc_getAssociatedObject(self, &gestureLoginKey);
    // 账户登录
    void(^userBlock)(BOOL interfaceSuccess, BOOL socketSuccess) = objc_getAssociatedObject(self, &userLoginKey);
    // 三方登录
    void(^thirdBlock)(BOOL interfaceSuccess,BOOL socketSuccess ,BOOL PDThirdLoginBackType) = objc_getAssociatedObject(self, &thirdLoginKey);
    // 跳过
    void(^contureBlock)(BOOL interfaceSuccess,BOOL socketSuccess) = objc_getAssociatedObject(self, &contureKey);
    if (status == YES){
        // 1.【游客block】
        if (gestureBlock){
            gestureBlock(YES,YES);
        }
        
        // 2.【用户block】
        if (userBlock){
            userBlock (YES,YES);
        }
        // 3.【三方登录block】
        if (thirdBlock){
            thirdBlock(YES,YES,[AccountModel sharedAccountModel].thirdLoginBackType);
        }
        // 4. 【跳过】
        if (contureBlock){
            contureBlock(YES,YES);
        }
        
    } else {
        // 1.【游客block】
        if (gestureBlock){
            gestureBlock(YES,NO);
        }
        // 2.【用户block】
        if (userBlock){
            userBlock (YES,NO);
        }
        // 3.【三方登录block】
        if (thirdBlock){
            thirdBlock(YES,NO,[AccountModel sharedAccountModel].thirdLoginBackType);
        }
        // 4. 【跳过】
        if (contureBlock){
            contureBlock(YES,NO);
        }
    }
}

#pragma mark - 【用户登录】
-(void)loginWithUserAccount:(NSString *)account password:(NSString *)passeord handle:(void(^)(BOOL interfaceSuccess,BOOL socketSuccess))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"username":account,@"password":passeord};
    [[NetworkAdapter sharedAdapter] fetchWithPath:tokenLogin requestParams:params responseObjectClass:[PDThirdLoginSessionModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [PDHUD dismiss];
            objc_setAssociatedObject(strongSelf, &userLoginKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
            
            PDThirdLoginSessionModel *loginModel = (PDThirdLoginSessionModel *)responseObject;
            [AccountModel sharedAccountModel].lifeSocketHost = loginModel.session.socketAddress;
            [AccountModel sharedAccountModel].lifeSocketPort = loginModel.session.socketPort;
            [AccountModel sharedAccountModel].accountType = [loginModel.session.status isEqualToString:@"member"]?AccountTypeMember:AccountTypeGesture;
            // 存入token
            if (loginModel.session.token.length){
                [Tool userDefaulteWithKey:CustomerToken Obj:loginModel.session.token];
            }
            
            [Tool userDefaulteWithKey:CustomeSocketAddress Obj:loginModel.session.socketAddress];
            [Tool userDefaulteWithKey:CustomeSocketPort Obj:[NSString stringWithFormat:@"%li",(long)loginModel.session.socketPort]];
            [Tool userDefaulteWithKey:CustomerType Obj:loginModel.session.status];              // 保存当前的status;
            if (loginModel.session.memberId.length){
                [Tool userDefaulteWithKey:CustomerMemberId Obj:loginModel.session.memberId];
            }
            
            [AccountModel sharedAccountModel].token = loginModel.session.token;
            if (loginModel.avatar.length){
                [AccountModel sharedAccountModel].avatar = loginModel.avatar;
                [Tool userDefaulteWithKey:CustomerAvatar Obj:loginModel.avatar];
            }
            if (loginModel.nickname.length){
                [AccountModel sharedAccountModel].nickname = loginModel.nickname;
                [Tool userDefaulteWithKey:CustomerNickname Obj:loginModel.nickname];
            }
            [AccountModel sharedAccountModel].uid = loginModel.session.memberId;
            
            // 登录IM
            if (loginModel.session.memberId.length){
                [strongSelf loginWithIMWithAccountId:loginModel.session.memberId];
                [AccountModel sharedAccountModel].memberId = loginModel.session.memberId;
            }
            
            // 【获取当前的新手引导】
            [weakSelf loginSuccessToUploadGuider];
            

            __weak typeof(self)weakSelf = self;
            [self sendRequestToGetUserInfoWithBlock:^(BOOL success) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf linkSocketManager];
            }];

        } else {
            block(NO,NO);
        }
    }];
}

#pragma mark - 【三方登录】
-(void)loginThirdLoginWithType:(PDThirdLoginType)type openId:(NSString *)openId unionId:(NSString *)unionId avatar:(NSString *)avatar nickname:(NSString *)nickname withBlkc:(void(^)(BOOL interfaceSuccess,BOOL socketSuccess ,PDThirdLoginBackType thirdLoginBackType))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"type":@(type),@"openId":openId,@"unionId":unionId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:thirdLogin requestParams:params responseObjectClass:[PDThirdLoginSessionModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDThirdLoginSessionModel *loginModel = (PDThirdLoginSessionModel *)responseObject;
            if (loginModel.type == PDThirdLoginBackType1){              // 表示用户已经注册过了
                objc_setAssociatedObject(strongSelf, &thirdLoginKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
                [AccountModel sharedAccountModel].lifeSocketHost = loginModel.session.socketAddress;
                [AccountModel sharedAccountModel].lifeSocketPort = loginModel.session.socketPort;
                [AccountModel sharedAccountModel].accountType = [loginModel.session.status isEqualToString:@"member"]?AccountTypeMember:AccountTypeGesture;
                [AccountModel sharedAccountModel].token = loginModel.session.token;
                [AccountModel sharedAccountModel].thirdLoginBackType = loginModel.type;
                
                // 存入token
                if (loginModel.session.token.length){
                    [Tool userDefaulteWithKey:CustomerToken Obj:loginModel.session.token];
                }
                
                [Tool userDefaulteWithKey:CustomeSocketAddress Obj:loginModel.session.socketAddress];
                [Tool userDefaulteWithKey:CustomeSocketPort Obj:[NSString stringWithFormat:@"%li",(long)loginModel.session.socketPort]];
                [Tool userDefaulteWithKey:CustomerType Obj:loginModel.session.status];              // 保存当前的status;
                if (loginModel.session.memberId.length){
                    [Tool userDefaulteWithKey:CustomerMemberId Obj:loginModel.session.memberId];
                }
                
                
                if (loginModel.avatar.length){
                    [AccountModel sharedAccountModel].avatar = loginModel.avatar;
                    [Tool userDefaulteWithKey:CustomerAvatar Obj:loginModel.avatar];
                }
                if (loginModel.nickname.length){
                    [AccountModel sharedAccountModel].nickname = loginModel.nickname;
                    [Tool userDefaulteWithKey:CustomerNickname Obj:loginModel.nickname];
                }
                
                // 登录IM
                if (loginModel.session.memberId.length){
                    [strongSelf loginWithIMWithAccountId:loginModel.session.memberId];
                    [AccountModel sharedAccountModel].memberId = loginModel.session.memberId;
                }
                
                // 【获取当前的新手引导】
                [weakSelf loginSuccessToUploadGuider];

                // 获取个人信息
                __weak typeof(self)weakSelf = self;
                [self sendRequestToGetUserInfoWithBlock:^(BOOL success) {
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf linkSocketManager];
                }];

            } else if (loginModel.type == PDThirdLoginTypeType2){
                block(YES,NO,loginModel.type);
            } else {
                
            }
        } else {
            block(NO,NO,NO);
        }
    }];
}

#pragma mark - 【账号跳过】
-(void)loginContureWithOpenId:(NSString *)openId type:(PDThirdLoginType)type avatar:(NSString *)avatar nick:(NSString *)nick handle:(void(^)(BOOL interfaceSuccess,BOOL socketSuccess))block{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary  *dic = [NSMutableDictionary dictionary];
    if (openId.length){
        [dic setObject:openId forKey:@"openId"];
    }
    [dic setObject:@(type) forKey:@"type"];
    if (avatar.length){
        [dic setObject:avatar forKey:@"avatar"];
    }
    if (nick.length){
        [dic setObject:nick forKey:@"nickName"];
    } else {
        [dic setObject:@"PANDAOL" forKey:@"nickName"];
    }
    

    [[NetworkAdapter sharedAdapter] fetchWithPath:userContinue requestParams:dic responseObjectClass:[PDThirdLoginSessionModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            objc_setAssociatedObject(strongSelf, &contureKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
            PDThirdLoginSessionModel *thirdLogin =(PDThirdLoginSessionModel *)responseObject;
            [AccountModel sharedAccountModel].lifeSocketHost = thirdLogin.session.socketAddress;
            [AccountModel sharedAccountModel].lifeSocketPort = thirdLogin.session.socketPort;
            [AccountModel sharedAccountModel].accountType = [thirdLogin.session.status isEqualToString:@"member"]?AccountTypeMember:AccountTypeGesture;
            [AccountModel sharedAccountModel].token = thirdLogin.session.token;
            
            [Tool userDefaulteWithKey:CustomeSocketAddress Obj:thirdLogin.session.socketAddress];
            [Tool userDefaulteWithKey:CustomeSocketPort Obj:[NSString stringWithFormat:@"%li",(long)thirdLogin.session.socketPort]];
            [Tool userDefaulteWithKey:CustomerType Obj:thirdLogin.session.status];              // 保存当前的status;
            if (thirdLogin.session.memberId.length){
                [Tool userDefaulteWithKey:CustomerMemberId Obj:thirdLogin.session.memberId];
            }
            if (thirdLogin.session.token.length){
                [Tool userDefaulteWithKey:CustomerToken Obj:thirdLogin.session.token];
            }
            

            if (thirdLogin.avatar.length){
                [AccountModel sharedAccountModel].avatar = thirdLogin.avatar;
                [Tool userDefaulteWithKey:CustomerAvatar Obj:thirdLogin.avatar];
            }
            if (thirdLogin.nickname.length){
                [AccountModel sharedAccountModel].nickname = thirdLogin.nickname;
                [Tool userDefaulteWithKey:CustomerAvatar Obj:thirdLogin.nickname];
            }
            
            // 登录IM
            if (thirdLogin.session.memberId.length){
                [strongSelf loginWithIMWithAccountId:thirdLogin.session.memberId];
                [AccountModel sharedAccountModel].memberId = thirdLogin.session.memberId;
            }
            
            // 【获取当前的新手引导】
            [weakSelf loginSuccessToUploadGuider];
            // 【签到】
//            [weakSelf sendRequestToQiandao];
            // 获取个人信息
            __weak typeof(self)weakSelf = self;
            [self sendRequestToGetUserInfoWithBlock:^(BOOL success) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf linkSocketManager];
            }];

            
        } else {
            block(NO,NO);
        }
    }];
}

#pragma mark - 退出登录
-(void)logoutBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:userLogout requestParams:@{@"token":[AccountModel sharedAccountModel].token} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
//            [AccountModel sharedAccountModel].token = @"";
            [AccountModel sharedAccountModel].lifeSocketHost = @"";
            [AccountModel sharedAccountModel].lifeSocketPort = 0;
            [AccountModel sharedAccountModel].accountType = AccountTypeGesture;
//            [Tool userDefaultDelegtaeWithKey:CustomerToken];
            [Tool userDefaultDelegtaeWithKey:CustomeSocketAddress];
            [Tool userDefaultDelegtaeWithKey:CustomeSocketPort];
            [Tool userDefaulteWithKey:CustomerType Obj:@"guest"];
            [Tool userDefaultDelegtaeWithKey:CustomerMemberId];
            [[PDEMManager sharedInstance] logout:nil];
            
            // 更新首页
            [[PDPandaRootViewController sharedController]removeAccountInfoSet];
            [PDPandaRootViewController sharedController].headerImageView.hidden = YES;
            [[PDCenterViewController sharedController] removeMemberGameUser];
            [[PDCenterViewController sharedController] clearMemberInfo];
            if (block){
                block();
            }
        }
    }];
}

-(void)thirdLoginManagerWithModel:(PDThirdLoginSessionModel *)thirdLogin{
    [AccountModel sharedAccountModel].lifeSocketHost = thirdLogin.session.socketAddress;
    [AccountModel sharedAccountModel].lifeSocketPort = thirdLogin.session.socketPort;
    [AccountModel sharedAccountModel].accountType = [thirdLogin.session.status isEqualToString:@"member"]?AccountTypeMember:AccountTypeGesture;
    
    [Tool userDefaulteWithKey:CustomeSocketAddress Obj:thirdLogin.session.socketAddress];
    [Tool userDefaulteWithKey:CustomeSocketPort Obj:[NSString stringWithFormat:@"%li",(long)thirdLogin.session.socketPort]];
    [Tool userDefaulteWithKey:CustomerType Obj:thirdLogin.session.status];              // 保存当前的status;
    if (thirdLogin.session.memberId.length){
        [Tool userDefaulteWithKey:CustomerMemberId Obj:thirdLogin.session.memberId];
    }
    
    // 存入token
    if (thirdLogin.session.token.length){
        [Tool userDefaulteWithKey:CustomerToken Obj:thirdLogin.session.token];
        [AccountModel sharedAccountModel].token = thirdLogin.session.token;
    }
    
    if (thirdLogin.avatar.length){
        [AccountModel sharedAccountModel].avatar = thirdLogin.avatar;
    }
    if (thirdLogin.nickname.length){
        [AccountModel sharedAccountModel].nickname = thirdLogin.nickname;
    }
    
    [self linkSocketManager];                                    // 连接socket
    
    // 登录IM
    if (thirdLogin.session.memberId.length){
        [self loginWithIMWithAccountId:thirdLogin.session.memberId];
    }
}


#pragma mark - 登录IM
-(void)loginWithIMWithAccountId:(NSString *)accountId{
    [[PDEMManager sharedInstance] loginWithAccount:accountId password:accountId complication:^(EMError *error) {
        DLog(@"IM登陆 -> %u,%@",error.code,error.errorDescription);
    }];
}

#pragma mark - 获取当前的新手引导列表
-(void)sendRequestToGetNewFeatureListWithBlock:(void(^)())block{
//    __weak typeof(self)weakSelf = self;
//    [[NetworkAdapter sharedAdapter] fetchWithPath:memberguideline requestParams:nil responseObjectClass:[PDNewFeatureListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf){
//            return ;
//        }
//        if(isSucceeded){
//            PDNewFeatureListModel *newfeature = (PDNewFeatureListModel *)responseObject;
//            for (PDNewFeatureSingleModel *singleModel in newfeature.items){
//                if ([singleModel.type isEqualToString:@"messaageBox"]){     // 网吧
//                    [AccountModel sharedAccountModel].messaageBox = singleModel.complete;
//                } else if([singleModel.type isEqualToString:@"bar"]){
//                    [AccountModel sharedAccountModel].bar = singleModel.complete;
//                } else if ([singleModel.type isEqualToString:@"memberInfo"]){
//                    [AccountModel sharedAccountModel].memberInfo = singleModel.complete;
//                } else if([singleModel.type isEqualToString:@"mine"]){
//                    [AccountModel sharedAccountModel].mine = singleModel.complete;
//                } else if([singleModel.type isEqualToString:@"bindGameUser"]){
//                    [AccountModel sharedAccountModel].bindGameUser = singleModel.complete;
//                } else if([singleModel.type isEqualToString:@"guess"]){
//                    [AccountModel sharedAccountModel].guess = singleModel.complete;
//                } else if ([singleModel.type isEqualToString:@"matchGuess"]){
//                    [AccountModel sharedAccountModel].matchGuess = singleModel.complete;
//                } else if([singleModel.type isEqualToString:@"championGuess"]){
//                    [AccountModel sharedAccountModel].championGuess =singleModel.complete;
//                }
//            }
//        }
//    }];
    
    
    if ([Tool userDefaultGetWithKey:@"bar"].length){
        [AccountModel sharedAccountModel].bar = YES;
    }
    if ([Tool userDefaultGetWithKey:@"bindGameUser"].length){
        [AccountModel sharedAccountModel].bindGameUser = YES;
    }
    if ([Tool userDefaultGetWithKey:@"championGuess1"].length){
//        [AccountModel sharedAccountModel].championGuess1 = YES;
    }
    if ([Tool userDefaultGetWithKey:@"championGuess2"].length) {
//        [AccountModel sharedAccountModel].championGuess2 = YES;
    }
    if ([Tool userDefaultGetWithKey:@"guess"].length){
        [AccountModel sharedAccountModel].guess = YES;
    }
    if ([Tool userDefaultGetWithKey:@"matchGuess"].length){
        [AccountModel sharedAccountModel].matchGuess = YES;
    }
//    if ([Tool userDefaultGetWithKey:@"memberInfo"].length){
//        [AccountModel sharedAccountModel].memberInfo = YES;
//    }
    if ([Tool userDefaultGetWithKey:@"messaageBox"].length){
        [AccountModel sharedAccountModel].messaageBox = YES;
    }
    if ([Tool userDefaultGetWithKey:@"mine"].length){
        [AccountModel sharedAccountModel].mine = YES;
    }
    if ([Tool userDefaultGetWithKey:@"toBindGameUser"].length){
        [AccountModel sharedAccountModel].toBindGameUser = YES;
    }
    if ([Tool userDefaultGetWithKey:@"bindingGameUser"].length){
        [AccountModel sharedAccountModel].bindingGameUser = YES;
    }
    if ([Tool userDefaultGetWithKey:@"GuestTypeMatchGuess2"].length ){
        [AccountModel sharedAccountModel].matchGuess2 = YES;
    }
    if ([Tool userDefaultGetWithKey:@"GuestTypeMatchGuess3"].length ){
        [AccountModel sharedAccountModel].matchGuess3 = YES;
    }
    if (block){
        block();
    }
}


#pragma mark - 登录之后去提交当前的所有的内容
-(void)loginSuccessToUploadGuider{
    NSMutableArray *typesMutableArr = [NSMutableArray array];
    if ([Tool userDefaultGetWithKey:@"bar"].length){
        [typesMutableArr addObject:@"bar"];
    }
    if ([Tool userDefaultGetWithKey:@"bindGameUser"].length){
        [typesMutableArr addObject:@"bindGameUser"];
    }
    if ([Tool userDefaultGetWithKey:@"championGuess"].length){
        [typesMutableArr addObject:@"championGuess"];
    }
    if ([Tool userDefaultGetWithKey:@"guess"]){
        [typesMutableArr addObject:@"guess"];
    }
    if ([Tool userDefaultGetWithKey:@"matchGuess"]){
        [typesMutableArr addObject:@"matchGuess"];
    }
    if ([Tool userDefaultGetWithKey:@"memberInfo"]){
        [typesMutableArr addObject:@"memberInfo"];
    }
    if ([Tool userDefaultGetWithKey:@"messaageBox"]){
        [typesMutableArr addObject:@"messaageBox"];
    }
    if ([Tool userDefaultGetWithKey:@"mine"]){
        [typesMutableArr addObject:@"mine"];
    }
    if ([Tool userDefaultGetWithKey:@"toBindGameUser"]){
        [typesMutableArr addObject:@"toBindGameUser"];
    }
    if ([Tool userDefaultGetWithKey:@"bindingGameUser"]){
        [typesMutableArr addObject:@"bindingGameUser"];
    }
    if ([Tool userDefaultGetWithKey:@"matchGuess2"]){
        [typesMutableArr addObject:@"matchGuess2"];
    }
    if ([Tool userDefaultGetWithKey:@"matchGuess3"]){
        [typesMutableArr addObject:@"matchGuess3"];
    }
    
    NSString *typesStr = @"";
    if (typesMutableArr.count){
        for (int i = 0 ; i < typesMutableArr.count;i++){
            NSString *info = [typesMutableArr objectAtIndex:i];
            if (i == typesMutableArr.count - 1){
                typesStr = [typesStr stringByAppendingString:[NSString stringWithFormat:@"%@",info]];
            } else {
                typesStr = [typesStr stringByAppendingString:[NSString stringWithFormat:@"%@,",info]];
            }
        }
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:guidelineComplete requestParams:@{@"types[]":typesStr} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetNewFeatureListWithBlock:NULL];
    }];
}

-(void)guestSuccessWithType:(GuestType)guestType block:(void(^)())block{
//    __weak typeof(self)weakSelf = self;
    NSString *type = @"";
    if (guestType == GuestTypeBar){
        type = @"bar";
    } else if (guestType == GuestTypeBindGameUser){
        type = @"bindGameUser";
    } else if (guestType == GuestTypeChampionGuess1){
        type = @"championGuess1";
    } else if (guestType == GuestTypeChampionGuess2) {
        type = @"championGuess2";
    } else if (guestType == GuestTypeGuess){
        type = @"guess";
    } else if (guestType == GuestTypeMatchGuess){
        type = @"matchGuess";
    } else if (guestType == GuestTypeMemberInfo){
        type = @"memberInfo";
    } else if (guestType == GuestTypeMessaageBox){
        type = @"messaageBox";
    } else if (guestType == GuestTypeMine){
        type = @"mine";
    } else if (guestType == GuestTypeToBindGameUser){
        type = @"toBindGameUser";
    } else if (guestType == GuestTypeBindingGameUser){
        type = @"bindingGameUser";
    } else if (guestType == GuestTypeMatchGuess2){
        type = @"GuestTypeMatchGuess2";
    } else if (guestType == GuestTypeMatchGuess3){
        type = @"GuestTypeMatchGuess3";
    }
    // 1. 保存到本地
    [Tool userDefaulteWithKey:type Obj:@"yes"];
    // 2.
    if (guestType == GuestTypeBar){
        [AccountModel sharedAccountModel].bar = YES;
    } else if (guestType == GuestTypeBindGameUser){
        [AccountModel sharedAccountModel].bindGameUser = YES;
    } else if (guestType == GuestTypeChampionGuess1){
//        [AccountModel sharedAccountModel].championGuess1 = YES;
        [Tool userDefaulteWithKey:championGuess1 Obj:@"YES"];
    } else if (guestType == GuestTypeChampionGuess2) {
        [Tool userDefaulteWithKey:championGuess2 Obj:@"YES"];
    } else if (guestType == GuestTypeGuess){
        [AccountModel sharedAccountModel].guess = YES;
    } else if (guestType == GuestTypeMatchGuess){
        [AccountModel sharedAccountModel].matchGuess = YES;
    } else if (guestType == GuestTypeMemberInfo){
//        [AccountModel sharedAccountModel].memberInfo = YES;
    } else if (guestType == GuestTypeMessaageBox){
        [AccountModel sharedAccountModel].messaageBox = YES;
    } else if (guestType == GuestTypeMine){
        [AccountModel sharedAccountModel].mine = YES;
    } else if (guestType == GuestTypeToBindGameUser){
        [AccountModel sharedAccountModel].toBindGameUser = YES;
    } else if (guestType == GuestTypeBindingGameUser){
        [AccountModel sharedAccountModel].bindingGameUser = YES;
    } else if (guestType == GuestTypeMatchGuess2){
        [AccountModel sharedAccountModel].matchGuess2 = YES;
    } else if (guestType == GuestTypeMatchGuess3){
        [AccountModel sharedAccountModel].matchGuess3 = YES;
    }
    if (block){
        block();
    }

    
    
//    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){            // 没有登录
//        NSDictionary *params = @{@"type[]":type};
//        [[NetworkAdapter sharedAdapter] fetchWithPath:guidelineComplete requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//            if (!weakSelf){
//                return ;
//            }
//            if (isSucceeded){
//                           }
//        }];
//    }
}



#pragma mark - 获取当前的状态
-(void)sendRequestToQiandao{
    __weak typeof(self)weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:loginandsign requestParams:nil responseObjectClass:[PDLoginandsign class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return;
        }
        PDLoginandsign *loginSign = (PDLoginandsign *)responseObject;
        if (loginSign.success){
            [[PDAlertView sharedAlertView] showSignAlertWithDrawComplication:^{
                PDMyTaskViewController *taskViewController = [[PDMyTaskViewController alloc] init];
                [[PDMainTabbarViewController sharedController].amusementViewController pushViewController:taskViewController animated:YES];
            }];
        }
    }];
}



#pragma mark - 获取当前的信息
//-(void)getCurrentRoleInfo{
//    __weak typeof(self) weakSelf = self;
//    [[NetworkAdapter sharedAdapter] fetchWithPath:myInfo requestParams:nil responseObjectClass:[PDCenterMyInfo class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf) {
//            return ;
//        }
//        
//        if (isSucceeded) {
//            PDCenterMyInfo *myInfo = (PDCenterMyInfo *)responseObject;
//            [AccountModel sharedAccountModel].userId = myInfo.userId;
//            [AccountModel sharedAccountModel].nickname = myInfo.nickname;
//            [AccountModel sharedAccountModel].avatar = myInfo.avatar;
//            [AccountModel sharedAccountModel].gold = myInfo.gold;
//            [AccountModel sharedAccountModel].bamboo = myInfo.bamboo;
//        }
//    }];
//}

-(void)directToAppStore{
    [[UIApplication sharedApplication]openURL:[NSURL URLWithString:appUrl]];
}


#pragma mark - 登录之后去获取用户信息
-(void)sendRequestToGetUserInfoWithBlock:(void(^)(BOOL success))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:slidermemberinfo requestParams:nil responseObjectClass:[AccountModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            AccountModel *accountModel = (AccountModel *)responseObject;
            
            PDImageView *imgView = [[PDImageView alloc]init];
            [imgView uploadImageWithAvatarURL:accountModel.memberInfo.avatar placeholder:nil callback:^(UIImage *image) {
                [AccountModel sharedAccountModel].userAvatarImg = image;
                [[PDMainTabbarViewController sharedController].amusementViewController.avatarButton setImage:image forState:UIControlStateNormal];
            }];
            
            
            [AccountModel sharedAccountModel].memberInfo = accountModel.memberInfo;
            [AccountModel sharedAccountModel].lolInfo = accountModel.lolInfo;
            [AccountModel sharedAccountModel].isBindCS = accountModel.isBindCS;
            [AccountModel sharedAccountModel].isBindDota2 = accountModel.isBindDota2;
            [AccountModel sharedAccountModel].isBindKing = accountModel.isBindKing;
            [AccountModel sharedAccountModel].isBindLOL = accountModel.isBindLOL;
            [AccountModel sharedAccountModel].isBindOW = accountModel.isBindOW;
            [AccountModel sharedAccountModel].isBindLOL = YES;
            if (block){
                block(YES);
            }
            
            // 进行刷新
            PDSliderViewController *sliderViewController = (PDSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
            if (![AccountModel sharedAccountModel].hasFirstAnimation){
                [sliderViewController.sliderHeaderView loginManager];           // 1. 头部更新
            }
            [sliderViewController.sliderTableView reloadData];              // 2. 列表更新
           
            // 更新slider
            [[AccountModel sharedAccountModel] sendRequestToGetSliderInfo]; // 请求slider数据
        }
    }];
}

// 获取侧边栏信息
-(void)sendRequestToGetSliderInfo{
    __weak typeof(self)weakSelf = self;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([AccountModel sharedAccountModel].gameType == GameTypeCS){
        [params setObject:@"cs" forKey:@"gameType"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        [params setObject:@"dota2" forKey:@"gameType"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        [params setObject:@"lol" forKey:@"gameType"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePubg){
        [params setObject:@"ow" forKey:@"gameType"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        [params setObject:@"king" forKey:@"gameType"];
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:sliderList requestParams:params responseObjectClass:[PDSliderListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        PDSliderListModel *sliderList = (PDSliderListModel *)responseObject;
        PDSliderViewController *sliderViewController = (PDSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
        if (!sliderViewController.sliderMutableArr){
            sliderViewController.sliderMutableArr = [NSMutableArray array];
        }
        [sliderViewController.sliderMutableArr removeAllObjects];
        
        NSMutableArray *sliderMutableArr = [NSMutableArray array];
        
        if ([AccountModel sharedAccountModel].isShenhe){
            for (int i = 0 ; i < sliderList.sliderlist.count;i++){
                PDSliderSingleModel *singleModel = [sliderList.sliderlist objectAtIndex:i];
                if ((![singleModel.appFunctionName isEqualToString:@"我的钱包"]) && (![singleModel.appFunctionName isEqualToString:@"我的背包"]) && (![singleModel.appFunctionName isEqualToString:@"我的任务"])){
                    [sliderMutableArr addObject:singleModel];
                }
            }
        } else {
            [sliderMutableArr addObjectsFromArray:sliderList.sliderlist];
        }
        [sliderViewController.sliderMutableArr addObject:sliderMutableArr];
        [sliderViewController.sliderTableView reloadData];
    }];
}

#pragma mark - 检查更新
-(void)sendRequestToGetNewAppVersionWithBlock:(void(^)(NSString *version))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"appType":@"ios"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:appversionGet requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if ([responseObject isKindOfClass:[NSDictionary class]]){
                NSDictionary *dic = (NSDictionary *)responseObject;
                if ([dic.allKeys containsObject:@"version"]){
                    NSString *dicInfo = [dic objectForKey:@"version"];
                    if (![dicInfo isEqualToString:[Tool appVersion]]){
                        if (block){
                            block(dicInfo);
                        }
                    } else {
                        block(@"");
                    }
                    return;
                } else {
                    block(@"");
                }
            }
            block(@"");
        }
    }];
}


-(UIColor *)mainStyleColor{
    if (_mainStyleColor == nil){
        return [UIColor colorWithCustomerName:@"lol"];
    } else {
        return  _mainStyleColor;
    }
}

-(GameType)gameType{
    if (_hasAppCacheLoading == NO){
        NSString *gameType = [Tool userDefaultGetWithKey:User_Choose_Game];
        NSArray *tempArr = [gameType componentsSeparatedByString:User_Choose_Game];
        NSString *tempStr = [tempArr lastObject];
        NSInteger tempIndex = [tempStr integerValue];
        _gameType = tempIndex;
        _lastGameType = _gameType;
        
        // 2. 修改颜色
        if (_gameType == GameTypeLoL){
            _mainStyleColor = [UIColor colorWithCustomerName:@"lol"];
        } else if (_gameType == GameTypePVP){
            _mainStyleColor = [UIColor colorWithCustomerName:@"pvp"];
        } else if (_gameType == GameTypeDota2){
            _mainStyleColor = [UIColor colorWithCustomerName:@"dota"];
        } else if (_gameType == GameTypePubg){
            _mainStyleColor = [UIColor colorWithCustomerName:@"ow"];
        } else if (_gameType == GameTypeCS){
            _mainStyleColor = [UIColor colorWithCustomerName:@"cs"];
        }
        [PDMainTabbarViewController sharedController].selectedColor = [AccountModel sharedAccountModel].mainStyleColor;
        _hasAppCacheLoading = YES;
    }

    return _gameType;
}


-(void)sendRequestToGetJingyanInfoBlock:(void(^)(PDAccountSliderOtherInfoModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:sliderotherinfo requestParams:nil responseObjectClass:[PDAccountSliderOtherInfoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            PDAccountSliderOtherInfoModel *singleModel = (PDAccountSliderOtherInfoModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}

@end
