//
//  AccountModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

#import "PDThirdLoginSessionModel.h"
#import "PDAccountMemberInfoModel.h"
#import "PDAccountLoLInfoSingleModel.h"
#import "PDAccountSliderOtherInfoModel.h"

// 消息状态
#import "PDMessageState.h"

//@protocol AccountModelDelegate <NSObject>
//
//-(void)accountSocketTransformationWithBackData:(id)responseObject;
//
//@end


typedef NS_ENUM(NSInteger,FindControllerType) {
    FindControllerTypeInformation = 0,                      /**< 资讯*/
    FindControllerTypeCircle = 1,                           /**< 圈子*/
};

typedef NS_ENUM(NSInteger,AccountType) {
    AccountTypeGesture = 0,                         /**< 游客登录*/
    AccountTypeMember = 1,                          /**< 账号登录*/
};



@interface AccountModel : FetchModel
// 【发现页面】
@property (nonatomic,assign)FindControllerType findType;
@property (nonatomic,assign)AccountType accountType;                    /**< 账户类型*/


// 【用户信息】
@property (nonatomic,copy)NSString *uid;                    /**< 用户id*/
@property (nonatomic, copy) NSString *account;              /**< 账号*/
@property (nonatomic, assign) NSInteger invite;             /**< 是否允许邀请组队*/

// 【召唤师】
@property (nonatomic,copy)NSString *lolGameUser;


// 有用
@property (nonatomic,copy)NSString *token;
@property (nonatomic,copy)NSString *lifeSocketHost;
@property (nonatomic,assign)NSInteger lifeSocketPort;
@property (nonatomic,assign)PDThirdLoginBackType thirdLoginBackType;            //
@property (nonatomic,copy)NSString *avatar;                 /**< 头像*/
@property (nonatomic,copy)NSString *nickname;               /**< 昵称*/
@property (nonatomic,copy)NSString *memberId;               /**< id*/
@property (nonatomic,copy)NSString *userId;
@property (nonatomic, assign) NSInteger gold;
@property (nonatomic, assign) NSInteger bamboo;



//////////////////////////// 三方Token //////////////////////////////
@property (nonatomic, copy) NSString *wx_accessToken;
@property (nonatomic, copy) NSString *wx_refreshToken;
@property (nonatomic, copy) NSString *wx_openId;

// 【2.绑定信息】
@property (nonatomic,assign)methodType method;                                  /**< 获取当前可绑定的模式*/

// 【审核】
@property (nonatomic,assign)BOOL isShenhe;                                  // 判断是否审核
@property (nonatomic,assign)BOOL auth;

// 【3.三方登录用来绑定type】

/// 消息状态
@property (nonatomic, strong) PDMessageState *messageState; // 保存消息状态

// 4. 用来保存当前的主播猜的id
@property (nonatomic,copy)NSString *liveActivityId;         /**< 主播猜id*/

// 5. 游戏类型
@property (nonatomic,assign)GameType gameType;
@property (nonatomic,assign)GameType lastGameType;          /**< 上一次的游戏类型*/
@property (nonatomic,strong)UIColor *mainStyleColor;        /**< 主题色*/

+(instancetype)sharedAccountModel;                                                               /**< 单例*/
//@property (nonatomic,weak)id<AccountModelDelegate>  delegate;

@property (nonatomic,assign)BOOL messaageBox;               /**< 消息盒子*/
@property (nonatomic,assign)BOOL bar;                       /**< 网吧*/
//@property (nonatomic,assign)BOOL memberInfo;                /**< 查看用户信息*/
@property (nonatomic,assign)BOOL mine;                      /**< 我的*/
@property (nonatomic,assign)BOOL toBindGameUser;            /**< to绑定召唤师*/
@property (nonatomic,assign)BOOL bindingGameUser;           /**< 已绑定召唤师*/
@property (nonatomic,assign)BOOL bindGameUser;              /**< 绑定召唤师*/
@property (nonatomic,assign)BOOL guess;                     /**< 竞猜*/
@property (nonatomic,assign)BOOL matchGuess;                /**< 赛事猜*/
@property (nonatomic,assign)BOOL matchGuess2;                /**< 赛事猜*/
@property (nonatomic,assign)BOOL matchGuess3;               /**< 赛事猜投注*/
@property (nonatomic,assign)BOOL championGuess1;             /**< 英雄猜详情*/
@property (nonatomic,assign)BOOL championGuess2;            /**< 英雄猜投注*/

#pragma mark - 打野大乱斗
@property (nonatomic,assign)NSInteger accountMemberDayStake;            /**< 我当前的账号已投入的金币*/
@property (nonatomic,assign)NSInteger accountCountDayStake;             /**< 一共需要投注*/

#pragma mark - 功能模块
@property (nonatomic,assign)BOOL gongnengLolchampionkill;                   /**< 英雄杀*/

#pragma mark - 判断【是否登录】
- (BOOL)hasLoggedIn;
#pragma mark - 判断是否是【游客登录】
-(BOOL)hasMemberLoggedIn;

#pragma mark - 【游客登录】
-(void)guestLoginManagerWithBlcok:(void(^)(BOOL interfaceSuccess, BOOL socketSuccess))block;

#pragma mark - 【用户登录】
-(void)loginWithUserAccount:(NSString *)account password:(NSString *)passeord handle:(void(^)(BOOL interfaceSuccess,BOOL socketSuccess))block;

#pragma mark - 【三方登录】
-(void)loginThirdLoginWithType:(PDThirdLoginType)type openId:(NSString *)openId unionId:(NSString *)unionId avatar:(NSString *)avatar nickname:(NSString *)nickname withBlkc:(void(^)(BOOL interfaceSuccess,BOOL socketSuccess ,PDThirdLoginBackType thirdLoginBackType))block;

#pragma mark - 【账号跳过】
-(void)loginContureWithOpenId:(NSString *)openId type:(PDThirdLoginType)type avatar:(NSString *)avatar  nick:(NSString *)nick handle:(void(^)(BOOL interfaceSuccess,BOOL socketSuccess))block;

#pragma mark - 【退出登录】
-(void)logoutBlock:(void(^)())block;

#pragma mark - 【一系列的三方绑定结果】
-(void)thirdLoginManagerWithModel:(PDThirdLoginSessionModel *)thirdLogin;
#pragma mark - 获取当前的新手引导列表
-(void)sendRequestToGetNewFeatureListWithBlock:(void(^)())block;
-(void)guestSuccessWithType:(GuestType)guestType block:(void(^)())block;
-(void)loginSuccessToUploadGuider;                                  // 提交新手引导

// 是否全屏
@property (nonatomic,assign)BOOL isLockScreen;              /**< 视频里面用来判断是否全屏*/

#pragma mark - 签到成功
-(void)sendRequestToQiandao;
-(void)getCurrentRoleInfo;


#pragma mark - directToAppStore
-(void)directToAppStore;



#pragma mark - 用户信息
@property (nonatomic,assign)BOOL hasAppCacheLoading;

@property (nonatomic,strong)PDAccountMemberInfoModel *memberInfo;
@property (nonatomic,strong)PDAccountLoLInfoSingleModel *lolInfo;
@property (nonatomic,strong)UIImage *userAvatarImg;

-(void)sendRequestToGetSliderInfo;
#pragma mark - 检查更新
-(void)sendRequestToGetNewAppVersionWithBlock:(void(^)(NSString *url))block;
#pragma mark - 登录之后去获取用户信息
-(void)sendRequestToGetUserInfoWithBlock:(void(^)(BOOL success))block;
-(void)sendRequestToGetJingyanInfoBlock:(void(^)(PDAccountSliderOtherInfoModel *singleModel))block;
@property (nonatomic,assign)BOOL isBindCS;                  /**< 是否绑定cs*/
@property (nonatomic,assign)BOOL isBindDota2;               /**< 是否绑定dota2*/
@property (nonatomic,assign)BOOL isBindKing;                /**< 是否绑定king*/
@property (nonatomic,assign)BOOL isBindLOL;                 /**< 是否绑定lol*/
@property (nonatomic,assign)BOOL isBindOW;                  /**< 是否绑定ow*/

@property (nonatomic,assign)BOOL hasFirstAnimation;         /**< 是否第一次动画*/



// 2017 - 10 - 10
@property (nonatomic,copy)NSString *verifyCode;
@property (nonatomic,copy)NSString *resetVerifyCode;        // 重置验证码

@end
