//
//  PDBindingCheckModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

typedef NS_ENUM(NSInteger,BindingCheckType) {
    BindingCheckTypeNo = 0,                     /**< 0不能绑定，已经被绑定了*/
    BindingCheckTypeHas = 1,                    /**< 1账号存在，可以绑定，无需设置密码，直接申请绑定*/
    BindingCheckTypeSuccess = 2,                /**< 2账号不存在，心增账号，继续设置密码后提交*/
};

@interface PDBindingCheckModel : FetchModel

@property (nonatomic,assign)BindingCheckType type;              /**< 绑定类型*/

@end
