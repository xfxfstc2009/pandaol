//
//  WebSocketEnumGroup.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 
  {type:100}
 已接单 {type:101,id:单号}
 绑定成功{type:102,id:单号,success:1}
 绑定失败{type:102,id:单号,success:0}
 数据有误{type:500}
 key有误或失效{type:502}
 */

typedef NS_ENUM(NSInteger,webSocketType) {
    webSocketType1 = 100,                     /**< 已收到绑定请求，请等待接单*/
    webSocketType2 = 101,                     /**< 已接单*/
    webSocketType3 = 1021,                    /**< 绑定成功*/
    webSocketType4 = 1020,                    /**< 绑定失败*/
    webSocketType5 = 500,                     /**< 数据有误*/
    webSocketType6 = 502,                     /**< key有误*/
};


@interface WebSocketEnumGroup : NSObject

@end
