//
//  PDAccountLoLInfoSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDAccountLoLInfoSingleModel : FetchModel

@property (nonatomic,copy)NSString *gameUserName;
@property (nonatomic,copy)NSString *gameServerName;
@property (nonatomic,copy)NSString *gameUserAvatar;
@property (nonatomic,assign)CGFloat kda;
@property (nonatomic,assign)CGFloat winRate;
@property (nonatomic,copy)NSString *state;
@property (nonatomic,copy)NSString *grade;
@property (nonatomic,assign)NSInteger gameCount;
@property (nonatomic,copy)NSString *lolGameUserId;


@end
