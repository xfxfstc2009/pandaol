//
//  PDVCodeSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/10/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDVCodeSingleModel : FetchModel

@property (nonatomic,copy)NSString *img;

@end
