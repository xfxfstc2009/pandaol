//
//  PDAccountSliderOtherInfoModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/25.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDAccountSliderOtherInfoModel : FetchModel

@property (nonatomic,assign)NSInteger level;
@property (nonatomic,assign)NSInteger empiricValue;
@property (nonatomic,assign)NSInteger upGradeEmpiricValue;
@property (nonatomic,copy)NSString *inviteAward;
@property (nonatomic,assign)NSInteger unReceiveTaskCount;
@property (nonatomic,assign)BOOL isLastLevel;



@end
