//
//  PDThirdLoginModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"


@interface PDThirdLoginModel : FetchModel

@property (nonatomic,assign)NSTimeInterval lastActiveTime;              /**< 最后活动时间*/
@property (nonatomic,copy)NSString *memberId;                           /**< 用户id*/
@property (nonatomic,copy)NSString *socketAddress;                      /**< socketAddress*/
@property (nonatomic,assign)NSInteger socketPort;                       /**< socketePort*/
@property (nonatomic,copy)NSString *status;                             /**< 当前的登录状态*/
@property (nonatomic,copy)NSString  *token;                             /**< token*/


@end
