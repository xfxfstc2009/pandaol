//
//  PDAccountMemberInfoModel.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAccountMemberInfoModel.h"

@implementation PDAccountMemberInfoModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end
