//
//  EnumGroup.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WebSocketEnumGroup.h"                  // webSocket枚举

// 【1. 绑定】
typedef NS_ENUM(NSInteger,methodType) {
    methodType1 = 1,                        /**< 只支持用户填写管理员名字*/
    methodType2 = 2,                        /**< 只支持后端反馈结果*/
    methodType3 = 3,                        /**< 全部支持*/
};

#pragma mark -
#pragma mark -- 绑定状态

typedef NS_ENUM(NSUInteger,PDBindStatus) {
    PDBindStatusFailed  = 0,    /**< 绑定失败*/
    PDBindStatusSuccess = 1,    /**< 绑定成功*/
    PDBindStatusing     = 2,    /**< 绑定中*/
};

#pragma mark -
#pragma mark -- 战卡等级

typedef NS_ENUM(NSInteger,PDCardLevel) {
    PDCardLevel0 = 0,
    PDCardLevel1,
    PDCardLevel2,
    PDCardLevel3,
    PDCardLevel4,
    PDCardLevel5,
    PDCardLevel6,
    PDCardLevel7,
    PDCardLevel8,
    PDCardLevel9,
    PDCardLevelKing = 999,              /**< 王卡*/
    PDCardLevelBuy = -1,
};

#pragma mark -  ____________个人中心
#pragma mark -- 实名认证

typedef NS_ENUM(NSUInteger,PDRealNameStatus) {
    PDRealNameStatusNone  = 0,           /**< 未实名认证*/
    PDRealNameStatusCheck = 1,           /**< 已实名认证*/
};

#pragma mark -- 获取历史战绩列表

typedef NS_ENUM(NSUInteger,PDCompetitionResultStatus) {
    PDCompetitionResultStatusVictory = 0,       /**< 胜利*/
    PDCompetitionResultStatusFail    = 1,       /**< 失败*/
    PDCompetitionResultStatusEscape  = 2,       /**< 逃跑*/
    PDCompetitionResultStatusInvaild = 3,       /**< 无效比赛*/
};

#pragma mark -- 同意或拒绝好友申请 | 处理好友请求时用户反映

typedef NS_ENUM(NSUInteger,PDHandleFriendRequestStatus) {
    PDHandleFriendRequestStatusAgree = 1,       /**< 通过*/
    PDHandleFriendRequestStatusDisAgree = 2,    /**< 不通过*/
};

#pragma mark -- 获取用户战卡列表 ｜ 战卡类型

typedef NS_ENUM(NSInteger,PDCardType) {
    PDCardTypeNormal = 0,                       /**< 可以出征的战卡*/
    PDCardTypePresent = 1,                      /**< 可以赠送的战卡*/
};

#pragma mark -- 获取好友列表 ｜ 好友在线状态

typedef NS_ENUM(NSUInteger,PDFriendOnlineStatus) {
    PDFriendOnlineStatusUnline = 0,             /**< 不在线*/
    PDFriendOnlineStatusOnline = 1,             /**< 在线*/
};


#pragma mark - 【role】
typedef NS_ENUM(NSInteger,PDRoleLevel) {        // 角色的等级
    PDRoleLevel7 = 7,                       /**< 30级*/
    PDRoleLevel6 = 6,                       /**< 黄铜*/
    PDRoleLevel5 = 5,                       /**< 白银*/
    PDRoleLevel4 = 4,                       /**< 黄金*/
    PDRoleLevel3 = 3,                       /**< 铂金*/
    PDRoleLevel2 = 2,                       /**< 钻石*/
    PDRoleLevel1 = 1,                       /**< 超凡大师*/
    PDRoleLevel0 = 0,                       /**< 最强王者*/
};


#pragma mark - 三方登录
typedef NS_ENUM(NSInteger,PDThirdLoginType) {
    PDThirdLoginTypeNor = 0,                /**< 盼达电竞*/
    PDThirdLoginTypeQQ = 1,                 /**< QQ*/
    PDThirdLoginTypeWechat = 2,             /**< 微信*/
};

#pragma mark - 用户登录方式
typedef NS_ENUM(NSInteger,UserLoginType) {
    UserLoginTypeCurrent = 1,               /**< 官方注册*/
    UserLoginTypeThird,                     /**< 三方登录*/
};

typedef NS_ENUM(NSInteger, LESScreenMode) {
    LESScreenModeIPhone4SOrEarlier,
    LESScreenModeIPhone5Series,
    LESScreenModeIPhone6,
    LESScreenModeIPhone6Plus,
    
    LESScreenModeIPadPortrait,
    LESScreenModeIPadLandscape,
    
    LESScreenModeUnknown,
};

typedef NS_ENUM(NSInteger,BindingStatus) {
    BindingStatusNoActivity,                   /**< 未激活*/
    BindingStatusActiviting,                   /**< 激活中*/
    BindingStatusActivited,                    /**< 已激活*/
};

typedef NS_ENUM(NSInteger,GuestType) {
    GuestTypeMessaageBox,                       /**< 消息*/
    GuestTypeBar,                               /**< 网吧*/
    GuestTypeMemberInfo,                        /**< 查看用户信息*/
    GuestTypeMine,                              /**< 我的*/
    GuestTypeToBindGameUser,                    /**< 去绑定召唤师*/
    GuestTypeBindingGameUser,                   /**< 绑定召唤师*/
    GuestTypeBindGameUser,                      /**< 已绑定召唤师*/
    GuestTypeGuess,                             /**< 竞猜*/
    GuestTypeMatchGuess,                        /**< 赛事猜*/
    GuestTypeMatchGuess2,                       /**< 赛事猜详情*/
    GuestTypeMatchGuess3,                       /**< 赛事猜投注*/
    GuestTypeChampionGuess1,                     /**< 英雄猜详情*/
    GuestTypeChampionGuess2,                    /**< 英雄猜投注*/
};

typedef NS_ENUM(NSInteger,GameType) {
    GameTypePubg = 0,                       /**< 守望先锋*/
    GameTypeLoL = 1,                        /**< LOL*/
    GameTypePVP = 2,                        /**< 王者荣耀*/
    GameTypeDota2 = 3,                      /**< Dota2*/
    GameTypeCS = 4,                         /**< CS*/
};



@interface EnumGroup : NSObject

@property (nonatomic,assign)methodType method;                  // 获取绑定支持方式

@end
