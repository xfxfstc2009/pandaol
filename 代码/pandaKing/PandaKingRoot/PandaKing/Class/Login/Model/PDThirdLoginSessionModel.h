//
//  PDThirdLoginSessionModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDThirdLoginModel.h"

typedef NS_ENUM(NSInteger,PDThirdLoginBackType) {
    PDThirdLoginBackType1 = 1,                      /**< 登录成功*/
    PDThirdLoginTypeType2 = 2,                      /**< 登录失败*/
};


@interface PDThirdLoginSessionModel : FetchModel

@property (nonatomic,strong)PDThirdLoginModel *session;             /**< sessgion*/
@property (nonatomic,assign)PDThirdLoginBackType type;
@property (nonatomic,copy)NSString *avatar;                             /**< 头像*/
@property (nonatomic,copy)NSString *nickname;                           /**< 昵称*/
@end
