//
//  PDAccountMemberInfoModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDAccountMemberInfoModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *nickname;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,assign)NSInteger level;
@property (nonatomic,assign)NSInteger gold;
@property (nonatomic,assign)NSInteger bamboo;
@end
