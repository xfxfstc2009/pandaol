//
//  PDRegisterIsEighteenViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/9/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDRegisterIsEighteenViewController : AbstractViewController

@property (nonatomic,copy)NSString *account;
@property (nonatomic,copy)NSString *pwd;

@end
