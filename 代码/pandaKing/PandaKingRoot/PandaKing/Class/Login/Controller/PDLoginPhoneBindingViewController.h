//
//  PDLoginPhoneBindingViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 手机号进行绑定
#import "AbstractViewController.h"
#import "PDLoginMainViewController.h"

typedef void(^hasAccountAndBindingSuccessBlock)();

typedef NS_ENUM(NSInteger,LoginPhoneBindingVCType) {
    LoginPhoneBindingVCTypeNormal = 0,                      /**< 默认状态*/
    LoginPhoneBindingVCTypeBinding = 1,
};

@class PDLoginMainViewController;
@interface PDLoginPhoneBindingViewController : AbstractViewController

@property (nonatomic,assign)PDThirdLoginType transferThirdLoginType;            /**< 上个页面传递过来的三方登录*/
@property (nonatomic,copy)NSString *transferOpen_id;                            /**< 上个页面传递过来的open_id*/
@property (nonatomic,copy)NSString *transferAvatar;                             /**< 上个页面传递的头像*/
@property (nonatomic,copy)NSString *transferNickName;                           /**< 上个页面传递的昵称*/
@property (nonatomic,assign)LoginPhoneBindingVCType transferPageType;

// 有账号，并且绑定成功，然后返回block
@property (nonatomic,copy)hasAccountAndBindingSuccessBlock hasAccountBlock;



@end
