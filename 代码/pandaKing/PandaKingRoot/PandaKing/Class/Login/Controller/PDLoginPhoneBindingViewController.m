//
//  PDLoginPhoneBindingViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginPhoneBindingViewController.h"
#import "PDPhoneBindingHeaderCell.h"            // 绑定头
#import "PDPhoneBindingOtherCell.h"
#import "NetworkAdapter+PDJavaLogin.h"              // socket

#import "PDThirdLoginModel.h"                       // 三方登录的model
#import "PDCenterSettingThirdBindingViewController.h"

@interface PDLoginPhoneBindingViewController()<UITableViewDataSource,UITableViewDelegate,PDPhoneBindingOtherCellDelegate,PDNetworkAdapterDelegate,PDLoginSocketDelegate>
@property (nonatomic,strong)UITableView *phoneBindingTableView;             /**< 主列表*/
@property (nonatomic,strong)NSArray *phoneBindingArr;                       /**< 绑定数组*/
@property (nonatomic,strong)UIButton *tempButton;
@property (nonatomic,strong)UIButton *tempLeftButton;                       /**< 左侧按钮*/

@end

@implementation PDLoginPhoneBindingViewController

-(void)directToLoginHomePage{
    
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    self.navigationController.navigationBar.layer.shadowOpacity = 2.0;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];               // 数组初始化
    [self createTableView];             // 创建tableView
//    [self createNotifi];                // 添加键盘代理
    
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
    self.navigationController.navigationBar.layer.shadowOpacity = 2.0;
    self.navigationController.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"手机号绑定";
    __weak typeof(self)wealSelf = self;
    self.tempButton = [wealSelf rightBarButtonWithTitle:@"跳过" barNorImage:nil barHltImage:nil action:^{
        if (!wealSelf){
            return ;
        }
        __strong typeof(wealSelf)strongSelf = wealSelf;
        [strongSelf sendRequestToContinueAccountBinding];
    }];
    
    if(self.transferPageType == LoginPhoneBindingVCTypeBinding){
        self.tempButton.hidden = YES;
    } else {
        self.tempButton.hidden = NO;
    }
    
    self.tempLeftButton = [wealSelf leftBarButtonWithTitle:@"返回" barNorImage:nil barHltImage:nil action:^{
        if(!wealSelf){
            return ;
        }
        __strong typeof(wealSelf)strongSelf = wealSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.phoneBindingArr = @[@[@"头"],@[@"尾巴"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.phoneBindingTableView){
        self.phoneBindingTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.phoneBindingTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.phoneBindingTableView.delegate = self;
        self.phoneBindingTableView.dataSource = self;
        self.phoneBindingTableView.backgroundColor = [UIColor clearColor];
        self.phoneBindingTableView.showsVerticalScrollIndicator = NO;
        self.phoneBindingTableView.scrollEnabled = YES;
        self.phoneBindingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.phoneBindingTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.phoneBindingArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.phoneBindingArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0 && indexPath.row == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDPhoneBindingHeaderCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDPhoneBindingHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferThirdLoginType = self.transferThirdLoginType;
        cellWithRowOne.transferPageType = PDPhoneBindingHeaderCellTypePhone;
        cellWithRowOne.transferCellHeight = cellHeight;
        return cellWithRowOne;
    } else if (indexPath.section == 1 && indexPath.row == 0){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDPhoneBindingOtherCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDPhoneBindingOtherCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.delegate = self;
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferThirdLoginType = self.transferThirdLoginType;
        cellWithRowTwo.transferOpen_Id = self.transferOpen_id;
        cellWithRowTwo.transferAvatar = self.transferAvatar;
        cellWithRowTwo.transferNick = self.transferNickName;
        cellWithRowTwo.transferPageType = PDPhoneBindingHeaderCellTypePhone;
        cellWithRowTwo.bindingVCType = self.transferPageType;
        cellWithRowTwo.transferCellHeight = cellHeight;
        return cellWithRowTwo;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [PDPhoneBindingHeaderCell calculationCellHeight];
    } else {
        return self.view.size_height - [PDPhoneBindingHeaderCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

#pragma mark - PDPhoneBindingOtherCellDelegate
-(void)updatePhoneBindingHeaderType:(PDPhoneBindingHeaderCellType)pageType{
    PDPhoneBindingHeaderCell * cell = (PDPhoneBindingHeaderCell *)[self.phoneBindingTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    [cell showAnimationChangeWithType:pageType];
}

-(void)userMainScrollNotifyManager{
    self.tempButton.hidden = YES;
    self.tempLeftButton.hidden = YES;
}

#pragma mark - 账号跳过绑定
-(void)sendRequestToContinueAccountBinding{
    self.tempButton.enabled = NO;
    NSString *alert = @"正在跳过绑定";
    [PDHUD showHUDProgress:alert diary:0];
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] loginContureWithOpenId:self.transferOpen_id type:self.transferThirdLoginType avatar:self.transferAvatar nick:self.transferNickName handle:^(BOOL interfaceSuccess, BOOL socketSuccess) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (interfaceSuccess && socketSuccess){
            [strongSelf direstToHomePageSuccessManager];
        }
    }];
}

-(void)direstToHomePageSuccessManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf directToAppHomePage];
}

#pragma mark 跳转到首页
-(void)directToAppHomePage{
    for (UIViewController *controller in self.navigationController.viewControllers){
        if ([controller isKindOfClass:[PDLoginMainViewController class]]){
            PDLoginMainViewController *loginMainController = (PDLoginMainViewController *)controller;
            
            [self dismissViewControllerAnimated:YES completion:^{
                if (loginMainController.dismissBlock){
                    loginMainController.dismissBlock();
                }
            }];
        }
    }
}


-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    PDPhoneBindingOtherCell *cell = (PDPhoneBindingOtherCell *)[self.phoneBindingTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [cell touchManager];
}

#pragma 已有账号，进行绑定
-(void)hasAccountAndBindingSuccessManager{
    for (UIViewController *controller in self.navigationController.childViewControllers){
        if ([controller isKindOfClass:[PDCenterSettingThirdBindingViewController class]]){
            if (self.hasAccountBlock){
                self.hasAccountBlock();
            }
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
}

@end
