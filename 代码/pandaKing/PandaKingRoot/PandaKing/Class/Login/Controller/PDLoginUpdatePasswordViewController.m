//
//  PDLoginUpdatePasswordViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginUpdatePasswordViewController.h"
#import "PDLoginUpdatePwdNormalCell.h"
#import "PDPhoneBindingInputCell.h"
#import "PDresetpasswordinfoModel.h"

@interface PDLoginUpdatePasswordViewController()<UITableViewDataSource,UITableViewDelegate,PDPhoneBindingInputCellDelegate>
{
    UITextField *oneTextField;
    UITextField *twoTextField;
    UITextField *thrTextField;
    UIButton *sendButton;                           /**< 提交按钮*/
    BOOL oneEnable;
    BOOL twoEnable;
    BOOL thrEnable;
    PDresetpasswordinfoModel *singleModel;
    
}
@property (nonatomic,strong)UITableView *updateTableView;
@property (nonatomic,strong)NSArray *updatePwdArr;
@end

@implementation PDLoginUpdatePasswordViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToInfo];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"修改密码";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.updatePwdArr = @[@[@"账号",@"当前密码"],@[@"设置新密码",@"确认新密码"],@[@"提交"]];
}

#pragma mark -UITableView
-(void)createTableView{
    if (!self.updateTableView){
        self.updateTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.updateTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.updateTableView.delegate = self;
        self.updateTableView.dataSource = self;
        self.updateTableView.backgroundColor = [UIColor clearColor];
        self.updateTableView.showsVerticalScrollIndicator = NO;
        self.updateTableView.scrollEnabled = YES;
        self.updateTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.updateTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.updatePwdArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.updatePwdArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0 && indexPath.row == 0){                          // row One
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDLoginUpdatePwdNormalCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDLoginUpdatePwdNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transforPhoneNum = [AccountModel sharedAccountModel].memberId.length?[AccountModel sharedAccountModel].memberId:@"****";
        return cellWithRowOne;
    } else if (indexPath.section == 2){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if(!cellWithRowThr){
            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowThr.backgroundColor = [UIColor clearColor];
            // 创建按钮
            sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [sendButton setTitle:@"提交" forState:UIControlStateNormal];
            sendButton.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11), cellHeight);
            sendButton.layer.cornerRadius = LCFloat(3);
            sendButton.clipsToBounds = YES;
            __weak typeof(self)weakSelf = self;
            [sendButton buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf comfirmUpdatePwd];
            }];
            [cellWithRowThr addSubview:sendButton];
        }
        [self sendBtnEnabled:NO];
        return cellWithRowThr;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDPhoneBindingInputCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDPhoneBindingInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.delegate = self;
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        
        if (indexPath.section == 0 && indexPath.row == 1){              // 1. 旧密码
            cellWithRowTwo.transferType = PhoneBindingInputCellTypeOldPwd;
            oneTextField = cellWithRowTwo.inputTextField;
        } else if (indexPath.section == 1 && indexPath.row == 0){       // 2. 新密码
            cellWithRowTwo.transferType = PhoneBindingInputCellTypeUpdatePwd;
            twoTextField = cellWithRowTwo.inputTextField;
        } else if (indexPath.section == 1 && indexPath.row == 1){
            cellWithRowTwo.transferType = PhoneBindingInputCellTypeUpdatePwdAgain;
            thrTextField = cellWithRowTwo.inputTextField;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        return cellWithRowTwo;
    }
    return nil;
}

#pragma mark UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(44);
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return LCFloat(10);
    } else {
        return LCFloat(30);
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.updateTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.updatePwdArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType  = SeparatorTypeBottom;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([[self.updatePwdArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    
    if (section == 0){
        
    } else {
        UILabel *fixedLabel = [[UILabel alloc]init];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
        fixedLabel.frame = CGRectMake(LCFloat(11), LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:fixedLabel.font]);
        [headerView addSubview:fixedLabel];
    }
    
    return headerView;
}


#pragma mark Delegate
-(void)inputTextConfirm:(PhoneBindingInputCellType)type textField:(UITextField *)textField status:(BOOL)status{
    if (textField == oneTextField){                 // 旧密码
        if (status == YES){
            oneEnable = YES;
        } else {
            oneEnable = NO;
        }
    } else if (textField == twoTextField){
        if (status == YES){
            twoEnable = YES;
        } else {
            twoEnable = NO;
        }
    } else if (textField == thrTextField){
        if (status == YES){
            thrEnable = YES;
        } else {
            thrEnable = NO;
        }
    }
    [self comfirmWithBtn];
}

#pragma mark - comfirmWithBtn
-(void)comfirmWithBtn{
    if (oneEnable == YES && twoEnable == YES && thrEnable == YES){
        [self sendBtnEnabled:YES];
    } else {
        [self sendBtnEnabled:NO];
    }
}


#pragma mark - normal
-(void)sendBtnEnabled:(BOOL)enabled{
    if (enabled == YES){                // 可以进行点击
        sendButton.enabled = YES;
        sendButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    } else {
        sendButton.enabled = NO;
        sendButton.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    }
}



#pragma mark - 修改密码
// 1. 验证修改密码
-(void)comfirmUpdatePwd{
    [self.view endEditing:YES];
    
    NSString *err = @"";
    if (!oneTextField.text.length){
        err = @"请输入旧密码";
    } else if (!twoTextField.text.length){
        err = @"请输入新密码";
    } else if (!thrTextField.text.length){
        err = @"请再次输入新密码";
    } else {
        if (![twoTextField.text isEqualToString:thrTextField.text]){
            err = @"两次密码输入不同，请重新输入";
        }
    }
    if (err.length){
        [[PDAlertView sharedAlertView] showError:err];
        return;
    }
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToUpdatePwd:oneTextField.text newPwd:twoTextField.text];
}

-(void)sendRequestToUpdatePwd:(NSString *)oldPwd newPwd:(NSString *)newPwd{
    __weak typeof(self)weakSelf = self;
    NSDictionary *dic = @{@"opassword":oldPwd,@"npassword":newPwd};
    [[NetworkAdapter sharedAdapter] fetchWithPath:loginUpdatePwd requestParams:dic responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"密码修改成功" conten:@"恭喜您密码修改成功!" isClose:NO btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
            }];
        }
    }];
}

-(void)sendRequestToInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:resetpasswordinfo requestParams:nil responseObjectClass:[PDresetpasswordinfoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf-> singleModel = (PDresetpasswordinfoModel *)responseObject;
            if (singleModel.isEnable == NO){
                [[PDAlertView sharedAlertView] showAlertWithTitle:@"无法修改密码" conten:@"您当前未绑定手机，无法修改密码" isClose:NO btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                    [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
                }];
            }
        }
    }];
}
@end
