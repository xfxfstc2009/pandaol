//
//  PDRegisterIsEighteenViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/9/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRegisterIsEighteenViewController.h"

@interface PDRegisterIsEighteenViewController ()<UITableViewDataSource,UITableViewDelegate>{
    UITextField *nameTextField;
    UITextField *numTextField;
}

@property (nonatomic,strong)UITableView *mainTableView;
@property (nonatomic,strong)NSArray *infoArr;
@end

@implementation PDRegisterIsEighteenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}


#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"实名认证";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.infoArr = @[@[@"姓名",@"身份证"],@[@"认证按钮"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.mainTableView){
        self.mainTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.mainTableView.dataSource = self;
        self.mainTableView.delegate = self;
        [self.view addSubview:self.mainTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.infoArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.infoArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWInputTextFieldTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellStyleDefault;
        }
        cellWithRowOne.transfrCellHeight = cellHeight;
        __weak typeof(self)weakSelf = self;
        if (indexPath.row == 0){
            nameTextField = cellWithRowOne.inputTextField;
            cellWithRowOne.transferTitle = @"姓名";
            cellWithRowOne.transferPlaceholeder = @"请输入您的姓名";
        } else if (indexPath.row == 1){
            numTextField = cellWithRowOne.inputTextField;
            cellWithRowOne.transferTitle = @"身份证号";
            cellWithRowOne.transferPlaceholeder = @"请输入您的身份证号";
        }
        [cellWithRowOne textFieldDidChangeBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf adjustInfo];
        }];
        return cellWithRowOne;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"提交审核";
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf authWithInfo];
        }];
        return cellWithRowTwo;
    }
}

#pragma makr - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footView = [[UIView alloc]init];
    footView.backgroundColor = [UIColor clearColor];
    
    UILabel *fixedLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    fixedLabel.numberOfLines = 0;
    fixedLabel.text = @"根据2010年8月1日实施的《网络游戏管理暂行办法》，网络游戏用户需使用有效身份证件进行实名注册。为保证流畅游戏体验，享受健康游戏生活，请广大【盼达电竞】的玩家尽快实名注册。";
    CGSize fixedSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    fixedLabel.frame = CGRectMake(LCFloat(11), LCFloat(20), kScreenBounds.size.width - 2 * LCFloat(11), fixedSize.height);
    [footView addSubview:fixedLabel];
    return footView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if (section == 1){
        return 100 + LCFloat(20);
    } else {
        return 0;
    }
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.mainTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.infoArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([[self.infoArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

-(void)adjustInfo{
    GWButtonTableViewCell *cell = (GWButtonTableViewCell *)[self.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if (nameTextField.text.length && [Tool validateIdentityCard:numTextField.text]){
        [cell setButtonStatus:YES];
    } else {
        [cell setButtonStatus:NO];
    }
}

-(void)authWithInfo{
    __weak typeof(self)weakSelf = self;
    NSString *error = @"";
    if (!nameTextField.text.length){
        error = @"请输入你的姓名";
    } else if (!numTextField.text.length){
        error = @"请输入你的身份证号码";
    }
    if (error.length){
        [PDHUD showConnectionErr:error];
        return;
    }

    [weakSelf sendRequestToShenhe:nameTextField.text number:numTextField.text];
}

-(void)sendRequestToShenhe:(NSString *)name number:(NSString *)num{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"name":name,@"idCard":num};
    [[NetworkAdapter sharedAdapter] fetchWithPath:authmemberAuth requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf autoLoginManagerWithAccount:strongSelf.account pwd:strongSelf.pwd];
        } else {                // 【好】
            for (UIViewController *controller in strongSelf.navigationController.viewControllers){
                if ([controller isKindOfClass:[PDLoginMainViewController class]]){
                    PDLoginMainViewController *loginMainController = (PDLoginMainViewController *)controller;
                    [self dismissViewControllerAnimated:YES completion:^{
                        if (loginMainController.dismissBlock){
                            loginMainController.dismissBlock();
                        }
                    }];
                    return ;
                }
            }
        }
    }];
}




#pragma mark - 登录成功dismiss方法
-(void)autoLoginManagerWithAccount:(NSString *)account pwd:(NSString *)pwd{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] loginWithUserAccount:account password:pwd handle:^(BOOL interfaceSuccess, BOOL socketSuccess) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        for (UIViewController *controller in strongSelf.navigationController.viewControllers){
            if ([controller isKindOfClass:[PDLoginMainViewController class]]){
                PDLoginMainViewController *loginMainController = (PDLoginMainViewController *)controller;
                [self dismissViewControllerAnimated:YES completion:^{
                    if (loginMainController.dismissBlock){
                        loginMainController.dismissBlock();
                    }
                }];
                return ;
            }
        }
    }];
}

@end
