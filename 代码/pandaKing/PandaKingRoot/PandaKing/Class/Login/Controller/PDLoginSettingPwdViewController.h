//
//  PDLoginSettingPwdViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDLoginMainViewController.h"

@class PDLoginMainViewController;
typedef NS_ENUM(NSInteger,loginPwdSettingType) {
    loginPwdSettingTypeAdd,                         /**< 设置密码*/
    loginPwdSettingTypeReset,                       /**< 重置密码*/
    loginPwdSettingTypeForget,                      /**< 忘记密码*/
    loginPwdSettingTypeForgetSetPwd,                /**< 忘记密码设置密码*/
};

@interface PDLoginSettingPwdViewController : AbstractViewController

@property (nonatomic,assign)loginPwdSettingType pageType;           /**< 页面类型*/
@property (nonatomic,copy)NSString *transferAccount;                /**< 上个页面传递的账户*/
@property (nonatomic,copy)NSString *transferVcode;                  /**< 上个页面传递的验证码*/

@end
