//
//  PDLoginSettingPwdViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginSettingPwdViewController.h"
#import "PDBindingButtonCell.h"                 // 按钮
#import "PDLoginClauseCell.h"                   // 用户协议
#import "PDPhoneBindingInputCell.h"
#import "PDRegisterIsEighteenViewController.h"
#import "PDVCodeSingleModel.h"

@interface PDLoginSettingPwdViewController()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,PDPhoneBindingInputCellDelegate>
@property (nonatomic,strong)UITableView *pwdTableView;
@property (nonatomic,strong)NSArray *pwdArr;

// temp
@property (nonatomic,strong)UITextField *tempOneTextField;          /**< 第一个*/
@property (nonatomic,assign)BOOL tempOneEnable;
@property (nonatomic,strong)UITextField *tempTwoTextField;          /**< 第二个*/
@property (nonatomic,assign)BOOL tempTwoEnable;
@end

@implementation PDLoginSettingPwdViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    if(![cellWithRowOne.inputTextField isFirstResponder]){
        [cellWithRowOne.inputTextField becomeFirstResponder];
    }
    [self sendRequestToGetVirsionCode];
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (self.pageType == loginPwdSettingTypeAdd){                   // 【设置密码】
        self.barMainTitle = @"设置密码";
    } else if (self.pageType == loginPwdSettingTypeReset){          // 【重置密码】
        self.barMainTitle = @"重置密码";
    } else if (self.pageType == loginPwdSettingTypeForget){         // 【找回密码】
        self.barMainTitle = @"密码找回";
    } else if (self.pageType == loginPwdSettingTypeForgetSetPwd){   // 【找回密码重新设置密码】
        self.barMainTitle = @"重新设置密码";
    }
}

#pragma mark - ArrayWithInit
-(void)arrayWithInit{
    if (self.pageType == loginPwdSettingTypeAdd){           // 设置密码
        self.pwdArr = @[@[@"密码1"],@[@"密码2"],@[@"注册"],@[@"协议"]];
    } else if (self.pageType == loginPwdSettingTypeReset){      // 重置密码
        self.pwdArr = @[@[@"密码1"],@[@"密码2"],@[@"注册"]];
    } else if (self.pageType == loginPwdSettingTypeForget){     //  忘记密码
        self.pwdArr = @[@[@"手机"],@[@"图形验证码",@"验证码"],@[@"按钮"]];
    } else if (self.pageType == loginPwdSettingTypeForgetSetPwd){   // 忘记密码-设置密码
        self.pwdArr = @[@[@"密码"],@[@"密码2"],@[@"按钮"]];
    }
}

#pragma mark - createTableView
-(void)createTableView{
    if (!self.pwdTableView){
        self.pwdTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.pwdTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.pwdTableView.delegate = self;
        self.pwdTableView.dataSource = self;
        self.pwdTableView.backgroundColor = [UIColor whiteColor];
        self.pwdTableView.showsVerticalScrollIndicator = NO;
        self.pwdTableView.scrollEnabled = YES;
        self.pwdTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.pwdTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.pwdArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.pwdArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0 || indexPath.section == 1){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDPhoneBindingInputCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDPhoneBindingInputCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.delegate = self;
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (self.pageType == loginPwdSettingTypeForget){            // 【忘记密码】
            if (indexPath.section == 0){                        // 手机号码
                cellWithRowOne.transferType = PhoneBindingInputCellTypeUser;
                cellWithRowOne.inputTextField.placeholder = @"手机号";
            } else if (indexPath.section == 1){                 // 验证码
                __weak typeof(self)weakSelf = self;
                if (indexPath.row == 0){                    // 图形验证码
                    cellWithRowOne.transferType = PhoneBindingInputCellTypeMineVCode;

                } else {                                    // 验证码
                    cellWithRowOne.transferType = PhoneBindingInputCellTypeSmsCode;

                    cellWithRowOne.block = ^(){
                        if (!weakSelf){
                            return ;
                        }
                        __strong typeof(weakSelf)strongSelf = weakSelf;
                        [strongSelf verificationPhoneNumber];
                    };
                }
            }
        } else if (self.pageType == loginPwdSettingTypeAdd || self.pageType == loginPwdSettingTypeReset || self.pageType == loginPwdSettingTypeForgetSetPwd){        // 【设置密码】&& 【重置密码】 && 【忘记密码】
            if (indexPath.section == 0){
                cellWithRowOne.transferType = PhoneBindingInputCellTypePwd;
            } else if (indexPath.section == 1){
                cellWithRowOne.transferType = PhoneBindingInputCellTypePwdAgain;
            }
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        return cellWithRowOne;
    } else if (indexPath.section == 2){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDBindingButtonCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDBindingButtonCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            [cellWithRowTwo showEnableWithStatus:NO];
        }
        
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo.cusomterButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf confirmButtonClickManager];
        }];
        
        if (self.pageType == loginPwdSettingTypeAdd){
            cellWithRowTwo.bingdingType = PDBindingButtonCellInRegister;
        } else if (self.pageType == loginPwdSettingTypeForget){
            cellWithRowTwo.bingdingType = PDBindingButtonCellFindPwd;
        } else if (self.pageType == loginPwdSettingTypeForgetSetPwd){
            cellWithRowTwo.bingdingType = PDBindingButtonCellTypeSend;
        }
        
        cellWithRowTwo.transferCellHeight = cellHeight;
        
        return cellWithRowTwo;
    } else if (indexPath.section == 3){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PDLoginClauseCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[PDLoginClauseCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        if (self.pageType == loginPwdSettingTypeAdd){
            cellWithRowThr.transferType = PDLoginClauseCellType1;
        }
        
        cellWithRowThr.transferCellHeight = cellHeight;
        return cellWithRowThr;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.pwdTableView) {
        if (indexPath.section == 0 || indexPath.section == 1){
            SeparatorType separatorType = SeparatorTypeBottom;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"login"];
        }
    }
}



-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0 || section == 1){
        return LCFloat(16);
    } else if (section == 2){
        return LCFloat(107);
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    if (section == 0){
        headerView.backgroundColor = [UIColor clearColor];
    } else {
        headerView.backgroundColor = [UIColor whiteColor];
    }
    return headerView;
}


#pragma mark - OtherManager
-(void)buttonStatus:(BOOL)status{
    PDBindingButtonCell *cellWithRowTwo = (PDBindingButtonCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    [cellWithRowTwo showEnableWithStatus:status];
}

#pragma mark 按钮点击方法
-(void)confirmButtonClickManager{
    [self touchManager];
    
    if (self.pageType == loginPwdSettingTypeAdd){               // 设置密码
        [self verificationRegister];
    } else if (self.pageType == loginPwdSettingTypeReset){      // 重置密码
        
    } else if (self.pageType == loginPwdSettingTypeForget){     // 忘记密码
        [self verificationForget];
    } else if (self.pageType == loginPwdSettingTypeForgetSetPwd){   // 忘记密码重置密码
        [self verificationForgetSetPwd];
    }
}

-(void)touchManager{
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];

    if([cellWithRowOne.inputTextField isFirstResponder]){
        [cellWithRowOne.inputTextField resignFirstResponder];
    } else if([cellWithRowTwo.inputTextField isFirstResponder]){
        [cellWithRowTwo.inputTextField resignFirstResponder];
    }
}


#pragma mark -PDPhoneBindingInputCellDelegate
-(void)inputTextConfirm:(PhoneBindingInputCellType)type textField:(UITextField *)textField status:(BOOL)status{
    if (self.pageType == loginPwdSettingTypeAdd || self.pageType == loginPwdSettingTypeForgetSetPwd){           // 【设置密码】
        if (type == PhoneBindingInputCellTypePwd){
            if (status == YES){
                self.tempOneTextField = textField;
                self.tempOneEnable = YES;
                // 判断验证码是否通过
                if (self.tempTwoEnable == YES && [textField.text isEqualToString:self.tempTwoTextField.text]){
                    [self buttonStatus:YES];
                } else {
                    [self buttonStatus:NO];
                }
            } else {
                self.tempOneEnable = NO;
                [self buttonStatus:NO];
            }
        } else if (type == PhoneBindingInputCellTypePwdAgain){
            if (status == YES){
                self.tempTwoTextField = textField;
                self.tempTwoEnable = YES;
                if (self.tempOneEnable == YES && [textField.text isEqualToString:self.tempOneTextField.text]){
                    [self buttonStatus: YES];
                } else {
                    [self buttonStatus:NO];
                }
            } else {
                self.tempTwoEnable = NO;
                [self buttonStatus:NO];
            }
        }
        
    } else if (self.pageType == loginPwdSettingTypeForget){         // 【忘记密码】
        if (type == PhoneBindingInputCellTypeUser){
            if (status == YES){
                self.tempOneTextField.text = textField.text;
                self.tempOneEnable = YES;
                // 判断验证码是否通过
                if (self.tempTwoEnable == YES){
                    [self buttonStatus:YES];
                } else {
                    [self buttonStatus:NO];
                }
            } else {
                self.tempOneEnable = NO;
                [self buttonStatus:NO];
            }
        } else if (type == PhoneBindingInputCellTypeSmsCode){       // 【验证码】
            if (status == YES){
                self.tempTwoTextField.text = textField.text;
                self.tempTwoEnable = YES;
                if (self.tempOneEnable == YES){
                    [self buttonStatus: YES];
                } else {
                    [self buttonStatus:NO];
                }
            } else {
                self.tempTwoEnable = NO;
                [self buttonStatus:NO];
            }
        } else if (type == PhoneBindingInputCellTypeMineVCode){         // 【图片验证码】
            if (status == YES){
                self.tempTwoTextField.text = textField.text;
                self.tempTwoEnable = YES;
                if (self.tempOneEnable == YES){
                    [self buttonStatus: YES];
                } else {
                    [self buttonStatus:NO];
                }
            } else {
                self.tempTwoEnable = NO;
                [self buttonStatus:NO];
            }
        }
    }
}


#pragma mark - 找回密码
-(void)sendRequestToFindPwd{
    
}

#pragma mark - Interface
#pragma mark 验证注册信息
-(void)verificationRegister{
    // 1.
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    NSString *err = @"";
    if (!cellWithRowOne.inputTextField.text.length){
        err = @"请输入密码";
    } else if (!cellWithRowTwo.inputTextField.text.length){
        err = @"请再次输入密码";
    } else if (![cellWithRowTwo.inputTextField.text isEqualToString:cellWithRowOne.inputTextField.text]){
        err = @"请确认两次输入密码是否一致";
    }
    if(err.length){
        [PDHUD showHUDError:err];
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToRegisterWithPassword:cellWithRowOne.inputTextField.text];
}

#pragma mark 进行注册
-(void)sendRequestToRegisterWithPassword:(NSString *)password{
    NSString *alert = @"熊猫君正在拼命注册中…";
    [PDHUD showHUDProgress:alert diary:0];
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]fetchWithPath:tokenRegister requestParams:@{@"account":self.transferAccount,@"password":password,@"group":@(0),@"code":self.transferVcode} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){      // 注册成功
//            【十八岁】
            if ([AccountModel sharedAccountModel].auth){
                PDRegisterIsEighteenViewController *registerEighteenVC = [[PDRegisterIsEighteenViewController alloc]init];
                registerEighteenVC.account = strongSelf.transferAccount;
                registerEighteenVC.pwd = password;
                [strongSelf.navigationController pushViewController:registerEighteenVC animated:YES];
            } else {
                [strongSelf autoLoginManagerWithAccount:strongSelf.transferAccount pwd:password];
            }
        } else {
            
        }
    }];
}

#pragma mark 验证忘记密码信息
-(void)verificationForget{
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    NSString *err = @"";
    if (!cellWithRowOne.inputTextField.text.length){
        err = @"请输入手机号";
    } else if (!cellWithRowTwo.inputTextField.text.length){
        err = @"请输入收到的验证码";
    }
    if (err.length){
        [PDHUD showHUDError:err];
        return;
    }
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToForgetPwd:cellWithRowOne.inputTextField.text vCode:cellWithRowTwo.inputTextField.text];
}

#pragma mark 接口进行重置密码验证
-(void)sendRequestToForgetPwd:(NSString *)account vCode:(NSString *)vCode{
    NSString *alert = @"熊猫君正在拼命验证…";
    [PDHUD showHUDProgress:alert diary:0];
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]fetchWithPath:resetValidate requestParams:@{@"account":account,@"code":vCode} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDLoginSettingPwdViewController *loginSettingPwdViewController = [[PDLoginSettingPwdViewController alloc]init];
            loginSettingPwdViewController.transferAccount = account;
            loginSettingPwdViewController.transferVcode = vCode;
            loginSettingPwdViewController.pageType = loginPwdSettingTypeForgetSetPwd;
            [strongSelf pushViewController:loginSettingPwdViewController animated:YES];
        } else {
        
        }
    }];
}

#pragma mark 重置密码-忘记密码
-(void)verificationForgetSetPwd{
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    NSString *err = @"";
    
    if (!cellWithRowOne.inputTextField.text.length){
        err = @"请输入密码";
    } else if (!cellWithRowTwo.inputTextField.text.length){
        err = @"请再次输入密码";
    } else if (![cellWithRowTwo.inputTextField.text isEqualToString:cellWithRowOne.inputTextField.text]){
        err = @"请确认两次输入密码是否一致";
    }
    if (err.length){
        [PDHUD showHUDError:err];
    }
    __weak typeof(self)weakSelf = self;
    
    [weakSelf sendRequestToResetPwdWithAccount:self.transferAccount code:self.transferVcode pwd:cellWithRowTwo.inputTextField.text];
}

-(void)sendRequestToResetPwdWithAccount:(NSString *)account code:(NSString *)code pwd:(NSString *)pwd{

    NSString *type = @"";
    if ([Tool validateMobile:account]){
        type = @"mobile";
    } else {
        type = @"email";
    }
    
    NSString *alert = @"熊猫君正在拼命替你重置密码…";
    [PDHUD showHUDProgress:alert diary:0];
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]fetchWithPath:resetPassword requestParams:@{@"account":account,@"code":code,@"type":type,@"password":pwd} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[PDAlertView sharedAlertView]showAlertWithTitle:@"重置成功" conten:@"请妥善保管好您的账号密码" isClose:NO btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
                [JCAlertView dismissWithCompletion:^{
                    [strongSelf autoLoginManagerWithAccount:account pwd:pwd];
                }];
            }];
        }
    }];
}

#pragma mark - 发送短信或者邮件找回密码
-(void)verificationPhoneNumber{
    [self touchManager];
    PDPhoneBindingInputCell *cellWithRowOne = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    NSString *err = @"";
    if (!cellWithRowOne.inputTextField.text.length){
        err = @"请输入账号";
    } else {
        if (!([Tool validateMobile:cellWithRowOne.inputTextField.text] || [Tool isValidateEmail:cellWithRowOne.inputTextField.text])){
            err = @"请确认输入的手机号是否正确";
        }
    }
    
    if (err.length){
        [[PDAlertView sharedAlertView]showError:err];
        return;
    }
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToResetMsmGetVCodeWithAccount:cellWithRowOne.inputTextField.text];
}

#pragma mark 发送短信或邮件找回密码
-(void)sendRequestToResetMsmGetVCodeWithAccount:(NSString *)account{
    
    NSString *type = @"";
    if ([Tool validateMobile:account]){
        type = @"mobile";
    } else {
        type = @"email";
    }
    
    NSString *alert = @"熊猫君正在逼迫服务器君下发验证码";
    [PDHUD showHUDProgress:alert diary:0];
    
    __weak typeof(self)weakSelf = self;

    if (![AccountModel sharedAccountModel].resetVerifyCode.length){
        [PDHUD showHUDProgress:@"请输入图形验证码" diary:2];
        return;
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:resetMsm requestParams:@{@"account":account,@"type":type,@"verifyCode":[AccountModel sharedAccountModel].resetVerifyCode} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [AccountModel sharedAccountModel].resetVerifyCode = @"";
            
            [PDHUD showHUDProgress:alert diary:2];
            PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[self.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1]];
            [cellWithRowTwo startTimer];                // 开启倒计时
            

            [cellWithRowTwo.inputTextField becomeFirstResponder];
        } else {
            [strongSelf sendRequestToGetVirsionCode];
        }
    }];
}

#pragma mark - 登录成功dismiss方法
-(void)autoLoginManagerWithAccount:(NSString *)account pwd:(NSString *)pwd{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] loginWithUserAccount:account password:pwd handle:^(BOOL interfaceSuccess, BOOL socketSuccess) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        for (UIViewController *controller in strongSelf.navigationController.viewControllers){
            if ([controller isKindOfClass:[PDLoginMainViewController class]]){
                PDLoginMainViewController *loginMainController = (PDLoginMainViewController *)controller;
                [self dismissViewControllerAnimated:YES completion:^{
                    if (loginMainController.dismissBlock){
                        loginMainController.dismissBlock();
                    }
                }];
                return ;
            }
        }
    }];
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    [self touchManager];
}



// 2010-10-10【获取图形验证码】
#pragma mark - 获取图片验证码
-(void)sendRequestToGetVirsionCode{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberverifycode requestParams:nil responseObjectClass:[PDVCodeSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDVCodeSingleModel *singleModel = (PDVCodeSingleModel *)responseObject;
            PDPhoneBindingInputCell *cellWithRowTwo = (PDPhoneBindingInputCell *)[strongSelf.pwdTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
            [cellWithRowTwo.vCodebutton changeCodeImg:singleModel.img];
            cellWithRowTwo.inputTextField.text = @"";
            [AccountModel sharedAccountModel].resetVerifyCode = @"";
        }
    }];
}

-(void)vCodeClickManager{
    [self sendRequestToGetVirsionCode];
}

@end
