//
//  PDFirstChooseGameTypeViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDFirstChooseGameTypeViewController.h"
#import "NewPagedFlowView.h"
#import "PGIndexBannerSubiew.h"
#import "PDFirstChooseGameCollectionViewCell.h"

static char chooseGameBlockKey;
@interface PDFirstChooseGameTypeViewController ()<NewPagedFlowViewDelegate, NewPagedFlowViewDataSource>{
    NewPagedFlowView *pageFlowView;
}
@property (nonatomic, strong) NSMutableArray *imageArray;
@property (nonatomic,strong)UIButton *actionButton;
@property (nonatomic,strong)UIView *fontBgImgView;
@property (nonatomic,strong)PDImageView *fontImageView;

@end

@implementation PDFirstChooseGameTypeViewController

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    if (pageFlowView.timer){
        [pageFlowView.timer invalidate];
        pageFlowView.timer = nil;
    }
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createCollectionView];
    [self createActionButton];
    [self createGameFont];
}

-(void)pageSetting{
    self.barMainTitle = @"选择游戏";
    self.view.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.imageArray = [NSMutableArray array];
    for (int index = 0; index < 5; index++) {
        UIImage *image;
        if (index == 0){
            image = [UIImage imageNamed:@"img_firstLogin_ow"];
        } else if (index == 1){
            image = [UIImage imageNamed:@"img_firstLogin_lol"];
        } else if (index == 2){
            image = [UIImage imageNamed:@"img_firstLogin_pvp"];
        } else if (index == 3){
            image = [UIImage imageNamed:@"img_firstLogin_dota"];
        } else if (index == 4){
            image = [UIImage imageNamed:@"img_firstLogin_cs"];
        }
        
        [self.imageArray addObject:image];
    }
    [AccountModel sharedAccountModel].gameType = GameTypePubg;
    [AccountModel sharedAccountModel].lastGameType = GameTypePubg;
}

#pragma mark - createCollectionView
-(void)createCollectionView{
    pageFlowView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0, 30, kScreenBounds.size.width, 300)];
    pageFlowView.backgroundColor = [UIColor clearColor];
    pageFlowView.delegate = self;
    pageFlowView.dataSource = self;
    pageFlowView.minimumPageAlpha = 1.;
    pageFlowView.isCarousel = YES;
    pageFlowView.orientation = NewPagedFlowViewOrientationHorizontal;
    pageFlowView.isOpenAutoScroll = YES;
    pageFlowView.leftRightMargin = 100;
    pageFlowView.autoTime = 3.f;
    
    UIScrollView *bottomScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    [bottomScrollView addSubview:pageFlowView];
    [pageFlowView reloadData];
    
    [self.view addSubview:bottomScrollView];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark NewPagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
    return CGSizeMake(kScreenBounds.size.width - 60, MAX((kScreenBounds.size.width - 60) * 9 / 16, LCFloat(220)));
}
- (void)didSelectCell:(UIView *)subView withSubViewIndex:(NSInteger)subIndex {
    
    NSLog(@"点击了第%ld张图",(long)subIndex + 1);
}

#pragma mark NewPagedFlowView Datasource
- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    return self.imageArray.count;
    
}

- (UIView *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    PGIndexBannerSubiew *bannerView = (PGIndexBannerSubiew *)[flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[PGIndexBannerSubiew alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width - 2 * LCFloat(30), LCFloat(220))];
        bannerView.tag = index;
    }
    bannerView.mainImageView.image = self.imageArray[index];
    
    return bannerView;
}

- (void)didScrollToPage:(NSInteger)pageNumber inFlowView:(NewPagedFlowView *)flowView {
    UIImage *image;
    UIColor *color;
    if (pageNumber == 0){
        image = [UIImage imageNamed:@"img_font_firstLogin_ow"];
        color = [UIColor colorWithCustomerName:@"ow"];
    } else if (pageNumber == 1){
        image = [UIImage imageNamed:@"img_font_firstLogin_lol"];
        color = [UIColor colorWithCustomerName:@"lol"];
    } else if (pageNumber == 2){
        image = [UIImage imageNamed:@"img_font_firstLogin_pvp"];
        color = [UIColor colorWithCustomerName:@"pvp"];
    } else if (pageNumber == 3){
        image = [UIImage imageNamed:@"img_font_firstLogin_dota"];
        color = [UIColor colorWithCustomerName:@"dota"];
    } else if (pageNumber == 4){
        image = [UIImage imageNamed:@"img_font_firstLogin_cs"];
        color = [UIColor colorWithCustomerName:@"cs"];
    }
    [AccountModel sharedAccountModel].gameType = pageNumber;
    [AccountModel sharedAccountModel].lastGameType = pageNumber;
    
    [UIView animateWithDuration:.5f animations:^{
        self.fontImageView.alpha = 0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.5f animations:^{
            self.actionButton.backgroundColor = color;
            self.fontImageView.image = image;
            self.fontImageView.frame = CGRectMake(0, (self.fontBgImgView.size_height - image.size.height) / 2., image.size.width, image.size.height);
            self.fontImageView.center_x = kScreenBounds.size.width / 2.;
            self.fontImageView.alpha = 1;
        } completion:^(BOOL finished) {
            
        }];
    }];
}

-(void)createGameFont{
    self.fontBgImgView = [[UIView alloc]init];
    self.fontBgImgView.backgroundColor = [UIColor clearColor];
    self.fontBgImgView.frame = CGRectMake(0, CGRectGetMaxY(pageFlowView.frame), kScreenBounds.size.width, LCFloat(60));
    [self.view addSubview:self.fontBgImgView];
    
    self.fontImageView = [[PDImageView alloc]init];
    self.fontImageView.backgroundColor = [UIColor clearColor];
    UIImage *lolImg = [UIImage imageNamed:@"img_font_firstLogin_ow"];
    self.fontImageView.image = lolImg;
    self.fontImageView.frame = CGRectMake(0, (self.fontBgImgView.size_height - lolImg.size.height) / 2., lolImg.size.width, lolImg.size.height);
    self.fontImageView.center_x = kScreenBounds.size.width / 2.;
    [self.fontBgImgView addSubview:self.fontImageView];
}


-(void)createActionButton{
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor colorWithCustomerName:@"蓝"];
    self.actionButton.layer.cornerRadius = LCFloat(4);
    self.actionButton.frame = CGRectMake(LCFloat(11) * 2, kScreenBounds.size.height - LCFloat(11) - LCFloat(50) - 64 - LCFloat(50), kScreenBounds.size.width - 4 * LCFloat(11), LCFloat(50));
    [self.view addSubview:self.actionButton];
    [self.actionButton setTitle:@"加入盼达电竞" forState:UIControlStateNormal];
    [self.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;

        // 保存userdefault
        [Tool userDefaulteWithKey:FirstChooseGame Obj:@"YES"];
        // 2. 抛出
        void(^block)() = objc_getAssociatedObject(strongSelf, &chooseGameBlockKey);
        if (block){
            block();
        }
        [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }];
}


-(void)chooseGameBlock:(void(^)())block{
    objc_setAssociatedObject(self, &chooseGameBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}


@end
