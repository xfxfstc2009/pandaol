//
//  PDFirstChooseGameTypeViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDFirstChooseGameTypeViewController : AbstractViewController

-(void)chooseGameBlock:(void(^)())block;

@end
