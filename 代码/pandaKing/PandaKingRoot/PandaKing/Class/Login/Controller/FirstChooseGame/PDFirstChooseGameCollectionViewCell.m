//
//  PDFirstChooseGameCollectionViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDFirstChooseGameCollectionViewCell.h"

@interface PDFirstChooseGameCollectionViewCell()
@property (nonatomic,strong)PDImageView *gameImgView;

@end

@implementation PDFirstChooseGameCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.gameImgView = [[PDImageView alloc]init];
    self.gameImgView.backgroundColor = [UIColor clearColor];
    self.gameImgView.frame = CGRectMake((self.size_width - LCFloat(220)) / 2., (self.size_height - LCFloat(220)) / 2. - LCFloat(100), LCFloat(220), LCFloat(220));
    [self addSubview:self.gameImgView];
}

-(void)setTransferImgStr:(NSString *)transferImgStr{
    _transferImgStr = transferImgStr;
    self.gameImgView.image = [UIImage imageNamed:transferImgStr];
}

@end
