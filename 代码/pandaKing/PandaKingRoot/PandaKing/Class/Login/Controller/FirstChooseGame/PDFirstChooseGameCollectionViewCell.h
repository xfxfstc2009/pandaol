//
//  PDFirstChooseGameCollectionViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDFirstChooseGameCollectionViewCell : UICollectionViewCell

@property (nonatomic,copy)NSString *transferImgStr;

@end
