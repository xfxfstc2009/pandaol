//
//  PDLoginMainViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

typedef void(^loginSuccessManager)();

@interface PDLoginMainViewController : AbstractViewController

@property (nonatomic,copy)loginSuccessManager dismissBlock;

-(void)loginSuccessDismissManager;

@end
