//
//  PDLoginMainViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLoginMainViewController.h"
#import "PDLoginSettingPwdViewController.h"         // 设置密码Controller
#import "PDLoginPhoneBindingViewController.h"       // 账号绑定

// 【Cell】
#import "PDLoginMainCell.h"                         // 登录cell
#import "PDLoginOtherCell.h"                        // 下面的dell

// 【Manager】
#import "NetworkAdapter+PDJavaLogin.h"
#import "PDOAuthLoginAndShareTool.h"                // 三方登录
#import "PDVCodeSingleModel.h"


@interface PDLoginMainViewController()<UITableViewDataSource,UITableViewDelegate,PDLoginMainDelegate,PDLoginOtherCellDelegate,PDNetworkAdapterDelegate,PDLoginSocketDelegate>{
    PDLoginOtherCell *tempCellWithRowOne;
}
@property (nonatomic,strong)UITableView *loginTableView;                /**< 登录tableView*/
@property (nonatomic,strong)NSArray *loginArr;                          /**< 登录注册按钮*/
@property (nonatomic,strong)PDOAuthLoginAndShareTool *thirdLoginTool;   /**< 三方登录*/
@property (nonatomic,strong)UIButton *leftBackButton;
@end

@implementation PDLoginMainViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (kScreenBounds.size.height < 500){
        [self.navigationController setNavigationBarHidden:YES];
    } else {
        [self.navigationController setNavigationBarHidden:NO];
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self touchManager];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                         // 页面设置
    [self arrayWithInit];                       // 数组初始化
    [self createTableView];                     // 创建列表
    [self keyBoardNotification];                // 键盘通知
    [AccountModel sharedAccountModel].hasFirstAnimation = YES;
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"账号登录";
    __weak typeof(self)weakSelf = self;
    self.leftBackButton = [weakSelf leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_main_back"] barHltImage:[UIImage imageNamed:@"icon_main_back"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDLoginOtherCell *cellWithRowOne = (PDLoginOtherCell *)[self.loginTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        [cellWithRowOne scrollToIndex:0];
        [strongSelf touchManager];
        strongSelf.leftBackButton.alpha = 0;
        strongSelf.leftBackButton.hidden = YES;
    }];
    self.leftBackButton.alpha = 0;
}

#pragma mark arrayWithInit
-(void)arrayWithInit{
    self.loginArr = @[@[@"头部",@"尾部"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.loginTableView){
        self.loginTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.loginTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.loginTableView.delegate = self;
        self.loginTableView.dataSource = self;
        self.loginTableView.backgroundColor = [UIColor clearColor];
        self.loginTableView.showsVerticalScrollIndicator = NO;
        self.loginTableView.scrollEnabled = YES;
        self.loginTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.loginTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.loginArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.loginArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.row == 0){                   // 【1.头部导航】
        static NSString *cellIdengifyWithRowOne = @"cellIdentifyWithRowOne";
        PDLoginMainCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDLoginMainCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.delegate = self;
        }
        return cellWithRowOne;
    } else {                                    // 【2.其他登录&注册】
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDLoginOtherCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDLoginOtherCell alloc]init];
            cellWithRowOne.backgroundColor = [UIColor clearColor];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.delegate = self;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        tempCellWithRowOne = cellWithRowOne;
        return cellWithRowOne;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){            // 【头部】
        return [PDLoginMainCell calculationCellHeight];
    } else {                            // 【尾部】
        return self.view.size_height - [PDLoginMainCell calculationCellHeight];
    }
}

#pragma mark - Delegate
-(void)segmentDidSelect:(NSInteger)buttonIndex{
    PDLoginOtherCell *cellWithRowOne = (PDLoginOtherCell *)[self.loginTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    [cellWithRowOne scrollToIndex:buttonIndex];
    [self touchManager];
    if (buttonIndex == 0){
        self.barMainTitle = @"账号登录";
    } else {
        self.barMainTitle = @"账号注册";
    }
}

-(void)userTouchMangerWithIndex:(otherDidSelectType)type{
    if (type == otherDidSelectTypeLogin){                       // 【登录】
        PDLoginOtherCell *cell = (PDLoginOtherCell *)[self.loginTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        __weak typeof(self)wealSelf = self;
        [wealSelf sendReqeustWithLoginWithUserName:cell.inputLoginUserView.inputTextField.text password:cell.inputLoginPwdView.inputTextField.text];
    } else if (type == otherDidSelectTypeQQ){                   // 【QQ登录】
        [self thirdLoginWithQQManagerWithThirdLoginType:PDThirdLoginTypeQQ];
    } else if (type == otherDidSelectTypeResetPwd){             // 【忘记密码】
        PDLoginSettingPwdViewController *loginSettingPwdViewController = [[PDLoginSettingPwdViewController alloc]init];
        loginSettingPwdViewController.pageType = loginPwdSettingTypeForget;
        [self.navigationController pushViewController:loginSettingPwdViewController animated:YES];
    } else if (type == otherDidSelectTypeWechat){               // 【微信登录】
        [self thirdLoginWithQQManagerWithThirdLoginType:PDThirdLoginTypeWechat];
    } else if (type == otherDidSelectTypeRegister){             // 【注册】
        PDLoginOtherCell *cell = (PDLoginOtherCell *)[self.loginTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        __weak typeof(self)weakSelf = self;
        [weakSelf sendRequestWithVericationAccount:cell.inputRegisterUserView.inputTextField.text smsCode:cell.inputRegisterPhoneSmsView.inputTextField.text];
        
        
    } else if (type == otherDidSelectTypeDelegate){             // 跳转协议
        PDWebViewController *delegateWebViewController = [[PDWebViewController alloc]init];
        [delegateWebViewController webDirectedWebUrl:registerDelegate];
        [self.navigationController pushViewController:delegateWebViewController animated:YES];
    } else if (type == otherDidSelectTypeThirdLogin){
        self.barMainTitle = @"账号登录";
        PDLoginOtherCell *cellWithRowOne = (PDLoginOtherCell *)[self.loginTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        [cellWithRowOne scrollToIndex:1];
        [self touchManager];
        
        [UIView animateWithDuration:.5f animations:^{
            self.leftBackButton.alpha = 1;
        } completion:^(BOOL finished) {
            self.leftBackButton.alpha = 1;
            self.leftBackButton.hidden = NO;
        }];
        
    } else if (type == otherDidSelectTypeThirdRegister){
        self.barMainTitle = @"账号注册";
        PDLoginOtherCell *cellWithRowOne = (PDLoginOtherCell *)[self.loginTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        [cellWithRowOne scrollToIndex:2];
        [self touchManager];
        
        [UIView animateWithDuration:.5f animations:^{
            self.leftBackButton.alpha = 1;
        } completion:^(BOOL finished) {
            self.leftBackButton.alpha = 1;
            self.leftBackButton.hidden = NO;
            
            // 获取当前图形验证码
            [self sendRequestToGetVirsionCode];
        }];
    }
}



#pragma mark - UIScrollViewDelegate
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self touchManager];
}

-(void)textFieldwillShow:(UITextField *)textField{

}

#pragma mark  验证验证码
-(void)sendRequestWithVericationAccount:(NSString *)account smsCode:(NSString *)smsCode{
    __weak typeof(self)weakSelf = self;
    if ((!account.length) || (!smsCode.length)){
        PDLoginOtherCell *cell = (PDLoginOtherCell *)[self.loginTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        NSString *title = (!account.length)?@"请输入【手机/账号】":@"请输入【验证码】";
        [[UIAlertView alertViewWithTitle:nil message:title buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (!weakSelf){
                return ;
            }
            if (!account.length){
                [cell.inputRegisterUserView.inputTextField becomeFirstResponder];
            } else {
                [cell.inputRegisterPhoneSmsView.inputTextField becomeFirstResponder];
            }
        }]show];
        return;
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:userVcode requestParams:@{@"account":account,@"code":smsCode,@"type":@(UserLoginTypeCurrent)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDLoginSettingPwdViewController *loginSettingPwdViewController = [[PDLoginSettingPwdViewController alloc]init];
            loginSettingPwdViewController.pageType = loginPwdSettingTypeAdd;
            loginSettingPwdViewController.transferAccount = account;
            loginSettingPwdViewController.transferVcode = smsCode;
            [strongSelf.navigationController pushViewController:loginSettingPwdViewController animated:YES];
        } else {
            // 无法获取到当前信息
        }
    }];
}

#pragma mark - 第三方登录接口
-(void)sendRequestToThirdLoginWithType:(PDThirdLoginType)type openId:(NSString *)openId unionId:(NSString *)unionId avatar:(NSString *)avatar nick:(NSString *)nick{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] loginThirdLoginWithType:type openId:openId unionId:unionId avatar:avatar nickname:nick withBlkc:^(BOOL interfaceSuccess, BOOL socketSuccess, PDThirdLoginBackType thirdLoginBackType) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (interfaceSuccess){          // 接口访问成功
            if (thirdLoginBackType == PDThirdLoginBackType1){         // 表示用户已经注册过了
                [strongSelf loginSuccessDismissManager];
            } else if (thirdLoginBackType == PDThirdLoginTypeType2){
                PDLoginPhoneBindingViewController *loginPhoneBindingViewController = [[PDLoginPhoneBindingViewController alloc]init];
                loginPhoneBindingViewController.transferThirdLoginType = type;              // 三方登录类型
                if (type == PDThirdLoginTypeWechat){            // 微信
                    loginPhoneBindingViewController.transferOpen_id = unionId;                   // 账号
                } else if(type == PDThirdLoginTypeQQ){
                    loginPhoneBindingViewController.transferOpen_id = openId;                   // 账号
                }
                
                loginPhoneBindingViewController.transferAvatar = avatar;
                loginPhoneBindingViewController.transferNickName = nick;
                [strongSelf.navigationController pushViewController:loginPhoneBindingViewController animated:YES];
            }
        }
    }];
}

#pragma mark - QQ 登录 && 微信登录
-(void)thirdLoginWithQQManagerWithThirdLoginType:(PDThirdLoginType)type{
    __weak typeof(self)weakSelf = self;
    
    if (type == PDThirdLoginTypeQQ){                // 【三方登录QQ】
        self.thirdLoginTool = [PDOAuthLoginAndShareTool oAuthLoginWithType:PDOAuthLoginTypeQQ respBlock:^(NSDictionary *QQUserInfo) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSString *token = [QQUserInfo objectForKey:@"open_id"];
            NSString *avatar = [QQUserInfo objectForKey:@"figureurl_qq_2"];
            NSString *nick = [QQUserInfo objectForKey:@"nickname"];
            if (!token.length){
                return;
            }
        
            [strongSelf sendRequestToThirdLoginWithType:PDThirdLoginTypeQQ openId:token unionId:token avatar:avatar nick:nick];
        }];
    } else if (type == PDThirdLoginTypeWechat){     // 【三方登录微信】
        [PDOAuthLoginAndShareTool oAuthLoginWithType:PDOAuthLoginTypeWX respBlock:^(NSDictionary *WeChatDic) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSString *token = [WeChatDic objectForKey:@"open_id"];
            NSString *avatar = [WeChatDic objectForKey:@"headimgurl"];
            NSString *nickName = [WeChatDic objectForKey:@"nickname"];
            NSString *unionId = [WeChatDic objectForKey:@"unionid"];
            
            if (!token.length){
                return;
            }
            [strongSelf sendRequestToThirdLoginWithType:PDThirdLoginTypeWechat openId:token unionId:unionId avatar:avatar nick:nickName];
        }];
    }
}

#pragma mark - Interface
#pragma mark 登录接口
-(void)sendReqeustWithLoginWithUserName:(NSString *)userName password:(NSString *)password{
    [self touchManager];
    NSString *err = @"";
    if (!userName.length){
        err = @"请输入账号名";
    } else if (!password.length){
        err = @"请输入密码";
    }
    if (err.length){
        [PDHUD showHUDError:err];
        return;
    }
    NSString *alert = @"熊猫君正在拼命登录中…";
    
    [PDHUD showHUDProgress:alert diary:0];
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] loginWithUserAccount:userName password:password handle:^(BOOL interfaceSuccess, BOOL socketSuccess) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (interfaceSuccess && socketSuccess){
            // 2. dismiss
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"登录成功" conten:@"欢迎使用盼达电竞" isClose:NO btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                [[PDAlertView sharedAlertView] dismissWithBlock:^{
                    [strongSelf loginSuccessDismissManager];
                }];
            }];
        }
    }];
}

#pragma mark - 自定义代理方法
#pragma mark 注册验证成功后进行跳转到正确页面
-(void)directToRegisterPageWithVcodeIsSuccess:(BOOL)isSuccess account:(NSString *)account smsCode:(NSString *)smsCode{
    PDLoginSettingPwdViewController *loginSettingPwdViewController = [[PDLoginSettingPwdViewController alloc]init];
    loginSettingPwdViewController.pageType = loginPwdSettingTypeAdd;
    loginSettingPwdViewController.transferAccount = account;
    loginSettingPwdViewController.transferVcode = smsCode;
    [self.navigationController pushViewController:loginSettingPwdViewController animated:YES];
}

-(void)loginMainViewControllerWillLoginWithUser:(NSString *)user pwd:(NSString *)password{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendReqeustWithLoginWithUserName:user password:password];
}


#pragma mark - OtherManager
#pragma mark 键盘通知
-(void)keyBoardNotification{
    // 添加键盘通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 添加手势
- (void)keyboardWillShow:(NSNotification *)notification {
    // 1. 获取高度
    CGFloat cellHeight = [self.loginTableView rectForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]].size.height;
//    // 2. 设置另外的高度
//    CGFloat otherCellHeight = self.view.size_height - cellHeight;
//    // 3. 充值tableview高度
//    self.loginTableView.size_height = otherCellHeight;
    // 4. 设置移动度
    [self.loginTableView setContentOffset:CGPointMake(0, cellHeight) animated:YES];
}

#pragma mark 键盘隐藏
- (void)keyboardWillHide:(NSNotification *)notification {
//    NSDictionary* userInfo = [notification userInfo];
//    
//    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
//    NSTimeInterval animationDuration;
//    [animationDurationValue getValue:&animationDuration];
//    
//    [UIView beginAnimations:nil context:NULL];
//    [UIView setAnimationDuration:animationDuration];
//    
//    self.loginTableView.frame = self.view.bounds;
    [self.loginTableView setContentOffset:CGPointMake(0, 0) animated:YES];
//
//    [UIView commitAnimations];
}

-(void)touchManager{
    PDLoginOtherCell *cell = (PDLoginOtherCell *)[self.loginTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if ([cell.inputLoginUserView.inputTextField isFirstResponder]){
        [cell.inputLoginUserView.inputTextField resignFirstResponder];
    } else if ([cell.inputLoginPwdView.inputTextField isFirstResponder]){
        [cell.inputLoginPwdView.inputTextField resignFirstResponder];
    } else if ([cell.inputRegisterUserView.inputTextField isFirstResponder]){
        [cell.inputRegisterUserView.inputTextField resignFirstResponder];
    } else if ([cell.inputRegisterPhoneSmsView.inputTextField isFirstResponder]){
        [cell.inputRegisterPhoneSmsView.inputTextField resignFirstResponder];
    }
}

-(void)loginSuccessDismissManager{
    if (self != [self.navigationController.viewControllers firstObject]){
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:^{
            if (self.dismissBlock){
                self.dismissBlock();
            }
        }];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self touchManager];
}


// 2017-10-10
-(void)vCodeGetManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetVirsionCode];
}

#pragma mark - 获取图片验证码
-(void)sendRequestToGetVirsionCode{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberverifycode requestParams:nil responseObjectClass:[PDVCodeSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDVCodeSingleModel *singleModel = (PDVCodeSingleModel *)responseObject;
            PDLoginOtherCell *cellWithRowOne = (PDLoginOtherCell *)[strongSelf.loginTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            [cellWithRowOne.inputMineCodeView.mineCodeButton changeCodeImg:singleModel.img];
        }
    }];
}


@end
