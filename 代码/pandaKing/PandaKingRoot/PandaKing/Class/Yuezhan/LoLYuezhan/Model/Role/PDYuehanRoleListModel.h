//
//  PDYuehanRoleListModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDYuezhanRoleGameUserSingleModel.h"

@interface PDYuehanRoleListModel : FetchModel

@property (nonatomic,assign)NSInteger count;
@property (nonatomic,strong)NSArray<PDYuezhanRoleGameUserSingleModel> *gameUsers;


@end
