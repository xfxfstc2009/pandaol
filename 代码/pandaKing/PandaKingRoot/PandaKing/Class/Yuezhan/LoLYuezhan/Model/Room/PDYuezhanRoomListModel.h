//
//  PDYuezhanRoomListModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDYuezhanRoomSingleModel.h"


@interface PDYuezhanRoomListModel : FetchModel

@property (nonatomic,strong)NSArray <PDYuezhanRoomSingleModel> *servers;


@end
