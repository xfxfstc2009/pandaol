//
//  PDYuezhanCombatRoomSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/30.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDYuezhanCombatRoomSingleModel : FetchModel

@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *password;

@end
