//
//  PDYuezhanCombatCreaterSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/30.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDYuezhanCombatCreaterSingleModel : FetchModel

@property (nonatomic,copy)NSString *memberId;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *nickName;
@property (nonatomic,assign)BOOL ready;                 /**< 是否准备*/
@property (nonatomic,assign)BOOL isFangzhu;

@end
