//
//  PDYuezhanReleaseWaitTimeSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/29.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

// 等待时间

#import "FetchModel.h"

@interface PDYuezhanReleaseWaitTimeSingleModel : FetchModel

@property (nonatomic,assign)NSInteger waitTime;                     /**< 等待时间*/
@property (nonatomic,copy)NSString *waitTimeStr;                    /**< 等待时间*/

@end
