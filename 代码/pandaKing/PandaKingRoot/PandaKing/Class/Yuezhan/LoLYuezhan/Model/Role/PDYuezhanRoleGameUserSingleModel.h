//
//  PDYuezhanRoleGameUserSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDYuezhanRoleGameUserSingleModel <NSObject>

@end

@interface PDYuezhanRoleGameUserSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *gameServerId;           /**< 大区ID*/
@property (nonatomic,copy)NSString *gameServerName;         /**< 大区名字*/
@property (nonatomic,copy)NSString *name;                   /**< 召唤师名字*/
@property (nonatomic,copy)NSString *avatar;                 /**< 头像*/

@end
