//
//  PDYuezhanCombatRootModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/30.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDYuezhanCombatSingleModel.h"

@interface PDYuezhanCombatRootModel : FetchModel

@property (nonatomic,strong)PDYuezhanCombatSingleModel *combat;

@end
