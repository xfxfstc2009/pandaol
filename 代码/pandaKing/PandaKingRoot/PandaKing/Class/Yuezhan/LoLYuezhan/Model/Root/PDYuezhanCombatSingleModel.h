//
//  PDYuezhanCombatSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/29.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDYuezhanCombatCreaterSingleModel.h"
#import "PDYuezhanCombatRoomSingleModel.h"

@protocol PDYuezhanCombatSingleModel <NSObject>

@end

@interface PDYuezhanCombatSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *password;
@property (nonatomic,assign)NSInteger gold;
@property (nonatomic,assign)BOOL encrypt;                       /**< 是否加密*/
@property (nonatomic,copy)NSString *gameServerId;
@property (nonatomic,copy)NSString *gameServerName;
@property (nonatomic,copy)NSString *state;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *groupId;

@property (nonatomic,strong)PDYuezhanCombatCreaterSingleModel *creator;
@property (nonatomic,strong)PDYuezhanCombatCreaterSingleModel *joiner;
@property (nonatomic,assign)NSTimeInterval createTime;
@property (nonatomic,copy)NSString *gameType;               /**< 约战类型*/
@property (nonatomic,copy)NSString *cancelReason;           /**< 取消原因*/
@property (nonatomic,assign)BOOL creatorWin;                /**< 创建者胜利*/
@property (nonatomic,assign)NSTimeInterval currentTime;
@property (nonatomic,assign)NSTimeInterval matchWaitTimeOutTime;    /**< 匹配超时时间*/
@property (nonatomic,assign)NSTimeInterval matchedTime;             /**< 匹配到的时间*/
@property (nonatomic,assign)NSTimeInterval readyTimeoutTime;        /**< 准备超时时间*/
@property (nonatomic,assign)NSTimeInterval settleTime;              /**< 结算时间*/
@property (nonatomic,strong)PDYuezhanCombatRoomSingleModel *room;   /**< room*/


@end
