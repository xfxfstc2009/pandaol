//
//  PDYuezhanRoomSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDYuezhanRoomSingleModel  <NSObject>

@end

@interface PDYuezhanRoomSingleModel : FetchModel

@property (nonatomic,assign)NSInteger encryptWaitMembersCount;
@property (nonatomic,copy)NSString *gameServerName;                         /**< 区服数量*/
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)NSInteger joinMembersCount;
@property (nonatomic,assign)NSInteger waitMembersCount;
@property (nonatomic,copy)NSString *avatar;

@end
