//
//  PDYuezhanGoundsSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDYuezhanGoundsSingleModel <NSObject>

@end

@interface PDYuezhanGoundsSingleModel : FetchModel

@property (nonatomic,assign)NSInteger gold;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)NSInteger inGroundMembersCount;
@property (nonatomic,copy)NSString *roomName;


@end
