//
//  PDYuezhanReleaseSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDYuezhanReleaseSingleModel : FetchModel

@property (nonatomic,copy) NSString *gameType;                  /**< 游戏类型*/
@property (nonatomic,assign)BOOL encrypt;                       /**< 是否加密*/
@property (nonatomic,assign)NSInteger gold;                     /**< 金币数量*/
@property (nonatomic,assign)NSInteger matchWaitSeconds;         /**< 等待时间*/
@property (nonatomic,copy)NSString *gameUserId;                 /**< 召唤师id*/


// temp
@property (nonatomic,copy)NSString *gamePwd;

@end
