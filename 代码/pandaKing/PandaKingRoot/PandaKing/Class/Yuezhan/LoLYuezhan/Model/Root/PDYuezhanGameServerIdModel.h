//
//  PDYuezhanGameServerIdModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDYuezhanGameServerIdModel : FetchModel

@property (nonatomic,copy)NSString *gameServerId;

@end
