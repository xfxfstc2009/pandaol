//
//  PDYuezhanReleaseWaitConfigModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDYuezhanReleaseWaitConfigModel : FetchModel

@property (nonatomic,strong)NSArray<NSString *> *waitSecondsConfig;

@end
