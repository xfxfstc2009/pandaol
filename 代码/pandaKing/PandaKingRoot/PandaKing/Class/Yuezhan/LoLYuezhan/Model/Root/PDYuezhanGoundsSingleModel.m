//
//  PDYuezhanGoundsSingleModel.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanGoundsSingleModel.h"

@implementation PDYuezhanGoundsSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

-(NSString *)roomName{
    NSString *roomNameStr = @"";
    if (_gold == 50000){
        roomNameStr = [NSString stringWithFormat:@"五万金币场-在线%li人",_inGroundMembersCount];
    } else if (_gold == 100000){
        roomNameStr = [NSString stringWithFormat:@"十万金币场-在线%li人",_inGroundMembersCount];
    } else if (_gold == 200000){
        roomNameStr = [NSString stringWithFormat:@"二十万金币场-在线%li人",_inGroundMembersCount];
    } else if (_gold == 500000){
        roomNameStr = [NSString stringWithFormat:@"五十万金币场-在线%li人",_inGroundMembersCount];
    } else if (_gold == 1000000){
        roomNameStr = [NSString stringWithFormat:@"一百万金币场-在线%li人",_inGroundMembersCount];
    } else if (_gold == 5000000){
        roomNameStr = [NSString stringWithFormat:@"五百万金币场-在线%li人",_inGroundMembersCount];
    }
    return roomNameStr;
}

@end
