//
//  PDYuezhanGoundsListModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDYuezhanGoundsSingleModel.h"
#import "PDYuezhanCombatSingleModel.h"

@interface PDYuezhanGoundsListModel : FetchModel

@property (nonatomic,strong)NSArray<PDYuezhanGoundsSingleModel > * grounds;
@property (nonatomic,strong)NSArray<PDYuezhanCombatSingleModel > *combats;


@end
