//
//  PDYuezhanChuzhengItemView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,yuezhanChuzhengType) {
    yuezhanChuzhengTypeQufu = 0,                        /**< 出征区服*/
    yuezhanChuzhengTypeZhaohuanshi = 1,                 /**< 出征召唤师*/
};

@interface PDYuezhanChuzhengItemView : UIView

@property (nonatomic,assign)yuezhanChuzhengType transferType;
@property (nonatomic,copy)NSString *transferTitle;

-(void)itemClickBlock:(void(^)())block;
@end
