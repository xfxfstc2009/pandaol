//
//  PDYuezhanMineNone.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanMineNone.h"

@interface PDYuezhanMineNone()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@end

@implementation PDYuezhanMineNone

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.image = [UIImage imageNamed:@"icon_nodata_panda"];
    self.avatarImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(29)) / 2., 0, LCFloat(29), LCFloat(29));
    [self addSubview:self.avatarImgView];
    self.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    self.titleLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    self.titleLabel.text = @"当前没有约战，请创建或者加入战斗";
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    CGFloat margin = (LCFloat(150) - LCFloat(29) - LCFloat(11) - [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]]) / 2.;
    self.avatarImgView.orgin_y = margin;
    self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.avatarImgView.frame) + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(150);
}

@end
