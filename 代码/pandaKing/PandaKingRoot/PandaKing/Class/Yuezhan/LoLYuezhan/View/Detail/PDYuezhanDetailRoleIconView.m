//
//  PDYuezhanDetailRoleIconView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanDetailRoleIconView.h"

static char yuezhanRoleActionClickBLockKey;
@interface PDYuezhanDetailRoleIconView()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *fangzhuLabel;
@property (nonatomic,strong)UIButton *actionButton;


@end

@implementation PDYuezhanDetailRoleIconView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake((self.size_width - LCFloat(60)) / 2., LCFloat(11), LCFloat(60), LCFloat(60));
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_width / 2.;
    self.avatarImgView.clipsToBounds = YES;
    [self addSubview:self.avatarImgView];
    
    // 2. 创建label
    self.nameLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"金"];
    self.nameLabel.adjustsFontSizeToFitWidth = YES;
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.avatarImgView.frame) + LCFloat(11), self.size_width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    [self addSubview:self.nameLabel];
    
    self.fangzhuLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    self.fangzhuLabel.backgroundColor = [UIColor colorWithCustomerName:@"金"];
    self.fangzhuLabel.font = [UIFont systemFontOfCustomeSize:8.];
    self.fangzhuLabel.textAlignment = NSTextAlignmentCenter;
    self.fangzhuLabel.text = @"房主";
    [self addSubview:self.fangzhuLabel];
    CGSize fangzhuSize = [self.fangzhuLabel.text sizeWithCalcFont:self.fangzhuLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fangzhuLabel.font])];
    
    CGFloat width = fangzhuSize.width + 2 * LCFloat(7);
    CGFloat height = [NSString contentofHeightWithFont:self.fangzhuLabel.font] + 2 * LCFloat(4);
    self.fangzhuLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(7) - width, CGRectGetMaxY(self.avatarImgView.frame) - height, width, height);
    self.fangzhuLabel.layer.cornerRadius = MIN(self.fangzhuLabel.size_width, self.fangzhuLabel.size_height) / 2;
    self.fangzhuLabel.clipsToBounds = YES;
    
    // 4. 创建按钮
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &yuezhanRoleActionClickBLockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.actionButton];
    
    self.size_height = CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11);
}

-(void)yuezhanRoleActionClickBLock:(void(^)())block{
    objc_setAssociatedObject(self, &yuezhanRoleActionClickBLockKey, self, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setTransferSingleModel:(PDYuezhanCombatCreaterSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    if (!transferSingleModel){
        self.avatarImgView.image = [UIImage imageNamed:@"icon_yuezhan_not_ready"];
        self.nameLabel.text = @"***";
        self.fangzhuLabel.hidden = YES;
    } else {
        [self.avatarImgView uploadImageWithURL:transferSingleModel.avatar placeholder:nil callback:NULL];
        self.nameLabel.text = transferSingleModel.nickName;
        if (transferSingleModel.isFangzhu){
            self.fangzhuLabel.hidden = NO;
        } else {
            self.fangzhuLabel.hidden = YES;
        }
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(60);
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end
