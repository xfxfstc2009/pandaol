//
//  PDYuezhanChuzhengRoleTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDYuezhanChuzhengItemView.h"

@interface PDYuezhanChuzhengRoleTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,assign)yuezhanChuzhengType transferType;
@property (nonatomic,copy)NSString *transferTitle;


-(void)itemActionClickBlock:(void(^)())block;

+(CGFloat)calculationCellHeight;

@end
