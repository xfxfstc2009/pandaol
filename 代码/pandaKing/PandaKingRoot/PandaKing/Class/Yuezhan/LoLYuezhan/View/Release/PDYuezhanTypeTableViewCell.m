//
//  PDYuezhanTypeTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanTypeTableViewCell.h"

@interface PDYuezhanTypeTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *typeBgView;

@end

static char itemsClickBlockKey;
@implementation PDYuezhanTypeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"浅灰"];
    [self addSubview:self.titleLabel];
    
    self.typeBgView = [[UIView alloc]init];
    self.typeBgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.typeBgView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
}

-(void)setTransferUserSelectedItems:(NSString *)transferUserSelectedItems{
    _transferUserSelectedItems = transferUserSelectedItems;
}


-(void)setTransferTypeArr:(NSArray *)transferTypeArr{
    _transferTypeArr = transferTypeArr;
    if (self.typeBgView.subviews.count){
        [self.typeBgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    __weak typeof(self)weakSelf = self;
    CGFloat origin_x = LCFloat(11);
    for (int i = 0 ;i < transferTypeArr.count;i++){
        NSString *title = [transferTypeArr objectAtIndex:i];
        UIView *menuView = [self createActionMenuWithTitle:title actionBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSString *itemStr) = objc_getAssociatedObject(strongSelf, &itemsClickBlockKey);
            if (block){
                block(title);
            }
        }];
        menuView.frame = CGRectMake(origin_x, 0, menuView.size_width, menuView.size_height);
        [self.typeBgView addSubview:menuView];
        origin_x += menuView.size_width + LCFloat(11);
    }
    // frame
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    self.typeBgView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), kScreenBounds.size.width, 44);
    
}


-(UIView *)createActionMenuWithTitle:(NSString *)title actionBlock:(void(^)())block{
    UIView *menuView = [[UIView alloc]init];
    menuView.backgroundColor = RGB(242, 242, 242, 1);
    menuView.userInteractionEnabled = YES;
    
    // 1. 创建label
    UILabel *titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    [menuView addSubview:titleLabel];
    
    UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    actionButton.backgroundColor = [UIColor clearColor];
    [menuView addSubview:actionButton];
    
    __weak typeof(self)weakSelf = self;
    [actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    
    // 计算宽高
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:titleLabel.font])];
    CGFloat width = titleSize.width + 2 * (LCFloat(11) + LCFloat(5));
    CGFloat height = [NSString contentofHeightWithFont:titleLabel.font] + 2 * LCFloat(7);
    menuView.frame = CGRectMake(0, 0, width, height);
    actionButton.frame = menuView.bounds;
    titleLabel.frame = menuView.bounds;
    
    menuView.clipsToBounds = YES;
    menuView.layer.cornerRadius = MIN(menuView.size_width, menuView.size_height) / 2.;
    
    if ([title isEqualToString:self.transferUserSelectedItems]){
        menuView.backgroundColor = RGB(187, 151, 83, 1);
        titleLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    } else {
        menuView.backgroundColor = RGB(242, 242, 242, 1);
        titleLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    }
    
    return menuView;
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(44);
    cellHeight += LCFloat(11);
    return cellHeight;
}

-(void)itemsClickBlock:(void(^)(NSString *itemsStr))block{
    objc_setAssociatedObject(self, &itemsClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
