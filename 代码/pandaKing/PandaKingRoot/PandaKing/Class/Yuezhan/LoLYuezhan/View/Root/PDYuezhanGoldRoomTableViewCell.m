//
//  PDYuezhanGoldRoomTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanGoldRoomTableViewCell.h"

static char actionClickWithBlockKey;
@interface PDYuezhanGoldRoomTableViewCell()
@property (nonatomic,strong)UIView *bgView;

@end

@implementation PDYuezhanGoldRoomTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.userInteractionEnabled = YES;
    [self addSubview:self.bgView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, transferCellHeight);
}



-(void)setTransferSceneArr:(NSArray<PDYuezhanGoundsSingleModel> *)transferSceneArr{
    _transferSceneArr = transferSceneArr;
    if (self.bgView.subviews.count){
        [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    // width
    CGFloat margin_width = (kScreenBounds.size.width - 3 * LCFloat(105)) / 4;
    CGFloat margin_height = LCFloat(11);
    for (int i = 0 ; i < transferSceneArr.count;i++){
        PDYuezhanGoundsSingleModel *singleModel = [transferSceneArr objectAtIndex:i];
        UIView *bgView = [self createBgView:singleModel];
        bgView.backgroundColor = [UIColor clearColor];
        
        CGFloat origin_x = margin_width + (margin_width + bgView.size_width) * (i % 3);
        CGFloat origin_y = margin_height + (bgView.size_height + margin_height) *( i / 3);
        bgView.frame = CGRectMake(origin_x, origin_y, bgView.size_width, bgView.size_height);
        [self.bgView addSubview:bgView];
    }
}


-(UIView *)createBgView:(PDYuezhanGoundsSingleModel *)singleModel{
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor clearColor];
    bgView.frame = CGRectMake(0, 0, 105, 109);
    bgView.userInteractionEnabled = YES;
    
    // 1. gold
    PDImageView *iconimgView = [[PDImageView alloc]init];
    iconimgView.image = [UIImage imageNamed:@"icon_yule_root_goldScene"];
    iconimgView.frame = CGRectMake(0, 0, LCFloat(100), LCFloat(131));
    [bgView addSubview:iconimgView];
    
    // 2. 创建金币场
    PDImageView *goldSceneTitle = [[PDImageView alloc]init];
    UIImage *goldImg;
    if (singleModel.gold == 50000){
        goldImg = [UIImage imageNamed:@"icon_yule_root_5gold"];
        iconimgView.image = [UIImage imageNamed:@"icon_lol_yuezhan_5wan"];
    } else if (singleModel.gold == 100000){
        goldImg = [UIImage imageNamed:@"icon_yule_root_10gold"];
        iconimgView.image = [UIImage imageNamed:@"icon_lol_yuezhan_10wan"];
    } else if (singleModel.gold == 200000){
        goldImg = [UIImage imageNamed:@"icon_yule_root_20gold"];
        iconimgView.image = [UIImage imageNamed:@"icon_lol_yuezhan_20wan"];
    } else if (singleModel.gold == 500000){
        goldImg = [UIImage imageNamed:@"icon_yule_root_50gold"];
        iconimgView.image = [UIImage imageNamed:@"icon_lol_yuezhan_50wan"];
    } else if (singleModel.gold == 1000000){
        goldImg = [UIImage imageNamed:@"icon_yule_root_100gold"];
        iconimgView.image = [UIImage imageNamed:@"icon_lol_yuezhan_100wan"];
    } else if (singleModel.gold == 5000000){
        goldImg = [UIImage imageNamed:@"icon_yule_root_500gold"];
        iconimgView.image = [UIImage imageNamed:@"icon_lol_yuezhan_500wan"];
    } else {
        iconimgView.image = [UIImage imageNamed:@"icon_yule_root_goldScene"];
    }
    goldSceneTitle.image = goldImg;
    goldSceneTitle.frame = CGRectMake((iconimgView.size_width - goldImg.size.width) / 2., CGRectGetMaxY(iconimgView.frame) + LCFloat(11), goldImg.size.width, goldImg.size.height);
    [bgView addSubview:goldSceneTitle];
    
    UILabel *numberLabel = [GWViewTool createLabelFont:@"提示" textColor:@"红"];
    numberLabel.frame = CGRectMake(0, CGRectGetMaxY(goldSceneTitle.frame) + LCFloat(11), iconimgView.size_width, [NSString contentofHeightWithFont:numberLabel.font]);
    numberLabel.text = [NSString stringWithFormat:@"%li人在线",singleModel.inGroundMembersCount];
    numberLabel.textAlignment = NSTextAlignmentCenter;
    [bgView addSubview:numberLabel];
    
    UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    actionButton.frame = bgView.bounds;
    __weak typeof(self)weakSelf = self;
    [actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(PDYuezhanGoundsSingleModel *singleModel) = objc_getAssociatedObject(strongSelf, &actionClickWithBlockKey);
        if (block){
            block(singleModel);
        }
    }];
    [bgView addSubview:actionButton];
    
    CGFloat bgViewSize_height = iconimgView.size_height + LCFloat(11) + goldImg.size.height + LCFloat(11) + numberLabel.size_height + LCFloat(11);
    bgView.frame = CGRectMake(0, 0, LCFloat(100), bgViewSize_height);
    
    return bgView;
}

-(void)actionClickWithBlock:(void(^)(PDYuezhanGoundsSingleModel *singleModel))block{
    objc_setAssociatedObject(self, &actionClickWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}


+(CGFloat)calculationCellHeightWithSceneArr:(NSArray *)array{
    NSInteger number = 0;
    if (array.count % 3 == 0){
        number = array.count / 3;
    } else {
        number = array.count / 3 + 1;
    }
    
    CGFloat itemSizeHeight = LCFloat(131) + LCFloat(11) + LCFloat(16) + LCFloat(11) + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]] + LCFloat(11);
    
    CGFloat origin_y = LCFloat(11) + number * (itemSizeHeight + LCFloat(11));
    
    return origin_y;
}

@end
