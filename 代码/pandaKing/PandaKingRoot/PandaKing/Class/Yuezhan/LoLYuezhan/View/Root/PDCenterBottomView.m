//
//  PDCenterBottomView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDCenterBottomView.h"

static char actionClickBlockKey;
@interface PDCenterBottomView()
@property (nonatomic,strong)UIButton *personButton;
@property (nonatomic,strong)UIButton *createButton;
@property (nonatomic,strong)UIButton *joinButton;

@end

@implementation PDCenterBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.personButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.personButton.backgroundColor = [UIColor clearColor];
    self.personButton.frame = CGRectMake(0, 0, LCFloat(50), LCFloat(50));
    [self.personButton setImage:[UIImage imageNamed:@"icon_yuezhan_root_mine"] forState:UIControlStateNormal];
    [self addSubview:self.personButton];
    __weak typeof(self)weakSelf = self;
    [self.personButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(yuezhanBottomType type) = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
        if (block){
            block(yuezhanBottomTypePerson);
        }
    }];
    
    
    // 2. 创建
    self.createButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.createButton.backgroundColor = c26;
    [self.createButton setTitle:@"创建约战" forState:UIControlStateNormal];
    [self addSubview:self.createButton];
    CGFloat width = (kScreenBounds.size.width - LCFloat(50)) / 2.;
    self.createButton.frame = CGRectMake(CGRectGetMaxX(self.personButton.frame), 0, width, self.personButton.size_height);
    [self.createButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(yuezhanBottomType type) = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
        if (block){
            block(yuezhanBottomTypeCreate);
        }
    }];

    // 3. 加入
    self.joinButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.joinButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self.joinButton setTitle:@"加入约战" forState:UIControlStateNormal];
    self.joinButton.frame = CGRectMake(CGRectGetMaxX(self.createButton.frame), 0, width, self.personButton.size_height);
    [self addSubview:self.joinButton];
    [self.joinButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(yuezhanBottomType type) = objc_getAssociatedObject(strongSelf, &actionClickBlockKey);
        if (block){
            block(yuezhanBottomTypeJoin);
        }
    }];
}

-(void)actionClickBlock:(void(^)(yuezhanBottomType yuezhanBottomTypes))block{
    objc_setAssociatedObject(self, &actionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
