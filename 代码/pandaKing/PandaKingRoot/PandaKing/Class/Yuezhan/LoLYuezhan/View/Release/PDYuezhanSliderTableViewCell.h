//
//  PDYuezhanSliderTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDYuezhanReleaseWaitTimeSingleModel.h"
#import "PDYuezhanReleaseMoneySingleModel.h"


@protocol PDYuezhanRoleGameUserSingleModelDelegate <NSObject>

-(void)sliderDidChangerWithMoneyItems:(PDYuezhanReleaseMoneySingleModel *)items;                 /**< 金币场金币数*/
-(void)sliderDidChangerWithWaitingTimeModel:(PDYuezhanReleaseWaitTimeSingleModel *)singleModel;

@end

@interface PDYuezhanSliderTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,copy)NSString *transferTitle;

@property (nonatomic,strong)NSArray *transferWaitingTimeArr;                /**< 传递等待时间*/
@property (nonatomic,strong)NSArray *transferMoneyArr;                      /**< 金额*/

@property (nonatomic,weak)id<PDYuezhanRoleGameUserSingleModelDelegate> delegate;

+(CGFloat)clculationCellHeight;




@end
