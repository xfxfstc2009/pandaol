//
//  PDYuezhanTypeTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDYuezhanTypeTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,assign)NSArray *transferTypeArr;
@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,copy)NSString *transferUserSelectedItems;


-(void)itemsClickBlock:(void(^)(NSString *itemsStr))block;

+(CGFloat)calculationCellHeight;

@end
