//
//  PDYuezhanZhuyiShixiangTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanZhuyiShixiangTableViewCell.h"

@interface PDYuezhanZhuyiShixiangTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *bgView;


@end

@implementation PDYuezhanZhuyiShixiangTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"灰"];
    self.titleLabel.text = @"注意事项";
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    if (!self.bgView){
        self.bgView = [[UIView alloc]init];
        self.bgView.backgroundColor = [UIColor clearColor];
        self.bgView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), kScreenBounds.size.width, 0);
        [self addSubview:self.bgView];
    }
}

-(void)setTransferCellheight:(CGFloat)transferCellheight{
    _transferCellheight = transferCellheight;
}

-(void)setTransferZhuyiArr:(NSArray *)transferZhuyiArr{
    _transferZhuyiArr = transferZhuyiArr;
    
    // 1. title
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    
    self.bgView.frame = CGRectMake(0,CGRectGetMaxY(self.titleLabel.frame), kScreenBounds.size.width, self.transferCellheight - CGRectGetMaxY(self.titleLabel.frame) - LCFloat(11));
    
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(11);
    CGFloat mainFrame = LCFloat(11);
    for (int i = 0 ; i < transferZhuyiArr.count;i++){
        NSString *titleStr = [transferZhuyiArr objectAtIndex:i];
        UILabel *titleLabel = [GWViewTool createLabelFont:@"提示" textColor:@"黑"];
        titleLabel.text = titleStr;
        titleLabel.numberOfLines = 0;
        [self.bgView addSubview:titleLabel];
        
        CGSize contentSize = [titleLabel.text sizeWithCalcFont:titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
        titleLabel.frame = CGRectMake(LCFloat(11), mainFrame, width, contentSize.height);
        mainFrame += contentSize.height + LCFloat(11);
    }
}

+(CGFloat)calculationCellHeightWithArr:(NSArray *)zhuyiArr{
    CGFloat cellheight = LCFloat(11);
    cellheight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellheight += LCFloat(11);
    CGFloat width = kScreenBounds.size.width - 2 * LCFloat(11);
    
    for (int i = 0 ; i < zhuyiArr.count;i++){
        NSString *titleStr = [zhuyiArr objectAtIndex:i];
        CGSize contentSize = [titleStr sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"提示"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
        cellheight += contentSize.height;
        cellheight += LCFloat(11);
    }
    cellheight += LCFloat(11);
    return cellheight;
}

@end
