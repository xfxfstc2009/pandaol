//
//  PDYuezhanChuzhengItemView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanChuzhengItemView.h"

static char itemClickBlockKey;
@interface PDYuezhanChuzhengItemView()
@property (nonatomic,strong)UIView *itemView;
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;
@property (nonatomic,strong)UIButton *actionButton;



@end

@implementation PDYuezhanChuzhengItemView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.itemView = [[UIView alloc]init];
    self.itemView.backgroundColor = RGB(247, 248, 249, 1);
    self.itemView.clipsToBounds = YES;
    self.itemView.layer.cornerRadius = LCFloat(3);
    [self addSubview:self.itemView];
    
    // icon
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self.itemView addSubview:self.iconImgView];
    
    // 2.
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    [self.itemView addSubview:self.titleLabel];
    
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame), 0, titleSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    // 3.
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    self.arrowImgView.image = [UIImage imageNamed:@"icon_arrow_right"];
    [self.itemView addSubview:self.arrowImgView];
    
    // 4. 创建按钮
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &itemClickBlockKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.actionButton];
}

-(void)setTransferType:(yuezhanChuzhengType)transferType{
    _transferType = transferType;
    
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    
    // 左侧类型icon
    if (self.transferType == yuezhanChuzhengTypeQufu){
        self.iconImgView.image = [UIImage imageNamed:@"icon_yuezhan_release_area"];
    } else if (self.transferType == yuezhanChuzhengTypeZhaohuanshi){
        self.iconImgView.image = [UIImage imageNamed:@"icon_yuezhan_release_lol"];
    }
    self.iconImgView.frame = CGRectMake(0, 0, LCFloat(20), LCFloat(20));
    
    // title
    self.titleLabel.text = transferTitle;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame), LCFloat(7), titleSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    // arrow
    self.arrowImgView.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + LCFloat(11), 0, LCFloat(6), LCFloat(12));
    self.arrowImgView.center_y = self.titleLabel.center_y;
    
    // 计算宽高
    CGFloat width = 0;
    width += LCFloat(20);
    width += LCFloat(7);
    width += titleSize.width;
    width += LCFloat(11);
    width += LCFloat(6);
    width += LCFloat(7);
    self.itemView.frame = CGRectMake(0, 0, width, LCFloat(7) + [NSString contentofHeightWithFont:self.titleLabel.font] + LCFloat(7));
    self.bounds = self.itemView.bounds;
    self.actionButton.frame = self.bounds;
}

-(void)itemClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &itemClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
        
@end
