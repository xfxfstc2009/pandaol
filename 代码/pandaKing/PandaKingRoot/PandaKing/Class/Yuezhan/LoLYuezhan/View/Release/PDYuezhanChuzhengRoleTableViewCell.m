//
//  PDYuezhanChuzhengRoleTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanChuzhengRoleTableViewCell.h"

static char itemActionClickBlockKey;
@interface PDYuezhanChuzhengRoleTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *bgView;

@end

@implementation PDYuezhanChuzhengRoleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"浅灰"];
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(7), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.userInteractionEnabled = YES;
    self.bgView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(7), kScreenBounds.size.width, LCFloat(50));
    [self addSubview:self.bgView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferType:(yuezhanChuzhengType)transferType{
    _transferType = transferType;
    
    if (transferType == yuezhanChuzhengTypeQufu){
        self.titleLabel.text = @"出征区服";
    } else if (transferType == yuezhanChuzhengTypeZhaohuanshi){
        self.titleLabel.text = @"出征召唤师";
    }
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    
    if (self.bgView.subviews.count){
        [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    __weak typeof(self)weakSelf = self;
    PDYuezhanChuzhengItemView *view = [self createItemViewBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &itemActionClickBlockKey);
        if (block){
            block();
        }
    }];
    view.transferTitle = self.transferTitle;
    view.orgin_x = LCFloat(11);
    [self.bgView addSubview:view];
}



-(PDYuezhanChuzhengItemView *)createItemViewBlock:(void(^)())block{
     PDYuezhanChuzhengItemView *view = [[PDYuezhanChuzhengItemView alloc]initWithFrame:CGRectMake(0, 0, 50, 50)];
    view.userInteractionEnabled = YES;
    view.transferType = self.transferType;
    view.backgroundColor = [UIColor clearColor];
    view.orgin_x = kScreenBounds.size.width ;
    __weak typeof(self)weakSelf = self;
    [view itemClickBlock:^{
        if (!weakSelf){
            return ;
        }
        if (block) {
            block();
        }
    }];
    return view;
}


-(void)changeItemWithTitle:(NSString *)title block:(void(^)())block{
    // 1. 创建固定的view
    if (!self.bgView.subviews.count){
       
        
    } else {
        // 1. 获取到上一个Item
        PDYuezhanChuzhengItemView *lastView = (PDYuezhanChuzhengItemView *)[self.bgView.subviews firstObject];
        [UIView animateWithDuration:.5f animations:^{
            lastView.orgin_x = - lastView.size_width;
        }];
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0 ;
    cellHeight += LCFloat(7);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(7);
    cellHeight += LCFloat(50);
    cellHeight += LCFloat(7);
    return cellHeight;
}

-(void)itemActionClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &itemActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
