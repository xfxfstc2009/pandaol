//
//  PDYuezhanSliderTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanSliderTableViewCell.h"
#import "CCHStepSizeSlider.h"

@interface PDYuezhanSliderTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)CCHStepSizeSlider *slider;
@property (nonatomic,strong)NSMutableArray *waitTimeStringMutableArr;
@property (nonatomic,strong)NSMutableArray *moneyStringMutableArr;

@end

@implementation PDYuezhanSliderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建title
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"浅灰"];
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    
    self.slider = [[CCHStepSizeSlider alloc]initWithFrame:CGRectMake(LCFloat(11), 0, kScreenBounds.size.width, 30)];
    self.slider.thumbImage = [UIImage imageNamed:@"icon_yuezhan_release_slider_huakuai"];
    self.slider.backgroundColor = [UIColor clearColor];
    self.slider.type = CCHStepSizeSliderTypeStep;
    self.slider.stepColor = [UIColor whiteColor];
    self.slider.lineColor = RGB(181, 141, 70, 1);
    self.slider.titleColor = c26;
    self.slider.stepWidth = 8.;
    [self.slider addTarget:self action:@selector(sliderMove:) forControlEvents:UIControlEventTouchUpInside];
    self.slider.titleOffset = - 20;
    self.slider.titleFont = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.slider.stepTouchRate = 2;
    [self addSubview:self.slider];
    
    if (!self.waitTimeStringMutableArr){
        self.waitTimeStringMutableArr = [NSMutableArray array];
    }
    if (!self.moneyStringMutableArr){
        self.moneyStringMutableArr = [NSMutableArray array];
    }
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
}

// 等待时间
-(void)setTransferWaitingTimeArr:(NSArray *)transferWaitingTimeArr{
    _transferWaitingTimeArr = transferWaitingTimeArr;
    
    // 1. title
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    self.slider.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(7), kScreenBounds.size.width, 50);
    
    if (!self.waitTimeStringMutableArr.count){
        for (int i = 0 ; i < transferWaitingTimeArr.count;i++){
            PDYuezhanReleaseWaitTimeSingleModel *singleModel = [transferWaitingTimeArr objectAtIndex:i];
            [self.waitTimeStringMutableArr addObject:singleModel.waitTimeStr];
        }
    }
    
    self.slider.numberOfStep = self.waitTimeStringMutableArr.count;
    self.slider.titleArray = self.waitTimeStringMutableArr;
}

// 金额

-(void)setTransferMoneyArr:(NSArray *)transferMoneyArr{
    _transferMoneyArr = transferMoneyArr;
    
    // 1. title
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.slider.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(7), kScreenBounds.size.width, 50);
    
    if (!self.moneyStringMutableArr.count){
        for (int i = 0 ; i < transferMoneyArr.count;i++){
            PDYuezhanReleaseMoneySingleModel *singleModel = [transferMoneyArr objectAtIndex:i];
            [self.moneyStringMutableArr addObject:singleModel.moneyStr];
        }
    }
    
    self.slider.numberOfStep = self.moneyStringMutableArr.count;
    self.slider.titleArray = self.moneyStringMutableArr;
}

-(void)sliderMove:(CCHStepSizeSlider *)slider{
    NSInteger index = (long)slider.index;
    if (self.transferWaitingTimeArr.count){             // 表示是等待时间的cell
        PDYuezhanReleaseWaitTimeSingleModel *singleModel = [self.transferWaitingTimeArr objectAtIndex:index];
        if (self.delegate && [self.delegate respondsToSelector:@selector(sliderDidChangerWithWaitingTimeModel:)]){
            [self.delegate sliderDidChangerWithWaitingTimeModel:singleModel];
        }
    } else if (self.transferMoneyArr.count){
        PDYuezhanReleaseMoneySingleModel *singleModel = [self.transferMoneyArr objectAtIndex:index];
        if (self.delegate && [self.delegate respondsToSelector:@selector(sliderDidChangerWithMoneyItems:)]){
            [self.delegate sliderDidChangerWithMoneyItems:singleModel];
        }
    }
}

+(CGFloat)clculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    cellHeight += 40;
    
    return cellHeight;
}

@end
