//
//  PDYueZhanSingleSceneTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYueZhanSingleSceneTableViewCell.h"

@interface PDYueZhanSingleSceneTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *waitLabel;

@end

@implementation PDYueZhanSingleSceneTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    [self addSubview:self.titleLabel];
    
    self.waitLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    [self addSubview:self.waitLabel];
}


+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}


@end
