//
//  PDYuezhanZhuyiShixiangTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDYuezhanZhuyiShixiangTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellheight;

@property (nonatomic,strong)NSArray *transferZhuyiArr;

+(CGFloat)calculationCellHeightWithArr:(NSArray *)zhuyiArr;

@end
