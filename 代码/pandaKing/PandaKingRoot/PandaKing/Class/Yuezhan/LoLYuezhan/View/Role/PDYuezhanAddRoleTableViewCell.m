//
//  PDYuezhanAddRoleTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanAddRoleTableViewCell.h"

@interface PDYuezhanAddRoleTableViewCell()
@property (nonatomic,strong)PDImageView *addImgView;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation PDYuezhanAddRoleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.addImgView = [[PDImageView alloc]init];
    self.addImgView.backgroundColor = [UIColor clearColor];
    self.addImgView.image = [UIImage imageNamed:@"icon_yuezhan_addRole"];
    [self addSubview:self.addImgView];
    
    // label
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleLabel.text = @"添加LOL召唤师";
    [self addSubview:self.titleLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    
    CGFloat margin = (kScreenBounds.size.width - titleSize.width - LCFloat(11) - LCFloat(18)) / 2.;
    
    _transferCellHeight = transferCellHeight;
    self.addImgView.frame = CGRectMake(margin, (transferCellHeight - LCFloat(18)) / 2., LCFloat(18), LCFloat(18));
    
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.addImgView.frame) + LCFloat(11), 0, titleSize.width, transferCellHeight);
}

@end
