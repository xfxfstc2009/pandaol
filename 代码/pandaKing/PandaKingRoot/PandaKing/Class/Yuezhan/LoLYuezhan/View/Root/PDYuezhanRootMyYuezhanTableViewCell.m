//
//  PDYuezhanRootMyYuezhanTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/29.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanRootMyYuezhanTableViewCell.h"

@interface PDYuezhanRootMyYuezhanTableViewCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *lockIconImgView;
@property (nonatomic,strong)UILabel *pwdLabel;
@property (nonatomic,strong)UILabel *areaLabel;                         /**< 区label*/
@property (nonatomic,strong)PDImageView *qiziImgView;                   /**< 旗帜图片*/

@end

@implementation PDYuezhanRootMyYuezhanTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.layer.cornerRadius = LCFloat(3);
    self.bgView.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
    self.bgView.layer.borderWidth = 1;
    [self addSubview:self.bgView];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.avatarImgView];
    
    // 2. 创建名字
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    [self.bgView addSubview:self.titleLabel];
    
    // 3.创建密码
    self.pwdLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self.bgView addSubview:self.pwdLabel];
    
    // 4. 创建锁
    self.lockIconImgView = [[PDImageView alloc]init];
    self.lockIconImgView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.lockIconImgView];
    
    // 5. 创建区域label
    self.areaLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self.bgView addSubview:self.areaLabel];
    
    // 6. 旗帜
    self.qiziImgView = [[PDImageView alloc]init];
    self.qiziImgView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.qiziImgView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferSingleModel:(PDYuezhanCombatSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    // 0.bg
    self.bgView.frame = CGRectMake(LCFloat(11), LCFloat(7), kScreenBounds.size.width - 2 * LCFloat(11), self.transferCellHeight - 2 * LCFloat(7));
    
    // 1. 头像
    [self.avatarImgView uploadImageWithURL:transferSingleModel.avatar placeholder:nil callback:NULL];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(LCFloat(11), LCFloat(11), (self.bgView.size_height - 2 * LCFloat(11)), (self.bgView.size_height - 2 * LCFloat(11)));
    
    self.titleLabel.text = transferSingleModel.name.length?transferSingleModel.name:transferSingleModel.gameServerName;
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), self.avatarImgView.orgin_y, 200, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    // 3. 锁
    self.lockIconImgView.image = [UIImage imageNamed:@"icon_root_challenge_pwd"];
    self.lockIconImgView.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), LCFloat(8), LCFloat(9));
    
    // 2. 创建锁
    self.pwdLabel.text = transferSingleModel.password;
    CGSize pwdSize = [self.pwdLabel.text sizeWithCalcFont:self.pwdLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.pwdLabel.font])];
    self.pwdLabel.frame = CGRectMake(CGRectGetMaxX(self.lockIconImgView.frame) + LCFloat(11), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), pwdSize.width, [NSString contentofHeightWithFont:self.pwdLabel.font]);
    self.pwdLabel.center_y = self.lockIconImgView.center_y;
    
    // 4. 创建地址
    self.areaLabel.text = [NSString stringWithFormat:@"%@ - %li金币场",transferSingleModel.gameServerName,transferSingleModel.gold];
    CGSize areaSize = [self.areaLabel.text sizeWithCalcFont:self.areaLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.areaLabel.font])];
    self.areaLabel.frame  = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.lockIconImgView.frame) + LCFloat(11), areaSize.width, [NSString contentofHeightWithFont:self.areaLabel.font]);
    
    // 4.1 重置高度
    CGFloat margin = (self.avatarImgView.size_height - [NSString contentofHeightWithFont:self.titleLabel.font] - LCFloat(9) - [NSString contentofHeightWithFont:self.areaLabel.font]) / 2.;
    self.lockIconImgView.orgin_y = CGRectGetMaxY(self.titleLabel.frame) + margin;
    self.pwdLabel.center_y = self.lockIconImgView.center_y;
    self.areaLabel.orgin_y = CGRectGetMaxY(self.lockIconImgView.frame) + margin;
    
    
    // 5.旗帜
//
    if ([transferSingleModel.state isEqualToString:@"matching"]){           // 匹配中
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_pipeizhong"];
        self.qiziImgView.hidden = NO;
    } else if ([transferSingleModel.state isEqualToString:@"readying"]){        // 准备中
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_zhunbeizhong"];
        self.qiziImgView.hidden = NO;
    } else if ([transferSingleModel.state isEqualToString:@"waitRoom"]){        // 等待中
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_dengdaizhong"];
        self.qiziImgView.hidden = NO;
    } else if ([transferSingleModel.state isEqualToString:@"battle"]){          // 对战中
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_zhandouzhong"];
        self.qiziImgView.hidden = NO;
    } else if ([transferSingleModel.state isEqualToString:@"settling"]){        // 结算中
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_jiesuanzhong"];
        self.qiziImgView.hidden = NO;
    } else if ([transferSingleModel.state isEqualToString:@"settled"]){         // 已结算
        self.qiziImgView.hidden = NO;
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_yijiesuan"];
    } else if ([transferSingleModel.state isEqualToString:@"cancel"]){          // 解散
        self.qiziImgView.hidden = NO;
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_yijiesan"];
    }
    
    self.qiziImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(26) - LCFloat(11) - LCFloat(11), 0, LCFloat(26), LCFloat(27));
    
    if (transferSingleModel.encrypt){
        self.lockIconImgView.hidden = NO;
        self.pwdLabel.hidden = NO;
    } else {
        self.lockIconImgView.hidden = YES;
        self.pwdLabel.hidden = YES;
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(14);
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}
@end
