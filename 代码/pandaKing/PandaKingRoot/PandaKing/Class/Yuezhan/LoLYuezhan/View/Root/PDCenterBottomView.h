//
//  PDCenterBottomView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,yuezhanBottomType) {
    yuezhanBottomTypePerson,
    yuezhanBottomTypeCreate,
    yuezhanBottomTypeJoin,
};

@interface PDCenterBottomView : UIView

-(void)actionClickBlock:(void(^)(yuezhanBottomType yuezhanBottomTypes))block;

@end
