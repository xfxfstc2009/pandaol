//
//  PDYuezhanRootMyYuezhanTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/29.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDYuezhanCombatSingleModel.h"

@interface PDYuezhanRootMyYuezhanTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDYuezhanCombatSingleModel *transferSingleModel;

+(CGFloat)calculationCellHeight;

@end
