//
//  PDYuezhanUserRoleTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanUserRoleTableViewCell.h"


static char yuezhaneditManagerWithBlockKey;
static char yuezhandeleteManagerWithBlockKey;
@interface PDYuezhanUserRoleTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)UIButton *deleteButton;
@property (nonatomic,strong)UIButton *editButton;

@end

@implementation PDYuezhanUserRoleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor lightGrayColor];
    [self addSubview:self.avatarImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    self.descLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"浅灰"];
    [self addSubview:self.descLabel];
    
    // 删除按钮
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.deleteButton setImage:[UIImage imageNamed:@"icon_yuezhan_role_delete"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.deleteButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(PDYuezhanRoleGameUserSingleModel *transferGameUserSingleModel) = objc_getAssociatedObject(strongSelf, &yuezhandeleteManagerWithBlockKey);
        if (block){
            block(strongSelf.transferGameUserSingleModel);
        }
    }];
    [self addSubview:self.deleteButton];
    
    // 编辑按钮
    self.editButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.editButton setImage:[UIImage imageNamed:@"icon_yuezhan_role_edit"] forState:UIControlStateNormal];
    [self.editButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(PDYuezhanRoleGameUserSingleModel *transferGameUserSingleModel) = objc_getAssociatedObject(strongSelf, &yuezhaneditManagerWithBlockKey);
        if (block){
            block(strongSelf.transferGameUserSingleModel);
        }
    }];
    [self.editButton setTitle:@"编辑" forState:UIControlStateNormal];
    [self addSubview:self.editButton];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferIsEdit:(BOOL)transferIsEdit{
    _transferIsEdit = transferIsEdit;
}

-(void)setTransferGameUserSingleModel:(PDYuezhanRoleGameUserSingleModel *)transferGameUserSingleModel{
    _transferGameUserSingleModel = transferGameUserSingleModel;
    
    self.avatarImgView.frame = CGRectMake(LCFloat(11), LCFloat(11), (self.transferCellHeight - 2 * LCFloat(11)), (self.transferCellHeight - 2 * LCFloat(11)));
    self.avatarImgView.clipsToBounds = YES;
    
    // 2. 名字
    self.titleLabel.text = transferGameUserSingleModel.name;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), LCFloat(11), titleSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    // 3. desc
    self.descLabel.text = transferGameUserSingleModel.gameServerName;
    CGSize gameSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.descLabel.font])];
    self.descLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11),gameSize.width, [NSString contentofHeightWithFont:self.descLabel.font]);
    
    // 4. 删除按钮
    self.deleteButton.frame =  CGRectMake(kScreenBounds.size.width - self.transferCellHeight, 0, self.transferCellHeight, self.transferCellHeight);
    
    // 5. 编辑按钮
    self.editButton.frame = CGRectMake(self.deleteButton.orgin_x - self.transferCellHeight, 0, self.transferCellHeight, self.transferCellHeight);
    
    // 1. avatar
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView uploadImageWithURL:transferGameUserSingleModel.avatar placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.avatarImgView.clipsToBounds = YES;
    }];
    
    if (self.transferIsEdit){
        self.deleteButton.hidden = NO;
        self.editButton.hidden = YES;
    } else {
        self.deleteButton.hidden = YES;
        self.editButton.hidden = YES;
    }
}

-(void)editManagerWithBlock:(void(^)(PDYuezhanRoleGameUserSingleModel *transferGameUserSingleModel))block{
    objc_setAssociatedObject(self, &yuezhaneditManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)deleteManagerWithBlock:(void(^)(PDYuezhanRoleGameUserSingleModel *transfernbGameUserSingleModel))block{
    objc_setAssociatedObject(self, &yuezhandeleteManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellheight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}
@end
