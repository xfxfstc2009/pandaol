//
//  PDYuezhanDetailRoleIconView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDYuezhanCombatCreaterSingleModel.h"


@interface PDYuezhanDetailRoleIconView : UIView

-(void)yuezhanRoleActionClickBLock:(void(^)())block;

@property (nonatomic,strong)PDYuezhanCombatCreaterSingleModel *transferSingleModel;


+(CGFloat)calculationCellHeight;
@end
