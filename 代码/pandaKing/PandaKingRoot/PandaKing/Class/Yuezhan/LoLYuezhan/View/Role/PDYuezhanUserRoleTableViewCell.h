//
//  PDYuezhanUserRoleTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDYuezhanRoleGameUserSingleModel.h"

@interface PDYuezhanUserRoleTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDYuezhanRoleGameUserSingleModel *transferGameUserSingleModel;
@property (nonatomic,assign)BOOL transferIsEdit;


+(CGFloat)calculationCellheight;

-(void)editManagerWithBlock:(void(^)(PDYuezhanRoleGameUserSingleModel *transferGameUserSingleModel))block;
-(void)deleteManagerWithBlock:(void(^)(PDYuezhanRoleGameUserSingleModel *transferGameUserSingleModel))block;
@end
