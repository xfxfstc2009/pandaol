//
//  PDYuezhanRoomListViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanRoomListViewController.h"
#import "PDYuezhanRoomsTableViewCell.h"
#import "PDYuezhanRoomListModel.h"
#import "HTHorizontalSelectionList.h"
#import "PDYuezhanDetailRootViewController.h"
#import "PDYuezhanRoleListViewController.h"
#import "PDYuezhanDetailRootViewController.h"
#import "PDYuezhanCombatRootModel.h"
#import "PDYueZhanReleaseViewController.h"

@interface PDYuezhanRoomListViewController ()<UITableViewDataSource,UITableViewDelegate,HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate>
@property (nonatomic,strong)UITableView *singleTableView;             /**< 房间号码*/
@property (nonatomic,strong)UITableView *groupTableView;             /**< 房间号码*/
@property (nonatomic,strong)NSMutableArray *singleMutableArr;        //
@property (nonatomic,strong)NSMutableArray *groupMutableArr;        //
@property (nonatomic,strong)UIScrollView *mainScrollView;           /**< mainScrollView*/
@property (nonatomic,strong)NSArray *segmentArr;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentRootList;
@property (nonatomic,strong)PDYuezhanRoomSingleModel *userChooseSingleModel;
@end


@implementation PDYuezhanRoomListViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createSegmentList];
    [self createMainScrollView];
    [self createTableView];
    [self sendRequestToGetInfoWithType:@"solo"];
    [self sendRequestToGetInfoWithType:@"fiveVsFive"];
}


#pragma mark - 创建scrollview
-(void)createMainScrollView{
    self.mainScrollView = [[UIScrollView alloc]init];
    self.mainScrollView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 64);
    self.mainScrollView.delegate = self;
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.segmentArr.count, self.mainScrollView.size_height);
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.showsVerticalScrollIndicator = NO;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.mainScrollView];
}

#pragma mark - createSegmentList
-(void)createSegmentList{
    if (!self.segmentRootList){
        self.segmentRootList = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake((kScreenBounds.size.width - LCFloat(150)) / 2., (LCFloat(44) - LCFloat(35)) / 2. ,LCFloat(150) , LCFloat(35))];
        self.segmentRootList.delegate = self;
        self.segmentRootList.dataSource = self;
        self.segmentRootList.backgroundColor = [UIColor clearColor];
        [self.segmentRootList setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
        self.segmentRootList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"白"];
        self.segmentRootList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeHeavyBounce;
        self.segmentRootList.bottomTrimColor = [UIColor clearColor];
        [self.segmentRootList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        
        self.segmentRootList.isNotScroll = YES;
        self.segmentRootList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleButtonBorder;
        self.navigationItem.titleView = self.segmentRootList;
    }
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * index, 0) animated:YES];
}


#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.singleMutableArr = [NSMutableArray array];
    self.groupMutableArr = [NSMutableArray array];
    self.segmentArr = @[@"单人赛",@"团体赛"];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.singleTableView){
        self.singleTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.singleTableView.dataSource = self;
        self.singleTableView.delegate = self;
        self.singleTableView.frame = CGRectMake(0, 0, self.view.size_width, self.mainScrollView.size_height);
        [self.mainScrollView addSubview:self.singleTableView];
    }
    if (!self.groupTableView){
        self.groupTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.groupTableView.dataSource = self;
        self.groupTableView.delegate = self;
        self.groupTableView.frame = CGRectMake(kScreenBounds.size.width, 0, self.view.size_width, self.mainScrollView.size_height);
        [self.mainScrollView addSubview:self.groupTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView == self.singleTableView){
        return self.singleMutableArr.count;
    } else if (tableView == self.groupTableView){
        return self.groupMutableArr.count;
    }
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    PDYuezhanRoomsTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDYuezhanRoomsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    if (tableView == self.singleTableView){
        cellWithRowOne.transferSingleModel = [self.singleMutableArr objectAtIndex:indexPath.row];
    } else {
        cellWithRowOne.transferSingleModel = [self.groupMutableArr objectAtIndex:indexPath.row];
    }
    
    
    return cellWithRowOne;
    
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PDYuezhanRoomSingleModel *singleModel;
    if (tableView == self.singleTableView){
        singleModel = [self.singleMutableArr objectAtIndex:indexPath.row];
    } else {
        singleModel = [self.groupMutableArr objectAtIndex:indexPath.row];
    }
    self.userChooseSingleModel = singleModel;
    
    if (singleModel.waitMembersCount <= 0){
        __weak typeof(self)weakSelf = self;
        [[PDAlertView sharedAlertView] showAlertWithTitle:@"约战提示" conten:@"当前没有约战，来创建一个吧" isClose:NO btnArr:@[@"创建",@"取消"] buttonClick:^(NSInteger buttonIndex) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
                if (buttonIndex == 0){
                    PDYueZhanReleaseViewController *yuezhanVC = [[PDYueZhanReleaseViewController alloc]init];
                    [yuezhanVC createSuccessManager:^{
                        [strongSelf createReloadManager];
                    }];
                    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:yuezhanVC];
                    [strongSelf.navigationController presentViewController:nav animated:YES completion:NULL];
                }
            }];
        }];
    } else {
        if (tableView == self.singleTableView){
            [self chooseGameRoleWithType:@"solo"];
        } else if (tableView == self.groupTableView){
            [self chooseGameRoleWithType:@"fiveVsFive"];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(50);
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *tempArr;
    if (tableView == self.singleTableView){
        tempArr = self.singleMutableArr;
    } else if (tableView == self.groupTableView){
        tempArr = self.groupMutableArr;
    }
    
    SeparatorType separatorType = SeparatorTypeMiddle;
    if ( [indexPath row] == 0) {
        separatorType = SeparatorTypeHead;
    } else if ([indexPath row] == [tempArr count] - 1) {
        separatorType = SeparatorTypeBottom;
    } else {
        separatorType = SeparatorTypeMiddle;
    }
    if ([tempArr count] == 1) {
        separatorType = SeparatorTypeSingle;
    }
    [cell addSeparatorLineWithType:separatorType];
}

-(void)sendRequestToGetInfoWithType:(NSString *)type{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"combatGameType":type,@"groundId":self.transferSingleModel.ID};
    [[NetworkAdapter sharedAdapter] fetchWithPath:gameserverlist requestParams:params responseObjectClass:[PDYuezhanRoomListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDYuezhanRoomListModel *listModel = (PDYuezhanRoomListModel *)responseObject;
            if ([type isEqualToString:@"solo"]){
                [strongSelf.singleMutableArr removeAllObjects];
                [strongSelf.singleMutableArr addObjectsFromArray:listModel.servers];
                [strongSelf.singleTableView reloadData];
            } else if ([type isEqualToString:@"fiveVsFive"]){           // 团体
                [strongSelf.groupMutableArr removeAllObjects];
                [strongSelf.groupMutableArr addObjectsFromArray:listModel.servers];
                [strongSelf.groupTableView reloadData];
            }
        }
    }];
}

#pragma mark - 选择出征玩家
-(void)chooseGameRoleWithType:(NSString *)type{
    PDYuezhanRoleListViewController *roleListVC = [[PDYuezhanRoleListViewController alloc]init];
    roleListVC.transferChooseServerId = self.userChooseSingleModel.ID;
    roleListVC.transferPageType = yuezhanRoleListViewControllerPageTypeChoose;
    __weak typeof(self)weakSelf = self;
    [roleListVC actionClickWithChooseItemBlock:^(PDYuezhanRoleGameUserSingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf joinScene:type userId:singleModel.ID];
    }];
    [self.navigationController pushViewController:roleListVC animated:YES];

}

#pragma makr - 加入金币场
-(void)joinScene:(NSString *)type userId:(NSString *)userId{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"combatGameType":type,@"groundId":self.transferSingleModel.ID,@"gameUserId":userId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:joincombat requestParams:params responseObjectClass:[PDYuezhanCombatRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDYuezhanCombatRootModel *singleModel = (PDYuezhanCombatRootModel *)responseObject;
            PDYuezhanDetailRootViewController *detailViewController = [[PDYuezhanDetailRootViewController alloc]init];
            detailViewController.detailSingleModel = singleModel;
            [strongSelf.navigationController pushViewController:detailViewController animated:YES];
        }
    }];
}

-(void)createReloadManager{
    NSInteger index = self.mainScrollView.contentOffset.x / kScreenBounds.size.width;
    if (index <= 0){
        [self sendRequestToGetInfoWithType:@"solo"];
    } else {
        [self sendRequestToGetInfoWithType:@"fiveVsFive"];
    }
}

@end
