//
//  PDYuezhanDetailRootViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDYuezhanCombatRootModel.h"

@interface PDYuezhanDetailRootViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferCombatId;
@property (nonatomic,strong)PDYuezhanCombatRootModel *detailSingleModel;
@end
