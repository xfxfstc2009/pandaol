//
//  PDYuezhanRootViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanRootViewController.h"
#import "PDYueZhanReleaseViewController.h"
#import "PDYuezhanRoleListViewController.h"                 // 成员管理
#import "PDCenterBottomView.h"                              // 创建底栏
#import "PDYuezhanGoundsListModel.h"
#import "PDYuezhanGoldRoomTableViewCell.h"
#import "PDYuezhanRoomListViewController.h"
#import "PDYuezhanDetailRootViewController.h"
#import "PDYuezhanRootMyYuezhanTableViewCell.h"
#import "PDYuezhanHistoryViewController.h"
#import "PDYuezhanCombatRootModel.h"
#import "PDTopUpViewController.h"
#import "PDYuezhanGameServerIdModel.h"
#import "PDYuezhanMineNone.h"


@interface PDYuezhanRootViewController ()<UITableViewDataSource,UITableViewDelegate>{
    PDYuezhanGoundsListModel *yuezhanGoundsListModel;
}
@property (nonatomic,strong)UITableView *yuezhanTableView;
@property (nonatomic,strong)NSMutableArray *yuezhanMutableArr;
@property (nonatomic,strong)NSMutableArray *myyuezhanMutableArr;                /**< 我的约战数组*/
@property (nonatomic,strong)PDCenterBottomView *bottomView;
@end

@implementation PDYuezhanRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createBottomView];
    [self createTableView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self sendRequestToGetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"约战中心";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_yuezhan_root_history"] barHltImage:[UIImage imageNamed:@"icon_yuezhan_root_history"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDYuezhanHistoryViewController *viewController = [[PDYuezhanHistoryViewController alloc]init];
        [strongSelf.navigationController pushViewController:viewController animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.yuezhanMutableArr = [NSMutableArray array];
    self.myyuezhanMutableArr = [NSMutableArray array];
}

-(void)createTableView{
    if (!self.yuezhanTableView){
        self.yuezhanTableView = [GWViewTool gwCreateTableViewRect:CGRectMake(0, 0, kScreenBounds.size.width, self.bottomView.orgin_y + 64)];
        self.yuezhanTableView.dataSource = self;
        self.yuezhanTableView.delegate = self;
        [self.view addSubview:self.yuezhanTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.yuezhanMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.yuezhanMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"金币场"] && indexPath.row == [self cellIndexPathRowWithcellData:@"金币场"]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferIcon = [UIImage imageNamed:@"icon_yuezhan_root_jinbichang"];
        cellWithRowOne.transferTitle = @"金币场";
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"金币场详情"] && indexPath.row == [self cellIndexPathRowWithcellData:@"金币场详情"]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDYuezhanGoldRoomTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDYuezhanGoldRoomTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferSceneArr = yuezhanGoundsListModel.grounds;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo actionClickWithBlock:^(PDYuezhanGoundsSingleModel *singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (singleModel.gold > [AccountModel sharedAccountModel].memberInfo.gold){
                [[PDAlertView sharedAlertView] showAlertWithTitle:@"提示" conten:@"金币不足，请充值" isClose:NO btnArr:@[@"充值",@"取消"] buttonClick:^(NSInteger buttonIndex) {
                    [[PDAlertView sharedAlertView]dismissAllWithBlock:^{
                        if (buttonIndex == 0){
                            PDTopUpViewController *topUpViewController = [[PDTopUpViewController alloc] init];
                            [strongSelf  pushViewController:topUpViewController animated:YES];
                        }
                    }];
                }];
            } else {
                PDYuezhanRoomListViewController *roomViewController = [[PDYuezhanRoomListViewController alloc]init];
                roomViewController.transferSingleModel = singleModel;
                [strongSelf.navigationController pushViewController:roomViewController animated:YES];
            }
        }];
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的约战"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的约战"]){
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            GWNormalTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFour.transferCellHeight = cellHeight;
            cellWithRowFour.transferTitle = @"我的约战";
            cellWithRowFour.transferIcon = [UIImage imageNamed:@"icon_yuezhan_root_challenge"];
            return cellWithRowFour;
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的约战无"]){
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            PDYuezhanMineNone *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[PDYuezhanMineNone alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            return cellWithRowFiv;
        } else {
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            PDYuezhanRootMyYuezhanTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[PDYuezhanRootMyYuezhanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
            cellWithRowFiv.transferSingleModel = [[self.yuezhanMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            return cellWithRowFiv;
        }
    }
    else {
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.backgroundColor = UURandomColor;
        return cellWithRowThr;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的约战"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的约战"]){
            
        } else {
            PDYuezhanCombatSingleModel *singleModel = [[self.yuezhanMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            PDYuezhanDetailRootViewController *yuezhanDetailViewController = [[PDYuezhanDetailRootViewController alloc]init];
            yuezhanDetailViewController.transferCombatId = singleModel.ID;
            [self.navigationController pushViewController:yuezhanDetailViewController animated:YES];
        }
    }
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"金币场"] && indexPath.row == [self cellIndexPathRowWithcellData:@"金币场"]){
        return LCFloat(44);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"金币场详情"] && indexPath.row == [self cellIndexPathRowWithcellData:@"金币场详情"]){
        return [PDYuezhanGoldRoomTableViewCell calculationCellHeightWithSceneArr:yuezhanGoundsListModel.grounds];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"我的约战"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的约战"]){
            return LCFloat(44);
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"我的约战无"]){
            return [PDYuezhanMineNone calculationCellHeight];
        } else {
            return [PDYuezhanRootMyYuezhanTableViewCell calculationCellHeight];
        }
    }
    return LCFloat(44);
}

-(void)createBottomView{
    self.bottomView = [[PDCenterBottomView alloc]initWithFrame:CGRectMake(0, kScreenBounds.size.height - LCFloat(50) - 64, kScreenBounds.size.width, LCFloat(50))];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    self.bottomView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.bottomView.layer.shadowOpacity = 0.5;
    self.bottomView.layer.shadowRadius = 3;
    self.bottomView.layer.shadowOffset = CGSizeMake(0, -1);

    __weak typeof(self)weakSelf = self;
    [self.bottomView actionClickBlock:^(yuezhanBottomType yuezhanBottomTypes) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (yuezhanBottomTypes == yuezhanBottomTypePerson){            // 成员管理
            PDYuezhanRoleListViewController *roleListVC = [[PDYuezhanRoleListViewController alloc]init];
            [strongSelf.navigationController pushViewController:roleListVC animated:YES];
        } else if (yuezhanBottomTypes == yuezhanBottomTypeCreate){
            PDYueZhanReleaseViewController *yuezhanVC = [[PDYueZhanReleaseViewController alloc]init];
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:yuezhanVC];
            [strongSelf.navigationController presentViewController:nav animated:YES completion:NULL];
        } else if (yuezhanBottomTypes == yuezhanBottomTypeJoin){
            [strongSelf showJoinGameAlert];
        }
    }];
    [self.view addSubview:self.bottomView];
}
    
#pragma mark - interface
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:combatgame requestParams:nil responseObjectClass:[PDYuezhanGoundsListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf->yuezhanGoundsListModel = (PDYuezhanGoundsListModel *)responseObject;
            if (strongSelf.yuezhanMutableArr.count){
                [strongSelf.yuezhanMutableArr removeAllObjects];
            }
            
            if (strongSelf->yuezhanGoundsListModel.combats.count){
                NSMutableArray *myChallengeMutableArr = [NSMutableArray array];
                [myChallengeMutableArr addObject:@"我的约战"];
                [myChallengeMutableArr addObjectsFromArray:strongSelf->yuezhanGoundsListModel.combats];
                [strongSelf.yuezhanMutableArr addObject:myChallengeMutableArr];
            } else {
                NSMutableArray *myChallengeMutableArr = [NSMutableArray array];
                [myChallengeMutableArr addObject:@"我的约战"];
                [myChallengeMutableArr addObject:@"我的约战无"];
                [strongSelf.yuezhanMutableArr addObject:myChallengeMutableArr];
            }
            
            [strongSelf.yuezhanMutableArr addObject:@[@"金币场",@"金币场详情"]];
            [strongSelf.yuezhanTableView reloadData];
        }
    }];
}
                              
                            
#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.yuezhanMutableArr.count ; i++){
        NSArray *dataTempArr = [self.yuezhanMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isKindOfClass:[NSString class]]){
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.yuezhanMutableArr.count ; i++){
        NSArray *dataTempArr = [self.yuezhanMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
           id itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isKindOfClass:[NSString class]]){
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}

#pragma mark - 创建加入战斗的alert
-(void)showJoinGameAlert{
    __weak typeof(self)weakSelf = self;
    [[PDAlertView sharedAlertView] showBlackAlertWithAlertType:alertBackgroundTypeNormal title:@"加入战斗" content:@"输入房间密码，与好友切磋" isClose:NO house:nil couponModel:nil inputPlaceholader:@"请输入6位数密码" isGender:NO btnArr:@[@"确定"] countDown:NO countDownBlock:NULL caipan:NO btnCliock:^(NSInteger buttonIndex, NSString *inputInfo) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
            if (inputInfo.length){
                [strongSelf sendRequestToAdjustPwd:inputInfo];
            } else {
                [PDHUD showConnectionErr:@"请输入正确的密码"];
            }
        }];
    }];
}

-(void)choostRoleInfoWithPwd:(NSString *)pwd serverId:(NSString *)serverId{
    PDYuezhanRoleListViewController *roleListVC = [[PDYuezhanRoleListViewController alloc]init];
    roleListVC.transferChooseServerId = serverId;
    roleListVC.transferPageType = yuezhanRoleListViewControllerPageTypeChoose;
    __weak typeof(self)weakSelf = self;
    [roleListVC actionClickWithChooseItemBlock:^(PDYuezhanRoleGameUserSingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf joinYuezhanControllerWithPwd:pwd roleId:singleModel.ID];
    }];
    [self.navigationController pushViewController:roleListVC animated:YES];
}


#pragma mark - 判断密码
-(void)sendRequestToAdjustPwd:(NSString *)pwd{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"password":pwd};
    [[NetworkAdapter sharedAdapter] fetchWithPath:judgePassword requestParams:params responseObjectClass:[PDYuezhanGameServerIdModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){               // 选择召唤师
            PDYuezhanGameServerIdModel *serverId = (PDYuezhanGameServerIdModel *)responseObject;
            [strongSelf choostRoleInfoWithPwd:pwd serverId:serverId.gameServerId];
        } else {
        
        }
    }];
}


-(void)joinYuezhanControllerWithPwd:(NSString *)pwd roleId:(NSString *)roldId{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"password":pwd,@"gameUserId":roldId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:joinencryptcombat requestParams:params responseObjectClass:[PDYuezhanCombatRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDYuezhanCombatRootModel *singleModel = (PDYuezhanCombatRootModel *)responseObject;
            PDYuezhanDetailRootViewController *detailViewController = [[PDYuezhanDetailRootViewController alloc]init];
            detailViewController.detailSingleModel = singleModel;
            [strongSelf.navigationController pushViewController:detailViewController animated:YES];
        }
    }];
}
@end
