//
//  PDYuezhanRoleListViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDYuezhanRoleGameUserSingleModel.h"

typedef NS_ENUM(NSInteger, yuezhanRoleListViewControllerPageType) {
    yuezhanRoleListViewControllerPageTypeNormal,
    yuezhanRoleListViewControllerPageTypeChoose,
    yuezhanRoleListViewControllerPageTypeOnlyReleaseChoose,
};

@interface PDYuezhanRoleListViewController : AbstractViewController
@property (nonatomic,assign)yuezhanRoleListViewControllerPageType transferPageType;
@property (nonatomic,copy)NSString *transferChooseServerId;


-(void)actionClickWithChooseItemBlock:(void(^)(PDYuezhanRoleGameUserSingleModel *singleModel))block;

@end
