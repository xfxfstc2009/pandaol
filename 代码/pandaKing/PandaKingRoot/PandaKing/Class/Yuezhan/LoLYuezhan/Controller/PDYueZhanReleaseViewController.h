//
//  PDYueZhanReleaseViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

// 【创建约战】
#import "AbstractViewController.h"

@interface PDYueZhanReleaseViewController : AbstractViewController

-(void)createSuccessManager:(void(^)())block;

@end
