//
//  PDYuezhanRoleListViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanRoleListViewController.h"
#import "PDYuezhanUserRoleTableViewCell.h"
#import "PDYuehanRoleListModel.h"
#import "PDYuezhanAddRoleTableViewCell.h"
#import "PDBindingInfoViewController.h"

static char actionClickWithChooseItemBlockKey;
@interface PDYuezhanRoleListViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *roleListTableView;
@property (nonatomic,strong)NSMutableArray *roleListMutableArr;
@property (nonatomic,strong)UIButton *rightButton;
@end

@implementation PDYuezhanRoleListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    __weak typeof(self)weakSelf = self;
    if (self.transferPageType == yuezhanRoleListViewControllerPageTypeNormal){
        [weakSelf sendReqeustToGetAllRoleList];
    } else if (self.transferPageType == yuezhanRoleListViewControllerPageTypeOnlyReleaseChoose){
        [weakSelf sendReqeustToGetAllRoleList];
    } else {
        [weakSelf sendRequestToGetChooseRole];
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"召唤师列表";
    __weak typeof(self)weakSelf = self;
    self.rightButton = [self rightBarButtonWithTitle:@"管理" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.roleListTableView.editing ){             // 编辑模式
            [strongSelf.roleListTableView setEditing:NO animated:YES];
            [strongSelf.rightButton setTitle:@"管理" forState:UIControlStateNormal];
        } else {                                                // 取消编辑模式
            [strongSelf.roleListTableView setEditing:YES animated:YES];
            [strongSelf.rightButton setTitle:@"完成" forState:UIControlStateNormal];
        }
        [strongSelf deleteAddButton];
        [strongSelf.roleListTableView reloadData];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.roleListMutableArr = [NSMutableArray array];
    [self.roleListMutableArr addObject:@"添加lol召唤师"];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.roleListTableView){
        self.roleListTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.roleListTableView.dataSource = self;
        self.roleListTableView.delegate = self;
        [self.view addSubview:self.roleListTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.roleListMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    NSInteger index = [self.roleListMutableArr indexOfObject:@"添加lol召唤师"];
    
    if (indexPath.row == index && [self.roleListMutableArr containsObject:@"添加lol召唤师"]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        PDYuezhanAddRoleTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[PDYuezhanAddRoleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
            cellWithRowZero.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowZero.transferCellHeight = cellHeight;
        
        return cellWithRowZero;
        
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDYuezhanUserRoleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDYuezhanUserRoleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cellWithRowOne.transferIsEdit = self.roleListTableView.editing;
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferGameUserSingleModel = [self.roleListMutableArr objectAtIndex:indexPath.row];
        
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne deleteManagerWithBlock:^(PDYuezhanRoleGameUserSingleModel *transferGameUserSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToDeleteWithMember:transferGameUserSingleModel];
        }];
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSInteger index = [self.roleListMutableArr indexOfObject:@"添加lol召唤师"];
    
    if (indexPath.row == index && [self.roleListMutableArr containsObject:@"添加lol召唤师"]){
        [self addRoleManager];
    } else {
        if ((self.transferPageType == yuezhanRoleListViewControllerPageTypeChoose) || (self.transferPageType == yuezhanRoleListViewControllerPageTypeOnlyReleaseChoose)){
            PDYuezhanRoleGameUserSingleModel *singleModel = [self.roleListMutableArr objectAtIndex:indexPath.row];
            void(^block)(PDYuezhanRoleGameUserSingleModel *singleModel) = objc_getAssociatedObject(self, &actionClickWithChooseItemBlockKey);
            if (block){
                block(singleModel);
            }
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSInteger index = [self.roleListMutableArr indexOfObject:@"添加lol召唤师"];
    
    if (indexPath.row == index && [self.roleListMutableArr containsObject:@"添加lol召唤师"]){
        return LCFloat(50);
    } else {
        return [PDYuezhanUserRoleTableViewCell calculationCellheight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(11);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

// 编辑模式
// 判断是否可以编辑
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == self.roleListMutableArr.count - 1 && [[self.roleListMutableArr objectAtIndex:indexPath.row]  isKindOfClass:[NSString class]]){
        return NO;
    } else {
        return YES;
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}


// 判断是否可以移动
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath{
    return NO;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.roleListTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [self.roleListMutableArr count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([self.roleListMutableArr count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}


#pragma mark - 获取所有召唤师
-(void)sendReqeustToGetAllRoleList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:allgameuser requestParams:nil responseObjectClass:[PDYuehanRoleListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            if (strongSelf.roleListMutableArr.count){
                [strongSelf.roleListMutableArr removeAllObjects];
            }
            
            PDYuehanRoleListModel *singleModel = (PDYuehanRoleListModel *)responseObject;
            [strongSelf.roleListMutableArr addObjectsFromArray:singleModel.gameUsers];
            
            [strongSelf.roleListMutableArr addObject:@"添加lol召唤师"];
            [strongSelf.roleListTableView reloadData];
        }
    }];
}

#pragma mark - 添加召唤师
-(void)addRoleManager{
    PDBindingInfoViewController *bindingViewController = [[PDBindingInfoViewController alloc]init];
    __weak typeof(self)weakSelf = self;
    [bindingViewController bindingDidEndWithYuezhanBlock:^(PDBindingAreaListSingleAreaListModel *serverModel, NSString *roleName) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToAddRoleInfoWithServerModel:serverModel roleName:roleName];
    }];
    
    bindingViewController.transferBindingSceneType = bindingSceneTypeYuezhan;
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:bindingViewController];
    [self.navigationController presentViewController:nav animated:YES completion:NULL];
}

-(void)sendRequestToAddRoleInfoWithServerModel:(PDBindingAreaListSingleAreaListModel *)serverModel roleName:(NSString *)roleName{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"gameServerId":serverModel.server_id,@"name":roleName};
    [[NetworkAdapter sharedAdapter] fetchWithPath:addgameuser requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf sendReqeustToGetAllRoleList];
            [PDHUD showHUDSuccess:@"绑定成功"];
        }
    }];
}

#pragma mark - 删除
-(void)sendRequestToDeleteWithMember:(PDYuezhanRoleGameUserSingleModel *)transferGameUserSingleModel{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"gameUserId":transferGameUserSingleModel.ID};
    [PDHUD showHUDProgress:@"正在删除中" diary:0];
    [[NetworkAdapter sharedAdapter] fetchWithPath:deletegameuser requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            NSInteger indexRow = [strongSelf.roleListMutableArr indexOfObject:transferGameUserSingleModel];
            [strongSelf.roleListMutableArr removeObject:transferGameUserSingleModel];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:indexRow inSection:0];
            [strongSelf.roleListTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            [PDHUD showHUDSuccess:@"删除成功"];
            if (strongSelf.roleListTableView.editing && strongSelf.roleListMutableArr.count <= 1){
                strongSelf.roleListTableView.editing = NO;
            }
        }
    }];
}

-(void)actionClickWithChooseItemBlock:(void(^)(PDYuezhanRoleGameUserSingleModel *singleModel))block{
    objc_setAssociatedObject(self, &actionClickWithChooseItemBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)deleteAddButton{
    if ([self.roleListMutableArr containsObject:@"添加lol召唤师"] && self.roleListTableView.editing){
        NSInteger index = [self.roleListMutableArr indexOfObject:@"添加lol召唤师"];
        [self.roleListMutableArr removeObject:@"添加lol召唤师"];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
        [self.roleListTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else {
        if (![self.roleListMutableArr containsObject:@"添加lol召唤师"]){
            [self.roleListMutableArr addObject:@"添加lol召唤师"];
            NSInteger index = [self.roleListMutableArr indexOfObject:@"添加lol召唤师"];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [self.roleListTableView insertRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
        }
    }
}

#pragma mark - 获取选择的召唤师
-(void)sendRequestToGetChooseRole{
    __weak typeof(self)weakSelf = self;
    if (!self.transferChooseServerId.length){
        [PDHUD showConnectionErr:@"找不到服务器ID"];
    }
    NSDictionary *params = @{@"gameServerId":self.transferChooseServerId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:gameuserlist requestParams:params responseObjectClass:[PDYuehanRoleListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.roleListMutableArr.count){
            [strongSelf.roleListMutableArr removeAllObjects];
        }
        
        PDYuehanRoleListModel *singleModel = (PDYuehanRoleListModel *)responseObject;
        [strongSelf.roleListMutableArr addObjectsFromArray:singleModel.gameUsers];
        
        
        if (strongSelf.roleListMutableArr.count){
            [strongSelf.roleListTableView dismissPrompt];
        } else {
            [strongSelf.roleListTableView showPrompt:@"当前区没有召唤师，请确认" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        }
        
        [strongSelf.roleListTableView reloadData];
    }];
}

@end
