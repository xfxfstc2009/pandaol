//
//  PDYuezhanDetailRootViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanDetailRootViewController.h"
#import "PDYuezhanDetailHeaderViewController.h"
#import "PDYuezhanDetailBottomViewController.h"
#import "PDYuezhanCombatRootModel.h"
#import "PDLotteryHeroChatRoomViewController.h"

@interface PDYuezhanDetailRootViewController ()<PDYuezhanDetailHeaderViewControllerDelegate,PDNetworkAdapterDelegate>
@property (nonatomic,strong)PDEMMessageViewController *chatRoomViewController;
@property (nonatomic,strong)PDYuezhanDetailHeaderViewController *headerViewController;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)NSTimer *timer;
@property (nonatomic,assign)NSInteger timerIndex;

@end

@implementation PDYuezhanDetailRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createMainScrollView];
    [self createHeaderViewController];
    
    __weak typeof(self)weakSelf = self;
    if (!self.detailSingleModel){
        [weakSelf sendRequestToGetInfo];
    } else {
        // 1. 视图更新
        [self infoManagerWithModel:self.detailSingleModel];
        
        // 2. 创建聊天室
        [self createIMControllerWithIMId:self.detailSingleModel.combat.groupId];
    }
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"标题";
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([NetworkAdapter sharedAdapter]){
        [NetworkAdapter sharedAdapter].delegate = self;
    }
}

#pragma mark - createMainScrollView
-(void)createMainScrollView{
    
}

#pragma mark - 1.创建头部信息
-(void)createHeaderViewController{
    self.headerViewController = [[PDYuezhanDetailHeaderViewController alloc]init];
    self.headerViewController.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, [PDYuezhanDetailHeaderViewController calculationControllerHeight]);
    self.headerViewController.view.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.headerViewController.view];
    [self addChildViewController:self.headerViewController];
    self.headerViewController.hasCancelSocket = YES;
    self.headerViewController.delegate = self;
}

#pragma mark - 2. 创建底部信息
-(void)createBottomViewController{
//    self.bottomViewController = [[PDLotteryHeroChatRoomViewController alloc]init];
//    self.bottomViewController.view.frame = CGRectMake(0, self.headerViewController.view.size_height, kScreenBounds.size.width, kScreenBounds.size.height - self.headerViewController.view.size_height);
//    self.bottomViewController.view.backgroundColor = [UIColor yellowColor];
//    [self.view addSubview:self.bottomViewController.view];
//    [self addChildViewController:self.bottomViewController];
}

-(void)createIMControllerWithIMId:(NSString *)imId{
    if(!self.chatRoomViewController){
        if (![[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            return;
        }
        self.chatRoomViewController = [[PDEMManager sharedInstance] conversationWithChatroom:imId];
        if(self.chatRoomViewController == nil){
            return;
        }
        
        self.chatRoomViewController.view.backgroundColor = [UIColor clearColor];
        self.chatRoomViewController.view.frame = CGRectMake(0, self.headerViewController.view.size_height, kScreenBounds.size.width, kScreenBounds.size.height - self.headerViewController.view.size_height);
        [self addChildViewController:self.chatRoomViewController];
        [self.view addSubview:self.chatRoomViewController.view];
    }
}

#pragma mark - dismiss
-(void)dismissManager{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)cancelChallenge{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToCancelYuezhan];
}

#pragma mark - 请求接口获取信息
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    NSString *combatId = @"";
    if (self.transferCombatId.length){
        combatId = self.transferCombatId;
    } else {
        combatId = self.detailSingleModel.combat.ID;
    }
    NSDictionary *params = @{@"combatId":combatId};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:combatdetail requestParams:params responseObjectClass:[PDYuezhanCombatRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf.detailSingleModel = (PDYuezhanCombatRootModel *)responseObject;
            // 1. 视图更新
            [strongSelf infoManagerWithModel:strongSelf.detailSingleModel];
            
            // 2. 创建聊天室
            [strongSelf createIMControllerWithIMId:strongSelf.detailSingleModel.combat.groupId];
        }
    }];
}

#pragma 取消约战
-(void)sendRequestToCancelYuezhan{
    __weak typeof(self)weakSelf = self;
    NSString *combatId = @"";
    if (self.transferCombatId.length){
        combatId = self.transferCombatId;
    } else {
        combatId = self.detailSingleModel.combat.ID;
    }
    NSDictionary *params = @{@"combatId":combatId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:cancelcombat requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSel = weakSelf;
        if (isSucceeded){
            [strongSel alertDismiss];
        }
    }];
}

-(void)alertDismiss{
    __weak typeof(self)weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"解散成功" conten:@"该场对战已经解散成功" isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }];
    }];
}

#pragma mark - 请求结算
-(void)sendRequestToJiesuan{
    __weak typeof(self)weakSelf = self;
    NSString *combatId = @"";
    if (self.transferCombatId.length){
        combatId = self.transferCombatId;
    } else {
        combatId = self.detailSingleModel.combat.ID;
    }
    NSDictionary *params = @{@"combatId":combatId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:requestsettle requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            // 停止计时器
            if (strongSelf.timer){
                [strongSelf.timer invalidate];
                strongSelf.timer = nil;
            }
            
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"请求结算成功" conten:@"客服人员会马上进行结算" isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                [strongSelf.headerViewController.readyButton setTitle:@"结算中" forState:UIControlStateNormal];
                strongSelf.headerViewController.readyButton.userInteractionEnabled = NO;
                [strongSelf.headerViewController housePwdHidenManager];
                [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
            }];
        }
    }];
}

#pragma mark - 准备约战
-(void)sendRequestToChallenge{
    NSString *combatId = @"";
    if (self.transferCombatId.length){
        combatId = self.transferCombatId;
    } else {
        combatId = self.detailSingleModel.combat.ID;
    }
    
    NSDictionary *params = @{@"combatId":combatId};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:readycombat requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            //
            [strongSelf adjustWithRoleStatusReady];
            
            [PDHUD showHUDSuccess:@"已准备"];
        }
    }];
}

#pragma mark - Socket 加入房间
-(void)socketDidBackData:(id)responseObject{
    __weak typeof(self)weakSelf = self;
    if ([responseObject isKindOfClass:[NSDictionary class]]){
        NSDictionary *dic = (NSDictionary *)responseObject;
        PDBaseSocketRootModel *baseSocket = [[PDBaseSocketRootModel alloc] initWithJSONDict:[dic objectForKey:@"data"]];
        if (baseSocket.type == socketType1100 || (baseSocket.type == socketType1101) || (baseSocket.type == socketType1102) || (baseSocket.type == socketType1103) || (baseSocket.type == socketType1104) || (baseSocket.type == socketType1105)){
            weakSelf.detailSingleModel.combat = baseSocket.combat;

            // 拿到数据后更新
            [weakSelf infoManagerWithModel:weakSelf.detailSingleModel];
        }
    }
}

-(void)infoManagerWithModel:(PDYuezhanCombatRootModel *)detailSingleModel{
    // 1.视图
    [self.headerViewController viewsLoadWithModel:detailSingleModel.combat];
    
    // 判断时间
    if([detailSingleModel.combat.state isEqualToString:@"matching"]){            // 匹配中
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    } else if ([detailSingleModel.combat.state isEqualToString:@"readying"]){    // 准备中
        if (detailSingleModel.combat.readyTimeoutTime != 0){
            [self startTime];
        }
    } else if ([detailSingleModel.combat.state isEqualToString:@"waitRoom"]){    // 等待开房间
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    } else if ([detailSingleModel.combat.state isEqualToString:@"battle"]){        // 对战中
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    } else if ([detailSingleModel.combat.state isEqualToString:@"settling"]){        // 结算中
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    } else if ([detailSingleModel.combat.state isEqualToString:@"settled"]){         // 已结算
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    } else if ([detailSingleModel.combat.state isEqualToString:@"cancel"]){          // 解散状态
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    }
    
    
}

#pragma mark - 比赛开始
#pragma mark - startTime
-(void)startTime{
    if (!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
        NSRunLoop *currentRunLoop = [NSRunLoop currentRunLoop];
        [currentRunLoop addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    NSInteger time = (self.detailSingleModel.combat.readyTimeoutTime - self.detailSingleModel.combat.currentTime) / 1000.;
    self.timerIndex = time;
}

-(void)timeChange{
    self.timerIndex--;
    if (self.timerIndex <= 0){              // 解散
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
        __weak typeof(self)weakSelf = self;
        [[PDAlertView sharedAlertView] showAlertWithTitle:@"时间到了解散" conten:@"由于该场比赛双方长时间未准备，比赛已作失效处理。" isClose:YES btnArr:@[@"确认"] buttonClick:^(NSInteger buttonIndex) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
                [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
            }];
        }];
        return;
    }
    [self.headerViewController timerChange:self.timerIndex];
}

#pragma mark - 请求结算
-(void)jiesuanButtonClick{
    [self sendRequestToJiesuan];
}

#pragma mark - 准备约战
-(void)readyButtonClick{
    [self sendRequestToChallenge];
}

-(void)tapManager{
    [self.chatRoomViewController releaseKeyBoard];
}

#pragma makr - 判断对方是否准备状态
-(void)adjustWithRoleStatusReady{
    if ([self.detailSingleModel.combat.creator.memberId isEqualToString:[AccountModel sharedAccountModel].memberInfo.ID]){          // 自己是创建者
        if (self.detailSingleModel.combat.joiner.ready){            // 对方准备了
            [self.headerViewController.readyButton setTitle:@"等待客服人员开房" forState:UIControlStateNormal];
            self.headerViewController.readyButton.userInteractionEnabled = NO;
        } else {            // 对方没准备
            [self.headerViewController.readyButton setTitle:@"等待对方准备" forState:UIControlStateNormal];
            self.headerViewController.readyButton.userInteractionEnabled = NO;
        }
    } else {                    // 对方是创建者
        if (self.detailSingleModel.combat.creator.ready){            // 对方准备了
            [self.headerViewController.readyButton setTitle:@"等待客服人员开房" forState:UIControlStateNormal];
            self.headerViewController.readyButton.userInteractionEnabled = NO;
        } else {            // 对方没准备
            [self.headerViewController.readyButton setTitle:@"等待对方准备" forState:UIControlStateNormal];
            self.headerViewController.readyButton.userInteractionEnabled = NO;
        }
    }
}
@end
