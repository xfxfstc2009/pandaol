//
//  PDYuezhanDetailHeaderViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDYuezhanCombatRootModel.h"

@class PDYuezhanDetailHeaderViewController;
@protocol PDYuezhanDetailHeaderViewControllerDelegate <NSObject>

-(void)dismissManager;
-(void)cancelChallenge;
-(void)readyButtonClick;
-(void)jiesuanButtonClick;
-(void)tapManager;
@end

@interface PDYuezhanDetailHeaderViewController : AbstractViewController
@property (nonatomic,weak)id<PDYuezhanDetailHeaderViewControllerDelegate> delegate;
@property (nonatomic,strong)UIButton *readyButton;

-(void)viewsLoadWithModel:(PDYuezhanCombatSingleModel *)singleModel;
-(void)timerChange:(NSInteger)time;
-(void)housePwdHidenManager;


+(CGFloat)calculationControllerHeight;
@end
