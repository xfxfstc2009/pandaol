//
//  PDYuezhanSingleSceneViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanSingleSceneViewController.h"
#import "PDYueZhanSingleSceneTableViewCell.h"

@interface PDYuezhanSingleSceneViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *singleTableView;
@property (nonatomic,strong)NSMutableArray *dataSourceArr;
@end

@implementation PDYuezhanSingleSceneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void)pageSetting{
    self.barMainTitle = @"1213";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@""] barHltImage:[UIImage imageNamed:@""] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
    }];
}

-(void)arrayWithInit{
    self.dataSourceArr = [NSMutableArray array];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.dataSourceArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    PDYueZhanSingleSceneTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDYueZhanSingleSceneTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(50);
}




@end
