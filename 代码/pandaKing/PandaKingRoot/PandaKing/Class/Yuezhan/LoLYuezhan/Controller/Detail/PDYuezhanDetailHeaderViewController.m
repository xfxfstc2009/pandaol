//
//  PDYuezhanDetailHeaderViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanDetailHeaderViewController.h"
#import "PDYuezhanDetailRoleIconView.h"

@interface PDYuezhanDetailHeaderViewController ()
@property (nonatomic,strong)PDImageView *headerBgImgView;               /**< 背景图片*/
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)UILabel *navTitleLabel;                     /**< 游戏标题*/
@property (nonatomic,strong)UILabel *gameInfoLabel;                     /**< 游戏介绍*/
@property (nonatomic,strong)PDImageView *gamePwdIcon;                   /**< 游戏密码锁*/
@property (nonatomic,strong)UILabel *gamePwdLabel;                      /**< 游戏密码*/
@property (nonatomic,strong)PDYuezhanDetailRoleIconView *leftView;      /**< 左侧的用户*/
@property (nonatomic,strong)PDYuezhanDetailRoleIconView *rightView;     /**< 右侧的用户*/
@property (nonatomic,strong)UILabel *numberLabel;                       /**< 时间*/
@property (nonatomic,strong)UILabel *houseLabel;                        /**< 房间名字*/
@property (nonatomic,strong)UILabel *housePwdLabel;                     /**< 密码名字*/
@property (nonatomic,strong)PDImageView *leftWinImgView;                /**< 左边胜利*/
@property (nonatomic,strong)PDImageView *rightWinImgView;                /**< 左边胜利*/
@property (nonatomic,strong)UITapGestureRecognizer *tapGuest;

@end

@implementation PDYuezhanDetailHeaderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createBgView];
    [self createLeftPopButton];
    [self createRightButton];
    [self createNavTitle];
    [self createGameInfo];
    [self createGamePwdLabel];
    [self createRoleInfo];
    [self createReadyButton];
    [self createHouseInfo];
    [self createWinImgInfo];
    self.tapGuest = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self.view addGestureRecognizer:self.tapGuest];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"头部";
}

-(void)createBgView{
    self.headerBgImgView = [[PDImageView alloc]init];
    self.headerBgImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [PDYuezhanDetailHeaderViewController calculationControllerHeight]);
    self.headerBgImgView.image = [UIImage imageNamed:@"bg_yuezhan_detail_header"];
    [self.view addSubview:self.headerBgImgView];
}

-(void)createLeftPopButton{
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.backgroundColor = [UIColor clearColor];
    self.leftButton.frame = CGRectMake(LCFloat(11), 20, 44, 44);
    __weak typeof(self)weakSelf = self;
    [self.leftButton setImage:[UIImage imageNamed:@"icon_yuezhan_detail_dismiss"] forState:UIControlStateNormal];
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(dismissManager)]){
            [strongSelf.delegate dismissManager];
        }
    }];
    [self.view addSubview:self.leftButton];
}

#pragma mark - 创建右侧按钮
-(void)createRightButton{
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.backgroundColor = [UIColor clearColor];
    [self.rightButton setTitleColor:c26 forState:UIControlStateNormal];
    self.rightButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - 44, 20, 44, 44);
    __weak typeof(self)weakSelf = self;
    [self.rightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(cancelChallenge)]){
            [strongSelf.delegate cancelChallenge];
        }
    }];
    [self.rightButton setTitle:@"解散" forState:UIControlStateNormal];
    self.rightButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self.view addSubview:self.rightButton];
}

#pragma mark - 创建标题
-(void)createNavTitle{
    self.navTitleLabel = [GWViewTool createLabelFont:@"标题" textColor:@"黑"];
    self.navTitleLabel.backgroundColor = [UIColor clearColor];
    self.navTitleLabel.frame = CGRectMake(CGRectGetMaxX(self.leftButton.frame) + LCFloat(11), 20, self.rightButton.orgin_x - LCFloat(11) - CGRectGetMaxX(self.leftButton.frame), 44);
    self.navTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.navTitleLabel.text = @"约战详情";
    self.navTitleLabel.textColor = c26;
    self.navTitleLabel.font = [self.navTitleLabel.font boldFont];
    self.navTitleLabel.center_y = self.leftButton.center_y;
    [self.view addSubview:self.navTitleLabel];
}

#pragma mark - 创建比赛信息
-(void)createGameInfo{
    self.gameInfoLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.gameInfoLabel.backgroundColor = [UIColor clearColor];
    self.gameInfoLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.navTitleLabel.frame) + LCFloat(20), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.gameInfoLabel.font]);
    self.gameInfoLabel.text = @"SOLO赛 - 艾欧尼亚 - 5万场";
    self.gameInfoLabel.textAlignment = NSTextAlignmentCenter;
    self.gameInfoLabel.textColor = c26;
    [self.view addSubview:self.gameInfoLabel];
}

-(void)createGamePwdLabel{
    // 11 12
    self.gamePwdIcon = [[PDImageView alloc]init];
    self.gamePwdIcon.image = [UIImage imageNamed:@"icon_yuezhan_detail_pwd"];
    [self.view addSubview:self.gamePwdIcon];
    
    // 2. label
    self.gamePwdLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.gamePwdLabel.text = @"未设置密码";
    self.gamePwdLabel.textColor = c26;
    CGSize gameSize = [self.gamePwdLabel.text sizeWithCalcFont:self.gamePwdLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gamePwdLabel.font])];
    [self.view addSubview:self.gamePwdLabel];
    
    // 3. 重置frame
    CGFloat margin_x = (kScreenBounds.size.width - gameSize.width - LCFloat(11) - LCFloat(11)) / 2.;
    self.gamePwdIcon.frame = CGRectMake(margin_x, CGRectGetMaxY(self.gameInfoLabel.frame) + LCFloat(15), LCFloat(11), LCFloat(12));
    
    self.gamePwdLabel.frame = CGRectMake(CGRectGetMaxX(self.gamePwdIcon.frame) + LCFloat(11), 0, gameSize.width, [NSString contentofHeightWithFont:self.gamePwdLabel.font]);
    self.gamePwdLabel.center_y = self.gamePwdIcon.center_y;
}

#pragma mark - 创建角色信息
-(void)createRoleInfo{
    CGFloat single_width = (kScreenBounds.size.width - 2 * (LCFloat(11) * 3) - 100) / 2.;
    self.leftView = [[PDYuezhanDetailRoleIconView alloc]initWithFrame:CGRectMake(LCFloat(11), CGRectGetMaxY(self.gamePwdLabel.frame) + LCFloat(5), single_width, [PDYuezhanDetailRoleIconView calculationCellHeight])];
    [self.view addSubview:self.leftView];
    
    self.rightView = [[PDYuezhanDetailRoleIconView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - LCFloat(11) - single_width, self.leftView.orgin_y, self.leftView.size_width, self.leftView.size_height)];
    [self.view addSubview:self.rightView];
    
    // 时间
    self.numberLabel = [GWViewTool createLabelFont:@"标题" textColor:@"金"];
    self.numberLabel.font = [[UIFont systemFontOfCustomeSize:30.] boldFont];
    self.numberLabel.frame = CGRectMake(0, 0, LCFloat(50), LCFloat(50));
    self.numberLabel.text = @"60";
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.numberLabel];
    self.numberLabel.center_x = kScreenBounds.size.width / 2.;
    self.numberLabel.center_y = self.leftView.center_y;
}

#pragma mark - 创建名字
-(void)createHouseInfo{
    self.houseLabel = [GWViewTool createLabelFont:@"提示" textColor:@"金"];
    self.houseLabel.textColor = c26;
    self.houseLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.houseLabel];
    
    // 创建房子密码
    self.housePwdLabel = [GWViewTool createLabelFont:@"提示" textColor:@"金"];
    self.housePwdLabel.textAlignment = NSTextAlignmentCenter;
    self.housePwdLabel.textColor = c26;
    [self.view addSubview:self.housePwdLabel];
}

#pragma mark - 创建按钮
-(void)createReadyButton{
    self.readyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.readyButton.backgroundColor = [UIColor clearColor];
    self.readyButton.frame = CGRectMake(LCFloat(80), CGRectGetMaxY(self.leftView.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(80), 50);
    [self.view addSubview:self.readyButton];
    __weak typeof(self)weakSelf = self;
    [self.readyButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(readyButtonClick)]){
            [strongSelf.delegate readyButtonClick];
        }
    }];
    [self.readyButton setTitle:@"准备" forState:UIControlStateNormal];
    [self.readyButton setBackgroundImage:[UIImage imageNamed:@"icon_yuezhan_ready"] forState:UIControlStateNormal];
    [self.readyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

#pragma mark - 创建胜利
-(void)createWinImgInfo{
    self.leftWinImgView = [[PDImageView alloc]init];
    self.leftWinImgView.backgroundColor = [UIColor clearColor];
    self.leftWinImgView.frame = CGRectMake(0, CGRectGetMaxY(self.leftView.frame), LCFloat(70), LCFloat(40));
    self.leftWinImgView.center_x = self.leftView.center_x;
    self.leftWinImgView.hidden = YES;
    [self.view addSubview:self.leftWinImgView];
    
    // right
    self.rightWinImgView = [[PDImageView alloc]init];
    self.rightWinImgView.backgroundColor = [UIColor clearColor];
    self.rightWinImgView.frame = CGRectMake(0, self.leftWinImgView.orgin_y, LCFloat(70), LCFloat(40));
    self.rightWinImgView.center_x = self.rightView.center_x;
    self.rightWinImgView.hidden = YES;
    [self.view addSubview:self.rightWinImgView];
}

-(void)viewsLoadWithModel:(PDYuezhanCombatSingleModel *)singleModel{
    // 1. 游戏类型
    self.gameInfoLabel.text = [NSString stringWithFormat:@"%@赛 - %@ - %@",singleModel.gameType,singleModel.gameServerName,[NSString stringWithFormat:@"%li万场",singleModel.gold / 10000]];
    
    // 2. 左侧的
    singleModel.creator.isFangzhu = YES;
    self.leftView.transferSingleModel = singleModel.creator;
    
    // 3. 右侧的
    singleModel.joiner.isFangzhu = NO;
    self.rightView.transferSingleModel = singleModel.joiner;
    
    // 4. 密码
    if (singleModel.encrypt){
        self.gamePwdLabel.text = singleModel.password;
    } else {
        self.gamePwdLabel.text = @"未设置密码";
    }
    
    
    // 1. 获取游戏类型
    if([singleModel.state isEqualToString:@"matching"]){            // 匹配中
        [self.readyButton setTitle:@"等待玩家进入" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = NO;
        self.numberLabel.hidden = YES;
        self.housePwdLabel.hidden = YES;
        self.houseLabel.hidden = YES;
        self.leftWinImgView.hidden = YES;
        self.rightWinImgView.hidden = YES;
    } else if ([singleModel.state isEqualToString:@"readying"]){    // 准备中
        [self.readyButton setTitle:@"准备" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = YES;
        self.numberLabel.hidden = NO;
        self.housePwdLabel.hidden = YES;
        self.houseLabel.hidden = YES;
        self.leftWinImgView.hidden = YES;
        self.rightWinImgView.hidden = YES;
    } else if ([singleModel.state isEqualToString:@"waitRoom"]){    // 等待中
        [self.readyButton setTitle:@"等待客服人员开房" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = NO;
        self.numberLabel.hidden = YES;
        self.housePwdLabel.hidden = YES;
        self.houseLabel.hidden = YES;
        self.leftWinImgView.hidden = YES;
        self.rightWinImgView.hidden = YES;
    } else if ([singleModel.state isEqualToString:@"battle"]){        // 对战中
        [self.readyButton setTitle:@"请求结算" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = YES;
        self.numberLabel.hidden = YES;
        self.housePwdLabel.hidden = NO;
        self.houseLabel.hidden = NO;
        
        // 更新位置
        [self housePwdManagerWithModel:singleModel];
        
        __weak typeof(self)weakSelf = self;
        [self.readyButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 请求结算按钮
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(jiesuanButtonClick)]){
                [strongSelf.delegate jiesuanButtonClick];
            }
        }];
        
        self.leftWinImgView.hidden = YES;
        self.rightWinImgView.hidden = YES;
    } else if ([singleModel.state isEqualToString:@"settling"]){        // 结算中
        [self.readyButton setTitle:@"结算中" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = NO;
        self.numberLabel.hidden = YES;
        self.housePwdLabel.hidden = YES;
        self.houseLabel.hidden = YES;
        self.leftWinImgView.hidden = YES;
        self.rightWinImgView.hidden = YES;
    } else if ([singleModel.state isEqualToString:@"settled"]){         // 已结算
        [self.readyButton setTitle:@"已结算" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = NO;
        self.numberLabel.hidden = YES;
        self.housePwdLabel.hidden = YES;
        self.houseLabel.hidden = YES;
        self.leftWinImgView.hidden = NO;
        self.rightWinImgView.hidden = NO;
        
        if (singleModel.creatorWin){
            self.leftWinImgView.image = [UIImage imageNamed:@"icon_yuezhan_detail_win"];
            self.rightWinImgView.image = [UIImage imageNamed:@"icon_yuezhan_detail_fail"];
        } else {
            self.leftWinImgView.image = [UIImage imageNamed:@"icon_yuezhan_detail_fail"];
            self.rightWinImgView.image = [UIImage imageNamed:@"icon_yuezhan_detail_win"];
        }
        self.readyButton.orgin_y = CGRectGetMaxY(self.leftView.frame) + LCFloat(11);
        
    } else if ([singleModel.state isEqualToString:@"cancel"]){          // 解散状态
        [self.readyButton setTitle:@"比赛已失效" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = NO;
        self.numberLabel.hidden = YES;
        self.housePwdLabel.hidden = YES;
        self.houseLabel.hidden = YES;
        self.leftWinImgView.hidden = NO;
        self.rightWinImgView.hidden = NO;
    }
    
    
    // 隐藏右侧按钮
    if ([singleModel.creator.memberId isEqualToString:[AccountModel sharedAccountModel].memberInfo.ID]){
        self.rightButton.hidden = NO;
    } else {
        self.rightButton.hidden = YES;
    }
}

-(void)timerChange:(NSInteger)time{
    self.numberLabel.text = [NSString stringWithFormat:@"%li",time];
}

-(void)housePwdManagerWithModel:(PDYuezhanCombatSingleModel *)singleModel{
    self.houseLabel.text = [NSString stringWithFormat:@"房间名:%@",singleModel.room.name];
    self.housePwdLabel.text = [NSString stringWithFormat:@"房间密码:%@",singleModel.room.password];
    self.houseLabel.adjustsFontSizeToFitWidth = YES;
    self.housePwdLabel.adjustsFontSizeToFitWidth = YES;
    self.houseLabel.hidden = NO;
    self.housePwdLabel.hidden = NO;
    
    CGFloat width = self.rightView.orgin_x - CGRectGetMaxX(self.leftView.frame) - 2 * LCFloat(11);
    self.houseLabel.frame = CGRectMake(CGRectGetMaxX(self.leftView.frame) + LCFloat(11), CGRectGetMaxY(self.leftView.frame) + LCFloat(11),width , [NSString contentofHeightWithFont:self.houseLabel.font]);
    
    self.housePwdLabel.frame = CGRectMake(self.houseLabel.orgin_x, CGRectGetMaxY(self.houseLabel.frame) + LCFloat(11), width, [NSString contentofHeightWithFont:self.housePwdLabel.font]);
    
    self.houseLabel.orgin_y = self.leftView.center_y - [NSString contentofHeightWithFont:self.houseLabel.font] - LCFloat(2);
    self.housePwdLabel.orgin_y = self.leftView.center_y + LCFloat(2) + [NSString contentofHeightWithFont:self.housePwdLabel.font];
//    self.readyButton.orgin_y = CGRectGetMaxY(self.leftView.frame) + LCFloat(11);
}

-(void)housePwdHidenManager{
    self.houseLabel.hidden = YES;
    self.housePwdLabel.hidden = YES;
    [UIView animateWithDuration:.5f animations:^{
        self.readyButton.orgin_y = CGRectGetMaxY(self.leftView.frame) + LCFloat(11);
    } completion:^(BOOL finished) {
        self.readyButton.orgin_y = CGRectGetMaxY(self.leftView.frame) + LCFloat(11);
    }];
}

-(void)tapManager{
    if (self.delegate && [self.delegate respondsToSelector:@selector(tapManager)]){
        [self.delegate tapManager];
    }
}

+(CGFloat)calculationControllerHeight{
    CGFloat cellHeight = 0;
    cellHeight += 20;
    cellHeight += 44;
    cellHeight += LCFloat(20);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(15);
    cellHeight += LCFloat(12);
    cellHeight += LCFloat(5);
    cellHeight += [PDYuezhanDetailRoleIconView calculationCellHeight];
    cellHeight += LCFloat(11);
    cellHeight += 50;
    cellHeight += LCFloat(11);
    return cellHeight;
}



@end
