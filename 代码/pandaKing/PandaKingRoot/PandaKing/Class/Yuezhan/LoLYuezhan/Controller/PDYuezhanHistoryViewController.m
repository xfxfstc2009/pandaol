//
//  PDYuezhanHistoryViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYuezhanHistoryViewController.h"
#import "PDYuezhanHistoryRootSingleModel.h"
#import "PDYuezhanRootMyYuezhanTableViewCell.h"
#import "PDYuezhanCombatSingleModel.h"
#import "PDYuezhanDetailRootViewController.h"

@interface PDYuezhanHistoryViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSInteger page;
}
@property (nonatomic,strong)UITableView *historyTableView;
@property (nonatomic,strong)NSMutableArray *historyMutableArr;

@end

@implementation PDYuezhanHistoryViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfoIsHornal:YES];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"约战历史";
}

-(void)arrayWithInit{
    self.historyMutableArr = [NSMutableArray array];
    page = 1;
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.historyTableView){
        self.historyTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.historyTableView.dataSource = self;
        self.historyTableView.delegate = self;
        [self.view addSubview:self.historyTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.historyTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoIsHornal:YES];
    }];
    
    [self.historyTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoIsHornal:NO];
    }];
    
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.historyMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
    PDYuezhanRootMyYuezhanTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
    if (!cellWithRowFiv){
        cellWithRowFiv = [[PDYuezhanRootMyYuezhanTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
        cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowFiv.transferCellHeight = cellHeight;
    cellWithRowFiv.transferSingleModel = [self.historyMutableArr  objectAtIndex:indexPath.row];
    return cellWithRowFiv;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PDYuezhanCombatSingleModel *singleModel = [self.historyMutableArr objectAtIndex:indexPath.row];
    PDYuezhanDetailRootViewController *yuezhanDetailViewController = [[PDYuezhanDetailRootViewController alloc]init];
    yuezhanDetailViewController.transferCombatId = singleModel.ID;
    [self.navigationController pushViewController:yuezhanDetailViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PDYuezhanRootMyYuezhanTableViewCell calculationCellHeight];
}

-(void)sendRequestToGetInfoIsHornal:(BOOL)hornal{
    __weak typeof(self)weakSelf = self;
    if (hornal){
        page = 1;
    }
    NSDictionary *params = @{@"pageNum":@(page)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:combatlist requestParams:params responseObjectClass:[PDYuezhanHistoryRootSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDYuezhanHistoryRootSingleModel *singleModel = (PDYuezhanHistoryRootSingleModel *)responseObject;
            if (hornal){
                [strongSelf.historyMutableArr removeAllObjects];
                [strongSelf.historyMutableArr addObjectsFromArray:singleModel.items];
            } else {
                [strongSelf.historyMutableArr addObjectsFromArray:singleModel.items];
            }
            strongSelf->page++;
            [strongSelf.historyTableView reloadData];
            if (strongSelf.historyMutableArr.count){
                [strongSelf.historyTableView dismissPrompt];
            } else {
                [strongSelf.historyTableView showPrompt:@"没有约战历史" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
            
            [strongSelf.historyTableView stopFinishScrollingRefresh];
            [strongSelf.historyTableView stopPullToRefresh];
        }
    }];
}

@end
