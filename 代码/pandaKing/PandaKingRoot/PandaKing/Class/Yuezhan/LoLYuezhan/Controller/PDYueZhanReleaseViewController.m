//
//  PDYueZhanReleaseViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDYueZhanReleaseViewController.h"
#import "PDYuezhanReleaseSingleModel.h"
#import "PDYuezhanTypeTableViewCell.h"
#import "PDYuezhanSliderTableViewCell.h"                    /**< slider 滑块*/
#import "PDYuezhanZhuyiShixiangTableViewCell.h"
#import "PDYuezhanChuzhengRoleTableViewCell.h"
#import "PDYuezhanRoleListViewController.h"
#import "PDYuezhanSliderTableViewCell.h"
#import "PDYuezhanReleaseWaitConfigModel.h"
#import "PDYuezhanReleaseWaitTimeSingleModel.h"


static char createSuccessManagerKey;
@interface PDYueZhanReleaseViewController ()<UITableViewDataSource,UITableViewDelegate,PDYuezhanRoleGameUserSingleModelDelegate>{
    PDYuezhanReleaseSingleModel *releaseSingleModel;
    
    PDYuezhanRoleGameUserSingleModel *userSelectedSingleModel;                          /**< 用户选中的Model*/
}
@property (nonatomic,strong)UITableView *yuezhanTableView;
@property (nonatomic,strong)NSArray *yuezhanArr;
@property (nonatomic,strong)UIPickerView *serverPickerView;
@property (nonatomic,strong)UIButton *bottomActionButton;
@property (nonatomic,strong)NSArray *gameTypeArr;                       /**< 游戏类型*/
@property (nonatomic,strong)NSArray *homePwdArr;                        /**< 房间密码*/
@property (nonatomic,strong)NSArray *zhuyiArr;                          /**< 注意事项*/
@property (nonatomic,strong)NSMutableArray *goldSceneMutableArr;        /**< 金币场*/
@property (nonatomic,strong)NSMutableArray *waitTimeMutableArr;         /**< 等待时间*/

@end

@implementation PDYueZhanReleaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithinit];
    [self createBttomButton];
    [self createTableView];
    
    // 获取等待时间
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetWaitTime];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"创建约战";
    __weak typeof(self)weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_page_cancle"] barHltImage:[UIImage imageNamed:@"icon_page_cancle"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithinit{
    self.yuezhanArr = @[@[@"游戏类型",@"房间密码",@"金额设置",@"等待时间限制",@"出征召唤师"],@[@"注意事项"]];
    
    // 出征区服
    userSelectedSingleModel = [[PDYuezhanRoleGameUserSingleModel alloc]init];
    userSelectedSingleModel.name = @"出征召唤师";
    
    // 初始化提交的model
    releaseSingleModel = [[PDYuezhanReleaseSingleModel alloc]init];
    releaseSingleModel.gameType = @"solo赛";
    releaseSingleModel.gamePwd = @"无密码";
    releaseSingleModel.gold = 50000;
    releaseSingleModel.matchWaitSeconds = 0;
    
    // 金币场
    self.goldSceneMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < 6;i++){
        PDYuezhanReleaseMoneySingleModel *moneySingleModel = [[PDYuezhanReleaseMoneySingleModel alloc]init];
        if (i == 0){
            moneySingleModel.money = 50000;
            moneySingleModel.moneyStr = @"5万";
            [self.goldSceneMutableArr addObject:moneySingleModel];
        } else if (i == 1){
            moneySingleModel.money = 100000;
            moneySingleModel.moneyStr = @"10万";
            [self.goldSceneMutableArr addObject:moneySingleModel];
        } else if (i == 2){
            moneySingleModel.money = 200000;
            moneySingleModel.moneyStr = @"20万";
            [self.goldSceneMutableArr addObject:moneySingleModel];
        } else if (i == 3){
            moneySingleModel.money = 500000;
            moneySingleModel.moneyStr = @"50万";
            [self.goldSceneMutableArr addObject:moneySingleModel];
        } else if (i == 4){
            moneySingleModel.money = 1000000;
            moneySingleModel.moneyStr = @"100万";
            [self.goldSceneMutableArr addObject:moneySingleModel];
        } else if (i == 5){
            moneySingleModel.money = 5000000;
            moneySingleModel.moneyStr = @"500万";
            [self.goldSceneMutableArr addObject:moneySingleModel];
        }
    }

    // 等待时间
    self.waitTimeMutableArr = [NSMutableArray array];
}

-(void)createBttomButton{
    self.bottomActionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.bottomActionButton.backgroundColor = [UIColor clearColor];
    [self.bottomActionButton setTitle:@"开始约战" forState:UIControlStateNormal];
    self.bottomActionButton.frame = CGRectMake(0, kScreenBounds.size.height - LCFloat(50) - 64, kScreenBounds.size.width, LCFloat(50));
    self.bottomActionButton.backgroundColor = [UIColor blackColor];
    __weak typeof(self)weakSelf = self;
    [self.bottomActionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToYuezhanManager];
    }];
    [self.view addSubview:self.bottomActionButton];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.yuezhanTableView){
        self.yuezhanTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.yuezhanTableView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.bottomActionButton.orgin_y + 64);
        self.yuezhanTableView.dataSource = self;
        self.yuezhanTableView.delegate = self;
        [self.view addSubview:self.yuezhanTableView];
    }
}

#pragma mark - UITableViewDataSouece
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.yuezhanArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.yuezhanArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"游戏类型"] && indexPath.row == [self cellIndexPathRowWithcellData:@"游戏类型"]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        
        PDYuezhanTypeTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDYuezhanTypeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferUserSelectedItems = releaseSingleModel.gameType;
        cellWithRowOne.transferTypeArr = self.gameTypeArr;
        cellWithRowOne.transferTitle = @"游戏类型";
        __weak typeof(self)weakSelf = self;
        [cellWithRowOne itemsClickBlock:^(NSString *itemsStr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->releaseSingleModel.gameType = itemsStr;
            [strongSelf.yuezhanTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }];
    
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"房间密码"] && indexPath.row == [self cellIndexPathRowWithcellData:@"房间密码"]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDYuezhanTypeTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDYuezhanTypeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferUserSelectedItems = releaseSingleModel.gamePwd;
        cellWithRowTwo.transferTypeArr = self.homePwdArr;
        cellWithRowTwo.transferTitle = @"房间密码";
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo itemsClickBlock:^(NSString *itemsStr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->releaseSingleModel.gamePwd = itemsStr;
            [strongSelf.yuezhanTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }];
        
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"金额设置"] && indexPath.row == [self cellIndexPathRowWithcellData:@"金额设置"]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PDYuezhanSliderTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[PDYuezhanSliderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"金额设置";
        cellWithRowThr.delegate = self;
        cellWithRowThr.transferMoneyArr = self.goldSceneMutableArr;
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"等待时间限制"] && indexPath.row == [self cellIndexPathRowWithcellData:@"等待时间限制"]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        PDYuezhanSliderTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[PDYuezhanSliderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferTitle = @"等待时间限制";
        cellWithRowFour.transferWaitingTimeArr = [self.waitTimeMutableArr copy];
        cellWithRowFour.delegate = self;
        return cellWithRowFour;
    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"出征区服"] && indexPath.row == [self cellIndexPathRowWithcellData:@"出征区服"]) ||(indexPath.section == [self cellIndexPathSectionWithcellData:@"出征召唤师"] && indexPath.row == [self cellIndexPathRowWithcellData:@"出征召唤师"]) ){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        PDYuezhanChuzhengRoleTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[PDYuezhanChuzhengRoleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFiv.transferCellHeight = cellHeight;
        __weak typeof(self)weakSelf = self;
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"出征区服"]){
            cellWithRowFiv.transferType = yuezhanChuzhengTypeQufu;
            cellWithRowFiv.transferTitle = userSelectedSingleModel.name;
            [cellWithRowFiv itemActionClickBlock:^{
                if (!weakSelf){
                    return ;
                }
                NSLog(@"出征区服");
            }];
        } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"出征召唤师"]){
            cellWithRowFiv.transferType = yuezhanChuzhengTypeZhaohuanshi;
            cellWithRowFiv.transferTitle = userSelectedSingleModel.name;
            [cellWithRowFiv itemActionClickBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf directToChooseRoleList];
            }];
        }
        
        return cellWithRowFiv;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"注意事项"] && indexPath.row == [self cellIndexPathRowWithcellData:@"注意事项"]){
        static NSString *cellIdentifyWithRowSix = @"cellIdentifyWithRowSix";
        PDYuezhanZhuyiShixiangTableViewCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
        if (!cellWithRowSix){
            cellWithRowSix = [[PDYuezhanZhuyiShixiangTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
            cellWithRowSix.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowSix.transferCellheight = cellHeight;
        cellWithRowSix.transferZhuyiArr = self.zhuyiArr;
        return cellWithRowSix;
        
    }
    else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"出征区服"] && indexPath.row == [self cellIndexPathRowWithcellData:@"出征区服"]){
       
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"房间密码"] && indexPath.row == [self cellIndexPathRowWithcellData:@"房间密码"]){
        
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"注意事项"] && indexPath.row == [self cellIndexPathRowWithcellData:@"注意事项"]){
        return [PDYuezhanZhuyiShixiangTableViewCell calculationCellHeightWithArr:self.zhuyiArr];
    }else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"游戏类型"] && indexPath.row == [self cellIndexPathRowWithcellData:@"游戏类型"]){
        return [PDYuezhanTypeTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"房间密码"] && indexPath.row == [self cellIndexPathRowWithcellData:@"房间密码"]){
        return [PDYuezhanTypeTableViewCell calculationCellHeight];
    } else if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"出征区服"] && indexPath.row == [self cellIndexPathRowWithcellData:@"出征区服"]) ||(indexPath.section == [self cellIndexPathSectionWithcellData:@"出征召唤师"] && indexPath.row == [self cellIndexPathRowWithcellData:@"出征召唤师"]) ){
        return  [PDYuezhanChuzhengRoleTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"金额设置"] && indexPath.row == [self cellIndexPathRowWithcellData:@"金额设置"]){
        return [PDYuezhanSliderTableViewCell clculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"等待时间限制"] && indexPath.row == [self cellIndexPathRowWithcellData:@"等待时间限制"]){
        return  [PDYuezhanSliderTableViewCell clculationCellHeight];
    }
    else {
        return LCFloat(44);
    }
}

#pragma mark - createPicker
-(void)createPickerView{
//    if (!self.serverPickerView){
//        self.serverPickerView = [[UIPickerView pickerViewWithDataSource:@[@"123"] block:^(NSInteger comment, NSInteger row) {
//            
//        }];
//        self.serverPickerView.delegate = self;
//                                 
//    }
}

#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.yuezhanArr.count ; i++){
        NSArray *dataTempArr = [self.yuezhanArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.yuezhanArr.count ; i++){
        NSArray *dataTempArr = [self.yuezhanArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}

#pragma mark - 等待时间配置
-(void)sendRequestToGetWaitTime{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:waitconfig requestParams:nil responseObjectClass:[PDYuezhanReleaseWaitConfigModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDYuezhanReleaseWaitConfigModel *singleModel = (PDYuezhanReleaseWaitConfigModel *)responseObject;
            if (strongSelf.waitTimeMutableArr.count){
                [strongSelf.waitTimeMutableArr removeAllObjects];
            }

            if (singleModel.waitSecondsConfig.count){
                for (int i = 0 ; i < singleModel.waitSecondsConfig.count;i++){
                    PDYuezhanReleaseWaitTimeSingleModel *waitSingleModel = [[PDYuezhanReleaseWaitTimeSingleModel alloc]init];
                    NSInteger number = [[singleModel.waitSecondsConfig objectAtIndex:i] integerValue];
                    waitSingleModel.waitTime = number;
                    
                    NSString *waitStr = @"";
                    if (number == 0){
                        waitStr = @"一直等";
                    } else {
                        waitStr = [NSString stringWithFormat:@"%li分钟",(number / 60)];
                    }
                    waitSingleModel.waitTimeStr = waitStr;
                    
                    [strongSelf.waitTimeMutableArr addObject:waitSingleModel];
                }
            }
            
            NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"等待时间限制"];
            NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"等待时间限制"];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
            if (strongSelf.yuezhanTableView){
                [strongSelf.yuezhanTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
    }];
}

-(void)directToChooseRoleList{
    PDYuezhanRoleListViewController *roleListController = [[PDYuezhanRoleListViewController alloc]init];
    roleListController.transferPageType = yuezhanRoleListViewControllerPageTypeOnlyReleaseChoose;
    __weak typeof(self)weakSelf = self;
    [roleListController actionClickWithChooseItemBlock:^(PDYuezhanRoleGameUserSingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->userSelectedSingleModel = singleModel;
        NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"出征召唤师"];
        NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"出征召唤师"];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
        [strongSelf.yuezhanTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
    }];
    [self.navigationController pushViewController:roleListController animated:YES];
}


#pragma mark - GetSet
-(NSArray *)gameTypeArr{
    if (!_gameTypeArr){
        _gameTypeArr = @[@"solo赛",@"团体赛"];
    }
    return _gameTypeArr;
}

-(NSArray *)homePwdArr{
    if (!_homePwdArr){
        _homePwdArr = @[@"无密码",@"有密码"];
    }
    return _homePwdArr;
}

-(NSArray *)zhuyiArr{
    if (!_zhuyiArr){
        _zhuyiArr = @[@"1.请确保召唤师名称正确。",@"2.约战成功创建时会扣除所设置的金币，若约战不成功金币自动退还。"];
    }
    return _zhuyiArr;
}

#pragma mark - 约战接口
-(void)sendRequestToYuezhanManager{
    // 1. 游戏类型
    NSString *gameType = @"";
    if ([releaseSingleModel.gameType isEqualToString:@"solo赛"]){
        gameType = @"solo";
    } else if ([releaseSingleModel.gameType isEqualToString:@"团体赛"]){
        gameType = @"fiveVsFive";
    }
    
    // 2. 房间密码
    BOOL hasPwd = NO;
    if ([releaseSingleModel.gamePwd isEqualToString:@"无密码"]){
        hasPwd = NO;
    } else {
        hasPwd = YES;
    }
    
    // 3.金币数量
    NSInteger gold = releaseSingleModel.gold;
    
    // 4.等待时间
    NSInteger waitTime = releaseSingleModel.matchWaitSeconds;
    
    // 5. 召唤师
    if (!userSelectedSingleModel.ID.length){
        [PDHUD showConnectionErr:@"请选择出征召唤师"];
        return;
    }
    NSString *roleId = userSelectedSingleModel.ID;
    
    NSDictionary *params = @{@"combatGameType":gameType,@"encrypt":@(hasPwd),@"gold":@(gold),@"matchWaitSeconds":@(waitTime),@"gameUserId":roleId};
    __weak typeof(self)wealSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:createcombat requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!wealSelf){
            return ;
        }
        __strong typeof(wealSelf)strongSelf = wealSelf;
        if (isSucceeded){
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"房间创建成功" conten:@"房间已创建成功,请等待玩家挑战" isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
               [[PDAlertView sharedAlertView]dismissAllWithBlock:^{
                   [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
                   void (^block)() = objc_getAssociatedObject(strongSelf, &createSuccessManagerKey);
                   if (block){
                       block();
                   }
               }];
            }];
        }
    }];
}

-(void)sliderDidChangerWithMoneyItems:(PDYuezhanReleaseMoneySingleModel *)items{
    releaseSingleModel.gold = items.money;
}

-(void)sliderDidChangerWithWaitingTimeModel:(PDYuezhanReleaseWaitTimeSingleModel *)singleModel{
    releaseSingleModel.matchWaitSeconds = singleModel.waitTime;
}

-(void)createSuccessManager:(void(^)())block{
    objc_setAssociatedObject(self, &createSuccessManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
