//
//  PDYuezhanRoomListViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDYuezhanGoundsSingleModel.h"

@interface PDYuezhanRoomListViewController : AbstractViewController

@property (nonatomic,strong)PDYuezhanGoundsSingleModel *transferSingleModel;

@end
