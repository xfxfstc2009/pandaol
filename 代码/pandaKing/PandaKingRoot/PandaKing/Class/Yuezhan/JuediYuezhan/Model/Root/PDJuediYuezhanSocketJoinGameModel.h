//
//  PDJuediYuezhanSocketJoinGameModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

// 1110
#import "FetchModel.h"

//"":90， //最大人数
//"joinMemberCount":80, //当前加入人数
//"winGold":900000 //预计可获得的金币

@interface PDJuediYuezhanSocketJoinGameModel : FetchModel
@property (nonatomic,assign)NSInteger maxJoinCount;         /**< 最大人数*/
@property (nonatomic,assign)NSInteger joinMemberCount;      /**< 当前加入人数*/
@property (nonatomic,assign)NSInteger winGold;              /**< 预计可获得的金币*/

@property (nonatomic,copy)NSString *memberId;               /**< 胜利者ID*/
@property (nonatomic,copy)NSString *gameUserName;           /**< 游戏ID*/

@property (nonatomic,copy)NSString *combatId;

@property (nonatomic,copy)NSString *winnerMemberId;         /**< 胜者ID*/
@property (nonatomic,copy)NSString *winnerGameUserName;     /**< 游戏ID*/

@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *password;

@end
