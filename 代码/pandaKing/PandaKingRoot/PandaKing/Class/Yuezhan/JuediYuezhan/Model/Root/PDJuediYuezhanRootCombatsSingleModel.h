//
//  PDJuediYuezhanRootCombatsSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDJuediYuezhanRootCombatsSingleModel <NSObject>

@end

@interface PDJuediYuezhanRootCombatsSingleModel : FetchModel

@property (nonatomic,copy)NSString *combatId;
@property (nonatomic,copy)NSString *combatName;
@property (nonatomic,assign)NSInteger gold;
@property (nonatomic,assign)NSInteger joinMemberCount;
@property (nonatomic,assign)NSInteger maxJoinCount;
@property (nonatomic,assign)NSTimeInterval startTime;
@property (nonatomic,copy)NSString *state;
@property (nonatomic,assign)BOOL joined;

@end
