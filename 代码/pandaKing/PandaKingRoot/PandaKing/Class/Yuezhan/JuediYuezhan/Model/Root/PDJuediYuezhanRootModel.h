//
//  PDJuediYuezhanRootModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDJuediYuezhanRootMycombatsModel.h"
#import "PDJuediYuezhanRootCombatsSingleModel.h"

@interface PDJuediYuezhanRootModel : FetchModel

@property (nonatomic,strong)NSArray<PDJuediYuezhanRootMycombatsModel> *mycombats;
@property (nonatomic,strong)NSArray<PDJuediYuezhanRootCombatsSingleModel> *combats;
@property (nonatomic,assign)BOOL bindGameUser;
@property (nonatomic,copy)NSString *gameUserName;
@property (nonatomic,assign)NSInteger mygold;

@end
