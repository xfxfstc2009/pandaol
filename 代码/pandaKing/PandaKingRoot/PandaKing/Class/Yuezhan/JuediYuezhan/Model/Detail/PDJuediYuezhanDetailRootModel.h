//
//  PDJuediYuezhanDetailRootModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDJuediYuezhanDetaillCombatSingleModel.h"

@interface PDJuediYuezhanDetailRootModel : FetchModel

@property (nonatomic,strong)PDJuediYuezhanDetaillCombatSingleModel *combat;

@end
