//
//  PDJuediYuezhanRootMycombatsModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDJuediYuezhanRootMycombatsModel <NSObject>
@end

@interface PDJuediYuezhanRootMycombatsModel : FetchModel

@property (nonatomic,copy)NSString *nickName;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *gameUserName;
@property (nonatomic,copy)NSString *combatId;
@property (nonatomic,copy)NSString *combatName;
@property (nonatomic,copy)NSString *state;
@property (nonatomic,assign)NSTimeInterval startTime;

@end
