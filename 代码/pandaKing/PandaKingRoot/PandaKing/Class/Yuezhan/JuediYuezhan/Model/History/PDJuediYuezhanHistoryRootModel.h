//
//  PDJuediYuezhanHistoryRootModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDJuediYuezhanRootMycombatsModel.h"
@interface PDJuediYuezhanHistoryRootModel : FetchModel

@property (nonatomic,strong)NSArray<PDJuediYuezhanRootMycombatsModel> *items;

@end
