//
//  PDJuediYuezhanDetaillCombatSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDYuezhanCombatRoomSingleModel.h"

@interface PDJuediYuezhanDetaillCombatSingleModel : FetchModel

//"":"dfdsfsdfasdfsdfsd",
//"":"剛剛發給",
//"":199999,
//"":1999999,
//"":90， //最大人数
//"":80, //当前加入人数
//"":"readying", //约战状态
//"":9000,
//"room":{
//    "name":"gggg",
//    "password":"hhhhhh"
//}
//"":"ggggg",
//"":"fdsfdf",
//"nickName":"哈哈哈哈",
//"avatar":"/po/../.png",
//"gameUserName":"ffff",
@property (nonatomic,copy)NSString *combatId;
@property (nonatomic,copy)NSString *combatName;
@property (nonatomic,assign)NSTimeInterval currentTime;         /**< 当前时间*/
@property (nonatomic,assign)NSTimeInterval startTime;           /**< 开始时间*/
@property (nonatomic,assign)NSInteger maxJoinCount;             /**< 最大人数*/
@property (nonatomic,assign)NSInteger joinMemberCount;          /**< 当前加入人数*/
@property (nonatomic,copy)NSString *state;                      /**< 约战状态*/
@property (nonatomic,copy)NSString *groupId;                    /**< 队伍id*/
@property (nonatomic,strong)PDYuezhanCombatRoomSingleModel *room;   /**< 房间*/
@property (nonatomic,copy)NSString *winnerGameUserName;         /**< 胜者人名字*/
@property (nonatomic,copy)NSString *winnerMemberId;             /**< 胜利者id*/
@property (nonatomic,copy)NSString *nickName;                   /**< 昵称*/
@property (nonatomic,copy)NSString *avatar;                     /**< 头像*/
@property (nonatomic,copy)NSString *gameUserName;               /**< 游戏名字*/
@property (nonatomic,assign)NSInteger winGold;                  /**< 胜利可获得*/


@property (nonatomic,assign)NSTimeInterval tempTargetTimeInterval;
@end
