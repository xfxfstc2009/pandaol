//
//  PDPubgSearchModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDPubgSearchModel : FetchModel

@property (nonatomic,copy)NSString *gameUserName;
@property (nonatomic,assign)BOOL has;

@end
