//
//  PDJuediYuezhanBindingTishiTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanBindingTishiTableViewCell.h"

@interface PDJuediYuezhanBindingTishiTableViewCell()
@property (nonatomic,strong)NSArray *normalTitleArr;
@property (nonatomic,strong)NSMutableArray *normalLabelMutableArr;
@property (nonatomic,strong)UIView *bgView;

@end

@implementation PDJuediYuezhanBindingTishiTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self arrayWithInit];
        [self createView];
    }
    return self;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.normalTitleArr = @[@"* 请核对输入ID是否正确，一旦绑定无法修改",@"* 暂时只支持绝地求生：大逃杀【亚服】用户"];
    self.normalLabelMutableArr = [NSMutableArray array];
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgView];
    
    CGFloat margin_x = kScreenBounds.size.width;
    for (int i = 0 ; i <self.normalTitleArr.count;i++){
        UILabel *titleLabel = [GWViewTool createLabelFont:@"提示" textColor:@"灰"];
        titleLabel.text = [self.normalTitleArr objectAtIndex:i];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [self.bgView addSubview:titleLabel];
        CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:titleLabel.font])];
        CGFloat width = (kScreenBounds.size.width - titleSize.width) / 2.;
        if (margin_x > width){
            margin_x = width;
        }
        [self.normalLabelMutableArr addObject:titleLabel];
    }
    CGFloat origin_y = LCFloat(11);
    for (UILabel *label in self.normalLabelMutableArr){
        CGSize titleSize = [label.text sizeWithCalcFont:label.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:label.font])];
        label.frame = CGRectMake(margin_x, origin_y, titleSize.width, [NSString contentofHeightWithFont:label.font]);
        origin_y += ([NSString contentofHeightWithFont:label.font] + LCFloat(11));
    }
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [PDJuediYuezhanBindingTishiTableViewCell calculationCellHeight]);
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeihgt = 0;
    cellHeihgt += LCFloat(11);
    cellHeihgt += 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
    cellHeihgt += LCFloat(11);
    cellHeihgt += LCFloat(11);
    return cellHeihgt;
}


@end
