//
//  PDSearchCleanAllTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/8/1.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDSearchCleanAllTableViewCell.h"

@interface PDSearchCleanAllTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation PDSearchCleanAllTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    self.iconImgView.image = [UIImage imageNamed:@"icon_pubg_cleanAll"];
    [self addSubview:self.iconImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"金"];
    self.titleLabel.text = @"清空历史记录";
    [self addSubview:self.titleLabel];
    
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    CGFloat margin = (kScreenBounds.size.width - LCFloat(11) - LCFloat(14) - titleSize.width) / 2.;
    self.iconImgView.frame = CGRectMake(margin, (LCFloat(44) - LCFloat(14)) / 2., LCFloat(14), LCFloat(14));
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(11), 0, titleSize.width, LCFloat(44));
}

@end
