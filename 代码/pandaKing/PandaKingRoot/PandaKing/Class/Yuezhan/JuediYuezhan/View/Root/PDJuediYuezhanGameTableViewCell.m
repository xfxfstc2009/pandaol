//
//  PDJuediYuezhanGameTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanGameTableViewCell.h"

@interface PDJuediYuezhanGameTableViewCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *areaLabel;                         /**< 区label*/
@property (nonatomic,strong)PDImageView *qiziImgView;                   /**< 旗帜图片*/
@property (nonatomic,strong)UILabel *timeLabel;
@end

@implementation PDJuediYuezhanGameTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.layer.cornerRadius = LCFloat(3);
    self.bgView.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
    self.bgView.layer.borderWidth = 1;
    [self addSubview:self.bgView];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.avatarImgView];
    
    // 2. 创建名字
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
    [self.bgView addSubview:self.titleLabel];
    
    // 5. 创建区域label
    self.areaLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self.bgView addSubview:self.areaLabel];
    
    // 6. 旗帜
    self.qiziImgView = [[PDImageView alloc]init];
    self.qiziImgView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.qiziImgView];
    
    // 7. 时间
    self.timeLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self.bgView addSubview:self.timeLabel];
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(14);
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferMyCombatsModel:(PDJuediYuezhanRootMycombatsModel *)transferMyCombatsModel{
    _transferMyCombatsModel = transferMyCombatsModel;
    
    // 0.bg
    self.bgView.frame = CGRectMake(LCFloat(11), LCFloat(7), kScreenBounds.size.width - 2 * LCFloat(11), self.transferCellHeight - 2 * LCFloat(7));
    
    // 1. 头像
    [self.avatarImgView uploadImageWithURL:transferMyCombatsModel.avatar placeholder:nil callback:NULL];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(LCFloat(11), LCFloat(11), (self.bgView.size_height - 2 * LCFloat(11)), (self.bgView.size_height - 2 * LCFloat(11)));
    
    self.titleLabel.text = transferMyCombatsModel.nickName.length?transferMyCombatsModel.nickName:transferMyCombatsModel.gameUserName;
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), self.avatarImgView.orgin_y, 200, [NSString contentofHeightWithFont:self.titleLabel.font]);

    
    // 4. 创建地址
    self.areaLabel.text = transferMyCombatsModel.combatName;
    CGSize areaSize = [self.areaLabel.text sizeWithCalcFont:self.areaLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.areaLabel.font])];
    self.areaLabel.frame  = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.avatarImgView.frame) - [NSString contentofHeightWithFont:self.areaLabel.font], areaSize.width, [NSString contentofHeightWithFont:self.areaLabel.font]);
    
    if ([transferMyCombatsModel.state isEqualToString:@"readying"]){                    // 准备中
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_zhunbeizhong"];
        self.qiziImgView.hidden = NO;
    } else if ([transferMyCombatsModel.state isEqualToString:@"waitOver"]){                // 结算中
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_jiesuanzhong"];
        self.qiziImgView.hidden = NO;
    } else if ([transferMyCombatsModel.state isEqualToString:@"settled"]){                 // 已经结算
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_yijiesuan"];
        self.qiziImgView.hidden = NO;
    } else if ([transferMyCombatsModel.state isEqualToString:@"canceled"]){                // 已取消
        self.qiziImgView.image = [UIImage imageNamed:@"icon_yuezhan_root_yiquxiao"];
        self.qiziImgView.hidden = NO;
    }
    
    self.qiziImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(26) - LCFloat(11) - LCFloat(11), 0, LCFloat(26), LCFloat(27));
    
    // 时间
    self.timeLabel.text = [NSDate getTimeWithLotteryString:self.transferMyCombatsModel.startTime / 1000.];
    CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.timeLabel.font])];
    self.timeLabel.frame = CGRectMake(self.bgView.size_width - LCFloat(11) - timeSize.width, CGRectGetMaxY(self.bgView.frame) - LCFloat(11) - [NSString contentofHeightWithFont:self.timeLabel.font], timeSize.width, [NSString contentofHeightWithFont:self.timeLabel.font]);
}

@end
