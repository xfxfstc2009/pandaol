//
//  PDJuediYuezhanSaishiTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanSaishiTableViewCell.h"

@interface PDJuediYuezhanSaishiTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;             /**< 图片*/
@property (nonatomic,strong)UILabel *nameLabel;                     /**< 服务器名字*/
@property (nonatomic,strong)UILabel *timeLabel;                     /**< 时间人数*/
@property (nonatomic,strong)UILabel *numberLabel;                   /**< 玩家人数*/
@property (nonatomic,assign)CGFloat timeCenter_x;

@end

@implementation PDJuediYuezhanSaishiTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.hidden = YES;
    self.avatarImgView.image = [UIImage imageNamed:@"icon_juediyuezhan_yafu"];
    [self addSubview:self.avatarImgView];

    // 创建名字
    self.nameLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.nameLabel];
    
    // 3. 创建时间
    self.timeLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.timeLabel];
    
    // 4. 创建玩家人数
    self.numberLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.numberLabel];
    
    
    // 设置
    self.nameLabel.text = @"游戏区服/战斗场";
    self.timeLabel.text = @"游戏时间";
    self.numberLabel.text = @"玩家人数";
    
    self.avatarImgView.frame = CGRectMake(LCFloat(11), (self.transferCellHeight - LCFloat(20)) / 2., LCFloat(20), LCFloat(20));
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    self.nameLabel.frame = CGRectMake(LCFloat(11), 0, nameSize.width, LCFloat(50));
    
    CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.timeLabel.font])];
    
    CGSize numberSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.numberLabel.font])];
    
    CGFloat margin = (kScreenBounds.size.width - 2 * LCFloat(11) - nameSize.width - timeSize.width - numberSize.width) / 2.;
    self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + margin, 0, timeSize.width, LCFloat(50));
    self.numberLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - numberSize.width, 0, numberSize.width, LCFloat(50));
    
    self.timeCenter_x = self.timeLabel.center_x;
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}


-(void)setTransferJuedisaishiType:(juediYuezhanSaishiType)transferJuedisaishiType{
    _transferJuedisaishiType = transferJuedisaishiType;
    if (self.transferJuedisaishiType == juediYuezhanSaishiTypeTitle){
        self.avatarImgView.hidden = YES;
        self.nameLabel.orgin_x = LCFloat(11);
    }
}

-(void)setTransferSingleModel:(PDJuediYuezhanRootCombatsSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    if (self.transferJuedisaishiType == juediYuezhanSaishiTypeNormal){          // 其他
        self.avatarImgView.frame = CGRectMake(LCFloat(11), (self.transferCellHeight - LCFloat(20)) / 2., LCFloat(20), LCFloat(20));
        self.avatarImgView.hidden = YES;
        // 1. name
        self.nameLabel.text = self.transferSingleModel.combatName;
        
        // 2. time
        self.timeLabel.textAlignment = NSTextAlignmentCenter;
        self.timeLabel.text = [NSDate getTimeWithLotteryString:self.transferSingleModel.startTime / 1000];
        CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.timeLabel.font])];
        self.timeLabel.frame = CGRectMake(0, 0, timeSize.width, self.transferCellHeight);
        self.timeLabel.center_x = self.timeCenter_x;
        // 3. 玩家人数
        self.numberLabel.textAlignment = NSTextAlignmentRight;
        self.numberLabel.text = [NSString stringWithFormat:@"%li/%li",self.transferSingleModel.joinMemberCount,self.transferSingleModel.maxJoinCount];
        
        if ([self.transferSingleModel.state isEqualToString:@"waitOver"]){
            self.timeLabel.textColor = [UIColor colorWithCustomerName:@"红"];
            self.numberLabel.textColor = [UIColor colorWithCustomerName:@"红"];
            self.timeLabel.text = @"已开";
            
        } else {
            self.timeLabel.textColor = [UIColor blackColor];
            self.numberLabel.textColor = [UIColor blackColor];
        }
    } else {
        self.avatarImgView.hidden = YES;
        self.nameLabel.orgin_x = LCFloat(11);
    }
}

+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}

@end
