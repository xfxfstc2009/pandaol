//
//  PDJuediSearchTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediSearchTableViewCell.h"

@interface PDJuediSearchTableViewCell()
@property (nonatomic,strong)PDImageView *bannerImgView;

@end

@implementation PDJuediSearchTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bannerImgView = [[PDImageView alloc]init];
    self.bannerImgView.backgroundColor = [UIColor clearColor];
    self.bannerImgView.image = [UIImage imageNamed:@"banner.jpg"];
    self.bannerImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(210));
    [self addSubview:self.bannerImgView];
}


+(CGFloat)calculationCellHeight{
    return LCFloat(210);
}

@end
