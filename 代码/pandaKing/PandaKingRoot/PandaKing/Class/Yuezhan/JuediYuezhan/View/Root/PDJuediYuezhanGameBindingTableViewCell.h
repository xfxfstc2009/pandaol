//
//  PDJuediYuezhanGameBindingTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDJuediYuezhanGameBindingTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;

@end
