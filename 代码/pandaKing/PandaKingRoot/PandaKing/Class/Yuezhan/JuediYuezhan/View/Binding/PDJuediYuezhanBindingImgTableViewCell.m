//
//  PDJuediYuezhanBindingImgTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanBindingImgTableViewCell.h"

@interface PDJuediYuezhanBindingImgTableViewCell()
@property (nonatomic,strong)PDImageView *bindingImgView;                /**< 绑定大图*/

@end

@implementation PDJuediYuezhanBindingImgTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bindingImgView = [[PDImageView alloc]init];
    self.bindingImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bindingImgView];
    self.bindingImgView.image = [UIImage imageNamed:@"img_firstLogin_ow"];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.bindingImgView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(220)) / 2., 0, LCFloat(220), LCFloat(220));
}

+(CGFloat)calculationCellHeight{
    return LCFloat(220);
}


@end
