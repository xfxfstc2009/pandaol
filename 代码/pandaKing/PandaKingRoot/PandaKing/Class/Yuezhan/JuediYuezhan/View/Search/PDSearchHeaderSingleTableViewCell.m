//
//  PDSearchHeaderSingleTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDSearchHeaderSingleTableViewCell.h"

@interface PDSearchHeaderSingleTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *zhanjiLabel;

@end

@implementation PDSearchHeaderSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.titleLabel.text = @"查询历史";
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(LCFloat(11), 0, titleSize.width, LCFloat(50));
    [self addSubview:self.titleLabel];
    
    // 战绩label
    self.zhanjiLabel = [GWViewTool createLabelFont:@"提示" textColor:@"灰"];
    self.zhanjiLabel.text = @"我的战绩";
    self.zhanjiLabel.textAlignment = NSTextAlignmentCenter;
    CGSize zhanjiSize = [self.zhanjiLabel.text sizeWithCalcFont:self.zhanjiLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.zhanjiLabel.font])];
    self.zhanjiLabel.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
    CGFloat width = zhanjiSize.width + 2 * LCFloat(11);
    CGFloat height = zhanjiSize.height + 2 * LCFloat(5);
    self.zhanjiLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - width, 0, width, height);
    self.zhanjiLabel.layer.borderWidth = .5f;
    self.zhanjiLabel.layer.cornerRadius = MIN(width, height) / 2.;
    [self addSubview:self.zhanjiLabel];
    
    self.zhanjiLabel.center_y = LCFloat(25);
}

+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}

-(void)zhanjiStatus:(BOOL)status{
    if (status){
        self.zhanjiLabel.text = @"我的战绩";
    } else {
        self.zhanjiLabel.text = @"绑定角色";
    }
}
@end
