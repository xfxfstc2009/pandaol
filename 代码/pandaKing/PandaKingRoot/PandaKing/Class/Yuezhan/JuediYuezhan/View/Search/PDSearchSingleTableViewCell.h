//
//  PDSearchSingleTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDSearchSingleTableViewCell : UITableViewCell

@property (nonatomic,copy)NSString *transferUser;
@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;
-(void)closeManager:(void(^)(NSString *closeManager))block;

@end
