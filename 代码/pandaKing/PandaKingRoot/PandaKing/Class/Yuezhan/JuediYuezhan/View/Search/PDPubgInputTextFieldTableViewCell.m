//
//  PDPubgInputTextFieldTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPubgInputTextFieldTableViewCell.h"

static char textFieldDidChangeBlockKey;
@interface PDPubgInputTextFieldTableViewCell()

@end

@implementation PDPubgInputTextFieldTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self  createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    //2.创建输入框
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor colorWithCustomerName:@"分隔线"];
    self.inputTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.inputTextField.placeholder = @"请输入需要查询的绝地求生游戏昵称";
    self.inputTextField.textAlignment = NSTextAlignmentCenter;
    self.inputTextField.borderStyle = UITextBorderStyleRoundedRect;
    
    
    [self addSubview:self.inputTextField];
    self.inputTextField.frame = CGRectMake(LCFloat(11), LCFloat(34), kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(40));
    [self.inputTextField addTarget:self action:@selector(textFieldDidChanged) forControlEvents:UIControlEventEditingChanged];
}

-(void)textFieldDidChanged{
    void(^block)(NSString *str) = objc_getAssociatedObject(self, &textFieldDidChangeBlockKey);
    if (block){
        block(self.inputTextField.text);
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(34);
    cellHeight += LCFloat(40);
    cellHeight += LCFloat(34);
    return cellHeight;
}

-(void)textFieldDidChangeBlock:(void(^)(NSString *str))block{
    objc_setAssociatedObject(self, &textFieldDidChangeBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
