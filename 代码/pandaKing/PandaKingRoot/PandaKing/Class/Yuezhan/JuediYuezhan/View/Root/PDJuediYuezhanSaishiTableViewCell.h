//
//  PDJuediYuezhanSaishiTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDJuediYuezhanRootCombatsSingleModel.h"

typedef NS_ENUM(NSInteger,juediYuezhanSaishiType) {
    juediYuezhanSaishiTypeTitle = 1,                        /**< 绝地逃生标题*/
    juediYuezhanSaishiTypeNormal = 2,                       /**< 绝地逃生其他*/
};

@interface PDJuediYuezhanSaishiTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,assign)juediYuezhanSaishiType transferJuedisaishiType;
@property (nonatomic,strong)PDJuediYuezhanRootCombatsSingleModel *transferSingleModel;

+(CGFloat)calculationCellHeight;

@end
