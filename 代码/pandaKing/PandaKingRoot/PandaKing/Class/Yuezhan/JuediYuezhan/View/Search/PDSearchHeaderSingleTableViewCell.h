//
//  PDSearchHeaderSingleTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDSearchHeaderSingleTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;

-(void)zhanjiStatus:(BOOL)status;

@end
