//
//  PDPubgInputTextFieldTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDPubgInputTextFieldTableViewCell : UITableViewCell
@property (nonatomic,strong)UITextField *inputTextField;

-(void)textFieldDidChangeBlock:(void(^)(NSString *str))block;

+(CGFloat)calculationCellHeight;
@end
