//
//  PDJuediYuezhanBindingImgTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDJuediYuezhanBindingImgTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;

@end
