//
//  PDSearchSingleTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDSearchSingleTableViewCell.h"


static char closeManagerKey;
@interface PDSearchSingleTableViewCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *closeButton;

@end

@implementation PDSearchSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.image = [UIImage imageNamed:@"icon_pubg_search_history"];
    self.iconImgView.frame = CGRectMake(LCFloat(11), 0, LCFloat(14), LCFloat(14));
    self.iconImgView.center_y = self.center_y;
    [self addSubview:self.iconImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11) - LCFloat(14) - LCFloat(14) - LCFloat(11), LCFloat(44));
    [self addSubview:self.titleLabel];
    
    self.closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.closeButton setImage:[UIImage imageNamed:@"icon_pubg_search_delete"] forState:UIControlStateNormal];
    self.closeButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.closeButton];
    __weak typeof(self)weakSelf = self;
    [self.closeButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSString *user) = objc_getAssociatedObject(strongSelf, &closeManagerKey);
        if (block){
            block(strongSelf.transferUser);
        }
    }];
    self.closeButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(44), 0, LCFloat(44), LCFloat(44));
    self.closeButton.center_y = self.center_y;
}

-(void)closeManager:(void(^)(NSString *closeManager))block{
    objc_setAssociatedObject(self, &closeManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferUser:(NSString *)transferUser{
    _transferUser = transferUser;
    self.titleLabel.text = transferUser;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}

@end
