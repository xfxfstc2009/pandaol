//
//  PDJuediYuezhanGameBindingTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanGameBindingTableViewCell.h"

@interface PDJuediYuezhanGameBindingTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *goldLabel;

@end

@implementation PDJuediYuezhanGameBindingTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"黑"];
}

+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}


@end
