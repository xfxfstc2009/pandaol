//
//  PDJuediYuezhanHeaderViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanHeaderViewController.h"

@interface PDJuediYuezhanHeaderViewController ()
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *gameIdLabel;
@property (nonatomic,strong)UILabel *houseLabel;
@property (nonatomic,strong)UILabel *housePwdLabel;
@property (nonatomic,strong)UILabel *daojishiLabel;
@property (nonatomic,strong)UILabel *normalLabel;
@property (nonatomic,strong)UIButton *readyButton;
@property (nonatomic,strong)PDJuediYuezhanDetaillCombatSingleModel *transferCombatSingleModel;
@property (nonatomic,strong)UITapGestureRecognizer *tapGuest;

@end

@implementation PDJuediYuezhanHeaderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self createView];
    self.tapGuest = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapGestureManager)];
    [self.view addGestureRecognizer:self.tapGuest];
}

#pragma mark - createView
-(void)createView{
    // 0 .
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.leftButton setImage:[UIImage imageNamed:@"icon_packet_back"] forState:UIControlStateNormal];
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    self.leftButton.frame = CGRectMake(LCFloat(11), 20, 44, 44);
    [self.view addSubview:self.leftButton];
    
    // 0.1
    self.titleLabel = [GWViewTool createLabelFont:@"标题" textColor:@"金"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.textColor = c26;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.leftButton.frame), 20, kScreenBounds.size.width - 2 * CGRectGetMaxX(self.leftButton.frame), 44);
    self.titleLabel.text = @"";
    [self.view addSubview:self.titleLabel];
    
    // 1. 创建头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(LCFloat(11),CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), LCFloat(60), LCFloat(60));
    self.avatarImgView.clipsToBounds = YES;
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_height / 2.;
    [self.view addSubview:self.avatarImgView];
    
    // 2. 创建label
    self.nickNameLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"金"];
    self.nickNameLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.nickNameLabel];
    
    // 3. 创建id
    self.gameIdLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"金"];
    self.gameIdLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.gameIdLabel];
    
    // 4. 创建房间
    self.houseLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"金"];
    self.houseLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.houseLabel];
    
    // 5. 创建密码
    self.housePwdLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"金"];
    self.housePwdLabel.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.housePwdLabel];
    
    // 6. 倒计时
    self.daojishiLabel = [GWViewTool createLabelFont:@"标题" textColor:@"金"];
    self.daojishiLabel.backgroundColor = [UIColor clearColor];
    self.daojishiLabel.font = [[UIFont systemFontOfCustomeSize:40] boldFont];
    self.daojishiLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.daojishiLabel];
    
    // 7. 创建normal
    self.normalLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"金"];
    self.normalLabel.backgroundColor = [UIColor clearColor];
    self.normalLabel.textAlignment = NSTextAlignmentCenter;
    [self.view addSubview:self.normalLabel];
    
    // 8. 创建按钮
    self.readyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.readyButton.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.readyButton];
    [self.readyButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(readyButtonClick)]){
            [strongSelf.delegate readyButtonClick];
        }
    }];
    [self.readyButton setTitle:@"准备" forState:UIControlStateNormal];
    [self.readyButton setBackgroundImage:[UIImage imageNamed:@"icon_yuezhan_ready"] forState:UIControlStateNormal];
    [self.readyButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

-(void)viewsLoadWithModel:(PDJuediYuezhanDetaillCombatSingleModel *)singleModel{
    _transferCombatSingleModel = singleModel;
    
    self.titleLabel.text = singleModel.combatName;
    
    // 1. 图片
    [self.avatarImgView uploadImageWithURL:self.transferCombatSingleModel.avatar placeholder:nil callback:NULL];
    
    // 2. 名字
    CGFloat margin_width = (kScreenBounds.size.width - LCFloat(60) - 2 * LCFloat(11) - LCFloat(30)) / 2.;
    
    self.nickNameLabel.text = self.transferCombatSingleModel.nickName;
    self.nickNameLabel.adjustsFontSizeToFitWidth = YES;
    self.nickNameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), self.avatarImgView.orgin_y, margin_width, [NSString contentofHeightWithFont:self.nickNameLabel.font]);
    
    // 3. 游戏ID
    self.gameIdLabel.text = self.transferCombatSingleModel.gameUserName;
    self.gameIdLabel.adjustsFontSizeToFitWidth = YES;
    self.gameIdLabel.frame = CGRectMake(self.nickNameLabel.orgin_x, CGRectGetMaxY(self.avatarImgView.frame) -[NSString contentofHeightWithFont:self.gameIdLabel.font], self.nickNameLabel.size_width, [NSString contentofHeightWithFont:self.gameIdLabel.font]);
    
    // 4. 房间
    self.houseLabel.text = [NSString stringWithFormat:@"房间:%@",self.transferCombatSingleModel.room.name.length?self.transferCombatSingleModel.room.name:@"暂无"];
    self.houseLabel.adjustsFontSizeToFitWidth = YES;
    self.houseLabel.frame = CGRectMake(CGRectGetMaxX(self.nickNameLabel.frame) + LCFloat(30), self.nickNameLabel.orgin_y, margin_width, [NSString contentofHeightWithFont:self.houseLabel.font]);
    
    // 5. 房间密码
    
    self.housePwdLabel.text = [NSString stringWithFormat:@"密码:%@",self.transferCombatSingleModel.room.password.length?self.transferCombatSingleModel.room.password:@"暂无"];
    self.housePwdLabel.adjustsFontSizeToFitWidth = YES;
    self.housePwdLabel.frame = CGRectMake(self.houseLabel.orgin_x, self.gameIdLabel.orgin_y, margin_width, [NSString contentofHeightWithFont:self.housePwdLabel.font]);
    
    // 6.倒计时
    self.daojishiLabel.frame = CGRectMake(0, CGRectGetMaxY(self.avatarImgView.frame) + LCFloat(15), kScreenBounds.size.width, LCFloat(50));
    self.daojishiLabel.adjustsFontSizeToFitWidth = YES;
    self.daojishiLabel.text = @"";
    
    // 7. normal
    
    self.normalLabel.text = [NSString stringWithFormat:@"当前%li/%li人,吃鸡预计可获得%li金币",self.transferCombatSingleModel.joinMemberCount,self.transferCombatSingleModel.maxJoinCount,self.transferCombatSingleModel.winGold];
    self.normalLabel.frame = CGRectMake(0, CGRectGetMaxY(self.daojishiLabel.frame) + LCFloat(11), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.normalLabel.font]);
    self.normalLabel.textAlignment = NSTextAlignmentCenter;
    
    // 8 .
    self.readyButton.frame = CGRectMake(LCFloat(80), CGRectGetMaxY(self.normalLabel.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(80), LCFloat(50));
    
    
    // 判断时间
    if ([singleModel.state isEqualToString:@"readying"]){         // 准备中
        [self.readyButton setTitle:@"已准备" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = YES;
    } else if ([singleModel.state isEqualToString:@"waitOver"]){          // 结算中
        [self.readyButton setTitle:@"结算中" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = NO;
        self.daojishiLabel.text = @"战斗中";
    } else if ([singleModel.state isEqualToString:@"settled"]){           // 已经结算
        [self.readyButton setTitle:@"已结算" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = NO;
        [self.readyButton setTitleColor:RGB(112, 110, 105, 1) forState:UIControlStateNormal];
        
        NSArray *winerArr = [singleModel.winnerMemberId componentsSeparatedByString:@","];
        if ([winerArr containsObject:[AccountModel sharedAccountModel].memberInfo.ID]){
            self.daojishiLabel.text = @"大吉大利，今晚吃鸡";
            self.normalLabel.text = [NSString stringWithFormat:@"恭喜您获得%li金币",singleModel.winGold];
            self.daojishiLabel.textColor = c26;
            self.normalLabel.textColor = c26;
            self.readyButton.hidden = NO;
        } else {
            self.daojishiLabel.text = @"再接再厉,下次吃鸡";
            
            self.normalLabel.text = [NSString stringWithFormat:@"玩家【%@】吃鸡成功获得%li金币",singleModel.winnerGameUserName,singleModel.winGold];
            self.daojishiLabel.textColor = RGB(112, 110, 105, 1);
            self.normalLabel.textColor = RGB(112, 110, 105, 1);
            
            CGSize winHeight = [self.normalLabel.text sizeWithCalcFont:self.normalLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
            if (winHeight.height > 3 * [NSString contentofHeightWithFont:self.normalLabel.font]){
                self.normalLabel.size_height = 3 * [NSString contentofHeightWithFont:self.normalLabel.font];
                self.normalLabel.numberOfLines = 3;
            } else {
                self.normalLabel.size_height = winHeight.height;
                self.normalLabel.numberOfLines = 0;
            }
            self.readyButton.orgin_y = CGRectGetMaxY(self.normalLabel.frame);
            self.readyButton.hidden = YES;
        }
        self.daojishiLabel.font = [UIFont systemFontOfCustomeSize:25.];
        [self.readyButton setTitle:@"已结算" forState:UIControlStateNormal];

        
        self.daojishiLabel.font = [UIFont systemFontOfCustomeSize:25.];
    } else if ([singleModel.state isEqualToString:@"canceled"]){          // 已取消
        [self.readyButton setTitle:@"已取消" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = NO;
    }
}

-(void)timerChange:(NSInteger)time{
    self.daojishiLabel.text = [NSDate getJuediLotteryTargetTime:self.transferCombatSingleModel.tempTargetTimeInterval / 1000.];
}


+(CGFloat)calculationControllerHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(60);
    cellHeight += LCFloat(15);
    cellHeight += LCFloat(50);
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(50);
    cellHeight += LCFloat(11);
    cellHeight += 64;
    return cellHeight;
}

-(void)tapGestureManager{
    if (self.delegate && [self.delegate respondsToSelector:@selector(tapManager)]){
        [self.delegate tapManager];
    }
}


// 加入约战
-(void)socketJiaruyuezhanManager:(PDJuediYuezhanSocketJoinGameModel *)singleModel{
    if ([singleModel.combatId isEqualToString:self.transferCombatSingleModel.combatId]){
        self.transferCombatSingleModel.joinMemberCount = singleModel.joinMemberCount;
        self.transferCombatSingleModel.maxJoinCount = singleModel.maxJoinCount;
        self.normalLabel.text = [NSString stringWithFormat:@"当前%li/%li人,吃鸡预计可获得%li金币",singleModel.joinMemberCount,singleModel.maxJoinCount,singleModel.winGold];
    }
}

// 房子配置
-(void)socketPeizhifangjianManager:(PDJuediYuezhanSocketJoinGameModel *)singleModel{
    if ([singleModel.combatId isEqualToString:self.transferCombatSingleModel.combatId]){
        self.transferCombatSingleModel.room.name = singleModel.name;
        self.transferCombatSingleModel.room.password = singleModel.password;
        self.houseLabel.text = [NSString stringWithFormat:@"房间:%@",singleModel.name.length?singleModel.name:@"暂无"];
        self.housePwdLabel.text = [NSString stringWithFormat:@"密码:%@",singleModel.password.length?singleModel.password:@"暂无"];
    }
}

// 约战开始
-(void)socketYuezhankaishiManager:(PDJuediYuezhanSocketJoinGameModel *)singleModel{
    if ([singleModel.combatId isEqualToString:self.transferCombatSingleModel.combatId]){
        self.daojishiLabel.text = @"战斗中";
        [self.readyButton setTitle:@"结算中" forState:UIControlStateNormal];
        self.readyButton.userInteractionEnabled = NO;
    }
}

// 约战结算
-(void)socketYuezhanjiesuanManager:(PDJuediYuezhanSocketJoinGameModel *)singleModel{
    if ([singleModel.combatId isEqualToString:self.transferCombatSingleModel.combatId]){
        [self.readyButton setTitleColor:RGB(112, 110, 105, 1) forState:UIControlStateNormal];
        
        
        
        NSArray *winerArr = [singleModel.winnerMemberId componentsSeparatedByString:@","];
        if ([winerArr containsObject:[AccountModel sharedAccountModel].memberInfo.ID]){
            self.daojishiLabel.text = @"大吉大利，今晚吃鸡";
            self.normalLabel.text = [NSString stringWithFormat:@"恭喜您获得%li金币",singleModel.winGold];
            self.daojishiLabel.textColor = c26;
            self.normalLabel.textColor = c26;
            self.readyButton.hidden = NO;
        } else {
            self.daojishiLabel.text = @"再接再厉,下次吃鸡";
            
            self.normalLabel.text = [NSString stringWithFormat:@"玩家【%@】吃鸡成功获得%li金币",singleModel.winnerGameUserName,singleModel.winGold];
            self.daojishiLabel.textColor = RGB(112, 110, 105, 1);
            self.normalLabel.textColor = RGB(112, 110, 105, 1);
            
            CGSize winHeight = [self.normalLabel.text sizeWithCalcFont:self.normalLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
            if (winHeight.height > 3 * [NSString contentofHeightWithFont:self.normalLabel.font]){
                self.normalLabel.size_height = 3 * [NSString contentofHeightWithFont:self.normalLabel.font];
                self.normalLabel.numberOfLines = 3;
            } else {
                self.normalLabel.size_height = winHeight.height;
                self.normalLabel.numberOfLines = 0;
            }
            self.readyButton.orgin_y = CGRectGetMaxY(self.normalLabel.frame);
            self.readyButton.hidden = YES;
        }
        self.daojishiLabel.font = [UIFont systemFontOfCustomeSize:25.];
        [self.readyButton setTitle:@"已结算" forState:UIControlStateNormal];
    }
}
@end

