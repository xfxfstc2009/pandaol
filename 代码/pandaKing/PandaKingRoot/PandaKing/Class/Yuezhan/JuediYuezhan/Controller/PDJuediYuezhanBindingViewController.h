//
//  PDJuediYuezhanBindingViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDJuediYuezhanBindingViewController : AbstractViewController

-(void)juediBindingSuccessBlock:(void(^)())block;

@end
