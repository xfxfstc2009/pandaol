//
//  PDJuediYuezhanSearchViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanSearchViewController.h"
#import "PDStupSQLiteManager.h"
#import "PDJuediSearchTableViewCell.h"
#import "PDSearchHeaderSingleTableViewCell.h"
#import "PDSearchSingleTableViewCell.h"
#import "PDPubgInputTextFieldTableViewCell.h"
#import "PDWebViewController.h"
#import "PDPubgSearchModel.h"
#import "PDJuediYuezhanBindingViewController.h"
#import "PDSearchCleanAllTableViewCell.h"

@interface PDJuediYuezhanSearchViewController()<UITableViewDataSource,UITableViewDelegate>{
    UITextField *tempInputTextField;
    PDPubgSearchModel *searchModel;
    PDSearchHeaderSingleTableViewCell *tempCell;
}
@property (nonatomic,strong)UITableView *searchTableView;
@property (nonatomic,strong)NSMutableArray *searchRootMutableArr;
@property (nonatomic,strong)NSMutableArray *searchHistoryMutableArr;

@end

@implementation PDJuediYuezhanSearchViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithinit];
    [self createTableView];
    [self getCurrentArr];
    [self sendRequestToGetInfo];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"数据查询";
}

#pragma mark - arrayWithInit
-(void)arrayWithinit{
    self.searchHistoryMutableArr = [NSMutableArray array];
    self.searchRootMutableArr = [NSMutableArray array];
    
    NSArray *infoArr = @[@[@"banner"],@[@"搜索"],@[@"确定按钮",@"空"]];
    [self.searchRootMutableArr addObjectsFromArray:infoArr];
    [self.searchRootMutableArr addObject:self.searchHistoryMutableArr];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.searchTableView){
        self.searchTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.searchTableView.dataSource = self;
        self.searchTableView.delegate = self;
        [self.view addSubview:self.searchTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.searchRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *seationOfArr = [self.searchRootMutableArr objectAtIndex:section];
    return seationOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"banner"] && indexPath.row == [self cellIndexPathRowWithcellData:@"banner"]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDJuediSearchTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDJuediSearchTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"搜索"] && indexPath.row == [self cellIndexPathRowWithcellData:@"搜索"]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDPubgInputTextFieldTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDPubgInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        tempInputTextField = cellWithRowTwo.inputTextField;
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo textFieldDidChangeBlock:^(NSString *str) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf calculationBtnStatus];
        }];
        
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"确定按钮"] && indexPath.row == [self cellIndexPathRowWithcellData:@"确定按钮"]){
       static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.backgroundColor = [UIColor whiteColor];
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"开始查询";
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf searchInfoManager];
        }];
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"空"] && indexPath.row == [self cellIndexPathRowWithcellData:@"空"]){
        static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
        UITableViewCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
        if (!cellWithRowSex){
            cellWithRowSex = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
            cellWithRowSex.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowSex;
    } else {
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            PDSearchHeaderSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowOne){
                cellWithRowOne = [[PDSearchHeaderSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            [cellWithRowOne zhanjiStatus:searchModel.has];
            tempCell = cellWithRowOne;
            return cellWithRowOne;
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"清空历史记录1566006123"] && indexPath.row == [self cellIndexPathRowWithcellData:@"清空历史记录1566006123"]){
            static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
            PDSearchCleanAllTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[PDSearchCleanAllTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            return cellWithRowFiv;
        } else {
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            PDSearchSingleTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[PDSearchSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
            cellWithRowFiv.transferUser = [[self.searchRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            __weak typeof(self)weakSelf = self;
            [cellWithRowFiv closeManager:^(NSString *closeManager) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf deleteManager:closeManager];
            }];
            return cellWithRowFiv;
        }
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"banner"] && indexPath.row == [self cellIndexPathRowWithcellData:@"banner"]){
       
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"搜索"] && indexPath.row == [self cellIndexPathRowWithcellData:@"搜索"]){
    
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"确定按钮"] && indexPath.row == [self cellIndexPathRowWithcellData:@"确定按钮"]){
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"清空历史记录1566006123"] && indexPath.row == [self cellIndexPathRowWithcellData:@"清空历史记录1566006123"]){
        [PDStupSQLiteManager pubgDeleteAll];
        // 3. 更新底部内容
        [self getCurrentArr];
        
        // 4. 刷新
        [self.searchTableView reloadData];
        
    } else {
        if (indexPath.row == 0){
            if (searchModel.has){           // 搜索
                [self webDirectToUrl:searchModel.gameUserName];
            } else {                        // 绑定
                PDJuediYuezhanBindingViewController *bindingViewController = [[PDJuediYuezhanBindingViewController alloc]init];
                __weak typeof(self)weakSelf = self;
                [bindingViewController juediBindingSuccessBlock:^{
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    strongSelf->searchModel.has = YES;
                    [strongSelf->tempCell zhanjiStatus:strongSelf->searchModel.has];
                }];
                [self.navigationController pushViewController:bindingViewController animated:YES];
            }
        } else {
            NSString *indexStr = [[self.searchRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            [self webDirectToUrl:indexStr];
        }
    }
    
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.searchTableView) {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"banner"] && indexPath.row == [self cellIndexPathRowWithcellData:@"banner"]){
            
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"搜索"] && indexPath.row == [self cellIndexPathRowWithcellData:@"搜索"]){
            
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"确定按钮"] && indexPath.row == [self cellIndexPathRowWithcellData:@"确定按钮"]){
        } else {
            SeparatorType separatorType = SeparatorTypeMiddle;
            if (indexPath.row > 0){
                if ( [indexPath row] == 1) {
                    separatorType = SeparatorTypeHead;
                } else if ([indexPath row] == [[self.searchRootMutableArr objectAtIndex:indexPath.section] count] - 1) {
                    separatorType = SeparatorTypeBottom;
                } else {
                    separatorType = SeparatorTypeMiddle;
                }
                if ([[self.searchRootMutableArr objectAtIndex:indexPath.section] count] == 1) {
                    separatorType = SeparatorTypeSingle;
                }
                [cell addSeparatorLineWithType:separatorType];
            }
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"banner"] && indexPath.row == [self cellIndexPathRowWithcellData:@"banner"]){
        return [PDJuediSearchTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"搜索"] && indexPath.row == [self cellIndexPathRowWithcellData:@"搜索"]){
        return [PDPubgInputTextFieldTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"确定按钮"] && indexPath.row == [self cellIndexPathRowWithcellData:@"确定按钮"]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"空"] && indexPath.row == [self cellIndexPathRowWithcellData:@"空"]){
        return LCFloat(34);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"清空历史记录1566006123"] && indexPath.row == [self cellIndexPathRowWithcellData:@"清空历史记录1566006123"]){
        return LCFloat(44);
    } else {
        if (indexPath.row == 0){
            return LCFloat(50);
        } else {
            return LCFloat(44);
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"banner"] ){
        return 0;
    } else if (section == [self cellIndexPathSectionWithcellData:@"搜索"] ){
        return 0;
    } else if (section == [self cellIndexPathSectionWithcellData:@"确定按钮"] ){
        return 0;
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - SQLite Manager
-(void)getCurrentArr{
    NSArray *pubgArr = [PDStupSQLiteManager pubgSearchListInfo];
    if (pubgArr.count){
        if (self.searchHistoryMutableArr.count){
            [self.searchHistoryMutableArr removeAllObjects];
        }
        [self.searchHistoryMutableArr addObject:@"查询历史！@#￥%……&*（）"];
        [self.searchHistoryMutableArr addObjectsFromArray:pubgArr];
        [self.searchHistoryMutableArr addObject:@"清空历史记录1566006123"];
    } else {
        if (self.searchHistoryMutableArr.count){
            [self.searchHistoryMutableArr removeAllObjects];
        }
        [self.searchHistoryMutableArr addObject:@"查询历史！@#￥%……&*（）"];
    }
}



#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.searchRootMutableArr.count ; i++){
        NSArray *dataTempArr = [self.searchRootMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isKindOfClass:[NSString class]]){
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.searchRootMutableArr.count ; i++){
        NSArray *dataTempArr = [self.searchRootMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isKindOfClass:[NSString class]]){
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}

-(void)calculationBtnStatus{
    GWButtonTableViewCell *buttonCell = (GWButtonTableViewCell *)[self.searchTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"确定按钮"] inSection:[self cellIndexPathSectionWithcellData:@"确定按钮"]]];
    if (tempInputTextField.text.length){
        [buttonCell setButtonStatus:YES];
    } else {
        [buttonCell setButtonStatus:NO];
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if ([tempInputTextField isFirstResponder]){
        [tempInputTextField resignFirstResponder];
    }
}

-(void)searchInfoManager{
    [tempInputTextField resignFirstResponder];
    
    // 1. 插入数据库
    [PDStupSQLiteManager pubgInsertStr:tempInputTextField.text];
    
    // 2. 页面跳转
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self webDirectToUrl:tempInputTextField.text];
        tempInputTextField.text = @"";
    });
    
    // 3. 更新底部内容
    [self getCurrentArr];
    
    // 4. 刷新
    [self.searchTableView reloadData];
    
    // 5. 更新按钮
    NSInteger section = [self cellIndexPathSectionWithcellData:@"确定按钮"];
    NSInteger row = [self cellIndexPathRowWithcellData:@"确定按钮"];
    GWButtonTableViewCell *cell = [self.searchTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];
    [cell setButtonStatus:NO];
}

-(void)deleteManager:(NSString *)user{
    // 1. 删除
    [PDStupSQLiteManager pubgDeleteStr:user];
    
    // 2.
    [self getCurrentArr];
    
    // 4. 刷新
    [self.searchTableView reloadData];
}

-(void)webDirectToUrl:(NSString *)url{
    PDWebViewController *webViewController = [[PDWebViewController alloc]init];

    NSString *mainUrl = [NSString stringWithFormat:@"http://%@:%@/po/pd_web/record.html?token=%@&playerName=%@",JAVA_Host,JAVA_Port,[AccountModel sharedAccountModel].token,url];
    [webViewController webDirectedWebUrl:mainUrl];
    [self.navigationController pushViewController:webViewController animated:YES];
}

#pragma mark - sendRequestToGetInfo
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:pubgcombatgameGameuser requestParams:nil responseObjectClass:[PDPubgSearchModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf->searchModel = (PDPubgSearchModel *)responseObject;
            if (strongSelf.searchTableView){
                if (strongSelf->tempCell){
                    NSIndexPath *mineIndexPath = [strongSelf.searchTableView indexPathForCell:strongSelf->tempCell];
                    [strongSelf.searchTableView reloadRowsAtIndexPaths:@[mineIndexPath] withRowAnimation:UITableViewRowAnimationNone];
                }
            }
        }
    }];
}

@end
