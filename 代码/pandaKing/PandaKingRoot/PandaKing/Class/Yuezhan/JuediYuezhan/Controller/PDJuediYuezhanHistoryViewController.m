//
//  PDJuediYuezhanHistoryViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanHistoryViewController.h"
#import "PDJuediYuezhanGameTableViewCell.h"
#import "PDJuediYuezhanHistoryRootModel.h"
#import "PDJuediYuezhanDetailRootViewController.h"

@interface PDJuediYuezhanHistoryViewController ()<UITableViewDataSource,UITableViewDelegate>{
    NSInteger page;
}
@property (nonatomic,strong)UITableView *historyTableView;
@property (nonatomic,strong)NSMutableArray *historyMutableArr;
@end

@implementation PDJuediYuezhanHistoryViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfoIsHornal:YES];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"对战历史";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    page = 1;
    self.historyMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.historyTableView){
        self.historyTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.historyTableView.dataSource = self;
        self.historyTableView.delegate = self;
        [self.view addSubview:self.historyTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.historyTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoIsHornal:YES];
    }];
    
    [self.historyTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoIsHornal:NO];
    }];
    
}

#pragma mark  - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.historyMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
    PDJuediYuezhanGameTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
    if (!cellWithRowOther){
        cellWithRowOther = [[PDJuediYuezhanGameTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
        cellWithRowOther.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOther.transferCellHeight = cellHeight;
    cellWithRowOther.transferMyCombatsModel = [self.historyMutableArr objectAtIndex:indexPath.row];
    return cellWithRowOther;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PDJuediYuezhanGameTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PDJuediYuezhanDetailRootViewController *detailViewController = [[PDJuediYuezhanDetailRootViewController alloc]init];
    PDJuediYuezhanRootMycombatsModel *myCombatsSingleModel = [self.historyMutableArr objectAtIndex:indexPath.row];
    detailViewController.combatId = myCombatsSingleModel.combatId;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(11);
}

#pragma mark - sendRequestToGetInfo
-(void)sendRequestToGetInfoIsHornal:(BOOL)hornal{
    __weak typeof(self)weakSelf = self;
    if (hornal){
        page = 1;
    }
    NSDictionary *params = @{@"pageNum":@(page)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:pubgcombatgameList requestParams:params responseObjectClass:[PDJuediYuezhanHistoryRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDJuediYuezhanHistoryRootModel *singleModel = (PDJuediYuezhanHistoryRootModel *)responseObject;
            if (hornal){
                [strongSelf.historyMutableArr removeAllObjects];
                [strongSelf.historyMutableArr addObjectsFromArray:singleModel.items];
            } else {
                [strongSelf.historyMutableArr addObjectsFromArray:singleModel.items];
            }
            strongSelf->page++;
            [strongSelf.historyTableView reloadData];
            if (strongSelf.historyMutableArr.count){
                [strongSelf.historyTableView dismissPrompt];
            } else {
                [strongSelf.historyTableView showPrompt:@"没有约战历史" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
            
            [strongSelf.historyTableView stopFinishScrollingRefresh];
            [strongSelf.historyTableView stopPullToRefresh];
        }
    }];
}


@end
