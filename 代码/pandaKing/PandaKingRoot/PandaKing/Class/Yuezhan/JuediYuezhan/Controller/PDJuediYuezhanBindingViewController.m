//
//  PDJuediYuezhanBindingViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanBindingViewController.h"
#import "PDJuediYuezhanBindingTishiTableViewCell.h"
#import "PDJuediYuezhanBindingImgTableViewCell.h"

static char juediBindingSuccessBlockKey;
@interface PDJuediYuezhanBindingViewController ()<UITableViewDataSource,UITableViewDelegate>{
    UITextField *inputTextField;
}
@property (nonatomic,strong)UITableView *juediTableView;
@property (nonatomic,strong)NSArray *juediArr;
@property (nonatomic,strong)UITapGestureRecognizer *tapGesture;
@end

@implementation PDJuediYuezhanBindingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self.view addGestureRecognizer:self.tapGesture];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"绑定ID";
    self.view.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
}


#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.juediArr = @[@[@"图片"],@[@"输入"],@[@"立即绑定"],@[@"其他"]];
    
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.juediTableView){
        self.juediTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.juediTableView.dataSource = self;
        self.juediTableView.delegate = self;
        [self.view addSubview:self.juediTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.juediArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.juediArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDJuediYuezhanBindingImgTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDJuediYuezhanBindingImgTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        return cellWithRowTwo;
    } else if (indexPath.section == 1){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWInputTextFieldTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transfrCellHeight = cellHeight;
        cellWithRowThr.transferPlaceholeder = @"绝地求生：大逃杀ID（非steamID）";
        inputTextField = cellWithRowThr.inputTextField;
        cellWithRowThr.backgroundColor = [UIColor clearColor];
        inputTextField.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
        
        return cellWithRowThr;
    } else if (indexPath.section == 2){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        GWButtonTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferTitle = @"立即绑定";
        cellWithRowFour.backgroundColor = [UIColor clearColor];
        [cellWithRowFour setButtonStatus:YES];
        __weak typeof(self)weakSelf = self;
        [cellWithRowFour buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToBindingManager];
        }];
        cellWithRowFour.button.backgroundColor = RGB(238, 173, 60, 1);
        return cellWithRowFour;
    } else if (indexPath.section == 3){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        PDJuediYuezhanBindingTishiTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[PDJuediYuezhanBindingTishiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFiv.transferCellHeight = cellHeight;
        return cellWithRowFiv;
    } else {
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        UITableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithRowOther){
            cellWithRowOther = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
            cellWithRowOther.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowOther;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [PDJuediYuezhanBindingImgTableViewCell calculationCellHeight];
    } else if (indexPath.section == 1){
        return [GWInputTextFieldTableViewCell calculationCellHeight];
    } else if (indexPath.section == 2){
        return [GWButtonTableViewCell calculationCellHeight];
    } else if (indexPath.section == 3){
        return [PDJuediYuezhanBindingTishiTableViewCell calculationCellHeight];
    } else {
        return LCFloat(44);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(11);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - Interface
-(void)sendRequestToBindingManager{
    if ([inputTextField isFirstResponder]){
        [inputTextField resignFirstResponder];
    }
    
    if (!inputTextField.text.length){
        [PDHUD showConnectionErr:@"请输入绝地求生：大逃杀ID"];
        return;
    }
    NSDictionary *params = @{@"name":inputTextField.text};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:creategameuser requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"绑定成功" conten:@"您已成功绑定绝地求生，开始战斗吧!" isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
               [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
                   void(^block)() = objc_getAssociatedObject(strongSelf, &juediBindingSuccessBlockKey);
                   if (block){
                       block();
                   }
                   [strongSelf.navigationController popViewControllerAnimated:YES];
               }];
            }];
        }
    }];
}

-(void)juediBindingSuccessBlock:(void(^)())block{
    objc_setAssociatedObject(self, &juediBindingSuccessBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)tapManager{
    if ([inputTextField isFirstResponder]){
        [inputTextField resignFirstResponder];
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    [self tapManager];
}
@end
