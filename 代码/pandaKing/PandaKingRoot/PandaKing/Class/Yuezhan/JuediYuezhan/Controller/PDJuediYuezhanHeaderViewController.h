//
//  PDJuediYuezhanHeaderViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDJuediYuezhanDetaillCombatSingleModel.h"
@protocol PDJuediYuezhanHeaderViewControllerDelegate <NSObject>

-(void)readyButtonClick;
-(void)tapManager;
@end

@interface PDJuediYuezhanHeaderViewController : AbstractViewController

@property (nonatomic,weak)id<PDJuediYuezhanHeaderViewControllerDelegate> delegate;

-(void)viewsLoadWithModel:(PDJuediYuezhanDetaillCombatSingleModel *)singleModel;

-(void)timerChange:(NSInteger)time;

+(CGFloat)calculationControllerHeight;

// 加入约战
-(void)socketJiaruyuezhanManager:(PDJuediYuezhanSocketJoinGameModel *)singleModel;
// 配置房间
-(void)socketPeizhifangjianManager:(PDJuediYuezhanSocketJoinGameModel *)singleModel;
// 约战开始
-(void)socketYuezhankaishiManager:(PDJuediYuezhanSocketJoinGameModel *)singleModel;
// 约战结算
-(void)socketYuezhanjiesuanManager:(PDJuediYuezhanSocketJoinGameModel *)singleModel;
@end
