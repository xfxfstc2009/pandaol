//
//  PDJuediYuezhanRootViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanRootViewController.h"
#import "PDJuediYuezhanHeaderViewController.h"
#import "PDJuediYuezhanRootModel.h"
#import "PDJuediYuezhanHistoryViewController.h"
#import "PDJuediYuezhanSaishiTableViewCell.h"
#import "PDJuediYuezhanDetailRootViewController.h"
#import "PDJuediYuezhanBindingViewController.h"
#import "PDJuediYuezhanDetailRootModel.h"
#import "PDJuediYuezhanGameTableViewCell.h"
#import "PDTopUpViewController.h"

@interface PDJuediYuezhanRootViewController ()<UITableViewDataSource,UITableViewDelegate>{
    PDJuediYuezhanRootModel *juediRootsingleModel;
}
@property (nonatomic,strong)NSMutableArray *juediMutableArr;
@property (nonatomic,strong)UITableView *juediTableView;

@end

@implementation PDJuediYuezhanRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];

}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self sendRequestTogetYuezhanRootInfo];
}

#pragma mark - createView
-(void)pageSetting{
    self.barMainTitle = @"绝地逃生约战";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_yuezhan_root_history"] barHltImage:[UIImage imageNamed:@"icon_yuezhan_root_history"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDJuediYuezhanHistoryViewController *historyViewController = [[PDJuediYuezhanHistoryViewController alloc]init];
        [strongSelf.navigationController pushViewController:historyViewController animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.juediMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.juediTableView){
        self.juediTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.juediTableView.dataSource = self;
        self.juediTableView.delegate = self;
        [self.view addSubview:self.juediTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.juediMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.juediMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"绑定账号"] && indexPath.row == [self cellIndexPathRowWithcellData:@"绑定账号"]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferIcon = [UIImage imageNamed:@"icon_pubg_icondataosha"];
        if (juediRootsingleModel.bindGameUser){              // 已经绑定
            cellWithRowOne.transferTitle = juediRootsingleModel.gameUserName;
            cellWithRowOne.transferDesc = [NSString stringWithFormat:@"我的金币：%li",juediRootsingleModel.mygold];
            cellWithRowOne.transferHasArrow = NO;
        } else {
            cellWithRowOne.transferTitle = @"立即绑定";
            cellWithRowOne.transferDesc = @"";
            cellWithRowOne.transferHasArrow = YES;
        }

        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"游戏区服"]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDJuediYuezhanSaishiTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDJuediYuezhanSaishiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        if (indexPath.row == 0){
            cellWithRowTwo.transferJuedisaishiType = juediYuezhanSaishiTypeTitle;
        } else {
            cellWithRowTwo.transferJuedisaishiType = juediYuezhanSaishiTypeNormal;
            cellWithRowTwo.transferSingleModel = [[self.juediMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        }
        return cellWithRowTwo;
    } else {
        static NSString *cellIdentifyWithRowOther = @"cellIdentifyWithRowOther";
        PDJuediYuezhanGameTableViewCell *cellWithRowOther = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOther];
        if (!cellWithRowOther){
            cellWithRowOther = [[PDJuediYuezhanGameTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOther];
            cellWithRowOther.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOther.transferCellHeight = cellHeight;
        cellWithRowOther.transferMyCombatsModel = [[self.juediMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        return cellWithRowOther;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    __weak typeof(self)weakSelf = self;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"游戏区服"]){
        if(indexPath.row != 0){
            if (!juediRootsingleModel.bindGameUser){
                [[PDAlertView sharedAlertView] showAlertWithTitle:@"加入房间错误" conten:@"请先绑定您绝地求生账户" isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
                        PDJuediYuezhanBindingViewController *bindingViewController = [[PDJuediYuezhanBindingViewController alloc]init];
                        [bindingViewController juediBindingSuccessBlock:^{
                            strongSelf->juediRootsingleModel.bindGameUser = YES;
                        }];
                        [self.navigationController pushViewController:bindingViewController animated:YES];
                    }];
                }];
                return;
            }
            
            PDJuediYuezhanRootCombatsSingleModel *juediSingleModel = [[self.juediMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            
            if (juediSingleModel.joined){
                [self sendRequestToJoinYuezhanWithCombatId:juediSingleModel.combatId];
            } else {
                if ([juediSingleModel.state isEqualToString:@"readying"]){
                    if (juediSingleModel.gold > [AccountModel sharedAccountModel].memberInfo.gold){
                        NSString *content = [NSString stringWithFormat:@"金币不足%li无法加入战场，请充值",juediSingleModel.gold];
                        [[PDAlertView sharedAlertView] showAlertWithTitle:@"提示" conten:content isClose:NO btnArr:@[@"确定",@"取消"] buttonClick:^(NSInteger buttonIndex) {
                            if (!weakSelf){
                                return ;
                            }
                            __strong typeof(weakSelf)strongSelf = weakSelf;
                            [[PDAlertView sharedAlertView]dismissAllWithBlock:^{
                                if (buttonIndex == 0){
                                    PDTopUpViewController *topUpViewController = [[PDTopUpViewController alloc] init];
                                    [strongSelf.navigationController pushViewController:topUpViewController animated:YES];
                                }
                            }];
                        }];
                    }
                    
                    if (juediSingleModel.maxJoinCount <= juediSingleModel.joinMemberCount){
                        [[PDAlertView sharedAlertView] showAlertWithTitle:@"加入房间错误" conten:@"人数已满，请选择其他战斗场" isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                            [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
                        }];
                        return;
                    }
                    
                    NSString *content = [NSString stringWithFormat:@"入场需要%li金币入场费",juediSingleModel.gold];
                    __weak typeof(self)weakSelf = self;
                    [[PDAlertView sharedAlertView] showAlertWithTitle:@"提示" conten:content isClose:YES btnArr:@[@"确定",@"取消"] buttonClick:^(NSInteger buttonIndex) {
                        if (!weakSelf){
                            return ;
                        }
                        __strong typeof(weakSelf)strongSelf = weakSelf;
                        [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
                            if (buttonIndex == 0){
                                [strongSelf sendRequestToJoinYuezhanWithCombatId:juediSingleModel.combatId];
                            }
                        }];
                    }];
                } else {
                    [PDHUD showConnectionErr:@"该赛事已经开场，敬请参加下一场比赛"];
                }
            }
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"绑定账号"]){
        if (!juediRootsingleModel.bindGameUser){
            PDJuediYuezhanBindingViewController *bindingViewController = [[PDJuediYuezhanBindingViewController alloc]init];
            [self.navigationController pushViewController:bindingViewController animated:YES];
        }
    } else {
        PDJuediYuezhanDetailRootViewController *detailViewController = [[PDJuediYuezhanDetailRootViewController alloc]init];
        PDJuediYuezhanRootMycombatsModel *myCombatsSingleModel = [[self.juediMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        detailViewController.combatId = myCombatsSingleModel.combatId;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(15);
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"绑定账号"]){
        return LCFloat(50);
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"游戏区服"]){
        return [PDJuediYuezhanSaishiTableViewCell calculationCellHeight];
    } else {
        return [PDJuediYuezhanGameTableViewCell calculationCellHeight];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.juediTableView) {
        NSInteger section1 = [self cellIndexPathSectionWithcellData:@"游戏区服"];
        NSInteger section2 = [self cellIndexPathSectionWithcellData:@"绑定账号"];
        if (indexPath.section == section1 || indexPath.section == section2){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType = SeparatorTypeHead;
            } else if ([indexPath row] == [[self.juediMutableArr objectAtIndex:indexPath.section] count] - 1) {
                separatorType = SeparatorTypeBottom;
            } else {
                separatorType = SeparatorTypeMiddle;
            }
            if ([[self.juediMutableArr objectAtIndex:indexPath.section] count] == 1) {
                separatorType = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - 创建约战首页
-(void)sendRequestTogetYuezhanRootInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:pubgcombatgame requestParams:nil responseObjectClass:[PDJuediYuezhanRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf->juediRootsingleModel = (PDJuediYuezhanRootModel *)responseObject;
            
            if (strongSelf.juediMutableArr.count){
                [strongSelf.juediMutableArr removeAllObjects];
            }
            
            [strongSelf.juediMutableArr addObject:@[@"绑定账号"]];
            
            if (strongSelf->juediRootsingleModel.mycombats.count){
                [strongSelf.juediMutableArr addObject:strongSelf->juediRootsingleModel.mycombats];
            }
            
            NSMutableArray *infoMutableArr = [NSMutableArray array];
            [infoMutableArr addObject:@"游戏区服"];
            [infoMutableArr addObjectsFromArray:strongSelf->juediRootsingleModel.combats];
            
            [strongSelf.juediMutableArr addObject:infoMutableArr];
            [strongSelf.juediTableView reloadData];
        }
    }];
}



#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.juediMutableArr.count ; i++){
        NSArray *dataTempArr = [self.juediMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isKindOfClass:[NSString class]]){
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.juediMutableArr.count ; i++){
        NSArray *dataTempArr = [self.juediMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isKindOfClass:[NSString class]]){
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}

#pragma mark - 加入约战
-(void)sendRequestToJoinYuezhanWithCombatId:(NSString *)combatId{
    __weak typeof(self)weakSelf = self;
    if (!combatId.length){
        [PDHUD showConnectionErr:@"该场比赛发生错误，请稍后在试"];
    }
    
    NSDictionary *params = @{@"combatId":combatId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:pubgcombatgameJoin requestParams:params responseObjectClass:[PDJuediYuezhanDetailRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDJuediYuezhanDetailRootModel *yuezhanDetailModel = (PDJuediYuezhanDetailRootModel *)responseObject;
            
            
            // 计算真正的目标时间
            // 1. 计算当前时间
            NSTimeInterval phone_currentTime = [NSDate getNSTimeIntervalWithCurrent];
            NSInteger shijiancha = yuezhanDetailModel.combat.startTime - yuezhanDetailModel.combat.currentTime;
            NSTimeInterval phone_targetTime = phone_currentTime + shijiancha;
            yuezhanDetailModel.combat.tempTargetTimeInterval = phone_targetTime;
            
            
            PDJuediYuezhanDetailRootViewController *detailViewController = [[PDJuediYuezhanDetailRootViewController alloc]init];
            detailViewController.transferDetailModel = yuezhanDetailModel;
            [strongSelf.navigationController pushViewController:detailViewController animated:YES];
        }
    }];
}


@end
