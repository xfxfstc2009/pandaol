//
//  PDJuediYuezhanDetailRootViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDJuediYuezhanDetailRootModel.h"

@interface PDJuediYuezhanDetailRootViewController : AbstractViewController

@property (nonatomic,strong)PDJuediYuezhanDetailRootModel *transferDetailModel;
@property (nonatomic,copy)NSString *combatId;

@end
