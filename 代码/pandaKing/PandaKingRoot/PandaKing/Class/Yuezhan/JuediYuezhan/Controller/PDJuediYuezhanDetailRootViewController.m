//
//  PDJuediYuezhanDetailRootViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/7/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDJuediYuezhanDetailRootViewController.h"
#import "PDJuediYuezhanHeaderViewController.h"
#import "PDJuediYuezhanDetailRootModel.h"

@interface PDJuediYuezhanDetailRootViewController ()<PDNetworkAdapterDelegate,PDJuediYuezhanHeaderViewControllerDelegate>
@property (nonatomic,strong)PDJuediYuezhanHeaderViewController *headerViewController;
@property (nonatomic,strong)PDEMMessageViewController *chatRoomViewController;
@property (nonatomic,strong)NSTimer *timer;
@property (nonatomic,assign)NSInteger timerIndex;

@end

@implementation PDJuediYuezhanDetailRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createRootView];
    [self createHeaderViewController];
    
    if (!self.transferDetailModel){
        [self sendRequestToGetYuezhanDetail];
    } else {
        // 1. 视图更新
        [self infoManagerWithModel:self.transferDetailModel.combat];
        // 2. 链接IM
        [self createIMControllerWithIMId:self.transferDetailModel.combat.groupId];
    }
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"";
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([NetworkAdapter sharedAdapter]){
        [NetworkAdapter sharedAdapter].delegate = self;
    }
}

#pragma mark - 主页面
-(void)createRootView{
    
}

#pragma mark - 1.创建头部信息
-(void)createHeaderViewController{
    self.headerViewController = [[PDJuediYuezhanHeaderViewController alloc]init];
    self.headerViewController.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, [PDJuediYuezhanHeaderViewController calculationControllerHeight]);
    [self.view addSubview:self.headerViewController.view];
    [self addChildViewController:self.headerViewController];
    self.headerViewController.hasCancelSocket = YES;
    self.headerViewController.delegate = self;
}

-(void)createIMControllerWithIMId:(NSString *)imId{
    if(!self.chatRoomViewController){
        if (![[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            return;
        }
        self.chatRoomViewController = [[PDEMManager sharedInstance] conversationWithChatroom:imId];
        if(self.chatRoomViewController == nil){
            return;
        }
        
        self.chatRoomViewController.view.backgroundColor = [UIColor clearColor];
        self.chatRoomViewController.view.frame = CGRectMake(0, self.headerViewController.view.size_height, kScreenBounds.size.width, kScreenBounds.size.height - self.headerViewController.view.size_height);
        [self addChildViewController:self.chatRoomViewController];
        [self.view addSubview:self.chatRoomViewController.view];
    }
}

-(void)infoManagerWithModel:(PDJuediYuezhanDetaillCombatSingleModel *)detailSingleModel{
    [self.headerViewController viewsLoadWithModel:detailSingleModel];
    
    // 判断时间
    if ([detailSingleModel.state isEqualToString:@"readying"]){         // 准备中
        [self startTime];
    } else if ([detailSingleModel.state isEqualToString:@"waitOver"]){          // 结算中
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    } else if ([detailSingleModel.state isEqualToString:@"settled"]){           // 已经结算
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    } else if ([detailSingleModel.state isEqualToString:@"canceled"]){          // 已取消
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
    }
}

#pragma mark - 约战详情
-(void)sendRequestToGetYuezhanDetail{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"combatId":self.combatId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:pubgcombatgameDetail requestParams:params responseObjectClass:[PDJuediYuezhanDetailRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf.transferDetailModel = (PDJuediYuezhanDetailRootModel *)responseObject;
            
            // 计算真正的目标时间
            // 1. 计算当前时间
            NSTimeInterval phone_currentTime = [NSDate getNSTimeIntervalWithCurrent];
            NSInteger shijiancha = strongSelf.transferDetailModel.combat.startTime - strongSelf.transferDetailModel.combat.currentTime;
            NSTimeInterval phone_targetTime = phone_currentTime + shijiancha;
            strongSelf.transferDetailModel.combat.tempTargetTimeInterval = phone_targetTime;
                        
            // 1. 视图更新
            [strongSelf infoManagerWithModel:strongSelf.transferDetailModel.combat];
            // 2. IM
            [strongSelf createIMControllerWithIMId:strongSelf.transferDetailModel.combat.groupId];
        }
    }];
}


#pragma mark - 比赛开始
#pragma mark - startTime
-(void)startTime{
    if (!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
        NSRunLoop *currentRunLoop = [NSRunLoop currentRunLoop];
        [currentRunLoop addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    NSInteger time = (self.transferDetailModel.combat.startTime - self.transferDetailModel.combat.currentTime) / 1000.;
    self.timerIndex = time;
}

-(void)timeChange {
    self.timerIndex--;
    if (self.timerIndex <= 0){              // 解散
        if (self.timer){
            [self.timer invalidate];
            self.timer = nil;
        }
        [self.headerViewController timerChange:self.timerIndex];
        return;
    }
    [self.headerViewController timerChange:self.timerIndex];
}

-(void)tapManager{
    [self.chatRoomViewController releaseKeyBoard];
}

-(void)readyButtonClick{
    
}


#pragma mark - Socket 加入房间
-(void)socketDidBackData:(id)responseObject{
    if ([responseObject isKindOfClass:[NSDictionary class]]){
        NSDictionary *dic = (NSDictionary *)responseObject;
        PDBaseSocketRootModel *baseSocket = [[PDBaseSocketRootModel alloc] initWithJSONDict:[dic objectForKey:@"data"]];
        if(baseSocket.type == socketTypePubgCombatJoin){                // 加入约战
            [self.headerViewController socketJiaruyuezhanManager:baseSocket.pubgCombat];
        } else if (baseSocket.type == socketTypePubgCombatConfigRoom){
            [self.headerViewController socketPeizhifangjianManager:baseSocket.pubgCombat];
        } else if (baseSocket.type == socketTypePubgCombatStarted){
            [self timeStop];
            [self.headerViewController socketYuezhankaishiManager:baseSocket.pubgCombat];
        } else if (baseSocket.type == socketTypePubgCombatSettled){
            [self timeStop];
            [self.headerViewController socketYuezhanjiesuanManager:baseSocket.pubgCombat];
        }
    }
}

-(void)timeStop{
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}
@end
