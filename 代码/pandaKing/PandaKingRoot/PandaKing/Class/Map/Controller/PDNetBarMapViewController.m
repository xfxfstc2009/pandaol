//
//  PDNetBarMapViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNetBarMapViewController.h"
#import "PlusingAnnotationView.h"                                   // 大头指针【当前定位地址】
#import "PDNetBarAnnotation.h"

#define kCalloutViewMargin          -8

@interface PDNetBarMapViewController()<UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIPanGestureRecognizer *panGest;              /**< 拖动手势*/

@end

@implementation PDNetBarMapViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self mapBaseSetup];
    [self addGestureRecognizer];                                            // 添加手势
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self addlocation];
}

-(void)mapBaseSetup{
    [self changeMapCompassShow:NO];                                         // 1. 不显示罗盘
    [self changeMapScaleShow:NO];                                           // 1. 不显示比例尺
    [self changeMapTrafficType:NO];                                         // 1. 显示当前交通情况
    [self changeMapTypeWithType:MAMapTypeStandard];                         // 2. 显示当前普通地图
    [self showLocationWithStatus:YES];                                      // 3. 显示当前定位地址
    [self showLocationWithType:MAUserTrackingModeFollow];                   // 3.1 当前定位模式
    
}

#pragma mark - 显示罗盘
-(void)changeMapCompassShow:(BOOL)isShow{
    self.mapView.showsCompass = isShow;
}

#pragma mark - 显示比例尺
-(void)changeMapScaleShow:(BOOL)isShow{
    self.mapView.showsScale = isShow;
}

#pragma mark - 修改地图类型【标准，卫星，黑夜】
-(void)changeMapTypeWithType:(MAMapType)mapType{
    self.mapView.mapType = mapType;
}

#pragma mark - 修改是否交通
-(void)changeMapTrafficType:(BOOL)show{
    self.mapView.showTraffic = show;
}

#pragma mark - 显示当前定位地址
#pragma mark 定位状态【打开\关闭】
-(void)showLocationWithStatus:(BOOL)isOpen{
    self.mapView.showsUserLocation = isOpen;
}

#pragma mark 定位模式
-(void)showLocationWithType:(MAUserTrackingMode)type{
    [self.mapView setUserTrackingMode:type animated:YES];
    self.mapView.customizeUserLocationAccuracyCircleRepresentation = YES;           // 去除周围的光圈
}

#pragma mark - 获取当前的大头指针
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation {
    if ([annotation isKindOfClass:[MAUserLocation class]]){         // 【当前的定位地址】
        static NSString *annotationIdentifyWithOne = @"annotationIdentifyWithOne";
        PlusingAnnotationView *annotaionView = (PlusingAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifyWithOne];
        if (!annotaionView){
            annotaionView = [[PlusingAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifyWithOne];
        }
        return annotaionView;
    } else if ([annotation isKindOfClass:[MAPointAnnotation class]]){
        static NSString *annotationIdentifyWithTwo = @"annotationIdentifyWithTwo";
        PDNetBarAnnotation *netBarAnnotation = (PDNetBarAnnotation *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifyWithTwo];
        if (!netBarAnnotation){
            netBarAnnotation = [[PDNetBarAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifyWithTwo];
        }
        // must set to NO, so we can show the custom callout view.
        netBarAnnotation.canShowCallout   = NO;
        netBarAnnotation.draggable        = YES;
        netBarAnnotation.calloutOffset    = CGPointMake(0, -5);
        
        netBarAnnotation.portrait         = [UIImage imageNamed:@"hema.png"];
        netBarAnnotation.name             = @"河马";
        
        return netBarAnnotation;
    }
    return nil;
}

#pragma mark- 大头针点击方法
- (void)mapView:(MAMapView *)mapView didSelectAnnotationView:(MAAnnotationView *)view {
    /* Adjust the map center in order to show the callout view completely. */
    if ([view isKindOfClass:[PlusingAnnotationView class]]) {
        PlusingAnnotationView *cusView = (PlusingAnnotationView *)view;
        CGRect frame = [cusView convertRect:cusView.calloutView.frame toView:self.mapView];
        
        frame = UIEdgeInsetsInsetRect(frame, UIEdgeInsetsMake(kCalloutViewMargin, kCalloutViewMargin, kCalloutViewMargin, kCalloutViewMargin));
        
        if (!CGRectContainsRect(self.mapView.frame, frame))
        {
            /* Calculate the offset to make the callout view show up. */
            CGSize offset = [self offsetToContainRect:frame inRect:self.mapView.frame];
            
            CGPoint screenAnchor = [self.mapView getMapStatus].screenAnchor;
            CGPoint theCenter = CGPointMake(self.mapView.bounds.size.width * screenAnchor.x, self.mapView.bounds.size.height * screenAnchor.y);
            theCenter = CGPointMake(theCenter.x - offset.width, theCenter.y - offset.height);
            
            CLLocationCoordinate2D coordinate = [self.mapView convertPoint:theCenter toCoordinateFromView:self.mapView];
            
            [self.mapView setCenterCoordinate:coordinate animated:YES];
        }
    } else if ([view isKindOfClass:[PDNetBarAnnotation class]]){
        PlusingAnnotationView *cusView = (PlusingAnnotationView *)view;
        CGRect frame = [cusView convertRect:cusView.calloutView.frame toView:self.mapView];
        
        frame = UIEdgeInsetsInsetRect(frame, UIEdgeInsetsMake(kCalloutViewMargin, kCalloutViewMargin, kCalloutViewMargin, kCalloutViewMargin));
        
        if (!CGRectContainsRect(self.mapView.frame, frame))
        {
            /* Calculate the offset to make the callout view show up. */
            CGSize offset = [self offsetToContainRect:frame inRect:self.mapView.frame];
            
            CGPoint screenAnchor = [self.mapView getMapStatus].screenAnchor;
            CGPoint theCenter = CGPointMake(self.mapView.bounds.size.width * screenAnchor.x, self.mapView.bounds.size.height * screenAnchor.y);
            theCenter = CGPointMake(theCenter.x - offset.width, theCenter.y - offset.height);
            
            CLLocationCoordinate2D coordinate = [self.mapView convertPoint:theCenter toCoordinateFromView:self.mapView];
            
            [self.mapView setCenterCoordinate:coordinate animated:YES];
        }
    }
}

- (CGSize)offsetToContainRect:(CGRect)innerRect inRect:(CGRect)outerRect
{
    CGFloat nudgeRight = fmaxf(0, CGRectGetMinX(outerRect) - (CGRectGetMinX(innerRect)));
    CGFloat nudgeLeft = fminf(0, CGRectGetMaxX(outerRect) - (CGRectGetMaxX(innerRect)));
    CGFloat nudgeTop = fmaxf(0, CGRectGetMinY(outerRect) - (CGRectGetMinY(innerRect)));
    CGFloat nudgeBottom = fminf(0, CGRectGetMaxY(outerRect) - (CGRectGetMaxY(innerRect)));
    return CGSizeMake(nudgeLeft ?: nudgeRight, nudgeTop ?: nudgeBottom);
}




#pragma mark - AMapSearchDelegate
- (void)searchPoiWithType:(AMapPOISearchType)searchType keyword:(NSString *)key{
    [self.mapView removeAnnotations:self.mapView.annotations];                      /* 清除存在的annotation. */
    
    if (searchType == AMapPOISearchTypeAround){                 // 根据周围索索
        [self searchPoiByCenterCoordinateWithKey:key];
    } else if (searchType == AMapPOISearchTypeKeywords){    // 根据关键字搜索
        [self searchPoiByKeyword:key];
    }
}

// 关键字搜索
-(void)searchPoiByKeyword:(NSString *)key{
    AMapPOIKeywordsSearchRequest *request = [[AMapPOIKeywordsSearchRequest alloc] init];
    request.keywords            = key;
    request.city                = @"北京";
    request.requireExtension    = YES;
    request.cityLimit           = YES;
    request.requireSubPOIs      = YES;
    [self.search AMapPOIKeywordsSearch:request];
}

// 进行周围搜索
- (void)searchPoiByCenterCoordinateWithKey:(NSString *)key{
    AMapPOIAroundSearchRequest *request = [[AMapPOIAroundSearchRequest alloc] init];
    
    request.location            = [AMapGeoPoint locationWithLatitude:39.990459 longitude:116.481476];
    request.keywords            = key;
    
    request.sortrule            = 0;
    request.requireExtension    = YES;
    
    [self.search AMapPOIAroundSearch:request];
}


#pragma mark POI 搜索回调

- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response {
    if (response.pois.count == 0) {
        return;
    }
    NSMutableArray *poiAnnotations = [NSMutableArray arrayWithCapacity:response.pois.count];
    [response.pois enumerateObjectsUsingBlock:^(AMapPOI *obj, NSUInteger idx, BOOL *stop) {
        MAPointAnnotation *pointAnnotation = [[MAPointAnnotation alloc]init];
        CLLocationCoordinate2D coordinate;
        coordinate.latitude = obj.location.latitude;
        coordinate.longitude = obj.location.longitude;
        pointAnnotation.coordinate = coordinate;
        [poiAnnotations addObject:pointAnnotation];
    }];
    
    /* 将结果以annotation的形式加载到地图上. */
    [self.mapView addAnnotations:poiAnnotations];
    
    /* 如果只有一个结果，设置其为中心点. */
    if (poiAnnotations.count == 1)
    {
        [self.mapView setCenterCoordinate:[poiAnnotations[0] coordinate]];
    }
    /* 如果有多个结果, 设置地图使所有的annotation都可见. */
    else
    {
        [self.mapView showAnnotations:poiAnnotations animated:NO];
    }
}


#pragma mark - Other Manager
-(void)addGestureRecognizer{
    self.panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizedWithMap:)];
    self.panGest.delegate = self;
    [self.view addGestureRecognizer:self.panGest];
}

- (void)panGestureRecognizedWithMap:(UIPanGestureRecognizer *)recognizer{
    [[RESideMenu shareInstance] panGestureRecognized:recognizer];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (gestureRecognizer == self.panGest && [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]){
        CGPoint point = [touch locationInView:gestureRecognizer.view];
        if (point.x < 50.0 || point.x > self.view.frame.size.width - 50.0) {
            self.mapView.scrollEnabled = NO;
            return YES;
        } else {
            self.mapView.scrollEnabled = YES;
            return NO;
        }
    }
    
    return YES;
}


#pragma mark test
-(void)addlocation{
    MAPointAnnotation *annotation = [[MAPointAnnotation alloc] init];
    CLLocationCoordinate2D location;
    location.latitude = 30.188449;
    location.longitude = 120.289899;
    annotation.coordinate = location;
    annotation.title    = @"我是网吧";
    annotation.subtitle = @"我是网吧哈哈哈哈";
    
    [self.mapView addAnnotation:annotation];
}
@end
