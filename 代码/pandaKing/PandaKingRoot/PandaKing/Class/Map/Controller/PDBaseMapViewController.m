//
//  PDBaseMapViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBaseMapViewController.h"

@interface PDBaseMapViewController()<MAMapViewDelegate, AMapSearchDelegate>
@property (nonatomic, assign) BOOL isFirstAppear;


@end

@implementation PDBaseMapViewController

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (_isFirstAppear) {
        _isFirstAppear = NO;
        
    }
}

-(void)viewDidLoad{
    [super viewDidLoad];
    _isFirstAppear = YES;
    [self mapWithInit];
}

#pragma mark - MapWithInit
-(void)mapWithInit{
    [self initMapView];                         // 创建地图
    [self initSearch];                          // 创建搜索
}
- (void)initSearch {
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
}

- (void)initMapView {
    self.mapView = [[MAMapView alloc]initWithFrame:self.view.bounds];
    self.mapView.delegate = self;
    [self.view addSubview:self.mapView];
}


#pragma mark - clear
- (void)clearMapView {
    self.mapView.showsUserLocation = NO;
    [self.mapView removeAnnotations:self.mapView.annotations];
    [self.mapView removeOverlays:self.mapView.overlays];
    self.mapView.delegate = nil;
}

- (void)clearSearch {
    self.search.delegate = nil;
}


@end
