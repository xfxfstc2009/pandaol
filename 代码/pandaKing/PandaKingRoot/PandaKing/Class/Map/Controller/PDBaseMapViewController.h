//
//  PDBaseMapViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import <MAMapKit/MAMapKit.h>
#import <AMapSearchKit/AMapSearchKit.h>

typedef NS_ENUM(NSInteger,AMapPOISearchType) {
    AMapPOISearchTypeKeywords,                      /**< 根据关键字搜索*/
    AMapPOISearchTypeAround,                        /**< 搜索周边*/
};

@interface PDBaseMapViewController : AbstractViewController

@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) AMapSearchAPI *search;


@end
