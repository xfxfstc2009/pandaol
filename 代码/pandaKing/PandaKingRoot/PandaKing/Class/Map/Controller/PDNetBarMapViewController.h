//
//  PDNetBarMapViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBaseMapViewController.h"

@interface PDNetBarMapViewController : PDBaseMapViewController

#pragma mark - 修改地图类型【标准，卫星，黑夜】
-(void)changeMapTypeWithType:(MAMapType)mapType;
#pragma mark - 修改是否交通
-(void)changeMapTrafficType:(BOOL)show;

#pragma mark - POI
// 关键字搜索
- (void)searchPoiWithType:(AMapPOISearchType)searchType keyword:(NSString *)key;



@end
