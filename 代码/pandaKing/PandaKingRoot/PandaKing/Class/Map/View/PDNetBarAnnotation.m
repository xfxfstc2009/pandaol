//
//  PDNetBarAnnotation.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNetBarAnnotation.h"
#import "PDCustomCalloutView.h"
#import <objc/runtime.h>

#define kWidth  60.f
#define kHeight 60.f

#define kHoriMargin 5.f
#define kVertMargin 5.f

#define kPortraitWidth  50.f
#define kPortraitHeight 50.f

#define kCalloutWidth   250.0                       // 气泡宽度
#define kCalloutHeight  70.0

static char locationKey;
@interface PDNetBarAnnotation()

@property (nonatomic, strong) UIImageView *portraitImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic,strong)PDImageView *iconImageView;

@end

@implementation PDNetBarAnnotation
@synthesize calloutView;
@synthesize portraitImageView   = _portraitImageView;
@synthesize nameLabel           = _nameLabel;

#pragma mark - Handle Action

#pragma mark - Override

- (NSString *)name
{
    return self.nameLabel.text;
}

- (void)setName:(NSString *)name
{
    self.nameLabel.text = name;
}

- (UIImage *)portrait
{
    return self.portraitImageView.image;
}

- (void)setPortrait:(UIImage *)portrait
{
    self.portraitImageView.image = portrait;
}

- (void)setSelected:(BOOL)selected
{
    [self setSelected:selected animated:NO];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    if (self.selected == selected)
    {
        return;
    }
    
    if (selected) {
        if (self.calloutView == nil) {
            /* Construct custom callout. */
            self.calloutView = [[PDCustomCalloutView alloc] initWithFrame:CGRectMake(0, 0, kCalloutWidth, kCalloutHeight)];
            self.calloutView.center = CGPointMake(CGRectGetWidth(self.bounds) / 2.f + self.calloutOffset.x, -CGRectGetHeight(self.calloutView.bounds) / 2.f + self.calloutOffset.y);
            self.calloutView.clipsToBounds = YES;
            
            CGFloat img_margin = LCFloat(5);
            CGFloat img_height = LCFloat(50);
            // 1. 头像
            PDImageView *avatar = [[PDImageView alloc]init];
            avatar.backgroundColor = [UIColor clearColor];
            [avatar uploadImageWithURL:self.transferNetBarSingleModel.bar.avatar placeholder:nil callback:NULL];
            avatar.frame = CGRectMake(img_margin, img_margin, img_height, img_height);
            [self.calloutView addSubview:avatar];

            // 2. 标题
            UILabel *fixedLabel = [[UILabel alloc]init];
            fixedLabel.text = self.transferNetBarSingleModel.bar.name;
            fixedLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"]boldFont];
            fixedLabel.numberOfLines = 0;
            [self.calloutView addSubview:fixedLabel];
            
            // 3. 内容
            UILabel *dymicLabel = [[UILabel alloc]init];
            dymicLabel.text = self.transferNetBarSingleModel.bar.priceRange;
            dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
            dymicLabel.numberOfLines = 0;
            dymicLabel.textColor = [UIColor colorWithCustomerName:@"红"];
            [self.calloutView addSubview:dymicLabel];
            
            // 4. 创建导航按钮
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
            btn.frame = CGRectMake(kCalloutWidth - 50, 0,50, self.calloutView.size_height - kArrorHeight);
            [btn setTitle:@"导航" forState:UIControlStateNormal];
            [btn setBackgroundColor:[UIColor colorWithCustomerName:@"蓝"]];
            [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            __weak typeof(self)weakSelf = self;
            [btn buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)() = objc_getAssociatedObject(strongSelf, &locationKey);
                if (block){
                    block();
                }
            }];
            [self.calloutView addSubview:btn];
            
            // 计算
            CGFloat maxWidth = kCalloutWidth - 50 - img_margin  * 3 - LCFloat(50);
            
            CGSize fixedSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(maxWidth, CGFLOAT_MAX)];
            fixedLabel.frame = CGRectMake(CGRectGetMaxX(avatar.frame) + img_margin, img_margin, maxWidth, fixedSize.height);
            
            // 详情文字
            CGSize dymicSize = [dymicLabel.text sizeWithCalcFont:dymicLabel.font constrainedToSize:CGSizeMake(maxWidth, CGFLOAT_MAX)];
            dymicLabel.frame = CGRectMake(fixedLabel.orgin_x, CGRectGetMaxY(fixedLabel.frame) + LCFloat(10), maxWidth, dymicSize.height);
            
            
            CGFloat paopaoSizeHeight =  img_margin + fixedSize.height + LCFloat(10) + dymicSize.height + img_margin;
            if (paopaoSizeHeight > 2 * img_margin + avatar.size_height){
                self.calloutView.size_height = paopaoSizeHeight + kArrorHeight;
                avatar.frame = CGRectMake(img_margin, (self.calloutView.size_height - avatar.size_height) / 2., avatar.size_width, avatar.size_height);
           
            } else{
                self.calloutView.size_height = 2 * img_margin + avatar.size_height + kArrorHeight;
                avatar.frame = CGRectMake(img_margin, img_margin, avatar.size_height, avatar.size_height);
            }
            
            fixedLabel.frame = CGRectMake(CGRectGetMaxX(avatar.frame) + img_margin, img_margin, maxWidth, fixedSize.height);
            
            dymicLabel.frame = CGRectMake(fixedLabel.orgin_x, CGRectGetMaxY(fixedLabel.frame) + LCFloat(10), maxWidth, dymicSize.height);
            
            
            btn.frame = CGRectMake(self.calloutView.size_width - 50, 0,50, self.calloutView.size_height - kArrorHeight);
            btn.layer.mask = [self setButtoncornerRadius:UIRectCornerBottomRight | UIRectCornerTopRight Btn:btn cornerRadius:kradus];
        }
        
        [self addSubview:self.calloutView];
    }
    else
    {
        [self.calloutView removeFromSuperview];
    }
    
    [super setSelected:selected animated:animated];
}

- (BOOL)pointInside:(CGPoint)point withEvent:(UIEvent *)event
{
    BOOL inside = [super pointInside:point withEvent:event];
    /* Points that lie outside the receiver’s bounds are never reported as hits,
     even if they actually lie within one of the receiver’s subviews.
     This can occur if the current view’s clipsToBounds property is set to NO and the affected subview extends beyond the view’s bounds.
     */
    if (!inside && self.selected)
    {
        inside = [self.calloutView pointInside:[self convertPoint:point toView:self.calloutView] withEvent:event];
    }
    
    return inside;
}

#pragma mark - Life Cycle

- (id)initWithAnnotation:(id<MAAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        self.bounds = CGRectMake(0, 0, kWidth, kHeight);
        self.backgroundColor = [UIColor clearColor];
        
        self.iconImageView = [[PDImageView alloc]init];
        self.iconImageView.frame = CGRectMake(0,0, kWidth, kHeight);
//        [self.iconImageView uploadImageWithURL:self.transferNetBarSingleModel.avatar placeholder:nil callback:NULL];
        self.iconImageView.clipsToBounds = YES;
        self.iconImageView.layer.cornerRadius = LCFloat(4);
        [self addSubview:self.iconImageView];
    }
    
    return self;
}

-(void)setTransferNetBarSingleModel:(PDInternetCafesSingleModel *)transferNetBarSingleModel{
    _transferNetBarSingleModel = transferNetBarSingleModel;
//    [self.iconImageView uploadImageWithURL:self.transferNetBarSingleModel.avatar placeholder:nil callback:NULL];
}



-(CAShapeLayer*)setButtoncornerRadius:(UIRectCorner)corners  Btn:(UIButton*)btn  cornerRadius:(float)cornerRadius {
    [btn.layer setMasksToBounds:YES];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.path = [UIBezierPath bezierPathWithRoundedRect:btn.bounds byRoundingCorners: corners cornerRadii: (CGSize){cornerRadius, cornerRadius}].CGPath;
    
    
    return maskLayer;
}

#pragma mark - 创建view
-(void)locationShow:(void(^)())block{
    objc_setAssociatedObject(self, &locationKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
