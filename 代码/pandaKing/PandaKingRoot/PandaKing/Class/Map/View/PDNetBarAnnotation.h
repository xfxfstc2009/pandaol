//
//  PDNetBarAnnotation.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <MAMapKit/MAMapKit.h>
#import "PDInternetCafesSingleModel.h"


@interface PDNetBarAnnotation : MAAnnotationView

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) UIImage *portrait;

@property (nonatomic, strong) UIView *calloutView;

-(void)locationShow:(void(^)())block;

@property (nonatomic,strong)PDInternetCafesSingleModel *transferNetBarSingleModel;

@end
