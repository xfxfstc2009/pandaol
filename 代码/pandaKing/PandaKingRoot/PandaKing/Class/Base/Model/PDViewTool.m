//
//  PDViewTool.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDViewTool.h"

@implementation PDViewTool


+(UITableView *)gwCreateTableViewRect:(CGRect)rect{
    UITableView *tableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.scrollEnabled = YES;
    return tableView;
}

+(UILabel *)createLabelFont:(NSString *)font textColor:(NSString *)color{
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithCustomerName:color];
    label.font = [UIFont fontWithCustomerSizeName:font];
    return label;
}

+(UILabel *)gameLabelFont:(NSString *)font textColor:(NSString *)color{
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithCustomerName:color];
    label.font = [UIFont fontWithGameCustomerSizeName:font];
    return label;
}


+(UIView *)createBottomButtonViewframe:(CGRect)frame image:(UIImage *)image label:(NSString *)text buttonBlock:(void(^)())block{
    UIView *view = [[UIView alloc]init];
    view.backgroundColor = [UIColor clearColor];
    view.frame = frame;
    
    // 2. 创建图片
    PDImageView *imageView = [[PDImageView alloc]init];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.image = image;
    [view addSubview:imageView];
    
    // 3. 创建label
    UILabel *titleLabel = [PDViewTool createLabelFont:@"提示" textColor:@"灰"];
    titleLabel.text = text;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    [view addSubview:titleLabel];
    
    
//    CGFloat margin = (frame.size.height - [NSString contentofHeightWithFont:titleLabel.font] - image.size.height);
    
    
    // 4. 创建按钮
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = view.bounds;
    __weak typeof(self)weakSelf = self;
    [button buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    [view addSubview:button];
    
    return view;
}


+(UIView *)goButtonWithTitle:(NSString *)title block:(void(^)())block{
    UIView *infoView = [[UIView alloc]init];
    infoView.backgroundColor = [UIColor colorWithCustomerName:@"棕"];
    
    UILabel *goLabel = [[UILabel alloc]init];
    goLabel.backgroundColor = [UIColor clearColor];
    goLabel.text = title;
    goLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [infoView addSubview:goLabel];
    
    CGSize goSize = [goLabel.text sizeWithCalcFont:goLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:goLabel.font])];
    goLabel.frame = CGRectMake(LCFloat(7), LCFloat(3), goSize.width, [NSString contentofHeightWithFont:goLabel.font]);
    
    PDImageView *arrowImgView = [[PDImageView alloc]init];
    arrowImgView.backgroundColor = [UIColor clearColor];
    arrowImgView.image = [UIImage imageNamed:@"sy_ic_jinru"];
    arrowImgView.frame = CGRectMake(CGRectGetMaxX(goLabel.frame) + LCFloat(2), 0, LCFloat(8), LCFloat(12));
    arrowImgView.center_y = goLabel.center_y;
    [infoView addSubview:arrowImgView];
    
    infoView.frame = CGRectMake(0, 0, goSize.width + LCFloat(2) + 2 * LCFloat(7) + LCFloat(8), 2 * LCFloat(3) + [NSString contentofHeightWithFont:goLabel.font]);
    infoView.clipsToBounds = YES;
    infoView.layer.cornerRadius = MIN(infoView.size_height, infoView.size_width) / 2.;
    return infoView;
}





@end
