//
//  PDAppleShenheModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDAppleShenheModel : FetchModel

@property (nonatomic,assign)BOOL enable;
@property (nonatomic,assign)BOOL auth;

@end
