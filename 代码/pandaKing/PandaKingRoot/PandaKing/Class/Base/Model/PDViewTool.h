//
//  PDViewTool.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDViewTool : NSObject

+(UITableView *)gwCreateTableViewRect:(CGRect)rect;

+(UILabel *)createLabelFont:(NSString *)font textColor:(NSString *)color;
+(UILabel *)gameLabelFont:(NSString *)font textColor:(NSString *)color;

+(UIView *)createBottomButtonViewframe:(CGRect)frame image:(UIImage *)image label:(NSString *)text buttonBlock:(void(^)())block;

// 创建go按钮
+(UIView *)goButtonWithTitle:(NSString *)title block:(void(^)())block;

// 创建进度条
//+(UIView *)createProgressViewWithTitle:(NSString *)title;

+(void)animationManagerWithLabel:(UILabel *)label toValue:(CGFloat)toValue;

@end
