//
//  GWSelectedTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWSelectedTableViewCell : UITableViewCell
{
    BOOL			isChecked;
}
@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,copy)NSString *transferTitle;

+(CGFloat)calculationCellHeight;

-(void)setChecked:(BOOL)checked;

@end
