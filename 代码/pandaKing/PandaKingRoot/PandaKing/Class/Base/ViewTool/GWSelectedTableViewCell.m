//
//  GWSelectedTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWSelectedTableViewCell.h"

@interface GWSelectedTableViewCell ()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)PDImageView *checkImgView;

@end

@implementation GWSelectedTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    //1. 创建名字
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.fixedLabel];
    
    // 2.创建选择
    self.checkImgView = [[PDImageView alloc]init];
    self.checkImgView.backgroundColor = [UIColor clearColor];
    self.checkImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11), 0, LCFloat(15), LCFloat(10));
    self.checkImgView.image = [UIImage imageNamed:@"icon_tick"];
    [self addSubview:self.checkImgView];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.fixedLabel.text = transferTitle;
    [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    
    CGSize titleSize =[self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellHeight)];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, titleSize.width, self.transferCellHeight);
    
    // 2.
    self.checkImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(15), 0, LCFloat(15), LCFloat(10));
    self.checkImgView.center_y = self.center_y;
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0 ;
    cellHeight += LCFloat(44);
    return cellHeight;
}



- (void)setChecked:(BOOL)checked{
    if (checked) {
        self.checkImgView.image = [UIImage imageNamed:@"icon_tick"];
        [Tool clickZanWithView:self.checkImgView block:NULL];
    } else {
        self.checkImgView.image = nil;
    }
    isChecked = checked;
}

@end
