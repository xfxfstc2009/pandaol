//
//  GWButtonTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GWButtonTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,copy)NSString *transferTitle;


@property (nonatomic,strong)UIButton *button;

-(void)setButtonStatus:(BOOL)enable;

-(void)buttonClickManager:(void(^)())block;

+(CGFloat)calculationCellHeight;

@end
