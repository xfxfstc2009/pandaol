//
//  PDSliderTableViewCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSliderTableViewCell.h"

@interface PDSliderTableViewCell()
@property (nonatomic,strong)PDImageView *iconImageView;             /**< icon*/
@property (nonatomic,strong)UILabel *fixedLabel;                    /**< fixedLaberl*/
@property (nonatomic,strong)UILabel *dymicLabel;                    /**< dymicLabel*/

@end

@implementation PDSliderTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.iconImageView = [[PDImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImageView];
    
    // 2. 创建label
    self.fixedLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.fixedLabel.textColor = [UIColor hexChangeFloat:@"999999"];
    self.fixedLabel.font = [UIFont systemFontOfCustomeSize:13.];
    [self addSubview:self.fixedLabel];
    
    self.dymicLabel = [GWViewTool createLabelFont:@"提示" textColor:@"白"];
    self.dymicLabel.font = [UIFont systemFontOfCustomeSize:11.];
    self.dymicLabel.textColor = [UIColor hexChangeFloat:@"999999"];
    [self addSubview:self.dymicLabel];
    
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferSingleModel:(PDSliderSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    // 1. img
    self.iconImageView.frame = CGRectMake(LCFloat(11), (self.transferCellHeight - LCFloat(16)) / 2., LCFloat(16), LCFloat(16));
    [self.iconImageView uploadSliderWithImageCode:transferSingleModel.functionCode callback:NULL];
    
    // 2. title
    self.fixedLabel.text = transferSingleModel.appFunctionName;
    CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + LCFloat(11), 0,fixedSize.width , self.transferCellHeight);
    
}

-(void)setTransferOthersingleModel:(PDAccountSliderOtherInfoModel *)transferOthersingleModel{
    _transferOthersingleModel = transferOthersingleModel;
    
    if ([self.transferSingleModel.appFunctionName isEqualToString:@"我的任务"]){
        if (self.transferOthersingleModel.unReceiveTaskCount != 0){
            self.dymicLabel.text = @"(您有未领取的福利)";
        } else {
            self.dymicLabel.text = @"";
        }
    } else if ([self.transferSingleModel.appFunctionName isEqualToString:@"邀请有奖"]){
        if (self.transferOthersingleModel.inviteAward.length){
            self.dymicLabel.text = [NSString stringWithFormat:@"(%@)",self.transferOthersingleModel.inviteAward];
        }
    } else {
        self.dymicLabel.text = @"";
    }
    
    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLabel.font])];
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame), 0, dymicSize.width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    self.dymicLabel.center_y = self.fixedLabel.center_y;
}


+(CGFloat)calculationCellHeight{
    return LCFloat(44);
}



@end
