//
//  PDPopView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,PopType){
    PopTypeGamer,                       /**< 筛选玩家*/
    PopTypeGroup,                       /**< 筛选战队*/
};


@interface PDPopView : UIView

-(void)viewShow;                    // 视图显示
-(void)viewDismiss;                 // 视图隐藏




@end
