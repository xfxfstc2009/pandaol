//
//  PDSliderTableViewCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "BFPaperTableViewCell.h"
#import "PDSliderSingleModel.h"
#import "PDAccountSliderOtherInfoModel.h"

@interface PDSliderTableViewCell : BFPaperTableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;                         /**< 传递上来的cell高度*/
@property (nonatomic,strong)PDSliderSingleModel *transferSingleModel;           /**< 传递过去的model*/
@property (nonatomic,strong)PDAccountSliderOtherInfoModel *transferOthersingleModel;

+(CGFloat)calculationCellHeight;

@end
