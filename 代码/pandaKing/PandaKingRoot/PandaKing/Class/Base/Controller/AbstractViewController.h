//
//  AbstractViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TNSexyImageUploadProgress.h"
#import "PDSliderViewController.h"
#import "PDBaseSocketRootModel.h"

#define BAR_BUTTON_FONT       [UIFont systemFontOfSize:14.]
#define BAR_MAIN_TITLE_FONT   [UIFont boldSystemFontOfSize:18.]
#define BAR_SUB_TITLE_FONT    [UIFont systemFontOfSize:12.]
#define BAR_TITLE_PADDING_TOP -3.
#define BAR_TITLE_MAX_WIDTH   LCFloat(230)

@class PDSliderViewController;
@interface AbstractViewController : UIViewController

@property (nonatomic,copy,nullable)NSString *barMainTitle;                       /**< 标题*/
@property (nonatomic,copy,nullable)NSString *barSubTitle;                        /**< 副标题*/

- (nonnull UIButton *)leftBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock ;

- (nonnull UIButton *)rightBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock;

- (void)hidesTabBarWhenPushed;

- (void)hidesTabBar:(BOOL)hidden animated:(BOOL)animated;

#pragma mark - 是否需要socket
@property (nonatomic,assign)BOOL hasCancelSocket;
#pragma mark - 是否需要左滑删除
@property (nonatomic,assign)BOOL hasCancelPanDismiss;

#pragma mark - Push Manager
-(void)pushViewController:(nonnull UIViewController *)controller animated:(BOOL)animated;

- (void)authorizeWithCompletionHandler:(nullable void(^)())handler;
- (void)pandaNotLoginNeedLoginManager;

#pragma mark - 弹出登录进行登录方法
-(void)showLoginViewControllerWithBlock:(nullable void(^)())block;

#pragma mark - Socket
-(void)socketDidBackData:(nullable id)responseObject;


/// 右上角消息按钮
@property (nonatomic,strong, nullable) UIButton *tempMsgButton;
@end
