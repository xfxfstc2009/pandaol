//
//  AdaptationShared.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AdaptationShared : NSObject

+(instancetype)sharedAdapter;               /**< 单例*/

@property (nonatomic,assign)TBAnimationButtonState buttonState;                     /**< 基类的按钮状态*/

// 【登录】
@property (nonatomic,copy)NSString *token;

@end
