//
//  PDMainSliderProgressView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/24.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMainSliderProgressView.h"
#import "WGradientProgress.h"

@interface PDMainSliderProgressView(){

}

@property (nonatomic,strong)UILabel *levelLabel;
@property (nonatomic,strong)UILabel *jingyanLabel;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UIView *progressView;
@property (nonatomic,strong)UILabel *maxLabel;

@property (nonatomic,strong)WGradientProgress * gradualProgressView;

@end

@implementation PDMainSliderProgressView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self pageSetting];
    }
    return self;
    
}

-(void)pageSetting{
    // 1. 创建等级
    self.levelLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    self.levelLabel.text = @"Lv 0";
    self.levelLabel.font = [UIFont systemFontOfCustomeSize:9.];
    [self addSubview:self.levelLabel];
    
    // 2. 创建进度条
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"999999"];
    [self addSubview:self.lineView];
    
    // 3. 创建进度
    self.progressView = [[UIView alloc]init];
    self.progressView.backgroundColor = [UIColor whiteColor];
    [self.lineView addSubview:self.progressView];
    
    // 3.
    self.jingyanLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    self.jingyanLabel.text = @"Max";
    self.jingyanLabel.font = [UIFont systemFontOfCustomeSize:9.];
    [self addSubview:self.jingyanLabel];
    
    self.maxLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    self.maxLabel.font = [UIFont systemFontOfCustomeSize:9.];
    [self addSubview:self.maxLabel];
}

-(void)setLevel:(NSString *)level current:(NSInteger)current max:(NSInteger)max{
    
    // 1. level
    self.levelLabel.text = [NSString stringWithFormat:@"%@",level];
    CGSize levelSize = [self.levelLabel.text sizeWithCalcFont:self.levelLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.levelLabel.font])];
    self.levelLabel.frame = CGRectMake(LCFloat(11), 0, levelSize.width, [NSString contentofHeightWithFont:self.levelLabel.font]);
    
    // 2. max
    self.maxLabel.text = [NSString stringWithFormat:@"/%li",max];
    CGSize maxSize = [self.maxLabel.text sizeWithCalcFont:self.maxLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.maxLabel.font])];
    self.maxLabel.frame = CGRectMake(self.size_width - LCFloat(11) - maxSize.width, self.levelLabel.orgin_y, maxSize.width, [NSString contentofHeightWithFont:self.maxLabel.font]);
    
    // 3.current
    NSString *currentStr = [NSString stringWithFormat:@"%li",(long)current];
    CGSize currentSize = [currentStr sizeWithCalcFont:self.jingyanLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.jingyanLabel.font])];
    self.jingyanLabel.frame = CGRectMake(self.maxLabel.orgin_x - currentSize.width, self.levelLabel.orgin_y, currentSize.width, [NSString contentofHeightWithFont:self.jingyanLabel.font]);
    self.jingyanLabel.adjustsFontSizeToFitWidth = YES;
    [self.jingyanLabel animationWithScrollToValue:current];
    
    // 4.line
    self.lineView.frame = CGRectMake(CGRectGetMaxX(self.levelLabel.frame) + LCFloat(11), 0, self.jingyanLabel.orgin_x - LCFloat(11) - CGRectGetMaxX(self.levelLabel.frame) - LCFloat(11), 3);
    self.lineView.center_y = self.levelLabel.center_y;
    
    // progress
    self.progressView.frame = CGRectMake(0, 0, 0, self.lineView.size_height);
    
    CGFloat progress = current * 1.f / max;
    CGFloat jingyanWidth = self.lineView.size_width * progress;
    [UIView animateWithDuration:3.f animations:^{
        self.progressView.size_width = jingyanWidth;
    } completion:^(BOOL finished) {
        self.progressView.size_width = jingyanWidth;
    }];
}


@end
