//
//  PDMainSliderProgressView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/24.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDMainSliderProgressView : UIView

-(void)setLevel:(NSString *)level current:(NSInteger)current max:(NSInteger)max;
@end
