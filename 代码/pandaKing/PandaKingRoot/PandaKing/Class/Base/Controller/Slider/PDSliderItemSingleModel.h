//
//  PDSliderItemSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDSliderItemSingleModel : FetchModel

@property (nonatomic,copy)NSString *name;
@property (nonatomic,strong)UIImage *imgNor;
@property (nonatomic,strong)UIImage *imgHlt;

@end
