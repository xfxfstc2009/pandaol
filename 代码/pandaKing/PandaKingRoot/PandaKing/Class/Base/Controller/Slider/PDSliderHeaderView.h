//
//  PDSliderHeaderView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AccountModel.h"
@interface PDSliderHeaderView : UIView
@property (nonatomic,strong)PDImageView *headerImageView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)AccountModel *transferAccountModel;             /**< 上个页面传递的Model*/
@property (nonatomic,strong)UIButton *chongzhiButton;

#pragma mark - 登录方法
-(void)loginManager;                                                        /**< 登录方法*/

-(void)touchManagerWithBlock:(void(^)())block;

+(CGFloat)calculationCellHeight;

-(void)chongzhiActionClick:(void(^)())block;
-(void)showAnimationWithBambooAndGold;
-(void)jinduAnimationWithModel:(PDAccountSliderOtherInfoModel *)singleModel;

@end
