//
//  PDSliderBottomView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDSliderBottomView.h"
#import "PDSliderItemSingleModel.h"
#import "SnailIconLabel.h"


#define ROW_COUNT 5 // 每行显示4个
#define ROWS 1      // 每页显示1行


static char gameTypeActionClickBlockKey;
@interface PDSliderBottomView(){
    CGFloat _gap, _space;
    CGSize _itemSize;
}
@property (nonatomic,strong)NSMutableArray *itemModelMutableArray;                       /**< item*/
@property (nonatomic, strong) NSMutableArray<PDImageView *> *pageViews;
@property (nonatomic, strong) NSMutableArray<SnailIconLabel *> *items;         /**< itemsArr*/
@property (nonatomic, strong) NSMutableArray<SnailIconLabelModel *> *models;
@end

@implementation PDSliderBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self arrayWithInit];
        [self createView];
    }
    return self;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{    
    // array
    self.items = @[].mutableCopy;
    self.models = [NSMutableArray<SnailIconLabelModel *> array];
    self.itemModelMutableArray = [NSMutableArray array];

    for (int i = 0 ; i < 5;i++){
        PDSliderItemSingleModel *singleModel = [[PDSliderItemSingleModel alloc]init];
        SnailIconLabelModel *snailIconLabelModel = [[SnailIconLabelModel alloc]init];
        
        if (i == 0){
            singleModel.name = @"绝地求生";
            if ([AccountModel sharedAccountModel].gameType == GameTypePubg){
                snailIconLabelModel.icon = [UIImage imageNamed:@"icon_slider_pubg_hlt"];
            } else {
                snailIconLabelModel.icon = [UIImage imageNamed:@"icon_slider_pubg_nor"];
            }
        } else if (i == 1){
            singleModel.name = @"英雄联盟";
            if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
                snailIconLabelModel.icon = [UIImage imageNamed:@"icon_slider_lol_hlt"];
            } else {
                snailIconLabelModel.icon = [UIImage imageNamed:@"icon_slider_lol_nor"];
            }
        } else if (i == 2){
            singleModel.name = @"王者荣耀";
            if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
                snailIconLabelModel.icon = [UIImage imageNamed:@"icon_slider_pvp_hlt"];
            } else {
                snailIconLabelModel.icon = [UIImage imageNamed:@"icon_slider_pvp_nor"];
            }
        } else if (i == 3){
            singleModel.name = @"DotA2";
            
            if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
                snailIconLabelModel.icon = [UIImage imageNamed:@"icon_slider_dota2_hlt"];
            } else {
                snailIconLabelModel.icon = [UIImage imageNamed:@"icon_slider_dota2_nor"];
            }
        } else if (i == 4){
            singleModel.name = @"反恐精英";

            if ([AccountModel sharedAccountModel].gameType == GameTypeCS){
                snailIconLabelModel.icon = [UIImage imageNamed:@"icon_slider_cs_hlt"];
            } else {
                snailIconLabelModel.icon = [UIImage imageNamed:@"icon_slider_cs_nor"];
            }
        }
        snailIconLabelModel.text = singleModel.name;
        [self.itemModelMutableArray addObject:singleModel];
        [self.models addObject:snailIconLabelModel];
    }
}

#pragma mark - createView
-(void)createView{
    CGFloat margin = LCFloat(11);
    CGFloat width = (self.size_width - (self.itemModelMutableArray.count + 1 ) * LCFloat(11)) / self.itemModelMutableArray.count;
    
    _itemSize = CGSizeMake(width, width);
    _gap = 15;
    _space = margin;
    
    [self animationManager];
}

-(void)animationManager{
    for (NSInteger i = 0; i < ROWS * ROW_COUNT; i++) {
        NSInteger l = i % ROW_COUNT;
        
        SnailIconLabel *item = [[SnailIconLabel alloc]init];;
        [self addSubview:item];
        [_items addObject:item];
        item.tag = i;
        if (item.tag < self.models.count) {
            [item addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(itemClicked:)]];
            item.model = [self.models objectAtIndex:item.tag];
            item.iconView.userInteractionEnabled = NO;
            item.textLabel.font = [UIFont systemFontOfSize:14];
            item.textLabel.textColor = [UIColor whiteColor];
            item.alpha = 0;
            [item updateLayoutBySize:_itemSize finished:^(SnailIconLabel *item) {
                item.orgin_x = _space + (_itemSize.width  + _space) * l;
                item.orgin_y = (self.size_height - _itemSize.height) / 2.;
            }];
        }
    }
}

-(void)showAnimation{
    [self startAnimationsCompletion:NULL];
}

- (void)startAnimationsCompletion:(void (^ __nullable)(BOOL finished))completion {
    
    [_items enumerateObjectsUsingBlock:^(SnailIconLabel *item, NSUInteger idx, BOOL * _Nonnull stop) {
        item.alpha = 0;
        item.transform = CGAffineTransformMakeTranslation(0, ROWS * _itemSize.height);
        [UIView animateWithDuration:0.85
                              delay:idx * 0.02
             usingSpringWithDamping:0.6
              initialSpringVelocity:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                             item.alpha = 1;
                             item.transform = CGAffineTransformIdentity;
                         } completion:completion];
    }];
}

-(void)stopAnimation{
    [_items enumerateObjectsUsingBlock:^(SnailIconLabel * _Nonnull item, NSUInteger idx, BOOL * _Nonnull stop) {
        [UIView animateWithDuration:.15f delay:0.015 * (_items.count - idx) options:UIViewAnimationOptionCurveLinear animations:^{
            item.alpha = 0;
            item.transform = CGAffineTransformMakeTranslation(0, ROWS * _itemSize.height);
        } completion:NULL];

    }];
}


- (void)itemClicked:(UITapGestureRecognizer *)recognizer  {
    if (recognizer.view.tag == 0){
        [AccountModel sharedAccountModel].gameType = GameTypePubg;
    } else if (recognizer.view.tag == 1){
        [AccountModel sharedAccountModel].gameType = GameTypeLoL;
    } else if (recognizer.view.tag == 2){
        [AccountModel sharedAccountModel].gameType = GameTypePVP;
    } else if (recognizer.view.tag == 3){
        [AccountModel sharedAccountModel].gameType = GameTypeDota2;
    } else if (recognizer.view.tag == 4){
        [AccountModel sharedAccountModel].gameType = GameTypeCS;
    }
    
    [self itemClickWithIndex:recognizer.view.tag];
    
    [AccountModel sharedAccountModel].lastGameType = recognizer.view.tag;
    
    void(^block)() = objc_getAssociatedObject(self, &gameTypeActionClickBlockKey);
    if (block){
        block();
    }
}


#pragma mark - 按钮点击
-(void)itemClickWithIndex:(NSInteger)index{
    SnailIconLabel *selectedItem = [self.items objectAtIndex:index];
    SnailIconLabel *lastSelectedItem = [self.items objectAtIndex:[AccountModel sharedAccountModel].lastGameType];
    // 1.查找出上一个item进行dismiss
    if ([AccountModel sharedAccountModel].lastGameType == GameTypeLoL){
        [lastSelectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_lol_nor"]];
    } else if ([AccountModel sharedAccountModel].lastGameType == GameTypePVP){
        [lastSelectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_pvp_nor"]];
    } else if ([AccountModel sharedAccountModel].lastGameType == GameTypeDota2){
        [lastSelectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_dota2_nor"]];
    } else if ([AccountModel sharedAccountModel].lastGameType == GameTypePubg){
        [lastSelectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_pubg_nor"]];
    } else if ([AccountModel sharedAccountModel].lastGameType == GameTypeCS){
        [lastSelectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_cs_nor"]];
    }

    // 2. 查找出当前的进行show
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        [selectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_lol_hlt"]];
        [AccountModel sharedAccountModel].mainStyleColor = [UIColor colorWithCustomerName:@"lol"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        [selectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_pvp_hlt"]];
        [AccountModel sharedAccountModel].mainStyleColor = [UIColor colorWithCustomerName:@"pvp"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        [selectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_dota2_hlt"]];
        [AccountModel sharedAccountModel].mainStyleColor = [UIColor colorWithCustomerName:@"dota"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePubg){
        [selectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_pubg_hlt"]];
        [AccountModel sharedAccountModel].mainStyleColor = [UIColor colorWithCustomerName:@"ow"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeCS){
        [selectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_cs_hlt"]];
        [AccountModel sharedAccountModel].mainStyleColor = [UIColor colorWithCustomerName:@"cs"];
    }
    
    // 3. 切换slider
    [[AccountModel sharedAccountModel] sendRequestToGetSliderInfo];
}


-(void)gameTypeActionClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &gameTypeActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



-(void)firstChooseGameManager{
    if ([AccountModel sharedAccountModel].gameType != GameTypeLoL){
        
        SnailIconLabel *norSelectedItem = [self.items objectAtIndex:0];
        [norSelectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_lol_nor"]];
        
        SnailIconLabel *selectedItem = [self.items objectAtIndex:[AccountModel sharedAccountModel].gameType];
        
        // 2. 查找出当前的进行show
        if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
            [selectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_lol_hlt"]];
            [AccountModel sharedAccountModel].mainStyleColor = [UIColor colorWithCustomerName:@"lol"];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
            [selectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_pvp_hlt"]];
            [AccountModel sharedAccountModel].mainStyleColor = [UIColor colorWithCustomerName:@"pvp"];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
            [selectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_dota2_hlt"]];
            [AccountModel sharedAccountModel].mainStyleColor = [UIColor colorWithCustomerName:@"dota"];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePubg){
            [selectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_pubg_hlt"]];
            [AccountModel sharedAccountModel].mainStyleColor = [UIColor colorWithCustomerName:@"ow"];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeCS){
            [selectedItem animationWithIconDismissWithImg:[UIImage imageNamed:@"icon_slider_cs_hlt"]];
            [AccountModel sharedAccountModel].mainStyleColor = [UIColor colorWithCustomerName:@"cs"];
        }
        // 3. 切换slider
        [[AccountModel sharedAccountModel] sendRequestToGetSliderInfo];
    }
}

@end
