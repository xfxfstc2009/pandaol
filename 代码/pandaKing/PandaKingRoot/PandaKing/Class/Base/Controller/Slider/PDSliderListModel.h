//
//  PDSliderListModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDSliderSingleModel.h"

@interface PDSliderListModel : FetchModel

@property (nonatomic,strong)NSArray<PDSliderSingleModel> *sliderlist;


@end
