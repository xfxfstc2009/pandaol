//
//  PDSliderSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDSliderSingleModel <NSObject>

@end

@interface PDSliderSingleModel : FetchModel

@property (nonatomic,copy)NSString *functionCode;                    /**< code*/
@property (nonatomic,copy)NSString *appFunctionName;
@property (nonatomic,assign)BOOL enable;
@property (nonatomic,assign)BOOL direct;
@property (nonatomic,copy)NSString *url;
@property (nonatomic,copy)NSString *showTitle;

@end
