//
//  PDSliderBottomView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDSliderBottomView : UIView

-(void)gameTypeActionClickBlock:(void(^)())block;
-(void)showAnimation;
-(void)stopAnimation;

-(void)firstChooseGameManager;

@end
