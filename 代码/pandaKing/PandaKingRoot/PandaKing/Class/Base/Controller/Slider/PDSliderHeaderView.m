//
//  PDSliderHeaderView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSliderHeaderView.h"
#import <objc/runtime.h>
#import "PDMainSliderProgressView.h"

static char loginKey;
static char chongzhiSliderActionClickKey;
@interface PDSliderHeaderView()
@property (nonatomic,strong)UILabel *levelLabel;
@property (nonatomic,strong)PDImageView *goldIconImgView;
@property (nonatomic,strong)UILabel *goldLabel;
@property (nonatomic,strong)PDImageView *bambooIconImgView;
@property (nonatomic,strong)UILabel *bambooLabel;

@property (nonatomic,strong)UIButton *loginButton;
@property (nonatomic,strong)PDMainSliderProgressView *sliderProgressView;
@property (nonatomic,strong)UIView *lineView;

@end

@implementation PDSliderHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. avatar
    self.headerImageView = [[PDImageView alloc]init];
    self.headerImageView.backgroundColor = [UIColor clearColor];
    self.headerImageView.clipsToBounds = YES;
    self.headerImageView.frame = CGRectMake(LCFloat(15), (self.size_height - LCFloat(60)) / 2., LCFloat(60), LCFloat(60));
    self.headerImageView.layer.cornerRadius = self.headerImageView.size_height / 2. ;
    [self addSubview:self.headerImageView];
    
    // 2. 创建name
    self.nameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    [self addSubview:self.nameLabel];
    
    // 3. 创建level
    self.levelLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    self.levelLabel.backgroundColor = [UIColor colorWithCustomerName:@"橙"];
    [self addSubview:self.levelLabel];
    
    // 4. 创建金币
    self.goldIconImgView = [[PDImageView alloc]init];
    [self addSubview:self.goldIconImgView];
    
    // 5. 创建金钱
    self.goldLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"金"];
    [self addSubview:self.goldLabel];
    
    // 6. 创建竹子
    self.bambooIconImgView = [[PDImageView alloc]init];
    [self addSubview:self.bambooIconImgView];
    
    // 7. 创建竹子
    self.bambooLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"金"];
    [self addSubview:self.bambooLabel];
    
    // 8.创建充值按钮
    
    // 9. 创建进度条
    self.sliderProgressView = [[PDMainSliderProgressView alloc]initWithFrame:CGRectMake(self.headerImageView.orgin_x,  CGRectGetMaxY(self.headerImageView.frame) + LCFloat(11), self.size_width - 2 * self.headerImageView.orgin_x, LCFloat(30))];
    [self addSubview:self.sliderProgressView];

    // 10. 创建充值按钮
    self.chongzhiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.chongzhiButton.backgroundColor = [UIColor clearColor];
    [self.chongzhiButton setTitle:@"充值" forState:UIControlStateNormal];
    self.chongzhiButton.titleLabel.font = [UIFont systemFontOfCustomeSize:10.];
    [self.chongzhiButton setTitleColor:[UIColor colorWithCustomerName:@"金"] forState:UIControlStateNormal];
    CGSize chongzhiSize = [self.chongzhiButton.titleLabel.text sizeWithCalcFont:self.chongzhiButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.chongzhiButton.titleLabel.font])];
    CGFloat chongzhiWidth = chongzhiSize.width + 2 * LCFloat(5);
    CGFloat chongzhiHeight = [NSString contentofHeightWithFont:self.chongzhiButton.titleLabel.font] + 2 * LCFloat(3);
    self.chongzhiButton.frame = CGRectMake(0, 0, chongzhiWidth, chongzhiHeight);
    self.chongzhiButton.layer.cornerRadius = LCFloat(3);
    self.chongzhiButton.layer.borderColor = [UIColor colorWithCustomerName:@"金"].CGColor;
    self.chongzhiButton.layer.borderWidth = 1.;
    __weak typeof(self)weakSelf = self;
    self.chongzhiButton.hidden = YES;
    
    [self.chongzhiButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &chongzhiSliderActionClickKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.chongzhiButton];
    
    
    // 3. 创建按钮
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.loginButton.backgroundColor = [UIColor clearColor];
    [self.loginButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
       void(^block)() =  objc_getAssociatedObject(strongSelf, &loginKey);
        if (block){
            block();
        }
    }];
    self.loginButton.frame = self.headerImageView.frame;
    [self addSubview:self.loginButton];
    
    // 4. 创建line
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"侧边分割线"];
    self.lineView.frame = CGRectMake(LCFloat(10), self.size_height - .5f, kScreenBounds.size.width / 4. * 3 - 2 * LCFloat(10), .5f);
    [self addSubview:self.lineView];
}

-(void)setTransferAccountModel:(AccountModel *)transferAccountModel{
    _transferAccountModel = transferAccountModel;
    
    // 1. 判断是否登录
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){           // 判断是否登录
        [self userInfoSet];
    } else {
        [self noUserInfoSet];
    }
}

//
-(void)userInfoSet{
    if ([AccountModel sharedAccountModel].memberInfo){
        self.levelLabel.hidden = NO;
        self.goldIconImgView.hidden = NO;
        self.goldLabel.hidden = NO;
        self.goldIconImgView.hidden = NO;
        self.bambooIconImgView.hidden = NO;
        self.bambooLabel.hidden = NO;
        self.chongzhiButton.hidden = NO;
        
        // 1. 头像
        [self.headerImageView uploadImageWithAvatarURL:[AccountModel sharedAccountModel].memberInfo.avatar placeholder:nil callback:NULL];
        
        CGFloat margin = (self.headerImageView.size_height - [NSString contentofHeightWithFont:self.nameLabel.font] - LCFloat(15) - LCFloat(11)) / 2.;
        
        // 2. 昵称
        self.nameLabel.text = [AccountModel sharedAccountModel].memberInfo.nickname;
        CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.nameLabel.font])];
        self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + LCFloat(11), self.headerImageView.orgin_y + margin, nameSize.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
        
        // 3. 等级
        self.levelLabel.text = [NSString stringWithFormat:@"Lv%li",[AccountModel sharedAccountModel].memberInfo.level];
        self.levelLabel.layer.cornerRadius = LCFloat(3);
        self.levelLabel.clipsToBounds = YES;
        self.levelLabel.textAlignment = NSTextAlignmentCenter;
        self.levelLabel.font = [self.levelLabel.font boldFont];
        CGSize levelSize = [self.levelLabel.text sizeWithCalcFont:self.levelLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.levelLabel.font])];
        CGFloat width = levelSize.width + 2 * LCFloat(7);
        CGFloat height = [NSString contentofHeightWithFont:self.levelLabel.font] + 2 * LCFloat(5);
        self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + LCFloat(11), 0, width, height);
        self.levelLabel.center_y = self.nameLabel.center_y;
        
        // 4.gold
        self.goldIconImgView.image = [UIImage imageNamed:@"icon_center_gold"];
        self.goldIconImgView.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), LCFloat(13), LCFloat(13));
        
        // 5. 创建gold
        self.goldLabel.text = [Tool transferWithInfoNumber:[AccountModel sharedAccountModel].memberInfo.gold];
        CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.goldLabel.font])];
        self.goldLabel.frame = CGRectMake(CGRectGetMaxX(self.goldIconImgView.frame) + LCFloat(5), self.goldIconImgView.orgin_y, goldSize.width, [NSString contentofHeightWithFont:self.goldLabel.font]);
        self.goldLabel.center_y = self.goldIconImgView.center_y;
        
        // 6. bamboo
        self.bambooIconImgView.image = [UIImage imageNamed:@"icon_center_bamboo"];
        self.bambooIconImgView.frame = CGRectMake(CGRectGetMaxX(self.goldLabel.frame) + LCFloat(11), self.goldIconImgView.orgin_y, LCFloat(13), LCFloat(13));
        
        // 6.1 bamboo
        self.bambooLabel.text = [Tool transferWithInfoNumber:[AccountModel sharedAccountModel].memberInfo.bamboo];
        CGSize bambooSize = [self.bambooLabel.text sizeWithCalcFont:self.bambooLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.bambooLabel.font])];
        self.bambooLabel.frame = CGRectMake(CGRectGetMaxX(self.bambooIconImgView.frame) + LCFloat(5), self.goldLabel.orgin_y, bambooSize.width, [NSString contentofHeightWithFont:self.bambooLabel.font]);
        
        // 7. 添加充值按钮
        self.chongzhiButton.frame = CGRectMake(CGRectGetMaxX(self.bambooLabel.frame) + LCFloat(11), 0, self.chongzhiButton.size_width, self.chongzhiButton.size_height);
        self.chongzhiButton.center_y = self.bambooLabel.center_y;
        
    }
}

-(void)noUserInfoSet{
    // 1. 头像
    self.headerImageView.image = [Tool imageByComposingImage: [UIImage imageNamed:@"icon_nordata"] withMaskImage:[UIImage imageNamed:@"round"]];
    
    CGFloat margin = (self.headerImageView.size_height - [NSString contentofHeightWithFont:self.nameLabel.font] - LCFloat(15) - LCFloat(11)) / 2.;
    
    // 2. 昵称
    self.nameLabel.text = @"登录 / 注册";
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + LCFloat(11), self.headerImageView.orgin_y + margin, nameSize.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    // 3. 等级
    self.levelLabel.text = [NSString stringWithFormat:@"Lv%li",[AccountModel sharedAccountModel].memberInfo.level];
    self.levelLabel.layer.cornerRadius = LCFloat(3);
    self.levelLabel.clipsToBounds = YES;
    self.levelLabel.textAlignment = NSTextAlignmentCenter;
    self.levelLabel.font = [self.levelLabel.font boldFont];
    CGSize levelSize = [self.levelLabel.text sizeWithCalcFont:self.levelLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.levelLabel.font])];
    CGFloat width = levelSize.width + 2 * LCFloat(7);
    CGFloat height = [NSString contentofHeightWithFont:self.levelLabel.font] + 2 * LCFloat(5);
    self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + LCFloat(11), 0, width, height);
    self.levelLabel.center_y = self.nameLabel.center_y;
    
    // 4.gold
    self.goldIconImgView.image = [UIImage imageNamed:@"icon_center_gold"];
    self.goldIconImgView.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), LCFloat(13), LCFloat(13));
    
    // 5. 创建gold
    self.goldLabel.text = [Tool transferWithInfoNumber:[AccountModel sharedAccountModel].gold];
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.goldLabel.font])];
    self.goldLabel.frame = CGRectMake(CGRectGetMaxX(self.goldIconImgView.frame) + LCFloat(5), self.goldIconImgView.orgin_y, goldSize.width, [NSString contentofHeightWithFont:self.goldLabel.font]);
    self.goldLabel.center_y = self.goldIconImgView.center_y;
    
    // 6. bamboo
    self.bambooIconImgView.image = [UIImage imageNamed:@"icon_center_bamboo"];
    self.bambooIconImgView.frame = CGRectMake(CGRectGetMaxX(self.goldLabel.frame) + LCFloat(11), self.goldIconImgView.orgin_y, LCFloat(13), LCFloat(13));
    
    // 6.1 bamboo
    self.bambooLabel.text = [Tool transferWithInfoNumber:[AccountModel sharedAccountModel].bamboo];
    CGSize bambooSize = [self.bambooLabel.text sizeWithCalcFont:self.bambooLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.bambooLabel.font])];
    self.bambooLabel.frame = CGRectMake(CGRectGetMaxX(self.bambooIconImgView.frame) + LCFloat(5), self.goldLabel.orgin_y, bambooSize.width, [NSString contentofHeightWithFont:self.bambooLabel.font]);

    
    self.levelLabel.hidden = YES;
    self.goldIconImgView.hidden = YES;
    self.goldLabel.hidden = YES;
    self.goldIconImgView.hidden = YES;
    self.bambooIconImgView.hidden = YES;
    self.bambooLabel.hidden = YES;
    self.chongzhiButton.hidden = YES;
}


#pragma mark - 登录方法
-(void)loginManager{
    [self userInfoSet];
}

-(void)touchManagerWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &loginKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - Height
+(CGFloat)calculationCellHeight{
    return LCFloat(190);
}

-(void)chongzhiActionClick:(void(^)())block{
    objc_setAssociatedObject(self, &chongzhiSliderActionClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}

-(void)showAnimationWithBambooAndGold{
    // 4.gold
    self.goldIconImgView.image = [UIImage imageNamed:@"icon_center_gold"];
    self.goldIconImgView.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), LCFloat(13), LCFloat(13));
    
    // 5. 创建gold
    self.goldLabel.text = [Tool transferWithInfoNumber:[AccountModel sharedAccountModel].memberInfo.gold];
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.goldLabel.font])];
    self.goldLabel.frame = CGRectMake(CGRectGetMaxX(self.goldIconImgView.frame) + LCFloat(5), self.goldIconImgView.orgin_y, goldSize.width, [NSString contentofHeightWithFont:self.goldLabel.font]);
    self.goldLabel.center_y = self.goldIconImgView.center_y;
    
    // 6. bamboo
    self.bambooIconImgView.image = [UIImage imageNamed:@"icon_center_bamboo"];
    self.bambooIconImgView.frame = CGRectMake(CGRectGetMaxX(self.goldLabel.frame) + LCFloat(11), self.goldIconImgView.orgin_y, LCFloat(13), LCFloat(13));
    
    // 6.1 bamboo
    self.bambooLabel.text = [Tool transferWithInfoNumber:[AccountModel sharedAccountModel].memberInfo.bamboo];
    CGSize bambooSize = [self.bambooLabel.text sizeWithCalcFont:self.bambooLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.bambooLabel.font])];
    self.bambooLabel.frame = CGRectMake(CGRectGetMaxX(self.bambooIconImgView.frame) + LCFloat(5), self.goldLabel.orgin_y, bambooSize.width, [NSString contentofHeightWithFont:self.bambooLabel.font]);
}

-(void)jinduAnimationWithModel:(PDAccountSliderOtherInfoModel *)singleModel{
    self.levelLabel.text = [NSString stringWithFormat:@"Lv%li",singleModel.level];
    
    [self.sliderProgressView setLevel:[NSString stringWithFormat:@"Lv %li",singleModel.level] current:singleModel.empiricValue max:singleModel.upGradeEmpiricValue];
}

@end
