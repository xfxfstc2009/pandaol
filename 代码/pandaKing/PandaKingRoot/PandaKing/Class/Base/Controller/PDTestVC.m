//
//  PDTestVC.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTestVC.h"
#import "PDTestMoreSocketTestVC.h"

@interface PDTestVC()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong) UITableView *settingTableView;
@property (nonatomic,strong)NSArray *testArr;

// layer
@property (nonatomic,strong)UISwitch *layerSwitch;
@property (nonatomic,strong)UISwitch *socketSwitch;
@property (nonatomic,strong)UISwitch *netSwitch;
// socket
@property (nonatomic,strong)UITextField *ipTextField;
@property (nonatomic,strong)UITextField *portTextField;

@end

@implementation PDTestVC

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"测试人员工具";
    
    [self rightBarButtonWithTitle:@"提交" barNorImage:nil barHltImage:nil action:^{
        NSString *info = [NSString stringWithFormat:@"ip:%@\nport:%@",self.ipTextField.text,self.portTextField.text];
        [[UIAlertView alertViewWithTitle:nil message:info buttonTitles:@[@"确定",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 0){
                [Tool userDefaulteWithKey:testNet_address Obj:self.ipTextField.text];
                [Tool userDefaulteWithKey:testNet_port Obj:self.portTextField.text];
                [[UIAlertView alertViewWithTitle:@"需要重启" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    exit(0);
                }]show];
            }
        }]show];
    }];
}

#pragma mark - arrayWithinit
-(void)arrayWithInit{
    self.testArr = @[@[@"Layer"],@[@"Socket测试",@"Socket打印"],@[@"IP",@"Port",@"网络打印"],@[@"SocketMore"],@[@"清除token"]];
    
}

-(void)createTableView{
    if (!self.settingTableView){
        self.settingTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.settingTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.settingTableView.delegate = self;
        self.settingTableView.dataSource = self;
        self.settingTableView.backgroundColor = [UIColor clearColor];
        self.settingTableView.showsVerticalScrollIndicator = NO;
        self.settingTableView.scrollEnabled = YES;
        self.settingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.settingTableView];
    }
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.testArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.testArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellheight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;

        UILabel *fixedLabel = [[UILabel alloc]init];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.stringTag = @"fixedLabel";
        fixedLabel.frame = CGRectMake(LCFloat(11), 0, LCFloat(100), cellheight);
        [cellWithRowOne addSubview:fixedLabel];
        
        // 2. 创建dymiclabel
        UITextField *inputTextField = [[UITextField alloc]init];
        inputTextField.backgroundColor = [UIColor clearColor];
        inputTextField.stringTag = @"inputTextField";
        inputTextField.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        inputTextField.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        inputTextField.textAlignment = NSTextAlignmentCenter;
        inputTextField.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame), 0, LCFloat(100), cellheight);
        [cellWithRowOne addSubview:inputTextField];
        
        // 3.
        UISwitch *normalSwitch = [[UISwitch alloc] init];
        normalSwitch.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - 51, 0, 51, cellheight);
        [normalSwitch addTarget:self action:@selector(switchAction:) forControlEvents:UIControlEventTouchUpInside];
        normalSwitch.stringTag = @"normalSwitch";
        [cellWithRowOne addSubview:normalSwitch];
    }
    UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedLabel"];
    UITextField *inputTextField = (UITextField *)[cellWithRowOne viewWithStringTag:@"inputTextField"];
    UISwitch *normalSwitch = (UISwitch *)[cellWithRowOne viewWithStringTag:@"normalSwitch"];
    normalSwitch.hidden = YES;
    fixedLabel.text = [[self.testArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Layer"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Layer"]){
        self.layerSwitch = normalSwitch;
        normalSwitch.hidden = NO;
        if ([[Tool userDefaultGetWithKey:View_Layer] isEqualToString:@"y"]){
            [self.layerSwitch setOn:YES];
        } else {
            [self.layerSwitch setOn:NO];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Socket打印"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Socket打印"]){
        self.socketSwitch = normalSwitch;
        normalSwitch.hidden = NO;
        if ([[Tool userDefaultGetWithKey:testSocketLogAlert] isEqualToString:@"y"]){
            [self.socketSwitch setOn:YES];
        } else {
            [self.socketSwitch setOn:NO];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"网络打印"] && indexPath.row == [self cellIndexPathRowWithcellData:@"网络打印"]){
        normalSwitch.hidden = NO;
        self.netSwitch = normalSwitch;
        if ([[Tool userDefaultGetWithKey:testNet_Log] isEqualToString:@"y"]){
            [self.netSwitch setOn:YES];
        } else {
            [self.netSwitch setOn:NO];
        }
    } else if  (indexPath.section == [self cellIndexPathSectionWithcellData:@"IP"] && indexPath.row == [self cellIndexPathRowWithcellData:@"IP"]){
        self.ipTextField = inputTextField;
        self.ipTextField.placeholder = @"输入ip";
        self.ipTextField.text = [Tool userDefaultGetWithKey:testNet_address];
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Port"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Port"]){
        self.portTextField = inputTextField;
        self.portTextField.placeholder = @"输入端口号";
        self.portTextField.text = [Tool userDefaultGetWithKey:testNet_port];
    }
        
    
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"Socket测试"] && indexPath.row == [self cellIndexPathRowWithcellData:@"Socket测试"]){
        [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:@{@"123":@"我是ios-socket测试"} socket:[NetworkAdapter sharedAdapter].loginSocketDelegate];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"SocketMore"]){
        PDTestMoreSocketTestVC *ts = [[PDTestMoreSocketTestVC alloc]init];
        [self.navigationController pushViewController:ts animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"清除token"]){
        [Tool userDefaultDelegtaeWithKey:CustomerToken];
        [AccountModel sharedAccountModel].token = @"123";
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.backgroundColor = [UIColor clearColor];
    fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    if (section == 0){
        fixedLabel.text = @"Layer";
    } else if (section == 1){
        fixedLabel.text = @"Socket设置";
    } else if (section == 2){
        fixedLabel.text = @"服务器设置";
    }
    fixedLabel.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width, [NSString contentofHeightWithFont:fixedLabel.font]);
    [headerView addSubview:fixedLabel];
    return headerView;
}

-(void)switchAction:(UISwitch *)sender{
    if (sender == self.layerSwitch){                             // 要关闭
        if (self.layerSwitch.isOn == YES){
            [Tool userDefaulteWithKey:View_Layer Obj:@"y"];
        } else {
            [Tool userDefaulteWithKey:View_Layer Obj:@"n"];
        }
        [[UIAlertView alertViewWithTitle:@"需要重启" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            exit(0);
        }]show];
    } else if(sender == self.socketSwitch){
        if (self.socketSwitch.isOn == YES){
            [Tool userDefaulteWithKey:testSocketLogAlert Obj:@"y"];
        } else {
            [Tool userDefaulteWithKey:testSocketLogAlert Obj:@"n"];
        }
        
    } else if (sender == self.netSwitch){
        if (self.netSwitch.isOn == YES){
            [Tool userDefaulteWithKey:testNet_Log Obj:@"y"];
        } else {
            [Tool userDefaulteWithKey:testNet_Log Obj:@"n"];
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.settingTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.testArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([[self.testArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.testArr.count ; i++){
        NSArray *dataTempArr = [self.testArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.testArr.count ; i++){
        NSArray *dataTempArr = [self.testArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}


@end
