//
//  PDTestMoreSocketTestVC.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDTestMoreSocketTestVC.h"
#import "PDMoreSocketSingleModel.h"
#import "PDNetworkAdapterMoreTest.h"


@interface PDTestMoreSocketTestVC()<UITableViewDataSource,UITableViewDelegate,PDNetworkAdapterDelegate,PDSocketConnectionDelegate>
@property (nonatomic,strong)UITableView *moreTableView;
@property (nonatomic,strong)NSMutableArray *moreMutableArr;
@property (nonatomic,strong)PDMoreSocketSingleModel *tempSingleModel;
@property (nonatomic,strong)PDNetworkAdapterMoreTest *socketMoreTest;

@end

@implementation PDTestMoreSocketTestVC

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self login];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"用来测试很多很多Socket";
    [self rightBarButtonWithTitle:@"123" barNorImage:nil barHltImage:nil action:^{
        [self.moreTableView reloadData];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.moreMutableArr = [NSMutableArray array];
}

-(void)createTableView{
    if (!self.moreTableView){
        self.moreTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.moreTableView.dataSource = self;
        self.moreTableView.delegate = self;
        [self.view addSubview:self.moreTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.moreMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;

    PDMoreSocketSingleModel *singleModel = [self.moreMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferTitle = [NSString stringWithFormat:@"【%li】,%@",indexPath.row,singleModel.memberId];
    if (singleModel.hasOpen){
        cellWithRowOne.transferDesc = @"在房间内";
        cellWithRowOne.dymicLabel.textColor = [UIColor redColor];
    } else {
        cellWithRowOne.transferDesc = @"在房间外";
        cellWithRowOne.dymicLabel.textColor = [UIColor greenColor];
    }
    return cellWithRowOne;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PDMoreSocketSingleModel *singleModel = [self.moreMutableArr objectAtIndex:indexPath.row];
    singleModel.hasOpen = !singleModel.hasOpen;
    if (singleModel.hasOpen){
        [self inhouseWithModel:singleModel];
    } else {
        [self outhouseWithModel:singleModel];
    }
    [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
}

-(void)inhouseWithModel:(PDMoreSocketSingleModel *)singleModel{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (singleModel.token.length){
        [params setObject:singleModel.token forKey:@"token"];
        [params setObject:@"jungleguess" forKey:@"type"];
    }
    self.tempSingleModel = singleModel;
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:params socket:singleModel.socketConnect];
}


-(void)outhouseWithModel:(PDMoreSocketSingleModel *)singleModel{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:@"quitjungleguess" forKey:@"type"];
    [params setObject:singleModel.token forKey:@"token"];
    [params setObject:singleModel.guessId forKey:@"guessId"];
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:params socket:singleModel.socketConnect];
}

-(void)loginWithModel:(PDMoreSocketSingleModel *)model{
    if (!self.socketMoreTest){
        self.socketMoreTest = [[PDNetworkAdapterMoreTest alloc]init];
    }
    [self.socketMoreTest socketConnectionWithSocket:model LoginHost:[AccountModel sharedAccountModel].lifeSocketHost port:[AccountModel sharedAccountModel].lifeSocketPort];
}

-(void)socketDidBackData:(id)responseObject{
    if ([responseObject isKindOfClass:[NSDictionary class]]){

        
        PDBaseSocketRootModel *baseSocket = [[PDBaseSocketRootModel alloc] initWithJSONDict:[responseObject objectForKey:@"data"]];
        if (baseSocket.type == socketType970){                                                              // 【人数变化】
        } else if (baseSocket.type == socketType971){                                                       // 【人刚进来获取数据】
            self.tempSingleModel.guessId = baseSocket.jungleGuessInfo.guess.guessId;
        }
    }
}

-(void)login{
    [self loginManagerWithAccount:[NSString stringWithFormat:@"ios%li",(long)0] pwd:@"qqqqqq" index:0];
}

-(void)loginManagerWithAccount:(NSString *)account pwd:(NSString *)pwd index:(NSInteger)i{
    __weak typeof(self)weakSelf = self;
        NSDictionary *params = @{@"username":account,@"password":pwd,@"token":account};
    [[NetworkAdapter sharedAdapter] fetchWithPath:tokenLogin requestParams:params responseObjectClass:[PDThirdLoginSessionModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            PDThirdLoginSessionModel *singleModel = (PDThirdLoginSessionModel *)responseObject;
            PDMoreSocketSingleModel *socketSingleModel = [[PDMoreSocketSingleModel alloc]init];
            socketSingleModel.memberId = singleModel.session.memberId;
            if (i == 0 ){
                [NetworkAdapter sharedAdapter].tempSocket1 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket1;
            } else if (i == 1){
                [NetworkAdapter sharedAdapter].tempSocket2 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket2;
            } else if (i == 2){
                [NetworkAdapter sharedAdapter].tempSocket3 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket3;
            } else if (i == 3){
                [NetworkAdapter sharedAdapter].tempSocket4 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket4;
            } else if (i == 4){
                [NetworkAdapter sharedAdapter].tempSocket5 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket5;
            } else if (i == 5){
                [NetworkAdapter sharedAdapter].tempSocket6 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket6;
            } else if (i == 6){
                [NetworkAdapter sharedAdapter].tempSocket7 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket7;
            } else if (i == 7){
                [NetworkAdapter sharedAdapter].tempSocket8 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket8;
            } else if (i == 8){
                [NetworkAdapter sharedAdapter].tempSocket9 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket9;
            } else if (i == 9){
                [NetworkAdapter sharedAdapter].tempSocket10 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket10;
            } else if (i == 10){
                [NetworkAdapter sharedAdapter].tempSocket11 = [[SocketConnection alloc]init];
                socketSingleModel.socketConnect =            [NetworkAdapter sharedAdapter].tempSocket11;
            }
            socketSingleModel.token = account;
            socketSingleModel.hasOpen = NO;
            socketSingleModel.socketConnect.delegate = self;

            [self.moreMutableArr addObject:socketSingleModel];
            
            NSInteger index= i+1;
            [self loginWithModel:socketSingleModel];
            [self loginManagerWithAccount:[NSString stringWithFormat:@"ios%li",(long)index] pwd:@"qqqqqq" index:index];
        }
    }];
}

@end
