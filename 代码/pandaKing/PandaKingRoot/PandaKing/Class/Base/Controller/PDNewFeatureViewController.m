//
//  PDNewFeatureViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNewFeatureViewController.h"


#define kImageCount 4
@interface PDNewFeatureViewController()<UIScrollViewDelegate>
@property (nonatomic,strong)UIButton *statusButton;
@property (nonatomic,strong)UIPageControl *pageControl;

@end

@implementation PDNewFeatureViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self createScrollView];
}



#pragma mark createScrollView
-(void)createScrollView{
    UIScrollView *scrollView = [[UIScrollView  alloc]init];
    scrollView.frame = kScreenBounds;
    scrollView.contentSize = CGSizeMake(kImageCount * kScreenBounds.size.width, 0);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    
    // 创建imageView
    for (int i = 0 ; i < kImageCount ; i++){
        [self createIamgeViewWithIndex:i andSuperView:scrollView];
    }
    
    // 创建pageControl
    self.pageControl = [[UIPageControl alloc]init];
    self.pageControl.center = CGPointMake(kScreenBounds.size.width * .5f, kScreenBounds.size.height * .9f);
    self.pageControl.bounds = CGRectMake(0, 0, 100, 0);
    self.pageControl.numberOfPages = kImageCount;
    self.pageControl.userInteractionEnabled = NO;
    self.pageControl.pageIndicatorTintColor=[UIColor colorWithCustomerName:@"浅灰"]; // 设置点的颜色
    self.pageControl.currentPageIndicatorTintColor=[UIColor colorWithCustomerName:@"白"];
    [self.view addSubview:self.pageControl];
}

#pragma mark createImageView
-(void)createIamgeViewWithIndex:(NSInteger)index andSuperView:(UIView *)superView{
    // 1. 创建 imageView
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.frame = CGRectMake(index * kScreenBounds.size.width , 0, kScreenBounds.size.width, kScreenBounds.size.height);
    // 2. 设置图片
    LESScreenMode screenMode = [Tool currentScreenMode];
    NSString *name = [NSString stringWithFormat:@"guide_%ld_%ld", (long)screenMode, (long)index + 1];
    UIImage *image = [UIImage imageNamed:name];
    imageView.image = image == nil? [UIImage imageNamed:@"panda_rootBgView"]:image;
    [superView addSubview:imageView];
    
    if (index == kImageCount - 1){
        [self lastImageSettingInView:imageView];
    }
}

#pragma mark lastImageSetting
-(void)lastImageSettingInView:(UIView *)view{
    view.userInteractionEnabled = YES;
    
    //    // 创建一个button
    self.statusButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.statusButton.selected = YES;
    self.statusButton.backgroundColor = [UIColor clearColor];
    self.statusButton.frame = CGRectMake((kScreenBounds.size.width - LCFloat(174)) / 2., kScreenBounds.size.height - LCFloat(98) - LCFloat(26), LCFloat(174), LCFloat(98));
    __weak typeof(self)weakSelf = self;
    [self.statusButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf animationAlpha];
    }];
    [view addSubview:self.statusButton];
}

-(void)animationAlpha{
    [UIView animateWithDuration:.5f animations:^{
        self.view.alpha = 0;
    } completion:^(BOOL finished) {
        if (self.startBlock){
            self.startBlock();
        }
    }];
}


#pragma mark - actionClick

#pragma mark UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    _pageControl.currentPage = scrollView.contentOffset.x / scrollView.frame.size.width;
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView.contentOffset.x > 2 * scrollView.bounds.size.width){
        self.pageControl.hidden = YES;
    } else {
        self.pageControl.hidden = NO;
    }
}

//#pragma mark - createView
//-(void)createView{
//    self.fixedLabel = [[UILabel alloc]init];
//    self.fixedLabel.backgroundColor = [UIColor clearColor];
//    self.fixedLabel.text = @"我是欢迎页面";
//    self.fixedLabel.frame = CGRectMake(0, 100, kScreenBounds.size.width, 50);
//    [self.view addSubview:self.fixedLabel];
//
//    self.tempButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.tempButton.frame = CGRectMake(100, 200, 100, 100);
//    self.tempButton.backgroundColor = [UIColor blackColor];
//    __weak typeof(self)weakSelf = self;
//    [self.tempButton buttonWithBlock:^(UIButton *button) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (strongSelf.startBlock){
//            strongSelf.startBlock();
//        }
//    }];
//    [self.view addSubview:self.tempButton];
//}


@end
