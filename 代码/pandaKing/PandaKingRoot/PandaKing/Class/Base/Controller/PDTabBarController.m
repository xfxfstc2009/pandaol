//
//  PDTabBarController.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTabBarController.h"
#import "PDPandaRootViewController.h"               // 【首页】
#import "PDShopRootViewController.h"                // 【夺宝、商店】
#import "PDFindRootViewController.h"                // 【发现】
#import "PDCenterViewController.h"                  // 【我的】
#import "PDLotteryRootViewController.h"             // 【竞猜】
#import "PDLotteryGameViewController.h"             // 【赛事猜】
#import "PDLotteryHeroViewController.h"             // 【英雄猜】
#import "PDPandaRoleDetailViewController.h"         // 【我的战场】
//#import "PDLiveGuessRootViewController.h"         // 【主播猜】
#import "PDInformationRootViewController.h"         // 【资讯】
#import "PDAmusementRootViewController.h"           // 【娱乐】
#import "PDZoneViewController.h"                    // 【圈子】
#import "PDShopRootMainViewController.h"
#import "AppDelegate.h"

#import "PDGradientNavBar.h"
#import "PopMenu.h"                                 // 【popMenu】
#import <QuartzCore/QuartzCore.h>
@class AppDelegate;

@interface PDTabBarController()<ZYTabBarDelegate>
@property (nonatomic, strong) PopMenu *popMenu;
@end

@implementation PDTabBarController


-(void)viewDidLoad{
    [super viewDidLoad];
    [self createCustomerTabBar];
    [self pageSetting];
}

-(void)createCustomerTabBar{
    self.customerTabBar = [[UITabBar alloc]init];;
    self.customerTabBar.tintColor = [UIColor whiteColor];
    [self setValue:self.customerTabBar forKeyPath:@"tabBar"];
}

+(instancetype)sharedController{
    static PDTabBarController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[PDTabBarController alloc] init];
    });
    return _sharedAccountModel;
}

#pragma mark page
-(void)pageSetting{
    PDNavigationController *navigationController = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [[navigationController navigationBar] setTranslucent:NO];
    
    UIColor *color1 = [UIColor colorWithCustomerName:@"黑"];
    UIColor *color6 = [UIColor colorWithCustomerName:@"黑"];
    NSArray *navBarArr = [NSArray arrayWithObjects:(id)color6.CGColor,(id)color1.CGColor,nil];

    UIImage *unselectedImage = [UIImage imageNamed:@"icon_tabbar_challenge_nor"];
    UIImage *selectedImage = [UIImage imageNamed:@"icon_tabbar_challenge_hlt"];
    
    // 1. 【首页】
    PDPandaRootViewController *pandaViewController = [PDPandaRootViewController sharedController];
    PDNavigationController *pandaNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [pandaNav setViewControllers:@[pandaViewController]];
    pandaNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    pandaNav.navigationBar.layer.shadowOpacity = 2.0;
    pandaNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    pandaViewController.tabBarItem.tag = 1;
    pandaViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"战场" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // 2. 【资讯】
    PDInformationRootViewController *informationViewController = [[PDInformationRootViewController alloc]init];
    PDNavigationController *informationNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [informationNav setViewControllers:@[informationViewController]];
    informationNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    informationNav.navigationBar.layer.shadowOpacity = 2.0;
    informationNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    informationViewController.tabBarItem.tag = 2;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_information_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_information_hlt"];
    informationViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"资讯" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // 3. 【娱乐】
    PDAmusementRootViewController *amusementRootViewController = [PDAmusementRootViewController sharedController];
    PDNavigationController *amusementNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [amusementNav setViewControllers:@[amusementRootViewController]];
    amusementNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    amusementNav.navigationBar.layer.shadowOpacity = 2.0;
    amusementNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    amusementRootViewController.tabBarItem.tag = 3;
    
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_entertainment_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_entertainment_hlt"];
    amusementRootViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"娱乐" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    // 【消息】
    PDZoneViewController *messageViewController = [[PDZoneViewController alloc]init];
    PDNavigationController *messageNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    [messageNav setViewControllers:@[messageViewController]];
    messageNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    messageNav.navigationBar.layer.shadowOpacity = 2.0;
    messageNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    messageViewController.tabBarItem.tag = 4;
    unselectedImage = [UIImage imageNamed:@"icon_tabbar_msg_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tabbar_msg_hlt"];
    
    messageViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"消息" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];

    
        // 4.【个人中心】
    PDCenterViewController *centerViewController = [PDCenterViewController sharedController];
    PDNavigationController *centerNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    
    [centerNav setViewControllers:@[centerViewController]];
    
    centerNav.navigationBar.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    centerNav.navigationBar.layer.shadowOpacity = 2.0;
    centerNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    centerViewController.tabBarItem.tag = 5;
    unselectedImage = [UIImage imageNamed:@"icon_tab_center_nor"];
    selectedImage = [UIImage imageNamed:@"icon_tab_center_hlt"];
    centerViewController.tabBarItem = [[UITabBarItem alloc] initWithTitle:@"我的" image:[unselectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] selectedImage:[selectedImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    self.viewControllers = @[pandaNav,informationNav,amusementNav,messageNav,centerNav];

    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:[UIColor colorWithCustomerName:@"白"]} forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName:c15} forState:UIControlStateSelected];
    
    
    UITabBar *tabBar = self.tabBar;
//    [UITabBar appearance].translucent = YES;

    [[PDGradientNavBar appearance] setBarTintGradientColors:navBarArr];
    // 设置背景颜色

    if ([tabBar respondsToSelector:@selector(setBarTintColor:)]){
        [tabBar setBarTintColor:[UIColor whiteColor]];
    }else{
        for (UIView *view in tabBar.subviews) {
            if ([NSStringFromClass([view class]) hasSuffix:@"TabBarBackgroundView"]) {
                [view removeFromSuperview];
                break;
            }
        }
    }
    
    UIView *backView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 49)];
    backView.backgroundColor = [UIColor colorWithCustomerName:@"白"];
    [self.tabBar insertSubview:backView atIndex:0];
    self.tabBar.opaque = NO;
}

//#pragma mark - 显示菜单
//- (void)showMenu {
//    UIWindow * window=[[[UIApplication sharedApplication] delegate] window];
//    
//    NSMutableArray *items = [[NSMutableArray alloc] initWithCapacity:3];
//
//    if (![AccountModel sharedAccountModel].isShenhe){
//        MenuItem *menuItem =  [MenuItem itemWithTitle:@"赛事猜" iconName:@"icon_lottery_game" glowColor:COLOR(232, 198, 131, .4f)];
//        
//        [items addObject:menuItem];
//        
//        
//       MenuItem * menuItem1 = [MenuItem itemWithTitle:@"英雄猜" iconName:@"icon_lottery_hero" glowColor: COLOR(232, 198, 131, .4f)];
//        
//        [items addObject:menuItem1];
//        
//        MenuItem *menuItem2 = [MenuItem itemWithTitle:@"主播猜" iconName:@"icon_lottery_hero" glowColor: COLOR(232, 198, 131, .4f)];
//        
//        [items addObject:menuItem2];
//    } else {
//        MenuItem *menuItem =  [MenuItem itemWithTitle:@"我的战场" iconName:@"icon_lottery_game" glowColor:COLOR(232, 198, 131, .4f)];
//        
//        [items addObject:menuItem];
//    }
//    
//    if (!self.popMenu) {
//        self.popMenu = [[PopMenu alloc] initWithFrame:window.bounds items:items];
//        self.popMenu.menuAnimationType = kPopMenuAnimationTypeNetEase;
//        self.popMenu.perRowItemCount = items.count;;
//    }
//    if (self.popMenu.isShowed) {
//        return;
//    }
//    __weak typeof(self)weakSelf = self;
//    weakSelf.popMenu.didSelectedItemCompletion = ^(MenuItem *selectedItem) {
//        if (!weakSelf){
//            return ;
//        }
//        PDTabBarController *tabbarController = (PDTabBarController *)[RESideMenu shareInstance].contentViewController;
//        [tabbarController.customerTabBar.plusBtn pathCenterButtonFold];
//        if (selectedItem == nil){
//            return;
//        }
//
//        if ([selectedItem.title isEqualToString:@"赛事猜"]){
//            PDLotteryGameViewController *lotteryGameViewController = [[PDLotteryGameViewController alloc]init];
//            PDNavigationController *lotteryRootNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
//            [lotteryRootNav setViewControllers:@[lotteryGameViewController]];
//            
//            lotteryRootNav.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
//            lotteryRootNav.navigationBar.layer.shadowOpacity = 2.0;
//            lotteryRootNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
//            
//            [tabbarController presentViewController:lotteryRootNav animated:YES completion:NULL];
//        } else if ([selectedItem.title isEqualToString:@"英雄猜"]){
//            
//            PDLotteryHeroViewController *lotteryHeroViewController = [[PDLotteryHeroViewController alloc]init];
//            PDNavigationController *lotteryRootNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
//            [lotteryRootNav setViewControllers:@[lotteryHeroViewController]];
//            
//            lotteryRootNav.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
//            lotteryRootNav.navigationBar.layer.shadowOpacity = 2.0;
//            lotteryRootNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
//            
//            [tabbarController presentViewController:lotteryRootNav animated:YES completion:NULL];
//        } else if ([selectedItem.title isEqualToString:@"我的战场"]){
//            if ([[AccountModel sharedAccountModel]hasMemberLoggedIn]){
//                PDPandaRoleDetailViewController *pandaRolDetailViewController = [[PDPandaRoleDetailViewController alloc]init];
//                pandaRolDetailViewController.transferLolGameUserId =             [AccountModel sharedAccountModel].lolGameUser;
//                PDNavigationController *lotteryRootNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
//                [lotteryRootNav setViewControllers:@[pandaRolDetailViewController]];
//                
//                lotteryRootNav.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
//                lotteryRootNav.navigationBar.layer.shadowOpacity = 2.0;
//                lotteryRootNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
//                
//                [tabbarController presentViewController:lotteryRootNav animated:YES completion:NULL];
//            } else {
//                [[PDAlertView sharedAlertView] showAlertWithTitle:@"请登录" conten:@"请登录后进行查看" isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
//                    [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
//                }];
//            }
//        } else if ([selectedItem.title isEqualToString:@"主播猜"]){
//            PDLiveGuessRootViewController *pandaRolDetailViewController = [[PDLiveGuessRootViewController alloc]init];
//            PDNavigationController *lotteryRootNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
//            [lotteryRootNav setViewControllers:@[pandaRolDetailViewController]];
//            
//            lotteryRootNav.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
//            lotteryRootNav.navigationBar.layer.shadowOpacity = 2.0;
//            lotteryRootNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
//            
//            [tabbarController presentViewController:lotteryRootNav animated:YES completion:NULL];
//        }
//    };
//    
//    CGPoint startPoint = CGPointMake(kScreenBounds.size.width / 2., self.customerTabBar.orgin_y + LCFloat(60));
//    [_popMenu showMenuAtView:window startPoint:startPoint endPoint:startPoint];
//    
//}

#pragma mark - 跳转
+(void)directToProductWithController:(UIViewController *)controller{
    PDTabBarController *tabbarController = (PDTabBarController *)[RESideMenu shareInstance].contentViewController;
    [tabbarController setSelectedIndex:PDTabBarControllerViewControllerIndexHappy];
    
    controller.hidesBottomBarWhenPushed = YES;
    PDNavigationController *personalCenterNavi = [tabbarController.viewControllers objectAtIndex:2];
    NSMutableArray *sourceMutableArr = [NSMutableArray arrayWithArray:personalCenterNavi.viewControllers];
    [sourceMutableArr addObject:controller];
    personalCenterNavi.viewControllers = sourceMutableArr;
}



-(void)centerItemBtnClick{
//    [self showMenu];
}

//-(void)showGestureWithItemsArr:(NSArray *)itemsArr{
//    PDGuideRootViewController *guideRootViewController = [[PDGuideRootViewController alloc]init];
//    
//    MenuItem *item = [itemsArr objectAtIndex:0];
//    MenuItem *item1 = [itemsArr objectAtIndex:1];
//    
//    // 1. 创建区服
//    PDGuideSingleModel *paopaoModel = [[PDGuideSingleModel alloc]init];
//    paopaoModel.transferShowFrame = CGRectMake(item.toFrame.origin.x, item.toFrame.origin.y - ((MAX(item.toFrame.size.width, item.toFrame.size.height) - MIN(item.toFrame.size.width, item.toFrame.size.height)) / 2.), MAX(item.toFrame.size.width, item.toFrame.size.height), MAX(item.toFrame.size.width, item.toFrame.size.height));
//    paopaoModel.index = 1;
//    paopaoModel.text = @"最精彩的比赛快开始了快来竞猜啊";
//    paopaoModel.guideType = GuideGuesterTypeNormal;
//    
//    PDGuideSingleModel *paopaoModel1 = [[PDGuideSingleModel alloc]init];
//    paopaoModel1.transferShowFrame = CGRectMake(item1.toFrame.origin.x, item1.toFrame.origin.y - ((MAX(item1.toFrame.size.width, item1.toFrame.size.height) - MIN(item1.toFrame.size.width, item1.toFrame.size.height)) / 2.), MAX(item1.toFrame.size.width, item1.toFrame.size.height), MAX(item1.toFrame.size.width, item1.toFrame.size.height));
//    paopaoModel1.index = 1;
//    paopaoModel1.text = @"少年快来感受下心的竞猜体验吧";
//    paopaoModel1.guideType = GuideGuesterTypeNormal;
//    
//    [guideRootViewController showInView:self.parentViewController withViewActionArr:@[paopaoModel,paopaoModel1]];
//    
//    __weak typeof(self)weakSelf = self;
//    [guideRootViewController lotteryTabBarManager:^{
//        [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeGuess block:NULL];
//    }];
//}

@end
