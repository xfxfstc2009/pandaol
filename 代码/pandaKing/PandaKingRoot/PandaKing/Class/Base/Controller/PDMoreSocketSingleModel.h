//
//  PDMoreSocketSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "SocketConnection.h"
@interface PDMoreSocketSingleModel : FetchModel

@property (nonatomic,copy)NSString *memberId;
@property (nonatomic,copy)NSString *token;
@property (nonatomic,copy)NSString *pwd;
@property (nonatomic,assign)BOOL hasOpen;
@property (nonatomic,strong)SocketConnection *socketConnect;
@property (nonatomic,copy)NSString *guessId;
@end
