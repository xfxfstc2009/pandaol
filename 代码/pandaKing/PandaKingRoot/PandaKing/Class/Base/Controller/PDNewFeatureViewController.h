//
//  PDNewFeatureViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDNewFeatureViewController : AbstractViewController

@property (nonatomic, copy) void (^startBlock)();         // 是否登录

@end
