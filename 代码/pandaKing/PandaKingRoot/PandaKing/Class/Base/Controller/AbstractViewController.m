//
//  AbstractViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import <objc/runtime.h>
#import "PDLoginMainViewController.h"
#import "GWPopImgView.h"
#import <pop/POP.h>
// trotingView
#import "CCZTrotingLabel.h"
#import "UITabBarController+Autorotate.h"
#import "UINavigationController+Autorotate.h"
// info
#import "PDSideMenu.h"
// 消息控制器
#import "PDMessageViewController.h"

typedef struct {
    CGFloat progress;
    CGFloat toValue;
    CGFloat currentValue;
} AnimationInfo;

static char buttonActionBlockKey;
@interface AbstractViewController()<PDLoginSocketDelegate,PDNetworkAdapterDelegate>{
    BOOL hasShowLogin;          /**< 判断是否登录*/
}
@property (nonatomic,strong)UIView *barTitleView;
@property (nonatomic,strong)LTMorphingLabel *barMainTitleLabel;
@property (nonatomic,strong)UILabel *barSubTitleLabel;
@property (nonatomic,strong)GWPopImgView *topMenu;                          /**< 上层的menu*/
// trot
@property (nonatomic, strong) CCZTrotingLabel *trotingTopLabel;
@property (nonatomic,strong)UIButton *absLeftButton;
@end

@implementation AbstractViewController

-(void)dealloc{
    DLog(@"释放了");
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    
    if (self != [self.navigationController.viewControllers firstObject]){
        if (!([self isKindOfClass:[PDLaungchViewController class]] || [self isKindOfClass:[PDShopRootSubViewController class]] || [self isKindOfClass:[PDMallRootSubViewController class]])){
            [[PDMainTabbarViewController sharedController] setBarHidden:YES animated:YES];
        }
    } else {
        [[PDMainTabbarViewController sharedController] setBarHidden:NO animated:YES];
    }
    
    // 2. 增加左侧头像
    [self updateLeftBtn];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (!self.hasCancelSocket){
        if ([NetworkAdapter sharedAdapter]){
            [NetworkAdapter sharedAdapter].delegate = self;
        }
    }
}

-(void)viewDidLoad {
    [super viewDidLoad];
    [self createBarTitleView];
    [self setupNavBarStatus];
    [self leftButtonStatus];                                        // 左侧按钮状态
    [self __addTrotView];
    if (!self.hasCancelPanDismiss){
        [self.navigationController setEnableBackGesture:YES];
    } else {
        [self.navigationController setEnableBackGesture:NO];
    }
}

#pragma mark - PDNetworkAdapterDelegate
-(void)pandaNotLoginNeedLoginManager{
    if (hasShowLogin == YES){
        return;
    }
    [self showLoginViewControllerWithBlock:NULL];
}

-(void)socketDidBackData:(id)responseObject{
    __weak typeof(self)weakSelf = self;
    
    if ([responseObject isKindOfClass:[NSString class]]){
        
    } else if ([responseObject isKindOfClass:[NSDictionary class]]){
        
        PDBaseSocketRootModel *responseModelObject = [[PDBaseSocketRootModel  alloc] initWithJSONDict:[responseObject objectForKey:@"data"]];
        if (responseModelObject.type == socketType780){
            
        } else if (responseModelObject.type == socketType781){          // 激活
            if (responseModelObject.activate){            // 激活成功
                [[PDAlertView sharedAlertView] showNormalAlertTitle:@"绑定提示" content:@"召唤师认证成功，您可换回其他头像" isClose:YES btnArr:@[@"确定"] btnCliock:^(NSInteger buttonIndex, NSString *inputInfo) {
                    if (!weakSelf){
                        return ;
                    }
                    [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
                    if (responseModelObject.oldMemberBind){
                        [[PDPandaRootViewController sharedController] sendRequestToGetPersonalInfoHasAnimation:YES block:NULL];
                    } else {
                        [[PDPandaRootViewController sharedController] uploadCurrentRenzhengStatus:@"activated"];
                    }
                    [[PDCenterViewController sharedController] fetchMemberGameUser];
                }];
            } else {                                        // 激活失败
                [weakSelf activityFailManagerWithMsg:[responseObject objectForKey:@"msg"]];
            }
            
        } else if (responseModelObject.type == socketType801) {
            NSDictionary *resp = (NSDictionary *)responseObject;
            if ([resp.allKeys containsObject:@"data"]){
                NSDictionary *data = resp[@"data"];
                NSString *name = data[@"nickname"];
                NSString *productName = data[@"treasureName"];
                CCZTrotingAttribute *attribute = [[CCZTrotingAttribute alloc] init];
                attribute.text = [NSString stringWithFormat:@"恭喜用户%@获得%@",name, productName];
                NSMutableAttributedString *strAtt = [[NSMutableAttributedString alloc] initWithString:attribute.text attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
                [strAtt addAttribute:NSForegroundColorAttributeName value:c14 range:NSMakeRange(4, name.length)];
                attribute.attribute = strAtt;
                [self.trotingTopLabel addTrotingAttributes:@[attribute]];
                
                [[[UIApplication sharedApplication].delegate window] bringSubviewToFront:self.trotingTopLabel];
            }
        }
    }
}

-(void)activitySuccessManager{
    __weak typeof(self)weakSelf = self;
    [[PDAlertView sharedAlertView] showNormalAlertTitle:@"绑定提示" content:@"恭喜，您已通过召唤师认证，现已解锁全部功能" isClose:YES btnArr:@[@"确定"] btnCliock:^(NSInteger buttonIndex, NSString *inputInfo) {
        if (!weakSelf){
            return ;
        }
        [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
    }];
}




-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    //1. 判断是否有比赛

}

#pragma mark - Action
-(void)pushViewController:(nonnull UIViewController *)controller animated:(BOOL)animated{
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:animated];
}

#pragma mark -
#pragma mark  NavigationBar
-(void)setupNavBarStatus{
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.modalPresentationCapturesStatusBarAppearance = YES;
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    self.view.autoresizesSubviews = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.view.backgroundColor = BACKGROUND_VIEW_COLOR;
    //去掉navigationBar底部线
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc]init]];
}

-(void)leftButtonStatus{
    __weak typeof(self)weakSelf = self;
    if (self != [self.navigationController.viewControllers firstObject]){
        [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_main_back"] barHltImage:[UIImage imageNamed:@"icon_main_back"] action:^{
             [weakSelf.navigationController popViewControllerAnimated:YES];
        }];
    } else {
        self.absLeftButton = [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@""] barHltImage:[UIImage imageNamed:@""] action:^{
            [[RESideMenu shareInstance] presentLeftMenuViewController];
        }];
    }
}

-(void)updateLeftBtn{
    if (self == [self.navigationController.viewControllers firstObject] && (![self isKindOfClass:[PDFirstChooseGameTypeViewController class]]) && (![self isKindOfClass:[PDYueZhanReleaseViewController class]])){
        if ([AccountModel sharedAccountModel].userAvatarImg){
            
            [self.absLeftButton setImage:[AccountModel sharedAccountModel].userAvatarImg forState:UIControlStateNormal];
            [self.absLeftButton sizeThatFits:CGSizeMake(LCFloat(30), LCFloat(30))];
        }
    }
}

- (void)createBarTitleView
{
    _barTitleView = [[UIView alloc] initWithFrame:CGRectMake((kScreenBounds.size.width - BAR_TITLE_MAX_WIDTH) / 2., 0, BAR_TITLE_MAX_WIDTH, 44)];
    _barTitleView.backgroundColor = [UIColor clearColor];
    _barTitleView.clipsToBounds = YES;
    
    _barMainTitleLabel = [[LTMorphingLabel alloc] initWithFrame:_barTitleView.bounds];
    _barMainTitleLabel.backgroundColor = [UIColor clearColor];
    _barMainTitleLabel.font = BAR_MAIN_TITLE_FONT;
    _barMainTitleLabel.adjustsFontSizeToFitWidth = YES;
    _barMainTitleLabel.textColor = [UIColor whiteColor];
    _barMainTitleLabel.text = @"";
    [_barTitleView addSubview:_barMainTitleLabel];
    
    _barSubTitleLabel = [[UILabel alloc] initWithFrame:CGRectZero];
    _barSubTitleLabel.backgroundColor = [UIColor clearColor];
    _barSubTitleLabel.font = BAR_SUB_TITLE_FONT;
    _barSubTitleLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    [_barTitleView addSubview:_barSubTitleLabel];
    
    
    self.navigationItem.titleView = _barTitleView;
}

- (void)setBarMainTitle:(NSString *)barMainTitle {
    _barMainTitle = [barMainTitle copy];
    _barMainTitleLabel.morphingEffect = LTMorphingEffectSparkle;
    _barMainTitleLabel.text = _barMainTitle;
    
    [_barMainTitleLabel sizeToFit];
    CGRect rect = _barMainTitleLabel.bounds;
    rect.size.width = MIN(rect.size.width, BAR_TITLE_MAX_WIDTH);
    _barMainTitleLabel.bounds = rect;
    
    [self resetBarTitleView];
}


- (void)setBarSubTitle:(NSString *)barSubTitle {
    _barSubTitle = [barSubTitle copy];
    _barSubTitleLabel.text = _barSubTitle;
    
    [_barSubTitleLabel sizeToFit];
    CGRect rect = _barSubTitleLabel.bounds;
    rect.size.width = MIN(rect.size.width, BAR_TITLE_MAX_WIDTH);
    _barSubTitleLabel.bounds = rect;
    
    [self resetBarTitleView];
}

- (void)resetBarTitleView {
    CGSize mainTitleSize = _barMainTitleLabel.bounds.size;
    CGSize subTitleSize = _barSubTitleLabel.bounds.size;
    
    CGFloat titleViewHeight = mainTitleSize.height + subTitleSize.height;
    if (_barMainTitle.length && _barSubTitle.length) {
        titleViewHeight += BAR_TITLE_PADDING_TOP;
    }
    
    _barTitleView.bounds = CGRectMake(0., 0., BAR_TITLE_MAX_WIDTH, titleViewHeight);
    if ([Tool isEmpty:_barSubTitle]) {
        if (titleViewHeight < 44) {
            titleViewHeight = 44;
        }
        _barTitleView.bounds = CGRectMake(0., 0., BAR_TITLE_MAX_WIDTH, titleViewHeight);
        _barMainTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, 22);
    } else {
        _barMainTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, _barMainTitleLabel.bounds.size.height*0.5);
        _barSubTitleLabel.center = CGPointMake(BAR_TITLE_MAX_WIDTH*0.5, titleViewHeight - _barSubTitleLabel.bounds.size.height*0.5);
    }
}

#pragma mark - Button
- (void)hidesBackButton {
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
}

- (UIButton *)leftBarButtonWithTitle:(NSString *)title barNorImage:(UIImage *)norImage barHltImage:(UIImage *)hltImage action:(void(^)(void))actionBlock {
    UIButton *button = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(button, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.leftBarButtonItem = barButtonItem;
    
    
    return button;
}

- (UIButton *)rightBarButtonWithTitle:(NSString *)title barNorImage:(UIImage *)norImage barHltImage:(UIImage *)hltImage action:(void(^)(void))actionBlock {
    UIButton *button = [self buttonWithTitle:title buttonNorImage:norImage buttonHltImage:hltImage];
    objc_setAssociatedObject(button, &buttonActionBlockKey, actionBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    UIBarButtonItem *barButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = barButtonItem;
    
    return button;
}

- (UIButton *)buttonWithTitle:(NSString *)title buttonNorImage:(UIImage *)norImage buttonHltImage:(UIImage *)hltImage {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    [button setImage:norImage forState:UIControlStateNormal];
    [button setImage:hltImage forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithCustomerName:@"白"]forState:UIControlStateHighlighted];
    [button setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateDisabled];
    [button addTarget:self action:@selector(actionButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    button.backgroundColor = [UIColor clearColor];
    button.titleLabel.font = BAR_BUTTON_FONT;
    [button sizeToFit];
    
    return button;
}

- (void)actionButtonClicked:(UIButton *)sender {
    void (^actionBlock) (void) = objc_getAssociatedObject(sender, &buttonActionBlockKey);
    actionBlock();
}

#pragma mark - TabBar
- (void)hidesTabBarWhenPushed {
    [self setHidesBottomBarWhenPushed:YES];
}

- (void)hidesTabBar:(BOOL)hidden animated:(BOOL)animated {
    UITabBarController *tabBarController = self.tabBarController;
    UITabBar *tabBar = tabBarController.tabBar;
    if (!tabBarController || (tabBar.hidden == hidden)) {
        return;
    }
    
    CGFloat tabBarHeight = tabBar.bounds.size.height;
    CGFloat adjustY = hidden ? tabBarHeight : -tabBarHeight;
    
    if (!hidden) {
        tabBar.hidden = NO;
    }
    
    __weak typeof(self) weakSelf = self;
    [UIView animateWithDuration:animated ? 0.3 : 0. animations:^{
        CGRect rect = tabBar.frame;
        rect.origin.y += adjustY;
        tabBar.frame = rect;
        
        for (UIView *view in tabBarController.view.subviews) {
            if ([NSStringFromClass([view class]) hasSuffix:@"TransitionView"]) {
                if (!IS_IOS7_LATER) {
                    CGRect rect = view.frame;
                    rect.size.height += adjustY;
                    view.frame = rect;
                }
                view.backgroundColor = weakSelf.view.backgroundColor;
            }
        }
    } completion:^(BOOL finished) {
        tabBar.hidden = hidden;
        CGRect rect = weakSelf.view.frame;
        rect.size.height += adjustY;
        weakSelf.view.frame = rect;
    }];
}



#pragma mark - SocketLink


#pragma mark - 自动登录
- (void)authorizeWithCompletionHandler:(void(^)())handler{
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        handler();
    } else {
        PDLoginMainViewController *loginMainViewController = [[PDLoginMainViewController alloc]init];
        loginMainViewController.dismissBlock = ^(){
            handler();
        };
        UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginMainViewController];
        [[RESideMenu shareInstance] presentViewController:nav animated:YES completion:NULL];
    }
}




#pragma mark - 进行登录
-(void)showLoginViewControllerWithBlock:(void(^)())block{
    if ([self isKindOfClass:[PDLoginMainViewController class]] ){
        return;
    }
    hasShowLogin = YES;
    PDLoginMainViewController *loginViewController = [[PDLoginMainViewController alloc]init];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginViewController];
    __weak typeof(self)weakSelf = self;
    loginViewController.dismissBlock = ^(){
        if ([self isKindOfClass:[PDCenterViewController class]]){
            [[PDPandaRootViewController sharedController] sendRequestToGetPersonalInfoHasAnimation:NO block:NULL];
        }
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSlef = weakSelf;
        strongSlef -> hasShowLogin = NO;
        if (block){
            block();
        }
        
        // 1. 执行动画
        PDSliderViewController *sliderController = (PDSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
        // 2.
        [sliderController sliderLoginManager];
    };
    [self presentViewController:nav animated:YES completion:NULL];
}

#pragma mark -- 获奖信息
- (void)__addTrotView {
    static CCZTrotingLabel *label;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        label = [[CCZTrotingLabel alloc] initWithFrame:CGRectMake(0, 64, kScreenBounds.size.width, LCFloat(35))];
        label.backgroundColor = [UIColor clearColor];
        label.hideWhenStoped = YES;
        label.repeatTextArr = NO;
        label.pause = 2;
        label.backgroundImage = [UIImage imageNamed:@"icon_popview_bg"];
        CGFloat top = 10; // 顶端盖高度
        CGFloat bottom = 10; // 底端盖高度
        CGFloat left = 10; // 左端盖宽度
        CGFloat right = 10; // 右端盖宽度
        UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
        label.backgroundImage = [label.backgroundImage resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch];
        
        UIButton *leftIView = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, LCFloat(45), 35)];
        leftIView.enabled = NO;
        [leftIView setImage:[UIImage imageNamed:@"icon_message_win"] forState:UIControlStateNormal];
        label.leftView = leftIView;
        
        NSEnumerator *windowEnnumtor = [UIApplication sharedApplication].windows.reverseObjectEnumerator;
        for (UIWindow *window in windowEnnumtor) {
            BOOL isOnMainScreen = window.screen == [UIScreen mainScreen];
            BOOL isVisible      = !window.hidden && window.alpha > 0;
            BOOL isLevelNormal  = window.windowLevel == UIWindowLevelNormal;
            
            if (isOnMainScreen && isVisible && isLevelNormal) {
                [window addSubview:label];
            }
        }
        self.trotingTopLabel = label;
    });
}

#pragma mark - 激活召唤师
-(void)activityFailManagerWithMsg:(NSString *)msg{
    __weak typeof(self)weakSelf = self;
    if ([PDAlertView sharedAlertView].alertArr.count) {
        [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
    }
    [PDHUD dismiss];
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"认证提示" conten:msg isClose:NO btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
        [[PDPandaRootViewController sharedController] uploadCurrentRenzhengStatus:@"bind"];
    }];
}


#pragma mark - 主播猜
-(void)liveSwipeManagerWithIsLeft:(BOOL)isLeft{
//    if (isLeft){        // 左移
//        PDLivePlayerSubViewRightController *playerRightViewController = [[PDLivePlayerSubViewRightController alloc]init];
//        playerRightViewController.isHasGesture = YES;
//        [playerRightViewController showInView:self.parentViewController];
//    } else {
//        PDLivePlayerSubViewLeftController *playerLeftViewController = [[PDLivePlayerSubViewLeftController alloc]init];
//        playerLeftViewController.isHasGesture = YES;
//        [playerLeftViewController showInView:self.parentViewController];
//    }
}



#pragma mark - 旋转
- (BOOL)shouldAutorotate{
    //是否允许转屏
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //viewController所支持的全部旋转方向
    return UIInterfaceOrientationMaskPortrait ;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    //viewController初始显示的方向
    return UIInterfaceOrientationPortrait;
}


@end
