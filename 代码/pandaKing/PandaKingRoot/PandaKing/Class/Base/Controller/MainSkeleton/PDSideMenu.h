//
//  PDSideMenu.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDSideMenu : NSObject

+(RESideMenu *)setupSlider;

@end
