//
//  PDSliderViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSliderViewController.h"
#import "PDLoginMainViewController.h"
#import "PDTabBarController.h"

#import "PDSliderTableViewCell.h"
#import "PDLoginMainViewController.h"                           // 登录
//#import "PDBindingListViewController.h"                         // 绑定详情
#import "PDSliderBottomView.h"
#import "PDMainTabbarViewController.h"
#import "PDSliderSingleModel.h"
#import "PDGradientNavBar.h"
#import "PDFirstChooseGameTypeViewController.h"
#import "PDEditViewController.h"
#import "PDCenterSettingViewController.h"                       // 进行设置
#import "PDInternetCafesRootViewController.h"                   // 附近网吧
#import "PDInviteViewController.h"                              // 邀请有奖
#import "PDMyTaskViewController.h"                              // 我的任务
#import "PDBackpackViewController.h"                            // 我的背包
#import "PDBindingInfoViewController.h"                         // 绑定召唤师
#import "PDPandaPersonCenterCell.h"
#import "PDCenterPersonViewController.h"                 // 个人信息
#import "PDPandaRoleDetailViewController.h"
#import "PDTopUpViewController.h"
#import "PDFirstChooseGameTypeViewController.h"
#import "PDWalletViewController.h"

@interface PDSliderViewController()<UITableViewDelegate,UITableViewDataSource,RESideMenuDelegate>{
    PDAccountSliderOtherInfoModel *accountSliderSingleModel;
}
@property (nonatomic,strong,nullable)TNSexyImageUploadProgress *imageUploadProgress;             /**< progress*/
@property (nonatomic,strong,nullable)PDImageView *headerImageView;                               /**< 创建view */

@end

@implementation PDSliderViewController

-(void)dealloc{
    NSLog(@"释放");
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 1. 获取数据
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        [self sendRequestToGetJingyanInfo];
    }
    
    if (![AccountModel sharedAccountModel].hasFirstAnimation){
        if(self.sliderHeaderView){
            self.sliderHeaderView.transferAccountModel = [AccountModel sharedAccountModel];
        }
    }
    
    if ([AccountModel sharedAccountModel].isShenhe && self.sliderHeaderView.chongzhiButton){
        self.sliderHeaderView.chongzhiButton.hidden = YES;
    }
}



-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                                             /**< 页面初始化*/
    [self createHeaderView];                                        /**< 创建头部的view*/
    [self createBottomView];
    [self arrayWithInit];                                           /**< 数组初始化*/
    [self createTableView];                                         /**< 创建tableView*/
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = RGB(29, 30, 31, 1);
    [RESideMenu shareInstance].delegate = self;
}

-(void)arrayWithInit{
    self.sliderMutableArr = [NSMutableArray array];
}
 
#pragma mark - 创建头部view
-(void)createHeaderView{
    self.sliderHeaderView = [[PDSliderHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width / 4. * 3, [PDSliderHeaderView calculationCellHeight])];
    __weak typeof(self)weakSelf = self;
    [self.sliderHeaderView touchManagerWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){                 // 如果账号登录
            PDCenterPersonViewController *editViewController = [[PDCenterPersonViewController alloc]init];
            editViewController.isMe = YES;
            [strongSelf sliderPushController:editViewController];
        } else {                                                                    // 未登录状态
            PDLoginMainViewController *loginMainViewController = [[PDLoginMainViewController alloc]init];
            loginMainViewController.dismissBlock = ^(){
                [strongSelf sliderLoginManager];
            };
            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:loginMainViewController];
            [self.sideMenuViewController.contentViewController presentViewController:nav animated:YES completion:NULL];
        }
        [strongSelf.sideMenuViewController hideMenuViewController];
    }];
    
    [self.sliderHeaderView chongzhiActionClick:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDTopUpViewController *popViewController = [[PDTopUpViewController alloc]init];
        [strongSelf sliderPushController:popViewController];
    }];
    [self.view addSubview:self.sliderHeaderView];
}

#pragma mark - createBottomView
-(void)createBottomView{
    self.bottomView = [[PDSliderBottomView alloc]initWithFrame:CGRectMake(0, kScreenBounds.size.height - 100, kScreenBounds.size.width / 2. + kScreenBounds.size.width / 4., 100)];
    self.bottomView.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [self.bottomView gameTypeActionClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.sideMenuViewController hideMenuViewController];
        [[PDMainTabbarViewController sharedController].postViewController updatePosts];
    }];
    
    [self.view addSubview:self.bottomView];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.sliderTableView){
        self.sliderTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.sliderTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.sliderTableView.delegate = self;
        self.sliderTableView.dataSource = self;
        self.sliderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.sliderTableView.backgroundColor = [UIColor clearColor];
        self.sliderTableView.backgroundView = nil;
        self.sliderTableView.bounces = YES;
        self.sliderTableView.opaque = NO;
        [self.view addSubview:self.sliderTableView];
        
        self.sliderTableView.orgin_y = CGRectGetMaxY(self.sliderHeaderView.frame);
        self.sliderTableView.size_height = kScreenBounds.size.height - CGRectGetMaxY(self.sliderHeaderView.frame) - CGRectGetHeight(self.bottomView.frame);
        self.sliderTableView.size_width = kScreenBounds.size.width / 2. + kScreenBounds.size.width / 4.;
    }

}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sliderMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.sliderMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    PDSliderSingleModel *singleModel = [[self.sliderMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if ([singleModel.appFunctionName isEqualToString:@"绑定召唤师"]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDPandaPersonCenterCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDPandaPersonCenterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        if ([AccountModel sharedAccountModel].gameType == GameTypeCS){                // 绑定了cs
            cellWithRowTwo.transferLolinfo = nil;
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){          // 绑定了dota2
            cellWithRowTwo.transferLolinfo = nil;
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){           // 绑定king
            cellWithRowTwo.transferLolinfo = nil;
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){            // 绑定lol
            cellWithRowTwo.transferLolinfo = [AccountModel sharedAccountModel].lolInfo;
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePubg){
            cellWithRowTwo.transferLolinfo = nil;
        }
        
        return cellWithRowTwo;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDSliderTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDSliderTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor clearColor];
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferSingleModel = singleModel;
        cellWithRowOne.transferOthersingleModel = accountSliderSingleModel;
        
        return cellWithRowOne;
    }
}

#pragma mark - UITableDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    PDSliderSingleModel *singleModel = [[self.sliderMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if ([singleModel.appFunctionName isEqualToString:@"绑定召唤师"]){
        return [PDPandaPersonCenterCell calculationCellHeight];
    } else {
        return [PDSliderTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PDSliderSingleModel *singleModel = [[self.sliderMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    
    PDNavigationController *navigationController = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
    if ([singleModel.appFunctionName isEqualToString:@"绑定召唤师"]){
        if ([AccountModel sharedAccountModel].gameType == GameTypeCS){                // 绑定了cs
            [PDHUD showConnectionErr:@"暂未开放CS"];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){          // 绑定了dota2
            [PDHUD showConnectionErr:@"暂未开放DotA2"];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){           // 绑定king
            [PDHUD showConnectionErr:@"暂未开放王者荣耀"];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){            // 绑定lol
            if ([AccountModel sharedAccountModel].lolInfo){                 // 表示已经绑定
                PDPandaRoleDetailViewController *pandaRoleDetailViewController = [[PDPandaRoleDetailViewController alloc]init];
                pandaRoleDetailViewController.transferLolGameUserId = [AccountModel sharedAccountModel].lolInfo.lolGameUserId;
                __weak typeof(self)weakSelf = self;
                [pandaRoleDetailViewController bindingCancelWithBlock:^{
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf cancelBindingWithType:GameTypeLoL];
                }];
                
                [self sliderPushController:pandaRoleDetailViewController];
            } else {
                PDBindingInfoViewController *bingdingViewController = [[PDBindingInfoViewController alloc]init];
                __weak typeof(self)weakSelf = self;
                [bingdingViewController bindingDidEndWithBlock:^(BOOL isbind, BOOL isActivity) {
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf bangdingGameRoleWithGameType:GameTypeLoL];
                }];
                [self sliderPushController:bingdingViewController];
                
            }
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePubg){
            [PDHUD showConnectionErr:@"暂未开放绝地求生"];
        }
        
        return;
    }
    
    if (!singleModel.direct){                // 需要跳转到页面
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:singleModel.url];
        [navigationController setViewControllers:@[webViewController]];
        [self.sideMenuViewController setContentViewController:navigationController animated:YES];
        [self.sideMenuViewController hideMenuViewController];
        return;
    }
    
    if (singleModel.showTitle.length){
        [[PDAlertView sharedAlertView] showAlertWithTitle:@"提示" conten:singleModel.showTitle isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
            [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
        }];
        [self.sideMenuViewController hideMenuViewController];
        return;
    }
    
    if ([singleModel.appFunctionName isEqualToString:@"我的背包"]){
        PDBackpackViewController *backpackViewController = [[PDBackpackViewController alloc] init];
        [self sliderPushController:backpackViewController];
    } else if ([singleModel.appFunctionName isEqualToString:@"我的任务"]){
        PDMyTaskViewController *myTaskViewController = [[PDMyTaskViewController alloc]init];
        [self sliderPushController:myTaskViewController];
    } else if ([singleModel.appFunctionName isEqualToString:@"邀请有奖"]){
        PDInviteViewController *inviteViewController = [[PDInviteViewController alloc]init];
        [self sliderPushController:inviteViewController];
    } else if ([singleModel.appFunctionName isEqualToString:@"战绩查询"]){
        PDFirstChooseGameTypeViewController *VC = [[PDFirstChooseGameTypeViewController alloc]init];
        [self sliderPushController:VC];
        return;
    } else if ([singleModel.appFunctionName isEqualToString:@"附近网吧"]){
        PDInternetCafesRootViewController *netbarViewController = [[PDInternetCafesRootViewController alloc]init];
        netbarViewController.transferInternetCafeType = InternetCafesTypeNearBy;
        [self sliderPushController:netbarViewController];
    } else if ([singleModel.appFunctionName isEqualToString:@"设置"]){
        PDCenterSettingViewController *settingViewController = [[PDCenterSettingViewController alloc]init];
        [self sliderPushController:settingViewController];
    } else if ([singleModel.appFunctionName isEqualToString:@"我的钱包"]){
        PDWalletViewController *walletViewController = [[PDWalletViewController alloc]init];
        [self sliderPushController:walletViewController];
    }
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.sliderTableView) {
        PDSliderSingleModel *singleModel = [[self.sliderMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        if ([singleModel.appFunctionName isEqualToString:@"绑定召唤师"]){
            [cell addSeparatorLineWithTypeWithAres:SeparatorTypeBottom andUseing:@"slider"];
        }
    }
}

#pragma mark 登录成功操作

-(void)sliderLoginManager{
    if (!self.headerImageView){
        self.headerImageView = [[PDImageView alloc]init];
    }
    
    self.headerImageView.frame = CGRectMake(0, 0, LCFloat(150), LCFloat(150));
    [self loginSuccessManager1];
}


-(void)loginSuccessManager{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.sideMenuViewController hideMenuViewController];
    });
    [[PDMainTabbarViewController sharedController].amusementViewController showFirstChooseGameInfo];
}


#pragma mark - 登录动画
-(void)loginSuccessManager1{
    [self.headerImageView uploadImageWithURL:[AccountModel sharedAccountModel].memberInfo.avatar placeholder:nil callback:^(UIImage *image) {
        
        self.imageUploadProgress = [[TNSexyImageUploadProgress alloc] init];
        self.imageUploadProgress.radius = LCFloat(100);
        self.imageUploadProgress.progressBorderThickness = -10;
        self.imageUploadProgress.trackColor = [UIColor blackColor];
        self.imageUploadProgress.progressColor = [UIColor colorWithCustomerName:@"黑"];
        self.imageUploadProgress.imageToUpload = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
        [self.imageUploadProgress show];
        [self performSelector:@selector(showSliderWithAnimation) withObject:nil afterDelay:.5f];
    }];
}

#pragma mark - 显示
-(void)showSliderWithAnimation{
    [[RESideMenu shareInstance] presentLeftMenuViewController];
    [self performSelector:@selector(loginSuccessAnimationWithProView:) withObject:self.imageUploadProgress afterDelay:.3f];
}

#pragma mark - Animation Login
-(void)loginSuccessAnimationWithProView:(TNSexyImageUploadProgress *)progressView{
    PDSliderViewController *leftViewController = (PDSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
    // 2. 寻找到view 的位置
    CGRect convertFrame = [leftViewController.sliderHeaderView convertRect:leftViewController.sliderHeaderView.headerImageView.frame toView:self.view.window];
    progressView.imageView.backgroundColor = [UIColor whiteColor];
    
    // 3. 创建一个副本图像
    UIImageView *animationImageView = [[UIImageView alloc]init];
    animationImageView.image = progressView.imageView.image;
    animationImageView.frame = progressView.imageView.frame;
    [self.view.window addSubview:animationImageView];
    
    [progressView outroAnimation];
    [UIView animateWithDuration:.9  delay:.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        animationImageView.frame = convertFrame;
    } completion:^(BOOL finished) {
        animationImageView.hidden = YES;
        [animationImageView removeFromSuperview];
        [leftViewController.sliderHeaderView loginManager];
        [UIView animateWithDuration:1 animations:^{
            [leftViewController loginSuccessManager];
        }];
        [AccountModel sharedAccountModel].hasFirstAnimation = NO;
    }];
}



#pragma mark RESideMenu Delegate
- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController {
    NSLog(@"视图将要展开: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.bottomView showAnimation];
    });
    [self statusBarHiden:YES];
    
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        [self.sliderHeaderView showAnimationWithBambooAndGold];
    }
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController {
    NSLog(@"视图将要关闭: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController {
    [self statusBarHiden:NO];
    [self.bottomView stopAnimation];
    // 修改tabbar图片
    [self uploadTabbarIconsManager];
    
}

-(void)uploadTabbarIconsManager{
    // 进行判断，并且修改图片
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        [[PDMainTabbarViewController sharedController] updateItemImg:[UIImage imageNamed:@"icon_tabbar_lol_yule_nor"] atIndex:0];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP ){
        [[PDMainTabbarViewController sharedController] updateItemImg:[UIImage imageNamed:@"icon_tabbar_pvp_yule_nor"] atIndex:0];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2 ){
        [[PDMainTabbarViewController sharedController] updateItemImg:[UIImage imageNamed:@"icon_tabbar_dota_yule_nor"] atIndex:0];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePubg ){
        [[PDMainTabbarViewController sharedController] updateItemImg:[UIImage imageNamed:@"icon_tabbar_pubg_yule_nor"] atIndex:0];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeCS ){
        [[PDMainTabbarViewController sharedController] updateItemImg:[UIImage imageNamed:@"icon_tabbar_cs_yule_nor"] atIndex:0];
    }
    [self tabbarItemSetting];
    [PDMainTabbarViewController sharedController].separatorLineColor = [UIColor colorWithCustomerName:@"灰"];
    [PDMainTabbarViewController sharedController].selectedColor = [AccountModel sharedAccountModel].mainStyleColor;
    // 更新娱乐items
    [[PDMainTabbarViewController sharedController].amusementViewController updateAmusementItems];
}

-(void)tabbarItemSetting{
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        [PDMainTabbarViewController sharedController].tabIcons = @[[UIImage imageNamed:@"icon_tabbar_lol_yule_nor"],[UIImage imageNamed:@"icon_tabbar_lol_fuli_nor"],[UIImage imageNamed:@"icon_tabbar_lol_yequ_nor"],[UIImage imageNamed:@"icon_tabbar_lol_shejiao_nor"]];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP ){
        [PDMainTabbarViewController sharedController].tabIcons = @[[UIImage imageNamed:@"icon_tabbar_pvp_yule_nor"],[UIImage imageNamed:@"icon_tabbar_lol_fuli_nor"],[UIImage imageNamed:@"icon_tabbar_lol_yequ_nor"],[UIImage imageNamed:@"icon_tabbar_lol_shejiao_nor"]];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2 ){
        [PDMainTabbarViewController sharedController].tabIcons = @[[UIImage imageNamed:@"icon_tabbar_dota_yule_nor"],[UIImage imageNamed:@"icon_tabbar_lol_fuli_nor"],[UIImage imageNamed:@"icon_tabbar_lol_yequ_nor"],[UIImage imageNamed:@"icon_tabbar_lol_shejiao_nor"]];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePubg ){
        [PDMainTabbarViewController sharedController].tabIcons = @[[UIImage imageNamed:@"icon_tabbar_pubg_yule_nor"],[UIImage imageNamed:@"icon_tabbar_lol_fuli_nor"],[UIImage imageNamed:@"icon_tabbar_lol_yequ_nor"],[UIImage imageNamed:@"icon_tabbar_lol_shejiao_nor"]];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeCS ){
        [PDMainTabbarViewController sharedController].tabIcons = @[[UIImage imageNamed:@"icon_tabbar_cs_yule_nor"],[UIImage imageNamed:@"icon_tabbar_lol_fuli_nor"],[UIImage imageNamed:@"icon_tabbar_lol_yequ_nor"],[UIImage imageNamed:@"icon_tabbar_lol_shejiao_nor"]];
    }
    
    // 2. 保存到本地
    NSString *gameType = [NSString stringWithFormat:@"%@%li",User_Choose_Game,[AccountModel sharedAccountModel].gameType];
    [Tool userDefaulteWithKey:User_Choose_Game Obj:gameType];
}

// 【状态栏隐藏和显示】
-(void)statusBarHiden:(BOOL)hiden{
    UIScrollView *statusBar = [[UIApplication sharedApplication] valueForKey:@"statusBarWindow"];
    [UIView animateWithDuration:.2f animations:^{
        if (hiden){
            statusBar.orgin_y = -20;
        } else {
            statusBar.orgin_y = 0;
        }
    }];
}


-(void)sliderPushController:(UIViewController *)controller{
    [self.sideMenuViewController hideMenuViewController];
    PDNavigationController *contentViewController =(PDNavigationController *)[PDMainTabbarViewController sharedController].currentNavController;
    [contentViewController pushViewController:controller animated:YES];
}


#pragma mark - 获取经验条
-(void)sendRequestToGetJingyanInfo{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel]sendRequestToGetJingyanInfoBlock:^(PDAccountSliderOtherInfoModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->accountSliderSingleModel = singleModel;
        [strongSelf.sliderHeaderView jinduAnimationWithModel:singleModel];
        
        // 1.
        [strongSelf.sliderTableView reloadData];
    
        [AccountModel sharedAccountModel].memberInfo.level = singleModel.level;
    }];
}


#pragma mark - 解除绑定
-(void)cancelBindingWithType:(GameType)gameType{
    [[RESideMenu shareInstance] presentLeftMenuViewController];
    if (gameType == GameTypeLoL){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.7f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [AccountModel sharedAccountModel].lolInfo = nil;
            // 2.移除对应cell
            PDSliderSingleModel *singleModel = [[self.sliderMutableArr objectAtIndex:0] objectAtIndex:0];
            if ([singleModel.appFunctionName isEqualToString:@"绑定召唤师"]){
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.sliderTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            }
        });
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[RESideMenu shareInstance] hideMenuViewController];
    });
}

-(void)bangdingGameRoleWithGameType:(GameType)gameType{
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] sendRequestToGetUserInfoWithBlock:^(BOOL success) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[RESideMenu shareInstance] presentLeftMenuViewController];
        if (gameType == GameTypeLoL){
            PDSliderSingleModel *singleModel = [[strongSelf.sliderMutableArr objectAtIndex:0] objectAtIndex:0];
            if ([singleModel.appFunctionName isEqualToString:@"绑定召唤师"]){
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [strongSelf.sliderTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [[RESideMenu shareInstance] hideMenuViewController];
        });
    }];
}
@end
