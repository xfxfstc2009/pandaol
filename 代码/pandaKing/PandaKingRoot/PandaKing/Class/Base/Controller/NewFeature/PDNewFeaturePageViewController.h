//
//  PDNewFeaturePageViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,PDNewFeaturePageViewControllerType) {
    PDNewFeaturePageViewControllerTypeBinding,                          /**< 绑定*/
    PDNewFeaturePageViewControllerTypeHomePage,                         /**< 主页*/
    PDNewFeaturePageViewControllerTypeMessage,                          /**< 消息*/
    PDNewFeaturePageViewControllerTypeNetBar,                           /**< 网吧*/
    PDNewFeaturePageViewControllerTypePersonal,                         /**< 个人中心*/
    PDNewFeaturePageViewControllerTypeCenter,                           /**< 我的*/
};


@interface PDNewFeaturePageViewController : AbstractViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势

- (void)showInView:(UIViewController *)viewController type:(PDNewFeaturePageViewControllerType)type;
- (void)dismissFromView:(UIViewController *)viewController;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果

@end
