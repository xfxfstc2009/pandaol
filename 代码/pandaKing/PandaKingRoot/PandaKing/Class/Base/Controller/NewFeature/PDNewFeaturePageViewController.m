//
//  PDNewFeaturePageViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNewFeaturePageViewController.h"

@interface PDNewFeaturePageViewController()
@property (nonatomic,strong)UIViewController *showViewController;               /**< 显示的控制器*/
@property (nonatomic,strong)UIView *backgroundView;
@property (nonatomic,strong)PDImageView *shareView;                             /**< shareView*/

@end

@implementation PDNewFeaturePageViewController
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createSheetView];
    
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    // 创建背景色
    self.backgroundView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgroundView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.65f];
    [self.view addSubview:self.backgroundView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    
    // 3. 创建背景
    self.shareView = [[PDImageView alloc]init];
    self.shareView.frame = kScreenBounds;
    self.shareView.clipsToBounds = YES;
    self.shareView.image = [UIImage imageNamed:@"newfeature_binding"];
    self.shareView.userInteractionEnabled = NO;
    self.shareView.backgroundColor = [UIColor clearColor];
    self.shareView.alpha = 0;
    [self.view addSubview:self.shareView];
    
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
    [self.backgroundView addGestureRecognizer:tapGestureRecognizer];
}


#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgroundView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 显示view
- (void)showInView:(UIViewController *)viewController type:(PDNewFeaturePageViewControllerType)type{
    __weak PDNewFeaturePageViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
//    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.5f animations:^{
        weakVC.shareView.alpha = 1;
    }];
    
    if (type == PDNewFeaturePageViewControllerTypeBinding){         // 绑定
        self.shareView.image = [UIImage imageNamed:@"newfeature_binding"];
        [Tool userDefaulteWithKey:NewFeature_Binding Obj:@"Y"];
    } else if (type == PDNewFeaturePageViewControllerTypeHomePage){     // 首页
        self.shareView.image = [UIImage imageNamed:@"newfeature_homePage"];
        [Tool userDefaulteWithKey:NewFeature_HomePage Obj:@"Y"];
    } else if (type == PDNewFeaturePageViewControllerTypeMessage){      // 消息
        self.shareView.image = [UIImage imageNamed:@"newfeature_message"];
        [Tool userDefaulteWithKey:NewFeature_Message Obj:@"Y"];
    } else if (type == PDNewFeaturePageViewControllerTypeNetBar){       // 网吧
        self.shareView.image = [UIImage imageNamed:@"newfeature_netbar"];
        [Tool userDefaulteWithKey:NewFeature_NetBar Obj:@"Y"];
    } else if (type == PDNewFeaturePageViewControllerTypePersonal){     // 个人中心
        self.shareView.image = [UIImage imageNamed:@"newfeature_personal"];
        [Tool userDefaulteWithKey:NewFeature_Personal Obj:@"Y"];
    } else if (type  == PDNewFeaturePageViewControllerTypeCenter){
        self.shareView.image = [UIImage imageNamed:@"newfeature_center"];
        [Tool userDefaulteWithKey:NewFeature_Center Obj:@"Y"];
    }
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak PDNewFeaturePageViewController *weakVC = self;
    
    [UIView animateWithDuration:0.5f animations:^{
        weakVC.shareView.alpha = 0;
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{
    
}

@end
