//
//  PDTabBarController.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZYTabBar.h"                                // customerTabBar

typedef NS_ENUM(NSInteger, PDTabBarControllerViewControllerIndex) {
    PDTabBarControllerViewControllerIndexHome = 0,
    PDTabBarControllerViewControllerIndexProduct = 1,
    PDTabBarControllerViewControllerIndexFind = 3,
    PDTabBarControllerViewControllerIndexCenter = 4,
    PDTabBarControllerViewControllerIndexInformation = 1,
    PDTabBarControllerViewControllerIndexHappy = 2,
};


@interface PDTabBarController : UITabBarController
+(instancetype)sharedController;

@property (nonatomic,strong)UINavigationController *transferNavController;      /**< 传递过来的nav*/
@property (nonatomic,strong)UITabBar *customerTabBar;
//- (void)showMenu ;
+(void)directToProductWithController:(UIViewController *)controller;

@end
