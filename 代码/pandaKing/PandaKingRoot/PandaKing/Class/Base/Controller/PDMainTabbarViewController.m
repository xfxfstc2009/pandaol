//
//  PDMainTabbarViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMainTabbarViewController.h"
#import "PDBindingRoleDetailViewController.h"
#import "PDChallengeTestViewController.h"
#import "PDGradientNavBar.h"
#import "PDAppleShenheModel.h"
#import "PDPandaRootViewController.h"

@interface PDMainTabbarViewController ()

@end

@implementation PDMainTabbarViewController

+(instancetype)sharedController{
    static PDMainTabbarViewController *_sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *gameTypeStr = [Tool userDefaultGetWithKey:User_Choose_Game];
        NSArray *tempArr = [gameTypeStr componentsSeparatedByString:User_Choose_Game];
        NSString *tempStr = [tempArr lastObject];
        GameType tempIndex = [tempStr integerValue];
        NSArray *iconArr;
        
        // 2. 修改颜色
        if (tempIndex == GameTypeLoL){
            iconArr = @[@"icon_tabbar_lol_yule_nor",@"icon_tabbar_lol_fuli_nor",@"icon_tabbar_lol_yequ_nor",@"icon_tabbar_lol_shejiao_nor"];
        } else if (tempIndex == GameTypePVP){
            iconArr = @[@"icon_tabbar_pvp_yule_nor",@"icon_tabbar_lol_fuli_nor",@"icon_tabbar_lol_yequ_nor",@"icon_tabbar_lol_shejiao_nor"];
        } else if (tempIndex == GameTypeDota2){
            iconArr = @[@"icon_tabbar_dota_yule_nor",@"icon_tabbar_lol_fuli_nor",@"icon_tabbar_lol_yequ_nor",@"icon_tabbar_lol_shejiao_nor"];
        } else if (tempIndex == GameTypePubg){
            iconArr = @[@"icon_tabbar_pubg_yule_nor",@"icon_tabbar_lol_fuli_nor",@"icon_tabbar_lol_yequ_nor",@"icon_tabbar_lol_shejiao_nor"];
        } else if (tempIndex == GameTypeCS){
            iconArr = @[@"icon_tabbar_cs_yule_nor",@"icon_tabbar_lol_fuli_nor",@"icon_tabbar_lol_yequ_nor",@"icon_tabbar_lol_shejiao_nor"];
        }
        
        _sharedController = [[PDMainTabbarViewController alloc] initWithTabIconNames:iconArr];
        _sharedController.selectedColor = [AccountModel sharedAccountModel].mainStyleColor;
        _sharedController.buttonsBackgroundColor = [UIColor whiteColor];
        _sharedController.separatorLineVisible = YES;
        _sharedController.separatorLineColor = [UIColor colorWithCustomerName:@"灰"];
        _sharedController.selectionIndicatorHeight = 1;
        
        PDNavigationController *navigationController = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
        [[navigationController navigationBar] setTranslucent:NO];
        
        UIColor *color1 = [UIColor colorWithCustomerName:@"黑"];
        UIColor *color6 = [UIColor colorWithCustomerName:@"黑"];
        NSArray *navBarArr = [NSArray arrayWithObjects:(id)color6.CGColor,(id)color1.CGColor,nil];
        [[PDGradientNavBar appearance] setBarTintGradientColors:navBarArr];

        // 1. 娱乐
        PDAmusementRootViewController *amusementRootViewController = [[PDAmusementRootViewController alloc]init];
        PDNavigationController *amusementRootNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
        [amusementRootNav setViewControllers:@[amusementRootViewController]];
        [_sharedController setViewController:amusementRootNav atIndex:0];
        
        // 2. 福利
        PDShopRootMainViewController *shopRootmainViewController = [[PDShopRootMainViewController alloc]init];
        PDNavigationController *shopRootmainNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
        [shopRootmainNav setViewControllers:@[shopRootmainViewController]];
        [_sharedController setViewController:shopRootmainNav atIndex:1];
        
        // 3.野区
        PDPostRootViewController *postViewController = [[PDPostRootViewController alloc] init];
        PDNavigationController *postNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
        [postNav setViewControllers:@[postViewController]];
        [_sharedController setViewController:postNav atIndex:2];
        
        // 4. 消息中心
        PDZoneViewController *zoneViewController = [[PDZoneViewController alloc]init];
        PDNavigationController *zoneNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
        [zoneNav setViewControllers:@[zoneViewController]];
        [_sharedController setViewController:zoneNav atIndex:3];
        
        _sharedController.amusementViewController = amusementRootViewController;
        _sharedController.shopViewController = shopRootmainViewController;
        _sharedController.postViewController = postViewController;
        _sharedController.zoneViewController = zoneViewController;
        
    });
    return _sharedController;
}

- (UIViewController *)viewControllerAtIndex:(int)index {
    if (index == 0) {
        return self.amusementViewController;
    } else if (index == 1) {
        return self.shopViewController;
    } else if (index == 2) {
        return self.postViewController;
    } else {
        return self.zoneViewController;
    }
}

#pragma mark - 审核标记
-(void)sendRequestToGetShenheWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"version":[Tool appVersion]};
    [[NetworkAdapter sharedAdapter] fetchWithPath:shenhe requestParams:params responseObjectClass:[PDAppleShenheModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!
            weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDAppleShenheModel *shenheModel = (PDAppleShenheModel *)responseObject;
            [AccountModel sharedAccountModel].isShenhe = shenheModel.enable;
#ifdef DEBUG        // 测试           // 【JAVA】
            [AccountModel sharedAccountModel].isShenhe = YES;
#else               // 线上
#endif
        }
        if (block){
            block();
        }
    }];
}

@end
