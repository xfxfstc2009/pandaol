//
//  PDMainTabbarViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTabBarController.h"
#import "PDAmusementRootViewController.h"                       /**< 娱乐*/
#import "PDShopRootMainViewController.h"                        /**< 夺宝*/
#import "PDZoneViewController.h"                                /**< 消息中心*/
#import "PDPostRootViewController.h"                            /**< 野区*/

@interface PDMainTabbarViewController : ESTabBarController
@property (nonatomic, strong) PDAmusementRootViewController *amusementViewController;
@property (nonatomic, strong) PDShopRootMainViewController *shopViewController;
@property (nonatomic, strong) PDZoneViewController *zoneViewController;
@property (nonatomic, strong) PDPostRootViewController *postViewController;

+(instancetype)sharedController;

- (UIViewController *)viewControllerAtIndex:(int)index;

@end
