//
//  AdaptationShared.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AdaptationShared.h"

@implementation AdaptationShared

+(instancetype)sharedAdapter{
    static AdaptationShared *_sharedAdapter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAdapter = [[AdaptationShared alloc] init];
    });
    return _sharedAdapter;
}


@end
