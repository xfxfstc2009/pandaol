//
//  PDScuffleViewController.h
//  PandaKing
//
//  Created by Cranz on 17/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
// 右上角scence model
#import "PDScuffleScenceModel.h"

typedef NS_ENUM(NSUInteger, PDScuffleMineSate) {
    /**
     * 不再游戏中，可能推出界面，也可能在匹配界面
     */
    PDScuffleMineSateNone,
    /**
     * 在游戏中
     */
    PDScuffleMineSatePlaying,
};

typedef NS_ENUM(NSUInteger, PDScuffleMatchState) {
    /**
     * 选择状态，可以离开
     */
    PDScuffleMatchStateSel = -1,
    /**
     * 游戏中，离开口金币
     */
    PDScuffleMatchStatePlay = 1,
};

typedef NS_ENUM(NSUInteger, PDScuffleGameState) {
    /**
     * 准备阶段，卡牌可以移动
     */
    PDScuffleGameStateReady,
    /**
     * 游戏中，卡牌不能移动
     */
    PDScuffleGameStatePlaying,
};

typedef NS_ENUM(NSUInteger, PDOpponentState) {
    /**
     * 对手存活
     */
    PDOpponentStateExist = 1,
    /**
     * 对手离开
     */
    PDOpponentStateLeave = -1,
};

typedef NS_ENUM(NSUInteger, PDScuffleCardState) {
    /**
     * 正面朝上
     */
    PDScuffleCardOpen = 1,
    /**
     * 反面朝上
     */
    PDScuffleCardReverse = 0,
};

/// 大乱斗控制器
@interface PDScuffleViewController : AbstractViewController
/**
 * 传递过去的金币场id
 */
@property (nonatomic, copy) NSString *groundId;
/**
 * 传递右上角在线人数的model，金币场
 */
@property (nonatomic, strong) PDScuffleScenceModel *scenceModel;
@end
