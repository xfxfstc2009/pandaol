//
//  PDScuffleViewController.m
//  PandaKing
//
//  Created by Cranz on 17/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleViewController.h"
#import "PDScuffleReadyView.h"
#import "PDScuffleSceneView.h"
#import "CCZComposeButton.h"
// 对手的信息View
#import "PDScuffleOtherView.h"
// 英雄卡容器
#import "PDScuffleCardView.h"
// 英雄卡
#import "PDScuffleCard.h"
// 金币充值
#import "PDPaySheetView.h"
#import "PDAlertView+PDScuffleAlert.h"
// 支付
#import "PDTopUpOrder.h"
#import "PDAlipayHandle.h"
#import "PDAlipayOrderInfo.h"
#import "PDWXPayHandle.h"
// 顶部文案tip
#import "PDScuffleTipBar.h"
// 卡牌model
#import "PDScuffleCardList.h"
// socket
#import "PDScuffleSocketEnum.h"
#import "PDScuffleSocketMatchModel.h"
// 匹配正计时label
#import "PDScuffleMatchingLabel.h"
// 个人信息接口
#import "PDCenterMyInfo.h"
#import "PDFindMemberInfo.h"
#import "PDMemberObj.h"
// 倒计时，战果显示
#import "PDScuffleResultView.h"
// 开奖
#import "PDScuffleSocketDrawModel.h"
// 开始新的一局
#import "PDScuffleSocketNewGameModel.h"
// 继续
#import "PDScuffleContinueModel.h"
// 音效
#import "ZCAudioTool.h"

#import "PDStrokeLabel.h"

typedef void(^PDScuffleBlockP0)();

#define kSCUFFLE_UPSIDE_HEIGHT LCFloat(480)
// 人物信息底栏
#define kSCUFFLE_BOTTOM_HEIGHT (kScreenBounds.size.height - kSCUFFLE_UPSIDE_HEIGHT)
// 玩家头像大小
#define kSCUFFLE_HEADER_WIDTH LCFloat(97/2)

@interface PDScuffleViewController ()<PDScuffleCardGestureRecognizerDelegate>

@property (nonatomic) PDScuffleSocketType socketType;

///////////////// 匹配界面
// 匹配准备层
@property (nonatomic, strong) PDScuffleReadyView *readyUpView;
@property (nonatomic, strong) PDScuffleReadyView *readyDownView;
@property (nonatomic, strong) UIButton *cancleButton;
/**
 * 匹配正计时
 */
@property (nonatomic, strong) PDScuffleMatchingLabel *matchingLabel;

// 场景提示图
@property (nonatomic, strong) PDScuffleSceneView *scenceView;

////////////////// 上部
// 上部分的view
@property (nonatomic, strong) UIImageView *upsideView;
@property (nonatomic, strong) PDScuffleOtherView *otherInfoView;
/**
 * 倒计时的那层View
 */
@property (nonatomic, strong) PDScuffleResultView *timeView;

///////////// 主内容中3路英雄卡槽布局
// 敌方的卡槽容器，开场默认展示的，带有❓
@property (nonatomic, strong) PDScuffleCardView *upsideOtherCardView0;
@property (nonatomic, strong) PDScuffleCardView *upsideOtherCardView1;
@property (nonatomic, strong) PDScuffleCardView *upsideOtherCardView2;
// 敌方的英雄卡，因为不需要移动，只要一个objCard就好
@property (nonatomic, strong) NSMutableArray *otherCards;

// 我方的卡槽容器
@property (nonatomic, strong) PDScuffleCardView *upsideMineCardView0;
@property (nonatomic, strong) PDScuffleCardView *upsideMineCardView1;
@property (nonatomic, strong) PDScuffleCardView *upsideMineCardView2;

/////////////// 底部
// 下部分的view
@property (nonatomic, strong) UIImageView *bottomView;
// 自己的头像
@property (nonatomic, strong) PDImageView *userHeaderView;
// 昵称
@property (nonatomic, strong) CCZComposeButton *nicknameCompose;
// 自己的金币
@property (nonatomic, strong) CCZComposeButton *goldCompose;

// 准备栏四个英雄槽
@property (nonatomic, strong) PDScuffleCardView *bottomCardView0;
@property (nonatomic, strong) PDScuffleCardView *bottomCardView1;
@property (nonatomic, strong) PDScuffleCardView *bottomCardView2;
@property (nonatomic, strong) PDScuffleCardView *bottomCardView3;

// 创建自己的英雄卡s
@property (nonatomic, strong) PDScuffleCardList *mineCards; // 我的卡列表
@property (nonatomic, strong) PDScuffleCard *scuffleCard0; // 6300
@property (nonatomic, strong) PDScuffleCard *scuffleCard1; // 4800
@property (nonatomic, strong) PDScuffleCard *scuffleCard2; // 3150
@property (nonatomic, strong) PDScuffleCard *scuffleCard3; // 1350

// 目前是否有卡片正在移动中，确保只有一张在移动
@property (nonatomic, weak) PDScuffleCard *moveCard;

// 界面顶部文案tip
@property (nonatomic, strong) PDScuffleTipBar *tipBar;

/// 出牌
@property (nonatomic, strong) PDScuffleSocketMatchModel *matchModel;

// 游戏结果，对手信息
@property (nonatomic, strong) PDScuffleSocketDrawModel *drawModel;

// 卡牌状态
@property (nonatomic) PDScuffleGameState gameState;
// 游戏状态
@property (nonatomic) PDScuffleMatchState matchState;
// 对手状态
@property (nonatomic) PDOpponentState opponentState;
// 我的状态
@property (nonatomic) PDScuffleMineSate mineState;
// 两个按钮
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, strong) UIButton *leaveButton;

// 当前金币数额，需要及时刷新
@property (nonatomic, assign) NSUInteger currentGold;

// 开状态
@property (nonatomic, assign) PDScuffleCardState otherCardState;
// 是否出过牌了
@property (nonatomic) BOOL isPlayed;

// 加钱动画的label
@property (nonatomic, strong) PDStrokeLabel *addLabel;
@end

@implementation PDScuffleViewController

- (NSMutableArray *)otherCards {
    if (!_otherCards) {
        _otherCards = [NSMutableArray array];
    }
    return _otherCards;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    // 界面绘制完再发请求
    [self fetchBeginMatching];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[ZCAudioTool sharedZCAudioTool] stopWithAudioName:@"mic_scuffle_ready.wav"];
    [[ZCAudioTool sharedZCAudioTool] stopWithAudioName:@"mic_scuffle_mate.mp3"];
    
    self.mineState = PDScuffleMineSateNone;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupContentView];
    [self setupHeroCards];
    [self setupTipBar];
    [self setupReadyView];
    [self setupScenceView];
    [self netFetch];
}

- (void)netFetch {
    [self fetchMineCards];
    [self fetchMemberInfoSynchronize:YES];
}

/**
 * 布局主内容视图
 */
- (void)setupContentView {
    [self setupUpsideContentView];
    [self setupBottomContentView];
}

/**
 * 布局顶部黄色文案显示tip
 */
- (void)setupTipBar {
    self.tipBar = [[PDScuffleTipBar alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.otherInfoView.frame) + 10, kScreenBounds.size.width, 40) type:PDScuffleTipAnimationTypeLeft];
    [self.view addSubview:self.tipBar];
    
    [self.view insertSubview:self.timeView belowSubview:self.tipBar];
}

/**
 * 布局匹配内容试图
 */
- (void)setupReadyView {
    NSArray *imgs = [PDCenterTool separateBgImgae:[UIImage imageNamed:@"bg_scuffle_bg"]];
    
    self.readyUpView = [PDScuffleReadyView readyWithBackgroundImage:imgs[0] status:PDReadyStatusUp];
    PDScuffleReadyModel *readyUpModel = [PDScuffleReadyModel new];
    readyUpModel.nickname = @"等待玩家...";
    self.readyUpView.model = readyUpModel;
    [self.view addSubview:self.readyUpView];
    
    self.readyDownView = [PDScuffleReadyView readyWithBackgroundImage:imgs[1] status:PDReadyStatusDown];
    PDScuffleReadyModel *readyDownModel = [PDScuffleReadyModel new];
    readyDownModel.nickname = [Tool userDefaultGetWithKey:CustomerNickname];
    readyDownModel.avatar = [Tool userDefaultGetWithKey:CustomerAvatar];
    self.readyDownView.model = readyDownModel;
    [self.view addSubview:self.readyDownView];
    
    // 取消匹配按钮
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cancleButton = cancleButton;
    [cancleButton setBackgroundImage:[UIImage imageNamed:@"bg_scuffle_tip"] forState:UIControlStateNormal];
    cancleButton.titleLabel.font = [UIFont systemFontOfCustomeSize:16];
    [cancleButton setTitleColor:c26 forState:UIControlStateNormal];
    [self setCancleNormal];
    cancleButton.frame = CGRectMake(0, CGRectGetHeight(self.readyDownView.frame) - 90, kScreenBounds.size.width, 50);
    [cancleButton addTarget:self action:@selector(cancleMatching:) forControlEvents:UIControlEventTouchUpInside];
    [self.readyDownView addSubview:cancleButton];
    
    // 匹配计时
    self.matchingLabel = [[PDScuffleMatchingLabel alloc] init];
    [self.view addSubview:self.matchingLabel];
}

- (void)cancleMatching:(UIButton *)button {
    [button setTitle:@"正在为您取消匹配，请稍后" forState:UIControlStateNormal];
    [self fetchStopMatching];
}

- (void)setCancleNormal {
    [self.cancleButton setTitle:@"取消匹配" forState:UIControlStateNormal];
    self.cancleButton.enabled = YES;
}

- (void)setCancleUnabled {
    [self.cancleButton setTitle:@"匹配成功" forState:UIControlStateNormal];
    self.cancleButton.enabled = NO;
}

/**
 * 布局在线人数场次相关v视图
 */
- (void)setupScenceView {
    self.scenceView = [[PDScuffleSceneView alloc] init];
    __weak typeof(self) weakSelf = self;
    [self.scenceView updateFrame:^(CGSize size) {
        weakSelf.scenceView.frame = CGRectMake(kScreenBounds.size.width - 13 - size.width, 13, size.width, size.height);
    }];
    [self.view addSubview:self.scenceView];
    // 设置一个空的model
    self.scenceView.model = self.scenceModel;
}

#pragma mark - 设置主要的内容视图
/**
 * 上部分容器视图
 */
- (void)setupUpsideContentView {
    self.upsideView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kSCUFFLE_UPSIDE_HEIGHT)];
    self.upsideView.image = [UIImage imageNamed:@"bg_scuffle_bg"];
    self.upsideView.userInteractionEnabled = YES;
    [self.view addSubview:self.upsideView];
    
    [self setupOtherInfoView];
    [self setupHeroGroove];
}

/**
 * 对方信息栏
 */
- (void)setupOtherInfoView {
    self.otherInfoView = [[PDScuffleOtherView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kSCUFFLE_HEADER_WIDTH + 12 * 2)];
    [self.upsideView addSubview:self.otherInfoView];
}

//////////////////// 主页英雄卡槽
- (void)setupHeroGroove {
    // 叠放两层卡牌的空间
    CGFloat heroContentHeight = kSCUFFLE_UPSIDE_HEIGHT - CGRectGetHeight(self.otherInfoView.frame);
    
    // 中间倒计时高度
    CGFloat timeHeight = LCFloat(60);
    PDScuffleResultView *timeView = [[PDScuffleResultView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.otherInfoView.frame) + (heroContentHeight - timeHeight) / 2, kScreenBounds.size.width, timeHeight)];
    self.timeView = timeView;
    
    // 敌人的卡s
    CGFloat card_left = LCFloat(67/2);
    CGFloat card_space = LCFloat(19);
    CGFloat card_width = (kScreenBounds.size.width - card_left * 2 - card_space * 2) / 3;
    CGFloat card_time_space = 5;
    self.upsideOtherCardView0 = [[PDScuffleCardView alloc] initWithPlaceholdImage:[UIImage imageNamed:@"icon_scuffle_card_noopen"] origin:CGPointMake(card_left, CGRectGetMinY(self.timeView.frame) - card_time_space - [PDScuffleCardView getHeightFromWidth:card_width]) constraintWidth:card_width textType:PDScuffleCardTypeUp placehold:nil];
    self.upsideOtherCardView1 = [[PDScuffleCardView alloc] initWithPlaceholdImage:[UIImage imageNamed:@"icon_scuffle_card_noopen"] origin:CGPointMake(CGRectGetMaxX(self.upsideOtherCardView0.frame) + card_space, CGRectGetMinY(self.upsideOtherCardView0.frame)) constraintWidth:card_width textType:PDScuffleCardTypeUp placehold:nil];
    self.upsideOtherCardView2 = [[PDScuffleCardView alloc] initWithPlaceholdImage:[UIImage imageNamed:@"icon_scuffle_card_noopen"] origin:CGPointMake(CGRectGetMaxX(self.upsideOtherCardView1.frame) + card_space, CGRectGetMinY(self.upsideOtherCardView1.frame)) constraintWidth:card_width textType:PDScuffleCardTypeUp placehold:nil];
    
    self.upsideOtherCardView0.textColor = c46;
    self.upsideOtherCardView1.textColor = c46;
    self.upsideOtherCardView2.textColor = c46;
    
    [self.upsideView addSubview:self.upsideOtherCardView0];
    [self.upsideView addSubview:self.upsideOtherCardView1];
    [self.upsideView addSubview:self.upsideOtherCardView2];
    
    // 敌人的英雄卡
    
    
    // 我的卡s
    self.upsideMineCardView0 = [[PDScuffleCardView alloc] initWithPlaceholdImage:[UIImage imageNamed:@"icon_scuffle_card_moveplace"] origin:CGPointMake(card_left, CGRectGetMaxY(self.timeView.frame) + card_time_space) constraintWidth:card_width textType:PDScuffleCardTypeBottom placehold:nil];
    self.upsideMineCardView1 = [[PDScuffleCardView alloc] initWithPlaceholdImage:[UIImage imageNamed:@"icon_scuffle_card_moveplace"] origin:CGPointMake(CGRectGetMaxX(self.upsideMineCardView0.frame) + card_space, CGRectGetMinY(self.upsideMineCardView0.frame)) constraintWidth:card_width textType:PDScuffleCardTypeBottom placehold:nil];
    self.upsideMineCardView2 = [[PDScuffleCardView alloc] initWithPlaceholdImage:[UIImage imageNamed:@"icon_scuffle_card_moveplace"] origin:CGPointMake(CGRectGetMaxX(self.upsideMineCardView1.frame) + card_space, CGRectGetMinY(self.upsideMineCardView1.frame)) constraintWidth:card_width textType:PDScuffleCardTypeBottom placehold:nil];
    
    self.upsideMineCardView0.textColor = c46;
    self.upsideMineCardView1.textColor = c46;
    self.upsideMineCardView2.textColor = c46;
    
    [self.upsideView addSubview:self.upsideMineCardView0];
    [self.upsideView addSubview:self.upsideMineCardView1];
    [self.upsideView addSubview:self.upsideMineCardView2];
}

#pragma mark - 下部分

/**
 * 下部分容器视图
 */
- (void)setupBottomContentView {
    self.bottomView = [[UIImageView alloc] initWithFrame:CGRectMake(0, kSCUFFLE_UPSIDE_HEIGHT, kScreenBounds.size.width, kSCUFFLE_BOTTOM_HEIGHT)];
    self.bottomView.image = [UIImage imageNamed:@"bg_scuffle_bottom_bg"];
    self.bottomView.userInteractionEnabled = YES;
    [self.view addSubview:self.bottomView];
    
    [self setupHerosViewWithHeight:LCFloat(110)];
    [self setupUserInfoViewWithHeight:kSCUFFLE_BOTTOM_HEIGHT - LCFloat(110) - 5];
}

/**
 * 底下英雄卡片一栏
 */
- (void)setupHerosViewWithHeight:(CGFloat)height {
    UIImageView *herosContainerView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, CGRectGetWidth(self.bottomView.frame) - 10, height)];
    herosContainerView.userInteractionEnabled = YES;
    herosContainerView.image = [UIImage imageNamed:@"bg_scuffle_cards_bg"];
    [self.bottomView addSubview:herosContainerView];
    
    CGFloat bottomCard_left = LCFloat(12);
    CGFloat bottomCard_space = LCFloat(35);

    CGFloat cardWidth = (CGRectGetWidth(herosContainerView.frame) - bottomCard_left * 2 - bottomCard_space * 3) / 4;

    self.bottomCardView0 = [[PDScuffleCardView alloc] initWithPlaceholdImage:[UIImage imageNamed:@"icon_scuffle_card_bg"] origin:CGPointMake(bottomCard_left, LCFloat(15/2)) constraintWidth:cardWidth textType:PDScuffleCardTypeBottom placehold:@"6300"];
    self.bottomCardView1 = [[PDScuffleCardView alloc] initWithPlaceholdImage:[UIImage imageNamed:@"icon_scuffle_card_bg"] origin:CGPointMake(CGRectGetMaxX(self.bottomCardView0.frame) + bottomCard_space, CGRectGetMinY(self.bottomCardView0.frame)) constraintWidth:cardWidth textType:PDScuffleCardTypeBottom placehold:@"4800"];
    self.bottomCardView2 = [[PDScuffleCardView alloc] initWithPlaceholdImage:[UIImage imageNamed:@"icon_scuffle_card_bg"] origin:CGPointMake(CGRectGetMaxX(self.bottomCardView1.frame) + bottomCard_space, CGRectGetMinY(self.bottomCardView1.frame)) constraintWidth:cardWidth textType:PDScuffleCardTypeBottom placehold:@"3150"];
    self.bottomCardView3 = [[PDScuffleCardView alloc] initWithPlaceholdImage:[UIImage imageNamed:@"icon_scuffle_card_bg"] origin:CGPointMake(CGRectGetMaxX(self.bottomCardView2.frame) + bottomCard_space, CGRectGetMinY(self.bottomCardView2.frame)) constraintWidth:cardWidth textType:PDScuffleCardTypeBottom placehold:@"1350"];
    self.bottomCardView0.textColor = self.bottomCardView1.textColor = self.bottomCardView2.textColor = self.bottomCardView3.textColor =[UIColor hexChangeFloat:@"ffe6aa"];
    
    [herosContainerView addSubview:self.bottomCardView0];
    [herosContainerView addSubview:self.bottomCardView1];
    [herosContainerView addSubview:self.bottomCardView2];
    [herosContainerView addSubview:self.bottomCardView3];
}

/**
 * 最底下人物信息一栏
 */
- (void)setupUserInfoViewWithHeight:(CGFloat)height {
    UIView *userInfoContainerView = [[UIView alloc] initWithFrame:CGRectMake(0, kSCUFFLE_BOTTOM_HEIGHT - height, kScreenBounds.size.width, height)];
    [self.bottomView addSubview:userInfoContainerView];
    
    // 头像
    self.userHeaderView = [[PDImageView alloc] initWithFrame:CGRectMake(10, (CGRectGetHeight(userInfoContainerView.frame) - kSCUFFLE_HEADER_WIDTH) / 2, kSCUFFLE_HEADER_WIDTH, kSCUFFLE_HEADER_WIDTH)];
    self.userHeaderView.layer.cornerRadius = kSCUFFLE_HEADER_WIDTH / 2;
    self.userHeaderView.clipsToBounds = YES;
    self.userHeaderView.layer.borderWidth = 1;
    self.userHeaderView.layer.borderColor = [UIColor brownColor].CGColor;
    [userInfoContainerView addSubview:self.userHeaderView];
    
    // 昵称
    //
    CGFloat com_y = LCFloat(8);
    CGFloat com_s = LCFloat(2);
    CGFloat com_w = kScreenBounds.size.width / 2 - CGRectGetMaxX(self.userHeaderView.frame) - 10 - LCFloat(10);
    CGFloat com_h = (CGRectGetHeight(userInfoContainerView.frame) - com_s - com_y * 2) / 2;
    UIImage *backgroundImage = [UIImage imageNamed:@"bg_amusement_button"];
    self.nicknameCompose = [[CCZComposeButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.userHeaderView.frame) + 10, com_y, com_w, com_h) leftImage:nil rightImage:nil];
    self.nicknameCompose.autoAdjustToWidth = NO;
    self.nicknameCompose.backgroundImage = backgroundImage;
    self.nicknameCompose.titleColor = [UIColor whiteColor];
    self.nicknameCompose.titleFont = [UIFont systemFontOfCustomeSize:12];
    [userInfoContainerView addSubview:self.nicknameCompose];
    
    self.goldCompose = [[CCZComposeButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.nicknameCompose.frame), CGRectGetMaxY(self.nicknameCompose.frame) + com_s, com_w, com_h) leftImage:[UIImage imageNamed:@"icon_amusement_gold"] rightImage:[UIImage imageNamed:@"icon_amusement_topup"]];
    self.goldCompose.autoAdjustToWidth = NO;
    self.goldCompose.titleColor = c26;
    self.goldCompose.titleFont = [UIFont systemFontOfCustomeSize:12];
    self.goldCompose.backgroundImage = backgroundImage;
    [userInfoContainerView addSubview:self.goldCompose];
    
    [self.goldCompose addTarget:self action:@selector(fetchTopupList) forControlEvents:UIControlEventTouchUpInside];
    
    // 离开按钮的宽度
    CGFloat leave_buttonWidth = LCFloat(67);
    CGFloat play_buttonWidth = LCFloat(100);
    // 按钮的高度
    CGFloat btn_h = LCFloat(42);
    
    UIButton *leaveButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leaveButton = leaveButton;
    [leaveButton setBackgroundImage:[UIImage imageNamed:@"icon_scuffle_button_leave"] forState:UIControlStateNormal];
    leaveButton.frame = CGRectMake(kScreenBounds.size.width - 10 - leave_buttonWidth, (CGRectGetHeight(userInfoContainerView.frame) - btn_h)/2, leave_buttonWidth, btn_h);
    [userInfoContainerView addSubview:leaveButton];
    [leaveButton addTarget:self action:@selector(exit) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.playButton = playButton;
    [self setPlayButtonReady:YES];
    playButton.frame = CGRectMake(CGRectGetMinX(leaveButton.frame) - LCFloat(10) - play_buttonWidth, CGRectGetMinY(leaveButton.frame), play_buttonWidth, btn_h);
    [userInfoContainerView addSubview:playButton];
    [playButton addTarget:self action:@selector(ready) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setMineCardsPlayState {
    self.upsideMineCardView0.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_moveplace_notext"];
    self.upsideMineCardView1.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_moveplace_notext"];
    self.upsideMineCardView2.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_moveplace_notext"];
}

- (void)setMineCardsSelState {
    self.upsideMineCardView0.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_moveplace"];
    self.upsideMineCardView1.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_moveplace"];
    self.upsideMineCardView2.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_moveplace"];
}

#pragma mark - 游戏按钮 state
// 准备
- (void)setPlayButtonReady:(BOOL)enbled {
    self.playButton.enabled = enbled;
    [self.playButton setBackgroundImage:[UIImage imageNamed:@"icon_scuffle_button_ready"] forState:UIControlStateNormal];
}
// 已准备
- (void)setPlayButtonAlready {
    self.playButton.enabled = NO;
    [self.playButton setBackgroundImage:[UIImage imageNamed:@"icon_scuffle_button_already"] forState:UIControlStateNormal];
}
// 游戏中
- (void)setPlayButtonPlaying {
    self.playButton.enabled = NO;
    [self.playButton setBackgroundImage:[UIImage imageNamed:@"icon_scuffle_button_playing"] forState:UIControlStateNormal];
}

#pragma mark - 游戏准备 和 离开

- (void)ready {
    [self fetchPlay];
}

- (void)exit {
    if (self.matchState == PDScuffleMatchStatePlay) {
        __weak typeof(self) weakSelf = self;
        [[PDAlertView sharedAlertView] showScuffleAlertWithTitle:@"提示" content:@"比赛中途退出，您的报名费将不会返还，而且无法回来，是否确认退出？" buttonTitles:@[@"我要走",@"再想想"] actionComplication:^(NSUInteger index) {
            if (index == 0) {
                [weakSelf fetchStopMatching];
            }
        }];
    } else {
        [self fetchStopMatching];
    }
}

#pragma mark - 创建英雄卡片
- (void)setupHeroCards {
    self.scuffleCard0 = [[PDScuffleCard alloc] initWithFrame:[self rectInViewForTargetView:self.bottomCardView0.placeholdImageView]];
    self.scuffleCard1 = [[PDScuffleCard alloc] initWithFrame:[self rectInViewForTargetView:self.bottomCardView1.placeholdImageView]];
    self.scuffleCard2 = [[PDScuffleCard alloc] initWithFrame:[self rectInViewForTargetView:self.bottomCardView2.placeholdImageView]];
    self.scuffleCard3 = [[PDScuffleCard alloc] initWithFrame:[self rectInViewForTargetView:self.bottomCardView3.placeholdImageView]];
    
    self.scuffleCard0.delegate = self;
    self.scuffleCard1.delegate = self;
    self.scuffleCard2.delegate = self;
    self.scuffleCard3.delegate = self;
    
    [self.view addSubview:self.scuffleCard0];
    [self.view addSubview:self.scuffleCard1];
    [self.view addSubview:self.scuffleCard2];
    [self.view addSubview:self.scuffleCard3];
}

/**
 * 将cardView上的展位Imageview的坐标转换到控制器的View上
 */
- (CGRect)rectInViewForTargetView:(UIView *)targetView {
    return [targetView convertRect:targetView.frame toView:self.view];
}

#pragma mark - 卡片代理 <PDScuffleCardGestureRecognizerDelegate>

- (void)scuffleCard:(PDScuffleCard *)card didMoveCardWithGestureRecognizer:(UIPanGestureRecognizer *)pan {
    if (self.gameState == PDScuffleGameStatePlaying) {
        return;
    }
    
    if (self.moveCard && self.moveCard != card) {
        return;
    }
    self.moveCard = card;
    
    CGPoint movePoint = [pan translationInView:card];
    card.transform = CGAffineTransformTranslate(card.transform, movePoint.x, movePoint.y);
    [pan setTranslation:CGPointZero inView:card];
    
    if (pan.state == UIGestureRecognizerStateEnded) {
        
        BOOL intersect0 = CGRectIntersectsRect([self rectInViewForTargetView:self.upsideMineCardView0.placeholdImageView], card.frame);
        BOOL intersect1 = CGRectIntersectsRect([self rectInViewForTargetView:self.upsideMineCardView1.placeholdImageView], card.frame);
        BOOL intersect2 = CGRectIntersectsRect([self rectInViewForTargetView:self.upsideMineCardView2.placeholdImageView], card.frame);
        // 手势结束时，判断是否在卡槽范围内
        if (intersect0 && !intersect1 && !intersect2) {
            // 可以边缘高亮提示
            [self moveToCardGrooveWithCard:card cardView:self.upsideMineCardView0 duration:0.18];
        } else if (!intersect0 && intersect1 && !intersect2) {
            [self moveToCardGrooveWithCard:card cardView:self.upsideMineCardView1 duration:0.18];
        } else if (!intersect0 && !intersect1 && intersect2) {
            [self moveToCardGrooveWithCard:card cardView:self.upsideMineCardView2 duration:0.18];
        } else {
            [self moveToOriginWithCard:card duration:0.1];
        }
    }
}

- (void)didTapAtScuffleCard:(PDScuffleCard *)card {
    if (self.gameState == PDScuffleGameStatePlaying) {
        return;
    }
    
    if (card.changed == YES) {
        [self moveToOriginWithCard:card duration:0.1];
    }
}

/**
 * 将卡牌移动到卡槽位置
 */
- (void)moveToCardGrooveWithCard:(PDScuffleCard *)card cardView:(PDScuffleCardView *)cardView duration:(NSTimeInterval)duration {
    
    self.moveCard = nil;
    
    if (cardView.cards.count >= 2) { // 超过两张的时候自动归位
        [self moveToOriginWithCard:card duration:0.1];
        return;
    }
    
    if (card.cardView) { // 这种情况发生在两个卡槽之间横移的时候
        PDScuffleCardView *cardView = (PDScuffleCardView *)card.cardView;
        if (cardView.cards.count) {
            [cardView.cards removeObject:card];
        }
        
        if (cardView.cards.count) {
            // 如果此时cardView还存在剩余的卡,那么将其变大适应
            PDScuffleCard *firstCard = cardView.cards.firstObject;
            [UIView animateWithDuration:duration animations:^{
               firstCard.frame = [cardView.placeholdImageView convertRect:[cardView getFrameInCardViewAtIndex:0] toView:self.view];
            } completion:^(BOOL finished) {
                [self calculateNumberTextWithCardView:cardView];
            }];
        } else {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(duration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self calculateNumberTextWithCardView:cardView];
            });
        }
    }
    
    BOOL hasOtherCardInCardView = cardView.cards.count;
    [UIView animateWithDuration:duration animations:^{
        if (hasOtherCardInCardView) {
            PDScuffleCard *firstCard = cardView.cards.firstObject;
            firstCard.frame = [cardView.placeholdImageView convertRect:[cardView getFrameInCardViewAtIndex:1] toView:self.view];
            card.frame = [cardView.placeholdImageView convertRect:[cardView getFrameInCardViewAtIndex:2] toView:self.view];
        } else {
            card.frame = [cardView.placeholdImageView convertRect:[cardView getFrameInCardViewAtIndex:0] toView:self.view];
        }
    
    } completion:^(BOOL finished) {
        card.changed = YES;
        [cardView.cards addObject:card];
        card.cardView = cardView;
        
        [self calculateNumberTextWithCardView:cardView];
    }];
}

/**
 * 将卡牌移到原始位置
 */
- (void)moveToOriginWithCard:(PDScuffleCard *)card duration:(NSTimeInterval)duration {
    self.moveCard = nil;
    
    PDScuffleCardView *cardView = (PDScuffleCardView *)card.cardView;
    if (cardView.cards.count) {
        [cardView.cards removeObject:card];
    }
    
    [UIView animateWithDuration:duration animations:^{
        card.frame = card.originRect;
        if (cardView.cards.count) {
            PDScuffleCard *firstCard = cardView.cards.lastObject;
            firstCard.frame = [cardView.placeholdImageView convertRect:[cardView getFrameInCardViewAtIndex:0] toView:self.view];
        }
        
    } completion:^(BOOL finished) {
        card.changed = NO;
        card.cardView = nil;
        [self calculateNumberTextWithCardView:cardView];
    }];
}

/**
 * 计算cardView下的价格综合
 */
- (void)calculateNumberTextWithCardView:(PDScuffleCardView *)cardView {
    NSInteger number = 0;
    for (PDScuffleCard *card in cardView.cards) {
        number += card.model.cardType.value;
    }
    
    if (number == 0) {
        cardView.model = nil;
    } else {
        cardView.model = [NSString stringWithFormat:@"%@",@(number)];
    }
}

#pragma mark - 网络请求

// 开始匹配
- (void)fetchBeginMatching {
    self.matchState = PDScuffleMatchStateSel;
    self.mineState = PDScuffleMineSateNone;
    self.isPlayed = NO;
    // 播放匹配音效
    [[ZCAudioTool sharedZCAudioTool] playAudioWithAudioName:@"mic_scuffle_mate.mp3"];
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:scuffleStartMatching requestParams:@{@"groundId":self.groundId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            weakSelf.scenceModel.memberCount = [responseObject[@"membersCount"] integerValue];
            weakSelf.scenceView.model = weakSelf.scenceModel;
            [weakSelf.matchingLabel beginTiming];
        }
    }];
}

// 取消匹配、逃跑
- (void)fetchStopMatching {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:scuffleStopMatching requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        if (isSucceeded) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        }
    }];
}

/**
 * 获取本人的个人信息
 */
- (void)fetchMemberInfoSynchronize:(BOOL)synchronized {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:myInfo requestParams:nil responseObjectClass:[PDCenterMyInfo class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (isSucceeded) {
            PDCenterMyInfo *myInfo = (PDCenterMyInfo *)responseObject;
            strongSelf.currentGold = myInfo.gold;
            [strongSelf.userHeaderView uploadImageWithAvatarURL:myInfo.member.avatar placeholder:nil callback:nil];
            strongSelf.nicknameCompose.title = myInfo.member.nickname;
            strongSelf.goldCompose.title = [PDCenterTool numberStringChangeWithoutSpecialCharacter:myInfo.gold];
            
            if (synchronized) {
                // 同步一下匹配界面的头像
                PDScuffleReadyModel *readyDownModel = [PDScuffleReadyModel new];
                readyDownModel.nickname = myInfo.member.nickname;
                readyDownModel.avatar = myInfo.member.avatar;
                strongSelf.readyDownView.model = readyDownModel;
            }
        }
    }];
}

/**
 * 获取我方卡牌信息
 */
- (void)fetchMineCards {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:scuffleMineCardsInfo requestParams:nil responseObjectClass:[PDScuffleCardList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            weakSelf.mineCards = (PDScuffleCardList *)responseObject;
            [weakSelf setupMineCardsWithCardList:weakSelf.mineCards];
        }
    }];
}

- (void)setupMineCardsWithCardList:(PDScuffleCardList *)mineCards {
    for (int i = 0; i < self.mineCards.list.count; i++) {
        PDScuffleCardItem *item = self.mineCards.list[i];
        __weak typeof(self) weakSelf = self;
        if (i == 0) {
            self.scuffleCard0.model = item;
            // 皮肤默认先取第一张
            [self.scuffleCard0 uploadScuffleImageUrl:item.skinIds.firstObject callback:^(UIImage *image) {
                weakSelf.scuffleCard0.originImage = self.scuffleCard0.image;
            }];
            self.scuffleCard0.originImage = self.scuffleCard0.image;
        } else if (i == 1) {
            self.scuffleCard1.model = item;
            [self.scuffleCard1 uploadScuffleImageUrl:item.skinIds.firstObject callback:^(UIImage *image) {
                weakSelf.scuffleCard1.originImage = self.scuffleCard1.image;
            }];
            
        } else if (i == 2) {
            self.scuffleCard2.model = item;
            [self.scuffleCard2 uploadScuffleImageUrl:item.skinIds.firstObject callback:^(UIImage *image) {
                weakSelf.scuffleCard2.originImage = self.scuffleCard2.image;
            }];
            self.scuffleCard2.originImage = self.scuffleCard2.image;
        } else {
            self.scuffleCard3.model = item;
            [self.scuffleCard3 uploadScuffleImageUrl:item.skinIds.firstObject callback:^(UIImage *image) {
                weakSelf.scuffleCard3.originImage = self.scuffleCard3.image;
            }];
            self.scuffleCard3.originImage = self.scuffleCard3.image;
        }
    }
}

/**
 * 主要用于获取对手的信息
 */
- (void)fetchMemberInfoWithId:(NSString *)memberId fetchComplication:(PDScuffleBlockP0)complication {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:findTaMemberInfo requestParams:@{@"memberId":memberId} responseObjectClass:[PDFindMemberInfo class] succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDFindMemberInfo *memberInfo = (PDFindMemberInfo *)responseObject;
            PDMemberObj *memberObj = [PDMemberObj new];
            memberObj.goldBalance = memberInfo.goldBalance;
            memberObj.nickname = memberInfo.member.nickname;
            memberObj.avatar = memberInfo.member.avatar;
            weakSelf.otherInfoView.otherModel = memberObj;
            
            // 更新准备界面上对手的信息
            PDScuffleReadyModel *readyModel = [PDScuffleReadyModel new];
            readyModel.nickname = memberObj.nickname;
            readyModel.avatar = memberObj.avatar;
            weakSelf.readyUpView.model = readyModel;
            
            if (complication) {
                complication();
            }
        }
    }];
}

/**
 * 准备完毕，出牌
 */
- (void)fetchPlay {
    [[ZCAudioTool sharedZCAudioTool] playSoundWithSoundName:@"mic_scuffle_start.mp3"];
    
    // 出牌策略
    NSMutableArray *key1Arr = [NSMutableArray array];
    NSMutableArray *key2Arr = [NSMutableArray array];
    NSMutableArray *key3Arr = [NSMutableArray array];
    for (PDScuffleCard *card in self.upsideMineCardView0.cards) {
        NSDictionary *keyDic = @{@"type_id":card.model.cardType.cardId,
                                 @"skin_id":card.model.skinIds.firstObject};
        [key1Arr addObject:keyDic];
    }
    for (PDScuffleCard *card in self.upsideMineCardView1.cards) {
        NSDictionary *keyDic = @{@"type_id":card.model.cardType.cardId,
                                 @"skin_id":card.model.skinIds.firstObject};
        [key2Arr addObject:keyDic];
    }
    for (PDScuffleCard *card in self.upsideMineCardView2.cards) {
        NSDictionary *keyDic = @{@"type_id":card.model.cardType.cardId,
                                 @"skin_id":card.model.skinIds.firstObject};
        [key3Arr addObject:keyDic];
    }
    
    NSDictionary *cardDic = @{@"key1":key1Arr,
                              @"key2":key2Arr,
                              @"key3":key3Arr};
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:scufflePlayCards requestParams:@{@"groundId":self.groundId,@"mateId":self.matchModel.mateId,@"strategy":[PDCenterTool jsonFromObjc:cardDic]} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            
            
            weakSelf.gameState = PDScuffleGameStatePlaying;
            
            // 如果此事对手还没出牌，那么应该有个提示
            BOOL isDraw = [responseObject[@"draw"] boolValue];
            if (!isDraw) {
                [weakSelf.tipBar setText:@"等待对手准备..."];
                [weakSelf setPlayButtonAlready];
            } else {
                [weakSelf setPlayButtonPlaying];
            }
            
            // 出过牌
            weakSelf.isPlayed = YES;
        }
    }];
}

/**
 * 点击继续
 */
- (void)fetchContinue {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:scuffleContinue requestParams:@{@"groundId":self.groundId, @"mateId":self.matchModel.mateId} responseObjectClass:[PDScuffleContinueModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDScuffleContinueModel *continueModel = (PDScuffleContinueModel *)responseObject;
            
            if ([continueModel.opponent isEqualToString:@"wait"]) {
                weakSelf.matchState = PDScuffleMatchStateSel;
                [weakSelf setPlayButtonReady:NO];
                return;
            }
            
            // 开始新一局
            [weakSelf setPlayButtonReady:YES];
            
            weakSelf.isPlayed = NO;

            // 需要更新两人的金币
            [weakSelf updateMineGold:continueModel.memberBalance];
            [weakSelf updateOthersGold:continueModel.opponentBalance];
            
            [weakSelf timecountWithGameTime:continueModel.newGameGameTime];
            
            [weakSelf.tipBar setText:@"新一局开始"];
            [weakSelf resetOtherCards];
            
            [[ZCAudioTool sharedZCAudioTool] stopWithAudioName:@"mic_scuffle_ready.wav"];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [[ZCAudioTool sharedZCAudioTool] playAudioWithAudioName:@"mic_scuffle_ready.wav"];
            });
        }
    }];
}

#pragma mark - 支付相关

/**
 * 充值
 */
- (void)fetchTopupList {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:walletPayList requestParams:nil responseObjectClass:[PDCenterTopUpList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        if (isSucceeded) {
            PDCenterTopUpList *topUpList = (PDCenterTopUpList *)responseObject;
            [weakSelf showTopupAlert:topUpList];
        }
    }];
}

/** 请求订单 type: 1 -> 支付宝; 2 -> 微信支付*/
- (void)fetchOrderWithType:(NSInteger)type selectedItem:(PDCenterTopUpItem *)item {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerOrder requestParams:@{@"count":@(item.gold),@"money":item.cash} responseObjectClass:[PDTopUpOrder class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            
            PDTopUpOrder *topUpOrder = (PDTopUpOrder *)responseObject;
            
            if (type == 1) {
                PDPayOrderModel *order = [[PDPayOrderModel alloc] init];
                order.tradeNO = topUpOrder.ID;
                order.amount = [NSString stringWithFormat:@"%.2lf",topUpOrder.totalPrice.floatValue];
                order.productName = topUpOrder.orderName;
                order.productDescription = topUpOrder.orderName;
                PDAlipayHandle1 *alipayHandle1 = [PDAlipayHandle1 payHandle];
                alipayHandle1.notifyUrl = topUpOrder.alipayNotifyUrl;
                [alipayHandle1 payWithOrder:order payResultComplication:^(NSInteger code, NSString *errMsg) {
                    __strong typeof(weakSelf) strongSelf = weakSelf;
                    if (code == 9000) {
                        [strongSelf paySuccess];
                    } else {
                        [strongSelf payFail];
                    }
                }];
            } else {
                // 这个order可以用在支付宝最新版
                Order *order = [[Order alloc] init];
                order.biz_content = [BizContent new];
                order.biz_content.out_trade_no = topUpOrder.ID;
                order.biz_content.total_amount = [NSString stringWithFormat:@"%.2lf",topUpOrder.totalPrice.floatValue];
                order.biz_content.body = topUpOrder.orderName;
                
                PDWXPayHandle *wxpayHandle = [PDWXPayHandle payHandle];
                wxpayHandle.notifyUrl = topUpOrder.tenpayNotifyUrl;
                [wxpayHandle sendPayRequestWithOrder:order];
            }
            
        }
    }];
}

#pragma mark - 显示弹框

- (void)showTopupAlert:(PDCenterTopUpList *)list {
    __weak typeof(self) weakSelf = self;
    [[PDAlertView sharedAlertView] showScuffleAlertForTopupWithTitle:@"金币充值" topupList:list actions:^(PDCenterTopUpItem *item) {
        [weakSelf showPaySheetViewWithTopupItem:item];
    }];
}

- (void)showPaySheetViewWithTopupItem:(PDCenterTopUpItem *)item {
    PDPaySheetView *payView = [PDPaySheetView pay];
    payView.canTouchedCancle = NO;
    __weak typeof(self) weakSelf = self;
    [payView payWithSelectedType:^(NSInteger type) {
        [weakSelf fetchOrderWithType:type selectedItem:item];
    }];
}

- (void)paySuccess {
    // 支付成功
    [PDHUD showHUDProgress:@"支付成功！" diary:2];
    
    // 重新获取自己的金币
    [self fetchMemberInfoSynchronize:NO];
}

- (void)payFail {
    // 支付失败
    [PDHUD showHUDProgress:@"支付失败！" diary:2];
}

- (void)showOpponentLeaveAlert {
    [JCAlertView dismissAllCompletion:nil];
    __weak typeof(self) weakSelf = self;
    [[PDAlertView sharedAlertView] showScuffleAlertWithTitle:@"提示" content:@"您的对手被吓跑并没有选择继续，是否重新匹配对手？" buttonTitles:@[@"我要走", @"重新匹配"] actionComplication:^(NSUInteger index) {
        if (index == 0) {
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
        } else {
            [weakSelf resetMatching];
        }
    }];
}

#pragma mark - Socket

- (void)socketDidBackData:(id)responseObject {
    [super socketDidBackData:responseObject];
    
    __weak typeof(self) weakSelf = self;
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *dic = (NSDictionary *)responseObject;
            NSDictionary *data = dic[@"data"];
            NSNumber *type = data[@"type"];
            switch (type.intValue) {
                case ScuffleTypeCardsGameOnlineCount: {
                    self.scenceModel.memberCount = [data[@"membersCount"] integerValue];
                    self.scenceView.model = self.scenceModel;
                }
                    break;
                case ScuffleTypeCardsGameMatch: { // 匹配成功
                    [[ZCAudioTool sharedZCAudioTool] playSoundWithSoundName:@"mic_scuffle_mate_scuss.mp3"];
                    
                    // 匹配成功，请准备
                    self.gameState = PDScuffleGameStateReady;
                    
                    // 游戏开始
                    self.matchState = PDScuffleMatchStatePlay;
                    
                    self.mineState = PDScuffleMineSatePlaying;
                    
                    // 对手信息获取
                    self.otherInfoView.alpha = 1;
                    PDScuffleSocketMatchModel *matchModel = [[PDScuffleSocketMatchModel alloc] initWithJSONDict:data];
                    self.matchModel = matchModel;
                    
                    // 更新自己的金币
                    [self updateMineGold:matchModel.balance];
                    
//                    获取到对手基本信息
                    [self fetchMemberInfoWithId:matchModel.opponentId fetchComplication:^{
                        
                        weakSelf.opponentState = PDOpponentStateExist;
                        // 成功
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                            [weakSelf.matchingLabel reset];
                            [weakSelf.readyUpView dismiss:nil];
                            [weakSelf.readyDownView dismiss:nil];
                            [weakSelf setCancleNormal];
                            [weakSelf timecountWithGameTime:matchModel.gameTime];
                            [weakSelf.tipBar setText:@"请放置卡牌后点击准备按钮"];
                            
                            // 播放准备音效
                            [[ZCAudioTool sharedZCAudioTool] playAudioWithAudioName:@"mic_scuffle_ready.wav"];
                        });
                    }];
                    
                    // 正计时结束
                    [self.matchingLabel stop];
                    // 头像光圈滚动结束
                    [self.readyUpView stopRotation];
                    [self.readyDownView stopRotation];
                    // 取消按钮变更
                    [self setCancleUnabled];
                    
                    [self setPlayButtonReady:YES];
                    self.isPlayed = NO;
                    
                    [self setMineCardsSelState];
                }
                    break;
                case ScuffleTypeCardsGameKickout: {
                    [self.tipBar setText:@"对手被踢"];
                    [self resetMineCards];
                    [self resetOtherCards];
                    self.gameState = PDScuffleGameStateReady;
                    self.opponentState = PDOpponentStateExist;
                    self.matchState = PDScuffleMatchStateSel;
                }
                    break;
                case ScuffleTypeCardsGameDraw: {
                    self.gameState = PDScuffleGameStatePlaying;
                    self.opponentState = PDOpponentStateExist;
                    self.matchState = PDScuffleMatchStateSel;
                    
                    [self setPlayButtonPlaying];
                    [self setMineCardsPlayState];
                    
                    [self.timeView dismissTime];
                    [self.tipBar dismissTip];
                    self.isPlayed = NO;
                    
                    // 拿到对手卡牌信息
                    PDScuffleSocketDrawModel *drawModel = [[PDScuffleSocketDrawModel alloc] initWithJSONDict:data];
                    self.drawModel = drawModel;
                    [self updateOpponentCardsWithDrawModel:drawModel];
                }
                    break;
                case ScuffleTypeCardsGameOpponentReady: {
                    [self.tipBar setText:@"已准备"];
                    
                    [[ZCAudioTool sharedZCAudioTool] playSoundWithSoundName:@"mic_scuffle_start.mp3"];
                    
                    self.opponentState = PDOpponentStateExist;
                    self.matchState = PDScuffleMatchStatePlay;
                }
                    break;
                case ScuffleTypeCardsGameOpponentEscape: { // 逃跑
                    // 对家逃跑，游戏结束
                    self.opponentState = PDOpponentStateLeave;
                    self.matchState = PDScuffleMatchStateSel;
                    
                    [self setPlayButtonReady:YES];
                    
                    [self.timeView showResult:PDScuffleDrawResultScussed];
                    
                    // 刷新自己的金币
                    [self fetchMemberInfoSynchronize:NO];
                    // 提示对手已离开
                    [self.tipBar setText:@"对手已离开..."];
                    
                    // 淡化对手的信息
                    [UIView animateWithDuration:0.8 animations:^{
                        self.otherInfoView.alpha = 0;
                    }];
                    
                    [self resetOtherCards];
                    [self resetMineCards];
                    
                    NSString *content = [NSString stringWithFormat:@"您的对手在比赛中途退出，您直接获得胜利，金币+%@。是否继续对战？",@(self.scenceModel.gold)];
                    [[PDAlertView sharedAlertView] showScuffleAlertWithTitle:@"提示" content:content buttonTitles:@[@"我要走",@"重新匹配"] actionComplication:^(NSUInteger index) {
                        // 隐藏tip
                        [weakSelf.tipBar dismissTip];
                        
                        if (index == 0) {
                            [weakSelf dismissViewControllerAnimated:YES completion:nil];
                        } else {// 重新回到匹配界面
                            [weakSelf resetMatching];
                        }
                    }];
                }
                    break;
                case ScuffleTypeCardsGameOpponentQuit: { // 退出
                    // 提示对手已离开
                    [self.tipBar setText:@"已离开..."];
                    
                    self.opponentState = PDOpponentStateLeave;
                    self.matchState = PDScuffleMatchStateSel;
                    
                    [self setPlayButtonReady:YES];
                    
                    // 淡化对手的信息
                    [UIView animateWithDuration:0.8 animations:^{
                        self.otherInfoView.alpha = 0;
                    }];
                    
                    [self resetOtherCards];
                    [self resetMineCards];
                    
                    [self showOpponentLeaveAlert];
                }
                    break;
                    
                    // 第一个人点击之后，等第二个人点继续会有一个新游戏的socket
                case ScuffleTypeCardsGameNewGame: {
                    // 开始新一局
                    self.opponentState = PDOpponentStateExist;
                    self.matchState = PDScuffleMatchStatePlay;
                    self.isPlayed = NO;
                    
                    [self setPlayButtonReady:YES];
                    
                    PDScuffleSocketNewGameModel *gameModel = [[PDScuffleSocketNewGameModel alloc] initWithJSONDict:data];
                    // 需要更新两人的金币
                    [self updateMineGold:gameModel.memberBalance];
                    [self updateOthersGold:gameModel.opponentBalance];
                    
                    [self timecountWithGameTime:gameModel.newGameGameTime];
                    
                    [self.tipBar setText:@"新一局开始"];
                    [self resetOtherCards];
                    
                    [self setMineCardsSelState];
                    
                    [[ZCAudioTool sharedZCAudioTool] stopWithAudioName:@"mic_scuffle_ready.wav"];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [[ZCAudioTool sharedZCAudioTool] playAudioWithAudioName:@"mic_scuffle_ready.wav"];
                    });
                }
                    break;
                    // 对手选择继续，此时我还没选择；如果我已经选择了，那么直接发开始新一局的socket
                case ScuffleTypeCardsGameMemberContinue: {
                    [self.tipBar setText:@"TA选择继续..."];
                    
                    self.gameState = PDScuffleGameStateReady;
                    self.opponentState = PDOpponentStateExist;
                    self.matchState = PDScuffleMatchStateSel;
                }
                    break;
                default:
                    break;
            }
    }
}

#pragma mark - 匹配
/**
 * 重新开始匹配
 */
- (void)resetMatching {
    __weak typeof(self) weakSelf = self;
    if (!self.readyUpView.isDisplay) {
        [self.readyUpView ready];
        [self.readyUpView display:nil];
    }
    if (!self.readyDownView.isDisplay) {
        [self.readyDownView display:^{
            // 重新开始匹配，关闭准备音效
            [[ZCAudioTool sharedZCAudioTool] stopWithAudioName:@"mic_scuffle_ready.wav"];
            // 匹配界面展示出来，开始匹配
            [weakSelf fetchBeginMatching];
            // 开始正计时
            [weakSelf.matchingLabel beginTiming];
        }];
    }
    [self.tipBar dismissTip];
    
    [self.timeView dismissTime];
    [self.timeView dismissResult];
    
    self.isPlayed = NO;

    [self setPlayButtonReady:YES];
}

#pragma mark - 对手牌面信息

- (void)updateOpponentCardsWithDrawModel:(PDScuffleSocketDrawModel *)drawModel {
    // 左边第一路
    PDScuffleSocketResultModel *result0 = drawModel.opponentDrawResult.up;
    PDScuffleSocketResultModel *result1 = drawModel.opponentDrawResult.middle;
    PDScuffleSocketResultModel *result2 = drawModel.opponentDrawResult.down;
    
    NSMutableArray *result0Cards = [NSMutableArray array];
    NSMutableArray *result1Cards = [NSMutableArray array];
    NSMutableArray *result2Cards = [NSMutableArray array];
    
    NSUInteger value0 = 0,value1 = 0, value2 = 0;
    
    for (PDScuffleSocketCardItem *card in result0.cards) {
        PDScuffleCardItem *cardItem = [[PDScuffleCardItem alloc] init];
        cardItem.cardType = card.type;
        value0 += card.type.value;
        
        PDScuffleObjCard *otherCard = [[PDScuffleObjCard alloc] init];
        otherCard.model = cardItem;
        [otherCard uploadScuffleImageUrl:card.skinId callback:^(UIImage *image) {
            if (result0.result == PDScuffleDrawResultFailed) {
                otherCard.image = [image garyImage];
            }
        }];
       
        [self.upsideOtherCardView0 addSubview:otherCard];
        [result0Cards addObject:otherCard];
        
        [self.otherCards addObject:otherCard];
    }
    
    for (PDScuffleSocketCardItem *card in result1.cards) {
        PDScuffleCardItem *cardItem = [[PDScuffleCardItem alloc] init];
        cardItem.cardType = card.type;
        value1 += card.type.value;
        
        PDScuffleObjCard *otherCard = [[PDScuffleObjCard alloc] init];
        otherCard.model = cardItem;
        [otherCard uploadScuffleImageUrl:card.skinId callback:^(UIImage *image) {
            if (result1.result == PDScuffleDrawResultFailed) {
                otherCard.image = [image garyImage];
            }
        }];
        
        [self.upsideOtherCardView1 addSubview:otherCard];
        [result1Cards addObject:otherCard];
        
        [self.otherCards addObject:otherCard];
    }
    
    for (PDScuffleSocketCardItem *card in result2.cards) {
        PDScuffleCardItem *cardItem = [[PDScuffleCardItem alloc] init];
        cardItem.cardType = card.type;
        value2 += card.type.value;
        
        PDScuffleObjCard *otherCard = [[PDScuffleObjCard alloc] init];
        otherCard.model = cardItem;
        [otherCard uploadScuffleImageUrl:card.skinId callback:^(UIImage *image) {
            if (result2.result == PDScuffleDrawResultFailed) {
                otherCard.image = [image garyImage];
            }
        }];
        
        [self.upsideOtherCardView2 addSubview:otherCard];
        [result2Cards addObject:otherCard];
        
        [self.otherCards addObject:otherCard];
    }
    
    [[ZCAudioTool sharedZCAudioTool] playSoundWithSoundName:@"mic_scuffle_opencard.mp3"];
    [UIView transitionWithView:self.upsideOtherCardView0 duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.upsideOtherCardView0.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_moveplace_notext"];
        
        for (int i = 0; i < result0Cards.count; i++) {
            PDScuffleObjCard *card = result0Cards[i];
            if (result0Cards.count == 1) {
                card.frame = [self.upsideOtherCardView0 getFrameInCardViewAtIndex:0];
            } else {
                card.frame = [self.upsideOtherCardView0 getFrameInCardViewAtIndex:i+1];
            }
        }
        
    } completion:^(BOOL finished) {
        if (finished == NO) {
            return;
        }
        self.upsideOtherCardView0.model = [NSString stringWithFormat:@"%@",@(value0)];
        
        // 处理自己的卡
        if (result0.result == PDScuffleDrawResultScussed) {
            for (PDScuffleCard *mineCard in self.upsideMineCardView0.cards) {
                mineCard.image = [mineCard.image garyImage];
            }
        }
    }];
    
    // 过1s去翻第二张
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[ZCAudioTool sharedZCAudioTool] playSoundWithSoundName:@"mic_scuffle_opencard.mp3"];
        [UIView transitionWithView:self.upsideOtherCardView1 duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
            self.upsideOtherCardView1.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_moveplace_notext"];
            
            for (int i = 0; i < result1Cards.count; i++) {
                PDScuffleObjCard *card = result1Cards[i];
                if (result1Cards.count == 1) {
                    card.frame = [self.upsideOtherCardView1 getFrameInCardViewAtIndex:0];
                } else {
                    card.frame = [self.upsideOtherCardView1 getFrameInCardViewAtIndex:i+1];
                }
            }
        } completion:^(BOOL finished) {
            if (finished == NO) {
                return;
            }
            self.upsideOtherCardView1.model = [NSString stringWithFormat:@"%@",@(value1)];
            
            if (result1.result == PDScuffleDrawResultScussed) {
                for (PDScuffleCard *mineCard in self.upsideMineCardView1.cards) {
                    mineCard.image = [mineCard.image garyImage];
                }
            }
        }];
    });
    
    // 过2s去翻第三张
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [[ZCAudioTool sharedZCAudioTool] playSoundWithSoundName:@"mic_scuffle_opencard.mp3"];
        [UIView transitionWithView:self.upsideOtherCardView2 duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
            self.upsideOtherCardView2.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_moveplace_notext"];
            
            for (int i = 0; i < result2Cards.count; i++) {
                PDScuffleObjCard *card = result2Cards[i];
                if (result2Cards.count == 1) {
                    card.frame = [self.upsideOtherCardView2 getFrameInCardViewAtIndex:0];
                } else {
                    card.frame = [self.upsideOtherCardView2 getFrameInCardViewAtIndex:i+1];
                }
            }
        } completion:^(BOOL finished) {
            if (finished == NO) {
                return;
            }
            self.upsideOtherCardView2.model = [NSString stringWithFormat:@"%@",@(value2)];
            
            if (result2.result == PDScuffleDrawResultScussed) {
                for (PDScuffleCard *mineCard in self.upsideMineCardView2.cards) {
                    mineCard.image = [mineCard.image garyImage];
                }
            }
            
            // 记录对手的卡翻开
            self.otherCardState = PDScuffleCardOpen;
            
            // 在最后翻牌后，更新liangr的金币
            [self updateBotherGold];
            
            // 显示结果
            if (self.drawModel.result == PDScuffleDrawResultScussed) {
                [self.timeView showResult:PDScuffleDrawResultScussed];
                [[ZCAudioTool sharedZCAudioTool] playSoundWithSoundName:@"mic_scuffle_victory.mp3"];
                
                // 显示加金币动画
                [self showAddGoldAnimationWithGoldAdd:self.scenceModel.gold];
            } else if (self.drawModel.result == PDScuffleDrawResultFailed) {
                [self.timeView showResult:PDScuffleDrawResultFailed];
                [[ZCAudioTool sharedZCAudioTool] playSoundWithSoundName:@"mic_scuffle_defeat.mp3"];
            } else {
                [self.timeView showResult:PDScuffleDrawResultDraw];
                [[ZCAudioTool sharedZCAudioTool] playSoundWithSoundName:@"mic_scuffle_draw.mp3"];
            }
            
            
            // 延迟一会再提示是否要继续对局
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                
                if (self.opponentState == PDOpponentStateLeave || !self || self.mineState == PDScuffleMineSateNone) {
                    return;
                }
                
                __weak typeof(self) weakSelf = self;
                [[PDAlertView sharedAlertView] showScuffleAlertWithTitle:@"提示" content:@"对局结束，是否继续与TA对战？" buttonTitles:@[@"我要走",@"继续"] actionComplication:^(NSUInteger index) {
                    if (index == 0) {
                        [weakSelf fetchStopMatching];
                    } else {
                        [weakSelf continueAction];
                    }
                }];
                
                [self.tipBar setText:@"等待对手选择..."];
                [self.timeView dismissResult];
            });
        }];
    });
}

#pragma mark - 继续逻辑
/**
 * 点击继续
 */
- (void)continueAction {
    if (self.opponentState == PDOpponentStateLeave) {
        [self showOpponentLeaveAlert];
        return;
    }
    
    if ([self isGoldEnough]) {
        [self fetchContinue];
        [self resetMineCards];
    } else {
        // 去充值
        [self fetchTopupList];
    }
}

/**
 * 判断本人的金币够不够
 */
- (BOOL)isGoldEnough {
    if (self.currentGold < self.scenceModel.gold) {
        return NO;
    }
    return YES;
}

#pragma mark - 更新金币

- (void)updateBotherGold {
    if (self.drawModel.result == PDScuffleDrawResultFailed) {
        [self updateOthersGold:self.drawModel.opponentBalance];
    } else if (self.drawModel.result == PDScuffleDrawResultScussed) {
        [self updateMineGold:self.drawModel.memberBalance];
    } else {
        [self updateOthersGold:self.drawModel.opponentBalance];
        [self updateMineGold:self.drawModel.memberBalance];
    }
}

- (void)updateMineGold:(NSUInteger)gold {
    self.currentGold = gold;
    self.goldCompose.title = [PDCenterTool numberStringChangeWithoutSpecialCharacter:gold];
}

- (void)updateOthersGold:(NSUInteger)gold {
    self.otherInfoView.otherModel.goldBalance = gold;
    self.otherInfoView.otherModel = self.otherInfoView.otherModel;
}

#pragma mark - 重置对手卡牌信息

- (void)resetOtherCards {
    if (self.otherCardState == PDScuffleCardReverse) {
        return;
    }
    
    for (PDScuffleObjCard *card in self.otherCards) {
        [card removeFromSuperview];
    }
    if (self.otherCards.count) {
        [self.otherCards removeAllObjects];
    }
    
    [UIView transitionWithView:self.upsideOtherCardView0 duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.upsideOtherCardView0.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_noopen"];
    } completion:^(BOOL finished) {
        if (finished == NO) {
            return;
        }
        self.upsideOtherCardView0.model = @"";
    }];
    
    [UIView transitionWithView:self.upsideOtherCardView1 duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.upsideOtherCardView1.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_noopen"];
    } completion:^(BOOL finished) {
        if (finished == NO) {
            return;
        }
        self.upsideOtherCardView1.model = @"";
    }];
    
    [UIView transitionWithView:self.upsideOtherCardView2 duration:0.5 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.upsideOtherCardView2.placeholdImageView.image = [UIImage imageNamed:@"icon_scuffle_card_noopen"];
    } completion:^(BOOL finished) {
        if (finished == NO) {
            return;
        }
        self.upsideOtherCardView2.model = @"";
        
        self.otherCardState = PDScuffleCardReverse;
    }];
}

#pragma mark - 重置自己的卡牌信息

- (void)resetMineCards {
    while (self.upsideMineCardView0.cards.count) {
        PDScuffleCard *card = self.upsideMineCardView0.cards.firstObject;
        card.image = card.originImage;
        [self moveToOriginWithCard:card duration:0.1];
    }
    
    while (self.upsideMineCardView1.cards.count) {
        PDScuffleCard *card = self.upsideMineCardView1.cards.firstObject;
        card.image = card.originImage;
        [self moveToOriginWithCard:card duration:0.1];
    }
    
    while (self.upsideMineCardView2.cards.count) {
        PDScuffleCard *card = self.upsideMineCardView2.cards.firstObject;
        card.image = card.originImage;
        [self moveToOriginWithCard:card duration:0.1];
    }
    
    self.gameState = PDScuffleGameStateReady;
}

#pragma mark - 倒计时

- (void)timecountWithGameTime:(NSTimeInterval)gameTime {
    __weak typeof(self) weakSelf = self;
    [self.timeView showTimecountWithGameTime:gameTime timeResult:^{
        if (!weakSelf.isPlayed) {
            [weakSelf fetchPlay];
        }
    }];
}

#pragma mark - 显示加钱的动画

- (PDStrokeLabel *)addLabel {
    if (!_addLabel) {
        _addLabel = [[PDStrokeLabel alloc] init];
        _addLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
        _addLabel.backgroundColor = [UIColor clearColor];
        _addLabel.textColor = [UIColor hexChangeFloat:@"fad46f"];
    }
    return _addLabel;
}

- (void)showAddGoldAnimationWithGoldAdd:(NSUInteger)add {
    self.addLabel.text = [NSString stringWithFormat:@"+%@",@(add*1.95)];
    [self.view addSubview:self.addLabel];
    
    CGSize addSize = [self.addLabel.text sizeWithCalcFont:self.addLabel.font];
    self.addLabel.frame = CGRectMake(CGRectGetMinX(self.goldCompose.frame) + (CGRectGetWidth(self.goldCompose.frame) - addSize.width)/2, kScreenBounds.size.height, addSize.width, addSize.height);
    
    [UIView animateWithDuration:2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.addLabel.alpha = 0.3;
        self.addLabel.transform = CGAffineTransformMakeScale(2, 2);
        self.addLabel.frame = CGRectOffset(self.addLabel.frame, 0, -120);
    } completion:^(BOOL finished) {
        [self.addLabel removeFromSuperview];
        self.addLabel.alpha = 1;
        self.addLabel.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

@end
