//
//  PDScuffleEnterViewController.h
//  PandaKing
//
//  Created by Cranz on 17/4/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 大乱斗入口控制器
@interface PDScuffleEnterViewController : AbstractViewController

@end
