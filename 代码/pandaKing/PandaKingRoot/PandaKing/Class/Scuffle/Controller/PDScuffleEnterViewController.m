//
//  PDScuffleEnterViewController.m
//  PandaKing
//
//  Created by Cranz on 17/4/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleEnterViewController.h"
#import "PDScuffleViewController.h"
#import "PDScuffleSelAlertCell.h"
#import "PDAlertView+PDScuffleAlert.h"
#import "PDScuffleEnterList.h"
#import "ZCAudioTool.h"

// 支付
#import "PDTopUpOrder.h"
#import "PDAlipayHandle.h"
#import "PDAlipayOrderInfo.h"
#import "PDWXPayHandle.h"
// 金币充值
#import "PDPaySheetView.h"

// 配置入口个数
#define kSCUFFLE_ENTER_N 5

@interface PDScuffleEnterViewController ()
<
UITableViewDataSource,
UITableViewDelegate,
PDScuffleSelAlertCellDelegate
>

@property (nonatomic, strong) UIView *topBar;
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, assign) CGSize rowSize;
@property (nonatomic, strong) PDScuffleEnterList *enterList;
@property (nonatomic, strong) NSMutableArray *itemsArr;

@end

@implementation PDScuffleEnterViewController

- (NSArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

- (void)dealloc {
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupBackgroundView];
    [self setupTopBar];
    [self setupTableView];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self fetchData];
}

- (void)setupBackgroundView {
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_scuffle_bg"]];
    backgroundImageView.frame = self.view.bounds;
    [self.view addSubview:backgroundImageView];
}

- (void)setupTopBar {
    self.topBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    [self.view addSubview:self.topBar];
    
    UIImage *titleImage = [UIImage imageNamed:@"text_scuffle_lolscuffle"];
    UIImageView *titleImageView = [[UIImageView alloc] initWithImage:titleImage];
    titleImageView.frame = CGRectMake((kScreenBounds.size.width - titleImage.size.width) / 2, 20 + (44 - titleImage.size.height) / 2, titleImage.size.width, titleImage.size.height);
    [self.topBar addSubview:titleImageView];
    
    // 返回按钮
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *backImage = [UIImage imageNamed:@"icon_scuffle_back"];
    [backButton setBackgroundImage:backImage forState:UIControlStateNormal];
    [backButton setFrame:CGRectMake(kTableViewSectionHeader_left, 20 + (44 - backImage.size.height) / 2, backImage.size.width, backImage.size.height)];
    [self.topBar addSubview:backButton];
    [backButton addTarget:self action:@selector(didClickBack) forControlEvents:UIControlEventTouchUpInside];
    
    // 问号按钮
    UIButton *questionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *ruleImage = [UIImage imageNamed:@"icon_scuffle_rule"];
    [questionButton setBackgroundImage:ruleImage forState:UIControlStateNormal];
    [questionButton setFrame:CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - ruleImage.size.width, 20 + (44 - ruleImage.size.height) / 2, ruleImage.size.width, ruleImage.size.height)];
    [self.topBar addSubview:questionButton];
    [questionButton addTarget:self action:@selector(didClickQuestion) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupTableView {
    CGFloat tb_left = LCFloat(40);
    CGFloat tb_top = LCFloat(30);
    CGFloat tb_bottom = LCFloat(38);
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(tb_left, CGRectGetMaxY(self.topBar.frame) + tb_top, kScreenBounds.size.width - tb_left * 2, kScreenBounds.size.height - 64 - tb_bottom - tb_top)];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mainTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.mainTableView];
    
    _rowSize = CGSizeMake(kScreenBounds.size.width - tb_left * 2, CGRectGetHeight(self.mainTableView.frame) / kSCUFFLE_ENTER_N);
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.mainTableView.bounds];
    backgroundImageView.image = [UIImage imageNamed:@"bg_scuffle_god_bg"];
    self.mainTableView.backgroundView = backgroundImageView;
    self.mainTableView.rowHeight = _rowSize.height;
}

#pragma mark - 控件方法

- (void)didClickBack {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didClickQuestion {
    [[PDAlertView sharedAlertView] showScuffleRuleAlert];
}

#pragma mark - UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellID = @"scuffleEnterCellId";
    PDScuffleSelAlertCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    if (!cell) {
        cell = [[PDScuffleSelAlertCell alloc] initWithRowSize:_rowSize reuseIdentifier:cellID];
        cell.delegate = self;
    }
    cell.indexPath = indexPath;
    if (self.itemsArr.count > indexPath.row) {
        cell.model = self.itemsArr[indexPath.row];
    }
    
    return cell;
}

#pragma mark - PDScuffleSelAlertCellDelegate

- (void)scuffleCell:(PDScuffleSelAlertCell *)cell didClickAtGoldModel:(PDScuffleEnterItem *)model atIndexPath:(NSIndexPath *)indexPath {
    
    [self fetchMemberTotalGoldWithModel:model];
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:scuffleEnterList requestParams:nil responseObjectClass:[PDScuffleEnterList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            weakSelf.enterList = (PDScuffleEnterList *)responseObject;
            if (weakSelf.itemsArr.count) {
                [weakSelf.itemsArr removeAllObjects];
            }
            [weakSelf.itemsArr addObjectsFromArray:weakSelf.enterList.grounds];
            [weakSelf.mainTableView reloadData];
        }
    }];

}

/** 会员金币资产*/
- (void)fetchMemberTotalGoldWithModel:(PDScuffleEnterItem *)model {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberWallet requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            NSNumber *totalgold = responseObject[@"totalgold"]; // 总资产
            
            if (totalgold.integerValue < model.gold) {
                [weakSelf fetchTopupList];
            } else {
                PDScuffleViewController *scuffleViewController = [[PDScuffleViewController alloc] init];
                scuffleViewController.groundId = model.ID;
                PDScuffleScenceModel *scence = [[PDScuffleScenceModel alloc] init];
                scence.memberCount = model.inGroundMembersCount;
                scence.gold = model.gold;
                scuffleViewController.scenceModel = scence;
                
                scuffleViewController.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
                [weakSelf presentViewController:scuffleViewController animated:YES completion:nil];
            }
        }
    }];
}

#pragma mark - 支付相关

/**
 * 充值
 */
- (void)fetchTopupList {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:walletPayList requestParams:nil responseObjectClass:[PDCenterTopUpList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        if (isSucceeded) {
            PDCenterTopUpList *topUpList = (PDCenterTopUpList *)responseObject;
            [weakSelf showTopupAlert:topUpList];
        }
    }];
}

/** 请求订单 type: 1 -> 支付宝; 2 -> 微信支付*/
- (void)fetchOrderWithType:(NSInteger)type selectedItem:(PDCenterTopUpItem *)item {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerOrder requestParams:@{@"count":@(item.gold),@"money":item.cash} responseObjectClass:[PDTopUpOrder class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            
            PDTopUpOrder *topUpOrder = (PDTopUpOrder *)responseObject;
            
            if (type == 1) {
                PDPayOrderModel *order = [[PDPayOrderModel alloc] init];
                order.tradeNO = topUpOrder.ID;
                order.amount = [NSString stringWithFormat:@"%.2lf",topUpOrder.totalPrice.floatValue];
                order.productName = topUpOrder.orderName;
                order.productDescription = topUpOrder.orderName;
                PDAlipayHandle1 *alipayHandle1 = [PDAlipayHandle1 payHandle];
                alipayHandle1.notifyUrl = topUpOrder.alipayNotifyUrl;
                [alipayHandle1 payWithOrder:order payResultComplication:^(NSInteger code, NSString *errMsg) {
                    __strong typeof(weakSelf) strongSelf = weakSelf;
                    if (code == 9000) {
                        [strongSelf paySuccess];
                    } else {
                        [strongSelf payFail];
                    }
                }];
            } else {
                // 这个order可以用在支付宝最新版
                Order *order = [[Order alloc] init];
                order.biz_content = [BizContent new];
                order.biz_content.out_trade_no = topUpOrder.ID;
                order.biz_content.total_amount = [NSString stringWithFormat:@"%.2lf",topUpOrder.totalPrice.floatValue];
                order.biz_content.body = topUpOrder.orderName;
                
                PDWXPayHandle *wxpayHandle = [PDWXPayHandle payHandle];
                wxpayHandle.notifyUrl = topUpOrder.tenpayNotifyUrl;
                [wxpayHandle sendPayRequestWithOrder:order];
            }
            
        }
    }];
}

#pragma mark - 显示弹框

- (void)showTopupAlert:(PDCenterTopUpList *)list {
    __weak typeof(self) weakSelf = self;
    [[PDAlertView sharedAlertView] showScuffleAlertForTopupWithTitle:@"金币不足" topupList:list actions:^(PDCenterTopUpItem *item) {
        [weakSelf showPaySheetViewWithTopupItem:item];
    }];
}

- (void)showPaySheetViewWithTopupItem:(PDCenterTopUpItem *)item {
    PDPaySheetView *payView = [PDPaySheetView pay];
    payView.canTouchedCancle = NO;
    __weak typeof(self) weakSelf = self;
    [payView payWithSelectedType:^(NSInteger type) {
        [weakSelf fetchOrderWithType:type selectedItem:item];
    }];
}

- (void)paySuccess {
    // 支付成功
    [PDHUD showHUDProgress:@"支付成功！" diary:2];
}

- (void)payFail {
    // 支付失败
    [PDHUD showHUDProgress:@"支付失败！" diary:2];
}

@end
