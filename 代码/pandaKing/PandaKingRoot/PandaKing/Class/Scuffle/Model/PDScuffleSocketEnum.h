//
//  PDScuffleSocketEnum.h
//  PandaKing
//
//  Created by Cranz on 17/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#ifndef PDScuffleSocketEnum_h
#define PDScuffleSocketEnum_h

typedef NS_ENUM(NSUInteger, PDScuffleSocketType) {
    /**
     * LOL大乱斗-在线人数
     */
    ScuffleTypeCardsGameOnlineCount = 1000,
    /**
     * LOL大乱斗-匹配
     */
    ScuffleTypeCardsGameMatch = 1010,
    /**
     *  LOL大乱斗-踢出
     */
    ScuffleTypeCardsGameKickout = 1020,
    /**
     * LOL大乱斗-开奖
     */
    ScuffleTypeCardsGameDraw = 1030,
    /**
     * LOL大乱斗-对家出牌
     */
    ScuffleTypeCardsGameOpponentReady = 1040,
    /**
     * LOL大乱斗-对家逃跑
     */
    ScuffleTypeCardsGameOpponentEscape = 1050,
    /**
     * LOL大乱斗-对家退出
     */
    ScuffleTypeCardsGameOpponentQuit = 1060,
    /**
     * LOL大乱斗-继续新游戏
     */
    ScuffleTypeCardsGameNewGame = 1070,
    /**
     * LOL大乱斗-对手点击继续
     */
    ScuffleTypeCardsGameMemberContinue = 1080,
};


#endif /* PDScuffleSocketEnum_h */
