//
//  PDScuffleCardTypeModel.m
//  PandaKing
//
//  Created by Cranz on 17/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleCardTypeModel.h"

@implementation PDScuffleCardTypeModel
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"cardId":@"id"};
}
@end
