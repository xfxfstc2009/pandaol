//
//  PDScuffleCardItem.h
//  PandaKing
//
//  Created by Cranz on 17/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDScuffleCardTypeModel.h"

/*
 "cardType": {
 "id": "6",
 "value": 7800
 },
 "skinIds": ["1"],
 "defaultSkinId": null
 */

@protocol PDScuffleCardItem <NSObject>

@end

@interface PDScuffleCardItem : FetchModel
@property (nonatomic, strong) PDScuffleCardTypeModel *cardType;
/**
 * 皮肤素组ids
 */
@property (nonatomic, strong) NSArray *skinIds;
/**
 * 默认皮肤id，当没有时取皮肤数组的第一个
 */
@property (nonatomic, copy) NSString *defaultSkinId;
@end
