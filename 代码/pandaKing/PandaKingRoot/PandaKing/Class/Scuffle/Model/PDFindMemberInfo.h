//
//  PDFindMemberInfo.h
//  PandaKing
//
//  Created by Cranz on 16/10/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDFindMember.h"

@interface PDFindMemberInfo : FetchModel
@property (nonatomic, strong) PDFindMember *member;
@property (nonatomic, assign) BOOL isFollow;                      /**< 是否关注*/
@property (nonatomic, copy)   NSString *follow;                     /**< 关注数量*/
@property (nonatomic, copy)   NSString *fans;                       /**< 关注的粉丝数量*/
@property (nonatomic, assign) NSTimeInterval latestStakeTime;     /**< 最后的投注时间*/
@property (nonatomic, copy)   NSString *gameUserName;               /**< 游戏召唤师名字*/
@property (nonatomic, assign) NSUInteger goldBalance;
@property (nonatomic, copy)   NSString *imUserId;           /**<  im的uid*/
@end
