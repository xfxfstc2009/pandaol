//
//  PDScuffleEnterItem.h
//  PandaKing
//
//  Created by Cranz on 17/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDScuffleEnterItem <NSObject>

@end

@interface PDScuffleEnterItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, assign) NSUInteger gold;
/**
 * 在线人数
 */
@property (nonatomic, assign) NSUInteger inGroundMembersCount;
@end
