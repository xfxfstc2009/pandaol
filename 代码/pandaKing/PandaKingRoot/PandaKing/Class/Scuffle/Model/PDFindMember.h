//
//  PDFindMember.h
//  PandaKing
//
//  Created by Cranz on 16/10/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDFindMember : FetchModel
@property (nonatomic,copy)NSString *personId;                   /**< 用户编号*/
@property (nonatomic,copy)NSString *nickname;                   /**< 用户昵称*/
@property (nonatomic,copy)NSString *cellphone;                  /**< 联系号码*/
@property (nonatomic,copy)NSString *gender;                     /**< 性别*/
@property (nonatomic,assign)BOOL newGender;                /**< 新性别*/
@property (nonatomic,copy)NSString *age;                        /**< 年龄*/
@property (nonatomic,copy)NSString *qq;                         /**< QQ*/
@property (nonatomic,copy)NSString *weChat;                     /**< 微信号*/
@property (nonatomic,copy)NSString *signature;                  /**< 签名*/
@property (nonatomic,copy)NSString *avatar;                     /**< 用户头像*/
@property (nonatomic,strong)NSArray *bindGames;                 /**< 绑定的游戏*/
@end
