//
//  PDScuffleSocketCardItem.h
//  PandaKing
//
//  Created by Cranz on 17/5/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDScuffleCardTypeModel.h"

@protocol PDScuffleSocketCardItem <NSObject>

@end

@interface PDScuffleSocketCardItem : FetchModel
@property (nonatomic, strong) PDScuffleCardTypeModel *type;
@property (nonatomic, copy) NSString *skinId;
@end
