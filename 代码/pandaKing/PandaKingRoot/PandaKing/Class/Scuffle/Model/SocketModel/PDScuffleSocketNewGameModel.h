//
//  PDScuffleSocketNewGameModel.h
//  PandaKing
//
//  Created by Cranz on 17/5/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDScuffleSocketNewGameModel : FetchModel
@property (nonatomic, assign) NSTimeInterval newGameStartTime;
@property (nonatomic, assign) NSUInteger memberBalance;
@property (nonatomic, assign) NSUInteger opponentBalance;
@property (nonatomic, copy)   NSString *gameId;
@property (nonatomic, assign) NSTimeInterval newGameGameTime;
@property (nonatomic, assign) NSUInteger type;
@end
