//
//  PDScuffleSocketDrawModel.h
//  PandaKing
//
//  Created by Cranz on 17/5/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDScuffleSocketDrawResult.h"

/// 开奖的
@interface PDScuffleSocketDrawModel : FetchModel
@property (nonatomic, copy) NSString *gameId;
@property (nonatomic, assign) PDScuffleDrawResult result;
@property (nonatomic, assign) NSUInteger type;
@property (nonatomic, copy) NSString *mateId;
@property (nonatomic, copy) NSString *groundId;
/**
 * 当我赢了，我有值
 */
@property (nonatomic, assign) NSUInteger memberBalance;
/**
 * 当对手赢了，对手有值；平手都有
 */
@property (nonatomic, assign) NSUInteger opponentBalance;
@property (nonatomic, strong) PDScuffleSocketDrawResult *opponentDrawResult;
@end
