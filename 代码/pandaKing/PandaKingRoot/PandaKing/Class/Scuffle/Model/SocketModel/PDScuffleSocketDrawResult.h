//
//  PDScuffleSocketDrawResult.h
//  PandaKing
//
//  Created by Cranz on 17/5/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDScuffleSocketResultModel.h"

@interface PDScuffleSocketDrawResult : FetchModel
@property (nonatomic, strong) PDScuffleSocketResultModel *middle;
@property (nonatomic, strong) PDScuffleSocketResultModel *up;
@property (nonatomic, strong) PDScuffleSocketResultModel *down;
@end
