//
//  PDScuffleSocketMatchModel.h
//  PandaKing
//
//  Created by Cranz on 17/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 匹配成功后
@interface PDScuffleSocketMatchModel : FetchModel
/**
 * 游戏id
 */
@property (nonatomic, copy) NSString *gameId;
/**
 * 对手id
 */
@property (nonatomic, copy) NSString *opponentId;
/**
 * 匹配总共给的倒计时
 */
@property (nonatomic, assign) NSTimeInterval gameTime;
/**
 * 服务器开始匹配的时间
 */
@property (nonatomic, assign) NSTimeInterval startTime;
/**
 * 扣款之后的金币余额
 */
@property (nonatomic, assign) NSTimeInterval balance;
/**
 * 配对id
 */
@property (nonatomic, copy) NSString *mateId;
/**
 * 金币场id
 */
@property (nonatomic, copy) NSString *groundId;
@property (nonatomic, assign) NSUInteger type;
@end
