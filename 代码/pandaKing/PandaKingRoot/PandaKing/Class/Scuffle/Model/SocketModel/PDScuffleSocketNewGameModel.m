//
//  PDScuffleSocketNewGameModel.m
//  PandaKing
//
//  Created by Cranz on 17/5/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleSocketNewGameModel.h"

@implementation PDScuffleSocketNewGameModel
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"gameId":@"newGameId"};
}
@end
