//
//  PDScuffleSocketResultModel.h
//  PandaKing
//
//  Created by Cranz on 17/5/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDScuffleSocketCardItem.h"

typedef NS_ENUM(int, PDScuffleDrawResult) {
    /**
     * 胜利
     */
    PDScuffleDrawResultScussed = 1,
    /**
     * 失败
     */
    PDScuffleDrawResultFailed = -1,
    /**
     * 平手
     */
    PDScuffleDrawResultDraw = 0,
};

@interface PDScuffleSocketResultModel : FetchModel
/**
 * 这个是对手的胜负
 */
@property (nonatomic, assign) PDScuffleDrawResult result;
@property (nonatomic, strong) NSArray <PDScuffleSocketCardItem> *cards;
@end
