//
//  PDScuffleCardList.h
//  PandaKing
//
//  Created by Cranz on 17/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDScuffleCardItem.h"

@interface PDScuffleCardList : FetchModel
@property (nonatomic, strong) NSArray<PDScuffleCardItem> *list;
@end
