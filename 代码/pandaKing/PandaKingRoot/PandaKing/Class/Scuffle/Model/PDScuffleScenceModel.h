//
//  PDScuffleScenceModel.h
//  PandaKing
//
//  Created by Cranz on 17/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDScuffleScenceModel : NSObject
@property (nonatomic, assign) NSUInteger memberCount;
@property (nonatomic, assign) NSUInteger gold;
@end
