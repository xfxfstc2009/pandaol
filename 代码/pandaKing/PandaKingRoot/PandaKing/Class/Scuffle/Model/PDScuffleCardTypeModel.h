//
//  PDScuffleCardTypeModel.h
//  PandaKing
//
//  Created by Cranz on 17/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDScuffleCardTypeModel : FetchModel
@property (nonatomic, copy) NSString *cardId;
@property (nonatomic, assign) int value;
@end
