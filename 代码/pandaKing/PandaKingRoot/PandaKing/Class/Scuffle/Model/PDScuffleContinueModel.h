//
//  PDScuffleContinueModel.h
//  PandaKing
//
//  Created by Cranz on 17/5/4.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDScuffleContinueModel : FetchModel
/**
 * 本人金币
 */
@property (nonatomic, assign) NSUInteger memberBalance;
@property (nonatomic, assign) NSTimeInterval newGameGameTime;
@property (nonatomic, copy) NSString *gameId;
@property (nonatomic, assign) NSTimeInterval newGameStartTime;

/**
 * 这个是对手的状态，如果是自己先点继续，那么返回
 wait
 否则两个人的数据都有
 */
@property (nonatomic, copy) NSString *opponent;
/**
 * 对手金币
 */
@property (nonatomic, assign) NSUInteger opponentBalance;
@end
