//
//  PDFindMember.m
//  PandaKing
//
//  Created by Cranz on 16/10/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFindMember.h"

@implementation PDFindMember
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"personId": @"id"};
}
@end
