//
//  PDScuffleTopupAlertCell.m
//  PandaKing
//
//  Created by Cranz on 17/4/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleTopupAlertCell.h"

#define kSCUFFLE_TOPUP_BUTTON_SIZE (CGSizeMake(LCFloat(80),LCFloat(38)))

/// 充值cell
@interface PDScuffleTopupAlertCell ()

/**
 * 钱袋图标
 */
@property (nonatomic, strong) UIImageView *goldImageView;
/**
 * xxxx金币
 */
@property (nonatomic, strong) UILabel *goldLabel;
@property (nonatomic, strong) UIImageView *line;
/**
 * 送xxxx
 */
@property (nonatomic, strong) UILabel *presentLabel;
/**
 * 绿色支付按钮
 */
@property (nonatomic, strong) UIButton *payButton;

@property (nonatomic) CGSize size;
@end

@implementation PDScuffleTopupAlertCell

+ (CGFloat)cellHeight {
    return LCFloat(58);
}

- (instancetype)initWithCellWidth:(CGFloat)width reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        _size = CGSizeMake(width, [PDScuffleTopupAlertCell cellHeight]);
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_scuffle_alert_topup_cell_bg"]];
    self.backgroundView = backgroundImageView;
    
    UIImage *goldImage = [UIImage imageNamed:@"alert_scuffle_enter_button_img"];
    self.goldImageView = [[UIImageView alloc] initWithImage:goldImage];
    self.goldImageView.frame = CGRectMake((LCFloat(70) - goldImage.size.width)/2, (_size.height - goldImage.size.height) / 2, goldImage.size.width, goldImage.size.height);
    [self.contentView addSubview:self.goldImageView];
    
    self.goldLabel = [[UILabel alloc] init];
    self.goldLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.goldLabel.textColor = c44;
    [self.contentView addSubview:self.goldLabel];
    
    self.line = [[UIImageView alloc] init];
    self.line.image = [UIImage imageNamed:@"icon_scuffle_alert_line"];
    [self.contentView addSubview:self.line];
    
    self.presentLabel = [[UILabel alloc] init];
    self.presentLabel.font = [UIFont systemFontOfCustomeSize:11];
    self.presentLabel.textColor = c44;
    [self.contentView addSubview:self.presentLabel];
    
    self.payButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.payButton.titleLabel.font = [[UIFont systemFontOfCustomeSize:12] boldFont];
    [self.payButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.payButton setBackgroundImage:[UIImage imageNamed:@"bg_scuffle_alert_button_green"] forState:UIControlStateNormal];
    [self.payButton setFrame:CGRectMake((_size.width - LCFloat(10) - kSCUFFLE_TOPUP_BUTTON_SIZE.width), (_size.height - kSCUFFLE_TOPUP_BUTTON_SIZE.height) / 2, kSCUFFLE_TOPUP_BUTTON_SIZE.width, kSCUFFLE_TOPUP_BUTTON_SIZE.height)];
    [self.contentView addSubview:self.payButton];
    [self.payButton addTarget:self action:@selector(payAction) forControlEvents:UIControlEventTouchUpInside];
}

/**
 * 点击付款
 */
- (void)payAction {
    if ([self.delegate respondsToSelector:@selector(scuffleCell:didClickCellWithModel:)]) {
        [self.delegate scuffleCell:self didClickCellWithModel:self.model];
    }
}

- (void)setModel:(PDCenterTopUpItem *)model {
    _model = model;
    
    self.goldLabel.text = [NSString stringWithFormat:@"%@金币",[PDCenterTool numberStringChange:model.gold]];
    self.presentLabel.text = [NSString stringWithFormat:@"送%@",[PDCenterTool numberStringChange:model.goldAwardForPay]];
    [self.payButton setTitle:[NSString stringWithFormat:@"%@元",model.cash] forState:UIControlStateNormal];
    
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    CGSize presentSize = [self.presentLabel.text sizeWithCalcFont:self.presentLabel.font];
    
    if (model.goldAwardForPay == 0) { // 此时没有送
        self.line.hidden = YES;
        self.presentLabel.hidden = YES;
        
        self.goldLabel.frame = CGRectMake(CGRectGetMaxX(self.goldImageView.frame) + 10, (_size.height - goldSize.height) / 2, goldSize.width, goldSize.height);
    } else {
        self.line.hidden = NO;
        self.presentLabel.hidden = NO;
        
        CGFloat space = (_size.height - goldSize.height - presentSize.height) / 3;
        self.goldLabel.frame = CGRectMake(CGRectGetMaxX(self.goldImageView.frame) + 10, space, goldSize.width, goldSize.height);
        self.line.frame = CGRectMake(CGRectGetMinX(self.goldLabel.frame), CGRectGetMaxY(self.goldLabel.frame) + (space - self.line.image.size.height) / 2, self.line.image.size.width, self.line.image.size.height);
        self.presentLabel.frame = CGRectMake(CGRectGetMinX(self.goldLabel.frame), CGRectGetMaxY(self.goldLabel.frame) + space, presentSize.width, presentSize.height);
    }
}

@end

