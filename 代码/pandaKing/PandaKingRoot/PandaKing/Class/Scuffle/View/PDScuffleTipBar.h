//
//  PDScuffleTipBar.h
//  PandaKing
//
//  Created by Cranz on 17/4/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PDScuffleTipAnimationType) {
    PDScuffleTipAnimationTypeLeft,
    PDScuffleTipAnimationTypeFade,
};

/// 至于顶部的黄色文案提示
@interface PDScuffleTipBar : UIView
/**
 * 默认13
 */
@property (nonatomic, strong) UIFont *font;
/**
 * 默认金色
 */
@property (nonatomic, strong) UIColor *textColor;
/**
 * 默认有一张单灰色的背景图
 */
@property (nonatomic, strong) UIImage *backgroundImage;
/**
 * 当这个tip在忙碌时，不能进行重复显示
 */
@property (nonatomic, assign) BOOL isBusy;

- (instancetype)initWithFrame:(CGRect)frame type:(PDScuffleTipAnimationType)type;

- (void)setText:(NSString *)text;
- (void)setText:(NSString *)text delayed:(NSTimeInterval)delay;
- (void)dismissTip;


@end
