//
//  PDScuffleAlertView.h
//  PandaKing
//
//  Created by Cranz on 17/4/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDScuffleAlertView : UIView

- (instancetype)initWithTitle:(NSString *)title content:(NSString *)content buttonTitlesArr:(NSArray *)titlesArr;
- (void)alertViewClickHandle:(void(^)(NSUInteger index))handle;

@end
