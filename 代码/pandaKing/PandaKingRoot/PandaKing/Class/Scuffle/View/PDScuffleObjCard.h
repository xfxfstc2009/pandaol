//
//  PDScuffleObjCard.h
//  PandaKing
//
//  Created by Cranz on 17/4/17.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDImageView.h"
#import "PDScuffleCardItem.h"

@interface PDScuffleObjCard : PDImageView
/**
 * 自带属性封杀！！！
 */
@property (nonatomic, strong) PDScuffleCardItem *model;

/**
 * 原始图片,用于从灰色图片恢复
 */
@property (nonatomic, strong) UIImage *originImage;

@end
