//
//  PDAlertView+PDScuffleAlert.h
//  PandaKing
//
//  Created by Cranz on 17/4/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAlertView.h"
#import "PDScuffleAlertView.h"
#import "PDScuffleTopupAlertView.h"
#import "PDScuffleRuleAlertView.h"

@interface PDAlertView (PDScuffleAlert)
/**
 * 普通弹框
 */
- (void)showScuffleAlertWithTitle:(NSString *)title
                          content:(NSString *)content
                     buttonTitles:(NSArray *)titles
               actionComplication:(void(^)(NSUInteger index))complication;

/**
 * 金币充值弹框
 */
- (void)showScuffleAlertForTopupWithTitle:(NSString *)title
                                topupList:(PDCenterTopUpList *)list
                                  actions:(void(^)(PDCenterTopUpItem *item))actions;
- (void)showScuffleRuleAlert;

@end
