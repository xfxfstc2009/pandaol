//
//  PDScuffleCard.m
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleCard.h"

@interface PDScuffleCard ()
@property (nonatomic, strong, readwrite) UIPanGestureRecognizer *pan;
@property (nonatomic, strong, readwrite) UITapGestureRecognizer *tap;
@property (nonatomic, readwrite) CGRect originRect;
@end

@implementation PDScuffleCard

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupConfig];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupConfig];
    }
    return self;
}

- (void)setupConfig {
    self.isMoving = NO;
    self.originRect = self.frame;
    self.userInteractionEnabled = YES;
    self.pan = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panAction:)];
    [self addGestureRecognizer:self.pan];
    
    self.tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
    [self addGestureRecognizer:self.tap];
}

- (void)panAction:(UIPanGestureRecognizer *)pan {
    if ([self.delegate respondsToSelector:@selector(scuffleCard:didMoveCardWithGestureRecognizer:)]) {
        [self.delegate scuffleCard:self didMoveCardWithGestureRecognizer:pan];
    }
}

- (void)tapAction:(UITapGestureRecognizer *)tap {
    if ([self.delegate respondsToSelector:@selector(didTapAtScuffleCard:)]) {
        [self.delegate didTapAtScuffleCard:self];
    }
}

@end
