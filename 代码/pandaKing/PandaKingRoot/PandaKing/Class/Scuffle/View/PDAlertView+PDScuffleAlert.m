//
//  PDAlertView+PDScuffleAlert.m
//  PandaKing
//
//  Created by Cranz on 17/4/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAlertView+PDScuffleAlert.h"

@implementation PDAlertView (PDScuffleAlert)

- (void)showScuffleAlertWithTitle:(NSString *)title content:(NSString *)content buttonTitles:(NSArray *)titles actionComplication:(void (^)(NSUInteger))complication {
    PDScuffleAlertView *scuffleAlertView = [[PDScuffleAlertView alloc] initWithTitle:title content:content buttonTitlesArr:titles];
    [JCAlertView initWithCustomView:scuffleAlertView dismissWhenTouchedBackground:NO];
    [scuffleAlertView alertViewClickHandle:^(NSUInteger index) {
        if (complication) {
            complication(index);
        }
        [JCAlertView dismissAllCompletion:NULL];
    }];
}

- (void)showScuffleAlertForTopupWithTitle:(NSString *)title topupList:(PDCenterTopUpList *)list actions:(void (^)(PDCenterTopUpItem *))actions {
    PDScuffleTopupAlertView *topupAlertView = [[PDScuffleTopupAlertView alloc] initWithTopupList:list title:title];
    [JCAlertView initWithCustomView:topupAlertView dismissWhenTouchedBackground:YES];
    [topupAlertView didClickActions:^(PDCenterTopUpItem *item) {
        if (actions) {
            actions(item);
        }
        [JCAlertView dismissAllCompletion:NULL];
    }];
    [topupAlertView didClickCloseActions:^{
        [JCAlertView dismissAllCompletion:NULL];
    }];
}

- (void)showScuffleRuleAlert {
    PDScuffleRuleAlertView *ruleAlert = [[PDScuffleRuleAlertView alloc] init];
    [JCAlertView initWithCustomView:ruleAlert dismissWhenTouchedBackground:YES];
    [ruleAlert alertCancleBlock:^{
        [JCAlertView dismissAllCompletion:NULL];
    }];
}

@end
