//
//  PDScuffleResultView.m
//  PandaKing
//
//  Created by Cranz on 17/5/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleResultView.h"

@interface PDScuffleResultView ()
@property (nonatomic, weak) NSTimer *timer;
@property (nonatomic, strong) UIImageView *resultImageView;
@property (nonatomic, strong) UIImageView *timeImageView;

@property (nonatomic, assign) NSTimeInterval gameTime;

@property (nonatomic, copy) PDScufflerResultBlock resultBlock;

@property (nonatomic, strong) CAEmitterLayer *emitterLayer;
@end

@implementation PDScuffleResultView

- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeAction) userInfo:nil repeats:YES];
    }
    return _timer;
}

- (CAEmitterLayer *)emitterLayer {
    if (!_emitterLayer) {
        _emitterLayer = [CAEmitterLayer layer];
        _emitterLayer.position = CGPointMake(self.bounds.size.width / 2, -CGRectGetMinY(self.frame));
        _emitterLayer.emitterSize = CGSizeMake(self.bounds.size.width - 40, 0);
        _emitterLayer.emitterShape = kCAEmitterLayerLine;
        _emitterLayer.emitterMode = kCAEmitterLayerOutline;
        [self.layer addSublayer:_emitterLayer];
        
        CAEmitterCell *cell = [CAEmitterCell emitterCell];
        cell.lifetime = 8;
        cell.name = @"gold";
        cell.scale = 0.1;
        cell.scaleRange = 0.05;
        cell.spin = 0.3;
        cell.spinRange = 0.2;
        cell.yAcceleration = 200;
        cell.contents = (id)[UIImage imageNamed:@"icon_scuflle_golds"].CGImage;
        _emitterLayer.emitterCells = @[cell];
    }
    return _emitterLayer;
}

- (void)timeAction {
    if (self.gameTime <= 0) {
        [self.timer invalidate];
        self.timer = nil;
        
        if (self.resultBlock) {
            self.resultBlock();
        }
    }
    
    UIImage *timeImage = [UIImage imageNamed:[NSString stringWithFormat:@"icon_chaosFight_other_time_%@",@(self.gameTime)]];
    self.timeImageView.frame = CGRectMake((self.frame.size.width - timeImage.size.width) / 2, (self.frame.size.height - timeImage.size.height) / 2, timeImage.size.width, timeImage.size.height);
    self.timeImageView.image = timeImage;
    
    self.gameTime--;
}

- (UIImageView *)resultImageView {
    if (!_resultImageView) {
        _resultImageView = [[UIImageView alloc] init];
        UIImage *image = [UIImage imageNamed:@"icon_scuffle_result_scuss"];
        _resultImageView.frame = CGRectMake((self.frame.size.width - image.size.width) / 2, (self.frame.size.height - image.size.height) / 2, image.size.width, image.size.height);
        [self addSubview:_resultImageView];
    }
    return _resultImageView;
}

- (UIImageView *)timeImageView {
    if (!_timeImageView) {
        _timeImageView = [[UIImageView alloc] init];
        [self addSubview:_timeImageView];
    }
    return _timeImageView;
}

- (void)showResult:(PDScuffleDrawResult)result {
    if (result == PDScuffleDrawResultScussed) {
        self.resultImageView.image = [UIImage imageNamed:@"icon_scuffle_result_scuss"];
    } else if (result == PDScuffleDrawResultFailed) {
        self.resultImageView.image = [UIImage imageNamed:@"icon_scuffle_result_fail"];
    } else {
        self.resultImageView.image = [UIImage imageNamed:@"icon_scuffle_result_draw"];
    }
    
    self.resultImageView.transform = CGAffineTransformMakeScale(0, 0);
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.resultImageView.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {
        if (finished && result == PDScuffleDrawResultScussed) {
            // 显示金币雨
            [self showGoldRain];
        }
    }];
}

- (void)showTimecountWithGameTime:(NSTimeInterval)gameTime timeResult:(PDScufflerResultBlock)results {
    self.timeImageView.transform = CGAffineTransformMakeScale(0, 0);
    UIImage *timeImage = [UIImage imageNamed:[NSString stringWithFormat:@"icon_chaosFight_other_time_%@",@(self.gameTime)]];
    self.timeImageView.frame = CGRectMake((self.frame.size.width - timeImage.size.width) / 2, (self.frame.size.height - timeImage.size.height) / 2, timeImage.size.width, timeImage.size.height);
    self.timeImageView.image = timeImage;
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.timeImageView.transform = CGAffineTransformMakeScale(1, 1);
    } completion:^(BOOL finished) {

    }];
    
    self.gameTime = gameTime / 1000;
    [self.timer fire];
    
    if (results) {
        self.resultBlock = results;
    }
}

- (void)dismissResult {
    [UIView animateWithDuration:0.3 animations:^{
        self.resultImageView.transform = CGAffineTransformMakeScale(0, 0);
    }];
}

- (void)dismissTime {
    [self.timer invalidate];
    self.timer = nil;
    [UIView animateWithDuration:0.3 animations:^{
        self.timeImageView.transform = CGAffineTransformMakeScale(0, 0);
    }];
}

- (void)showGoldRain {
    [self.emitterLayer removeAnimationForKey:@"goldAnim"];
    
    CABasicAnimation *velocityAnim = [CABasicAnimation animationWithKeyPath:@"emitterCells.gold.velocity"];
    velocityAnim.fromValue = @1;
    velocityAnim.toValue = @10;
    
    CABasicAnimation *birthAnim = [CABasicAnimation animationWithKeyPath:@"emitterCells.gold.birthRate"];
    birthAnim.fromValue = @8;
    birthAnim.toValue = @15;
    
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.duration = 1;
    group.animations = @[velocityAnim, birthAnim];
    
    [self.emitterLayer addAnimation:group forKey:@"goldAnim"];
}

@end
