//
//  PDScuffleSelAlertCell.m
//  PandaKing
//
//  Created by Cranz on 17/4/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleSelAlertCell.h"

// 按钮的size：LCFloat(336/2)，LCFloat(107/2)
#define kSCUFFLE_SEL_BUTTON_SIZE (CGSizeMake(LCFloat(390/2),LCFloat(107/2)))
@interface PDScuffleSelAlertCell ()
@property (nonatomic, strong) UIButton *selButton;
@property (nonatomic, strong) UILabel *countLabel;
@property (nonatomic, assign) CGSize size;
/**
 * 按钮和在线人数之间的空隙
 */
@property (nonatomic) CGFloat space;
/**
 * cell上下两边的间隙
 */
@property (nonatomic) CGFloat vEdge;
@end

@implementation PDScuffleSelAlertCell

- (instancetype)initWithRowSize:(CGSize)rowSize reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor clearColor];
        _size = rowSize;
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.selButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.selButton setBackgroundImage:[UIImage imageNamed:@"alert_scuffle_enter_button_bg"] forState:UIControlStateNormal];
    [self.selButton setImage:[UIImage imageNamed:@"alert_scuffle_enter_button_img"] forState:UIControlStateNormal];
    self.selButton.titleLabel.font = [UIFont systemFontOfCustomeSize:16];
    [self.contentView addSubview:self.selButton];
    [self.selButton addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
    
    self.countLabel = [[UILabel alloc] init];
    self.countLabel.font = [[UIFont systemFontOfCustomeSize:14] boldFont];
    self.countLabel.textColor = [UIColor whiteColor];
    [self.contentView addSubview:self.countLabel];
    
    CGFloat textHeight = [NSString contentofHeightWithFont:self.countLabel.font];
    _space = 5;
    _vEdge = (_size.height - kSCUFFLE_SEL_BUTTON_SIZE.height - textHeight - _space)/2;
    
    self.selButton.frame = CGRectMake((_size.width - kSCUFFLE_SEL_BUTTON_SIZE.width)/2, _vEdge, kSCUFFLE_SEL_BUTTON_SIZE.width, kSCUFFLE_SEL_BUTTON_SIZE.height);
}

- (void)didClickButton {
    if ([self.delegate respondsToSelector:@selector(scuffleCell:didClickAtGoldModel:atIndexPath:)]) {
        [self.delegate scuffleCell:self didClickAtGoldModel:self.model atIndexPath:self.indexPath];
    }
}

- (void)setModel:(PDScuffleEnterItem *)model {
    _model = model;
    
    [self.selButton setTitle:[NSString stringWithFormat:@"%@金币场",@(model.gold)] forState:UIControlStateNormal];
    
    NSShadow *countShadow = [[NSShadow alloc] init];
    countShadow.shadowColor = [UIColor blueColor];
    countShadow.shadowBlurRadius = 5;
    self.countLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@人在线",@(model.inGroundMembersCount)] attributes:@{NSShadowAttributeName:countShadow}];
    
    CGSize countSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font];
    self.countLabel.frame = CGRectMake((_size.width - countSize.width)/2, CGRectGetMaxY(self.selButton.frame) + _space, countSize.width, countSize.height);
}

@end
