//
//  PDScuffleReadyView.h
//  PandaKing
//
//  Created by Cranz on 17/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDScuffleReadyModel : NSObject
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *nickname;
@end


typedef NS_ENUM(NSUInteger, PDReadyStatus) {
    PDReadyStatusUp,
    PDReadyStatusDown,
};


typedef void(^PDReadyViewBlock)();

/// 准备页面的view，显示两个玩家的头像
@interface PDScuffleReadyView : UIImageView

@property (nonatomic, strong) PDScuffleReadyModel *model;
/**
 * 是否是展示状态,刚创建出来的时候是展示状态YES
 */
@property (nonatomic) BOOL isDisplay;
@property (nonatomic, readonly) PDReadyStatus status;

+ (instancetype)readyWithBackgroundImage:(UIImage *)image status:(PDReadyStatus)status;
/**
 * 展示，从隐藏状态展示出来
 */
- (void)display:(PDReadyViewBlock)displayComplication;
/**
 * 从展示状态隐藏
 */
- (void)dismiss:(PDReadyViewBlock)dismissComplication;

/**
 * 停止滚动
 */
- (void)stopRotation;
/**
 * 开始滚动，创建好的时候是默认滚动的
 */
- (void)startRotation;

- (void)ready;

@end
