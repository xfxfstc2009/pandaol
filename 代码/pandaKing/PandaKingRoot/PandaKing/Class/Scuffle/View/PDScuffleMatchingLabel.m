//
//  PDScuffleMatchingLabel.m
//  PandaKing
//
//  Created by Cranz on 17/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleMatchingLabel.h"

@interface PDScuffleMatchingLabel ()
@property (nonatomic, weak) NSTimer *timer;
@property (nonatomic, strong) UIView *leftLine;
@property (nonatomic, strong) UIView *rightLine;
@property (nonatomic, strong) UILabel *timeLabel;
/**
 * 计时的秒
 */
@property (nonatomic, assign) int sec;
@end

@implementation PDScuffleMatchingLabel

- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeAction) userInfo:nil repeats:YES];
    }
    return _timer;
}

- (void)timeAction {
    self.timeLabel.text = [NSString stringWithFormat:@"正在为你匹配对手(%d)",_sec];
    [self updateMatchViewFrame];
    _sec++;
}

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:CGRectMake(-kScreenBounds.size.width, (kScreenBounds.size.height - 40) / 2, kScreenBounds.size.width, 40)];
    if (self) {
        [self setupMatchingView];
    }
    return self;
}

- (void)setupMatchingView {
    self.leftLine = [[UIView alloc] init];
    [self addSubview:self.leftLine];
    
    self.rightLine = [[UIView alloc] init];
    [self addSubview:self.rightLine];
    
    self.leftLine.backgroundColor = self.rightLine.backgroundColor = c2;
    
    self.timeLabel = [[UILabel alloc] init];
    self.timeLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.textColor = c2;
    [self addSubview:self.timeLabel];
}

- (void)updateMatchViewFrame {
    CGSize size = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font];
    self.timeLabel.frame = CGRectMake((self.frame.size.width - size.width) / 2, (self.frame.size.height - size.height) / 2, size.width, size.height);
    CGFloat width = (self.frame.size.width - size.width - 20 - 12) / 2;
    self.leftLine.frame = CGRectMake(10, (self.frame.size.height - 1) / 2, width, 1);
    self.rightLine.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame) + 6, (self.frame.size.height - 1) / 2, width, 1);
}

- (void)beginTiming {
    _sec = 0;
    
    self.alpha = 1;
    self.timeLabel.text = @"正在为你匹配对手(0)";
    [self updateMatchViewFrame];
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = CGRectMake(0, (kScreenBounds.size.height - 40) / 2, kScreenBounds.size.width, 40);
    } completion:^(BOOL finished) {
        [self timeAction];
        [self.timer fire];
    }];
}

- (void)stop {
    [self.timer invalidate];
    self.timer = nil;
    _sec = 0;
    
    self.timeLabel.text = @"已成功匹配您的对手，等待系统处理...";
    [self updateMatchViewFrame];
}

- (void)reset {
    [UIView animateWithDuration:0.35 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        self.frame = CGRectMake(-kScreenBounds.size.width, (kScreenBounds.size.height - 40) / 2, kScreenBounds.size.width, 40);
    }];
}

@end
