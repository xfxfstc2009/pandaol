//
//  PDScuffleTipBar.m
//  PandaKing
//
//  Created by Cranz on 17/4/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleTipBar.h"

@interface PDScuffleTipBar ()
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UILabel *textLabel;
/**
 * 结束动画的frame
 */
@property (nonatomic, assign) CGRect endFrame;
/**
 * 开始动画的frame
 */
@property (nonatomic, assign) CGRect beginFrame;
/**
 * 动画类型
 */
@property (nonatomic) PDScuffleTipAnimationType type;
/**
 * 是否在界面上
 */
@property (nonatomic) BOOL isInScreen;
@end

@implementation PDScuffleTipBar

- (instancetype)initWithFrame:(CGRect)frame type:(PDScuffleTipAnimationType)type {
    self = [super initWithFrame:frame];
    if (self) {
        _type = type;
        _isBusy = NO;
        [self setupBar];
    }
    return self;
}

- (void)setupBar {
    
    if (_type == PDScuffleTipAnimationTypeLeft) {
        self.endFrame = self.frame;
        self.beginFrame = CGRectMake(-self.frame.size.width, self.frame.origin.y, self.frame.size.width, self.frame.size.height);
        self.frame = self.beginFrame;
    }
    self.alpha = 0;
    
    self.backgroundImageView  =[[UIImageView alloc] initWithFrame:self.bounds];
    self.backgroundImageView.image = [UIImage imageNamed:@"bg_scuffle_tip"];
    [self addSubview:self.backgroundImageView];
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.font = [[UIFont systemFontOfCustomeSize:16] boldFont];
    self.textLabel.textColor = c14;
    [self addSubview:self.textLabel];
}

- (void)setText:(NSString *)text {
    [self setText:text delayed:0];
}

- (void)setText:(NSString *)text delayed:(NSTimeInterval)delay {
    self.textLabel.text = text;
    [self updateTextLabel];
    
    _isBusy = YES;
    if (_type == PDScuffleTipAnimationTypeLeft) {
        [self showTipAnimationWithDelay:delay];
    } else {
        [self showFadeAnimationWithDelay:delay];
    }
}

- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    
    self.textLabel.textColor = textColor;
}

- (void)setFont:(UIFont *)font {
    _font = font;
    
    self.textLabel.font = font;
    [self updateTextLabel];
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    _backgroundImage = backgroundImage;
    
    self.backgroundImageView.image = backgroundImage;
}

- (void)updateTextLabel {
    CGSize size = [self.textLabel.text sizeWithCalcFont:self.textLabel.font constrainedToSize:CGSizeMake(self.frame.size.width, [NSString contentofHeightWithFont:self.textLabel.font])];
    self.textLabel.frame = CGRectMake((self.frame.size.width - size.width) / 2, (self.frame.size.height - size.height) / 2, size.width, size.height);
}

#pragma mark - 动画

- (void)showTipAnimationWithDelay:(NSTimeInterval)delay {
    self.alpha = 1;
    self.frame = self.beginFrame;
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.5 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.frame = self.endFrame;
    } completion:^(BOOL finished) {
        if (delay <= 0) {
            _isBusy = NO;
            return;
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismissTip];
        });
    }];
}

- (void)dismissTipWithAnimation:(BOOL)animated {
    [UIView animateWithDuration:animated? 1 : 0 animations:^{
        self.alpha = 0;
    } completion:^(BOOL finished) {
        _isBusy = NO;
        if (self.type == PDScuffleTipAnimationTypeLeft) {
            self.frame = self.beginFrame;
        } 
    }];
}

- (void)dismissTip {
    [self dismissTipWithAnimation:YES];
}

- (void)showFadeAnimationWithDelay:(NSTimeInterval)delay {
    self.alpha = 0;
    [UIView animateWithDuration:0.8 animations:^{
        self.alpha = 1;
    } completion:^(BOOL finished) {
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delay * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self dismissTip];
        });
    }];
}

@end
