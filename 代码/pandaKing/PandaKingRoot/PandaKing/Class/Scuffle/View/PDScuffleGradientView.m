//
//  PDScuffleGradientView.m
//  PandaKing
//
//  Created by Cranz on 17/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleGradientView.h"

@interface PDScuffleGradientView ()
@property (nonatomic, strong) NSArray *colors;
@property (nonatomic, strong) CAGradientLayer *gLayer;
@property (nonatomic, strong) CADisplayLink *link;
// 是否在滚动
@property (nonatomic) BOOL isDisplay;
@end

@implementation PDScuffleGradientView

- (CADisplayLink *)link {
    if (!_link) {
        _link = [CADisplayLink displayLinkWithTarget:self selector:@selector(rotationAction)];
        [_link addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    }
    return _link;
}

- (instancetype)initWithFrame:(CGRect)frame colors:(NSArray *)colors {
    self = [super initWithFrame:frame];
    if (self) {
        _colors = colors;
        self.layer.cornerRadius = frame.size.width / 2;
        self.layer.masksToBounds = YES;
        [self setupGradientView];
    }
    return self;
}

- (void)setupGradientView {
    self.gLayer = [CAGradientLayer layer];
    self.gLayer.frame = self.bounds;
    self.gLayer.colors = self.colors;
    self.gLayer.locations = @[@0.2,@0.5,@0.8];
    self.gLayer.startPoint = CGPointMake(0.5, 0);
    self.gLayer.endPoint = CGPointMake(0.5, 1);
    [self.layer addSublayer:self.gLayer];
}

- (void)rotationAction {
    self.layer.transform = CATransform3DRotate(self.layer.transform, M_PI*2/300, 0, 0, 1);
}

- (void)rotation {
    self.isDisplay = YES;
    self.link.paused = NO;

}

- (void)stop {
    self.isDisplay = NO;
    self.link.paused = YES;
}

@end
