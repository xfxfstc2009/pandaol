//
//  PDScuffleOtherView.h
//  PandaKing
//
//  Created by Cranz on 17/4/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMemberObj.h"

/// 对方的信息卡
@interface PDScuffleOtherView : UIView
@property (nonatomic, strong) PDMemberObj *otherModel;
@end
