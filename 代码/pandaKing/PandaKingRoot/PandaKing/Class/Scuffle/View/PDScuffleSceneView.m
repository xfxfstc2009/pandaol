//
//  PDScuffleSceneView.m
//  PandaKing
//
//  Created by Cranz on 17/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleSceneView.h"

// 水平左右间距
#define kSCENCE_H 10
// 垂直上下间距
#define kSCENCE_V 6

typedef void(^PDScenceBlock)(CGSize size);
@interface PDScuffleSceneView ()
// 玩家人数
@property (nonatomic, strong) UILabel *countLabel;
// 金币场次
@property (nonatomic, strong) UILabel *goldLabel;
@property (nonatomic, copy) PDScenceBlock scenceBlock;
@end

@implementation PDScuffleSceneView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupScenceView];
    }
    return self;
}

- (void)setupScenceView {
    self.userInteractionEnabled = YES;
    self.image = [UIImage imageNamed:@"bg_scence_bg"];
    
    self.countLabel = [[UILabel alloc] init];
    self.countLabel.font = [[UIFont systemFontOfCustomeSize:10] boldFont];
    self.countLabel.textColor = [UIColor hexChangeFloat:@"fad679"];
    [self addSubview:self.countLabel];
    
    self.goldLabel = [[UILabel alloc] init];
    self.goldLabel.font = self.countLabel.font;
    self.goldLabel.textColor = [UIColor hexChangeFloat:@"f27b4c"];
    [self addSubview:self.goldLabel];
}

- (void)updateFrame:(void (^)(CGSize))frameHandle {
    if (frameHandle) {
        self.scenceBlock = frameHandle;
    }
}

- (void)setModel:(PDScuffleScenceModel *)model {
    _model = model;
    
    self.countLabel.text = [NSString stringWithFormat:@"%@在线",@(model.memberCount)];
    self.goldLabel.text = [NSString stringWithFormat:@"%@金币场",@(model.gold)];

    CGSize countSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font];
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    CGSize size = CGSizeMake(MAX(countSize.width, goldSize.width) + 2 * kSCENCE_H, countSize.height + 2 + goldSize.height + kSCENCE_V * 2);
    self.countLabel.frame = CGRectMake((size.width - countSize.width) / 2, kSCENCE_V, countSize.width, countSize.height);
    self.goldLabel.frame = CGRectMake((size.width - goldSize.width) / 2, CGRectGetMaxY(self.countLabel.frame) + 2, goldSize.width, goldSize.height);
    
    !self.scenceBlock ?: self.scenceBlock(size);
}

@end
