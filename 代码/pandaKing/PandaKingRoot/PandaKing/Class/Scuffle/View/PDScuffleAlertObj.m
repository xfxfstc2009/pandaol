//
//  PDScuffleAlertObj.m
//  PandaKing
//
//  Created by Cranz on 17/5/4.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleAlertObj.h"

@interface PDScuffleAlertObj ()
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UIButton *cancleButton;
@property (nonatomic, copy) void(^cancleBlock)();
@end

@implementation PDScuffleAlertObj

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupAlert];
    }
    return self;
}

- (void)setupAlert {
    UIImage *image = [UIImage imageNamed:@"bg_scuffle_alert_bg"];
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    self.backgroundImageView.image = image;
    [self addSubview:self.backgroundImageView];
    self.frame = self.backgroundImageView.bounds;
    
    // 关闭按钮
    self.cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *closeImage = [UIImage imageNamed:@"alert_scuffle_enter_close"];
    [self.cancleButton setFrame:CGRectMake(self.frame.size.width - closeImage.size.width / 2 - 5, -6, closeImage.size.width, closeImage.size.height)];
    [self.cancleButton setBackgroundImage:closeImage forState:UIControlStateNormal];
    [self addSubview:self.cancleButton];
    [self.cancleButton addTarget:self action:@selector(didClickClose) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setContentView:(UIView *)contentView {
    if (_contentView) {
        _contentView = nil;
        [_contentView removeFromSuperview];
    }
    _contentView = contentView;
    
    CGSize contentSize = contentView.frame.size;
    contentView.frame = CGRectMake(20, 50, contentSize.width, contentSize.height);
    self.frame = CGRectMake(0, 0, contentSize.width + 20 * 2, contentSize.height+ 30 + CGRectGetMinY(contentView.frame));
    self.backgroundImageView.frame = self.bounds;
    [self addSubview:contentView];
    
    UIImage *closeImage = [UIImage imageNamed:@"alert_scuffle_enter_close"];
    self.cancleButton.frame = CGRectMake(self.frame.size.width - closeImage.size.width / 2 - 5, -6, closeImage.size.width, closeImage.size.height);
}

- (void)alertCancleBlock:(void (^)())block {
    if (block) {
        self.cancleBlock = block;
    }
}

- (void)didClickClose {
    if (self.cancleBlock) {
        self.cancleBlock();
    }
}

@end
