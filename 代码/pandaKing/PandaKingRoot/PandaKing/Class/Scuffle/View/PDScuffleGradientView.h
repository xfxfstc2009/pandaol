//
//  PDScuffleGradientView.h
//  PandaKing
//
//  Created by Cranz on 17/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 匹配界面头像背景的渐变圆圈
@interface PDScuffleGradientView : UIView
- (instancetype)initWithFrame:(CGRect)frame colors:(NSArray *)colors;
/**
 * 开始转动
 */
- (void)rotation;
/**
 * 停止转动
 */
- (void)stop;

@end
