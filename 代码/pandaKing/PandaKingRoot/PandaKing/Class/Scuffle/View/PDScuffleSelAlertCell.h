//
//  PDScuffleSelAlertCell.h
//  PandaKing
//
//  Created by Cranz on 17/4/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDScuffleEnterItem.h"

@class PDScuffleSelAlertCell;
@protocol PDScuffleSelAlertCellDelegate <NSObject>

- (void)scuffleCell:(PDScuffleSelAlertCell *)cell didClickAtGoldModel:(PDScuffleEnterItem *)model atIndexPath:(NSIndexPath *)indexPath;

@end

/// 大乱斗弹框选择cell
@interface PDScuffleSelAlertCell : UITableViewCell
@property (nonatomic, strong) PDScuffleEnterItem *model;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) id<PDScuffleSelAlertCellDelegate> delegate;

- (instancetype)initWithRowSize:(CGSize)rowSize reuseIdentifier:(NSString *)reuseIdentifier;

@end
