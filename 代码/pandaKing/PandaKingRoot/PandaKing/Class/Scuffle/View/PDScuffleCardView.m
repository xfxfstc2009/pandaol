//
//  PDScuffleCardView.m
//  PandaKing
//
//  Created by Cranz on 17/4/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleCardView.h"

#define kSCUFFLECARD_FONT [[UIFont systemFontOfCustomeSize:14]boldFont]
#define kSCUFFLECARD_TEXT_HEIGHT (LCFloat(4)+[NSString contentofHeightWithFont:kSCUFFLECARD_FONT])
#define KCARD_SCALE 1.256
@interface PDScuffleCardView ()
@property (nonatomic, strong, readwrite) UIImageView *placeholdImageView;
/**
 * 卡片底下的价格
 */
@property (nonatomic, strong) UILabel *descLabel;

@property (nonatomic, assign) PDScuffleCardType type;
@property (nonatomic, copy) NSString *placehold;

@end

@implementation PDScuffleCardView

+ (CGFloat)getHeightFromWidth:(CGFloat)width {
    CGFloat height = 0;
    // 计算英雄的高度
    height += width * KCARD_SCALE;
    // 下面字体的高度
    height += kSCUFFLECARD_TEXT_HEIGHT;
    
    return height;
}

- (NSMutableArray<PDScuffleCard *> *)cards {
    if (!_cards) {
        _cards = [NSMutableArray arrayWithCapacity:2];
    }
    return _cards;
}

- (instancetype)initWithPlaceholdImage:(UIImage *)placeholdImage origin:(CGPoint)origin constraintWidth:(CGFloat)width textType:(PDScuffleCardType)type placehold:(NSString *)placehold {
    _type = type;
    _placehold = placehold;
    self = [super initWithFrame:CGRectMake(origin.x, origin.y, width, [PDScuffleCardView getHeightFromWidth:width])];
    if (self) {
        [self setupCardViewWithPlaceholdImage:placeholdImage];
    }
    return self;
}

- (void)setupCardViewWithPlaceholdImage:(UIImage *)image {
    // 英雄
    if (_type == PDScuffleCardTypeBottom) {
        self.placeholdImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.width * KCARD_SCALE)];
    } else {
        self.placeholdImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, kSCUFFLECARD_TEXT_HEIGHT, self.frame.size.width, self.frame.size.width * KCARD_SCALE)];
    }
    
    self.placeholdImageView.userInteractionEnabled = YES;
    self.placeholdImageView.image = image;
    [self addSubview:self.placeholdImageView];

    // 价格
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = kSCUFFLECARD_FONT;
    self.descLabel.text = _placehold;
    [self updateTextframe];
    [self addSubview:self.descLabel];
}

- (void)setModel:(NSString *)model {
    _model = model;
    
    self.descLabel.text = model;
    
    [self updateTextframe];
}

- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    self.descLabel.textColor = textColor;
}

- (void)setFont:(UIFont *)font {
    _font = font;
    
    self.descLabel.font = font;
    [self updateTextframe];
}

- (void)updateTextframe {
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    if (_type == PDScuffleCardTypeBottom) {
        self.descLabel.frame = CGRectMake((self.frame.size.width - descSize.width) / 2, (self.frame.size.height - CGRectGetHeight(self.placeholdImageView.frame) - descSize.height) / 2 + CGRectGetMaxY(self.placeholdImageView.frame), descSize.width, descSize.height);
    } else {
        self.descLabel.frame = CGRectMake((self.frame.size.width - descSize.width) / 2, LCFloat(2), descSize.width, descSize.height);
    }
}

- (CGRect)getFrameInCardViewAtIndex:(NSUInteger)index {
    CGRect rect;
    CGFloat cardWidth2s = 0.0,cardHeight2s = 0.0;
    // 当有两张卡的时候，卡的宽度
    if (index != 0) {
        cardWidth2s = self.placeholdImageView.bounds.size.width / 2;
        cardHeight2s = cardWidth2s * KCARD_SCALE;
    }
    
    switch (index) {
        case 0:
            rect = self.placeholdImageView.frame;
            break;
        case 1:
            rect = CGRectMake(0, _type == PDScuffleCardTypeBottom? (self.placeholdImageView.bounds.size.height - cardHeight2s) / 2 : (kSCUFFLECARD_TEXT_HEIGHT + (self.placeholdImageView.bounds.size.height - cardHeight2s) / 2), cardWidth2s, cardHeight2s);
            break;
        case 2:
            rect = CGRectMake(cardWidth2s, _type == PDScuffleCardTypeBottom? (self.placeholdImageView.bounds.size.height - cardHeight2s) / 2 : (kSCUFFLECARD_TEXT_HEIGHT + (self.placeholdImageView.bounds.size.height - cardHeight2s) / 2), cardWidth2s, cardHeight2s);
            break;
        default:
            break;
    }
    return rect;
}


@end
