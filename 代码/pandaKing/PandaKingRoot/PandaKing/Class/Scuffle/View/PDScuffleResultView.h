//
//  PDScuffleResultView.h
//  PandaKing
//
//  Created by Cranz on 17/5/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDScuffleSocketResultModel.h"

typedef void(^PDScufflerResultBlock)();

/// 显示倒计时，战败，胜利的中间显示层
@interface PDScuffleResultView : UIView

- (void)showResult:(PDScuffleDrawResult)result;
- (void)showTimecountWithGameTime:(NSTimeInterval)gameTime timeResult:(PDScufflerResultBlock)results;

- (void)dismissTime;
- (void)dismissResult;

@end
