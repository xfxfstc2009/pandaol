//
//  PDScuffleTopupAlertView.m
//  PandaKing
//
//  Created by Cranz on 17/4/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleTopupAlertView.h"
#import "PDScuffleTopupAlertCell.h"

// 最多显示6行
#define kALERT_ROW_MAX 6

@interface PDScuffleTopupAlertView ()<UITableViewDataSource, UITableViewDelegate, PDScuffleTopupAlertCellDelegate>
@property (nonatomic, strong) UIImageView *backgroundImageView;
/**
 * 金币充值标题
 */
@property (nonatomic, strong) UILabel *titleLabel;
/**
 * 承载tb和我的金币label
 */
@property (nonatomic, strong) UIView *contentView;
/**
 * 首冲label
 */
@property (nonatomic, strong) UILabel *firstTopupLabel;
@property (nonatomic, strong) UITableView *mainTableView;
/**
 * 我的金币
 */
@property (nonatomic, strong) UILabel *goldLabel;

@property (nonatomic, strong) PDCenterTopUpList *list;

@property (nonatomic, copy) void(^scuffleTopupBlock)(PDCenterTopUpItem *item);
@property (nonatomic, copy) void(^scuffleCloseBlock)();
@end

@implementation PDScuffleTopupAlertView

- (instancetype)initWithTopupList:(PDCenterTopUpList *)list title:(NSString *)title {
    self = [super init];
    if (self) {
        self.list = list;
        [self setupAlertViewWithTitle:title];
    }
    return self;
}

- (void)setupAlertViewWithTitle:(NSString *)title {
    CGFloat totalWidth = 0;
    
    self.backgroundImageView = [[UIImageView alloc] init];
    self.backgroundImageView.image = [UIImage imageNamed:@"bg_scuffle_alert_bg"];
    self.backgroundImageView.userInteractionEnabled = YES;
    [self addSubview:self.backgroundImageView];
    
    // 定了标题，可以确定弹框的宽度
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = title;
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:29];
    self.titleLabel.textColor = [UIColor hexChangeFloat:@"583724"];
    [self addSubview:self.titleLabel];
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font];
    self.titleLabel.frame = CGRectMake(LCFloat(194/2), LCFloat(48), titleSize.width, titleSize.height);
    totalWidth = titleSize.width + LCFloat(194);
    
    // 承载金币充值的tableview和首冲
    self.contentView = [[UIView alloc] init];
    self.contentView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
    self.contentView.layer.cornerRadius = 5;
    self.contentView.layer.masksToBounds = YES;
    [self addSubview:self.contentView];
    
    // 首冲
    self.firstTopupLabel = [[UILabel alloc] init];
    self.firstTopupLabel.font = [[UIFont systemFontOfCustomeSize:12] boldFont];
    self.firstTopupLabel.textColor = c45;
    self.firstTopupLabel.text = [NSString stringWithFormat:@"首冲多送%@％金币",@(self.list.goldAwardPercentForFirstPay*100)];
    [self.contentView addSubview:self.firstTopupLabel];
    CGSize firstSize = [self.firstTopupLabel.text sizeWithCalcFont:self.firstTopupLabel.font];
    self.firstTopupLabel.frame = CGRectMake(LCFloat(25/2), 10, firstSize.width, firstSize.height);
    
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(LCFloat(10), CGRectGetMaxY(self.firstTopupLabel.frame) + 10, totalWidth - LCFloat(27) * 2, self.list.items.count <= kALERT_ROW_MAX ? self.list.items.count * ([PDScuffleTopupAlertCell cellHeight] + 10) : kALERT_ROW_MAX * ([PDScuffleTopupAlertCell cellHeight] + 10)) style:UITableViewStyleGrouped];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.dataSource  = self;
    self.mainTableView.delegate = self;
    self.mainTableView.rowHeight = [PDScuffleTopupAlertCell cellHeight];
    [self.contentView addSubview:self.mainTableView];
    
    self.contentView.frame = CGRectMake(LCFloat(17), LCFloat(93), totalWidth - LCFloat(34), CGRectGetMaxY(self.mainTableView.frame));
    self.frame = CGRectMake(0, 0, totalWidth, CGRectGetMaxY(self.contentView.frame) + LCFloat(47/2));
    self.backgroundImageView.frame = self.bounds;
    
    
    // 关闭按钮
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *closeImage = [UIImage imageNamed:@"alert_scuffle_enter_close"];
    [closeButton setFrame:CGRectMake(totalWidth - closeImage.size.width / 2 - 5, -6, closeImage.size.width, closeImage.size.height)];
    [closeButton setBackgroundImage:closeImage forState:UIControlStateNormal];
    [self addSubview:closeButton];
    [closeButton addTarget:self action:@selector(didClickClose) forControlEvents:UIControlEventTouchUpInside];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.list.items.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"topupCellId";
    PDScuffleTopupAlertCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[PDScuffleTopupAlertCell alloc] initWithCellWidth:tableView.frame.size.width reuseIdentifier:cellId];
        cell.delegate = self;
    }
    
    if (self.list.items.count > indexPath.section) {
        cell.model = self.list.items[indexPath.section];
    }
    
    return cell;
}

- (void)didClickActions:(void (^)(PDCenterTopUpItem *))actions {
    if (actions) {
        self.scuffleTopupBlock = actions;
    }
}

- (void)didClickCloseActions:(void (^)())actions {
    if (actions) {
        self.scuffleCloseBlock = actions;
    }
}

- (void)didClickClose {
    self.scuffleCloseBlock();
}

#pragma mark - PDScuffleTopupAlertCellDelegate

- (void)scuffleCell:(PDScuffleTopupAlertCell *)cell didClickCellWithModel:(PDCenterTopUpItem *)item {
    if (self.scuffleTopupBlock) {
        self.scuffleTopupBlock(item);
    }
}

@end
