//
//  PDScuffleOtherView.m
//  PandaKing
//
//  Created by Cranz on 17/4/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleOtherView.h"
#import "CCZComposeButton.h"

@interface PDScuffleOtherView ()
@property (nonatomic, strong) PDImageView *headImageView;
@property (nonatomic, strong) CCZComposeButton *nameCom;
@property (nonatomic, strong) CCZComposeButton *goldCom;
@end

@implementation PDScuffleOtherView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    CGFloat headWidth = LCFloat(97/2);
    self.headImageView = [[PDImageView alloc] initWithFrame:CGRectMake(12, 12, headWidth, headWidth)];
    self.headImageView.layer.cornerRadius = headWidth / 2;
    self.headImageView.clipsToBounds = YES;
    self.headImageView.layer.borderWidth = 1;
    self.headImageView.layer.borderColor = [UIColor brownColor].CGColor;
    [self addSubview:self.headImageView];
    
    CGFloat com_y = LCFloat(8);
    CGFloat com_s = LCFloat(2);
    CGFloat com_h = (CGRectGetHeight(self.frame) - com_s - com_y * 2) / 2;
    
    self.nameCom = [[CCZComposeButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.headImageView.frame) + LCFloat(10), com_y, 0, com_h) leftImage:nil rightImage:nil];
    self.nameCom.backgroundImage = [UIImage imageNamed:@"bg_amusement_button"];
    self.nameCom.titleColor = [UIColor whiteColor];
    self.nameCom.titleFont = [UIFont systemFontOfCustomeSize:12];
    [self addSubview:self.nameCom];
    
    self.goldCom = [[CCZComposeButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.nameCom.frame), CGRectGetMaxY(self.nameCom.frame) + com_s, 0, com_h) leftImage:[UIImage imageNamed:@"icon_amusement_gold"] rightImage:nil];
    self.goldCom.backgroundImage = [UIImage imageNamed:@"bg_amusement_button"];
    self.goldCom.titleColor = c26;
    self.goldCom.titleFont = [UIFont systemFontOfCustomeSize:12];
    [self addSubview:self.goldCom];
}

- (void)setOtherModel:(PDMemberObj *)otherModel {
    _otherModel = otherModel;
    
    self.nameCom.title = otherModel.nickname;
    self.goldCom.title = [NSString stringWithFormat:@"%@",[PDCenterTool numberStringChangeWithoutSpecialCharacter:otherModel.goldBalance]];
    [self.headImageView uploadImageWithAvatarURL:otherModel.avatar placeholder:nil callback:nil];
}

@end
