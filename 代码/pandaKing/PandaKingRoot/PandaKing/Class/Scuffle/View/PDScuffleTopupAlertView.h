//
//  PDScuffleTopupAlertView.h
//  PandaKing
//
//  Created by Cranz on 17/4/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterTopUpList.h"

/// 金币充值alert
@interface PDScuffleTopupAlertView : UIView

- (instancetype)initWithTopupList:(PDCenterTopUpList *)list title:(NSString *)title;

- (void)didClickActions:(void(^)(PDCenterTopUpItem *item))actions;
- (void)didClickCloseActions:(void(^)())actions;

@end
