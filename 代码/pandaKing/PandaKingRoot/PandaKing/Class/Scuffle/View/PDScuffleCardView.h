//
//  PDScuffleCardView.h
//  PandaKing
//
//  Created by Cranz on 17/4/11.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDScuffleCard.h"

typedef NS_ENUM(NSUInteger, PDScuffleCardType) {
    /// 文字在下
    PDScuffleCardTypeBottom,
    /// 文字在上
    PDScuffleCardTypeUp,
};

/// 英雄和其价格的容器
/// 英雄的宽高比是不变的
@interface PDScuffleCardView : UIView
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong, readonly) UIImageView *placeholdImageView;

+ (CGFloat)getHeightFromWidth:(CGFloat)width;

- (instancetype)initWithPlaceholdImage:(UIImage *)placeholdImage
                                origin:(CGPoint)origin
                       constraintWidth:(CGFloat)width
                              textType:(PDScuffleCardType)type
                             placehold:(NSString *)placehold;


@property (nonatomic, strong) NSString *model;

@property (nonatomic, strong) NSMutableArray *cards;

/**
 * 目前有3个位置：0，就是只有一张卡；1，就是有两张卡是的第0张；2，就是两张卡时的第1张
 */
- (CGRect)getFrameInCardViewAtIndex:(NSUInteger)index;

@end
