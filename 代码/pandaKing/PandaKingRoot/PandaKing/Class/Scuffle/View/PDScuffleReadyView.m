//
//  PDScuffleReadyView.m
//  PandaKing
//
//  Created by Cranz on 17/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleReadyView.h"
#import "PDScuffleGradientView.h"

#define kREADY_WIDTH (kScreenBounds.size.width)
#define kREADY_HEIGHT (kScreenBounds.size.height / 2)
#define kREADY_ANIMATION_DURATION 0.35
// 头像的宽度
#define kGRADIENT_WIDTH (LCFloat(218/2))
// 垂直方向上，头像的上方或者下方距离边框的距离
#define kGRADIENT_V (LCFloat(132/2))

@interface PDScuffleReadyView ()
@property (nonatomic, readwrite) PDReadyStatus status;
@property (nonatomic, strong) PDScuffleGradientView *gradientView; // 转动的渐变色
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@end

@implementation PDScuffleReadyView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupReadyView];
    }
    return self;
}

- (void)setupReadyView {
    _isDisplay = YES;
    self.userInteractionEnabled = YES;
    
    self.headerImageView = [[PDImageView alloc] init];
    [self setupGradientView];
    [self setupUerInfoView];
}

+ (instancetype)readyWithBackgroundImage:(UIImage *)image status:(PDReadyStatus)status {
    PDScuffleReadyView *readyView = [[PDScuffleReadyView alloc] initWithFrame:CGRectMake(0, status == PDReadyStatusUp? 0 : kREADY_HEIGHT, kREADY_WIDTH, kREADY_HEIGHT)];
    readyView.image = image;
    readyView.status = status;
    return readyView;
}

- (void)setupGradientView {
    NSArray *colors = @[(id)[UIColor hexChangeFloat:@"d0ac66"].CGColor,(id)[UIColor hexChangeFloat:@"452a08"].CGColor,(id)[UIColor hexChangeFloat:@"d0ac66"].CGColor];
    self.gradientView = [[PDScuffleGradientView alloc] initWithFrame:CGRectMake((kScreenBounds.size.width - kGRADIENT_WIDTH) / 2, self.status == PDReadyStatusUp? kGRADIENT_V : kREADY_HEIGHT - kGRADIENT_V - kGRADIENT_WIDTH, kGRADIENT_WIDTH, kGRADIENT_WIDTH) colors:colors];
    [self addSubview:self.gradientView];
    [self.gradientView rotation];
}

- (void)setupUerInfoView {
    CGPoint p = self.gradientView.frame.origin;
    self.headerImageView = [[PDImageView alloc] init];
    self.headerImageView.frame = CGRectMake(p.x + 2, p.y + 2, kGRADIENT_WIDTH - 4, kGRADIENT_WIDTH - 4);
    self.headerImageView.layer.cornerRadius = (kGRADIENT_WIDTH - 4) / 2;
    self.headerImageView.layer.masksToBounds = YES;
    [self addSubview:self.headerImageView];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.nameLabel.textColor = [UIColor hexChangeFloat:@"3a2616"];
    [self addSubview:self.nameLabel];
}

- (void)setModel:(PDScuffleReadyModel *)model {
    _model = model;
    
    if (model.avatar.length == 0) {
        self.headerImageView.image = [UIImage imageNamed:@"icon_scuffle_none"];
    } else {
        [self.headerImageView uploadImageWithAvatarURL:model.avatar placeholder:nil callback:nil];
    }
    
    self.nameLabel.text = model.nickname;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font];
    self.nameLabel.frame = CGRectMake((kScreenBounds.size.width - nameSize.width) / 2, CGRectGetMaxY(self.headerImageView.frame) + LCFloat(14), nameSize.width, nameSize.height);
}

#pragma mark - 操作

- (void)display:(PDReadyViewBlock)displayComplication {
    if (self.isDisplay == YES) {
        return;
    }
    self.hidden = NO;
    if (self.status == PDReadyStatusUp) {
        [UIView animateWithDuration:kREADY_ANIMATION_DURATION animations:^{
            self.frame = CGRectMake(0, 0, kREADY_WIDTH, kREADY_HEIGHT);
        } completion:^(BOOL finished) {
            self.isDisplay = YES;
            [self startRotation];
            if (displayComplication) {
                displayComplication();
            }
        }];
    } else {
        [UIView animateWithDuration:kREADY_ANIMATION_DURATION animations:^{
            self.frame = CGRectMake(0, kREADY_HEIGHT, kREADY_WIDTH, kREADY_HEIGHT);
        } completion:^(BOOL finished) {
            self.isDisplay = YES;
            [self startRotation];
            if (displayComplication) {
                displayComplication();
            }
        }];
    }
}

- (void)dismiss:(PDReadyViewBlock)dismissComplication {
    if (self.isDisplay == NO) {
        return;
    }
    if (self.status == PDReadyStatusUp) {
        [UIView animateWithDuration:kREADY_ANIMATION_DURATION animations:^{
            self.frame = CGRectMake(0, -kREADY_HEIGHT, kREADY_WIDTH, kREADY_HEIGHT);
        } completion:^(BOOL finished) {
            self.isDisplay = NO;
            self.hidden = YES;
            [self stopRotation];
            if (dismissComplication) {
                dismissComplication();
            }
        }];
    } else {
        [UIView animateWithDuration:kREADY_ANIMATION_DURATION animations:^{
            self.frame = CGRectMake(0, kScreenBounds.size.height, kREADY_WIDTH, kREADY_HEIGHT);
        } completion:^(BOOL finished) {
            self.isDisplay = NO;
            self.hidden = YES;
            [self stopRotation];
            if (dismissComplication) {
                dismissComplication();
            }
        }];
    }
}

- (void)stopRotation {
    [self.gradientView stop];
}

- (void)startRotation {
    [self.gradientView rotation];
}

- (void)ready {
    self.headerImageView.image = [UIImage imageNamed:@"icon_scuffle_none"];
    self.nameLabel.text = @"等待对手";
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font];
    self.nameLabel.frame = CGRectMake((kScreenBounds.size.width - nameSize.width) / 2, CGRectGetMaxY(self.headerImageView.frame) + LCFloat(14), nameSize.width, nameSize.height);
}

@end


@implementation PDScuffleReadyModel
@end
