//
//  PDScuffleSceneView.h
//  PandaKing
//
//  Created by Cranz on 17/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDScuffleScenceModel.h"

/// 屏幕右上角的view
@interface PDScuffleSceneView : UIImageView

@property (nonatomic, strong) PDScuffleScenceModel *model;

/**
 * 每次更新model的时候都会触发下面的回调
 */
- (void)updateFrame:(void(^)(CGSize size))frameHandle;

@end
