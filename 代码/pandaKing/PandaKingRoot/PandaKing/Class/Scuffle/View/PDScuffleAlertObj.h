//
//  PDScuffleAlertObj.h
//  PandaKing
//
//  Created by Cranz on 17/5/4.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDScuffleAlertObj : UIView
@property (nonatomic, strong) UIView *contentView;

- (void)alertCancleBlock:(void(^)())block;

@end
