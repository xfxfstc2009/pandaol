//
//  PDScuffleTopupAlertCell.h
//  PandaKing
//
//  Created by Cranz on 17/4/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterTopUpItem.h"

@class PDScuffleTopupAlertCell;
@protocol PDScuffleTopupAlertCellDelegate <NSObject>
- (void)scuffleCell:(PDScuffleTopupAlertCell *)cell didClickCellWithModel:(PDCenterTopUpItem *)item;
@end

@interface PDScuffleTopupAlertCell : UITableViewCell
@property (nonatomic, strong) PDCenterTopUpItem *model;
@property (nonatomic, weak) id<PDScuffleTopupAlertCellDelegate> delegate;

+ (CGFloat)cellHeight;
- (instancetype)initWithCellWidth:(CGFloat)width reuseIdentifier:(NSString *)reuseIdentifier;

@end
