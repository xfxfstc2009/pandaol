//
//  PDScuffleCard.h
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleObjCard.h"

@class PDScuffleCard;
@protocol PDScuffleCardGestureRecognizerDelegate <NSObject>

@optional
/**
 * 拖动手势
 */
- (void)scuffleCard:(PDScuffleCard *)card didMoveCardWithGestureRecognizer:(UIPanGestureRecognizer *)pan;
/**
 * 点击手势
 */
- (void)didTapAtScuffleCard:(PDScuffleCard *)card;

@end

/**
 * 可以QQ弹的英雄卡
 */
@interface PDScuffleCard : PDScuffleObjCard

@property (nonatomic, strong, readonly) UIPanGestureRecognizer *pan;
@property (nonatomic, strong, readonly) UITapGestureRecognizer *tap;

/**
 * 如果是YES，说明卡牌放大了；是NO，则是原来大小
 */
@property (nonatomic) BOOL changed;

/**
 * 卡片是否在移动
 */
@property (nonatomic) BOOL isMoving;

/**
 * 记录下原来的frame
 */
@property (nonatomic, readonly) CGRect originRect;

@property (nonatomic, weak) id<PDScuffleCardGestureRecognizerDelegate> delegate;

@property (nonatomic, weak) UIView *cardView;

@end
