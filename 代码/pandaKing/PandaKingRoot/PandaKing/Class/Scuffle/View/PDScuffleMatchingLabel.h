//
//  PDScuffleMatchingLabel.h
//  PandaKing
//
//  Created by Cranz on 17/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 正在匹配时的正计时
@interface PDScuffleMatchingLabel : UIView

/**
 * 开始计时
 */
- (void)beginTiming;
/**
 * 计时结束，等待后续处理
 */
- (void)stop;

/**
 * 显示消失
 */
- (void)reset;

@end
