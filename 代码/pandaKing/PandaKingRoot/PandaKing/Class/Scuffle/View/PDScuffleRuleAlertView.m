//
//  PDScuffleRuleAlertView.m
//  PandaKing
//
//  Created by Cranz on 17/5/4.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleRuleAlertView.h"

#define kALERT_WIDTH LCFloat(260)

@interface PDScuffleRuleAlertView ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *subContentLabel;
@property (nonatomic, strong) UILabel *subTitleLabel;
@property (nonatomic, strong) UILabel *mainContentLabel;
@property (nonatomic, strong) UIView *subContainerView;
@property (nonatomic, strong) UIView *containerView;
@end

@implementation PDScuffleRuleAlertView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupRuleView];
        [self updateFrame];
        
        self.contentView = self.containerView;
    }
    return self;
}

- (void)setupRuleView {
    self.containerView = [[UIView alloc] init];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = @"游戏规则";
    self.titleLabel.font = [[UIFont systemFontOfCustomeSize:28] boldFont];
    self.titleLabel.textColor = c46;
    [self.containerView addSubview:self.titleLabel];
    
    self.subContentLabel = [[UILabel alloc] init];
    self.subContentLabel.text = @"LOL大乱斗震撼上线！玩家对战，斗智斗勇，谁能赢得奖金，拭目以待。";
    self.subContentLabel.numberOfLines = 0;
    self.subContentLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.subContentLabel.textColor = c44;
    [self.containerView addSubview:self.subContentLabel];
    
    self.subContainerView = [[UIView alloc] init];
    self.subContainerView.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.2];
    self.subContainerView.layer.cornerRadius = 3;
    self.subContainerView.layer.masksToBounds = YES;
    [self.containerView addSubview:self.subContainerView];
    
    self.subTitleLabel = [[UILabel alloc] init];
    self.subTitleLabel.text = @"基本规则";
    self.subTitleLabel.font = [self.subContentLabel.font boldFont];
    self.subTitleLabel.textColor = c45;
    [self.subContainerView addSubview:self.subTitleLabel];
    
    self.mainContentLabel = [[UILabel alloc] init];
    self.mainContentLabel.text = @"玩家可以选择不同金币场匹配对手进行对战，匹配成功后双方会扣除金币场所要求的金币。\n对战准备时间为30秒，对战双方会各发4张不同面值的英雄卡牌（1350、3150、4800、6300），玩家可以进行自由组合将英雄卡牌放置在上中下三路内，一路最多可以放置2张英雄卡牌，最少可以放置0张英雄卡牌。对战开始时，双方玩家卡牌会逐步翻开，分值大的一方获胜，赢得2路以上的玩家获得整场对战的胜利，赢得对方的金币作为奖金。\n每局对战系统会扣除获胜方所赢得的金币的5%作为手续费。";
    self.mainContentLabel.numberOfLines = 0;
    self.mainContentLabel.font = self.subContentLabel.font;
    self.mainContentLabel.textColor = self.subContentLabel.textColor;
    [self.subContainerView addSubview:self.mainContentLabel];
    
}

- (void)updateFrame {
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font];
    CGSize subContentSize = [self.subContentLabel.text sizeWithCalcFont:self.subContentLabel.font constrainedToSize:CGSizeMake(kALERT_WIDTH, CGFLOAT_MAX)];
    CGSize subTitleSize = [self.subTitleLabel.text sizeWithCalcFont:self.subTitleLabel.font];
    NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
    paragraph.paragraphSpacing = 10;
    CGSize mainContentSize = [self.mainContentLabel.text boundingRectWithSize:CGSizeMake(kALERT_WIDTH, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSParagraphStyleAttributeName: paragraph, NSFontAttributeName: self.mainContentLabel.font} context:nil].size;
    self.mainContentLabel.attributedText = [[NSAttributedString alloc] initWithString:self.mainContentLabel.text attributes:@{NSParagraphStyleAttributeName: paragraph}];
    
    self.titleLabel.frame = CGRectMake((kALERT_WIDTH - titleSize.width) / 2, 0, titleSize.width, titleSize.height);
    self.subContentLabel.frame = CGRectMake((kALERT_WIDTH - subContentSize.width) / 2, CGRectGetMaxY(self.titleLabel.frame) + 15, subContentSize.width, subContentSize.height);
    self.subContainerView.frame = CGRectMake(0, CGRectGetMaxY(self.subContentLabel.frame) + 10, kALERT_WIDTH, 10 + subTitleSize.height + 10 + mainContentSize.height + 10);
    self.subTitleLabel.frame = CGRectMake(0, 10, subTitleSize.width, subTitleSize.height);
    self.mainContentLabel.frame = CGRectMake(0, CGRectGetMaxY(self.subTitleLabel.frame) + 10, mainContentSize.width, mainContentSize.height);
    self.containerView.frame = CGRectMake(0, 0, kALERT_WIDTH, CGRectGetMaxY(self.subContainerView.frame));
}

@end
