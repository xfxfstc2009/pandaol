//
//  PDScuffleAlertView.m
//  PandaKing
//
//  Created by Cranz on 17/4/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDScuffleAlertView.h"

#define kSCUFFLE_CONTENT_EDGE UIEdgeInsetsMake(55, 30, 30, 30)
#define kSCUFFLE_ALERT_WIDTH (kScreenBounds.size.width - 2 * 30)
#define kSCUFFLE_ALERT_CONTENT_WIDTH (kSCUFFLE_ALERT_WIDTH - 2 * kSCUFFLE_CONTENT_EDGE.left)
#define kSCUFFLE_ALERT_BUTTON_SIZE CGSizeMake((kSCUFFLE_ALERT_CONTENT_WIDTH - 20)/2, 40)
#define kSCUFFLE_TAG 114

typedef void(^PDScuffleAlertBlock)(NSUInteger);

@interface PDScuffleAlertView ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UIView *contentView;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, strong) NSArray *titlesArr;

@property (nonatomic, copy) PDScuffleAlertBlock alertBlock;
@end

@implementation PDScuffleAlertView

- (instancetype)initWithTitle:(NSString *)title content:(NSString *)content buttonTitlesArr:(NSArray *)titlesArr {
    self = [super init];
    if (self) {
        self.title = title;
        self.content = content;
        self.titlesArr = titlesArr;
        
        [self setupContent];
    }
    return self;
}

- (void)setupContent {
    self.backgroundImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_scuffle_alert_bg"]];
    self.backgroundImageView.userInteractionEnabled = YES;
    [self addSubview:self.backgroundImageView];
    
    self.contentView = [[UIView alloc] init];
    [self addSubview:self.contentView];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [[UIFont systemFontOfCustomeSize:28] boldFont];
    self.titleLabel.textColor = c44;
    [self.contentView addSubview:self.titleLabel];
    
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font = [UIFont systemFontOfCustomeSize:16];
    self.contentLabel.textColor = c44;
    self.contentLabel.numberOfLines = 3;
    [self.contentView addSubview:self.contentLabel];

    self.titleLabel.text = self.title;
    self.contentLabel.text = self.content;
    
    [self updateFrames];
}

- (void)updateFrames {
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font];
    CGSize contentSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kSCUFFLE_ALERT_CONTENT_WIDTH, 3 * [NSString contentofHeightWithFont:self.contentLabel.font])];
    
    UIEdgeInsets edge = kSCUFFLE_CONTENT_EDGE;
    self.frame = CGRectMake(0, 0, kSCUFFLE_ALERT_WIDTH, edge.top + titleSize.height + 20 + contentSize.height + 20 + kSCUFFLE_ALERT_BUTTON_SIZE.height + edge.bottom);
    self.backgroundImageView.frame = self.bounds;
    self.contentView.frame = CGRectMake(edge.left, edge.top, kSCUFFLE_ALERT_CONTENT_WIDTH, self.frame.size.height - edge.top - edge.bottom);
    self.titleLabel.frame = CGRectMake((kSCUFFLE_ALERT_CONTENT_WIDTH - titleSize.width) / 2, 0, titleSize.width, titleSize.height);
    self.contentLabel.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + 20, contentSize.width, contentSize.height);
    
    for (int i = 0; i < self.titlesArr.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:self.titlesArr[i] forState:UIControlStateNormal];
        [button setTag:kSCUFFLE_TAG + i];
        button.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
        
        switch (i) {
            case 0:
                [button setBackgroundImage:[UIImage imageNamed:@"bg_scuffle_alert_button_yellow"] forState:UIControlStateNormal];
                break;
            case 1:
                [button setBackgroundImage:[UIImage imageNamed:@"bg_scuffle_alert_button_green"] forState:UIControlStateNormal];
                break;
            default:
                break;
        }
        
        [button setFrame:CGRectMake(i * (kSCUFFLE_ALERT_BUTTON_SIZE.width + LCFloat(20)), CGRectGetMaxY(self.contentLabel.frame) + 20, kSCUFFLE_ALERT_BUTTON_SIZE.width, kSCUFFLE_ALERT_BUTTON_SIZE.height)];
        [self.contentView addSubview:button];
        
        [button addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)didClickButton:(UIButton *)button {
    if (self.alertBlock) {
        self.alertBlock(button.tag - kSCUFFLE_TAG);
    }
}

- (void)alertViewClickHandle:(void (^)(NSUInteger index))handle {
    if (handle) {
        self.alertBlock = handle;
    }
}

@end
