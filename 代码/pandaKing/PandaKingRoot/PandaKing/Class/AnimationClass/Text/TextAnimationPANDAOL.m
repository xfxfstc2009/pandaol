//
//  TextAnimationPANDAOL.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "TextAnimationPANDAOL.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>

@interface TextAnimationPANDAOL()
@property (nonatomic,strong)CALayer *animationLayer;
@property (nonatomic,strong)CAShapeLayer *pathLayer;
@property (nonatomic,assign)textAnimationType type;
@property (nonatomic,copy)NSString *titleStrting;

@end

@implementation TextAnimationPANDAOL

-(instancetype)initWithFrame:(CGRect)frame type:(textAnimationType)type withTitle:(NSString *)title{
    self = [super initWithFrame:frame];
    if (self){
        _type = type;
        _titleStrting = title;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.animationLayer = [CALayer layer];
    self.animationLayer.frame = self.bounds;
    [self.layer addSublayer:self.animationLayer];
}


-(void)setupTextLayerWithTitle:(NSString *)title{
    if (self.pathLayer != nil){
        [self.pathLayer removeFromSuperlayer];
        self.pathLayer = nil;
    }
    
    CGMutablePathRef letters = CGPathCreateMutable();
    CTFontRef font = CTFontCreateWithName(CFSTR("Plateia-Bold"), 60.0f, NULL);
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id _Nonnull)((CTFontRef)font), kCTFontAttributeName, nil];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:title attributes:attrs];
    CTLineRef line = CTLineCreateWithAttributedString((CFAttributedStringRef)attrString);
    CFArrayRef runArray = CTLineGetGlyphRuns(line);
    for (CFIndex runIndex = 0 ; runIndex < CFArrayGetCount(runArray);runIndex++){
        CTRunRef run = (CTRunRef)CFArrayGetValueAtIndex(runArray, runIndex);
        CTFontRef runFont = CFDictionaryGetValue(CTRunGetAttributes(run), kCTFontAttributeName);
        for (CFIndex runGlyphIndex = 0; runGlyphIndex < CTRunGetGlyphCount(run); runGlyphIndex++) {
            CFRange thisGlyphRange = CFRangeMake(runGlyphIndex, 1);
            CGGlyph glyph;
            CGPoint position;
            CTRunGetGlyphs(run, thisGlyphRange, &glyph);
            CTRunGetPositions(run, thisGlyphRange, &position);
            {
                CGMutablePathRef letters1 = CGPathCreateMutable();
                CGPathRef letter = CTFontCreatePathForGlyph(runFont, glyph, NULL);
                CGAffineTransform t = CGAffineTransformMakeTranslation(position.x, position.y);
                CGPathAddPath(letters, &t, letter);
                
                CGPathAddPath(letters1, &t, letter);
                
                
                CGPathRelease(letter);
            }
        }
    }
    CFRelease(line);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointZero];
    [path appendPath:[UIBezierPath bezierPathWithCGPath:letters]];
    
    CGPathRelease(letters);
    CFRelease(font);

    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    pathLayer.frame = self.animationLayer.bounds;
    pathLayer.bounds = CGPathGetBoundingBox(path.CGPath);
    pathLayer.geometryFlipped = YES;
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor =    [[UIColor lightGrayColor] CGColor];
    pathLayer.fillColor = nil;
    pathLayer.lineWidth = .5f;
    pathLayer.lineJoin = kCALineJoinBevel;
    
    [self.animationLayer addSublayer:pathLayer];
    
    self.pathLayer = pathLayer;
}


#pragma mark - Action
-(void)showAnimation{
    [self.pathLayer removeAllAnimations];
    [self setupTextLayerWithTitle:self.titleStrting];
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 1.5;
    pathAnimation.delegate = self;
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.0f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [pathAnimation setValue:@"step1" forKey:@"kName"];
//    [self.pathLayer addAnimation:pathAnimation forKey:@"strokeEnd"];
    
    
    // 旋转
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    animation.duration = 2.5;                           // 持续时间
    animation.repeatCount = 1;                           // 重复次数
    animation.fromValue = [NSNumber numberWithFloat:0.0]; // 起始角度
    animation.toValue = [NSNumber numberWithFloat:66435646758768 * M_PI]; // 终止角度
//    animation.fromValue = [NSValue valueWithCATransform3D:CATransform3DIdentity];
//    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(0, 0, 0, 1)];
    
    
    // 组合
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.duration = 3.0;
    group.repeatCount = 1;
    
    group.animations = [NSArray arrayWithObjects:pathAnimation, animation, nil];
    [self.pathLayer addAnimation:group forKey:@"group1"];
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if (self.type == textAnimationTypeOne){         // 动画1
        if ([[anim valueForKey:@"kName"] isEqualToString:@"step1"]){           // 执行动画1完成以后
            self.pathLayer.fillColor = [UIColor blackColor].CGColor;
            self.pathLayer.strokeColor = [UIColor clearColor].CGColor;
        }
    } else if (self.type == textAnimationTypeTwo){      // 动画2
        
    } else if (self.type == textAnimationTypeThr){      // 动画3
        
    }
}

@end
