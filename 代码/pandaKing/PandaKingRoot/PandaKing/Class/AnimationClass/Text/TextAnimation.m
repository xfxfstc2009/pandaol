//
//  TextAnimation.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "TextAnimation.h"
#import <QuartzCore/QuartzCore.h>
#import <CoreText/CoreText.h>
#import <objc/runtime.h>

static char animationFinishBlock;
@interface TextAnimation()
{
    
}
@property (nonatomic,strong)CALayer *animationLayer;
@property (nonatomic,strong)CAShapeLayer *pathLayerP;
@property (nonatomic,strong)CAShapeLayer *pathLayerA;
@property (nonatomic,strong)CAShapeLayer *pathLayerN;
@property (nonatomic,strong)CAShapeLayer *pathLayerD;
@property (nonatomic,strong)CAShapeLayer *pathLayerA2;
@property (nonatomic,strong)CAShapeLayer *pathLayerO;
@property (nonatomic,strong)CAShapeLayer *pathLayerL;
@property (nonatomic,strong)CAShapeLayer *pathLayerTM;


@end

@implementation TextAnimation

-(instancetype)initWithFrame:(CGRect)frame type:(animationType)type withTitle:(NSString *)title{
    self = [super initWithFrame:frame];
    if (self){
        self.size_height = [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]];
        [self createView];
        [self setupTextLayerWithTitle:@"PANDAOL"];
        [self setupTextLayerWithTM];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.animationLayer = [CALayer layer];
    self.animationLayer.frame = self.bounds;
    [self.layer addSublayer:self.animationLayer];
}

-(void)cleanPath{
    if (self.pathLayerP != nil){
        [self.pathLayerP removeFromSuperlayer];
        self.pathLayerP = nil;
    }
    if (self.pathLayerA != nil){
        [self.pathLayerA removeFromSuperlayer];
        self.pathLayerA = nil;
    }
    if (self.pathLayerN != nil){
        [self.pathLayerN removeFromSuperlayer];
        self.pathLayerN = nil;
    }
    if (self.pathLayerD != nil){
        [self.pathLayerD removeFromSuperlayer];
        self.pathLayerD = nil;
    }
    if (self.pathLayerA2 != nil){
        [self.pathLayerA2 removeFromSuperlayer];
        self.pathLayerA2 = nil;
    }
    if (self.pathLayerO != nil){
        [self.pathLayerO removeFromSuperlayer];
        self.pathLayerO = nil;
    }
    if (self.pathLayerL != nil){
        [self.pathLayerL removeFromSuperlayer];
        self.pathLayerL = nil;
    }
    if (self.pathLayerTM != nil){
        [self.pathLayerTM removeFromSuperlayer];
        self.pathLayerTM = nil;
    }
  
}

-(void)setupTextLayerWithTM{
//    [self cleanPath];
    
    CGMutablePathRef letters = CGPathCreateMutable();
    CTFontRef font = CTFontCreateWithName(CFSTR("Plateia-Bold"), 20.0f, NULL);
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id _Nonnull)((CTFontRef)font), kCTFontAttributeName, nil];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:@"®" attributes:attrs];
    CTLineRef line = CTLineCreateWithAttributedString((CFAttributedStringRef)attrString);
    CFArrayRef runArray = CTLineGetGlyphRuns(line);
    for (CFIndex runIndex = 0 ; runIndex < CFArrayGetCount(runArray);runIndex++){
        CTRunRef run = (CTRunRef)CFArrayGetValueAtIndex(runArray, runIndex);
        CTFontRef runFont = CFDictionaryGetValue(CTRunGetAttributes(run), kCTFontAttributeName);
        for (CFIndex runGlyphIndex = 0; runGlyphIndex < CTRunGetGlyphCount(run); runGlyphIndex++) {
            CFRange thisGlyphRange = CFRangeMake(runGlyphIndex, 1);
            CGGlyph glyph;
            CGPoint position;
            CTRunGetGlyphs(run, thisGlyphRange, &glyph);
            CTRunGetPositions(run, thisGlyphRange, &position);
            {
                CGPathRef letter = CTFontCreatePathForGlyph(runFont, glyph, NULL);
                CGAffineTransform t = CGAffineTransformMakeTranslation(position.x, position.y);
                CGPathAddPath(letters, &t, letter);
                
                CGPathRelease(letter);
            }
        }
    }
    CFRelease(line);
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointZero];
    [path appendPath:[UIBezierPath bezierPathWithCGPath:letters]];
    
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    self.pathLayerTM = pathLayer;
    CGSize contenOfSize = [@"®" sizeWithCalcFont:[UIFont fontWithName:@"Plateia-Bold" size:12] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:12]])];
        
    self.pathLayerTM.frame = CGRectMake(0, 0, contenOfSize.width, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:12]]);
    pathLayer.bounds = CGPathGetBoundingBox(path.CGPath);
    pathLayer.geometryFlipped = YES;
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor =    [[UIColor clearColor] CGColor];
    pathLayer.fillColor = nil;
    pathLayer.lineWidth = .5f;
    pathLayer.lineJoin = kCALineJoinBevel;
    
    [self.animationLayer addSublayer:pathLayer];
    
    CGPathRelease(letters);
    CFRelease(font);

}


-(void)setupTextLayerWithTitle:(NSString *)title{
//    [self cleanPath];
    
    CGMutablePathRef letters = CGPathCreateMutable();
    CTFontRef font = CTFontCreateWithName(CFSTR("Plateia-Bold"), 37.0f, NULL);
    NSDictionary *attrs = [NSDictionary dictionaryWithObjectsAndKeys:(__bridge id _Nonnull)((CTFontRef)font), kCTFontAttributeName, nil];
    NSAttributedString *attrString = [[NSAttributedString alloc] initWithString:title attributes:attrs];
    CTLineRef line = CTLineCreateWithAttributedString((CFAttributedStringRef)attrString);
    CFArrayRef runArray = CTLineGetGlyphRuns(line);
    for (CFIndex runIndex = 0 ; runIndex < CFArrayGetCount(runArray);runIndex++){
        CTRunRef run = (CTRunRef)CFArrayGetValueAtIndex(runArray, runIndex);
        CTFontRef runFont = CFDictionaryGetValue(CTRunGetAttributes(run), kCTFontAttributeName);
        for (CFIndex runGlyphIndex = 0; runGlyphIndex < CTRunGetGlyphCount(run); runGlyphIndex++) {
            CFRange thisGlyphRange = CFRangeMake(runGlyphIndex, 1);
            CGGlyph glyph;
            CGPoint position;
            CTRunGetGlyphs(run, thisGlyphRange, &glyph);
            CTRunGetPositions(run, thisGlyphRange, &position);
            {
                CGMutablePathRef letters1 = CGPathCreateMutable();
                CGPathRef letter = CTFontCreatePathForGlyph(runFont, glyph, NULL);
                CGAffineTransform t = CGAffineTransformMakeTranslation(position.x, position.y);
                CGPathAddPath(letters1, &t, letter);
                CGPathAddPath(letters, &t, letter);
                
                UIBezierPath *path = [UIBezierPath bezierPath];
                [path moveToPoint:CGPointZero];
                [path appendPath:[UIBezierPath bezierPathWithCGPath:letters1]];
                
                [self smartPathWithPath:path andIndex:runGlyphIndex];
                
                CGPathRelease(letters1);
            }
        }
    }
    CFRelease(line);

    CGPathRelease(letters);
    CFRelease(font);
    
    
}

-(void)smartPathWithPath:(UIBezierPath *)path andIndex:(NSInteger)index{
    CAShapeLayer *pathLayer = [CAShapeLayer layer];
    
    if (index == 0){
        self.pathLayerP = pathLayer;
        CGSize contenOfSize = [@"P" sizeWithCalcFont:[UIFont fontWithName:@"Plateia-Bold" size:37] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]])];
        
        self.pathLayerP.frame = CGRectMake(0, 0, contenOfSize.width, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]]);
    } else if (index == 1){
        self.pathLayerA = pathLayer;
        CGSize contenOfSize = [@"A" sizeWithCalcFont:[UIFont fontWithName:@"Plateia-Bold" size:37] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]])];
        self.pathLayerA.frame = CGRectMake(CGRectGetMaxX(self.pathLayerP.frame) + (- LCFloat(2)), 0, contenOfSize.width, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]]);
    } else if (index == 2){
        self.pathLayerN = pathLayer;
        CGSize contenOfSize = [@"N" sizeWithCalcFont:[UIFont fontWithName:@"Plateia-Bold" size:37] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]])];
        self.pathLayerN.frame = CGRectMake(CGRectGetMaxX(self.pathLayerA.frame) + LCFloat(0), 0, contenOfSize.width, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]]);
    } else if (index == 3){
        self.pathLayerD = pathLayer;
        CGSize contenOfSize = [@"D" sizeWithCalcFont:[UIFont fontWithName:@"Plateia-Bold" size:37] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]])];
        self.pathLayerD.frame = CGRectMake(CGRectGetMaxX(self.pathLayerN.frame) + LCFloat(4), 0, contenOfSize.width, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]]);
    } else if (index == 4){
        self.pathLayerA2 = pathLayer;
        CGSize contenOfSize = [@"A" sizeWithCalcFont:[UIFont fontWithName:@"Plateia-Bold" size:37] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]])];
        self.pathLayerA2.frame = CGRectMake(CGRectGetMaxX(self.pathLayerD.frame) + LCFloat(0), 0, contenOfSize.width, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]]);
    } else if (index == 5){
        self.pathLayerO = pathLayer;
        CGSize contenOfSize = [@"O" sizeWithCalcFont:[UIFont fontWithName:@"Plateia-Bold" size:37] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]])];
        self.pathLayerO.frame = CGRectMake(CGRectGetMaxX(self.pathLayerA2.frame) + LCFloat(0), 0, contenOfSize.width, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]]);
    } else if (index == 6){
        self.pathLayerL = pathLayer;
        CGSize contenOfSize = [@"L" sizeWithCalcFont:[UIFont fontWithName:@"Plateia-Bold" size:37] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]])];
        self.pathLayerL.frame = CGRectMake(CGRectGetMaxX(self.pathLayerO.frame) + LCFloat(4), 0, contenOfSize.width, [NSString contentofHeightWithFont:[UIFont fontWithName:@"Plateia-Bold" size:37]]);
        self.size_width = CGRectGetMaxX(self.pathLayerL.frame);
    }
    
    
    pathLayer.bounds = CGPathGetBoundingBox(path.CGPath);
    pathLayer.geometryFlipped = YES;
    pathLayer.path = path.CGPath;
    pathLayer.strokeColor =    [[UIColor clearColor] CGColor];
    pathLayer.fillColor = nil;
    pathLayer.lineWidth = .5f;
    pathLayer.lineJoin = kCALineJoinBevel;

    [self.animationLayer addSublayer:pathLayer];
}



#pragma mark - Action
-(void)showAnimationWithFinishBlock:(void(^)())finish{
    objc_setAssociatedObject(self, &animationFinishBlock, finish, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self showAnimation];
}


-(void)showAnimation{
    [self.pathLayerP removeAllAnimations];
    [self.pathLayerA removeAllAnimations];
    [self.pathLayerN removeAllAnimations];
    [self.pathLayerD removeAllAnimations];
    [self.pathLayerA2 removeAllAnimations];
    [self.pathLayerO removeAllAnimations];
    [self.pathLayerL removeAllAnimations];
    
    
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 0.5;
    pathAnimation.delegate = self;
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.5f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [pathAnimation setValue:@"0" forKey:@"ani1"];
    
    [self.pathLayerP addAnimation:pathAnimation forKey:@"ani1"];
}


#pragma mark - 动画1
-(void)showAnimationManager1WithIndex:(NSInteger)index{
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 0.01f;
    pathAnimation.delegate = self;
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.5f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [pathAnimation setValue:[NSString stringWithFormat:@"%li",(long)index] forKey:@"ani1"];
    
    if (index == 0){
        [self.pathLayerP addAnimation:pathAnimation forKey:@"ani1"];
    } else if (index == 1){
        [self.pathLayerA addAnimation:pathAnimation forKey:@"ani1"];
    } else if (index == 2){
        [self.pathLayerN addAnimation:pathAnimation forKey:@"ani1"];
    } else if (index == 3){
        [self.pathLayerD addAnimation:pathAnimation forKey:@"ani1"];
    } else if (index == 4){
        [self.pathLayerA2 addAnimation:pathAnimation forKey:@"ani1"];
    } else if (index == 5){
        [self.pathLayerO addAnimation:pathAnimation forKey:@"ani1"];
    } else if (index == 6){
        [self.pathLayerL addAnimation:pathAnimation forKey:@"ani1"];
    }
}

#pragma mark - 动画2
-(void)showAnimation2WithIndex:(NSInteger)index{
    // 【翻转】
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.y"];
    animation.duration = .25f;                                             // 持续时间
    animation.repeatCount = 1;                                          // 重复次数
    animation.fromValue = [NSNumber numberWithFloat: M_PI];               // 起始角度
    animation.toValue = [NSNumber numberWithFloat:2 * M_PI]; // 终止角度
    animation.delegate = self;
    NSInteger indexValue = index ;
    [animation setValue:[NSString stringWithFormat:@"%li",(long)indexValue] forKey:@"ani2"];

    // 【渐变】
    CABasicAnimation *animation2=[CABasicAnimation animationWithKeyPath:@"opacity"];
    animation2.fromValue=[NSNumber numberWithFloat:.0f];
    animation2.toValue=[NSNumber numberWithFloat:1.0f];
    animation2.duration = .3f;
    animation2.repeatCount = 1;
    animation2.removedOnCompletion = NO;
    animation2.fillMode = kCAFillModeForwards;
    
    // 【组合】
    CAAnimationGroup *group = [CAAnimationGroup animation];
    group.duration = .3f;
    group.repeatCount = 1;
    group.animations = [NSArray arrayWithObjects:animation, animation2, nil];
    group.delegate = self;
    [group setValue:[NSString stringWithFormat:@"%li",(long)indexValue] forKey:@"ani2"];
    

    if (indexValue == 0){
        [self.pathLayerP addAnimation:group forKey:@"ani2"];
    } else if (indexValue == 1){
        [self.pathLayerA addAnimation:group forKey:@"ani2"];
    } else if (indexValue == 2){
        [self.pathLayerN addAnimation:group forKey:@"ani2"];
    } else if (indexValue == 3){
        [self.pathLayerD addAnimation:group forKey:@"ani2"];
    } else if (indexValue == 4){
        [self.pathLayerA2 addAnimation:group forKey:@"ani2"];
    } else if (indexValue == 5){
        [self.pathLayerO addAnimation:group forKey:@"ani2"];
    } else if (indexValue == 6){
        [self.pathLayerL addAnimation:group forKey:@"ani2"];
    }
}

-(void)showAnimation3{
    self.pathLayerTM.fillColor = [UIColor blackColor].CGColor;
    self.pathLayerTM.strokeColor = [UIColor blackColor].CGColor;
    CABasicAnimation *pathAnimation = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
    pathAnimation.duration = 0.2f;
    pathAnimation.delegate = self;
    pathAnimation.fromValue = [NSNumber numberWithFloat:0.5f];
    pathAnimation.toValue = [NSNumber numberWithFloat:1.0f];
    [pathAnimation setValue:@"0" forKey:@"ani3"];
    [self.pathLayerTM addAnimation:pathAnimation forKey:@"ani3"];
    self.pathLayerTM.frame = CGRectMake(CGRectGetMaxX(self.pathLayerL.frame) - LCFloat(8), self.pathLayerL.frame.origin.y, self.pathLayerTM.frame.size.width, self.pathLayerTM.frame.size.height);
}


-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if ([[anim valueForKey:@"ani1"] isEqualToString:@"0"]){
        self.pathLayerP.fillColor = [UIColor blackColor].CGColor;
        self.pathLayerP.strokeColor = [UIColor clearColor].CGColor;
        [self showAnimation2WithIndex:0];
    } else if ([[anim valueForKey:@"ani2"] isEqualToString:@"0"]){
        [self showAnimationManager1WithIndex:1];
    } else if ([[anim valueForKey:@"ani1"] isEqualToString:@"1"]){
        self.pathLayerA.fillColor = [UIColor blackColor].CGColor;
        self.pathLayerA.strokeColor = [UIColor clearColor].CGColor;
        [self showAnimation2WithIndex:1];
    } else if ([[anim valueForKey:@"ani2"] isEqualToString:@"1"]){
        [self showAnimationManager1WithIndex:2];
    } else if ([[anim valueForKey:@"ani1"] isEqualToString:@"2"]){
        self.pathLayerN.fillColor = [UIColor blackColor].CGColor;
        self.pathLayerN.strokeColor = [UIColor clearColor].CGColor;
        [self showAnimation2WithIndex:2];
    } else if ([[anim valueForKey:@"ani2"] isEqualToString:@"2"]){
        [self showAnimationManager1WithIndex:3];
    } else if ([[anim valueForKey:@"ani1"] isEqualToString:@"3"]){
        self.pathLayerD.fillColor = [UIColor blackColor].CGColor;
        self.pathLayerD.strokeColor = [UIColor clearColor].CGColor;
        [self showAnimation2WithIndex:3];
    } else if ([[anim valueForKey:@"ani2"] isEqualToString:@"3"]){
        [self showAnimationManager1WithIndex:4];
    } else if ([[anim valueForKey:@"ani1"] isEqualToString:@"4"]){
        self.pathLayerA2.fillColor = [UIColor blackColor].CGColor;
        self.pathLayerA2.strokeColor = [UIColor clearColor].CGColor;
        [self showAnimation2WithIndex:4];
    } else if ([[anim valueForKey:@"ani2"] isEqualToString:@"4"]){
        [self showAnimationManager1WithIndex:5];
    } else if ([[anim valueForKey:@"ani1"] isEqualToString:@"5"]){
        self.pathLayerO.fillColor = [UIColor blackColor].CGColor;
        self.pathLayerO.strokeColor = [UIColor clearColor].CGColor;
        [self showAnimation2WithIndex:5];
    } else if ([[anim valueForKey:@"ani2"] isEqualToString:@"5"]){
        [self showAnimationManager1WithIndex:6];
    } else if ([[anim valueForKey:@"ani1"] isEqualToString:@"6"]){
        self.pathLayerL.fillColor = [UIColor blackColor].CGColor;
        self.pathLayerL.strokeColor = [UIColor clearColor].CGColor;
        [self showAnimation2WithIndex:6];
    } else if ([[anim valueForKey:@"ani2"]isEqualToString:@"6"]){
        [self showAnimation3];

        void(^block)() = objc_getAssociatedObject(self, &animationFinishBlock);
        if (block){
            block();
        }
    }
}

@end
