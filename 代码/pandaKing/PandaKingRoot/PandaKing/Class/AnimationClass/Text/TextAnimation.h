//
//  TextAnimation.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,animationType) {
    animationType1,                       /**< 第一种动画*/
    animationType2,                       /**< 第二种动画*/
    animationType3,                       /**< 第三种动画*/
};

@interface TextAnimation : UIView

-(instancetype)initWithFrame:(CGRect)frame type:(animationType)type withTitle:(NSString *)title;

-(void)showAnimationWithFinishBlock:(void(^)())finish;

@end
