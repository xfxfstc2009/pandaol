//
//  TextAnimationPANDAOL.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/5/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,textAnimationType) {
    textAnimationTypeOne,                       /**< 第一种动画*/
    textAnimationTypeTwo,                       /**< 第二种动画*/
    textAnimationTypeThr,                       /**< 第三种动画*/
};

@interface TextAnimationPANDAOL : UIView

-(instancetype)initWithFrame:(CGRect)frame type:(textAnimationType)type withTitle:(NSString *)title;

-(void)showAnimation;

@end
