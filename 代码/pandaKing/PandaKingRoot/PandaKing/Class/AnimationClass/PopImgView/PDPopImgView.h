//
//  PDPopImgView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDPopImgView : UIControl

@property (nonatomic,strong)UIImage *image;

@end
