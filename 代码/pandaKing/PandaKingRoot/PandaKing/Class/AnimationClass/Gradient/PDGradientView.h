//
//  PDGradientView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDGradientView : UIView

@property (nonatomic,strong)NSArray *colors;                /**< 所有的色彩*/
@property (nonatomic,strong)NSArray *locations;             /**< 所有的位置*/
@property (nonatomic,assign)CGPoint startPoint;             /**< 开始的point*/
@property (nonatomic,assign)CGPoint endPoint;               /**< 结束的point*/


@end
