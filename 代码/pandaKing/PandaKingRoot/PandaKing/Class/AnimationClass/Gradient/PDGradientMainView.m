//
//  PDGradientMainView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDGradientMainView.h"
#import "PDGradientMaskView.h"
#import "GCD.h"

typedef enum : NSUInteger {
    kTypeOne,
    kTypeTwo,
} EType;

@interface PDGradientMainView()
@property (nonatomic,strong)NSArray *imgArr;
@property (nonatomic,assign)NSInteger count;
@property (nonatomic,strong)PDGradientMaskView *tranformFadeViewOne;            /**< 蒙层1*/
@property (nonatomic,strong)PDGradientMaskView *tranformFadeViewTwo;            /**< 蒙*/
@property (nonatomic,strong)GCDTimer *timer;                                    /**< */
@property (nonatomic,assign)EType type;                                         /**< 切换类型*/

@end

@implementation PDGradientMainView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self setup];
    }
    return self;
}

-(void)setup{
//    UIImage *img1= [UIImage imageNamed:@"smart"];
//    UIImage *img2 = [UIImage imageNamed:@"duigou.jpg"];
    NSString *img1 = @"http://img1.91.com/uploads/allimg/160503/703-160503112G6.jpg";
    NSString *img2 = @"http://photocdn.sohu.com/20150626/Img415673975.jpg";
    NSString *img3 = @"http://photocdn.sohu.com/20150917/Img421412068.jpg";
//    NSString *img4 = @"http://img4.imgtn.bdimg.com/it/u=3236377747,4229191129&fm=21&gp=0.jpg";
    NSString *img5 = @"http://img001.21cnimg.com/photos/album/20140723/m600/B44ED4657D68722F0D32B111E7AB3103.jpeg";
    _imgArr = @[img1,img2,img3,img5];
    
    self.tranformFadeViewOne               = [[PDGradientMaskView alloc] initWithFrame:self.bounds];
    self.tranformFadeViewOne.contentMode   = UIViewContentModeScaleAspectFill;
    self.tranformFadeViewOne.fadeDuradtion = 10.f;
    
    [self.tranformFadeViewOne.imageView uploadImageWithURL:[self currentImage] placeholder:nil callback:^(UIImage *image) {

    }];
    [self addSubview:self.tranformFadeViewOne];
    
    self.tranformFadeViewTwo               = [[PDGradientMaskView alloc] initWithFrame:self.bounds];
    self.tranformFadeViewTwo.contentMode   = UIViewContentModeScaleAspectFill;
    self.tranformFadeViewTwo.fadeDuradtion = 10.f;
    [self addSubview:self.tranformFadeViewTwo];
    [self.tranformFadeViewTwo fadeAnimated:NO];
    
    // timer
    __weak typeof(self) weakSelf = self;
    self.timer = [[GCDTimer alloc] initInQueue:[GCDQueue mainQueue]];
    [self.timer event:^{
        [weakSelf timerEvent];
    } timeIntervalWithSecs:6 delaySecs:1.f];
    [self.timer start];
}

- (void)timerEvent {
    if (self.type == kTypeOne) {
        self.type = kTypeTwo;
        [self sendSubviewToBack:self.tranformFadeViewTwo];
        [self.tranformFadeViewTwo.imageView uploadImageWithURL:[self currentImage] placeholder:nil callback:^(UIImage *image) {
            [self.tranformFadeViewTwo showAnimated:NO];
            [self.tranformFadeViewOne fadeAnimated:YES];
        }];
    } else {
        self.type = kTypeOne;
        [self sendSubviewToBack:self.tranformFadeViewOne];
        [self.tranformFadeViewOne.imageView uploadImageWithURL:[self currentImage] placeholder:nil callback:^(UIImage *image) {
            [self.tranformFadeViewOne showAnimated:NO];
            [self.tranformFadeViewTwo fadeAnimated:YES];
        }];
    }
}

- (NSString *)currentImage {
    self.count = ++ self.count % self.imgArr.count;
    return self.imgArr[self.count];
}
@end
