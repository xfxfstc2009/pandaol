//
//  PDGradientMaskView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDGradientMaskView.h"
#import "PDGradientView.h"


@interface PDGradientMaskView()
@property (nonatomic,strong)UIView *theMaskView;                    /**< maskView*/
@property (nonatomic,assign)CGFloat width;                          /**< 图片宽度*/
@property (nonatomic,assign)CGFloat height;                         /**< 图片高度*/
@property (nonatomic,assign)CGRect theOldFrame;                     /**< 第一个frame*/
@property (nonatomic,assign)CGRect theNewFrame;                     /**< 第二个frame*/

@end

@implementation PDGradientMaskView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.layer.masksToBounds = YES;
    self.imageView = [[PDImageView alloc]initWithFrame:self.bounds];
    [self addSubview:self.imageView];
    
    self.width = self.frame.size.width;
    self.height = self.frame.size.height;
    self.theOldFrame = CGRectMake(-self.width / 2.f, 0, self.width + self.width / 2.f, self.height);
    self.theNewFrame = CGRectMake(self.width, 0, self.width + self.width / 2.f, self.height);
    
    self.theMaskView = [[UIView alloc]initWithFrame:self.theOldFrame];
    UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(self.width / 2.f, 0, self.width, self.height)];
    blackView.backgroundColor = [UIColor blackColor];
    [self.theMaskView addSubview:blackView];
    
    PDGradientView *gradientView = [[PDGradientView alloc] initWithFrame:CGRectMake(0, 0, self.width / 2.f, self.height)];
    gradientView.colors          = @[[UIColor clearColor], [UIColor blackColor]];
    gradientView.locations       = @[@(0.5f), @(1.f)];
    gradientView.startPoint      = CGPointMake(0, 0);
    gradientView.endPoint        = CGPointMake(1, 0);
    [self.theMaskView addSubview:gradientView];
    self.imageView.layer.mask = self.theMaskView.layer;
}

-(void)fadeAnimated:(BOOL)animated{
    if (animated) {
        [UIView animateWithDuration:(self.fadeDuradtion <= 0 ? 1 : self.fadeDuradtion) animations:^{
            self.theMaskView.frame = self.theNewFrame;
        }];
    } else {
        self.theMaskView.frame = self.theNewFrame;
    }
}

-(void)showAnimated:(BOOL)animated{
    if (animated) {
        [UIView animateWithDuration:(self.fadeDuradtion <= 0 ? 1 : self.fadeDuradtion) animations:^{
            self.theMaskView.frame = self.theOldFrame;
        }];
    } else {
        self.theMaskView.frame = self.theOldFrame;
    }
}

@synthesize contentMode = _contentMode;
- (UIViewContentMode)contentMode {
    return _imageView.contentMode;
}

- (void)setContentMode:(UIViewContentMode)contentMode {
    _imageView.contentMode = contentMode;
}

//@synthesize image = _image;

//- (UIImage *)image {
//    return _imageView.image;
//}
//
//- (void)setImage:(UIImage *)image {
//    _imageView.image = image;
//}


@end
