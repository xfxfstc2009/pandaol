//
//  PDGradientMaskView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/15.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

// mask View

#import <UIKit/UIKit.h>

@interface PDGradientMaskView : UIView

@property (nonatomic,assign)UIViewContentMode contentMode;              /**< img显示方式*/
@property (nonatomic,assign)NSTimeInterval fadeDuradtion;               /**< 动画渐变时间*/
@property (nonatomic,strong)PDImageView *imageView;

-(void)fadeAnimated:(BOOL)animated;                                     /**< 开始隐藏动画*/
-(void)showAnimated:(BOOL)animated;                                     /**< 开始显示动画*/

@end
