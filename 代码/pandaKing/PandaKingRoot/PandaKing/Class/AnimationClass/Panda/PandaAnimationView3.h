//
//  PandaAnimationView3.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

extern CGFloat PDBiliFloat(CGFloat floatValue);
extern CGFloat PDBiliFloatWithPadding(CGFloat floatValue, CGFloat padding);
static CGFloat animationWidth3 = 100;

@interface PandaAnimationView3 : UIView

//-(void)startAnimation;                  /**< 开始动画*/

@end