//
//  PandaAnimationView1.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PandaAnimationView1.h"
#import <pop/POP.h>
#import <objc/runtime.h>

static char animationFinishBlock;
static NSString *const kName = @"animationName";    /**< 动画的key*/
static CGFloat const duration1 = .2f;               /**< 脸出现时间*/
static CGFloat const duration2 = .3f;               /**< mask出现时间*/
static CGFloat const duration3 = .3f;               /**< 眼睛出现时间*/
static CGFloat const duration4 = .3f;               /**< 耳朵出现时间*/

@interface PandaAnimationView1()
@property (nonatomic,strong)CAShapeLayer *faceShapLayer;
@property (nonatomic,strong) CAShapeLayer *faceShapeLayer1;              /**< 脸layer*/
@property (nonatomic, strong) CAShapeLayer *faceMaskLayer;               /**< 脸的mask*/

@property (nonatomic,strong)CAShapeLayer *leftEyeLayer;                  /**< 左眼*/
@property (nonatomic,strong)CAShapeLayer *rightEyeLayer;                 /**< 右眼*/

// 耳朵
@property (nonatomic,strong)UIView *earView;
@property (nonatomic,strong)CAShapeLayer *earLayer;

@end

@implementation PandaAnimationView1

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}


#pragma mark - public
-(void)startAnimation{
//    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//        [self reset];
        [self do1];
//    });
}

-(void)reset{
    [self.faceShapLayer removeFromSuperlayer];
    [self.faceShapeLayer1 removeFromSuperlayer];
    [self.faceMaskLayer removeFromSuperlayer];
    [self.leftEyeLayer removeFromSuperlayer];
    [self.rightEyeLayer removeFromSuperlayer];
    [self.earView removeFromSuperview];
    [self.earLayer removeFromSuperlayer];
}

#pragma mark - do1
-(void)do1{
    self.faceShapLayer = [CAShapeLayer layer];
    self.faceShapLayer.path = [self createFacePath].CGPath;
    self.faceShapLayer.backgroundColor = [UIColor clearColor].CGColor;
    self.faceShapLayer.position = CGPointMake(self.bounds.size.width / 2., self.bounds.size.height / 2.);
    [self.layer addSublayer:self.faceShapLayer];
    
    self.faceShapLayer.bounds = CGRectMake(PDBiliFloat(19) , PDBiliFloat(20), (self.bounds.size.width - PDBiliFloat(20) * 2), (self.bounds.size.height - PDBiliFloat(20) * 2));
    self.faceShapLayer.position = CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMidY(self.bounds));
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.duration = duration1;
    animation.fromValue = @0.0;
    animation.toValue = @1.;
    animation.delegate = self;
    [animation setValue:@"do1" forKey:kName];
    [self.faceShapLayer addAnimation:animation forKey:nil];
}

#pragma mark - 脸部抖动
-(void)do11{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    scaleAnimation.springBounciness    = 20.f;
    [scaleAnimation setValue:@"do11" forKey:kName];
    scaleAnimation.delegate = self;
    [self.faceShapLayer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

#pragma mark - 脸部变白
-(void)do2{
    [self do2a];
    [self do2b];
    [self do2c];
}

-(void)do2a{
    self.faceShapeLayer1 = [CAShapeLayer layer];
    UIBezierPath* oval2Path = [UIBezierPath bezierPathWithOvalInRect: CGRectMake(30, 30, 79.7, 79.7)];
    self.faceShapeLayer1.path = oval2Path.CGPath;
    self.faceShapeLayer1.fillColor = [UIColor whiteColor].CGColor;
    self.faceShapeLayer1.opacity = 0;
    self.faceShapeLayer1.transform = CATransform3DMakeScale(0, 0, 0);
    [self.layer insertSublayer:self.faceShapeLayer1 atIndex:0];
}

-(void)do2b{
    self.faceMaskLayer = [CAShapeLayer layer];
    self.faceMaskLayer.path = self.faceShapeLayer1.path;
    self.faceMaskLayer.frame = self.bounds;
    self.faceMaskLayer.position = CGPointMake(self.bounds.size.width / 2., self.bounds.size.height / 2.);
    self.faceMaskLayer.fillColor = [UIColor blackColor].CGColor;
    self.faceMaskLayer.opacity = 0;
    self.faceMaskLayer.transform = CATransform3DMakeScale(0, 0, 0);
    [self.layer insertSublayer:self.faceMaskLayer atIndex:0];
}

-(void)do2c{
    self.faceShapeLayer1.opacity = 1;
    self.faceShapeLayer1.transform = CATransform3DMakeScale(1, 1, 0);
    
    self.faceMaskLayer.transform = CATransform3DMakeScale(1, 1, 0);
    
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.fromValue = [NSNumber numberWithDouble:0.0];
    animation.toValue = [NSNumber numberWithDouble:1.0];
    animation.duration = duration2;
    //    animation.beginTime = CACurrentMediaTime() + 0.2f;
    animation.fillMode = kCAFillModeBackwards;
    animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    
    animation.delegate = self;
    [animation setValue:@"do2" forKey:kName];
    [self.faceMaskLayer addAnimation:animation forKey:@"growMask"];
}

-(void)do3{
    [self do3a];
    [self do3b];
    [self do3c];
    
}

-(void)do3a{
    self.leftEyeLayer = [CAShapeLayer layer];
    self.leftEyeLayer.path = [self createLeftEyePath].CGPath;
    self.leftEyeLayer.fillColor = [UIColor blackColor].CGColor;
    self.leftEyeLayer.strokeColor = [UIColor blackColor].CGColor;
    self.leftEyeLayer.lineWidth = PDBiliFloat(3);
    self.leftEyeLayer.bounds = CGRectMake(33.24, 57.22, 32.6, 21.82);
    self.leftEyeLayer.position = CGPointMake(33.24 + 32.6 / 2., 57.22 + 21.82 / 2.);
    [self.layer addSublayer:self.leftEyeLayer];
}

-(void)do3b{
    self.rightEyeLayer = [CAShapeLayer layer];
    self.rightEyeLayer.path = [self createRightEyePath].CGPath;
    self.rightEyeLayer.fillColor = [UIColor blackColor].CGColor;
    self.rightEyeLayer.strokeColor = [UIColor blackColor].CGColor;
    self.rightEyeLayer.lineWidth = PDBiliFloat(3);
    self.rightEyeLayer.bounds = CGRectMake(74.39, 57.22, 32.6, 21.82);
    self.rightEyeLayer.position = CGPointMake(74.39 + 32.6 / 2., 57.22 + 21.82 / 2.);
    [self.layer addSublayer:self.rightEyeLayer];
}

-(void)do3c{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation.duration = duration3;
    animation.fromValue = @0.0;
    animation.toValue = @1.0;
    animation.delegate = self;
    [animation setValue:@"do3" forKey:kName];
    [self.leftEyeLayer addAnimation:animation forKey:nil];
    
    CABasicAnimation *animation1 = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    animation1.duration = duration3;
    animation1.fromValue = @0.0;
    animation1.toValue = @1.0;
    animation1.delegate = self;
    [self.rightEyeLayer addAnimation:animation1 forKey:nil];
}

-(void)do3d{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    scaleAnimation.springBounciness    = 30.f;
    [self.leftEyeLayer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    
    POPSpringAnimation *scaleAnimation1 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation1.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation1.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    scaleAnimation1.springBounciness    = 30.f;
    scaleAnimation1.delegate = self;
    [scaleAnimation1 setValue:@"do3d" forKey:kName];
    [self.rightEyeLayer pop_addAnimation:scaleAnimation1 forKey:@"scaleAnimation"];
}



// 耳朵
-(void)do4{
    [self do4a];
    [self do4b];
    [self do4c];
}

-(void)do4a{
    self.earView = [[UIView alloc]init];
    self.earView.frame = self.bounds;
    self.earView.backgroundColor = [UIColor blackColor];
    [self insertSubview:self.earView atIndex:0];
}

-(void)do4b{
    self.earLayer = [CAShapeLayer layer];
    self.earLayer.path = [self createEarsPath].CGPath;
    self.earLayer.frame = self.earView.bounds;
    self.earLayer.fillRule = kCAFillRuleEvenOdd;
    self.earLayer.position = CGPointMake(self.bounds.size.width / 2., self.bounds.size.height / 2.);
    self.earLayer.anchorPoint = CGPointMake(.5f,.5f);
    self.earLayer.fillColor = [UIColor purpleColor].CGColor;
    self.earLayer.transform = CATransform3DMakeScale(0, 0, 0);
    self.earView.layer.mask = self.earLayer;
    
    self.earView.layer.masksToBounds = YES;
}

-(void)do4c{
    self.earLayer.transform = CATransform3DMakeScale(1, 1, 0);
    
    CABasicAnimation *maskAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    maskAnimation.fromValue = [NSNumber numberWithDouble:0.0];
    maskAnimation.toValue = [NSNumber numberWithDouble:1.0];
    maskAnimation.duration = duration4;
    maskAnimation.fillMode = kCAFillModeBackwards;
    maskAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    maskAnimation.delegate = self;
    [maskAnimation setValue:@"do4c" forKey:kName];
    [self.earLayer addAnimation:maskAnimation forKey:@"growMask"];
}

-(void)do4d{
    POPSpringAnimation *scaleAnimation1 = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation1.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation1.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
    scaleAnimation1.springBounciness    = 25.f;
    
    scaleAnimation1.delegate = self;
    [scaleAnimation1 setValue:@"do4d" forKey:kName];
    [self.earView.layer pop_addAnimation:scaleAnimation1 forKey:@"scaleAnimation"];
}

-(void)do4e{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.duration = .3f;                                             // 持续时间
    animation.repeatCount = 1;                                          // 重复次数
    animation.fromValue = [NSNumber numberWithFloat: 0];               // 起始角度
    animation.toValue = [NSNumber numberWithFloat: - M_PI / 40.];  // 终止角度
    animation.delegate = self;
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    [animation setValue:@"do4e" forKey:kName];
    [self.earLayer addAnimation:animation forKey:@"smart"];
    
    
}

-(void)do4e2{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.duration = .3f;                                             // 持续时间
    animation.repeatCount = 1;                                          // 重复次数
    animation.fromValue = [NSNumber numberWithFloat: - M_PI / 40.];             // 起始角度
    animation.toValue = [NSNumber numberWithFloat: M_PI / 2. + M_PI / 40];  // 终止角度
    animation.delegate = self;
    [animation setValue:@"do4e2" forKey:kName];
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    [self.earLayer addAnimation:animation forKey:@"smart"];
}

-(void)do4e3{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.duration = .1f;                                             // 持续时间
    animation.repeatCount = 1;                                          // 重复次数
    animation.fromValue = [NSNumber numberWithFloat: M_PI / 2. + M_PI / 40];           // 起始角度
    animation.toValue = [NSNumber numberWithFloat: M_PI / 2. - M_PI / 40];  // 终止角度
    animation.delegate = self;
    [animation setValue:@"do4e3" forKey:kName];
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    [self.earLayer addAnimation:animation forKey:@"smart"];
}

-(void)do4e4{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    animation.duration = .1f;                                             // 持续时间
    animation.repeatCount = 1;                                          // 重复次数
    animation.fromValue = [NSNumber numberWithFloat: M_PI / 2. - M_PI / 40];           // 起始角度
    animation.toValue = [NSNumber numberWithFloat: M_PI / 2.];  // 终止角度
    animation.delegate = self;
    [animation setValue:@"do4e4" forKey:kName];
    animation.fillMode = kCAFillModeForwards;
    animation.removedOnCompletion = NO;
    [self.earLayer addAnimation:animation forKey:@"smart"];
}


#pragma mark - AnimationDelegate
-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    if ([[anim valueForKey:kName] isEqualToString:@"do1"]){           // 【动画1：画圆】
        [self do11];                // 1. 执行抖动
        [self do2];                 // 2. 脸部变白
        [self do3];                 // 3. 眼睛出来
    }  else if ([[anim valueForKey:kName] isEqualToString:@"do3"]){
        // 1. 耳朵出来
        [self do4];
        // 2. 眼睛抖动
        [self do3d];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do4c"]){
        [self do4d];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do4e"]){
        //        CGAffineTransform at =CGAffineTransformMakeRotation(- M_PI / 4.);
        //
        //        [self.earLayer setAffineTransform:at];
        [self do4e2];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do4e2"]){
        [self do4e3];
    } else if ([[anim valueForKey:kName] isEqualToString:@"do4e3"]){
        [self do4e4];
    } else if ([[anim valueForKey:kName]isEqualToString:@"do4e4"]){
        void(^block)() = objc_getAssociatedObject(self, &animationFinishBlock);
        if (block){
            block();
        }
    }
}

- (void)pop_animationDidStop:(POPAnimation *)anim finished:(BOOL)finished{
    if ([[anim valueForKey:kName] isEqualToString:@"do4d"]){
        [self do4e];
    }
}



#pragma mark - Path
// 脸轨迹
-(UIBezierPath *)createFacePath{
    UIBezierPath* bezier5Path = [UIBezierPath bezierPath];
    [bezier5Path moveToPoint: CGPointMake(69.85, 30)];
    [bezier5Path addCurveToPoint: CGPointMake(48.52, 36.18) controlPoint1: CGPointMake(62.01, 30) controlPoint2: CGPointMake(54.69, 32.27)];
    [bezier5Path addCurveToPoint: CGPointMake(30, 69.85) controlPoint1: CGPointMake(37.39, 43.25) controlPoint2: CGPointMake(30, 55.69)];
    [bezier5Path addCurveToPoint: CGPointMake(69.85, 109.7) controlPoint1: CGPointMake(30, 91.86) controlPoint2: CGPointMake(47.84, 109.7)];
    [bezier5Path addCurveToPoint: CGPointMake(109.7, 69.85) controlPoint1: CGPointMake(91.86, 109.7) controlPoint2: CGPointMake(109.7, 91.86)];
    [bezier5Path addCurveToPoint: CGPointMake(69.85, 30) controlPoint1: CGPointMake(109.7, 47.84) controlPoint2: CGPointMake(91.86, 30)];
    [bezier5Path closePath];
    [bezier5Path moveToPoint: CGPointMake(114.7, 69.85)];
    [bezier5Path addCurveToPoint: CGPointMake(69.85, 114.7) controlPoint1: CGPointMake(114.7, 94.62) controlPoint2: CGPointMake(94.62, 114.7)];
    [bezier5Path addCurveToPoint: CGPointMake(25, 69.85) controlPoint1: CGPointMake(45.08, 114.7) controlPoint2: CGPointMake(25, 94.62)];
    [bezier5Path addCurveToPoint: CGPointMake(44.21, 33.04) controlPoint1: CGPointMake(25, 54.61) controlPoint2: CGPointMake(32.6, 41.15)];
    [bezier5Path addCurveToPoint: CGPointMake(69.85, 25) controlPoint1: CGPointMake(51.48, 27.97) controlPoint2: CGPointMake(60.32, 25)];
    [bezier5Path addCurveToPoint: CGPointMake(114.7, 69.85) controlPoint1: CGPointMake(94.62, 25) controlPoint2: CGPointMake(114.7, 45.08)];
    [bezier5Path closePath];
    
    return bezier5Path;
}

// 创建左眼轨迹
-(UIBezierPath *)createLeftEyePath{
    UIBezierPath* bezier4Path = [UIBezierPath bezierPath];
    [bezier4Path moveToPoint: CGPointMake(48.02, 58.05)];
    [bezier4Path addCurveToPoint: CGPointMake(51.96, 60.32) controlPoint1: CGPointMake(49.42, 58.74) controlPoint2: CGPointMake(50.79, 59.43)];
    [bezier4Path addCurveToPoint: CGPointMake(65.84, 65.12) controlPoint1: CGPointMake(56.13, 63.05) controlPoint2: CGPointMake(60.73, 64.66)];
    [bezier4Path addCurveToPoint: CGPointMake(42.48, 79.04) controlPoint1: CGPointMake(61.44, 73.55) controlPoint2: CGPointMake(52.66, 79.04)];
    [bezier4Path addCurveToPoint: CGPointMake(33.24, 69.93) controlPoint1: CGPointMake(37.41, 79.04) controlPoint2: CGPointMake(33.24, 74.93)];
    [bezier4Path addCurveToPoint: CGPointMake(40.88, 58.08) controlPoint1: CGPointMake(33.24, 64.93) controlPoint2: CGPointMake(36.24, 60.35)];
    [bezier4Path addCurveToPoint: CGPointMake(48.06, 58.08) controlPoint1: CGPointMake(43.18, 56.93) controlPoint2: CGPointMake(45.75, 56.93)];
    [bezier4Path addLineToPoint: CGPointMake(48.06, 58.08)];
    [bezier4Path addLineToPoint: CGPointMake(48.02, 58.05)];
    [bezier4Path closePath];
    bezier4Path.miterLimit = 4;
    return bezier4Path;
    
}

// 创建右眼轨迹
-(UIBezierPath *)createRightEyePath{
    //// Bezier 3 Drawing
    UIBezierPath* bezier3Path = [UIBezierPath bezierPath];
    [bezier3Path moveToPoint: CGPointMake(92.19, 58)];
    [bezier3Path addCurveToPoint: CGPointMake(88.26, 60.29) controlPoint1: CGPointMake(90.8, 58.69) controlPoint2: CGPointMake(89.41, 59.37)];
    [bezier3Path addCurveToPoint: CGPointMake(74.39, 65.09) controlPoint1: CGPointMake(84.1, 63.03) controlPoint2: CGPointMake(79.47, 64.63)];
    [bezier3Path addCurveToPoint: CGPointMake(97.73, 79.03) controlPoint1: CGPointMake(78.78, 73.55) controlPoint2: CGPointMake(87.56, 79.03)];
    [bezier3Path addCurveToPoint: CGPointMake(106.98, 69.89) controlPoint1: CGPointMake(102.82, 79.03) controlPoint2: CGPointMake(106.98, 74.92)];
    [bezier3Path addCurveToPoint: CGPointMake(99.35, 58) controlPoint1: CGPointMake(106.98, 64.86) controlPoint2: CGPointMake(103.98, 60.29)];
    [bezier3Path addCurveToPoint: CGPointMake(92.19, 58) controlPoint1: CGPointMake(97.04, 56.86) controlPoint2: CGPointMake(94.5, 56.86)];
    [bezier3Path closePath];
    bezier3Path.miterLimit = 4;
    
    return bezier3Path;
}

-(UIBezierPath *)createEarsPath{
    UIBezierPath* bezier5Path = [UIBezierPath bezierPath];
    [bezier5Path moveToPoint: CGPointMake(46.64, 22.57)];
    [bezier5Path addCurveToPoint: CGPointMake(70.34, 46) controlPoint1: CGPointMake(54.56, 30.4) controlPoint2: CGPointMake(62.46, 38.21)];
    [bezier5Path addLineToPoint: CGPointMake(74.64, 41.75)];
    [bezier5Path addCurveToPoint: CGPointMake(93.8, 22.8) controlPoint1: CGPointMake(81.02, 35.44) controlPoint2: CGPointMake(87.4, 29.13)];
    [bezier5Path addCurveToPoint: CGPointMake(106.29, 22.8) controlPoint1: CGPointMake(97.27, 19.37) controlPoint2: CGPointMake(102.82, 19.37)];
    [bezier5Path addLineToPoint: CGPointMake(117.85, 34.23)];
    [bezier5Path addCurveToPoint: CGPointMake(117.85, 46.57) controlPoint1: CGPointMake(121.32, 37.66) controlPoint2: CGPointMake(121.32, 43.14)];
    [bezier5Path addCurveToPoint: CGPointMake(94.38, 69.78) controlPoint1: CGPointMake(110.01, 54.33) controlPoint2: CGPointMake(102.19, 62.06)];
    [bezier5Path addCurveToPoint: CGPointMake(117.85, 92.98) controlPoint1: CGPointMake(102.19, 77.5) controlPoint2: CGPointMake(110.01, 85.22)];
    [bezier5Path addCurveToPoint: CGPointMake(117.85, 105.32) controlPoint1: CGPointMake(121.32, 96.41) controlPoint2: CGPointMake(121.32, 101.89)];
    [bezier5Path addLineToPoint: CGPointMake(106.29, 116.75)];
    [bezier5Path addCurveToPoint: CGPointMake(93.8, 116.75) controlPoint1: CGPointMake(102.82, 120.18) controlPoint2: CGPointMake(97.27, 120.18)];
    [bezier5Path addCurveToPoint: CGPointMake(70.34, 93.55) controlPoint1: CGPointMake(85.96, 109) controlPoint2: CGPointMake(78.15, 101.27)];
    [bezier5Path addCurveToPoint: CGPointMake(46.64, 116.98) controlPoint1: CGPointMake(62.46, 101.35) controlPoint2: CGPointMake(54.56, 109.15)];
    [bezier5Path addCurveToPoint: CGPointMake(34.16, 116.98) controlPoint1: CGPointMake(43.18, 120.41) controlPoint2: CGPointMake(37.63, 120.41)];
    [bezier5Path addLineToPoint: CGPointMake(22.6, 105.55)];
    [bezier5Path addCurveToPoint: CGPointMake(22.6, 93.21) controlPoint1: CGPointMake(19.13, 102.12) controlPoint2: CGPointMake(19.13, 96.64)];
    [bezier5Path addCurveToPoint: CGPointMake(44.87, 71.19) controlPoint1: CGPointMake(30.04, 85.85) controlPoint2: CGPointMake(37.46, 78.51)];
    [bezier5Path addLineToPoint: CGPointMake(46.3, 69.78)];
    [bezier5Path addCurveToPoint: CGPointMake(22.6, 46.35) controlPoint1: CGPointMake(38.41, 61.98) controlPoint2: CGPointMake(30.52, 54.18)];
    [bezier5Path addCurveToPoint: CGPointMake(22.6, 34) controlPoint1: CGPointMake(19.13, 42.92) controlPoint2: CGPointMake(19.13, 37.43)];
    [bezier5Path addCurveToPoint: CGPointMake(32.66, 24.06) controlPoint1: CGPointMake(22.6, 34) controlPoint2: CGPointMake(29.53, 27.15)];
    [bezier5Path addCurveToPoint: CGPointMake(34.16, 22.57) controlPoint1: CGPointMake(33.57, 23.15) controlPoint2: CGPointMake(34.16, 22.57)];
    [bezier5Path addCurveToPoint: CGPointMake(46.64, 22.57) controlPoint1: CGPointMake(37.63, 19.14) controlPoint2: CGPointMake(43.18, 19.14)];
    [bezier5Path closePath];
    
    return bezier5Path;
}

-(void)startAnimationWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &animationFinishBlock, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self startAnimation];
}

#pragma mark - 消失动画
-(void)dismissAnimation{
    
}
@end
