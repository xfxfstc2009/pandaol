//
//  ArcToCircleLayer.h
//  MyAnimation
//
//  Created by 裴烨烽 on 16/5/19.
//  Copyright © 2016年 Animation. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIKit.h>

@interface ArcToCircleLayer : CALayer

@property (nonatomic,assign) CGFloat progress;
@property (nonatomic,strong) UIColor *color;
@property (nonatomic,assign) CGFloat lineWidth;

@end
