//
//  PandaAnimationView1.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/6/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

extern CGFloat PDBiliFloat(CGFloat floatValue);
extern CGFloat PDBiliFloatWithPadding(CGFloat floatValue, CGFloat padding);
static CGFloat animationWidth3 = 100;


@interface PandaAnimationView1 : UIView

-(void)startAnimationWithBlock:(void(^)())block;
-(void)dismissAnimation;
@end

