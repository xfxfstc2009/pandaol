//
//  PDPandaHUDView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

extern CGFloat PDBiliFloat(CGFloat floatValue);
extern CGFloat PDBiliFloatWithPadding(CGFloat floatValue, CGFloat padding);
static CGFloat animationWidth11 = 100;

@interface PDPandaHUDView : UIView

-(void)startAnimationWithBlock:(void(^)())block;
-(void)animation;
@end

