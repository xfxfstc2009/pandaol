//
//  PDInformationRootViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInformationRootViewController.h"
#import "PDWebView.h"


@interface PDInformationRootViewController()<PDWebViewDelegate>
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)PDWebView *webView;
@end

@implementation PDInformationRootViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createWebView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
    self.leftButton = [weakSelf leftBarButtonWithTitle:@"侧边" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSrlf = weakSelf;
        if (strongSrlf.webView.isCanGoback){
            [strongSrlf.webView goBack];
        } else {
            [[RESideMenu shareInstance] presentLeftMenuViewController];
        }
    }];
}


-(void)createWebView{
    self.webView = [[PDWebView alloc]initWithFrame:self.view.bounds];
    [self.webView loadURLString:@"http://www.pandaol.com/article/default/mobile"];
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.webView.delegate = self;
    [self.view addSubview:self.webView];
}



- (void)webViewDidStartLoad:(PDWebView *)webview {
    NSLog(@"页面开始加载");
}

- (void)webView:(PDWebView *)webview shouldStartLoadWithURL:(NSURL *)URL {
    NSLog(@"截取到URL：%@",URL);
}

- (void)webView:(PDWebView *)webview didFinishLoadingURL:(NSURL *)URL { // 加载完成
    NSString *title = @"";
    if (webview.uiWebView){
        title = [webview.uiWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
    } else {
        title = webview.wkWebView.title;
    }
    self.barMainTitle = title;
    if (self.webView.isCanGoback){
        [self.leftButton setTitle:@"返回" forState:UIControlStateNormal];
    } else {
        [self.leftButton setTitle:@"侧边" forState:UIControlStateNormal];
    }
}

- (void)webView:(PDWebView *)webview didFailToLoadURL:(NSURL *)URL error:(NSError *)error {
    NSLog(@"加载出现错误");
}


@end
