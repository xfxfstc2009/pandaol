//
//  GWLaunchViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/13.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWLaunchViewController.h"
#import "PandaAnimationView1.h"                         /**< 动画模块*/
#import "PDTabBarController.h"
#import "TextAnimation.h"
#import "PDPandaRootViewController.h"

@interface GWLaunchViewController()
@property (nonatomic,strong)PandaAnimationView1 *pandaAnimationView1;
@property (nonatomic,strong)UIView *bgView;

@property (nonatomic,strong)PDImageView *launchImageView;
@property (nonatomic,strong)PDImageView *logoImageView;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *moonLabel;
@property (nonatomic,strong)UILabel *contentLabel;

@end

@implementation GWLaunchViewController

-(void)dealloc{
    NSLog(@"释放");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self createView];
//    [self createAnimationView];
    __weak typeof(self)waekSelf = self;
    [waekSelf sendRequestGetLaunchImageInfo];
}

#pragma mark - createView
-(void)createView{
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.textColor = [UIColor whiteColor];
    self.numberLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:120];
    self.numberLabel.autoresizesSubviews = YES;
    self.numberLabel.text = [NSDate getCurrentTimeWithDay];
    self.numberLabel.adjustsFontSizeToFitWidth = YES;
    [self.view addSubview:self.numberLabel];
    
    // Moon
    self.moonLabel = [[UILabel alloc] init];
    self.moonLabel.textColor = [UIColor whiteColor];
    self.moonLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:30];
    
    NSString *moon =  [NSDate getCurrentTimeWithMoon];
    NSString *realMoon = @"";
    if ([moon isEqualToString:@"1"]){
        realMoon = @"Jan.";
    } else if ([moon isEqualToString:@"2"]){
        realMoon = @"Feb.";
    } else if ([moon isEqualToString:@"3"]){
        realMoon = @"Mar.";
    } else if ([moon isEqualToString:@"4"]){
        realMoon = @"Apr.";
    } else if ([moon isEqualToString:@"5"]){
        realMoon = @"May.";
    } else if ([moon isEqualToString:@"6"]){
        realMoon = @"Jun.";
    } else if ([moon isEqualToString:@"7"]){
        realMoon = @"Jul.";
    } else if ([moon isEqualToString:@"8"]){
        realMoon = @"Aug.";
    } else if ([moon isEqualToString:@"9"]){
        realMoon = @"Sep.";
    } else if ([moon isEqualToString:@"10"]){
        realMoon = @"Oct.";
    } else if ([moon isEqualToString:@"11"]){
        realMoon = @"Nov.";
    } else if ([moon isEqualToString:@"12"]){
        realMoon = @"Dec.";
    }
    
    self.moonLabel.text = realMoon;
    [self.view addSubview:self.moonLabel];
    
    // Moon
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.textColor = [UIColor whiteColor];
    self.contentLabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:20];
    self.contentLabel.numberOfLines = 0;
    [self.view addSubview:self.contentLabel];
    
}

-(void)sendRequestGetLaunchImageInfo{
    FetchModel *baseSettingModel = [[FetchModel alloc]init];
    __weak typeof(self)weakSelf = self;
    
    [baseSettingModel fetchWithPath:bindingGameserver completionHandler:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
             [UIView animateWithDuration:3 animations:^{
                strongSelf.launchImageView.transform = CGAffineTransformMakeScale(1.1, 1.1);
                strongSelf.launchImageView.alpha = 0;
                strongSelf.view.alpha = 0;
            } completion:^(BOOL finished) {
                [strongSelf.view removeFromSuperview];
            }];
        } else {
            [UIView animateWithDuration:3 animations:^{
                strongSelf.launchImageView.transform = CGAffineTransformMakeScale(1.1, 1.1);
                strongSelf.launchImageView.alpha = 0;
                strongSelf.view.alpha = 0;
            } completion:^(BOOL finished) {
                [strongSelf.view removeFromSuperview];
            }];
        }
    }];
}


@end
