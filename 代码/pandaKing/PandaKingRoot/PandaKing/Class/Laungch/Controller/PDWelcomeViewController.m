//
//  PDWelcomeViewController.m
//  PandaKing
//
//  Created by Cranz on 16/10/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDWelcomeViewController.h"

@interface PDWelcomeViewController ()<UIScrollViewDelegate>
@property (nonatomic, strong) NSArray *lunchImages;

@property (nonatomic, strong) UIPageControl *pageControl;
@end

@implementation PDWelcomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self lunchScrollView];
//    [self setPageView];
}

- (NSArray *)lunchImages {
    
    return @[@"01",@"02",@"03",@"04",@"05"];
    
    if (iphone4) {
        return @[@"ip4_1",@"ip4_2",@"ip4_3",@"ip4_4"];
    } else if (iphone5) {
        return @[@"ip5_1",@"ip5_2",@"ip5_3",@"ip5_4"];
    } else if (iphone6) {
        return @[@"ip6_1",@"ip6_2",@"ip6_3",@"ip6_4"];
    } else { //7P
        return @[@"ip7p_1",@"ip7p_2",@"ip7p_3",@"ip7p_4"];
    }
}

- (void)lunchScrollView {
    UIScrollView *scrollView = [[UIScrollView alloc] initWithFrame:kScreenBounds];
    scrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.lunchImages.count, kScreenBounds.size.height);
    scrollView.showsHorizontalScrollIndicator = NO;
    scrollView.bounces = NO;
    scrollView.pagingEnabled = YES;
    scrollView.delegate = self;
    [self.view addSubview:scrollView];
    for (int i = 0; i < self.lunchImages.count; i++) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width * i, 0, kScreenBounds.size.width, kScreenBounds.size.height)];
        imageView.image = [UIImage imageNamed:[self.lunchImages objectAtIndex:i]];
        [scrollView addSubview:imageView];
        if (i == (self.lunchImages.count - 1)) {
            imageView.userInteractionEnabled = YES;
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.backgroundColor = [UIColor clearColor];
            if (iphone4) {
                button.frame = CGRectMake(self.view.frame.size.width/2- LCFloat(50), self.view.frame.size.height - LCFloat(75), LCFloat(100), LCFloat(30));
            } else if (iphone5) {
                button.frame = CGRectMake(self.view.frame.size.width/2- LCFloat(50), self.view.frame.size.height - LCFloat(80), LCFloat(100), LCFloat(30));
            } else if (iphone6) {
                button.frame = CGRectMake(self.view.frame.size.width/2- LCFloat(50), self.view.frame.size.height - LCFloat(80), LCFloat(100), LCFloat(30));
            } else if (iphone6Plus) {
                button.frame = CGRectMake(self.view.frame.size.width/2- LCFloat(50), self.view.frame.size.height - LCFloat(80), LCFloat(100), LCFloat(30));
            }
            __weak typeof(self)weakSelf = self;
            [button buttonWithBlock:^(UIButton *button) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (strongSelf.startBlock){
                    strongSelf.startBlock();
                }
            }];
            
            [imageView addSubview:button];
        }
    }
}

- (void)setPageView {
    UIPageControl *pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width * .4f, 30)];
    self.pageControl = pageControl;
    pageControl.center = CGPointMake(self.view.center.x, kScreenBounds.size.height - 20);
    pageControl.numberOfPages = self.lunchImages.count;
    pageControl.currentPageIndicatorTintColor = [UIColor redColor];
    pageControl.pageIndicatorTintColor = [UIColor whiteColor];
    pageControl.userInteractionEnabled = NO;
    pageControl.hidden = YES;
    [self.view addSubview:pageControl];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    NSInteger index = round(scrollView.contentOffset.x / kScreenBounds.size.width);
    self.pageControl.currentPage = index;
    if (index == self.lunchImages.count - 1) {
        self.pageControl.hidden = YES;
    } else {
        self.pageControl.hidden = NO;
    }
}

@end
