//
//  PDLaungchViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/7/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLaungchViewController.h"
#import "PandaAnimationView1.h"                         /**< 动画模块*/
#import "PDTabBarController.h"
#import "TextAnimation.h"
#import "PDAmusementRootViewController.h"


@interface PDLaungchViewController()
@property (nonatomic,strong)PandaAnimationView1 *pandaAnimationView1;
@property (nonatomic,strong)UITapGestureRecognizer *tapGesture;
@property (nonatomic,assign)BOOL hasTap;
@property (nonatomic,strong)PDImageView *fontView;

@end

@implementation PDLaungchViewController

-(void)dealloc{
    NSLog(@"释放");
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    __weak typeof(self)weakSelf = self;
    [weakSelf createAnimationView];
    
    [self createFontInfoAnimation];
}


-(void)createFontInfoAnimation{
    self.fontView = [[PDImageView alloc]init];
    self.fontView.backgroundColor = [UIColor clearColor];
    UIImage *fontImg = [UIImage imageNamed:@"pandaol_font_laungch"];
    self.fontView.image = fontImg;
    self.fontView.frame = CGRectMake((kScreenBounds.size.width - fontImg.size.width) / 2., kScreenBounds.size.height - LCFloat(30) - 64, fontImg.size.width, fontImg.size.height);
    self.fontView.alpha = 0;
    [self.view addSubview:self.fontView];
    [UIView animateWithDuration:.8f animations:^{
        self.fontView.alpha = 1;
    }];
}

#pragma mark - createAnimationView
-(void)createAnimationView{
    // 1. 创建熊猫头像
    self.pandaAnimationView1 = [[PandaAnimationView1 alloc]init];
    self.pandaAnimationView1.frame = CGRectMake((kScreenBounds.size.width - 140) / 2., (kScreenBounds.size.height - 140) / 2. - 104, 140, 140);
    self.pandaAnimationView1.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.pandaAnimationView1];
    
    // 2. 熊猫动画结束后消失
    __weak typeof(self)weakSelf = self;
    [weakSelf.pandaAnimationView1 startAnimationWithBlock:^{
        [self animationMove];
    }];
    
    // 3. 创建文字
    TextAnimation *textAnimation = [[TextAnimation alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(self.pandaAnimationView1.frame) , kScreenBounds.size.width, [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:37.]]) type:animationType1 withTitle:@"PANDAOL"];
    textAnimation.frame = CGRectMake((kScreenBounds.size.width - textAnimation.size_width) / 2., CGRectGetMaxY(self.pandaAnimationView1.frame) + 30, textAnimation.size_width, textAnimation.size_height);
    [textAnimation showAnimationWithFinishBlock:NULL];
    [self.view addSubview:textAnimation];
    
    // 4. 创建手势
    self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(animationStopTapManager)];
    [self.view addGestureRecognizer:self.tapGesture];
}

-(void)animationMove{
    CGRect convertFrame = [[PDPandaRootViewController sharedController].view convertRect:[PDPandaRootViewController sharedController].paopaoImageView.frame toView:self.view.window];
    [UIView animateWithDuration:.3f animations:^{
     self.pandaAnimationView1.center = CGPointMake(convertFrame.origin.x + convertFrame.size.width / 2., convertFrame.origin.y + convertFrame.size.height / 2.);
    } completion:^(BOOL finished) {
        [self performSelector:@selector(dismissAniamtionManager3) withObject:nil afterDelay:0];
    }];
}


#pragma mark - 消失动画1
-(void)dismissAniamtionManager1{
    [UIView animateWithDuration:1 animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(10, 10);
        self.pandaAnimationView1.alpha = 0;
        self.view.alpha = 0;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

#pragma mark - 消失动画2
-(void)dismissAniamtionManager2{
    [UIView animateWithDuration:.2 animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(1.4, 1.4);
    } completion:^(BOOL finished) {
        [self dismissAnimationManager21];
    }];
}

-(void)dismissAnimationManager21{
    [UIView animateWithDuration:.3f animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(.1f, .1f);
//        self.pandaAnimationView1.alpha = 0;
    } completion:^(BOOL finished) {
        [self.view removeFromSuperview];
    }];
}

#pragma mark - 消失动画3
-(void)dismissAniamtionManager3{
    [UIView animateWithDuration:.2 animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(1.4, 1.4);
    } completion:^(BOOL finished) {
        [self dismissAnimationManager31];
    }];
}

-(void)dismissAnimationManager31{
    [UIView animateWithDuration:.3f animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(.7f, .7f);
    } completion:^(BOOL finished) {
        self.view.backgroundColor = [UIColor whiteColor];
        if (self.hasTap){
            return ;
        }
        [self dismissAnimationManager32];
    }];
}

-(void)dismissAnimationManager32{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [UIView mdDeflateTransitionFromView:self.view toView:keywindow.rootViewController.view originalPoint:self.pandaAnimationView1.center duration:.9f completion:^{
        [self.pandaAnimationView1 removeFromSuperview];
        self.pandaAnimationView1 = nil;
        [self.view removeFromSuperview];
        [[PDMainTabbarViewController sharedController].amusementViewController showNewFeature];
        
        // 【判断是否登录】
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            // 已经登录了，就去加载头像
            [PDPandaRootViewController sharedController].hasLaungchLinkAnimation = NO;
            
            // 签到
            [[AccountModel sharedAccountModel] sendRequestToQiandao];
        }
    }];
}


#pragma mark - 消失动画4
-(void)dismissAniamtionManager4{
    [UIView animateWithDuration:.2 animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(1.4, 1.4);
    } completion:^(BOOL finished) {
        [self dismissAnimationManager41];
    }];
}

-(void)dismissAnimationManager41{
    [UIView animateWithDuration:.3f animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(.8f, .8f);
        
    } completion:^(BOOL finished) {
        self.view.backgroundColor = [UIColor clearColor];
        [self dismissAnimationManager42];
    }];
}

-(void)dismissAnimationManager42{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [UIView mdDeflateTransitionFromView:self.view toView:keywindow.window originalPoint:self.pandaAnimationView1.center duration:.9f completion:^{
        [self.view removeFromSuperview];
    }];
}

#pragma mark - 消失动画5
-(void)dismissAniamtionManager5{
    [UIView animateWithDuration:.2 animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(1.4, 1.4);
    } completion:^(BOOL finished) {
        [self dismissAnimationManager51];
    }];
}

-(void)dismissAnimationManager51{
    [UIView animateWithDuration:.3f animations:^{
        self.pandaAnimationView1.transform = CGAffineTransformMakeScale(.8f, .8f);
    } completion:^(BOOL finished) {
        self.view.backgroundColor = [UIColor whiteColor];
        [self dismissAnimationManager52];
    }];
}

-(void)dismissAnimationManager52{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [UIView mdInflateTransitionFromView:self.view toView:keywindow.window originalPoint:self.pandaAnimationView1.center duration:.9f completion:^{
        //        [self.view removeFromSuperview];
    }];
}




#pragma mark - 手势点击页面消失
-(void)animationStopTapManager{
    self.pandaAnimationView1.transform = CGAffineTransformMakeScale(.7f, .7f);
    self.hasTap = YES;
    [self dismissAnimationManager32];
}


#pragma mark -
@end
