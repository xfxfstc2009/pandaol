//
//  PDPandaPaopaoImgView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaPaopaoImgView.h"
#import <objc/runtime.h>

static char tapKey;

@interface PDPandaPaopaoImgView()
@property (nonatomic,strong)UILabel *paopaoFixedLabel;                  /**< */
@property (nonatomic,strong)UILabel *paopaoDymicLabel;                  /**< 泡泡动态label*/

@end

@implementation PDPandaPaopaoImgView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.backgroundColor = [UIColor clearColor];
    self.image = [UIImage imageNamed:@"home_paopao"];
    self.userInteractionEnabled = YES;
    
    // 2. 创建label
    self.paopaoFixedLabel = [[UILabel alloc]init];
    self.paopaoFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.paopaoFixedLabel.frame = CGRectMake(0, LCFloat(40), self.size_width, [NSString contentofHeightWithFont:self.paopaoFixedLabel.font]);
    self.paopaoFixedLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.paopaoFixedLabel.textAlignment = NSTextAlignmentCenter;
    self.paopaoFixedLabel.text = @"今日新增金币";
    [self addSubview:self.paopaoFixedLabel];
    
    // 3. 创建动态label
    self.paopaoDymicLabel = [[UILabel alloc]init];
    self.paopaoDymicLabel.font = [[UIFont systemFontOfCustomeSize:30.]boldFont];
    self.paopaoDymicLabel.frame = CGRectMake(LCFloat(20), CGRectGetMaxY(self.paopaoFixedLabel.frame) + LCFloat(10), self.size_width - 2 * LCFloat(20), [NSString contentofHeightWithFont:self.paopaoDymicLabel.font]);
    self.paopaoDymicLabel.textAlignment = NSTextAlignmentCenter;
    self.paopaoDymicLabel.adjustsFontSizeToFitWidth = YES;
    self.paopaoDymicLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.paopaoDymicLabel.text = @"80";
    [self addSubview:self.paopaoDymicLabel];
    
    [self adjustWithHasLogin];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
    [self addGestureRecognizer:tap];
    
}

-(void)adjustWithHasLogin{
    if (![AccountModel sharedAccountModel].hasLoggedIn){
        self.paopaoDymicLabel.hidden = YES;
        self.paopaoFixedLabel.hidden = NO;
        self.paopaoFixedLabel.font = [[UIFont fontWithCustomerSizeName:@"标题"]boldFont];
        self.paopaoFixedLabel.text = @"登录/注册";
        self.paopaoFixedLabel.frame = self.bounds;
    } else {
        
    }
}


#pragma mark - 气泡点击方法
-(void)tapManager:(void(^)())block{
    objc_setAssociatedObject(self, &tapKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)tapClick{
    void(^block)() = objc_getAssociatedObject(self, &tapKey);
    if (block){
        block();
    }
}


@end
