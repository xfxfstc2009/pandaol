//
//  KMDetailsPageViewController.m
//  TheMovieDB
//
//  Created by Kevin Mindeguia on 04/02/2014.
//  Copyright (c) 2014 iKode Ltd. All rights reserved.
//

#import "KMScrollingHeaderView.h"
#import "PDGradientMainView.h"
#import "DKLiveBlurView.h"

@interface KMScrollingHeaderView () <UIScrollViewDelegate>

@property (nonatomic, strong) UIButton* imageButton;
@property (nonatomic,strong)UIView *alphaView;
@end

@implementation KMScrollingHeaderView

#pragma mark - Init Methods

- (instancetype)init {
    self = [super init];
    if (self) {
        _headerImageViewHeight = kDefaultImagePagerHeight;
        _headerImageViewScalingFactor = kDefaultImageScalingFactor;                 /**< 图片缩放因素*/
        _headerImageViewContentMode = UIViewContentModeScaleAspectFit;
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _headerImageViewHeight = kDefaultImagePagerHeight;
        _headerImageViewScalingFactor = kDefaultImageScalingFactor;
        _headerImageViewContentMode = UIViewContentModeScaleAspectFit;
        [self initialize];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame WithHeight:(CGFloat)height{
    self = [super initWithFrame:frame];
    if (self) {
        [self initializeWithHeight:height];
    }
    return self;
}

-(void)initializeWithHeight:(CGFloat)height{
    _headerImageViewHeight = height;
    _headerImageViewScalingFactor = kDefaultImageScalingFactor;
    _headerImageViewContentMode = UIViewContentModeScaleAspectFit;
    [self initialize];
    
    _navbarViewFadingOffset = ABS(_headerImageViewHeight - (CGRectGetHeight(_navbarView.frame) + kDefaultTableViewHeaderMargin));
}


- (void)initialize {

    [self setupTableView];
    [self setupTableViewHeader];
    [self setupImageView];

    self.autoresizesSubviews = YES;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin;
}

- (void)dealloc {
    [self.tableView removeObserver:self forKeyPath:@"contentOffset"];
}

#pragma mark - View layout

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _navbarViewFadingOffset = _headerImageViewHeight - (CGRectGetHeight(_navbarView.frame) + kDefaultTableViewHeaderMargin);
    
    if (!self.tableView)
        [self setupTableView];
    
    if (!self.tableView.tableHeaderView)
        [self setupTableViewHeader];
    
    if(!self.backgroundImageView)
        [self setupImageView];
    
    [self setupBackgroundColor];

    [self setupImageButton];
    
}

#pragma mark - Setup Methods

- (void)setupTableView {
    _tableView = [[UITableView alloc] initWithFrame:self.bounds];
    
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    
    void *context = (__bridge void *)self;

    [self.tableView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:context];
    
    [self addSubview:self.tableView];
}

- (void)setupTableViewHeader {
    CGRect tableHeaderViewFrame = CGRectMake(0.0, 0.0, self.tableView.frame.size.width, self.headerImageViewHeight - kDefaultTableViewHeaderMargin);
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:tableHeaderViewFrame];
    tableHeaderView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = tableHeaderView;
}

- (void)setupImageButton {
    if (!self.imageButton)
        self.imageButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, self.headerImageViewHeight)];
    
    [self.imageButton addTarget:self action:@selector(imageButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    [self.tableView.tableHeaderView addSubview:self.imageButton];
}

- (void)setupImageView {
    self.backgroundImageView = [[DKLiveBlurView alloc] initWithFrame:CGRectMake(0.0f, 0, self.tableView.frame.size.width, self.headerImageViewHeight)];
    self.backgroundImageView.backgroundColor = [UIColor blackColor];
    self.backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    self.backgroundImageView.clipsToBounds = YES;
    self.backgroundImageView.contentMode = self.headerImageViewContentMode;
    [self insertSubview:self.backgroundImageView belowSubview:self.tableView];
    self.backgroundImageView.size_height = kDefaultImageScalingFactor;
    
    if ([self.delegate respondsToSelector:@selector(detailsPage:headerImageView:)]){
        [self.delegate detailsPage:self headerImageView:self.backgroundImageView];
        self.backgroundImageView.isGlassEffectOn = YES;
    }
    
    self.alphaView = [[UIView alloc]init];
    self.alphaView.backgroundColor = c7;
    self.alphaView.alpha = .5f;
    self.alphaView.frame = self.backgroundImageView.bounds;
    [self.backgroundImageView addSubview:self.alphaView];
}

- (void)setupBackgroundColor {
    self.backgroundColor = [UIColor clearColor];
    self.tableView.backgroundColor = [UIColor clearColor];
}

- (void)setupImageViewGradient {
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.backgroundImageView.bounds;
    gradientLayer.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithRed:0 green:0 blue:0 alpha:1] CGColor], [(id)[UIColor colorWithRed:0 green:0 blue:0 alpha:0] CGColor], nil];
    
    gradientLayer.startPoint = CGPointMake(0.6f, 0.6);
    gradientLayer.endPoint = CGPointMake(0.6f, 1.0f);
    
    self.backgroundImageView.layer.mask = gradientLayer;
}

#pragma mark - Data Reload

- (void)reloadScrollingHeader;
{
    if ([self.delegate respondsToSelector:@selector(detailsPage:headerImageView:)])
        [self.delegate detailsPage:self headerImageView:self.backgroundImageView];

    [self.tableView reloadData];
}

#pragma mark - Setters

- (void)setNavbarView:(UIView *)navbarView
{
    if (_navbarView == navbarView)
    {
        return;
    }
    _navbarView = navbarView;

    [_navbarView setAlpha:0.0];
//    [_navbarView setHidden:YES];
}

- (void)setHeaderImageViewContentMode:(UIViewContentMode)headerImageViewContentMode
{
    if (_headerImageViewContentMode == headerImageViewContentMode)
    {
        return;
    }

    _headerImageViewContentMode = headerImageViewContentMode;

    self.backgroundImageView.contentMode = _headerImageViewContentMode;
}

#pragma mark - KVO Methods

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context != (__bridge void *)self)
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        return;
    }
    
    if ((object == self.tableView) && ([keyPath isEqualToString:@"contentOffset"] == YES))
    {
        [self scrollViewDidScrollWithOffset:self.tableView.contentOffset.y];
        return;
    }
}

#pragma mark - Action Methods

- (void)imageButtonPressed:(UIButton *)button
{
    if ([self.delegate respondsToSelector:@selector(detailsPage:headerImageViewWasSelected:)])
        [self.delegate detailsPage:self headerImageViewWasSelected:self.backgroundImageView];
}

#pragma mark - ScrollView Methods

- (void)scrollViewDidScrollWithOffset:(CGFloat)scrollOffset {
    
    CGPoint scrollViewDragPoint = self.tableView.contentOffset;
 
    // navBar变色
    [self animateNavigationBar:scrollOffset draggingPoint:scrollViewDragPoint];
    // 2. 背景图片变色
    [self scrollViewDidScrollWithBackground:scrollOffset];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(detailsPage:scrollViewWithScrollOffset:)]){
        [self.delegate detailsPage:self scrollViewWithScrollOffset:scrollOffset];
    }
    
}

- (void)animateNavigationBar:(CGFloat)scrollOffset draggingPoint:(CGPoint)scrollViewDragPoint {
    CGFloat alpha_margin = scrollOffset / self.navbarViewFadingOffset;
    if (alpha_margin > 1){
        alpha_margin = 1;
    }
    _navbarView.alpha = alpha_margin;
}

-(void)scrollViewDidScrollWithBackground:(CGFloat)pointY{
    [self.backgroundImageView setBlurLevel:(pointY / self.navbarViewFadingOffset)];
}


-(void)maskHeaderImage:(UIImage *)image{
    [self.backgroundImageView mdInflateAnimatedFromPoint:CGPointMake(kScreenBounds.size.width / 2., self.backgroundImageView.size_height / 2.) backgroundColor:[UIColor colorWithCustomerName:@"白"] duration:.3f completion:^{
        self.backgroundImageView.image = image;
    }];
}
@end
