//
//  PDPandaHomeTopLabel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaHomeTopLabel.h"
#import <pop/POP.h>

@interface PDPandaHomeTopLabel()
@property (nonatomic,strong)UILabel *label1;                /**< */
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *label2;
@property (nonatomic,strong)UILabel *goldLabel;
@property (nonatomic,strong)UILabel *label3;
@property (nonatomic,assign)NSInteger time;
@property (nonatomic,assign)NSInteger money;

@end

@implementation PDPandaHomeTopLabel

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建label
    self.label1 = [self mainLabel];
    self.label1.text = @"最近";
    [self addSubview:self.label1];
    
    // 2. 创建时间
    self.timeLabel = [self mainLabel];
    self.timeLabel.text = @"0";
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.timeLabel];
    
    // 3. 创建天
    self.label2 = [self mainLabel];
    self.label2.text = @"天：";
    [self addSubview:self.label2];
    
    // 3. 创建金币
    self.goldLabel = [self mainLabel];
    self.goldLabel.text = @"0";
    self.goldLabel.textAlignment = NSTextAlignmentCenter;
    self.goldLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.goldLabel];
    
    // 4. 创建金币
    self.label3 = [self mainLabel];
    self.label3.text = @"金币";
    [self addSubview:self.label3];

    self.size_height = [NSString contentofHeightWithFont:self.label1.font];
    
    // 5. 修改size
    [self uploadSize];
}

-(void)uploadSize {
    // 1.
    CGSize label1Size = [self.label1.text sizeWithCalcFont:self.label1.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.label1.font])];
    
    // 2.
    CGSize label2Size = [self.label2.text sizeWithCalcFont:self.label2.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.label2.font])];
    
    // 3.
    CGSize label3Size = [self.label3.text sizeWithCalcFont:self.label3.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.label3.font])];
    
    // 4. time
    CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.timeLabel.font])];
    
    // 5. gold
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.goldLabel.font])];
    
    self.label1.frame = CGRectMake(LCFloat(LCFloat(11)), 0, label1Size.width, [NSString contentofHeightWithFont:self.label1.font]);
    self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.label1.frame) + LCFloat(3), self.label1.orgin_y, timeSize.width, [NSString contentofHeightWithFont:self.timeLabel.font]);
    self.label2.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame) + LCFloat(3), self.label1.orgin_y, label2Size.width, [NSString contentofHeightWithFont:self.label2.font]);
    self.goldLabel.frame = CGRectMake(CGRectGetMaxX(self.label2.frame) + LCFloat(3), self.label1.orgin_y, goldSize.width, [NSString contentofHeightWithFont:self.goldLabel.font]);
    self.label3.frame = CGRectMake(CGRectGetMaxX(self.goldLabel.frame) + LCFloat(3), self.label1.orgin_y, label3Size.width, [NSString contentofHeightWithFont:self.label3.font]);
}

-(void)animationWithTime:(NSInteger)time moneyCount:(NSInteger)money{
    _time = time;
    _money = money;
    self.timeLabel.text = [NSString stringWithFormat:@"%li",time];
    self.goldLabel.text = [NSString stringWithFormat:@"%li",money];
//    [self animationWithInfoWithLabel:self.timeLabel];
//    [self animationWithInfoWithLabel:self.goldLabel];
    [self uploadSize];
}

-(void)animationWithInfoWithLabel:(UILabel *)label{
    POPBasicAnimation *anim = [POPBasicAnimation animation];
    anim.duration = 1;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    POPAnimatableProperty * prop = [POPAnimatableProperty propertyWithName:@"count" initializer:^(POPMutableAnimatableProperty *prop){
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [[obj description] floatValue];
        };
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
            [obj setText:[NSString stringWithFormat:@"%li",(long)values[0]]];
        };
        // dynamics threshold
        prop.threshold = 1;
    }];
    
    anim.property = prop;
    
    anim.fromValue = @(0);

    if (label == self.timeLabel){
        anim.toValue = @(self.time);
    } else if (label == self.goldLabel){
        anim.toValue = @(self.money);
    }
    
    [label pop_addAnimation:anim forKey:@"counting"];
}


-(UILabel *)mainLabel{
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithCustomerSizeName:@"提示"];
    label.textColor = [UIColor whiteColor];
    return label;
}

@end

