//
//  PDPandaHomeTopLabel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 最近10天，128金币
#import <UIKit/UIKit.h>

@interface PDPandaHomeTopLabel : UIView

-(void)animationWithTime:(NSInteger)time moneyCount:(NSInteger)money;

@end
