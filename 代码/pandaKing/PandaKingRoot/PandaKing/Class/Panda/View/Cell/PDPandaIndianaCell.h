//
//  PDPandaIndianaCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 夺宝信息
#import <UIKit/UIKit.h>
#import "PDPandaTreasuresSingleModel.h"

@protocol PDPandaIndianaCellDelegate <NSObject>

-(void)pandaTreasuresDidselectedWithModel:(PDPandaTreasuresSingleModel *)treasureSingleModel;
-(void)timeOutToNotificationRootInfoManager;
@end

@interface PDPandaIndianaCell : UITableViewCell

@property (nonatomic,strong)NSArray *transferIndianaArr;            /**< 传递进来的数组*/
@property (nonatomic,assign)CGFloat transferCellHeight;

@property (nonatomic,weak)id<PDPandaIndianaCellDelegate> delegate;


+(CGFloat)calculationCellHeight;

@end
