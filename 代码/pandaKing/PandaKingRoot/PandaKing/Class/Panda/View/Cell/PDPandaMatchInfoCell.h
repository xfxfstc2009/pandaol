//
//  PDPandaMatchInfoCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 最近比赛
#import <UIKit/UIKit.h>
#import "PDPandaMatchSingleModel.h"

@interface PDPandaMatchInfoCell : UITableViewCell

@property (nonatomic,strong)PDPandaMatchSingleModel *transferMatchModel;            /**< 传入比赛Model*/
@property (nonatomic,assign)CGFloat transferCellHeight;                             /**< 传入的高度*/


+(CGFloat)calculationCellHeight;
@end
