//
//  PDPandaLotteryTableViewCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLoteryGameRootSubSingleModel.h"


@interface PDPandaLotteryTableViewCell : UITableViewCell

@property (nonatomic,strong)PDLoteryGameRootSubSingleModel *transferGameRootSubSingleModel;
@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;

@end
