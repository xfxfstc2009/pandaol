//
//  PDPandaIndianaSingleCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPandaTreasuresSingleModel.h"                    // 单个model

@interface PDPandaIndianaSingleCell : UICollectionViewCell

@property (nonatomic,assign)CGSize transferCellSize;
@property (nonatomic,strong)PDPandaTreasuresSingleModel *transferSingleModel;              /**< 传入的单个Model*/

+(CGSize )calculationCellSize;

-(void)timeOutToNotificationWithRoot:(void(^)())block;
@end
