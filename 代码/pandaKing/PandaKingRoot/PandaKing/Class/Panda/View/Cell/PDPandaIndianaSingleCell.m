//
//  PDPandaIndianaSingleCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaIndianaSingleCell.h"

static char timeOutKey;
@interface PDPandaIndianaSingleCell()
@property (nonatomic,strong)UIView *bgView;                             /**< 背景白色*/
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)PDImageView *productImgView;                /**< 商品图片*/
@property (nonatomic,strong)UILabel *productNameLabel;                  /**< 商品名称*/
@property (nonatomic,strong)UILabel *productTimeLabel;          /**< 商品时间label*/
@property (nonatomic,strong)UILabel *joinCountLabel;                    /**< 参与人数*/

@property (nonatomic,strong)NSTimer *timer;                             /**< timer*/

@end

@implementation PDPandaIndianaSingleCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
        self.backgroundColor = [UIColor whiteColor];
//        self.clipsToBounds = YES;
//        self.layer.shadowColor = [[UIColor colorWithCustomerName:@"灰"] CGColor];
//        self.layer.shadowOpacity = 1.0;
//        self.layer.shadowOffset = CGSizeMake(1.0f, 1.0f);
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建背景view
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    
    self.convertView = [[UIView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.clipsToBounds = YES;
    [self addSubview:self.convertView];
    
    // 1. 创建图片
    self.productImgView = [[PDImageView alloc]init];
    self.productImgView.backgroundColor = [UIColor clearColor];
    [self.convertView addSubview:self.productImgView];
    
    // 2. 创建名称
    self.productNameLabel = [[UILabel alloc]init];
    self.productNameLabel.backgroundColor = [UIColor clearColor];
    self.productNameLabel.font = [[UIFont fontWithCustomerSizeName:@"小正文"] boldFont];
    self.productNameLabel.textColor = c3;
    self.productNameLabel.numberOfLines = 0;
    [self.bgView addSubview:self.productNameLabel];
    
    // 3. 创建时间
    self.productTimeLabel = [[UILabel alloc]init];
    self.productTimeLabel.backgroundColor = [UIColor clearColor];
    self.productTimeLabel.font = [[UIFont fontWithCustomerSizeName:@"标题"]boldFont];
    self.productTimeLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    self.productTimeLabel.numberOfLines = 1;
    self.productTimeLabel.adjustsFontSizeToFitWidth = YES;
    self.productTimeLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.productTimeLabel];
    
    // 4. 创建参加人数
    self.joinCountLabel = [[UILabel alloc]init];
    self.joinCountLabel.backgroundColor = [UIColor clearColor];
    self.joinCountLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.joinCountLabel.textAlignment = NSTextAlignmentCenter;
    self.joinCountLabel.textColor = c15;
    self.joinCountLabel.numberOfLines = 1;
    [self.bgView addSubview:self.joinCountLabel];
}

-(void)setTransferCellSize:(CGSize)transferCellSize{
    _transferCellSize = transferCellSize;
}

-(void)setTransferSingleModel:(PDPandaTreasuresSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    // 1. 背景view
    self.bgView.frame = CGRectMake(0, 0, self.transferCellSize.width, self.transferCellSize.width);
 
    // 1. 图片
    self.convertView.frame = CGRectMake(LCFloat(2), LCFloat(2), self.transferCellSize.width - 2 * LCFloat(2), self.transferCellSize.width - 2 * LCFloat(2));
    self.convertView.backgroundColor = [UIColor clearColor];
    
    __weak typeof(self)weakSelf = self;
    [self.productImgView uploadImageWithURL:transferSingleModel.image placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf setTransferSingleAsset:image imgView:strongSelf.productImgView convertView:strongSelf.convertView];
    }];
    
    // 2. 创建商品名称
    self.productNameLabel.text = transferSingleModel.name;
    CGSize productNameSize = [self.productNameLabel.text sizeWithCalcFont:self.productNameLabel.font constrainedToSize:CGSizeMake(self.transferCellSize.width - 2 * LCFloat(5), CGFLOAT_MAX)];
    
    if (productNameSize.height >= [NSString contentofHeightWithFont:self.productNameLabel.font] * 2){           // 【2行】
        self.productNameLabel.numberOfLines = 2;
        self.productNameLabel.size_height = 2 * [NSString contentofHeightWithFont:self.productNameLabel.font];
        self.productNameLabel.frame = CGRectMake(LCFloat(5), CGRectGetMaxY(self.convertView.frame) + LCFloat(5), self.transferCellSize.width - 2 * LCFloat(5), productNameSize.height);
    } else {
        self.productNameLabel.numberOfLines = 1;
        self.productNameLabel.textAlignment = NSTextAlignmentCenter;
        self.productNameLabel.size_height = [NSString contentofHeightWithFont:self.productNameLabel.font];
        self.productNameLabel.frame = CGRectMake(LCFloat(5), CGRectGetMaxY(self.convertView.frame) + LCFloat(5), self.transferCellSize.width - 2 * LCFloat(5), productNameSize.height);
    }
    
    // 3. 创建商品倒计时时间
    self.productTimeLabel.text = [NSDate getTimeDistance:transferSingleModel.drawTime / 1000.];
    self.productTimeLabel.frame = CGRectMake(0, CGRectGetMaxY(self.convertView.frame) + LCFloat(5) + 2 * [NSString contentofHeightWithFont:self.productNameLabel.font] + LCFloat(7), self.transferCellSize.width, [NSString contentofHeightWithFont:self.productTimeLabel.font]);

    // 4.参加人数
    self.joinCountLabel.text = [NSString stringWithFormat:@"当前已有%li人参与",(long)self.transferSingleModel.joinedCount];
    self.joinCountLabel.frame = CGRectMake(0, CGRectGetMaxY(self.productTimeLabel.frame) + LCFloat(5), self.size_width, [NSString contentofHeightWithFont:self.joinCountLabel.font]);

    // 5. 更新bgView高度
    self.bgView.size_height = CGRectGetMaxY(self.joinCountLabel.frame) + LCFloat(5);

    // 5. 创建时间
    [self createTimer];
}

+(CGSize )calculationCellSize{
    CGFloat cellHeight = 0;
    // 1.margin_y
    cellHeight += LCFloat(2);
    // 2. 图片
    cellHeight += (kScreenBounds.size.width - LCFloat(35)) / 2 - 2 * LCFloat(2);
    // 3. 商品名字
    cellHeight += LCFloat(5);
    cellHeight += 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    // 3. 时间
    cellHeight += LCFloat(7);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"标题"]];
    // 4. 参与
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(5);
//    cellHeight += 2 * 10;
    
    return CGSizeMake((kScreenBounds.size.width - LCFloat(35)) / 2., cellHeight);
}

-(void)createTimer{
    if (!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:self.timer forMode:NSRunLoopCommonModes];
    }
}

-(void)daojishiManager{
    if (self.transferSingleModel){
        NSString *timeStr = [NSDate getTimeDistance:self.transferSingleModel.drawTime / 1000.];
        if ([timeStr isEqualToString:@"已揭晓"]){
            if (self.timer){
                [self.timer invalidate];
                self.timer = nil;
            }
            
            void(^block)() = objc_getAssociatedObject(self, &timeOutKey);
            if (block) {
                block();
            }
        } else {
            self.productTimeLabel.text = [NSDate getTimeDistance:self.transferSingleModel.drawTime / 1000.];
        }
    }
}

-(void)timeOutToNotificationWithRoot:(void(^)())block{
    objc_setAssociatedObject(self, &timeOutKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setTransferSingleAsset:(UIImage *)transferSingleAsset imgView:(PDImageView*)imgView convertView:(UIView *)convertView{
    // 1. 获取到图片
    if (!transferSingleAsset){
        return;
    }
    imgView.image = nil;
    CGFloat img_width = transferSingleAsset.size.width;
    CGFloat img_height = transferSingleAsset.size.height;
    
    imgView.backgroundColor = UURandomColor;
    convertView.backgroundColor = UURandomColor;
    
    if ( img_height > img_width){        // 宽度大于高度
        CGFloat img_New_height =  (convertView.size_width / img_width) * img_height;
        CGFloat margin =  ((convertView.size_width / img_width) * img_height - convertView.size_height) / 2.;
        imgView.frame = CGRectMake(0,-margin,convertView.size_width,img_New_height);
    } else {
        CGFloat img_New_width = (convertView.size_width / img_height) * img_width;
        CGFloat margin = (convertView.size_width - img_New_width) / 2.;
        imgView.frame = CGRectMake(margin, 0, img_New_width, convertView.size_height);
    }
    imgView.image = transferSingleAsset;
}
@end
