//
//  PDPandaRecordSingleView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaRecordSingleView.h"
#import <pop/POP.h>

@interface PDPandaRecordSingleView()
@property (nonatomic,strong)UILabel *dymicLabel;                /**< 动态label*/
@property (nonatomic,strong)UILabel *fixedLabel;                /**< 固定label*/

@end

@implementation PDPandaRecordSingleView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建label
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.textColor = [UIColor hexChangeFloat:@"878787"];
    self.dymicLabel.font = [UIFont systemFontOfCustomeSize:9.];
    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
    self.dymicLabel.adjustsFontSizeToFitWidth = YES;
    self.dymicLabel.frame = CGRectMake(0, 0, self.size_width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    [self addSubview:self.dymicLabel];
    
    // 2. 创建label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont systemFontOfCustomeSize:9.];
    self.fixedLabel.textColor = [UIColor hexChangeFloat:@"878787"];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.fixedLabel.adjustsFontSizeToFitWidth = YES;
    self.fixedLabel.frame = CGRectMake(0, CGRectGetMaxY(self.dymicLabel.frame), self.size_width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    [self addSubview:self.fixedLabel];
    
    // 3. 重新修改frame
    CGFloat dymicHeight = [NSString contentofHeightWithFont:self.dymicLabel.font];
    CGFloat fixedHeight = [NSString contentofHeightWithFont:self.fixedLabel.font];
    CGFloat margin = (self.size_height - dymicHeight - fixedHeight) / 3.;
    self.dymicLabel.orgin_y = margin;
    self.fixedLabel.orgin_y = CGRectGetMaxY(self.dymicLabel.frame) + margin;
    
}

-(void)setTransferRecordSingleModdel:(PDRecordSingleModel *)transferRecordSingleModdel{
    _transferRecordSingleModdel = transferRecordSingleModdel;
    if (transferRecordSingleModdel.duanweiStr.length){
        self.dymicLabel.text = transferRecordSingleModdel.duanweiStr;
    } else {
        [self animationWithInfoWithLabel:self.dymicLabel];
    }
    self.fixedLabel.text = transferRecordSingleModdel.fixedStr;
}

-(void)animationWithInfoWithLabel:(UILabel *)label{
    POPBasicAnimation *anim = [POPBasicAnimation animation];
    anim.duration = 1;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    POPAnimatableProperty * prop = [POPAnimatableProperty propertyWithName:@"count" initializer:^(POPMutableAnimatableProperty *prop){
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [[obj description] floatValue];
        };
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
            if ([self.transferRecordSingleModdel.fixedStr isEqualToString:@"胜率"]){
                [obj setText:[NSString stringWithFormat:@"%.1f%%",(float)values[0]]];
            } else if ([self.transferRecordSingleModdel.fixedStr isEqualToString:@"场次"]){
                [obj setText:[NSString stringWithFormat:@"%li",(long)values[0]]];
            } else if ([self.transferRecordSingleModdel.fixedStr isEqualToString:@"战斗力"]){
                [obj setText:[NSString stringWithFormat:@"%li",(long)values[0]]];
            } else if ([self.transferRecordSingleModdel.fixedStr isEqualToString:@"KDA"]){
                [obj setText:[NSString stringWithFormat:@"%.1f",values[0]]];
            } else if ([self.transferRecordSingleModdel.fixedStr isEqualToString:@"总场次"]){
                [obj setText:[NSString stringWithFormat:@"%li",(long)values[0]]];
            }
        };
        // dynamics threshold
        prop.threshold = 1;
    }];
    
    anim.property = prop;
    
    anim.fromValue = @(0);
    if ([self.transferRecordSingleModdel.fixedStr isEqualToString:@"胜率"]){
        anim.toValue = @(self.transferRecordSingleModdel.dymicStr * 100.);
    } else if ([self.transferRecordSingleModdel.fixedStr isEqualToString:@"场次"]){
        anim.toValue = @(self.transferRecordSingleModdel.dymicStr);
    } else if ([self.transferRecordSingleModdel.fixedStr isEqualToString:@"战斗力"]){
        anim.toValue = @(self.transferRecordSingleModdel.dymicStr);
    } else if ([self.transferRecordSingleModdel.fixedStr isEqualToString:@"KDA"]){
        anim.toValue = @(self.transferRecordSingleModdel.dymicStr);
    } else if ([self.transferRecordSingleModdel.fixedStr isEqualToString:@"总场次"]){
        anim.toValue = @(self.transferRecordSingleModdel.dymicStr);
    }
                                                                                  
    [label pop_addAnimation:anim forKey:@"counting"];
}


@end
