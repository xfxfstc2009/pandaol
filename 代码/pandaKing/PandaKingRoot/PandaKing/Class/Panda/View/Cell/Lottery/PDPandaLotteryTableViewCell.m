//
//  PDPandaLotteryTableViewCell.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/16.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaLotteryTableViewCell.h"
#import "PDLotteryGameMainHeaderView.h"

@interface PDPandaLotteryTableViewCell()
@property (nonatomic,strong)PDLotteryGameMainHeaderView *headerView;
@end

@implementation PDPandaLotteryTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.headerView = [[PDLotteryGameMainHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [PDLotteryGameMainHeaderView calculationCellHeightWithType:LotterGameHeaderTypeNormal])];
    self.headerView.transferType = LotterGameHeaderTypeNormal;
    self.headerView.transferLotteryCellType = LotteryCellTypeHome;
    self.headerView.userInteractionEnabled = NO;
    [self addSubview:self.headerView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferGameRootSubSingleModel:(PDLoteryGameRootSubSingleModel *)transferGameRootSubSingleModel{
    _transferGameRootSubSingleModel = transferGameRootSubSingleModel;

    self.headerView.transferGameRootSubSingleModel = self.transferGameRootSubSingleModel;
}


+(CGFloat)calculationCellHeight{
    return [PDLotteryGameMainHeaderView calculationCellHeightWithType:LotterGameHeaderTypeNormal] - LCFloat(8);
}

@end
