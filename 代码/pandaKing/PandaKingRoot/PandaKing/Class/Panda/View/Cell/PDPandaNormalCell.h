//
//  PDPandaNormalCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDPandaNormalCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,copy)NSString *transferInfo;

// 主播猜使用
@property (nonatomic,assign)BOOL hasNotMore;

+(CGFloat)calculationCellHeight;

@end
