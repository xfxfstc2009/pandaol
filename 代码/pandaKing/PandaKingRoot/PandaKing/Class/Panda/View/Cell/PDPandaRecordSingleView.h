//
//  PDPandaRecordSingleView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDRecordSingleModel.h"
@interface PDPandaRecordSingleView : UIView

@property (nonatomic,strong)PDRecordSingleModel *transferRecordSingleModdel;

@end
