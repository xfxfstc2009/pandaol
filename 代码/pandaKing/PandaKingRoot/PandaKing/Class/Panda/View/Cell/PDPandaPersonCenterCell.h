//
//  PDPandaPersonCenterCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 个人中心
#import <UIKit/UIKit.h>
#import "PDPandaHomeMemberLolGameUserModel.h"
#import "PDPandaHomeRootModel.h"

#define USER_Nick_Height LCFloat(15)

@interface PDPandaPersonCenterCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDPandaHomeRootModel *transferHomeRootModel;                /**< 传入的战绩信息*/
@property (nonatomic,strong)PDAccountLoLInfoSingleModel *transferLolinfo;               /**< 传入lol战绩信息*/


@property (nonatomic,strong)PDImageView *avatarImageView;
@property (nonatomic,strong)PDImageView *bgImageView;               /**< 背景图片*/
+(CGFloat)calculationCellHeight;

// 修改认证状态
-(void)changeRenzhengStatus:(NSString *)status;

@end
