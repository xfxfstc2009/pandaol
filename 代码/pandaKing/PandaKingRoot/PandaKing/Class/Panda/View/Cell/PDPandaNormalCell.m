//
//  PDPandaNormalCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaNormalCell.h"

@interface PDPandaNormalCell()
@property (nonatomic,strong)UIView *fangView;                       /**< 方块view*/
@property (nonatomic,strong)UILabel *fixedLabel;                    /**< 固定文字*/
@property (nonatomic,strong)PDImageView *arrowImageView;            /**< 箭头符号*/
@property (nonatomic,strong)UILabel *dymicLabel;                    /**< 动态label*/


@end

@implementation PDPandaNormalCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createVierw
-(void)createView{
    // 0. 创建左侧的方块
    self.fangView = [[UIView alloc]init];
    self.fangView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.fangView];
    
    // 1. 创建label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    [self addSubview:self.fixedLabel];
    
    // 2. 创建arrowIage
    self.arrowImageView = [[PDImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.arrowImageView];
    
    // 3. 创建dymicLabel
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.text = @"查看更多";
    self.dymicLabel.textAlignment = NSTextAlignmentRight;
    self.dymicLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.dymicLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferInfo:(NSString *)transferInfo{
    self.fangView.frame = CGRectMake(0, LCFloat(9), LCFloat(6), self.transferCellHeight - 2 * LCFloat(9));
    
    // fixedLabel
    self.fixedLabel.text = transferInfo;
    CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.fangView.frame) + LCFloat(22), 0,fixedSize.width , self.transferCellHeight);
    
    // 2. arrow
    self.arrowImageView.image = [UIImage imageNamed:@"icon_home_arrow"];
    self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(12) - LCFloat(12), (self.transferCellHeight - LCFloat(12)) / 2., LCFloat(12), LCFloat(12));
    
    // 3. 查看更多
    CGSize chakanGengduoSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLabel.font])];
    self.dymicLabel.frame = CGRectMake(self.arrowImageView.orgin_x - LCFloat(5) - chakanGengduoSize.width, 0, chakanGengduoSize.width, self.transferCellHeight);
    
    
    
    if ([transferInfo isEqualToString:@"最近比赛"]){
        self.arrowImageView.hidden = YES;
        self.dymicLabel.hidden = YES;
    } else {
        self.arrowImageView.hidden = NO;
        self.dymicLabel.hidden = NO;
    }
    
    if (self.hasNotMore == YES){
        self.dymicLabel.hidden = YES;
        self.arrowImageView.hidden = YES;
    } else {
        self.dymicLabel.hidden = NO;
        self.arrowImageView.hidden = NO;
    }
}

-(void)setHasNotMore:(BOOL)hasNotMore{
    _hasNotMore = hasNotMore;
}


+(CGFloat)calculationCellHeight{
    CGFloat cellheight = 0;
    cellheight += LCFloat(17);
    cellheight += LCFloat(17);
    cellheight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    return cellheight;
}

@end
