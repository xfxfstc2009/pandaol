//
//  PDPandaPersonCenterCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaPersonCenterCell.h"
#import "PDRecordSingleModel.h"
#import "PDPandaRecordSingleView.h"

@interface PDPandaPersonCenterCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UIView *alphaView;
@property (nonatomic,strong)PDImageView *bgImgView;

@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *areaLabel;
@property (nonatomic,strong)PDImageView *activityImgView;

// 战绩
@property (nonatomic,strong)UIView *recordView;
@property (nonatomic,assign)BOOL isLoop;

@end

@implementation PDPandaPersonCenterCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.alphaView = [[UIView alloc]init];
    self.alphaView.backgroundColor = [UIColor clearColor];
    self.alphaView.frame = CGRectMake(LCFloat(10), 0, kScreenBounds.size.width - 2 * LCFloat(10), LCFloat(127));
//    self.alphaView.layer.cornerRadius = LCFloat(6);
//    self.alphaView.layer.shadowColor = [[UIColor colorWithCustomerName:@"黑"] CGColor];
//    self.alphaView.layer.shadowOpacity = 5.0;
//    self.alphaView.layer.shadowOffset = CGSizeMake(3.5f, 3.5f);
    [self addSubview:self.alphaView];
    
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.clipsToBounds = YES;
//    self.bgView.layer.cornerRadius = LCFloat(6);
    self.bgView.frame = self.alphaView.bounds;
    [self.alphaView addSubview:self.bgView];
    
    // 1. 创建图片
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.frame = self.bgView.bounds;
    [self.bgView addSubview:self.bgImgView];
    
    PDImageView *imageView = [[PDImageView alloc]init];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.frame = self.alphaView.bounds;
//    imageView.image = [UIImage imageNamed:@"img_panda_personAlpha"];
    [self.bgView addSubview:imageView];
    
    // 1. 创建头像
    self.avatarImageView = [[PDImageView alloc]init];
    self.avatarImageView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.avatarImageView];
    
    // 2. 创建name
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = c11;
    self.nameLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"]boldFont];
    [self.bgView addSubview:self.nameLabel];
    
    // 3. 创建dymicLabel
    self.areaLabel = [[UILabel alloc]init];
    self.areaLabel.backgroundColor = [UIColor clearColor];
    self.areaLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.areaLabel.textColor = c10;
    [self.bgView addSubview:self.areaLabel];
    
    // 4. 创建是否激活
    self.activityImgView = [[PDImageView alloc]init];
    self.activityImgView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.activityImgView];
    
    self.recordView = [[UIView alloc]init];
    self.recordView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.recordView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferLolinfo:(PDAccountLoLInfoSingleModel *)transferLolinfo{
    _transferLolinfo = transferLolinfo;
    
    if (transferLolinfo){
        // 1. 头像
        self.avatarImageView.frame = CGRectMake(LCFloat(10), LCFloat(10), LCFloat(34), LCFloat(34));
        self.avatarImageView.layer.cornerRadius = self.avatarImageView.size_height / 2.;
        self.avatarImageView.clipsToBounds = YES;
        self.avatarImageView.backgroundColor = [UIColor clearColor];
        [self.avatarImageView uploadImageWithURL:transferLolinfo.gameUserAvatar placeholder:nil callback:NULL];
        
        // 2. 角色名称
        self.nameLabel.text = transferLolinfo.gameUserName;
        self.nameLabel.font = [UIFont systemFontOfCustomeSize:13.];

        
        CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.nameLabel.font])];
        self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImageView.frame) + LCFloat(12), USER_Nick_Height, nameSize.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
        
        
        self.activityImgView.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + LCFloat(11), 0, LCFloat(9), LCFloat(11));
        self.activityImgView.center_y = self.nameLabel.center_y;
        [self changeRenzhengStatus:transferLolinfo.state];
        
        //
        // 3. 创建地址
        self.areaLabel.text = [NSString stringWithFormat:@"%@",transferLolinfo.gameServerName];
        self.areaLabel.font = [UIFont systemFontOfCustomeSize:9.];
        self.areaLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(9), self.nameLabel.size_width, [NSString contentofHeightWithFont:self.areaLabel.font]);
        
        // 4. 战绩信息
        CGFloat width = kScreenBounds.size.width / 4. * 3 - CGRectGetMaxX(self.avatarImageView.frame) - LCFloat(20);
        CGFloat heihgt = self.transferCellHeight - CGRectGetMaxY(self.avatarImageView.frame) - 2 * LCFloat(7);
        
        self.recordView.frame = CGRectMake(CGRectGetMaxX(self.avatarImageView.frame), CGRectGetMaxY(self.avatarImageView.frame) + LCFloat(7), width, heihgt);
        
        if (self.recordView){
            [self.recordView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            for (int i = 0 ; i < 4;i++){
                CGFloat origin_X = i * width / 4.;
                CGFloat origin_y = 0;
                CGFloat size_width = width / 4.;
                CGFloat size_height = heihgt;
                
                PDRecordSingleModel *recordSingleModel = [[PDRecordSingleModel alloc]init];
                if (i == 0 ){
                    recordSingleModel.fixedStr = @"胜率";
                    recordSingleModel.dymicStr = transferLolinfo.winRate;
                } else if (i == 1){
                    recordSingleModel.fixedStr = @"KDA";
                    recordSingleModel.dymicStr = transferLolinfo.kda;
                } else if (i == 2){
                    recordSingleModel.fixedStr = @"段位";
                    recordSingleModel.duanweiStr = transferLolinfo.grade;
                } else if (i == 3){
                    recordSingleModel.fixedStr = @"总场次";
                    recordSingleModel.dymicStr = transferLolinfo.gameCount;
                }
                
                PDPandaRecordSingleView *pandaRecordSingleView = [[PDPandaRecordSingleView alloc]initWithFrame:CGRectMake(origin_X, origin_y, size_width, size_height)];
                pandaRecordSingleView.transferRecordSingleModdel = recordSingleModel;
                pandaRecordSingleView.backgroundColor = [UIColor clearColor];
                [self.recordView addSubview:pandaRecordSingleView];
            }
        }
    } else {
        self.avatarImageView.image = [UIImage imageNamed:@"icon_slider_add_role"];
        self.avatarImageView.frame = CGRectMake(LCFloat(11), (self.transferCellHeight - LCFloat(30)) / 2., LCFloat(30), LCFloat(30));
        
        self.nameLabel.text = @"绑定召唤师";
        CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.nameLabel.font])];
        self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImageView.frame) + LCFloat(11), 0, nameSize.width, self.transferCellHeight);
    }
}






-(void)updateWithUSerInfo:(PDPandaHomeMemberLolGameUserModel *)transferLoLGameUserInfo{
    // 0. 背景图123
    if (transferLoLGameUserInfo.isAnimation){
        self.alphaView.orgin_y = 0;
    } else {
        self.alphaView.orgin_y = self.transferCellHeight;
    }
    
    // 1. 头像
    self.avatarImageView.frame = CGRectMake(LCFloat(10), LCFloat(10), LCFloat(48), LCFloat(48));
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.size_height / 2.;
    self.avatarImageView.clipsToBounds = YES;
    self.avatarImageView.backgroundColor = [UIColor clearColor];
    [self.avatarImageView uploadImageWithURL:self.transferHomeRootModel.gameUserAvatar placeholder:nil callback:NULL];
    
    // 2. 角色名称
    self.nameLabel.text = transferLoLGameUserInfo.lolGameUser.roleName;
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImageView.frame) + LCFloat(12), USER_Nick_Height, kScreenBounds.size.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    
    // 3. 创建地址
    NSString *xinxi = @"";
    if ([transferLoLGameUserInfo.lolGameUser.telecomLine isEqualToString:@"CT"]){
        xinxi = @"电信";
    } else if ([transferLoLGameUserInfo.lolGameUser.telecomLine isEqualToString:@"CM"]){
        xinxi = @"移动";
    } else {
        xinxi = @"其他";
    }
    
    self.areaLabel.text = [NSString stringWithFormat:@"%@(%@)",transferLoLGameUserInfo.lolGameUser.serverName,xinxi];
    self.areaLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(9), self.nameLabel.size_width, [NSString contentofHeightWithFont:self.areaLabel.font]);
    
    if (!transferLoLGameUserInfo.isAnimation){
        [UIView animateWithDuration:.5f animations:^{
            self.alphaView.orgin_y = 0;
        } completion:^(BOOL finished) {
            transferLoLGameUserInfo.isAnimation = YES;
        }];
    }
}

-(void)setTransferHomeRootModel:(PDPandaHomeRootModel *)transferHomeRootModel{
    _transferHomeRootModel = transferHomeRootModel;
    
    [self updateWithUSerInfo:transferHomeRootModel.memberLOLGameUser];
    
    // 修改背景图片
    __weak typeof(self)weakSelf = self;
    [self.bgImgView uploadAliYunHomeCardImageWithURL:transferHomeRootModel.record.backgroundImg placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(!image){
            strongSelf.bgImgView.image = [UIImage imageNamed:@"img_panda_home_star"];
        } else {
//            [strongSelf autImgSizeWithImage:image];
        }
        [strongSelf bgImgAnimation];
    }];
    
    // 高度
    CGFloat heihgt = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]] + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]] + 3 * LCFloat(3);
    CGFloat width = self.alphaView.size_width * .6f;
    
    // 1. 战绩信息
    self.recordView.frame = CGRectMake(LCFloat(13), self.bgView.size_height - LCFloat(13) - heihgt, width, heihgt);
    if (self.recordView && transferHomeRootModel){
        [self.recordView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        for (int i = 0 ; i < 4;i++){
            CGFloat origin_X = i * width / 4.;
            CGFloat origin_y = 0;
            CGFloat size_width = width / 4.;
            CGFloat size_height = heihgt;
            
            PDRecordSingleModel *recordSingleModel = [[PDRecordSingleModel alloc]init];
            if (i == 0 ){
                recordSingleModel.fixedStr = @"胜率";
                recordSingleModel.dymicStr = transferHomeRootModel.stats.winRate;
            } else if (i == 1){
                recordSingleModel.fixedStr = @"KDA";
                recordSingleModel.dymicStr = transferHomeRootModel.stats.kda;
            } else if (i == 2){
                recordSingleModel.fixedStr = @"段位";
                if ((transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 255) && (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 255)){
                    recordSingleModel.duanweiStr = @"未定位";
                } else {
                    NSString *level = @"";
                    if (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 1){
                        level = @"I";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 2){
                        level = @"II";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 3){
                        level = @"III";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 4){
                        level = @"IV";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 5){
                        level = @"V";
                    }
                    
                    NSString *duanwei1 = @"";
                    if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 0){
                        duanwei1 = @"最强王者";
                        level = @"";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 1){
                        duanwei1 = @"钻石";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 2){
                        duanwei1 = @"白金";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 3){
                        duanwei1 = @"黄金";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 4){
                        duanwei1 = @"白银";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 5){
                        duanwei1 = @"青铜";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 6){
                        duanwei1 = @"超凡大师";
                    }
                    
                    recordSingleModel.duanweiStr = [NSString stringWithFormat:@"%@%@",duanwei1,level];
                }
            } else if (i == 3){
                recordSingleModel.fixedStr = @"总场次";
                recordSingleModel.dymicStr = transferHomeRootModel.stats.gameCount;
            }
            
            PDPandaRecordSingleView *pandaRecordSingleView = [[PDPandaRecordSingleView alloc]initWithFrame:CGRectMake(origin_X, origin_y, size_width, size_height)];
            pandaRecordSingleView.transferRecordSingleModdel = recordSingleModel;
            pandaRecordSingleView.backgroundColor = [UIColor clearColor];
            [self.recordView addSubview:pandaRecordSingleView];
        }
    }
}



+(CGFloat)calculationCellHeight{
    return LCFloat(92);
}

-(void)bgImgAnimation{
//    [UIView animateWithDuration:10 animations:^{
//        if (!self.isLoop){
//            self.bgImgView.transform = CGAffineTransformMakeScale(1.1f, 1.1f);
//        } else {
//            self.bgImgView.transform = CGAffineTransformMakeScale(1.0, 1.0);
//        }
//    } completion:^(BOOL finished) {
//        self.isLoop = !self.isLoop;
//        [self bgImgAnimation];
//    }];
}


#pragma mark - 修改认证状态
-(void)changeRenzhengStatus:(NSString *)status{
    if ([status isEqualToString:@"activating"]){       // 激活中
        self.activityImgView.image = [UIImage imageNamed:@"icon_slider_role_activity_nor"];
    } else if ([status isEqualToString:@"bind"]){      // 未绑定
        self.activityImgView.image = [UIImage imageNamed:@"icon_slider_role_activity_nor"];
    } else if ([status isEqualToString:@"activated"]){ // 已认证
        self.activityImgView.image = [UIImage imageNamed:@"icon_slider_role_activity_hlt"];
    }
}



@end
