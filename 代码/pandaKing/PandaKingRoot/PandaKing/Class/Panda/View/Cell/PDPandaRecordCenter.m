//
//  PDPandaRecordCenter.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaRecordCenter.h"
#import "PDRecordSingleModel.h"
#import "PDPandaRecordSingleView.h"

@interface PDPandaRecordCenter()
@property (nonatomic,strong)UIView *recordView;

@end

@implementation PDPandaRecordCenter

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
 
}


#pragma makr - SET
-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferHomeRootModel:(PDPandaHomeRootModel *)transferHomeRootModel{
    _transferHomeRootModel = transferHomeRootModel;

    // 1. 战绩信息
    self.recordView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.transferCellHeight);
    if (self.recordView && transferHomeRootModel){
        [self.recordView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        for (int i = 0 ; i < 4;i++){
            CGFloat origin_X = i * kScreenBounds.size.width / 4.;
            CGFloat origin_y = 0;
            CGFloat size_width = kScreenBounds.size.width / 4.;
            CGFloat size_height = self.transferCellHeight;
            
            PDRecordSingleModel *recordSingleModel = [[PDRecordSingleModel alloc]init];
            if (i == 0 ){
                recordSingleModel.fixedStr = @"胜率";
                recordSingleModel.dymicStr = transferHomeRootModel.stats.winRate;
            } else if (i == 1){
                recordSingleModel.fixedStr = @"KDA";
                recordSingleModel.dymicStr = transferHomeRootModel.stats.kda;
            } else if (i == 2){
                recordSingleModel.fixedStr = @"段位";
                if ((transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 255) && (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 255)){
                    recordSingleModel.duanweiStr = @"未定位";
                } else {
                    NSString *level = @"";
                    if (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 1){
                        level = @"I";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 2){
                        level = @"II";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 3){
                        level = @"III";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 4){
                        level = @"IV";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.gradeLevel == 5){
                        level = @"V";
                    }

                    NSString *duanwei1 = @"";
                    if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 0){
                        duanwei1 = @"最强王者";
                        level = @"";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 1){
                        duanwei1 = @"钻石";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 2){
                        duanwei1 = @"白金";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 3){
                        duanwei1 = @"黄金";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 4){
                        duanwei1 = @"白银";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 5){
                        duanwei1 = @"青铜";
                    } else if (transferHomeRootModel.memberLOLGameUser.lolGameUser.grade == 6){
                        duanwei1 = @"超凡大师";
                    }
                    
                    recordSingleModel.duanweiStr = [NSString stringWithFormat:@"%@%@",duanwei1,level];
                }
            } else if (i == 3){
                recordSingleModel.fixedStr = @"总场次";
                recordSingleModel.dymicStr = transferHomeRootModel.stats.gameCount;
            }
            
            PDPandaRecordSingleView *pandaRecordSingleView = [[PDPandaRecordSingleView alloc]initWithFrame:CGRectMake(origin_X, origin_y, size_width, size_height)];
            pandaRecordSingleView.transferRecordSingleModdel = recordSingleModel;
            pandaRecordSingleView.backgroundColor = [UIColor clearColor];
            [self.recordView addSubview:pandaRecordSingleView];
        }
    }
}



@end
