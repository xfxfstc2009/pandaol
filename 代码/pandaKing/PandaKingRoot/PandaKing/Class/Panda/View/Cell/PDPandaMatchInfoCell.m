//
//  PDPandaMatchInfoCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaMatchInfoCell.h"

@interface PDPandaMatchInfoCell()
@property (nonatomic,strong)PDImageView *avatarView;            /**< 头像*/
@property (nonatomic,strong)UILabel *gameTypeLabel;             /**< 比赛类别*/
@property (nonatomic,strong)UILabel *gameEndLabel;              /**< 比赛结果*/
@property (nonatomic,strong)UILabel *gameRecordLabel;           /**< 比赛战绩*/
@property (nonatomic,strong)UILabel *moneyLabel;                /**< 比赛打钱*/
@property (nonatomic,strong)UILabel *timeLabel;                 /**< 比赛时间*/
@property (nonatomic,strong)UILabel *rewardLabel;               /**< 比赛奖励*/


@end

@implementation PDPandaMatchInfoCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 头像
    self.avatarView = [[PDImageView alloc]init];
    self.avatarView.backgroundColor = [UIColor clearColor];
    self.avatarView.frame = CGRectMake(LCFloat(12), LCFloat(7), LCFloat(60), LCFloat(60));
    self.avatarView.clipsToBounds = YES;
    self.avatarView.layer.cornerRadius = LCFloat(30);
    [self addSubview:self.avatarView];
    
    // 2. 比赛类别
    self.gameTypeLabel = [[UILabel alloc]init];
    self.gameTypeLabel.backgroundColor = [UIColor clearColor];
    self.gameTypeLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.gameTypeLabel.textColor = c5;
    [self addSubview:self.gameTypeLabel];

    // 3. 比赛结果
    self.gameEndLabel = [[UILabel alloc]init];
    self.gameEndLabel.backgroundColor = [UIColor clearColor];
    self.gameEndLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.gameEndLabel.textColor = c9;
    [self addSubview:self.gameEndLabel];
    
    // 4. 创建比赛战绩
    self.gameRecordLabel = [[UILabel alloc]init];
    self.gameRecordLabel.backgroundColor = [UIColor clearColor];
    self.gameRecordLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.gameRecordLabel.textColor = c5;
    [self addSubview:self.gameRecordLabel];
    
    // 5.比赛打钱
    self.moneyLabel = [[UILabel alloc]init];
    self.moneyLabel.backgroundColor = [UIColor clearColor];
    self.moneyLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.moneyLabel.textColor = c5;
    [self addSubview:self.moneyLabel];
    
    // 6. 比赛时间
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.timeLabel.textColor = c5;
    [self addSubview:self.timeLabel];
    
    // 7. 柱子
    self.rewardLabel = [[UILabel alloc]init];
    self.rewardLabel.backgroundColor = [UIColor clearColor];
    self.rewardLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.rewardLabel.textColor = c15;
    [self addSubview:self.rewardLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferMatchModel:(PDPandaMatchSingleModel *)transferMatchModel{
    _transferMatchModel = transferMatchModel;
    
    // 1. 头像
    [self.avatarView uploadImageWithURL:@"5.jpg" placeholder:nil callback:^(UIImage *image) {
        NSLog(@"%@",image);
    }];
    
    // 2. 创建排位赛
    self.gameTypeLabel.text = transferMatchModel.type;
    CGSize gameTypeSize = [self.gameTypeLabel.text sizeWithCalcFont:self.gameTypeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gameTypeLabel.font])];
    self.gameTypeLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarView.frame) + LCFloat(11), LCFloat(12), gameTypeSize.width, [NSString contentofHeightWithFont:self.gameTypeLabel.font]);
    
    // 3. 创建比赛结果
    self.gameEndLabel.text = transferMatchModel.gameEnd;
    self.gameEndLabel.frame = CGRectMake(self.gameTypeLabel.orgin_x, CGRectGetMaxY(self.gameTypeLabel.frame) + LCFloat(12), self.gameTypeLabel.size_width, [NSString contentofHeightWithFont:self.gameEndLabel.font]);
    
    // 3. 创建比赛战绩
    self.gameRecordLabel.text = transferMatchModel.gameRecord;
    CGSize gameRecordSize = [self.gameRecordLabel.text sizeWithCalcFont:self.gameRecordLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellHeight)];
    self.gameRecordLabel.frame = CGRectMake(CGRectGetMaxX(self.gameTypeLabel.frame) + LCFloat(26), 0, gameRecordSize.width, self.transferCellHeight);
    
    //4. 创建比赛打钱数量
    self.moneyLabel.text = transferMatchModel.gameMoney;
    CGSize moneySize = [self.moneyLabel.text sizeWithCalcFont:self.moneyLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellHeight)];
    self.moneyLabel.frame = CGRectMake(CGRectGetMaxX(self.gameRecordLabel.frame) + LCFloat(26), 0, moneySize.width, self.transferCellHeight);
    
    // 5. 创建时间
    self.timeLabel.text = [NSDate getTimeWithString:transferMatchModel.time];
    CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellHeight)];
    self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.moneyLabel.frame) + LCFloat(26), 0, timeSize.width, self.transferCellHeight);
    
    // 6. 创建奖励
    self.rewardLabel.text = self.transferMatchModel.reward;
    CGSize rewardSize = [self.rewardLabel.text sizeWithCalcFont:self.rewardLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.rewardLabel.font])];
    self.rewardLabel.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame) + LCFloat(26), 0, rewardSize.width, self.transferCellHeight);
    
    
    // 7.重新修改定位
    CGFloat paiweiHeight = [NSString contentofHeightWithFont:self.gameTypeLabel.font];
    CGFloat gameEntHeight = [NSString contentofHeightWithFont:self.gameEndLabel.font];
    CGFloat oneMargin = (self.transferCellHeight - paiweiHeight - gameEntHeight - LCFloat(6)) / 2.;
    self.gameTypeLabel.orgin_y = oneMargin;
    self.gameEndLabel.orgin_y = CGRectGetMaxY(self.gameTypeLabel.frame) + LCFloat(6);
    
    // 7.1 竹子
    self.rewardLabel.orgin_x = kScreenBounds.size.width - LCFloat(19) - rewardSize.width;
    
    CGFloat tempWidth = self.rewardLabel.orgin_x - MAX(CGRectGetMaxX(self.gameTypeLabel.frame), CGRectGetMaxX(self.gameEndLabel.frame));
    CGFloat tempWidth1 = (tempWidth - gameRecordSize.width - moneySize.width - timeSize.width) /4.;
    
    self.gameRecordLabel.orgin_x = MAX(CGRectGetMaxX(self.gameTypeLabel.frame), CGRectGetMaxX(self.gameEndLabel.frame)) + tempWidth1;
    self.moneyLabel.orgin_x = CGRectGetMaxX(self.gameRecordLabel.frame) + tempWidth1;
    
    self.timeLabel.orgin_x = CGRectGetMaxX(self.moneyLabel.frame) + tempWidth1;
}


+(CGFloat)calculationCellHeight{
    CGFloat cellheight = 0;
    cellheight += LCFloat(7);
    cellheight += LCFloat(60);
    cellheight += LCFloat(7);
    return cellheight;
}
@end
