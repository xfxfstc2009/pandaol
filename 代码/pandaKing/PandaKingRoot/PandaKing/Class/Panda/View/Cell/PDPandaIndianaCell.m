//
//  PDPandaIndianaCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaIndianaCell.h"
#import "PDPandaIndianaSingleCell.h"

@interface PDPandaIndianaCell()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout>
@property (nonatomic,strong)UICollectionView *productCollectionView;            /**< collection*/
@property (nonatomic,strong)NSMutableArray *collectionArr;                             /**< 数据*/

@end

static NSString *collectionCell = @"collectionCell";
@implementation PDPandaIndianaCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = BACKGROUND_VIEW_COLOR;
        
        if (!self.collectionArr){                                               // 2. 更新当前的数据源
            self.collectionArr = [NSMutableArray array];
        }
        [self createCollectionView];                                            // 创建collectionView
    }
    return self;
}


#pragma mark - createView
-(void)createCollectionView{
    if (!self.productCollectionView){
        UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
        
        self.productCollectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:flowLayout];
        self.productCollectionView.frame = CGRectMake(0, 0, kScreenBounds.size.width,  [PDPandaIndianaSingleCell calculationCellSize].height + 10);
        self.productCollectionView.showsVerticalScrollIndicator = NO;
        self.productCollectionView.backgroundColor = [UIColor clearColor];
        self.productCollectionView.delegate = self;
        self.productCollectionView.dataSource = self;
        self.productCollectionView.scrollsToTop = YES;
        self.productCollectionView.scrollEnabled = NO;
        // 注册 Cell - collectionIdentify
        [self.productCollectionView registerClass:[PDPandaIndianaSingleCell class] forCellWithReuseIdentifier:collectionCell];
        
        [self addSubview:self.productCollectionView];
    }
}

#pragma mark - UICollectionViewCell
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.collectionArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PDPandaIndianaSingleCell *cell = (PDPandaIndianaSingleCell *)[collectionView dequeueReusableCellWithReuseIdentifier:collectionCell forIndexPath:indexPath];
    __weak typeof(self)weakSelf = self;
    [cell timeOutToNotificationWithRoot:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(timeOutToNotificationRootInfoManager)]){
            [strongSelf.delegate timeOutToNotificationRootInfoManager];
        }
    }];
    
    cell.transferCellSize = [PDPandaIndianaSingleCell calculationCellSize];
    cell.transferSingleModel = [self.transferIndianaArr objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegate
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, LCFloat(10), 5, LCFloat(10));
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PDPandaTreasuresSingleModel *treasuresSingleModel = [self.transferIndianaArr objectAtIndex:indexPath.row];
    if (self.delegate && [self.delegate respondsToSelector:@selector(pandaTreasuresDidselectedWithModel:)]){
        [self.delegate pandaTreasuresDidselectedWithModel:treasuresSingleModel];
    }
}



#pragma mark - UICollectionViewDelegateFlowLayout;
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [PDPandaIndianaSingleCell calculationCellSize];
}

#pragma mark - SET
-(void)setTransferIndianaArr:(NSArray *)transferIndianaArr{
    _transferIndianaArr = transferIndianaArr;
    
    if (self.collectionArr.count){
        [self.collectionArr removeAllObjects];
    }
    [self.collectionArr addObjectsFromArray:transferIndianaArr];
    [self.productCollectionView reloadData];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.productCollectionView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.transferCellHeight);
}



+(CGFloat)calculationCellHeight{
    CGFloat height = [PDPandaIndianaSingleCell calculationCellSize].height + 10;
    return height;
}

@end
