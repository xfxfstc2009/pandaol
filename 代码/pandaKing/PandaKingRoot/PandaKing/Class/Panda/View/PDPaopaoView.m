//
//  PDPaopaoView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPaopaoView.h"
#import <objc/runtime.h>
#import <pop/POP.h>
#import "GCD.h"

static char tapKey;
@interface PDPaopaoView()

@property (nonatomic,strong)UILabel *paopaoFixedLabel;                  /**< */
@property (nonatomic,strong)UILabel *paopaoDymicLabel;                  /**< 泡泡动态label*/
@property (nonatomic,assign)BOOL isPopAnimation;
@end

@implementation PDPaopaoView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.paopaoImageView = [[PDImageView alloc]init];
    self.paopaoImageView.backgroundColor = [UIColor clearColor];
    self.paopaoImageView.image = [UIImage imageNamed:@"home_paopao"];
    self.paopaoImageView.userInteractionEnabled = YES;
    [self addSubview:self.paopaoImageView];
    self.paopaoImageView.frame = self.bounds;
    
    // 2. 创建label
    self.paopaoFixedLabel = [[UILabel alloc]init];
    self.paopaoFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.paopaoFixedLabel.frame = CGRectMake(0, LCFloat(40), self.size_width, [NSString contentofHeightWithFont:self.paopaoFixedLabel.font]);
    self.paopaoFixedLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.paopaoFixedLabel.textAlignment = NSTextAlignmentCenter;
    self.paopaoFixedLabel.text = @"今日新增金币";
//    self.paopaoFixedLabel.backgroundColor = [UIColor yellowColor];
    self.paopaoFixedLabel.adjustsFontSizeToFitWidth = YES;
    [self.paopaoImageView addSubview:self.paopaoFixedLabel];
    
    // 3. 创建动态label
    self.paopaoDymicLabel = [[UILabel alloc]init];
    self.paopaoDymicLabel.font = [[UIFont systemFontOfCustomeSize:35.]boldFont];
    self.paopaoDymicLabel.frame = CGRectMake(LCFloat(20), CGRectGetMaxY(self.paopaoFixedLabel.frame) + LCFloat(10), self.size_width - 2 * LCFloat(20), [NSString contentofHeightWithFont:self.paopaoDymicLabel.font]);
    self.paopaoDymicLabel.textAlignment = NSTextAlignmentCenter;
    self.paopaoDymicLabel.adjustsFontSizeToFitWidth = YES;
    self.paopaoDymicLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.paopaoDymicLabel.text = @"80";
//    self.paopaoDymicLabel.backgroundColor = [UIColor redColor];
    [self.paopaoImageView addSubview:self.paopaoDymicLabel];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapClick)];
    [self addGestureRecognizer:tap];
    
    // 泡泡初始化
    [self paopaoTypeManager:paopaoViewTypeNormal number:0];
    
    [GCDQueue executeInMainQueue:^{
        [self scaleAnimation];
    } afterDelaySecs:3.f];
    
}


#pragma mark - 气泡点击方法
-(void)tapManager:(void(^)())block{
    objc_setAssociatedObject(self, &tapKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)tapClick{
    void(^block)() = objc_getAssociatedObject(self, &tapKey);
    if (block){
        block();
    }
}

-(void)autoSettingWithSizeWithZoom:(CGFloat)zoom{
    CGFloat size = MAX(self.size_height, self.size_width);
    self.paopaoImageView.frame = CGRectMake(0, 0, size, size);
    //
    self.paopaoDymicLabel.transform = CGAffineTransformMakeScale(zoom, zoom);
    self.paopaoFixedLabel.transform = CGAffineTransformMakeScale(zoom, zoom);
    

    // 修改frame
    if (![AccountModel sharedAccountModel].hasLoggedIn){
        self.paopaoFixedLabel.frame = self.bounds;
    } else {
        // 1. 计算头部的高度
        CGFloat oneHeight = [NSString contentofHeightWithFont:self.paopaoFixedLabel.font] * zoom;
        CGFloat twoHeitht = [NSString contentofHeightWithFont:self.paopaoDymicLabel.font]* zoom;
        CGFloat margin_center_Height = LCFloat(10)* zoom;
        CGFloat margin_Top = (CGRectGetHeight(self.frame) - oneHeight - twoHeitht - margin_center_Height) / 2.;
        
        self.paopaoFixedLabel.frame = CGRectMake(0, margin_Top, CGRectGetWidth(self.bounds), oneHeight);
        self.paopaoDymicLabel.frame = CGRectMake(0, CGRectGetMaxY(self.paopaoFixedLabel.frame) + margin_center_Height, self.paopaoFixedLabel.size_width, twoHeitht);
    }
}




#pragma mark - Animation
-(void)startAnimation{
    self.isPopAnimation = YES;
}

-(void)stopAnimation{
    self.isPopAnimation = NO;
}


#pragma mark - 弹簧动画
- (void)scaleAnimation {
    if (self.isPopAnimation == NO){
        return;
    }
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    
    scaleAnimation.name               = @"scaleSmallAnimation";
    scaleAnimation.delegate           = self;
    
    scaleAnimation.duration           = 0.15f;
    scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(1.25, 1.25)];
    
    if (self.isPopAnimation == NO){
        scaleAnimation.paused = YES;
    }
    
    [self pop_addAnimation:scaleAnimation forKey:nil];
}

- (void)pop_animationDidStop:(POPAnimation *)anim finished:(BOOL)finished {
    
    if ([anim.name isEqualToString:@"scaleSmallAnimation"]) {
        if (self.isPopAnimation == NO){
            return;
        }
        POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        
        scaleAnimation.name                = @"SpringAnimation";
        scaleAnimation.delegate            = self;
        
        scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
        scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(-2, -2)];
        scaleAnimation.springBounciness    = 20.f;
        scaleAnimation.springSpeed         = 10.f;
        scaleAnimation.dynamicsTension     = 700.f;
        scaleAnimation.dynamicsFriction    = 7.f;
        scaleAnimation.dynamicsMass        = 3.f;
        
        if (self.isPopAnimation == NO){
            scaleAnimation.paused = YES;
        }
        [self pop_addAnimation:scaleAnimation forKey:nil];
        
    } else if ([anim.name isEqualToString:@"SpringAnimation"]) {
        
        [self performSelector:@selector(scaleAnimation) withObject:nil afterDelay:3];
    }
}




-(void)animationManagerWithLabel:(UILabel *)label toValue:(CGFloat)toValue{
    [label pop_removeAllAnimations];
    POPBasicAnimation *anim = [POPBasicAnimation animation];
    anim.duration = 1;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    POPAnimatableProperty * prop = [POPAnimatableProperty propertyWithName:@"count" initializer:^(POPMutableAnimatableProperty *prop){
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [[obj description] floatValue];
        };
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
            if (label == self.paopaoDymicLabel){
                [obj setText:[NSString stringWithFormat:@"%li",(long)values[0]]];
            }
        };
        // dynamics threshold
        prop.threshold = 1;
    }];
    
    anim.property = prop;
    
    anim.fromValue = @(0);
    anim.toValue = @(toValue);
    
    [label pop_addAnimation:anim forKey:@"counting"];
}


-(void)paopaoTypeManager:(paopaoViewType)type number:(NSInteger)goldCount{
    
    if (type == paopaoViewTypeNormal){              // 登录
        [self paopaoViewTypeNormalManager];
    } else if (type == paopaoViewTypeNoBinding){
        [self paopaoViewTypeNoBindingManager];
    } else if (type == paopaoViewTypeZhuzi){
        [self paopaoViewTypeZhuziManagerWithGold:goldCount];
    }
}

-(void)paopaoViewTypeNormalManager{
    self.paopaoDymicLabel.hidden = YES;
    self.paopaoFixedLabel.hidden = NO;
    self.paopaoFixedLabel.font = [[UIFont fontWithCustomerSizeName:@"标题"]boldFont];
    self.paopaoFixedLabel.alpha = 0;
    [UIView animateWithDuration:.7f animations:^{
        self.paopaoFixedLabel.text = @"登录/注册";
        self.paopaoFixedLabel.frame = self.paopaoImageView.bounds;
        self.paopaoFixedLabel.alpha = 1;
    } completion:^(BOOL finished) {
        self.paopaoFixedLabel.alpha = 1;
        self.paopaoFixedLabel.frame = self.paopaoImageView.bounds;
    }];
}

-(void)paopaoViewTypeNoBindingManager{
    self.paopaoDymicLabel.hidden = YES;
    self.paopaoFixedLabel.hidden = NO;
    self.paopaoFixedLabel.font = [[UIFont fontWithCustomerSizeName:@"标题"]boldFont];
    self.paopaoFixedLabel.alpha = 0;
    [UIView animateWithDuration:.7f animations:^{
        self.paopaoFixedLabel.text = @"绑定召唤师";
        self.paopaoFixedLabel.frame = self.paopaoImageView.bounds;
        self.paopaoFixedLabel.alpha = 1;
    } completion:^(BOOL finished) {
        self.paopaoFixedLabel.alpha = 1;
        self.paopaoFixedLabel.frame = self.paopaoImageView.bounds;
    }];
}

-(void)paopaoViewTypeZhuziManagerWithGold:(NSInteger)gold{
    self.paopaoDymicLabel.hidden = NO;
    self.paopaoFixedLabel.hidden = NO;
    self.paopaoFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.paopaoFixedLabel.alpha = 0.f;
    self.paopaoDymicLabel.alpha = 0.f;
    // 1. 计算头部的高度
    CGFloat oneHeight = [NSString contentofHeightWithFont:self.paopaoFixedLabel.font];
    CGFloat twoHeitht = [NSString contentofHeightWithFont:self.paopaoDymicLabel.font];
    CGFloat margin_center_Height = LCFloat(10);
    CGFloat margin_Top = (CGRectGetHeight(self.frame)  - oneHeight - twoHeitht - margin_center_Height) / 2.;
    
    [UIView animateWithDuration:.7f animations:^{
        self.paopaoFixedLabel.text = @"今日新增金币";
        self.paopaoFixedLabel.alpha = 1;
        self.paopaoFixedLabel.frame = CGRectMake(0, margin_Top, CGRectGetWidth(self.bounds), [NSString contentofHeightWithFont:self.paopaoFixedLabel.font]);
    } completion:^(BOOL finished) {
        self.paopaoFixedLabel.alpha = 1;
        self.paopaoFixedLabel.frame = CGRectMake(0, margin_Top, CGRectGetWidth(self.bounds), [NSString contentofHeightWithFont:self.paopaoFixedLabel.font]);
        
        self.paopaoDymicLabel.alpha = 1;
        self.paopaoDymicLabel.frame = CGRectMake(0, CGRectGetMaxY(self.paopaoFixedLabel.frame) + margin_center_Height, self.paopaoFixedLabel.size_width, [NSString contentofHeightWithFont:self.paopaoDymicLabel.font]);
        [self animationManagerWithLabel:self.paopaoDymicLabel toValue:gold];
    }];
}

@end
