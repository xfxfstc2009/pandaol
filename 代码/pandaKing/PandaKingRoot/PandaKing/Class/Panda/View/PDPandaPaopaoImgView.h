//
//  PDPandaPaopaoImgView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDImageView.h"

@interface PDPandaPaopaoImgView : PDImageView

-(void)tapManager:(void(^)())block;

@end
