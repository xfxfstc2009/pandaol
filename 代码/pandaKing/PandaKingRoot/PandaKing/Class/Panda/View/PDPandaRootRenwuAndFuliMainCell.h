//
//  PDPandaRootRenwuAndFuliMainCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,PDPandaRootRenwuAndFuliMainCellDirectType) {
    PDPandaRootRenwuAndFuliMainCellDirectTypeRenwu,
    PDPandaRootRenwuAndFuliMainCellDirectTypeFuli,
};

@interface PDPandaRootRenwuAndFuliMainCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;

-(void)directWithtype:(void(^)(PDPandaRootRenwuAndFuliMainCellDirectType directType))block;



+(CGFloat)calculationCellHeight;
@end
