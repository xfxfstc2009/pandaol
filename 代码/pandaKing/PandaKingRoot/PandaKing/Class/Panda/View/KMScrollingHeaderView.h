//
//  KMScrollingHeaderView.h
//  TheMovieDB
//
//  Created by Kevin Mindeguia on 04/02/2014.
//  Copyright (c) 2014 iKode Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DKLiveBlurView.h"

#define kDefaultImagePagerHeight 380.0f
#define kDefaultTableViewHeaderMargin 95.0f
#define kDefaultImageScalingFactor 450.0f

@class KMScrollingHeaderView;

@protocol KMScrollingHeaderViewDelegate <NSObject>

@required

- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView headerImageView:(PDImageView *)imageView;

@optional

- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView headerImageViewWasSelected:(PDImageView *)imageView;
- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView scrollViewWithScrollOffset:(CGFloat)scrollOffset;
@end

@interface KMScrollingHeaderView : UIView
@property (nonatomic,strong)DKLiveBlurView *backgroundImageView;
@property (nonatomic) CGFloat headerImageViewHeight;
@property (nonatomic) CGFloat headerImageViewScalingFactor;
@property (nonatomic) CGFloat navbarViewFadingOffset;
@property (nonatomic, strong) UITableView* tableView;
@property (nonatomic, strong) UIView* navbarView;
@property (nonatomic) UIViewContentMode headerImageViewContentMode;
@property (nonatomic, weak) id<KMScrollingHeaderViewDelegate> delegate;
//@property (nonatomic,assign)BOOL navAlpha;


- (instancetype)initWithFrame:(CGRect)frame WithHeight:(CGFloat)height;
- (void)reloadScrollingHeader;
-(void)maskHeaderImage:(UIImage *)image;
@end
