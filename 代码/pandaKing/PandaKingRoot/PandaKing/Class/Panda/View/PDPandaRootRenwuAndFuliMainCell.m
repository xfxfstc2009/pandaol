//
//  PDPandaRootRenwuAndFuliMainCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPandaRootRenwuAndFuliMainCell.h"
#import "PDShopRootMainHeaderSingleView.h"

static char actionClickWithLeftKey;
@interface PDPandaRootRenwuAndFuliMainCell()
@property (nonatomic,strong)PDShopRootMainHeaderSingleView *leftView;
@property (nonatomic,strong)PDShopRootMainHeaderSingleView *rightView;


@end

@implementation PDPandaRootRenwuAndFuliMainCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.leftView = [[PDShopRootMainHeaderSingleView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width / 2., [PDShopRootMainHeaderSingleView calculationCellHeight])];
    self.leftView.transferIcon = [UIImage imageNamed:@"icon_panda_home_renwu"];
    self.leftView.transferTitle = @"任务中心";
    self.leftView.transferDesc = @"成就任务领百元现金";
    [self addSubview:self.leftView];
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.backgroundColor = [UIColor clearColor];
    btn1.frame = self.leftView.bounds;
    [self.leftView addSubview:btn1];
    __weak typeof(self)weakSelf = self;
    [btn1 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithLeftKey);
        if (block){
            block(PDPandaRootRenwuAndFuliMainCellDirectTypeRenwu);
        }
    }];
    
    // right
    self.rightView = [[PDShopRootMainHeaderSingleView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width / 2., 0, kScreenBounds.size.width / 2., [PDShopRootMainHeaderSingleView calculationCellHeight])];
    self.rightView.transferIcon = [UIImage imageNamed:@"icon_panda_home_fuli"];
    self.rightView.transferTitle = @"福利中心";
    self.rightView.transferDesc = @"0元夺宝天天赚";
    [self addSubview:self.rightView];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.backgroundColor = [UIColor clearColor];
    btn2.frame = self.leftView.bounds;
    [self.rightView addSubview:btn2];
    [btn2 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithLeftKey);
        if (block){
            block(PDPandaRootRenwuAndFuliMainCellDirectTypeFuli);
        }
    }];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    if ([AccountModel sharedAccountModel].isShenhe){
        self.leftView.transferTitle = @"新手引导";
        self.leftView.transferDesc = @"教你如何玩转盼达电竞";
        
        self.rightView.transferDesc = @"赚好多钱哦";
    
    } else {
        self.rightView.transferDesc = @"0元夺宝天天赚";
        self.leftView.transferTitle = @"任务中心";
        self.leftView.transferDesc = @"成就任务领百元现金";
    
    }
}

-(void)directWithtype:(void(^)(PDPandaRootRenwuAndFuliMainCellDirectType directType))block{
    objc_setAssociatedObject(self, &actionClickWithLeftKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return [PDShopRootMainHeaderSingleView calculationCellHeight];
}
@end
