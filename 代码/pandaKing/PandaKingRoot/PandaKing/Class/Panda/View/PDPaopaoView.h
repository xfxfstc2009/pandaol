//
//  PDPaopaoView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,paopaoViewType) {
    paopaoViewTypeNormal,                       /**< 未登录*/
    paopaoViewTypeNoBinding,                    /**< 已登录，未绑定*/
    paopaoViewTypeZhuzi,
};


@interface PDPaopaoView : UIView

@property (nonatomic,strong)PDImageView *paopaoImageView;

-(void)tapManager:(void(^)())block;

-(void)autoSettingWithSizeWithZoom:(CGFloat)zoom;

-(void)startAnimation;
-(void)stopAnimation;

#pragma mark 登录成功以后更新泡泡状态
-(void)paopaoTypeManager:(paopaoViewType)type number:(NSInteger)goldCount;


@end
