//
//  PDPandaHomeMemberLolGameUserModel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaHomeMemberLolGameUserModel.h"

@implementation PDPandaHomeMemberLolGameUserModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"userId":@"id"};
}

@end
