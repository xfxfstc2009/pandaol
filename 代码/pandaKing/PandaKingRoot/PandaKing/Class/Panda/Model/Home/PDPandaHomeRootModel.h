//
//  PDPandaHomeRootModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDGamerRecordRootModel.h"
#import "PDPandaHomeStatusModel.h"
#import "PDPandaHomeMemberLolGameUserModel.h"
@interface PDPandaHomeRootModel : FetchModel

@property (nonatomic,copy)NSString *gameUserAvatar;
@property (nonatomic,assign)NSInteger days;                                /**< 最近几天*/
@property (nonatomic,assign)NSInteger recentlyGoldIncrease;                        /**< 金币数量*/
@property (nonatomic,assign)NSInteger todayGoldIncrease;                            /**< 今日增加*/
@property (nonatomic,strong)PDGamerRecordRootModel *record;                             /**< 战绩*/
@property (nonatomic,strong)PDPandaHomeStatusModel *stats;                              /**< 状态*/
@property (nonatomic,strong)PDPandaHomeMemberLolGameUserModel *memberLOLGameUser;       /**< LoL信息*/
@property (nonatomic,assign)BOOL isBind;                                             /**< 判断是否绑定*/

// temp - animation
@property (nonatomic,assign)BOOL animationWillBinding;                          /**< 用来判断是否是刚刚绑定的动画*/

@end
