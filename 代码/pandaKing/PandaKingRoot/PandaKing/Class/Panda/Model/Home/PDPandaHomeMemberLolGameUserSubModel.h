//
//  PDPandaHomeMemberLolGameUserSubModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDPandaHomeMemberLolGameUserSubModel : FetchModel

@property (nonatomic,copy)NSString *gameUserId;             /**< 游戏编号*/
@property (nonatomic,copy)NSString *roleName;               /**< 角色名字*/
@property (nonatomic,copy)NSString *serverName;             /**< 服务器名字*/
@property (nonatomic,copy)NSString *telecomLine;            /**< 电信线路*/
@property (nonatomic,copy)NSString *gameServerId;           /**< 游戏服务器id*/
@property (nonatomic,assign)NSInteger grade;                /**< 青铜*/
@property (nonatomic,assign)NSInteger gradeLevel;           /**< 3级*/
@property (nonatomic,copy)NSString *fightingCapacity;       /**< 战斗力*/
@property (nonatomic,copy)NSString *avatar;                 /**< 头像*/

@end
