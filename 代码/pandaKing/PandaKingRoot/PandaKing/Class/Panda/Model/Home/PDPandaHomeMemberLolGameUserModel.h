//
//  PDPandaHomeMemberLolGameUserModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPandaHomeMemberLolGameUserSubModel.h"

typedef NS_ENUM(NSInteger,gameUserStatus) {
    gameUserStatusBind,                 /**< 绑定*/
    gameUserStatusActivating,           /**< 认证中*/
    gameUserStatusActivated,            /**< 已认证*/
};

@interface PDPandaHomeMemberLolGameUserModel : FetchModel

@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *userId;
@property (nonatomic,copy)NSString *gameId;
@property (nonatomic,copy)NSString *memberId;
@property (nonatomic,assign)BOOL enabled;
@property (nonatomic,strong)PDPandaHomeMemberLolGameUserSubModel *lolGameUser;


@property (nonatomic,assign)BOOL isAnimation;

@property (nonatomic,copy)NSString *state;
@property (nonatomic,assign)gameUserStatus activateStatus;
@end
