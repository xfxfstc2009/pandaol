//
//  PDNewFeatureListModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDNewFeatureSingleModel.h"
@interface PDNewFeatureListModel : FetchModel

@property (nonatomic,strong)NSArray<PDNewFeatureSingleModel> *items;

@end
