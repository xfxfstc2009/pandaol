//
//  PDNewFeatureSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDNewFeatureSingleModel <NSObject>


@end

@interface PDNewFeatureSingleModel : FetchModel

@property (nonatomic,copy)NSString *type;               /**< 新手引导类型*/
@property (nonatomic,assign)BOOL complete;              /**< 是否完成*/


@end
