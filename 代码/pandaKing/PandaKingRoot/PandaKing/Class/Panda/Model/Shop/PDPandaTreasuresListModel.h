//
//  PDPandaTreasuresListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPandaTreasuresSingleModel.h"

@interface PDPandaTreasuresListModel : FetchModel

@property (nonatomic,strong)NSArray<PDPandaTreasuresSingleModel> *items;

@end
