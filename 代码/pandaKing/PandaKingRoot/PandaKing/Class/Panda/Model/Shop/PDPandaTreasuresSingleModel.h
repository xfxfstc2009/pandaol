//
//  PDPandaTreasuresSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDPandaTreasuresSingleModel <NSObject>

@end

@interface PDPandaTreasuresSingleModel : FetchModel

@property (nonatomic,copy)NSString *productId;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *image;
@property (nonatomic,assign)NSInteger joinedCount;
@property (nonatomic,assign)NSTimeInterval leftTime;            /**< 时间差*/
@property (nonatomic,assign)NSTimeInterval drawTime;            /**< 结束时间*/
@end
