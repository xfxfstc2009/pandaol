//
//  PDPandaHomeRecordRootModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDPandaHomeRecordRootModel : FetchModel

@property (nonatomic,assign)NSTimeInterval date;                    /**< 时间*/
@property (nonatomic,assign)BOOL isWin;                             /**< 是否胜利*/
@property (nonatomic,copy)NSString *type;                           /**< 排位赛*/
@property (nonatomic,copy)NSString *kda;                            /**< KDA*/
@property (nonatomic,copy)NSString *lolGold;                        /**< 游戏打钱数*/

@end
