//
//  PDRecordSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDRecordSingleModel : FetchModel

@property (nonatomic,copy)NSString *fixedStr;               /**< 固定文字*/
@property (nonatomic,assign)CGFloat dymicStr;               /**< 动态文字*/
@property (nonatomic,copy)NSString *duanweiStr;             /**< 段位*/



@end
