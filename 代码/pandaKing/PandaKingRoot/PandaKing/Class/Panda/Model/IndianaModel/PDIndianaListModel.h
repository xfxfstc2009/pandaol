//
//  PDIndianaListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDIndianaSingleModel.h"
@interface PDIndianaListModel : FetchModel

@property (nonatomic,strong)NSArray <PDIndianaSingleModel> *proList;

@end
