//
//  PDPandaMatchSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 比赛model 
#import "FetchModel.h"

@interface PDPandaMatchSingleModel : FetchModel

@property (nonatomic,copy)NSString *avatar;         /**< 头像*/
@property (nonatomic,copy)NSString *type;           /**< 类别*/
@property (nonatomic,copy)NSString *gameEnd;        /**< 比赛结束*/
@property (nonatomic,copy)NSString *gameRecord;     /**< 比赛战绩*/
@property (nonatomic,copy)NSString *gameMoney;      /**< 游戏打钱*/
@property (nonatomic,assign)NSTimeInterval time;    /**< 比赛时间*/
@property (nonatomic,copy)NSString *reward;         /**< 比赛奖励*/

@end
