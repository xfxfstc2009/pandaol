//
//  PDPandaRootViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaRootViewController.h"
#import "KMScrollingHeaderView.h"                                   // 底部框架
#import "PDCurrentLocationManager.h"
#import "PDAppleShenheModel.h"

// 【Cell】
#import "PDPandaNormalCell.h"                                       // 默认的cell
#import "PDPandaPersonCenterCell.h"                                 // 个人中心
#import "PDPandaRecordCenter.h"                                     // 战绩中心
#import "PDInternetCafesSingleCell.h"                               // 附近的网吧
#import "PDRoleRecordSingleCell.h"                                  // 最近比赛
#import "PDPandaIndianaCell.h"                                      // 夺宝
#import "PDPandaLotteryTableViewCell.h"                             // 战绩


// 【Controller】
#import "PDPandaRoleDetailViewController.h"                         // 个人信息
#import "PDLoginMainViewController.h"                               // Login
#import "PDMessageViewController.h"
#import "PDInternetCafesRootViewController.h"                       // 我的附近网吧列表
#import "PDInternetCafesDetailViewController.h"                     // 网吧详情
#import "PDBindingInfoViewController.h"                             // 绑定页面
#import "PDProductDetailViewController.h"                           // 夺宝信息
#import "PDCenterViewController.h"
#import "MMHLocationNavigationManager.h"
#import "PDNewFeaturePageViewController.h"
#import "PDShopRootViewController.h"
#import "PDLotteryGameViewController.h"                             // 跳转到赛事猜
#import "PDLotteryGameDetailRootViewController.h"                       // 赛事猜详情
#import "PDEditViewController.h"
#import "PDShopRootMainViewController.h"
#import "PDLotteryGameDetailMainViewController.h"
// 【view】h
#import "PDPaopaoView.h"                                            // 泡泡view
#import "PDPandaPaopaoImgView.h"
#import "PDPandaTreasuresListModel.h"
#import "PDPandaHomeTopLabel.h"                                     // 最近10天
#import "PDGradientNavBar.h"


// 【Model】
#import "PDPandaHomeRootModel.h"                                    // 首页的详情信息
#import "PDLoteryGameRootListModel.h"                              // 赛事
#import "UIView+PDBadgeNumber.h"

// 消息状态
#import "PDMessageState.h"
#import "PDNewFeatureListModel.h"                                   // 新手指引
#import "PDLotteryGameDetailMainViewController.h"
#import "PDLotteryGameMainViewController.h"
#define PaoPao_Size LCFloat(200)                                        // 泡泡宽度
#import "PDChaosFightRootBgViewController.h"
#import "PDNavigationController.h"
#import "PDChaosFightRootViewController.h"
#import "PDCenterMyInfo.h"
#import "PDPandaRootRenwuAndFuliMainCell.h"
#import "PDShopRootMainViewController.h"
#import "PDMyTaskViewController.h"
#import "PDInviteViewController.h"

#import "PDPandaNewmoduleSingleModel.h"


@class PDCenterViewController;
@interface PDPandaRootViewController()<UIDynamicAnimatorDelegate,UITableViewDelegate,UITableViewDataSource,KMScrollingHeaderViewDelegate,PDPandaIndianaCellDelegate>{
    PDPandaHomeRootModel *pandaHomeRootModel;                                       /**< 首页的主接口*/
    BOOL isBindEnd;
}
@property (nonatomic,strong)NSMutableArray *homeMutableArr;                         /**< 数据源*/
@property (nonatomic,strong)KMScrollingHeaderView* scrollingHeaderView;             /**< 头部*/
@property (nonatomic,strong)NSMutableArray *netBarMutableArr;                       /**< 网吧数组*/
@property (nonatomic,strong)NSMutableArray *indianaMutableArr;                      /**< 夺宝数组*/
@property (nonatomic,strong)NSMutableArray *recordMutableArr;                       /**< 战绩数组*/
// 【NAV】
@property (nonatomic,strong)UIView *navBarView;                                     /**< NavBar*/
@property (nonatomic,strong)PDImageView *barMainTitleImageView;                     /**< 标题导航栏*/
@property (nonatomic,strong)UIButton *navBarMessageBtn;                             /**< 导航栏上的按钮*/
// 【最近10天】
@property (nonatomic,strong)PDPandaHomeTopLabel *topLabel;                          /**< 头部的label*/

// 【泡泡】
@property (nonatomic,strong)UIPanGestureRecognizer *paopaoPanGesture;               /**< 泡泡拖动*/
@property (nonatomic,strong)UIAttachmentBehavior *paopaoAttachmentBehavior;
@property (nonatomic,strong)UIDynamicAnimator *paopaoDynamicAniator;                /**< 泡泡的重力*/
@property (nonatomic,assign)CGPoint paopaoCenterPoint;                              /**< 泡泡中间的点*/
@property (nonatomic,strong)UISnapBehavior *paopaoSnapBehavior;                     /**< 泡泡的重力*/

// 【浮层view】
@property (nonatomic,strong,nullable)TNSexyImageUploadProgress *imageUploadProgress;             /**< progress*/

@property (nonatomic,assign)BOOL isPanGesture;                                      /**< 判断是否拖动*/
@property (nonatomic,assign)BOOL isScroll;                                          /**< 判断是否scroll*/
@property (nonatomic,assign)BOOL isPaopaoDynamicAni;                                /**< 判断泡泡动画是否完成*/

@property (nonatomic,strong)UIButton *tempRightButton;
@property (nonatomic,strong)UIButton *tempLeftButton;
@property (nonatomic, strong) UIButton *messageButton;


// 赛事猜
@property (nonatomic,strong)NSMutableArray *lotteryMutableArr;
@end

@implementation PDPandaRootViewController

#pragma mark - shared
+(instancetype)sharedController{
    static PDPandaRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[PDPandaRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if (self.topLabel){
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            self.topLabel.hidden = NO;
        } else {
            self.topLabel.hidden = YES;
        }
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        // 去获取消息信息
        __weak typeof(self)weakSelf = self;
        [weakSelf fetchMessageState];
        [weakSelf getCurrentRoleInfo];
    }
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];                               // 1. 数据初始化
    [self pageSetting];
    [self setupDetailsPageView];                        // 2. 创建view
    [self mainInterfaceComfirm];                        // 3. 执行数据
    [self sendRequestToGetUpInfo];                      // 4. 获取当前最新的功能模块
}

-(void)pageSetting{
    self.tempRightButton = [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_panda_message_nor"] barHltImage:[UIImage imageNamed:@"icon_panda_message_nor"] action:NULL];
    
    self.tempLeftButton = [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_panda_message_nor"] barHltImage:[UIImage imageNamed:@"icon_panda_message_nor"] action:NULL];
}

-(void)mainInterfaceComfirm{               // 游客登录
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] guestLoginManagerWithBlcok:^(BOOL interfaceSuccess, BOOL socketSuccess) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (interfaceSuccess && socketSuccess){                 // 接口成功
            [strongSelf shenheManager];
        } else {                                                // 接口不成功
            [strongSelf mainInterfaceComfirm];
        }
    }];
}

-(void)shenheManager{
    __weak typeof(self)weakSelf = self;
    [self sendRequestToGetShenheWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf getDataInfo];                           // 执行获取首页信息
    }];
}

-(void)getDataInfo{
    __weak typeof(self)weakSelf = self;
    if ([PDPandaRootViewController sharedController].hasLaungchLinkAnimation){          // 【laungch衔接动画已经执行过了】
        [weakSelf sendRequestToGetPersonalInfoHasAnimation:NO block:NULL];        // 获取首页信息
    }
    
    // 3.【获取当前的最新夺宝】
    if (![AccountModel sharedAccountModel].isShenhe){
        [weakSelf sendReqeustToGetTreasures];
        // 3. 【获取竞猜详情】
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//           [weakSelf sendRequestToGetGuessList];
//        });
    }
    
    
    // 2.【获取网吧数据】
    [[PDCurrentLocationManager sharedLocation] getCurrentLocationManager:^(CGFloat lat, CGFloat lng, AMapAddressComponent *addressComponent) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToChangePositionWithLat:lat lng:lng];             // 1. 提交当前的经纬度
        if ([AccountModel sharedAccountModel].isShenhe){
            [strongSelf sendRequestToGetNetBarList];              // 2. 获取当前的网吧
        }
    }];
}

#pragma mark - 1.1 【Array Init】
-(void)arrayWithInit{
    // 【主数据源】
    self.homeMutableArr = [NSMutableArray array];
    [self.homeMutableArr addObject:@[@"福利任务"]];
    // 【战绩信息】
    self.recordMutableArr = [NSMutableArray array];
    // 【网吧更新】
    self.netBarMutableArr = [NSMutableArray array];
    // 【夺宝详情】
    self.indianaMutableArr = [NSMutableArray array];
    // 【赛事】
    self.lotteryMutableArr = [NSMutableArray array];
    
}

#pragma mark - 2.1 【Main View】
-(void)setupDetailsPageView{
    self.scrollingHeaderView = [[KMScrollingHeaderView alloc]initWithFrame:self.view.bounds];
    self.scrollingHeaderView.backgroundColor = [UIColor whiteColor];
    self.scrollingHeaderView.delegate = self;
    self.scrollingHeaderView.tableView.dataSource = self;
    self.scrollingHeaderView.tableView.delegate = self;
    self.scrollingHeaderView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.scrollingHeaderView.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.scrollingHeaderView.tableView.showsVerticalScrollIndicator = YES;
    self.scrollingHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    self.scrollingHeaderView.tableView.separatorColor = [UIColor whiteColor];
    self.scrollingHeaderView.headerImageViewContentMode = UIViewContentModeScaleToFill;
    self.scrollingHeaderView.backgroundImageView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollingHeaderView];
    [self.scrollingHeaderView reloadScrollingHeader];
    
    // 2. 创建泡泡
    [self createPaopaoView];
    // 3. 创建右侧的按钮
    [self createRightMessageButton];
    // 4. 创建navBar
    [self createNavBar];
    // 5. 创建【最近10天：128金币】
    [self createTopView];
}

#pragma mark 2.2 【更新背景图片】
- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView headerImageView:(PDImageView *)imageView{
    self.scrollingHeaderView.backgroundImageView.originalImage = [UIImage imageNamed:@"panda_rootBgView"];
}

- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView scrollViewWithScrollOffset:(CGFloat)scrollOffset{
    if (scrollOffset < 0) {
        self.scrollingHeaderView.backgroundImageView.transform = CGAffineTransformMakeScale(1 - (scrollOffset / (self.scrollingHeaderView.navbarViewFadingOffset - [PDPandaPersonCenterCell calculationCellHeight])), 1 - (scrollOffset / (self.scrollingHeaderView.navbarViewFadingOffset - [PDPandaPersonCenterCell calculationCellHeight])));
    } else {
        self.scrollingHeaderView.backgroundImageView.transform = CGAffineTransformMakeScale(1.0, 1.0);
    }
}

#pragma mark 2.3 【创建右侧按钮】
-(void)createRightMessageButton{
    UIButton *messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    messageButton.frame = CGRectMake(self.tempRightButton.orgin_x, 20, self.tempRightButton.size_width, 44);
    [messageButton setImage:[UIImage imageNamed:@"icon_panda_message_nor"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [messageButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf directToMessage];
    }];
    
    [self.view addSubview:messageButton];
    self.messageButton = messageButton;
}

#pragma mark 2.4 【创建NavBar】
-(void)createNavBar{
    // 1. 创建导航栏
    self.navBarView = [[UIView alloc]init];
    self.navBarView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.navBarView.clipsToBounds = YES;
    self.navBarView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 64);
    self.navBarView.layer.shadowColor = [[UIColor colorWithCustomerName:@"灰"] CGColor];
    self.navBarView.layer.shadowOpacity = 6.0;
    self.navBarView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    [self.view addSubview:self.navBarView];
    
    // 2. 创建消息按钮
    self.navBarMessageBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.navBarMessageBtn setImage:[UIImage imageNamed:@"icon_panda_message_hlt"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.navBarMessageBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf directToMessage];
    }];
    self.navBarMessageBtn.frame = CGRectMake(self.tempRightButton.orgin_x, 20, self.tempRightButton.size_width, 44);
    [self.navBarView addSubview:self.navBarMessageBtn];
    
    // 3. 创建PandaOL
    self.barMainTitleImageView = [[PDImageView alloc]init];
    self.barMainTitleImageView.image = [UIImage imageNamed:@"pandaol_logo_r"];
    self.barMainTitleImageView.backgroundColor = [UIColor clearColor];
    self.barMainTitleImageView.frame = CGRectMake((kScreenBounds.size.width - 227 /2.) / 2., self.navBarView.size_height , 227 /2., 21/2.);
    [self.navBarView addSubview:self.barMainTitleImageView];
    
    // 4. 添加
    self.scrollingHeaderView.navbarView = self.navBarView;
    self.scrollingHeaderView.navbarView.layer.shadowColor = [[UIColor colorWithCustomerName:@"灰"] CGColor];
    self.scrollingHeaderView.navbarView.layer.shadowOpacity = 6.0;
    self.scrollingHeaderView.navbarView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
}

#pragma mark 2.5 【创建TopLabel】
-(void)createTopView{
    self.topLabel = [[PDPandaHomeTopLabel alloc]initWithFrame:CGRectMake(0,  - LCFloat(11) - 40, kScreenBounds.size.width, 40)];
    CGFloat origin_y = self.scrollingHeaderView.navbarViewFadingOffset - self.topLabel.size_height - LCFloat(15);
    self.topLabel.orgin_y = origin_y;
    [self.view addSubview:self.topLabel];
}


#pragma mark - 【UITableViewDataSource】
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.homeMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr =  [self.homeMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"个人中心"] && indexPath.row == [self cellIndexPathRowWithcellData:@"个人中心"]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PDPandaPersonCenterCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[PDPandaPersonCenterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferHomeRootModel = pandaHomeRootModel;
        
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"战绩中心"] && indexPath.row == [self cellIndexPathRowWithcellData:@"战绩中心"]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        PDPandaRecordCenter *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[PDPandaRecordCenter alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferHomeRootModel = pandaHomeRootModel;
        
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新夺宝"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            PDPandaNormalCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[PDPandaNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferInfo = @"最新夺宝";
            return cellWithRowOne;
        } else {                        // 【夺宝信息】
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            PDPandaIndianaCell  *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[PDPandaIndianaCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.delegate = self;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferIndianaArr = self.indianaMutableArr;
            
            return cellWithRowTwo;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"附近的网吧"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            PDPandaNormalCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[PDPandaNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
            cellWithRowFiv.transferInfo = @"附近的网吧";
            return cellWithRowFiv;
        } else {
            static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
            PDInternetCafesSingleCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
            if (!cellWithRowSex){
                cellWithRowSex = [[PDInternetCafesSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
                cellWithRowSex.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowSex.transferCellheight = cellHeight;
            cellWithRowSex.transferInternetCafesSingleModel = [[self.homeMutableArr objectAtIndex:indexPath.section ] objectAtIndex:indexPath.row];
            return cellWithRowSex;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"比赛详情"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"比赛详情"]){
            static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
            PDRoleRecordSingleCell  *cellWithRowEig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
            if (!cellWithRowEig){
                cellWithRowEig = [[PDRoleRecordSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
                cellWithRowEig.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowEig.transferCellHeight = cellHeight;
            cellWithRowEig.transferHomeRecordModel = pandaHomeRootModel.record;
            return cellWithRowEig;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"赛事竞猜"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"赛事竞猜"]){
            static NSString *cellIdentifyWithRowNig = @"cellIdentifyWithRowNig";
            PDPandaNormalCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowNig];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[PDPandaNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowNig];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
            cellWithRowFiv.transferInfo = @"赛事竞猜";
            return cellWithRowFiv;
        } else {
            static NSString *cellIdentifyWithRowTen = @"cellIdentifyWithRowTen";
            PDPandaLotteryTableViewCell *cellWithRowTen = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTen];
            if (!cellWithRowTen){
                cellWithRowTen = [[PDPandaLotteryTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTen];
                cellWithRowTen.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTen.transferCellHeight = cellHeight;
            PDLoteryGameRootSubSingleModel *subListModel = [[self.homeMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];

            cellWithRowTen.transferGameRootSubSingleModel = subListModel;

            return cellWithRowTen;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"福利任务"] && indexPath.row == [self cellIndexPathRowWithcellData:@"福利任务"]){
        static NSString *cellIdentifyWithRowEle = @"cellIdentifyWithRowEle";
        PDPandaRootRenwuAndFuliMainCell *cellWithRowEle = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEle];
        if (!cellWithRowEle){
            cellWithRowEle = [[PDPandaRootRenwuAndFuliMainCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEle];
            cellWithRowEle.selectionStyle = UITableViewCellSelectionStyleNone;

        }
        cellWithRowEle.transferCellHeight = cellHeight;
        __weak typeof(self)weakSelf = self;
        [cellWithRowEle directWithtype:^(PDPandaRootRenwuAndFuliMainCellDirectType directType) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (directType == PDPandaRootRenwuAndFuliMainCellDirectTypeFuli){           // 福利
                if ([AccountModel sharedAccountModel].isShenhe == YES ){
                    PDInviteViewController *yaoqingVC = [[PDInviteViewController alloc]init];
                    [yaoqingVC hidesTabBarWhenPushed];
                    [self.navigationController pushViewController:yaoqingVC animated:YES];
                    return;
                }
                
                PDShopRootMainViewController *shopViewController = [[PDShopRootMainViewController alloc]init];
                [shopViewController hidesTabBarWhenPushed];
                [strongSelf.navigationController pushViewController:shopViewController animated:YES];
            } else if (directType == PDPandaRootRenwuAndFuliMainCellDirectTypeRenwu){       // 任务
                if ([AccountModel sharedAccountModel].isShenhe == YES){
                    PDWebViewController *webViewController = [[PDWebViewController alloc] init];
                    [webViewController webDirectedWebUrl:amusementTheGuide];
                    [self pushViewController:webViewController animated:YES];
                    return;
                }
                
                
                
                [strongSelf authorizeWithCompletionHandler:^{
                    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
                        PDMyTaskViewController *taskViewController = [[PDMyTaskViewController alloc]init];
                        [taskViewController hidesTabBarWhenPushed];
                        [strongSelf.navigationController pushViewController:taskViewController animated:YES];
                    }
                }];
            }
        }];
        return cellWithRowEle;
    }
    else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell  *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.backgroundColor = [UIColor redColor];
        }
        return cellWithRowOne;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if ((indexPath.section == [self cellIndexPathSectionWithcellData:@"个人中心"] && indexPath.row == [self cellIndexPathRowWithcellData:@"个人中心"]) || (indexPath.section == [self cellIndexPathSectionWithcellData:@"比赛详情"])){
        PDPandaRoleDetailViewController *pandaRoleDetailViewController = [[PDPandaRoleDetailViewController alloc]init];
        pandaRoleDetailViewController.transferLolGameUserId = pandaHomeRootModel.memberLOLGameUser.lolGameUser.gameUserId;
        [pandaRoleDetailViewController hidesTabBarWhenPushed];
        __weak typeof(self)weakSelf = self;
        [pandaRoleDetailViewController bindingCancelWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSlef = weakSelf;
            [strongSlef removeAccountInfoSet];
            [[PDCenterViewController sharedController] removeMemberGameUser];
        }];
        
        [pandaRoleDetailViewController activityInfoWithBlock:^(BindingStatus bindingStatus) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (bindingStatus == BindingStatusActiviting){
                strongSelf->pandaHomeRootModel.memberLOLGameUser.state = @"activating";
            } else if (bindingStatus == BindingStatusActivited){
                strongSelf->pandaHomeRootModel.memberLOLGameUser.state = @"activated";
            } else if (bindingStatus == BindingStatusNoActivity){
                strongSelf->pandaHomeRootModel.memberLOLGameUser.state = @"bind";
            }
            NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"个人中心"];
            NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"个人中心"];
            
            [strongSelf.scrollingHeaderView.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationBottom];
            
            // 【修改个人中心】
            [[PDCenterViewController sharedController] fetchMemberGameUser];
        }];
        
        [self.navigationController pushViewController:pandaRoleDetailViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"附近的网吧"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"附近的网吧"]){
            PDInternetCafesRootViewController *internetCafesRootViewController = [[PDInternetCafesRootViewController alloc]init];
            internetCafesRootViewController.transferInternetCafeType = InternetCafesTypeNearBy;
            [internetCafesRootViewController hidesTabBarWhenPushed];
            [self.navigationController pushViewController:internetCafesRootViewController animated:YES];
        } else {
            PDInternetCafesSingleModel *internetCafesSingleModel = [[self.homeMutableArr objectAtIndex:indexPath.section ] objectAtIndex:indexPath.row];
            PDInternetCafesDetailViewController *internetCafesDetailViewController = [[PDInternetCafesDetailViewController alloc]init];
            [internetCafesDetailViewController hidesTabBarWhenPushed];
            internetCafesDetailViewController.transferInterNetCafesSingleModel = internetCafesSingleModel;
            [self.navigationController pushViewController:internetCafesDetailViewController animated:YES];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新夺宝"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"最新夺宝"]){          // 最新夺宝详情
            PDShopRootMainViewController *shopRootViewController = [[PDShopRootMainViewController alloc]init];
            [shopRootViewController hidesTabBarWhenPushed];
            [self.navigationController pushViewController:shopRootViewController animated:YES];
        } else {
            
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"赛事竞猜"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"赛事竞猜"]){
            PDLotteryGameMainViewController *lotteryGameViewController = [[PDLotteryGameMainViewController alloc]init];
            PDNavigationController *lotteryRootNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
            [lotteryRootNav setViewControllers:@[lotteryGameViewController]];
            
            lotteryRootNav.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
            lotteryRootNav.navigationBar.layer.shadowOpacity = 2.0;
            lotteryRootNav.navigationBar.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
            
            [self presentViewController:lotteryRootNav animated:YES completion:NULL];
        } else {
           PDLoteryGameRootSubSingleModel *singleModel = [[self.homeMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            
            PDLotteryGameDetailMainViewController *lotteryGameDetailViewController = [[PDLotteryGameDetailMainViewController alloc]init];
            [lotteryGameDetailViewController hidesTabBarWhenPushed];
            lotteryGameDetailViewController.transferMatchId = singleModel.matchId;
            [self.navigationController pushViewController:lotteryGameDetailViewController animated:YES];
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.scrollingHeaderView.tableView) {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最近比赛"] && indexPath.row == [self cellIndexPathRowWithcellData:@"最近比赛"]){
            [cell addSeparatorLineWithType:SeparatorTypeSingle];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新夺宝"] && indexPath.row == [self cellIndexPathRowWithcellData:@"最新夺宝"]){
            [cell addSeparatorLineWithType:SeparatorTypeSingle];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"附近的网吧"]){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType  = SeparatorTypeHead;
            } else if ([indexPath row] == [[self.homeMutableArr objectAtIndex:indexPath.section] count] - 1) {
                separatorType  = SeparatorTypeBottom;
            } else {
                separatorType  = SeparatorTypeMiddle;
            }
            if ([[self.homeMutableArr objectAtIndex:indexPath.section] count] == 1) {
                separatorType  = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新夺宝"]){
        if (indexPath.row == 0){
            return [PDPandaNormalCell calculationCellHeight];
        } else {
            return 2 * [PDPandaIndianaCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"个人中心"] && indexPath.row == [self cellIndexPathRowWithcellData:@"个人中心"]){
        return [PDPandaPersonCenterCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"战绩中心"] && indexPath.row == [self cellIndexPathRowWithcellData:@"战绩中心"]){
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"比赛详情"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"比赛详情"]){
            return  [PDRoleRecordSingleCell calculationCellHeight];
        } else {
            return 44;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"附近的网吧"]){
        if (indexPath.row == 0){
            return 44;
        } else {
            return [PDInternetCafesSingleCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"赛事竞猜"]){
        if (indexPath.row == 0){
            return 44;
        } else {
            return [PDPandaLotteryTableViewCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"福利任务"]){
        return [PDPandaRootRenwuAndFuliMainCell calculationCellHeight];
    }
    return 44;
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return LCFloat(10);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    return headerView;
}

#pragma mark - 【创建泡泡】
-(void)createPaopaoView{
    self.paopaoImageView = [[PDPaopaoView alloc]initWithFrame:CGRectMake((kScreenBounds.size.width - PaoPao_Size) / 2., 40, PaoPao_Size, PaoPao_Size)];
    [self.view addSubview:self.paopaoImageView];
    self.paopaoImageView.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.paopaoImageView tapManager:^{               // 点击登录注册
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf paopaoTapManager];
    }];
    [self.paopaoImageView startAnimation];
    [self paopaoManager];                           // 添加泡泡的手势方法
}

-(void)paopaoTapManager{
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){             // 【账号登录】
        if (pandaHomeRootModel.isBind){                                     // 已经绑定
            [self paopaoManagerToReoloadInfo];
        } else {                                                            // 没有绑定->跳转绑定
            PDBindingInfoViewController *bindingVC = [[PDBindingInfoViewController alloc]init];
            __weak typeof(self)weakSelf = self;
            [bindingVC bindingDidEndWithBlock:^(BOOL isbind, BOOL isActivity) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                // 设置新手引导
                strongSelf ->isBindEnd = YES;
                
                strongSelf ->pandaHomeRootModel.animationWillBinding = NO;
                [strongSelf sendRequestToGetPersonalInfoHasAnimation:YES block:NULL];              // 【绑定结束，增加头像动画】
                strongSelf->pandaHomeRootModel.isBind = isbind;
                // 修改【我的】
                if (isbind){
                    [[PDCenterViewController sharedController] fetchMemberGameUser];
                } else {
                    [[PDCenterViewController sharedController] removeMemberGameUser];
                }
            }];
            
            UINavigationController *bindingNav = [[UINavigationController alloc]initWithRootViewController:bindingVC];
            [self presentViewController:bindingNav animated:YES completion:NULL];
        }
    } else {                                                                // 【没有登录】
        self.headerImageView.hidden = YES;
        __weak typeof(self)weakSelf = self;
        PDLoginMainViewController *loginViewController = [[PDLoginMainViewController alloc]init];
        loginViewController.dismissBlock = ^(){
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetPersonalInfoHasAnimation:YES block:^{
                [strongSelf getGuidesList];
            }];
        };
        UINavigationController *loginNav = [[UINavigationController alloc]initWithRootViewController:loginViewController];
        [[PDMainTabbarViewController sharedController] presentViewController:loginNav animated:YES completion:NULL];
    }
}

-(void)paopaoAnimationManager{
    BOOL isPopAni = YES;
    if (self.isPanGesture || self.isPaopaoDynamicAni || self.isScroll){  //
        isPopAni = NO;
    }
    if (isPopAni){
        [self.paopaoImageView startAnimation];
    } else {
        [self.paopaoImageView stopAnimation];
    }
}



-(void)paopaoManager{
    [self createPaopaoAnimator];                    // 添加泡泡的动画
    [self addPanGesture];                           // 添加拖动手势
}

// 添加拖动手势
-(void)addPanGesture{
    self.paopaoPanGesture = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(paopaoPanGestureManager:)];
    [self.paopaoImageView addGestureRecognizer:self.paopaoPanGesture];
}

// 添加动画
-(void)createPaopaoAnimator{
    self.paopaoDynamicAniator = [[UIDynamicAnimator alloc]initWithReferenceView:self.view];
    self.paopaoDynamicAniator.delegate = self;
    [self paopaoViewUpdateSnapPoint];
}

// 修改当前的弹性动画
-(void)paopaoViewUpdateSnapPoint{
    self.paopaoCenterPoint = [self.paopaoImageView convertPoint:CGPointMake(self.paopaoImageView.bounds.size.width / 2, self.paopaoImageView.bounds.size.height / 2) toView:self.view];
    self.paopaoSnapBehavior = [[UISnapBehavior alloc] initWithItem:self.paopaoImageView snapToPoint:self.paopaoCenterPoint];
    self.paopaoSnapBehavior.damping = .2f;
}

-(void)paopaoPanGestureManager:(UIPanGestureRecognizer *)pan{
    // 如果在拖动的时候
    
    CGPoint panLocation = [pan locationInView:self.view];
    
    if (pan.state == UIGestureRecognizerStateBegan){        // 【开始拖动】
        self.isPanGesture = YES;
        UIOffset offset = UIOffsetMake(panLocation.x - self.paopaoCenterPoint.x, panLocation.y - self.paopaoCenterPoint.y);
        [self.paopaoDynamicAniator removeAllBehaviors];
        self.paopaoAttachmentBehavior = [[UIAttachmentBehavior alloc] initWithItem:self.paopaoImageView offsetFromCenter:offset attachedToAnchor:panLocation];
        [self.paopaoDynamicAniator addBehavior:self.paopaoAttachmentBehavior];
        [self paopaoAnimationManager];              // 判断泡泡是否要跳动
    } else if (pan.state == UIGestureRecognizerStateChanged) {      // 【拖动中】
        // 1. 如果在泡泡拖动中，那么拖动scrollView 泡泡不改变内容
        self.isPanGesture = YES;
        [self.paopaoAttachmentBehavior setAnchorPoint:panLocation];
    } else if (pan.state == UIGestureRecognizerStateEnded || pan.state == UIGestureRecognizerStateCancelled || pan.state == UIGestureRecognizerStateFailed) {                           // 【拖动结束】
        self.isPanGesture = NO;
        // 拖动结束
        // 2. 如果说pan松手了，获取scrollPointY然后进行缩放
        [self panEndManger];
        
        [self.paopaoDynamicAniator removeAllBehaviors];
        [self.paopaoDynamicAniator addBehavior:self.paopaoSnapBehavior];
        [self paopaoAnimationManager];              // 判断泡泡是否要跳动
    }
}

-(void)panEndManger{
    // 1. 获取当前泡泡高度
    CGFloat pointY = self.scrollingHeaderView.tableView.contentOffset.y;
    CGFloat zoom = 1 - (pointY / self.scrollingHeaderView.navbarViewFadingOffset);
    CGFloat size = PaoPao_Size *zoom;
    // 3. 获取间距
    CGFloat origin_y = - pointY - size + 40 + PaoPao_Size ;
    
    if (IS_IOS9_LATER){
        self.paopaoSnapBehavior.snapPoint = CGPointMake(kScreenBounds.size.width / 2., origin_y + size / 2.);
    }
    
}

- (void)dynamicAnimatorWillResume:(UIDynamicAnimator *)animator{                // 抖动开始
    self.isPaopaoDynamicAni = YES;
}
- (void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator{      NSLog(@"动画结束");
    if (self.isPanGesture){         // 当动画结束的时候才能进行拖动
        return;
    }
    self.isPaopaoDynamicAni = NO;
    
    CGFloat pointY = self.scrollingHeaderView.tableView.contentOffset.y;
    CGFloat zoom = 1 - (pointY / self.scrollingHeaderView.navbarViewFadingOffset);
    CGFloat size = PaoPao_Size *zoom;
    CGFloat origin_y = - pointY - size + 40 + PaoPao_Size ;
    [UIView animateWithDuration:1.f animations:^{
        self.paopaoImageView.frame = CGRectMake((kScreenBounds.size.width - size) / 2., origin_y, size, size);
        [self.paopaoImageView autoSettingWithSizeWithZoom:zoom];
    }];
    
    [self paopaoAnimationManager];
    
    // 【更新数据】
    [self paopaoManagerToReoloadInfo];
}

#pragma mark - 【Other Manager】
// 跳转到消息详情
-(void)directToMessage{

    PDMessageViewController *messageViewController = [[PDMessageViewController alloc] init];
    [self pushViewController:messageViewController animated:YES];
    
    [self.navBarMessageBtn clearNormalBadge];
    [self.messageButton clearNormalBadge];
}

-(void)pandaTreasuresDidselectedWithModel:(PDPandaTreasuresSingleModel *)treasureSingleModel{
    PDProductDetailViewController *productDetailViewController = [[PDProductDetailViewController alloc]init];
    productDetailViewController.productId = treasureSingleModel.productId;
    [productDetailViewController hidesTabBarWhenPushed];
    [self.navigationController pushViewController:productDetailViewController animated:YES];
}

-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.homeMutableArr.count ; i++){
        NSArray *dataTempArr = [self.homeMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.homeMutableArr.count ; i++){
        NSArray *dataTempArr = [self.homeMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}


#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint point = scrollView.contentOffset;
    [self scrollViewDidScrollWithNavBar:point.y];                   // 导航透明
    [self scrollViewDidScrollWithPaopao:point.y];                   // 气球缩放
    [self scrollViewDidScrollWithPaopaoAni];                        // 泡泡动画
    [self getCurrentBanbooInfoWithPoint:point.y];                   // 获取当前的【近10天获取信息】
    [self scrollViewDidScrollWithBackground:point.y];               // 修改背景图片缩放度
}

// 0.背景
-(void)scrollViewDidScrollWithBackground:(CGFloat)pointY{
    if (pointY <= 0) {
        CGFloat zoom = 1 - (pointY / self.scrollingHeaderView.navbarViewFadingOffset) + .5f;
        self.scrollingHeaderView.backgroundImageView.transform = CGAffineTransformMakeScale(zoom, zoom);
    }
}


// 1. 导航条增加文字
-(void)scrollViewDidScrollWithNavBar:(CGFloat)pointY{
    CGFloat fixedFloat = - self.navBarView.size_height + (self.navBarView.size_height - 20 - 14) / 2. + 5;
    CGFloat dymicFloat = self.scrollingHeaderView.navbarViewFadingOffset - pointY + USER_Nick_Height;
    
    CATransform3D labelTransform = CATransform3DMakeTranslation(0, MAX(fixedFloat, dymicFloat) + USER_Nick_Height, 0);
    self.barMainTitleImageView.layer.transform = labelTransform;
    self.barMainTitleImageView.layer.zPosition = 20;
}

// 2. 气球缩小
-(void)scrollViewDidScrollWithPaopao:(CGFloat)pointY{
    // 1. 如果泡泡pan ，拖动scrollView 不修改泡泡大小
    if (self.isPanGesture || self.isPaopaoDynamicAni){
        return;
    }
    
    CGFloat zoom = 1 - (pointY / self.scrollingHeaderView.navbarViewFadingOffset);
    CGFloat size = PaoPao_Size *zoom;
    CGFloat origin_y = - pointY - self.paopaoImageView.size_height + 40 + PaoPao_Size;
    self.paopaoImageView.frame = CGRectMake((kScreenBounds.size.width - size) / 2., origin_y, size, size);
    [self.paopaoImageView autoSettingWithSizeWithZoom:zoom];
}

// 3. 修改泡泡pop 动画
-(void)scrollViewDidScrollWithPaopaoAni{
    [self paopaoAnimationManager];
}

// 5. 更新最近获取的竹子
-(void)getCurrentBanbooInfoWithPoint:(CGFloat)pointY{
    // 1.计算origin_y
    self.topLabel.orgin_y = self.scrollingHeaderView.navbarViewFadingOffset - pointY + self.navBarView.size_height - self.topLabel.size_height - LCFloat(15);
    // 2. 降低透明度
    CGFloat zoom = 1 - (pointY / (self.scrollingHeaderView.navbarViewFadingOffset - LCFloat(15) - self.topLabel.size_height));
    self.topLabel.alpha = zoom;
}

//开始拖拽视图
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView; {
    self.isScroll = YES;
    DLog(@"开始拖拽");
    [self paopaoAnimationManager];              // 判断泡泡是否要跳动
}

//完成拖拽
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate; {
    DLog(@"完成拖拽");
    self.isScroll = NO;
    [self paopaoAnimationManager];              // 判断泡泡是否要跳动
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    self.isScroll = NO;
    DLog(@"滑动视图，当手指离开屏幕");
    [self paopaoAnimationManager];              // 判断泡泡是否要跳动
}


- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    self.isScroll = NO;
    DLog(@"// 滚动视图减速完成，滚动将停止时，调用该方法。一次有效滑动，只执行一次。");
    [self paopaoAnimationManager];              // 判断泡泡是否要跳动
  
}

#pragma mark - 3.1【登录成功之后动画】
-(void)loginSuccessAnimation{
    if (!self.headerImageView){
        self.headerImageView = [[PDImageView alloc]init];
        self.headerImageView.frame = CGRectMake(-LCFloat(150), -LCFloat(150), LCFloat(150), LCFloat(150));
        self.headerImageView.hidden = YES;
        self.headerImageView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.headerImageView];
        
        self.headerImageView.userInteractionEnabled = YES;
        
        UITapGestureRecognizer *headerImgViewGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerImgGestureManager)];
        [self.headerImageView addGestureRecognizer:headerImgViewGesture];
    }
    [self loginSuccessManager];
}

-(void)loginSuccessAnimationWithCenter{
    
    if (!self.headerImageView){
        self.headerImageView = [[PDImageView alloc]init];
        self.headerImageView.frame = CGRectMake(-LCFloat(150), -LCFloat(150), LCFloat(150), LCFloat(150));
        self.headerImageView.backgroundColor = [UIColor clearColor];
        __weak typeof(self)weakSelf = self;
        [self.headerImageView uploadImageWithAvatarURL:[AccountModel sharedAccountModel].avatar placeholder:nil callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.headerImageView.image = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
        }];
        
        [self.view addSubview:self.headerImageView];
    } else {
        self.headerImageView.hidden = YES;
    }
    
    CGRect convertFrame = CGRectMake(20, 20 + (44 - LCFloat(30)) / 2., LCFloat(30), LCFloat(30));

    self.headerImageView.frame = convertFrame;
    self.headerImageView.hidden = NO;
    
    self.headerImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *headerImgViewGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(headerImgGestureManager)];
    [self.headerImageView addGestureRecognizer:headerImgViewGesture];
    
}

-(void)headerImgGestureManager{
    PDEditViewController *editViewController = [[PDEditViewController alloc]init];
    [editViewController hidesTabBarWhenPushed];
    [self.navigationController pushViewController:editViewController animated:YES];
}

#pragma mark 3.2 【登录动画】
-(void)loginSuccessManager{
    __weak typeof(self)weakSelf = self;
    [self.headerImageView uploadImageWithAvatarURL:[AccountModel sharedAccountModel].avatar placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.imageUploadProgress = [[TNSexyImageUploadProgress alloc] init];
        strongSelf.imageUploadProgress.radius = LCFloat(100);
        strongSelf.imageUploadProgress.progressBorderThickness = -10;
        strongSelf.imageUploadProgress.trackColor = [UIColor blackColor];
        strongSelf.imageUploadProgress.progressColor = [UIColor colorWithCustomerName:@"黑"];
        strongSelf.imageUploadProgress.imageToUpload = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
        [strongSelf.imageUploadProgress show];
        [strongSelf performSelector:@selector(showSliderWithAnimation) withObject:nil afterDelay:.5f];
    }];
}

#pragma mark 3.3 【显示】
-(void)showSliderWithAnimation{
    [self performSelector:@selector(loginSuccessAnimationWithProView:) withObject:self.imageUploadProgress afterDelay:.3f];
}

#pragma mark 3.4 A【nimation Login】
-(void)loginSuccessAnimationWithProView:(TNSexyImageUploadProgress *)progressView{
    //  登录刷新竹子
    
    self.scrollingHeaderView.tableView.scrollEnabled = NO;
    
    // 2. 寻找到view的位置
    CGRect convertFrame =  CGRectMake(20, 20 + (44 - LCFloat(30)) / 2., LCFloat(30), LCFloat(30));

    progressView.imageView.backgroundColor = [UIColor whiteColor];
    
    // 3. 创建一个副本图像
    UIImageView *animationImageView = [[UIImageView alloc]init];
    animationImageView.image = progressView.imageView.image ?progressView.imageView.image:progressView.imageToUpload;
    animationImageView.frame = progressView.imageView.frame;
    [self.view.window addSubview:animationImageView];
    
    [progressView outroAnimation];
    [UIView animateWithDuration:.9  delay:.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        animationImageView.frame = convertFrame;
    } completion:^(BOOL finished) {
        animationImageView.hidden = YES;
        [animationImageView removeFromSuperview];
        self.headerImageView.frame = convertFrame;
        self.headerImageView.hidden = NO;
        self.scrollingHeaderView.tableView.scrollEnabled = YES;
        pandaHomeRootModel.animationWillBinding = YES;
    }];
}

#pragma mark - 【刷新数据】
-(void)paopaoManagerToReoloadInfo{
    if ([AccountModel sharedAccountModel].isShenhe){
        return;
    }
    // 进行刷新
    if (pandaHomeRootModel.isBind == YES){              // 绑定了，就去更新数据，否则不更新数据
        [self userToUploadHomePage];
    }
    __weak typeof(self)weakSelf = self;
    [weakSelf sendReqeustToGetTreasures];                       // 获取最新夺宝信息
//    [weakSelf sendRequestToGetGuessList];                       // 获取最新竞猜信息
}


#pragma mark - 【接口】
#pragma mark 4.1  【获取当前的网吧】
-(void)sendRequestToGetNetBarList{
    __weak typeof(self)weakSelf = self;
    
    NSDictionary *params = @{@"latitude":@([PDCurrentLocationManager sharedLocation].lat),@"longtitude":@([PDCurrentLocationManager sharedLocation].lng),@"page":@"1"};
    [[NetworkAdapter sharedAdapter] fetchWithPath:neearBars requestParams:params responseObjectClass:[PDInternetCafesListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDInternetCafesListModel *internetCafesList = (PDInternetCafesListModel *)responseObject;
            if (internetCafesList.items.count){
                if (strongSelf.netBarMutableArr.count){
                    [strongSelf.netBarMutableArr removeAllObjects];
                }
                [strongSelf.netBarMutableArr addObject:@"附近的网吧"];
                [strongSelf.netBarMutableArr addObjectsFromArray:internetCafesList.items];
            } else {
                if (strongSelf.netBarMutableArr.count){
                    [strongSelf.netBarMutableArr removeAllObjects];
                }
                [strongSelf.netBarMutableArr addObject:@"附近的网吧"];
            }
            // 刷新网吧
            [strongSelf getHomeInfoWithNetBar];
        }
    }];
}

#pragma mark - 【刷新数据 - 网吧】
-(void)getHomeInfoWithNetBar{
    if ([self cellIndexPathSectionWithcellData:@"夺宝详情"] != -1){             // 【有】夺宝详情
        if ([self cellIndexPathSectionWithcellData:@"附近的网吧"] != -1){                // 有附近的网吧,进行刷新
            NSInteger netbarIndex = [self cellIndexPathSectionWithcellData:@"附近的网吧"];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:netbarIndex];
            [self.scrollingHeaderView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
        } else {                                                                        // 没有附近网吧，进行插入
            NSInteger index = [self cellIndexPathSectionWithcellData:@"夺宝详情"] + 1;
            [self.homeMutableArr insertObject:self.netBarMutableArr atIndex:index];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
            [self.scrollingHeaderView.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationMiddle];
        }
    } else {                                                                    // 没有夺宝详情
        if ([self cellIndexPathSectionWithcellData:@"个人中心"] != -1){             // 有个人中心，插入到个人中心后面
            if ([self cellIndexPathSectionWithcellData:@"附近的网吧"] != -1){        // 存在附近的网吧
                NSInteger netbarIndex = [self cellIndexPathSectionWithcellData:@"附近的网吧"];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:netbarIndex];
                [self.scrollingHeaderView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
            } else {
                NSInteger index = [self cellIndexPathSectionWithcellData:@"个人中心"] + 1;
                [self.homeMutableArr insertObject:self.netBarMutableArr atIndex:index];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
                [self.scrollingHeaderView.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationMiddle];
            }
        } else {                                                                    // 插入到第一行
            if ([self cellIndexPathSectionWithcellData:@"附近的网吧"] != -1){        // 存在附近的网吧
                NSInteger netbarIndex = [self cellIndexPathSectionWithcellData:@"附近的网吧"];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:netbarIndex];
                [self.scrollingHeaderView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
            } else {
                [self.homeMutableArr insertObject:self.netBarMutableArr atIndex:0];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
                [self.scrollingHeaderView.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationMiddle];
            }
        }
    }
}


#pragma mark  4.2 【获取当前最新夺宝】
-(void)sendReqeustToGetTreasures{
    __weak typeof(self)weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:homeTreasures requestParams:nil responseObjectClass:[PDPandaTreasuresListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDPandaTreasuresListModel *treasuresListModel = (PDPandaTreasuresListModel *)responseObject;
            if (strongSelf.indianaMutableArr.count){
                [strongSelf.indianaMutableArr  removeAllObjects];
            }
            
            for (int i = 0 ;i <treasuresListModel.items.count;i++){
                PDPandaTreasuresSingleModel *item = [treasuresListModel.items objectAtIndex:i];
                item.drawTime = [NSDate getNSTimeIntervalWithCurrent] + item.leftTime;
            }
            
            [strongSelf.indianaMutableArr addObjectsFromArray:treasuresListModel.items];
            // 刷新最新夺宝
            [strongSelf getHomeInfoWithDuobaoInfo];
        }
    }];
}

#pragma mark - 【刷新数据 - 夺宝】
-(void)getHomeInfoWithDuobaoInfo{
    // 1. 判断是否有夺宝信息
    if([self cellIndexPathSectionWithcellData:@"夺宝详情"] != -1){              // 已经存在【刷新】
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:[self cellIndexPathSectionWithcellData:@"夺宝详情"]];
        [self.scrollingHeaderView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
    } else {                                                                   // 没存在【新增】
        NSArray *dataArr = @[@"最新夺宝",@"夺宝详情"];
        
        // 1. 判断是否有赛事猜
        NSInteger insertIndex = -1;
        
        if (pandaHomeRootModel && [self cellIndexPathSectionWithcellData:@"赛事竞猜"] != -1){   // 【如果有赛事竞猜，就插入到赛事竞猜之后】
            insertIndex = [self cellIndexPathSectionWithcellData:@"赛事竞猜"] + 1;
        } else {
            if (pandaHomeRootModel && ([self cellIndexPathSectionWithcellData:@"福利任务"] != -1)){         // 有个人信息
                insertIndex = [self cellIndexPathSectionWithcellData:@"福利任务"] + 1;
            } else {
                insertIndex = 1;
            }
            if (insertIndex == -1){
                return;
            }
            [self.homeMutableArr insertObject:dataArr atIndex:insertIndex];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:insertIndex];          // 最新夺宝的set
            [self.scrollingHeaderView.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationMiddle];
        }
    }
}

#pragma mark  4.3 【获取首页接口】
-(void)sendRequestToGetPersonalInfoHasAnimation:(BOOL)animation block:(void(^)())block{
    if (self.headerImageView.hidden == NO && self.headerImageView){
        self.headerImageView.hidden = YES;
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:membergameuserstat requestParams:nil responseObjectClass:[PDPandaHomeRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf -> pandaHomeRootModel = (PDPandaHomeRootModel *)responseObject;
            if (strongSelf->pandaHomeRootModel.memberLOLGameUser.memberId.length){
                [AccountModel sharedAccountModel].memberId = strongSelf->pandaHomeRootModel.memberLOLGameUser.memberId;
            }
            if (strongSelf->pandaHomeRootModel.memberLOLGameUser.avatar.length){
                [AccountModel sharedAccountModel].avatar = strongSelf->pandaHomeRootModel.memberLOLGameUser.avatar;
            }
            if (strongSelf->pandaHomeRootModel.memberLOLGameUser.lolGameUser.roleName.length){
                [AccountModel sharedAccountModel].nickname = strongSelf->pandaHomeRootModel.memberLOLGameUser.lolGameUser.roleName;
            }
            
            // 修改头像
            [strongSelf.headerImageView uploadImageWithAvatarURL:[AccountModel sharedAccountModel].avatar placeholder:nil callback:^(UIImage *image) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                strongSelf.headerImageView.image = [Tool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
            }];
            
            // 首页信息更新
            [strongSelf getHomeInfoWithUserInfoWithBool:animation];
            
            // 添加userId
            [AccountModel sharedAccountModel].lolGameUser = strongSelf->pandaHomeRootModel.memberLOLGameUser.lolGameUser.gameUserId;

            // 增加新手引导
            if (isBindEnd){
                [strongSelf showGuideManager];
                isBindEnd = NO;
            }
            if (block){
                block();
            }
        }
    }];
}


#pragma mark - 【手动更新首页信息】
-(void)userToUploadHomePage{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberLolSettle requestParams:nil responseObjectClass:[PDPandaHomeRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf -> pandaHomeRootModel = (PDPandaHomeRootModel *)responseObject;
            // 首页信息更新
            [strongSelf getHomeInfoWithUserInfoWithBool:NO];
        }
    }];
}


#pragma mark 【刷新数据- 战绩】
-(void)getHomeInfoWithUserInfoWithBool:(BOOL)isRefresh{
    self.headerImageView.hidden = YES;
    if (pandaHomeRootModel.isBind){          // 已经绑定
        if (pandaHomeRootModel.memberLOLGameUser){              // 判断是否有账户条
            if ([self cellIndexPathSectionWithcellData:@"个人中心"] == -1){
                [self.recordMutableArr addObject:@"个人中心"];                  // 如果没有个人中心，增加个人中心
            }
        }
//        if (pandaHomeRootModel.record){
//            if ([self cellIndexPathSectionWithcellData:@"比赛详情"] == -1){
//                [self.recordMutableArr addObjectsFromArray:@[@"比赛详情"]];
//            }
//        }
//        
        if (![self.homeMutableArr containsObject:self.recordMutableArr]){           // 没有存在【战绩中心就插入】
            [self.homeMutableArr insertObject:self.recordMutableArr atIndex:0];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
            [self.scrollingHeaderView.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationMiddle];
            
        } else {
            NSInteger secIndex = [self cellIndexPathSectionWithcellData:@"个人中心"];
            if (secIndex != -1){
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:secIndex];
                [self.scrollingHeaderView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            }
        }
        
        
        // 【更新泡泡】
        [self.paopaoImageView paopaoTypeManager:paopaoViewTypeZhuzi number:pandaHomeRootModel.todayGoldIncrease];
        // 5. 更新【最近10天得到金币数量】
        [self uploadCurrentGoldInfo];
        
        // 6. 更新登录的头像
        [AccountModel sharedAccountModel].avatar = pandaHomeRootModel.memberLOLGameUser.avatar.length?pandaHomeRootModel.memberLOLGameUser.avatar:@"";
        
        if (isRefresh){                         // 【使用loading动画】
            
            [self loginSuccessAnimation];
        } else {                                // 【不使用loading动画】

            [self loginSuccessAnimationWithCenter];
        }
        
    } else {                                    // 没有绑定
        // 【更新泡泡】
        [self.paopaoImageView paopaoTypeManager:paopaoViewTypeNoBinding number:0];
    }
}

#pragma mark - 【移除绑定账户信息】
-(void)removeAccountInfoSet{
//    [self.headerImageView removeFromSuperview];
//    self.headerImageView = nil;
    
    if ([self cellIndexPathSectionWithcellData:@"个人中心"] != -1){             // 存在
        NSInteger personalIndex = [self cellIndexPathSectionWithcellData:@"个人中心"];
        NSIndexSet *indeSet = [NSIndexSet indexSetWithIndex:personalIndex];
        if ([self.homeMutableArr containsObject:self.recordMutableArr] && self.recordMutableArr.count){
            [self.homeMutableArr removeObject:self.recordMutableArr];
            [self.recordMutableArr removeAllObjects];
            [self.scrollingHeaderView.tableView deleteSections:indeSet withRowAnimation:UITableViewRowAnimationMiddle];
        }
    }
    if (pandaHomeRootModel.isBind == YES){
        pandaHomeRootModel.isBind = NO;
    }
    
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        [self.paopaoImageView paopaoTypeManager:paopaoViewTypeNoBinding number:0];
    } else {
        [self.paopaoImageView paopaoTypeManager:paopaoViewTypeNormal number:0];
    }
}




#pragma mark 4.3.2 【更新10天数量】
-(void)uploadCurrentGoldInfo{
    [self.topLabel animationWithTime:self -> pandaHomeRootModel.days moneyCount:self -> pandaHomeRootModel.recentlyGoldIncrease];
    
    self.topLabel.orgin_y = self.scrollingHeaderView.navbarViewFadingOffset - self.scrollingHeaderView.tableView.contentOffset.y + self.navBarView.size_height - self.topLabel.size_height - LCFloat(15);
}

#pragma mark 4.4 【修改会员当前的经纬度】
-(void)sendRequestToChangePositionWithLat:(CGFloat)lat lng:(CGFloat)lng{
    __weak typeof(self)weakSelf = self;
    
//    CLLocationCoordinate2D location = [MMHLocationNavigationManager transferLocation];
    
    NSDictionary *params = @{@"latitude":@(lat),@"longtitude":@(lng)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:changePosition requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            
        }
    }];
}

#pragma mark - 如果时间到了就去刷新
-(void)timeOutToNotificationRootInfoManager{
    return;
    __weak typeof(self)weakSelf = self;
    [weakSelf sendReqeustToGetTreasures];
}


#pragma mark - messageHasRead
-(void)messageHasReadManager:(BOOL)hasHotPort{
    if (hasHotPort){                    /**< 有红点*/
        
    } else {
        
    }
}

#pragma mark - 审核标记
-(void)sendRequestToGetShenheWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"version":[Tool appVersion]};
    [[NetworkAdapter sharedAdapter] fetchWithPath:shenhe requestParams:params responseObjectClass:[PDAppleShenheModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!
            weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDAppleShenheModel *shenheModel = (PDAppleShenheModel *)responseObject;
            [AccountModel sharedAccountModel].isShenhe = shenheModel.enable;
#ifdef DEBUG        // 测试           // 【JAVA】
            [AccountModel sharedAccountModel].isShenhe = NO;
#else               // 线上

#endif
            // 刷新任务
            NSInteger renwuSec = [self cellIndexPathSectionWithcellData:@"福利任务"];
            NSInteger renwuRow = [self cellIndexPathRowWithcellData:@"福利任务"];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:renwuRow inSection:renwuSec];
            [strongSelf.scrollingHeaderView.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
        }
        if (block){
            block();
        }
    }];
}

#pragma mark -- 读取消息盒子状态

- (void)fetchMessageState {
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageState requestParams:nil responseObjectClass:[PDMessageState class] succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDMessageState *state = (PDMessageState *)responseObject;
            // 重置
            [AccountModel sharedAccountModel].messageState = state;
            
            if (state.hasUnReadMsg) {
                [weakSelf.messageButton addNormalBadgeWithColor:[UIColor redColor] borderColor:[UIColor clearColor]];
                [weakSelf.navBarMessageBtn addNormalBadgeWithColor:[UIColor redColor] borderColor:[UIColor clearColor]];
            } else {
                [weakSelf.messageButton clearNormalBadge];
                [weakSelf.navBarMessageBtn clearNormalBadge];
            }
        }
    }];
}

#pragma mark - 获取竞猜信息
-(void)sendRequestToGetGuessList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberGuessMatchguess requestParams:nil responseObjectClass:[PDLoteryGameRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDLoteryGameRootListModel *lotteryList = (PDLoteryGameRootListModel *)responseObject;
            
            
            for (int i = 0 ; i < lotteryList.lotteryList.count;i++){
                // 1. 获取当前所有的赛事
                PDLoteryGameRootSingleModel *lotteryGameRootSingleModel = [lotteryList.lotteryList objectAtIndex:i];
                for (int j = 0 ; j < lotteryGameRootSingleModel.subList.count;j++){
                    PDLoteryGameRootSubSingleModel *subSingleModel = [lotteryGameRootSingleModel.subList objectAtIndex:j];
                    subSingleModel.gameStartTempTime = [NSDate getNSTimeIntervalWithCurrent] + subSingleModel.matchLeftTime;
                    if (j == 0){
                        subSingleModel.currentTempTime = lotteryGameRootSingleModel.dateTime;
                    } else {
                        subSingleModel.currentTempTime = -1;
                    }
                    
                    for (int k = 0 ; k < subSingleModel.lotteryDetailList.count;k++){
                        PDLotteryGameSubListSingleModel *subListSingleModel = [subSingleModel.lotteryDetailList objectAtIndex:k];
                        subListSingleModel.endTime = [NSDate getNSTimeIntervalWithCurrent] + subListSingleModel.leftTime;
                    }
                }
            }
            
            
            if (lotteryList.lotteryList.count){
                if (strongSelf.lotteryMutableArr.count){
                    [strongSelf.lotteryMutableArr removeAllObjects];
                }
                [strongSelf.lotteryMutableArr addObject:@"赛事竞猜"];
                
                PDLoteryGameRootSingleModel *lotteryGameRootSingleModel = [lotteryList.lotteryList firstObject];
            
                [strongSelf.lotteryMutableArr addObjectsFromArray:lotteryGameRootSingleModel.subList];
                
            } else {
                if (strongSelf.lotteryMutableArr.count){
                    [strongSelf.lotteryMutableArr removeAllObjects];
                }
                [strongSelf.lotteryMutableArr addObject:@"赛事竞猜"];
            }
            // 进行刷新
            [strongSelf getHomeInfoWithGuessInfo];
        }
    }];
}

#pragma mark - 【刷新数据 - 竞猜】
-(void)getHomeInfoWithGuessInfo{
    // 1. 判断是否有个人中心
    if ([self cellIndexPathSectionWithcellData:@"赛事竞猜"] != -1){             // 已经存在 【刷新】
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:[self cellIndexPathSectionWithcellData:@"赛事竞猜"]];
        [self.scrollingHeaderView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationFade];
        
        
        
    
    } else {            // 没有存在【插入】
        // 判断是否有个人中心
        if (pandaHomeRootModel && ([self cellIndexPathSectionWithcellData:@"个人中心"] != -1)){ // 【有个人中心】
            NSInteger personalIndex = [self cellIndexPathSectionWithcellData:@"个人中心"];
            [self.homeMutableArr insertObject:self.lotteryMutableArr atIndex:(personalIndex + 1)];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:(personalIndex + 1)];          // 最新夺宝的set
            [self.scrollingHeaderView.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationMiddle];
        } else {
            [self.homeMutableArr insertObject:self.lotteryMutableArr atIndex:0];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];          // 最新夺宝的set
            [self.scrollingHeaderView.tableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationMiddle];
        }
    }
}

-(void)uploadUserAvatar:(UIImage *)avatar{
    if (self.headerImageView){
        self.headerImageView.image = [Tool imageByComposingImage:avatar withMaskImage:[UIImage imageNamed:@"round"]];
    }
}

// 修改当前认证状态
-(void)uploadCurrentRenzhengStatus:(NSString *)status{
    PDPandaPersonCenterCell *cell = (PDPandaPersonCenterCell *)[self.scrollingHeaderView.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"个人中心"] inSection: [self cellIndexPathSectionWithcellData:@"个人中心"]]];
    if (cell){
        [cell changeRenzhengStatus:status];
    }
}



#pragma mark - show Guide
-(void)getGuidesList{
    // 3. 获取内容
    __weak typeof(self)weakSelf = self;
    [[AccountModel sharedAccountModel] sendRequestToGetNewFeatureListWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            [strongSelf showGuideManager];
        }
    }];
}


-(void)showGuideManager{
//    if (pandaHomeRootModel.isBind && [AccountModel sharedAccountModel].bindGameUser == NO){         // 已经绑定
//        PDGuideRootViewController *guideRootViewController = [[PDGuideRootViewController alloc]init];
//        PDGuideSingleModel *paopaoModel = [[PDGuideSingleModel alloc]init];
//        paopaoModel.transferShowFrame = self.paopaoImageView.frame;
//        paopaoModel.index = 1;
//        paopaoModel.text = @"点击下拉气泡进行刷新战绩";
//        paopaoModel.guideType = GuideGuesterTypeDrop;
//        [guideRootViewController showInView:self.parentViewController withViewActionArr:@[paopaoModel]];
//        [guideRootViewController bindingEndDropManager:^{
//
//        }];
//    } else if (pandaHomeRootModel.isBind == NO && [AccountModel sharedAccountModel].toBindGameUser == NO){                                // 没有绑定
//        PDGuideRootViewController *guideRootViewController = [[PDGuideRootViewController alloc]init];
//        PDGuideSingleModel *paopaoModel = [[PDGuideSingleModel alloc]init];
//        paopaoModel.transferShowFrame = self.paopaoImageView.frame;
//        paopaoModel.index = 1;
//        paopaoModel.text = @"点击气泡绑定召唤师";
//        paopaoModel.guideType = GuideGuesterTypeTap;
//        [guideRootViewController showInView:self.parentViewController withViewActionArr:@[paopaoModel]];
//        [guideRootViewController actionToClickPaopaoBindingRoleBlock:^{                 // 点击气泡绑定召唤师
//            
//        }];
//    }
    
    
    [PDCenterTool firstShowGuide];
}

#pragma mark - 获取当前的信息
-(void)getCurrentRoleInfo{
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:myInfo requestParams:nil responseObjectClass:[PDCenterMyInfo class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            PDCenterMyInfo *myInfo = (PDCenterMyInfo *)responseObject;
            [AccountModel sharedAccountModel].userId = myInfo.member.personId;
            [AccountModel sharedAccountModel].nickname = myInfo.member.nickname;
            [AccountModel sharedAccountModel].avatar = myInfo.member.avatar;
            [AccountModel sharedAccountModel].gold = myInfo.gold;
            [AccountModel sharedAccountModel].bamboo = myInfo.bamboo;
        }
    }];
}


-(void)showNewFeature{
    [PDCenterTool firstShowGuide];
}


#pragma mark - 旋转
- (BOOL)shouldAutorotate{
    //是否允许转屏
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //viewController所支持的全部旋转方向
    return UIInterfaceOrientationMaskPortrait ;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    //viewController初始显示的方向
    return UIInterfaceOrientationPortrait;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)orientation{
    if ((orientation == UIInterfaceOrientationPortrait) || (orientation == UIInterfaceOrientationLandscapeRight)){
        return YES;
    }
    return NO;
}



#pragma mark - 获取最新的上架信息
-(void)sendRequestToGetUpInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"appType":@"ios",@"moduleName":@"lolchampionkill"};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:appconfigNewmoduleFind requestParams:params responseObjectClass:[PDPandaNewmoduleSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        if (isSucceeded){
            PDPandaNewmoduleSingleModel *status = (PDPandaNewmoduleSingleModel *)responseObject;
            [AccountModel sharedAccountModel].gongnengLolchampionkill = status.up;
        }
    }];
}



@end
