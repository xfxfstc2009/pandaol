//
//  PDPandaViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaViewController.h"
#import "KMScrollingHeaderView.h"
#import "PDPandaNormalCell.h"                       // 最新夺宝等title
#import "PDPandaPersonCenterCell.h"                 // 个人中心
#import "PDPandaRecordCenter.h"                     // 战绩中心
#import "PDInternetCafesSingleCell.h"               // 网吧
#import "PDPandaIndianaCell.h"                      // 夺宝

// test
#import "PDInternetCafesListModel.h"              // 网吧
#import "PDPandaCenterModel.h"                      // 个人中心信息
#import "PDInternetCafesRootViewController.h"
#import "PDIndianaSingleModel.h"                    // 夺宝信息
#import "PDRecordSingleModel.h"
#import "PDPandaMatchSingleModel.h"
#import "PDPandaMatchInfoCell.h"                    // 比赛
#import "PDNetBarMapViewController.h"
#import "PDBindingInfoViewController.h"             // 绑定
#import "PDBindingRoleDetailViewController.h"       // 绑定角色详情
#import "PDPaopaoView.h"
#import "PDPandaRoleDetailViewController.h"
#import "PDPandaRootViewController.h"


CGFloat const offset_HeaderStop = 40.0;
CGFloat const offset_B_LabelHeader = 95.0;
CGFloat const distance_W_LabelHeader = 35.0;

@interface PDPandaViewController()<UIDynamicAnimatorDelegate,UITableViewDelegate,UITableViewDataSource,KMScrollingHeaderViewDelegate>
{
    PDPandaCenterModel *pandaCenterModel;
    PDPandaMatchSingleModel *pandaMatchSingleModel;
    BOOL isAnimation;
}
@property (nonatomic,strong)UITableView *pandaTableView;                    /**< 列表*/
@property (nonatomic,strong)NSMutableArray *pandaMutableArr;                /**< 数据源*/
@property (nonatomic,strong) KMScrollingHeaderView* scrollingHeaderView;    /**< 头部*/
@property (nonatomic,strong)NSMutableArray *netBarMutableArr;               /**< 网吧数组*/
@property (nonatomic,strong)NSMutableArray *indianaMutableArr;              /**< 夺宝信息*/
@property (nonatomic,strong)NSMutableArray *recordMutableArr;               /**< 战绩信息数组*/
@property (nonatomic,strong)PDPaopaoView *paopaoImgView;                     /**< 泡泡view*/
@property (nonatomic,strong)UIButton *navLeftBtn;                        /**< 左侧按钮*/


#pragma mark - 泡泡
@property (nonatomic,strong)UIDynamicAnimator *zy_animator;
@property (nonatomic,strong)UIPanGestureRecognizer *zy_panGesture;
@property (nonatomic,strong)UIAttachmentBehavior *zy_attachmentBehavior;
@property (nonatomic,strong)UISnapBehavior *zy_snapBehavior;
@property (nonatomic,assign)CGPoint zy_centerPoint;
@property (nonatomic,assign)BOOL isPan;                                 /**< 判断泡泡是否拖拽*/
@property (nonatomic,assign)BOOL isScroll;                              /**< 判断是否scroll*/
#pragma mark - 背景
@property (nonatomic,strong)PDImageView *backgroundImageView;               /**< 背景图片*/


#pragma mark - Nav
@property (nonatomic,strong)PDImageView *barMainTitleImageView;
@property (nonatomic,strong)UIView *navBarView;
@property (nonatomic,strong)PDImageView *navAvatarImageView;                /**< 导航栏头像*/
@end

@implementation PDPandaViewController

+(instancetype)sharedController{
    static PDPandaViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[PDPandaViewController alloc] init];
    });
    return _sharedAccountModel;
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self pageSetting];
    
    [self setupDetailsPageView];        // 1. 创建主题view
    [self setupNavbarButtons];          // 2. 创建返回按钮
    [self testInternet];
    
    [[PDCurrentLocationManager sharedLocation] getCurrentLocationManager:^(CGFloat lat, CGFloat lng, AMapAddressComponent *addressComponent) {
        
    }];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.navigationController.interactivePopGestureRecognizer.delegate = nil;
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - pageSetting
-(void)pageSetting{
    [self rightBarButtonWithTitle:@"123" barNorImage:nil barHltImage:nil action:^{

    }];
}


#pragma makr - arrayWithInit
-(void)arrayWithInit {
    self.pandaMutableArr = [NSMutableArray array];
    NSArray *mainArr = @[@[@"最近比赛",@"比赛详情"],@[@"最新夺宝",@"夺宝详情"]];
    [self.pandaMutableArr addObjectsFromArray:mainArr];
    
    // 网吧
    self.netBarMutableArr = [NSMutableArray array];
    
    // 夺宝
    self.indianaMutableArr = [NSMutableArray array];
    
    // 战绩
    self.recordMutableArr = [NSMutableArray array];
}

#pragma mark - 创建
-(void)setupNavbarButtons{
    UIButton *buttonBack = [UIButton buttonWithType:UIButtonTypeCustom];
    
    buttonBack.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - 44, 20, 44, 44);
    [buttonBack setImage:[UIImage imageNamed:@"icon_panda_message_nor"] forState:UIControlStateNormal];
    [buttonBack buttonWithBlock:^(UIButton *button) {
//        PDBindingInfoViewController *nav = [[PDBindingInfoViewController alloc]init];
//        PDInternetCafesRootViewController*nav = [[PDInternetCafesRootViewController alloc]init];
//        nav.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:nav animated:YES];
        [self loginManager];
        
        
        
    }];
    
    [self.view addSubview:buttonBack];
    
    self.scrollingHeaderView.navbarView = [self createNavBar];
}

-(UIView *)createNavBar{
    self.navBarView = [[UIView alloc]init];
    self.navBarView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.navBarView.clipsToBounds = YES;
    self.navBarView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 64);
    [self.view addSubview:self.navBarView];
    
    // 1. 创建回退按钮
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setImage:[UIImage imageNamed:@"icon_panda_message_hlt"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [rightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        PDPandaRootViewController *nav = [[PDPandaRootViewController alloc]init];
        [self.navigationController pushViewController:nav animated:YES];
    }];
    rightButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(15) - 44, 20, 44, 44);
    [self.navBarView addSubview:rightButton];
    
    // 2. 创建label
    self.barMainTitleImageView = [[PDImageView alloc]init];
    self.barMainTitleImageView.image = [UIImage imageNamed:@"icon_panda_pandaol"];
    self.barMainTitleImageView.backgroundColor = [UIColor clearColor];
    self.barMainTitleImageView.frame = CGRectMake((kScreenBounds.size.width - 114) / 2., self.navBarView.size_height , 114, 14);
    [self.navBarView addSubview:self.barMainTitleImageView];
    
    // 3. 创建leftBtn
    
    self.navLeftBtn = [[UIButton alloc]init];
    self.navLeftBtn.frame = CGRectMake(LCFloat(11), 20, 44, 44);
    self.navLeftBtn.orgin_y = - self.navLeftBtn.size_height;
    self.navLeftBtn.backgroundColor = [UIColor clearColor];
    [self.navLeftBtn setImage:[UIImage imageNamed:@"home_paopao"] forState:UIControlStateNormal];
    self.navLeftBtn.userInteractionEnabled = YES;
    [self.navBarView addSubview:self.navLeftBtn];
    [self.navLeftBtn buttonWithBlock:^(UIButton *button) {
        [self.scrollingHeaderView.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
    }];
    
    // 4. 创建图片
    self.navAvatarImageView = [[PDImageView alloc]init];
    self.navAvatarImageView.backgroundColor = [UIColor clearColor];
    self.navAvatarImageView.frame = CGRectMake(0, 0, LCFloat(50), LCFloat(50));
    self.navAvatarImageView.clipsToBounds = YES;
    self.navAvatarImageView.layer.cornerRadius = self.navAvatarImageView.size_height / 2.;
    // 1. 计算图片位置
    
    [self.navBarView addSubview:self.navAvatarImageView];
    
    
    return self.navBarView;
}

#pragma mark - create Main View
-(void)setupDetailsPageView{
    self.scrollingHeaderView = [[KMScrollingHeaderView alloc]initWithFrame:self.view.bounds];
    self.scrollingHeaderView.backgroundColor = [UIColor whiteColor];
    self.scrollingHeaderView.tableView.dataSource = self;
    self.scrollingHeaderView.tableView.delegate = self;
    self.scrollingHeaderView.delegate = self;
    
    self.scrollingHeaderView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.scrollingHeaderView.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.scrollingHeaderView.tableView.showsVerticalScrollIndicator = YES;
    self.scrollingHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    
    self.scrollingHeaderView.tableView.separatorColor = [UIColor whiteColor];
    self.scrollingHeaderView.headerImageViewContentMode = UIViewContentModeScaleToFill;
    self.scrollingHeaderView.backgroundImageView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.scrollingHeaderView];
    
    [self.scrollingHeaderView reloadScrollingHeader];

    // 创建泡泡
    [self createPaopaoView];
}

- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView headerImageView:(PDImageView *)imageView{
    self.backgroundImageView = imageView;
    [imageView uploadImageWithURL:@"http://pic.baike.soso.com/p/20131204/20131204122431-1808867473.jpg" placeholder:nil callback:^(UIImage *image) {
        self.scrollingHeaderView.backgroundImageView.originalImage = image;
    }];
}

-(void)createPaopaoView{
    // 创建泡泡
    self.paopaoImgView = [[PDPaopaoView alloc]initWithFrame:CGRectMake((kScreenBounds.size.width - LCFloat(200)) / 2., 40, LCFloat(200), LCFloat(200))];
    self.paopaoImgView.userInteractionEnabled = YES;
    self.paopaoImgView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.paopaoImgView];
    [self.paopaoImgView tapManager:^{               // 点击登录注册
        
    }];
    [self paopaoManager];
}



- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView scrollViewWithScrollOffset:(CGFloat)scrollOffset{

}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.pandaMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr =  [self.pandaMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新夺宝"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            PDPandaNormalCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[PDPandaNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferInfo = @"最新夺宝";
            return cellWithRowOne;
        } else {                        // 【夺宝信息】
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            PDPandaIndianaCell  *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[PDPandaIndianaCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.backgroundColor = [UIColor whiteColor];
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferIndianaArr = self.indianaMutableArr;
            
            return cellWithRowTwo;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"个人中心"] && indexPath.row == [self cellIndexPathRowWithcellData:@"个人中心"]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PDPandaPersonCenterCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[PDPandaPersonCenterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            self.personCenterCell = cellWithRowThr;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferPandaCenterModel = pandaCenterModel;
        
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"战绩中心"] && indexPath.row == [self cellIndexPathRowWithcellData:@"战绩中心"]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        PDPandaRecordCenter *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[PDPandaRecordCenter alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferArr = self.recordMutableArr;
        
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"附近的网吧"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            PDPandaNormalCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[PDPandaNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
            cellWithRowFiv.transferInfo = @"附近的网吧";
            return cellWithRowFiv;
        } else {
            static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
            PDInternetCafesSingleCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
            if (!cellWithRowSex){
                cellWithRowSex = [[PDInternetCafesSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
                cellWithRowSex.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowSex.transferCellheight = cellHeight;
            cellWithRowSex.transferInternetCafesSingleModel = [[self.pandaMutableArr objectAtIndex:indexPath.section ] objectAtIndex:indexPath.row];
            return cellWithRowSex;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最近比赛"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"最近比赛"]){
            static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
            PDPandaNormalCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
            if (!cellWithRowSev){
                cellWithRowSev = [[PDPandaNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
                cellWithRowSev.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowSev.transferCellHeight = cellHeight;
            cellWithRowSev.transferInfo = @"最近比赛";
            return cellWithRowSev;
        } else {
            static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
            PDPandaMatchInfoCell  *cellWithRowEig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
            if (!cellWithRowEig){
                cellWithRowEig = [[PDPandaMatchInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
                cellWithRowEig.backgroundColor = [UIColor whiteColor];
            }
            cellWithRowEig.transferCellHeight = cellHeight;
            cellWithRowEig.transferMatchModel = pandaMatchSingleModel;
            return cellWithRowEig;
        }
    }
    else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        UITableViewCell  *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.backgroundColor = [UIColor whiteColor];
        }
        cellWithRowOne.textLabel.text = [NSString stringWithFormat:@"%li",indexPath.row];
        return cellWithRowOne;

    }
    return nil;
}



#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"个人中心"] && indexPath.row == [self cellIndexPathRowWithcellData:@"个人中心"]){
        PDPandaRoleDetailViewController *pandaRoleDetailViewController = [[PDPandaRoleDetailViewController alloc]init];
        pandaRoleDetailViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:pandaRoleDetailViewController animated:YES];
    }
}

/**
 
 */

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最新夺宝"]){
        if (indexPath.row == 0){
            return [PDPandaNormalCell calculationCellHeight];
        } else {
            return [PDPandaIndianaCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"个人中心"] && indexPath.row == [self cellIndexPathRowWithcellData:@"个人中心"]){
        return [PDPandaPersonCenterCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"战绩中心"] && indexPath.row == [self cellIndexPathRowWithcellData:@"战绩中心"]){
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"最近比赛"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"最近比赛"]){
            return 44;
        } else {
            return  [PDPandaMatchInfoCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"附近的网吧"]){
        if (indexPath.row == 0){
            return 44;
        } else {
           return [PDInternetCafesSingleCell calculationCellHeight];
        }
    }
    return 44;
    
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.scrollingHeaderView.tableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.pandaMutableArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([[self.pandaMutableArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return 0;
    } else {
        return LCFloat(10);
    }
}


#pragma mark - Other Manager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.pandaMutableArr.count ; i++){
        NSArray *dataTempArr = [self.pandaMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.pandaMutableArr.count ; i++){
        NSArray *dataTempArr = [self.pandaMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}


#pragma mark - tyest
-(void)testInternet{
    pandaCenterModel = [[PDPandaCenterModel alloc]init];
    pandaCenterModel.avatar = @"2.jpg";
    pandaCenterModel.name = @"GiganticWhale";
    pandaCenterModel.area = @"艾欧尼亚";
    [self.navAvatarImageView uploadImageWithURL:pandaCenterModel.avatar placeholder:nil callback:NULL];
    
    
    NSMutableArray *netMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < 15 ; i++){
//        PDInternetCafesSingleModel * singleModel = [[PDInternetCafesSingleModel alloc]init];
//        singleModel.name = @"网鱼网咖";
//        singleModel.avatar = @"3.jpg";
//        singleModel.address = @"23rewfdsafjdsfl";
//        singleModel.priceRange = @"4-10元/小时";
//        singleModel.distance = @"222";
//        singleModel.isAvtivity = YES;
//        [self.netBarMutableArr addObject:singleModel];
    }
    
    // 1.判断是否有附近的网吧
    if ([self cellIndexPathSectionWithcellData:@"附近的网吧"] == -1){        // 没有
        if (self.netBarMutableArr.count){
            [netMutableArr addObject:@"附近的网吧"];
            [netMutableArr addObjectsFromArray: self.netBarMutableArr];
            // 2. 插入
            [self.pandaMutableArr addObject:netMutableArr];
            NSInteger section = [self cellIndexPathSectionWithcellData:@"最新夺宝"];
            NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:section];
            [self.pandaTableView insertSections:indexSet withRowAnimation:UITableViewRowAnimationMiddle];
        }
    }
    
    // 夺宝
    for (int i = 0;i< 2;i++){
        PDIndianaSingleModel *singleModel = [[PDIndianaSingleModel alloc]init];
        singleModel.productImg = @"http://img2.imgtn.bdimg.com/it/u=1615976267,4105793833&fm=11&gp=0.jpg";
        singleModel.productName = @"魔爆V40电竞游戏USB发光自定义宏游戏鼠标";
        singleModel.productTime = 1472628607;
        [self.indianaMutableArr addObject:singleModel];
    }
    
    
    // 战绩
    PDRecordSingleModel *recordSingleModel = [[PDRecordSingleModel alloc]init];
    recordSingleModel.fixedStr = @"胜率";
    recordSingleModel.dymicStr = 88.2;
    [self.recordMutableArr addObject:recordSingleModel];
    
    PDRecordSingleModel *recordSingleModel1 = [[PDRecordSingleModel alloc]init];
    recordSingleModel1.fixedStr = @"KDA";
    recordSingleModel1.dymicStr = 7.9;
    [self.recordMutableArr addObject:recordSingleModel1];

    PDRecordSingleModel *recordSingleModel2 = [[PDRecordSingleModel alloc]init];
    recordSingleModel2.fixedStr = @"段位";
    recordSingleModel2.duanweiStr = @"未定位";
    [self.recordMutableArr addObject:recordSingleModel2];

    PDRecordSingleModel *recordSingleModel3 = [[PDRecordSingleModel alloc]init];
    recordSingleModel3.fixedStr = @"总场次";
    recordSingleModel3.dymicStr = 856;
    [self.recordMutableArr addObject:recordSingleModel3];

    
    // 真正战绩
    pandaMatchSingleModel = [[PDPandaMatchSingleModel alloc]init];
//    @property (nonatomic,copy)NSString *avatar;         /**< 头像*/
//    @property (nonatomic,copy)NSString *type;           /**< 类别*/
//    @property (nonatomic,copy)NSString *gameEnd;        /**< 比赛结束*/
//    @property (nonatomic,copy)NSString *gameRecord;     /**< 比赛战绩*/
//    @property (nonatomic,copy)NSString *gameMoney;      /**< 游戏打钱*/
//    @property (nonatomic,assign)NSTimeInterval time;    /**< 比赛时间*/
//    @property (nonatomic,copy)NSString *reward;         /**< 比赛奖励*/

    pandaMatchSingleModel.avatar = @"http://img5.imgtn.bdimg.com/it/u=272368002,65664521&fm=11&gp=0.jpg";
    pandaMatchSingleModel.type = @"排位赛";
    pandaMatchSingleModel.gameEnd = @"失败";
    pandaMatchSingleModel.gameRecord = @"9/7/6";
    pandaMatchSingleModel.gameMoney = @"9.8k";
    pandaMatchSingleModel.time = 1472648607;
    pandaMatchSingleModel.reward = @"竹子+1";
    
    [self.pandaTableView reloadData];
}







#pragma mark - 泡泡方法
-(void)paopaoManager{
    [self zy_creatAnimator];
    [self addPanGesture];
}

// 1. 添加拖动方法
-(void)addPanGesture{
    self.zy_panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(zy_panGesture:)];
    [self.paopaoImgView addGestureRecognizer:self.zy_panGesture];
}

- (void)updateSnapPoint {
    self.zy_centerPoint = [self.paopaoImgView convertPoint:CGPointMake(self.paopaoImgView.bounds.size.width / 2, self.paopaoImgView.bounds.size.height / 2) toView:self.view];
    self.zy_snapBehavior = [[UISnapBehavior alloc] initWithItem:self.paopaoImgView snapToPoint:self.zy_centerPoint];
    self.zy_snapBehavior.damping = .2f;
}

- (void)zy_creatAnimator {
    self.zy_animator = [[UIDynamicAnimator alloc] initWithReferenceView:self.view];
    self.zy_animator.delegate = self;
    [self updateSnapPoint];
}


-(void)zy_panGesture:(UIPanGestureRecognizer *)pan{
    CGPoint panLocation = [pan locationInView:self.view];
    
    if (self.isScroll){
        return;
    }
    
    
    if (pan.state == UIGestureRecognizerStateBegan) {
        self.isPan = YES;                       // 设置已经拖动
        UIOffset offset = UIOffsetMake(panLocation.x - self.zy_centerPoint.x, panLocation.y - self.zy_centerPoint.y);
        [self.zy_animator removeAllBehaviors];
        self.zy_attachmentBehavior = [[UIAttachmentBehavior alloc] initWithItem:self.paopaoImgView offsetFromCenter:offset attachedToAnchor:panLocation];
        [self.zy_animator addBehavior:self.zy_attachmentBehavior];
        
    } else if (pan.state == UIGestureRecognizerStateChanged) {
        [self.zy_attachmentBehavior setAnchorPoint:panLocation];
    } else if (pan.state == UIGestureRecognizerStateEnded || pan.state == UIGestureRecognizerStateCancelled || pan.state == UIGestureRecognizerStateFailed) {
        self.isPan = NO;                       // 设置拖动结束
        [self.zy_animator removeAllBehaviors];
        [self.zy_animator addBehavior:self.zy_snapBehavior];
        
        CGFloat pointY = self.scrollingHeaderView.tableView.contentOffset.y;
        
        self.paopaoImgView.transform = CGAffineTransformMakeScale(1 - (pointY / self.scrollingHeaderView.navbarViewFadingOffset), 1 - (pointY / self.scrollingHeaderView.navbarViewFadingOffset));
        self.paopaoImgView.orgin_y = - pointY - self.paopaoImgView.size_height + 40 + LCFloat(200);
        NSLog(@"%.2f",self.paopaoImgView.orgin_y);
    }
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.isPan){
        return;
        
    }
    
    CGPoint point = scrollView.contentOffset;
    NSLog(@"===============> %.2f",point.y);
    CGFloat pointY = point.y;
    [self.scrollingHeaderView.backgroundImageView setBlurLevel:(pointY / self.scrollingHeaderView.navbarViewFadingOffset)];
    
    
    if (scrollView == self.scrollingHeaderView.tableView){
        if (pointY >= self.scrollingHeaderView.navbarViewFadingOffset){             // 大泡泡不显示
            if(pointY > self.scrollingHeaderView.navbarViewFadingOffset && self.navBarView.alpha == 0.0) {
                self.navBarView.alpha = 0;
                self.navBarView.hidden = NO;
                
                [UIView animateWithDuration:0.3 animations:^{
                    self.navBarView.alpha = 1;
                }];
            } else if(pointY < self.scrollingHeaderView.navbarViewFadingOffset && self.navBarView.alpha == 1.0) {
                [UIView animateWithDuration:0.3 animations:^{
                    self.navBarView.alpha = 0;
                } completion: ^(BOOL finished) {
                    self.navBarView.hidden = YES;
                }];
            }
        } else {                                                                    // 大泡泡显示
            if (!self.isPan){
                self.paopaoImgView.transform = CGAffineTransformMakeScale(1 - (pointY / self.scrollingHeaderView.navbarViewFadingOffset), 1 - (pointY / self.scrollingHeaderView.navbarViewFadingOffset));
                self.paopaoImgView.orgin_y = - pointY - self.paopaoImgView.size_height + 40 + LCFloat(200);
            }
        }
    }
    
    [self scrollViewAnimationWithPoint:pointY];
}

#pragma mark - scrollViewAnimation
-(void)scrollViewAnimationWithPoint:(CGFloat)offset{
    CGFloat fixedFloat = - self.navBarView.size_height + (self.navBarView.size_height - 20 - 14) / 2. + 5;
    CGFloat dymicFloat = self.scrollingHeaderView.navbarViewFadingOffset - offset + USER_Nick_Height;
    
    CATransform3D labelTransform = CATransform3DMakeTranslation(0, MAX(fixedFloat, dymicFloat) + USER_Nick_Height, 0);
    self.self.barMainTitleImageView.layer.transform = labelTransform;
    self.self.barMainTitleImageView.layer.zPosition = 20;
    
    
    NSInteger cellRow = [self cellIndexPathRowWithcellData:@"个人中心"];
    NSInteger cellSection = [self cellIndexPathSectionWithcellData:@"个人中心"];
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellRow inSection:cellSection];
    PDPandaPersonCenterCell *personCenterCell = (PDPandaPersonCenterCell *)[self.scrollingHeaderView.tableView cellForRowAtIndexPath:indexPath];

    CGRect convertFrame = [personCenterCell.bgImageView convertRect:personCenterCell.avatarImageView.frame toView:self.view.window];
    
    self.navAvatarImageView.frame = convertFrame;
    self.navAvatarImageView.orgin_y = LCFloat(10);
}


//完成拖拽
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate; {
    self.isScroll = NO;
    NSLog(@"完成拖拽");
}

//开始拖拽视图
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView; {
    self.isScroll = YES;
    NSLog(@"开始拖拽");
}

// 滚动视图减速完成，滚动将停止时，调用该方法。一次有效滑动，只执行一次。
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    NSLog(@"滚动视图减速完成，滚动将停止时，调用该方法。一次有效滑动，只执行一次。");
    
}

- (void)dynamicAnimatorWillResume:(UIDynamicAnimator *)animator{
    NSLog(@"开始");
}
- (void)dynamicAnimatorDidPause:(UIDynamicAnimator *)animator{
        self.paopaoImgView.orgin_y = - self.scrollingHeaderView.tableView.contentOffset.y - self.paopaoImgView.size_height + 40 + LCFloat(200);
}



#pragma mark - LoginManager{

-(void)loginManager{
    self.headerImageView = [[PDImageView alloc]init];
    self.headerImageView.frame = CGRectMake(0, 0, LCFloat(150), LCFloat(150));
    
    
    if (([self cellIndexPathSectionWithcellData:@"个人中心"] == -1)){               // 如果没有个人中心添加个人中心
        NSArray *rowOneArr = @[@"个人中心",@"战绩中心"];
        [self.pandaMutableArr insertObject:rowOneArr atIndex:0];
        NSIndexSet *insertSet = [NSIndexSet indexSetWithIndex:0];
        [self.scrollingHeaderView.tableView insertSections:insertSet withRowAnimation:UITableViewRowAnimationLeft];
    } else {
    
    }
}



@end
