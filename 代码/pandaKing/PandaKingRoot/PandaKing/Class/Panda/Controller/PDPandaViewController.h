//
//  PDPandaViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 首页
#import "AbstractViewController.h"
#import "PDPandaPersonCenterCell.h"

@interface PDPandaViewController : AbstractViewController

+(instancetype)sharedController;

@property (nonatomic,strong)PDPandaPersonCenterCell *personCenterCell;              /**< 个人中心cell */

@end
