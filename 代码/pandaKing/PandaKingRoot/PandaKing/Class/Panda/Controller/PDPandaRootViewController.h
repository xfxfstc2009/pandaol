//
//  PDPandaRootViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDPaopaoView.h"


@interface PDPandaRootViewController : AbstractViewController

+(_Null_unspecified instancetype)sharedController;
@property (nonatomic,strong,nonnull)PDPaopaoView *paopaoImageView;                  /**< 泡泡的view*/
@property (nonatomic,assign)BOOL hasLaungchLinkAnimation;                             /**< 头部动画是否执行*/
@property (nonatomic,strong,nullable)PDImageView *headerImageView;                               /**< 创建view */

#pragma mark - 获取当前的数据
-(void)sendRequestToGetPersonalInfoHasAnimation:(BOOL)animation block:(void(^_Null_unspecified)())block;                /**< 获取数据，是否使用动画*/
-(void)removeAccountInfoSet;                                                    /**< */

-(void)mainInterfaceComfirm;

-(void)uploadUserAvatar:(UIImage *_Null_unspecified)avatar;

// 修改当前认证状态
-(void)uploadCurrentRenzhengStatus:(NSString *_Null_unspecified)status;
-(void)getGuidesList;

#pragma mark - 显示newFeature
-(void)showNewFeature;
@end
