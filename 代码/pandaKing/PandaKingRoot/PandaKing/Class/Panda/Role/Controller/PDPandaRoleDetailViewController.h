//
//  PDPandaRoleDetailViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 角色详情
#import "AbstractViewController.h"

@interface PDPandaRoleDetailViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferLolGameUserId;

-(void)bindingCancelWithBlock:(void(^)())block;                     /**< 解除绑定*/

-(void)activityInfoWithBlock:(void(^)(BindingStatus bindingStatus))block;


@end
