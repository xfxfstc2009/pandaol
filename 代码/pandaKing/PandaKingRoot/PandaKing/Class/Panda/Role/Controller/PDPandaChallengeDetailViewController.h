//
//  PDPandaChallengeDetailViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 查看战绩详情
#import "AbstractViewController.h"
#import "PDGamerRecordRootModel.h"                  // 查看战绩详情

@interface PDPandaChallengeDetailViewController : AbstractViewController

@property (nonatomic,strong)PDGamerRecordRootModel *transferRecordRootModel;

@end
