//
//  PDPandaChallengeDetailViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaChallengeDetailViewController.h"
#import "PDLotteryGameRootTableView.h"
#import "PDRoleChallengeDetailTableViewCell.h"
#import "PDGameRecordDetailRootModel.h"
#import "PDRoleChallengeDetailTableViewHeaderView.h"
#import "PDCenterPersonViewController.h"
#import "PDInviteViewController.h"


@interface PDPandaChallengeDetailViewController ()<PDLotteryGameRootTableViewDelegate,PDRoleChallengeDetailTableViewHeaderViewDelegate>{
    PDGameRecordDetailRootModel *gameRecordDetailRootModel;
}
@property (nonatomic,strong)PDLotteryGameRootTableView *chanllengeTableView;               /**< */
@property (nonatomic,strong)NSMutableArray *challengeMutableArr;            /**< */
@property (nonatomic,strong)PDImageView *headerBgView;                            /**< 背景view*/
@property (nonatomic,strong)UIView *headerSubBgView;                              /**< 次级view*/
@property (nonatomic,strong)NSMutableArray *headerMutableArr;

@end

@implementation PDPandaChallengeDetailViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createHeaderView];
    [self createTableView];
    [self interfaceManager];
}


#pragma mark InterfaceManager
-(void)interfaceManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestTogetRecordInfoManager];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"战绩详情";
    
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.challengeMutableArr = [NSMutableArray array];
    self.headerMutableArr = [NSMutableArray array];
}

#pragma mark - createView
-(void)createHeaderView{
    self.headerBgView = [[PDImageView alloc]init];
    self.headerBgView.backgroundColor = [UIColor clearColor];
    self.headerBgView.image = [UIImage imageNamed:@"img_roleDetail_header"];
    self.headerBgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 64 + LCFloat(51));
    self.headerBgView.userInteractionEnabled = YES;
    [self.view addSubview:self.headerBgView];
    
    __weak typeof(self)weaKSelf = self;
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    leftButton.userInteractionEnabled = YES;
    leftButton.frame = CGRectMake(LCFloat(11), 20, 44, 44);
    [leftButton setImage:[UIImage imageNamed:@"icon_main_back"] forState:UIControlStateNormal];
    [leftButton buttonWithBlock:^(UIButton *button) {
        if (!weaKSelf){
            return ;
        }
        __strong typeof(weaKSelf)strongSelf = weaKSelf;
        if (strongSelf.navigationController.childViewControllers.count == 1){
            [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
        } else {
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    [self.headerBgView addSubview:leftButton];
    
    // 创建title
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = @"对战详情";
    titleLabel.font = [UIFont systemFontOfCustomeSize:18.];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor whiteColor];
    CGSize contentOfTitle = [titleLabel.text sizeWithCalcFont:titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:titleLabel.font])];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - contentOfTitle.width) / 2., 20, contentOfTitle.width, [NSString contentofHeightWithFont:titleLabel.font]);
    [self.headerBgView addSubview:titleLabel];
    titleLabel.center_y = leftButton.center_y;
    
    // 创建view
    self.headerSubBgView = [[UIView alloc]init];
    self.headerSubBgView.backgroundColor = [UIColor clearColor];;
    self.headerSubBgView.frame = CGRectMake(0, self.headerBgView.size_height - LCFloat(51), kScreenBounds.size.width, LCFloat(51));
    [self.headerBgView addSubview:self.headerSubBgView];
    
    
}


#pragma mark - UITableView
-(void)createTableView{
    if (!self.chanllengeTableView) {
        self.chanllengeTableView = [[PDLotteryGameRootTableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.chanllengeTableView.orgin_y = CGRectGetMaxY(self.headerBgView.frame);
        self.chanllengeTableView.size_height = kScreenBounds.size.height - self.headerBgView.size_height;
        self.chanllengeTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.chanllengeTableView.backgroundColor = [UIColor clearColor];
        self.chanllengeTableView.showsVerticalScrollIndicator = NO;
        self.chanllengeTableView.scrollEnabled = YES;
        self.chanllengeTableView.foldDelegate = self;
        self.chanllengeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.chanllengeTableView];
    }
    
}

#pragma mark -UITableViewDataSource
-(NSInteger)numberOfSectionForFlodTableView:(UITableView *)tableView{
    NSInteger count = gameRecordDetailRootModel.tempGameUsers.count;
    return count;
}

-(NSInteger)foldTableView:(UITableView *)tableView numberOfRowInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)foldTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    PDRoleChallengeDetailTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDRoleChallengeDetailTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    cellWithRowOne.backgroundColor = c8;
    cellWithRowOne.transferGamersSingleModel = [gameRecordDetailRootModel.tempGameUsers objectAtIndex:indexPath.section];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)foldTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(CGFloat)foldTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PDRoleChallengeDetailTableViewCell calculationCellHeight];
}

-(CGFloat)foldTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    
    if (section == 0){
        return [PDRoleChallengeDetailTableViewHeaderView calculationCellHeightWithIndex:0];
    } else if (section == gameRecordDetailRootModel.winTeam.gamers.count){
        return [PDRoleChallengeDetailTableViewHeaderView calculationCellHeightWithIndex:0];
    } else {
        return [PDRoleChallengeDetailTableViewHeaderView calculationCellHeightWithIndex:1];
    }
}

-(UIView *)foldTableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    PDRoleChallengeDetailTableViewHeaderView *headerView = [[PDRoleChallengeDetailTableViewHeaderView alloc]init];
    if (![self.headerMutableArr containsObject:headerView]){
        [self.headerMutableArr addObject:headerView];
    }
    headerView.backgroundColor = [UIColor clearColor];
    headerView.transferCellIndex = section;
    headerView.delegate = self;
    if (section == 0){
        headerView.gameStatus = GameDetailStatusTypeWin;
        headerView.transferTeamDetailModel = gameRecordDetailRootModel.winTeam;
    } else if (section == gameRecordDetailRootModel.winTeam.gamers.count){
        headerView.gameStatus = GameDetailStatusTypeLose;
        headerView.transferTeamDetailModel = gameRecordDetailRootModel.loseTeam;
    } else {
        headerView.gameStatus = GameDetailStatusTypeNormal;
    }

    headerView.transferGamersSingleModel = [gameRecordDetailRootModel.tempGameUsers objectAtIndex:section];
    __weak typeof(self)weakSelf = self;
    [headerView avatarButtonClickBlock:^(PDGameRecordDetailGamersSingleModel *transferGamersSingleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (transferGamersSingleModel.isUser){          // 【我们平台用户】
            if (transferGamersSingleModel.memberId.length){
                if ([transferGamersSingleModel.memberId isEqualToString:[AccountModel sharedAccountModel].memberId]){
                    return;
                }
                
                PDCenterPersonViewController *personVC = [[PDCenterPersonViewController alloc]init];
                personVC.transferMemberId = transferGamersSingleModel.memberId;
                [strongSelf.navigationController pushViewController:personVC animated:YES];
            }
        } else {
            PDInviteViewController *inviteViewController = [[PDInviteViewController alloc]init];
            [self.navigationController pushViewController:inviteViewController animated:YES];
        }
    }];
    return headerView;
}

-(void)foldTableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(void)foldTableViewHeaderDidSelectedWithIndex:(NSInteger)index{
    // 当前的view箭头打开
    PDRoleChallengeDetailTableViewHeaderView *headerView = [self.headerMutableArr objectAtIndex:index];
    headerView.bottomIsShow = !headerView.bottomIsShow;
    if(headerView.bottomIsShow){
        [headerView showBottomStatusBar:NO];
    } else {
        [headerView showBottomStatusBar:YES];
    }

    [self.chanllengeTableView foldFoldingSectionHeaderTappedAtIndex:index];            // 折叠，展示
}

#pragma mark - Interface
-(void)sendRequestTogetRecordInfoManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:gamerecorddetail requestParams:@{@"gameRecordId":self.transferRecordRootModel.gameRecordId} responseObjectClass:[PDGameRecordDetailRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf -> gameRecordDetailRootModel = (PDGameRecordDetailRootModel *)responseObject;
            // 更新
            NSMutableArray *tempGameUsersList = [NSMutableArray array];
            if (strongSelf->gameRecordDetailRootModel.winTeam.gamers.count){
                [tempGameUsersList addObjectsFromArray:strongSelf->gameRecordDetailRootModel.winTeam.gamers];
            }
            if (strongSelf->gameRecordDetailRootModel.loseTeam.gamers.count){
                [tempGameUsersList addObjectsFromArray:strongSelf->gameRecordDetailRootModel.loseTeam.gamers];
            }
            strongSelf->gameRecordDetailRootModel.tempGameUsers = [tempGameUsersList copy];
            
            [strongSelf.chanllengeTableView reloadData];
            [strongSelf uploadWithHeaderViewInfo];
        }
    }];
}


#pragma mark - 创建头部的view
-(UIView *)createHeaderSingleViewWithTitle:(NSString *)title subTitle:(NSString *)subTitle{
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor clearColor];
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width / 4., LCFloat(51));
    [self.headerSubBgView addSubview:bgView];
    
    // 2. 创建开始时间
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.backgroundColor = [UIColor clearColor];
    fixedLabel.font = [UIFont systemFontOfCustomeSize:11.];
    fixedLabel.stringTag = @"fixedLabel";
    fixedLabel.textColor = [UIColor whiteColor];
    fixedLabel.textAlignment = NSTextAlignmentCenter;
    fixedLabel.adjustsFontSizeToFitWidth = YES;
    fixedLabel.text = title;
    [bgView addSubview:fixedLabel];
    
    // 3.  创建时间
    UILabel *dymicLabel = [[UILabel alloc]init];
    dymicLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    dymicLabel.textColor = c26;
    dymicLabel.adjustsFontSizeToFitWidth = YES;
    dymicLabel.textAlignment = NSTextAlignmentCenter;
    dymicLabel.stringTag = @"dymicLabel";
    dymicLabel.text = subTitle;
    [bgView addSubview:dymicLabel];
    
    // 4. 计算宽高
    CGFloat margin = (LCFloat(51) - [NSString contentofHeightWithFont:fixedLabel.font] - [NSString contentofHeightWithFont:dymicLabel.font]) / 3.;
    fixedLabel.frame = CGRectMake(0, margin, bgView.size_width, [NSString contentofHeightWithFont:fixedLabel.font]);
    
    dymicLabel.frame = CGRectMake(0, CGRectGetMaxY(fixedLabel.frame) + margin, bgView.size_width, [NSString contentofHeightWithFont:dymicLabel.font]);
    return bgView;
}

-(void)uploadWithHeaderViewInfo{
    if (self.headerSubBgView.subviews.count){
        [self.headerSubBgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    UIView *startView = [self createHeaderSingleViewWithTitle:@"开始时间" subTitle:gameRecordDetailRootModel.startTime];
    UIView *lineView = [self createHeaderSingleViewWithTitle:@"持续时间" subTitle:gameRecordDetailRootModel.duration];
    UIView *gameTypeView = [self createHeaderSingleViewWithTitle:@"比赛模式" subTitle:gameRecordDetailRootModel.type];
    UIView *jushiView = [self createHeaderSingleViewWithTitle:@"比赛局势" subTitle:gameRecordDetailRootModel.gameSitutaion];
    
    startView.frame = CGRectMake(0, 0, kScreenBounds.size.width / 4., self.headerSubBgView.size_height);
    lineView.frame = CGRectMake(CGRectGetMaxX(startView.frame), 0, kScreenBounds.size.width / 4., self.headerSubBgView.size_height);
    gameTypeView.frame = CGRectMake(CGRectGetMaxX(lineView.frame), 0, kScreenBounds.size.width / 4., self.headerSubBgView.size_height);
    jushiView.frame = CGRectMake(CGRectGetMaxX(gameTypeView.frame), 0, kScreenBounds.size.width / 4., self.headerSubBgView.size_height);
    
}


@end
