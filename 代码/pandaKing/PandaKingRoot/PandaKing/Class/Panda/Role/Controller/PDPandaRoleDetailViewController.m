//
//  PDPandaRoleDetailViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaRoleDetailViewController.h"
#import "PDPandaRoleNormalCell.h"                                           /**< 默认的cell */

#import "PDPandaRoleKDACell.h"                                              /**< 表的cell*/
#import "PDRoleRecordSingleCell.h"
#import "PDGamerRecordListModel.h"
#import "KMScrollingHeaderView.h"                                           // 底部框架
#import "PDAccountAvatarView.h"
#import <pop/POP.h>
#import <objc/runtime.h>
#import "PDBindactivateGameUserModel.h"
#import "PDGameUserActivityModel.h"
#import "PDPandaChallengeDetailViewController.h"                            // 跳转到详情
#import "PDRoleDetailAvatarCell.h"
#import "PDPandaRoleRongyuqiangCell.h"
#import "PDPandaZhanjiqiangCell.h"
#import "PDRoleChallengeTableViewCell.h"


static char cancelBindingKey;
static char activityKey;
@interface PDPandaRoleDetailViewController()<UITableViewDataSource,UITableViewDelegate,KMScrollingHeaderViewDelegate,PDNetworkAdapterDelegate>{
    PDRoleDetailRootModel *roleDetailRootModel;
    BOOL hasUpdateFrame;
}
@property (nonatomic,strong)KMScrollingHeaderView *scrollingHeaderView;
@property (nonatomic,strong)NSMutableArray *roleDetailMutableArr;
@property (nonatomic,strong)UIView *bottomView;                                        /**< 底部的view*/
@property (nonatomic,strong)NSMutableArray *recordMutableArr;                          /**< 战绩数组*/

@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UIButton *rightButton;

@property (nonatomic,strong)PDImageView *navBarView;                     /**< navBar*/
@property (nonatomic,strong)UILabel *headerTitleLabel;
@property (nonatomic,strong)UIButton *leftNavBtn;

@end

@implementation PDPandaRoleDetailViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createBottomView];                            // 创建底部的view
    [self createNavBar];
    [self interfaceManager];
}

#pragma mark - InterfaceManager
-(void)interfaceManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetUserInfoWithLolGameUserId:weakSelf.transferLolGameUserId];
    [weakSelf getRecordListManager];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"角色详情";
}

#pragma mark 2.4 【创建NavBar】
-(void)createNavBar{
    // 1. 创建导航栏
    self.navBarView = [[PDImageView alloc]init];
    self.navBarView.backgroundColor = RGB(19, 19, 19, 1);
    self.navBarView.clipsToBounds = YES;
    self.navBarView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 64);
    [self.view addSubview:self.navBarView];
    
    // 3. 创建PandaOL
    self.headerTitleLabel = [[UILabel alloc]init];
    self.headerTitleLabel.backgroundColor = [UIColor clearColor];
    self.headerTitleLabel.frame = CGRectMake((kScreenBounds.size.width - 114) / 2., 20, 114, 44);
    self.headerTitleLabel.text = @"角色详情";
    self.headerTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.headerTitleLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.headerTitleLabel.font = [[UIFont systemFontOfCustomeSize:18.]boldFont];
    [self.view addSubview:self.headerTitleLabel];
    
    // 4. 添加
    self.scrollingHeaderView.navbarView = self.navBarView;
    
    UIButton *messageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    messageButton.frame = CGRectMake(LCFloat(15), 20, 44, 44);
    [messageButton setImage:[UIImage imageNamed:@"icon_main_back"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [messageButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.navigationController.childViewControllers.count == 1){
            [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
        } else {
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    
    [self.navBarView addSubview:messageButton];
}


#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.roleDetailMutableArr = [NSMutableArray array];
    NSArray *info = @[@[@"头像",@"胜率"],@[@"荣誉",@"荣誉-normal"]];
    [self.roleDetailMutableArr addObjectsFromArray:info];
    
    self.recordMutableArr = [NSMutableArray array];
    [self.recordMutableArr addObject:@"战绩"];
    [self.roleDetailMutableArr addObject:self.recordMutableArr];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.scrollingHeaderView){
        self.scrollingHeaderView = [[KMScrollingHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, self.view.size_height - 50) WithHeight:30];
        if ([AccountModel sharedAccountModel].isShenhe){
            self.scrollingHeaderView.size_height = self.view.size_height;
        } else {
            self.scrollingHeaderView.size_height = self.view.size_height - 50;
        }
        self.scrollingHeaderView.backgroundColor = [UIColor whiteColor];
        self.scrollingHeaderView.delegate = self;
        self.scrollingHeaderView.tableView.dataSource = self;
        self.scrollingHeaderView.tableView.delegate = self;
        self.scrollingHeaderView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.scrollingHeaderView.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.scrollingHeaderView.tableView.showsVerticalScrollIndicator = YES;
        self.scrollingHeaderView.tableView.backgroundColor = [UIColor whiteColor];
        self.scrollingHeaderView.tableView.separatorColor = [UIColor whiteColor];
        self.scrollingHeaderView.headerImageViewContentMode = UIViewContentModeScaleToFill;
        self.scrollingHeaderView.backgroundImageView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.scrollingHeaderView];
        self.scrollingHeaderView.headerImageViewScalingFactor = 64 + 127 - [PDPandaRoleNormalCell calculationCellHeight];
        self.scrollingHeaderView.headerImageViewHeight = 64 + 127 - [PDPandaRoleNormalCell calculationCellHeight];
        
        [self.scrollingHeaderView reloadScrollingHeader];
        
        
        self.leftNavBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.leftNavBtn.frame = CGRectMake(LCFloat(15), 20, 44, 44);
        [self.leftNavBtn setImage:[UIImage imageNamed:@"icon_main_back"] forState:UIControlStateNormal];
        __weak typeof(self)weakSelf = self;
        [self.leftNavBtn buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf.navigationController.childViewControllers.count == 1){
                [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
            } else {
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }
        }];
        
        [self.view addSubview:self.leftNavBtn];
    }
    
    __weak typeof(self)weakSelf = self;
    [self.scrollingHeaderView.tableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf getRecordListManager];
    }];
    
    [self.scrollingHeaderView.tableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf getRecordListManager];
    }];
}

- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView scrollViewWithScrollOffset:(CGFloat)scrollOffset{
    if (scrollOffset > 0){
        self.navBarView.alpha = 1;
    } else {
        self.navBarView.alpha = 0;
    }
}

#pragma mark 2.2 【更新背景图片】
- (void)detailsPage:(KMScrollingHeaderView *)scrollingHeaderView headerImageView:(PDImageView *)imageView{
    imageView.image = [UIImage imageNamed:@"icon_role_background_new"];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.roleDetailMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.roleDetailMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if(indexPath.section == [self cellIndexPathSectionWithcellData:@"头像"] && indexPath.row == [self cellIndexPathRowWithcellData:@"头像"]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        PDRoleDetailAvatarCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[PDRoleDetailAvatarCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
            cellWithRowZero.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowZero.transferCellHeight = cellHeight;
        cellWithRowZero.transferRoleDetailRootModel = roleDetailRootModel;
        return cellWithRowZero;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"胜率"]&& indexPath.row == [self cellIndexPathRowWithcellData:@"胜率"]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDPandaZhanjiqiangCell*cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDPandaZhanjiqiangCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferDetailModel = roleDetailRootModel;
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"KDA"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"KDA"]){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            PDPandaRoleNormalCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[PDPandaRoleNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferType = normalCellTypeKDA;
            cellWithRowTwo.transferRoleModel = roleDetailRootModel;
            return cellWithRowTwo;
        } else {
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            PDPandaRoleKDACell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[PDPandaRoleKDACell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowThr.transferCellHeight = cellHeight;
            cellWithRowThr.transferType = PandaRoleTableTypeKDA;
            cellWithRowThr.transferRoleDetaolModel = roleDetailRootModel;
            return cellWithRowThr;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"荣誉"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            PDPandaRoleNormalCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[PDPandaRoleNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFour.transferCellHeight = cellHeight;
            cellWithRowFour.transferType = normalCellTypeHonor;
            cellWithRowFour.transferRoleModel = roleDetailRootModel;
            return cellWithRowFour;
        } else {
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            PDPandaRoleRongyuqiangCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[PDPandaRoleRongyuqiangCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
            cellWithRowFiv.transferSingleModel = roleDetailRootModel;
            return cellWithRowFiv;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"战绩"]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWIthRowSex = @"cellIdentifyWIthRowSex";
            PDPandaRoleNormalCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWIthRowSex];
            if (!cellWithRowSex){
                cellWithRowSex = [[PDPandaRoleNormalCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWIthRowSex];
                cellWithRowSex.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowSex.transferCellHeight = cellHeight;
            cellWithRowSex.transferType = normalCellTypeRecord;
            cellWithRowSex.transferRoleModel = roleDetailRootModel;
            
            return cellWithRowSex;
        } else {
            static NSString *cellIdentifyWithRowSev = @"cellIdentifyWithRowSev";
            PDRoleChallengeTableViewCell *cellWithRowSev = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSev];
            if (!cellWithRowSev){
                cellWithRowSev = [[PDRoleChallengeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSev];
                cellWithRowSev.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowSev.transferCellHeight = cellHeight;
            cellWithRowSev.transferHomeRecordModel = [[self.roleDetailMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            return cellWithRowSev;
        }
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"战绩"]){
        if (indexPath.row != 0){
            PDGamerRecordRootModel *recordRootModel = [[self.roleDetailMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            PDPandaChallengeDetailViewController *challengeDetailViewController = [[PDPandaChallengeDetailViewController alloc]init];
            challengeDetailViewController.transferRecordRootModel = recordRootModel;
            [self.navigationController pushViewController:challengeDetailViewController animated:YES];
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"胜率"]){
        return 0;
    } else {
        return 0;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == [self cellIndexPathSectionWithcellData:@"头像"] && indexPath.row == [self cellIndexPathRowWithcellData:@"头像"]){
        return [PDRoleDetailAvatarCell calculationCellheight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"胜率"] && indexPath.row == [self cellIndexPathRowWithcellData:@"胜率"]){
        return [PDPandaRoleNormalCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"KDA"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"KDA"]){
            return [PDPandaRoleNormalCell calculationCellHeight];
        } else {
            return [PDPandaRoleKDACell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"荣誉"]){
        if (indexPath.row == 0){
            return [PDPandaRoleNormalCell calculationCellHeight];
        } else {
            return [PDPandaRoleRongyuqiangCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"战绩"]){
        if (indexPath.row == 0){
            return [PDPandaRoleNormalCell calculationCellHeight];
        } else {
            return [PDRoleChallengeTableViewCell calculationCellHeight];
        }
    }
    
    
    else {
        return [PDPandaRoleKDACell calculationCellHeight];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.scrollingHeaderView.tableView) {
        if(indexPath.section == [self cellIndexPathSectionWithcellData:@"战绩"]){
            if (indexPath.row != 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                if ( [indexPath row] == 0) {
                    separatorType = SeparatorTypeHead;
                } else if ([indexPath row] == [[self.roleDetailMutableArr objectAtIndex:indexPath.section] count] - 1) {
                    separatorType = SeparatorTypeBottom;
                } else {
                    separatorType = SeparatorTypeMiddle;
                }
                if ([[self.roleDetailMutableArr objectAtIndex:indexPath.section] count] == 1) {
                    separatorType = SeparatorTypeSingle;
                }
                [cell addSeparatorLineWithType:separatorType];
            }
        }
    }
}


#pragma mark - BottomView
-(void)createBottomView{
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    self.bottomView.frame = CGRectMake(0, kScreenBounds.size.height - 50, kScreenBounds.size.width, 50);
    self.bottomView.layer.shadowColor = [[UIColor colorWithCustomerName:@"灰"] CGColor];
    self.bottomView.layer.shadowOpacity = 2.0;
    self.bottomView.layer.shadowOffset = CGSizeMake(-2.0f, -2.0f);
    
    [self.view addSubview:self.bottomView];
    
    if ([AccountModel sharedAccountModel].isShenhe){
        self.bottomView.hidden = YES;
    } else {
        self.bottomView.hidden = NO;
    }
    
    // 1. 创建左侧的按钮
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.leftButton setTitle:@"立即认证" forState:UIControlStateNormal];
    [self.leftButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.leftButton.backgroundColor = [UIColor blackColor];
    self.leftButton.frame = CGRectMake(0, 0, kScreenBounds.size.width / 2. , self.bottomView.size_height);
    __weak typeof(self)weakSelf = self;
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToPanduanBangdingNumber:strongSelf.transferLolGameUserId block:^(PDFindactivatedtimesSingleModel *singleModel) {
            if (singleModel.canGetAward){   // 能获取奖励
               [strongSelf sendRequestToActivityRole:strongSelf.transferLolGameUserId];
            } else {
                [[PDAlertView sharedAlertView] showAlertBeirenzhengWithModel:singleModel btnArr:@[@"放弃认证",@"继续认证"] btnClick:^(NSInteger buttonIndex) {
                    if (buttonIndex == 1){
                        [[PDAlertView sharedAlertView] dismissWithBlock:^{
                            [strongSelf sendRequestToActivityRole:strongSelf.transferLolGameUserId];
                        }];
                    } else {
                        [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
                        void(^block)(BindingStatus bindingStatus) = objc_getAssociatedObject(strongSelf, &activityKey);
                        if (block){
                            block(BindingStatusNoActivity);
                        }
                        if (strongSelf.navigationController.childViewControllers.count == 1){
                            [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
                        } else {
                            [strongSelf.navigationController popViewControllerAnimated:YES];
                        }

                    }
                }];
            }
        }];
    }];
    [self.bottomView addSubview:self.leftButton];
    
    // 2.  创建右侧按钮
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    [self.rightButton setTitle:@"解除绑定" forState:UIControlStateNormal];
    self.rightButton.backgroundColor = [UIColor whiteColor];
    self.rightButton.frame = CGRectMake(kScreenBounds.size.width / 2., 0, kScreenBounds.size.width / 2., self.bottomView.size_height);
    [self.rightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf comfirmCancelBindingWithUserId:strongSelf.transferLolGameUserId];
    }];
    [self.bottomView addSubview:self.rightButton];
}

#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.roleDetailMutableArr.count ; i++){
        NSArray *dataTempArr = [self.roleDetailMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.roleDetailMutableArr.count ; i++){
        NSArray *dataTempArr = [self.roleDetailMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}

#pragma mark - 解除绑定
-(void)comfirmCancelBindingWithUserId:(NSString *)userId{
    __weak typeof(self)weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"正在解绑召唤师" conten:@"解绑后，将无法享受盼达在线收益，解绑后可重新绑定。" isClose:NO btnArr:@[@"确定解绑",@"取消"] buttonClick:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSlef = weakSelf;
        if (buttonIndex == 0){
            [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
            [strongSlef sendRequestToCloseBindingWithUserId:userId];
        }
        [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
    }];
}



-(void)sendRequestToCloseBindingWithUserId:(NSString *)userId{
    __weak typeof(self)weakSelf = self;
    NSDictionary *dic = @{@"lolGameUserId":userId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:unbindGameUser requestParams:dic responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            void(^block)() = objc_getAssociatedObject(strongSelf, &cancelBindingKey);
            [[PDAlertView sharedAlertView] showNormalAlertTitle:@"解除绑定" content:@"您已成功解除绑定" isClose:NO btnArr:@[@"确定"] btnCliock:^(NSInteger buttonIndex, NSString *inputInfo) {
                [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
                if (block){
                    block();
                }
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }];
        }
    }];
}

#pragma mark - 获取角色详情
-(void)sendRequestToGetUserInfoWithLolGameUserId:(NSString *)userId{
    __weak typeof(self)weakSelf = self;
    NSDictionary *dic = @{@"lolGameUserId":userId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:gameUser requestParams:dic responseObjectClass:[PDRoleDetailRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf->roleDetailRootModel = (PDRoleDetailRootModel *)responseObject;
            
            // 2. 更新背景
            strongSelf.headerTitleLabel.text = strongSelf->roleDetailRootModel.memberLOLGameUser.lolGameUser.serverName;
            
            [strongSelf.scrollingHeaderView.tableView reloadData];
            
            [strongSelf uploadBottomView];
        }
    }];
}


#pragma 激活召唤师
-(void)sendRequestToActivityRole:(NSString *)lolGameUserId{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"lolGameUserId":lolGameUserId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:activateGameUser requestParams:params responseObjectClass:[PDBindactivateGameUserModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDBindactivateGameUserModel *activityModel = (PDBindactivateGameUserModel *)responseObject;
            
            if ([PDAlertView sharedAlertView].alertArr.count){
                [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
                    [strongSelf bindingWithChangeAvatarAlert:activityModel];
                }];
            } else {
                [strongSelf bindingWithChangeAvatarAlert:activityModel];
            }
            
        } else {
            [[PDAlertView sharedAlertView] showNormalAlertTitle:@"认证提示" content:@"认证失败,是否重试" isClose:YES btnArr:@[@"确定重试",@"不重试"] btnCliock:^(NSInteger buttonIndex, NSString *inputInfo) {
                if (buttonIndex == 0){
                    [strongSelf sendRequestToActivityRole:strongSelf.transferLolGameUserId];
                }
                [[PDAlertView sharedAlertView]dismissWithBlock:NULL];
            }];
        }
    }];
}

-(void)bindingWithChangeAvatarAlert:(PDBindactivateGameUserModel *)activityGameUserModel{
    __weak typeof(self)weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithBindingCurrentAva:activityGameUserModel.avatar afterAva:activityGameUserModel.activateAvatar WithBlock:^(NSInteger btnIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (btnIndex == 0){         // 我已更换
            [strongSelf bindingToActivityManager];
            [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
        } else {                    // 稍后再说
            [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
            void(^block)(BindingStatus bindingStatus) = objc_getAssociatedObject(strongSelf, &activityKey);
            if (block){
                block(BindingStatusNoActivity);
            }
            if (strongSelf.navigationController.childViewControllers.count == 1){
                [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
            } else {
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }
        }
    }];
}

-(void)bindingToActivityManager{
    __weak typeof(self)weakSelf = self;
    if ([PDAlertView sharedAlertView].alertArr.count){
        [[PDAlertView sharedAlertView]dismissAllWithBlock:NULL];
    }
    [weakSelf sendRequestToRenzhengWithLolGameUserId:weakSelf.transferLolGameUserId serverId:@"nil" roleName:@"nil"];
}

#pragma mark 获取当前的战绩
-(void)getRecordListManager{
    __weak typeof(self)weakSelf = self;
    NSDictionary *dic = @{@"pageNum":self.scrollingHeaderView.tableView.currentPage};
    [[NetworkAdapter sharedAdapter] fetchWithPath:gamerUserRecord requestParams:dic responseObjectClass:[PDGamerRecordListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDGamerRecordListModel *gamerRecordListModel = (PDGamerRecordListModel *)responseObject;
            // 1. 寻找是否存在战绩
            if ([strongSelf.scrollingHeaderView.tableView.isXiaLa isEqualToString:@"YES"]){           // 下啦
                [strongSelf.recordMutableArr removeAllObjects];
            }
            if ([self cellIndexPathSectionWithcellData:@"战绩"] != -1){           // 【】
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:[strongSelf cellIndexPathSectionWithcellData:@"战绩"]];
                [strongSelf.recordMutableArr addObjectsFromArray:gamerRecordListModel.items];
                [strongSelf.scrollingHeaderView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            } else {
                [strongSelf.recordMutableArr addObject:@"战绩"];
                [strongSelf.recordMutableArr addObjectsFromArray:gamerRecordListModel.items];
                NSInteger hornorIndex = [strongSelf cellIndexPathSectionWithcellData:@"荣誉"];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:hornorIndex + 1];
                [strongSelf.scrollingHeaderView.tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            }
            if ([strongSelf.scrollingHeaderView.tableView.isXiaLa isEqualToString:@"YES"]){
                [strongSelf.scrollingHeaderView.tableView stopPullToRefresh];
            } else {
                [strongSelf.scrollingHeaderView.tableView stopFinishScrollingRefresh];
            }
        }
    }];
}

#pragma mark - 解除绑定
-(void)bindingCancelWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &cancelBindingKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 激活
-(void)activityInfoWithBlock:(void(^)(BindingStatus bindingStatus))block{
    objc_setAssociatedObject(self, &activityKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}




#pragma mark 修改当前的底部的frame
-(void)uploadBottomView{
    if ([roleDetailRootModel.memberLOLGameUser.state isEqualToString:@"activating"]){       // 激活中
        [self.leftButton setTitle:@"认证申请处理中……" forState:UIControlStateNormal];
        self.leftButton.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
        self.leftButton.enabled = NO;
        self.rightButton.hidden = YES;
        
        [UIView animateWithDuration:.1f animations:^{
            self.leftButton.alpha = 1;
            self.leftButton.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.bottomView.size_height);
        } completion:^(BOOL finished) {
            self.leftButton.hidden = NO;
            self.leftButton.alpha = 1;
        }];
        
    } else if ([roleDetailRootModel.memberLOLGameUser.state isEqualToString:@"bind"]){      // 未绑定
        [self.leftButton setTitle:@"立即认证" forState:UIControlStateNormal];
        self.leftButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        self.leftButton.enabled = YES;
        self.rightButton.hidden = NO;
        [UIView animateWithDuration:.1f animations:^{
            self.leftButton.alpha = 1;
            self.leftButton.frame = CGRectMake(0, 0, kScreenBounds.size.width / 2., self.bottomView.size_height);
            self.rightButton.frame = CGRectMake(kScreenBounds.size.width / 2., 0, kScreenBounds.size.width / 2., self.bottomView.size_height);
        } completion:^(BOOL finished) {
            self.leftButton.hidden = NO;
            self.leftButton.alpha = 1;
            self.leftButton.frame = CGRectMake(0, 0, kScreenBounds.size.width / 2., self.bottomView.size_height);
            self.rightButton.frame = CGRectMake(kScreenBounds.size.width / 2., 0, kScreenBounds.size.width / 2., self.bottomView.size_height);
        }];
    } else if ([roleDetailRootModel.memberLOLGameUser.state isEqualToString:@"activated"]){ // 已认证
        self.rightButton.hidden = NO;
        [UIView animateWithDuration:.1f animations:^{
            self.leftButton.alpha = 0;
            self.rightButton.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.bottomView.size_height);
        } completion:^(BOOL finished) {
            self.leftButton.hidden = YES;
        }];
    }
}


#pragma mark - 进行头像验证
-(void)sendRequestToRenzhengWithLolGameUserId:(NSString *)userId serverId:(NSString *)serverId roleName:(NSString *)roleName{
    __weak typeof(self)weakSelf = self;
    
    NSString *error = @"";
    if (!userId.length){
        error = @"没有召唤师Id，请重试";
    } else if (!serverId.length){
        error = @"没有区服Id，请重试";
    } else if (!roleName.length){
        error = @"没有角色名，请重试";
    }
    
    if (error.length){
        [PDHUD showHUDError:error];
        return;
    }
    
    NSDictionary *params = @{@"lolGameUserId":userId,@"gameServerId":serverId,@"gameUserName":roleName};
    
    [PDHUD showHUDProgress:@"正在验证中，请稍后" diary:0];
    [[NetworkAdapter sharedAdapter] fetchWithPath:dealactivategameuser requestParams:params responseObjectClass:[PDGameUserActivityModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"认证提示" conten:@"认证申请已提交，系统核对完成后将会通知您" isClose:YES btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
                void(^block)(BindingStatus bindingStatus) = objc_getAssociatedObject(strongSelf, &activityKey);
                if (block){
                    block(BindingStatusActiviting);
                }
                [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
                
                if (strongSelf.navigationController.childViewControllers.count == 1){
                    [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
                } else {
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    }];
}









#pragma mark - 旋转
- (BOOL)shouldAutorotate{
    //是否允许转屏
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    //viewController所支持的全部旋转方向
    return UIInterfaceOrientationMaskPortrait ;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    //viewController初始显示的方向
    return UIInterfaceOrientationPortrait;
}

#pragma mark - 判断绑定了多少次
-(void)sendRequestToPanduanBangdingNumber:(NSString *)member block:(void(^)(PDFindactivatedtimesSingleModel *singleModel))modelBlock{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"lolGameUserId":member};
    [[NetworkAdapter sharedAdapter] fetchWithPath:findactivatedtimes requestParams:params responseObjectClass:[PDFindactivatedtimesSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            PDFindactivatedtimesSingleModel *singleModel = (PDFindactivatedtimesSingleModel *)responseObject;
            if (modelBlock){
                modelBlock(singleModel);
            }
        }
    }];
}


@end
