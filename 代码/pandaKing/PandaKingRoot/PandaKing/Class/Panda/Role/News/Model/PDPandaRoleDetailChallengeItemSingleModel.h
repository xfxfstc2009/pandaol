//
//  PDPandaRoleDetailChallengeItemSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDPandaRoleDetailChallengeItemSingleModel <NSObject>

@end

@interface PDPandaRoleDetailChallengeItemSingleModel : FetchModel

@property (nonatomic,assign)BOOL win;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,assign)NSInteger championsKilled;
@property (nonatomic,assign)NSInteger numDeaths;
@property (nonatomic,assign)NSInteger assists;
@property (nonatomic,copy)NSString *gameRecordId;
@property (nonatomic,copy)NSString *gold;
@property (nonatomic,copy)NSString *championAvatar;

@end
