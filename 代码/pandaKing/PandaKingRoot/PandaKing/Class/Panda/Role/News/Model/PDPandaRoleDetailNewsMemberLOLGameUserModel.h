//
//  PDPandaRoleDetailNewsMemberLOLGameUserModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPandaRoleDetailNewsLoLGameUserModel.h"

@interface PDPandaRoleDetailNewsMemberLOLGameUserModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *gameId;
@property (nonatomic,copy)NSString *memberId;
@property (nonatomic,assign)BOOL enabled;
@property (nonatomic,copy)NSString *state;
@property (nonatomic,strong) PDPandaRoleDetailNewsLoLGameUserModel *lolGameUser;

@end



