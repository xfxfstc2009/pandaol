//
//  PDPandaRoleDetailNewsModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPandaRoleDetailNewsStatusModel.h"
#import "PDPandaRoleDetailNewsMemberLOLGameUserModel.h"

@interface PDPandaRoleDetailNewsModel : FetchModel

@property (nonatomic,assign)NSInteger totalGoldCount;                   /**< 累计获得的金币*/
@property (nonatomic,strong)PDPandaRoleDetailNewsStatusModel *stats;    /**< 详情*/
@property (nonatomic,strong)PDPandaRoleDetailNewsMemberLOLGameUserModel *memberLOLGameUser;
@end
