//
//  PDPandaZhanjiqiangCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPandaZhanjiqiangCell.h"
#import "PDRecordSingleModel.h"
#import "PDPandaRecordSingleView.h"

@interface PDPandaZhanjiqiangCell()
@property (nonatomic,strong)UIView *recordView;


@end

@implementation PDPandaZhanjiqiangCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}



#pragma mark - createView
-(void)createView{
    self.recordView = [[UIView alloc]init];
    self.recordView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.recordView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferDetailModel:(PDRoleDetailRootModel *)transferDetailModel{
    _transferDetailModel = transferDetailModel;
    CGFloat width = kScreenBounds.size.width;
    // 1. 战绩信息
    self.recordView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.transferCellHeight);
    if (self.recordView && transferDetailModel){
        [self.recordView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        for (int i = 0 ; i < 4;i++){
            CGFloat origin_X = i * width / 4.;
            CGFloat origin_y = 0;
            CGFloat size_width = width / 4.;
            CGFloat size_height = self.transferCellHeight;
            
            PDRecordSingleModel *recordSingleModel = [[PDRecordSingleModel alloc]init];
            if (i == 0 ){
                recordSingleModel.fixedStr = @"胜率";
                recordSingleModel.dymicStr = transferDetailModel.stats.winRate;
            } else if (i == 1){
                recordSingleModel.fixedStr = @"场次";
                recordSingleModel.dymicStr = transferDetailModel.stats.gameCount;
            } else if (i == 2){
                recordSingleModel.fixedStr = @"战斗力";
                recordSingleModel.dymicStr = transferDetailModel.memberLOLGameUser.lolGameUser.fightingCapacity;
            } else if (i == 3){
                recordSingleModel.fixedStr = @"KDA";
                recordSingleModel.dymicStr = transferDetailModel.stats.kda;
            }
            
            PDPandaRecordSingleView *pandaRecordSingleView = [[PDPandaRecordSingleView alloc]initWithFrame:CGRectMake(origin_X, origin_y, size_width, size_height)];
            pandaRecordSingleView.transferRecordSingleModdel = recordSingleModel;
            pandaRecordSingleView.backgroundColor = [UIColor clearColor];
            [self.recordView addSubview:pandaRecordSingleView];
        }
    }
}

+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}
@end
