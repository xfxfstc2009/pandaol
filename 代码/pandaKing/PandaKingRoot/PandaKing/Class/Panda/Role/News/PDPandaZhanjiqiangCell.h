//
//  PDPandaZhanjiqiangCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDRoleDetailRootModel.h"

@interface PDPandaZhanjiqiangCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDRoleDetailRootModel *transferDetailModel;

+(CGFloat)calculationCellHeight;

@end
