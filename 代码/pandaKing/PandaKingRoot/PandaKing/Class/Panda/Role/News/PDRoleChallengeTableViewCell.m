//
//  PDRoleChallengeTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRoleChallengeTableViewCell.h"

@interface PDRoleChallengeTableViewCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *gameNameLabel;
@property (nonatomic,strong)UILabel *challengeLabel;
@property (nonatomic,strong)UILabel *statusLabel;
@property (nonatomic,strong)UILabel *goldLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;

@end

@implementation PDRoleChallengeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    // 2. 创建名字
    self.gameNameLabel = [GWViewTool createLabelFont:@"正文" textColor:@"浅灰"];
    [self addSubview:self.gameNameLabel];
    
    // 3. 创建战绩
    self.challengeLabel = [GWViewTool createLabelFont:@"正文" textColor:@"浅灰"];
    [self addSubview:self.challengeLabel];
    
    // 4. 创建状态
    self.statusLabel = [self createStatusLabel];
    [self addSubview:self.statusLabel];
    
    // 5. 创建金币
    self.goldLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"浅灰"];
    [self addSubview:self.goldLabel];
    
    // 6.创建箭头
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.arrowImgView];
}

-(void)setTransferHomeRecordModel:(PDGamerRecordRootModel *)transferHomeRecordModel{
    _transferHomeRecordModel = transferHomeRecordModel;
 
    // 1. img
    [self.avatarImgView uploadImageWithURL:transferHomeRecordModel.championAvatar placeholder:nil callback:NULL];
    self.avatarImgView.frame = CGRectMake(LCFloat(11), LCFloat(5), (self.transferCellHeight - 2 * LCFloat(5)), (self.transferCellHeight - 2 * LCFloat(5)));
    
    // 2.label
    self.gameNameLabel.text = transferHomeRecordModel.type;
    CGSize gameSize = [self.gameNameLabel.text sizeWithCalcFont:self.gameNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gameNameLabel.font])];
    self.gameNameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), self.avatarImgView.orgin_y, gameSize.width, [NSString contentofHeightWithFont:self.gameNameLabel.font]);
    
    // 3.  zhanji
    self.challengeLabel.text =  [NSString stringWithFormat:@"%li/%li/%li",(long)transferHomeRecordModel.gamerRecord.championsKilled,(long)transferHomeRecordModel.gamerRecord.numDeaths,(long)transferHomeRecordModel.gamerRecord.assists];
    CGSize chanllengeSize = [self.challengeLabel.text sizeWithCalcFont:self.challengeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.challengeLabel.font])];
    self.challengeLabel.frame = CGRectMake(self.gameNameLabel.orgin_x, CGRectGetMaxY(self.avatarImgView.frame) - [NSString contentofHeightWithFont:self.challengeLabel.font],chanllengeSize.width , [NSString contentofHeightWithFont:self.challengeLabel.font]);
    
    self.arrowImgView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(9), (self.transferCellHeight - LCFloat(15)) / 2., LCFloat(9), LCFloat(15));
    
    // 4.
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    if (transferHomeRecordModel.win){
        self.statusLabel.text = @"胜利";
        self.statusLabel.backgroundColor = [UIColor colorWithCustomerName:@"绿"];
    } else {
        self.statusLabel.text = @"失败";
        self.statusLabel.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    }
    self.statusLabel.frame = CGRectMake(self.arrowImgView.orgin_x - LCFloat(11) - self.statusLabel.size_width, 0, self.statusLabel.size_width, self.statusLabel.size_height);
    self.statusLabel.center_y = self.gameNameLabel.center_y;
    
    // 5. gold
    self.goldLabel.text = [NSString stringWithFormat:@"+%li金币",(long)transferHomeRecordModel.gold];
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.goldLabel.font])];
    self.goldLabel.frame =CGRectMake(self.arrowImgView.orgin_x - LCFloat(11) - goldSize.width, 0, goldSize.width, [NSString contentofHeightWithFont:self.goldLabel.font]);
    self.goldLabel.center_y = self.challengeLabel.center_y;
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

+(CGFloat)calculationCellHeight{
    return LCFloat(58);
}

#pragma mark - 创建按钮
-(UILabel *)createStatusLabel{
    UILabel *statusLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"白"];
    CGSize statusSize = [@"胜利" sizeWithCalcFont:statusLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:statusLabel.font])];
    statusLabel.frame = CGRectMake(0, 0, statusSize.width + 2 * LCFloat(11), [NSString contentofHeightWithFont:statusLabel.font] + 2 * LCFloat(3));
    statusLabel.clipsToBounds = YES;
    statusLabel.layer.cornerRadius = MIN(statusLabel.size_width, statusLabel.size_height) / 2.;
    return statusLabel;
}

@end
