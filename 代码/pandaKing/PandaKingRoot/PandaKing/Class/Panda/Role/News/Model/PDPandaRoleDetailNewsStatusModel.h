//
//  PDPandaRoleDetailNewsStatusModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDPandaRoleDetailNewsStatusModel : FetchModel

@property (nonatomic,assign)NSInteger gameCount;
@property (nonatomic,assign)NSInteger winGameCount;
@property (nonatomic,assign)NSInteger loseGameCount;
@property (nonatomic,assign)CGFloat winRate;
@property (nonatomic,assign)NSInteger kill;
@property (nonatomic,assign)NSInteger death;
@property (nonatomic,assign)NSInteger assists;
@property (nonatomic,assign)NSInteger kda;
@property (nonatomic,assign)NSInteger threeKill;
@property (nonatomic,assign)NSInteger fourKill;
@property (nonatomic,assign)NSInteger fiveKill;
@property (nonatomic,assign)NSInteger superKill;
@property (nonatomic,assign)NSInteger mvpCount;
@end
