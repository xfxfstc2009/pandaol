//
//  PDRoleChallengeTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDGamerRecordRootModel.h"

@interface PDRoleChallengeTableViewCell : UITableViewCell

@property (nonatomic,strong)PDGamerRecordRootModel *transferHomeRecordModel;
@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;
@end
