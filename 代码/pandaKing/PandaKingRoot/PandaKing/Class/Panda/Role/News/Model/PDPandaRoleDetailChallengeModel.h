//
//  PDPandaRoleDetailChallengeModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPandaRoleDetailChallengeItemSingleModel.h"

@interface PDPandaRoleDetailChallengeModel : FetchModel

@property (nonatomic,assign)NSInteger pageNum;
@property (nonatomic,assign)NSInteger totalItemsCount;
@property (nonatomic,assign)NSInteger pageItemsCount;
@property (nonatomic,strong)NSArray<PDPandaRoleDetailChallengeItemSingleModel> *items;


@end
