//
//  PDGamerRecordListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDGamerRecordRootModel.h"
@interface PDGamerRecordListModel : FetchModel

@property (nonatomic,strong)NSArray<PDGamerRecordRootModel> *items;

@end
