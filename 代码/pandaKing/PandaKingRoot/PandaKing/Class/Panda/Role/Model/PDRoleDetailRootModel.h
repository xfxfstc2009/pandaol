//
//  PDRoleDetailRootModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDRoleSingleKDASingleModel.h"
#import "PDMemberLOLGameUser.h"
#import "PDPandaHomeStatusModel.h"
@interface PDRoleDetailRootModel : FetchModel

@property (nonatomic,strong)PDRoleSingleKDASingleModel *avg;            /**< KDA*/
@property (nonatomic,strong)PDPandaHomeStatusModel *stats;              /**<status*/
@property (nonatomic,assign)NSInteger goldCount;                          /**< 钱数量*/
@property (nonatomic,strong)PDMemberLOLGameUser *memberLOLGameUser;     /**< memberLolGameUser*/

@property (nonatomic,copy)NSString *gradeName;
@property (nonatomic,copy)NSString *gradeImage;




@end
