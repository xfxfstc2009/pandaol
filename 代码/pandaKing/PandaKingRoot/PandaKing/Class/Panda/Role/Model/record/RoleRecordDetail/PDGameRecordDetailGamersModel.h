//
//  PDGameRecordDetailGamersModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDGameRecordDetailGamersSingleModel.h"

@interface PDGameRecordDetailGamersModel : FetchModel

@property (nonatomic,strong)NSArray <PDGameRecordDetailGamersSingleModel> *gamers;

@end
