//
//  PDRoleRecordSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDRoleRecordSingleModel : FetchModel

@property (nonatomic,assign)NSTimeInterval time;                       /**< 时间*/
@property (nonatomic,assign)BOOL victory;                               /**< 胜败*/
@property (nonatomic,assign)NSInteger zhuzi;                            /**< 竹子*/
@property (nonatomic,copy)NSString *avatar;                             /**< 头像*/
@property (nonatomic,copy)NSString *gameType;                           /**< 比赛类型*/
@property (nonatomic,copy)NSString *record;                             /**< 战绩*/
@property (nonatomic,copy)NSString *kda;                                /**< kda*/
@property (nonatomic,copy)NSString *price;                              /**< 价格*/

@end
