//
//  PDGameRecordDetailGamersSingleGamerRecordModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDGameRecordDetailGamersSingleGamerRecordChampionModel.h"

@interface PDGameRecordDetailGamersSingleGamerRecordModel : FetchModel

@property (nonatomic,assign)NSInteger assists;              /**< 助攻*/
@property (nonatomic,assign)NSInteger barracksKilled;       /**< 兵营*/
@property (nonatomic,assign)NSInteger turretsKilled;                /**< 推塔*/

@property (nonatomic,strong)PDGameRecordDetailGamersSingleGamerRecordChampionModel *champion;
@property (nonatomic,assign)NSInteger championsKilled;      /**< 杀人数*/

@property (nonatomic,assign)NSInteger goldEarned;           /**< 收入*/
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)NSInteger largestKillingSpree;  /**< 最多连杀数*/
@property (nonatomic,assign)NSInteger largestMultiKill;     /**< 最大的多杀*/
@property (nonatomic,assign)NSInteger magicDamageDealtToChampions;  /**< 魔法伤害*/
@property (nonatomic,assign)NSInteger minionsKilled;                /**< 补兵*/
@property (nonatomic,copy)NSString *name;                           /**< 名字*/
@property (nonatomic,assign)NSInteger numDeaths;                    /**< 死亡数*/
@property (nonatomic,assign)NSInteger physicalDamageDealtToChampions;/**< 物理伤害*/
@property (nonatomic,copy)NSString *qquin;                          /**< QQ*/
@property (nonatomic,assign)NSInteger totalDamageDealt;             /**< 总伤害*/
@property (nonatomic,assign)NSInteger totalDamageDealtToChampions;  /**< 对英雄造成的总伤害*/
@property (nonatomic,assign)NSInteger totalDamageTaken;             /**< 总承受伤害*/
@property (nonatomic,assign)NSInteger totalHealth;                  /**< 总治疗*/

@property (nonatomic,assign)NSInteger win;                          /**< 胜利*/

@property (nonatomic,copy)NSString *tier;

@end
