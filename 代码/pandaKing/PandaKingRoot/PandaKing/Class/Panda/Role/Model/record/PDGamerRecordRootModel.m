//
//  PDGamerRecordRootModel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDGamerRecordRootModel.h"

@implementation PDGamerRecordRootModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end
