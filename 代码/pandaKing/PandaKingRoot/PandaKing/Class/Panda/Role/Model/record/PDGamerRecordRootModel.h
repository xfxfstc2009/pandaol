//
//  PDGamerRecordRootModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDGamerRecordSingleModel.h"

@protocol PDGamerRecordRootModel <NSObject>

@end

@interface PDGamerRecordRootModel : FetchModel

@property (nonatomic,copy)NSString *backgroundImg;
@property (nonatomic,copy)NSString *championAvatar;
@property (nonatomic,copy)NSString *date;
@property (nonatomic,copy)NSString *gameRecordId;
@property (nonatomic,copy)NSString *gameUserAvatar;

@property (nonatomic,strong)PDGamerRecordSingleModel *gamerRecord;
@property (nonatomic,assign)NSInteger gold;
@property (nonatomic,assign)CGFloat kda;
@property (nonatomic,assign)CGFloat lolGold;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,assign)NSInteger win;

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *memberId;
@property (nonatomic,assign)NSInteger statisticed;              /**< 统计*/


@end
