//
//  PDRoleSingleKDASingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDRoleSingleKDASingleModel : FetchModel

@property (nonatomic,assign)CGFloat assistsAvg;                  /**< 助攻avg*/
@property (nonatomic,assign)CGFloat deathAvg;                    /**< 死亡率*/
@property (nonatomic,assign)CGFloat killAvg;                     /**< 杀人率*/

@end
