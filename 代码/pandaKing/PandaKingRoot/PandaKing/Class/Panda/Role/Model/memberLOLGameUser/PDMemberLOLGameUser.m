//
//  PDMemberLOLGameUser.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMemberLOLGameUser.h"

@implementation PDMemberLOLGameUser

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"userId":@"id"};
}

@end
