//
//  PDGameRecordDetailGamersSingleGamerRecordModel.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDGameRecordDetailGamersSingleGamerRecordModel.h"

@implementation PDGameRecordDetailGamersSingleGamerRecordModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end
