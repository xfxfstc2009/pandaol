//
//  PDMemberLOLGameUser.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDlolGameUser.h"

@interface PDMemberLOLGameUser : FetchModel
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *userId;           /**< 账户id*/
@property (nonatomic,copy)NSString *gameId;             /**< 游戏账户*/
@property (nonatomic,copy)NSString *memberId;           /**< memberId*/
@property (nonatomic,assign)BOOL enabled;                /**< 判断是否可用*/
@property (nonatomic,strong)PDlolGameUser *lolGameUser; /**< 游戏名*/

@property (nonatomic,copy)NSString *state;              /**< 激活状态*/


@end
