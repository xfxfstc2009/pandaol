//
//  PDGameRecordDetailGamersSingleGamerRecordChampionModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDGameRecordDetailGamersSingleGamerRecordChampionModel : FetchModel

@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,assign)BOOL far;            /**< 是否远程*/
@property (nonatomic,assign)NSInteger goldPrice;    /**< 打到的钱*/
@property (nonatomic,copy)NSString *ID;             /**< 编号*/
@property (nonatomic,copy)NSString*name;            /**< 名字*/
@property (nonatomic,copy)NSString *nick;           /**< 昵称*/
@property (nonatomic,copy)NSString *showPic;        /**< 显示图片*/
@property (nonatomic,copy)NSString *title;          /**< 标题*/

@end
