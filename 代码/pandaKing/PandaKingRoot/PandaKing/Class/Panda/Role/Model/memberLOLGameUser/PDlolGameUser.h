//
//  PDlolGameUser.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDlolGameUser : FetchModel

@property (nonatomic,copy)NSString *gameUserId;             /**< 游戏userid*/
@property (nonatomic,copy)NSString *roleName;               /**< 角色名字*/
@property (nonatomic,copy)NSString *serverName;             /**< 服务器名字*/
@property (nonatomic,copy)NSString *telecomLine;            /**< 电信线路*/
@property (nonatomic,copy)NSString *gameServerId;           /**< 游戏服务器id*/
@property (nonatomic,copy)NSString *grade;                  /**< 等级*/
@property (nonatomic,copy)NSString *gradeLevel;             /**< 等级名*/
@property (nonatomic,assign)NSInteger fightingCapacity;       /**< 战斗力*/
@property (nonatomic,copy)NSString *avatar;                 /**< 头像*/

@end
