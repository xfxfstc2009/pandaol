//
//  PDGameRecordDetailGamersSingleGamerRecordChampionModel.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDGameRecordDetailGamersSingleGamerRecordChampionModel.h"

@implementation PDGameRecordDetailGamersSingleGamerRecordChampionModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end
