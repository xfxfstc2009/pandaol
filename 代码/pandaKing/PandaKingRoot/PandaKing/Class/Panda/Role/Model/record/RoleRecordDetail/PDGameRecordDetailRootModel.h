//
//  PDGameRecordDetailRootModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 战绩详情
#import "FetchModel.h"
#import "PDGameRecordDetailTeamModel.h"
@interface PDGameRecordDetailRootModel : FetchModel

@property (nonatomic,copy)NSString *duration;                   /**< 经历时间*/
@property (nonatomic,copy)NSString *gameSitutaion;              /**< 比赛局势*/

@property (nonatomic,strong)PDGameRecordDetailTeamModel *loseTeam;  /**< 失败队伍*/
@property (nonatomic,strong)PDGameRecordDetailTeamModel *winTeam;  /**< 失败队伍*/

@property (nonatomic,copy)NSString *gold;
@property (nonatomic,assign)NSInteger kill;
@property (nonatomic,copy)NSString *startTime;
@property (nonatomic,copy)NSString *type;

// temp
@property (nonatomic,strong)NSArray <PDGameRecordDetailGamersSingleModel> *tempGameUsers;

@end
