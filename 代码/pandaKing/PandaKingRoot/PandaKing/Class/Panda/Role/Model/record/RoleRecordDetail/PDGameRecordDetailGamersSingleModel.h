//
//  PDGameRecordDetailGamersSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDGameRecordDetailGamersSingleGamerModel.h"
#import "PDGameRecordDetailGamersSingleGamerRecordModel.h"

@protocol PDGameRecordDetailGamersSingleModel <NSObject>

@end

@interface PDGameRecordDetailGamersSingleModel : FetchModel

@property (nonatomic,strong) NSArray *equipments;               /**< 装备数组*/
@property (nonatomic,strong)PDGameRecordDetailGamersSingleGamerModel *gameUser;     /**< 游戏名*/
@property (nonatomic,strong)PDGameRecordDetailGamersSingleGamerRecordModel *gamerRecord;    /**< 游戏战绩*/
@property (nonatomic,assign)BOOL isUser;                                /**< 我们平台用户*/
@property (nonatomic,copy)NSString *memberId;
@property (nonatomic,copy)NSString *kda;
@property (nonatomic,copy)NSString *avatar;
@end
