//
//  PDGamerRecordSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDGameRecordSingleModel.h"

@interface PDGamerRecordSingleModel : FetchModel

@property (nonatomic,assign)NSInteger assists;                          /**< 助攻/次*/
@property (nonatomic,copy)NSString *champion;                           /**< 击杀？*/
@property (nonatomic,assign)NSInteger championsKilled;                  /**< 击杀*/
@property (nonatomic,assign)NSInteger goldEarned;                       /**< 打到的钱*/
@property (nonatomic,copy)NSString *ID;                                 /**< 编号*/
@property (nonatomic,assign)NSInteger largestKillingSpree;              /**< */
@property (nonatomic,assign)NSInteger largestMultiKill;
@property (nonatomic,assign)NSInteger numDeaths;                        /**< 死亡数*/
@property (nonatomic,copy)NSString *qquin;                              /**< qquin*/
@property (nonatomic,assign)NSInteger win;
@property (nonatomic,strong)PDGameRecordSingleModel *gameRecord;        /**< 游戏战绩*/

/*      "champion": {
 "id": "英雄id",
 "name": "xxx",
 "nick": "英雄名",
 "avatar":"/po/img/....png" //英雄头像
 },
 "championsKilled": 0,//击杀次数
 "numDeaths": 0,//死亡次数
 "assists": 0,//助攻次数
 "largestKillingSpree": 0,
 "largestMultiKill": 0,
 "goldEarned": 0//lol金币（加“k”）
 */

@end
