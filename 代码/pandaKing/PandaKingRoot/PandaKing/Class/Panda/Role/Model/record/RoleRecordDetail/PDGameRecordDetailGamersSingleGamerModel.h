//
//  PDGameRecordDetailGamersSingleGamerModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDGameRecordDetailGamersSingleGamerModel : FetchModel

@property (nonatomic,copy)NSString *avatar;         /**< 头像*/
@property (nonatomic,copy)NSString *fightingCapacity;   /**< 战斗力*/
@property (nonatomic,copy)NSString *gameServerId;       /**< 游戏服务区*/
@property (nonatomic,copy)NSString *gameUserId;         /**< 游戏id*/
@property (nonatomic,copy)NSString *grade;              /**< 等级*/
@property (nonatomic,assign)NSInteger gradeLevel;       /**< 等级*/
@property (nonatomic,copy)NSString *roleName;           /**< 角色名*/
@property (nonatomic,copy)NSString *serverName;         /**< 服务器名*/
@property (nonatomic,copy)NSString *telecomLine;        /**< 线路*/
@end
