//
//  PDGameRecordSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDGameRecordSingleModel : FetchModel

@property (nonatomic,assign)NSInteger battleMap;            /**< 战斗地图*/
@property (nonatomic,assign)NSInteger gameMode;             /**< 游戏模式*/
@property (nonatomic,assign)NSInteger gameType;             /**< 游戏类型*/
@property (nonatomic,assign)NSInteger gamerNum;             /**< 玩家数量*/
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *startTime;              /**< 开始时间*/
@property (nonatomic,copy)NSString *stopTime;               /**< 结束时间*/

@end
