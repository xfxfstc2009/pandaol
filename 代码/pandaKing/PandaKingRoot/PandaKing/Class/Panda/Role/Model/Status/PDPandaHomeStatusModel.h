//
//  PDPandaHomeStatusModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDPandaHomeStatusModel : FetchModel

@property (nonatomic,assign)NSInteger gameCount;              /**< 游戏总数*/
@property (nonatomic,copy)NSString *winGameCount;           /**< 胜场*/
@property (nonatomic,copy)NSString *loseGameCount;          /**< 输掉的比赛*/
@property (nonatomic,assign)CGFloat winRate;                /**< 胜率*/
@property (nonatomic,copy)NSString *kill;                   /**< 杀人数*/
@property (nonatomic,copy)NSString *death;                  /**< 死亡数*/
@property (nonatomic,copy)NSString *assists;                /**< 助攻*/
@property (nonatomic,assign)CGFloat kda;                    /**< kda*/
@property (nonatomic,copy)NSString *threeKill;              /**< 三杀*/
@property (nonatomic,copy)NSString *fourKill;               /**< 四杀*/
@property (nonatomic,copy)NSString *fiveKill;               /**< 五杀*/
@property (nonatomic,copy)NSString *superKill;              /**< 超神*/
@property (nonatomic,copy)NSString *mvpCount;               /**< mvp*/

@end
