//
//  PDGameRecordDetailTeamModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDGameRecordDetailGamersModel.h"
#import "PDGameRecordDetailGamersSingleModel.h"
@interface PDGameRecordDetailTeamModel : FetchModel

@property (nonatomic,strong)NSArray <PDGameRecordDetailGamersSingleModel>  *gamers;
@property (nonatomic,copy)NSString *gold;
@property (nonatomic,copy)NSString *kill;
@end
