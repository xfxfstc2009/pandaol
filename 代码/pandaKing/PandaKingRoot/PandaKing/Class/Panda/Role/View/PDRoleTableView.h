//
//  PDRoleTableView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

// 角色的table的列表

#import <UIKit/UIKit.h>

@interface PDRoleTableView : UIView

-(instancetype)initWithFrame:(CGRect)frame tableArr:(NSArray *)tableArr;

@property (nonatomic,strong)NSArray *transferArr;               /**< 传入数组*/
@property (nonatomic,assign)CGFloat transferCellHeight;

-(void)changeCellHeight:(CGFloat)transferCellHeight;

@end
