//
//  PDPandaRoleNormalCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaRoleNormalCell.h"
#import "PDChallengerProgressView.h"
#import <pop/POP.h>

@interface PDPandaRoleNormalCell()
@property (nonatomic,strong)UIView *alphaView;
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UILabel *fixedLabel;                  /**< 战绩固定*/
@property (nonatomic,strong)UILabel *dymicLabel;                  /**< 战绩动态*/
@property (nonatomic,strong)UILabel *mainLabel;                   /**< 主label*/
@property (nonatomic,strong)PDChallengerProgressView *progress;   /**< progress*/

@end

@implementation PDPandaRoleNormalCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.alphaView = [[UIView alloc]init];
    self.alphaView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.alphaView.alpha = .5f;
    [self addSubview:self.alphaView];
    
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor blackColor];
    [self addSubview:self.bgView];
    
    // label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.fixedLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.fixedLabel];
    
    // 创建record
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.dymicLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.dymicLabel];
    
    self.mainLabel = [[UILabel alloc]init];
    self.mainLabel.backgroundColor = [UIColor clearColor];
    self.mainLabel.font = [UIFont systemFontOfCustomeSize:13.];
    self.mainLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.mainLabel];
    
    // 4. 创建进度条
    CGSize contentOfDymic = [@"100.00% 胜率" sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLabel.font])];
    CGFloat width = kScreenBounds.size.width - LCFloat(11) - contentOfDymic.width - 3 * LCFloat(11);
    self.progress = [[PDChallengerProgressView alloc]initWithFrame:CGRectMake(100, 100, width, 7)];
    [self.progress animationWithProgress:0.f];
    [self addSubview:self.progress];
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    
    self.alphaView.frame = self.bounds;
    
    // 1. 添加左侧的view
    self.bgView.frame = CGRectMake(0, LCFloat(14), LCFloat(6), self.transferCellHeight - 2 * LCFloat(14));
    
}

-(void)setTransferType:(normalCellType)transferType{
    _transferType = transferType;
    if (transferType == normalCellTypeShenglv){
        self.fixedLabel.text = @"胜率";
    } else if (transferType == normalCellTypeKDA){
        self.fixedLabel.text = @"KDA";
    } else if (transferType == normalCellTypeHonor){
        self.fixedLabel.text = @"";
        self.dymicLabel.text = @"我的荣誉";
    } else if (transferType == normalCellTypeRecord){
        self.fixedLabel.text = @"";
        self.dymicLabel.text = @"最近比赛";
    }
    
    // 2. 创建胜率动态
    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLabel.font])];
    self.dymicLabel.frame = CGRectMake(LCFloat(20), LCFloat(12), dymicSize.width + LCFloat(11), [NSString contentofHeightWithFont:self.dymicLabel.font]);
    
    // 3. 创建胜率固定
    CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.dymicLabel.frame) + LCFloat(2), self.transferCellHeight - LCFloat(12) - [NSString contentofHeightWithFont:self.fixedLabel.font], fixedSize.width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    
}

-(void)setTransferRoleModel:(PDRoleDetailRootModel *)transferRoleModel{
    _transferRoleModel = transferRoleModel;
    // 1. 胜率 [执行动画]
    [self animationWithInfoWithLabel:self.dymicLabel];
    // 2. 右边的文字
    if (self.transferType == normalCellTypeShenglv){
        self.backgroundColor = [UIColor clearColor];
        self.progress.hidden = NO;
        
    } else if(self.transferType == normalCellTypeKDA){
        NSString *info = [NSString stringWithFormat:@"击杀·%.1f|死亡·%.1f|助攻·%.1f",self.transferRoleModel.avg.killAvg,self.transferRoleModel.avg.deathAvg,self.transferRoleModel.avg.assistsAvg];
        self.mainLabel.attributedText = [Tool rangeLabelWithContent:info hltContentArr:@[@"击杀·",@"|死亡·",@"|助攻·"] hltColor:c28 normolColor:c26];
        
        self.progress.hidden = YES;
        self.mainLabel.hidden = NO;
        self.backgroundColor = [UIColor whiteColor];
    } else if (self.transferType == normalCellTypeHonor){           // 荣誉
        
        NSString *info =  [NSString stringWithFormat:@"总场次·%li|胜利·%@|失败·%@",(long)transferRoleModel.stats.gameCount,transferRoleModel.stats.winGameCount,transferRoleModel.stats.loseGameCount];
        
        self.mainLabel.attributedText = [Tool rangeLabelWithContent:info hltContentArr:@[@"总场次·",@"|胜利·",@"|失败·"] hltColor:c28 normolColor:c26];
        // progress hiden
        self.progress.hidden = YES;
        self.mainLabel.hidden = YES;
        self.backgroundColor = BACKGROUND_VIEW_COLOR;
    } else if (self.transferType == normalCellTypeRecord){          // 战绩
        NSString *info =  [NSString stringWithFormat:@"累计获得金币%li",transferRoleModel.goldCount];
        
        self.mainLabel.text = info;
        self.mainLabel.backgroundColor = [UIColor whiteColor];
        
        // progress hiden
        self.progress.hidden = YES;
        self.mainLabel.hidden = NO;
        self.mainLabel.textColor = c26;
        self.backgroundColor = BACKGROUND_VIEW_COLOR;
    } else {
        self.mainLabel.hidden = YES;
        self.progress.hidden = YES;
        self.backgroundColor = BACKGROUND_VIEW_COLOR;
    }
    CGSize mainSize = [self.mainLabel.text sizeWithCalcFont:self.mainLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.mainLabel.font])];
    self.mainLabel.frame = CGRectMake(kScreenBounds.size.width - mainSize.width - LCFloat(11), CGRectGetMaxY(self.dymicLabel.frame) - [NSString contentofHeightWithFont:self.mainLabel.font], mainSize.width, [NSString contentofHeightWithFont:self.mainLabel.font]);
    
    
    if (self.transferType == normalCellTypeRecord){
        CGSize mainSize = [self.mainLabel.text sizeWithCalcFont:self.mainLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.mainLabel.font])];
        self.mainLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
        self.mainLabel.frame = CGRectMake(kScreenBounds.size.width - mainSize.width - LCFloat(11) - 2 * LCFloat(11), CGRectGetMaxY(self.dymicLabel.frame) - [NSString contentofHeightWithFont:self.mainLabel.font] - 2 * LCFloat(3), mainSize.width + 2 * LCFloat(11), [NSString contentofHeightWithFont:self.mainLabel.font] + 2* LCFloat(3));
        
        NSInteger main_temp_width = self.mainLabel.size_width;
        NSInteger main_temp_height = self.mainLabel.size_height;
        CGFloat main_radius = MIN(main_temp_width, main_temp_height) / 2.;
        self.mainLabel.layer.cornerRadius = main_radius;
        self.mainLabel.textAlignment = NSTextAlignmentCenter;
        self.mainLabel.clipsToBounds = YES;
        
        self.mainLabel.frame = CGRectMake(kScreenBounds.size.width - mainSize.width - LCFloat(11) - 2 * LCFloat(11), CGRectGetMaxY(self.dymicLabel.frame) - [NSString contentofHeightWithFont:self.mainLabel.font] - 2 * LCFloat(3), mainSize.width + 2 * LCFloat(11), main_temp_height);
    }
    
    // 3. 创建进度条
    self.progress.frame = CGRectMake(CGRectGetMaxX(self.dymicLabel.frame) + LCFloat(11), self.fixedLabel.center_y - self.progress.size_height / 2., kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.dymicLabel.frame) - LCFloat(11), self.progress.size_height);
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        CGFloat proportion = [self.transferRoleModel.stats.winGameCount integerValue] * 1.f / self.transferRoleModel.stats.gameCount ;
        [self.progress animationWithProgress: proportion];
    });
    
    if (self.transferType == normalCellTypeShenglv){
        self.alphaView.hidden = NO;
        
        self.bgView.hidden = YES;
        self.dymicLabel.font = [UIFont systemFontOfCustomeSize:12.];
        self.fixedLabel.hidden = NO;
        self.fixedLabel.textColor = c26;
        self.fixedLabel.font = [UIFont systemFontOfCustomeSize:12.];
    } else {
        self.alphaView.hidden = YES;
        self.dymicLabel.font = [UIFont systemFontOfCustomeSize:16.];
        self.bgView.hidden = NO;
        self.fixedLabel.textColor = c3;
        self.fixedLabel.font = [UIFont systemFontOfCustomeSize:15.];
    }
    
    // 修改frame
    NSString *progressStr = [NSString stringWithFormat:@"%.f%%",self.transferRoleModel.stats.winRate * 100];
    //    CGSize progressSize = [progressStr sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:12.]])];
    
    //    PDLogSize(progressSize);
    if (self.transferType == normalCellTypeShenglv){
        self.fixedLabel.text = @"胜率";
        CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
        self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.dymicLabel.frame) + LCFloat(11), 0, fixedSize.width,self.transferCellHeight);
        self.progress.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(2), 0, kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.fixedLabel.frame) - LCFloat(2), self.progress.size_height);
        self.progress.center_y = self.fixedLabel.center_y;
    } else {
        CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
        self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.dymicLabel.frame) + LCFloat(2), self.transferCellHeight - LCFloat(12) - [NSString contentofHeightWithFont:self.fixedLabel.font], fixedSize.width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    }
    
    self.dymicLabel.center_y = self.size_height / 2.;
    self.fixedLabel.center_y = self.size_height / 2.;
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(12);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"标题"]];
    cellHeight += LCFloat(12);
    return cellHeight;
}


-(void)animationWithInfoWithLabel:(UILabel *)label{
    POPBasicAnimation *anim = [POPBasicAnimation animation];
    anim.duration = 1;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    POPAnimatableProperty * prop = [POPAnimatableProperty propertyWithName:@"count" initializer:^(POPMutableAnimatableProperty *prop){
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [[obj description] floatValue];
        };
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
            if (self.transferType == normalCellTypeShenglv){
                [obj setText:[NSString stringWithFormat:@"%.2f%%",values[0]]];
            } else if (self.transferType == normalCellTypeKDA){
                [obj setText:[NSString stringWithFormat:@"%.1f",values[0]]];
            }
        };
        // dynamics threshold
        prop.threshold = 1;
    }];
    
    anim.property = prop;
    
    anim.fromValue = @(0);
    if (self.transferType == normalCellTypeShenglv){
        anim.toValue = @(self.transferRoleModel.stats.winRate * 100);
    } else if (self.transferType == normalCellTypeKDA){
        anim.toValue = @(self.transferRoleModel.stats.kda);
    }
    
    [label pop_addAnimation:anim forKey:@"counting"];
}


@end
