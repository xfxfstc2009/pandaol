//
//  PDRoleDetailAvatarCell.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRoleDetailAvatarCell.h"

@interface PDRoleDetailAvatarCell()
@property (nonatomic,strong)PDImageView *avatarImgView;                        // 头像
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *areaLabel;
@property (nonatomic,strong)PDImageView *levelImgView;
@property (nonatomic,strong)UILabel *levelNameLabel;


@end

@implementation PDRoleDetailAvatarCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    [self createRoleAvatar];
}

#pragma mark - 创建角色头像
-(void)createRoleAvatar{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.frame = CGRectMake(0, 64 + LCFloat(18) , LCFloat(60), LCFloat(60));
    self.avatarImgView.clipsToBounds = YES;
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_height / 2.;
    self.avatarImgView.layer.borderColor = c26.CGColor;
    self.avatarImgView.layer.borderWidth = .5f;
    self.avatarImgView.center_x = kScreenBounds.size.width / 2.;
    [self addSubview:self.avatarImgView];
    
    // 1. 创建名字
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:16.];
    self.nameLabel.textColor = [UIColor whiteColor];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.nameLabel];
    
    // 2. 创建区服
    self.areaLabel = [[UILabel alloc]init];
    self.areaLabel.backgroundColor = [UIColor clearColor];
    self.areaLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.areaLabel.textColor = c26;
    [self addSubview:self.areaLabel];
    
    // 3. 创建标记
    self.levelImgView = [[PDImageView alloc]init];
    self.levelImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.levelImgView];
    
    self.levelNameLabel = [[UILabel alloc]init];
    self.levelNameLabel.backgroundColor = [UIColor clearColor];
    self.levelNameLabel.font = [UIFont systemFontOfCustomeSize:8.];
    self.levelNameLabel.textColor = c15;
    self.levelNameLabel.hidden = YES;
    [self addSubview:self.levelNameLabel];
    
    
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferRoleDetailRootModel:(PDRoleDetailRootModel *)transferRoleDetailRootModel{
    _transferRoleDetailRootModel = transferRoleDetailRootModel;
    
    // 1. 更新头像
    self.avatarImgView.frame = CGRectMake(LCFloat(14), LCFloat(18),  self.avatarImgView.size_width, self.avatarImgView.size_height);
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView uploadImageWithURL:self.transferRoleDetailRootModel.memberLOLGameUser.avatar placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (image){
            strongSelf.avatarImgView.image = image;
        } else {
            strongSelf.avatarImgView.image = [UIImage imageNamed:@"icon_nordata"];
        }
    }];
    self.avatarImgView.center_x = kScreenBounds.size.width / 2.;
    
    
    // 3. 更新名字
    self.nameLabel.text = self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.roleName;
    //    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(20), self.avatarImgView.orgin_y + LCFloat(10) , kScreenBounds.size.width - CGRectGetMaxX(self.avatarImgView.frame) - LCFloat(20), [NSString contentofHeightWithFont:self.nameLabel.font]);
    self.nameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.avatarImgView.frame) + LCFloat(18), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    // 4. 区服
    
    NSString *lineStr = @"";
    if ([self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.telecomLine isEqualToString:@"CT"]){
        lineStr = @"电信";
    } else if ([self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.telecomLine isEqualToString:@"CM"]){
        lineStr = @"移动";
    } else {
        lineStr = @"其他";
    }
    
    
    // 5. 最强王者
    NSString *levelStr = @"";
    if ([self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.grade isEqualToString:@"0"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_zuiqiangwangzhe"];
        levelStr = @"最强王者";
    } else if ([self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.grade isEqualToString:@"1"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_cuicanzuanshi"];
        levelStr= @"璀璨钻石";
    }else if ([self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.grade isEqualToString:@"2"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_huaguibojin"];
        levelStr = @"华贵铂金";
    }else if ([self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.grade isEqualToString:@"3"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_rongyaohuangjin"];
        levelStr = @"荣耀黄金";
    }else if ([self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.grade isEqualToString:@"4"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_buqubaiyin"];
        levelStr= @"不屈白银";
    }else if ([self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.grade isEqualToString:@"5"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_yingyonghuangtong"];
        levelStr = @"英勇黄铜";
    }else if ([self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.grade isEqualToString:@"6"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_chaofandashi"];
        levelStr = @"超凡大师";
    }else if ([self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.grade isEqualToString:@"255"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_wuduanwei"];
        levelStr = @"无段位";
    }
    
    self.areaLabel.text = [NSString stringWithFormat:@"%@ · %@",self.transferRoleDetailRootModel.memberLOLGameUser.lolGameUser.serverName,levelStr];
    self.areaLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(5) , self.nameLabel.size_width, [NSString contentofHeightWithFont:self.areaLabel.font]);
    
    
    self.levelImgView.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) - LCFloat(35) * .4f - LCFloat(11), CGRectGetMaxY(self.avatarImgView.frame) - LCFloat(35), LCFloat(35), LCFloat(35));
    
    CGSize contentOfLevelSize = [@"超凡大师" sizeWithCalcFont:self.levelNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.levelNameLabel.font])];
    self.levelNameLabel.frame = CGRectMake(self.levelImgView.center_x - contentOfLevelSize.width / 2., CGRectGetMaxY(self.levelImgView.frame), contentOfLevelSize.width, [NSString contentofHeightWithFont:self.levelNameLabel.font]);
    
    self.areaLabel.hidden = YES;
    self.levelImgView.hidden = YES;
    self.levelNameLabel.hidden = YES;
}


+(CGFloat)calculationCellheight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(18) * 2 + LCFloat(60) + LCFloat(18) + [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:16.]];
    return cellHeight;
}
@end
