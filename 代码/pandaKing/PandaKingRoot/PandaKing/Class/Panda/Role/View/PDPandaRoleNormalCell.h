//
//  PDPandaRoleNormalCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDRoleDetailRootModel.h"

typedef NS_ENUM(NSInteger,normalCellType) {
    normalCellTypeShenglv,                          /**< 胜率*/
    normalCellTypeKDA,                              /**< KDA*/
    normalCellTypeHonor,                            /**< 荣誉*/
    normalCellTypeRecord,                           /**< 战绩*/
};

@interface PDPandaRoleNormalCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,assign)normalCellType transferType;                /**< 传入的类型*/
@property (nonatomic,strong)PDRoleDetailRootModel *transferRoleModel;   /**< 传入role信息*/

+(CGFloat)calculationCellHeight;
@end
