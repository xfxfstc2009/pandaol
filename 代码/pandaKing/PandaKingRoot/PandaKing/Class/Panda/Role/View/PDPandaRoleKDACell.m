//
//  PDPandaRoleKDACell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPandaRoleKDACell.h"
#import "PDRoleTableView.h"

@interface PDPandaRoleKDACell()
@property (nonatomic,strong)UILabel *kdaDymicLabel;                 /**< kda 动态*/
@property (nonatomic,strong)UILabel *kdaFixedLabel;                 /**< kda 固定*/
@property (nonatomic,strong)UILabel *countLabel;
@property (nonatomic,strong)PDRoleTableView *cellView;
@end

@implementation PDPandaRoleKDACell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    NSArray *tempArr = @[@[@"杀人/次",@"0"],@[@"死亡/次",@"0"],@[@"助攻/次",@"0"]];
    self.cellView = [[PDRoleTableView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(70)) tableArr:tempArr];
    self.cellView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.cellView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    //    self.cellView.size_height = transferCellHeight;
    
}

-(void)setTransferRoleDetaolModel:(PDRoleDetailRootModel *)transferRoleDetaolModel{
    _transferRoleDetaolModel = transferRoleDetaolModel;
    if (transferRoleDetaolModel){
        if (self.transferType == PandaRoleTableTypeKDA){
            if (transferRoleDetaolModel.stats){
                NSArray *transArr = @[@[@"杀人/次",transferRoleDetaolModel.stats.kill],@[@"死亡/次",transferRoleDetaolModel.stats.death],@[@"助攻/次",transferRoleDetaolModel.stats.assists]];
                
                self.cellView.transferArr = transArr;
            }
            
        } else if (self.transferType == PandaRoleTableTypeHornor){
            if (transferRoleDetaolModel.stats){
                NSArray *transferArr = @[@[@"三杀",transferRoleDetaolModel.stats.threeKill],@[@"四杀",transferRoleDetaolModel.stats.fourKill],@[@"五杀",transferRoleDetaolModel.stats.fiveKill],@[@"超神",transferRoleDetaolModel.stats.superKill]];
                self.cellView.transferArr = transferArr;
            }
        }
    }
    [self.cellView changeCellHeight:LCFloat(70)];
}

-(void)setTransferType:(PandaRoleTableType)transferType{
    _transferType = transferType;
    
    if (self.transferType == PandaRoleTableTypeKDA){
        NSArray *tempArr = @[@[@"杀人/次",@"0"],@[@"死亡/次",@"0"],@[@"助攻/次",@"0"]];
        self.cellView.transferArr = tempArr;
    } else if (self.transferType == PandaRoleTableTypeHornor){
        NSArray *transferArr = @[@[@"三杀",@"0"],@[@"四杀",@"0"],@[@"五杀",@"0"],@[@"超神",@"0"]];
        self.cellView.transferArr = transferArr;
    }
}




+(CGFloat)calculationCellHeight{
    return LCFloat(85);
}

@end
