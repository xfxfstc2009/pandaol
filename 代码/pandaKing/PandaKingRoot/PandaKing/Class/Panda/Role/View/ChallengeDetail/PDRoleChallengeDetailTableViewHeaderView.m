//
//  PDRoleChallengeDetailTableViewHeaderView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRoleChallengeDetailTableViewHeaderView.h"
#import "PDRoleChallengeDetailTableViewHeaderEquipmentsView.h"
static char avataraKey;
@interface PDRoleChallengeDetailTableViewHeaderView()
@property (nonatomic,strong)UIView *teamBgView;
@property (nonatomic,strong)UIView *teamLeftView;
@property (nonatomic,strong)UILabel *teamLabel;                     /**< 队伍名字*/
@property (nonatomic,strong)UILabel *teamCountLabel;                /**< 队伍战绩*/


@property (nonatomic,strong)UIView *gameView;
@property (nonatomic,strong)PDImageView *avatar;
@property (nonatomic,strong)UILabel *rolenameLabel;                 /**< 召唤师名字*/
@property (nonatomic,strong)PDImageView *levelImgView;              /**< 等级图片*/
@property (nonatomic,strong)UILabel *levelLabel;                    /**< 等级label*/
@property (nonatomic,strong)UILabel *challengeLabel;                /**< 战绩信息label*/
@property (nonatomic,strong)PDRoleChallengeDetailTableViewHeaderEquipmentsView *equipmentView;                  /**< 装备view*/
@property (nonatomic,strong)PDImageView *eyeView;                   /**< 眼位view*/
@property (nonatomic,strong)PDImageView *userAvaImgView;            /**< 用户view*/

@property (nonatomic,strong)PDImageView *arrowShowImgView;          /**< 打开状态的箭头*/
@property (nonatomic,strong)PDImageView *arrowHidenImgView;         /**< 隐藏状态的箭头*/

@property (nonatomic,strong)UIButton *avatarButton;

@end

@implementation PDRoleChallengeDetailTableViewHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createTeamBgView];
        [self createView];
        [self createTapGesture];
    }
    return self;
}

-(void)createTapGesture{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self addGestureRecognizer:tap];
}

#pragma mark - createView
-(void)createView{
    // 0.创建游戏view
    self.gameView = [[UIView alloc]init];
    self.gameView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.gameView];
    
    // 1. 创建头像
    self.avatar = [[PDImageView alloc]init];
    self.avatar.backgroundColor = [UIColor clearColor];
    [self.gameView addSubview:self.avatar];
    
    // 2. 创建名字
    self.rolenameLabel = [[UILabel alloc]init];
    self.rolenameLabel.backgroundColor = [UIColor clearColor];
    self.rolenameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.rolenameLabel.adjustsFontSizeToFitWidth = YES;
    [self.gameView addSubview:self.rolenameLabel];
    
    // 3. 创建等级view
    self.levelImgView = [[PDImageView alloc]init];
    self.levelImgView.backgroundColor = [UIColor clearColor];
    [self.gameView addSubview:self.levelImgView];
    
    // 4. 创建等级label
    self.levelLabel = [[UILabel alloc]init];
    self.levelLabel.backgroundColor = [UIColor clearColor];
    self.levelLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.levelLabel.textColor = c26;
    [self.gameView addSubview:self.levelLabel];
    
    // 5. 创建战绩
    self.challengeLabel = [[UILabel alloc]init];
    self.challengeLabel.backgroundColor = [UIColor clearColor];
    self.challengeLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.challengeLabel.adjustsFontSizeToFitWidth = YES;
    self.challengeLabel.textAlignment = NSTextAlignmentRight;
    [self.gameView addSubview:self.challengeLabel];

    
    // 6. 创建装备view
    self.equipmentView = [[PDRoleChallengeDetailTableViewHeaderEquipmentsView alloc]initWithFrame:CGRectMake(0, 0, 3 * LCFloat(21) + 2 * LCFloat(1), 2 * LCFloat(21) + LCFloat(1))];
    self.equipmentView.backgroundColor = [UIColor clearColor];
    [self.gameView addSubview:self.equipmentView];
    
    // 7.创建图片
    self.eyeView = [[PDImageView alloc]init];
    self.eyeView.backgroundColor = [UIColor clearColor];
    [self.gameView addSubview:self.eyeView];
    
    // 8. 创建头像
    self.userAvaImgView = [[PDImageView alloc]init];
    self.userAvaImgView.backgroundColor = [UIColor clearColor];
    [self.gameView addSubview:self.userAvaImgView];
    
    self.avatarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.avatarButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)waekSekf = self;
    [self.avatarButton buttonWithBlock:^(UIButton *button) {
        if (!waekSekf){
            return ;
        }
        __strong typeof(waekSekf)strongSelf = waekSekf;
        void(^block)(PDGameRecordDetailGamersSingleModel *transferGamersSingleModel) = objc_getAssociatedObject(strongSelf, &avataraKey);
        if (block){
            block(strongSelf.transferGamersSingleModel);
        }
    }];
    [self.gameView addSubview:self.avatarButton];
    
    
    // 9创建箭头
    self.arrowShowImgView = [[PDImageView alloc]init];
    self.arrowShowImgView.backgroundColor = [UIColor clearColor];
    self.arrowShowImgView.image = [UIImage imageNamed:@"img_challengeDetail_hlt"];
    [self.gameView addSubview:self.arrowShowImgView];
    
    // 10.创建隐藏箭头
    self.arrowHidenImgView = [[PDImageView alloc]init];
    self.arrowHidenImgView.backgroundColor = [UIColor clearColor];
    self.arrowHidenImgView.image = [UIImage imageNamed:@"img_challengeDetail_nor"];
    [self.gameView addSubview:self.arrowHidenImgView];
    
}

#pragma mark - 创建背景view
-(void)createTeamBgView{
    self.teamBgView = [[UIView alloc]init];
    self.teamBgView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.teamBgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(23));
    [self addSubview:self.teamBgView];
    
    // 2. 创建view
    self.teamLeftView = [[UIView alloc]init];
    self.teamLeftView.backgroundColor = [UIColor clearColor];
    self.teamLeftView.frame = CGRectMake(0, (LCFloat(23) - LCFloat(12)) / 2., LCFloat(3), LCFloat(12));
    [self.teamBgView addSubview:self.teamLeftView];
    
    // 3. 创建胜利队伍
    self.teamLabel = [[UILabel alloc]init];
    self.teamLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self.teamBgView addSubview:self.teamLabel];
    
    // 4. 创建团队label
    self.teamCountLabel = [[UILabel alloc]init];
    self.teamCountLabel.backgroundColor = [UIColor clearColor];
    self.teamCountLabel.textAlignment = NSTextAlignmentRight;
    self.teamCountLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.teamCountLabel.textColor = c28;
    [self.teamBgView addSubview:self.teamCountLabel];
}

-(void)setTransferTeamDetailModel:(PDGameRecordDetailTeamModel *)transferTeamDetailModel{
    _transferTeamDetailModel = transferTeamDetailModel;
    
}

-(void)setTransferGamersSingleModel:(PDGameRecordDetailGamersSingleModel *)transferGamersSingleModel{
    _transferGamersSingleModel = transferGamersSingleModel;
    
    // 1. 判断是否显示上面的队伍信息
    if (self.gameStatus == GameDetailStatusTypeWin){
        self.teamBgView.hidden = NO;
        self.gameView.frame = CGRectMake(0, CGRectGetMaxY(self.teamBgView.frame), kScreenBounds.size.width, LCFloat(60));
        // 胜利标记
        self.teamLeftView.backgroundColor = c36;
        self.teamLabel.text = @"胜利队伍";
        self.teamLabel.textColor = c36;
        
    } else if (self.gameStatus == GameDetailStatusTypeLose){
        self.teamBgView.hidden = NO;
        self.gameView.frame = CGRectMake(0, CGRectGetMaxY(self.teamBgView.frame), kScreenBounds.size.width, LCFloat(60));
        // 失败标记
        self.teamLeftView.backgroundColor = c37;
        self.teamLabel.text = @"失败队伍";
        self.teamLabel.textColor = c37;
    } else {
        self.teamBgView.hidden = YES;
        self.gameView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(60));
    }
    
    // 1. 计算失败队伍位置
    CGSize teamLabelSize = [self.teamLabel.text sizeWithCalcFont:self.teamLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.teamLabel.font])];
    self.teamLabel.frame = CGRectMake(CGRectGetMaxX(self.teamLeftView.frame) + LCFloat(20), 0, teamLabelSize.width, self.teamBgView.size_height);
    
    // 2. 计算人头数量
    self.teamCountLabel.text = [NSString stringWithFormat:@"人头：%@  金钱：%@",self.transferTeamDetailModel.kill,self.transferTeamDetailModel.gold];
    self.teamCountLabel.frame = CGRectMake(CGRectGetMaxX(self.teamLabel.frame) + LCFloat(11), 0, kScreenBounds.size.width - LCFloat(11) - CGRectGetMaxX(self.teamLabel.frame) - LCFloat(11), self.teamBgView.size_height);
    
    
    // 【创建用户】
    
    // 1. 创建头像
    [self.avatar uploadImageWithURL:transferGamersSingleModel.gamerRecord.champion.avatar placeholder:nil callback:NULL];
    self.avatar.frame = CGRectMake(LCFloat(9), (self.gameView.size_height - LCFloat(43)) / 2., LCFloat(43), LCFloat(43));
    
    // 2. 创建名字
    self.rolenameLabel.text = transferGamersSingleModel.gamerRecord.name;
    CGSize roleNameSize = [@"领着与一起走" sizeWithCalcFont:self.rolenameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.rolenameLabel.font])];
    
    self.rolenameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatar.frame) + LCFloat(11), self.avatar.orgin_y, roleNameSize.width, [NSString contentofHeightWithFont:self.rolenameLabel.font]);
    if ([self.transferGamersSingleModel.memberId isEqualToString:[AccountModel sharedAccountModel].memberId]){
        self.rolenameLabel.textColor = c26;
    } else {
        self.rolenameLabel.textColor = [UIColor blackColor];
    }
    // 3. 创建标记
    if ([self.transferGamersSingleModel.gamerRecord.tier isEqualToString:@"0"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_zuiqiangwangzhe"];
        self.levelLabel.text = @"最强王者";
    } else if ([self.transferGamersSingleModel.gamerRecord.tier isEqualToString:@"1"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_cuicanzuanshi"];
        self.levelLabel.text = @"璀璨钻石";
    } else if ([self.transferGamersSingleModel.gamerRecord.tier isEqualToString:@"2"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_huaguibojin"];
        self.levelLabel.text = @"华贵铂金";
    } else if ([self.transferGamersSingleModel.gamerRecord.tier isEqualToString:@"3"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_rongyaohuangjin"];
        self.levelLabel.text = @"荣耀黄金";
    } else if ([self.transferGamersSingleModel.gamerRecord.tier isEqualToString:@"4"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_buqubaiyin"];
        self.levelLabel.text = @"不屈白银";
    } else if ([self.transferGamersSingleModel.gamerRecord.tier isEqualToString:@"5"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_yingyonghuangtong"];
        self.levelLabel.text = @"英勇黄铜";
    } else if ([self.transferGamersSingleModel.gamerRecord.tier isEqualToString:@"6"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_chaofandashi"];
        self.levelLabel.text = @"超凡大师";
    } else if ([self.transferGamersSingleModel.gamerRecord.tier isEqualToString:@"255"]){
        self.levelImgView.image = [UIImage imageNamed:@"icon_lol_level_wuduanwei"];
        self.levelLabel.text = @"无段位";
    } else if ([self.transferGamersSingleModel.gamerRecord.tier isEqualToString:@"-1"]){
        self.levelImgView.image = nil;
        self.levelLabel.text = @"";
    }
    self.levelImgView.frame = CGRectMake(self.rolenameLabel.orgin_x, CGRectGetMaxY(self.rolenameLabel.frame) + LCFloat(7), LCFloat(20), LCFloat(20));
    
    CGSize levelSize = [self.levelLabel.text sizeWithCalcFont:self.levelLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.levelLabel.font])];
    self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.levelImgView.frame), 0, levelSize.width, [NSString contentofHeightWithFont:self.levelLabel.font]);
    self.levelLabel.center_y = self.levelImgView.center_y;
    
    // 2.创建头像
    self.userAvaImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(9) - self.avatar.size_width, self.avatar.orgin_y, self.avatar.size_width, self.avatar.size_height);
    [self.userAvaImgView uploadImageWithURL:self.transferGamersSingleModel.avatar placeholder:nil callback:NULL];
    self.userAvaImgView.clipsToBounds = YES;
    self.userAvaImgView.layer.cornerRadius = self.userAvaImgView.size_height / 2.;
    
    self.avatarButton.frame = self.userAvaImgView.frame;
    
    // 3. 创建眼位
    if (self.transferGamersSingleModel.equipments.count == 7){
        NSString *equipment = [self.transferGamersSingleModel.equipments lastObject];
        [self.eyeView uploadImageWithURL:equipment placeholder:nil callback:NULL];
        self.eyeView.frame = CGRectMake(self.userAvaImgView.orgin_x - LCFloat(9) - LCFloat(21), 0, LCFloat(21), LCFloat(21));
        self.eyeView.center_y = self.userAvaImgView.center_y;
    }
    
    // 4.创建装备
    self.equipmentView.transferEquipmentsArr = self.transferGamersSingleModel.equipments;
    self.equipmentView.frame = CGRectMake(self.eyeView.orgin_x - LCFloat(1) - self.equipmentView.size_width, 0, self.equipmentView.size_width, self.equipmentView.size_height);
    self.equipmentView.center_y = self.eyeView.center_y;
    
    // 5. 创建战绩信息
    self.challengeLabel.text = [NSString stringWithFormat:@"%li/%li/%li",(long)self.transferGamersSingleModel.gamerRecord.championsKilled,(long)self.transferGamersSingleModel.gamerRecord.numDeaths,(long)self.transferGamersSingleModel.gamerRecord.assists];
    self.challengeLabel.frame = CGRectMake(MAX(CGRectGetMaxX(self.rolenameLabel.frame), CGRectGetMaxX(self.levelLabel.frame)) + LCFloat(11) , 0, (self.equipmentView.orgin_x - MAX(CGRectGetMaxX(self.rolenameLabel.frame), CGRectGetMaxX(self.levelLabel.frame)) - 2 * LCFloat(11)), self.gameView.size_height);
    
    self.arrowShowImgView.frame = CGRectMake(0, self.gameView.size_height - LCFloat(4), kScreenBounds.size.width, LCFloat(4));
    self.arrowShowImgView.hidden = YES;
    
    self.arrowHidenImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(4));

}

-(void)setGameStatus:(GameDetailStatusType)gameStatus{
    _gameStatus = gameStatus;
}

+(CGFloat)calculationCellHeightWithIndex:(NSInteger)index{
    if (index == 0){
        return (LCFloat(120) + LCFloat(46)) / 2.;
    } else {
        return LCFloat(120) / 2.;
    }
}

#pragma mark - ActionClickManager
-(void)tapManager{
    if (self.delegate && [self.delegate respondsToSelector:@selector(foldTableViewHeaderDidSelectedWithIndex:)]){
        [self.delegate foldTableViewHeaderDidSelectedWithIndex:self.transferCellIndex];
    }
}

-(void)showBottomStatusBar:(BOOL)show{
    if (show){
        self.arrowShowImgView.hidden = NO;
    } else {
        self.arrowShowImgView.hidden = YES;
    }
}

-(void)showTopStatusBar:(BOOL)show{
    if(show){
        self.arrowHidenImgView.hidden = NO;
    } else {
        self.arrowHidenImgView.hidden = YES;
    }
}


-(void)avatarButtonClickBlock:(void(^)(PDGameRecordDetailGamersSingleModel *transferGamersSingleModel))block{
    objc_setAssociatedObject(self, &avataraKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
