//
//  PDPandaZhanjiqiangCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPandaHomeRootModel.h"

@interface PDPandaZhanjiqiangCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDPandaHomeRootModel *transferHomeRootModel;                /**< 传入的战绩信息*/

+(CGFloat)calculationCellHeight;

@end
