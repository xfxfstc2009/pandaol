//
//  PDRoleChallengerSingleLabelView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDRoleChallengerSingleLabelView : UIView

@property (nonatomic,copy)NSString *transferFixedInfo;

@property (nonatomic,assign)NSInteger transferDymicInfo;

@end
