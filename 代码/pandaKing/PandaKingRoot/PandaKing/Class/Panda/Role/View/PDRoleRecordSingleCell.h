//
//  PDRoleRecordSingleCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDRoleRecordSingleModel.h"
#import "PDGamerRecordRootModel.h"

@interface PDRoleRecordSingleCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;

// 首页使用的战绩信息
@property (nonatomic,strong)PDGamerRecordRootModel *transferHomeRecordModel;

+(CGFloat)calculationCellHeight;


- (void)startAnimationWithDelay:(CGFloat)delayTime ;
@end
