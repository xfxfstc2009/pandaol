//
//  PDRoleChallengeDetailTableViewHeaderEquipmentsView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRoleChallengeDetailTableViewHeaderEquipmentsView.h"

#define kMarinWidth         LCFloat(1)
#define kTagWidthWithProductRelease LCFloat(21)
#define kTagHeightWithProductRelease LCFloat(21)

@interface PDRoleChallengeDetailTableViewHeaderEquipmentsView()
@property (nonatomic,strong)UIView *equipmentsBgView;

@end

@implementation PDRoleChallengeDetailTableViewHeaderEquipmentsView

-(instancetype)init{
    self = [super init];
    if (self){
        self.bounds = CGRectMake(0, 0, 3 * kTagWidthWithProductRelease + LCFloat(2), 2 * kTagWidthWithProductRelease + LCFloat(1));
        [self createView];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.bounds = CGRectMake(0, 0, 3 * kTagWidthWithProductRelease + LCFloat(2), 2 * kTagWidthWithProductRelease + LCFloat(1));
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.equipmentsBgView = [[UIView alloc]init];
    self.equipmentsBgView.backgroundColor = [UIColor clearColor];
    self.equipmentsBgView.frame = self.bounds;
    [self addSubview:self.equipmentsBgView];
}

-(void)setTransferEquipmentsArr:(NSArray *)transferEquipmentsArr{
    _transferEquipmentsArr = transferEquipmentsArr;
    if (self.equipmentsBgView.subviews.count){
        [self.equipmentsBgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    if (transferEquipmentsArr.count){
        for (int i = 0 ; i < 6;i++){
            [self createCustomerItemWithIndex:i];
        }
    }
}

#pragma mark - CreateCustomerItem
-(void)createCustomerItemWithIndex:(NSInteger)index{
    // 1. origin_x
    CGFloat origin_x = kMarinWidth + (index % 3) * kTagWidthWithProductRelease + (index % 3) * kMarinWidth;
    // 2. origin_y
    CGFloat origin_y = 0 + ( index / 3 ) * (kTagHeightWithProductRelease + kMarinWidth);
    // 3 .frame
    CGRect tempRect = CGRectMake(origin_x, origin_y, kTagWidthWithProductRelease,kTagHeightWithProductRelease);
    
    NSString *imageURL = [self.transferEquipmentsArr objectAtIndex:index];

    // 2. 创建imageview
    PDImageView *imageView = [[PDImageView alloc]init];
    imageView.backgroundColor = [UIColor clearColor];
    [imageView uploadImageWithURL:imageURL placeholder:nil callback:^(UIImage *image) {
        if (!image){
            imageView.image = [UIImage imageNamed:@"icon_pandaRole_zhuangbei_nor"];
        }
    }];
    imageView.frame = tempRect;
    [self.equipmentsBgView addSubview:imageView];
}

@end
