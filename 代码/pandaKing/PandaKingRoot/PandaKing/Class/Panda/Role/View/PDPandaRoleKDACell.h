//
//  PDPandaRoleKDACell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDRoleDetailRootModel.h"

typedef NS_ENUM(NSInteger,PandaRoleTableType) {
    PandaRoleTableTypeKDA,                          /**< KDA*/
    PandaRoleTableTypeHornor,                       /**< 荣誉*/
    
};

@interface PDPandaRoleKDACell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDRoleDetailRootModel *transferRoleDetaolModel;
@property (nonatomic,assign)PandaRoleTableType transferType;

+(CGFloat)calculationCellHeight;

@end
