//
//  PDRoleChallengeDetailTableViewCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 对战详情cell 
#import <UIKit/UIKit.h>
#import "PDGameRecordDetailGamersSingleModel.h"

@interface PDRoleChallengeDetailTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDGameRecordDetailGamersSingleModel *transferGamersSingleModel;


+(CGFloat)calculationCellHeight;
@end
