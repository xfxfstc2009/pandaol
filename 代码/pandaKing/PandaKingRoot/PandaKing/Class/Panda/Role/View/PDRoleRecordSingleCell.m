//
//  PDRoleRecordSingleCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRoleRecordSingleCell.h"
#import <pop/POP.h>

@interface PDRoleRecordSingleCell()
@property (nonatomic,strong)UILabel *timeLabel;                     /**< 时间label*/
@property (nonatomic,strong)UILabel *endLabel;                      /**< 结果label*/
@property (nonatomic,strong)UILabel *zhuziLabel;                    /**< 竹子label*/
@property (nonatomic,strong)UIView *bgView;                         /**< 背景view*/
@property (nonatomic,strong)UIView *leftBgView;                     /**< 左侧的条*/
@property (nonatomic,strong)PDImageView *avatarImgView;             /**< 头像*/
@property (nonatomic,strong)UILabel *gameTypeLabel;                 /**< 比赛类型*/
@property (nonatomic,strong)UILabel *recordLabel;                   /**< 战绩label*/
@property (nonatomic,strong)UILabel *kdaLabel;                      /**< KDA label*/
@property (nonatomic,strong)UILabel *priceLabel;                    /**< 得到的钱label*/


@end

@implementation PDRoleRecordSingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = BACKGROUND_VIEW_COLOR;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建时间label
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.timeLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.timeLabel];
    
    // 2. 创建比赛结果label
    self.endLabel = [[UILabel alloc]init];
    self.endLabel.backgroundColor = [UIColor clearColor];
    self.endLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self addSubview:self.endLabel];
    
    // 3. 创建竹子
    self.zhuziLabel = [[UILabel alloc]init];
    self.zhuziLabel.backgroundColor = [UIColor clearColor];
    self.zhuziLabel.font = self.timeLabel.font;
    self.zhuziLabel.textColor = c15;
    [self addSubview:self.zhuziLabel];
    
    // 4. 创建北京view
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    
    // 5. 创建左侧的条
    self.leftBgView = [[UIView alloc]init];
    [self.bgView addSubview:self.leftBgView];
    
    // 6. 创建头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.avatarImgView];
    
    // 7.创建比赛类型
    self.gameTypeLabel = [[UILabel alloc]init];
    self.gameTypeLabel.backgroundColor = [UIColor clearColor];
    self.gameTypeLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.gameTypeLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self.bgView addSubview:self.gameTypeLabel];
    
    // 8.创建比赛的战绩
    self.recordLabel = [[UILabel alloc]init];
    self.recordLabel.backgroundColor = [UIColor clearColor];
    self.recordLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.recordLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self.bgView addSubview:self.recordLabel];
    
    // 9. 创建kda
    self.kdaLabel = [[UILabel alloc]init];
    self.kdaLabel.backgroundColor = [UIColor clearColor];
    self.kdaLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.kdaLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self.bgView addSubview:self.kdaLabel];
    
    // 10.创建打到的钱
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.priceLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self.bgView addSubview:self.priceLabel];
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}




-(void)setTransferHomeRecordModel:(PDGamerRecordRootModel *)transferHomeRecordModel{
    _transferHomeRecordModel = transferHomeRecordModel;
    
    // 1. 创建时间
    self.timeLabel.text = [NSString stringWithFormat:@"%@·",transferHomeRecordModel.date];
    CGSize contentOfTimeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.timeLabel.font])];
    self.timeLabel.frame = CGRectMake(LCFloat(25), 0, contentOfTimeSize.width, [NSString contentofHeightWithFont:self.timeLabel.font]);
    
    CGFloat margin_y = (self.transferCellHeight - LCFloat(65));
    self.timeLabel.size_height = margin_y;
    
    // 2. 创建胜利是否
    self.endLabel.backgroundColor = [UIColor clearColor];
    if (transferHomeRecordModel.win){     // 胜利
        self.endLabel.text = @"胜利";
        self.endLabel.textColor = [UIColor colorWithCustomerName:@"绿"];
    } else {
        self.endLabel.text = @"失败";
        self.endLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    }
    
    CGSize endSize = [self.endLabel.text sizeWithCalcFont:self.endLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.endLabel.font])];
    self.endLabel.frame = CGRectMake(CGRectGetMaxX(self.timeLabel.frame), self.timeLabel.orgin_y, endSize.width, margin_y);
    
    // 3. 创建竹子
    self.zhuziLabel.text = [NSString stringWithFormat:@"金币+%li",(long)transferHomeRecordModel.gold];
    CGSize zhuziSize = [self.zhuziLabel.text sizeWithCalcFont:self.zhuziLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.zhuziLabel.font])];
    self.zhuziLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - zhuziSize.width, self.timeLabel.orgin_y, zhuziSize.width, margin_y);
    
    // 4. 创建背景
    self.bgView.frame = CGRectMake(0, CGRectGetMaxY(self.timeLabel.frame), kScreenBounds.size.width, LCFloat(65));
    
    // 5. 创建颜色view
    if (transferHomeRecordModel.win){
        self.leftBgView.backgroundColor = [UIColor colorWithCustomerName:@"绿"];
    } else {
        self.leftBgView.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    }
    self.leftBgView.frame = CGRectMake(0, 0, LCFloat(6), self.bgView.size_height);
    
    // 6.创建头像
    self.avatarImgView.frame = CGRectMake(self.timeLabel.orgin_x, LCFloat(11), self.bgView.size_height - 2 * LCFloat(11), self.bgView.size_height - 2 * LCFloat(11));
    [self.avatarImgView uploadImageWithURL:transferHomeRecordModel.championAvatar placeholder:nil callback:NULL];
    
    // 7.排位赛
    self.gameTypeLabel.text = transferHomeRecordModel.type;
    CGSize gameTypeSize = [self.gameTypeLabel.text sizeWithCalcFont:self.gameTypeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gameTypeLabel.font])];
    
    // 8. 战绩
    self.recordLabel.text = [NSString stringWithFormat:@"%li/%li/%li",(long)transferHomeRecordModel.gamerRecord.championsKilled,(long)transferHomeRecordModel.gamerRecord.numDeaths,(long)transferHomeRecordModel.gamerRecord.assists];
    CGSize recordSize = [self.recordLabel.text sizeWithCalcFont:self.recordLabel.font  constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.recordLabel.font])];
    
    // 9. 创建kda
    self.kdaLabel.text = [NSString stringWithFormat:@"%.1f KDA",transferHomeRecordModel.kda];
    CGSize kdaSize = [self.kdaLabel.text sizeWithCalcFont:self.kdaLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.kdaLabel.font])];
    
    // 10.创建钱
    self.priceLabel.text = [NSString stringWithFormat:@"%.f k",transferHomeRecordModel.lolGold];
    CGSize priceSize = [self.priceLabel.text sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.priceLabel.font])];
    
    CGFloat margin = (kScreenBounds.size.width - CGRectGetMaxX(self.avatarImgView.frame) - LCFloat(11) - gameTypeSize.width - recordSize.width - kdaSize.width - priceSize.width) / 4.;
    self.gameTypeLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + margin, 0, gameTypeSize.width, self.bgView.size_height);
    
    self.recordLabel.frame = CGRectMake(CGRectGetMaxX(self.gameTypeLabel.frame) + margin, 0, recordSize.width, self.bgView.size_height);
    
    self.kdaLabel.frame = CGRectMake(CGRectGetMaxX(self.recordLabel.frame) + margin, 0, kdaSize.width, self.bgView.size_height);
    
    self.priceLabel.frame = CGRectMake(CGRectGetMaxX(self.kdaLabel.frame) + margin, 0, priceSize.width, self.bgView.size_height);
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(90);
    return cellHeight;
}



#pragma mark - Animation
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    if (self.highlighted) {
        [self showBlunesManager1];
        
    } else {
        [self showBlunesManager];
    }
}

-(void)showBlunesManager{
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.duration           = 0.2f;
    scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(0.95, 0.95)];
    [self.timeLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.avatarImgView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

-(void)showBlunesManager1{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(6, 6)];
    scaleAnimation.springBounciness    = 1.f;
    [self.timeLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.avatarImgView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)startAnimationWithDelay:(CGFloat)delayTime {
    //    self.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    //    [UIView animateWithDuration:1. delay:delayTime usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
    //        self.transform = CGAffineTransformIdentity;
    //    } completion:^(BOOL finished) {
    //        [self showBlunesManager];
    //    }];
}

@end
