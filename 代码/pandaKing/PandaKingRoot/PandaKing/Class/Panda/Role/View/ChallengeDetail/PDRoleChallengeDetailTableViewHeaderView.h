//
//  PDRoleChallengeDetailTableViewHeaderView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 1. 对战详情头部信息
#import <UIKit/UIKit.h>
#import "PDGameRecordDetailGamersSingleModel.h"
#import "PDGameRecordDetailTeamModel.h"

typedef NS_ENUM(NSInteger,GameDetailStatusType) {
    GameDetailStatusTypeWin,
    GameDetailStatusTypeLose,
    GameDetailStatusTypeNormal,
};

@protocol PDRoleChallengeDetailTableViewHeaderViewDelegate <NSObject>

-(void)foldTableViewHeaderDidSelectedWithIndex:(NSInteger)cellIndex;

@end

@interface PDRoleChallengeDetailTableViewHeaderView : UIView

@property (nonatomic,assign)CGFloat transferHeight;                 /**< 传递的高度*/
@property (nonatomic,weak)id<PDRoleChallengeDetailTableViewHeaderViewDelegate> delegate;
@property (nonatomic,strong)PDGameRecordDetailGamersSingleModel *transferGamersSingleModel;
@property (nonatomic,assign)NSInteger transferCellIndex;
@property (nonatomic,assign)GameDetailStatusType gameStatus;
@property (nonatomic,strong)PDGameRecordDetailTeamModel *transferTeamDetailModel;
@property (nonatomic,assign)BOOL bottomIsShow;
@property (nonatomic,assign)BOOL topIsShow;


+(CGFloat)calculationCellHeightWithIndex:(NSInteger)index;

-(void)showBottomStatusBar:(BOOL)show;

-(void)showTopStatusBar:(BOOL)show;

-(void)avatarButtonClickBlock:(void(^)(PDGameRecordDetailGamersSingleModel *transferGamersSingleModel))block;
@end
