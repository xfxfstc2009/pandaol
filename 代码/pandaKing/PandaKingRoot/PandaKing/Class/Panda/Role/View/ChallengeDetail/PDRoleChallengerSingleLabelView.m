//
//  PDRoleChallengerSingleLabelView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRoleChallengerSingleLabelView.h"
#import <pop/POP.h>

@interface PDRoleChallengerSingleLabelView()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;

@end

@implementation PDRoleChallengerSingleLabelView

-(instancetype)init{
    self = [super init];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建fixedLabel
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont systemFontOfCustomeSize:11.];
    self.fixedLabel.textColor = c28;
    [self addSubview:self.fixedLabel];
 
    // 2. 创建动态Label
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont systemFontOfCustomeSize:11.];
    self.dymicLabel.textColor = c28;
    [self addSubview:self.dymicLabel];
}


#pragma mark 动态
-(void)setTransferDymicInfo:(NSInteger)transferDymicInfo{
    _transferDymicInfo = transferDymicInfo;
    self.dymicLabel.text = [NSString stringWithFormat:@"%li",transferDymicInfo] ;
    CGSize dymicSize = [[NSString stringWithFormat:@"%li",transferDymicInfo] sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLabel.font])];
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame), self.fixedLabel.orgin_y, dymicSize.width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    
//    [self animationWithInfoWithLabel:self.dymicLabel];
}

#pragma mark 固定
-(void)setTransferFixedInfo:(NSString *)transferFixedInfo{
    _transferFixedInfo = transferFixedInfo;
    self.fixedLabel.text = transferFixedInfo;
    CGSize contentOfSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(0, 0, contentOfSize.width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
}

-(void)animationWithInfoWithLabel:(UILabel *)label{
    POPBasicAnimation *anim = [POPBasicAnimation animation];
    anim.duration = 1;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    POPAnimatableProperty * prop = [POPAnimatableProperty propertyWithName:@"count" initializer:^(POPMutableAnimatableProperty *prop){
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [[obj description] floatValue];
        };
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
            [obj setText:[NSString stringWithFormat:@"%li",(long)values[0]]];
        };
        
        // dynamics threshold
        prop.threshold = 1;
    }];
    
    anim.property = prop;
    
    anim.fromValue = @(0);
    anim.toValue = @([label.text integerValue]);
    
    [label pop_addAnimation:anim forKey:@"counting"];
}




@end
