//
//  PDRoleDetailAvatarCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDRoleDetailRootModel.h"
@interface PDRoleDetailAvatarCell : UITableViewCell


@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDRoleDetailRootModel *transferRoleDetailRootModel;

+(CGFloat)calculationCellheight;
@end
