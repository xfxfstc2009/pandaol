//
//  PDAccountAvatarView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 账号头像
#import <UIKit/UIKit.h>

@interface PDAccountAvatarView : UIView

@property (nonatomic,strong)PDImageView *avatarImgView;                                /**< 头像1*/
@property (nonatomic,strong)UIImage *avatarImage;                       /**< 头像*/
@property (nonatomic,assign)CGRect avatarFrame;                         /**< frame*/
@property (nonatomic,copy)NSString *avatarUrl;                          /**< 头像地址*/

-(void)startAnimationDidEnd:(void(^)())animation;

@end
