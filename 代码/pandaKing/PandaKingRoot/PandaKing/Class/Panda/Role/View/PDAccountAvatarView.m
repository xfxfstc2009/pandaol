//
//  PDAccountAvatarView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAccountAvatarView.h"

@interface PDAccountAvatarView()
@property (nonatomic,strong)PDImageView *lineView1;                             /**< 边上的view1*/
@property (nonatomic,strong)PDImageView *lineView2;                             /**< 边上的line2*/
@property (nonatomic,assign)NSInteger animationDuration;                        /**< 动画执行时间*/
@end

@implementation PDAccountAvatarView


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(instancetype)init{
    self = [super init];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建边上的view
    self.lineView1 = [[PDImageView alloc]init];
    self.lineView1.backgroundColor = [UIColor clearColor];
    self.lineView1.image = [UIImage imageNamed:@"img_in"];
    [self addSubview:self.lineView1];
    
    // 2. 创建view2
    self.lineView2 = [[PDImageView alloc]init];
    self.lineView2.backgroundColor = [UIColor clearColor];
    self.lineView2.image = [UIImage imageNamed:@"img_out"];
    [self addSubview:self.lineView2];
    
    // 3. 创建头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.clipsToBounds = YES;
    [self addSubview:self.avatarImgView];
}

-(void)setAvatarFrame:(CGRect)avatarFrame{
    _avatarFrame = avatarFrame;
    self.frame = avatarFrame;
    
    self.lineView1.frame = CGRectMake(0, 0, avatarFrame.size.width, avatarFrame.size.height);
    self.lineView2.frame = CGRectMake(0, 0, avatarFrame.size.width, avatarFrame.size.height);
    
    self.avatarImgView.frame = CGRectMake(0, 0, self.lineView1.size_width - 2 * LCFloat(11), self.lineView1.size_height - 2 * LCFloat(11));
    self.avatarImgView.center = CGPointMake(self.lineView1.center_x, self.lineView1.center_y);
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_height / 2.;
}


-(void)setAvatarImage:(UIImage *)avatarImage{
    _avatarImage = avatarImage;
    self.avatarImgView.image = avatarImage;
}

-(void)setAvatarUrl:(NSString *)avatarUrl{
    _avatarUrl = avatarUrl;
    [self.avatarImgView uploadImageWithURL:avatarUrl placeholder:nil callback:NULL];
}

-(void)startAnimationDidEnd:(void (^)())animation{
    self.animationDuration = 3;
    
    //停止动画，速度设置为0
    self.lineView1.layer.speed = self.animationDuration;
    [self.lineView1.layer addAnimation:[self basicAnimationWithSpeed:12] forKey:@"basicAnimation"];
    
    self.lineView2.layer.speed = self.animationDuration;
    [self.lineView2.layer addAnimation:[self basicAnimationWithSpeed:6] forKey:@"basicAnimation"];
}


#pragma mark - Animation
- (CABasicAnimation *)basicAnimationWithSpeed:(NSInteger)speed {
    CABasicAnimation *basicAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    //旋转一圈时长
    basicAnimation.duration = speed ;                    // 转动一圈的时长
    basicAnimation.delegate = self;
    //开始动画的起始位置
    basicAnimation.fromValue = [NSNumber numberWithInt:0];
    basicAnimation.byValue = [NSNumber numberWithInt:2];
    //M_PI是180度
    basicAnimation.toValue = [NSNumber numberWithInt:M_PI * 2];
    //动画重复次数
    CGFloat count = (self.animationDuration / (speed * 1.0));
    [basicAnimation setRepeatCount:count];
    //播放完毕之后是否逆向回到原来位置
    [basicAnimation setAutoreverses:YES];
    //是否叠加（追加）动画效果
    [basicAnimation setCumulative:YES];
    
    return basicAnimation;
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag{
    NSLog(@"结束");
}

@end
