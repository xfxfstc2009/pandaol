//
//  PDRoleChallengeDetailTableViewHeaderEquipmentsView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDRoleChallengeDetailTableViewHeaderEquipmentsView : UIView

@property (nonatomic,strong)NSArray *transferEquipmentsArr;

@end
