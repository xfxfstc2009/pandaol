//
//  PDRoleTableView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRoleTableView.h"
#import <pop/POP.h>
@interface PDRoleTableView()
@property (nonatomic,strong)UIView *tableBgView;                        /**< 背景view*/

@end

@implementation PDRoleTableView

-(instancetype)initWithFrame:(CGRect)frame tableArr:(NSArray *)tableArr{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.tableBgView = [[UIView alloc]init];
    self.tableBgView.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
    self.tableBgView.frame = CGRectMake(LCFloat(15), 0, self.size_width - 2 * LCFloat(15), LCFloat(50));
    [self addSubview:self.tableBgView];
    
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.tableBgView.size_height = transferCellHeight;
}

-(void)changeCellHeight:(CGFloat)transferCellHeight{
    self.tableBgView.size_height = transferCellHeight;
}


-(void)setTransferArr:(NSArray *)transferArr{
    _transferArr = transferArr;
    if (self.tableBgView){
        [self.tableBgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    for (int i = 0 ; i < transferArr.count;i++){
        UILabel *fixedLabel = [[UILabel alloc]init];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.font = [UIFont systemFontOfCustomeSize:11];
        fixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        fixedLabel.text = [[transferArr objectAtIndex:i] firstObject];
        fixedLabel.textAlignment = NSTextAlignmentCenter;
        [self.tableBgView addSubview:fixedLabel];
        
        // 2. 创建动态label
        UILabel *dymicLabel = [[UILabel alloc]init];
        dymicLabel.backgroundColor = [UIColor clearColor];
        dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
        dymicLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
        dymicLabel.textAlignment = NSTextAlignmentCenter;
        dymicLabel.text = [[transferArr objectAtIndex:i] lastObject];
        [self.tableBgView addSubview:dymicLabel];
        [self animationWithInfoWithLabel:dymicLabel];
        
        
        // 3. 创建view
        UIView *lineView = [[UIView alloc]init];
        lineView.backgroundColor = [UIColor whiteColor];
        [self.tableBgView addSubview:lineView];
        
        
        // 3. 计算宽度
        CGFloat width_margin = (self.tableBgView.size_width - (transferArr.count - 1) * 1)/ transferArr.count;
        CGFloat height_margin = (self.tableBgView.size_height - 1) / 2.;
        
        CGFloat origin_x = i * (width_margin + 1);
        fixedLabel.frame = CGRectMake(origin_x, 0, width_margin, height_margin);
        
        dymicLabel.frame = CGRectMake(origin_x, height_margin + 1, width_margin, height_margin);
        
        lineView.frame = CGRectMake(origin_x - 1, height_margin, 1, height_margin);
    }
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(0, self.tableBgView.size_height / 2., self.tableBgView.size_width, 1);
    lineView.backgroundColor = [UIColor whiteColor];
    [self.tableBgView addSubview:lineView];
}


-(void)animationWithInfoWithLabel:(UILabel *)label{
    POPBasicAnimation *anim = [POPBasicAnimation animation];
    anim.duration = 1;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    POPAnimatableProperty * prop = [POPAnimatableProperty propertyWithName:@"count" initializer:^(POPMutableAnimatableProperty *prop){
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [[obj description] floatValue];
        };
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
            [obj setText:[NSString stringWithFormat:@"%li",(long)values[0]]];
        };
        
        // dynamics threshold
        prop.threshold = 1;
    }];
    
    anim.property = prop;
    
    anim.fromValue = @(0);
    anim.toValue = @([label.text integerValue]);
    
    [label pop_addAnimation:anim forKey:@"counting"];
}



#pragma mark - 开启动画


- (void)startAnimationWithArr:(NSArray *)array{
    
}


@end
