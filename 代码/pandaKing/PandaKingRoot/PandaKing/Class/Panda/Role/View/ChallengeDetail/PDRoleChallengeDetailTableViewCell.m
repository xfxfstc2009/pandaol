//
//  PDRoleChallengeDetailTableViewCell.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDRoleChallengeDetailTableViewCell.h"
#import "PDRoleChallengerSingleLabelView.h"

@interface PDRoleChallengeDetailTableViewCell()
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *moneyLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *bubingLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *tuitaLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *bingyingLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *zuidalianshLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *zuidaduoshaLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *zongzhiliaoLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *shuchushanghaiLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *chengshoushanghaiLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *zongshanghaiLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *zongwulishanghaiLabel;
@property (nonatomic,strong)PDRoleChallengerSingleLabelView *zongmofashanghaiLabel;
@property (nonatomic,strong)UILabel *kdaLabel;

@end

@implementation PDRoleChallengeDetailTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    CGFloat width = (kScreenBounds.size.width - 2 * LCFloat(11)) / 3.;
    CGFloat height = [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:11.]];
    // 1.金钱
    self.moneyLabel = [self createSingleLabelViewWithFixedStr:@"金钱"];
    self.moneyLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), width, height);
    
    // 2.补兵
    self.bubingLabel = [self createSingleLabelViewWithFixedStr:@"补兵"];
    self.bubingLabel.frame = CGRectMake(CGRectGetMaxX(self.moneyLabel.frame), self.moneyLabel.orgin_y, self.moneyLabel.size_width, self.moneyLabel.size_height);
    
    // 3.推塔
    self.tuitaLabel = [self createSingleLabelViewWithFixedStr:@"推塔"];
    self.tuitaLabel.frame = CGRectMake(CGRectGetMaxX(self.bubingLabel.frame), self.moneyLabel.orgin_y, self.moneyLabel.size_width, self.moneyLabel.size_height);
    
    // 4.兵营
    self.bingyingLabel = [self createSingleLabelViewWithFixedStr:@"兵营"];
    self.bingyingLabel.frame = CGRectMake(self.moneyLabel.orgin_x, CGRectGetMaxY(self.moneyLabel.frame) + LCFloat(7), width,height);

    // 5.最大连杀
    self.zuidalianshLabel = [self createSingleLabelViewWithFixedStr:@"最大连杀"];
    self.zuidalianshLabel.frame = CGRectMake(CGRectGetMaxX(self.bingyingLabel.frame) , self.bingyingLabel.orgin_y, width, height);
    
    // 6.最大多杀
    self.zuidaduoshaLabel = [self createSingleLabelViewWithFixedStr:@"最大多杀"];
    self.zuidaduoshaLabel.frame = CGRectMake(CGRectGetMaxX(self.zuidalianshLabel.frame), self.bingyingLabel.orgin_y, self.moneyLabel.size_width, self.moneyLabel.size_height);
    
    // 7.总治疗
    self.zongzhiliaoLabel = [self createSingleLabelViewWithFixedStr:@"总治疗"];
    self.zongzhiliaoLabel.frame = CGRectMake(self.moneyLabel.orgin_x, CGRectGetMaxY(self.bingyingLabel.frame) + LCFloat(7), self.moneyLabel.size_width, height);
    
    // 8. 输出伤害
    self.shuchushanghaiLabel = [self createSingleLabelViewWithFixedStr:@"输出伤害"];
    self.shuchushanghaiLabel.frame = CGRectMake(CGRectGetMaxX(self.zongzhiliaoLabel.frame), self.zongzhiliaoLabel.orgin_y, width, height);
    
    // 9.承受伤害
    self.chengshoushanghaiLabel = [self createSingleLabelViewWithFixedStr:@"承受伤害"];
    self.chengshoushanghaiLabel.frame = CGRectMake(CGRectGetMaxX(self.shuchushanghaiLabel.frame), self.shuchushanghaiLabel.orgin_y, width, height);

    // 10.总伤害
    self.zongshanghaiLabel = [self createSingleLabelViewWithFixedStr:@"给对方英雄造成总伤害"];
    self.zongshanghaiLabel.frame = CGRectMake(self.moneyLabel.orgin_x, CGRectGetMaxY(self.zongzhiliaoLabel.frame) + LCFloat(7), kScreenBounds.size.width - 2 * LCFloat(11), height);
    // 11. 总物理伤害
    self.zongwulishanghaiLabel = [self createSingleLabelViewWithFixedStr:@"给对方英雄造成物理总伤害"];
    self.zongwulishanghaiLabel.frame = CGRectMake(self.moneyLabel.orgin_x, CGRectGetMaxY(self.zongshanghaiLabel.frame) + LCFloat(7), self.zongshanghaiLabel.size_width, height);
    
    // 12.总魔法伤害
    self.zongmofashanghaiLabel = [self createSingleLabelViewWithFixedStr:@"给对方英雄造成魔法总伤害"];
    self.zongmofashanghaiLabel.frame = CGRectMake(self.moneyLabel.orgin_x, CGRectGetMaxY(self.zongwulishanghaiLabel.frame) + LCFloat(7), self.zongshanghaiLabel.size_width, height);
    
    // 13.创建kda
    self.kdaLabel = [[UILabel alloc]init];
    self.kdaLabel.backgroundColor = [UIColor clearColor];
    self.kdaLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.kdaLabel.textAlignment = NSTextAlignmentRight;
    self.kdaLabel.textColor = c26;
    [self addSubview:self.kdaLabel];
}

#pragma mark - 创建singleView
-(PDRoleChallengerSingleLabelView *)createSingleLabelViewWithFixedStr:(NSString *)fixedStr{
    PDRoleChallengerSingleLabelView *challengerSingleLabel = [[PDRoleChallengerSingleLabelView alloc]init];
    challengerSingleLabel.transferFixedInfo = fixedStr;
    [self addSubview:challengerSingleLabel];
    return challengerSingleLabel;
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferGamersSingleModel:(PDGameRecordDetailGamersSingleModel *)transferGamersSingleModel{
    _transferGamersSingleModel = transferGamersSingleModel;
    
    // 1. 金钱
    self.moneyLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.goldEarned;
    
    // 2.补兵
    self.bubingLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.minionsKilled;
    
    // 3.推塔
    self.tuitaLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.turretsKilled;
    
    // 4.兵营
    self.bingyingLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.barracksKilled;

    // 5.最大连杀
    self.zuidalianshLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.largestKillingSpree;

    // 6.最大多杀
    self.zuidaduoshaLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.largestMultiKill;
    
    // 7.总治疗
    self.zongzhiliaoLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.totalHealth;

    // 8.输出伤害
    self.shuchushanghaiLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.totalDamageDealt;
    
    // 9.承受伤害
    self.chengshoushanghaiLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.totalDamageTaken;
    
    // 10.总伤害
    self.zongshanghaiLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.totalDamageDealtToChampions;

    // 11.总物理伤害
    self.zongwulishanghaiLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.physicalDamageDealtToChampions;
    
    // 12.总魔法伤害
    self.zongmofashanghaiLabel.transferDymicInfo = transferGamersSingleModel.gamerRecord.magicDamageDealtToChampions;
    
    // 13. kda
    self.kdaLabel.text = [NSString stringWithFormat:@"KDA·%@",transferGamersSingleModel.kda];
    CGSize kdaSize = [self.kdaLabel.text sizeWithCalcFont:self.kdaLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.kdaLabel.font])];
    self.kdaLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - kdaSize.width, 0, kdaSize.width, [NSString contentofHeightWithFont:self.kdaLabel.font]);
    self.kdaLabel.center_y = self.zongmofashanghaiLabel.center_y;
}



+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += 2 *LCFloat(11);
    cellHeight += 5 * LCFloat(7);
    cellHeight += 6 * [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:11.]];
    return cellHeight;
}
@end
