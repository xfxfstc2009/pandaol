//
//  PDInternetCafesDetailHeaderCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 网吧详情cell 
#import <UIKit/UIKit.h>
#import "PDInternetCafesSingleModel.h"


@interface PDInternetCafesDetailHeaderCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDInternetCafesSingleModel *transferInternetCafesSingleModel;

+(CGFloat)calculationCellHeightWithSingleModel:(PDInternetCafesSingleModel *)model;

@end
