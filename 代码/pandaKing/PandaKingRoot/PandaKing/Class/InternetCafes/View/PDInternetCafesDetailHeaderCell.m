//
//  PDInternetCafesDetailHeaderCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesDetailHeaderCell.h"

@interface PDInternetCafesDetailHeaderCell()
@property (nonatomic,strong)PDImageView *avatarImageView;           /**< 图片*/
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *phoneNameLabel;
@property (nonatomic,strong)UILabel *priceLabel;
@property (nonatomic,strong)UILabel *locationLabel;
@property (nonatomic,strong)PDImageView *phoneImgView;
@property (nonatomic,strong)PDImageView *locationImgView;

@end

@implementation PDInternetCafesDetailHeaderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    // 1. 创建图片地址
    self.avatarImageView = [[PDImageView alloc]init];
    self.avatarImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImageView];
    
    // 2. 创建名称
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.nameLabel];
    
    // 3. 创建电话
    self.phoneNameLabel = [[UILabel alloc]init];
    self.phoneNameLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.phoneNameLabel];
    
    // 4. 创建网费
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.priceLabel];
    
    // 5, 创建location
    self.locationLabel = [[UILabel alloc]init];
    self.locationLabel.backgroundColor = [UIColor clearColor];
    self.locationLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.locationLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferInternetCafesSingleModel:(PDInternetCafesSingleModel *)transferInternetCafesSingleModel{
    _transferInternetCafesSingleModel = transferInternetCafesSingleModel;
    
//    // 1. 图片
//    self.avatarImageView.frame = CGRectMake(LCFloat(11), LCFloat(5), LCFloat(80), LCFloat(80));
//    [self.avatarImageView uploadImageWithURL:transferInternetCafesSingleModel.avatar placeholder:nil callback:NULL];
//    
//    // 2, 创建名字
//    self.nameLabel.text = transferInternetCafesSingleModel.name;
//    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImageView.frame) + LCFloat(11), self.avatarImageView.orgin_y, kScreenBounds.size.width - LCFloat(11 - CGRectGetMaxX(self.avatarImageView.frame) - LCFloat(11)), [NSString contentofHeightWithFont:self.nameLabel.font]);
//    
//    // 3. 创建电话
//    self.phoneNameLabel.text = transferInternetCafesSingleModel.phone;
//    self.phoneNameLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), self.nameLabel.size_width, [NSString contentofHeightWithFont:self.phoneNameLabel.font]);
//    
//    // 4. 创建网费
//    self.priceLabel.text = transferInternetCafesSingleModel.priceRange;
//    self.priceLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.phoneNameLabel.frame) + LCFloat(11), self.nameLabel.size_width, [NSString contentofHeightWithFont:self.priceLabel.font]);
//    
//    // 5. 创建地址
//    self.locationLabel.text = transferInternetCafesSingleModel.address;
//    CGSize contentOfSize = [self.locationLabel.text sizeWithCalcFont:self.locationLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
//    self.locationLabel.frame = CGRectMake(self.avatarImageView.orgin_x, CGRectGetMaxY(self.avatarImageView.frame) + LCFloat(5), kScreenBounds.size.width - 2 * LCFloat(11), contentOfSize.height);
}

+(CGFloat)calculationCellHeightWithSingleModel:(PDInternetCafesSingleModel *)model{
    // 1. 图片
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(5);
    cellHeight += LCFloat(80);
    cellHeight += LCFloat(5);
    
//    CGSize contentOfSize = [model.address sizeWithCalcFont: [UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(80) - LCFloat(11) - LCFloat(11), CGFLOAT_MAX)];
//    
//    cellHeight += contentOfSize.height;
    
    cellHeight += LCFloat(5);
    
    
    return cellHeight;
}

@end
