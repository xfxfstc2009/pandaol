//
//  PDInternetCafesSingleCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDInternetCafesListModel.h"
@interface PDInternetCafesSingleCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellheight;
@property (nonatomic,strong)PDInternetCafesSingleModel *transferInternetCafesSingleModel;           /**< 单个网吧*/

#pragma mark - 返回高度
+(CGFloat)calculationCellHeight;
@end
