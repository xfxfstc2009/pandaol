//
//  PDInternetCafesDetailHeaderView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/5.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
//  增加头部信息
#import <UIKit/UIKit.h>
#import "PDInternetCafesSingleModel.h"
@interface PDInternetCafesDetailHeaderView : UIView
@property (nonatomic,strong)PDImageView *bgImageView;               /**< 背景图片*/

@property (nonatomic,strong)PDInternetCafesSingleModel *transferInternetCafes;

@end
