//
//  PDInternetCafesDetailCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesDetailCell.h"

@interface PDInternetCafesDetailCell()
@property (nonatomic,strong)PDImageView *bannerImageView;               /**< 背景的图片*/
@property (nonatomic,strong)UIView *alphaImageView;                     /**< 高斯模糊view*/
@property (nonatomic,strong)PDImageView *avatarImageView;           /**< 图片*/
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *phoneNameLabel;
@property (nonatomic,strong)UILabel *priceLabel;
@property (nonatomic,strong)UILabel *locationLabel;
@property (nonatomic,strong)PDImageView *phoneImgView;
@property (nonatomic,strong)UIButton *phoneButton;
@property (nonatomic,strong)PDImageView *locationImgView;

@end

@implementation PDInternetCafesDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - 
-(void)createView {
    // 1. 创建图片地址
    self.avatarImageView = [[PDImageView alloc]init];
    self.avatarImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImageView];
    
    // 2. 创建名称
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.nameLabel];
    
    // 3. 创建电话
    self.phoneNameLabel = [[UILabel alloc]init];
    self.phoneNameLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.phoneNameLabel];
    
    // 4. 创建网费
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.priceLabel];
    
    // 5, 创建location
    self.locationLabel = [[UILabel alloc]init];
    self.locationLabel.backgroundColor = [UIColor clearColor];
    self.locationLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.locationLabel];
}

+(CGFloat)calculationCellHeightWithSingleModel:(PDInternetCafesSingleModel *)model{
    return 100;
}

@end
