//
//  PDInternetCafesDetailLocationView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDInternetCafesDetailLocationView : UIView

@property (nonatomic,copy)NSString *transferLocation;

-(void)locationClickBlock:(void(^)())block;


@end
