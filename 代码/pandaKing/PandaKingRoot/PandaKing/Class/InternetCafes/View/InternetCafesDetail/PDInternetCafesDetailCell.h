//
//  PDInternetCafesDetailCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDInternetCafesSingleModel.h"


@interface PDInternetCafesDetailCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDInternetCafesSingleModel *transferInternetCafesSingleModel;

+(CGFloat)calculationCellHeightWithSingleModel:(PDInternetCafesSingleModel *)model;

@end
