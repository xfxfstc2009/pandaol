//
//  PDInternetCafesDetailHeaderView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/5.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesDetailHeaderView.h"

@interface PDInternetCafesDetailHeaderView()
@property (nonatomic,strong)PDImageView *alphaView;                      /**< 上面的view*/
@property (nonatomic,strong)PDImageView *avatarImageView;           /**< 头像*/
@property (nonatomic,strong)UILabel *nameLabel;                     /**< 名字*/
@property (nonatomic,strong)UILabel *phoneLabel;                    /**< 电话*/
@property (nonatomic,strong)PDImageView *phoneImgView;              /**< 号码的icon*/
@property (nonatomic,strong)UIButton *phoneBtn;                     /**< 电话按钮*/
@property (nonatomic,strong)UILabel *priceLabel;                    /**< 价格*/
@end

@implementation PDInternetCafesDetailHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建背景图片
    self.bgImageView = [[PDImageView alloc]init];
    self.bgImageView.backgroundColor = [UIColor clearColor];
    self.bgImageView.frame = self.bounds;
    [self addSubview:self.bgImageView];
    
    // 2. 创建名字
    self.alphaView = [[PDImageView alloc]init];
    self.alphaView.image = [UIImage imageWithRenderColor:[UIColor colorWithCustomerName:@"浅灰"] renderSize:self.frame.size];
    self.alphaView.alpha = .6f;
    self.alphaView.frame = self.bgImageView.bounds;
    [self addSubview:self.alphaView];
    
    // 3. 创建头像
    self.avatarImageView = [[PDImageView alloc]init];
    self.avatarImageView.backgroundColor = [UIColor clearColor];
    self.avatarImageView.frame = CGRectMake(LCFloat(11), (self.size_height - LCFloat(80)) / 2., LCFloat(80), LCFloat(80));
    self.avatarImageView.clipsToBounds = YES;
    [self addSubview:self.avatarImageView];
    
    // 4. 创建名字
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.nameLabel];
    
    // 5. 创建电话icon
    self.phoneImgView = [[PDImageView alloc]init];
    self.phoneImgView.backgroundColor = [UIColor clearColor];
    self.phoneImgView.image = [UIImage imageNamed:@"icon_netbar_phone"];
    [self addSubview:self.phoneImgView];
    
    // 5. 创建电话
    self.phoneLabel= [[UILabel alloc]init];
    self.phoneLabel.backgroundColor = [UIColor clearColor];
    self.phoneLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.phoneLabel.textColor = [UIColor whiteColor];
    self.phoneLabel.frame = CGRectMake(100, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), self.nameLabel.size_width, [NSString contentofHeightWithFont:self.phoneLabel.font]);
    [self addSubview:self.phoneLabel];
    
    // 6. 创建价格
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.priceLabel.textColor = UURed;
    self.priceLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.phoneLabel.frame) + LCFloat(11), self.nameLabel.size_width, [NSString contentofHeightWithFont:self.priceLabel.font]);
    [self addSubview:self.priceLabel];
    
    // 7.0 创建电话
    self.phoneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.phoneBtn.backgroundColor = [UIColor clearColor];
    [self addSubview:self.phoneBtn];
    __weak typeof(self)weakSelf = self;
    [self.phoneBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.transferInternetCafes.bar.phone.length){
            NSString *content = [NSString stringWithFormat:@"是否拨打给%@?",strongSelf.transferInternetCafes.bar.phone];
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"拨打电话" conten:content isClose:YES btnArr:@[@"确定",@"取消"] buttonClick:^(NSInteger buttonIndex) {
                if (buttonIndex == 0){
                    [Tool callCustomerServerPhoneNumber:strongSelf.transferInternetCafes.bar.phone];
                }
                [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
            }];
        }
    }];
    
    // 7. 重新更新frame
    self.avatarImageView.frame = CGRectMake(LCFloat(20), LCFloat(15), (self.size_height - 2 * LCFloat(44) - 2 * LCFloat(15)),  (self.size_height - 2 * LCFloat(44) - 2 * LCFloat(15)));
    self.avatarImageView.layer.cornerRadius = self.avatarImageView.size_height / 2.;
    
    // 7.1 网吧名字
    CGFloat margin = (self.avatarImageView.size_height - [NSString contentofHeightWithFont:self.phoneLabel.font] - [NSString contentofHeightWithFont:self.priceLabel.font] - [NSString contentofHeightWithFont:self.nameLabel.font]) / 2.;
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImageView.frame) + LCFloat(11), self.avatarImageView.orgin_y, kScreenBounds.size.width - CGRectGetMaxX(self.avatarImageView.frame) - LCFloat(11), [NSString contentofHeightWithFont:self.nameLabel.font]);
    // 电话
    self.phoneLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + margin, self.nameLabel.size_width, [NSString contentofHeightWithFont:self.phoneLabel.font]);
    
    self.phoneImgView.frame = CGRectMake(self.nameLabel.orgin_x, 0, LCFloat(10), LCFloat(10));
    self.phoneImgView.center_y = self.phoneLabel.center_y;
    self.phoneLabel.orgin_x = CGRectGetMaxX(self.phoneImgView.frame) + LCFloat(3);
    
    self.priceLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.phoneLabel.frame) + margin, self.nameLabel.size_width, [NSString contentofHeightWithFont:self.priceLabel.font]);
}


-(void)setTransferInternetCafes:(PDInternetCafesSingleModel *)transferInternetCafes{
    _transferInternetCafes = transferInternetCafes;
    
    // 1.背景图片
    __weak typeof(self)weakSelf = self;
    [self.bgImageView uploadImageWithURL:transferInternetCafes.bar.avatar placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.bgImageView.image = [image boxblurImageWithBlur:.4f];
    }];
    
    // 2.创建头像
    [self.avatarImageView uploadImageWithURL:transferInternetCafes.bar.avatar placeholder:nil callback:NULL];
    
    // 3. 创建名字
    self.nameLabel.text = transferInternetCafes.bar.name;
    
    // 4. 创建号码
    self.phoneLabel.text = transferInternetCafes.bar.phone;
    
    // 5. 创建价格
    self.priceLabel.text = transferInternetCafes.bar.priceRange;
}
@end
