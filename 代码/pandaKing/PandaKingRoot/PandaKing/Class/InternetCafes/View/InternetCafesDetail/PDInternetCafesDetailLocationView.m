//
//  PDInternetCafesDetailLocationView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesDetailLocationView.h"
#import <objc/runtime.h>

static char locationKey;
@interface PDInternetCafesDetailLocationView()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDImageView *locationIcon;
@property (nonatomic,strong)UILabel *locationLabel;                             /**< 地址label*/
@property (nonatomic,strong)PDImageView *arrowImageView;                        /**< 箭头*/
@property (nonatomic,strong)UIButton *actionButton;                             /**< 按钮*/
@end

@implementation PDInternetCafesDetailLocationView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.bgView.alpha = .3f;
    self.bgView.frame = self.bounds;
    [self addSubview:self.bgView];
    
    // 1. 创建标记
    self.locationIcon = [[PDImageView alloc]init];
    self.locationIcon.backgroundColor = [UIColor clearColor];
    self.locationIcon.image = [UIImage imageNamed:@"icon_netbar_location"];
    self.locationIcon.frame = CGRectMake(LCFloat(11), 10, LCFloat(12), LCFloat(12));
    [self addSubview:self.locationIcon];
    
    // 2. 创建label
    self.locationLabel = [[UILabel alloc]init];
    self.locationLabel.backgroundColor = [UIColor clearColor];
    self.locationLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.locationLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.locationLabel.numberOfLines = 0;
    self.locationLabel.text = @"";
    [self addSubview:self.locationLabel];
    
    // 3. 创建箭头符号
    self.arrowImageView = [[PDImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    self.arrowImageView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - 9, 0, 9, 15);
    [self addSubview:self.arrowImageView];
    
    // 4. 创建按钮
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &locationKey);
        if (block){
            block();
        }
        
    }];
    [self addSubview:self.actionButton];
}

-(void)setTransferLocation:(NSString *)transferLocation{
    _transferLocation = transferLocation;
    CGFloat width = kScreenBounds.size.width - LCFloat(11) - self.locationIcon.size_width - LCFloat(8) - LCFloat(11) - self.arrowImageView.size_width - LCFloat(11);
    self.locationLabel.text = transferLocation;
    CGSize locationSize = [transferLocation sizeWithCalcFont:self.locationLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    
    CGFloat sizeHeiht = LCFloat(14) + locationSize.height + LCFloat(14);
    // locationIcon
    self.locationIcon.orgin_y = (sizeHeiht - self.locationIcon.size_height) / 2.;
    
    // label
    self.locationLabel.frame = CGRectMake(CGRectGetMaxX(self.locationIcon.frame) + LCFloat(8), LCFloat(14),width, locationSize.height);
    
    // arrow
    self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - self.arrowImageView.size_width, (sizeHeiht - self.arrowImageView.size_height) / 2., self.arrowImageView.size_width, self.arrowImageView.size_height);
    
    self.size_height = sizeHeiht;
    
    // button
    self.actionButton.frame = CGRectMake(0, 0, kScreenBounds.size.width, sizeHeiht);
    
    // bg
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, sizeHeiht);
    
}


-(void)locationClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &locationKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
