//
//  PDInternetCafesActivityCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDInternetCafesActivitySingleModel.h"
@interface PDInternetCafesActivityCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDInternetCafesActivitySingleModel *transferActivityModel;


+(CGFloat)calculationCellHeight:(PDInternetCafesActivitySingleModel *)transferActivityModel;
@end
