//
//  PDInternetCafesActivityCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesActivityCell.h"

@interface PDInternetCafesActivityCell()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UIView *flagView;
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;

@end

@implementation PDInternetCafesActivityCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    
    // 2. 创建flag
    self.flagView = [[UIView alloc]init];
    self.flagView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.flagView];
    
    // 3. 创建label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.fixedLabel.numberOfLines = 0;
    [self addSubview:self.fixedLabel];
    
    // 4. 创建活动
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.dymicLabel.numberOfLines = 0;
    [self addSubview:self.dymicLabel];
    
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferActivityModel:(PDInternetCafesActivitySingleModel *)transferActivityModel{
    _transferActivityModel = transferActivityModel;
    
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.transferCellHeight);
    
    // 2.
    self.flagView.frame = CGRectMake(0, LCFloat(11), LCFloat(11), self.transferCellHeight - 2 * LCFloat(11));
    
    // 3.
    self.fixedLabel.attributedText = [Tool rangeLabelWithContent:[NSString stringWithFormat:@"活动内容:%@",transferActivityModel.content] hltContentArr:@[@"活动内容"] hltColor:[UIColor colorWithCustomerName:@"黑"] normolColor:[UIColor colorWithCustomerName:@"浅灰"]];
    CGSize contentWithFixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(self.flagView.frame) -  2 * LCFloat(11), CGFLOAT_MAX)];
    self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.flagView.frame) + LCFloat(11), LCFloat(11), kScreenBounds.size.width - CGRectGetMaxX(self.flagView.frame) - 2 * LCFloat(11), contentWithFixedSize.height);
    
    // 4.
    NSString *startTime = [NSDate getTimeWithString:transferActivityModel.startTime / 1000.];
    NSString *endTime = [NSDate getTimeWithString:transferActivityModel.endTime / 1000.];
    
    self.dymicLabel.attributedText = [Tool rangeLabelWithContent:[NSString stringWithFormat:@"活动时间:%@ - %@",startTime,endTime] hltContentArr:@[@"活动时间"] hltColor:[UIColor colorWithCustomerName:@"黑"] normolColor:[UIColor colorWithCustomerName:@"浅灰"]];
    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(self.fixedLabel.size_width, CGFLOAT_MAX)];
    self.dymicLabel.frame = CGRectMake(self.fixedLabel.orgin_x, CGRectGetMaxY(self.fixedLabel.frame) + LCFloat(5), self.fixedLabel.size_width, dymicSize.height);
}

+(CGFloat)calculationCellHeight:(PDInternetCafesActivitySingleModel *)transferActivityModel{
    NSString *fixedInfo = [NSString stringWithFormat:@"活动内容:%@",transferActivityModel.content];
     CGSize contentWithFixedSize = [fixedInfo sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) - 2 * LCFloat(11), CGFLOAT_MAX)];
    
    NSString *startTime = [NSDate getTimeWithString:transferActivityModel.startTime];
    NSString *endTime = [NSDate getTimeWithString:transferActivityModel.endTime];
    NSString *dymicInfo = [NSString stringWithFormat:@"活动时间:%@ - %@",startTime,endTime];
    CGSize dymicSize = [dymicInfo sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - LCFloat(11) - 2 * LCFloat(11), CGFLOAT_MAX)];
    
    CGFloat cellheight = 0;
    cellheight += LCFloat(11);
    cellheight += contentWithFixedSize.height;
    cellheight += LCFloat(5);
    cellheight += dymicSize.height;
    cellheight += LCFloat(11);
    return cellheight;

}

@end
