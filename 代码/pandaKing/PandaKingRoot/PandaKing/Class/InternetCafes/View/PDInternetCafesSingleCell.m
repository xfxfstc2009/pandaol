//
//  PDInternetCafesSingleCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesSingleCell.h"

@interface PDInternetCafesSingleCell()
@property (nonatomic,strong)PDImageView *avatarImgView;             /**< 网吧图片*/
@property (nonatomic,strong)UILabel *nameLabel;                     /**< 网吧名字*/
@property (nonatomic,strong)PDImageView *locationImageView;         /**< 地址图片*/
@property (nonatomic,strong)UILabel *locationLabel;                 /**< 网吧地址*/
@property (nonatomic,strong)UILabel *priceLabel;                    /**< 网吧价格*/
@property (nonatomic,strong)UILabel *distanceLabel;                 /**< 网吧距离*/
@property (nonatomic,strong)PDImageView *activityImgView;           /**< 活动图片*/
@end

@implementation PDInternetCafesSingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建网吧图片
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    // 2. 创建网吧名字label
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"] boldFont];
    self.nameLabel.textColor = c3;
    [self addSubview:self.nameLabel];
    
    self.locationImageView = [[PDImageView alloc]init];
    self.locationImageView.backgroundColor = [UIColor clearColor];
    self.locationImageView.image = [UIImage imageNamed:@"icon_nearby_location"];
    [self addSubview:self.locationImageView];
    
    // 3. 创建地址
    self.locationLabel = [[UILabel alloc]init];
    self.locationLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.locationLabel.textColor = c5;
    self.locationLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.locationLabel];
    
    // 4. 创建网费
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.priceLabel.textColor = c9;
    [self addSubview:self.priceLabel];

    // 5. 创建距离
    self.distanceLabel = [[UILabel alloc]init];
    self.distanceLabel.backgroundColor = [UIColor clearColor];
    self.distanceLabel.textColor = c15;
    self.distanceLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self addSubview:self.distanceLabel];
    
    self.activityImgView = [[PDImageView alloc]init];
    self.activityImgView.backgroundColor = [UIColor clearColor];
    self.activityImgView.image = [UIImage imageNamed:@"icon_netbar_activity"];
    self.activityImgView.frame= CGRectMake(kScreenBounds.size.width - 54, 0, 54, 40);
    [self addSubview:self.activityImgView];
}

-(void)setTransferCellheight:(CGFloat)transferCellheight{
    _transferCellheight = transferCellheight;
}

-(void)setTransferInternetCafesSingleModel:(PDInternetCafesSingleModel *)transferInternetCafesSingleModel{
    _transferInternetCafesSingleModel = transferInternetCafesSingleModel;
    
    self.avatarImgView.frame = CGRectMake(LCFloat(14), LCFloat(10), (self.transferCellheight - 2 * LCFloat(10)), (self.transferCellheight - 2 * LCFloat(10)));
    self.avatarImgView.clipsToBounds = YES;
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_height / 2.;
    [self.avatarImgView uploadImageWithURL:transferInternetCafesSingleModel.bar.avatar placeholder:nil callback:NULL];
    
    // 2. 创建名称
    self.nameLabel.text = transferInternetCafesSingleModel.bar.name;
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(24), self.avatarImgView.orgin_y, kScreenBounds.size.width - LCFloat(12) - CGRectGetMaxX(self.avatarImgView.frame) - 54, [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    // 3. 创建location
    self.locationLabel.text = transferInternetCafesSingleModel.bar.address;
    self.locationLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(8), self.nameLabel.size_width, [NSString contentofHeightWithFont:self.locationLabel.font]);
    
    // 4. 创建网费
    self.priceLabel.text = transferInternetCafesSingleModel.bar.priceRange;
    self.priceLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.locationLabel.frame) + LCFloat(10), self.nameLabel.size_width, [NSString contentofHeightWithFont:self.priceLabel.font]);
    
    // 5. 创建距离
    self.distanceLabel.text = transferInternetCafesSingleModel.distance;
    CGSize distanceSize = [self.distanceLabel.text sizeWithCalcFont:self.distanceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.distanceLabel.font])];
    self.distanceLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(18) - distanceSize.width, CGRectGetMaxY(self.avatarImgView.frame) - [NSString contentofHeightWithFont:self.distanceLabel.font], distanceSize.width, [NSString contentofHeightWithFont:self.distanceLabel.font]);
    
    // 5.1 图标
    self.locationImageView.frame = CGRectMake(self.distanceLabel.orgin_x - LCFloat(2) - LCFloat(9), self.distanceLabel.center_y - LCFloat(9) / 2., LCFloat(9), LCFloat(9));

    // 6. 创建活动
    if (transferInternetCafesSingleModel.activity){
        self.activityImgView.hidden = NO;
    } else {
        self.activityImgView.hidden = YES;
    }
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"正文"] boldFont]];
    cellHeight += LCFloat(8);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
    cellHeight += LCFloat(10);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
    cellHeight += LCFloat(10);
    return cellHeight;
}
@end
