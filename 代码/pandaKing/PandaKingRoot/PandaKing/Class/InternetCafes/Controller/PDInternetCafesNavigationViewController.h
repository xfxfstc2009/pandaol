//
//  PDInternetCafesNavigationViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBaseMapViewController.h"
#import "PDInternetCafesSingleModel.h"

@interface PDInternetCafesNavigationViewController : PDBaseMapViewController

@property (nonatomic,strong)PDInternetCafesSingleModel *transferNetBarSingleModel;

@end
