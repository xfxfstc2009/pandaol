//
//  PDInternetCafesNavigationViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesNavigationViewController.h"
#import "PlusingAnnotationView.h"                                   // 大头指针【当前定位地址】
#import "PDNetBarAnnotation.h"
#import "MMHLocationNavigationManager.h"

#define kCalloutViewMargin          -8
@interface PDInternetCafesNavigationViewController()<UIGestureRecognizerDelegate>
@property (nonatomic, strong) UIPanGestureRecognizer *panGest;              /**< 拖动手势*/
@property (nonatomic,strong)MAPointAnnotation *annotation;
@end

@implementation PDInternetCafesNavigationViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"网吧位置";
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self mapBaseSetup];
    [self addlocation];
//    self.mapView.selectedAnnotations = @[self.annotation];
}


-(void)mapBaseSetup{
    [self changeMapCompassShow:NO];                                         // 1. 不显示罗盘
    [self changeMapScaleShow:NO];                                           // 1. 不显示比例尺
    [self changeMapTrafficType:NO];                                         // 1. 显示当前交通情况
    [self changeMapTypeWithType:MAMapTypeStandard];                         // 2. 显示当前普通地图
    [self showLocationWithStatus:YES];                                      // 3. 显示当前定位地址
    [self showLocationWithType:MAUserTrackingModeFollow];                   // 3.1 当前定位模式
    [self showLocationWithStatus:YES];
}

#pragma mark - 显示罗盘
-(void)changeMapCompassShow:(BOOL)isShow{
    self.mapView.showsCompass = isShow;
}

#pragma mark - 显示比例尺
-(void)changeMapScaleShow:(BOOL)isShow{
    self.mapView.showsScale = isShow;
}

#pragma mark - 修改地图类型【标准，卫星，黑夜】
-(void)changeMapTypeWithType:(MAMapType)mapType{
    self.mapView.mapType = mapType;
}

#pragma mark - 修改是否交通
-(void)changeMapTrafficType:(BOOL)show{
    self.mapView.showTraffic = show;
}

#pragma mark - 显示当前定位地址
#pragma mark 定位状态【打开\关闭】
-(void)showLocationWithStatus:(BOOL)isOpen{
    self.mapView.showsUserLocation = isOpen;
}

#pragma mark 定位模式
-(void)showLocationWithType:(MAUserTrackingMode)type{
    [self.mapView setUserTrackingMode:type animated:YES];
    self.mapView.customizeUserLocationAccuracyCircleRepresentation = YES;           // 去除周围的光圈
}

#pragma mark - 获取当前的大头指针
- (MAAnnotationView *)mapView:(MAMapView *)mapView viewForAnnotation:(id<MAAnnotation>)annotation {
    if ([annotation isKindOfClass:[MAUserLocation class]]){         // 【当前的定位地址】
        static NSString *annotationIdentifyWithOne = @"annotationIdentifyWithOne";
        PlusingAnnotationView *annotaionView = (PlusingAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifyWithOne];
        if (!annotaionView){
            annotaionView = [[PlusingAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifyWithOne];
        }
        return annotaionView;
    } else if ([annotation isKindOfClass:[MAPointAnnotation class]]){
        static NSString *annotationIdentifyWithThr = @"annotationIdentifyWithThr";
        PDNetBarAnnotation *annotaionView = (PDNetBarAnnotation *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifyWithThr];
        if (!annotaionView){
            annotaionView = [[PDNetBarAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifyWithThr];
        }
        annotaionView.backgroundColor = [UIColor clearColor];
        
        annotaionView.canShowCallout   = NO;
        annotaionView.draggable        = YES;
        annotaionView.transferNetBarSingleModel = self.transferNetBarSingleModel;
        annotaionView.calloutOffset    = CGPointMake(annotaionView.center_x, annotaionView.center_y);
        
        annotaionView.image = [UIImage imageNamed:@"icon_netbar_mapLocation"];
        __weak typeof(self)weakSelf = self;
        [annotaionView locationShow:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            MMHLocationNavigationManager *locationNav = [[MMHLocationNavigationManager alloc]init];
            [locationNav showActionSheetWithView:self.view shopName:strongSelf.transferNetBarSingleModel.bar.name shopLocationLat:strongSelf.transferNetBarSingleModel.latitude shopLocationLon:strongSelf.transferNetBarSingleModel.longtitude];
        }];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            CGRect frame = [annotaionView convertRect:annotaionView.calloutView.frame toView:self.mapView];
            annotaionView.frame = CGRectMake(frame.origin.x, frame.origin.y, LCFloat(10), LCFloat(10));
            [annotaionView setSelected:YES animated:YES];
        });
        return annotaionView;
    }
    return nil;
}

#pragma mark- 大头针点击方法
- (void)mapView:(MAMapView *)mapView didSelectAnnotationView:(MAAnnotationView *)view {
    /* Adjust the map center in order to show the callout view completely. */
    if ([view isKindOfClass:[PlusingAnnotationView class]]) {
        PlusingAnnotationView *cusView = (PlusingAnnotationView *)view;
        CGRect frame = [cusView convertRect:cusView.calloutView.frame toView:self.mapView];
        
        frame = UIEdgeInsetsInsetRect(frame, UIEdgeInsetsMake(kCalloutViewMargin, kCalloutViewMargin, kCalloutViewMargin, kCalloutViewMargin));
        
        if (!CGRectContainsRect(self.mapView.frame, frame))
        {
            /* Calculate the offset to make the callout view show up. */
            CGSize offset = [self offsetToContainRect:frame inRect:self.mapView.frame];
            
            CGPoint screenAnchor = [self.mapView getMapStatus].screenAnchor;
            CGPoint theCenter = CGPointMake(self.mapView.bounds.size.width * screenAnchor.x, self.mapView.bounds.size.height * screenAnchor.y);
            theCenter = CGPointMake(theCenter.x - offset.width, theCenter.y - offset.height);
            
            CLLocationCoordinate2D coordinate = [self.mapView convertPoint:theCenter toCoordinateFromView:self.mapView];
            
            [self.mapView setCenterCoordinate:coordinate animated:YES];
        }
    } else if ([view isKindOfClass:[PDNetBarAnnotation class]]){
        PlusingAnnotationView *cusView = (PlusingAnnotationView *)view;
        CGRect frame = [cusView convertRect:cusView.calloutView.frame toView:self.mapView];
        
        frame = UIEdgeInsetsInsetRect(frame, UIEdgeInsetsMake(kCalloutViewMargin, kCalloutViewMargin, kCalloutViewMargin, kCalloutViewMargin));
        
        if (!CGRectContainsRect(self.mapView.frame, frame))
        {
            /* Calculate the offset to make the callout view show up. */
            CGSize offset = [self offsetToContainRect:frame inRect:self.mapView.frame];
            
            CGPoint screenAnchor = [self.mapView getMapStatus].screenAnchor;
            CGPoint theCenter = CGPointMake(self.mapView.bounds.size.width * screenAnchor.x, self.mapView.bounds.size.height * screenAnchor.y);
            theCenter = CGPointMake(theCenter.x - offset.width, theCenter.y - offset.height);
            
            CLLocationCoordinate2D coordinate = [self.mapView convertPoint:theCenter toCoordinateFromView:self.mapView];
            
            [self.mapView setCenterCoordinate:coordinate animated:YES];
        }
    }
}

- (CGSize)offsetToContainRect:(CGRect)innerRect inRect:(CGRect)outerRect
{
    CGFloat nudgeRight = fmaxf(0, CGRectGetMinX(outerRect) - (CGRectGetMinX(innerRect)));
    CGFloat nudgeLeft = fminf(0, CGRectGetMaxX(outerRect) - (CGRectGetMaxX(innerRect)));
    CGFloat nudgeTop = fmaxf(0, CGRectGetMinY(outerRect) - (CGRectGetMinY(innerRect)));
    CGFloat nudgeBottom = fminf(0, CGRectGetMaxY(outerRect) - (CGRectGetMaxY(innerRect)));
    return CGSizeMake(nudgeLeft ?: nudgeRight, nudgeTop ?: nudgeBottom);
}





- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (gestureRecognizer == self.panGest && [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]){
        CGPoint point = [touch locationInView:gestureRecognizer.view];
        if (point.x < 50.0 || point.x > self.view.frame.size.width - 50.0) {
            self.mapView.scrollEnabled = NO;
            return YES;
        } else {
            self.mapView.scrollEnabled = YES;
            return NO;
        }
    }
    
    return YES;
}


#pragma mark ADDLOCATION
-(void)addlocation{
    self.annotation = [[MAPointAnnotation alloc] init];
    CLLocationCoordinate2D location;
    location.latitude = self.transferNetBarSingleModel.latitude;
    location.longitude = self.transferNetBarSingleModel.longtitude;
    self.annotation.coordinate = location;
    self.annotation.title    = self.transferNetBarSingleModel.bar.name;
    self.annotation.subtitle = self.transferNetBarSingleModel.bar.address;
    
    [self.mapView addAnnotation:self.annotation];
    [self.mapView showAnnotations:@[self.annotation] edgePadding:UIEdgeInsetsMake(0, 0, 0, 80) animated:YES];
}

@end
