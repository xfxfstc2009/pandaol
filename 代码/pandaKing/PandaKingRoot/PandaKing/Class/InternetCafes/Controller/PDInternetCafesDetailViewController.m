//
//  PDInternetCafesDetailViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesDetailViewController.h"
#import "PDInternetCafesDetailCell.h"                               // 头部的 cell
#import "MMHLocationNavigationManager.h"
#import "PDInternetCafesNavigationViewController.h"
#import "PDInternetCafesActivityListModel.h"
#import <HTHorizontalSelectionList.h>
#import "PDInternetCafesDetailHeaderView.h"
#import "PDInternetCafesDetailLocationView.h"                       // 地址
#import "PDInternetCafesActivityCell.h"
#define kNetBarHeader_Height LCFloat(200)

@interface PDInternetCafesDetailViewController()<UITableViewDataSource,UITableViewDelegate,HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate>{
    PDInternetCafesSingleModel *internetCasfesSingleModel;
    CGFloat _tableViewH;
    CGFloat _lastOffset;
    CGFloat _yOffset;
    CGFloat _changW;
    CGFloat _changY;
}
// 【网吧】
@property (nonatomic,strong)UITableView *netDetailTableView;                /**< 网吧详情*/
@property (nonatomic,strong)NSMutableArray *netDetailMutableArr;            /**< 网络详情*/
// 【赛事】
@property (nonatomic,strong)UITableView *challengerTableView;               /**< 赛事详情*/
@property (nonatomic,strong)NSMutableArray *challengerMutableArr;           /**< 赛事详情*/
// 【战队】
@property (nonatomic,strong)UITableView *teamTableView;                     /**< 战队*/
@property (nonatomic,strong)NSMutableArray *teamMutableArr;                 /**< 战队*/
@property (nonatomic,strong)UIButton *rightButton;                          /**< 关注网吧按钮*/


@property (nonatomic,strong)UIScrollView *mainScrollView;               /**< 主scrollview*/
@property (nonatomic,strong)UIView *headerView;                         /**< 头部的内容*/
@property (nonatomic,strong)PDInternetCafesDetailHeaderView *netHeaderView;    /**<头部信息*/

@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;     /**< segmentList*/
@property (nonatomic,strong)NSMutableArray *segmentMutableArr;          /**< segmentMutableArr*/
@property (nonatomic,strong)PDInternetCafesDetailLocationView *locationView;        /**< 地址view*/


@end

@implementation PDInternetCafesDetailViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 【新手引导】
    if (![Tool userDefaultGetWithKey:NewFeature_NetBar].length){
        PDNewFeaturePageViewController *newFeatureViewController = [[PDNewFeaturePageViewController alloc]init];
        [newFeatureViewController showInView:self.parentViewController type:PDNewFeaturePageViewControllerTypeNetBar];
    }
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                                             // 页面设置
    [self arrayWithInit];                                           // 数据初始化
    [self createScrollView];                                        // 创建背景的scrollview
    [self createHeaderView];                                        // 创建头部的view
    
    __weak typeof(self)weakSelf = self;
    if ([AccountModel sharedAccountModel].hasLoggedIn){             // 如果登录状态就去获取详情
        [weakSelf sendRequestToGetInfoWithBlock:NULL];
    }
    [self sendRequestToGetNetBarActivity];                          // 获取网吧活动
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"网吧详情";
    __weak typeof(self)weakSelf = self;
    self.rightButton = [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_netbar_notCollection"] barHltImage:[UIImage imageNamed:@"icon_netbar_notCollection"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf collectionButtonAnimation];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    //  【segment】
    self.segmentMutableArr = [NSMutableArray array];
    [self.segmentMutableArr addObjectsFromArray:@[@"活动",@"赛事",@"战队"]];
    
    self.netDetailMutableArr = [NSMutableArray array];
}

#pragma mark - createScrollView
-(void)createScrollView{
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        self.mainScrollView.frame = kScreenBounds;
        self.mainScrollView.delegate = self;
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 3, self.mainScrollView.size_height - self.tabBarController.tabBar.size_height - 64);
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.alwaysBounceVertical = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:self.mainScrollView];
    }
    self.netDetailTableView = [self createTableView];
    
    // 赛事
    self.challengerTableView = [self createTableView];
    self.challengerTableView.orgin_x = kScreenBounds.size.width;
    [self.challengerTableView showPrompt:@"暂无赛事" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
    
    
    // 战队
    self.teamTableView = [self createTableView];
    self.teamTableView.orgin_x =  2 * kScreenBounds.size.width;
    [self.teamTableView showPrompt:@"暂无战队" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
}



#pragma mark - UITableView
-(UITableView *)createTableView{
    UITableView *tableView= [[UITableView alloc] initWithFrame:self.mainScrollView.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.orgin_y = 0;
    tableView.size_height = self.mainScrollView.size_height - 64;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.showsVerticalScrollIndicator = YES;
    tableView.backgroundColor = [UIColor clearColor];
    [self createTableHeadView:tableView];
    [self.mainScrollView addSubview:tableView];
    
    return tableView;
}



#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.netDetailMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (tableView == self.netDetailTableView){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PDInternetCafesActivityCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[PDInternetCafesActivityCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferActivityModel = [self.netDetailMutableArr objectAtIndex:indexPath.section];
        
        return cellWithRowThr;
    } else if (tableView == self.challengerTableView){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        UITableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;

        }
        return cellWithRowFour;
    } else if (tableView == self.teamTableView){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        UITableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;

        }
        return cellWithRowFiv;
    }
    
    
//    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"网吧"]&& indexPath.row == [self cellIndexPathRowWithcellData:@"网吧"]){
//        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
//        PDInternetCafesDetailCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
//        if (!cellWithRowOne){
//            cellWithRowOne = [[PDInternetCafesDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
//            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
//            
//        }
//        cellWithRowOne.transferCellHeight = cellHeight;
//        cellWithRowOne.transferInternetCafesSingleModel = internetCasfesSingleModel;
//        
//        return cellWithRowOne;
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"标签"] && indexPath.row == [self cellIndexPathRowWithcellData:@"标签"]){
//        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
//        UITableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
//        if (!cellWithRowTwo){
//            cellWithRowTwo = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
//            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
//            
//        }
//        return cellWithRowTwo;
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"活动"] && indexPath.row == [self cellIndexPathRowWithcellData:@"活动"]){
//        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
//        UITableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
//        if (!cellWithRowThr){
//            cellWithRowThr = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
//            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
//            
//        }
//        return cellWithRowThr;
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"赛事"] && indexPath.row == [self cellIndexPathRowWithcellData:@"赛事"]){
//    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"战队"] && indexPath.row == [self     cellIndexPathRowWithcellData:@"战队"]){

//    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.netDetailTableView){
        PDInternetCafesActivitySingleModel *infoSingleModel = [self.netDetailMutableArr objectAtIndex:indexPath.section];
        return [PDInternetCafesActivityCell calculationCellHeight:infoSingleModel];
    } else {
        return 44;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.netDetailTableView) {
        SeparatorType separatorType = SeparatorTypeSingle;
        [cell addSeparatorLineWithType:separatorType];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.netDetailTableView){
        return LCFloat(15);
    } else {
        return LCFloat(10);
    }
}

#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.netDetailMutableArr.count ; i++){
        NSArray *dataTempArr = [self.netDetailMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.netDetailMutableArr.count ; i++){
        NSArray *dataTempArr = [self.netDetailMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}



#pragma mark - 接口

-(void)collectionButtonAnimation{
    if ([AccountModel sharedAccountModel].hasLoggedIn){         // 登录
        if (!self.transferInterNetCafesSingleModel.followed){          // 已经关注了
            [self sendRequestToCollection];
        } else {                                                      //
            [self sendRequestToNotCollection];
        }
    } else {                                                    // 未登录
        __weak typeof(self)weakSelf = self;
        [weakSelf authorizeWithCompletionHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            // 1. 获取数据
            [strongSelf sendRequestToGetInfoWithBlock:^{
                if (strongSelf.transferInterNetCafesSingleModel.followed){          // 已经关注了
                    [strongSelf sendRequestToCollection];
                } else {
                    [strongSelf sendRequestToNotCollection];
                }
            }];
        }];
    }
}

#pragma mark  获取网吧详情
-(void)sendRequestToGetInfoWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"barId":self.transferInterNetCafesSingleModel.bar.barId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:barDetail requestParams:params responseObjectClass:[PDInternetCafesSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDInternetCafesSingleModel *netbarSingleModel = (PDInternetCafesSingleModel *)responseObject;
            strongSelf.transferInterNetCafesSingleModel = netbarSingleModel;
            strongSelf.netHeaderView.transferInternetCafes = netbarSingleModel;
            strongSelf.locationView.transferLocation = netbarSingleModel.bar.address;
            if (strongSelf.transferInterNetCafesSingleModel.followed){
                [strongSelf.rightButton setImage:[UIImage imageNamed:@"icon_netbar_collection"] forState:UIControlStateNormal];
            } else {
                [strongSelf.rightButton setImage:[UIImage imageNamed:@"icon_netbar_notCollection"] forState:UIControlStateNormal];
            }
            if (block){
                block();
            }
        }
    }];
    
}


#pragma mark - 接口
// 关注网吧
-(void)sendRequestToCollection{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"barId":self.transferInterNetCafesSingleModel.bar.barId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:barCollection requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [Tool clickZanWithView:strongSelf.rightButton block:^{
                strongSelf.transferInterNetCafesSingleModel.followed = YES;
                [strongSelf.rightButton setImage:[UIImage imageNamed:@"icon_netbar_collection"] forState:UIControlStateNormal];
            }];
        }
    }];
}


// 取消关注
-(void)sendRequestToNotCollection{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"barId":self.transferInterNetCafesSingleModel.bar.barId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:barNotCollection requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){

            [Tool clickZanWithView:strongSelf.rightButton block:^{
                strongSelf.transferInterNetCafesSingleModel.followed = NO;
                [strongSelf.rightButton setImage:[UIImage imageNamed:@"icon_netbar_notCollection"] forState:UIControlStateNormal];
            }];
        }
    }];
}

// 获取网吧活动
-(void)sendRequestToGetNetBarActivity{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"barId":self.transferInterNetCafesSingleModel.bar.barId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:barActivity requestParams:params responseObjectClass:[PDInternetCafesActivityListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDInternetCafesActivityListModel *internetCafesActivityList = (PDInternetCafesActivityListModel *)responseObject;
            [strongSelf.netDetailMutableArr addObjectsFromArray:internetCafesActivityList.items];
            [strongSelf.netDetailTableView reloadData];
            
            if (strongSelf.netDetailMutableArr.count){
                [strongSelf.netDetailTableView dismissPrompt];
            } else {
                [strongSelf.netDetailTableView showPrompt:@"暂无网吧活动" withImage:nil andImagePosition:PDPromptImagePositionTop tapBlock:NULL];
            }
        }
    }];
}



#pragma mark scrollView
- (void)scrollViewDidScroll:(UIScrollView *)scrollView{
    
    if (![scrollView isKindOfClass:[UICollectionView class]]){
        CGFloat offsetY = scrollView.contentOffset.y;
        
        if (scrollView.contentOffset.y > (kNetBarHeader_Height - LCFloat(44))) {
            _headerView.center = CGPointMake(_headerView.center.x, _yOffset - (kNetBarHeader_Height - LCFloat(44)));
            return;
        }
        CGFloat h = _yOffset - offsetY;
        _headerView.center = CGPointMake(_headerView.center.x, h);
    }
}


-(void)createTableHeadView:(UITableView *)tableView{
    
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kNetBarHeader_Height)];
    tableHeaderView.backgroundColor = [UIColor clearColor];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.tableHeaderView = tableHeaderView;
    tableView.backgroundColor = [UIColor clearColor];
}

-(void)createHeaderView{
    self.headerView = [[UIView alloc]init];
    self.headerView.backgroundColor = [UIColor clearColor];
    self.headerView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kNetBarHeader_Height);
    _yOffset = self.headerView.center_y;
    
    // 创建头部内容
    self.netHeaderView = [[PDInternetCafesDetailHeaderView alloc]initWithFrame:self.headerView.bounds];
    self.netHeaderView.backgroundColor = [UIColor clearColor];
    [self.headerView addSubview:self.netHeaderView];
    
    [self createSegmentList];
    
    // 创建地址view
    self.locationView = [[PDInternetCafesDetailLocationView alloc]initWithFrame:CGRectMake(0, self.headerView.size_height - self.segmentList.size_height - LCFloat(44), kScreenBounds.size.width, LCFloat(44))];
    __weak typeof(self)weakSelf = self;
    [self.locationView locationClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDInternetCafesNavigationViewController *netInfoVC = [[PDInternetCafesNavigationViewController alloc]init];
        netInfoVC.transferNetBarSingleModel = strongSelf.transferInterNetCafesSingleModel;
        [strongSelf.navigationController pushViewController:netInfoVC animated:YES];
    }];
    [self.headerView addSubview:self.locationView];
    

    [self.view addSubview:self.headerView];
}


-(void)createSegmentList{
    self.segmentList = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake(0, kNetBarHeader_Height - LCFloat(44), kScreenBounds.size.width, LCFloat(44))];
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"灰"] forState:UIControlStateNormal];
    self.segmentList.selectionIndicatorColor = c15;
    self.segmentList.bottomTrimColor = [UIColor clearColor];
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.segmentList.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.segmentList.layer.shadowColor = [[UIColor colorWithCustomerName:@"灰"] CGColor];
    self.segmentList.layer.shadowOpacity = 2.0;
    self.segmentList.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    self.segmentList.isNotScroll = YES;
    [self.headerView addSubview:self.segmentList];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentMutableArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentMutableArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self.mainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.mainScrollView){
        CGPoint point = scrollView.contentOffset;
        NSInteger page = point.x / kScreenBounds.size.width;
        [self.segmentList setSelectedButtonIndex:page animated:YES];
        
        return;
    }
}

@end
