//
//  PDInternetCafesDetailMainViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesDetailMainViewController.h"
#import "MBTwitterScroll.h"
#import "PDInternetCafesNavigationViewController.h"
@interface PDInternetCafesDetailMainViewController()<UITableViewDelegate, UITableViewDataSource, MBTwitterScrollDelegate>
@property (nonatomic,strong) MBTwitterScroll* scrollingHeaderView;    /**< 头部*/
@property (nonatomic,strong)NSMutableArray *infoArr;

@property (nonatomic,strong)NSMutableDictionary *tableViewMutableDic;
@property (nonatomic,strong)NSMutableDictionary *dataSourceMutableDic;
@property (nonatomic,strong)NSMutableArray *currentMutableArr;
@end

@implementation PDInternetCafesDetailMainViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createMainView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"附近网吧";
    [self rightBarButtonWithTitle:@"确定" barNorImage:nil barHltImage:nil action:^{
        
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.tableViewMutableDic = [NSMutableDictionary dictionary];
    self.dataSourceMutableDic = [NSMutableDictionary dictionary];
    self.currentMutableArr = [NSMutableArray array];
}

#pragma mark - createMainView
-(void)createMainView{
    MBTwitterScroll *myTableView = [[MBTwitterScroll alloc]
                                    initTableViewWithBackgound:[UIImage imageNamed:@"bg_alert_black"]
                                    avatarImage:[UIImage imageNamed:@"icon_center_gou"]
                                    titleString:@"Main title"
                                    subtitleString:@"Sub title"
                                    buttonTitle:@"Follow"];  // Set nil for no button
    
    myTableView.tableView.delegate = self;
    myTableView.tableView.dataSource = self;
    myTableView.delegate = self;
    [self.view addSubview:myTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 10;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 10;
}


-(void) recievedMBTwitterScrollEvent {
//    [self performSegueWithIdentifier:@"showPopover" sender:self];
}


- (void) locationViewDidClick {
    PDInternetCafesNavigationViewController *VC = [[PDInternetCafesNavigationViewController alloc]init];
    [self.navigationController pushViewController:VC animated:YES];
}





@end
