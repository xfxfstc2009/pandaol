//
//  PDInternetCafesDetailMainViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDInternetCafesSingleModel.h"
@interface PDInternetCafesDetailMainViewController : AbstractViewController

@property (nonatomic,strong)PDInternetCafesSingleModel *transferInterNetCafesSingleModel;

@end
