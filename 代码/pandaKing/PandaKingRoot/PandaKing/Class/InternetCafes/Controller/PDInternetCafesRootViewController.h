//
//  PDInternetCafesRootViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,InternetCafesType) {
    InternetCafesTypeCollection,                    /**< 我的收藏的网吧*/
    InternetCafesTypeNearBy,                        /**< 附近的网吧*/
};

@interface PDInternetCafesRootViewController : AbstractViewController

@property (nonatomic,assign)InternetCafesType transferInternetCafeType;         /**< 传递的网吧类型*/

@end
