//
//  PDInternetCafesDetailViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDInternetCafesSingleModel.h"

@interface PDInternetCafesDetailViewController : AbstractViewController

@property (nonatomic,strong)PDInternetCafesSingleModel *transferInterNetCafesSingleModel;

@end
