//
//  PDInternetCafesRootViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesRootViewController.h"
#import "PDInternetCafesDetailViewController.h"
#import "PDInternetCafesSingleCell.h"

#import "PDInternetCafesListModel.h"                                    // Model


@interface PDInternetCafesRootViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *mainTableView;
@property (nonatomic,strong)NSMutableArray *netMutableArr;
@property (nonatomic,strong)UIButton *rightNavButton;               /**< 右侧nav按钮*/

@end

@implementation PDInternetCafesRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    __weak typeof(self)weakSelf = self;
    [weakSelf networkRootInterface];
}

#pragma mark - pageSetting
-(void)pageSetting{
    if (self.transferInternetCafeType == InternetCafesTypeCollection){
        self.barMainTitle = @"我的网吧";
    } else if (self.transferInternetCafeType == InternetCafesTypeNearBy){
        self.barMainTitle = @"附近的网吧";
        __weak typeof(self)weakSelf = self;
        [self rightBarButtonWithTitle:@"我的收藏" barNorImage:nil barHltImage:nil action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDInternetCafesRootViewController *cafesRootVC = [[PDInternetCafesRootViewController alloc]init];
            cafesRootVC.transferInternetCafeType = InternetCafesTypeCollection;
            [strongSelf.navigationController pushViewController:cafesRootVC animated:YES];
        }];
    }
    
//    __weak typeof(self)weakSelf = self;
   
//    self.rightNavButton = [weakSelf rightBarButtonWithTitle:@"杭州" barNorImage: [UIImage imageNamed:@"icon_challenge_slider"] barHltImage: [UIImage imageNamed:@"icon_challenge_slider"] action:^{
//        PDInternetCafesDetailRootViewController *netDetailViewController = [[PDInternetCafesDetailRootViewController alloc]init];
//
//        [self.navigationController pushViewController:netDetailViewController animated:YES];
//    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.netMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.mainTableView) {
        self.mainTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.mainTableView.delegate = self;
        self.mainTableView.dataSource = self;
        self.mainTableView.backgroundColor = [UIColor clearColor];
        self.mainTableView.showsVerticalScrollIndicator = NO;
        self.mainTableView.scrollEnabled = YES;
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.mainTableView];
    }
    // 添加下拉刷新
    __weak typeof(self)weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf networkRootInterface];
    }];

    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf networkRootInterface];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.netMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeiht = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    PDInternetCafesSingleCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDInternetCafesSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellheight = cellHeiht;
    
    PDInternetCafesSingleModel *internetCafesSingleModel = [self.netMutableArr objectAtIndex:indexPath.section];
    cellWithRowOne.transferInternetCafesSingleModel = internetCafesSingleModel;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PDInternetCafesDetailViewController *interNetCafesDetailMainViewController = [[PDInternetCafesDetailViewController alloc]init];
    interNetCafesDetailMainViewController.transferInterNetCafesSingleModel = [self.netMutableArr objectAtIndex:indexPath.section];
    [self.navigationController pushViewController:interNetCafesDetailMainViewController animated:YES];
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PDInternetCafesSingleCell calculationCellHeight];
}


#pragma mark - 接口
-(void)networkRootInterface{
    __weak typeof(self)weakSelf = self;
    if (self.transferInternetCafeType == InternetCafesTypeCollection){          // 【我的收藏】
        [weakSelf authorizeWithCompletionHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetNetBarInfoList];
        }];

    } else if (self.transferInternetCafeType == InternetCafesTypeNearBy){       // 【我附近的网吧】
        [weakSelf sendRequestToGetNearbyNetList];
    }
}

#pragma mark - 获取我收藏的网吧
-(void)sendRequestToGetNetBarInfoList{
    __weak typeof(self)weakSelf = self;
    
    NSDictionary *params = @{@"start":self.mainTableView.currentPage,@"latitude":@([PDCurrentLocationManager sharedLocation].lat),@"longtitude":@([PDCurrentLocationManager sharedLocation].lng)};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:myNetbarCollection requestParams:params responseObjectClass:[PDInternetCafesListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDInternetCafesListModel *internetCafesListModel = (PDInternetCafesListModel *)responseObject;
            if ([strongSelf.mainTableView.isXiaLa isEqualToString:@"YES"]){
                [strongSelf.netMutableArr removeAllObjects];
            }
            [strongSelf.netMutableArr addObjectsFromArray:internetCafesListModel.items];
            
            if(strongSelf.netMutableArr.count){
                [strongSelf.mainTableView dismissPrompt];
            } else {
                if (strongSelf.transferInternetCafeType == InternetCafesTypeCollection){
                    [strongSelf.mainTableView showPrompt:@"还没有关注网吧呢，快去收藏吧" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
                } else if (strongSelf.transferInternetCafeType == InternetCafesTypeNearBy){
                    [strongSelf.mainTableView showPrompt:@"找不到附近合作网吧，换个姿势试试" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
                }
            }

            [strongSelf.mainTableView reloadData];
            // 取消下拉
            if ([strongSelf.mainTableView.isXiaLa isEqualToString:@"YES"]){
                [strongSelf.mainTableView stopPullToRefresh];
            } else {
                [strongSelf.mainTableView stopFinishScrollingRefresh];
            }
        } else {
            [strongSelf.mainTableView showPrompt:@"服务器开了点小差，重新点击试试？" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:^{
                [strongSelf sendRequestToGetNetBarInfoList];
            }];
        }
    }];
}

#pragma mark - 获取我附近网吧列表
-(void)sendRequestToGetNearbyNetList{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"start":self.mainTableView.currentPage,@"latitude":@([PDCurrentLocationManager sharedLocation].lat),@"longtitude":@([PDCurrentLocationManager sharedLocation].lng)};
    [[NetworkAdapter sharedAdapter] fetchWithPath:myNearNetbarList requestParams:params responseObjectClass:[PDInternetCafesListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDInternetCafesListModel *internetCafesListModel = (PDInternetCafesListModel *)responseObject;
            if ([strongSelf.mainTableView.isXiaLa isEqualToString:@"YES"]){
                [strongSelf.netMutableArr removeAllObjects];
            }
            [strongSelf.netMutableArr addObjectsFromArray:internetCafesListModel.items];
            
            if(strongSelf.netMutableArr.count){
                [strongSelf.mainTableView dismissPrompt];
            } else {
                if (strongSelf.transferInternetCafeType == InternetCafesTypeCollection){
                    [strongSelf.mainTableView showPrompt:@"还没有关注网吧呢，快去收藏吧" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
                } else if (strongSelf.transferInternetCafeType == InternetCafesTypeNearBy){
                    [strongSelf.mainTableView showPrompt:@"找不到附近合作网吧，换个姿势试试" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
                }
            }
            
            [strongSelf.mainTableView reloadData];
            // 取消下拉
            if ([strongSelf.mainTableView.isXiaLa isEqualToString:@"YES"]){
                [strongSelf.mainTableView stopPullToRefresh];
            } else {
                [strongSelf.mainTableView stopFinishScrollingRefresh];
            }
        } else {
            [strongSelf.mainTableView showPrompt:@"服务器开了点小差，重新点击试试？" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:^{
                [strongSelf sendRequestToGetNetBarInfoList];
            }];
        }
    }];
}
@end
