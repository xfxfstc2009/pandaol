//
//  PDInternetCafesInfoSingleModel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesInfoSingleModel.h"

@implementation PDInternetCafesInfoSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"barId":@"id"};
}

@end
