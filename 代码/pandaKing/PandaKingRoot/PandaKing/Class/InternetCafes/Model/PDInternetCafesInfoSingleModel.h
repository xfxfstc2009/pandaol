//
//  PDInternetCafesInfoSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDInternetCafesInfoSingleModel : FetchModel

/*
 address = ggggg;
 avatar = "<null>";
 contact = "<null>";
 id = 57e9f3369e5a933441291d21;
 name = 3;
 phone = 17088742845;
 position =                     (
 "120.2686006",
 "30.2015031"
 );
 priceRange = "\Uffe55~10\U5143/\U5c0f\U65f6";
 up = 1;
 };
 */
@property (nonatomic,copy)NSString *address;            /**< 地址*/
@property (nonatomic,copy)NSString *avatar;             /**< 头像*/
@property (nonatomic,copy)NSString *contact;            /**< 联系人*/
@property (nonatomic,copy)NSString *barId;              /**< 网吧编号*/
@property (nonatomic,copy)NSString *name;               /**< 名字*/
@property (nonatomic,copy)NSString *phone;              /**< 联系号码*/
@property (nonatomic,copy)NSString *priceRange;         /**< 价格区间*/




@end
