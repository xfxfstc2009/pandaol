//
//  PDInternetCafesListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDInternetCafesSingleModel.h"
@interface PDInternetCafesListModel : FetchModel

@property (nonatomic,assign)NSInteger pageNum;                  /**< 页码数量*/
@property (nonatomic,assign)NSInteger totalItemsCount;          /**< 总条数*/
@property (nonatomic,assign)NSInteger pageItemsCount;           /**< 1页显示条数*/

@property (nonatomic,assign)BOOL hasPreviousPage;               /**< 是否有上页*/
@property (nonatomic,assign)BOOL hasNextPage;                   /**< 是否有下页*/
@property (nonatomic,assign)NSInteger pageCount;                /**< 页面总数*/

@property (nonatomic,strong)NSArray<PDInternetCafesSingleModel> *items;

@end
