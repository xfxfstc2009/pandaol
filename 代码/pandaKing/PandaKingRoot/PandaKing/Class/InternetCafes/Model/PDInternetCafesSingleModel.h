//
//  PDInternetCafesSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 单一网吧model
#import "FetchModel.h"
#import "PDInternetCafesInfoSingleModel.h"
@protocol PDInternetCafesSingleModel <NSObject>

@end

@interface PDInternetCafesSingleModel : FetchModel

@property (nonatomic,assign)BOOL activity;                                  /**< 是否活动*/
@property (nonatomic,strong)PDInternetCafesInfoSingleModel *bar;            /**< 网吧*/
@property (nonatomic,copy)NSString *distance;
@property (nonatomic,assign)CGFloat latitude;
@property (nonatomic,assign)CGFloat longtitude;
@property (nonatomic,assign)BOOL followed;                                  /**< 判断是否关注*/


@end
