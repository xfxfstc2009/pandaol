//
//  PDInternetCafesActivityListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDInternetCafesActivitySingleModel.h"

@interface PDInternetCafesActivityListModel : FetchModel

@property (nonatomic,strong)NSArray<PDInternetCafesActivitySingleModel > *items;

@end
