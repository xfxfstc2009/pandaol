//
//  PDInternetCafesActivitySingleModel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInternetCafesActivitySingleModel.h"

@implementation PDInternetCafesActivitySingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end
