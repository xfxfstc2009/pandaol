//
//  PDInternetCafesActivitySingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDInternetCafesActivitySingleModel <NSObject>

@end

@interface PDInternetCafesActivitySingleModel : FetchModel


@property (nonatomic,copy)NSString *barId;
@property (nonatomic,copy)NSString *content;
@property (nonatomic,assign)NSTimeInterval createTime;
@property (nonatomic,assign)NSTimeInterval startTime;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)NSTimeInterval endTime;

@end
