//
//  PDImageLabel.m
//  PandaKing
//
//  Created by Cranz on 17/3/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDImageLabel.h"

@interface PDImageLabel ()
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, assign, readwrite) PDImageLabelType type;
@property (nonatomic, assign) CGSize size;
@end

@implementation PDImageLabel

+ (instancetype)imageLabelWithType:(PDImageLabelType)type {
    return [[PDImageLabel alloc] initWithType:type];
}

- (instancetype)initWithType:(PDImageLabelType)type {
    self = [super init];
    if (!self) {
        return nil;
    }
    self.type = type;
    [self setupLabel];
    
    return self;
}

- (void)setupLabel {
    self.backgroundImageView = [[UIImageView alloc] init];
    [self addSubview:self.backgroundImageView];
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.textColor = [UIColor blackColor];
    self.textLabel.backgroundColor = [UIColor clearColor];
    self.textLabel.font = [UIFont systemFontOfSize:13];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.textLabel];
    
    self.imageView = [[UIImageView alloc] init];
    [self addSubview:self.imageView];
}


- (void)setText:(NSString *)text {
    _text = text;
    
    self.textLabel.text = text;
}

- (void)setFont:(UIFont *)font {
    _font = font;
    
    self.textLabel.font = font;
}

- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    
    self.textLabel.textColor = textColor;
}

- (void)setImage:(UIImage *)image {
    _image = image;
    
    self.imageView.image = image;
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    _backgroundImage = backgroundImage;
    
    self.backgroundImageView.image = backgroundImage;
}

- (CGSize)tSize {
    return self.size;
}

- (void)updateFrames {
    CGSize textSize = [self.textLabel.text sizeWithCalcFont:self.textLabel.font];
    CGSize imageSize = self.image.size;
    
    if (self.type == PDImageLabelTypeNormal) {
        self.size = CGSizeMake(self.edgeInsets.left + self.edgeInsets.right + textSize.width, self.edgeInsets.top + self.edgeInsets.bottom + textSize.height);
        
        self.textLabel.frame = CGRectMake(self.edgeInsets.left, self.edgeInsets.top, textSize.width, textSize.height);
        
    } else if (self.type == PDImageLabelTypeImageLeft) {
        self.size = CGSizeMake(self.edgeInsets.left + self.edgeInsets.right + textSize.width + kIMAGELABEL_SPACE + imageSize.width, self.edgeInsets.top + self.edgeInsets.bottom + textSize.height);
        
        self.imageView.frame = CGRectMake(self.edgeInsets.left, self.edgeInsets.top + (textSize.height - imageSize.height) / 2, imageSize.width, imageSize.height);
        self.textLabel.frame = CGRectMake(CGRectGetMaxX(self.imageView.frame) + kIMAGELABEL_SPACE, self.edgeInsets.top, textSize.width, textSize.height);
    } else if (self.type == PDImageLabelTypeImageRight) {
        self.size = CGSizeMake(self.edgeInsets.left + self.edgeInsets.right + textSize.width + kIMAGELABEL_SPACE + imageSize.width, self.edgeInsets.top + self.edgeInsets.bottom + textSize.height);
        
        self.textLabel.frame = CGRectMake(self.edgeInsets.left, self.edgeInsets.top, textSize.width, textSize.height);
        self.imageView.frame = CGRectMake(CGRectGetMaxX(self.textLabel.frame) + kIMAGELABEL_SPACE, (textSize.height - imageSize.height) / 2 + self.edgeInsets.top, imageSize.width, imageSize.height);
    }
    
    self.backgroundImageView.frame = CGRectMake(0, 0, self.tSize.width, self.tSize.height);
}

@end
