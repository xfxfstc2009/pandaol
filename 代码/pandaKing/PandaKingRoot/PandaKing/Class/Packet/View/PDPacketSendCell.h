//
//  PDPacketSendCell.h
//  PandaKing
//
//  Created by Cranz on 17/3/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PDPacketSendCellType) {
    PDPacketSendCellTypeGold,  // 金币红包
    PDPacketSendCellTypeBamboo, // 竹子红包
    PDPacketSendCellTypeNumber,  // 红包个数
};

/// 发红包界面cell
@interface PDPacketSendCell : UITableViewCell
@property (nonatomic, strong, readonly) UITextField *textField; // 输入
@property (nonatomic, strong) NSString *model;


+ (CGFloat)cellHeight;
- (instancetype)initWithSendStyle:(PDPacketSendCellType)type reuseIdentifier:(NSString *)reuseIdentifier;

- (void)packetSendCellDidChangeToAnotherSubdesc:(void(^)(NSString *tag))complication;
@end
