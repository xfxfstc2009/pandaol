//
//  PDPacketListCell.m
//  PandaKing
//
//  Created by Cranz on 17/3/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPacketListCell.h"
#import "PDImageLabel.h"

#define kPACKET_HEADER_WIDTH LCFloat(87/2) // 头像宽度
#define kPACKET_CELL_HEIGHT LCFloat(108/2) // cell的高度

@interface PDPacketListCell ()
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) PDImageLabel *specialLabel; // 特殊标记label<手气最佳>
@end

@implementation PDPacketListCell

- (PDImageLabel *)specialLabel {
    if (!_specialLabel) {
        _specialLabel = [PDImageLabel imageLabelWithType:PDImageLabelTypeImageLeft];
        _specialLabel.image = [UIImage imageNamed:@"icon_packet_luck"];
        _specialLabel.text = @"手气最佳";
        _specialLabel.textColor = [UIColor hexChangeFloat:@"fcae3a"];
        _specialLabel.hidden = YES;
        [_specialLabel updateFrames];
        [self.contentView addSubview:_specialLabel];
    }
    return _specialLabel;
}

+ (CGFloat)cellHeight {
    return kPACKET_CELL_HEIGHT;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupCell];
    }
    
    return self;
}

- (void)setupCell {
    // 头像
    self.headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, (kPACKET_CELL_HEIGHT - kPACKET_HEADER_WIDTH) / 2, kPACKET_HEADER_WIDTH, kPACKET_HEADER_WIDTH)];
    self.headerImageView.layer.cornerRadius = kPACKET_HEADER_WIDTH / 2;
    self.headerImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.headerImageView];
    
    // 昵称
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    // 时间
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.dateLabel.textColor = c28;
    [self.contentView addSubview:self.dateLabel];
    
    // 金币或者是竹子
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.numberLabel.textColor = [UIColor hexChangeFloat:@"232323"];
    [self.contentView addSubview:self.numberLabel];
}

- (void)setModel:(PDPacketRecordItem *)model {
    _model = model;
    
    [self.headerImageView uploadImageWithAvatarURL:model.avatar placeholder:nil callback:nil];
    self.nameLabel.text = model.nickName;
    self.dateLabel.text = [PDCenterTool dateWithFormat:@"MM-dd HH:mm" fromTimestamp:model.snatchTime];
    self.numberLabel.text = [NSString stringWithFormat:@"%@%@",@(model.amount),model.type];
    
    if (model.bestLuck) {
        self.specialLabel.hidden = NO;
    } else {
        self.specialLabel.hidden = YES;
    }
    
    [self updateFrames];
}

- (void)updateFrames {
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width / 2, [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGSize numSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font];
    
    CGFloat y = (kPACKET_CELL_HEIGHT - nameSize.height - dateSize.height - 6) / 2;
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + 10, y, nameSize.width, nameSize.height);
    self.dateLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + 6, dateSize.width, dateSize.height);
    self.numberLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - numSize.width, CGRectGetMinY(self.nameLabel.frame), numSize.width, numSize.height);
    self.specialLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - self.specialLabel.tSize.width, CGRectGetMinY(self.dateLabel.frame), self.specialLabel.tSize.width, self.specialLabel.tSize.height);
}

@end
