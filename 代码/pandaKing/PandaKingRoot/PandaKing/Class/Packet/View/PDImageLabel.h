//
//  PDImageLabel.h
//  PandaKing
//
//  Created by Cranz on 17/3/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PDImageLabelType) {
    PDImageLabelTypeNormal, // 和正常label一样
    PDImageLabelTypeImageLeft, // image在左侧
    PDImageLabelTypeImageRight, // image在右侧
};

#define kIMAGELABEL_SPACE 5

@interface PDImageLabel : UIControl
@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@property (nonatomic, assign, readonly) PDImageLabelType type;
@property (nonatomic, assign, readonly) CGSize tSize;

+ (instancetype)imageLabelWithType:(PDImageLabelType)type;
/** 主动去调用布局方法*/
- (void)updateFrames;
@end
