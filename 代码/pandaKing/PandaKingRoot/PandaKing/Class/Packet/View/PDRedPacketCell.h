//
//  PDRedPacketCell.h
//  PandaKing
//
//  Created by Cranz on 17/3/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "EaseBaseMessageCell.h"
#import "PDPacketMessageModel.h"

/// 红包显示cell
@interface PDRedPacketCell : EaseBaseMessageCell

@end
