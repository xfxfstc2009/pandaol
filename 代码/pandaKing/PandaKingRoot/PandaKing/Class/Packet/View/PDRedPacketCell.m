//
//  PDRedPacketCell.m
//  PandaKing
//
//  Created by Cranz on 17/3/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRedPacketCell.h"
#import "EaseBubbleView+PDPacket.h"

@interface PDRedPacketCell ()

@end

@implementation PDRedPacketCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier model:(id<IMessageModel>)model {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier model:model];
    
    if (self) {
        self.hasRead.hidden = YES;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.avatarSize = 40;
        self.avatarCornerRadius = self.avatarSize / 2;
    }
    
    return self;
}

- (BOOL)isCustomBubbleView:(id<IMessageModel>)model {
    return YES;
}

- (void)setCustomModel:(id<IMessageModel>)model {
    UIImage *image = model.image;
    if (!image) {
        [self.bubbleView.imageView sd_setImageWithURL:[NSURL URLWithString:model.fileURLPath] placeholderImage:[UIImage imageNamed:model.failImageName]];
    } else {
        _bubbleView.imageView.image = image;
    }
    
    if (model.avatarURLPath) {
        [self.avatarView sd_setImageWithURL:[NSURL URLWithString:model.avatarURLPath] placeholderImage:model.avatarImage];
    } else {
        self.avatarView.image = model.avatarImage;
    }
}

- (void)setCustomBubbleView:(id<IMessageModel>)model {
    [_bubbleView setupPacketBubbleView];
    
    _bubbleView.imageView.image = [UIImage imageNamed:@"imageDownloadFail"];
}

- (void)updateCustomBubbleViewMargin:(UIEdgeInsets)bubbleMargin model:(id<IMessageModel>)model
{
    [_bubbleView updatePacketMargin:bubbleMargin];
    _bubbleView.translatesAutoresizingMaskIntoConstraints = NO;
}

+ (NSString *)cellIdentifierWithModel:(id<IMessageModel>)model
{
    return model.isSender? @"PDRedPacketSenderId" : @"PDRedPacketReceiverId";
}

+ (CGFloat)cellHeightWithModel:(id<IMessageModel>)model
{
    return BUBBLE_PACKET_HEIGHT + 20;
}

- (void)setModel:(id<IMessageModel>)model {
    [super setModel:model];
    
    PDPacketMessageModel *messageModel = [PDPacketMessageModel getPacketMessageFromExt:model.message.ext];
    
    self.bubbleView.packetIcon.image = messageModel.packetIsGold? [UIImage imageNamed:@"img_packet_gold"] : [UIImage imageNamed:@"img_packet_bamboo"];
    self.bubbleView.titleLabel.text = messageModel.title;
    self.bubbleView.subTitleLabel.text = messageModel.packetIsGold? @"领取金币红包":@"领取竹子红包";
}

@end
