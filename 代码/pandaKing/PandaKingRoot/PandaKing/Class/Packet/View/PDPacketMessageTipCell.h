//
//  PDPacketMessageTipCell.h
//  PandaKing
//
//  Created by Cranz on 17/3/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPacketMessageModel.h"

typedef void(^PDPacketTipMessageBlock)(NSString *);
/// 红包领取情况
@interface PDPacketMessageTipCell : UITableViewCell

- (void)configWithPacketMessageModel:(PDPacketMessageModel *)messageModel;

+ (CGFloat)HeightForPacketMessageTipCell;

- (void)didClickPacketText:(void(^)(NSString *packetId))complication;

@end
