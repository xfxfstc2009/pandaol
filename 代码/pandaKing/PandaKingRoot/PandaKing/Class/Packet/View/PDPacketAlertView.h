//
//  PDPacketAlertView.h
//  PandaKing
//
//  Created by Cranz on 17/3/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPacketCheckInfo.h"

/// 红包弹框View
@interface PDPacketAlertView : UIView

- (instancetype)initWithModel:(PDPacketCheckInfo *)model;

/**
 * 点击取消
 */
- (void)packetViewDidClickCancle:(void(^)())complication;
/**
 * 点击开
 */
- (void)packetViewDidClickOpen:(void(^)())complication;
/**
 * 查看大家的手气
 */
- (void)packetViewDidClickLinkOthers:(void(^)())complication;
@end
