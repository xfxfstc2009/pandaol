//
//  PDPacketSendCell.m
//  PandaKing
//
//  Created by Cranz on 17/3/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPacketSendCell.h"
#import "PDCenterLabel.h"
#import "TYAttributedLabel.h"

#define kINPUT_HEIGHT   40
#define kSUBDESC_HEIGHT 25

typedef void(^PDPacketSendBlock)(NSString *tag);

@interface PDPacketSendCell ()<TYAttributedLabelDelegate>
@property (nonatomic, strong, readwrite) UITextField *textField; // 输入
@property (nonatomic, strong) PDCenterLabel *descLabel; // 输入前置描述
@property (nonatomic, strong) UILabel *numberLabel; // 数额单位
@property (nonatomic, strong) TYAttributedLabel *subDescLabel; // 附加描述
@property (nonatomic, assign) PDPacketSendCellType type;
@property (nonatomic, copy)   PDPacketSendBlock block;
@end

@implementation PDPacketSendCell

+ (CGFloat)cellHeight {
    return kINPUT_HEIGHT + kSUBDESC_HEIGHT;
}

- (instancetype)initWithSendStyle:(PDPacketSendCellType)type reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    _type = type;
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = c42;
    [self cellSetting];
    return self;
}

- (void)cellSetting {
    self.textField = [[UITextField alloc] init];
    self.textField.textAlignment = NSTextAlignmentRight;
    self.textField.borderStyle = UITextBorderStyleRoundedRect;
    self.textField.font = [UIFont systemFontOfCustomeSize:15];
    self.textField.keyboardType = UIKeyboardTypeNumberPad;
    self.textField.rightViewMode = UITextFieldViewModeAlways;
    self.textField.leftViewMode = UITextFieldViewModeAlways;
    [self.contentView addSubview:self.textField];
    
    // 左边
    self.descLabel = [[PDCenterLabel alloc] init];
    self.descLabel.textColor = [UIColor blackColor];
    self.descLabel.textFont = self.textField.font;
    self.descLabel.contentInsets = UIEdgeInsetsMake(0, 4, 0, 4);
    
    // 右边
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.font = self.descLabel.textFont;
    
    
    // 底下富文本
    self.subDescLabel = [[TYAttributedLabel alloc] init];
    self.subDescLabel.backgroundColor = c42;
    self.subDescLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.subDescLabel.linkColor = nil;
    self.subDescLabel.highlightedLinkBackgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.subDescLabel];
    
    [self updateCellContentWithType:_type];
    
    self.descLabel.frame = CGRectMake(0, 0, self.descLabel.size.width, self.descLabel.size.height);
    CGSize numSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font];
    self.numberLabel.frame = CGRectMake(0, 0, numSize.width + 8, numSize.height);
    
    self.textField.leftView = self.descLabel;
    self.textField.rightView = self.numberLabel;
    
    [self.subDescLabel setFrameWithOrign:CGPointMake(kTableViewSectionHeader_leftMaxtter, CGRectGetMaxY(self.textField.frame)) Width:kScreenBounds.size.width - kTableViewSectionHeader_leftMaxtter * 2];
}

/** 更新内容*/
- (void)updateCellContentWithType:(PDPacketSendCellType)type {
    self.textField.frame = CGRectMake(kTableViewSectionHeader_leftMaxtter, 0, kScreenBounds.size.width - kTableViewSectionHeader_leftMaxtter * 2, kINPUT_HEIGHT);
    if (type == PDPacketSendCellTypeBamboo) {
        self.textField.textColor = [UIColor blackColor];
        self.textField.placeholder = @"0.00";
        
        self.descLabel.image = [UIImage imageNamed:@"icon_packet_bamboo"];
        self.descLabel.text = @"总竹子";
        
        self.numberLabel.text = @"竹子";
        
        self.subDescLabel.delegate = self;
        self.subDescLabel.text = @"当前为竹子红包，改为金币红包";
        self.subDescLabel.textColor = [UIColor blackColor];
        TYLinkTextStorage *link = [[TYLinkTextStorage alloc] init];
        link.range = NSMakeRange(8, 6);
        link.textColor = [UIColor hexChangeFloat:@"46a8f3"];
        link.linkData = @"bamboo";
        link.underLineStyle = kCTUnderlineStyleNone;
        [self.subDescLabel addTextStorage:link];
        
    } else if (type == PDPacketSendCellTypeGold) {
        self.textField.textColor = [UIColor blackColor];
        self.textField.placeholder = @"0.00";
        
        self.descLabel.image = [UIImage imageNamed:@"icon_packet_gold"];
        self.descLabel.text = @"总金额";
        
        self.numberLabel.text = @"金币";
        
        self.subDescLabel.delegate = self;
        self.subDescLabel.text = @"当前为金币红包，改为竹子红包";
        self.subDescLabel.textColor = [UIColor blackColor];
        TYLinkTextStorage *link = [[TYLinkTextStorage alloc] init];
        link.range = NSMakeRange(8, 6);
        link.textColor = [UIColor hexChangeFloat:@"46a8f3"];
        link.linkData = @"gold";
        link.underLineStyle = kCTUnderlineStyleNone;
        [self.subDescLabel addTextStorage:link];
    } else {
        self.textField.textColor = c43;
        self.textField.placeholder = @"填写个数";
        
        self.descLabel.style = PDCenterLabelStyleReverse;
        self.descLabel.text = @"红包个数";
        
        self.subDescLabel.textColor = [UIColor lightGrayColor];
        
        self.numberLabel.text = @"个";
    }
    
    self.descLabel.frame = CGRectMake(self.descLabel.frame.origin.x, self.descLabel.frame.origin.y, self.descLabel.size.width, self.descLabel.size.height);
    CGSize numSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font];
    self.numberLabel.frame = CGRectMake(self.numberLabel.frame.origin.x, self.numberLabel.frame.origin.y, numSize.width + 8, numSize.height);
    
}

- (void)setModel:(NSString *)model {
    _model = model;
    
    if (_type == PDPacketSendCellTypeNumber) {
        self.subDescLabel.text = [NSString stringWithFormat:@"聊天室当前%@人",model];
    }
    
    [self.subDescLabel setFrameWithOrign:CGPointMake(kTableViewSectionHeader_leftMaxtter, CGRectGetMaxY(self.textField.frame)) Width:kScreenBounds.size.width - kTableViewSectionHeader_leftMaxtter * 2];
}

#pragma mark - TYAttributedLabelDelegate

- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)textStorage atPoint:(CGPoint)point {
    if ([textStorage isKindOfClass:[TYLinkTextStorage class]]) {
        NSString *linkData = ((TYLinkTextStorage *)textStorage).linkData;
        
        if ([linkData isEqualToString:@"bamboo"]) {
            [self updateCellContentWithType:PDPacketSendCellTypeGold];
            if (self.block) {
                self.block(@"gold");
            }
        } else {
            [self updateCellContentWithType:PDPacketSendCellTypeBamboo];if (self.block) {
                self.block(@"bamboo");
            }
        }
    }
}

- (void)packetSendCellDidChangeToAnotherSubdesc:(void (^)(NSString *))complication {
    if (complication) {
        self.block = complication;
    }
}

@end
