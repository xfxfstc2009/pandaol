//
//  PDPacketGetHeaderView.h
//  PandaKing
//
//  Created by Cranz on 17/3/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPacketDetail.h"

@interface PDPacketGetHeaderView : UIView
@property (nonatomic, strong) PDPacketDetail *model;

- (void)headerViewDidClickCheck:(void(^)())complication;
@end
