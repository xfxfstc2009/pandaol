//
//  PDPacketListCell.h
//  PandaKing
//
//  Created by Cranz on 17/3/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPacketRecordItem.h"

/// 收红包界面用户领取信息cell
@interface PDPacketListCell : UITableViewCell
@property (nonatomic, strong) PDPacketRecordItem *model;

+ (CGFloat)cellHeight;
@end
