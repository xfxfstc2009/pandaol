//
//  PDPacketDetail.h
//  PandaKing
//
//  Created by Cranz on 17/3/29.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPacketRecordItem.h"

/// 红包领取详情
@interface PDPacketDetail : FetchModel
/**
 * 创建红包者 id
 */
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *remark;
/**
 * 红包个数
 */
@property (nonatomic) NSUInteger peopleCount;
/**
 *  红包总金额
 */
@property (nonatomic) NSUInteger amount;
/**
 * 红包类型
 */
@property (nonatomic, copy) NSString *type;
/**
 * 是否 已经抢到过此红包
 */
@property (nonatomic) BOOL snatched;
/**
 * 未抢到的原因
 */
@property (nonatomic, copy) NSString *message;
/**
 * 已经被抢的个数
 */
@property (nonatomic) NSUInteger snatchedTotalCount;
/**
 * 已经被抢的总金额
 */
@property (nonatomic) NSUInteger snatchedTotalAmount;
/**
 * 当前会员抢到的红包金额
 */
@property (nonatomic) NSUInteger snatchedAmount;
/**
 * 是否被抢完
 */
@property (nonatomic) BOOL over;
/**
 * 抢完时间  2分钟
 */
@property (nonatomic, copy) NSString *snatchOverTime;
@property (nonatomic, strong) NSArray <PDPacketRecordItem> *records;
@end
