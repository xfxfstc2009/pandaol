//
//  PDPacketRecordItem.h
//  PandaKing
//
//  Created by Cranz on 17/3/29.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDPacketRecordItem <NSObject>

@end

/// 红包领取个人信息
@interface PDPacketRecordItem : FetchModel
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *avatar;
/**
 * 抢到的金额
 */
@property (nonatomic) NSUInteger amount;
/**
 * 抢到的时间的时间戳
 */
@property (nonatomic) NSTimeInterval snatchTime;
/**
 * 是否是最佳手气
 */
@property (nonatomic) BOOL bestLuck;
/**
 * 红包类型
 */
@property (nonatomic, copy) NSString *type;
@end
