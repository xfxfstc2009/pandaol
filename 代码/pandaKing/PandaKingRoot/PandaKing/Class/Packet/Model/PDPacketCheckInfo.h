//
//  PDPacketCheckInfo.h
//  PandaKing
//
//  Created by Cranz on 17/3/29.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 红包查看消息
@interface PDPacketCheckInfo : FetchModel
/**
 * 红包id
 */
@property (nonatomic, copy) NSString *redEnvelopeId;
/**
 * 创建红包者 id
 */
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, copy) NSString *nickName;
@property (nonatomic, copy) NSString *avatar;
/**
 * 是否显示开字（即是否能抢）
 */
@property (nonatomic) BOOL canSnatch;
/**
 * 是否 已经抢到过此红包
 */
@property (nonatomic) BOOL snatched;
/**
 * 是否超时
 */
@property (nonatomic) BOOL timeOut;
/**
 * 消息
 */
@property (nonatomic, copy) NSString *message;
@end
