//
//  PDPacketMessageModel.m
//  PandaKing
//
//  Created by Cranz on 17/3/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPacketMessageModel.h"

@implementation PDPacketMessageModel
+ (PDPacketMessageModel *)getPacketMessageFromExt:(NSDictionary *)ext {
    PDPacketMessageModel *model = [[PDPacketMessageModel alloc] init];
    
    int messageType = [ext[@"packetMessageType"] intValue];
    if (messageType == 1) { // 红包
        model.messageType = messageType;
    } else if (messageType == 2) { // 领取消息
        model.messageType = messageType;
    } else { // 其他消息
        model.messageType = 0;
    }
    
    if ([ext[@"packetId"] isKindOfClass:[NSString class]]) {
        model.packetId = ext[@"packetId"];
    }
    
    NSNumber *type = ext[@"packetType"];
    if (type) {
        model.type = type.intValue;
    }
    
    model.packetIsGold = [ext[@"packetIsGold"] boolValue];
    
    if ([ext[@"packetTitle"] isKindOfClass:[NSString class]]) {
        model.title = ext[@"packetTitle"];
    }
    
    model.packetIsFinished = [ext[@"packetIsFinished"] boolValue];
    
    if ([ext[@"packetOwnerId"] isKindOfClass:[NSString class]]) {
        model.packetOwnerId = ext[@"packetOwnerId"];
    }
    
    if ([ext[@"nickname"] isKindOfClass:[NSString class]]) {
        model.messageSenderNickname = ext[@"nickname"];
    }
    
    if ([ext[@"packetOwnerNickname"] isKindOfClass:[NSString class]]) {
        model.packetSenderNickname = ext[@"packetOwnerNickname"];
    }
    
    return model;
}

- (BOOL)isPacketSender {
    NSString *userId = [[NSUserDefaults standardUserDefaults] valueForKey:CustomerMemberId];
    return [userId isEqualToString:self.packetOwnerId]? YES : NO;
}

@end
