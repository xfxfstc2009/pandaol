//
//  PDPacketMessageModel.h
//  PandaKing
//
//  Created by Cranz on 17/3/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * 红包类型
 */
typedef NS_ENUM(NSUInteger, PDPacketType) {
    PDPacketTypeGroup,   // 群红包
};

/**
 * 消息类型
 */
typedef NS_ENUM(NSInteger, PDPacketMessageType) {
    PDPacketMessageTypeOther,  // 其他消息
    PDPacketMessageTypePacket,      // 红包消息
    PDPacketMessageTypePacketTip,   // 红包领取消息
};

/// 红包传递时的model 并非后台的数据model
@interface PDPacketMessageModel : NSObject
/**
 * 红包id
 */
@property (nonatomic, copy) NSString *packetId;
@property (nonatomic) PDPacketType type;
@property (nonatomic) PDPacketMessageType messageType;
/**
 * 是金币红包还是竹子红包
 */
@property (nonatomic) BOOL packetIsGold;
/**
 * 备注
 */
@property (nonatomic, copy) NSString *title;
/**
 * 红包是否领取完毕
 */
@property (nonatomic) BOOL packetIsFinished;
/**
 * 红包发送者id
 */
@property (nonatomic, copy) NSString *packetOwnerId;
/**
 * 红包发送者昵称
 */
@property (nonatomic, copy) NSString *packetSenderNickname;
/**
 * 红包发送者头像
 */
@property (nonatomic, copy) NSString *avatar;
/**
 * 当前用户是否是红包发送者
 */
@property (nonatomic, readonly) BOOL isPacketSender;
/**
 * 消息发送者的id
 */
@property (nonatomic, copy) NSString *messageSenderId;
/**
 * 消息发送者的昵称
 */
@property (nonatomic, copy) NSString *messageSenderNickname;


+ (PDPacketMessageModel *)getPacketMessageFromExt:(NSDictionary *)ext;
@end
