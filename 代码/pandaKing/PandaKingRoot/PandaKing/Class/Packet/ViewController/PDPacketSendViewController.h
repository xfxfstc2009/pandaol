//
//  PDPacketSendViewController.h
//  PandaKing
//
//  Created by Cranz on 17/3/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDPacketMessageModel.h"

typedef void(^PDPacketSendBlock)(PDPacketMessageModel *);
typedef void(^PDPacketCancleBlock)();
/// 发红包
@interface PDPacketSendViewController : AbstractViewController
/// 传递的房间号
@property (nonatomic, copy) NSString *chatroomId;
/**
 * 聊天室人数
 */
@property (nonatomic) NSUInteger membersCount;

/**
 * 发送红包后的回调
 */
- (void)didSendPacket:(void(^)(PDPacketMessageModel *message))sendComplication;

/**
 * 点击取消回调
 */
- (void)didCancleAcation:(void(^)())complication;
@end
