//
//  PDPacketGetViewController.h
//  PandaKing
//
//  Created by Cranz on 17/3/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

typedef void(^PDPacketGetBlock)(BOOL, BOOL, NSString *);

/// 领红包控制器
@interface PDPacketGetViewController : AbstractViewController
/**
 * 传递的红包id
 */
@property (nonatomic, copy) NSString *packetId;
/**
 * 第一次抢红包
 */
@property (nonatomic) BOOL firstTimeGrabRed;
/**
 * 是否是查看红包详情，当自己是红包发送者的时候可以选择查看
 */
@property (nonatomic) BOOL isCheckPacketDetail;

/**
 * 第一次抢到红包的时候的回调
 */
- (void)firstGrabRedPacket:(void(^)(BOOL packetIsGold, BOOL packetIsOver, NSString *nickname))complication;
@end
