//
//  PDPacketGetViewController.m
//  PandaKing
//
//  Created by Cranz on 17/3/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPacketGetViewController.h"
#import "PDPacketListCell.h"
#import "PDPacketGetHeaderView.h"
#import "PDPacketDetail.h" // 红包详情
#import "PDWalletViewController.h"

@interface PDPacketGetViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UIView *titleBar;
@property (nonatomic, strong) UITableView *mainTabelView;
@property (nonatomic, strong) PDPacketGetHeaderView *headerView;
@property (nonatomic, strong) UIView *footerView;
@property (nonatomic, strong) PDPacketDetail *packetDetail;
@property (nonatomic, strong) NSMutableArray *itemsArr;

@property (nonatomic, copy) PDPacketGetBlock firstBlock;
@end

@implementation PDPacketGetViewController

- (NSMutableArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = c43;
    [self titleBarSetting];
    [self setupWhiteView];
    [self setupTableView];
    if (self.isCheckPacketDetail == YES) {
        [self fetchPacketDetail];
    } else {
        [self fetchPacketOpen];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)titleBarSetting {
    self.titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    self.titleBar.backgroundColor = c43;
    [self.view addSubview:self.titleBar];
    
    // 关闭按钮
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *closeImage = [UIImage imageNamed:@"icon_packet_close"];
    CGSize closeImageSize = CGSizeMake(closeImage.size.width + 6, closeImage.size.height + 6);
    [closeButton setFrame:CGRectMake(kTableViewSectionHeader_left, 20 + (44 - closeImageSize.height) / 2, closeImageSize.width, closeImageSize.height)];
    [closeButton setImage:closeImage forState:UIControlStateNormal];
    [self.titleBar addSubview:closeButton];
    
    [closeButton addTarget:self action:@selector(didClickCloseButton) forControlEvents:UIControlEventTouchUpInside];
    
    // 标题
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    titleLabel.textColor = c40;
    titleLabel.text = @"红包";
    [self.titleBar addSubview:titleLabel];
    
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, 20 + (44 - titleSize.height)/2, titleSize.width, titleSize.height);
}

- (void)setupWhiteView {
    UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenBounds.size.height / 2, kScreenBounds.size.width, kScreenBounds.size.height / 2)];
    whiteView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:whiteView];
}

- (void)setupTableView {
    self.mainTabelView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height    )];
    self.mainTabelView.backgroundColor = [UIColor clearColor];
    self.mainTabelView.delegate = self;
    self.mainTabelView.dataSource = self;
    self.mainTabelView.separatorColor = c27;
    self.mainTabelView.tableFooterView = [UIView new];
    self.mainTabelView.rowHeight = [PDPacketListCell cellHeight];
    [self.view addSubview:self.mainTabelView];
    
    self.headerView = [[PDPacketGetHeaderView alloc] init];
    __weak typeof(self) weakSelf = self;
    [self.headerView headerViewDidClickCheck:^{
        PDWalletViewController *walletViewController = [[PDWalletViewController alloc] init];
        [weakSelf.navigationController pushViewController:walletViewController animated:YES];
    }];
    self.mainTabelView.tableHeaderView = self.headerView;
    
    // 如果是自己才会有这个
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(70))];
    footerView.backgroundColor = [UIColor whiteColor];
    
    UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 1)];
    line.backgroundColor = c27;
    [footerView addSubview:line];
    
    UILabel *descLabel = [[UILabel alloc] init];
    descLabel.text = @"未领取的红包，将于1小时后退还至您的钱包";
    descLabel.font = [UIFont systemFontOfCustomeSize:12];
    descLabel.textColor = c4;
    [footerView addSubview:descLabel];
    CGSize descSize = [descLabel.text sizeWithCalcFont:descLabel.font];
    descLabel.frame = CGRectMake((kScreenBounds.size.width - descSize.width) / 2, CGRectGetHeight(footerView.frame) - 10 - descSize.height, descSize.width, descSize.height);
    self.footerView = footerView;
    
    [self.view bringSubviewToFront:self.titleBar];
}

#pragma mark - 按钮方法

/** 关闭*/
- (void)didClickCloseButton {
    if (self.navigationController) {
        if (!self.navigationController.navigationBarHidden) {
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        }
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
     NSString *cellId = @"packetCellId";
    PDPacketListCell *listCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!listCell) {
        listCell = [[PDPacketListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    NSString *type;
    if ([self.packetDetail.type isEqualToString:@"gold"]) {
        type = @"金币";
    } else {
        type = @"竹子";
    }
    
    if (indexPath.row < self.itemsArr.count) {
        PDPacketRecordItem *item = self.itemsArr[indexPath.row];
        item.type = type;
        listCell.model = item;
    }
    
    return listCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
}

#pragma mark - 网络请求

/**
 * 打开红包领取的详情
 */
- (void)fetchPacketOpen {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:packetOpen requestParams:@{@"redEnvelopeId":self.packetId} responseObjectClass:[PDPacketDetail class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) return;
        if (isSucceeded) {
            weakSelf.packetDetail = (PDPacketDetail *)responseObject;
            
            [weakSelf.itemsArr addObjectsFromArray:weakSelf.packetDetail.records];
            [weakSelf.mainTabelView reloadData];
            
            weakSelf.headerView.model = weakSelf.packetDetail;
            
            if (weakSelf.firstTimeGrabRed && weakSelf.packetDetail.snatched) { // 第一次领取并且还要抢到红包，会发一条领取消息出去
                BOOL isGold = [weakSelf.packetDetail.type isEqualToString:@"gold"]? YES : NO;
                !weakSelf.firstBlock ?: weakSelf.firstBlock(isGold, weakSelf.packetDetail.over,weakSelf.packetDetail.nickName);
            }
            
            BOOL isPacketSender = [weakSelf.packetDetail.memberId isEqualToString:[Tool userDefaultGetWithKey:CustomerMemberId]]? YES : NO;
            if (isPacketSender) {
                weakSelf.mainTabelView.tableFooterView = weakSelf.footerView;
            } else {
                weakSelf.mainTabelView.tableFooterView = [UIView new];
            }
        }
    }];
}

/**
 * 显示红包详情，不领取
 */
- (void)fetchPacketDetail {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:packetDetail requestParams:@{@"redEnvelopeId":self.packetId} responseObjectClass:[PDPacketDetail class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) return ;
        if (isSucceeded) {
            weakSelf.packetDetail = (PDPacketDetail *)responseObject;
            
            [weakSelf.itemsArr addObjectsFromArray:weakSelf.packetDetail.records];
            [weakSelf.mainTabelView reloadData];
            
            weakSelf.headerView.model = weakSelf.packetDetail;
            
            BOOL isPacketSender = [weakSelf.packetDetail.memberId isEqualToString:[Tool userDefaultGetWithKey:CustomerMemberId]]? YES : NO;
            if (isPacketSender) {
                weakSelf.mainTabelView.tableFooterView = weakSelf.footerView;
            } else {
                weakSelf.mainTabelView.tableFooterView = [UIView new];
            }
        }
    }];
}

- (void)firstGrabRedPacket:(void (^)(BOOL packetIsGold, BOOL packetIsOver, NSString *nickname))complication {
    if (complication) {
        self.firstBlock = complication;
    }
}

@end
