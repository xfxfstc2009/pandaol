//
//  PDPacketSendViewController.m
//  PandaKing
//
//  Created by Cranz on 17/3/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPacketSendViewController.h"
#import "PDPacketSendCell.h"

#define kSENDPACKET_TITLECELL_HEIGHT 30 // 标题cell的高度
#define kSENDPACKET_DESCCELL_HEIGHT  84 // 红包附带描述的高度
#define kSENDPACKET_NUMBERLABEL_SPACE 36 // 总金额label的上下高度

@interface PDPacketSendViewController ()<UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong) UIView *titleBar;
@property (nonatomic, strong) UITableView *mainTableView;

@property (nonatomic, strong) UILabel *numberLabel; // 显示的总金额
@property (nonatomic, strong) UIButton *submitButton; // 提交按钮

@property (nonatomic, copy) PDPacketSendBlock sendBlock;
@property (nonatomic, copy) PDPacketCancleBlock cancleBlock;

/// 发送红包需要的一系列参数
@property (nonatomic, assign) NSUInteger amount; // 红包金额
@property (nonatomic, assign) NSUInteger count;  // 红包个数
@property (nonatomic, copy)   NSString *remark;  // 备注
@property (nonatomic, copy)   NSString *type;    // 类型 gold/bamboo
@end

@implementation PDPacketSendViewController

- (void)dealloc {
    [NOTIFICENTER removeObserver:self name:kKEYBOARD_HIDE object:nil];
    [NOTIFICENTER removeObserver:self name:kKEYBOARD_SHOW object:nil];
    [NOTIFICENTER removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self titleBarSetting];
    [self tableViewSetting];
    [self totalCountLabelSetting];
    [self setupSubmitButton];
    [self setupRemarkLabel];
    [self addNotifications];
}

- (void)titleBarSetting {
    self.titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    self.titleBar.backgroundColor = c43;
    [self.view addSubview:self.titleBar];
    
    // 关闭按钮
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *closeImage = [UIImage imageNamed:@"icon_packet_close"];
    CGSize closeImageSize = CGSizeMake(closeImage.size.width + 6, closeImage.size.height + 6);
    [closeButton setFrame:CGRectMake(kTableViewSectionHeader_left, 20 + (44 - closeImageSize.height) / 2, closeImageSize.width, closeImageSize.height)];
    [closeButton setImage:closeImage forState:UIControlStateNormal];
    [self.titleBar addSubview:closeButton];
    
    [closeButton addTarget:self action:@selector(didClickCloseButton) forControlEvents:UIControlEventTouchUpInside];
    
    // 标题
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    titleLabel.textColor = c40;
    titleLabel.text = @"发红包";
    [self.titleBar addSubview:titleLabel];
    
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, 20 + (44 - titleSize.height)/2, titleSize.width, titleSize.height);
}

- (void)tableViewSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, kScreenBounds.size.width, kScreenBounds.size.height - 64) style:UITableViewStyleGrouped];
    self.mainTableView.backgroundColor = c42;
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mainTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:self.mainTableView];
    
    [self.view bringSubviewToFront:self.titleBar];
}

- (void)totalCountLabelSetting {
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.textColor = [UIColor hexChangeFloat:@"000033"];
    self.numberLabel.font = [UIFont systemFontOfCustomeSize:44];
    self.numberLabel.text = @"0.00";
    [self.mainTableView addSubview:self.numberLabel];
    
    [self updateCountLabelsFrame];
}

- (void)updateCountLabelsFrame {
    CGSize size = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font];
    
    self.numberLabel.frame = CGRectMake((kScreenBounds.size.width - size.width)/2, kSENDPACKET_TITLECELL_HEIGHT + [PDPacketSendCell cellHeight] * 2 + kSENDPACKET_DESCCELL_HEIGHT + kTableViewHeader_height * 2 + kSENDPACKET_NUMBERLABEL_SPACE, size.width, size.height);
}

- (void)setupSubmitButton {
    self.submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.submitButton.frame = CGRectMake(55/2, CGRectGetMaxY(self.numberLabel.frame) + kSENDPACKET_NUMBERLABEL_SPACE, kScreenBounds.size.width - 55, 44);
    [self.submitButton setBackgroundImage:[UIImage imageWithRenderColor:c43 renderSize:self.submitButton.frame.size] forState:UIControlStateNormal];
    [self.submitButton setBackgroundImage:[UIImage imageWithRenderColor:c27 renderSize:self.submitButton.frame.size] forState:UIControlStateHighlighted];
    self.submitButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    [self.submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.submitButton setTitle:@"塞金币进红包" forState:UIControlStateNormal];
    self.submitButton.enabled = NO;
    self.submitButton.layer.cornerRadius = 2;
    self.submitButton.clipsToBounds = YES;
    [self.mainTableView addSubview:self.submitButton];
    
    [self.submitButton addTarget:self action:@selector(didClickSubmitButton) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setupRemarkLabel {
    UILabel *remarkLabel = [[UILabel alloc] init];
    remarkLabel.text = @"未领取的红包，将于1小时后退还至您的钱包";
    remarkLabel.textColor = [UIColor lightGrayColor];
    remarkLabel.font = [UIFont systemFontOfCustomeSize:12];
    [self.mainTableView addSubview:remarkLabel];
    
    CGSize size = [remarkLabel.text sizeWithCalcFont:remarkLabel.font];
    remarkLabel.frame = CGRectMake((kScreenBounds.size.width - size.width) / 2, CGRectGetHeight(self.mainTableView.frame) - 15 - size.height, size.width, size.height);
}

- (void)addNotifications {
    [NOTIFICENTER addObserver:self selector:@selector(keyBoardShow:) name:kKEYBOARD_SHOW object:nil];
    [NOTIFICENTER addObserver:self selector:@selector(keyBoardHide:) name:kKEYBOARD_HIDE object:nil];
    [NOTIFICENTER addObserver:self selector:@selector(textFieldDidChangeAction:) name:UITextFieldTextDidChangeNotification object:nil];
}

#pragma mark - 按钮方法

- (void)didCancleAcation:(void (^)())complication {
    if (complication) {
        self.cancleBlock = complication;
    }
}

- (void)didClickCloseButton {
    [self.view endEditing:YES];
    !self.cancleBlock ?: self.cancleBlock();
    [self dismissViewControllerAnimated:YES completion:nil];
}

/**
 * 发送红包
 */
- (void)didClickSubmitButton {
    UITableViewCell *textCell = [self.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    UITextView *textView = (UITextView *)[textCell viewWithStringTag:@"textView"];
    if (textView.text.length > 20) {
        [PDHUD showHUDSuccess:@"最长只能输入20个字！"];
        [textView.text substringToIndex:20];
        return;
    }
    
    self.remark = textView.text.length == 0? @"恭喜发财，大吉大利" : textView.text;
    
    PDPacketSendCell *amountCell = (PDPacketSendCell *)[self.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    self.amount = amountCell.textField.text.integerValue;
    if (self.amount == 0) {
        [PDHUD showHUDSuccess:@"您的总金额数量应大于零"];
        return;
    }
    
    PDPacketSendCell *countCell = (PDPacketSendCell *)[self.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    self.count = countCell.textField.text.integerValue;
    if (self.count == 0) {
        [PDHUD showHUDSuccess:@"您的红包个数应大于零"];
        return;
    }
    
    self.type = self.type? self.type : @"gold";
    
    // 发送
    [self fetchPacketCheck];
}

#pragma mark - 通知

- (void)keyBoardShow:(NSNotification *)noti {
    UITableViewCell *cell = [self.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
    UITextView *textView = (UITextView *)[cell viewWithStringTag:@"textView"];
    
    CGFloat height = kScreenBounds.size.height - (64 + CGRectGetMaxY(self.submitButton.frame));
    CGRect rect = [noti.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    if (height >= rect.size.height) {
        return;
    }
    
    if ([textView isFirstResponder]) {
        [UIView animateWithDuration:[noti.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
            self.mainTableView.frame = CGRectMake(0, 64 - (rect.size.height - height + 6), kScreenBounds.size.width, kScreenBounds.size.height - 64);
        }];
    }
}

- (void)keyBoardHide:(NSNotification *)noti {
    [UIView animateWithDuration:[noti.userInfo[UIKeyboardAnimationDurationUserInfoKey] floatValue] animations:^{
        self.mainTableView.frame = CGRectMake(0, 64, kScreenBounds.size.width, kScreenBounds.size.height - 64);
    }];
}

- (void)textFieldDidChangeAction:(NSNotification *)noti {
    UITableViewCell *titleCell = (UITableViewCell *)[self.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    
    PDPacketSendCell *amountCell = (PDPacketSendCell *)[self.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
    if ([amountCell.textField isFirstResponder]) {
        self.numberLabel.text = amountCell.textField.text;
        [self updateCountLabelsFrame];
    }
    
    PDPacketSendCell *countCell = (PDPacketSendCell *)[self.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    UITextField *textField = countCell.textField;
    if ([textField isFirstResponder]) {
        if (textField.text.integerValue > amountCell.textField.text.integerValue) {
            textField.text = amountCell.textField.text;
            
            [PDHUD showHUDSuccess:@"红包的个数必须小于等于总金额"];
        }
        
        if (textField.text.integerValue > 100) {
            textField.textColor = c43;
            // 显示超出提示
            titleCell.contentView.alpha = 1;
            titleCell.backgroundColor = c40;
            
            textField.text = @"100";
        } else {
            textField.textColor = [UIColor blackColor];
            // 隐藏超出提示
            titleCell.contentView.alpha = 0;
            titleCell.backgroundColor = [UIColor clearColor];
        }
    }
    
    if (amountCell.textField.text.length > 0 && countCell.textField.text.length > 0) {
        self.submitButton.enabled = YES;
    } else {
        self.submitButton.enabled = NO;
    }
}

#pragma mark - UITabelView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 2;
    } else if (section == 1) {
        return 1;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            NSString *cellId = @"titleCellId";
            UITableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:cellId];
            if (!titleCell) {
                titleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
                titleCell.selectionStyle = UITableViewCellSelectionStyleNone;
                titleCell.backgroundColor = [UIColor clearColor];
                titleCell.contentView.alpha = 0;
                
                UILabel *titleLabel = [[UILabel alloc] init];
                titleLabel.font = [UIFont systemFontOfCustomeSize:12];
                titleLabel.textColor = c43;
                titleLabel.text = @"一次最多发100个红包";
                titleLabel.stringTag = @"titleLabel";
                [titleCell.contentView addSubview:titleLabel];
                
                CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
                titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) /  2, (kSENDPACKET_TITLECELL_HEIGHT - titleSize.height) / 2, titleSize.width, titleSize.height);
            }
            
            return titleCell;
        } else {
            NSString *cellId = @"totalCoinCellId"; // 总金额
            PDPacketSendCell *totalCell = [tableView dequeueReusableCellWithIdentifier:cellId];
            if (!totalCell) {
                totalCell = [[PDPacketSendCell alloc] initWithSendStyle:PDPacketSendCellTypeGold reuseIdentifier:cellId];
                totalCell.textField.delegate = self;
            }
            
            __weak typeof(self) weakSelf = self;
            [totalCell packetSendCellDidChangeToAnotherSubdesc:^(NSString *tag) {
                if ([tag isEqualToString:@"gold"]) {
                    [weakSelf.submitButton setTitle:@"塞金币进红包" forState:UIControlStateNormal];
                } else {
                    [weakSelf.submitButton setTitle:@"塞竹子进红包" forState:UIControlStateNormal];
                }
                weakSelf.type = tag;
            }];
            return totalCell;
        }
    } else if (indexPath.section == 1) {
        NSString *cellId = @"numberCellId"; // 红包个数
        PDPacketSendCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[PDPacketSendCell alloc] initWithSendStyle:PDPacketSendCellTypeNumber reuseIdentifier:cellId];
            cell.textField.delegate = self;
        }

        return cell;
    } else {
        NSString *descCellId = @"descCellId";
        UITableViewCell *descCell = [tableView dequeueReusableCellWithIdentifier:descCellId];
        if (!descCell) {
            descCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:descCellId];
            descCell.backgroundColor = c42;
            
            UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_leftMaxtter, 0, kScreenBounds.size.width - kTableViewSectionHeader_leftMaxtter * 2, kSENDPACKET_DESCCELL_HEIGHT)];
            textView.layer.borderColor = c27.CGColor;
            textView.delegate = self;
            textView.layer.cornerRadius = 5;
            textView.layer.borderWidth = 1;
            textView.clipsToBounds = YES;
            textView.font = [UIFont systemFontOfCustomeSize:15];
            textView.placeholder = @"恭喜发财，大吉大利～";
            textView.limitMax = 20;
            textView.textColor = [UIColor blackColor];
            textView.stringTag = @"textView";
            [descCell.contentView addSubview:textView];
        }
        
        return descCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return kSENDPACKET_TITLECELL_HEIGHT;
        } else {
            return [PDPacketSendCell cellHeight];
        }
    } else if (indexPath.section == 1) {
        return [PDPacketSendCell cellHeight];
    } else {
        return kSENDPACKET_DESCCELL_HEIGHT;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.01;
    }
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

#pragma mark - UITextView delegate
- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [UIView animateWithDuration:0.3 animations:^{
        self.mainTableView.frame = CGRectMake(0, 64, kScreenBounds.size.width, kScreenBounds.size.height - 64);
    }];
}

#pragma mark - 红包相关接口

- (void)didSendPacket:(void (^)(PDPacketMessageModel *))sendComplication {
    if (sendComplication) {
        self.sendBlock = sendComplication;
    }
}

/**
 * 查看红包
 */
- (void)fetchPacketCheck {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:packetGetId requestParams:@{@"chatRoomId":self.chatroomId, @"amount":@(self.amount), @"peopleCount":@(self.count), @"remark":self.remark, @"type":self.type} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary* responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        if (isSucceeded) {
            PDPacketMessageModel *messageModel = [PDPacketMessageModel new];
            messageModel.packetId = responseObject[@"redEnvelopeId"];
            messageModel.packetSenderNickname = responseObject[@"nickName"];
            messageModel.avatar = responseObject[@"avatar"];
            messageModel.packetOwnerId = responseObject[@"memberId"];
            messageModel.packetIsGold = [responseObject[@"type"] isEqualToString:@"gold"]? YES : NO;
            messageModel.title = responseObject[@"remark"];
            
            [weakSelf dismissViewControllerAnimated:YES completion:nil];
            
            if (weakSelf.sendBlock) {
                weakSelf.sendBlock(messageModel);
            }
        }
    }];
}

- (void)setMembersCount:(NSUInteger)membersCount {
    _membersCount = membersCount;
    
    PDPacketSendCell *cell = [self.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    cell.model = [NSString stringWithFormat:@"%@",@(membersCount)];
}

@end
