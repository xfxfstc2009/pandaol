//
//  PDBindingInfoViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingInfoViewController.h"
#import "PDBindingDetailSegmentTableViewCell.h"                     // segment
#import "PDBindingDetailPickerTableViewCell.h"                      // picker
#import "PDBindingAreaInputRoleTableViewCell.h"                     // input
#import "PDBidingDetailBindingBtnTableViewCell.h"                   // btn
#import "PDBindingDetailCustomerCell.h"                             // Customer
#import "PDCustomerCenterViewController.h"

// Model
#import "PDBindingAreaListSingleModel.h"
#import "PDBindRoleModel.h"                                         // 召唤师model
#import "PDGameUserSingleModel.h"                                   // 获取我的召唤师
#import <objc/runtime.h>
#import "PDGameUserActivityModel.h"
#import "PDBindactivateGameUserModel.h"
#import "PDFindactivatedtimesSingleModel.h"

static char bindingKey;
static char bindingDidEndWithYuezhanBlockKey;
@interface PDBindingInfoViewController()<UITableViewDataSource,UITableViewDelegate,PDBindingDetailSegmentTableViewCellDelegate,PDBindingAreaInputRoleTableViewCellDelegate,PDNetworkAdapterDelegate,PDGuideRootViewControllerDelegate>{
    PDBindRoleModel *bindRoleModel;                         /**< 绑定角色的model*/
}
@property (nonatomic,strong)UITableView *bindingInfoTableView;          /**< 绑定tableView*/
@property (nonatomic,strong)NSArray *bindingArr;                        /**< 绑定数组*/
@property (nonatomic,strong)NSMutableArray *areaListTempArr;            /**< 地区列表*/

// Model
@property (nonatomic,strong)PDBindingAreaListModel *areaListModel;      /**< 获取绑定大区的列表*/
@property (nonatomic,strong)PDBindingAreaListSingleAreaListModel *selectedAreaModel;            /**< 用户选中的model*/

// view
@property (nonatomic,strong)UIButton *bindingButton;                /**< 绑定按钮*/
@property (nonatomic,strong)UIButton *leftButton;                   /**< 左侧按钮*/
@end

@implementation PDBindingInfoViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
//    if([AccountModel sharedAccountModel].bindingGameUser == NO){                      // 删除绑定召唤师
//        [self bindingRoleManager];
//    }
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createNotifi];                        // 添加代理
    
    // 获取大区
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestGetAreaWithJavaInfo];
    }];
    [NetworkAdapter sharedAdapter].delegate = self;
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"绑定召唤师";
    __weak typeof(self)weakSelf = self;
    if (self.navigationController.childViewControllers.count == 1){
        [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_page_cancle"] barHltImage:[UIImage imageNamed:@"icon_page_cancle"] action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
        }];
    }
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.areaListTempArr = [NSMutableArray array];
    self.bindingArr = @[@[@"服务商",@"区名"],@[@"召唤师输入"],@[@"绑定"],@[@"绑定帮助"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.bindingInfoTableView) {
        self.bindingInfoTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.bindingInfoTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.bindingInfoTableView.delegate = self;
        self.bindingInfoTableView.dataSource = self;
        self.bindingInfoTableView.backgroundColor = [UIColor clearColor];
        self.bindingInfoTableView.showsVerticalScrollIndicator = NO;
        self.bindingInfoTableView.scrollEnabled = YES;
        self.bindingInfoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.bindingInfoTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.bindingArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.bindingArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"服务商"] && indexPath.row == [self cellIndexPathRowWithcellData:@"服务商"]){          // segment
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDBindingDetailSegmentTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDBindingDetailSegmentTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.delegate = self;
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferAreaListModel = self.areaListModel;
        
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"区名"] && indexPath.row == [self cellIndexPathRowWithcellData:@"区名"]){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDBindingDetailPickerTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDBindingDetailPickerTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferAreaArr = self.areaListTempArr;
        cellWithRowTwo.transferCellHeight = cellHeight;
        
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo pickerViewDidSelectedWithBlock:^(PDBindingAreaListSingleAreaListModel *areaSingleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.selectedAreaModel = areaSingleModel;             // 获取当前选中的区
        }];
        
        cellWithRowTwo.transferModel = [self.areaListModel.server objectAtIndex:0];
        
        return cellWithRowTwo;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"召唤师输入"] && indexPath.row == [self cellIndexPathRowWithcellData:@"召唤师输入"]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PDBindingAreaInputRoleTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[PDBindingAreaInputRoleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowThr.delegate = self;
        }
        return cellWithRowThr;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"绑定"] && indexPath.row == [self cellIndexPathRowWithcellData:@"绑定"]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        PDBidingDetailBindingBtnTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[PDBidingDetailBindingBtnTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            self.bindingButton = cellWithRowFour.bindingButton;
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowFour bindingButtonClickManagerWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            
            if (strongSelf.transferBindingSceneType == bindingSceneTypeYuezhan){
                [strongSelf bindingInfoWithYuezhan];
                return;
            }
            
            [strongSelf sendRequestToBindRoleManager];
        }];
        
        cellWithRowFour.transferCellHeight = cellHeight;
        return cellWithRowFour;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"绑定帮助"] && indexPath.row == [self cellIndexPathRowWithcellData:@"绑定帮助"]){
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        PDBindingDetailCustomerCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[PDBindingDetailCustomerCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            
        }
        cellWithRowFiv.transferCellHeight = cellHeight;
        return cellWithRowFiv;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"绑定帮助"] && indexPath.row == [self cellIndexPathRowWithcellData:@"绑定帮助"]){
        PDWebViewController *customerCenterViewController = [[PDWebViewController alloc]init];
        [customerCenterViewController webDirectedWebUrl:@"/po/html/member/lol/bindgameuserrules.html"];
        [self.navigationController pushViewController:customerCenterViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"服务商"] && indexPath.row == [self cellIndexPathRowWithcellData:@"服务商"]){
        return [PDBindingDetailSegmentTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"区名"] && indexPath.row == [self cellIndexPathRowWithcellData:@"区名"]){
        return [PDBindingDetailPickerTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"召唤师输入"] && indexPath.row == [self cellIndexPathRowWithcellData:@"召唤师输入"]){
        return [PDBindingAreaInputRoleTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"绑定"] && indexPath.row == [self cellIndexPathRowWithcellData:@"绑定"]){
        return [PDBidingDetailBindingBtnTableViewCell calculationCellHeight];
    } else{
        return 44;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return LCFloat(30);
    } else {
        return LCFloat(10);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *viewForHeader = [[UIView alloc] init];
    
    if (section == 0){
        UILabel *headerTitle = [[UILabel alloc]init];
        headerTitle.backgroundColor = [UIColor clearColor];
        headerTitle.frame = CGRectMake(LCFloat(20), 0, kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(30));
        headerTitle.font = [UIFont systemFontOfSize:13.];
        headerTitle.textColor = [UIColor lightGrayColor];
        headerTitle.text = @"请选择绑定大区";
        [viewForHeader addSubview:headerTitle];
    }
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [viewForHeader addGestureRecognizer:tap];
    
    return viewForHeader;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = LCFloat(30);
    if (scrollView.contentOffset.y <= sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.bindingArr.count ; i++){
        NSArray *dataTempArr = [self.bindingArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellIndexPathOfSection = i;
                break;
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.bindingArr.count ; i++){
        NSArray *dataTempArr = [self.bindingArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            NSString *itemString = [dataTempArr objectAtIndex:j];
            if ([itemString isEqualToString:string]){
                cellRow = j;
                break;
            }
        }
    }
    return cellRow;;
}

-(void)tapManager{
    NSInteger section = [self cellIndexPathSectionWithcellData:@"召唤师输入"];
    NSInteger row = [self cellIndexPathRowWithcellData:@"召唤师输入"];
    PDBindingAreaInputRoleTableViewCell *cellWithRowThr = [self.bindingInfoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];
    if ([cellWithRowThr.roleNameTextField isFirstResponder]){
        [cellWithRowThr.roleNameTextField resignFirstResponder];
    }
}

#pragma mark - 键盘代理
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.view convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.view.bounds;
    newTextViewFrame.size.height = keyboardTop - self.view.bounds.origin.y;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    _bindingInfoTableView.frame = newTextViewFrame;
    [UIView commitAnimations];
    
}

- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    _bindingInfoTableView.frame = self.view.bounds;
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}



#pragma mark - Interface
#pragma mark 获取可绑定大区Arr
-(void)sendRequestGetAreaWithJavaInfo{
    NSString *alert = @"正在获取可绑大区";
    [PDHUD showHUDProgress:alert diary:0];
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:bindingGameserver requestParams:nil responseObjectClass:[PDBindingAreaListSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (isSucceeded) {
            [PDHUD showHUDProgress:alert diary:1];
            
            PDBindingAreaListSingleModel *bindingArea = (PDBindingAreaListSingleModel *)responseObject;
            NSMutableArray *serverArr = [NSMutableArray array];
            for (int i = 0 ;i<4;i++){
                PDBindingAreaListSingleModel *bindingTempArea = [[PDBindingAreaListSingleModel alloc]init];
                if (i == 0){
                    bindingTempArea.areaList = bindingArea.CT;
                    bindingTempArea.area = @"电信";
                } else if (i == 1){
                    bindingTempArea.areaList = bindingArea.CM;
                    bindingTempArea.area = @"网通";
                } else if (i == 2){
                    bindingTempArea.areaList = bindingArea.OTH;
                    bindingTempArea.area = @"其他";
                } else if (i == 3){
                    bindingTempArea.areaList = bindingArea.OS;
                    bindingTempArea.area = @"海外";
                }
                [serverArr addObject:bindingTempArea];
            }
            
            if (!strongSelf.areaListModel){
                strongSelf.areaListModel = [[PDBindingAreaListModel alloc]init];
            }
            strongSelf.areaListModel.server = [serverArr copy];
            
            if (strongSelf.areaListModel.server.count){
                // 1. 获取到当前的数组
                for (int i = 0 ; i < strongSelf.areaListModel.server.count;i++){
                    PDBindingAreaListSingleModel *bindingAreaListSingleModel = [strongSelf.areaListModel.server objectAtIndex:i];
                    if ([bindingAreaListSingleModel.area isEqualToString:@"电信"]){
                        [strongSelf.areaListTempArr removeAllObjects];
                        [strongSelf.areaListTempArr addObjectsFromArray:bindingAreaListSingleModel.areaList];
                        // 1. 将当前第一个数据保存起来
                        if (strongSelf.areaListTempArr.count){
                            strongSelf.selectedAreaModel = [strongSelf.areaListTempArr objectAtIndex:0];
                        }
                    }
                }
            } else {
                [strongSelf.bindingInfoTableView showPrompt:@"没有数据" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }

            [strongSelf.bindingInfoTableView reloadData];
        }
    }];
}

#pragma mark 申请绑定角色
-(void)sendRequestToBindRoleManager{
    // 1. 获取当前的区model
    NSString *serverId = self.selectedAreaModel.server_id;
    // 2. 获取当前用户选择的召唤师姓名
    NSInteger section = [self cellIndexPathSectionWithcellData:@"召唤师输入"];
    NSInteger row = [self cellIndexPathRowWithcellData:@"召唤师输入"];
    PDBindingAreaInputRoleTableViewCell *cellWithRowThr = (PDBindingAreaInputRoleTableViewCell *)[self.bindingInfoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];
    NSString *roleName = cellWithRowThr.roleNameTextField.text;
    [cellWithRowThr.roleNameTextField resignFirstResponder];
    
    NSString *err = @"";
    if (!serverId.length){
        err = @"没有选择服务区";
    } else if (!roleName.length){
        err = @"请输入召唤师名字";
    }
    
    if (err.length){
        [[PDAlertView sharedAlertView] showError:err];
    }
    
    [PDHUD showHUDProgress:@"正在拼命绑定中" diary:0];

    __weak typeof(self)weakSelf = self;
    NSDictionary *roleDic = @{@"gameUserName":roleName,@"gameServerId":serverId};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:bindingRole requestParams:roleDic responseObjectClass:[PDBindRoleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (isSucceeded) {
            [PDHUD showHUDProgress:@"正在拼命绑定中" diary:1];
            strongSelf->bindRoleModel = (PDBindRoleModel *)responseObject;
             void(^block)(BOOL isbind,BOOL isActivity) = objc_getAssociatedObject(strongSelf, &bindingKey);
            if ( strongSelf->bindRoleModel.alreadybind){             // 表示已经被绑定
                // 此时走
                [[PDAlertView sharedAlertView] showAlertWithTitle:@"绑定提示" conten:@"该召唤师已被其他玩家绑定，请立即进行认证以确认您的召唤师所有权" isClose:YES btnArr:@[@"立即认证"] buttonClick:^(NSInteger buttonIndex) {
                    [strongSelf sendRequestToPanduanBangdingNumber:strongSelf->bindRoleModel.gameUserId block:^(PDFindactivatedtimesSingleModel *singleModel) {
                        if (!singleModel.canGetAward){
                            [[PDAlertView sharedAlertView] dismissWithBlock:^{
                                [[PDAlertView sharedAlertView] showAlertBeirenzhengWithModel:singleModel btnArr:@[@"放弃认证",@"继续认证"] btnClick:^(NSInteger buttonIndex) {
                                    if (buttonIndex == 1){
                                        [[PDAlertView sharedAlertView] dismissWithBlock:^{
                                            [strongSelf sendRequestToActivityRole: strongSelf->bindRoleModel.gameUserId];
                                        }];
                                    } else {
                                        [strongSelf sendRequestToShaohouwithBlock:^{
                                            [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
                                            if (block){
                                                block(YES,NO);
                                            }
                                            if (strongSelf.navigationController.childViewControllers.count == 1){
                                                [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
                                            } else {
                                                [strongSelf.navigationController popViewControllerAnimated:YES];
                                            }
                                        }];
                                    }
                                }];
                            }];
                        } else {
                            [[PDAlertView sharedAlertView] dismissWithBlock:^{
                                [strongSelf sendRequestToActivityRole: strongSelf->bindRoleModel.gameUserId];
                            }];
                        }
                    }];
                }];
            } else {
                [[PDAlertView sharedAlertView] showBlackAlertWithAlertType:alertBackgroundTypeNormal title:@"绑定提示" content:[NSString stringWithFormat:@"绑定成功，您现在已可以参加玩游戏得金币的活动：\n胜：%@金币，负：%@金币\n认证召唤师后\n胜：%@金币，负：%@金币",strongSelf->bindRoleModel.winGold,strongSelf->bindRoleModel.loseGold,strongSelf->bindRoleModel.activeWinGold,strongSelf->bindRoleModel.activeLoseGold] isClose:NO house:nil couponModel:nil inputPlaceholader:nil isGender:NO btnArr:@[@"立即认证",@"稍后再说"] countDown:NO countDownBlock:NULL caipan:NO btnCliock:^(NSInteger buttonIndex, NSString *inputInfo) {
                    
                    if (buttonIndex == 0){              // 激活【立即认证】
                        [strongSelf sendRequestToPanduanBangdingNumber:strongSelf->bindRoleModel.gameUserId block:^(PDFindactivatedtimesSingleModel *singleModel) {
                            if (!singleModel.canGetAward){
                                [[PDAlertView sharedAlertView] dismissWithBlock:^{
                                    [[PDAlertView sharedAlertView] showAlertBeirenzhengWithModel:singleModel btnArr:@[@"放弃认证",@"继续认证"] btnClick:^(NSInteger buttonIndex) {
                                        if (buttonIndex == 1){
                                            [[PDAlertView sharedAlertView] dismissWithBlock:^{
                                                [strongSelf sendRequestToActivityRole: strongSelf->bindRoleModel.gameUserId];
                                            }];
                                        } else {
                                            [strongSelf sendRequestToShaohouwithBlock:^{
                                                [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
                                                if (block){
                                                    block(YES,NO);
                                                }
                                                if (strongSelf.navigationController.childViewControllers.count == 1){
                                                    [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
                                                } else {
                                                    [strongSelf.navigationController popViewControllerAnimated:YES];
                                                }
                                            }];
                                        }
                                    }];
                                }];
                            } else {
                                [[PDAlertView sharedAlertView] dismissWithBlock:^{
                                    [strongSelf sendRequestToActivityRole: strongSelf->bindRoleModel.gameUserId];
                                }];
                            }
                        }];
                    } else {                            // 取消【稍后再说】
                        [strongSelf sendRequestToShaohouwithBlock:^{
                            [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
                            if (block){
                                block(YES,NO);
                            }
                            if (strongSelf.navigationController.childViewControllers.count == 1){
                                [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
                            } else {
                                [strongSelf.navigationController popViewControllerAnimated:YES];
                            }
                        }];
                    }
                }];
            }
        } else {
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"绑定提示" conten:error.localizedDescription isClose:NO btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
                [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
            }];
        }
    }];
}

#pragma 激活召唤师
-(void)sendRequestToActivityRole:(NSString *)lolGameUserId{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params;
    if (lolGameUserId.length){
        params = @{@"lolGameUserId":lolGameUserId};
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:activateGameUser requestParams:params responseObjectClass:[PDBindactivateGameUserModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDBindactivateGameUserModel *activityModel = (PDBindactivateGameUserModel *)responseObject;
            
            if ([PDAlertView sharedAlertView].alertArr.count){
                [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
                    [strongSelf bindingWithChangeAvatarAlert:activityModel];
                }];
            } else {
                [strongSelf bindingWithChangeAvatarAlert:activityModel];
            }
        } else {
            [[PDAlertView sharedAlertView] showNormalAlertTitle:@"认证提示" content:@"认证失败,是否重试" isClose:NULL btnArr:@[@"确定重试",@"不重试"] btnCliock:^(NSInteger buttonIndex, NSString *inputInfo) {
                if (buttonIndex == 0){
                    [strongSelf sendRequestToActivityRole:strongSelf->bindRoleModel.gameUserId];
                }
               [[PDAlertView sharedAlertView]dismissWithBlock:NULL];
            }];
        }
    }];
}

-(void)bindingWithChangeAvatarAlert:(PDBindactivateGameUserModel *)activityGameUserModel{
    __weak typeof(self)weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithBindingCurrentAva:activityGameUserModel.avatar afterAva:activityGameUserModel.activateAvatar WithBlock:^(NSInteger btnIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (btnIndex == 0){         // 我已更换
            [strongSelf bindingToActivityManager];
            [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
        } else {                    // 稍后再说
            void(^block)(BOOL isbind,BOOL isActivity) = objc_getAssociatedObject(strongSelf, &bindingKey);
            [strongSelf sendRequestToShaohouwithBlock:^{
                [[PDAlertView sharedAlertView] dismissWithBlock:NULL];
                if (block){
                    block(YES,NO);
                }
                if (strongSelf.navigationController.childViewControllers.count == 1){
                    [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
                } else {
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }
            }];
        }
    }];
}

-(void)bindingToActivityManager{
    NSString *serverId = self.selectedAreaModel.server_id;
    
    NSInteger section = [self cellIndexPathSectionWithcellData:@"召唤师输入"];
    NSInteger row = [self cellIndexPathRowWithcellData:@"召唤师输入"];
    PDBindingAreaInputRoleTableViewCell *cellWithRowThr = (PDBindingAreaInputRoleTableViewCell *)[self.bindingInfoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];
    NSString *roleName = cellWithRowThr.roleNameTextField.text;
    
    __weak typeof(self)weakSelf = self;
    if ([PDAlertView sharedAlertView].alertArr.count){
        [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
    }
    [weakSelf sendRequestToRenzhengWithLolGameUserId:bindRoleModel.gameUserId serverId:serverId roleName:roleName];
}

#pragma mark - 代理
#pragma mark 代理服务器选择后进行修改
-(void)segmentDidSelectWithIndexStr:(NSString *)indexStr{
    // 1. 获取到当前的区名
    for (PDBindingAreaListSingleModel *singleMoel in self.areaListModel.server){
        if ([singleMoel.area isEqualToString:indexStr]){
            if (self.areaListTempArr.count){
                [self.areaListTempArr removeAllObjects];
            }
            [self.areaListTempArr addObjectsFromArray:singleMoel.areaList];
            // 刷新对应行
            [self.bindingInfoTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            
            PDBindingAreaListSingleAreaListModel *areaSingleModel = [self.areaListTempArr firstObject];
            self.selectedAreaModel = areaSingleModel;             // 获取当前选中的区
            
            return;
        }
    }
}

#pragma mark - 当文字修改
-(void)bindingAreaInputRoleTextFieldDidChange:(UITextField *)textField{
    [self adjustWithBindingButtonEnableWithTextField:textField];
}

-(void)bindingAreaInputRoleTextFieldDidEnd:(UITextField *)textField{
    __weak typeof(self)weakSelf = self;
    if (weakSelf.transferBindingSceneType == bindingSceneTypeYuezhan){
        [weakSelf bindingInfoWithYuezhan];
        return;
    }
    
    [weakSelf sendRequestToBindRoleManager];
}

-(void)bindingAreaInputRoleTextFieldDidConfirm:(UITextField *)textField{
    [self adjustWithBindingButtonEnableWithTextField:textField];
}

-(void)adjustWithBindingButtonEnableWithTextField:(UITextField *)textField{
    if (self.selectedAreaModel.server_id.length && textField.text.length){
        self.bindingButton.enabled = YES;
        self.bindingButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    } else {
        self.bindingButton.enabled = NO;
        self.bindingButton.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    }
}

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self tapManager];
}




#pragma mark - 绑定按钮
-(void)bindingDidEndWithBlock:(void(^)(BOOL isbind,BOOL isActivity))block{
    objc_setAssociatedObject(self, &bindingKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 稍后认证
-(void)sendRequestToShaohouwithBlock:(void(^)())block{
    if (block){
        block();
    }
}

#pragma mark - 进行头像验证
-(void)sendRequestToRenzhengWithLolGameUserId:(NSString *)userId serverId:(NSString *)serverId roleName:(NSString *)roleName{
    __weak typeof(self)weakSelf = self;
    
    NSString *error = @"";
    if (!userId.length){
        error = @"没有召唤师Id，请重试";
    } else if (!serverId.length){
        error = @"没有区服Id，请重试";
    } else if (!roleName.length){
        error = @"没有角色名，请重试";
    }
    
    if (error.length){
        [PDHUD showHUDError:error];
        return;
    }
    
    NSDictionary *params = @{@"lolGameUserId":userId,@"gameServerId":serverId,@"gameUserName":roleName};
    
    [PDHUD showHUDProgress:@"正在验证中，请稍后" diary:0];
    [[NetworkAdapter sharedAdapter] fetchWithPath:dealactivategameuser requestParams:params responseObjectClass:[PDGameUserActivityModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (isSucceeded){
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"认证提示" conten:@"认证申请已提交，系统核对完成后将会通知您" isClose:YES btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
                [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
                    void(^block)(BOOL isbind,BOOL isActivity) = objc_getAssociatedObject(strongSelf, &bindingKey);
                    if (block){
                        block(YES,YES);
                    }
                    if (strongSelf.navigationController.childViewControllers.count == 1){
                        [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
                    } else {
                        [strongSelf.navigationController popViewControllerAnimated:YES];
                    }
                }];
            }];
        }
    }];
}



-(void)bindingRoleManager{
    PDGuideRootViewController *guideRootViewController = [[PDGuideRootViewController alloc]init];
    
    // 1. 创建区服
    PDGuideSingleModel *paopaoModel = [[PDGuideSingleModel alloc]init];
    PDBindingDetailSegmentTableViewCell *cell = [self.bindingInfoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"服务商"] inSection:[self cellIndexPathSectionWithcellData:@"服务商"]]];
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    CGRect convertFrame = [cell convertRect:cell.segmentList.frame toView:keyWindow];
    paopaoModel.transferShowFrame = convertFrame;
    paopaoModel.index = 1;
    paopaoModel.text = @"选择您要绑定的游戏服务器";
    paopaoModel.guideType = GuideGuesterTypeNormal;
    paopaoModel.foundationType = foundationTypeRect;

    // 2.创建滚动框 创建区服
    PDGuideSingleModel *gundongModel = [[PDGuideSingleModel alloc]init];
    PDBindingDetailPickerTableViewCell *pickerCell = [self.bindingInfoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"区名"] inSection:[self cellIndexPathSectionWithcellData:@"区名"]]];
    CGRect pickerCellFrame = [pickerCell convertRect:pickerCell.locationPicker.frame toView:keyWindow];
    gundongModel.transferShowFrame = pickerCellFrame;
    gundongModel.index = 1;
    gundongModel.text = @"上下滑动选区，选择您要绑定的游戏大区";
    gundongModel.guideType = GuideGuesterTypeDrag;
    gundongModel.foundationType = foundationTypeRect;
    
    // 3. 输入框
    PDGuideSingleModel *shuruModel = [[PDGuideSingleModel alloc]init];
    PDBindingAreaInputRoleTableViewCell *shuruCell = [self.bindingInfoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"召唤师输入"] inSection:[self cellIndexPathSectionWithcellData:@"召唤师输入"]]];
    CGRect shuruCellFrame = [shuruCell convertRect:shuruCell.roleNameTextField.frame toView:keyWindow];
    shuruModel.transferShowFrame = shuruCellFrame;
    shuruModel.index = 1;
    shuruModel.text = @"输入您的召唤师昵称";
    shuruModel.guideType = GuideGuesterTypeNormal;
    shuruModel.foundationType = foundationTypeRect;
    
    // 4. 点击绑定等待裁判确定
    PDGuideSingleModel *bangdingModel = [[PDGuideSingleModel alloc]init];
    PDBidingDetailBindingBtnTableViewCell *bindingCell = [self.bindingInfoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"绑定"] inSection:[self cellIndexPathSectionWithcellData:@"绑定"]]];
    CGRect bangdingCellFrame = [bindingCell convertRect:bindingCell.bindingButton.frame toView:keyWindow];
    bangdingModel.transferShowFrame = bangdingCellFrame;
    bangdingModel.index = 1;
    bangdingModel.text = @"点击绑定";
    bangdingModel.guideType = GuideGuesterTypeNormal;
    bangdingModel.foundationType = foundationTypeRect;
    
    // 5. 如果有疑问可以点击帮助了解详情
    PDGuideSingleModel *yiwenModel = [[PDGuideSingleModel alloc]init];
    PDBindingDetailCustomerCell *bindingbangzhuCell = [self.bindingInfoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:[self cellIndexPathRowWithcellData:@"绑定帮助"] inSection:[self cellIndexPathSectionWithcellData:@"绑定帮助"]]];
    CGRect bangdingbangzhuCellFrame = [bindingbangzhuCell convertRect:bindingbangzhuCell.customerLabel.frame toView:keyWindow];
    yiwenModel.transferShowFrame = bangdingbangzhuCellFrame;
    yiwenModel.index = 1;
    yiwenModel.text = @"如有疑问可以点击帮助了解详情";
    yiwenModel.guideType = GuideGuesterTypeNormal;

    
    guideRootViewController.delegate = self;
    [guideRootViewController showInView:self.parentViewController withViewActionArr:@[paopaoModel,gundongModel,shuruModel,bangdingModel,yiwenModel]];
    [guideRootViewController bindingGameUserManager:^{
        
    }];
}


-(void)actionClickWithGuide:(PDGuideSingleModel *)guideSingleModel{
    
}

-(void)actionDismissGuide{
    
}


#pragma mark - 判断绑定了多少次
-(void)sendRequestToPanduanBangdingNumber:(NSString *)member block:(void(^)(PDFindactivatedtimesSingleModel *singleModel))modelBlock{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"lolGameUserId":member};
    [[NetworkAdapter sharedAdapter] fetchWithPath:findactivatedtimes requestParams:params responseObjectClass:[PDFindactivatedtimesSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            PDFindactivatedtimesSingleModel *singleModel = (PDFindactivatedtimesSingleModel *)responseObject;
            if (modelBlock){
                modelBlock(singleModel);
            }
        }
    }];
}


#pragma mark - 如果是约战的，就直接返回区和召唤师
-(void)bindingInfoWithYuezhan{
    // 2. 获取当前用户选择的召唤师姓名
    NSInteger section = [self cellIndexPathSectionWithcellData:@"召唤师输入"];
    NSInteger row = [self cellIndexPathRowWithcellData:@"召唤师输入"];
    PDBindingAreaInputRoleTableViewCell *cellWithRowThr = (PDBindingAreaInputRoleTableViewCell *)[self.bindingInfoTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];
    NSString *roleName = cellWithRowThr.roleNameTextField.text;
    
    void(^block)(PDBindingAreaListSingleAreaListModel *serverModel,NSString *roleName) = objc_getAssociatedObject(self, &bindingDidEndWithYuezhanBlockKey);
    if (block){
        block(self.selectedAreaModel,roleName);
    }
    if (self.navigationController.childViewControllers.count == 1){
        [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)bindingDidEndWithYuezhanBlock:(void(^)(PDBindingAreaListSingleAreaListModel *serverModel,NSString *roleName))block{
    objc_setAssociatedObject(self, &bindingDidEndWithYuezhanBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
