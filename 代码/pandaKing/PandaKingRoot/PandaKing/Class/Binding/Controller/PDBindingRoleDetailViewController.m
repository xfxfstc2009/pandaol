//
//  PDBindingRoleDetailViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingRoleDetailViewController.h"

@interface PDBindingRoleDetailViewController()<UITextViewDelegate>
@property (nonatomic,strong)UIView *bottomView;             /**< 底部的view*/
@property (nonatomic,strong)UITextView *adviceTextView;

@end

@implementation PDBindingRoleDetailViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createBottomView];
    [self createView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"角色详情";
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:@"分享" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSLog(@"123");
    }];
}


-(void)createView{
    self.adviceTextView = [[UITextView alloc]init];
    self.adviceTextView.delegate = self;
    self.adviceTextView.textColor = [UIColor blackColor];
    self.adviceTextView.returnKeyType = UIReturnKeyDefault;
    self.adviceTextView.keyboardType = UIKeyboardTypeDefault;
    self.adviceTextView.backgroundColor = [UIColor whiteColor];
    self.adviceTextView.scrollEnabled = YES;
    self.adviceTextView.limitMax = 300;
    self.adviceTextView.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.adviceTextView.frame = CGRectMake(LCFloat(16),LCFloat(20), kScreenBounds.size.width - 2 * LCFloat(16), LCFloat(203));
    [self.view addSubview:self.adviceTextView];
    self.adviceTextView.text = @"123";
    self.adviceTextView.placeholder = @"输入你的建议";
    __weak typeof(self)weakSelf = self;
    [weakSelf.adviceTextView textViewDidChangeWithBlock:^(NSInteger currentCount) {
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
        
    }];
}


#pragma mark - createView
-(void)createBottomView{
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.bottomView.frame = CGRectMake(0, kScreenBounds.size.height - 64 - LCFloat(44), kScreenBounds.size.width, LCFloat(44));
    [self.view addSubview:self.bottomView];
    
    UIButton *leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(0, 0, kScreenBounds.size.width / 2., self.bottomView.size_height);
    [leftBtn setTitle:@"立即认证" forState:UIControlStateNormal];
    leftBtn.backgroundColor = [UIColor colorWithCustomerName:@"绿"];
    __weak typeof(self)weakSelf = self;
    [leftBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        NSLog(@"立即认证");
    }];
    [self.bottomView addSubview:leftBtn];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(kScreenBounds.size.width / 2. , 0, kScreenBounds.size.width / 2., self.bottomView.size_height);
    [rightBtn setTitle:@"解除绑定" forState:UIControlStateNormal];
    rightBtn.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    [rightBtn buttonWithBlock:^(UIButton *button) {
       NSLog(@"解除绑定");
    }];
    [self.bottomView addSubview:rightBtn];
}


@end
