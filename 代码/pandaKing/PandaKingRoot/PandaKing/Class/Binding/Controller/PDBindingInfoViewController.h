//
//  PDBindingInfoViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDBindingAreaListSingleAreaListModel.h"

typedef NS_ENUM(NSInteger,bindingSceneType) {
    bindingSceneTypeNormal,
    bindingSceneTypeYuezhan,                    /**< 约战*/
};

@interface PDBindingInfoViewController : AbstractViewController

@property (nonatomic,assign)bindingSceneType transferBindingSceneType;              /**< 传递绑定类型*/

-(void)bindingDidEndWithBlock:(void(^)(BOOL isbind,BOOL isActivity))block;


#pragma mark - 如果是约战的，就直接返回区和召唤师
-(void)bindingDidEndWithYuezhanBlock:(void(^)(PDBindingAreaListSingleAreaListModel *serverModel,NSString *roleName))block;

@end
