//
//  PDCustomerCenterViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCustomerCenterViewController.h"
#import "PDBindingCustomerCenterCell.h"

@interface PDCustomerCenterViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *mainTableView;
@property (nonatomic,strong)NSArray *dataSourceArr;

@end

@implementation PDCustomerCenterViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - pageSettting
-(void)pageSetting{
    self.barMainTitle = @"客服中心";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.dataSourceArr = @[@"QQ",@"wechat",@"mail",@"link"];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.mainTableView) {
        self.mainTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.mainTableView.delegate = self;
        self.mainTableView.dataSource = self;
        self.mainTableView.backgroundColor = [UIColor clearColor];
        self.mainTableView.showsVerticalScrollIndicator = NO;
        self.mainTableView.scrollEnabled = YES;
        self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.mainTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.dataSourceArr.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdengifyWithRowOne = @"cellIdengifyWithRowOne";
    PDBindingCustomerCenterCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDBindingCustomerCenterCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.row == 0){
        cellWithRowOne.transferType = customeCentetTypeQQ;
    } else if (indexPath.row == 1){
        cellWithRowOne.transferType = customeCentetTypeQQTeam;
    } else if (indexPath.row == 2){
        cellWithRowOne.transferType = customeCentetTypeWechat;
    } else if (indexPath.row == 3){
        cellWithRowOne.transferType = customeCentetTypeLink;
    }
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return self.mainTableView.size_height / self.dataSourceArr.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 0;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    SeparatorType separatorType = SeparatorTypeMiddle;
    if ( [indexPath row] == 0) {
        separatorType  = SeparatorTypeHead;
    } else if ([indexPath row] == [self.dataSourceArr count] - 1) {
        separatorType  = SeparatorTypeBottom;
    } else {
        separatorType  = SeparatorTypeMiddle;
    }
    if ([self.dataSourceArr count] == 1) {
        separatorType  = SeparatorTypeSingle;
    }
    [cell addSeparatorLineWithType:separatorType];
    
    
    // Animation
    static CGFloat initialDelay = 0.2f;
    static CGFloat stutter = 0.26f;
    
    if ([cell isKindOfClass:[PDBindingCustomerCenterCell class]]){
        PDBindingCustomerCenterCell *cardCell = (PDBindingCustomerCenterCell *)cell;
        [cardCell startAnimationWithDelay:initialDelay + ((indexPath.row) * stutter)];
    }
    
}
@end
