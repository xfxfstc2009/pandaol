//
//  PDBindingCustomerCenterCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,customeCentetType) {
    customeCentetTypeQQ,
    customeCentetTypeQQTeam,
    customeCentetTypeWechat,
    customeCentetTypeMail,
    customeCentetTypeLink,
};

@interface PDBindingCustomerCenterCell : UITableViewCell

@property (nonatomic,assign)customeCentetType transferType;             /**< 传递的类型*/
@property (nonatomic,assign)CGFloat transferCellHeight;

-(void)startAnimationWithDelay:(CGFloat)delayTime;

@end
