//
//  PDBindingCustomerCenterCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingCustomerCenterCell.h"
#import <pop/POP.h>

@interface PDBindingCustomerCenterCell()
@property (nonatomic,strong)PDImageView *avatarImageView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *descLabel;

@end

@implementation PDBindingCustomerCenterCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建头像
    self.avatarImageView = [[PDImageView alloc]init];
    self.avatarImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImageView];
    
    // 2. 创建名字
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self addSubview:self.nameLabel];
    
    // 3. 创建desc
    self.descLabel = [[UILabel alloc]init];
    self.descLabel.backgroundColor = [UIColor clearColor];
    self.descLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.descLabel.textAlignment = NSTextAlignmentCenter;
    self.descLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.descLabel];
}

-(void)setTransferType:(customeCentetType)transferType{
    if (transferType == customeCentetTypeQQ){
        self.nameLabel.text = @"客服 QQ";
        self.descLabel.text = @"800080664";
        self.avatarImageView.image = [UIImage imageNamed:@"icon_customerCenter_qq"];
    } else if (transferType == customeCentetTypeQQTeam){
        self.nameLabel.text = @"玩家讨论群";
        self.descLabel.text = @"498534602";
        self.avatarImageView.image = [UIImage imageNamed:@"icon_customerCenter_qqTeam"];
    } else if (transferType == customeCentetTypeWechat){
        self.nameLabel.text = @"微信公众号";
        self.descLabel.text = @"盼达电竞";
        self.avatarImageView.image = [UIImage imageNamed:@"icon_customerCenter_wechat"];
    } else if (transferType == customeCentetTypeLink){
        self.nameLabel.text = @"商务合作";
        self.descLabel.text = @"support@panda-e.com";
        self.avatarImageView.image = [UIImage imageNamed:@"icon_customerCenter_link"];
    }
    
    self.avatarImageView.frame = CGRectMake(0, 0, LCFloat(65), LCFloat(65));
    // 1. 计算高度
    CGFloat singleMargin = (self.transferCellHeight - self.avatarImageView.size_height - [NSString contentofHeightWithFont:self.nameLabel.font] - [NSString contentofHeightWithFont:self.descLabel.font]) / 5.;
    
    self.avatarImageView.frame = CGRectMake((kScreenBounds.size.width - LCFloat(65)) / 2., singleMargin * 2, LCFloat(65), LCFloat(65));
    
    // 2. 创建名字
    self.nameLabel.frame = CGRectMake(0, CGRectGetMaxY(self.avatarImageView.frame) + singleMargin, kScreenBounds.size.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    // 3. 创建desc
    self.descLabel.frame = CGRectMake(0, CGRectGetMaxY(self.nameLabel.frame) + singleMargin, kScreenBounds.size.width, [NSString contentofHeightWithFont:self.descLabel.font]);
    
    
    self.avatarImageView.alpha = 0;
    self.nameLabel.alpha = 0;
    self.descLabel.alpha = 0;
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    
}


-(void)startAnimationWithDelay:(CGFloat)delayTime{
    [self showBlunesManager1];
    [UIView animateWithDuration:3. delay:delayTime usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        self.avatarImageView.alpha = 1;
        self.nameLabel.alpha = 1;
        self.descLabel.alpha = 1;
    } completion:^(BOOL finished) {
        [self showBlunesManager];
    }];
}



#pragma mark - Animation
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    if (self.highlighted) {
        [self showBlunesManager1];
        
    } else {
        [self showBlunesManager];
    }
}

-(void)showBlunesManager{
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.duration           = 0.2f;
    scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(.95, .95)];
    [self.avatarImageView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.nameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.descLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

-(void)showBlunesManager1{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(6, 6)];
    scaleAnimation.springBounciness    = 1.f;
    [self.avatarImageView pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.nameLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.descLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}



@end
