//
//  PDBindingDetailSegmentTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingDetailSegmentTableViewCell.h"
@interface PDBindingDetailSegmentTableViewCell()<HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate>
@property (nonatomic,strong) NSMutableArray *segmentArr;                        /**< 数据源数组*/
@end

@implementation PDBindingDetailSegmentTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView;
-(void)createView{
    if (!self.segmentList){
        self.segmentList = [[HTHorizontalSelectionList alloc] initWithFrame:CGRectMake(0, 0,kScreenBounds.size.width ,LCFloat(50))];
        self.segmentList.backgroundColor = [UIColor clearColor];
        self.segmentList.bottomTrimHidden = YES;
        self.segmentList.dataSource = self;
        self.segmentList.delegate = self;
        self.segmentList.isNotScroll = YES;
        self.segmentList.selectionIndicatorColor = c15;
        self.segmentList.bottomTrimColor = c15;
        [self.segmentList setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self addSubview:self.segmentList];
    }
    self.segmentArr = [NSMutableArray array];
}

#pragma mark 
-(void)setTransferAreaListModel:(PDBindingAreaListModel *)transferAreaListModel{
    _transferAreaListModel = transferAreaListModel;
    
    if (self.segmentArr.count){
        [self.segmentArr removeAllObjects];
    }
    for (PDBindingAreaListSingleModel *bindingAreaModel in transferAreaListModel.server){
        [self.segmentArr addObject:bindingAreaModel.area];
    }
    [self.segmentList reloadData];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    NSString *selectedStr = [self.segmentArr objectAtIndex:index];
    if (self.delegate && [self.delegate respondsToSelector:@selector(segmentDidSelectWithIndexStr:)]){
        [self.delegate segmentDidSelectWithIndexStr:selectedStr];
    }
}

#pragma mark - 抛出cell高度
+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}

@end
