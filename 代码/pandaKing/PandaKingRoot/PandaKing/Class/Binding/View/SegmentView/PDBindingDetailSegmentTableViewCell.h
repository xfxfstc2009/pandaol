//
//  PDBindingDetailSegmentTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBindingAreaListModel.h"
#import "HTHorizontalSelectionList.h"

@protocol PDBindingDetailSegmentTableViewCellDelegate <NSObject>

-(void)segmentDidSelectWithIndexStr:(NSString *)indexStr;

@end

@interface PDBindingDetailSegmentTableViewCell : UITableViewCell
@property (nonatomic,strong) HTHorizontalSelectionList *segmentList;            /**< segment */

@property (nonatomic,strong)PDBindingAreaListModel *transferAreaListModel;          /**< 传递过来的数据*/
@property (nonatomic,weak)id<PDBindingDetailSegmentTableViewCellDelegate> delegate;

+(CGFloat)calculationCellHeight;
@end
