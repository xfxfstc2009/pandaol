//
//  PDSpecialSingleView.m
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSpecialSingleView.h"
#import <objc/runtime.h>
@interface PDSpecialSingleView()
@property (nonatomic,strong)UIView *singleView;
@property (nonatomic,strong)UILabel *singleLabel;
@property (nonatomic,strong)UITapGestureRecognizer *tap;

@end

static char tapKey;
@implementation PDSpecialSingleView

-(instancetype)initWithText:(NSString *)text buttonClick:(void (^)(NSString *text))block{
    self = [super init];
    if (self){
        objc_setAssociatedObject(self, &tapKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        self.frame = CGRectMake(0, 0, LCFloat(30), LCFloat(30));
        [self createViewWithText:text];
    }
    return self;
}

-(void)createViewWithText:(NSString *)text {
    self.singleLabel = [[UILabel alloc]init];
    self.singleLabel.backgroundColor = [UIColor clearColor];
    self.singleLabel.font = [UIFont systemFontOfCustomeSize:16.];
    self.singleLabel.text = text;
    self.singleLabel.textAlignment = NSTextAlignmentCenter;
    self.singleLabel.frame = self.bounds;
    self.singleLabel.userInteractionEnabled = YES;
    [self addSubview:self.singleLabel];
    
    self.tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self.singleLabel addGestureRecognizer:self.tap];
}

-(void)tapManager{
    void (^block)(NSString *text) = objc_getAssociatedObject(self, &tapKey);
    if (block){
        block(self.singleLabel.text);
    }
}
@end
