//
//  PDBindingDetailPickerTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBindingAreaListSingleModel.h"
@interface PDBindingDetailPickerTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;                         /**< 传递过来的cell高度*/
@property (nonatomic,strong)NSArray *transferAreaArr;                           /**< 传递过来的地区数组*/
@property (nonatomic,strong)PDBindingAreaListSingleModel *transferModel;        /**< 传递过来进行修改的内容*/
@property (nonatomic,strong)UIPickerView *locationPicker;                       /**< picker*/

+(CGFloat)calculationCellHeight;                                                /**< 传出cell高度*/

-(void)pickerViewDidSelectedWithBlock:(void(^)(PDBindingAreaListSingleAreaListModel *areaSingleModel))block;

@end
