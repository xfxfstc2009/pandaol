//
//  PDBindingDetailPickerTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingDetailPickerTableViewCell.h"
#import <objc/runtime.h>

static char pickerViewDidSelectedKey;
@interface PDBindingDetailPickerTableViewCell()<UIPickerViewDelegate,UIPickerViewDataSource>
@property (nonatomic,strong)UIView *lineView;                                   /**< 上面的线条*/
@property (nonatomic,strong)NSMutableArray *dataSourceMutableArr;               /**< picker的数组对象*/

@end

@implementation PDBindingDetailPickerTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
        self.backgroundColor = [UIColor whiteColor];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    self.lineView.frame = CGRectMake(0, 0, kScreenBounds.size.width, .5f);
    [self addSubview:self.lineView];
    
    // 1. 创建一个picker
    self.locationPicker = [[UIPickerView alloc] initWithFrame:CGRectMake(LCFloat(25), CGRectGetMaxY(self.lineView.frame) , kScreenBounds.size.width - 2 * LCFloat(25), LCFloat(215))];
    self.locationPicker.delegate = self;
    self.locationPicker.showsSelectionIndicator = YES;
    self.locationPicker.backgroundColor = [UIColor clearColor];
    [self addSubview:self.locationPicker];
    // 2. 数据源初始化
    self.dataSourceMutableArr = [NSMutableArray array];
}

#pragma mark - UIPickerViewDataSource
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return self.dataSourceMutableArr.count;
}

-(CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return LCFloat(30);
}

-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    UILabel * pickerLabel = (UILabel*)view;
    if (!pickerLabel){
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.minimumScaleFactor = 8.;
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        [pickerLabel setTextAlignment:NSTextAlignmentCenter];
        [pickerLabel setBackgroundColor:[UIColor clearColor]];
        [pickerLabel setFont:[UIFont boldSystemFontOfSize:15]];
    }
    pickerLabel.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (self.dataSourceMutableArr.count){
        PDBindingAreaListSingleAreaListModel *areaSingleModel = [self.dataSourceMutableArr objectAtIndex:row];
        return areaSingleModel.server_name;
    } else {
        return @"";
    }
}


#pragma mark - UIPickerViewDelegate
-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    PDBindingAreaListSingleModel *areaSingleModel = [self.dataSourceMutableArr objectAtIndex:row];
    void(^block)(PDBindingAreaListSingleModel *areaSingleModel) = objc_getAssociatedObject(self, &pickerViewDidSelectedKey);
    if (block){
        block(areaSingleModel);
    }
}


-(void)setTransferAreaArr:(NSArray *)transferAreaArr{
    [self.dataSourceMutableArr removeAllObjects];
    [self.dataSourceMutableArr addObjectsFromArray:transferAreaArr];
    [self.locationPicker reloadAllComponents];
    
    if (!transferAreaArr.count){
        self.locationPicker.hidden = YES;
        [self.locationPicker showPrompt:@"当前没有对象信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
    } else {
        self.locationPicker.hidden = NO;
        [self.locationPicker dismissPrompt];
    }
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.locationPicker.frame = CGRectMake(0, 0, kScreenBounds.size.width, transferCellHeight);
}

-(void)setTransferModel:(PDBindingAreaListSingleModel *)transferModel{
    _transferModel = transferModel;
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeihgt = 0 ;
    cellHeihgt += LCFloat(215);
    return cellHeihgt;
}

-(void)pickerViewDidSelectedWithBlock:(void (^)(PDBindingAreaListSingleAreaListModel *))block{
    objc_setAssociatedObject(self, &pickerViewDidSelectedKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


@end
