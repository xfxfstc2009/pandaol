//
//  PDBindingDetailCustomerCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingDetailCustomerCell.h"

@interface PDBindingDetailCustomerCell()

@end

@implementation PDBindingDetailCustomerCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 添加客服中心label
    self.customerLabel = [[UILabel alloc]init];
    self.customerLabel.backgroundColor = [UIColor clearColor];
    self.customerLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.customerLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.customerLabel.textAlignment = NSTextAlignmentCenter;
    self.customerLabel.text = @"绑定帮助";
    [self addSubview:self.customerLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    CGSize bindingSize = [self.customerLabel.text sizeWithCalcFont:self.customerLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.customerLabel.font])];
    CGFloat width = bindingSize.width + 2 * LCFloat(11);
    self.customerLabel.frame = CGRectMake((kScreenBounds.size.width - width) / 2., 0, width, transferCellHeight);
}

@end
