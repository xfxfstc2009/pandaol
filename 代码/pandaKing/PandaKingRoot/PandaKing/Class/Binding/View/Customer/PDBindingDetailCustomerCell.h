//
//  PDBindingDetailCustomerCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDBindingDetailCustomerCell : UITableViewCell
@property (nonatomic,strong)UILabel *customerLabel;
@property (nonatomic,assign)CGFloat transferCellHeight;

@end
