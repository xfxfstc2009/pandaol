//
//  PDBidingDetailBindingBtnTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

// 创建绑定按钮
#import <UIKit/UIKit.h>

@interface PDBidingDetailBindingBtnTableViewCell : UITableViewCell

@property (nonatomic,strong)UIButton *bindingButton;            /**< 绑定按钮*/
@property (nonatomic,assign)CGFloat transferCellHeight;             /**< 上个页面*/

-(void)bindingButtonClickManagerWithBlock:(void(^)())block;

-(void)buttonStatus:(BOOL)status;

+(CGFloat)calculationCellHeight;

@end
