//
//  PDBindingAreaInputRoleTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PDBindingAreaInputRoleTableViewCellDelegate <NSObject>

-(void)bindingAreaInputRoleTextFieldDidChange:(UITextField *)textField;
-(void)bindingAreaInputRoleTextFieldDidEnd:(UITextField *)textField;
-(void)bindingAreaInputRoleTextFieldDidConfirm:(UITextField *)textField;

@end

@interface PDBindingAreaInputRoleTableViewCell : UITableViewCell

@property (nonatomic,weak)id<PDBindingAreaInputRoleTableViewCellDelegate> delegate;
@property (nonatomic,strong)UITextField *roleNameTextField;         /**< 角色名字输入框*/

+(CGFloat)calculationCellHeight;

@end
