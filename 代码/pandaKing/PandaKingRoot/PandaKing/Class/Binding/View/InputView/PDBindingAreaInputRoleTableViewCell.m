//
//  PDBindingAreaInputRoleTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingAreaInputRoleTableViewCell.h"
#import <objc/runtime.h>
#import "PDSpecialView.h"

//static char bindingButtonKey;
@interface PDBindingAreaInputRoleTableViewCell()<UITextFieldDelegate>{
    NSRange tempRange;
}
@property (nonatomic,strong)UIView *lineView;                       /**< line*/
@property (nonatomic,strong)NSArray *dymicArr;
@property (nonatomic,strong)PDSpecialView *specialView;

@end

@implementation PDBindingAreaInputRoleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self arrayWithInit];
        [self createView];
        [self createKey];
    }
    return self;
}

-(void)arrayWithInit{
    self.dymicArr = @[@"提示:请确保召唤师名与游戏中相同"];
}

-(void)createView{
    // 1. 输入框
    self.roleNameTextField = [[UITextField alloc]init];
    self.roleNameTextField.backgroundColor = [UIColor clearColor];
    self.roleNameTextField.placeholder = @"请输入召唤师姓名";
    self.roleNameTextField.textAlignment = NSTextAlignmentCenter;
    self.roleNameTextField.frame = CGRectMake(LCFloat(16),0, kScreenBounds.size.width - 2 * LCFloat(16), LCFloat(44));
    self.roleNameTextField.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.roleNameTextField.textColor = [UIColor blackColor];
    self.roleNameTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
    self.roleNameTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    self.roleNameTextField.returnKeyType = UIReturnKeyDone;
    self.roleNameTextField.delegate = self;
    [self.roleNameTextField addTarget:self action:@selector(textChageManager) forControlEvents:UIControlEventEditingChanged];
    self.roleNameTextField.keyboardType = UIKeyboardTypeDefault;
    self.roleNameTextField.backgroundColor = [UIColor clearColor];
    [self addSubview:self.roleNameTextField];
    __weak typeof(self)weakSelf = self;
    [self.roleNameTextField showBarCallback:^(UITextField *textField, NSInteger buttonIndex, UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (button == nil){
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(bindingAreaInputRoleTextFieldDidConfirm:)]){
                [strongSelf.delegate bindingAreaInputRoleTextFieldDidConfirm:strongSelf.roleNameTextField];
            }
            [textField resignFirstResponder];
            return ;
        }
        if (buttonIndex == 0){
            strongSelf->tempRange = [strongSelf.roleNameTextField selectedRange];
            [strongSelf changeKeyBoard];
            if ([button.titleLabel.text isEqualToString:@"特殊字符"]){
                [button setTitle:@"返回键盘" forState:UIControlStateNormal];
            } else {
                [button setTitle:@"特殊字符" forState:UIControlStateNormal];
            }
        } else if (buttonIndex == 1){
            if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(bindingAreaInputRoleTextFieldDidConfirm:)]){
                [strongSelf.delegate bindingAreaInputRoleTextFieldDidConfirm:strongSelf.roleNameTextField];
            }
            
            [textField resignFirstResponder];
        }

    }];
    

    // 2. 创建lineView
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    self.lineView.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.roleNameTextField.frame), kScreenBounds.size.width - 2 * LCFloat(11), .5f);
    [self addSubview:self.lineView];
    
    // 3. 创建label
    NSMutableArray *labelMutableArr = [NSMutableArray array];
    CGFloat dymic_size_width = 0;
    CGFloat origin_y = CGRectGetMaxY(self.lineView.frame) + LCFloat(5);
    for (int i = 0 ; i < self.dymicArr.count;i++){
        UILabel *fixedLabel = [[UILabel alloc]init];
        fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        fixedLabel.attributedText = [Tool rangeLabelWithContent:[self.dymicArr objectAtIndex:i] hltContentArr:@[@"提示:"] hltColor:[UIColor colorWithCustomerName:@"红"] normolColor:[UIColor colorWithCustomerName:@"黑"]];
        CGSize contentOfSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11),CGFLOAT_MAX)];
        fixedLabel.frame = CGRectMake(LCFloat(20), origin_y, kScreenBounds.size.width - 2 * LCFloat(11), contentOfSize.height);
        origin_y += contentOfSize.height;
        
        if (contentOfSize.width > dymic_size_width){
            dymic_size_width = contentOfSize.width;
        }
        
        [self addSubview:fixedLabel];
        [labelMutableArr addObject:fixedLabel];
    }
    
    for (UILabel *fixedLabel in labelMutableArr){
        fixedLabel.orgin_x = (kScreenBounds.size.width - dymic_size_width) / 2.;
    }
}

#pragma mark - 
-(void)textChageManager{
    if (self.delegate && [self.delegate respondsToSelector:@selector(bindingAreaInputRoleTextFieldDidChange:)]){
        [self.delegate bindingAreaInputRoleTextFieldDidChange:self.roleNameTextField];
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    if (self.delegate && [self.delegate respondsToSelector:@selector(bindingAreaInputRoleTextFieldDidEnd:)]){
        [self.delegate bindingAreaInputRoleTextFieldDidEnd:self.roleNameTextField];
    }
    return YES;
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(44);
    cellHeight += LCFloat(1);
    cellHeight += LCFloat(5);
    
    NSArray *dymicArr = @[@"1.请确保召唤师等级已达到30级",@"2.请登录游戏进行验证"];
    
    for (int i = 0 ; i < dymicArr.count;i++){
        NSString *title = [dymicArr objectAtIndex:i];
        CGSize contentOfSize = [title sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小提示"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11),CGFLOAT_MAX)];
        cellHeight += contentOfSize.height;
    }
    
    cellHeight += LCFloat(5);
    return cellHeight;
}

#pragma mark - 修改键盘输入
-(void)changeKeyBoard{
    [UIView animateWithDuration:.3f animations:^{
        if ([self.roleNameTextField.inputView isKindOfClass:[UIView class]]){
            self.roleNameTextField.inputView = nil;
        } else {
            self.roleNameTextField.inputView = self.specialView;
        }
        if ([self.roleNameTextField isFirstResponder]){
            [self.roleNameTextField resignFirstResponder];
            [self.roleNameTextField becomeFirstResponder];
        } else {
            
        }
        [self.roleNameTextField setSelectedRange:tempRange];
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - 创建键盘
-(void)createKey{
    self.specialView = [[PDSpecialView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(220))];
    self.specialView.backgroundColor = [UIColor whiteColor];
    __weak typeof(self)weakSelf = self;
    [self.specialView chooseBackBlock:^(NSString *string) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSRange range = [strongSelf.roleNameTextField selectedRange];
        
        if (strongSelf.roleNameTextField){
            // 1. 获取当前的字符串
            NSString *tempString = strongSelf.roleNameTextField.text;
            // 2. 截取字符串
            // 2.1 0 - location
            NSString *tempString1 = [tempString substringToIndex:range.location];
            NSString *tempString2 = [tempString substringFromIndex:range.location];
            NSString *newTempString = [tempString1 stringByAppendingString:string];
            NSString *newString = [newTempString stringByAppendingString:tempString2];
            strongSelf.roleNameTextField.text = newString;
            NSRange newRange = NSMakeRange(range.location + 1, 0);
            strongSelf->tempRange = newRange;
            [strongSelf.roleNameTextField setSelectedRange:newRange];
        }
    }];
}



@end
