//
//  PDBindactivateGameUserModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDBindactivateGameUserModel : FetchModel

@property (nonatomic,copy)NSString *activateAvatar;             /**< 修改头像*/
@property (nonatomic,copy)NSString *avatar;                     /**< 当前头像*/

@end
