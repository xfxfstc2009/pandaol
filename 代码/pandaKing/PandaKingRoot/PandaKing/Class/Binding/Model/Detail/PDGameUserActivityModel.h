//
//  PDGameUserActivityModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDGameUserActivityModel : FetchModel

@property (nonatomic,assign)BOOL activate;              /**< 是否正确*/

@end
