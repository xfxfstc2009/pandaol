//
//  PDBindingAreaListSingleAreaListModel.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingAreaListSingleAreaListModel.h"

@implementation PDBindingAreaListSingleAreaListModel

- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"server_id":@"id",@"server_name":@"name"};
}


@end
