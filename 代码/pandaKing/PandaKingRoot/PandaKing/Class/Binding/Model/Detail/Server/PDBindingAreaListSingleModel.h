//
//  PDBindingAreaListSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDBindingAreaListSingleAreaListModel.h"

@protocol PDBindingAreaListSingleModel <NSObject>

@end

@interface PDBindingAreaListSingleModel : FetchModel

@property (nonatomic,copy)NSString *area;               /**< 区*/
@property (nonatomic,strong)NSArray<PDBindingAreaListSingleAreaListModel> *areaList;  /**< 区域列表*/


// 映射
@property (nonatomic,strong)NSArray<PDBindingAreaListSingleAreaListModel> *CT;
@property (nonatomic,strong)NSArray<PDBindingAreaListSingleAreaListModel> *CM;
@property (nonatomic,strong)NSArray<PDBindingAreaListSingleAreaListModel> *OTH;
@property (nonatomic,strong)NSArray<PDBindingAreaListSingleAreaListModel> *OS;

@end
