//
//  PDBindingAreaListModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDBindingAreaListSingleModel.h"
@interface PDBindingAreaListModel : FetchModel

@property (nonatomic,strong)NSArray<PDBindingAreaListSingleModel> *server;

@end
