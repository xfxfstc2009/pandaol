//
//  PDSpecialListModel.h
//  PandaChallenge
//
//  Created by 裴烨烽 on 16/4/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 特殊字符的Model
#import "FetchModel.h"

@interface PDSpecialListModel : FetchModel

@property (nonatomic,copy)NSString *special;

@end
