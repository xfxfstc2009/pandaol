//
//  PDBindingAreaListSingleAreaListModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol  PDBindingAreaListSingleAreaListModel<NSObject>


@end

@interface PDBindingAreaListSingleAreaListModel : FetchModel

@property (nonatomic,copy)NSString *server_id;                  /**< 1*/
@property (nonatomic,copy)NSString *server_name;                /**< 艾欧尼亚*/
@property (nonatomic,copy)NSString *telecomLine;                /**< 电信*/

@end
