//
//  PDFindactivatedtimesSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDFindactivatedtimesSingleModel : FetchModel

@property (nonatomic,assign)NSInteger activatedTimes;               /**< 已经激活的次数*/
@property (nonatomic,assign)NSInteger limitTimes;                   /**< 限制次数*/
@property (nonatomic,assign)BOOL canGetAward;                       /**< 本次激活后能否获得奖励*/

@end
