//
//  PDBindingAreaListModel.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingAreaListModel.h"

@implementation PDBindingAreaListModel

- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"电信":@"dianxin",@"其他":@"qita",@"网通":@"wangtong"};
}

@end
