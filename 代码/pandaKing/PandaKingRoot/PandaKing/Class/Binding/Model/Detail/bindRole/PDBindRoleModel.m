//
//  PDBindRoleModel.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/7/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindRoleModel.h"

@implementation PDBindRoleModel
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"id":@"roleId"};
}

@end
