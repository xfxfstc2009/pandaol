//
//  PDBindRoleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 16/7/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchFileModel.h"

@interface PDBindRoleModel : FetchModel

@property (nonatomic,copy)NSString *activeLoseGold;         /**< 绑定成功后输了获得*/
@property (nonatomic,copy)NSString *activeWinGold;          /**< 绑定成功后赢了获得*/
@property (nonatomic,assign)BOOL enabled;                   /**< 是否可用*/
@property (nonatomic,copy)NSString *gameId;                 /** 游戏区服*/
@property (nonatomic,copy)NSString *gameUserId;             /**< 游戏用户id*/
@property (nonatomic,copy)NSString *memberId;               /**< 账号id*/

@property (nonatomic,copy)NSString *loseGold;               /**< 绑定成功后输了获得*/
@property (nonatomic,copy)NSString *winGold;                /**< 绑定成功后赢了获得*/


@property (nonatomic,assign)BOOL alreadybind;               /**< 是否被绑定*/

@end
