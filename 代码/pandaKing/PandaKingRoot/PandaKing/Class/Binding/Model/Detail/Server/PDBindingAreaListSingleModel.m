//
//  PDBindingAreaListSingleModel.m
//  PandaKing
//
//  Created by 裴烨烽 on 16/8/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBindingAreaListSingleModel.h"

@implementation PDBindingAreaListSingleModel

- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"电信":@"CT",@"网通":@"CM",@"其他":@"OTH"};
}

@end
