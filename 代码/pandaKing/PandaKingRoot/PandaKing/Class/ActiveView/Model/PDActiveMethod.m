//
//  PDActiveMethod.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDActiveMethod.h"

@implementation PDActiveMethod

+(instancetype)sharedActiveMethod{
    static PDActiveMethod *_sharedActiveMethod = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedActiveMethod = [[PDActiveMethod alloc] init];
    });
    return _sharedActiveMethod;
}

@end
