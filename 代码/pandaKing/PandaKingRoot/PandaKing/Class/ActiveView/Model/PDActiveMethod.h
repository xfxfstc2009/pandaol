//
//  PDActiveMethod.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

// 用来记录当前的数据
#import "FetchModel.h"

@interface PDActiveMethod : FetchModel

+(instancetype)sharedActiveMethod;

@end
