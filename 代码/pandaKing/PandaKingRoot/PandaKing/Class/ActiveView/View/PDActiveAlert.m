//
//  PDActiveAlert.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDActiveAlert.h"

@interface NSBundle (ios7Bundle)

@end

@implementation NSBundle(ios7Bundle)

+ (instancetype)ios7Bundle{
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSURL *bundleUrl = [mainBundle URLForResource:@"EHPlainAlert" withExtension:@"EHPlainAlert"];
    NSBundle *bundle = [NSBundle bundleWithURL:bundleUrl];
    return bundle;
}

+ (UIImage*)imageNamed:(NSString*)name{
    UIImage *image;
    
    image = [UIImage imageNamed:[NSString stringWithFormat:@"EHPlainAlert.bundle/%@",name]];
    if (image) {
        return image;
    }
    
    image = [UIImage imageWithContentsOfFile:[[[NSBundle ios7Bundle] resourcePath] stringByAppendingPathComponent:name]];
    
    return image;
}

@end


@interface PDActiveAlert()

@end


#pragma mark - PDActiveAlert
@implementation PDActiveAlert

#define max_numbers 1                       /**< 最大显示数量*/

static NSMutableArray * currentAlertArray = nil;
static NSMutableDictionary * _EHColorsDictionary = nil;
static NSMutableDictionary * _EHIconsDictionary = nil;
static NSInteger numberOfVisibleAlerts = max_numbers;                    /**< 最大显示条数*/
static CGFloat alertHeight = 60;                                            /**< 单个弹窗高度*/

+ (instancetype)showError:(NSError *)error {
    return [self showAlertWithTitle:@"Error" message:error.localizedDescription type:PDActiveAlertError];
}


+ (instancetype)showDomainError:(NSError *)error {
    return [self showAlertWithTitle:error.domain message:error.localizedDescription type:PDActiveAlertError];
}


+ (instancetype)showAlertWithTitle:(NSString *)title message:(NSString *)message type:(PDActiveType)type {
//    PDAlertView * alert = [[PDAlertView alloc] initWithTitle:title message:message type:type];
//    [alert show];
//    return alert;

    return nil;
}

- (id)initWithTitle:(NSString *)title message:(NSString *)message type:(PDActiveType)type {
    self = [super init];
    if (self) {
        self.titleString = title;
        self.subTitleString = message;
        if (!currentAlertArray) {
            currentAlertArray = [NSMutableArray array];
        }
//        [PDAlertView  updateColorsDictionary];
//        [PDAlertView  updateIconsDictionary];
        _alertType = type;
    }
    return self;
}

- (instancetype)init{
    self = [super init];
    if (self) {
        if (!currentAlertArray) {
            currentAlertArray = [NSMutableArray new];
        }
//        [PDAlertView updateColorsDictionary];
//        [PDAlertView  updateIconsDictionary];
    }
    return self;
}

+ (void)updateColorsDictionary {
    if (!_EHColorsDictionary) {
        _EHColorsDictionary = [@{ @(PDActiveAlertError) : UURed,
                                  @(PDActiveAlertInfo) : [UIColor colorWithRed:1 green:0 blue:0 alpha:.1f],
                                  @(PDActiveAlertPanic) :  UUBlue,
                                  @(PDActiveAlertSuccess) :UUYellow,
                                  } mutableCopy];
    }
}


+ (void)updateIconsDictionary
{

}

+ (UIImage *)imageNamed:(NSString *)name {
    if ([Tool ios_version] < 8) {
        return [NSBundle imageNamed:name];
    } else {
        NSBundle * pbundle = [NSBundle bundleForClass:[self class]];
        NSString *bundleURL = [pbundle pathForResource:@"EHPlainAlert" ofType:@"bundle"];
        NSBundle *imagesBundle = [NSBundle bundleWithPath:bundleURL];
        UIImage * image = [UIImage imageNamed:name inBundle:imagesBundle compatibleWithTraitCollection:nil];
        return image;
    }
}




-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
    [self addTapGeture];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = [UIColor clearColor];
    self.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, alertHeight);
    self.view.layer.masksToBounds = NO;
}

#pragma mark - createView
-(void)createView{
    UIView *infoView = [[UIView alloc]init];
    infoView.backgroundColor = [UIColor clearColor];
    infoView.frame = CGRectMake(0, 0, kScreenBounds.size.width, alertHeight);
    [self.view addSubview:infoView];
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.numberOfLines = 0;
    [infoView addSubview:titleLabel];
    

    infoView.backgroundColor = UURandomColor;
}

-(void)addTapGeture{
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onTap)];
    [self.view addGestureRecognizer:tapGesture];
}

+(instancetype)showAlertWithTitle:(NSString *)title message:(NSString *)message subView:(UIView *)subView{
    PDActiveAlert *alert = [[PDActiveAlert alloc]initWithTitle:title message:message];
    [alert showInMain:subView];
    return alert;
}

- (id)initWithTitle:(NSString *)title message:(NSString *)message {
    self = [super init];
    if (self)
    {
        self.titleString = title;
        self.subTitleString = message;
        if (!currentAlertArray) {
            currentAlertArray = [NSMutableArray new];
        }
//        [EHPlainAlert  updateColorsDictionary];
//        [EHPlainAlert  updateIconsDictionary];
    }
    return self;
}




#pragma mark - 显示方法
-(void)showWithView:(UIView *)view{
    [self performSelectorOnMainThread:@selector(showInMain:) withObject:view waitUntilDone:NO];
}

-(void)dismissAnimation{
    
}

-(void)showInMain:(UIView *)view{
    @synchronized(currentAlertArray) {
        if (currentAlertArray.count >= numberOfVisibleAlerts){
            [[currentAlertArray firstObject] hide:@(YES)];
        }
        NSInteger numberOfAlerts = currentAlertArray.count;
        if (numberOfAlerts == 0){
            [view addSubview:self.view];
        } else {
            [view insertSubview:self.view belowSubview:[((PDActiveAlert *)[currentAlertArray lastObject]) view]];
        }
        [UIView animateWithDuration:0.3 animations:^{
            self.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, alertHeight);
        }];
        
        [currentAlertArray addObject:self];
    }
}

#pragma mark - 隐藏方法
- (void)hide:(NSNumber *)nAnimated {
    [self performSelectorOnMainThread:@selector(hideInMain:) withObject:nAnimated waitUntilDone:NO];
}

-(void)hideInMain:(NSNumber *)nAnimated{
    @synchronized(currentAlertArray) {
        [currentAlertArray removeObject:self];
        BOOL animated = [nAnimated boolValue];
        if (animated){
            [UIView animateWithDuration:1. animations:^{
                self.view.alpha = 0.5;
                self.view.frame = CGRectMake(0, 0,kScreenBounds.size.width , alertHeight);
            } completion:^(BOOL finished) {
                [self.view removeFromSuperview];
            }];
            
            for (int i = 0 ; i < currentAlertArray.count;i++){
                PDActiveAlert *alert = [currentAlertArray objectAtIndex:i];
     
                [UIView animateWithDuration:.5f animations:^{
                    alert.view.frame = CGRectMake(0, alertHeight * (i + 1) - 0.5 * (i), kScreenBounds.size.width, alertHeight);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:.5f animations:^{
                        alert.view.frame = CGRectMake(0, alertHeight * (i) - 0.5 * (i), kScreenBounds.size.width, alertHeight);
                    }];
                }];
            }
        } else {
            [self.view removeFromSuperview];
            for (int i = 0; i < [currentAlertArray count]; i++) {
                PDActiveAlert * alert = [currentAlertArray objectAtIndex:i];
                alert.view.frame = CGRectMake(0,kScreenBounds.size.height - alertHeight * (i + 1) - 0.5 * (i), kScreenBounds.size.width, alertHeight);
            }
        }
    }
}


- (void)hide {
    [self hide:@(YES)];
}


#pragma mark - Update Manager
#pragma mark - Setters
- (void)setIconImage:(UIImage *)iconImage {
    _iconImage = iconImage;
}

#pragma mark - Default behaviour

+ (void)updateNumberOfAlerts:(NSInteger)numberOfAlerts {
    if (numberOfAlerts > 0) {
        numberOfVisibleAlerts = numberOfAlerts;
    }
}


#pragma mark - onTap
-(void)onTap{
    if (_actionBlock){
        _actionBlock();
    }
}

@end
