//
//  PDActiveAlert.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

typedef void(^actionBlock)();

typedef NS_ENUM(NSInteger,PDActiveType) {
    PDActiveAlertError,
    PDActiveAlertSuccess,
    PDActiveAlertInfo,
    PDActiveAlertPanic,
    PDActiveAlertUnknown,
};

@interface PDActiveAlert : AbstractViewController

@property (nonatomic,copy)actionBlock actionBlock;          /**< 手势点击方法*/
@property (nonatomic,strong)UIFont *titleFont;               /**< 文字的font*/
@property (nonatomic,strong)UIFont *subTitleFont;            /**< subTitle*/
@property (nonatomic,strong)UIColor *messageColor;           /**< 消息颜色*/
@property (nonatomic,strong)UIImage *iconImage;             /**< 图片*/
@property (nonatomic,copy)NSString *titleString;            /**< 标题string*/
@property (nonatomic,copy)NSString *subTitleString;         /**< 副标题string*/
@property (nonatomic,assign)PDActiveType alertType;         /**< 弹框的类型*/


+(instancetype)showAlertWithTitle:(NSString *)title message:(NSString *)message subView:(UIView *)subView;        /**< 显示现在的标题内容*/

@end
