//
//  PDPostPriseListViewController.h
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 点赞列表
@interface PDPostPriseListViewController : AbstractViewController
@property (nonatomic) NSInteger postId;
@end
