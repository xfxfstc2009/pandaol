//
//  PDPostHotestViewController.h
//  PandaKing
//
//  Created by Cranz on 17/6/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 最热野区
@interface PDPostHotestViewController : UIViewController
/**
 * 更新到最新
 */
- (void)updateToLastest;
@end
