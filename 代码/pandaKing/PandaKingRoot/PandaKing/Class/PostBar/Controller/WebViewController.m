//
//  WebViewController.m
//  Zhuandao
//
//  Created by 裴烨烽 on 2017/1/1.
//  Copyright © 2017年 GiganticWhale. All rights reserved.
//

#import "WebViewController.h"
//导入Keychain依赖库
#import <Security/Security.h>
#import "PDPostWebViewController.h"

#define kOriginalScrollViewWithWeb     @"kOriginalScrollViewWithWeb"
#define kOriginalImageViewWithWeb      @"kOriginalImageViewWithWeb"
#define kOriginalMaskImageViewWithWeb  @"kOriginalMaskImageViewWithWeb"
#define kOriginalNaviBarWithWeb        @"kOriginalNaviBarWithWeb"
#define kSelectionLabKeyWithWeb        @"kSelectionLabKeyWithWeb"
#define kSelectionImageKeyWithWeb      @"kSelectionImageKeyWithWeb"

@interface WebViewController()<UIWebViewDelegate,UIScrollViewDelegate>
@property (nonatomic,strong,readwrite)UIWebView *h5WebView;
@property (nonatomic, strong, readwrite) JSContext *context;
@property (nonatomic,strong)UIButton *downButton;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UIPageControl *pageControl;
@property (nonatomic,strong)UIView *topView;
@end

@implementation WebViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createWebView];
}

#pragma mark - createWebView
-(void)createWebView{
    if (!self.h5WebView) {
        self.h5WebView = [[UIWebView alloc] initWithFrame:self.view.bounds];
        self.h5WebView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
        self.h5WebView.delegate = self;
        self.h5WebView.backgroundColor = [UIColor whiteColor];
        self.h5WebView.scalesPageToFit = YES;
        [self.h5WebView.scrollView setAlwaysBounceVertical:YES];
        self.h5WebView.scrollView.bounces = NO;
        
        UIDataDetectorTypes dataDetectorTypes = UIDataDetectorTypeLink;
        self.h5WebView.dataDetectorTypes = dataDetectorTypes;
        
        NSString *gameType = @"";
        if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
            gameType = @"lol";
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
            gameType = @"dota2";
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
            gameType = @"king";
        }

        NSString *info = @"";
        if (self.transferYequType == YequTypeALL){
            info = [NSString stringWithFormat:@"http://%@:%@%@?token=%@&gameType=%@",Jungle_Host,Jungle_Port,postList, [Tool userDefaultGetWithKey:CustomerToken],gameType];
        } else if (self.transferYequType == YequTypeHe){
            info = [NSString stringWithFormat:@"http://%@:%@%@?token=%@&gameType=%@&memberId=%@",Jungle_Host,Jungle_Port,postJungle, [Tool userDefaultGetWithKey:CustomerToken],gameType,self.transferMemberId];
            self.barMainTitle = @"TA的野区";
        } else if (self.transferYequType == YequTypeMine){
            info = [NSString stringWithFormat:@"http://%@:%@%@?token=%@&gameType=%@&memberId=%@",Jungle_Host,Jungle_Port,postJungle, [Tool userDefaultGetWithKey:CustomerToken],gameType,self.transferMemberId];
            self.barMainTitle = @"我的野区";
        }

        
        NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:info]];
            urlRequest.timeoutInterval = 20.;
            urlRequest.HTTPShouldHandleCookies = YES;
            urlRequest.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
            [self.h5WebView loadRequest:urlRequest];
    }
    [self.view addSubview:self.h5WebView];
    
}

- (void)requestPostList {
    NSString *gameType = @"";
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        gameType = @"lol";
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        gameType = @"dota2";
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        gameType = @"king";
    }
    
    NSString *info = [NSString stringWithFormat:@"http://%@:%@%@?token=%@&gameType=%@",Jungle_Host,Jungle_Port,postList, [Tool userDefaultGetWithKey:CustomerToken],gameType];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:info]];
    urlRequest.timeoutInterval = 20.;
    urlRequest.HTTPShouldHandleCookies = YES;
    urlRequest.cachePolicy = NSURLRequestReloadIgnoringLocalAndRemoteCacheData;
    [self.h5WebView loadRequest:urlRequest];
}


#pragma mark -UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self actionDidFinishLoad];
}


#pragma mark -actionDidFinishLoad
- (void)actionDidFinishLoad {

    JSContext *context  = [self.h5WebView valueForKeyPath:@"documentView.webView.mainFrame.javaScriptContext"];
    self.context = context;
    context[@"imgScroll"] = ^(id obj,NSInteger index){
        if ([obj isKindOfClass:[NSString class]]){
            NSString *imgUrl = (NSString *)obj;
            NSArray *imgUrlArr = [imgUrl componentsSeparatedByString:@","];
            [self showBigImageManagerWithSelectedUrlArr:imgUrlArr index:index];
        }
    };
    
    __weak typeof(self) weakSelf = self;
    self.context[@"initDetail"] = ^(NSString *postId, BOOL me, NSString *token){
        PDPostWebViewController *webViewController = [[PDPostWebViewController alloc] init];
        webViewController.isMe = me;
        webViewController.postId = postId;
        webViewController.transferUrl = [NSString stringWithFormat:@"http://%@:%@%@?postId=%@&me=%d&token=%@",Jungle_Host,Jungle_Port,postDetail,postId,me,token];
        [weakSelf pushViewController:webViewController animated:YES];
        
        [weakSelf deletePostWithController:webViewController];
    };
    
    
    NSString *jsManager = [NSString stringWithFormat:@"window.wildarea.imgScroll()"];
    [context evaluateScript:jsManager];
}

- (void)deletePostWithController:(PDPostWebViewController *)webViewController {
    __weak typeof(self) weakSelf = self;
    [webViewController didDeletePostBlock:^{
        [weakSelf.h5WebView reload];
    }];
}

#pragma mark - 图片放大
-(void)showBigImageManagerWithSelectedUrlArr:(NSArray *)urlArr index:(NSInteger)index{
    if (!self.topView){
        self.topView = [[UIView alloc]init];
    }
    self.topView.frame = CGRectMake(kScreenBounds.size.width / 2., kScreenBounds.size.height / 2., 0, 0);
    self.topView.alpha = 0;
    self.topView.backgroundColor = [UIColor blackColor];
    [self.view.window addSubview:self.topView];
    
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
    }
    self.mainScrollView.backgroundColor = [UIColor blackColor];
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    self.mainScrollView.showsVerticalScrollIndicator = NO;
    self.mainScrollView.layer.zPosition = MAXFLOAT;
    self.mainScrollView.delegate = self;
    self.mainScrollView.userInteractionEnabled = YES;
    self.mainScrollView.maximumZoomScale=2.0;
    self.mainScrollView.minimumZoomScale=0.5;
    self.mainScrollView.bounces = YES;
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.bouncesZoom = YES;
    [self.topView addSubview:self.mainScrollView];

    // 2. 创建pagecontrol
    self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, kScreenBounds.size.height - LCFloat(11) * 3,kScreenBounds.size.width,LCFloat(11))];
    self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
    self.pageControl.numberOfPages = urlArr.count;
    self.pageControl.currentPage = index;
    [self.topView.window addSubview:self.pageControl];
    
    UITapGestureRecognizer * tapForHideKeyBoard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDismissOriginalPhoto)];
    [self.mainScrollView addGestureRecognizer:tapForHideKeyBoard];
    
//    // 1. 创建多张图片
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < urlArr.count;i++){
        PDImageView *originalImageView = [[PDImageView alloc] init];
        originalImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.mainScrollView addSubview:originalImageView];
        NSString *imgUrl = [urlArr objectAtIndex:i];
        [originalImageView uploadImageWithURL:imgUrl placeholder:nil callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            UIImage *originalImage = [UIImage scaleDown:image withSize:CGSizeMake(strongSelf.view.size_width, strongSelf.view.size_width * image.size.height/image.size.width)];
            originalImageView.image = originalImage;
            
            // 设置比例
            CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
            CGFloat screenRadio = CGRectGetWidth(strongSelf.view.window.frame)/CGRectGetHeight(strongSelf.view.window.frame);
            if (imageRadio >= screenRadio) {
                CGFloat currentImageHeight = CGRectGetWidth(strongSelf.view.window.frame)/imageRadio;
                self.mainScrollView.maximumZoomScale = CGRectGetHeight(strongSelf.view.window.frame)/currentImageHeight;
            } else {
                CGFloat currentImageWidth = CGRectGetWidth(strongSelf.view.window.frame)*imageRadio;
                self.mainScrollView.maximumZoomScale = CGRectGetWidth(strongSelf.view.window.frame)/currentImageWidth;
            }
        }];
        originalImageView.frame =   kScreenBounds;
        originalImageView.orgin_x = kScreenBounds.size.width * i;
    }
    

    // 3. 创建下载按钮
    if (!self.downButton){
        self.downButton = [UIButton buttonWithType:UIButtonTypeCustom];
    }
    self.downButton.frame = CGRectMake(LCFloat(11), kScreenBounds.size.height - LCFloat(11) - LCFloat(40), LCFloat(40), LCFloat(40));
    [self.topView.window addSubview:self.downButton];
    self.downButton.backgroundColor = [UIColor clearColor];
    [self.downButton setImage:[UIImage imageNamed:@"icon_postBar_down"] forState:UIControlStateNormal];
    [self.downButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger currentIndex = self.mainScrollView.contentOffset.x / kScreenBounds.size.width;
        NSString *url = [urlArr objectAtIndex:currentIndex];
        [strongSelf getNetImgSaveToCurrentWithUrl:url];
    }];
    
    // 开启动画
    [UIView animateWithDuration:.5f delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.topView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height);
        self.topView.alpha = 1;
    } completion:^(BOOL finished) {
        self.mainScrollView.frame = kScreenBounds;
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * urlArr.count, kScreenBounds.size.height);
        //    // 2. scrollview移动到位置
        [self.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * index, 0)];
    }];
}

-(void)actionDismissOriginalPhoto{
    [UIView animateWithDuration:.5 animations:^{
        self.topView.frame = CGRectMake(kScreenBounds.size.width / 2., kScreenBounds.size.height / 2., 0, 0);
        self.topView.alpha = 0.;
    } completion:^(BOOL finished) {
        [self.topView removeFromSuperview];
        [self.mainScrollView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        [self.mainScrollView removeFromSuperview];
        [self.downButton removeFromSuperview];
        [self.pageControl removeFromSuperview];
        self.mainScrollView = nil;
        self.pageControl = nil;
        self.topView = nil;
        self.downButton = nil;
    }];
}

// 缩放
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollViewWithWeb];
    UIImageView *originalImageView = (UIImageView *)[originalScrollView viewWithStringTag:kOriginalImageViewWithWeb];
    return originalImageView;
}

#pragma mark - 获取网络图片保存到本地
-(void)getNetImgSaveToCurrentWithUrl:(NSString *)urlString{
    NSURL * url = [NSURL URLWithString:urlString];
    NSData * data = [[NSData alloc]initWithContentsOfURL:url];
    UIImage *image = [[UIImage alloc]initWithData:data];
    [self saveImageToPhotos:image];
}

- (void)saveImageToPhotos:(UIImage*)savedImage {
    UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}

- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo {
       [[UIAlertView alertViewWithTitle:@"保存成功" message:nil
                           buttonTitles:@[@"确定"] callBlock:NULL]show];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.mainScrollView){
        NSInteger index = self.mainScrollView.contentOffset.x / kScreenBounds.size.width;
        self.pageControl.currentPage = index;
    }
}

@end
