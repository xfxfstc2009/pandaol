//
//  PDPostRootViewController.h
//  PandaKing
//
//  Created by Cranz on 17/5/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
/// 野区基类控制器
@interface PDPostRootViewController : AbstractViewController
+ (instancetype)sharedController;

/**
 * 在切换游戏的时候去更新贴吧信息
 */
- (void)updatePosts;
@end
