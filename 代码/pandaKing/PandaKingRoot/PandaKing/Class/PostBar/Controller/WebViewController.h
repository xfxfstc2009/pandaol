//
//  WebViewController.h
//  Zhuandao
//
//  Created by 裴烨烽 on 2017/1/1.
//  Copyright © 2017年 GiganticWhale. All rights reserved.
//

#import "AbstractViewController.h"
#import <JavaScriptCore/JavaScriptCore.h>

typedef NS_ENUM(NSInteger,YequType) {
    YequTypeALL,
    YequTypeMine,
    YequTypeHe,
};


@interface WebViewController : AbstractViewController
@property (nonatomic,strong,readonly)UIWebView *h5WebView;
@property (nonatomic, strong, readonly) JSContext *context;

- (void)webViewDidFinishLoad:(UIWebView *)webView;

@property (nonatomic,assign)YequType transferYequType;
@property (nonatomic,copy)NSString *transferMemberId;

// 刷新野区列表
- (void)requestPostList;

@end
