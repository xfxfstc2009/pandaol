//
//  PDPostWebViewController.m
//  PandaKing
//
//  Created by Cranz on 17/5/17.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostWebViewController.h"
#import "PDShareSheetView.h"
#import "PDOAuthLoginAndShareTool.h"
#import "PDPostReportList.h"
// 弹框
#import "PDAlertObjView.h"
#import "PDPostReportView.h"

@interface PDPostWebViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, copy) NSString *postTitle;
@property (nonatomic, copy) NSString *fTitle;
@property (nonatomic, copy) NSString *image;

@property (nonatomic, copy) void(^deleteBlock)();
@end

@implementation PDPostWebViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"postShareNotification" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barMainTitle = @"详情";
    
    self.webView = [[UIWebView alloc] initWithFrame:self.view.bounds];
    self.webView.delegate = self;
    self.webView.scalesPageToFit = YES;
    self.webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.webView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.webView];
    
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:[NSURL URLWithString:self.transferUrl]];
    [self.webView loadRequest:request];
    
    // 添加通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveSharedInfo:) name:@"postShareNotification" object:nil];
    
    __weak typeof(self) weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_post_more"] barHltImage:nil action:^{
        [weakSelf showShareAlert];
    }];
    
    [self fetchShareInfo];
}

- (void)showShareAlert {
    __weak typeof(self) weakSelf = self;
    
    NSArray *itemArr,*imageArr;
    if (self.isMe) {
        itemArr = @[@"QQ好友",@"微信好友",@"QQ空间",@"朋友圈",@"删除"];
        imageArr = @[@"icon_share_qq",@"icon_share_wx",@"icon_share_qqzone",@"icon_share_wxzone",@"icon_share_delete"];
    } else {
        itemArr = @[@"QQ好友",@"微信好友",@"QQ空间",@"朋友圈",@"举报"];
        imageArr = @[@"icon_share_qq",@"icon_share_wx",@"icon_share_qqzone",@"icon_share_wxzone",@"icon_share_report"];
    }
    
    [PDShareSheetView shareWithItemArr:itemArr imageArr:imageArr clickComplication:^(NSString *item) {
        if ([item isEqualToString:@"QQ好友"]) {
            if ([TencentOAuth iphoneQQInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeQQFriend];
            } else {
                [PDHUD showHUDError:@"您未安装QQ手机客户端！"];
            }
        } else if ([item isEqualToString:@"微信好友"]) {
            if ([WXApi isWXAppInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeWXSession];
            } else {
                [PDHUD showHUDError:@"您未安装微信手机客户端！"];
            }
        } else if ([item isEqualToString:@"QQ空间"]) {
            if ([TencentOAuth iphoneQQInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeQQZone];
            } else {
                [PDHUD showHUDError:@"您未安装QQ手机客户端！"];
            }
        } else if ([item isEqualToString:@"朋友圈"]) {
            if ([WXApi isWXAppInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeWXTimeLine];
            } else {
                [PDHUD showHUDError:@"您未安装微信手机客户端！"];
            }
        } else if ([item isEqualToString:@"举报"]) {
            [weakSelf fetchRepostList];
        } else if ([item isEqualToString:@"删除"]) {
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"提示" conten:@"你确定要删除该帖子吗？" isClose:NO btnArr:@[@"确定",@"取消"] buttonClick:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [weakSelf fetchDeletePost];
                }
                [JCAlertView dismissAllCompletion:nil];
            }];
        }
    }];
}

- (void)shareWithType:(PD3RShareType)type {
    __weak typeof(self) weakSelf = self;
    PDImageView *iv = [[PDImageView alloc] init];
    NSString *picPath = self.image;
    [iv uploadImageWithAvatarURL:picPath placeholder:nil callback:^(UIImage *image) {
        NSData *imgData = [PDCenterTool compressImage:image];
        if (imgData.length >= 32 * 1024) { // 判断字节数
            imgData = [PDCenterTool compressImage:image];
        }
        [PDOAuthLoginAndShareTool sharedWithType:type text:nil title:weakSelf.postTitle desc:weakSelf.fTitle thubImageData:imgData webUrl:[NSString stringWithFormat:@"http://%@:%@%@?postId=%@",Jungle_Host,Jungle_Port,postDetail,weakSelf.postId]];
    }];
}


- (void)showReportSheetViewWithReportArr:(NSArray *)reportArr {
    __weak typeof(self) weakSelf = self;
    [PDShareSheetView showSheetWithTitle:@"请选择举报原因" selContentArr:reportArr clickComplication:^(NSUInteger index, NSString *item) {
        if ([item containsString:@"其他"]) {
            [weakSelf showReportEditView];
        } else {
            [weakSelf fetchReportWithReason:item];
        }
    }];
}

/**
 * 弹出举报输入框
 */
- (void)showReportEditView {
    __weak typeof(self) weakSelf = self;
    PDPostReportView *reportView = [[PDPostReportView alloc] initWithTitle:@"填报举报理由"];
    PDAlertObjView *alert = [PDAlertObjView alertWithCustomView:reportView];
    [alert showAlertComplication:nil];
    [reportView didClickCancleActions:^{
        [alert dismissAlertComplication:nil];
    }];
    [reportView didClickSubmitActions:^(NSString *text) {
        [alert dismissAlertComplication:nil];
        
        [weakSelf fetchReportWithReason:text];
    }];
    
    [alert addKeyboardNotification];
}

- (void)didDeletePostBlock:(void (^)())deleteBlock {
    if (deleteBlock) {
        self.deleteBlock = deleteBlock;
    }
}

#pragma mark - 网络请求

/**
 * 获取举报列表
 */
- (void)fetchRepostList {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postReportList] requestParams:nil responseObjectClass:[PDPostReportList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDPostReportList *list = (PDPostReportList *)responseObject;
            NSMutableArray *itemsArr = [NSMutableArray arrayWithCapacity:5];
            for (PDPostReportItem *item in list.items) {
                [itemsArr addObject:item.reportReason];
            }
            [weakSelf showReportSheetViewWithReportArr:[itemsArr copy]];
        }
    }];
}

/**
 * 删除帖子
 */
- (void)fetchDeletePost {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postDelete] requestParams:@{@"postId":self.postId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) return;
        if (isSucceeded) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
            if (weakSelf.deleteBlock) {
                weakSelf.deleteBlock();
            }
        }
    }];
}

/**
 * 举报
 */
- (void)fetchReportWithReason:(NSString *)reason {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postReport] requestParams:@{@"postId":self.postId,@"reportReason":reason} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            [PDHUD showHUDSuccess:@"举报成功！"];
        }
    }];
}

/**
 * 获取分享的信息
 */
- (void)fetchShareInfo {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postShareInfo] requestParams:@{@"postId":self.postId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.postTitle = responseObject[@"title"];
            weakSelf.fTitle = responseObject[@"content"];
            weakSelf.image = responseObject[@"img"];
        }
    }];
}

/**
 * 告诉服务器分享结果
 */
- (void)didReceiveSharedInfo:(NSNotification *)noti {
    NSDictionary *userInfo = noti.userInfo;
    if ([userInfo[@"result"] isEqualToString:@"success"]) {
        // 发送
        [[NetworkAdapter sharedAdapter] fetchWithPath:centerTaskShareSuccess requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
            if (isSucceeded) {
                [PDHUD showHUDSuccess:@"分享成功！"];
            }
        }];
    }
}

@end
