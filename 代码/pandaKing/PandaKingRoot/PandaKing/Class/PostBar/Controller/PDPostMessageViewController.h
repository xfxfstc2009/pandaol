//
//  PDPostMessageViewController.h
//  PandaKing
//
//  Created by Cranz on 17/5/15.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 发帖控制器
@interface PDPostMessageViewController : AbstractViewController
/**
 * 发帖成功
 */
- (void)postMessageSuccess:(void(^)())successBlock;
@end
