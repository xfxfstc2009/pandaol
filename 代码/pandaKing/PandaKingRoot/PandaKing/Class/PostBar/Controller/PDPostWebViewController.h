//
//  PDPostWebViewController.h
//  PandaKing
//
//  Created by Cranz on 17/5/17.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 野区详情web页
@interface PDPostWebViewController : AbstractViewController
/**
 * 是否是我自己；1:我，0:别人
 */
@property (nonatomic, assign) BOOL isMe;
/**
 * 传递过来的帖子id
 */
@property (nonatomic, copy) NSString *postId;
@property (nonatomic, copy) NSString *transferUrl;

- (void)didDeletePostBlock:(void(^)())deleteBlock;

@end
