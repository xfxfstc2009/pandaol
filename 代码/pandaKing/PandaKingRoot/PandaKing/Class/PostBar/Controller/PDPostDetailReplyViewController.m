//
//  PDPostDetailReplyViewController.m
//  PandaKing
//
//  Created by Cranz on 17/6/12.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailReplyViewController.h"
#import "PDPostDetailReplyFloorHostCell.h"
#import "PDPostDetailReplyFloorCell.h"
#import "PDShareSheetView.h"
#import "PDPostReplyUserView.h"
#import "PDPostReplyHostView.h"
#import "PDPostKeyboardTempView.h"
#import "PDPostReplyButton.h"
#import "PDPostReportList.h"
#import "PDPostReportView.h"
#import "PDAlertObjView.h"
#import "PDCenterPersonViewController.h"

@interface PDPostDetailReplyViewController ()<UITableViewDataSource, UITableViewDelegate, PDPostDetailReplyFloorHostCellDelegate, PDPostDetailReplyFloorCellDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, strong) PDPostKeyboardTempView *tempView;
@property (nonatomic, strong) PDPostReplyButton *replyButton;
@property (nonatomic, strong) UITextField *replyField;
@property (nonatomic, copy) void(^deleteBlock)(PDPostDetailReplyModel *);
@property (nonatomic, copy) void(^updateBlock)(PDPostDetailReplyModel *);
@property (nonatomic) BOOL needUpdate;
@end

@implementation PDPostDetailReplyViewController

- (PDPostKeyboardTempView *)tempView {
    if (!_tempView) {
        _tempView = [[PDPostKeyboardTempView alloc] init];
    }
    return _tempView;
}

- (NSMutableArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.needUpdate = NO;
    self.barMainTitle = [NSString stringWithFormat:@"第%@楼",@(self.model.floor)];
    [self.itemsArr addObjectsFromArray:self.model.replys];
    [self setupReplyButton];
    [self setupTableView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (self.updateBlock && self.needUpdate == YES) {
        self.updateBlock(self.model);
    }
}

- (void)setupReplyButton {
    UIView *replyView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenBounds.size.height - 64 - 44, kScreenBounds.size.width, 44)];
    replyView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:replyView];
    UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(replyView.frame), 1)];
    topLine.backgroundColor = c27;
    [replyView addSubview:topLine];
    
    self.replyButton = [[PDPostReplyButton alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 6, CGRectGetWidth(replyView.frame) - kTableViewSectionHeader_left * 2, CGRectGetHeight(replyView.frame) - 12)];
    self.replyButton.text = @"回复楼主";
    self.replyButton.layer.cornerRadius = 2.5;
    self.replyButton.layer.borderColor = c27.CGColor;
    self.replyButton.layer.borderWidth = 1;
    [self.replyButton addTarget:self action:@selector(didClickReply:) forControlEvents:UIControlEventTouchUpInside];
    [replyView addSubview:self.replyButton];
    
    self.replyField = [[UITextField alloc] initWithFrame:self.replyButton.bounds];
    self.replyField.hidden = YES;
    [self.view insertSubview:self.replyField belowSubview:self.replyButton];
}

- (void)setupTableView {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 44)];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.separatorColor = c27;
    self.mainTableView.tableFooterView = [UIView new];
    [self.view addSubview:self.mainTableView];
}

- (void)didClickReply:(UIButton *)button {
    [self showReplyUserKeyboardWithModel:self.model];
}

#pragma mark - UITableView datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsArr.count + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        PDPostDetailReplyFloorHostCell *hostCell = [tableView dequeueReusableCellWithIdentifier:@"hostCellId"];
        if (!hostCell) {
            hostCell = [[PDPostDetailReplyFloorHostCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"hostCellId"];
            hostCell.delegate = self;
        }
        hostCell.model = self.model;
        return hostCell;
    } else {
        PDPostDetailReplyFloorCell *floorCell = [tableView dequeueReusableCellWithIdentifier:@"floorCellId"];
        if (!floorCell) {
            floorCell = [[PDPostDetailReplyFloorCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"floorCellId"];
            floorCell.delegate = self;
        }
        floorCell.item = self.itemsArr[indexPath.row - 1];
        return floorCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [PDPostDetailReplyFloorHostCell cellHeightWithModel:self.model];
    } else {
        return [PDPostDetailReplyFloorCell cellHeightWithItem:self.itemsArr[indexPath.row - 1]];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        [PDCenterTool calculateCell:cell leftSpace:kTableViewSectionHeader_left rightSpace:kTableViewSectionHeader_left];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != 0) {
        PDPostDetailReplyItem *item = self.itemsArr[indexPath.row - 1];
        [self showReplyUserKeyboardWithSubReplyItem:item];
    }
}

#pragma mark - PDPostDetailReplyFloorHostCellDelegate

- (void)hostCell:(PDPostDetailReplyFloorHostCell *)cell didClickPriseButton:(UIButton *)button {
    [self fetchPraise:!button.selected postId:cell.model.postId praiseButton:button];
}

- (void)hostCell:(PDPostDetailReplyFloorHostCell *)cell didClickMoreButton:(UIButton *)button {
    __weak typeof(self) weakSelf = self;
    if (cell.model.me) {
        [PDShareSheetView showSheetWithTitle:nil selContentArr:@[@"删除评论"] clickComplication:^(NSUInteger index, NSString *item) {
            if (index == 0) {
                [weakSelf fetchDeleteReplyWithReplyModel:cell.model];
            }
        }];
    } else {
        [PDShareSheetView showSheetWithTitle:nil selContentArr:@[@"举报评论"] clickComplication:^(NSUInteger index, NSString *item) {
            if (index == 0) {
                [weakSelf fetchRepostListWithPostId:cell.model.postId];
            }
        }];
    }
}

- (void)hostCell:(PDPostDetailReplyFloorHostCell *)cell didClickImageView:(PDImageView *)imageView {
    
}

#pragma mark - PDPostDetailReplyFloorCellDelegate

- (void)floorCell:(PDPostDetailReplyFloorCell *)cell didClickReplyUserNameWithUserId:(NSString *)userId {
    // 跳个人详情
    PDCenterPersonViewController *personViewController = [[PDCenterPersonViewController alloc] init];
    personViewController.transferMemberId = userId;
    personViewController.isMe = [[Tool userDefaultGetWithKey:CustomerMemberId] isEqualToString:userId];
    [self pushViewController:personViewController animated:YES];
}

#pragma mark - 设置不同的回复键盘小窗口
/**
 * 回复一楼评论
 */
- (void)showReplyUserKeyboardWithModel:(PDPostDetailReplyModel *)model {
    PDPostReplyUserView *replyView = [[PDPostReplyUserView alloc] init];
    self.replyField.inputAccessoryView = replyView;
    replyView.model = model;
    
    __weak typeof(self) weakSelf = self;
    [replyView postView:^(NSString *text, PDPostDetailReplyModel *model) {
        [PDHUD showHUDProgress:@"正在发表您的评论,请稍后..." diary:0];
        [weakSelf fetchReplyCommentWithContent:text postId:model.postId parentId:model.parentId replyUserName:model.postUserName replyId:model.replyId];
        [weakSelf.replyField resignFirstResponder];
        [weakSelf.tempView dismissTempView];
    }];
    
    [self.tempView showTempView];
    [self.tempView didTouchAtTempView:^{
        [replyView.textView resignFirstResponder];
        [weakSelf.replyField resignFirstResponder];
        [weakSelf.tempView dismissTempView];
    }];
    
    [self.replyField becomeFirstResponder];
    [replyView.textView becomeFirstResponder];
}

/**
 * 回复楼中楼评论
 */
- (void)showReplyUserKeyboardWithSubReplyItem:(PDPostDetailReplyItem *)item {
    PDPostReplyUserView *replyView = [[PDPostReplyUserView alloc] init];
    self.replyField.inputAccessoryView = replyView;
    replyView.item = item;
    
    __weak typeof(self) weakSelf = self;
    [replyView postViewReplyWithItemBlock:^(NSString *text, PDPostDetailReplyItem *item) {
        [PDHUD showHUDProgress:@"正在发表您的评论,请稍后..." diary:0];
        [weakSelf fetchReplyCommentWithContent:text postId:item.postId parentId:item.parentId replyUserName:item.userName replyId:item.replyId];
        [weakSelf.replyField resignFirstResponder];
        [weakSelf.tempView dismissTempView];
    }];
    
    [self.tempView showTempView];
    [self.tempView didTouchAtTempView:^{
        [replyView.textView resignFirstResponder];
        [weakSelf.replyField resignFirstResponder];
        [weakSelf.tempView dismissTempView];
    }];
    
    [self.replyField becomeFirstResponder];
    [replyView.textView becomeFirstResponder];
}


#pragma mark - 网络请求

// praise:YES 点赞  NO 取消
- (void)fetchPraise:(BOOL)praise postId:(NSInteger)postId praiseButton:(UIButton *)button {
    __weak typeof(self) weakSelf = self;
    NSDictionary *param;
    if (praise) {
        param = @{@"postId":@(postId),@"name":@"zan"};
    } else {
        param = @{@"postId":@(postId),@"name":@""};
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postPraise] requestParams:param responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            button.selected = !button.selected;
            
            weakSelf.model.good = button.selected;
            if (weakSelf.model.good) {
                weakSelf.model.postGood++;
            } else {
                weakSelf.model.postGood--;
            }
            
            NSUInteger priseCount = button.tag;
            if (praise == YES) {
                ++priseCount;
            } else {
                --priseCount;
            }
            button.tag = priseCount;
            if ([button.titleLabel.text containsString:@"赞"]) {
                [button setTitle:[NSString stringWithFormat:@"%@人觉得赞",[PDCenterTool numberStringChangeWithoutSpecialCharacter:priseCount]] forState:UIControlStateNormal];
            } else {
                [button setTitle:[NSString stringWithFormat:@"%@",[PDCenterTool numberStringChangeWithoutSpecialCharacter:priseCount]] forState:UIControlStateNormal];
            }
            [weakSelf updateButton:button];
        }
    }];
}

- (void)updateButton:(UIButton *)button {
    CGSize praiseImageSize = button.imageView.image.size;
    CGSize praiseTextSize = [button.titleLabel.text sizeWithCalcFont:button.titleLabel.font];
    CGSize praiseSize = CGSizeMake(praiseImageSize.width + 10 + praiseTextSize.width, praiseTextSize.height + 10);
    button.frame = CGRectMake(button.frame.origin.x, button.frame.origin.y, praiseSize.width, praiseSize.height);
}

// 回复
- (void)fetchReplyCommentWithContent:(NSString *)content postId:(NSInteger)postId parentId:(NSInteger)parentId replyUserName:(NSString *)name replyId:(NSInteger)replyId {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postReplyComment] requestParams:@{@"pId":@(parentId),@"ppId ":@(postId),@"postContent":content,@"postReplyUserName":name,@"id":@(replyId)} responseObjectClass:[PDPostDetailReplyModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        [PDHUD dismiss];
        if (isSucceeded) {
            [PDHUD showHUDSuccess:@"发表成功！"];
            weakSelf.needUpdate = YES;
            weakSelf.model = (PDPostDetailReplyModel *)responseObject;
            if (weakSelf.itemsArr.count) {
                [weakSelf.itemsArr removeAllObjects];
            }
            [weakSelf.itemsArr addObjectsFromArray:weakSelf.model.replys];
            [weakSelf.mainTableView reloadData];
        }
    }];
}

/**
 * 删除评论
 */
- (void)fetchDeleteReplyWithReplyModel:(PDPostDetailReplyModel *)model {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postDeleteReply] requestParams:@{@"postId":@(model.postId)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            [PDHUD showHUDSuccess:@"删除成功！"];
            [weakSelf.navigationController popViewControllerAnimated:YES];
            if (weakSelf.deleteBlock) {
                weakSelf.deleteBlock(weakSelf.model);
            }
        }
    }];
}

- (void)didDeleteReplyBlock:(void (^)(PDPostDetailReplyModel *))block {
    if (block) {
        self.deleteBlock = block;
    }
}

/**
 * 获取举报列表
 */
- (void)fetchRepostListWithPostId:(NSInteger)postId {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postReportList] requestParams:nil responseObjectClass:[PDPostReportList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDPostReportList *list = (PDPostReportList *)responseObject;
            NSMutableArray *itemsArr = [NSMutableArray arrayWithCapacity:5];
            for (PDPostReportItem *item in list.items) {
                [itemsArr addObject:item.reportReason];
            }
            [weakSelf showReportSheetViewWithReportArr:[itemsArr copy] postId:postId];
        }
    }];
}

- (void)showReportSheetViewWithReportArr:(NSArray *)reportArr postId:(NSInteger)postId {
    __weak typeof(self) weakSelf = self;
    [PDShareSheetView showSheetWithTitle:@"请选择举报原因" selContentArr:reportArr clickComplication:^(NSUInteger index, NSString *item) {
        if ([item containsString:@"其他"]) {
            [weakSelf showReportEditViewWithPostId:postId];
        } else {
            [weakSelf fetchReportWithReason:item postId:postId];
        }
    }];
}

/**
 * 弹出举报输入框
 */
- (void)showReportEditViewWithPostId:(NSInteger)postId {
    __weak typeof(self) weakSelf = self;
    PDPostReportView *reportView = [[PDPostReportView alloc] initWithTitle:@"填报举报理由"];
    PDAlertObjView *alert = [PDAlertObjView alertWithCustomView:reportView];
    [alert showAlertComplication:nil];
    [reportView didClickCancleActions:^{
        [alert dismissAlertComplication:nil];
    }];
    [reportView didClickSubmitActions:^(NSString *text) {
        if (text.length == 0) {
            [PDHUD showHUDSuccess:@"请填写举报理由！"];
            return;
        }
        [alert dismissAlertComplication:nil];
        
        [weakSelf fetchReportWithReason:text postId:postId];
    }];
    
    [alert addKeyboardNotification];
}

/**
 * 举报
 */
- (void)fetchReportWithReason:(NSString *)reason postId:(NSInteger)postId {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postReport] requestParams:@{@"postId":@(postId),@"reportReason":reason} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            [PDHUD showHUDSuccess:@"举报成功！"];
        }
    }];
}

- (void)didUpdateModelBlock:(void (^)(PDPostDetailReplyModel *))block {
    if (block) {
        self.updateBlock = block;
    }
}

@end
