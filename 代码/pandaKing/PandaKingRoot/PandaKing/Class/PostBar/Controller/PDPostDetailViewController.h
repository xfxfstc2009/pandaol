//
//  PDPostDetailViewController.h
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 帖子详情
@interface PDPostDetailViewController : AbstractViewController
@property (nonatomic) NSInteger postId;

- (void)didDeletePostBlock:(void(^)())deleteBlock;
@end


/*
 {
 "postId": 138,
 "parentId": 138,
 "postUserId": "7csk93",
 "postUserName": "18748228036",
 "postUserHead": "http://panda-e.com:81/po/upload/avatar/robot/avatar60.PNG",
 "activateGameUser": 0,
 "enable": 1,
 "level": 11,
 "gender": "FEMALE",
 "age": 0,
 "postTitle": "问大家个问题，既然uzi这么强",
 "postContent": "uzi这么强为啥s5的omg，和qg拿不到冠军而且相继爆炸，按理uzi加入ad位置增强没有短板了，为啥s5的omg本来是上单之光的狗狗硬突然变成木马，智商打野变成智障打野，qg春季赛亚军夏季赛也爆炸,s6本来rng春季赛冠军，uzi加入后ad位置也是增强，按理说又是没有短板，为啥夏季赛卫冕不了，上野相继变坑，难道这3个队伍大腿中上野不惜牺牲自己职业生涯一起演我狗？真心求解",
 "gameType": "lol",
 "num": 0,
 "numQq": 124,
 "good": 1,
 "host": 0,
 "me": 0,
 "replyHost": 0,
 "postGood": -2,
 "postBad": 0,
 "postView": 104,
 "postReply": 22,
 "reportNumber": 0,
 "postCreateTime": 1496737275000,
 "postUpdateTime": 1496813806000,
 "postIp": "115.197.192.202",
 "postReplyUserName": null,
 "postReplyContent": null,
 "postTop": 0,
 "postStatus": 0,
 "statusReason": null,
 "postDelete": 0,
 "deleteReason": null,
 "pictures": [{
 "pictureId": 172,
 "postId": 0,
 "pictureRoute": "/bbs/upload/image/11496737274478.jpg"
 }],
 "reports": [],
 "upvotes": [{
 "upvoteId": 49,
 "postId": 138,
 "userId": "fxd99w",
 "userName": "\uD83C\uDF1E",
 "userHead": "http://wx.qlogo.cn/mmopen/ajNVdqHZLLCZQcPWK0atLfZIQsh7aLSrpHSWWcVl5WQqjiaicBZ0kFDltOWaYZicZibfC9TIT6XgRJUWaVH0At4xmg/0",
 "activateGameUser": 1,
 "enable": 1,
 "level": 2,
 "gender": "MALE",
 "age": 0,
 "good": 1,
 "bad": 0,
 "time": 1496820993000
 }, {
 "upvoteId": 48,
 "postId": 138,
 "userId": "92du9r",
 "userName": "很不 错哦`",
 "userHead": "http://q.qlogo.cn/qqapp/1105265453/9B67C04F374FA63C4B3C3EBEB870D6B3/100",
 "activateGameUser": 0,
 "enable": 1,
 "level": 1,
 "gender": "MALE",
 "age": 0,
 "good": 1,
 "bad": 0,
 "time": 1496817992000
 }, {
 "upvoteId": 40,
 "postId": 138,
 "userId": "267udu",
 "userName": "小帅哥",
 "userHead": "http://panda-e.com:81/po/upload/avatar/481493261509573.jpg",
 "activateGameUser": 0,
 "enable": 1,
 "level": 0,
 "gender": "MALE",
 "age": 17,
 "good": 1,
 "bad": 0,
 "time": 1496815410000
 }]
	}
 */