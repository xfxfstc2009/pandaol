//
//  PDPostRootViewController.m
//  PandaKing
//
//  Created by Cranz on 17/5/19.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostRootViewController.h"
#import "PDPostMessageViewController.h"
#import "PDPostWebViewController.h"
#import "PDPostHotestViewController.h"
#import "PDPostLasetedViewController.h"
#import "CCZSegmentController.h"
#import "PDMessageViewController.h"
#import "PDYuezhanRootViewController.h"         // 约战

@interface PDPostRootViewController ()
@property (nonatomic, strong) CCZSegmentController *segment;
@end

@implementation PDPostRootViewController

+ (instancetype)sharedController{
    static PDPostRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[PDPostRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    [self fetchMessageState];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barMainTitle = @"野区";
    [self setupSegment];
    [self setupPostButton];
    [self setupRightItem];
}

- (void)setupSegment {
    PDPostHotestViewController *hotestViewController = [[PDPostHotestViewController alloc] init];
    PDPostLasetedViewController *lastestViewController = [[PDPostLasetedViewController alloc] init];
    
    CCZSegmentController *segement = [[CCZSegmentController alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height) titles:@[@"最新",@"最热"] titleFont:[UIFont systemFontOfSize:13] contentEdgeSpace:15];
    segement.viewControllers = @[lastestViewController,hotestViewController];
    segement.segmentView.segmentTintColor = c26;
    segement.segmentView.backgroundColor = [UIColor whiteColor];
    segement.segmentView.style = CCZSegmentStyleFlush;
    segement.segmentView.showSeparateLine = NO;
    [self addSegmentController:segement];
    self.segment = segement;
}

- (void)setupRightItem {
    if (self == [self.navigationController.viewControllers firstObject]){
        __weak typeof(self)weakSelf = self;
        self.tempMsgButton = [weakSelf rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_panda_message_nor"] barHltImage:[UIImage imageNamed:@"icon_panda_message_nor"] action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDMessageViewController *messageViewController = [[PDMessageViewController alloc]init];
            [messageViewController setHidesBottomBarWhenPushed:YES];
            [strongSelf.navigationController pushViewController:messageViewController animated:YES];
        }];
        [self.tempMsgButton sizeToFit];
    }
}

- (void)setupPostButton {
    UIImage *postImage = [UIImage imageNamed:@"icon_post_button"];
    UIButton *postButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [postButton setBackgroundImage:postImage forState:UIControlStateNormal];
    [postButton setFrame:CGRectMake(20, kScreenBounds.size.height - 49 - 44 - 36 - postImage.size.height, postImage.size.width, postImage.size.height)];
    [self.view addSubview:postButton];
    [postButton addTarget:self action:@selector(didClickPostMessage) forControlEvents:UIControlEventTouchUpInside];
}

- (void)updatePosts {
    // 最新列表去刷新
    PDPostLasetedViewController *lastestViewController = (PDPostLasetedViewController *)self.segment.viewControllers.firstObject;
    [lastestViewController updateToLastest];
    
    // 更新最热列表
    PDPostHotestViewController *hostestViewController = (PDPostHotestViewController *)self.segment.viewControllers[1];
    [hostestViewController updateToLastest];
}

- (void)didClickPostMessage {
    PDPostMessageViewController *postMessageViewController = [[PDPostMessageViewController alloc] init];
    [self pushViewController:postMessageViewController animated:YES];
    __weak typeof(self) weakSelf = self;
    [postMessageViewController postMessageSuccess:^{
        [weakSelf updatePosts];
    }];
}

- (void)fetchMessageState {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageState requestParams:nil responseObjectClass:[PDMessageState class] succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded) {
            PDMessageState *state = (PDMessageState *)responseObject;
            // 重置
            [AccountModel sharedAccountModel].messageState = state;
            
            if (state.hasUnReadMsg) {
                [strongSelf.tempMsgButton setImage:[UIImage imageNamed:@"icon_panda_message_has_nor"] forState:UIControlStateNormal];
                [strongSelf.tempMsgButton sizeToFit];
            } else {
                [strongSelf.tempMsgButton setImage:[UIImage imageNamed:@"icon_panda_message_nor"] forState:UIControlStateNormal];
                [strongSelf.tempMsgButton sizeToFit];
            }
        }
    }];
}

@end
