//
//  PDPostDetailViewController.m
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailViewController.h"
#import "PDShareSheetView.h"
#import "PDOAuthLoginAndShareTool.h"
#import "PDPostPriseListViewController.h"
#import "PDCenterPersonViewController.h"
#import "PDPostDetailReplyViewController.h"
#import "PDPostBarShowImgListViewController.h"

// 弹框
#import "PDAlertObjView.h"
#import "PDPostReportView.h"

// cells
#import "PDPostDetailTextCell.h" // 文本
#import "PDPostDetailPictureCell.h" // 图片
#import "PDPostBarUserCell.h" // 发帖人信息
#import "PDPostDetailPraiseCell.h" // 点赞
#import "PDPostDetailCommentNavigationCell.h" // 评论切换导航
#import "PDPostDetailReplyBasicCell.h" // 回复

// model
#import "PDPostReportList.h"
#import "PDPostDetailReplyList.h"

// custom view
#import "PDPostReplyButton.h"
#import "PDPostReplyUserView.h"
#import "PDPostReplyHostView.h"
#import "PDPostKeyboardTempView.h"
#import "PDCenterLabel.h"
#import "PDButton.h"

@interface PDPostDetailViewController ()<UITableViewDataSource, UITableViewDelegate, PDPostDetailPraiseCellDelegate, PDPostDetailReplyBasicCellDelegate, PDPostDetailPictureCellDelegate>
@property (nonatomic, copy) NSString *postTitle;
@property (nonatomic, copy) NSString *postContent;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) void(^deleteBlock)();
@property (nonatomic, strong) UITableView *mainTableView;
// 帖子详情
@property (nonatomic, strong) PDPostDetailModel *postDetail;
/**
 * 图片链接个数
 */
@property (nonatomic, strong) NSMutableArray *imageUrls;
/**
 * 回复按钮
 */
@property (nonatomic, strong) PDPostReplyButton *replyButton;
/**
 * 回复框里的点赞按钮
 */
@property (nonatomic, strong) PDButton *praiseButton;
/**
 * 回复框中的分享按钮
 */
@property (nonatomic, strong) PDButton *shareButton;
@property (nonatomic, strong) UITextField *replyField;
@property (nonatomic, strong) PDPostKeyboardTempView *tempView;
/**
 * 评论切换cell
 */
@property (nonatomic, strong) PDPostDetailCommentNavigationCell *naviCell;
// 详情里图片的高度
@property (nonatomic) CGFloat detailImagesHeight;

/**
 * 回复列表model
 */
@property (nonatomic, strong) PDPostDetailReplyList *replyList;
@property (nonatomic) NSUInteger page;
/**
 * 评论的数组
 */
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, strong) UIView *footerView;
@end

@implementation PDPostDetailViewController

- (UIView *)footerView {
    if (!_footerView) {
        _footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 44)];
        UIImage *icon = [UIImage imageNamed:@"icon_nodata_panda"];
        PDCenterLabel *label = [[PDCenterLabel alloc] init];
        label.image = icon;
        label.text = @"已经到底啦~";
        label.textColor = [UIColor lightGrayColor];
        CGSize size = label.size;
        label.frame = CGRectMake((kScreenBounds.size.width - size.width) / 2, (44 - size.height) / 2, size.width, size.height);
        [_footerView addSubview:label];
    }
    return _footerView;
}

- (NSMutableArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"postShareNotification" object:nil];
    [PDHUD dismiss];
}

- (NSMutableArray *)imageUrls {
    if (!_imageUrls) {
        _imageUrls = [NSMutableArray array];
    }
    return _imageUrls;
}

- (PDPostKeyboardTempView *)tempView {
    if (!_tempView) {
        _tempView = [[PDPostKeyboardTempView alloc] init];
    }
    return _tempView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupBasic];
    [self setupTableView];
    [self setupReplyButton];
    [self fetchShareInfo];
    [self fetchPostDetail];
    [self fetchReplyListWithUpdate:YES];
}

- (void)setupBasic {
    self.barMainTitle = @"详情";
    
    self.detailImagesHeight = 0;
    self.page = 1;
    
    __weak typeof(self) weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_post_more"] barHltImage:nil action:^{
        [weakSelf showShareAlert];
    }];
    
    // 添加通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didReceiveSharedInfo:) name:@"postShareNotification" object:nil];
}

- (void)setupTableView {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, self.view.bounds.size.height - 49) style:UITableViewStyleGrouped];
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.separatorColor = c27;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:self.mainTableView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.replyList.hasNextPage) {
            ++weakSelf.page;
            if (weakSelf.naviCell.selIndex == 0) {
                [weakSelf fetchReplyListWithUpdate:NO];
            } else {
                [weakSelf fetchHostReplyListWithUpdate:NO];
            }
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            [weakSelf.mainTableView stopPullToRefresh];
        }
    }];
    
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.itemsArr.count) {
            [weakSelf.itemsArr removeAllObjects];
        }
        
        if (weakSelf.naviCell.selIndex == 0) {
            [weakSelf fetchReplyListWithUpdate:YES];
        } else {
            [weakSelf fetchHostReplyListWithUpdate:YES];
        }
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [weakSelf fetchPostDetail];
        });
    }];
}

- (void)setupReplyButton {
    UIView *replyView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenBounds.size.height - 64 - 49, kScreenBounds.size.width, 49)];
    replyView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:replyView];
    UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(replyView.frame), 1)];
    topLine.backgroundColor = c27;
    [replyView addSubview:topLine];
    
    CGFloat itemHeight = CGRectGetHeight(replyView.frame);
    // 按钮的高度
    CGFloat buttonWidth = 30;
    // 按钮和输入框之间的空隙
    CGFloat buttonSpace = 20;
    self.replyButton = [[PDPostReplyButton alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 6, CGRectGetWidth(replyView.frame) - kTableViewSectionHeader_left * 2 - buttonWidth * 2 - buttonSpace * 2, itemHeight - 12)];
    self.replyButton.layer.cornerRadius = 2.5;
    self.replyButton.layer.borderColor = c27.CGColor;
    self.replyButton.layer.borderWidth = 1;
    [self.replyButton addTarget:self action:@selector(didClickReply:) forControlEvents:UIControlEventTouchUpInside];
    [replyView addSubview:self.replyButton];
    
    PDButton *praiseButton = [PDButton buttonWithType:UIButtonTypeCustom];
    self.praiseButton = praiseButton;
    praiseButton.frame = CGRectMake(CGRectGetMaxX(self.replyButton.frame) + buttonSpace, 0, buttonWidth, itemHeight);
    praiseButton.titleLabel.font = [UIFont systemFontOfCustomeSize:10];
    praiseButton.imageView.image = [UIImage imageNamed:@"icon_post_reply_praise"];
    [praiseButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [praiseButton setTitle:@"点赞" forState:UIControlStateNormal];
    [praiseButton addTarget:self action:@selector(didClickReplyPriseButton:) forControlEvents:UIControlEventTouchUpInside];
    [replyView addSubview:praiseButton];
    
    PDButton *shareButton = [PDButton buttonWithType:UIButtonTypeCustom];
    self.shareButton = shareButton;
    shareButton.frame = CGRectMake(CGRectGetMaxX(praiseButton.frame) + 20, 0, buttonWidth, itemHeight);
    shareButton.imageView.image = [UIImage imageNamed:@"icon_post_reply_share"];
    [shareButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    shareButton.titleLabel.font = [UIFont systemFontOfCustomeSize:10];
    [shareButton setTitle:@"分享" forState:UIControlStateNormal];
    [shareButton addTarget:self action:@selector(didClickReplyShareButton:) forControlEvents:UIControlEventTouchUpInside];
    [replyView addSubview:shareButton];
    
    self.replyField = [[UITextField alloc] initWithFrame:self.replyButton.bounds];
    self.replyField.hidden = YES;
    [self.view insertSubview:self.replyField belowSubview:self.replyButton];
}

#pragma mark - 点击回复栏的按钮

- (void)didClickReply:(UIButton *)button {
    [self showReplyHostKeyboard];
}

- (void)didClickReplyPriseButton:(PDButton *)button {
    __weak typeof(self) weakSelf = self;
    NSDictionary *param;
    if (button.selected == NO) {
        param = @{@"postId":@(self.postId),@"name":@"zan"};
    } else {
        param = @{@"postId":@(self.postId),@"name":@""};
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postPraise] requestParams:param responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            [weakSelf fetchPostDetail];
        }
    }];
}

- (void)didClickReplyShareButton:(PDButton *)button {
    [self showShareAlert];
}

#pragma mark - 设置不同的回复键盘小窗口
/**
 * 回复一楼评论
 */
- (void)showReplyUserKeyboardWithModel:(PDPostDetailReplyModel *)model indexPath:(NSIndexPath *)indexPath {
    PDPostReplyUserView *replyView = [[PDPostReplyUserView alloc] init];
    self.replyField.inputAccessoryView = replyView;
    replyView.model = model;
    
    __weak typeof(self) weakSelf = self;
    [replyView postView:^(NSString *text, PDPostDetailReplyModel *model) {
        [PDHUD showHUDProgress:@"正在发表您的评论,请稍后..." diary:0];
        [weakSelf fetchReplyCommentWithContent:text postId:model.postId parentId:model.parentId replyUserName:model.postUserName replyId:model.replyId indexPath:indexPath];
        [weakSelf.replyField resignFirstResponder];
        [weakSelf.tempView dismissTempView];
    }];
    
    [self.tempView showTempView];
    [self.tempView didTouchAtTempView:^{
        [replyView.textView resignFirstResponder];
        [weakSelf.replyField resignFirstResponder];
        [weakSelf.tempView dismissTempView];
    }];
    
    [self.replyField becomeFirstResponder];
    [replyView.textView becomeFirstResponder];
}

/**
 * 回复楼中楼评论
 */
- (void)showReplyUserKeyboardWithSubReplyItem:(PDPostDetailReplyItem *)item indexPath:(NSIndexPath *)indexPath {
    PDPostReplyUserView *replyView = [[PDPostReplyUserView alloc] init];
    self.replyField.inputAccessoryView = replyView;
    replyView.item = item;
    
    __weak typeof(self) weakSelf = self;
    [replyView postViewReplyWithItemBlock:^(NSString *text, PDPostDetailReplyItem *item) {
        [PDHUD showHUDProgress:@"正在发表您的评论,请稍后..." diary:0];
        [weakSelf fetchReplyCommentWithContent:text postId:item.postId parentId:item.parentId replyUserName:item.userName replyId:item.replyId indexPath:indexPath];
        [weakSelf.replyField resignFirstResponder];
        [weakSelf.tempView dismissTempView];
    }];
    
    [self.tempView showTempView];
    [self.tempView didTouchAtTempView:^{
        [replyView.textView resignFirstResponder];
        [weakSelf.replyField resignFirstResponder];
        [weakSelf.tempView dismissTempView];
    }];
    
    [self.replyField becomeFirstResponder];
    [replyView.textView becomeFirstResponder];
}

/**
 * 回复楼主评论
 */
- (void)showReplyHostKeyboard {
    PDPostReplyHostView *replyView = [[PDPostReplyHostView alloc] init];
    self.replyField.inputAccessoryView = replyView;
    
    __weak typeof(self) weakSelf = self;
    [replyView replyViewDidClickImageButton:^{
        [weakSelf.tempView dismissTempView];
        [weakSelf goPhotoControllerWithReplyView:replyView];
    }];
    
    [replyView replyView:^(NSString *text, UIImage *image) {
        [PDHUD showHUDProgress:@"正在发表您的评论,请稍后..." diary:0];
        if (image) {
            [weakSelf fetchCommitImage:image content:text];
        } else {
            [weakSelf fetchReplyWithContent:text pictureString:@""];
        }
        [weakSelf.replyField resignFirstResponder];
        [weakSelf.tempView dismissTempView];
    }];
    
    [self.tempView showTempView];
    [self.tempView didTouchAtTempView:^{
        [replyView.textView resignFirstResponder];
        [weakSelf.replyField resignFirstResponder];
    }];
    
    [self.replyField becomeFirstResponder];
    [replyView.textView becomeFirstResponder];
}

- (void)goPhotoControllerWithReplyView:(PDPostReplyHostView *)replyView {
    GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc]init];
    __weak typeof(self) weakSelf = self;
    [assetVC selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
        if (selectedImgArr.count == 0) {
            return;
        }
        replyView.selImage = [selectedImgArr firstObject];
        [weakSelf.tempView showTempView];
    }];
    
    [self pushViewController:assetVC animated:YES];
}

#pragma mark - UITableView DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 6;
    } else if (section == 1) {
        return 1;
    } else {
        return self.itemsArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    __weak typeof(self) weakSelf = self;
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            PDPostBarUserCell *userCell = [tableView dequeueReusableCellWithIdentifier:@"userCellId"];
            if (!userCell) {
                userCell = [[PDPostBarUserCell alloc] initWithReuseIdentifier:@"userCellId"];
                userCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            userCell.nameLabel.text = self.postDetail.postUserName;
            if (self.postDetail.admin == YES) {
                userCell.nameLabel.textColor = [UIColor redColor];
            } else {
                userCell.nameLabel.textColor = [UIColor blackColor];
            }
            userCell.levelLabel.text = [NSString stringWithFormat:@"Lv.%@",@(self.postDetail.level)];
            userCell.dateLabel.text = [NSDate getTimeGap:self.postDetail.postCreateTime/1000];
            [userCell.headerIcon uploadImageWithAvatarURL:self.postDetail.postUserHead placeholder:nil callback:nil];
            [userCell updateSubviews];
            return userCell;
        } else if (indexPath.row == 1) {
            PDPostDetailTextCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"titleCellId"];
            if (!titleCell) {
                titleCell = [[PDPostDetailTextCell alloc] initWithTextType:PDPostDetailTextTypeTitle reuseIdentifier:@"titleCellId"];
            }
            titleCell.text = self.postDetail.postTitle;
            return titleCell;
        } else if (indexPath.row == 2) {
            PDPostDetailTextCell *contentCell = [tableView dequeueReusableCellWithIdentifier:@"contentCellId"];
            if (!contentCell) {
                contentCell = [[PDPostDetailTextCell alloc] initWithTextType:PDPostDetailTextTypeContent reuseIdentifier:@"contentCellId"];
            }
            contentCell.text = self.postDetail.postContent;
            return contentCell;
        } else if (indexPath.row == 3) {
            PDPostDetailPictureCell *pictureCell = [tableView dequeueReusableCellWithIdentifier:@"pictureCellId"];
            if (!pictureCell) {
                pictureCell = [[PDPostDetailPictureCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"pictureCellId"];
                pictureCell.selectionStyle = UITableViewCellSelectionStyleNone;
                pictureCell.delegate = self;
            }
            pictureCell.urls = [self.imageUrls copy];
            return pictureCell;
        } else if (indexPath.row == 4) {
            UITableViewCell *seeCell = [tableView dequeueReusableCellWithIdentifier:@"seeCellId"];
            if (!seeCell) {
                seeCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"seeCellId"];
                UIButton *seeButton = [UIButton buttonWithType:UIButtonTypeCustom];
                seeButton.stringTag = @"seeButton";
                [seeButton setImage:[UIImage imageNamed:@"icon_post_see"] forState:UIControlStateNormal];
                [seeButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 10, 0, 0)];
                seeButton.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
                [seeButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
                [seeCell.contentView addSubview:seeButton];
            }
            
            UIButton *seeButton = (UIButton *)[seeCell viewWithStringTag:@"seeButton"];
            [seeButton setTitle:[NSString stringWithFormat:@"%@人浏览",[PDCenterTool numberStringChangeWithoutSpecialCharacter:self.postDetail.postView]] forState:UIControlStateNormal];
            CGSize seeImageSize = seeButton.imageView.image.size;
            CGSize seeTextSize = [seeButton.titleLabel.text sizeWithCalcFont:seeButton.titleLabel.font];
            CGSize seeSize = CGSizeMake(seeImageSize.width + 10 + seeTextSize.width, seeTextSize.height + 10);
            seeButton.frame = CGRectMake(kTableViewSectionHeader_left, 35 - seeSize.height, seeSize.width, seeSize.height);
            return seeCell;
        } else if (indexPath.row == 5) {
            PDPostDetailPraiseCell *praiseCell = [tableView dequeueReusableCellWithIdentifier:@"praiseCellId"];
            if (!praiseCell) {
                praiseCell = [[PDPostDetailPraiseCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"praiseCellId"];
                praiseCell.delegate = self;
            }
            praiseCell.model = self.postDetail;
            return praiseCell;
        } else {
            return nil;
        }
    } else if (indexPath.section == 1) {
        PDPostDetailCommentNavigationCell *naviCell = [tableView dequeueReusableCellWithIdentifier:@"naviCellId"];
        if (!naviCell) {
            naviCell = [[PDPostDetailCommentNavigationCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"naviCellId"];
            self.naviCell = naviCell;
        }
        
//        naviCell.commentCount = self.replyList.totalItemsCount;
        [naviCell didChangeSelected:^(NSUInteger index) {
            weakSelf.page = 1;
            if (weakSelf.itemsArr.count) {
                [weakSelf.itemsArr removeAllObjects];
            }
            if (index == 0) {
                [weakSelf fetchReplyListWithUpdate:YES];
            } else {
                [weakSelf fetchHostReplyListWithUpdate:YES];
            }
        }];
        return naviCell;
    } else {
        PDPostDetailReplyBasicCell *replyCell = [tableView dequeueReusableCellWithIdentifier:@"replyCellId"];
        if (!replyCell) {
            replyCell = [[PDPostDetailReplyBasicCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"replyCellId"];
            replyCell.delegate = self;
        }
        replyCell.indexPath = indexPath;
        if (self.itemsArr.count > indexPath.row) {
            replyCell.model = self.itemsArr[indexPath.row];
        }
        
        return replyCell;
    }
}

- (void)updateButton:(UIButton *)button {
    CGSize praiseImageSize = button.imageView.image.size;
    CGSize praiseTextSize = [button.titleLabel.text sizeWithCalcFont:button.titleLabel.font];
    CGSize praiseSize = CGSizeMake(praiseImageSize.width + 10 + praiseTextSize.width, praiseTextSize.height + 10);
    button.frame = CGRectMake(button.frame.origin.x, button.frame.origin.y, praiseSize.width, praiseSize.height);
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            return [PDPostBarUserCell cellHeight];
        } else if (indexPath.row == 1) {
            return [PDPostDetailTextCell cellHeightWithText:self.postDetail.postTitle type:PDPostDetailTextTypeTitle];
        } else if (indexPath.row == 2) {
            return [PDPostDetailTextCell cellHeightWithText:self.postDetail.postContent type:PDPostDetailTextTypeContent];
        } else if (indexPath.row == 3){
            return [PDPostDetailPictureCell cellHeightWithUrlArr:self.imageUrls];
        } else if (indexPath.row == 4) {
            return 35;
        } else if (indexPath.row == 5) {
            return [PDPostDetailPraiseCell cellHeight];
        } else {
            return 0;
        }
    } else if (indexPath.section == 1) {
        return [PDPostDetailCommentNavigationCell cellHeight];
    } else {
        if (self.itemsArr.count > indexPath.row) {
            return [PDPostDetailReplyBasicCell cellHeightWithModel:self.itemsArr[indexPath.row]];
        }
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (section == 0) {
        return kTableViewHeader_height;
    } else {
        return 0.01;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
    } else {
        [PDCenterTool calculateCell:cell leftSpace:kScreenBounds.size.width - 0.01 rightSpace:0];
    }
}

#pragma mark - UITableView delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.itemsArr.count > indexPath.row && indexPath.section == 2) {
        PDPostDetailReplyModel *model = self.itemsArr[indexPath.row];
        [self showReplyUserKeyboardWithModel:model indexPath:indexPath];
    }
}

#pragma mark - PDPostDetailPictureCellDelegate

- (void)pictureCell:(PDPostDetailPictureCell *)cell didClickImgaeView:(PDImageView *)imageView atIndex:(NSInteger)index {
    
}

#pragma mark - PDPostDetailReplyBasicCellDelegate

- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickImageView:(PDImageView *)imageView {
    
}

- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickMoreButton:(UIButton *)button {
    __weak typeof(self) weakSelf = self;
    if (cell.model.me) {
        [PDShareSheetView showSheetWithTitle:nil selContentArr:@[@"删除评论"] clickComplication:^(NSUInteger index, NSString *item) {
            if (index == 0) {
                [weakSelf fetchDeleteReplyWithReplyModel:cell.model atIndexPath:cell.indexPath];
            }
        }];
    } else {
        [PDShareSheetView showSheetWithTitle:nil selContentArr:@[@"举报评论"] clickComplication:^(NSUInteger index, NSString *item) {
            if (index == 0) {
                [weakSelf fetchRepostListWithPostId:cell.model.postId];
            }
        }];
    }
}

- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickPriseButton:(UIButton *)button {
    [self fetchPraise:!button.selected postId:cell.model.postId praiseButton:button replyModel:cell.model needToUpdate:NO];
}

- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickReplyCellWithModel:(PDPostDetailReplyItem *)item {
    // 回复楼中楼的评论
    [self showReplyUserKeyboardWithSubReplyItem:item indexPath:cell.indexPath];
}

- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickReplyUserNameWithUserId:(NSString *)userId {
    // 跳个人详情
    PDCenterPersonViewController *personViewController = [[PDCenterPersonViewController alloc] init];
    personViewController.transferMemberId = userId;
    personViewController.isMe = [[Tool userDefaultGetWithKey:CustomerMemberId] isEqualToString:userId];
    [self pushViewController:personViewController animated:YES];
}

/**
 * 跳转更多评论
 */
- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickLookMoreReplyButton:(UIButton *)button {
    PDPostDetailReplyViewController *replyViewController = [[PDPostDetailReplyViewController alloc] init];
    replyViewController.model = cell.model;
    [self pushViewController:replyViewController animated:YES];
    
    __weak typeof(self) weakSelf = self;
    [replyViewController didDeleteReplyBlock:^(PDPostDetailReplyModel *model) {
        [weakSelf fetchDeleteReplyWithReplyModel:model atIndexPath:cell.indexPath];
    }];
    
    [replyViewController didUpdateModelBlock:^(PDPostDetailReplyModel *model) {
        
        weakSelf.naviCell.commentCount = model.postReply;
        
        [weakSelf.itemsArr replaceObjectAtIndex:cell.indexPath.row withObject:model];
        [weakSelf.mainTableView beginUpdates];
        [weakSelf.mainTableView reloadRowsAtIndexPaths:@[cell.indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        [weakSelf.mainTableView endUpdates];
    }];
}

#pragma mark - PDPostDetailPraiseCellDelegate

- (void)cell:(PDPostDetailPraiseCell *)cell didClickPraise:(UIButton *)button {
    // 点赞
    [self fetchPraise:!button.selected postId:self.postId praiseButton:button replyModel:nil needToUpdate:YES];
}

#pragma mark - 弹框

- (void)showShareAlert {
    __weak typeof(self) weakSelf = self;
    
    NSArray *itemArr,*imageArr;
    if (self.postDetail.me) {
        itemArr = @[@"QQ好友",@"微信好友",@"QQ空间",@"朋友圈",@"删除"];
        imageArr = @[@"icon_share_qq",@"icon_share_wx",@"icon_share_qqzone",@"icon_share_wxzone",@"icon_share_delete"];
    } else {
        itemArr = @[@"QQ好友",@"微信好友",@"QQ空间",@"朋友圈",@"举报"];
        imageArr = @[@"icon_share_qq",@"icon_share_wx",@"icon_share_qqzone",@"icon_share_wxzone",@"icon_share_report"];
    }
    
    [PDShareSheetView shareWithItemArr:itemArr imageArr:imageArr clickComplication:^(NSString *item) {
        if ([item isEqualToString:@"QQ好友"]) {
            if ([TencentOAuth iphoneQQInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeQQFriend];
            } else {
                [PDHUD showHUDError:@"您未安装QQ手机客户端！"];
            }
        } else if ([item isEqualToString:@"微信好友"]) {
            if ([WXApi isWXAppInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeWXSession];
            } else {
                [PDHUD showHUDError:@"您未安装微信手机客户端！"];
            }
        } else if ([item isEqualToString:@"QQ空间"]) {
            if ([TencentOAuth iphoneQQInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeQQZone];
            } else {
                [PDHUD showHUDError:@"您未安装QQ手机客户端！"];
            }
        } else if ([item isEqualToString:@"朋友圈"]) {
            if ([WXApi isWXAppInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeWXTimeLine];
            } else {
                [PDHUD showHUDError:@"您未安装微信手机客户端！"];
            }
        } else if ([item isEqualToString:@"举报"]) {
            [weakSelf fetchRepostListWithPostId:self.postId];
        } else if ([item isEqualToString:@"删除"]) {
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"提示" conten:@"你确定要删除该帖子吗？" isClose:NO btnArr:@[@"确定",@"取消"] buttonClick:^(NSInteger buttonIndex) {
                if (buttonIndex == 0) {
                    [weakSelf fetchDeletePost];
                }
                [JCAlertView dismissAllCompletion:nil];
            }];
        }
    }];
}

- (void)shareWithType:(PD3RShareType)type {
    __weak typeof(self) weakSelf = self;
    PDImageView *iv = [[PDImageView alloc] init];
    NSString *picPath = self.image;
    [iv uploadImageWithURL:picPath placeholder:nil callback:^(UIImage *image) {
        NSData *imgData = [PDCenterTool compressImage:image];
        if (imgData.length >= 32 * 1024) { // 判断字节数
            imgData = [PDCenterTool compressImage:image];
        }
        [PDOAuthLoginAndShareTool sharedWithType:type text:nil title:weakSelf.postTitle desc:weakSelf.postContent thubImageData:imgData webUrl:[NSString stringWithFormat:@"http://%@:%@%@?postId=%@",Jungle_Host,Jungle_Port,postWebDetail,@(weakSelf.postId)]];
    }];
}

- (void)showReportSheetViewWithReportArr:(NSArray *)reportArr postId:(NSInteger)postId {
    __weak typeof(self) weakSelf = self;
    [PDShareSheetView showSheetWithTitle:@"请选择举报原因" selContentArr:reportArr clickComplication:^(NSUInteger index, NSString *item) {
        if ([item containsString:@"其他"]) {
            [weakSelf showReportEditViewWithPostId:postId];
        } else {
            [weakSelf fetchReportWithReason:item postId:postId];
        }
    }];
}

/**
 * 弹出举报输入框
 */
- (void)showReportEditViewWithPostId:(NSInteger)postId {
    __weak typeof(self) weakSelf = self;
    PDPostReportView *reportView = [[PDPostReportView alloc] initWithTitle:@"填报举报理由"];
    PDAlertObjView *alert = [PDAlertObjView alertWithCustomView:reportView];
    [alert showAlertComplication:nil];
    [reportView didClickCancleActions:^{
        [alert dismissAlertComplication:nil];
    }];
    [reportView didClickSubmitActions:^(NSString *text) {
        if (text.length == 0) {
            [PDHUD showHUDSuccess:@"请填写举报理由！"];
            return;
        }
        [alert dismissAlertComplication:nil];
        
        [weakSelf fetchReportWithReason:text postId:postId];
    }];
    
    [alert addKeyboardNotification];
}

- (void)didDeletePostBlock:(void (^)())deleteBlock {
    if (deleteBlock) {
        self.deleteBlock = deleteBlock;
    }
}

#pragma mark - 网络请求

/**
 * 获取举报列表
 */
- (void)fetchRepostListWithPostId:(NSInteger)postId {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postReportList] requestParams:nil responseObjectClass:[PDPostReportList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDPostReportList *list = (PDPostReportList *)responseObject;
            NSMutableArray *itemsArr = [NSMutableArray arrayWithCapacity:5];
            for (PDPostReportItem *item in list.items) {
                [itemsArr addObject:item.reportReason];
            }
            [weakSelf showReportSheetViewWithReportArr:[itemsArr copy] postId:postId];
        }
    }];
}

/**
 * 删除帖子
 */
- (void)fetchDeletePost {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postDelete] requestParams:@{@"postId":@(self.postId)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) return;
        if (isSucceeded) {
            [weakSelf.navigationController popViewControllerAnimated:YES];
            if (weakSelf.deleteBlock) {
                weakSelf.deleteBlock();
            }
        }
    }];
}

/**
 * 举报
 */
- (void)fetchReportWithReason:(NSString *)reason postId:(NSInteger)postId {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postReport] requestParams:@{@"postId":@(postId),@"reportReason":reason} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            [PDHUD showHUDSuccess:@"举报成功！"];
        }
    }];
}

/**
 * 获取分享的信息
 */
- (void)fetchShareInfo {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postShareInfo] requestParams:@{@"postId":@(self.postId)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.postTitle = responseObject[@"title"];
            weakSelf.postContent = responseObject[@"content"];
            weakSelf.image = responseObject[@"img"];
        }
    }];
}

/**
 * 告诉服务器分享结果
 */
- (void)didReceiveSharedInfo:(NSNotification *)noti {
    NSDictionary *userInfo = noti.userInfo;
    if ([userInfo[@"result"] isEqualToString:@"success"]) {
        // 发送
        [[NetworkAdapter sharedAdapter] fetchWithPath:centerTaskShareSuccess requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
            if (isSucceeded) {
                [PDHUD showHUDSuccess:@"分享成功！"];
            }
        }];
    }
}

// praise:YES 点赞  NO 取消
// update:YES 直接刷新详情，NO 前端控制
- (void)fetchPraise:(BOOL)praise postId:(NSInteger)postId praiseButton:(UIButton *)button replyModel:(PDPostDetailReplyModel *)model needToUpdate:(BOOL)update {
    __weak typeof(self) weakSelf = self;
    NSDictionary *param;
    if (praise) {
        param = @{@"postId":@(postId),@"name":@"zan"};
    } else {
        param = @{@"postId":@(postId),@"name":@""};
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postPraise] requestParams:param responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            if (update == YES) {
                [weakSelf fetchPostDetail];
                return;
            }
            
            button.selected = !button.selected;
        
            NSUInteger priseCount = button.tag;
            if (praise == YES) {
                ++priseCount;
            } else {
                --priseCount;
            }
            
            // 评论的点赞
            if (model) {
                model.good = button.selected;
                if (model.good) {
                    model.postGood++;
                } else {
                    model.postGood--;
                }
            }
            
            button.tag = priseCount;
            if ([button.titleLabel.text containsString:@"赞"]) {
                [button setTitle:[NSString stringWithFormat:@"%@人觉得赞",[PDCenterTool numberStringChangeWithoutSpecialCharacter:priseCount]] forState:UIControlStateNormal];
            } else {
                [button setTitle:[NSString stringWithFormat:@"%@",[PDCenterTool numberStringChangeWithoutSpecialCharacter:priseCount]] forState:UIControlStateNormal];
            }
            [weakSelf updateButton:button];
        }
    }];
}

// 请求详情
- (void)fetchPostDetail {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postDetail] requestParams:@{@"postId":@(self.postId)} responseObjectClass:[PDPostDetailModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            weakSelf.postDetail = (PDPostDetailModel *)responseObject;
            
            // 回复按钮
            if (weakSelf.postDetail.postStatus == NO) {
                weakSelf.replyButton.text = @"回复楼主";
            } else {
                weakSelf.replyButton.text = @"主题已被锁定，不可回复";
            }
            
            if (weakSelf.postDetail.good) {
                [weakSelf.praiseButton setTitle:@"已点赞" forState:UIControlStateNormal];
                weakSelf.praiseButton.selected = YES;
            } else {
                [weakSelf.praiseButton setTitle:@"点赞" forState:UIControlStateNormal];
                weakSelf.praiseButton.selected = NO;
            }
            
            weakSelf.naviCell.commentCount = weakSelf.postDetail.postReply;
            
            // 处理图片
            if (weakSelf.imageUrls.count) {
                [weakSelf.imageUrls removeAllObjects];
            }
            for (PDPostPicture *picture in weakSelf.postDetail.pictures) {
                [weakSelf.imageUrls addObject:[PDCenterTool absoluteUrlWithPostUrl:picture.pictureRoute]];
            }
            
            [weakSelf.mainTableView beginUpdates];
            [weakSelf.mainTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
            [weakSelf.mainTableView endUpdates];
            
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            [weakSelf.mainTableView stopPullToRefresh];
        } else {
            NSUInteger code = [responseObject[@"code"] integerValue];
            if (code == 501) {
                [weakSelf.view showPrompt:@"该帖子已被删除" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:0 tapBlock:nil];
            }
        }
    }];
}

// 获取评论列表
- (void)fetchReplyListWithUpdate:(BOOL)update {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postReplyList] requestParams:@{@"postId":@(self.postId),@"currentPage":@(self.page),@"pageSize":@(5)} responseObjectClass:[PDPostDetailReplyList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDPostDetailReplyList *list = (PDPostDetailReplyList *)responseObject;
            weakSelf.replyList = list;
            
            [weakSelf.itemsArr addObjectsFromArray:list.items];
            [weakSelf.mainTableView beginUpdates];
            if (update) {
                [weakSelf.mainTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationNone];
            } else {
                NSMutableArray *rows = [NSMutableArray arrayWithCapacity:list.items.count];
                for (int i = 0; i < list.items.count; i++) {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:weakSelf.itemsArr.count - list.items.count + i inSection:2];
                    [rows addObject:indexPath];
                }
                [weakSelf.mainTableView insertRowsAtIndexPaths:[rows copy] withRowAnimation:UITableViewRowAnimationNone];
            }
            [weakSelf.mainTableView endUpdates];
            
            [weakSelf.mainTableView stopPullToRefresh];
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            
            if (!list.hasNextPage) {
                weakSelf.mainTableView.tableFooterView = weakSelf.footerView;
            }
        }
    }];
}

/**
 * 只看楼主
 * update：YES的时候是刷新表，NO是插入
 */
- (void)fetchHostReplyListWithUpdate:(BOOL)update {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postHostReplyList] requestParams:@{@"postId":@(self.postId),@"currentPage":@(self.page),@"pageSize":@(5)} responseObjectClass:[PDPostDetailReplyList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDPostDetailReplyList *list = (PDPostDetailReplyList *)responseObject;
            [weakSelf.itemsArr addObjectsFromArray:list.items];
            [weakSelf.mainTableView beginUpdates];
            if (update) {
                [weakSelf.mainTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationNone];
            } else {
                NSMutableArray *rows = [NSMutableArray arrayWithCapacity:list.items.count];
                for (int i = 0; i < list.items.count; i++) {
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:weakSelf.itemsArr.count - list.items.count + i inSection:2];
                    [rows addObject:indexPath];
                }
                [weakSelf.mainTableView insertRowsAtIndexPaths:[rows copy] withRowAnimation:UITableViewRowAnimationNone];
            }
            [weakSelf.mainTableView endUpdates];
            
            [weakSelf.mainTableView stopPullToRefresh];
            [weakSelf.mainTableView stopFinishScrollingRefresh];
        }
    }];
}

// 发表评论
- (void)fetchReplyWithContent:(NSString *)content pictureString:(NSString *)picture {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postReplyHost] requestParams:@{@"pId":@(self.postId),@"postContent":content,@"pictures":picture} responseObjectClass:[PDPostDetailReplyModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        [PDHUD dismiss];
        if (isSucceeded) {
            [PDHUD showHUDSuccess:@"发表成功！"];
            PDPostDetailReplyModel *model = (PDPostDetailReplyModel *)responseObject;
            weakSelf.naviCell.commentCount = model.postReply;
            
            if (!weakSelf.replyList.hasNextPage && weakSelf.naviCell.selIndex == 0) {
                [weakSelf.itemsArr addObject:responseObject];
                [weakSelf.mainTableView beginUpdates];
                [weakSelf.mainTableView insertRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:weakSelf.itemsArr.count - 1 inSection:2]] withRowAnimation:UITableViewRowAnimationAutomatic];
                [weakSelf.mainTableView endUpdates];
            }
        }
    }];
}

// 回复评论
// name:被回复人的id
- (void)fetchReplyCommentWithContent:(NSString *)content postId:(NSInteger)postId parentId:(NSInteger)parentId replyUserName:(NSString *)name replyId:(NSInteger)replyId indexPath:(NSIndexPath *)indexpath {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postReplyComment] requestParams:@{@"pId":@(parentId),@"ppId ":@(postId),@"postContent":content,@"postReplyUserName":name,@"id":@(replyId)} responseObjectClass:[PDPostDetailReplyModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        [PDHUD dismiss];
        if (isSucceeded) {
            [PDHUD showHUDSuccess:@"发表成功！"];
            
            PDPostDetailReplyModel *model = (PDPostDetailReplyModel *)responseObject;
            weakSelf.naviCell.commentCount = model.postReply;
            
            [weakSelf.itemsArr replaceObjectAtIndex:indexpath.row withObject:(PDPostDetailReplyModel *)responseObject];
            [weakSelf.mainTableView beginUpdates];
            [weakSelf.mainTableView reloadRowsAtIndexPaths:@[indexpath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [weakSelf.mainTableView endUpdates];
        }
    }];
}

// 上传图片
- (void)fetchCommitImage:(UIImage *)image content:(NSString *)content {

    FetchFileModel *file = [[FetchFileModel alloc] init];
    file.uploadImage = image;
    
    FetchModel *fileModel = [[FetchModel alloc] init];
    fileModel.requestFileDataArr = @[file];
    
    __weak typeof(self)weakSelf = self;
    [fileModel fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postImages] completionHandler:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return;
        }
        
        if (isSucceeded){
            NSArray *images = responseObject[@"image"];
            
            NSMutableString *imageString = [NSMutableString string];
            for (int i = 0;i < images.count; i++) {
                NSString *img = images[i];
                NSString *appendStr = @"";
                if (i == 0) {
                    appendStr = img;
                } else {
                    appendStr = [NSString stringWithFormat:@",%@",img];
                }
                [imageString appendString:appendStr];
            }
            
            [weakSelf fetchReplyWithContent:content pictureString:[imageString copy]];
        }
    }];
}

/**
 * 删除评论
 */
- (void)fetchDeleteReplyWithReplyModel:(PDPostDetailReplyModel *)model atIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postDeleteReply] requestParams:@{@"postId":@(model.postId)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            [PDHUD showHUDSuccess:@"删除成功！"];
            
            NSInteger count = [responseObject[@"postReply"]integerValue];
            weakSelf.naviCell.commentCount = count;
            
            if ([weakSelf.itemsArr containsObject:model]) {
                [weakSelf.itemsArr removeObject:model];
                [weakSelf.mainTableView beginUpdates];
                [weakSelf.mainTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                [weakSelf.mainTableView endUpdates];
            }
        }
    }];
}

@end
