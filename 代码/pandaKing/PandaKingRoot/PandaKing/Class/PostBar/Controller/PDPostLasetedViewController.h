//
//  PDPostLasetedViewController.h
//  PandaKing
//
//  Created by Cranz on 17/6/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 野区最新列表
@interface PDPostLasetedViewController : UIViewController
- (void)updateToLastest;
@end
