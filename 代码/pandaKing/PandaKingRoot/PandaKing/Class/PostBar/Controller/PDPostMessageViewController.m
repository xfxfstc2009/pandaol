//
//  PDPostMessageViewController.m
//  PandaKing
//
//  Created by Cranz on 17/5/15.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostMessageViewController.h"
#import "PDEditInfoTableViewCell.h"
#import "PDPostMessageLimitCell.h"
#import "PDPostMessageContentCell.h"

@interface PDPostMessageViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) PDPostMessageLimitCell *limitCell;
@property (nonatomic, strong) PDPostMessageContentCell *contentCell;
@property (nonatomic, copy) void(^postSuccessBlock)();
@end

@implementation PDPostMessageViewController

- (void)dealloc {
    [PDHUD dismiss];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barMainTitle = @"发帖";
    self.view.backgroundColor = [UIColor whiteColor];
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self setupNavigationItems];
    [self setupView];
}

- (void)setupNavigationItems {
    __weak typeof(self) weakSelf = self;
    [self leftBarButtonWithTitle:@"取消" barNorImage:nil barHltImage:nil action:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    
    [self rightBarButtonWithTitle:@"发布" barNorImage:nil barHltImage:nil action:^{
        [weakSelf.view endEditing:YES];

        PDEditInfoTableViewCell *titleCell = [weakSelf.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        PDPostMessageContentCell *contentCell = [weakSelf.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
        NSString *title = titleCell.textField.text;
        NSString *content = contentCell.textView.text;
        
        if (title.length == 0 || content.length == 0 ) {
            [PDHUD showHUDSuccess:@"标题或内容不能为空！"];
            return;
        }
        
        [PDHUD showHUDProgress:@"正在发布，请稍后..." diary:0];
        if ([contentCell getImages].count) {
            [weakSelf fetchPostImagesWithImageArr:[contentCell getImages]];
        } else {
            [weakSelf fetchPostMessageWithTitle:title content:content pictures:nil];
        }
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)setupView {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    self.mainTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:self.mainTableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        NSString *cellId = @"postTitleCellId";
        PDEditInfoTableViewCell *titleCell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!titleCell) {
            titleCell = [[PDEditInfoTableViewCell alloc] initWithTextFont:[UIFont systemFontOfCustomeSize:15] reuseIdentifier:cellId];
            titleCell.textField.placeholder = @"标题(选填，最多36个字符)";
            titleCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return titleCell;
    } else if (indexPath.row == 1) {
        NSString *cellId = @"postContentCellId";
        PDPostMessageContentCell *contentCell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!contentCell) {
            contentCell = [[PDPostMessageContentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            contentCell.selectionStyle = UITableViewCellSelectionStyleNone;
            self.contentCell = contentCell;
        }
        
        __weak typeof(self) weakSelf = self;
        [contentCell textViewDidEditWithText:^(NSString *text, NSUInteger length) {
            [weakSelf.limitCell setTextLength:length];
        }];
        
        return contentCell;
    } else {
        NSString *cellId = @"postLimitCellId";
        PDPostMessageLimitCell *limitCell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!limitCell) {
            limitCell = [[PDPostMessageLimitCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            limitCell.selectionStyle = UITableViewCellSelectionStyleNone;
            self.limitCell = limitCell;
        }
        
        __weak typeof(self) weakSelf = self;
        [limitCell didClickMoreButton:^{
            [weakSelf openPhotos];
        }];
        return limitCell;
    }
}

/**
 * 打开相册
 */
- (void)openPhotos {
    __weak typeof(self) weakSelf = self;
    GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc]init];
    [assetVC selectImageArrayFromImagePickerWithMaxSelected:9 andBlock:^(NSArray *selectedImgArr) {
        if (selectedImgArr.count == 0) {
            return;
        }
        
        [weakSelf.contentCell addImages:selectedImgArr];
    }];
    
    [self pushViewController:assetVC animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return [PDEditInfoTableViewCell cellHeight];
    } else if (indexPath.row == 1) {
        return [PDPostMessageContentCell cellHeight];
    } else {
        return [PDPostMessageLimitCell cellHeight];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (void)postMessageSuccess:(void (^)())successBlock {
    if (successBlock) {
        self.postSuccessBlock = successBlock;
    }
}

#pragma mark - 网络请求

- (void)fetchPostMessageWithTitle:(NSString *)title content:(NSString *)content pictures:(NSString *)pictures {
    
    if (!title) title = @"";
    if (!content) content = @"";
    if (!pictures) pictures = @"";
    
    NSString *gameType = @"";
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        gameType = @"lol";
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        gameType = @"dota2";
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        gameType = @"king";
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeCS) {
        gameType = @"cs";
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePubg) {
        gameType = @"pubg";
    }
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postMessage] requestParams:@{@"postTitle":title, @"postContent":content, @"pictures": pictures, @"gameType":gameType} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        [PDHUD dismiss];
        if (isSucceeded) {
            if (weakSelf.postSuccessBlock) {
                weakSelf.postSuccessBlock();
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)fetchPostImagesWithImageArr:(NSArray *)imageArr {
    NSMutableArray *fileArr = [NSMutableArray arrayWithCapacity:imageArr.count];
    for (int i = 0; i < imageArr.count; i++) {
        UIImage *image = imageArr[i];
        FetchFileModel *file = [[FetchFileModel alloc] init];
        file.uploadImage = image;
        [fileArr addObject:file];
    }
    FetchModel *fileModel = [[FetchModel alloc] init];
    fileModel.requestFileDataArr = [fileArr copy];
    __weak typeof(self)weakSelf = self;
    [fileModel fetchWithPath:[NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,postImages] completionHandler:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return;
        }
        
        [PDHUD dismiss];
        if (isSucceeded){
            NSArray *images = responseObject[@"image"];
            PDEditInfoTableViewCell *titleCell = [weakSelf.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
            PDPostMessageContentCell *contentCell = [weakSelf.mainTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:0]];
            NSString *title = titleCell.textField.text;
            NSString *content = contentCell.textView.text;
            
            NSMutableString *imageString = [NSMutableString string];
            for (int i = 0;i < images.count; i++) {
                NSString *img = images[i];
                NSString *appendStr = @"";
                if (i == 0) {
                    appendStr = img;
                } else {
                    appendStr = [NSString stringWithFormat:@",%@",img];
                }
                [imageString appendString:appendStr];
            }
            [weakSelf fetchPostMessageWithTitle:title content:content pictures:imageString];
        }
    }];
}

@end
