//
//  PDPostBarShowImgListViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/12.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarShowImgListViewController.h"

@interface PDPostBarShowImgListViewController()<UIScrollViewDelegate>
@property (nonatomic,weak)UIViewController *showViewController;                 /**< 显示的控制器*/
@property (nonatomic,strong)UIView *backView;                                   /**< 背景View*/
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UIPageControl *pageControl;
@property (nonatomic,strong)PDImageView *userAnimationImgView;;
@property (nonatomic,strong)NSArray *tempImgArr;
@property (nonatomic,assign)NSInteger tempCurrentIndex;
@property (nonatomic,strong)UIButton *downButton;
@property (nonatomic,strong)PDPostBarImageView *tempConvertView;
@property (nonatomic,strong)PDPostBarListContentCell *tempCell;
@end

@implementation PDPostBarShowImgListViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
}

-(void)pageSetting{
    if (!self.backView){
        self.backView = [[UIView alloc]init];
        self.backView.backgroundColor = [UIColor blackColor];
        self.backView.frame = self.view.bounds;
        self.backView.alpha = 1;
        self.backView.userInteractionEnabled = YES;
        [self.view addSubview:self.backView];
        // 2. 创建手势
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backView addGestureRecognizer:tapGestureRecognizer];
    }
    
    // 2. 创建scrollview
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        self.mainScrollView.backgroundColor = [UIColor blackColor];
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.layer.zPosition = MAXFLOAT;
        self.mainScrollView.delegate = self;
        self.mainScrollView.userInteractionEnabled = YES;
        self.mainScrollView.maximumZoomScale=2.0;
        self.mainScrollView.minimumZoomScale=0.5;
        self.mainScrollView.bounces = YES;
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.bouncesZoom = YES;
        self.mainScrollView.frame = kScreenBounds;
        [self.backView addSubview:self.mainScrollView];
    }
    // 2. 创建pagecontrol
    if (!self.pageControl){
        self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, kScreenBounds.size.height - LCFloat(11) * 3,kScreenBounds.size.width,LCFloat(11))];
        self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
        [self.view addSubview:self.pageControl];
    }
    
    // 3. 创建下载按钮
    if (!self.downButton){
        __weak typeof(self)weakSelf = self;
        self.downButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.downButton.frame = CGRectMake(LCFloat(11), kScreenBounds.size.height - LCFloat(11) - LCFloat(40), LCFloat(40), LCFloat(40));
        [self.view addSubview:self.downButton];
        self.downButton.backgroundColor = [UIColor clearColor];
        [self.downButton setImage:[UIImage imageNamed:@"icon_postBar_down"] forState:UIControlStateNormal];
        [self.downButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger currentIndex = self.mainScrollView.contentOffset.x / kScreenBounds.size.width;
            PDPostPicture *pictureModel = [self.tempImgArr objectAtIndex:currentIndex];
            NSString *imgUrl = pictureModel.pictureRoute;
            [strongSelf getNetImgSaveToCurrentWithUrl:imgUrl];
        }];

    }
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak PDPostBarShowImgListViewController *weakVC = self;
    
    self.backView.alpha = 0;
    self.mainScrollView.alpha = 0;
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
    // 1. 获取当前的index
    if (self.pageControl.currentPage > 2){
        [UIView animateWithDuration:.3f animations:^{
            
        } completion:^(BOOL finished) {
            [self.userAnimationImgView removeFromSuperview];
            self.userAnimationImgView = nil;
            [weakVC willMoveToParentViewController:nil];
            [weakVC.view removeFromSuperview];
            [weakVC removeFromParentViewController];
        }];
    } else {
        // 1. 找到当前的view
        PDImageView *targetImgView;
        if (self.pageControl.currentPage == 0){
            targetImgView = self.tempConvertView.imageView0;
        } else if (self.pageControl.currentPage == 1){
            targetImgView = self.tempConvertView.imageView1;
        } else if (self.pageControl.currentPage == 2){
            targetImgView = self.tempConvertView.imageView2;
        }
        // 1.
        self.userAnimationImgView.image = targetImgView.image;
        CGSize fromImgSize = [self convertToCalculationImg:self.userAnimationImgView.image];
        self.userAnimationImgView.frame = CGRectMake((kScreenBounds.size.width - fromImgSize.width) / 2., (kScreenBounds.size.height - fromImgSize.height) / 2., fromImgSize.width, fromImgSize.height);
        self.userAnimationImgView.alpha = 1;
        
        // 2. 寻找target frame
        CGRect targetRect = [self.tempConvertView convertRect:targetImgView.frame toView:self.view.window];
        
        [UIView animateWithDuration:.3f animations:^{
            self.userAnimationImgView.frame = targetRect;
        } completion:^(BOOL finished) {
            [self.userAnimationImgView removeFromSuperview];
            self.userAnimationImgView = nil;
            [weakVC willMoveToParentViewController:nil];
            [weakVC.view removeFromSuperview];
            [weakVC removeFromParentViewController];
        }];
    }
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    // 1. 计算将要变成的frame
    CGSize maxSize = [self convertToCalculationImg:self.userAnimationImgView.image];
    
    [UIView animateWithDuration:.3f animations:^{
        self.backView.alpha = 1;
        self.userAnimationImgView.frame = CGRectMake((kScreenBounds.size.width - maxSize.width) / 2., (kScreenBounds.size.height - maxSize.height ) / 2., maxSize.width, maxSize.height);
    } completion:^(BOOL finished) {
        [self addOtherImgView];
    }];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}


- (void)showInView:(UIViewController *)viewController imgArr:(NSArray *)imgArr currendIndex:(NSInteger)currentIndex convertView:(PDPostBarImageView *)convertView touchImgView:(PDImageView *)imgView cell:(PDPostBarListContentCell *)cell{
    [self pageSetting];
    
    // 1. pageControl
    self.pageControl.numberOfPages = imgArr.count;
    self.pageControl.currentPage = currentIndex;
    
    // 2. img
    self.tempImgArr = imgArr;
    CGRect convertImgFrame = [convertView convertRect:imgView.frame toView:self.view.window];
    
    // 3. scroll
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.tempImgArr.count, kScreenBounds.size.height);
    [self.mainScrollView setContentOffset:CGPointMake(currentIndex * kScreenBounds.size.width, 0)];
    
    // 4. temp
    self.tempCell = cell;
    self.tempConvertView = convertView;
    
    if (!self.userAnimationImgView){
        self.userAnimationImgView = [[PDImageView alloc]init];
        self.userAnimationImgView.backgroundColor = [UIColor clearColor];
        self.userAnimationImgView.frame = convertImgFrame;
        self.userAnimationImgView.userInteractionEnabled = NO;
        [self.view addSubview:self.userAnimationImgView];
        self.userAnimationImgView.image = imgView.image;
    }
    
    [self showInView:viewController];
}

-(CGSize)convertToCalculationImg:(UIImage *)image{
    UIImage *originalImage = [UIImage scaleDown:image withSize:CGSizeMake(self.view.size_width, self.view.size_width * image.size.height/image.size.width)];
    
    // 设置比例
    CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
    CGFloat screenRadio = CGRectGetWidth(self.view.window.frame)/CGRectGetHeight(self.view.window.frame);
    if (imageRadio >= screenRadio) {
        CGFloat currentImageHeight = CGRectGetWidth(self.view.window.frame)/imageRadio;
        self.mainScrollView.maximumZoomScale = CGRectGetHeight(self.view.window.frame)/currentImageHeight;
        return CGSizeMake(kScreenBounds.size.width, currentImageHeight);
    } else {
        CGFloat currentImageWidth = CGRectGetWidth(self.view.window.frame)*imageRadio;
        self.mainScrollView.maximumZoomScale = CGRectGetWidth(self.view.window.frame)/currentImageWidth;
        return CGSizeMake(currentImageWidth, kScreenBounds.size.height);
    }
}

-(void)addOtherImgView{
    //    // 1. 创建多张图片
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < self.tempImgArr.count;i++){
        PDImageView *originalImageView = [[PDImageView alloc] init];
        originalImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.mainScrollView addSubview:originalImageView];
        PDPostPicture *pictureModel = [self.tempImgArr objectAtIndex:i];
        NSString *imgUrl = pictureModel.pictureRoute;
        [originalImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithPostUrl:imgUrl] placeholder:nil callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            UIImage *originalImage = [UIImage scaleDown:image withSize:CGSizeMake(strongSelf.view.size_width, strongSelf.view.size_width * image.size.height/image.size.width)];
            originalImageView.image = originalImage;
            
            // 设置比例
            CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
            CGFloat screenRadio = CGRectGetWidth(strongSelf.view.window.frame)/CGRectGetHeight(strongSelf.view.window.frame);
            if (imageRadio >= screenRadio) {
                CGFloat currentImageHeight = CGRectGetWidth(strongSelf.view.window.frame)/imageRadio;
                self.mainScrollView.maximumZoomScale = CGRectGetHeight(strongSelf.view.window.frame)/currentImageHeight;
            } else {
                CGFloat currentImageWidth = CGRectGetWidth(strongSelf.view.window.frame)*imageRadio;
                self.mainScrollView.maximumZoomScale = CGRectGetWidth(strongSelf.view.window.frame)/currentImageWidth;
            }
            
            if (i == strongSelf.tempCurrentIndex){
                strongSelf.userAnimationImgView.alpha = 0;
            }
        }];
        originalImageView.frame =   kScreenBounds;
        originalImageView.orgin_x = kScreenBounds.size.width * i;
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.mainScrollView){
        NSInteger index = self.mainScrollView.contentOffset.x / kScreenBounds.size.width;
        self.pageControl.currentPage = index;
    }
}

#pragma mark - 获取网络图片保存到本地
-(void)getNetImgSaveToCurrentWithUrl:(NSString *)urlString{
    NSURL * url = [NSURL URLWithString:urlString];
    NSData * data = [[NSData alloc]initWithContentsOfURL:url];
    UIImage *image = [[UIImage alloc]initWithData:data];
    [self saveImageToPhotos:image];
}

- (void)saveImageToPhotos:(UIImage*)savedImage {
    UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}

- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo {
    [PDHUD showHUDSuccess:@"图片下载成功"];
}

@end
