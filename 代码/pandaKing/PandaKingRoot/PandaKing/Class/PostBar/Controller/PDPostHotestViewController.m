//
//  PDPostHotestViewController.m
//  PandaKing
//
//  Created by Cranz on 17/6/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostHotestViewController.h"
#import "PDPostBarListContentCell.h"
#import "PDPostBarList.h"
#import "PDPostDetailViewController.h"
#import "PDPostBarShowImgListViewController.h"

@interface PDPostHotestViewController ()<UITableViewDataSource, UITableViewDelegate, PDPostBarListContentCellDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, strong) PDPostBarList *hotList;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@end

@implementation PDPostHotestViewController

- (NSMutableArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _page = 1;
    [self setupTableView];
    [self fetchList];
}

- (void)setupTableView {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, self.view.bounds.size.height - 49 - 44 - 64) style:UITableViewStyleGrouped];
//    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.itemsArr.count) {
            [weakSelf.itemsArr removeAllObjects];
        }
        [weakSelf fetchList];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        weakSelf.page++;
        if (weakSelf.hotList.hasNextPage) {
            [weakSelf fetchList];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            [weakSelf.mainTableView stopPullToRefresh];
        }
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.itemsArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PDPostBarListContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"postBarList_cell"];
    if (!cell) {
        cell = [[PDPostBarListContentCell alloc] initWithReuseIdentifier:@"postBarList_cell"];
        cell.delegate = self;
    }
    
    if (self.itemsArr.count > indexPath.section) {
        PDPostBarItem *item = self.itemsArr[indexPath.section];
        cell.model = item;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemsArr.count > indexPath.section) {
        PDPostBarItem *item = self.itemsArr[indexPath.section];
        return [PDPostBarListContentCell cellHeightWithModel:item];
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.itemsArr.count > indexPath.section) {
        PDPostBarItem *item = self.itemsArr[indexPath.section];
        [self goPostDetailWithItem:item];
    }
}

#pragma mark - 去帖子详情

- (void)goPostDetailWithItem:(PDPostBarItem *)item {
    PDPostDetailViewController *detailViewController = [[PDPostDetailViewController alloc] init];
    detailViewController.postId = item.postId;
    detailViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:detailViewController animated:YES];
}

#pragma mark - PDPostBarListContentCellDelegate

-(void)cell:(PDPostBarListContentCell *)cell convertView:(PDPostBarImageView *)convertView imgList:(NSArray *)imgArr didTouchWithImage:(PDImageView *)imageView atIndex:(NSUInteger)index{
    PDPostBarShowImgListViewController *viewController = [[PDPostBarShowImgListViewController alloc]init];
    [viewController showInView:self.parentViewController imgArr:imgArr currendIndex:index convertView:convertView touchImgView:imageView cell:cell];
}

- (void)cell:(PDPostBarListContentCell *)cell didTouchWithPraise:(UIButton *)praiseButton {
    [self fetchPraise:!praiseButton.selected postItem:cell.model praiseButton:praiseButton cell:cell];
}

- (void)cell:(PDPostBarListContentCell *)cell didTouchWithChat:(UIButton *)chatButton {
    [self goPostDetailWithItem:cell.model];
}

#pragma mark - 网络请求

- (void)fetchList {
    NSString *gameType = @"";
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        gameType = @"lol";
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        gameType = @"dota2";
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        gameType = @"king";
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeCS) {
        gameType = @"cs";
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePubg) {
        gameType = @"pubg";
    }
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postBarLists] requestParams:@{@"currentPage":@(_page),@"pageSize":@5,@"name":@"hot",@"gameType":gameType} responseObjectClass:[PDPostBarList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            weakSelf.hotList = (PDPostBarList *)responseObject;
            [weakSelf.itemsArr addObjectsFromArray:weakSelf.hotList.items];
            
            [weakSelf.mainTableView reloadData];
            if (weakSelf.itemsArr.count == 0) {
                [weakSelf showNoDataPage];
            } else {
                [weakSelf.view dismissPrompt];
            }
        }
        
        [weakSelf.mainTableView stopFinishScrollingRefresh];
        [weakSelf.mainTableView stopPullToRefresh];
    }];
}

// praise:YES 点赞  NO 取消
- (void)fetchPraise:(BOOL)praise postItem:(PDPostBarItem *)item praiseButton:(UIButton *)button cell:(PDPostBarListContentCell *)cell {
    __weak typeof(self) weakSelf = self;
    NSDictionary *param;
    if (praise) {
        param = @{@"postId":@(item.postId),@"name":@"zan"};
    } else {
        param = @{@"postId":@(item.postId),@"name":@""};
    }

    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postPraise] requestParams:param responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            button.selected = !button.selected;
        }
        
        NSUInteger priseCount = button.tag;
        if (praise == YES) {
            ++priseCount;
        } else {
            --priseCount;
        }
        button.tag = priseCount;
        [button setTitle:[NSString stringWithFormat:@"%@",[PDCenterTool numberStringChangeWithoutSpecialCharacter:priseCount]] forState:UIControlStateNormal];
        [cell updatePriseButton];
    }];
}

- (void)showNoDataPage {
    [self.view showPrompt:@"暂无任何野区内容！" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:0 tapBlock:NULL];
}

- (void)updateToLastest {
    self.page = 1;
    if (self.itemsArr.count) {
        [self.itemsArr removeAllObjects];
    }
    
    [self fetchList];
}

@end
