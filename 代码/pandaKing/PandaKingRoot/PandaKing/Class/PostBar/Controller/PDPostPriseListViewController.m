//
//  PDPostPriseListViewController.m
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostPriseListViewController.h"
#import "PDPostDetailPriseListCell.h"

@interface PDPostPriseListViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@end

@implementation PDPostPriseListViewController

- (NSMutableArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.barMainTitle = @"点赞列表";
    [self setupTableView];
    [self fetchList];
}

- (void)setupTableView {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.backgroundColor = [UIColor whiteColor];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PDPostDetailPriseListCell *listCell = [tableView dequeueReusableCellWithIdentifier:@"listCellId"];
    if (!listCell) {
        listCell = [[PDPostDetailPriseListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"listCellId"];
    }
    listCell.model = @"";
    return listCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PDPostDetailPriseListCell cellHeight];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - 网络请求

- (void)fetchList {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDCenterTool absoluteUrlWithPostUrl:postPriseList] requestParams:@{@"postId":@(self.postId)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            
        }
    }];
}

@end
