//
//  PDPostBarShowImgListViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/6/12.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDPostBarImageView.h"
#import "PDPostBarListContentCell.h"

@interface PDPostBarShowImgListViewController : UIViewController

@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势

- (void)showInView:(UIViewController *)viewController imgArr:(NSArray *)imgArr currendIndex:(NSInteger)currentIndex convertView:(PDPostBarImageView *)convertView touchImgView:(PDImageView *)imgView cell:(PDPostBarListContentCell *)cell;
- (void)dismissFromView:(UIViewController *)viewController;

@end
