//
//  PDPostDetailReplyViewController.h
//  PandaKing
//
//  Created by Cranz on 17/6/12.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDPostDetailReplyModel.h"

/// 显示楼中楼详细
@interface PDPostDetailReplyViewController : AbstractViewController
@property (nonatomic, strong) PDPostDetailReplyModel *model;

- (void)didDeleteReplyBlock:(void(^)(PDPostDetailReplyModel *model))block;
- (void)didUpdateModelBlock:(void(^)(PDPostDetailReplyModel *model))block;

@end
