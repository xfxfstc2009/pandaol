//
//  PDPostMessageLimitCell.h
//  PandaKing
//
//  Created by Cranz on 17/5/15.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDPostMessageLimitCell : UITableViewCell
+ (CGFloat)cellHeight;

- (void)didClickMoreButton:(void(^)())actions;

- (void)setTextLength:(NSUInteger)length;
@end
