//
//  PDPostBarListContentCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarUserCell.h"
#import "PDPostBarItem.h"
#import "PDPostBarImageView.h"

/// 野区列表cell
@protocol PDPostBarListContentCellDelegate;
@interface PDPostBarListContentCell : PDPostBarUserCell
@property (nonatomic, strong) PDPostBarItem *model;
@property (nonatomic, weak) id<PDPostBarListContentCellDelegate> delegate;
@property (nonatomic, strong) PDPostBarImageView *postImageView;
+ (CGFloat)cellHeightWithModel:(PDPostBarItem *)model;
/**
 * 更新点赞按钮的frame
 */
- (void)updatePriseButton;
@end

@protocol PDPostBarListContentCellDelegate <NSObject>
@optional
- (void)cell:(PDPostBarListContentCell *)cell didTouchWithImageView:(UIImageView *)imageView atIndex:(NSUInteger)index;
- (void)cell:(PDPostBarListContentCell *)cell didTouchWithPraise:(UIButton *)praiseButton;
- (void)cell:(PDPostBarListContentCell *)cell didTouchWithChat:(UIButton *)chatButton;
- (void)cell:(PDPostBarListContentCell *)cell convertView:(PDPostBarImageView *)convertView imgList:(NSArray *)imgArr didTouchWithImage:(PDImageView *)imageView atIndex:(NSUInteger)index;
@end