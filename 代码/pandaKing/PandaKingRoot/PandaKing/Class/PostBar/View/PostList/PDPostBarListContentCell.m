//
//  PDPostBarListContentCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarListContentCell.h"
#import "PDPostBarImageView.h"
#import "PDCenterLabel.h"

#define kTITLE_FONT [UIFont systemFontOfCustomeSize:16]
#define kCONTENT_FONT [UIFont systemFontOfCustomeSize:13]

@interface PDPostBarListContentCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
/**
 * 置顶label
 */
@property (nonatomic, strong) PDCenterLabel *stickLabel;
/**
 * 竞猜   分类
 */
@property (nonatomic, strong) UILabel *typeLabel;
@property (nonatomic, strong) UIButton *praiseButton;
@property (nonatomic, strong) UIButton *chatButton;
@end

@implementation PDPostBarListContentCell

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    [self setupContentCell];
    return self;
}

- (void)setupContentCell {
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = kTITLE_FONT;
    self.titleLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.titleLabel];
    
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font = kCONTENT_FONT;
    self.contentLabel.numberOfLines = 2;
    self.contentLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.contentLabel];
    
    self.postImageView = [[PDPostBarImageView alloc] init];
    [self.contentView addSubview:self.postImageView];
    __weak typeof(self) weakSelf = self;
    [self.postImageView imageViewDidTouchActions:^(NSUInteger index, PDImageView *imageView) {
        if ([weakSelf.delegate respondsToSelector:@selector(cell:convertView:imgList:didTouchWithImage:atIndex:)]){
            [weakSelf.delegate cell:weakSelf convertView:weakSelf.postImageView imgList:weakSelf.model.pictures didTouchWithImage:imageView atIndex:index];
        }
        if ([weakSelf.delegate respondsToSelector:@selector(cell:didTouchWithImageView:atIndex:)]) {
            [weakSelf.delegate cell:weakSelf didTouchWithImageView:imageView atIndex:index];
        }
    }];
    
    self.stickLabel = [[PDCenterLabel alloc] init];
    self.stickLabel.textFont = [UIFont systemFontOfCustomeSize:13];
    self.stickLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.stickLabel];
    
//    self.typeLabel = [[UILabel alloc] init];
//    self.typeLabel.font = [UIFont systemFontOfCustomeSize:13];
//    self.typeLabel.textColor = [UIColor lightGrayColor];
//    [self.contentView addSubview:self.typeLabel];
    
    self.praiseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.praiseButton setImage:[UIImage imageNamed:@"icon_post_big_prised"] forState:UIControlStateSelected];
    [self.praiseButton setImage:[UIImage imageNamed:@"icon_post_unprised"] forState:UIControlStateNormal];
    self.praiseButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    self.praiseButton.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.praiseButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.contentView addSubview:self.praiseButton];
    [self.praiseButton addTarget:self action:@selector(didClickPraise:) forControlEvents:UIControlEventTouchUpInside];
    
    self.chatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.chatButton setImage:[UIImage imageNamed:@"icon_post_reply"] forState:UIControlStateNormal];
    self.chatButton.titleEdgeInsets = UIEdgeInsetsMake(0, 5, 0, 0);
    self.chatButton.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.chatButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.contentView addSubview:self.chatButton];
    [self.chatButton addTarget:self action:@selector(didClickChat:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didClickPraise:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(cell:didTouchWithPraise:)]) {
        [self.delegate cell:self didTouchWithPraise:button];
    }
}

- (void)didClickChat:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(cell:didTouchWithChat:)]) {
        [self.delegate cell:self didTouchWithChat:button];
    }
}

- (void)setModel:(PDPostBarItem *)model {
    _model = model;
    
    self.nameLabel.text = model.postUserName;
    if (model.admin == YES) {
        self.nameLabel.textColor = [UIColor redColor];
    } else {
        self.nameLabel.textColor = [UIColor blackColor];
    }
    self.levelLabel.level = model.level;
    self.dateLabel.text = [NSDate getTimeGap:model.postUpdateTime/1000];
    [self.headerIcon uploadImageWithAvatarURL:model.postUserHead placeholder:nil callback:nil];
    
    self.titleLabel.text = model.postTitle;
    self.contentLabel.text = model.postContent;
    NSMutableArray *images = [NSMutableArray arrayWithCapacity:model.pictures.count];
    for (PDPostPicture *piture in model.pictures) {
        NSString *imgUrl = [PDCenterTool absoluteUrlWithPostUrl:piture.pictureRoute];
        [images addObject:imgUrl];
    }
    self.postImageView.imageUrls = [images copy];

    if (model.postTop) {
        self.stickLabel.hidden = NO;
        self.stickLabel.image = [UIImage imageNamed:@"icon_post_strick"];
        self.stickLabel.text = @"置顶";
    } else {
        self.stickLabel.hidden = YES;
    }
    
//    self.typeLabel.text = @"#竞猜";
    
    self.praiseButton.selected = model.good;
    
    self.praiseButton.tag = model.postGood;
    [self.praiseButton setTitle:[NSString stringWithFormat:@"%@",[PDCenterTool numberStringChangeWithoutSpecialCharacter:model.postGood]] forState:UIControlStateNormal];
    [self.chatButton setTitle:[NSString stringWithFormat:@"%@",[PDCenterTool numberStringChangeWithoutSpecialCharacter:model.postReply]] forState:UIControlStateNormal];
    
    [self updateSubviews];
}

- (void)updateButtons {
    
    CGSize chatImageSize = self.chatButton.imageView.image.size;
    CGSize chatTextSize = [self.chatButton.titleLabel.text sizeWithCalcFont:self.chatButton.titleLabel.font];
    CGSize chatSize = CGSizeMake(chatImageSize.width + 5 + chatTextSize.width, chatTextSize.height + 10);
    self.chatButton.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - chatSize.width, CGRectGetMaxY(self.postImageView.frame) + ([NSString contentofHeightWithFont:kCONTENT_FONT] + 20 - chatSize.height) / 2, chatSize.width, chatSize.height);
    
    [self updatePriseButton];
}

- (void)updatePriseButton {
    CGSize praiseImageSize = self.praiseButton.imageView.image.size;
    CGSize praiseTextSize = [self.praiseButton.titleLabel.text sizeWithCalcFont:self.praiseButton.titleLabel.font];
    CGSize praiseSize = CGSizeMake(praiseImageSize.width + 5 + praiseTextSize.width, praiseTextSize.height + 10);
    self.praiseButton.frame = CGRectMake(CGRectGetMinX(self.chatButton.frame) - 15 - praiseSize.width, CGRectGetMinY(self.chatButton.frame), praiseSize.width, praiseSize.height);
}

- (void)updateSubviews {
    [super updateSubviews];
    
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2, [NSString contentofHeightWithFont:self.titleLabel.font])];
    CGSize contentSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2, 2 * [NSString contentofHeightWithFont:self.contentLabel.font])];
    CGSize stickSize = self.stickLabel.size;
//    CGSize typeSize = [self.typeLabel.text sizeWithCalcFont:self.typeLabel.font];
    
    self.titleLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.headerIcon.frame) + 10, titleSize.width, titleSize.height);
    self.contentLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.titleLabel.frame) + 10, contentSize.width, contentSize.height);
    
    // 有图片是加上图片的高度
    if (self.model.pictures.count) {
        self.postImageView.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.titleLabel.frame) + 10 + [NSString contentofHeightWithFont:self.contentLabel.font] * 2 + 10, kScreenBounds.size.width - kTableViewSectionHeader_left * 2, [PDPostBarImageView imageViewHeight]);
    } else {
        self.postImageView.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.titleLabel.frame) + 10 + [NSString contentofHeightWithFont:self.contentLabel.font] * 2, kScreenBounds.size.width - kTableViewSectionHeader_left * 2, 0);
    }
    
    self.stickLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.postImageView.frame) + 10, stickSize.width, stickSize.height);
    
//    if (self.model.postTop) {
//        self.typeLabel.frame = CGRectMake(CGRectGetMaxX(self.stickLabel.frame) + 15, CGRectGetMinY(self.stickLabel.frame), typeSize.width, typeSize.height);
//    } else {
//        self.typeLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMinY(self.stickLabel.frame), typeSize.width, typeSize.height);
//    }
    
    [self updateButtons];
}

+ (CGFloat)cellHeightWithModel:(PDPostBarItem *)model {
    if (model.pictures.count) {
        return [super cellHeight] + 10 + [NSString contentofHeightWithFont:kTITLE_FONT] + 10 + [NSString contentofHeightWithFont:kCONTENT_FONT] * 2 + 10 + [PDPostBarImageView imageViewHeight] + 10 + [NSString contentofHeightWithFont:kCONTENT_FONT];
    } else {
        return [super cellHeight] + 10 + [NSString contentofHeightWithFont:kTITLE_FONT] + 10 + [NSString contentofHeightWithFont:kCONTENT_FONT] * 2 + 10 + [NSString contentofHeightWithFont:kCONTENT_FONT];
    }
}

@end
