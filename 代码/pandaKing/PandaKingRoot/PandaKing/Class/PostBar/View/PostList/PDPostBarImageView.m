//
//  PDPostBarImageView.m
//  PandaKing
//
//  Created by Cranz on 17/6/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarImageView.h"

#define kIMAGE_WIDTH ((kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - 4) / 3)

@interface PDPostBarImageView ()
@property (nonatomic, copy) void(^imageTouchActions)(NSUInteger, PDImageView *);
@end

@implementation PDPostBarImageView

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    [self setupImageView];
    return self;
}

- (void)setupImageView {
    self.imageView0 = [self createImageView];
    self.imageView0.tag = 999;
    self.imageView1 = [self createImageView];
    self.imageView1.tag = 1000;
    self.imageView2 = [self createImageView];
    self.imageView2.tag = 1001;
    
    
}

- (PDImageView *)createImageView {
    PDImageView *imageView = [[PDImageView alloc] init];
    imageView.userInteractionEnabled = YES;
    imageView.contentMode = UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = YES;
    imageView.hidden = YES;
    [self addSubview:imageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTouchWithImage:)];
    [imageView addGestureRecognizer:tap];
    return imageView;
}

- (void)layoutSubviews {
    CGFloat imageWidth = kIMAGE_WIDTH;
    self.imageView0.frame = CGRectMake(0, 0, imageWidth, imageWidth);
    self.imageView1.frame = CGRectMake(CGRectGetMaxX(self.imageView0.frame) + 2, 0, imageWidth, imageWidth);
    self.imageView2.frame = CGRectMake(CGRectGetMaxX(self.imageView1.frame) + 2, 0, imageWidth, imageWidth);
}

- (void)imageViewDidTouchActions:(void (^)(NSUInteger, PDImageView *))actions {
    if (actions) {
        self.imageTouchActions = actions;
    }
}

- (void)didTouchWithImage:(UITapGestureRecognizer *)tap {
    PDImageView *imageView = (PDImageView *)tap.view;
    if (self.imageTouchActions) {
        self.imageTouchActions((imageView.tag - 999), imageView);
    }
}

- (void)setImageUrls:(NSArray *)imageUrls {
    _imageUrls = imageUrls;
    for (int i = 0; i < imageUrls.count; i++) {
        NSString *url = imageUrls[i];
        if (i == 0) {
            self.imageView0.hidden = NO;
            [self.imageView0 uploadImageWithURL:[PDCenterTool absoluteUrlWithPostUrl:url] placeholder:nil callback:nil];
        } else if (i == 1) {
            self.imageView1.hidden = NO;
            [self.imageView1 uploadImageWithURL:[PDCenterTool absoluteUrlWithPostUrl:url] placeholder:nil callback:nil];
        } else if (i == 2) {
            self.imageView2.hidden = NO;
            [self.imageView2 uploadImageWithURL:[PDCenterTool absoluteUrlWithPostUrl:url] placeholder:nil callback:nil];
        }
    }
    
    if (imageUrls.count == 0) {
        self.imageView0.hidden = YES;
        self.imageView1.hidden = YES;
        self.imageView2.hidden = YES;
    } else if (imageUrls.count == 1) {
        self.imageView1.hidden = YES;
        self.imageView2.hidden = YES;
    } else if (imageUrls.count == 2) {
        self.imageView2.hidden = YES;
    }
}

+ (CGFloat)imageViewHeight {
    return kIMAGE_WIDTH;
}

@end
