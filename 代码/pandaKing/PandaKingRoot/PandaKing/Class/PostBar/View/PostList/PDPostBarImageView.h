//
//  PDPostBarImageView.h
//  PandaKing
//
//  Created by Cranz on 17/6/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDPostBarImageView : UIView
@property (nonatomic, strong) PDImageView *imageView0;
@property (nonatomic, strong) PDImageView *imageView1;
@property (nonatomic, strong) PDImageView *imageView2;
@property (nonatomic, strong) NSArray *imageUrls;
- (void)imageViewDidTouchActions:(void(^)(NSUInteger index, PDImageView *imageView))actions;
+ (CGFloat)imageViewHeight;
@end
