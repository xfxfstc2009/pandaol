//
//  PDPostBarUserCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPostLabel.h"

/// 野区列表用户信息展示基础cell
@interface PDPostBarUserCell : UITableViewCell
/**
 * 图标部分
 */
@property (nonatomic, strong) PDImageView *headerIcon;
/**
 * 名字标题部分
 */
@property (nonatomic, strong) UILabel *nameLabel;
/**
 * 时间部分
 */
@property (nonatomic, strong) UILabel *dateLabel;
/**
 * 等级部分
 */
@property (nonatomic, strong) PDPostLabel *levelLabel;

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;
- (void)updateSubviews;
+ (CGFloat)cellHeight;
@end
