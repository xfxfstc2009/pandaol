//
//  PDPostBarUserCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarUserCell.h"

#define kICON_WIDTH LCFloat(50)

@interface PDPostBarUserCell ()

@end

@implementation PDPostBarUserCell

+ (CGFloat)cellHeight {
    return kICON_WIDTH + 26;
}

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self __setupCell];
    }
    return self;
}

- (void)__setupCell {
    self.headerIcon = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 13, kICON_WIDTH, kICON_WIDTH)];
    self.headerIcon.contentMode = UIViewContentModeScaleAspectFill;
    self.headerIcon.layer.cornerRadius = kICON_WIDTH / 2;
    self.headerIcon.layer.masksToBounds = YES;
    self.headerIcon.layer.borderColor = c26.CGColor;
    self.headerIcon.layer.borderWidth = 1;
    [self.contentView addSubview:self.headerIcon];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.dateLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.dateLabel];
    
    self.levelLabel = [[PDPostLabel alloc] init];
    [self.contentView addSubview:self.levelLabel];
}

- (void)updateSubviews {
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake((kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - kICON_WIDTH - 60), [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGSize levelSize = self.levelLabel.size;
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerIcon.frame) + 10, CGRectGetMinY(self.headerIcon.frame) + 3, nameSize.width, nameSize.height);
    self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - levelSize.height, levelSize.width, levelSize.height);
    self.dateLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + 5, dateSize.width, dateSize.height);
}

@end
