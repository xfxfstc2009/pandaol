//
//  PDPostLabel.h
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDPostLabel : UILabel
@property (nonatomic) CGSize textSize;
@property (nonatomic) NSUInteger level;
@end
