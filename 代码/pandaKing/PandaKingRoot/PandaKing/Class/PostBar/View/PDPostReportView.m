//
//  PDPostReportView.m
//  PandaKing
//
//  Created by Cranz on 17/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostReportView.h"
#import "PDTextView.h"

#define kPOSTREPORT_WIDTH 280
#define kPOSTTITLE_HEIGHT 40
#define kPOSTBUTTON_HEIGHT 40
#define kPOSTCONTENT_HEIGHT 120

@interface PDPostReportView ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) PDTextView *textView;
@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, copy) void(^cancleBlock)();
@property (nonatomic, copy) void(^submitBlock)(NSString *);
@end

@implementation PDPostReportView

- (instancetype)initWithTitle:(NSString *)title {
    self = [super initWithFrame:CGRectMake(0, 0, kPOSTREPORT_WIDTH, kPOSTCONTENT_HEIGHT + kPOSTTITLE_HEIGHT + kPOSTBUTTON_HEIGHT)];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupViewWithTitle:title];
    }
    return self;
}

- (void)setupViewWithTitle:(NSString *)title {
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:17];
    self.titleLabel.text = title;
    [self addSubview:self.titleLabel];
    [self updateTitleLabel];
    
    self.textView = [[PDTextView alloc] init];
    self.textView.frame = CGRectMake(kTableViewSectionHeader_left, kPOSTTITLE_HEIGHT, kPOSTREPORT_WIDTH - kTableViewSectionHeader_left * 2, kPOSTCONTENT_HEIGHT - 10);
    self.textView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.textView.font = [UIFont systemFontOfCustomeSize:13];
    self.textView.place = @"请输入举报理由...";
    self.textView.limitMax = 999;
    [self addSubview:self.textView];
    
    self.buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, kPOSTCONTENT_HEIGHT + kPOSTTITLE_HEIGHT, kPOSTREPORT_WIDTH, kPOSTBUTTON_HEIGHT)];
    [self addSubview:self.buttonView];
    for (int i = 0; i < 2; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        [button setFrame:CGRectMake(i * (kPOSTREPORT_WIDTH / 2), 0, kPOSTREPORT_WIDTH / 2, kPOSTBUTTON_HEIGHT)];
        [button setTag:i + 999];
        if (i == 0) {
            [button setTitle:@"取消" forState:UIControlStateNormal];
        } else {
            [button setTitle:@"确定" forState:UIControlStateNormal];
        }
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
        [PDCenterTool setButtonBackgroundEffect:button];
        [self.buttonView addSubview:button];
        [button addTarget:self action:@selector(didButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kPOSTREPORT_WIDTH, 1)];
    hLine.backgroundColor = c27;
    [self.buttonView addSubview:hLine];
    
    UIView *vLine = [[UIView alloc] initWithFrame:CGRectMake(kPOSTREPORT_WIDTH / 2, 0, 1, kPOSTBUTTON_HEIGHT)];
    vLine.backgroundColor = hLine.backgroundColor;
    [self.buttonView addSubview:vLine];
}

- (void)updateTitleLabel {
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font];
    self.titleLabel.frame = CGRectMake((kPOSTREPORT_WIDTH - titleSize.width) / 2, (kPOSTTITLE_HEIGHT - titleSize.height) / 2, titleSize.width, titleSize.height);
}

- (void)didButtonAction:(UIButton *)button {
    if ((button.tag - 999) == 0) {
        if (self.cancleBlock) {
            self.cancleBlock();
        }
    } else {
        if (self.submitBlock) {
            self.submitBlock(self.textView.text);
        }
    }
}

- (void)didClickCancleActions:(void (^)())actions {
    if (actions) {
        self.cancleBlock = actions;
    }
}

- (void)didClickSubmitActions:(void (^)(NSString *))actions {
    if (actions) {
        self.submitBlock = actions;
    }
}

@end
