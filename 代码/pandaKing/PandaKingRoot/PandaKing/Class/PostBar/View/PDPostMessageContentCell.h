//
//  PDPostMessageContentCell.h
//  PandaKing
//
//  Created by Cranz on 17/5/15.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDTextView.h"
/// 发帖输入和选图片的cell
@interface PDPostMessageContentCell : UITableViewCell
@property (nonatomic, strong) PDTextView *textView;

+ (CGFloat)cellHeight;
- (void)textViewDidEditWithText:(void(^)(NSString *text, NSUInteger length))block;
- (void)addImages:(NSArray *)images;
/**
 * 提示所选图片超过限制
 */
- (void)alertImageTooMuch:(void(^)())alert;

- (NSArray *)getImages;
@end
