//
//  PDPostLabel.m
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostLabel.h"

@implementation PDPostLabel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupLabel];
    }
    return self;
}

- (void)setupLabel {
    self.textColor = [UIColor whiteColor];
    self.font = [UIFont systemFontOfCustomeSize:11];
    self.textAlignment = NSTextAlignmentCenter;
    self.layer.cornerRadius = 2;
    self.layer.masksToBounds = YES;
    self.backgroundColor = c26;
}

- (void)setLevel:(NSUInteger)level {
    _level = level;
    
    self.text = [NSString stringWithFormat:@"Lv.%@",@(level)];
}

- (CGSize)textSize {
    CGSize t_size = [self.text sizeWithCalcFont:self.font];
    return CGSizeMake(t_size.width + 6, 2 + t_size.height);
}

- (CGSize)size {
    CGSize t_size = [self.text sizeWithCalcFont:self.font];
    return CGSizeMake(t_size.width + 6, 2 + t_size.height);
}

@end
