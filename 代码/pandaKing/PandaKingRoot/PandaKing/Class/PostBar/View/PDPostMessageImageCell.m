//
//  PDPostMessageImageCell.m
//  PandaKing
//
//  Created by Cranz on 17/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostMessageImageCell.h"

@interface PDPostMessageImageCell ()
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIButton *deleteButton;
@property (nonatomic, strong) void(^deleteBlock)(PDPostMessageImageCell *, UIImage *);
@end

@implementation PDPostMessageImageCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupcell];
    }
    return self;
}

- (void)setupcell {
    self.imageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.imageView.userInteractionEnabled = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.clipsToBounds = YES;
    [self.contentView addSubview:self.imageView];
    
    self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *deleteImage = [UIImage imageNamed:@"icon_post_delete"];
    [self.deleteButton setBackgroundImage:deleteImage forState:UIControlStateNormal];
    self.deleteButton.frame = CGRectMake(self.bounds.size.width - deleteImage.size.width - 2, 2, deleteImage.size.width, deleteImage.size.height );
    [self addSubview:self.deleteButton];
    [self.deleteButton addTarget:self action:@selector(didClickDelete) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setImage:(UIImage *)image {
    _image = image;
    
    self.imageView.image = image;
}

- (void)didClickDelete {
    if (self.deleteBlock) {
        self.deleteBlock(self, self.image);
    }
}

- (void)didDeleteCellActions:(void (^)(PDPostMessageImageCell *, UIImage *))actions {
    if (actions) {
        self.deleteBlock = actions;
    }
}

@end
