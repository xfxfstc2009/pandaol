//
//  PDPostDetailPictureCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PDPostDetailPictureCellDelegate;
@interface PDPostDetailPictureCell : UITableViewCell
@property (nonatomic, strong) NSArray *urls;
@property (nonatomic, weak) id<PDPostDetailPictureCellDelegate> delegate;

+ (CGFloat)cellHeightWithUrlArr:(NSArray *)urls;
@end

@protocol PDPostDetailPictureCellDelegate <NSObject>

- (void)pictureCell:(PDPostDetailPictureCell *)cell didClickImgaeView:(PDImageView *)imageView atIndex:(NSInteger)index;

@end