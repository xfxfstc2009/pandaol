//
//  PDPostDetailCommentNavigationCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 全部评论 只看楼主
@interface PDPostDetailCommentNavigationCell : UITableViewCell
+ (CGFloat)cellHeight;

// 0:全部评论，1:只看楼主
- (void)didChangeSelected:(void(^)(NSUInteger index))selBlock;

/**
 * 设置全部评论的数量
 */
@property (nonatomic) NSUInteger commentCount;
/**
 * 选中的是哪个
 */
@property (nonatomic, readonly) int selIndex;
@end
