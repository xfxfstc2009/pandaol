//
//  PDPostDetailReplyFloorCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/12.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPostDetailReplyItem.h"

/// 盖楼详情界面中的楼中楼cell
@protocol PDPostDetailReplyFloorCellDelegate;
@interface PDPostDetailReplyFloorCell : UITableViewCell
@property (nonatomic, strong) PDPostDetailReplyItem *item;
@property (nonatomic, weak) id<PDPostDetailReplyFloorCellDelegate> delegate;

+ (CGFloat)cellHeightWithItem:(PDPostDetailReplyItem *)item;
@end

@protocol PDPostDetailReplyFloorCellDelegate <NSObject>

- (void)floorCell:(PDPostDetailReplyFloorCell *)cell didClickReplyUserNameWithUserId:(NSString *)userId;

@end