//
//  PDPostDetailReplyBasicCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailReplyBasicCell.h"
#import "PDPostLabel.h"
#import "PDPostDetailReplyToReplyCell.h"

#define kICON_WIDTH LCFloat(38)
#define kCONTENT_FONT [UIFont systemFontOfCustomeSize:15]

@interface PDPostDetailReplyBasicCell ()<PDPostDetailReplyToReplyCellDelegate>
@property (nonatomic, strong) PDImageView *headerIcon;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) PDPostLabel *levelLabel;
@property (nonatomic, strong) PDPostLabel *isHostLabel;
@property (nonatomic, strong) UIButton *praiseButton;
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *floorLabel;
/**
 * 发表评论的
 */
@property (nonatomic, strong) PDImageView *commentImageView;
/**
 * 楼中楼View
 */
@property (nonatomic, strong) PDPostDetailReplyToReplyCell *replyCell;
/**
 * 楼中楼的回复数组
 */
@property (nonatomic, strong) NSMutableArray *replysArr;
@end

@implementation PDPostDetailReplyBasicCell

- (NSMutableArray *)replysArr {
    if (!_replysArr) {
        _replysArr = [NSMutableArray array];
    }
    return _replysArr;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setupCell];
    return self;
}

- (void)setupCell {
    self.headerIcon = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 10, kICON_WIDTH, kICON_WIDTH)];
    self.headerIcon.layer.cornerRadius = kICON_WIDTH / 2;
    self.headerIcon.layer.masksToBounds = YES;
    [self.contentView addSubview:self.headerIcon];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    self.isHostLabel = [[PDPostLabel alloc] init];
    self.isHostLabel.text = @"楼主";
    self.isHostLabel.hidden = YES;
    [self.contentView addSubview:self.isHostLabel];
    
    self.levelLabel = [[PDPostLabel alloc] init];
    [self.contentView addSubview:self.levelLabel];
    
    self.praiseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.praiseButton setImage:[UIImage imageNamed:@"icon_post_big_prised"] forState:UIControlStateSelected];
    [self.praiseButton setImage:[UIImage imageNamed:@"icon_post_unprised"] forState:UIControlStateNormal];
    self.praiseButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    self.praiseButton.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.praiseButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.contentView addSubview:self.praiseButton];
    [self.praiseButton addTarget:self action:@selector(didClickPraise:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *moreImage = [UIImage imageNamed:@"icon_post_sel_more"];
    self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.moreButton setImage:moreImage forState:UIControlStateNormal];
    [self.contentView addSubview:self.moreButton];
    [self.moreButton addTarget:self action:@selector(didClickMore:) forControlEvents:UIControlEventTouchUpInside];
    
    self.floorLabel = [[UILabel alloc] init];
    self.floorLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.floorLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.floorLabel];
    
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.dateLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.dateLabel];
    
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font = kCONTENT_FONT;
    self.contentLabel.textColor = [UIColor blackColor];
    self.contentLabel.numberOfLines = 0;
    [self.contentView addSubview:self.contentLabel];
    
    // 发表的图片
    self.commentImageView = [[PDImageView alloc] init];
    self.commentImageView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.commentImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didclickImageView:)];
    [self.commentImageView addGestureRecognizer:tap];
    
    
    self.replyCell = [[PDPostDetailReplyToReplyCell alloc] initWithConstraintWidth:kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - kICON_WIDTH - 10];
    self.replyCell.delegate = self;
    self.replyCell.hidden = YES;
    [self.contentView addSubview:self.replyCell];
}

- (void)didclickImageView:(UITapGestureRecognizer *)tap {
    PDImageView *imageView = (PDImageView *)tap.view;
    if ([self.delegate respondsToSelector:@selector(cell:didClickImageView:)]) {
        [self.delegate cell:self didClickImageView:imageView];
    }
}

- (void)didClickPraise:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(cell:didClickPriseButton:)]) {
        [self.delegate cell:self didClickPriseButton:button];
    }
}

- (void)didClickMore:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(cell:didClickMoreButton:)]) {
        [self.delegate cell:self didClickMoreButton:button];
    }
}

- (void)setModel:(PDPostDetailReplyModel *)model {
    _model = model;
    
    [self.headerIcon uploadImageWithAvatarURL:model.postUserHead placeholder:nil callback:nil];
    
    [self.praiseButton setTitle:[PDCenterTool numberStringChangeWithoutSpecialCharacter:model.postGood] forState:UIControlStateNormal];
    self.praiseButton.tag = model.postGood;
    self.praiseButton.selected = model.good;
    [self updateButton];
    
    self.nameLabel.text = model.postUserName;
    if (model.admin == YES) {
        self.nameLabel.textColor = [UIColor redColor];
    } else {
        self.nameLabel.textColor = [UIColor blackColor];
    }
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerIcon.frame) + 10, CGRectGetMinY(self.headerIcon.frame) + 5, nameSize.width, nameSize.height);
    
    self.levelLabel.level = model.level;
    
    if (model.host == YES) {
        self.isHostLabel.hidden = NO;
        self.isHostLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - self.isHostLabel.size.height, self.isHostLabel.size.width, self.isHostLabel.size.height);
        self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.isHostLabel.frame) + 10, CGRectGetMinY(self.isHostLabel.frame), self.levelLabel.size.width, self.levelLabel.size.height);
    } else {
        self.isHostLabel.hidden = YES;
        self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - self.levelLabel.size.height, self.levelLabel.size.width, self.levelLabel.size.height);
    }
    
    // 楼
    self.floorLabel.text = [NSString stringWithFormat:@"第%@楼",@(model.floor)];
    CGSize floorSize = [self.floorLabel.text sizeWithCalcFont:self.floorLabel.font];
    self.floorLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + 5, floorSize.width, floorSize.height);
    
    // 时间
    self.dateLabel.text = [NSDate getTimeGap:model.postCreateTime / 1000];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
   self.dateLabel.frame = CGRectMake(CGRectGetMaxX(self.floorLabel.frame) + 5, CGRectGetMinY(self.floorLabel.frame), dateSize.width, dateSize.height);
    // 内容
    self.contentLabel.text = model.postContent;
    CGSize contentSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kICON_WIDTH - kTableViewSectionHeader_left * 2 - 10, CGFLOAT_MAX)];
    self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.headerIcon.frame) + 10, CGRectGetMaxY(self.headerIcon.frame) + 10, contentSize.width, contentSize.height);
    
    if (model.pictures.count) { // 有图片
        PDPostPicture *picture = [model.pictures firstObject];
        NSString *url = [PDCenterTool absoluteUrlWithPostUrl:picture.pictureRoute];
        [self.commentImageView uploadImageWithURL:url placeholder:nil callback:nil];
        CGSize imageSize = [PDCenterTool sizeWithImageUrl:url];
        self.commentImageView.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame), CGRectGetMaxY(self.contentLabel.frame) + 10, kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - 10 - kICON_WIDTH, (kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - 10 - kICON_WIDTH) / imageSize.width * imageSize.height);
        self.commentImageView.hidden = NO;
    } else {
        self.commentImageView.image = nil;
        self.commentImageView.hidden = YES;
    }
    
    if (model.replys.count == 0) {
        self.replyCell.hidden = YES;
        return;
    }

    self.replyCell.hidden = NO;
    
    NSArray *texts = [PDPostDetailReplyBasicCell textContainersWithReplys:model.replys];
    self.replyCell.textContainters = texts;
    CGFloat replyHeight = [PDPostDetailReplyToReplyCell cellHeightWithTextArr:texts];
    
    if (model.pictures.count) {
        self.replyCell.frame = CGRectMake(CGRectGetMaxX(self.headerIcon.frame) + 10, CGRectGetMaxY(self.commentImageView.frame) + 10, self.replyCell.width, replyHeight);
    } else {
        self.replyCell.frame = CGRectMake(CGRectGetMaxX(self.headerIcon.frame) + 10, CGRectGetMaxY(self.contentLabel.frame) + 10, self.replyCell.width, replyHeight);
    }
}

- (void)updateButton {
    CGSize moreImageSize = self.moreButton.imageView.image.size;
    CGSize moreSize = CGSizeMake(moreImageSize.width + 10, moreImageSize.height);
    CGSize praiseImageSize = self.praiseButton.imageView.image.size;
    CGSize praiseTextSize = [self.praiseButton.titleLabel.text sizeWithCalcFont:self.praiseButton.titleLabel.font];
    CGSize praiseSize = CGSizeMake(praiseImageSize.width + 10 + praiseTextSize.width, praiseTextSize.height + 10);
    
    [self.moreButton setFrame:CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - moreSize.width, CGRectGetMinY(self.headerIcon.frame) + 5, moreSize.width, praiseSize.height)];
    self.praiseButton.frame = CGRectMake(CGRectGetMinX(self.moreButton.frame) - 10 - praiseSize.width, CGRectGetMinY(self.headerIcon.frame) + 5, praiseSize.width, praiseSize.height);
}

+ (CGFloat)cellHeightWithModel:(PDPostDetailReplyModel *)model {
    CGFloat totalHeight = 0;
    totalHeight = totalHeight + 10 + kICON_WIDTH;
    CGFloat contentHeight = [model.postContent sizeWithCalcFont:kCONTENT_FONT constrainedToSize:CGSizeMake(kScreenBounds.size.width - kICON_WIDTH - kTableViewSectionHeader_left * 2 - 10, CGFLOAT_MAX)].height;
    totalHeight = totalHeight + contentHeight + 10;
    if (model.pictures.count) {
        PDPostPicture *picture = [model.pictures firstObject];
        CGSize imageSize = [PDCenterTool sizeWithImageUrl:picture.pictureRoute];
        CGSize changeSize = CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - 10 - kICON_WIDTH, (kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - 10 - kICON_WIDTH) / imageSize.width * imageSize.height);
        totalHeight = totalHeight + changeSize.height + 10;
    }
    
    totalHeight += 10;
    
    if (model.replys.count) {
        totalHeight += [PDPostDetailReplyToReplyCell cellHeightWithTextArr:[self textContainersWithReplys:model.replys]];
        totalHeight += 10;
    }
    
    return totalHeight;
}

+ (NSArray *)textContainersWithReplys:(NSArray *)replys {
    // 楼中楼部分
    NSMutableArray *textArr = [NSMutableArray array];
    for (int i = 0; i < replys.count; i++) {
        PDPostDetailReplyItem *item = replys[i];
        
        TYTextContainer *textContainer = [[TYTextContainer alloc] init];
        NSString *text;
        NSString *dateText = [NSDate getTimeGap:item.postCreateTime/1000];
        if (item.host) {
            text = [NSString stringWithFormat:@"%@[host]回复 %@：%@ %@",item.userName,item.postReplyUserName,item.postContent,dateText];
        } else {
            text = [NSString stringWithFormat:@"%@回复 %@：%@ %@",item.userName,item.postReplyUserName,item.postContent,dateText];
        }
        textContainer.text = text;
        if (item.admin) {
            [textContainer addLinkWithLinkData:item.postUserId linkColor:[UIColor redColor] underLineStyle:kCTUnderlineStyleNone range:[text rangeOfString:item.userName]];
        } else {
            [textContainer addLinkWithLinkData:item.postUserId linkColor:c26 underLineStyle:kCTUnderlineStyleNone range:[text rangeOfString:item.userName]];
        }
        
//        [textContainer addLinkWithLinkData:item linkColor:[UIColor blackColor] underLineStyle:kCTUnderlineStyleNone range:[text rangeOfString:item.postContent]];
        [textContainer addLinkWithLinkData:nil linkColor:[UIColor lightGrayColor] underLineStyle:kCTUnderlineStyleNone range:[text rangeOfString:dateText]];

        if (item.host) {
            PDPostLabel *label = [[PDPostLabel alloc] init];
            label.text = @"楼主";
            label.frame = CGRectMake(0, 0, label.textSize.width, label.textSize.height);
            [textContainer addView:label range:[text rangeOfString:@"[host]"]];
        }
        
        [textContainer createTextContainerWithTextWidth:kScreenBounds.size.width - kICON_WIDTH - kTableViewSectionHeader_left * 2 - 30];
        
        [textArr addObject:textContainer];
    }
    return [textArr copy];
}

#pragma mark - PDPostDetailReplyToReplyCellDelegate

- (void)replyCell:(PDPostDetailReplyToReplyCell *)cell didClickReplyAtIndex:(NSInteger)index {
    PDPostDetailReplyItem *item = self.model.replys[index];
    if ([self.delegate respondsToSelector:@selector(cell:didClickReplyCellWithModel:)]) {
        [self.delegate cell:self didClickReplyCellWithModel:item];
    }
}

- (void)replyCell:(PDPostDetailReplyToReplyCell *)cell didClickReplyNameWithUserId:(NSString *)userId {
    if ([self.delegate respondsToSelector:@selector(cell:didClickReplyUserNameWithUserId:)]) {
        [self.delegate cell:self didClickReplyUserNameWithUserId:userId];
    }
}

- (void)replyCell:(PDPostDetailReplyToReplyCell *)cell didClickMoreWithButton:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(cell:didClickLookMoreReplyButton:)]) {
        [self.delegate cell:self didClickLookMoreReplyButton:button];
    }
}

@end
