//
//  PDPostDetailTextCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailTextCell.h"

@interface PDPostDetailTextCell ()
@property (nonatomic, strong) UILabel *myTextLabel;
@property (nonatomic) PDPostDetailTextType type;
@end

@implementation PDPostDetailTextCell

- (instancetype)initWithTextType:(PDPostDetailTextType)type reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _type = type;
        [self setupCellWithType:type];
    }
    return self;
}

- (void)setupCellWithType:(PDPostDetailTextType)type {
    self.myTextLabel = [[UILabel alloc] init];
    if (type == PDPostDetailTextTypeTitle) {
        self.myTextLabel.font = [[UIFont systemFontOfCustomeSize:20] boldFont];
        self.myTextLabel.textColor = [UIColor blackColor];
    } else if (PDPostDetailTextTypeContent) {
        self.myTextLabel.font = [UIFont systemFontOfCustomeSize:16];
        self.myTextLabel.textColor = c2;
    }
    
    self.myTextLabel.numberOfLines = 0;
    [self.contentView addSubview:self.myTextLabel];
}

- (void)setText:(NSString *)text {
    _text = text;
    
    self.myTextLabel.text = text;
    CGSize size = [self.myTextLabel.text sizeWithCalcFont:self.myTextLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2, CGFLOAT_MAX)];
    self.myTextLabel.frame = CGRectMake(kTableViewSectionHeader_left, _type == PDPostDetailTextTypeTitle? 5 : 0, size.width, size.height);
}

+ (CGFloat)cellHeightWithText:(NSString *)text type:(PDPostDetailTextType)type {
    if (type == PDPostDetailTextTypeTitle) {
        return [text sizeWithCalcFont:[[UIFont systemFontOfCustomeSize:20] boldFont] constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2, CGFLOAT_MAX)].height + 15;
    } else {
        return [text sizeWithCalcFont:[UIFont systemFontOfCustomeSize:16] constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2, CGFLOAT_MAX)].height + 5;
    }
}

@end
