//
//  PDPostDetailPictureCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailPictureCell.h"

@interface PDPostDetailPictureCell ()

@end

@implementation PDPostDetailPictureCell

- (void)setUrls:(NSArray *)urls {
    if (_urls.count) {
        return;
    }
    _urls = urls;
    
    CGFloat y = 0;
    for (int i = 0; i < urls.count; i++) {
        PDImageView *imageView = [[PDImageView alloc] init];
        imageView.tag = i;
        [self.contentView addSubview:imageView];
        
        NSString *imgUrl = [PDCenterTool absoluteUrlWithPostUrl:urls[i]];
        // 获取图片的宽高
        // 图片的格式...x30.1x30.2x.png
        CGSize size = [PDPostDetailPictureCell exchangeSizeFromSize:[PDCenterTool sizeWithImageUrl:imgUrl]];
        imageView.frame = CGRectMake(kTableViewSectionHeader_left, y, size.width, size.height);
        y = (size.height + 5) + y;
        
        [imageView uploadImageWithURL:imgUrl placeholder:nil callback:nil];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickImageView:)];
        [imageView addGestureRecognizer:tap];
    }
}

- (void)didClickImageView:(UITapGestureRecognizer *)tap {
    PDImageView *imageView = (PDImageView *)tap.view;
    if ([self.delegate respondsToSelector:@selector(pictureCell:didClickImgaeView:atIndex:)]) {
        [self.delegate pictureCell:self didClickImgaeView:imageView atIndex:imageView.tag];
    }
}

+ (CGSize)exchangeSizeFromSize:(CGSize)size {
    if (size.width > (kScreenBounds.size.width - kTableViewSectionHeader_left * 2)) {
        CGFloat limitWidth = kScreenBounds.size.width - kTableViewSectionHeader_left * 2;
        CGFloat calHeight = limitWidth / size.width * size.height;
        return CGSizeMake(limitWidth, calHeight);
    } else {
        return size;
    }
}

+ (CGFloat)cellHeightWithUrlArr:(NSArray *)urls {
    CGFloat y = 0;
    for (int i = 0; i < urls.count; i++) {
        CGSize size = [self exchangeSizeFromSize:[PDCenterTool sizeWithImageUrl:urls[i]]];
        y = (size.height + 5) + y;
    }
    return y;
}

@end
