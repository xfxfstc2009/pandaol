//
//  PDPostDetailReplyFloorHostCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/12.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailReplyFloorHostCell.h"
#import "PDPostLabel.h"

#define kICON_WIDTH LCFloat(38)
#define kCONTENT_FONT [UIFont systemFontOfCustomeSize:15]

@interface PDPostDetailReplyFloorHostCell ()
@property (nonatomic, strong) PDImageView *headerIcon;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) PDPostLabel *levelLabel;
@property (nonatomic, strong) PDPostLabel *isHostLabel;
@property (nonatomic, strong) UIButton *praiseButton;
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UILabel *floorLabel;
/**
 * 发表评论的
 */
@property (nonatomic, strong) PDImageView *commentImageView;
@end

@implementation PDPostDetailReplyFloorHostCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self setupCell];
    return self;
}

- (void)setupCell {
    self.headerIcon = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 10, kICON_WIDTH, kICON_WIDTH)];
    self.headerIcon.layer.cornerRadius = kICON_WIDTH / 2;
    self.headerIcon.layer.masksToBounds = YES;
    [self.contentView addSubview:self.headerIcon];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    self.isHostLabel = [[PDPostLabel alloc] init];
    self.isHostLabel.text = @"楼主";
    self.isHostLabel.hidden = YES;
    [self.contentView addSubview:self.isHostLabel];
    
    self.levelLabel = [[PDPostLabel alloc] init];
    [self.contentView addSubview:self.levelLabel];
    
    self.praiseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.praiseButton setImage:[UIImage imageNamed:@"icon_post_big_prised"] forState:UIControlStateSelected];
    [self.praiseButton setImage:[UIImage imageNamed:@"icon_post_unprised"] forState:UIControlStateNormal];
    self.praiseButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    self.praiseButton.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.praiseButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.contentView addSubview:self.praiseButton];
    [self.praiseButton addTarget:self action:@selector(didClickPraise:) forControlEvents:UIControlEventTouchUpInside];
    
    UIImage *moreImage = [UIImage imageNamed:@"icon_post_sel_more"];
    self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.moreButton setImage:moreImage forState:UIControlStateNormal];
    [self.contentView addSubview:self.moreButton];
    [self.moreButton addTarget:self action:@selector(didClickMore:) forControlEvents:UIControlEventTouchUpInside];
    
    self.floorLabel = [[UILabel alloc] init];
    self.floorLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.floorLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.floorLabel];
    
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.dateLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.dateLabel];
    
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font = kCONTENT_FONT;
    self.contentLabel.textColor = [UIColor blackColor];
    self.contentLabel.numberOfLines = 0;
    [self.contentView addSubview:self.contentLabel];
    
    // 发表的图片
    self.commentImageView = [[PDImageView alloc] init];
    self.commentImageView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.commentImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didclickImageView:)];
    [self.commentImageView addGestureRecognizer:tap];
}


- (void)didClickPraise:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(hostCell:didClickPriseButton:)]) {
        [self.delegate hostCell:self didClickPriseButton:button];
    }
}

- (void)didClickMore:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(hostCell:didClickMoreButton:)]) {
        [self.delegate hostCell:self didClickMoreButton:button];
    }
}

- (void)didclickImageView:(UITapGestureRecognizer *)tap {
    PDImageView *imageView = (PDImageView *)tap.view;
    if ([self.delegate respondsToSelector:@selector(hostCell:didClickImageView:)]) {
        [self.delegate hostCell:self didClickImageView:imageView];
    }
}

- (void)setModel:(PDPostDetailReplyModel *)model {
    _model = model;
    
    [self.headerIcon uploadImageWithAvatarURL:model.postUserHead placeholder:nil callback:nil];
    
    [self.praiseButton setTitle:[PDCenterTool numberStringChangeWithoutSpecialCharacter:model.postGood] forState:UIControlStateNormal];
    self.praiseButton.tag = model.postGood;
    self.praiseButton.selected = model.good;
    [self updateButton];
    
    self.nameLabel.text = model.postUserName;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerIcon.frame) + 10, CGRectGetMinY(self.headerIcon.frame) + 5, nameSize.width, nameSize.height);
    
    self.levelLabel.level = model.level;
    
    if (model.host == YES) {
        self.isHostLabel.hidden = NO;
        self.isHostLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - self.isHostLabel.size.height, self.isHostLabel.size.width, self.isHostLabel.size.height);
        self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.isHostLabel.frame) + 10, CGRectGetMinY(self.isHostLabel.frame), self.levelLabel.size.width, self.levelLabel.size.height);
    } else {
        self.isHostLabel.hidden = YES;
        self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - self.levelLabel.size.height, self.levelLabel.size.width, self.levelLabel.size.height);
    }
    
    // 楼
    self.floorLabel.text = [NSString stringWithFormat:@"第%@楼",@(model.floor)];
    CGSize floorSize = [self.floorLabel.text sizeWithCalcFont:self.floorLabel.font];
    self.floorLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + 5, floorSize.width, floorSize.height);
    
    // 时间
    self.dateLabel.text = [NSDate getTimeGap:model.postCreateTime / 1000];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    self.dateLabel.frame = CGRectMake(CGRectGetMaxX(self.floorLabel.frame) + 5, CGRectGetMinY(self.floorLabel.frame), dateSize.width, dateSize.height);
    // 内容
    self.contentLabel.text = model.postContent;
    CGSize contentSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kICON_WIDTH - kTableViewSectionHeader_left * 2 - 10, CGFLOAT_MAX)];
    self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.headerIcon.frame) + 10, CGRectGetMaxY(self.headerIcon.frame) + 10, contentSize.width, contentSize.height);
    
    if (model.pictures.count) { // 有图片
        PDPostPicture *picture = [model.pictures firstObject];
        NSString *url = picture.pictureRoute;
        [self.commentImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithPostUrl:url] placeholder:nil callback:nil];
        CGSize imageSize = [PDCenterTool sizeWithImageUrl:url];
        self.commentImageView.frame = CGRectMake(CGRectGetMinX(self.contentLabel.frame), CGRectGetMaxY(self.contentLabel.frame) + 10, kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - 10 - kICON_WIDTH, (kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - 10 - kICON_WIDTH) / imageSize.width * imageSize.height);
        self.commentImageView.hidden = NO;
    } else {
        self.commentImageView.image = nil;
        self.commentImageView.hidden = YES;
    }
}

- (void)updateButton {
    CGSize moreImageSize = self.moreButton.imageView.image.size;
    CGSize moreSize = CGSizeMake(moreImageSize.width + 10, moreImageSize.height);
    CGSize praiseImageSize = self.praiseButton.imageView.image.size;
    CGSize praiseTextSize = [self.praiseButton.titleLabel.text sizeWithCalcFont:self.praiseButton.titleLabel.font];
    CGSize praiseSize = CGSizeMake(praiseImageSize.width + 10 + praiseTextSize.width, praiseTextSize.height + 10);
    
    [self.moreButton setFrame:CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - moreSize.width, CGRectGetMinY(self.headerIcon.frame) + 5, moreSize.width, praiseSize.height)];
    self.praiseButton.frame = CGRectMake(CGRectGetMinX(self.moreButton.frame) - 10 - praiseSize.width, CGRectGetMinY(self.headerIcon.frame) + 5, praiseSize.width, praiseSize.height);
}

+ (CGFloat)cellHeightWithModel:(PDPostDetailReplyModel *)model {
    CGFloat totalHeight = 0;
    totalHeight = totalHeight + 10 + kICON_WIDTH;
    CGFloat contentHeight = [model.postContent sizeWithCalcFont:kCONTENT_FONT constrainedToSize:CGSizeMake(kScreenBounds.size.width - kICON_WIDTH - kTableViewSectionHeader_left * 2 - 10, CGFLOAT_MAX)].height;
    totalHeight = totalHeight + contentHeight + 10;
    if (model.pictures.count) {
        PDPostPicture *picture = [model.pictures firstObject];
        CGSize imageSize = [PDCenterTool sizeWithImageUrl:picture.pictureRoute];
        CGSize changeSize = CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - 10 - kICON_WIDTH, (kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - 10 - kICON_WIDTH) / imageSize.width * imageSize.height);
        totalHeight = totalHeight + changeSize.height + 10;
    }
    
    totalHeight += 10;
    
    return totalHeight;
}

@end
