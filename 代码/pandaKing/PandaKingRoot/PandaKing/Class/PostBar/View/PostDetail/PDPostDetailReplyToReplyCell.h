//
//  PDPostDetailReplyToReplyCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPostDetailReplyModel.h"
#import "TYAttributedLabel.h"

// 楼中楼cell
@protocol PDPostDetailReplyToReplyCellDelegate;
@interface PDPostDetailReplyToReplyCell : UIView
@property (nonatomic, strong) NSArray <TYTextContainer *>*textContainters;
@property (nonatomic, weak) id<PDPostDetailReplyToReplyCellDelegate> delegate;

+ (CGFloat)cellHeightWithTextArr:(NSArray *)model;
- (instancetype)initWithConstraintWidth:(CGFloat)width;

/**
 * view的宽度约束
 */
@property (nonatomic, readonly) CGFloat width;
@end

@protocol PDPostDetailReplyToReplyCellDelegate <NSObject>

/**
 * 点击了楼中楼评论人的名字
 */
- (void)replyCell:(PDPostDetailReplyToReplyCell *)cell didClickReplyNameWithUserId:(NSString *)userId;
/**
 * 点击了评论的回调
 */
- (void)replyCell:(PDPostDetailReplyToReplyCell *)cell  didClickReplyAtIndex:(NSInteger)index;
/**
 * 点击跳转更多
 */
- (void)replyCell:(PDPostDetailReplyToReplyCell *)cell didClickMoreWithButton:(UIButton *)button;

@end