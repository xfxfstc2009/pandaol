//
//  PDPostDetailCommentNavigationCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailCommentNavigationCell.h"

@interface PDPostDetailCommentNavigationCell ()
@property (nonatomic, strong) UIButton *allCommentButton;
@property (nonatomic, strong) UIButton *hostOnlyButton;
/**
 * 被选中的按钮的应用
 */
@property (nonatomic, strong) UIButton *selectedButton;
@property (nonatomic, strong) UIView *line;
@property (nonatomic, copy) void(^selBlock)();
@property (nonatomic, readwrite) int selIndex;
@end

@implementation PDPostDetailCommentNavigationCell

+ (CGFloat)cellHeight {
    return 40;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.allCommentButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.allCommentButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    [self.allCommentButton setTitle:@"全部评论(0)" forState:UIControlStateNormal];
    [self.allCommentButton setTitleColor:c26 forState:UIControlStateSelected];
    [self.allCommentButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.allCommentButton.tag = 0;
    [self.contentView addSubview:self.allCommentButton];
    [self.allCommentButton addTarget:self action:@selector(didClickAllComment:) forControlEvents:UIControlEventTouchUpInside];
    
    self.hostOnlyButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.hostOnlyButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    [self.hostOnlyButton setTitle:@"只看楼主" forState:UIControlStateNormal];
    [self.hostOnlyButton setTitleColor:c26 forState:UIControlStateSelected];
    [self.hostOnlyButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.hostOnlyButton.tag = 1;
    [self.contentView addSubview:self.hostOnlyButton];
    [self.hostOnlyButton addTarget:self action:@selector(didClickAllComment:) forControlEvents:UIControlEventTouchUpInside];
    
    self.line = [[UIView alloc] init];
    self.line.backgroundColor = c26;
    [self.contentView addSubview:self.line];
    
    // 默认让全部评论选中
    self.allCommentButton.selected = YES;
    self.selectedButton = self.allCommentButton;
    
    [self updateButtons];
    [self updateLine];
}

- (void)didClickAllComment:(UIButton *)button {
    if (button == self.selectedButton) {
        return;
    }
    
    self.selectedButton.selected = NO;
    self.selectedButton = button;
    self.selectedButton.selected = YES;
    
    if (self.selBlock) {
        self.selBlock(button.tag);
    }
    
    [self updateLine];
}

- (int)selIndex {
    return (int)self.selectedButton.tag;
}

- (void)didChangeSelected:(void (^)(NSUInteger))selBlock {
    if (selBlock) {
        self.selBlock = selBlock;
    }
}

- (void)setCommentCount:(NSUInteger)commentCount {
    _commentCount = commentCount;
    
    [self.allCommentButton setTitle:[NSString stringWithFormat:@"全部评论(%@)",@(commentCount)] forState:UIControlStateNormal];
    [self updateButtons];
    [self updateLine];
}

- (void)updateButtons {
    CGSize allSize = [self.allCommentButton.titleLabel.text sizeWithCalcFont:self.allCommentButton.titleLabel.font];
    CGSize hostSize = [self.hostOnlyButton.titleLabel.text sizeWithCalcFont:self.hostOnlyButton.titleLabel.font];
    
    self.allCommentButton.frame = CGRectMake(kTableViewSectionHeader_left, ([PDPostDetailCommentNavigationCell cellHeight] - allSize.height) / 2, allSize.width, allSize.height);
    self.hostOnlyButton.frame = CGRectMake(CGRectGetMaxX(self.allCommentButton.frame) + 20, ([PDPostDetailCommentNavigationCell cellHeight] - hostSize.height) / 2, hostSize.width, hostSize.height);
}

- (void)updateLine {
    CGSize selectedSize = self.selectedButton.frame.size;
    self.line.frame = CGRectMake(CGRectGetMinX(self.selectedButton.frame) + 10, [PDPostDetailCommentNavigationCell cellHeight] - 2, selectedSize.width - 20, 2);
}

@end
