//
//  PDPostReplyButton.m
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostReplyButton.h"

@interface PDPostReplyButton ()
@property (nonatomic, strong) UILabel *textLabel;
@end

@implementation PDPostReplyButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupButton];
    }
    return self;
}

- (void)setupButton {
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.textLabel.textColor = [UIColor lightGrayColor];
    [self addSubview:self.textLabel];
}

- (void)setText:(NSString *)text {
    _text = text;
    
    self.textLabel.text = text;
    [self updatelabel];
}

- (void)updatelabel {
    CGSize textSize = [self.textLabel.text sizeWithCalcFont:self.textLabel.font];
    self.textLabel.frame = CGRectMake(kTableViewSectionHeader_left, (self.frame.size.height - textSize.height) / 2, textSize.width, textSize.height);
}

@end
