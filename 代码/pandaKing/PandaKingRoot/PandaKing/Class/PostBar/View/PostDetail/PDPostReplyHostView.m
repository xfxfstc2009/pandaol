//
//  PDPostReplyHostView.m
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostReplyHostView.h"

#define kBUTTON_HEIGHT 26
#define kTEXT_HEIGHT 80

@interface PDPostReplyHostView ()
@property (nonatomic, strong) UIButton *selImageButton;
@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, copy) void(^sendBlock)(NSString *, UIImage *);
@property (nonatomic, copy) void(^imageBlock)();
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UIButton *clearButton;
@end

@implementation PDPostReplyHostView

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    CGFloat totalHeight = 10 + kTEXT_HEIGHT + 10 + kBUTTON_HEIGHT + 10;
    self = [super initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, totalHeight)];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupView];
    }
    return self;
}

- (void)setupView {
    self.textView = [[PDTextView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 10, self.frame.size.width - kTableViewSectionHeader_left * 2, kTEXT_HEIGHT)];
    self.textView.layer.cornerRadius = 2.5;
    self.textView.layer.borderColor = c27.CGColor;
    self.textView.layer.borderWidth = 1;
    self.textView.font = [UIFont systemFontOfCustomeSize:13];
    self.textView.place = @"回复楼主";
    self.textView.limitMax = 999;
    [self addSubview:self.textView];
    __weak typeof(self) weakSelf = self;
    [self.textView textViewDiDChangeBlock:^(NSString *text) {
        if (text.length) {
            weakSelf.sendButton.enabled = YES;
        } else {
            weakSelf.sendButton.enabled = NO;
        }
    }];
    
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sendButton.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - 50, CGRectGetMaxY(self.textView.frame) + 10, 50, kBUTTON_HEIGHT);
    self.sendButton.enabled = NO;
    self.sendButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.sendButton.layer.cornerRadius = 2.5;
    self.sendButton.layer.masksToBounds = YES;
    [self.sendButton setTitle:@"发送" forState:UIControlStateNormal];
    [self.sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.sendButton setBackgroundImage:[UIImage imageWithRenderColor:c26 renderSize:self.sendButton.frame.size] forState:UIControlStateNormal];
    [self.sendButton addTarget:self action:@selector(didClickSendButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.sendButton];
    
    self.selImageButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.selImageButton.hidden = NO;
    [self.selImageButton setFrame:CGRectMake(20, CGRectGetMaxY(self.textView.frame) + 10, kBUTTON_HEIGHT, kBUTTON_HEIGHT)];
    [self.selImageButton setImage:[UIImage imageNamed:@"icon_post_imagebtn"] forState:UIControlStateNormal];
    [self addSubview:self.selImageButton];
    [self.selImageButton addTarget:self action:@selector(didClickImageButton:) forControlEvents:UIControlEventTouchUpInside];
    
    CGFloat imageHeight = kBUTTON_HEIGHT + 10;
    self.imageView = [[UIImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.textView.frame) + 5, imageHeight, imageHeight)];
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
    self.imageView.clipsToBounds = YES;
    self.imageView.hidden = YES;
    [self addSubview:self.imageView];

    self.clearButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.clearButton.titleLabel.font = [UIFont systemFontOfCustomeSize:11];
    CGFloat buttonWidth = [@"点击清除" sizeWithCalcFont:self.clearButton.titleLabel.font].width;
    [self.clearButton setTitle:@"点击清除" forState:UIControlStateNormal];
    [self.clearButton setTitleColor:c4 forState:UIControlStateNormal];
    [self.clearButton setFrame:CGRectMake(CGRectGetMaxX(self.imageView.frame) + 10, CGRectGetMaxY(self.textView.frame) + 10, buttonWidth, kBUTTON_HEIGHT)];
    self.clearButton.hidden = YES;
    [self.clearButton addTarget:self action:@selector(didClickClearButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.clearButton];
}

- (void)didClickImageButton:(UIButton *)button {
    if (self.imageBlock) {
        self.imageBlock();
    }
}

- (void)didClickSendButton:(UIButton *)button {
    [self.textView resignFirstResponder];
    if (self.sendBlock) {
        self.sendBlock(self.textView.text, self.imageView.image);
    }
}

- (void)didClickClearButton:(UIButton *)button {
    [self showSelButton:YES];
    self.imageView.image = nil;
}

- (void)replyView:(void (^)(NSString *, UIImage *))block {
    if (block) {
        self.sendBlock = block;
    }
}

- (void)replyViewDidClickImageButton:(void (^)())block {
    if (block) {
        self.imageBlock = block;
    }
}

- (void)setSelImage:(UIImage *)selImage {
    _selImage = selImage;
    
    if (!selImage) {
        return;
    }
    
    self.imageView.image = selImage;
    [self showSelButton:NO];
}

- (void)showSelButton:(BOOL)s {
    self.imageView.hidden = s;
    self.clearButton.hidden = s;
    self.selImageButton.hidden = !s;
}

@end
