//
//  PDPostDetailTextCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PDPostDetailTextType) {
    /**
     * 标题
     */
    PDPostDetailTextTypeTitle,
    /**
     * 内容
     */
    PDPostDetailTextTypeContent,
};

@interface PDPostDetailTextCell : UITableViewCell
@property (nonatomic, copy) NSString *text;

- (instancetype)initWithTextType:(PDPostDetailTextType)type reuseIdentifier:(NSString *)reuseIdentifier;

+ (CGFloat)cellHeightWithText:(NSString *)text type:(PDPostDetailTextType)type;
@end
