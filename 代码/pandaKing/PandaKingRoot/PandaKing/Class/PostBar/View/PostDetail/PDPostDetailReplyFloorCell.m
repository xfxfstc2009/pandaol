//
//  PDPostDetailReplyFloorCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/12.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailReplyFloorCell.h"
#import "TYAttributedLabel.h"
#import "PDPostLabel.h"

@interface PDPostDetailReplyFloorCell ()<TYAttributedLabelDelegate>
@property (nonatomic, strong) TYAttributedLabel *label;
@end

@implementation PDPostDetailReplyFloorCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.label = [[TYAttributedLabel alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 10, kScreenBounds.size.width - 20, 0)];
    self.label.delegate = self;
    [self.contentView addSubview:self.label];
}

- (void)setItem:(PDPostDetailReplyItem *)item {
    _item = item;

    self.label.textContainer = [PDPostDetailReplyFloorCell textWithItem:item];
    [self.label sizeToFit];
}

+ (CGFloat)cellHeightWithItem:(PDPostDetailReplyItem *)item {
    TYTextContainer *text = [self textWithItem:item];
    return text.textHeight + 20;
}

+ (TYTextContainer *)textWithItem:(PDPostDetailReplyItem *)item {
    TYTextContainer *textContainer = [[TYTextContainer alloc] init];
    NSString *text;
    NSString *dateText = [NSDate getTimeGap:item.postCreateTime/1000];
    if (item.host) {
        text = [NSString stringWithFormat:@"%@[host]回复 %@：%@ %@",item.userName,item.postReplyUserName,item.postContent,dateText];
    } else {
        text = [NSString stringWithFormat:@"%@回复 %@：%@ %@",item.userName,item.postReplyUserName,item.postContent,dateText];
    }
    textContainer.text = text;
    [textContainer addLinkWithLinkData:item.postUserId linkColor:c26 underLineStyle:kCTUnderlineStyleNone range:[text rangeOfString:item.userName]];
    [textContainer addLinkWithLinkData:nil linkColor:[UIColor lightGrayColor] underLineStyle:kCTUnderlineStyleNone range:[text rangeOfString:dateText]];
    
    if (item.host) {
        PDPostLabel *hostLabel = [[PDPostLabel alloc] init];
        hostLabel.text = @"楼主";
        hostLabel.frame = CGRectMake(0, 0, hostLabel.textSize.width, hostLabel.textSize.height);
        [textContainer addView:hostLabel range:[text rangeOfString:@"[host]"]];
    }
    
    [textContainer createTextContainerWithTextWidth:kScreenBounds.size.width - 20];
    return textContainer;
}

- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)textStorage atPoint:(CGPoint)point {
    if ([textStorage isKindOfClass:[TYLinkTextStorage class]]) {
        id linkData = ((TYLinkTextStorage *)textStorage).linkData;
        if ([linkData isKindOfClass:[NSString class]]) {
            NSString *userId = (NSString *)linkData;
            if ([self.delegate respondsToSelector:@selector(floorCell:didClickReplyUserNameWithUserId:)]) {
                [self.delegate floorCell:self didClickReplyUserNameWithUserId:userId];
            }
        }
    }
}

@end
