//
//  PDPostReplyHostView.h
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDTextView.h"

/// 回复楼主 可以选择图片
@interface PDPostReplyHostView : UIView
@property (nonatomic, strong) PDTextView *textView;
/**
 * 设置选择好的图片
 */
@property (nonatomic, strong) UIImage *selImage;


- (void)replyView:(void(^)(NSString *text, UIImage *image))block;
- (void)replyViewDidClickImageButton:(void(^)())block;
@end
