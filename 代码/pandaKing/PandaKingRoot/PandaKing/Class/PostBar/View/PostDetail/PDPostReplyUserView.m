//
//  PDPostReplyUserView.m
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostReplyUserView.h"

#define kHEADER_WIDTH 32
#define kBUTTON_HEIGHT 26
#define kTEXT_HEIGHT 80

@interface PDPostReplyUserView ()
@property (nonatomic, strong) PDImageView *headerIcon;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UIButton *sendButton;
@property (nonatomic, copy) void(^sendModelBlock)(NSString *,PDPostDetailReplyModel *);
@property (nonatomic, copy) void(^sendItemBlock)(NSString *, PDPostDetailReplyItem *);
@end

@implementation PDPostReplyUserView

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    CGFloat totalHeight = 10 + kHEADER_WIDTH + 10 + kTEXT_HEIGHT + 10 + kBUTTON_HEIGHT + 10;
    self = [super initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, totalHeight)];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self setupView];
    }
    return self;
}

- (void)setupView {
    self.headerIcon = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 10, kHEADER_WIDTH, kHEADER_WIDTH)];
    self.headerIcon.layer.cornerRadius = kHEADER_WIDTH / 2;
    self.headerIcon.layer.masksToBounds = YES;
    self.headerIcon.backgroundColor = [UIColor redColor];
    [self addSubview:self.headerIcon];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.nameLabel.textColor = [UIColor blackColor];
    [self addSubview:self.nameLabel];
    
    self.textView = [[PDTextView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.headerIcon.frame) + 10, self.frame.size.width - kTableViewSectionHeader_left * 2, kTEXT_HEIGHT)];
    self.textView.layer.cornerRadius = 2.5;
    self.textView.layer.borderColor = c27.CGColor;
    self.textView.layer.borderWidth = 1;
    self.textView.font = [UIFont systemFontOfCustomeSize:13];
    self.textView.limitMax = 999;
    [self addSubview:self.textView];
    __weak typeof(self) weakSelf = self;
    [self.textView textViewDiDChangeBlock:^(NSString *text) {
        if (text.length) {
            weakSelf.sendButton.enabled = YES;
        } else {
            weakSelf.sendButton.enabled = NO;
        }
    }];
    
    self.sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sendButton.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - 50, CGRectGetMaxY(self.textView.frame) + 10, 50, kBUTTON_HEIGHT);
    self.sendButton.enabled = NO;
    self.sendButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.sendButton.layer.cornerRadius = 2.5;
    self.sendButton.layer.masksToBounds = YES;
    [self.sendButton setTitle:@"发送" forState:UIControlStateNormal];
    [self.sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.sendButton setBackgroundImage:[UIImage imageWithRenderColor:c26 renderSize:self.sendButton.frame.size] forState:UIControlStateNormal];
    [self.sendButton addTarget:self action:@selector(didClickSendButton:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.sendButton];
}

- (void)didClickSendButton:(UIButton *)button {
    [self.textView resignFirstResponder];
    if (self.sendModelBlock) {
        self.sendModelBlock(self.textView.text, self.model);
    }
    if (self.sendItemBlock) {
        self.sendItemBlock(self.textView.text, self.item);
    }
}

- (void)postView:(void (^)(NSString *, PDPostDetailReplyModel *))block {
    if (block) {
        self.sendModelBlock = block;
    }
}

- (void)postViewReplyWithItemBlock:(void (^)(NSString *, PDPostDetailReplyItem *))block {
    if (block) {
        self.sendItemBlock = block;
    }
}

- (void)setModel:(PDPostDetailReplyModel *)model {
    _model = model;
    
    [self.headerIcon uploadImageWithAvatarURL:model.postUserHead placeholder:nil callback:nil];
    self.nameLabel.text = model.postUserName;
    self.textView.place = [NSString stringWithFormat:@"回复%@",self.nameLabel.text];
    [self.textView setNeedsLayout];
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(self.frame.size.width - kTableViewSectionHeader_left * 2 - kHEADER_WIDTH - 10, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerIcon.frame) + 10, 10 + (kHEADER_WIDTH - nameSize.height) / 2, nameSize.width, nameSize.height);
}

- (void)setItem:(PDPostDetailReplyItem *)item {
    _item = item;
    
    [self.headerIcon uploadImageWithAvatarURL:item.postUserHead placeholder:nil callback:nil];
    self.nameLabel.text = item.userName;
    self.textView.place = [NSString stringWithFormat:@"回复%@",self.nameLabel.text];
    [self.textView setNeedsLayout];
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(self.frame.size.width - kTableViewSectionHeader_left * 2 - kHEADER_WIDTH - 10, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerIcon.frame) + 10, 10 + (kHEADER_WIDTH - nameSize.height) / 2, nameSize.width, nameSize.height);
}

@end
