//
//  PDPostDetailPraiseCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailPraiseCell.h"

#define kHEADER_WIDTH 30

@interface PDPostDetailPraiseCell ()
@property (nonatomic, strong) UIButton *praiseButton;
@property (nonatomic, strong) NSMutableArray *headerIcons;
@end

@implementation PDPostDetailPraiseCell

+ (CGFloat)cellHeight {
    return 55;
}

- (NSMutableArray *)headerIcons {
    if (!_headerIcons) {
        _headerIcons = [NSMutableArray arrayWithCapacity:6];
    }
    return _headerIcons;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
//    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    [self setupCell];
    return self;
}

- (void)setupCell {
    self.praiseButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.praiseButton setImage:[UIImage imageNamed:@"icon_post_big_prised"] forState:UIControlStateSelected];
    [self.praiseButton setImage:[UIImage imageNamed:@"icon_post_unprised"] forState:UIControlStateNormal];
    self.praiseButton.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    self.praiseButton.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.praiseButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    [self.contentView addSubview:self.praiseButton];
    [self.praiseButton addTarget:self action:@selector(didClickPraise:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setModel:(PDPostDetailModel *)model {
    _model = model;
    
    self.praiseButton.tag = model.postGood;
    self.praiseButton.selected = model.good;
    [self.praiseButton setTitle:[NSString stringWithFormat:@"%@人觉得赞",@(model.postGood)] forState:UIControlStateNormal];
    [self updateButton];
    
    // 头像存在，不再加载
    if (self.headerIcons.count) {
        return;
    }
    
    int count = (int)model.upvotes.count;
    if (count > 6) count = 6;
    // 所有头像叠加起来的宽度
    CGFloat totalsHeaderWidth = count * kHEADER_WIDTH - (count - 1) * 3;
    for (int i = count - 1; i >= 0; i--) {
        PDPostDetailPriseUserItem *userItem = model.upvotes[i];
        PDImageView *header = [[PDImageView alloc] init];
        header.frame = CGRectMake(CGRectGetMaxX(self.praiseButton.frame) + 10 + totalsHeaderWidth - (count - i) * (kHEADER_WIDTH - 3), ([PDPostDetailPraiseCell cellHeight] - kHEADER_WIDTH) / 2, kHEADER_WIDTH, kHEADER_WIDTH);
        header.contentMode = UIViewContentModeScaleAspectFill;
        header.layer.cornerRadius = kHEADER_WIDTH / 2;
        header.layer.masksToBounds = YES;
        [header uploadImageWithAvatarURL:userItem.userHead placeholder:nil callback:nil];
        [self.contentView addSubview:header];
        [self.headerIcons addObject:header];
    }
}

- (void)updateButton {
    CGSize praiseImageSize = self.praiseButton.imageView.image.size;
    CGSize praiseTextSize = [self.praiseButton.titleLabel.text sizeWithCalcFont:self.praiseButton.titleLabel.font];
    CGSize praiseSize = CGSizeMake(praiseImageSize.width + 10 + praiseTextSize.width, praiseTextSize.height + 10);
    self.praiseButton.frame = CGRectMake(kTableViewSectionHeader_left, ([PDPostDetailPraiseCell cellHeight] - praiseSize.height) / 2, praiseSize.width, praiseSize.height);
}

- (void)didClickPraise:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(cell:didClickPraise:)]) {
        [self.delegate cell:self didClickPraise:button];
    }
}

@end
