//
//  PDPostReplyUserView.h
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDTextView.h"
#import "PDPostDetailReplyModel.h"

/// 回复普通用户 带有对方头像和昵称
@interface PDPostReplyUserView : UIView
/**
 * 当是回复一楼的时候
 */
@property (nonatomic, strong) PDPostDetailReplyModel *model;
/**
 * 当回复楼中楼的时候
 */
@property (nonatomic, strong) PDPostDetailReplyItem *item;
@property (nonatomic, strong) PDTextView *textView;

- (void)postView:(void(^)(NSString *text, PDPostDetailReplyModel *model))block;
- (void)postViewReplyWithItemBlock:(void(^)(NSString *text, PDPostDetailReplyItem *item))block;
@end
