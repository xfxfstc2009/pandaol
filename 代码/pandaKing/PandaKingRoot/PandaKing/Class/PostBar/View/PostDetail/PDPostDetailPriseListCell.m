//
//  PDPostDetailPriseListCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailPriseListCell.h"
#import "PDCenterLabel.h"

#define kHEADER_WIDTH LCFloat(55)

@interface PDPostDetailPriseListCell ()
@property (nonatomic, strong) PDImageView *headerIcon;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *levelLabel;
@property (nonatomic, strong) PDCenterLabel *genderLabel;
@end

@implementation PDPostDetailPriseListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.headerIcon = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, ([PDPostDetailPriseListCell cellHeight] - kHEADER_WIDTH) / 2, kHEADER_WIDTH, kHEADER_WIDTH)];
    self.headerIcon.layer.cornerRadius = kHEADER_WIDTH / 2;
    self.headerIcon.layer.masksToBounds = YES;
    [self.contentView addSubview:self.headerIcon];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:16];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    self.genderLabel = [[PDCenterLabel alloc] init];
    self.genderLabel.contentInsets = UIEdgeInsetsMake(2.5, 10, 2.5, 10);
    self.genderLabel.layer.cornerRadius = 2;
    self.genderLabel.layer.masksToBounds = YES;
    self.genderLabel.textFont = [UIFont systemFontOfCustomeSize:12];
    [self.contentView addSubview:self.genderLabel];
    
    self.levelLabel = [[UILabel alloc] init];
    self.levelLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.levelLabel.textColor = [UIColor whiteColor];
    self.levelLabel.backgroundColor = c26;
    self.levelLabel.layer.cornerRadius = 2;
    self.levelLabel.layer.masksToBounds = YES;
    self.levelLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.levelLabel];
}

- (void)setModel:(NSString *)model {
    _model = model;
    
    [self.headerIcon uploadImageWithAvatarURL:@"" placeholder:nil callback:nil];
    self.headerIcon.backgroundColor = [UIColor redColor];
    self.nameLabel.text = @"科雷兹";
    self.levelLabel.text = @"Lv.1";
    self.genderLabel.text = @"11";
    self.genderLabel.image = [UIImage imageNamed:@"icon_friend_male"];
    self.genderLabel.backgroundColor = [UIColor blueColor];
    
    [self updateCell];
}

- (void)updateCell {
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * (kTableViewSectionHeader_left + kHEADER_WIDTH), [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize genderSize = [self.genderLabel size];
    CGSize levelSize = [self.levelLabel.text sizeWithCalcFont:self.levelLabel.font];
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerIcon.frame) + 10, CGRectGetMinY(self.headerIcon.frame) + 5, nameSize.width, nameSize.height);
    self.genderLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + 5, genderSize.width, genderSize.height);
    self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.genderLabel.frame) + 10, CGRectGetMinY(self.genderLabel.frame), levelSize.width + 10, levelSize.height);

}

+ (CGFloat)cellHeight {
    return LCFloat(75);
}

@end
