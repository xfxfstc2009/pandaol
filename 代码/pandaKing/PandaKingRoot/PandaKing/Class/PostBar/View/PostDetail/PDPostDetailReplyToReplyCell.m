//
//  PDPostDetailReplyToReplyCell.m
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailReplyToReplyCell.h"
#import "PDPostLabel.h"

static int const max = 2;

@interface PDPostDetailReplyToReplyCell ()<TYAttributedLabelDelegate>
@property (nonatomic, readwrite) CGFloat width;
@property (nonatomic, strong) UIButton *moreButton;
/**
 * 用来存储创建出来的楼中楼cell
 */
@property (nonatomic, strong) NSMutableArray *replyCells;
/**
 * 使用中的楼中楼cell
 */
@property (nonatomic, strong) NSMutableArray *usedCells;
@end

// 预先创建好三个
@implementation PDPostDetailReplyToReplyCell

- (NSMutableArray *)replyCells {
    if (!_replyCells) {
        _replyCells = [NSMutableArray array];
    }
    return _replyCells;
}

- (instancetype)initWithConstraintWidth:(CGFloat)width {
    self = [super init];
    if (self) {
        _width = width;
        self.backgroundColor = BACKGROUND_VIEW_COLOR;
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.moreButton.hidden = YES;
    [self.moreButton setTitle:@"显示更多" forState:UIControlStateNormal];
    [self.moreButton setTitleColor:c26 forState:UIControlStateNormal];
    self.moreButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    [self addSubview:self.moreButton];
    [self.moreButton addTarget:self action:@selector(didClickMoreButton:) forControlEvents:UIControlEventTouchUpInside];
    
    for (int i = 0; i<max; i++) {
        TYAttributedLabel *label = [[TYAttributedLabel alloc] init];
        label.tag = i;
        label.delegate = self;
        label.hidden = YES;
        [self addSubview:label];
        [self.replyCells addObject:label];
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickLabel:)];
        [label addGestureRecognizer:tap];
    }
}

- (void)setTextContainters:(NSArray<TYTextContainer *> *)textContainters {
    _textContainters = textContainters;
    
    for (TYAttributedLabel *label in self.replyCells) {
        label.hidden = YES;
    }
    
    CGFloat y = 10;
    for (int i = 0; i < textContainters.count; i++) {
        if (i < self.replyCells.count) {
            TYAttributedLabel *label = self.replyCells[i];
            label.hidden = NO;
            label.frame = CGRectMake(10, y, self.width, 0);
            
            TYTextContainer *text = textContainters[i];
            label.textContainer = text;
            [label sizeToFit];
            label.backgroundColor = BACKGROUND_VIEW_COLOR;
            
            y += label.frame.size.height;
        }
    }
    
    if (textContainters.count > max) {
        CGSize buttonSize = [@"查看更多" sizeWithCalcFont:self.moreButton.titleLabel.font];
        self.moreButton.hidden = NO;
        self.moreButton.frame = CGRectMake(self.width - 10 - buttonSize.width, y + (30 - buttonSize.height)/2, buttonSize.width, buttonSize.height);
    } else {
        self.moreButton.hidden = YES;
    }
}

- (void)didClickMoreButton:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(replyCell:didClickMoreWithButton:)]) {
        [self.delegate replyCell:self didClickMoreWithButton:button];
    }
}

- (void)didClickLabel:(UITapGestureRecognizer *)tap {
    TYAttributedLabel *label = (TYAttributedLabel *)tap.view;
    if ([self.delegate respondsToSelector:@selector(replyCell:didClickReplyAtIndex:)]) {
        [self.delegate replyCell:self didClickReplyAtIndex:label.tag];
    }
}

+ (CGFloat)cellHeightWithTextArr:(NSArray *)model {
    CGFloat height = 0;
    for (int i = 0; i < model.count; i++) {
        if (i > max-1) {
            break;
        }
        TYTextContainer *text = model[i];
        height += text.textHeight;
    }
    
    if (model.count > max) {
        // 加上按钮的高度
        height += 25;
    }
    
    return height + 20;
}

- (void)attributedLabel:(TYAttributedLabel *)attributedLabel textStorageClicked:(id<TYTextStorageProtocol>)textStorage atPoint:(CGPoint)point {
    if ([textStorage isKindOfClass:[TYLinkTextStorage class]]) {
        id linkData = ((TYLinkTextStorage *)textStorage).linkData;
        if ([linkData isKindOfClass:[NSString class]]) {
            NSString *userId = (NSString *)linkData;
            if ([self.delegate respondsToSelector:@selector(replyCell:didClickReplyNameWithUserId:)]) {
                [self.delegate replyCell:self didClickReplyNameWithUserId:userId];
            }
        }
    }
}

@end
