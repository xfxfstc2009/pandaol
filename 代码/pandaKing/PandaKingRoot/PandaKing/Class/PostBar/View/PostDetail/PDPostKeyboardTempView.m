//
//  PDPostKeyboardTempView.m
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostKeyboardTempView.h"

@interface PDPostKeyboardTempView ()
@property (nonatomic, copy) void(^touchBlock)();
@end

@implementation PDPostKeyboardTempView

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:kScreenBounds];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickAtTempView)];
    [self addGestureRecognizer:tap];
}

- (void)didClickAtTempView {
    if (self.touchBlock) {
        self.touchBlock();
    }
    
    [self dismissTempView];
}

- (void)showTempView {
    NSEnumerator *windowEnnumtor = [UIApplication sharedApplication].windows.reverseObjectEnumerator;
    for (UIWindow *window in windowEnnumtor) {
        BOOL isOnMainScreen = window.screen == [UIScreen mainScreen];
        BOOL isVisible      = !window.hidden && window.alpha > 0;
        BOOL isLevelNormal  = window.windowLevel == UIWindowLevelNormal;
        
        if (isOnMainScreen && isVisible && isLevelNormal) {
            [window addSubview:self];
        }
    }
    
    [UIView animateWithDuration:0.35 animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.5];
    }];
}

- (void)dismissTempView {
    [UIView animateWithDuration:0.35 animations:^{
        self.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)didTouchAtTempView:(void (^)())block {
    if (block) {
        self.touchBlock = block;
    }
}

@end
