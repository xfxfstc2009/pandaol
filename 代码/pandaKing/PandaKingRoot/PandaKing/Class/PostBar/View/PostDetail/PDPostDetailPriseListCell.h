//
//  PDPostDetailPriseListCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDPostDetailPriseListCell : UITableViewCell
@property (nonatomic, strong) NSString *model;

+ (CGFloat)cellHeight;
@end
