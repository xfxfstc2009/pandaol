//
//  PDPostDetailReplyFloorHostCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/12.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPostDetailReplyModel.h"

/// 楼主的cell
@protocol PDPostDetailReplyFloorHostCellDelegate;
@interface PDPostDetailReplyFloorHostCell : UITableViewCell
@property (nonatomic, strong) PDPostDetailReplyModel *model;
@property (nonatomic, weak) id<PDPostDetailReplyFloorHostCellDelegate> delegate;

+ (CGFloat)cellHeightWithModel:(PDPostDetailReplyModel *)model;
@end

@protocol PDPostDetailReplyFloorHostCellDelegate <NSObject>
- (void)hostCell:(PDPostDetailReplyFloorHostCell *)cell didClickPriseButton:(UIButton *)button;
- (void)hostCell:(PDPostDetailReplyFloorHostCell *)cell didClickMoreButton:(UIButton *)button;
- (void)hostCell:(PDPostDetailReplyFloorHostCell *)cell didClickImageView:(PDImageView *)imageView;
@end