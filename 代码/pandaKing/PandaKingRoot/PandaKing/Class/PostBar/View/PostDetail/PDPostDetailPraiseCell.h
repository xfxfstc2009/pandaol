//
//  PDPostDetailPraiseCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPostDetailModel.h"

// 点赞的cell ，显示头像列表
@protocol PDPostDetailPraiseCellDelegate;
@interface PDPostDetailPraiseCell : UITableViewCell
@property (nonatomic, strong) PDPostDetailModel *model;
@property (nonatomic, weak) id<PDPostDetailPraiseCellDelegate> delegate;

+ (CGFloat)cellHeight;
@end

@protocol PDPostDetailPraiseCellDelegate <NSObject>

- (void)cell:(PDPostDetailPraiseCell *)cell didClickPraise:(UIButton *)button;

@end