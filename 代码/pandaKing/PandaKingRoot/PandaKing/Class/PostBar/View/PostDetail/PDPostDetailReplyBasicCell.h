//
//  PDPostDetailReplyBasicCell.h
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPostDetailReplyModel.h"
#import "PDPostDetailReplyToReplyCell.h"

/// 评论cell 附带楼中楼
@protocol PDPostDetailReplyBasicCellDelegate;
@interface PDPostDetailReplyBasicCell : UITableViewCell
@property (nonatomic, strong) PDPostDetailReplyModel *model;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) id<PDPostDetailReplyBasicCellDelegate> delegate;

+ (CGFloat)cellHeightWithModel:(PDPostDetailReplyModel *)model;
@end

@protocol PDPostDetailReplyBasicCellDelegate <NSObject>

- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickPriseButton:(UIButton *)button;
- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickMoreButton:(UIButton *)button;
- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickImageView:(PDImageView *)imageView;

/**
 * 点击评论中的楼中楼
 */
- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickReplyCellWithModel:(PDPostDetailReplyItem *)item;
/**
 * 点击评论人的名字
 */
- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickReplyUserNameWithUserId:(NSString *)userId;
/**
 * 查看更多评论
 */
- (void)cell:(PDPostDetailReplyBasicCell *)cell didClickLookMoreReplyButton:(UIButton *)button;

@end