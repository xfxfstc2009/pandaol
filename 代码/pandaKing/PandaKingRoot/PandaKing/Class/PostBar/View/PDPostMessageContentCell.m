//
//  PDPostMessageContentCell.m
//  PandaKing
//
//  Created by Cranz on 17/5/15.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostMessageContentCell.h"
#import "PDPostMessageImageCell.h"

#define kTEXTVIEW_HEIGHT 150
#define kIMAGES_HEIGHT 100

#define kLIMITLENGTH 6000 // 最大限制字数
#define kLIMITIMGAES 9 // 最多选取9张图片

@interface PDPostMessageContentCell ()<UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, copy) void(^textBlock)(NSString *, NSUInteger);
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSMutableArray *itemsArr;

@property (nonatomic, copy) void(^alertBlock)();
@end

@implementation PDPostMessageContentCell

- (NSMutableArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

+ (CGFloat)cellHeight {
    return kTEXTVIEW_HEIGHT + 10 + kIMAGES_HEIGHT;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    [self setupcell];
    return self;
}

- (void)setupcell {
    self.textView = [[PDTextView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 10, kScreenBounds.size.width - 20, kTEXTVIEW_HEIGHT)];
    self.textView.font = [UIFont systemFontOfCustomeSize:15];
    self.textView.place = @"写点什么吧~";
    self.textView.limitMax = kLIMITLENGTH;
    __weak typeof(self) weakSelf = self;
    [self.textView textViewDiDChangeBlock:^(NSString *text) {
        NSUInteger length = text.length;
        if (length > kLIMITLENGTH) {
            NSString *newText = [text substringToIndex:kLIMITLENGTH];
            weakSelf.textView.text = newText;
            length = newText.length;
        }
        if (weakSelf.textBlock) {
            weakSelf.textBlock(text, length);
        }
    }];
    [self.contentView addSubview:self.textView];
    
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.itemSize = CGSizeMake(kIMAGES_HEIGHT - 10, kIMAGES_HEIGHT - 10);
    layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    layout.minimumLineSpacing = 3;
    layout.sectionInset = UIEdgeInsetsMake(10, 10, 10, 10);
    self.collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.textView.frame), kScreenBounds.size.width, kIMAGES_HEIGHT) collectionViewLayout:layout];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
    self.collectionView.backgroundColor = [UIColor whiteColor];
    [self.collectionView registerClass:[PDPostMessageImageCell class] forCellWithReuseIdentifier:@"collCellId"];
    [self.contentView addSubview:self.collectionView];
}

#pragma mark - UICollection

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.itemsArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PDPostMessageImageCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"collCellId" forIndexPath:indexPath];
    if (self.itemsArr.count > indexPath.row) {
        cell.image = self.itemsArr[indexPath.row];
    }
    
    __weak typeof(self) weakSelf = self;
    [cell didDeleteCellActions:^(PDPostMessageImageCell *imageCell, UIImage *deleteImage) {
        if ([weakSelf.itemsArr containsObject:deleteImage]) {
            NSIndexPath *index = [collectionView indexPathForCell:imageCell];
            [weakSelf.itemsArr removeObject:deleteImage];
            [collectionView deleteItemsAtIndexPaths:@[index]];
        }
    }];
    
    return cell;
}

- (void)textViewDidEditWithText:(void (^)(NSString *, NSUInteger))block {
    if (block) {
        self.textBlock = block;
    }
}

#pragma mark - update

- (void)addImages:(NSArray *)images {
    if (self.itemsArr.count >= kLIMITIMGAES) {
        if (self.alertBlock) {
            self.alertBlock();
        }
        return;
    }
    
    NSUInteger needSelImagesCount = kLIMITIMGAES - self.itemsArr.count;
    NSMutableArray *needSelImagesArr = [NSMutableArray arrayWithCapacity:needSelImagesCount];
    for (int i = 0; i < needSelImagesCount; i++) {
        if (i > (images.count - 1)) {
            break;
        }
        [needSelImagesArr addObject:images[i]];
    }
    
    [self.itemsArr addObjectsFromArray:[needSelImagesArr copy]];
    [self.collectionView reloadData];
}

- (void)alertImageTooMuch:(void (^)())alert {
    if (alert) {
        self.alertBlock = alert;
    }
}


- (NSArray *)getImages {
    return [self.itemsArr copy];
}
@end
