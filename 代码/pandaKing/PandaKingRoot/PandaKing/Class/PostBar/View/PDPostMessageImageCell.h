//
//  PDPostMessageImageCell.h
//  PandaKing
//
//  Created by Cranz on 17/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 选择照片cell
@interface PDPostMessageImageCell : UICollectionViewCell
@property (nonatomic, strong) UIImage *image;

- (void)didDeleteCellActions:(void(^)(PDPostMessageImageCell *imageCell,
                                      UIImage *deleteImage))actions;
@end
