//
//  PDPostReportView.h
//  PandaKing
//
//  Created by Cranz on 17/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 填写举报弹框
@interface PDPostReportView : UIView

- (instancetype)initWithTitle:(NSString *)title;
- (void)didClickCancleActions:(void(^)())actions;
- (void)didClickSubmitActions:(void(^)(NSString *text))actions;

@end
