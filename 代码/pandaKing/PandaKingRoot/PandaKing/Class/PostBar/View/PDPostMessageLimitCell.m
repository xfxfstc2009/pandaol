//
//  PDPostMessageLimitCell.m
//  PandaKing
//
//  Created by Cranz on 17/5/15.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostMessageLimitCell.h"

@interface PDPostMessageLimitCell ()
@property (nonatomic, strong) UILabel *limitLabel;
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, copy) void(^ButtonBlock)();
@end

@implementation PDPostMessageLimitCell

+ (CGFloat)cellHeight {
    return 36;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    [self setupCell];
    return self;
}

- (void)setupCell {
    self.limitLabel = [[UILabel alloc] init];
    self.limitLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.limitLabel.text = @"0/6000";
    [self updateLabelsFrame];
    self.limitLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.limitLabel];
    
    self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *moreImage = [UIImage imageNamed:@"icon_post_imagebtn"];
    self.moreButton.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - moreImage.size.width, ([PDPostMessageLimitCell cellHeight] - moreImage.size.height) / 2, moreImage.size.width, moreImage.size.height);
    [self.moreButton setBackgroundImage:moreImage forState:UIControlStateNormal];
    [self.moreButton addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.moreButton];
}

- (void)updateLabelsFrame {
    CGSize limitSize = [self.limitLabel.text sizeWithCalcFont:self.limitLabel.font];
    self.limitLabel.frame = CGRectMake(kTableViewSectionHeader_left, ([PDPostMessageLimitCell cellHeight] - limitSize.height) / 2, limitSize.width, limitSize.height);
}

- (void)buttonAction {
    if (self.ButtonBlock) {
        self.ButtonBlock();
    }
}

- (void)didClickMoreButton:(void (^)())actions {
    if (actions) {
        self.ButtonBlock = actions;
    }
}

- (void)setTextLength:(NSUInteger)length {
    if (length >= 6000) {
        self.limitLabel.textColor = [UIColor redColor];
    } else {
        self.limitLabel.textColor = [UIColor lightGrayColor];
    }
    self.limitLabel.text = [NSString stringWithFormat:@"%@/6000",@(length)];
    [self updateLabelsFrame];
}

@end
