//
//  PDPostReportList.h
//  PandaKing
//
//  Created by Cranz on 17/5/24.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPostReportItem.h"

@interface PDPostReportList : FetchModel
@property (nonatomic, strong) NSArray <PDPostReportItem>*items;
@end
