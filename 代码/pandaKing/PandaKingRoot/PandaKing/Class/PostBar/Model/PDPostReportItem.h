//
//  PDPostReportItem.h
//  PandaKing
//
//  Created by Cranz on 17/5/24.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDPostReportItem <NSObject>

@end

@interface PDPostReportItem : FetchModel
@property (nonatomic, copy) NSString *reportId;
@property (nonatomic, copy) NSString *reportReason;
@end
