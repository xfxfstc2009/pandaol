//
//  PDPostBarList.h
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPostBarItem.h"

@interface PDPostBarList : FetchModel
@property (nonatomic) BOOL hasNextPage;
@property (nonatomic, strong) NSArray <PDPostBarItem>*items;
@end
