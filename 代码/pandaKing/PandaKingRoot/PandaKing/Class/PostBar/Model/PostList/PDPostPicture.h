//
//  PDPostPicture.h
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDPostPicture <NSObject>

@end

@interface PDPostPicture : FetchModel
@property (nonatomic, copy) NSString *pictureId;
@property (nonatomic, copy) NSString *pictureRoute;
@property (nonatomic, copy) NSString *postId;
@end
