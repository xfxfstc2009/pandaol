//
//  PDPostBarItem.h
//  PandaKing
//
//  Created by Cranz on 17/6/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPostPicture.h"

@protocol PDPostBarItem <NSObject>

@end

@interface PDPostBarItem : FetchModel
@property (nonatomic) NSInteger postId;
@property (nonatomic, copy) NSString *gameType;
/**
 * 有没有点过赞
 */
@property (nonatomic) BOOL good;
/**
 * 判断是否是楼主
 */
@property (nonatomic) BOOL host;
@property (nonatomic) NSUInteger level;
/**
 * 是否是我的帖子
 */
@property (nonatomic) BOOL me;
@property (nonatomic, strong) NSArray <PDPostPicture>*pictures;
@property (nonatomic, copy) NSString *postContent;
@property (nonatomic, copy) NSString *postTitle;
/**
 * 帖子的创建时间
 */
@property (nonatomic, assign) NSTimeInterval postCreateTime;
@property (nonatomic) NSTimeInterval postUpdateTime;
/**
 * 点赞数
 */
@property (nonatomic) NSUInteger postGood;
/**
 * 帖子回复数
 */
@property (nonatomic) NSUInteger postReply;
/**
 * 帖子是否置顶
 */
@property (nonatomic) BOOL postTop;
/**
 * 帖子是否锁定
 */
@property (nonatomic) BOOL postStatus;
/**
 * 用户头像
 */
@property (nonatomic, copy) NSString *postUserHead;
/**
 * 用户id
 */
@property (nonatomic, copy) NSString *postUserId;
/**
 * 用户昵称
 */
@property (nonatomic, copy) NSString *postUserName;
/**
 * 是否是管理员
 */
@property (nonatomic) BOOL admin;
@end

