//
//  PDPostDetailReplyModel.h
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPostPicture.h"
#import "PDPostDetailReplyItem.h"

@protocol PDPostDetailReplyModel <NSObject>

@end

/// 评论的列表model
@interface PDPostDetailReplyModel : FetchModel
@property (nonatomic) BOOL admin;
/**
 * 此时的评论数
 */
@property (nonatomic) NSInteger postReply;
/**
 * 楼层
 */
@property (nonatomic) NSInteger floor;
@property (nonatomic) NSInteger replyId;
/**
 * 评论的id
 */
@property (nonatomic) NSInteger postId;
@property (nonatomic) NSInteger parentId;
/**
 * 是否点过赞
 */
@property (nonatomic) BOOL good;
/**
 * 点赞的个数
 */
@property (nonatomic) NSUInteger postGood;
@property (nonatomic) NSUInteger level;
/**
 * 是否是楼主
 */
@property (nonatomic) BOOL host;
/**
 * 是否是自己的评论
 */
@property (nonatomic) BOOL me;
/**
 * 回复
 */
@property (nonatomic, copy) NSString *postContent;
/**
 * 发表评论的用户名
 */
@property (nonatomic, copy) NSString *postUserName;
/**
 * 用户id
 */
@property (nonatomic, copy) NSString *postUserId;
@property (nonatomic, copy) NSString *postUserHead;
@property (nonatomic) NSTimeInterval postCreateTime;
@property (nonatomic, strong) NSArray <PDPostPicture>*pictures;
@property (nonatomic, strong) NSArray <PDPostDetailReplyItem>*replys;
@end
