//
//  PDPostDetailPriseUserItem.h
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDPostDetailPriseUserItem <NSObject>


@end

/// 点赞的用户model
@interface PDPostDetailPriseUserItem : FetchModel
@property (nonatomic, copy) NSString *upvoteId;
@property (nonatomic, copy) NSString *postId;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *userName;
@property (nonatomic, copy) NSString *userHead;
@end
