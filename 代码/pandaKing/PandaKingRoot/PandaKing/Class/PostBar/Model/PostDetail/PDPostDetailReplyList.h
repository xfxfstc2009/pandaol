//
//  PDPostDetailReplyList.h
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPostDetailReplyModel.h"

@interface PDPostDetailReplyList : FetchModel
@property (nonatomic) BOOL hasNextPage;
@property (nonatomic, strong) NSArray <PDPostDetailReplyModel>*items;
/**
 * 评论个数
 */
@property (nonatomic) NSInteger totalItemsCount;
@end
