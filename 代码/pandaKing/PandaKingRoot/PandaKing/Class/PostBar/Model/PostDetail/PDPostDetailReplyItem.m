//
//  PDPostDetailReplyItem.m
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailReplyItem.h"

@implementation PDPostDetailReplyItem
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"replyId":@"id"};
}
@end
