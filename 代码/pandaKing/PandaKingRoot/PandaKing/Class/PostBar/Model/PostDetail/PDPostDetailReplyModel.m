//
//  PDPostDetailReplyModel.m
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostDetailReplyModel.h"

@implementation PDPostDetailReplyModel
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"replyId":@"id"};
}
@end
