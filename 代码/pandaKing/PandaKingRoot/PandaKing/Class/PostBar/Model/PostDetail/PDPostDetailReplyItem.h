//
//  PDPostDetailReplyItem.h
//  PandaKing
//
//  Created by Cranz on 17/6/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDPostDetailReplyItem <NSObject>

@end

// 楼中楼单条回复
@interface PDPostDetailReplyItem : FetchModel
@property (nonatomic) BOOL admin;
@property (nonatomic, assign) NSInteger replyId;
/**
 * 这条评论的id
 */
@property (nonatomic) NSInteger postId;
@property (nonatomic) NSInteger parentId;
/**
 * 是不是楼主
 */
@property (nonatomic) BOOL host;
@property (nonatomic) NSTimeInterval postCreateTime;
/**
 * 回复者的名字
 */
@property (nonatomic, copy) NSString *userName;
/**
 * 回复者的id
 */
@property (nonatomic, copy) NSString *postUserId;
/**
 * 回复者的头像
 */
@property (nonatomic, copy) NSString *postUserHead;
/**
 * 被回复者的名字
 */
@property (nonatomic, copy) NSString *postReplyUserName;
/**
 * 回复内容
 */
@property (nonatomic, copy) NSString *postContent;
@end
