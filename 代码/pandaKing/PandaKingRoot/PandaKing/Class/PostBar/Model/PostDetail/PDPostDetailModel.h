//
//  PDPostDetailModel.h
//  PandaKing
//
//  Created by Cranz on 17/6/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPostPicture.h"
#import "PDPostDetailPriseUserItem.h"

@interface PDPostDetailModel : FetchModel
@property (nonatomic, copy) NSString *postId;
@property (nonatomic, copy) NSString *postUserId;
@property (nonatomic, copy) NSString *postUserName;
@property (nonatomic, copy) NSString *postUserHead;
@property (nonatomic) NSUInteger level;
/**
 * 是否死后管理员
 */
@property (nonatomic) BOOL admin;
/**
 * 帖子标题
 */
@property (nonatomic, copy) NSString *postTitle;
/**
 * 帖子内容
 */
@property (nonatomic, copy) NSString *postContent;
@property (nonatomic, copy) NSString *gameType;
/**
 * 自己有没有点过赞
 */
@property (nonatomic) BOOL good;
/**
 * 是不是自己的帖子
 */
@property (nonatomic) BOOL me;
/**
 * 点赞的个数
 */
@property (nonatomic) NSUInteger postGood;
/**
 * 回复的个数
 */
@property (nonatomic) NSUInteger postReply;
/**
 * 看帖个数
 */
@property (nonatomic) NSUInteger postView;
/**
 * 帖子创建的时间
 */
@property (nonatomic) NSTimeInterval postCreateTime;
/**
 * 是否置顶
 */
@property (nonatomic) BOOL postTop;
/**
 * 是否锁定 0:没被锁定， 1:被锁定
 */
@property (nonatomic) BOOL postStatus;
/**
 * 是否删除
 */
@property (nonatomic) BOOL postDelete;
@property (nonatomic, strong) NSArray <PDPostPicture> *pictures;
@property (nonatomic, strong) NSArray <PDPostDetailPriseUserItem> *upvotes;
@end
