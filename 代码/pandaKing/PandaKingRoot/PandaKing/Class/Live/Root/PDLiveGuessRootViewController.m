//
//  PDLiveGuessRootViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveGuessRootViewController.h"
#import "PDLivePlayerSubViewController.h"
#import "PDLiveGuessSingleCollectionViewCell.h"
#import "PDLiveGuessHeaderCollectionViewCell.h"
#import "PDLiveGuessBettingViewController.h"
#import "PDLiveMainLiveCollectionViewCell.h"
#import "PDLiveGuessRootHeaderCollectionReusableView.h"
#import "PDLiveMainLiveHeaderView.h"
#import "PDLiveRoomListModel.h"
#import "PDLiveGuessRootBottomPanView.h"
#import "PDLotteryGameTouzhuViewController.h"                   // 进行投注
#import "NetworkAdapter+PDJavaLogin.h"
#import "PDGuessListLogoutModel.h"
#import "PDTopUpViewController.h"
#import "PDLiveGuessBettingViewController.h"
#import "PDLiveGuessTouzhuModel.h"
#import "PDGuessListModel.h"
#import "ZFPlayerView.h"


static NSString *headerCellIdentify = @"headerCellIdentify";
static NSString *cellIdentify = @"cellIdentify";
static NSString *zhengdianCellIdentify = @"zhengdianCellIdentify";
static NSString *headerIdentify = @"headerIdentify";

@interface PDLiveGuessRootViewController ()<UICollectionViewDataSource,UICollectionViewDelegate,UICollectionViewDelegateFlowLayout,PDNetworkAdapterDelegate,PDSocketConnectionDelegate>{
    PDLiveRoomListModel *liveRoomListModel;
}
@property (nonatomic,strong)NSMutableArray *liveMutableArr;
@property (nonatomic,strong)UICollectionView *liveCollectionView;
@property (nonatomic,strong)NSMutableArray *candidateMutableArr;                /**< 候选主播*/
@property (nonatomic,strong)NSMutableArray *hotMutableArr;                      /**< 其他主播*/
@property (nonatomic,strong)PDLiveMainLiveHeaderView *headerView;
@property (nonatomic,strong)PDLiveGuessRootBottomPanView *bottomPanView;
@property (nonatomic,strong)UIPanGestureRecognizer *panRecognizer;              /**< 拖拉手势*/

@end

@implementation PDLiveGuessRootViewController

-(void)dealloc{
    NSLog(@"释放");
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if (self.liveCollectionView){
        [self.liveCollectionView reloadData];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([NetworkAdapter sharedAdapter]){
        [NetworkAdapter sharedAdapter].delegate = self;
    }
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self sendRequestToLogoutLiveGuess];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createCollectionView];
    [self createRootHeaderView];
    [self interfaceManager];
    [self createBottomPanView];
}

#pragma mark - InterfaceManager
-(void)interfaceManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendReqeustTogetLiveActivity];
}

-(void)createRootHeaderView{
    self.headerView = [[PDLiveMainLiveHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, headerSize_Height)];
    self.headerView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.headerView.layer.shadowColor = [[UIColor colorWithCustomerName:@"灰"] CGColor];
    self.headerView.layer.shadowOpacity = 1.0;
    self.headerView.layer.shadowOffset = CGSizeMake(.6f, .4f);
    [self.view addSubview:self.headerView];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat pointY = scrollView.contentOffset.y;
    if (pointY > (headerSize_Height - 44)){
        self.headerView.orgin_y = - (headerSize_Height - 44);
    } else {
        self.headerView.orgin_y = - pointY;
    }
    [self.headerView changeFrameWithOriginSet:pointY];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"主播猜";

    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_page_cancle"] barHltImage:[UIImage imageNamed:@"icon_page_cancle"] action:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf != [strongSelf.navigationController.viewControllers firstObject]){
            [strongSelf.navigationController popViewControllerAnimated:YES];
        } else {
            [strongSelf dismissViewControllerAnimated:YES completion:NULL];
        }
    }];
    
    [weakSelf rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_live_guize"] barHltImage:[UIImage imageNamed:@"icon_live_guize"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDLivePlayerSubViewController *playerViewController = [[PDLivePlayerSubViewController alloc]init];
        playerViewController.transferActivityId = strongSelf ->liveRoomListModel.activityId;
        playerViewController.hasCancelPanDismiss = YES;
        [strongSelf.navigationController pushViewController:playerViewController animated:YES];
    }];
}


#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.liveMutableArr = [NSMutableArray array];
    self.candidateMutableArr = [NSMutableArray array];
    self.hotMutableArr = [NSMutableArray array];
    
    // 添加
    [self.liveMutableArr addObject:self.candidateMutableArr];
    
    // 添加热门主播
    [self.liveMutableArr addObject:self.hotMutableArr];
}

#pragma mark - UICollectionView
-(void)createCollectionView{
    if (!self.liveCollectionView){
        UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
        self.liveCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        self.liveCollectionView.showsVerticalScrollIndicator = NO;
        self.liveCollectionView.backgroundColor = [UIColor whiteColor];
        self.liveCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.liveCollectionView.delegate = self;
        self.liveCollectionView.dataSource = self;
        self.liveCollectionView.scrollsToTop = YES;
        // 注册 Cell - collectionIdentify
        [self.liveCollectionView registerClass:[PDLiveGuessSingleCollectionViewCell class] forCellWithReuseIdentifier:cellIdentify];
        // normal
        [self.liveCollectionView registerClass:[PDLiveGuessHeaderCollectionViewCell class] forCellWithReuseIdentifier:headerCellIdentify];
        
        // 整点主播
        [self.liveCollectionView registerClass:[PDLiveMainLiveCollectionViewCell class] forCellWithReuseIdentifier:zhengdianCellIdentify];
        
        [self.liveCollectionView registerClass:[PDLiveGuessRootHeaderCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerIdentify];
        
        [self.view addSubview:self.liveCollectionView];
    }
}

#pragma mark - UICollectionViewCell
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.liveMutableArr.count;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.liveMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"整点主播"] && indexPath.row == [self cellIndexPathRowWithcellData:@"整点主播"]){
        PDLiveMainLiveCollectionViewCell *cell = (PDLiveMainLiveCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:zhengdianCellIdentify forIndexPath:indexPath];
        return cell;

    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"候选主播"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"候选主播"]){
            PDLiveGuessHeaderCollectionViewCell *cell = (PDLiveGuessHeaderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:headerCellIdentify forIndexPath:indexPath];
            cell.transferTitleStr = @"候选直播";
            return cell;
        } else {
            PDLiveGuessSingleCollectionViewCell *cell = (PDLiveGuessSingleCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentify forIndexPath:indexPath];
            cell.transferLiveRootSingleModel = [[self.liveMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            return cell;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"热门主播"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"热门主播"]){
            PDLiveGuessHeaderCollectionViewCell *cell = (PDLiveGuessHeaderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:headerCellIdentify forIndexPath:indexPath];
            cell.transferTitleStr = @"热门主播";
            return cell;
        } else {
            PDLiveGuessSingleCollectionViewCell *cell = (PDLiveGuessSingleCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentify forIndexPath:indexPath];
            cell.transferLiveRootSingleModel = [[self.liveMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            return cell;
        }
    } else {
        PDLiveGuessHeaderCollectionViewCell *cell = (PDLiveGuessHeaderCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:headerCellIdentify forIndexPath:indexPath];
        cell.backgroundColor = UURandomColor;
        cell.transferTitleStr = @"其他";
        return cell;
    }
    
    return nil;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    CGSize size;
    if (section == 0){
        size = CGSizeMake(kScreenBounds.size.width, headerSize_Height);
    } else {
        size = CGSizeMake(kScreenBounds.size.width, 0);
    }

    return size;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    PDLiveGuessRootHeaderCollectionReusableView *headView;
    
    if([kind isEqual:UICollectionElementKindSectionHeader]) {
        headView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:headerIdentify forIndexPath:indexPath];
        headView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    }
    return headView;
}

#pragma mark - UICollectionViewDelegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    PDLivePlayerSubViewController *playerViewController = [[PDLivePlayerSubViewController alloc]init];
    PDLiveRoomSingleModel *liveRootSingleModel = [[self.liveMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    playerViewController.transferLiveRootSingleModel = liveRootSingleModel;
    playerViewController.hasCancelPanDismiss = YES;
    playerViewController.transferActivityId = liveRoomListModel.activityId;
    [self.navigationController pushViewController:playerViewController animated:YES];
}



#pragma mark - UICollectionViewDelegateFlowLayout;
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"整点主播"] && indexPath.row == [self cellIndexPathRowWithcellData:@"整点主播"]){
        return [PDLiveMainLiveCollectionViewCell calculationCellSize];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"候选主播"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"候选主播"]){
           return CGSizeMake(kScreenBounds.size.width, LCFloat(44));
        } else {
           return [PDLiveGuessSingleCollectionViewCell calculationCellSize];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"热门主播"]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"热门主播"]){
            return CGSizeMake(kScreenBounds.size.width, LCFloat(44));
        } else {
            return [PDLiveGuessSingleCollectionViewCell calculationCellSize];
        }
    } else {
        return CGSizeMake(kScreenBounds.size.width, LCFloat(44));
    }
}



#pragma mark - Other Manager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.liveMutableArr.count ; i++){
        NSArray *dataTempArr = [self.liveMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.liveMutableArr.count ; i++){
        NSArray *dataTempArr = [self.liveMutableArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;;
}

#pragma mark - 获取当前主播猜
-(void)sendReqeustTogetLiveActivity{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:latestanchoractivity requestParams:nil responseObjectClass:[PDLiveRoomListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            strongSelf->liveRoomListModel = (PDLiveRoomListModel *)responseObject;
            // 保存当前的主播猜
            [AccountModel sharedAccountModel].liveActivityId = strongSelf->liveRoomListModel.activityId;
            [strongSelf getCurrentLiveActivity];
        }
    }];
}

-(void)getCurrentLiveActivity{

    if (liveRoomListModel.hotRooms.count){
        [self.hotMutableArr removeAllObjects];
        [self.hotMutableArr addObject:@"热门主播"];
        [self.hotMutableArr addObjectsFromArray:liveRoomListModel.hotRooms];
    }
    if (liveRoomListModel.chooseRooms.count){
        [self.candidateMutableArr removeAllObjects];
        [self.candidateMutableArr addObject:@"候选主播"];
        [self.candidateMutableArr addObjectsFromArray:liveRoomListModel.chooseRooms];
    }
    self.bottomPanView.transferActivityId = liveRoomListModel.activityId;
    [self.liveCollectionView reloadData];
    
    // 获取未来开始时间
    liveRoomListModel.tempTargetNextStateInterval = [NSDate getNSTimeIntervalWithCurrent] + liveRoomListModel.nextStateInterval;
    
    // 更新底部信息
    if (self.bottomPanView){
        [self.bottomPanView reloadBottomViewWithModel:liveRoomListModel];
    }
    if (self.headerView){
        [self.headerView reloadHeaderInfoWithModel:liveRoomListModel];
    }
}

#pragma mark - 创建底部可以拖拉的view
-(void)createBottomPanView{
    CGRect rect = CGRectMake(0, kScreenBounds.size.height - 64 - LCFloat(44), kScreenBounds.size.width, kScreenBounds.size.height * .6f + LCFloat(44));
    self.bottomPanView = [[PDLiveGuessRootBottomPanView alloc]initWithFrame:rect withActivityId:liveRoomListModel.activityId];
    __weak typeof(self)weakSelf = self;
    [self.bottomPanView topTapManagerWithBlcok:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.bottomPanView.isShow){           // 【当前是显示状态/ 要关闭】
            [UIView animateWithDuration:.5f animations:^{
                strongSelf.bottomPanView.orgin_y = kScreenBounds.size.height - 64 - LCFloat(44);
            } completion:^(BOOL finished) {
                strongSelf.bottomPanView.orgin_y = kScreenBounds.size.height - 64 - LCFloat(44);
            }];
        } else {                                        // 【当前是关闭状态要显示】
            [UIView animateWithDuration:.5f animations:^{
                strongSelf.bottomPanView.orgin_y = kScreenBounds.size.height * .4f - LCFloat(44);
            } completion:^(BOOL finished) {
                strongSelf.bottomPanView.orgin_y = kScreenBounds.size.height - strongSelf.bottomPanView.size_height;
            }];
        }
        strongSelf.bottomPanView.isShow = !strongSelf.bottomPanView.isShow;
    }];
    
    [self.bottomPanView pointItemDidSelected:^(PDGuessPointSingleModel *point) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf touZhuManagerWithPointModel:point];
    }];
    
    [self.view addSubview:self.bottomPanView];
    
    // 【添加手势】
    [self addPanGestureManager];
}

-(void)touZhuManagerWithPointModel:(PDGuessPointSingleModel *)point{
    __weak typeof(self)weakSelf = self;
    [weakSelf fetchMemberPropertyComplicationHandle:^(NSUInteger maxerGold) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDLiveGuessBettingViewController *touzhuViewController = [[PDLiveGuessBettingViewController alloc]init];
        touzhuViewController.hasCancelSocket = YES;
        touzhuViewController.isHasGesture = YES;
        point.tempMaxerGold = maxerGold;
        touzhuViewController.memberMaximalGold = maxerGold;
        touzhuViewController.transferPointSingleModel = point;
        [touzhuViewController touzhuBtnClickManager:^(NSString *gold) {
            if ([gold isEqualToString:@"0"]){
                [PDHUD showHUDError:@"请选择竞猜金币"];
                return ;
            }
            [strongSelf sendRequestToStakeanchorguessWithGuessId:point.tempGuessId gold:gold onPointA:point.isPointA odds:point.odds];
        }];
        [touzhuViewController showInView:strongSelf.parentViewController];
    }];

}


#pragma mark - 增加手势
-(void)addPanGestureManager{
    self.panRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(menuHandlePan:)];
    [self.bottomPanView addGestureRecognizer:self.panRecognizer];
}

- (void)menuHandlePan:(UIPanGestureRecognizer *)recognizer {
    CGPoint translation = [recognizer translationInView:self.bottomPanView];
    self.bottomPanView.center = CGPointMake(recognizer.view.center.x, recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.bottomPanView];
    
    static CGFloat startLocationY = 0;
    CGPoint location = [recognizer locationInView:[UIApplication sharedApplication].keyWindow];
    CGFloat progress = (location.y - startLocationY) / [UIScreen mainScreen].bounds.size.width;
    progress = -progress;
    progress = MIN(1.0, MAX(0.0, progress));
    
    if (recognizer.state == UIGestureRecognizerStateBegan) {
        startLocationY = location.y;
        
    } else if (recognizer.state == UIGestureRecognizerStateChanged) {
        if (self.bottomPanView.orgin_y <= kScreenBounds.size.height *.4f - LCFloat(44)){
            self.bottomPanView.orgin_y = kScreenBounds.size.height * .4f - LCFloat(44);
        }
        
    } else if (recognizer.state == UIGestureRecognizerStateEnded || recognizer.state == UIGestureRecognizerStateCancelled) {
        if (progress >= 0.2){
            [self panNormalManager];
        } else {
            [self panDismissManager];
        }
    }
}

#pragma mark - panDismiss
-(void)panDismissManager{
    [UIView animateWithDuration:.3f animations:^{
        self.bottomPanView.orgin_y = kScreenBounds.size.height - 64 - LCFloat(44);
    } completion:^(BOOL finished) {
        self.bottomPanView.isShow = NO;
    }];
}

-(void)panNormalManager{
    [UIView animateWithDuration:.3f animations:^{
        self.bottomPanView.orgin_y = kScreenBounds.size.height * .4f - LCFloat(44);
    } completion:^(BOOL finished) {
        self.bottomPanView.isShow = YES;
    }];
}



#pragma mark - Socket Back
-(void)socketDidBackData:(id)responseObject{
    NSLog(@"%@",responseObject);
    
    if ([responseObject isKindOfClass:[NSString class]]){
        
    } else if ([responseObject isKindOfClass:[NSDictionary class]]){
        NSDictionary *infoDic = [responseObject objectForKey:@"data"];
        PDBaseSocketRootModel *responseModelObject = [[PDBaseSocketRootModel  alloc] initWithJSONDict:infoDic];
        if (responseModelObject.type == socketType900){
            liveRoomListModel = [[PDLiveRoomListModel  alloc] initWithJSONDict:infoDic];
            [self getCurrentLiveActivity];
            if ([liveRoomListModel.state isEqualToString:@"broadcast"]){
                [self searchWattingAnchorShowInWindow];
            }
        } else if (responseModelObject.type == socketType901){
            PDGuessListModel *guessListModel = [[PDGuessListModel  alloc] initWithJSONDict:infoDic];
            [self.bottomPanView reloadLotteryListWithInfoList:guessListModel];
        }
    }
}

-(void)searchWattingAnchorShowInWindow{
    NSInteger indexSec = [self cellIndexPathSectionWithcellData:@"候选主播"] + 1;
    
    // 1. 获取选中的主播
    NSInteger index = -1;
    for (int i = 0 ; i < liveRoomListModel.chooseRooms.count;i++){
        PDLiveRoomSingleModel *tempSelectedModel = [liveRoomListModel.chooseRooms objectAtIndex:i];
        if ([liveRoomListModel.choosedRoom.roomId isEqualToString:tempSelectedModel.roomId]){
            index = i;
            break;
        }
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:indexSec];
    PDLiveGuessSingleCollectionViewCell *cell = (PDLiveGuessSingleCollectionViewCell *)[self.liveCollectionView cellForItemAtIndexPath:indexPath];
    
    // 1. 寻找当前cell 上面的图片
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    CGRect convertFrame = [cell convertRect:cell.avatar.frame toView:keyWindow];
    
    PDImageView *animationImageView = [[PDImageView alloc]init];
    animationImageView.frame = convertFrame;
    [animationImageView uploadImageWithURL:liveRoomListModel.choosedRoom.coverImg placeholder:nil callback:NULL];
    
    [keyWindow addSubview:animationImageView];
    [UIView animateWithDuration:3. animations:^{
        animationImageView.frame = CGRectMake(LCFloat(11), 300, kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(200));
    } completion:^(BOOL finished) {
        // 2. 执行下一步
        [self AnchorAnnouncementManagerWithWindowSingleView:animationImageView];
    }];
}

#pragma mark - 主播揭晓
-(void)AnchorAnnouncementManagerWithWindowSingleView:(PDImageView *)windowView{
    // 1. 判断是否有揭晓主播
    if ([self cellIndexPathSectionWithcellData:@"整点主播"] == -1){           //
        [self.liveMutableArr insertObject:@[@"整点主播"] atIndex:0];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
        [self.liveCollectionView insertSections:indexSet];
    }
    if ([self cellIndexPathSectionWithcellData:@"候选主播"] != -1){
        NSInteger indexSec = [self cellIndexPathSectionWithcellData:@"候选主播"];
        [self.liveMutableArr removeObjectAtIndex:indexSec];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:indexSec];
        [self.liveCollectionView deleteSections:indexSet];
    }
    
    // 2. 寻找到对应的cell
    NSInteger indexSec = [self cellIndexPathSectionWithcellData:@"整点主播"];
    NSInteger indexRow = [self cellIndexPathRowWithcellData:@"整点主播"];
    PDLiveMainLiveCollectionViewCell *cell = (PDLiveMainLiveCollectionViewCell *)[self.liveCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:indexRow inSection:indexSec]];
    // 2.1 寻找到对应cell的头像
    
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    CGRect indexCellRect = [cell convertRect:cell.liveImageView.frame toView:keyWindow];
    [UIView animateWithDuration:2.f animations:^{
        windowView.frame = indexCellRect;
    } completion:^(BOOL finished) {
        [windowView removeFromSuperview];
        [cell animationWithZhuboLoadingWithModel:liveRoomListModel.choosedRoom];
    }];
}

#pragma mark - 退出主播猜
-(void)sendRequestToLogoutLiveGuess{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:guessQuitlatestanchoractivity requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (!isSucceeded){
            [strongSelf sendRequestToLogoutLiveGuess];
        }
    }];
}

#pragma mark - 退出查看竞猜列表
-(void)guessQuitanchorguesslist{
    __weak typeof(self)weakSlef = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:guessQuitanchorguesslist requestParams:nil responseObjectClass:[PDGuessListLogoutModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSlef){
            return ;
        }
//        __strong typeof(weakSlef)strongSelf = weakSlef;
        if (isSucceeded){
//            PDGuessListLogoutModel *guesslogoutModel = (PDGuessListLogoutModel *)responseObject;
            
        }
    }];
}

#pragma mark - 获取当前的金币
- (void)fetchMemberPropertyComplicationHandle:(void(^)(NSUInteger maxerGold))complicationHandle {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberWallet requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            NSNumber *totalgold = responseObject[@"totalgold"]; // 总资产
            if (totalgold.integerValue < 100) { // 去充值
                [weakSelf showNotEnough];
            } else {
                if (complicationHandle) {
                    complicationHandle(totalgold.integerValue);
                }
            }
        }
    }];
}

/** 余额不足*/
- (void)showNotEnough {
    __weak typeof(self) weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"余额不足" conten:@"最低竞猜金额为100金币" isClose:NO btnArr:@[@"去充值", @"取消"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissWithCompletion:NULL];
        if (buttonIndex == 0) {
            PDTopUpViewController *topUpViewController = [[PDTopUpViewController alloc] init];
            [weakSelf pushViewController:topUpViewController animated:YES];
        }
    }];
}

#pragma mark - 进行投注
-(void)sendRequestToStakeanchorguessWithGuessId:(NSString *)guessId gold:(NSString *)gold onPointA:(BOOL)onPointA odds:(CGFloat)odds{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"guessId":guessId,@"gold":gold,@"onPointA":@(onPointA),@"odds":@(odds)};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:guessStakeanchorguess requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
        
    }];
    
}


@end


