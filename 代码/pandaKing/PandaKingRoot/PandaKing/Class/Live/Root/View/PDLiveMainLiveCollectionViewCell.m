//
//  PDLiveMainLiveCollectionViewCell.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveMainLiveCollectionViewCell.h"

@interface PDLiveMainLiveCollectionViewCell()
@property (nonatomic,strong)UIView *topView;
@property (nonatomic,strong)UIView *bottomView;


@property (nonatomic,strong)PDImageView *playerbutton;      /**< 播放按钮*/
@property (nonatomic,strong)LTMorphingLabel *nameLabel;             /**< 主播名字*/
@property (nonatomic,strong)UILabel *countLabel;            /**< 播放数量label*/
@property (nonatomic,strong)PDImageView *countImgView;      /**< 播放图片*/
@property (nonatomic,strong)LTMorphingLabel *dymicLabel;            /**< 动态label*/
@end

@implementation PDLiveMainLiveCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建上面的view
    self.topView = [[UIView alloc]init];
    self.topView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.topView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(80));
    [self addSubview:self.topView];
    
    // 2. 创建底部的白色的view
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bottomView];
    
    // 3. 创建播放图片
    self.liveImageView = [[PDImageView alloc]init];
    self.liveImageView.backgroundColor = [UIColor clearColor];
    self.liveImageView.frame = CGRectMake(LCFloat(11), 0, kScreenBounds.size.width - 2 * LCFloat(11), LCFloat(200));
    [self addSubview:self.liveImageView];
    
    // 4. 创建名字
    self.nameLabel = [[LTMorphingLabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"]boldFont];
    self.nameLabel.morphingEffect = LTMorphingEffectBurn;
    self.nameLabel.text = @"";
    self.nameLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.liveImageView.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.nameLabel.font]);
    [self addSubview:self.nameLabel];
    
    // 5. 创建数量
    self.countLabel = [[UILabel alloc]init];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.font = [UIFont systemFontOfCustomeSize:8.];
    self.countLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - 100, 0, 100, [NSString contentofHeightWithFont:self.countLabel.font]);
    self.countLabel.center_y = self.nameLabel.center_y;
    [self addSubview:self.countLabel];
    
    // 6.数量图片
    self.countImgView = [[PDImageView alloc]init];
    self.countImgView.backgroundColor = [UIColor clearColor];
    self.countImgView.frame = CGRectMake(self.countLabel.orgin_x - LCFloat(15), 0, LCFloat(15), LCFloat(15));
    self.countImgView.center_y = self.countLabel.center_y;
    [self addSubview:self.countImgView];
    
    // 7 .其他
    self.dymicLabel = [[LTMorphingLabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.dymicLabel.morphingEffect = LTMorphingEffectSparkle;
    self.dymicLabel.text = @"";
    self.dymicLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.dymicLabel.font]);
    [self addSubview:self.dymicLabel];
    
}

- (void)animationWithZhuboLoadingWithModel:(PDLiveRoomSingleModel *)choosedRoom{
    [self.liveImageView uploadImageWithURL:choosedRoom.coverImg placeholder:nil callback:NULL];
    self.nameLabel.text = choosedRoom.anchorName;
    self.dymicLabel.text = choosedRoom.title;
}


+(CGSize)calculationCellSize{
    CGFloat cellHeight = 0 ;
    cellHeight += LCFloat(200);
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[[UIFont fontWithCustomerSizeName:@"正文"] boldFont]];
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    return CGSizeMake(kScreenBounds.size.width, cellHeight);
}
@end
