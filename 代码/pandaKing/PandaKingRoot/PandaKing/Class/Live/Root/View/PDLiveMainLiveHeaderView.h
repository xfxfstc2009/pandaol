//
//  PDLiveMainLiveHeaderView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLiveGuessRootViewController.h"
#import "PDLiveRoomListModel.h"

@class PDLiveGuessRootViewController;
@interface PDLiveMainLiveHeaderView : UIView

-(void)changeFrameWithOriginSet:(CGFloat)origin;

-(void)reloadHeaderInfoWithModel:(PDLiveRoomListModel *)transferLiveRootListModel;

@end
