//
//  PDLiveGuessPanViewCell.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveGuessPanViewCell.h"
#import "PDLiveGuessSingleItemView.h"


static char itemTagKey;
@interface PDLiveGuessPanViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *guessBgView;
@property (nonatomic,strong)NSMutableArray *isSelectedMutableArr;
@property (nonatomic,strong)NSMutableArray *tagViewMutableArr;


@end

@implementation PDLiveGuessPanViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    
    // 1. 创建竞猜标的
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.titleLabel];
    
    // 2. 创建view
    self.guessBgView = [[UIView alloc]init];
    self.guessBgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.guessBgView];
    
    if (!self.isSelectedMutableArr){
        self.isSelectedMutableArr = [NSMutableArray array];
    }
    if (!self.tagViewMutableArr){
        self.tagViewMutableArr = [NSMutableArray array];
    }
}

-(void)setTransferCellHeigt:(CGFloat)transferCellHeigt{
    _transferCellHeigt = transferCellHeigt;
}

-(void)setTransferGuessDetailModel:(PDGuessDetailModel *)transferGuessDetailModel{
    if (self.isSelectedMutableArr.count && self.isSelectedMutableArr){
        [self.isSelectedMutableArr removeAllObjects];
    }
    
    
    _transferGuessDetailModel = transferGuessDetailModel;
    // 1. 名字
    self.titleLabel.text = transferGuessDetailModel.name;
    self.titleLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(40));
    
    // 2.
    self.guessBgView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), kScreenBounds.size.width, [PDLiveGuessSingleItemView calculationItemHeight]);
    
    if (self.guessBgView.subviews.count){
        [self.guessBgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    CGFloat margin = (kScreenBounds.size.width - LCFloat(100) * 2) / 3.;
    __weak typeof(self)weakSlef = self;
    for (int i = 0 ; i < 2 ;i++){
        PDLiveGuessSingleItemView *guessItem = [[PDLiveGuessSingleItemView alloc]initWithFrame:CGRectMake(margin + (LCFloat(100) + margin) * i, 0, LCFloat(100), [PDLiveGuessSingleItemView calculationItemHeight])];
        guessItem.transferCellHeight = [PDLiveGuessSingleItemView calculationItemHeight];
        if (i == 0){
            transferGuessDetailModel.pointA.tempForbidStake = self.transferGuessDetailModel.forbidStake;
            guessItem.transferGuessPointSingleModel = transferGuessDetailModel.pointA;
        } else {
            transferGuessDetailModel.pointB.tempForbidStake = self.transferGuessDetailModel.forbidStake;
            guessItem.transferGuessPointSingleModel = transferGuessDetailModel.pointB;
        }

        [guessItem tagItemDidSelectManager:^{
            if (!weakSlef){
                return ;
            }
            __strong typeof(weakSlef)strongSelf = weakSlef;
            void(^block)(PDGuessPointSingleModel *point) = objc_getAssociatedObject(strongSelf, &itemTagKey);
            if (block){
                PDGuessPointSingleModel *pointSingleModel = guessItem.transferGuessPointSingleModel;
                pointSingleModel.isPointA = (i == 0 ? YES:NO);
                pointSingleModel.tempGuessId = strongSelf.transferGuessDetailModel.guessId;
                block(pointSingleModel);
            }
            [strongSelf selectedInfoSelectedLayerManager:guessItem.transferGuessPointSingleModel];
        }];
        
        [self.guessBgView addSubview:guessItem];
        [self.tagViewMutableArr addObject:guessItem];
    }
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(40);
    cellHeight += LCFloat(11);
    cellHeight += [PDLiveGuessSingleItemView calculationItemHeight];
    return cellHeight;
}


-(void)tapManagerWithBlock:(void(^)(PDGuessPointSingleModel *point))block{
    objc_setAssociatedObject(self, &itemTagKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)selectedInfoSelectedLayerManager:(PDGuessPointSingleModel *)point{
    NSInteger index = -1;
    if (point == self.transferGuessDetailModel.pointA){
        index = 0;
    } else if (point == self.transferGuessDetailModel.pointB){
        index = 1;
    }
    PDLiveGuessSingleItemView *selectedGuessItem = [self.tagViewMutableArr objectAtIndex:index];
    PDLiveGuessSingleItemView *normalGuessItem = [self.tagViewMutableArr objectAtIndex:((index + 1) % 2)];
    [selectedGuessItem changeLayerColorMangerWithShow:YES];
    [normalGuessItem changeLayerColorMangerWithShow:NO];
}
@end
