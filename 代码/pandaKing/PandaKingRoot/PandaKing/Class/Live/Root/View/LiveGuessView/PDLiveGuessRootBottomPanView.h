//
//  PDLiveGuessRootBottomPanView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDGuessListModel.h"
#import "PDLiveRoomListModel.h"

@interface PDLiveGuessRootBottomPanView : UIView

-(instancetype)initWithFrame:(CGRect)frame withActivityId:(NSString *)activityId;

@property (nonatomic,copy)NSString *transferActivityId;

-(void)topTapManagerWithBlcok:(void(^)())block;

-(void)pointItemDidSelected:(void(^)(PDGuessPointSingleModel *point))block;

// 刷新底部栏
-(void)reloadBottomViewWithModel:(PDLiveRoomListModel *)bottomModel;
// 投注成功后外部返回的数据进行刷新
-(void)reloadLotteryListWithInfoList:(PDGuessListModel *)guessListModel;

@property (nonatomic,assign)BOOL isShow;
@end
