//
//  PDLiveGuessHeaderCollectionViewCell.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveGuessHeaderCollectionViewCell.h"

@interface PDLiveGuessHeaderCollectionViewCell()
@property (nonatomic,strong)UIView *fangView;
@property (nonatomic,strong)UILabel *fixedLabel;

@end

@implementation PDLiveGuessHeaderCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 0. 创建左侧的方块
    self.fangView = [[UIView alloc]init];
    self.fangView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.fangView.frame = CGRectMake(0, LCFloat(9), LCFloat(6), self.size_height - 2 * LCFloat(9));
    [self addSubview:self.fangView];
    
    // 1. 创建label
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.fangView.frame) + LCFloat(22), 0, kScreenBounds.size.width - CGRectGetMaxX(self.fangView.frame) - LCFloat(22), self.size_height);
    [self addSubview:self.fixedLabel];
}

-(void)setTransferTitleStr:(NSString *)transferTitleStr{
    _transferTitleStr = transferTitleStr;
    // fixedLabel
    self.fixedLabel.text = self.transferTitleStr;
    CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(CGRectGetMaxX(self.fangView.frame) + LCFloat(22), 0,fixedSize.width , self.size_height);
}

+(CGSize)calculationCellSize{
    return CGSizeMake(kScreenBounds.size.width, LCFloat(44));
}

@end
