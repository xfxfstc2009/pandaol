//
//  PDLiveMainLiveCollectionViewCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 主播
#import <UIKit/UIKit.h>
#import "PDLiveRoomSingleModel.h"

@interface PDLiveMainLiveCollectionViewCell : UICollectionViewCell

@property (nonatomic,strong)PDImageView *liveImageView;     /**< 主播的view*/

- (void)animationWithZhuboLoadingWithModel:(PDLiveRoomSingleModel *)choosedRoom;

+(CGSize)calculationCellSize;

@end
