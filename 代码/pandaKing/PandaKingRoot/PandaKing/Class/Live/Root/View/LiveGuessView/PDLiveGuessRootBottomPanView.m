//
//  PDLiveGuessRootBottomPanView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveGuessRootBottomPanView.h"
#import "PDLiveGuessPanViewCell.h"

static char guessDidSelectKey;
@interface PDLiveGuessRootBottomPanView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UIView *topView;
@property (nonatomic,strong)UILabel *joinLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;
@property (nonatomic,strong)UILabel *countLabel;
@property (nonatomic,strong)UILabel *lotteryFixedLabel;

// 2. 创建tableView
@property (nonatomic,strong)UITableView *guessTableView;
@property (nonatomic,strong)NSMutableArray *guessMutableArr;

@end

static char tapKey;
@implementation PDLiveGuessRootBottomPanView

-(instancetype)initWithFrame:(CGRect)frame withActivityId:(NSString *)activityId{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    [self createTopView];
    [self createTableView];
}


#pragma mark - 创建上面的view
-(void)createTopView{
    self.topView = [[UIView alloc]init];
    self.topView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.topView.alpha = .8f;
    self.topView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(44));
    self.topView.userInteractionEnabled = YES;
    [self addSubview:self.topView];
    
    self.joinLabel = [[UILabel alloc]init];
    self.joinLabel.backgroundColor = [UIColor clearColor];
    self.joinLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.joinLabel.frame = CGRectMake(LCFloat(11), 0, LCFloat(100), self.topView.size_height);
    [self.topView addSubview:self.joinLabel];
    
    // 2. 创建箭头
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(13), (self.topView.size_height - LCFloat(12)) / 2., LCFloat(13), LCFloat(12));
    self.arrowImgView.image = [UIImage imageNamed:@"icon_liveshow_hlt"];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    [self.topView addSubview:self.arrowImgView];
    
    // 3. 创建countLabel
    self.countLabel = [[UILabel alloc]init];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.countLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.countLabel.frame = CGRectMake(self.arrowImgView.orgin_x - LCFloat(11) , 0, (CGRectGetMaxX(self.arrowImgView.frame) - LCFloat(11) - CGRectGetMaxX(self.joinLabel.frame)), self.topView.size_height);
    [self.topView addSubview:self.countLabel];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(topViewActionClickManager)];
    [self.topView addGestureRecognizer:tap];
    
    self.lotteryFixedLabel = [[UILabel alloc]init];
    self.lotteryFixedLabel.backgroundColor = [UIColor clearColor];
    self.lotteryFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.lotteryFixedLabel.textAlignment = NSTextAlignmentCenter;
    self.lotteryFixedLabel.frame = CGRectMake(kScreenBounds.size.width - self.arrowImgView.orgin_x, 0, (kScreenBounds.size.width - 2 * (kScreenBounds.size.width - self.arrowImgView.orgin_x)), self.topView.size_height);
    self.lotteryFixedLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.lotteryFixedLabel.text = @"竞猜内容";
    self.lotteryFixedLabel.alpha = 0;
    [self.topView addSubview:self.lotteryFixedLabel];
    
}

#pragma mark - 2. 创建上面的tableView
-(void)createTableView{
    if (!self.guessMutableArr){
        self.guessMutableArr = [NSMutableArray array];
    }
    
    if (!self.guessTableView){
        self.guessTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.guessTableView.orgin_y = CGRectGetMaxY(self.topView.frame);
        self.guessTableView.size_height = self.size_height - CGRectGetMaxY(self.topView.frame) - 64;
        self.guessTableView.delegate = self;
        self.guessTableView.dataSource = self;
        self.guessTableView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        self.guessTableView.showsVerticalScrollIndicator = NO;
        self.guessTableView.scrollEnabled = YES;
        self.guessTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self addSubview:self.guessTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.guessMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    PDLiveGuessPanViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDLiveGuessPanViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
    }
    cellWithRowOne.transferCellHeigt = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferGuessDetailModel = [self.guessMutableArr objectAtIndex:indexPath.row];
    __weak typeof(self)weakSelf = self;
    [cellWithRowOne tapManagerWithBlock:^(PDGuessPointSingleModel *point) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^guessDidSelectBlock)(PDGuessPointSingleModel *point) = objc_getAssociatedObject(strongSelf, &guessDidSelectKey);
        if (guessDidSelectBlock){
            guessDidSelectBlock(point);
        }
    }];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PDLiveGuessPanViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.guessTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [self.guessMutableArr count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([self.guessMutableArr  count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"record"];
    }
}



#pragma mark 顶部的view 点击
-(void)topViewActionClickManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetGuessList];
    
    void(^block)() = objc_getAssociatedObject(self, &tapKey);
    if(block){
        block();
    }
}

-(void)topTapManagerWithBlcok:(void(^)())block{
    objc_setAssociatedObject(self, &tapKey, block, OBJC_ASSOCIATION_COPY);
}


-(void)setTransferActivityId:(NSString *)transferActivityId{
    _transferActivityId = transferActivityId;
}

#pragma mark - 竞猜接口
-(void)sendRequestToGetGuessList{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *paramsDic = [NSMutableDictionary dictionary];
    if (self.transferActivityId.length){
        [paramsDic setObject:self.transferActivityId forKey:@"activityId"];
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:anchorguesslist requestParams:paramsDic responseObjectClass:[PDGuessListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDGuessListModel *guessListModel = (PDGuessListModel *)responseObject;
            if (strongSelf.guessMutableArr.count){
                [strongSelf.guessMutableArr removeAllObjects];
            }
            [strongSelf.guessMutableArr removeAllObjects];
            [strongSelf.guessMutableArr addObjectsFromArray:guessListModel.items];
            [strongSelf.guessTableView reloadData];
        }
    }];
}

-(void)pointItemDidSelected:(void(^)(PDGuessPointSingleModel *point))block{
    objc_setAssociatedObject(self, &guessDidSelectKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)reloadBottomViewWithModel:(PDLiveRoomListModel *)bottomModel{
    self.joinLabel.text = [NSString stringWithFormat:@"共%li人参与",bottomModel.joinedCount];
    
    self.countLabel.text = [NSString stringWithFormat:@"总奖金池%li",bottomModel.goldCount];
    CGSize countSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.countLabel.font])];
    self.countLabel.frame = CGRectMake(self.arrowImgView.orgin_x - LCFloat(11) - countSize.width, 0, (CGRectGetMaxX(self.arrowImgView.frame) - LCFloat(11) - CGRectGetMaxX(self.joinLabel.frame)), self.topView.size_height);
    
}


-(void)setIsShow:(BOOL)isShow{
    _isShow = isShow;
    if (isShow){
        self.lotteryFixedLabel.hidden = NO;
        [UIView animateWithDuration:.5f animations:^{
            CGAffineTransform rotationTrans = CGAffineTransformMakeRotation(1 * M_PI);
            self.arrowImgView.transform = rotationTrans;
            self.joinLabel.alpha = 0;
            self.countLabel.alpha = 0;
            self.lotteryFixedLabel.alpha = 1;
        } completion:^(BOOL finished) {
            self.joinLabel.hidden = YES;
            self.countLabel.hidden = YES;
        }];
    } else {
        self.joinLabel.hidden = NO;
        self.countLabel.hidden = NO;
        [UIView animateWithDuration:.5f animations:^{
            CGAffineTransform rotationTrans = CGAffineTransformMakeRotation(2 * M_PI);
            self.arrowImgView.transform = rotationTrans;
            self.joinLabel.alpha = 1;
            self.countLabel.alpha = 1;
            self.lotteryFixedLabel.alpha = 0;
        } completion:^(BOOL finished) {
            self.lotteryFixedLabel.hidden = YES;
        }];
    }
}


-(void)reloadLotteryListWithInfoList:(PDGuessListModel *)guessListModel{
    if (self.guessMutableArr.count){
        [self.guessMutableArr removeAllObjects];
    }
    [self.guessMutableArr removeAllObjects];
    [self.guessMutableArr addObjectsFromArray:guessListModel.items];
    [self.guessTableView reloadData];
}

@end
