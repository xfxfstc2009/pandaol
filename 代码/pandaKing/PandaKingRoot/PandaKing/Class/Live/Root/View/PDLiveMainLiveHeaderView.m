//
//  PDLiveMainLiveHeaderView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveMainLiveHeaderView.h"

@interface PDLiveMainLiveHeaderView(){
    CGFloat tempOrigin_y;
}
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,assign)BOOL isAnimationShow;
@property (nonatomic,assign)BOOL isFinished;
@property (nonatomic,strong)NSTimer *timeTimer;
@property (nonatomic,strong)PDLiveRoomListModel *tempTransferLiveRootListModel;

@end

@implementation PDLiveMainLiveHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.isFinished = YES;
        [self createView];
    }
    return self;
}

#pragma makr - createView
-(void)createView{
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.titleLabel.text = @"距离最终候选主播出现还有00天00小时00分00秒";
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2., self.size_height - LCFloat(11) - [NSString contentofHeightWithFont:self.titleLabel.font], titleSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.titleLabel.textColor = c26;
    [self addSubview:self.titleLabel];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    CGFloat width = (self.size_height - LCFloat(11) * 2 - [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.avatarImgView.frame = CGRectMake((kScreenBounds.size.width - width) / 2., 0,width , width);
    [self addSubview:self.avatarImgView];
}

-(void)changeFrameWithOriginSet:(CGFloat)origin{
    if ((origin > (headerSize_Height - 44)) && (self.isAnimationShow == NO) && (origin > tempOrigin_y)){
        self.isAnimationShow = YES;
        if (self.isFinished){
            [UIView animateWithDuration:.5f animations:^{
                self.titleLabel.orgin_x = kScreenBounds.size.width - LCFloat(11) - self.titleLabel.size_width;
                self.avatarImgView.frame = CGRectMake(LCFloat(11), headerSize_Height - 44, 44, 44);
                self.isFinished = NO;
            } completion:^(BOOL finished) {
                self.titleLabel.orgin_x = kScreenBounds.size.width - LCFloat(11) - self.titleLabel.size_width;
                self.avatarImgView.frame = CGRectMake(LCFloat(11), headerSize_Height - 44, 44, 44);
                self.isFinished = YES;
            }];
        }
    } else {
        if ((origin < tempOrigin_y) && (origin <= (headerSize_Height - 44))){
            self.isAnimationShow = NO;
            if (self.isFinished){
                CGFloat width = (self.size_height - LCFloat(11) * 2 - [NSString contentofHeightWithFont:self.titleLabel.font]);
                [UIView animateWithDuration:.5f animations:^{
                    self.titleLabel.orgin_x = (kScreenBounds.size.width - self.titleLabel.size_width) / 2.;
                    self.avatarImgView.frame = CGRectMake((kScreenBounds.size.width - width) / 2., 0,width , width);
                    self.isFinished = NO;
                } completion:^(BOOL finished) {
                    self.titleLabel.orgin_x = (kScreenBounds.size.width - self.titleLabel.size_width) / 2.;
                    self.avatarImgView.frame = CGRectMake((kScreenBounds.size.width - width) / 2., 0,width , width);
                    self.isFinished = YES;
                }];
            }
        }
    }
    
    tempOrigin_y = origin;
}

-(void)reloadHeaderInfoWithModel:(PDLiveRoomListModel *)transferLiveRootListModel{
    _tempTransferLiveRootListModel = transferLiveRootListModel;
    
    // 1. 更新图片
    [self.avatarImgView uploadImageWithURL:transferLiveRootListModel.liveAvatar placeholder:nil callback:NULL];
    // 2. 更新文案
    self.titleLabel.text = [NSString stringWithFormat:@"距离最终还选主播出现还有%@",[NSDate getTimeDistance:transferLiveRootListModel.tempTargetNextStateInterval / 1000.]];
    [self startTimer];
}

-(void)startTimer{
    if (!self.timeTimer){
        self.timeTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
    }
}

-(void)daojishiManager{
    // 2. 更新文案
    self.titleLabel.text = [NSString stringWithFormat:@"距离最终还选主播出现还有%@",[NSDate getTimeDistance:self.tempTransferLiveRootListModel.tempTargetNextStateInterval / 1000.]];
}

@end
