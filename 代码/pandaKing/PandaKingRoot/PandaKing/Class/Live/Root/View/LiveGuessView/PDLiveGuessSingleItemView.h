//
//  PDLiveGuessSingleItemView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDGuessPointSingleModel.h"
@interface PDLiveGuessSingleItemView : UIView

@property (nonatomic,strong)PDGuessPointSingleModel *transferGuessPointSingleModel;
@property (nonatomic,assign)CGFloat transferCellHeight;

-(void)tagItemDidSelectManager:(void(^)())block;

+(CGFloat)calculationItemHeight ;

// 控制边框变色
-(void)changeLayerColorMangerWithShow:(BOOL)show;
@end
