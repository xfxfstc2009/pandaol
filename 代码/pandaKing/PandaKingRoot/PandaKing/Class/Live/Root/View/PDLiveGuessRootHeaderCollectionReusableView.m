//
//  PDLiveGuessRootHeaderCollectionReusableView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveGuessRootHeaderCollectionReusableView.h"

@interface PDLiveGuessRootHeaderCollectionReusableView()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)NSTimer *timer;

@end

@implementation PDLiveGuessRootHeaderCollectionReusableView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    // 2. 创建时间label
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    self.timeLabel.textColor = c26;
    self.timeLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.timeLabel];
}

@end
