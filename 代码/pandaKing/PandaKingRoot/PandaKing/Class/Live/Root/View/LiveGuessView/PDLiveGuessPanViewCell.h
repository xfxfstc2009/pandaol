//
//  PDLiveGuessPanViewCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDGuessDetailModel.h"
@interface PDLiveGuessPanViewCell : UITableViewCell

@property (nonatomic,strong)PDGuessDetailModel *transferGuessDetailModel;
@property (nonatomic,assign)CGFloat transferCellHeigt;

-(void)tapManagerWithBlock:(void(^)(PDGuessPointSingleModel *point))block;

+(CGFloat)calculationCellHeight;

@end
