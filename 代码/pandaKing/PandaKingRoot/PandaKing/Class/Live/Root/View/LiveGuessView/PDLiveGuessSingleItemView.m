//
//  PDLiveGuessSingleItemView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveGuessSingleItemView.h"


static char tapKey;
@interface PDLiveGuessSingleItemView()
@property (nonatomic,strong)UIView *bgView;                 /**< 背景view*/
@property (nonatomic,strong)UILabel *tagLabel;              /**< 投注标的*/
@property (nonatomic,strong)UILabel *countLabel;            /**< 总共*/
@property (nonatomic,strong)PDImageView *moneyImgView;
@property (nonatomic,strong)UILabel *touzhuCountLabel;
@property (nonatomic,strong)UIButton *tapButton;
@property (nonatomic,strong)UIView *topAlphaView;
@property (nonatomic,strong)PDImageView *topAlphaImageView; /**< 顶层的覆盖*/
@end

@implementation PDLiveGuessSingleItemView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建背景
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor blackColor];
    self.bgView.layer.borderWidth = 1.;
    [self addSubview:self.bgView];
    
    // 2. tagView
    self.tagLabel = [[UILabel alloc]init];
    self.tagLabel.backgroundColor = [UIColor clearColor];
    self.tagLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.tagLabel.textAlignment = NSTextAlignmentCenter;
    self.tagLabel.adjustsFontSizeToFitWidth = YES;
    self.tagLabel.textColor = [UIColor whiteColor];
    [self.bgView addSubview:self.tagLabel];
    
    // 3. 创建数量
    self.countLabel = [[UILabel alloc]init];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.countLabel.textColor = [UIColor whiteColor];
    [self.bgView addSubview:self.countLabel];
    
    // 4. 创建钱图标
    self.moneyImgView = [[PDImageView alloc]init];
    self.moneyImgView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.moneyImgView];
    
    // 5. 创建已投注数量
    self.touzhuCountLabel = [[UILabel alloc]init];
    self.touzhuCountLabel.backgroundColor = [UIColor clearColor];
    self.touzhuCountLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.touzhuCountLabel.textColor = c26;
    self.touzhuCountLabel.adjustsFontSizeToFitWidth = YES;
    self.touzhuCountLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.touzhuCountLabel];
    
    
    // 6. 创建顶层的锁view
    self.topAlphaView = [[UIView alloc]init];
    self.topAlphaView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.topAlphaView.alpha = .7f;
    [self.bgView addSubview:self.topAlphaView];
    
    self.topAlphaImageView = [[PDImageView alloc]init];
    self.topAlphaImageView.backgroundColor = [UIColor clearColor];
    self.topAlphaImageView.image = [UIImage imageNamed:@"icon_liveshow_clock"];
    [self.topAlphaView addSubview:self.topAlphaImageView];
    
    // 6. 创建手势
    self.tapButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.tapButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weaKSelf = self;
    [self.tapButton buttonWithBlock:^(UIButton *button) {
        if (!weaKSelf){
            return ;
        }
        __strong typeof(weaKSelf)strongSelf = weaKSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &tapKey);
        if (block){
            block();
        }
    }];
    [self.bgView addSubview:self.tapButton];
    
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferGuessPointSingleModel:(PDGuessPointSingleModel *)transferGuessPointSingleModel{
    _transferGuessPointSingleModel = transferGuessPointSingleModel;
    
    // 1. 创建标记
    self.tagLabel.text = [NSString stringWithFormat:@"%@ %.2f",transferGuessPointSingleModel.name,transferGuessPointSingleModel.odds];
    
    // 2. 创建一共投入
    self.countLabel.text = [NSString stringWithFormat:@"%li",(long)transferGuessPointSingleModel.totalStakeCount];
    
    // 3. 已经投入
    self.touzhuCountLabel.text = [NSString stringWithFormat:@"已投%li",(long)transferGuessPointSingleModel.mytotalStakeCount];
    
    // 4. 修改frame
    self.bgView.frame = CGRectMake(0, 0, self.size_width, LCFloat(50));
    
    self.tapButton.frame = self.bgView.bounds;
    
    // 4.2
    CGFloat margin = (self.bgView.size_height - [NSString contentofHeightWithFont:self.tagLabel.font] - [NSString contentofHeightWithFont:self.countLabel.font]) / 3.;
    self.tagLabel.frame = CGRectMake(0, margin, self.bgView.size_width, [NSString contentofHeightWithFont:self.tagLabel.font]);
    
    CGSize countSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.countLabel.font])];
    CGFloat margin_origin_x = (self.bgView.size_width - LCFloat(3) - countSize.width - LCFloat(5)) / 2.;
    self.countLabel.frame = CGRectMake(margin_origin_x, CGRectGetMaxY(self.tagLabel.frame) + margin, countSize.width, [NSString contentofHeightWithFont:self.countLabel.font]);
    
    self.moneyImgView.frame = CGRectMake(CGRectGetMaxX(self.countLabel.frame) + LCFloat(3), 0, LCFloat(5), LCFloat(5));
    self.moneyImgView.center_y = self.countLabel.center_y;
    
    // touzhu
    self.touzhuCountLabel.frame = CGRectMake(0, CGRectGetMaxY(self.bgView.frame) + LCFloat(11), self.bgView.size_width, [NSString contentofHeightWithFont:self.touzhuCountLabel.font]);
 
    self.topAlphaView.frame = self.bgView.bounds;
    
    self.topAlphaImageView.frame = CGRectMake((self.bgView.size_width - LCFloat(20)) / 2., (self.bgView.size_height - LCFloat(25)) / 2., LCFloat(20), LCFloat(25));
    
    if (transferGuessPointSingleModel.tempForbidStake ){                // 禁止投注
        self.topAlphaView.hidden = NO;
        self.tapButton.enabled = NO;
    } else {
        self.topAlphaView.hidden = YES;
        self.tapButton.enabled = YES;
    }
}

-(void)tagItemDidSelectManager:(void(^)())block{
    objc_setAssociatedObject(self, &tapKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationItemHeight {
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(50);
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}

-(void)changeLayerColorMangerWithShow:(BOOL)show{
    if (show){
        self.bgView.layer.borderColor = c26.CGColor;
    } else {
        self.bgView.layer.borderColor = [UIColor clearColor].CGColor;
    }
}

@end
