//
//  PDLiveGuessHeaderCollectionViewCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDLiveGuessHeaderCollectionViewCell : UICollectionViewCell

@property (nonatomic,copy)NSString *transferTitleStr;

+(CGSize)calculationCellSize;

@end
