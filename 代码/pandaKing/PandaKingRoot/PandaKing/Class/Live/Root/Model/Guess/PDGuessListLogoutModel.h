//
//  PDGuessListLogoutModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/5.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDGuessListLogoutModel : FetchModel

@property (nonatomic,assign)NSInteger goldCount;                        /**< 金额数量*/
@property (nonatomic,assign)NSInteger joinedCount;                      /**< 参加数量*/

@end
