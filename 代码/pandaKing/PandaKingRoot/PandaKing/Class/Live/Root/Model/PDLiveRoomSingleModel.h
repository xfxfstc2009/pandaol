//
//  PDLiveRoomSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDLiveRoomSingleModel <NSObject>

@end

@interface PDLiveRoomSingleModel : FetchModel

@property(nonatomic,copy)NSString *roomId;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *anchorName;
@property (nonatomic,copy)NSString *coverImg;
@property (nonatomic,copy)NSString *url;
@property (nonatomic,copy)NSString *streamPath;
@property (nonatomic,copy)NSString *watchCount;

@end
