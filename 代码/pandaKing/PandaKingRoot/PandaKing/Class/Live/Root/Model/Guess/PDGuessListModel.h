//
//  PDGuessListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDGuessDetailModel.h"

@interface PDGuessListModel : FetchModel

@property (nonatomic,strong)NSArray<PDGuessDetailModel > *items;


@end
