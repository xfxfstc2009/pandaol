//
//  PDGuessDetailModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDGuessPointSingleModel.h"

@protocol PDGuessDetailModel <NSObject>

@end

@interface PDGuessDetailModel : FetchModel

@property (nonatomic,copy)NSString *name;       /**< 竞猜详情*/
@property (nonatomic,copy)NSString*guessId;     /**< 竞猜id*/
@property (nonatomic,assign)BOOL forbidStake;   /**< 禁止投注*/

@property (nonatomic,strong)PDGuessPointSingleModel *pointA;
@property (nonatomic,strong)PDGuessPointSingleModel *pointB;

@end
