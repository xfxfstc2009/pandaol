//
//  PDLiveRoomListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLiveRoomSingleModel.h"

@interface PDLiveRoomListModel : FetchModel

@property (nonatomic,copy)NSString *activityId;
@property (nonatomic,assign)NSTimeInterval nextStateInterval;           /**< 下一场开始时间*/
@property (nonatomic,assign)BOOL hasActivity;                           /**< 是否有竞猜，如果返回no 就是当前污竞猜*/
@property (nonatomic,copy)NSString *state;                              /**< 状态*/
@property (nonatomic,assign)NSInteger goldCount;                        /**< 总奖金池*/
@property (nonatomic,assign)NSInteger joinedCount;
@property (nonatomic,copy)NSString *liveAvatar;

@property (nonatomic,strong)PDLiveRoomSingleModel *choosedRoom;         /**< 选中的主播*/
@property (nonatomic,strong)NSArray<PDLiveRoomSingleModel> * hotRooms;  /**< 热门主播*/
@property (nonatomic,strong)NSArray<PDLiveRoomSingleModel> *chooseRooms;/**< 候选主播*/

// 临时调用
@property (nonatomic,assign)NSTimeInterval tempTargetNextStateInterval; /**< 目标开始时间*/

@end
