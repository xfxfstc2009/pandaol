//
//  PDLiveGuessBettingViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDGuessPointSingleModel.h"

@interface PDLiveGuessBettingViewController : AbstractViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势
@property (nonatomic,strong)PDGuessPointSingleModel *transferPointSingleModel;  /**< 传递过去的观点model*/

@property (nonatomic, assign) NSUInteger memberMaximalGold; // 用户当前最大金币数额

- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果

-(void)touzhuBtnClickManager:(void(^)(NSString *gold))block;
@end
