//
//  PDLivePlayerViewBottomView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLivePlayerViewBottomView.h"

static char itemClickkey;
@interface PDLivePlayerViewBottomView()
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIView *itemsBackgroundView;

@end
#define item_Margin LCFloat(5)
@implementation PDLivePlayerViewBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建线条
    self.lineView = [[UIView alloc]init];
    self.lineView.frame = CGRectMake(0, 0, self.size_width, .5f);
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
    [self addSubview:self.lineView];
    
    // 2. 创建标题
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.text = @"投注金币";
    self.titleLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.titleLabel.font = [UIFont systemFontOfSize:12.];
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(LCFloat(11), item_Margin, titleSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    // 3. 创建
    self.itemsBackgroundView = [[UIView alloc]init];
    self.itemsBackgroundView.backgroundColor = [UIColor clearColor];
    self.itemsBackgroundView.frame = CGRectMake(0, CGRectGetMaxY(self.titleLabel.frame), self.size_width, LCFloat(44));
    [self addSubview:self.itemsBackgroundView];
    
    NSArray *itemArr = @[@"100",@"1000",@"10000",@"50000"];
    CGFloat width = (self.size_width - 5 * item_Margin) / 4.;
    for (int i = 0 ; i < itemArr.count; i++){
        CGFloat origin_x = item_Margin + (item_Margin + width) * i;
        CGFloat origin_y = item_Margin;
        UIButton *itemButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [itemButton setTitle:[itemArr objectAtIndex:i] forState:UIControlStateNormal];
        itemButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        itemButton.frame = CGRectMake(origin_x, origin_y, width, width * 0.55);
        itemButton.backgroundColor = c26;
        itemButton.layer.cornerRadius = LCFloat(2);
        [itemButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        __weak typeof(self)weakSelf = self;
        [itemButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(NSString *itemStr) = objc_getAssociatedObject(strongSelf, &itemClickkey);
            if (block){
                block([itemArr objectAtIndex:i]);
            }
        }];
        [self.itemsBackgroundView addSubview:itemButton];
    }
    
    self.itemsBackgroundView.size_height = item_Margin + width * .55f + item_Margin;
    
    self.size_height = item_Margin + [NSString contentofHeightWithFont:self.titleLabel.font] + self.itemsBackgroundView.size_height;
}

-(void)buttonActionClickManager:(void(^)(NSString *itemStr))block{
    objc_setAssociatedObject(self, &itemClickkey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}

@end
