//
//  PDLiveRootRightTopView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveRootRightTopView.h"

@interface PDLiveRootRightTopView()
@property (nonatomic,strong)UILabel *yuerLabel;
@property (nonatomic,strong)PDImageView *moneyImgView;                  /**< */
@property (nonatomic,strong)UIButton *chongzhiButton;                   /**< 充值按钮*/
@property (nonatomic,strong)UIView *lineView;                           /**< lineView*/

@end

@implementation PDLiveRootRightTopView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建充值
    self.chongzhiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.chongzhiButton.backgroundColor = [UIColor clearColor];
    [self.chongzhiButton setTitle:@"充值" forState:UIControlStateNormal];
    [self.chongzhiButton setTitleColor:c26 forState:UIControlStateNormal];
    self.chongzhiButton.titleLabel.font = [UIFont systemFontOfSize:15.];
    CGSize chongzhiSize = [self.chongzhiButton.titleLabel.text sizeWithCalcFont:self.chongzhiButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.size_height)];
    CGFloat width = chongzhiSize.width + 2 * LCFloat(8);
    self.chongzhiButton.frame = CGRectMake(self.size_width - 11 - width, 0, width, self.size_height);
    [self addSubview:self.chongzhiButton];
    
    // 2. 创建余额
    self.yuerLabel = [[UILabel alloc]init];
    self.yuerLabel.backgroundColor = [UIColor clearColor];
    self.yuerLabel.font = [UIFont systemFontOfSize:14.];
    self.yuerLabel.textColor = [UIColor whiteColor];
    self.yuerLabel.adjustsFontSizeToFitWidth = YES;
    self.yuerLabel.frame = CGRectMake(11, 0, 10, self.size_height);
    [self addSubview:self.yuerLabel];
    
    // 3. 创建图片
    self.moneyImgView = [[PDImageView alloc]init];
    self.moneyImgView.backgroundColor = [UIColor clearColor];
    self.moneyImgView.frame = CGRectMake(0, 0, 40, 40);
    self.moneyImgView.image = [UIImage imageNamed:@"icon_center_biggold"];
    [self addSubview:self.moneyImgView];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
    self.lineView.frame = CGRectMake(0, CGRectGetMaxY(self.chongzhiButton.frame), self.size_width, .5f);
    [self addSubview:self.lineView];
    
}

-(void)uploadCurrentTotalGold:(NSInteger)totalCount{
    self.yuerLabel.text = [NSString stringWithFormat:@"我的余额·%li",totalCount];
    
    CGSize yuerSize = [self.yuerLabel.text sizeWithCalcFont:self.yuerLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.yuerLabel.font])];
    
    CGFloat margin_w = self.chongzhiButton.orgin_x - 11 - 11 - self.moneyImgView.size_width;
    
    self.yuerLabel.frame = CGRectMake(11, 0,MIN(margin_w, yuerSize.width) , self.size_height);
    
    // 2. 创建图片
    self.moneyImgView.frame = CGRectMake(CGRectGetMaxX(self.yuerLabel.frame) + 5, 0, [NSString contentofHeightWithFont:self.yuerLabel.font], [NSString contentofHeightWithFont:self.yuerLabel.font]);
    self.moneyImgView.center_y = self.yuerLabel.center_y;
    
}
@end
