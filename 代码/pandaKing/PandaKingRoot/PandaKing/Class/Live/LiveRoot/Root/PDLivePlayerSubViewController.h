//
//  PDLivePlayerSubViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveRoomSingleModel.h"
@interface PDLivePlayerSubViewController : AbstractViewController

@property (nonatomic,strong)PDLiveRoomSingleModel * transferLiveRootSingleModel;

@property (nonatomic,copy)NSString *transferActivityId;
@end
