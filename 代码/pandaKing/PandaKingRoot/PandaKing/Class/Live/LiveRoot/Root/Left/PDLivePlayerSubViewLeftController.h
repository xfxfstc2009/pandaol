//
//  PDLivePlayerSubViewLeftController.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDLivePlayerSubViewLeftController : AbstractViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势

- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果



@end
