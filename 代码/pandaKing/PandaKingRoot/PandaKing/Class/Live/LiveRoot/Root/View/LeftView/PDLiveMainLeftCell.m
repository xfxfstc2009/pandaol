
//
//  PDLiveMainLeftCell.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLiveMainLeftCell.h"


@interface PDLiveMainLeftCell()
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)UILabel *countLabel;
@property (nonatomic,strong)PDImageView *countImgView;

@end

@implementation PDLiveMainLeftCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建背景
    self.convertView = [[UIView alloc]init];
    self.convertView.backgroundColor = [UIColor blackColor];
    self.convertView.clipsToBounds = YES;
    [self addSubview:self.convertView];
    
    // 2. 创建图片
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self.convertView addSubview:self.avatarImgView];
    
    // 3. 创建label
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont systemFontOfSize:14.];
    [self addSubview:self.titleLabel];
    
    // 4. 创建名字
    self.nickNameLabel = [[UILabel alloc]init];
    self.nickNameLabel.backgroundColor = [UIColor clearColor];
    self.nickNameLabel.font = [UIFont systemFontOfSize:13.];
    [self addSubview:self.nickNameLabel];
    
    // 5. 创建数量
    self.countLabel = [[UILabel alloc]init];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.font = [UIFont systemFontOfSize:12.];
    [self addSubview:self.countLabel];
    
    // 6. 创建数量图片
    self.countImgView = [[PDImageView alloc]init];
    self.countImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.countImgView];
}

-(void)setTransferCellSize:(CGSize)transferCellSize{
    _transferCellSize = transferCellSize;
}

-(void)setTransferLiveRoomSingleModel:(PDLiveRoomSingleModel *)transferLiveRoomSingleModel{
    _transferLiveRoomSingleModel = transferLiveRoomSingleModel;
    
    NSInteger padding = 10;
    
    // 1. 创建背景
    self.convertView.frame = CGRectMake(8, 8, 120, self.transferCellSize.height - 2 * 8);
    
    // 2. 创建图片
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView uploadImageWithURL:transferLiveRoomSingleModel.coverImg placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
    }];
    
    
    // 3. 创建标题
    self.titleLabel.text = transferLiveRoomSingleModel.title;
    CGFloat width = self.transferCellSize.width - 120 - 3 * padding;
    CGSize titleSize  = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (titleSize.height > [NSString contentofHeightWithFont:self.titleLabel.font]){
        self.titleLabel.numberOfLines = 2;
    } else {
        self.titleLabel.numberOfLines = 1;
    }
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + 8, self.convertView.orgin_y,width , titleSize.height);
    
    // 4. 创建播放数量
    self.countLabel.text = transferLiveRoomSingleModel.watchCount;
    CGSize countSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.countLabel.font])];
    self.countLabel.frame = CGRectMake(self.transferCellSize.width - LCFloat(11) - countSize.width, CGRectGetMaxY(self.convertView.frame) - [NSString contentofHeightWithFont:self.countLabel.font], countSize.width, [NSString contentofHeightWithFont:self.countLabel.font]);
    
    // 5. 创建播放图标
    self.countImgView.image = [UIImage imageNamed:@"icon_live_watchcount"];
    self.countImgView.frame = CGRectMake(self.countLabel.orgin_x - 9, 0, 9, 8);
    self.countImgView.center_y = self.countLabel.center_y;
    
    // 6. 创建游戏名字
    self.nickNameLabel.text = transferLiveRoomSingleModel.anchorName;
    self.nickNameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + 11, 0, self.countImgView.orgin_x - 11 - CGRectGetMaxX(self.convertView.frame) - 11, [NSString contentofHeightWithFont:self.nickNameLabel.font]);
    self.nickNameLabel.center_y = self.countLabel.center_y;
    
    
    
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeifght = 88;
    
    return cellHeifght;
}
@end
