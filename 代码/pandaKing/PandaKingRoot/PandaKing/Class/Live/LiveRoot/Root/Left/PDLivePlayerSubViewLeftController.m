//
//  PDLivePlayerSubViewLeftController.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLivePlayerSubViewLeftController.h"
#import "PDLiveMainLeftCell.h"
#import "PDLiveRoomListModel.h"


@interface PDLivePlayerSubViewLeftController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片


// @
@property (nonatomic,strong)UITableView *leftTableView;
@property (nonatomic,strong)NSMutableArray *leftMutableArr;

@end

@implementation PDLivePlayerSubViewLeftController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self createSheetView];                      // 加载Sheetview
    [self createDismissView];
    [self createLeftView];
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    self.view.backgroundColor = [UIColor clearColor];
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    // 创建背景色
    if (!self.backgrondView){
        self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
        self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.65f];
        [self.view addSubview:self.backgrondView];
    }
    
    // ShareViewShow
    if (!self.shareView){
        self.shareView = [[UIImageView alloc]initWithFrame:CGRectMake(-300,20, 300, kScreenBounds.size.height - 20)];
        self.shareView.clipsToBounds = YES;
        self.shareView.userInteractionEnabled = YES;
        self.shareView.contentMode = UIViewContentModeBottom;
        self.shareView.backgroundColor = [UIColor clearColor];
        self.shareView.alpha = .9f;
        [self.view addSubview:_shareView];
    }
    
    // 【创建tableView】
    [self createLeftView];
    
}


#pragma mark - 创建dismissView
-(void)createDismissView{
    if (self.isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
    }
}


#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = .9f;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak PDLivePlayerSubViewLeftController *weakVC = self;
    
    [UIView animateWithDuration:.9f animations:^{
        weakVC.shareView.frame = CGRectMake(- weakVC.shareView.size_width, 0, weakVC.shareView.size_width, weakVC.shareView.size_height);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak PDLivePlayerSubViewLeftController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    
    [self createSheetView];
    [UIView animateWithDuration:.9f animations:^{
        weakVC.shareView.frame = CGRectMake(0, weakVC.shareView.orgin_y,weakVC.shareView.size_width , weakVC.shareView.size_height);
    } completion:^(BOOL finished) {
        [self sendRequestToGetList];
    }];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.f];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}


#pragma mark - ArrayWithInit
-(void)arrayWithInit{
    self.leftMutableArr = [NSMutableArray array];
}


#pragma mark - 创建 tableView
-(void)createLeftView{
    self.leftTableView = [[UITableView alloc]initWithFrame:self.shareView.bounds style:UITableViewStylePlain];
    self.leftTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
    self.leftTableView.delegate = self;
    self.leftTableView.dataSource = self;
    self.leftTableView.backgroundColor = [UIColor clearColor];
    self.leftTableView.showsVerticalScrollIndicator = NO;
    self.leftTableView.scrollEnabled = YES;
    self.leftTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.shareView addSubview:self.leftTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.leftMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    PDLiveMainLeftCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDLiveMainLeftCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellSize = CGSizeMake(self.leftTableView.size_width, cellHeight);
    cellWithRowOne.transferLiveRoomSingleModel = [self.leftMutableArr objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.leftTableView){
        return [PDLiveMainLeftCell calculationCellHeight];
    }
    return 44;
}

#pragma mark - 接口
-(void)sendRequestToGetList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:latestanchoractivity requestParams:nil responseObjectClass:[PDLiveRoomListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded) {
            PDLiveRoomListModel *liveRootModel = (PDLiveRoomListModel *)responseObject;
            if (strongSelf.leftMutableArr.count){
                [strongSelf.leftMutableArr removeAllObjects];
            }
            if ([liveRootModel.state isEqualToString:@"candidate"]){ // 四个候选主播全都选定
                [strongSelf.leftMutableArr addObjectsFromArray:liveRootModel.chooseRooms];
                [strongSelf.leftMutableArr addObjectsFromArray:liveRootModel.hotRooms];
            } else if ([liveRootModel.state isEqualToString:@"broadcast"]){
                [strongSelf.leftMutableArr addObject:liveRootModel.choosedRoom];
                [strongSelf.leftMutableArr addObjectsFromArray:liveRootModel.hotRooms];
            } else if ([liveRootModel.state isEqualToString:@"init"]){
                [strongSelf.leftMutableArr addObjectsFromArray:liveRootModel.hotRooms];
            }
            [strongSelf.leftTableView reloadData];
        }
    }];
}
@end
