//
//  PDLivePlayerSubLivePlayerCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 直播列表
#import <UIKit/UIKit.h>
#import "PDLiveRoomSingleModel.h"

@interface PDLivePlayerSubLivePlayerCell : UITableViewCell

@property (nonatomic,strong)PDLiveRoomSingleModel *transferLiveRootSingleModel;
@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;

@end
