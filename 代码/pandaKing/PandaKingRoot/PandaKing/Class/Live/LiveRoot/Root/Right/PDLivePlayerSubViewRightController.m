//
//  PDLivePlayerSubViewRightController.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLivePlayerSubViewRightController.h"
#import "PDLiveRootRightTopView.h"
#import "PDLiveGuessPanViewCell.h"
#import "PDLivePlayerViewBottomView.h"
#import "PDGuessListModel.h"


@interface PDLivePlayerSubViewRightController()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片
@property (nonatomic,strong)PDLiveRootRightTopView *rightTopView;
@property (nonatomic,strong)UITableView *rightTableView;
@property (nonatomic,strong)NSMutableArray *rightMutableArr;
@property (nonatomic,strong)PDLivePlayerViewBottomView *livePlayerViewBottomView;
@end

@implementation PDLivePlayerSubViewRightController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createSheetView];                      // 加载Sheetview
    [self createDismissView];
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    self.view.backgroundColor = [UIColor clearColor];
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    // 创建背景色
    if (!self.backgrondView){
        self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
        self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.35f];
        [self.view addSubview:self.backgrondView];
    }
    
    // ShareViewShow
    if (!self.shareView){
        self.shareView = [[UIImageView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width , 0, 300, kScreenBounds.size.height)];
        self.shareView.clipsToBounds = YES;
        self.shareView.userInteractionEnabled = YES;
        self.shareView.contentMode = UIViewContentModeBottom;
        self.shareView.backgroundColor = [UIColor blackColor];
        self.shareView.alpha = .9f;
        [self.view addSubview:_shareView];
    }
    [self createBottomView];
    [self createRightView];
}


#pragma mark - 创建dismissView
-(void)createDismissView{
    if (self.isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
    }
}


#pragma mark - actionClick
-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = .9f;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak PDLivePlayerSubViewRightController *weakVC = self;
    
    [UIView animateWithDuration:.9f animations:^{
        weakVC.shareView.frame = CGRectMake(kScreenBounds.size.width, 0, weakVC.shareView.size_width, weakVC.shareView.size_height);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak PDLivePlayerSubViewRightController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    [self createSheetView];
    [UIView animateWithDuration:.9f animations:^{
        weakVC.shareView.frame = CGRectMake(kScreenBounds.size.width - weakVC.shareView.size_width, 0,weakVC.shareView.size_width , weakVC.shareView.size_height);
    }];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}






#pragma mark - arrayWithInit
-(void)arrayWithinit{
    self.rightMutableArr = [NSMutableArray array];
}



-(void)createRightView{
    // 1. 创建背景view
    if (!self.rightTopView){
        self.rightTopView = [[PDLiveRootRightTopView alloc]initWithFrame:CGRectMake(0, 0, self.shareView.size_width, 30)];
        [self.shareView addSubview:self.rightTopView];
        
        // 2. 创建tableView
        self.rightTableView = [[UITableView alloc]initWithFrame:self.shareView.bounds style:UITableViewStylePlain];
        self.rightTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.rightTableView.orgin_y = CGRectGetMaxY(self.rightTopView.frame) ;
        self.rightTableView.size_height = self.shareView.size_height - CGRectGetHeight(self.rightTopView.frame) - CGRectGetHeight(self.livePlayerViewBottomView.frame);
        self.rightTableView.delegate = self;
        self.rightTableView.dataSource = self;
        self.rightTableView.backgroundColor = [UIColor clearColor];
        self.rightTableView.showsVerticalScrollIndicator = NO;
        self.rightTableView.scrollEnabled = YES;
        self.rightTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.shareView addSubview:self.rightTableView];
        
        
        __weak typeof(self)weakSelf = self;
        [self sendRequestToGetCurrentMoneyWithBlock:^(NSInteger totalGold) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.rightTopView uploadCurrentTotalGold:totalGold];
        }];
        
        [weakSelf sendRequestToGetGuessList];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.rightMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
    PDLiveGuessPanViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
    if (!cellWithRowTwo){
        cellWithRowTwo = [[PDLiveGuessPanViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowTwo.backgroundColor = [UIColor clearColor];
    }
    cellWithRowTwo.transferCellHeigt = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowTwo.transferGuessDetailModel = [self.rightMutableArr objectAtIndex:indexPath.row];
    __weak typeof(self)weakSelf = self;
    [cellWithRowTwo tapManagerWithBlock:^(PDGuessPointSingleModel *point) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^guessDidSelectBlock)(PDGuessPointSingleModel *point) = objc_getAssociatedObject(strongSelf,@"123");
        if (guessDidSelectBlock){
            guessDidSelectBlock(point);
        }
    }];
    return cellWithRowTwo;

}


#pragma mark - 获取当前的金币
-(void)sendRequestToGetCurrentMoneyWithBlock:(void(^)(NSInteger totalGold))block{
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberWallet requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            NSNumber *totalgold = responseObject[@"totalgold"]; // 总资产
            if (block) {
                block(totalgold.integerValue);
            }
        }
    }];
}


#pragma mark - 竞猜接口
-(void)sendRequestToGetGuessList{
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *paramsDic = [NSMutableDictionary dictionary];
    if ([AccountModel sharedAccountModel].liveActivityId.length){
        [paramsDic setObject:[AccountModel sharedAccountModel].liveActivityId forKey:@"activityId"];
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:anchorguesslist requestParams:paramsDic responseObjectClass:[PDGuessListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDGuessListModel *guessListModel = (PDGuessListModel *)responseObject;
            if (strongSelf.rightMutableArr.count){
                [strongSelf.rightMutableArr removeAllObjects];
            }
            [strongSelf.rightMutableArr removeAllObjects];
            [strongSelf.rightMutableArr addObjectsFromArray:guessListModel.items];
            [strongSelf.rightTableView reloadData];
        }
    }];
}





-(void)createBottomView{
    self.livePlayerViewBottomView = [[PDLivePlayerViewBottomView alloc]initWithFrame:CGRectMake(0, self.shareView.size_height - LCFloat(50), self.shareView.size_width, LCFloat(50))];
    self.livePlayerViewBottomView.orgin_y = self.shareView.size_height - self.livePlayerViewBottomView.size_height;
    __weak typeof(self)weakSelf = self;
    [self.livePlayerViewBottomView buttonActionClickManager:^(NSString *itemStr) {
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSLog(@"%@",itemStr);
    }];
    [self.shareView addSubview:self.livePlayerViewBottomView];
}

@end
