//
//  PDPlayerViewCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ZFPlayerGoBackBlock)(void);

typedef NS_ENUM(NSInteger, ZFPlayerLayerGravity) {
     ZFPlayerLayerGravityResize,           // 非均匀模式。两个维度完全填充至整个视图区域
     ZFPlayerLayerGravityResizeAspect,     // 等比例填充，直到一个维度到达区域边界
     ZFPlayerLayerGravityResizeAspectFill  // 等比例填充，直到填充满整个视图区域，其中一个维度的部分区域会被裁剪
};

typedef NS_ENUM(NSInteger,PlayerType) {
    PlayerTypeVideo,                    /**< 播放视频*/
    PlayerTypeLive,                     /**< 播放直播*/
};


@protocol ZFPlayerViewDelegate <NSObject>
-(void)liveSwipeManagerWithIsLeft:(BOOL)isLeft;                         // 1. 拖动方法

@end


@interface ZFPlayerView : UIView

@property (nonatomic, strong) NSURL *videoURL;                          /** 视频URL */
@property (nonatomic, copy  ) ZFPlayerGoBackBlock  goBackBlock;         /** 返回按钮Block */
@property (nonatomic, assign) ZFPlayerLayerGravity playerLayerGravity;  /** 设置playerLayer的填充模式 */

@property (nonatomic,assign)PlayerType playerType;

@property (nonatomic,weak)id<ZFPlayerViewDelegate> delegate;

- (void)cancelAutoFadeOutControlBar;

+ (instancetype)sharedPlayerView;

- (void)addPlayerToCellImageView:(UIImageView *)imageView;

-(void)resetPlayerAndWithWindow;    /**< 保存再window上*/
- (void)resetPlayer;                /**< 重置player */
- (void)play;                       /**< 播放*/
- (void)pause;                      /** < 暂停 */

- (void)setVideoURL:(NSURL *)videoURL withTableView:(UITableView *)tableView AtIndexPath:(NSIndexPath *)indexPath withImageViewTag:(NSInteger)tag;
-(void)removeTableView;
@end
