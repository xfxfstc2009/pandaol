//
//  PDLivePlayerViewBottomView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDLivePlayerViewBottomView : UIView

-(void)buttonActionClickManager:(void(^)(NSString *itemStr))block;

@end
