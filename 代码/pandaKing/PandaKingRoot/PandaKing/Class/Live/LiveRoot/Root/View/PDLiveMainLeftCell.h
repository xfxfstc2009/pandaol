//
//  PDLiveMainLeftCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLiveRoomSingleModel.h"

@interface PDLiveMainLeftCell : UITableViewCell

@property (nonatomic,assign)CGSize transferCellSize;
@property (nonatomic,strong)PDLiveRoomSingleModel *transferLiveRoomSingleModel;

+(CGFloat)calculationCellHeight;

@end
