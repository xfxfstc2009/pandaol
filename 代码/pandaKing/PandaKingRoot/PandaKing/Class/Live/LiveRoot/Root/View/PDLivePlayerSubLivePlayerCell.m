//
//  PDLivePlayerSubLivePlayerCell.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLivePlayerSubLivePlayerCell.h"

@interface PDLivePlayerSubLivePlayerCell()
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)PDImageView *imgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)PDImageView *countImgView;
@property (nonatomic,strong)UILabel *countLabel;

@end

@implementation PDLivePlayerSubLivePlayerCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.convertView = [[PDImageView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.clipsToBounds = YES;
    [self addSubview:self.convertView];
    
    // 2. 创建图片
    self.imgView = [[PDImageView alloc]init];
    self.imgView.backgroundColor = [UIColor clearColor];
    [self.convertView addSubview:self.imgView];
    
    // 3. 创建标题
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.titleLabel];
    
    // 4. 创建详情
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self addSubview:self.nameLabel];
    
    // 5. 创建数量
    self.countLabel = [[UILabel alloc]init];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    [self addSubview:self.countLabel];
    
    // 6. 创建数量
    self.countImgView = [[PDImageView alloc]init];
    self.countImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.countImgView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferLiveRootSingleModel:(PDLiveRoomSingleModel *)transferLiveRootSingleModel{
    _transferLiveRootSingleModel = transferLiveRootSingleModel;
    
    // 1. 创建图片
    self.convertView.frame = CGRectMake(LCFloat(11), LCFloat(11), LCFloat(125), self.transferCellHeight - 2 * LCFloat(11));
    __weak typeof(self)weakSelf = self;
    [self.imgView uploadImageWithURL:transferLiveRootSingleModel.coverImg placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf setTransferSingleAsset:image];
    }];
    
    CGFloat width = kScreenBounds.size.width - CGRectGetMaxX(self.convertView.frame) - 2 * LCFloat(11);
    // 2. 创建数量
    self.titleLabel.text = transferLiveRootSingleModel.title;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.convertView.orgin_y, width, titleSize.height);
    if (titleSize.height >= 2 * [NSString contentofHeightWithFont:self.titleLabel.font]){
        self.titleLabel.numberOfLines = 2;
    } else {
        self.titleLabel.numberOfLines = 1;
    }
    
    // 3. 创建数量
    self.countLabel.text = transferLiveRootSingleModel.watchCount;
    CGSize countSize = [self.countLabel.text sizeWithCalcFont:self.countLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.countLabel.font])];
    self.countLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - countSize.width, CGRectGetMaxY(self.convertView.frame) - [NSString contentofHeightWithFont:self.countLabel.font], countSize.width, [NSString contentofHeightWithFont:self.countLabel.font]);
    
    // 4. 创建图片
    self.countImgView.image = [UIImage imageNamed:@"icon_live_watchcount"];
    self.countImgView.frame = CGRectMake(self.countLabel.orgin_x - 9 - LCFloat(3), 0, 9, 8);
    self.countImgView.center_y = self.countLabel.center_y;
    
    
    // 3. 创建名字
    self.nameLabel.text = transferLiveRootSingleModel.anchorName;
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(11), self.countImgView.orgin_y, self.countImgView.orgin_x - CGRectGetMaxX(self.convertView.frame) - LCFloat(11), [NSString contentofHeightWithFont:self.nameLabel.font]);
    self.nameLabel.adjustsFontSizeToFitWidth = YES;
    self.nameLabel.center_y = self.countLabel.center_y;
    
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(70);
    cellHeight += LCFloat(11);
    return cellHeight;
}


-(void)setTransferSingleAsset:(UIImage *)transferSingleAsset{
    
    CGImageRef posterImage = transferSingleAsset.CGImage;
    // 1. 获取当前图片高度
    size_t posterImageHeight = CGImageGetHeight(posterImage);
    CGFloat scale = posterImageHeight / self.convertView.size_height;
    self.imgView.image = [UIImage imageWithCGImage:posterImage scale:scale orientation:UIImageOrientationUp];
    
    // 基本尺寸参数
    CGSize boundsSize = self.convertView.bounds.size;
    CGFloat boundsWidth = boundsSize.width;
    CGFloat boundsHeight = boundsSize.height;
    
    CGSize imageSize = self.imgView.image.size;
    CGFloat imageWidth = imageSize.width;
    CGFloat imageHeight = imageSize.height;
    
    CGRect imageFrame = CGRectMake(0, (boundsHeight - boundsWidth) / 2., boundsWidth, imageHeight * boundsWidth / imageWidth);
    
    if (imageHeight > imageWidth){          // 图片高度大于图片宽度
        if (imageFrame.size.height < boundsHeight) {
            imageFrame.origin.y = floorf((boundsHeight - imageFrame.size.height) / 2.0);
        } else {
            imageFrame.origin.y = 0;
        }
    } else {                                // 图片宽度大于高度
        if (imageFrame.size.width < boundsWidth){
            imageFrame.origin.x = 0;
            imageFrame.size.width = (boundsHeight * 1.00 / imageFrame.size.height) *imageFrame.size.width;
            imageFrame.size.height = boundsHeight;
        } else {
            imageFrame.size.width = boundsWidth;
            imageFrame.size.height = (boundsWidth / imageFrame.size.width) * imageFrame.size.height;
            imageFrame.origin.x = - floorf((imageFrame.size.width - boundsWidth) / 2.0);
            imageFrame.origin.y = - floorf((imageFrame.size.height - boundsHeight) / 2.);
            
            if (imageFrame.size.height < boundsHeight){
                imageFrame.size.height = boundsHeight;
                imageFrame.size.width = (boundsHeight / imageFrame.size.height) * boundsWidth;
                imageFrame.origin.x = - floorf((imageFrame.size.width - boundsWidth) / 2.0);
                imageFrame.origin.y = - floorf((imageFrame.size.height - boundsHeight) / 2.);
            }
            
        }
    }
    self.imgView.frame = imageFrame;
}



@end
