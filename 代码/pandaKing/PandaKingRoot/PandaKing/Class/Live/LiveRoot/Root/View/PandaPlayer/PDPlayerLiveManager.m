//
//  PDPlayerLiveManager.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPlayerLiveManager.h"

static char complicationKey;
@interface PDPlayerLiveManager()<VMediaPlayerDelegate>

@end

@implementation PDPlayerLiveManager

#pragma mark - 1 初始化
- (void)setupPlayView:(UIView *)playView playURL:(NSURL *)playURL complicationHandle:(void(^)(BOOL isSetuped,NSError *error))complication{
    objc_setAssociatedObject(self, &complicationKey, complication, OBJC_ASSOCIATION_COPY_NONATOMIC);
    if ((!playURL) || (!playView)){
        return;
    }
    
    // 1. 设置播放layer
    [[VMediaPlayer sharedInstance] setupPlayerWithCarrierView:playView withDelegate:self];
    // 2. 设置url
    [[VMediaPlayer sharedInstance] setDataSource:playURL];
    [[VMediaPlayer sharedInstance] prepareAsync];
    // 3. 修改声音
    [self setVolume];
}

-(void)setVolume{
    [[VMediaPlayer sharedInstance] setVolume:.4f];
}


#pragma mark - actionManager
-(void)play{
    [[VMediaPlayer sharedInstance] start];
}

-(void)pause{
    [[VMediaPlayer sharedInstance] pause];
}

-(void)reset{
    [[VMediaPlayer sharedInstance] reset];
}

-(void)stopWithBlock:(void(^)(BOOL isSuccessed))block{
    [[VMediaPlayer sharedInstance] reset];
    [[VMediaPlayer sharedInstance] unSetupPlayer];
    if (block){
        block(YES);
    }
}

-(void)mediaPlayer:(VMediaPlayer *)player error:(id)arg{
    void(^block)(BOOL isSetuped,NSError *error) = objc_getAssociatedObject(self, &complicationKey);
    if (block){
        if (!arg){
            block(YES,nil);
        } else {
            block(NO,(NSError *)arg);
        }
    }
}

- (void)mediaPlayer:(VMediaPlayer *)player didPrepared:(id)arg {
    void(^block)(BOOL isSetuped,NSError *error) = objc_getAssociatedObject(self, &complicationKey);
    if (block){
        if (!arg){
            block(YES,nil);
        } else {
            block(NO,(NSError *)arg);
        }
    }
}

- (void)mediaPlayer:(VMediaPlayer *)player playbackComplete:(id)arg {
    NSLog(@"播放完成");
}

#pragma mark - GET SET
-(void)setQuality:(emVMVideoQuality)quality{
    [[VMediaPlayer sharedInstance] setVideoQuality:quality];
}

-(BOOL)isPlaying{
    return [[VMediaPlayer sharedInstance] isPlaying];
}

@end
