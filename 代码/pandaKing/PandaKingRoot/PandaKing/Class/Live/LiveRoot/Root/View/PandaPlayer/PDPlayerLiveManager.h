//
//  PDPlayerLiveManager.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Vitamio.h"

@interface PDPlayerLiveManager : NSObject

@property (nonatomic,assign)emVMVideoQuality quality;               /**< 视频质量*/
@property (nonatomic,assign)BOOL isPlaying;
- (void)setupPlayView:(UIView *)playView playURL:(NSURL *)playURL complicationHandle:(void(^)(BOOL isSetuped,NSError *error))complication;                           /**< 加入视频播放View*/

-(void)play;                /**< 播放*/
-(void)pause;               /**< 暂停*/
-(void)reset;               /**< 重置*/
-(void)stopWithBlock:(void(^)(BOOL isSuccessed))block;  /**< 退出*/




@end
