//
//  PDLiveRootRightTopView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 视频直播 - 右侧拉条 顶部view
#import <UIKit/UIKit.h>

@interface PDLiveRootRightTopView : UIView

-(void)uploadCurrentTotalGold:(NSInteger)totalCount;

@end
