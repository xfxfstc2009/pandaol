//
//  PDChallengeTestViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/7/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChallengeTestViewController.h"
#import "PDResposeModel1.h"
#import "PDOrganizeTeamModel.h"                     // 发送组队邀请

@interface PDChallengeTestViewController()<UITableViewDataSource,UITableViewDelegate,WebSocketConnectionDelegate,PDNetworkAdapterDelegate,PDLoginSocketDelegate>

@property (nonatomic,strong)UITableView *challengeTableView;
@property (nonatomic,strong)NSArray *testArr;                   /**< 测试数组*/

@end

@implementation PDChallengeTestViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self createTableView];
    
}

-(void)dealloc{
    NSLog(@"释放");
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.testArr = @[@[@"获取用户召唤师",@"多人出征列表"],@[@"1.开启永久长连接",@"发送组队邀请"],@[@"主动离队"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.challengeTableView){
        self.challengeTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.challengeTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.challengeTableView.delegate = self;
        self.challengeTableView.dataSource = self;
        self.challengeTableView.backgroundColor = [UIColor clearColor];
        self.challengeTableView.showsVerticalScrollIndicator = NO;
        self.challengeTableView.scrollEnabled = YES;
        self.challengeTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.challengeTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.testArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.testArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    cellWithRowOne.textLabel.text = [[self.testArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.row == 0 && indexPath.section == 0){
        [self getUserRole];
    } else if (indexPath.row == 1 && indexPath.section == 0){
        [self matchGroup];
    } else if (indexPath.section == 1 && indexPath.row == 0){       // 开启永久长连接
        [self openLoginLink];
    } else if (indexPath.section == 1 && indexPath.row == 1){       // 发送组队请求
        [[UIActionSheet actionSheetWithTitle:@"发送组队请求" buttonTitles:@[@"取消",@"发送给9617",@"发送给9618",@"发送给9619"] callback:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
            if (buttonIndex == 0){
                [self addTeamWithFid:@"4"];
            } else if (buttonIndex == 1){
                [self addTeamWithFid:@"100150"];
            } else if (buttonIndex == 2){
                [self addTeamWithFid:@"100153"];
            } else {
                NSLog(@"取消");
            }
        }] showInView:self.view];
    } else if (indexPath.section == 2 && indexPath.row == 0){
        
    }
}

#pragma mark - 发送组队邀请
-(void)addTeamWithFid:(NSString *)fid{
    PDOrganizeTeamModel *originModel = [[PDOrganizeTeamModel alloc]init];
    originModel.type = 200;
    originModel.uid = [AccountModel sharedAccountModel].uid;
    originModel.uname = [AccountModel sharedAccountModel].nickname;
    originModel.uavatar = [AccountModel sharedAccountModel].avatar;
    originModel.fid = fid;
    originModel.fname = @"5";
    originModel.card = 6;
    originModel.zone = @"7";
    NSDictionary *dic = [originModel dicDecrypt];
    [[NetworkAdapter sharedAdapter] webSocketFetchModelWithPHPRequestParams:dic socket:[NetworkAdapter sharedAdapter].loginWebSocketConnection];
}

#pragma mark - 发送离队信息
-(void)closeWithTeam{

}


-(void)openLoginLink{
//    [[NetworkAdapter sharedAdapter]webSocketConnectionWithLogin];
//    [NetworkAdapter sharedAdapter].delegate = self;
//    [NetworkAdapter sharedAdapter].loginSocketDelegate = self;
}

#pragma mark - 永久长连接连接成功
-(void)loginConnectedWithStatus:(BOOL)status{
    NSString *titleStr = @"";
    if (status == YES){     // 连接成功
        titleStr = @"成功";
    } else {                // 连接失败
        titleStr = @"不成功";
    }
    [[UIAlertView alertViewWithTitle:titleStr message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
}



-(void)smart{
    NSLog(@"123");
}

-(void)webSocketDidOpenWithSocket:(SRWebSocket *)webSocket{
//    [[NetworkAdapter sharedAdapter]  webSocketConnectionWithLoginStartWithToken:[AccountModel sharedAccountModel].token];
}

-(void)sendInfo{
//    [[NetworkAdapter sharedAdapter]  webSocketConnectionWithLoginStartWithToken:[AccountModel sharedAccountModel].token];
//    [[NetworkAdapter sharedAdapter]webSocketConnectionWithLoginStart];
}

-(void)webSocket:(SRWebSocket *)webSocket didReceiveData:(id)message{
    NSString *str = @"";
    if ([message isKindOfClass:[NSString class]]){
        str = (NSString *)message;
    }
    
    NSDictionary *dataSource = [NSJSONSerialization JSONObjectWithData:[str dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingMutableLeaves error:nil];
    
    PDResposeModel1 *resposeModel = [[PDResposeModel1 alloc]initWithJSONDict:dataSource];
    

    
    NSLog(@"%@,%@",message,resposeModel);
}

-(void)socketDidBackData:(id)responseObject{

}

-(void)webSocketDidBackData:(id)responseObject{

}


#pragma mark - 获取用户召唤师
-(void)getUserRole{
//    __weak typeof(self)weakSelf = self;
//    [[NetworkAdapter sharedAdapter]fetchWithPath:getRole requestParams:nil responseObjectClass:[PDRoleListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf){
//            return ;
//        }
////        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSucceeded){
//            PDRoleListModel *roleListModel = (PDRoleListModel *)responseObject;
//            NSLog(@"%@",roleListModel);
//        } else {
//            
//        }
//    }];
}

#pragma mark 多人出征列表
-(void)matchGroup{
//    __weak typeof(self)weakSelf = self;
//    [[NetworkAdapter sharedAdapter] fetchWithPath:matchGroup requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf){
//            return ;
//        }
////        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSucceeded){
//            
//        } else {
//            
//        }
//    }];
}

#pragma mark - 点击出征
-(void)startMatch{
//    __weak typeof(self)weakSelf = self;
//    [[NetworkAdapter sharedAdapter] fetchWithPath:startMatch requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!weakSelf){
//            return ;
//        }
////        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if (isSucceeded){
//        
//        } else {
//        
//        }
//    }];
}
@end
