//
//  PDChallengeHouseModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDChallengeHouseModel : FetchModel

@property (nonatomic,copy)NSString *houseName;              /**< 房间名称*/
@property (nonatomic,copy)NSString *housePwd;               /**< 房间密码*/

@end
