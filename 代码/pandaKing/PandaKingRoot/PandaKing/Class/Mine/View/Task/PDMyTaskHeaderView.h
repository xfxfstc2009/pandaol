//
//  PDMyTaskHeaderView.h
//  PandaKing
//
//  Created by Cranz on 16/12/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDMyTaskHeaderView : UITableViewHeaderFooterView

- (void)updateTaskWithTitle:(NSString *)title finishedCount:(NSUInteger)finishedCount totalCount:(NSUInteger)tatoalCount;
@end
