//
//  PDMyTaskHeaderView.m
//  PandaKing
//
//  Created by Cranz on 16/12/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMyTaskHeaderView.h"

@interface PDMyTaskHeaderView ()
@property (nonatomic, strong) UILabel *taskCountLabel;
@property (nonatomic, strong) UIView *signView;
@end

@implementation PDMyTaskHeaderView

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    
    [self viewSetting];
    return self;
}

- (void)viewSetting {
    self.signView = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 4, kTableViewSectionHeader_height - 13)];
    self.signView.backgroundColor = c7;
    [self addSubview:self.signView];
    
    self.taskCountLabel = [[UILabel alloc] init];
    self.taskCountLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.taskCountLabel.textColor = [UIColor grayColor];
    [self addSubview:self.taskCountLabel];
}

- (void)updateTaskWithTitle:(NSString *)title finishedCount:(NSUInteger)finishedCount totalCount:(NSUInteger)tatoalCount {
    NSString *text = [NSString stringWithFormat:@"%@(%lu/%lu)", title, (unsigned long)finishedCount, (unsigned long)tatoalCount];
    self.taskCountLabel.text = text;
    
    CGSize textSize = [text sizeWithCalcFont:self.taskCountLabel.font];
    self.taskCountLabel.frame = CGRectMake(kTableViewSectionHeader_leftMaxtter, (CGRectGetHeight(self.signView.frame) - textSize.height) / 2 + CGRectGetMinY(self.signView.frame), textSize.width, textSize.height);
}

@end
