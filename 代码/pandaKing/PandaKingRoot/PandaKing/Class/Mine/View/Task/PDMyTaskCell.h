//
//  PDMyTaskCell.h
//  PandaKing
//
//  Created by Cranz on 16/12/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDTaskObject.h"

typedef NS_ENUM(NSUInteger, PDTaskButtonType) {
    PDTaskButtonTypeProgress, // 进行中
    PDTaskButtonTypePrizeabled, // 可以领奖
    PDTaskButtonTypeFinished, // 完成
};

@protocol PDMyTaskCellDelegate;
@interface PDMyTaskCell : UITableViewCell
@property (nonatomic, strong) PDTaskObject *model;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) id<PDMyTaskCellDelegate> delegate;


+ (CGFloat)cellHeightWithModel:(PDTaskObject *)model;
- (void)changeButtonType:(PDTaskButtonType)type;

@end

@protocol PDMyTaskCellDelegate <NSObject>

- (void)taskCell:(PDMyTaskCell *)cell didClickMoreButton:(UIButton *)button;

@end