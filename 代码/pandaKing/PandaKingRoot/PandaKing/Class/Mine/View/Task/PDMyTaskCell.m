//
//  PDMyTaskCell.m
//  PandaKing
//
//  Created by Cranz on 16/12/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMyTaskCell.h"

@interface PDMyTaskCell ()
@property (nonatomic, strong) UILabel *taskCountLabel; // 任务描述
@property (nonatomic, strong) UILabel *goldLabel; // 奖励:可能是金币也可能是竹子
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, strong) UIImageView *achievementImageView; // 成就图标
@property (nonatomic, strong) UILabel *achievementLabel; // 成就名字
@property (nonatomic, assign) CGFloat rowHieght;
@property (nonatomic, assign) CGSize buttonSize;
@end

@implementation PDMyTaskCell

+ (CGFloat)cellHeightWithModel:(PDTaskObject *)model {
    CGFloat taskContentHeight = [[NSString stringWithFormat:@"%@(%ld/%ld)",model.name,model.doneTimes,model.times] sizeWithCalcFont:[UIFont systemFontOfCustomeSize:16] constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGSizeMake(LCFloat(88), LCFloat(34)).width - 2 * kTableViewSectionHeader_left - LCFloat(20)/*按钮和任务内容中间的空隙*/, CGFLOAT_MAX)].height;
    if (!model.archieveName.length) {
        return LCFloat(65) - [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:16]] + taskContentHeight;
    } else {
        return LCFloat(85) - [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:16]] + taskContentHeight; // 有成就
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    [self cellSetting];
    return self;
}

- (void)cellSetting {
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.buttonSize = CGSizeMake(LCFloat(88), LCFloat(34));
    
    // 任务描述
    self.taskCountLabel = [[UILabel alloc] init];
    self.taskCountLabel.font = [UIFont systemFontOfCustomeSize:16];
    self.taskCountLabel.textColor = c3;
    self.taskCountLabel.numberOfLines = 0;
    [self.contentView addSubview:self.taskCountLabel];
    
    // 奖励
    self.goldLabel = [[UILabel alloc] init];
    self.goldLabel.textColor = c26;
    self.goldLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.goldLabel];
    
    // 成就
    self.achievementImageView = [[UIImageView alloc] init];
    self.achievementImageView.image = [UIImage imageNamed:@"bg_task_archieve.png"];
    [self.contentView addSubview:self.achievementImageView];
    
    self.achievementLabel = [[UILabel alloc] init];
    self.achievementLabel.font = [[UIFont systemFontOfCustomeSize:14] boldFont];
    self.achievementLabel.textColor = c1;
    [self.achievementImageView addSubview:self.achievementLabel];

    self.moreButton = [[UIButton alloc] init];
    self.moreButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    [self.moreButton addTarget:self action:@selector(didClickMoreButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.moreButton];
}

- (void)didClickMoreButton:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(taskCell:didClickMoreButton:)]) {
        [self.delegate taskCell:self didClickMoreButton:button];
    }
}

- (void)setModel:(PDTaskObject *)model {
    _model = model;
    
    _rowHieght = [PDMyTaskCell cellHeightWithModel:model];
    
    self.taskCountLabel.text = [NSString stringWithFormat:@"%@(%ld/%ld)",model.name,model.doneTimes,model.times];
    if (model.award.gold && !model.award.bamboo) {
        self.goldLabel.text = [NSString stringWithFormat:@"+%ld金币",model.award.gold];
    } else if (!model.award.gold && model.award.bamboo) {
        self.goldLabel.text = [NSString stringWithFormat:@"+%ld竹子",model.award.bamboo];
    } else if (model.award.gold && model.award.bamboo) {
        self.goldLabel.text = [NSString stringWithFormat:@"+%ld金币,+%ld竹子",model.award.gold,model.award.bamboo];
    }
    
    self.achievementLabel.text = [NSString stringWithFormat:@"【%@】",model.archieveName];
    if (!model.done) { // 还没完成
        
        [self changeButtonType:PDTaskButtonTypeProgress];
    } else if (model.done && !model.receiveAward) { // 完成但还没领奖
        
        [self changeButtonType:PDTaskButtonTypePrizeabled];
    } else { // 已领过奖
        
        [self changeButtonType:PDTaskButtonTypeFinished];
    }
    
    CGSize titleSize = [self.taskCountLabel.text sizeWithCalcFont:self.taskCountLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - self.buttonSize.width - 2 * kTableViewSectionHeader_left - LCFloat(20)/*按钮和任务内容中间的空隙*/, CGFLOAT_MAX)];
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    CGSize achSize = [self.achievementLabel.text sizeWithCalcFont:self.achievementLabel.font];
    CGFloat space = 2; // 任务内容和金币之间的空隙, 成就和任务之间的间隙
    CGFloat y;
    CGFloat achSpace = LCFloat(10); // 成就文字和背景x上的空隙
    if (!model.archieveName.length) { // 没有成就
        self.achievementImageView.hidden = YES;
        y = (_rowHieght - titleSize.height - space - goldSize.height) / 2;
    } else { // 有成就就要显示成就标志
        self.achievementImageView.hidden = NO;
        y = (_rowHieght - titleSize.height - space * 2 - goldSize.height - achSize.height - 3 * 2) / 2;
    }
    
    self.achievementLabel.frame = CGRectMake(achSpace, 3, achSize.width, achSize.height);
    self.achievementImageView.frame = CGRectMake(kTableViewSectionHeader_leftMaxtter, y, achSize.width + achSpace * 2, achSize.height + 3 * 2);
    
    self.taskCountLabel.frame = CGRectMake(kTableViewSectionHeader_leftMaxtter, !model.archieveName.length? y : CGRectGetMaxY(self.achievementImageView.frame) + space, titleSize.width, titleSize.height);
    self.goldLabel.frame = CGRectMake(CGRectGetMinX(self.taskCountLabel.frame), CGRectGetMaxY(self.taskCountLabel.frame) + space, goldSize.width, goldSize.height);
    self.moreButton.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - self.buttonSize.width, CGRectGetMinY(self.taskCountLabel.frame), self.buttonSize.width, self.buttonSize.height);
}

- (void)changeButtonType:(PDTaskButtonType)type {
    if (type == PDTaskButtonTypeProgress) {
        [self.moreButton setTitle:@"去完成" forState:UIControlStateNormal];
        [self.moreButton setTitleColor:c26 forState:UIControlStateNormal];
        [self.moreButton setBackgroundImage:[UIImage imageWithRenderColor:[UIColor whiteColor] renderSize:self.buttonSize] forState:UIControlStateNormal];
        self.moreButton.layer.borderColor = c26.CGColor;
        self.moreButton.layer.borderWidth = 1;
        self.moreButton.layer.cornerRadius = 6;
        self.moreButton.clipsToBounds = YES;
        self.moreButton.enabled = YES;
    } else if (type == PDTaskButtonTypePrizeabled) {
        [self.moreButton setTitle:@"领取奖励" forState:UIControlStateNormal];
        [self.moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.moreButton setBackgroundImage:[UIImage imageWithRenderColor:c26 renderSize:self.buttonSize] forState:UIControlStateNormal];
        self.moreButton.layer.borderColor = c26.CGColor;
        self.moreButton.layer.borderWidth = 0;
        self.moreButton.layer.cornerRadius = 6;
        self.moreButton.clipsToBounds = YES;
        self.moreButton.enabled = YES;
    } else if (type == PDTaskButtonTypeFinished) {
        [self.moreButton setTitle:@"已完成" forState:UIControlStateNormal];
        [self.moreButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        [self.moreButton setBackgroundImage:[UIImage imageWithRenderColor:c27 renderSize:self.buttonSize] forState:UIControlStateNormal];
        self.moreButton.layer.borderColor = c26.CGColor;
        self.moreButton.layer.borderWidth = 0;
        self.moreButton.layer.cornerRadius = 6;
        self.moreButton.clipsToBounds = YES;
        self.moreButton.enabled = NO;
    }
}

@end
