//
//  PDBackpackTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBackpackItemModel.h"
#import "PDBackpackUsedItemModel.h"

typedef NS_ENUM(NSUInteger, PDBackpackTableViewCellType) {
    PDBackpackTableViewCellTypeHad,
    PDBackpackTableViewCellTypeUsed,
};
@protocol PDBackpackTableViewCellDelegate;
@interface PDBackpackTableViewCell : UITableViewCell
@property (nonatomic, weak) id<PDBackpackTableViewCellDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) PDBackpackItemModel *itemModel;
@property (nonatomic, strong) PDBackpackUsedItemModel *usedItemModel;

- (instancetype)initWithType:(PDBackpackTableViewCellType)type reuseIdentifier:(NSString *)reuseIdentifier;
+ (CGFloat)cellHeight;

@end

@protocol PDBackpackTableViewCellDelegate <NSObject>

- (void)tableViewCell:(PDBackpackTableViewCell *)cell didClickButtonAtIndex:(NSInteger)index tableViewAtIndexPath:(NSIndexPath *)indexPath;

@end
