//
//  PDExchaneDetailCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDExchaneDetailCell.h"
#import "UILabel+PDCategory.h"

CGFloat const rowHeight = 44;
CGFloat const textFont = 14;   /**< 下面详细信息的字体大小*/
CGFloat const heightTop = rowHeight;    /**< 兑换信息条的高度*/
CGFloat const heightCenter = 90.;
CGFloat const imageTop = 12.;
CGFloat const spaceImage_label = 15.;
CGFloat const spaceName_label = 8.;
CGFloat const spaceLabelRight = 20.;

#define kDetailSize ([@"计算单行" sizeWithCalcFont:[UIFont systemFontOfCustomeSize:textFont]])
#define kDetailSpace ((rowHeight - kDetailSize.height)/2)   // 每条信息的上下想对于（44）单元格的高度的空隙
#define kConstraintWidth (kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - kDetailSize.width - spaceLabelRight)

@interface PDExchaneDetailCell ()
@property (nonatomic, strong) UILabel *infoLabel;   /**< 兑换信息*/
@property (nonatomic, strong) UILabel *stateLabel;  /**< 奖品状态*/
@property (nonatomic, strong) UIView *line1;
@property (nonatomic, strong) PDImageView *productImageView;
@property (nonatomic, strong) UILabel *nameLabel;   /**< 名字可能多行*/
@property (nonatomic, strong) UILabel *bambooLabel;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UIView *line2;

@property (nonatomic, strong) UILabel *descOddLabel;    /**< 单号描述*/
@property (nonatomic, strong) UILabel *detailOddLabel;  /**< 单号*/

@property (nonatomic, strong) UILabel *descDateLabel;
@property (nonatomic, strong) UILabel *detailDateLabel;

@property (nonatomic, strong) UILabel *descAddressLabel;
@property (nonatomic, strong) UILabel *detailAddressLabel;  /**< 地址可能多行*/

@property (nonatomic, strong) UILabel *descMarkLabel;
@property (nonatomic, strong) UILabel *detailMarkLabel; /**< 备注可能多行*/
@end

@implementation PDExchaneDetailCell

+ (CGFloat)cellHeightWithModel:(PDBackpackUsedItemModel *)model {
    CGFloat height = heightTop + heightCenter;
    
    CGSize constraintSize = CGSizeMake(kConstraintWidth, CGFLOAT_MAX);
    CGSize oddSize = [model.ID sizeWithCalcFont:[UIFont systemFontOfCustomeSize:textFont] constrainedToSize:constraintSize];
    CGSize dateSize = [[PDCenterTool dateFromTimestamp:model.createTime] sizeWithCalcFont:[UIFont systemFontOfCustomeSize:textFont]];
    if ([model.exchangeCategory isEqualToString:@"realItem"]) {
        CGSize addressSize = [model.address sizeWithCalcFont:[UIFont systemFontOfCustomeSize:textFont] constrainedToSize:constraintSize];
        height += addressSize.height + kDetailSpace;
    }
    CGSize markSize = [model.remark sizeWithCalcFont:[UIFont systemFontOfCustomeSize:textFont] constrainedToSize:constraintSize];
    height += kDetailSpace + oddSize.height + kDetailSpace + dateSize.height + kDetailSpace + markSize.height + kDetailSpace;
    return height;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)pageSetting {
    // 兑换信息
    self.infoLabel = [[UILabel alloc] init];
    self.infoLabel.text = @"兑奖信息";
    self.infoLabel.font = [UIFont systemFontOfCustomeSize:15];
    CGSize infoSize = [self.infoLabel.text sizeWithCalcFont:self.infoLabel.font];
    self.infoLabel.frame = CGRectMake(kTableViewSectionHeader_left, (rowHeight - infoSize.height) / 2, infoSize.width, infoSize.height);
    self.infoLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.infoLabel];
    
    // 奖品已发出
    self.stateLabel = [[UILabel alloc] init];
    self.stateLabel.textColor = c26;
    self.stateLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.stateLabel];
    
    // 线
    self.line1 = [[UIView alloc] initWithFrame:CGRectMake(0, rowHeight - .5, kScreenBounds.size.width, .5)];
    self.line1.backgroundColor = c27;
    [self.contentView addSubview:self.line1];
    
    //商品图片
    self.productImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, heightTop + imageTop, (heightCenter - 2 * imageTop), (heightCenter - 2 * imageTop))];
    self.productImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.productImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.productImageView];
    
    // 名字
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:16];
    self.nameLabel.textColor = [UIColor blackColor];
    self.nameLabel.constraintSize = CGSizeMake(kScreenBounds.size.width - 2 * kTableViewSectionHeader_left - spaceImage_label - CGRectGetWidth(self.productImageView.frame), CGRectGetHeight(self.productImageView.frame) - spaceName_label - [@"计算" sizeWithCalcFont:self.nameLabel.font].height);
    self.nameLabel.numberOfLines = 0;
    [self.contentView addSubview:self.nameLabel];
    
    // 竹子
    self.bambooLabel = [[UILabel alloc] init];
    self.bambooLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.bambooLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.bambooLabel];
    
    // x1
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = self.bambooLabel.font;
    self.numLabel.textColor = self.bambooLabel.textColor;
    [self.contentView addSubview:self.numLabel];
    
    // 线
    self.line2 = [[UIView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, heightCenter + heightTop - .5, kScreenBounds.size.width - kTableViewSectionHeader_left, .5)];
    self.line2.backgroundColor = self.line1.backgroundColor;
    [self.contentView addSubview:self.line2];
    
    // 兑换单号
    self.descOddLabel = [[UILabel alloc] init];
    self.descOddLabel.font = [UIFont systemFontOfCustomeSize:textFont];
    self.descOddLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.descOddLabel];
    
    // 单号
    self.detailOddLabel = [self labelSetting];
    self.detailOddLabel.numberOfLines = 0;
    self.detailOddLabel.constraintSize = CGSizeMake(kConstraintWidth, CGFLOAT_MAX);
    
    // 兑换时间
    self.descDateLabel = [self labelSetting];
    
    // 时间
    self.detailDateLabel = [self labelSetting];
    
    // 收获地址
    self.descAddressLabel = [self labelSetting];
    
    // 地址
    self.detailAddressLabel = [self labelSetting];
    self.detailAddressLabel.numberOfLines = 0;
    self.detailAddressLabel.constraintSize = self.detailOddLabel.constraintSize;
    
    // 备注信息
    self.descMarkLabel = [self labelSetting];
    
    // 备注
    self.detailMarkLabel = [self labelSetting];
    self.detailMarkLabel.numberOfLines = 0;
    self.detailMarkLabel.constraintSize = self.detailAddressLabel.constraintSize;
}

- (UILabel *)labelSetting {
    UILabel *label = [[UILabel alloc] init];
    label.textColor = self.descOddLabel.textColor;
    label.font = self.descOddLabel.font;
    [self.contentView addSubview:label];
    return label;
}

- (void)setModel:(PDBackpackUsedItemModel *)model {
    _model = model;
    
    if ([model.state isEqualToString:@"NEEDDEAL"]) {
        self.stateLabel.text = @"等待处理";
    } else if ([model.state isEqualToString:@"DONE"]) {
        self.stateLabel.text = @"奖品已发出";
    }
    
    [self.productImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithRisqueUrl:model.commodityPic] placeholder:nil callback:NULL];
    
    self.nameLabel.text = model.commodityName;
    self.bambooLabel.text = [NSString stringWithFormat:@"%@¥",model.commodityTotalValue];
    self.numLabel.text = [NSString stringWithFormat:@"X%ld",(long)model.amount];
    self.descOddLabel.text = @"兑换单号";
    self.detailOddLabel.text = model.ID;
    self.descDateLabel.text = @"兑换时间";
    self.detailDateLabel.text = [PDCenterTool dateFromTimestamp:model.createTime];
    if ([model.exchangeCategory isEqualToString:@"realItem"]) {
        self.descAddressLabel.text = @"收货地址";
        self.detailAddressLabel.text = model.address;
    }
    
    self.descMarkLabel.text = @"备注信息";
    self.detailMarkLabel.text = model.remark;
    
    self.stateLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - self.stateLabel.size.width, (rowHeight - self.stateLabel.size.height) / 2, self.stateLabel.size.width, self.stateLabel.size.height);
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + spaceImage_label, CGRectGetMinY(self.productImageView.frame) + 3., self.nameLabel.size.width, self.nameLabel.size.height);
    self.bambooLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + spaceName_label, self.bambooLabel.size.width, self.bambooLabel.size.height);
    self.numLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - self.numLabel.size.width, CGRectGetMinY(self.bambooLabel.frame), self.numLabel.size.width, self.numLabel.size.height);
    self.descOddLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.line2.frame) + kDetailSpace, self.descOddLabel.size.width, self.descOddLabel.size.height);
    self.detailOddLabel.frame = CGRectMake(CGRectGetMaxX(self.descOddLabel.frame) + spaceLabelRight, CGRectGetMinY(self.descOddLabel.frame), self.detailOddLabel.size.width, self.detailOddLabel.size.height);
    self.descDateLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.detailOddLabel.frame) + kDetailSpace, self.descDateLabel.size.width, self.descDateLabel.size.height);
    self.detailDateLabel.frame = CGRectMake(CGRectGetMinX(self.detailOddLabel.frame), CGRectGetMinY(self.descDateLabel.frame), self.detailDateLabel.size.width, self.detailDateLabel.size.height);
    self.descAddressLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.detailDateLabel.frame) + kDetailSpace, self.descAddressLabel.size.width, self.descAddressLabel.size.height);
    self.detailAddressLabel.frame = CGRectMake(CGRectGetMinX(self.detailDateLabel.frame), CGRectGetMinY(self.descAddressLabel.frame), self.detailAddressLabel.size.width, self.detailAddressLabel.size.height);
    CGFloat space = self.detailAddressLabel.size.height != 0? kDetailSpace : .0;
    self.descMarkLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.detailDateLabel.frame) + kDetailSpace + self.detailAddressLabel.size.height + space, self.descMarkLabel.size.width, self.descMarkLabel.size.height);
    self.detailMarkLabel.frame = CGRectMake(CGRectGetMinX(self.detailDateLabel.frame), CGRectGetMinY(self.descMarkLabel.frame), self.detailMarkLabel.size.width, self.detailMarkLabel.size.height);
}

@end
