//
//  PDExchaneDetailCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDBackpackUsedItemModel.h"

/// 兑换详情信息
@interface PDExchaneDetailCell : UITableViewCell
@property (nonatomic, strong) PDBackpackUsedItemModel *model;

+ (CGFloat)cellHeightWithModel:(PDBackpackUsedItemModel *)model;
@end
