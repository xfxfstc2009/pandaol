//
//  PDBackpackTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBackpackTableViewCell.h"

#define hSpace  LCFloat(15) /**< 白色背景图案水平方向上的左｜右距离屏幕的间距*/
#define vSpace  LCFloat(18)  /**< 白色背景图案竖直方向上的上｜下距离屏幕的间距*/
#define kBorderSpace LCFloat(5.) /**< 背景图片的双边框厚度*/
CGFloat const backheightTop = 100.; /**< 上部内容的高度 除去边框，下同*/
CGFloat const backheightCenter = 40.;
CGFloat const heightBottom = 55.;

@interface PDBackpackTableViewCell ()
@property (nonatomic, assign) PDBackpackTableViewCellType type;

@property (nonatomic, strong) UIImageView *backgroudImageView;
@property (nonatomic, strong) PDImageView *productImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UIView *line1;
@property (nonatomic, strong) UIView *line2;
@property (nonatomic, strong) UILabel *descLabel;   /**< 使用时间描述标签*/
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *eventLabel;  /**< 处理状态标签*/

@property (nonatomic, assign) CGFloat imageInsetSpace;   /**< 图片距离边线的空隙*/
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, assign) CGFloat spaceImage_label; /**< 图到名字*/
@property (nonatomic, assign) CGFloat spaceNameLabel_label; /**< 名字到价格*/
@property (nonatomic, assign) CGFloat spaceDescLabel_label; /**< desc到date*/
@end

@implementation PDBackpackTableViewCell

+ (CGFloat)cellHeight {
    return kBorderSpace * 2 + backheightTop + backheightCenter + heightBottom + vSpace * 2;
}

- (instancetype)initWithType:(PDBackpackTableViewCellType)type reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        _type = type;
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _rowHeight = [PDBackpackTableViewCell cellHeight];
    _imageInsetSpace = 10.;
    _spaceImage_label = 8.;
    _spaceNameLabel_label = 10.;
    _spaceDescLabel_label = 20.;
}

- (void)pageSetting {

    self.backgroudImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, (kScreenBounds.size.width), _rowHeight)];
    self.backgroudImageView.image = [UIImage imageNamed:@"icon_backpack_bg1"];
    self.backgroudImageView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.backgroudImageView];
    
    // 商品图片
    CGFloat imageWidth = (backheightTop - 2 * _imageInsetSpace);
    self.productImageView = [[PDImageView alloc] initWithFrame:CGRectMake(_imageInsetSpace + kBorderSpace + hSpace, kBorderSpace + _imageInsetSpace + vSpace, imageWidth, imageWidth)];
    self.productImageView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.productImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.productImageView.clipsToBounds = YES;
    [self.backgroudImageView addSubview:self.productImageView];
    
    // 商品名字
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [[UIFont systemFontOfCustomeSize:18] boldFont];
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.textColor = [UIColor blackColor];
    [self.backgroudImageView addSubview:self.nameLabel];
    
    // 价格
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.priceLabel.textColor = c26;
    [self.backgroudImageView addSubview:self.priceLabel];
    
    // 线
    self.line1 = [[UIView alloc] initWithFrame:CGRectMake(kBorderSpace + hSpace + 1, kBorderSpace + backheightTop - .5 + vSpace, kScreenBounds.size.width - kBorderSpace * 2 - hSpace * 2 - 2, .5)];
    self.line1.backgroundColor = c27;
    [self.backgroudImageView addSubview:self.line1];
    
    // 时间desc
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.descLabel.textColor = [UIColor blackColor];
    [self.backgroudImageView addSubview:self.descLabel];
    
    // 时间
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.dateLabel.textColor = c28;
    [self.backgroudImageView addSubview:self.dateLabel];
    
    // 线2
    self.line2 = [[UIView alloc] initWithFrame:CGRectMake(kBorderSpace + hSpace + 1, kBorderSpace + backheightTop + backheightCenter - .5 + vSpace, CGRectGetWidth(self.line1.frame), .5)];
    self.line2.backgroundColor = c27;
    [self.backgroudImageView addSubview:self.line2];
    
    if (_type == PDBackpackTableViewCellTypeHad) {
        for (int i = 0; i < 2; i++) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.frame = CGRectMake(kBorderSpace + i * (CGRectGetWidth(self.line2.frame) / 2) + hSpace, CGRectGetMaxY(self.line2.frame), (CGRectGetWidth(self.line2.frame) / 2), heightBottom);
            button.tag = i;
            button.titleLabel.font = [UIFont systemFontOfCustomeSize:16];
            [button addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
            
            if (i == 0) {
                [button setTitle:@"赠送好友" forState:UIControlStateNormal];
                [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
            } else {
                [button setTitle:@"立即使用" forState:UIControlStateNormal];
                [button setTitleColor:c26 forState:UIControlStateNormal];
            }
            [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
            [self.backgroudImageView addSubview:button];
        }
        
    } else {
        self.eventLabel = [[UILabel alloc] init];
        self.eventLabel.font = [UIFont systemFontOfCustomeSize:15];
        [self.backgroudImageView addSubview:self.eventLabel];
    }
}

#pragma mark -- 控件方法

- (void)didClickButton:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(tableViewCell:didClickButtonAtIndex:tableViewAtIndexPath:)]) {
        [self.delegate tableViewCell:self didClickButtonAtIndex:button.tag tableViewAtIndexPath:self.indexPath];
    }
}

#pragma mark -- set

- (void)setItemModel:(PDBackpackItemModel *)itemModel {
    _itemModel = itemModel;
    
    self.nameLabel.text = itemModel.commodityName;
    self.priceLabel.text = [NSString stringWithFormat:@"价值：¥%@",itemModel.commodityValue];
    self.descLabel.text = @"获得时间";
    self.dateLabel.text = [PDCenterTool dateFromTimestamp:itemModel.createTime];
    [self.productImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithRisqueUrl:itemModel.pic] placeholder:nil callback:NULL];
    
    CGSize priceSize = [self.priceLabel.text sizeWithCalcFont:self.priceLabel.font];
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGRectGetWidth(self.backgroudImageView.frame) - kBorderSpace * 2 - _imageInsetSpace - _spaceImage_label * 2 - CGRectGetWidth(self.productImageView.frame) - hSpace * 2, [NSString contentofHeightWithFont:self.nameLabel.font] * 2)];
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    
    self.nameLabel.frame = CGRectMake((CGRectGetMaxX(self.productImageView.frame) + _spaceImage_label), _imageInsetSpace + 3. + vSpace, nameSize.width, nameSize.height);
    self.priceLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + _spaceNameLabel_label, priceSize.width, priceSize.height);
    self.dateLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), kBorderSpace + backheightTop + (backheightCenter - dateSize.height) / 2 + vSpace, dateSize.width, dateSize.height);
    self.descLabel.frame = CGRectMake(CGRectGetMinX(self.dateLabel.frame) - _spaceDescLabel_label - descSize.width, CGRectGetMinY(self.dateLabel.frame), descSize.width, descSize.height);
}

- (void)setUsedItemModel:(PDBackpackUsedItemModel *)usedItemModel {
    _usedItemModel = usedItemModel;
    
    self.nameLabel.text = usedItemModel.commodityName;
    self.priceLabel.text = [NSString stringWithFormat:@"价值：%@¥",usedItemModel.commodityTotalValue];
    self.descLabel.text = @"使用时间";
    
    if ([usedItemModel.state isEqualToString:@"NEEDDEAL"]) {
        self.eventLabel.text = @"正在处理...";
        self.eventLabel.textColor = c26;
    } else if ([usedItemModel.state isEqualToString:@"DONE"]) {
        self.eventLabel.text = @"已处理";
    }
    
    self.dateLabel.text = [PDCenterTool dateFromTimestamp:usedItemModel.createTime];
    [self.productImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithRisqueUrl:usedItemModel.commodityPic] placeholder:nil callback:NULL];
    
    CGSize priceSize = [self.priceLabel.text sizeWithCalcFont:self.priceLabel.font];
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGRectGetWidth(self.backgroudImageView.frame) - kBorderSpace * 2 - _imageInsetSpace - _spaceImage_label * 2 - CGRectGetWidth(self.productImageView.frame) - hSpace * 2, [NSString contentofHeightWithFont:self.nameLabel.font] * 2)];
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGSize eventSize = [self.eventLabel.text sizeWithCalcFont:self.eventLabel.font];
    
    self.nameLabel.frame = CGRectMake((CGRectGetMaxX(self.productImageView.frame) + _spaceImage_label), _imageInsetSpace + 3. + vSpace, nameSize.width, nameSize.height);
    self.priceLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + _spaceNameLabel_label, priceSize.width, priceSize.height);
    self.dateLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), kBorderSpace + backheightTop + (backheightCenter - dateSize.height) / 2 + vSpace, dateSize.width, dateSize.height);
    self.descLabel.frame = CGRectMake(CGRectGetMinX(self.dateLabel.frame) - _spaceDescLabel_label - descSize.width, CGRectGetMinY(self.dateLabel.frame), descSize.width, descSize.height);
    self.eventLabel.frame = CGRectMake((CGRectGetWidth(self.backgroudImageView.frame) - eventSize.width) / 2, kBorderSpace + backheightTop + backheightCenter + (heightBottom - eventSize.height) / 2 + vSpace, eventSize.width, eventSize.height);
}

@end
