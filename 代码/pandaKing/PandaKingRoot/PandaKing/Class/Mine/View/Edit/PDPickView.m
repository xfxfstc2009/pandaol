//
//  PDPickView.m
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPickView.h"

typedef void(^clickBlock)();
@interface PDPickView ()
@property (nonatomic, copy)   NSString *title;
@property (nonatomic, strong) UIView *pickerBar;
@property (nonatomic, strong) UIView *titleBar;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat barHeight;
@property (nonatomic, assign) CGFloat titleBarHeight;
@property (nonatomic, assign) NSTimeInterval duration;

@property (nonatomic, copy)   clickBlock sureBlcok;
@property (nonatomic, copy)   clickBlock cancleBlock;
@end

@implementation PDPickView

- (instancetype)initWithTitle:(NSString *)title {
    self = [super initWithFrame:kScreenBounds];
    if (self) {
        _title = title;
        [self _basicSetting];
        [self _pageSetting];
    }
    return self;
}

- (void)_basicSetting {
    _barHeight = LCFloat(260.);
    _titleBarHeight = LCFloat(50.);
    _duration = .3;
    _size = CGSizeMake(kScreenBounds.size.width, _barHeight);
}

- (void)_pageSetting {
    self.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickBackground)];
    [self addGestureRecognizer:tap];
    
    self.pickerBar = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenBounds.size.height, kScreenBounds.size.width, _barHeight)];
    self.pickerBar.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.pickerBar];
    
    // titleBar
    self.titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, _titleBarHeight)];
    self.titleBar.backgroundColor = RGB(246, 247, 248, 1);
    [self.pickerBar addSubview:self.titleBar];
    
    // 取消按钮
    //inset
    CGFloat inset = kTableViewSectionHeader_left / 2;
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancleButton setImage:[UIImage imageNamed:@"icon_center_cancle"] forState:UIControlStateNormal];
    [cancleButton setFrame:CGRectMake(0, 0, _titleBarHeight, _titleBarHeight)];
    [cancleButton setImageEdgeInsets:UIEdgeInsetsMake(inset, inset, inset, inset)];
    [cancleButton addTarget:self action:@selector(didClickCancle) forControlEvents:UIControlEventTouchUpInside];
    [self.titleBar addSubview:cancleButton];
    // 确定按钮
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureButton setImage:[UIImage imageNamed:@"icon_center_sure"] forState:UIControlStateNormal];
    [sureButton setImageEdgeInsets:UIEdgeInsetsMake(inset, inset, inset, inset)];
    [sureButton setFrame:CGRectMake(kScreenBounds.size.width - _titleBarHeight, 0, _titleBarHeight, _titleBarHeight)];
    [sureButton addTarget:self action:@selector(didClickSure) forControlEvents:UIControlEventTouchUpInside];
    [self.titleBar addSubview:sureButton];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    self.titleLabel.text = self.title;
    CGSize size = [self.title sizeWithCalcFont:self.titleLabel.font];
    self.titleLabel.frame = CGRectMake((kScreenBounds.size.width - size.width) / 2, (_titleBarHeight - size.height) / 2, size.width, size.height);
    [self.titleBar addSubview:self.titleLabel];
    
    [self addToWindow];
}

- (void)addToWindow {
    NSEnumerator *windowEnnumtor = [UIApplication sharedApplication].windows.reverseObjectEnumerator;
    for (UIWindow *window in windowEnnumtor) {
        BOOL isOnMainScreen = window.screen == [UIScreen mainScreen];
        BOOL isVisible      = !window.hidden && window.alpha > 0;
        BOOL isLevelNormal  = window.windowLevel == UIWindowLevelNormal;
        
        if (isOnMainScreen && isVisible && isLevelNormal) {
            [window addSubview:self];
            [self show];
        }
    }
}

- (void)setPutView:(UIView *)putView {
    _putView = putView;
    
    self.putView.frame = CGRectMake(0, _titleBarHeight, _size.width, _size.height - _titleBarHeight);
    self.putView.backgroundColor = [UIColor whiteColor];
    [self.pickerBar addSubview:self.putView];
}

- (void)clickCancle:(void (^)())clickBlock {
    if (clickBlock) {
        self.cancleBlock = clickBlock;
    }
}

- (void)clickSure:(void (^)())clickBlock {
    if (clickBlock) {
        self.sureBlcok = clickBlock;
    }
}

#pragma mark -- 控件方法

- (void)didClickCancle {
    if (self.cancleBlock) {
        self.cancleBlock();
    }
    [self dismiss];
}

- (void)didClickSure {
    if (self.sureBlcok) {
        self.sureBlcok();
    }
    [self dismiss];
}

- (void)didClickBackground {
    [self dismiss];
}

#pragma mark -- 显示隐藏

- (void)show {
    [UIView animateWithDuration:_duration animations:^{
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.4];
        self.pickerBar.frame = CGRectOffset(self.pickerBar.frame, 0, -_barHeight);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)dismiss {
    
    [UIView animateWithDuration:_duration animations:^{
        self.backgroundColor = [UIColor clearColor];
        self.pickerBar.frame = CGRectOffset(self.pickerBar.frame, 0, _barHeight);
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end



@interface PDAddressPickView ()
@end

@implementation PDAddressPickView

- (instancetype)initWithTitle:(NSString *)title {
    self = [super initWithTitle:title];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    
}

- (void)pageSetting {
    self.pickerView = [[UIPickerView alloc] init];
    self.putView = self.pickerView;
}


@end


@implementation PDAgePickView

- (instancetype)initWithTitle:(NSString *)title {
    self = [super initWithTitle:title];
    if (self) {
        self.pickerView = [[UIPickerView alloc] init];
        self.putView = self.pickerView;
    }
    return self;
}

@end


