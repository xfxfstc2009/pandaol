//
//  PDEditInfoTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 附带单一的输入框的基类单元格
@interface PDEditInfoTableViewCell : UITableViewCell<UITextFieldDelegate>
@property (nonatomic, strong) UITextField *textField;
+ (CGFloat)cellHeight;

- (instancetype)initWithTextFont:(UIFont *)font reuseIdentifier:(NSString *)reuseIdentifier;
@end
