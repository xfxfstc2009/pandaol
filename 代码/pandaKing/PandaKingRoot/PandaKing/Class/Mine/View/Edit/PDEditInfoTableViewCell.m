//
//  PDEditInfoTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDEditInfoTableViewCell.h"

@implementation PDEditInfoTableViewCell

- (instancetype)initWithTextFont:(UIFont *)font reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self pageSettingWithFont:font];
    }
    return self;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self pageSettingWithFont:[UIFont systemFontOfCustomeSize:16]];
    }
    return self;
}

+ (CGFloat)cellHeight {
    return 50.;
}

- (void)pageSettingWithFont:(UIFont *)font {
    // 输入框
    CGFloat textHeight = 36.;
    CGFloat rowHeight = [PDEditInfoTableViewCell cellHeight];
    UITextField *textField = [[UITextField alloc] init];
    textField.delegate = self;
    textField.font = font;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.frame = CGRectMake(kTableViewSectionHeader_left, (rowHeight - textHeight) / 2, kScreenBounds.size.width - 2 * kTableViewSectionHeader_left, textHeight);
    [self.contentView addSubview:textField];
    self.textField = textField;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.text.length > 36) {
        textField.text = [textField.text substringToIndex:36];
        [PDHUD showHUDSuccess:@"标题最多36个字符"];
    }
}

@end
