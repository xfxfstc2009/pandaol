//
//  PDSettingTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/8/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSettingTableViewCell.h"

#define kSettingCellLabel_width LCFloat(84)
#define kSettingCellButton_height LCFloat(30 + 20)
#define kSettingCellButton_width  LCFloat(30 + 10)

@interface PDSettingTableViewCell ()
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIButton *showButton;
@property (nonatomic, assign) CGFloat space;        /**< textField 左侧距离label的间隙*/
@property (nonatomic, strong) UIFont *font;
@end

@implementation PDSettingTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.font = [UIFont systemFontOfCustomeSize:16];
        self.space = LCFloat(10);
        self.isShow = NO;
        
        [self basicSetting];
    }
    return self;
}

- (void)basicSetting {
    UILabel *descLabel = [[UILabel alloc] init];
//    descLabel.backgroundColor = [UIColor yellowColor];
    descLabel.font = self.font;
    descLabel.textColor = [UIColor blackColor];
    descLabel.hidden = YES;
    [self.contentView addSubview:descLabel];
    self.descLabel = descLabel;
    
    // show button
    UIButton *showButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    showButton.backgroundColor = [UIColor redColor];
    showButton.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - kSettingCellButton_width, (kSettingsCell_height - kSettingCellButton_height) / 2, kSettingCellButton_width, kSettingCellButton_height);
    [showButton setImage:[UIImage imageNamed:@"icon_settings_passwordshow"] forState:UIControlStateNormal];
    [showButton setImage:[UIImage imageNamed:@"icon_settings_passwordhidden"] forState:UIControlStateSelected];
    [showButton setImageEdgeInsets:UIEdgeInsetsMake(LCFloat(10), LCFloat(10), LCFloat(10), 0)];
    [showButton addTarget:self action:@selector(showButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:showButton];
    self.showButton = showButton;
    
    // 输入框
    UITextField *textField = [[UITextField alloc] init];
    textField.font = self.font;
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    
    [self.contentView addSubview:textField];
    self.textField = textField;

}

#pragma mark -- 控件方法

- (void)showButtonClick:(UIButton *)button {
    button.selected = !button.selected;
    
    if (button.selected == YES) {   //不明文

         self.textField.secureTextEntry = YES;
    } else {
        
        self.textField.secureTextEntry = NO;
    }
}

#pragma mark -- set

- (void)setDesc:(NSString *)desc {
    _desc = desc;
    
    self.descLabel.text = desc;
    CGSize size = [desc sizeWithCalcFont:self.descLabel.font];
    self.descLabel.frame = CGRectMake(kTableViewSectionHeader_left, (kSettingsCell_height - size.height) / 2, kSettingCellLabel_width, size.height);
}

- (void)setIsShow:(BOOL)isShow {
    _isShow = isShow;
    
    CGFloat textField_width_showyes = kScreenBounds.size.width - kSettingCellLabel_width - kTableViewSectionHeader_left * 2 - self.space * 2 - kSettingCellButton_width;
    CGFloat textField_width_showno  = kScreenBounds.size.width - kSettingCellLabel_width - kTableViewSectionHeader_left * 2 - self.space;
    CGFloat textField_height = 30;
    
    if (!isShow) {
        self.showButton.hidden = YES;
        
        self.textField.frame = CGRectMake(kSettingCellLabel_width + kTableViewSectionHeader_left + self.space, (kSettingsCell_height - textField_height) / 2, textField_width_showno, textField_height);
    } else {
        self.showButton.hidden = NO;
        self.showButton.selected = YES;
        
        self.textField.frame = CGRectMake(kSettingCellLabel_width + kTableViewSectionHeader_left + self.space, (kSettingsCell_height - textField_height) / 2, textField_width_showyes, textField_height);
    }
}

@end
