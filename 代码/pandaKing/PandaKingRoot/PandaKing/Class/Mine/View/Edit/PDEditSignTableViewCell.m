//
//  PDEditSignTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDEditSignTableViewCell.h"


@implementation PDEditSignTableViewCell

+ (CGFloat)cellHeight {
    return 180.;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    CGFloat textHeight = 160.;
    CGFloat rowHeight = [PDEditSignTableViewCell cellHeight];
    
    self.textView = [[PDTextView alloc] init];
    self.textView.frame = CGRectMake(kTableViewSectionHeader_left, (rowHeight - textHeight) / 2, kScreenBounds.size.width - 2 * kTableViewSectionHeader_left, textHeight);
    self.textView.font = [UIFont systemFontOfCustomeSize:16];
    [self.contentView addSubview:self.textView];
}

@end
