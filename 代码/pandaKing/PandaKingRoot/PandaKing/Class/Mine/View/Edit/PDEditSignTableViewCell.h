//
//  PDEditSignTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDTextView.h"

/// 编辑签名的单元格
@interface PDEditSignTableViewCell : UITableViewCell
@property (nonatomic, strong) PDTextView *textView;
+ (CGFloat)cellHeight;
@end
