//
//  PDTextView.m
//  PandaKing
//
//  Created by Cranz on 16/9/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTextView.h"

@interface PDTextView ()<UITextViewDelegate>
@property (nonatomic, strong) UILabel *placeholderLabel;
@property (nonatomic, strong) UIFont *placeholderFont;

@property (nonatomic, assign) CGFloat x;
@property (nonatomic, assign) CGFloat y;

@property (nonatomic, copy) void(^textBlock)(NSString *text);
@end

@implementation PDTextView

- (UILabel *)placeholderLabel {
    if (!_placeholderLabel) {
        _placeholderLabel = [[UILabel alloc] init];
        _placeholderLabel.textColor = [UIColor lightGrayColor];
        [self addSubview:_placeholderLabel];
    }
    return _placeholderLabel;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        self.delegate = self;
        _x = 3;
        _y = 8;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.delegate = self;
        _x = 3;
        _y = 8;
    }
    return self;
}

- (void)layoutSubviews {
    
    self.placeholderFont = self.font;
    
    CGSize size = self.frame.size;
    CGSize placeholderSize = [self.placeholderLabel.text  boundingRectWithSize:CGSizeMake(size.width - _x * 2, [NSString contentofHeightWithFont:self.placeholderLabel.font]) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : self.placeholderLabel.font} context:nil].size;
    
    self.placeholderLabel.frame = CGRectMake(_x, _y, placeholderSize.width, placeholderSize.height);
    
    if (self.text.length) {
        self.placeholderLabel.hidden = YES;
    } else {
        self.placeholderLabel.hidden = NO;
    }
}

- (void)setPlace:(NSString *)place {
    _place = place;
    
    self.placeholderLabel.text = place;
}

- (void)setPlaceholderFont:(UIFont *)placeholderFont {
    _placeholderFont = placeholderFont;
    
    self.placeholderLabel.font = placeholderFont;
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor {
    _placeholderColor = placeholderColor;
    self.placeholderLabel.textColor = placeholderColor;
}

#pragma mark - 代理方法

- (void)textViewDidChange:(UITextView *)textView {
    if (self.textBlock) {
        self.textBlock(textView.text);
    }
    
    if (textView.text.length) {
        self.placeholderLabel.hidden = YES;
    } else {
        self.placeholderLabel.hidden = NO;
    }
}

- (void)textViewDiDChangeBlock:(void (^)(NSString *))block {
    if (block) {
        self.textBlock = block;
    }
}

@end
