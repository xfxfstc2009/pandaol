//
//  PDTextView.h
//  PandaKing
//
//  Created by Cranz on 16/9/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDTextView : UITextView
@property (nonatomic, copy)   NSString *place;
@property (nonatomic, strong) UIColor *placeholderColor;    /**< 默认灰色*/

- (void)textViewDiDChangeBlock:(void(^)(NSString *text))block;
@end
