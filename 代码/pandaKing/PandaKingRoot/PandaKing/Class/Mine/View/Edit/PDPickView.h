//
//  PDPickView.h
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 左侧叉叉 ｜ 中间标题 ｜ 右侧确定
@interface PDPickView : UIView
@property (nonatomic, strong) UIView *putView;
- (instancetype)initWithTitle:(NSString *)title;

- (void)clickSure:(void(^)())clickBlock;
- (void)clickCancle:(void(^)())clickBlock;
- (void)show;
- (void)dismiss;
@end

@interface PDAddressPickView : PDPickView
@property (nonatomic, strong) UIPickerView *pickerView;
@end

@interface PDAgePickView : PDPickView
@property (nonatomic, strong) UIPickerView *pickerView;
@end
