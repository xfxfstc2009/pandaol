//
//  PDSettingTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/8/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

static CGFloat const kSettingsCell_height = 50;

/// 通用于 ｜ 密码 **** 👀 
@interface PDSettingTableViewCell : UITableViewCell
@property (nonatomic, copy)   NSString *desc;   /**< 左侧描述信息*/
@property (nonatomic, strong) UITextField *textField;/**< 输入框*/
@property (nonatomic, assign) BOOL isShow;      /**< 是否显示眼镜按钮，默认为NO*/
@end
