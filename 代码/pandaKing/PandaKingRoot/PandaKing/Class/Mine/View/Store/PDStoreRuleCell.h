//
//  PDStoreRuleCell.h
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDStoreRuleCell : UITableViewCell
@property (nonatomic, copy) NSString *content;

+ (CGFloat)cellHeightWithContent:(NSString *)content;
@end
