//
//  PDStoreMyBambooCell.h
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDStoreMyBambooCell : UITableViewCell
@property (nonatomic, strong) NSString *model;

+ (CGFloat)cellHeight;
@end
