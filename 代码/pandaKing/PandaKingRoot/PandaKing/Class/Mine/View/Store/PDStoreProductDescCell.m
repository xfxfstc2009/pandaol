//
//  PDStoreProductDescCell.m
//  PandaKing
//
//  Created by Cranz on 17/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDStoreProductDescCell.h"
#import "PDCenterLabel.h"

@interface PDStoreProductDescCell ()
/**
 * 限购
 */
@property (nonatomic, strong) PDCenterLabel *limitLabel;
/**
 * 正品
 */
@property (nonatomic, strong) PDCenterLabel *qualityLabel;
/**
 * 上新奖品
 */
@property (nonatomic, strong) PDCenterLabel *toNewLabel;
@end

@implementation PDStoreProductDescCell

- (PDCenterLabel *)limitLabel {
    if (!_limitLabel) {
        _limitLabel = [[PDCenterLabel alloc] init];
        _limitLabel.image = [UIImage imageNamed:@"icon_store_ restriction"];
        _limitLabel.textFont = [UIFont systemFontOfCustomeSize:14];
        _limitLabel.textColor = c2;
        [self.contentView addSubview:_limitLabel];
    }
    return _limitLabel;
}

- (PDCenterLabel *)qualityLabel {
    if (!_qualityLabel) {
        _qualityLabel = [[PDCenterLabel alloc] init];
        _qualityLabel.image = [UIImage imageNamed:@"icon_store_ restriction"];
        _qualityLabel.textColor = c2;
        _qualityLabel.textFont = [UIFont systemFontOfCustomeSize:14];
        [self.contentView addSubview:_qualityLabel];
    }
    return _qualityLabel;
}

- (PDCenterLabel *)toNewLabel {
    if (!_toNewLabel) {
        _toNewLabel = [[PDCenterLabel alloc] init];
        _toNewLabel.image = [UIImage imageNamed:@"icon_store_ restriction"];
        _toNewLabel.textColor = c2;
        _toNewLabel.textFont = [UIFont systemFontOfCustomeSize:14];
        [self.contentView addSubview:_toNewLabel];
    }
    return _toNewLabel;
}

+ (CGFloat)cellHeight {
    return 44;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)setModel:(PDStoreActivityItem *)model {
    _model = model;
    
    if (model.limit) {
        self.limitLabel.text = [NSString stringWithFormat:@"限购%@份",@(model.limitAmount)];
    }
    
    self.qualityLabel.text = @"正品保障";
    self.toNewLabel.text = @"上新奖品";
    
    CGFloat cellHeight = [PDStoreProductDescCell cellHeight];
    self.limitLabel.frame = CGRectMake(20, (cellHeight - [self.limitLabel size].height) / 2, [self.limitLabel size].width, [self.limitLabel size].height);
    self.qualityLabel.frame = CGRectMake(model.limit? CGRectGetMaxX(self.limitLabel.frame) + 20 : 20, (cellHeight - [self.qualityLabel size].height) / 2, [self.qualityLabel size].width, [self.qualityLabel size].height);
    self.toNewLabel.frame = CGRectMake(CGRectGetMaxX(self.qualityLabel.frame) + 20, (cellHeight - [self.toNewLabel size].height) / 2, [self.toNewLabel size].width, [self.toNewLabel size].height);
}

@end
