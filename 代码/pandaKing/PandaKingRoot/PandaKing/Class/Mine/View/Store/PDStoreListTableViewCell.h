//
//  PDStoreListTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDStoreActivityItem.h"

static CGFloat const storeListCellUnitVSpaceHeight = 3; // 竖直方向上的空隙
static CGFloat const storeListCellUnitHSpaceHeight = 3; // 水平方向上的空隙
@interface PDStoreListTableViewCell : UITableViewCell
@property (nonatomic, strong) PDStoreActivityItem *model;

+ (CGFloat) cellHeight;
@end
