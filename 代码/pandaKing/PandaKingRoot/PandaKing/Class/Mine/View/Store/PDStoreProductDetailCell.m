//
//  PDStoreProductDetailCell.m
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDStoreProductDetailCell.h"

#define kLineSpace (LCFloat(6))
#define kNameFont ([[UIFont systemFontOfCustomeSize:16] boldFont])
#define kPriceFont ([[UIFont systemFontOfCustomeSize:25] boldFont])
#define kDateFont ([UIFont systemFontOfCustomeSize:13])
@interface PDStoreProductDetailCell ()
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *currentPriceLabel;
@property (nonatomic, strong) UILabel *oldPriceLable;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@end

@implementation PDStoreProductDetailCell

+ (CGFloat)cellHeightWithModel:(NSString *)model {
    CGSize nameSize = [model sizeWithCalcFont:kNameFont constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2, CGFLOAT_MAX)];
    CGFloat priceHeight = [NSString contentofHeightWithFont:kPriceFont];
    CGFloat dateHeight = [NSString contentofHeightWithFont:kDateFont];
    return nameSize.height + priceHeight + dateHeight + 4 * kLineSpace;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self pageSetting];
    return self;
}

- (void)pageSetting {
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = kNameFont;
    self.nameLabel.textColor = [UIColor blackColor];
    self.nameLabel.numberOfLines = 0;
    [self.contentView addSubview:self.nameLabel];
    
    self.currentPriceLabel = [[UILabel alloc] init];
    self.currentPriceLabel.font = kPriceFont;
    self.currentPriceLabel.textColor = [UIColor redColor];
    [self.contentView addSubview:self.currentPriceLabel];
    
    self.oldPriceLable = [[UILabel alloc] init];
    self.oldPriceLable.font = [UIFont systemFontOfCustomeSize:15];
    self.oldPriceLable.textColor = c4;
    [self.contentView addSubview:self.oldPriceLable];
    
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.textColor = [UIColor grayColor];
    self.numLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.numLabel.textAlignment = NSTextAlignmentCenter;
    self.numLabel.layer.cornerRadius = 3;
    self.numLabel.clipsToBounds = YES;
    [self.contentView addSubview:self.numLabel];
    
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = kDateFont;
    self.dateLabel.textColor = [UIColor grayColor];
    [self.contentView addSubview:self.dateLabel];
}

- (void)setModel:(PDStoreActivityItem *)model {
    _model = model;
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@%@", [PDCenterTool nameForCategory:model.commodity.exchangeCategory], model.commodity.name];
    self.currentPriceLabel.text = [NSString stringWithFormat:@"%.2f", model.discountPrice];
    self.oldPriceLable.text = [NSString stringWithFormat:@"%.2f", model.originalPrice];
    if (model.amount <= 0) {
        self.numLabel.text = @"已售罄";
        self.numLabel.backgroundColor = [UIColor lightGrayColor];
    } else {
        self.numLabel.text = [NSString stringWithFormat:@"库存%ld套",model.amount];
        self.numLabel.backgroundColor = RGB(229, 210, 195, 1);
    }
    
    self.dateLabel.text = [NSString stringWithFormat:@"活动时间:%@-%@",[PDCenterTool dateWithFormat:@"yyyy.MM.dd" fromTimestamp:model.startTime], [PDCenterTool dateWithFormat:@"yyyy.MM.dd" fromTimestamp:model.endTime]];
    
    NSMutableAttributedString *strikeAtt = [[NSMutableAttributedString alloc] initWithString:self.oldPriceLable.text attributes:@{NSStrikethroughStyleAttributeName:@1}];
    [strikeAtt addAttribute:NSStrikethroughColorAttributeName value:self.oldPriceLable.textColor range:NSMakeRange(0, self.oldPriceLable.text.length)];
    self.oldPriceLable.attributedText = [strikeAtt copy];
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2, CGFLOAT_MAX)];
    CGSize currentSize = [self.currentPriceLabel.text sizeWithCalcFont:self.currentPriceLabel.font];
    CGSize oldSize = [self.oldPriceLable.text sizeWithCalcFont:self.oldPriceLable.font];
    CGSize numTextSize = [self.numLabel.text sizeWithCalcFont:self.numLabel.font];
    CGSize numSize = CGSizeMake(numTextSize.width + 10, numTextSize.height + 4);
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    
    self.nameLabel.frame = CGRectMake(kTableViewSectionHeader_left, kLineSpace, nameSize.width, nameSize.height);
    self.currentPriceLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.nameLabel.frame) + kLineSpace, currentSize.width, currentSize.height);
    self.oldPriceLable.frame = CGRectMake(CGRectGetMaxX(self.currentPriceLabel.frame) + LCFloat(5), CGRectGetMaxY(self.currentPriceLabel.frame) - oldSize.height, oldSize.width, oldSize.height);
    self.numLabel.frame = CGRectMake(CGRectGetMaxX(self.oldPriceLable.frame) + LCFloat(20), CGRectGetMaxY(self.currentPriceLabel.frame) - numSize.height, numSize.width, numSize.height);
    self.dateLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.currentPriceLabel.frame) + kLineSpace, dateSize.width, dateSize.height);
}

@end
