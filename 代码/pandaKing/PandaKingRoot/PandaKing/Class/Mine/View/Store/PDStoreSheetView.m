//
//  PDStoreSheetView.m
//  PandaKing
//
//  Created by Cranz on 16/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDStoreSheetView.h"

#define kShowHeight   LCFloat(80)
#define kButtonHeight 44
#define kSelectHeight LCFloat(120)

typedef void(^submitGoldBlock)(NSInteger count);

@interface PDStoreSheetView ()
@property (nonatomic, strong) UILabel *bambooLabel;
@property (nonatomic, strong) UILabel *numLabel;  /**< 中奖比例*/
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, copy)   submitGoldBlock goldBlcok;
@property (nonatomic, strong) UIButton *sureButton;
@end

@implementation PDStoreSheetView


- (instancetype)init {
    self = [super init];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    self.canTouchedCancle = NO;
    
    UIView *putView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kShowHeight + kButtonHeight + kSelectHeight)];
    putView.backgroundColor = [UIColor whiteColor];
    self.putView = putView;
    
    // 金币 desc
    UILabel *goldDescLabel = [[UILabel alloc] init];
    goldDescLabel.text = @"竹子";
    goldDescLabel.textColor = [UIColor grayColor];
    goldDescLabel.backgroundColor = BACKGROUND_VIEW_COLOR;
    goldDescLabel.font = [UIFont systemFontOfCustomeSize:14];
    goldDescLabel.textAlignment = NSTextAlignmentCenter;
    CGSize goldDescSize = [goldDescLabel.text sizeWithCalcFont:goldDescLabel.font];
    goldDescLabel.frame = CGRectMake(kTableViewSectionHeader_left, (kShowHeight - goldDescSize.height - LCFloat(20)) / 2, goldDescSize.width + LCFloat(30), goldDescSize.height + LCFloat(20));
    [putView addSubview:goldDescLabel];
    
    self.bambooLabel = [[UILabel alloc] init];
    self.bambooLabel.font = [UIFont systemFontOfCustomeSize:18];
    [putView addSubview:self.bambooLabel];
    
    // 竖线
    UIView *vLine = [[UIView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width / 2 - .5, 0, 1, kShowHeight)];
    vLine.backgroundColor = c27;
    [putView addSubview:vLine];
    
    // 比例
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = [[UIFont systemFontOfCustomeSize:22] boldFont];
    self.numLabel.textColor = [UIColor blackColor];
    [putView addSubview:self.numLabel];
    
    // 比例 － desc
    UILabel *numDescLabel = [[UILabel alloc] init];
    numDescLabel.text = @"兑换\n数量";
    numDescLabel.textColor = [UIColor grayColor];
    numDescLabel.numberOfLines = 0;
    numDescLabel.font = [UIFont systemFontOfCustomeSize:14];
    CGSize scaleDescSize = [numDescLabel.text sizeWithCalcFont:numDescLabel.font];
    numDescLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - scaleDescSize.width, (kShowHeight - scaleDescSize.height) / 2, scaleDescSize.width, scaleDescSize.height);
    [putView addSubview:numDescLabel];
    
    // 横线
    UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, kShowHeight, kScreenBounds.size.width, 1)];
    hLine.backgroundColor = c27;
    [putView addSubview:hLine];
    
    // 滑杆
    // 滑杆的最大值会随着会员的总金币数变化
    CGFloat sliderX = LCFloat(60);
    self.slider = [[UISlider alloc] initWithFrame:CGRectMake(sliderX, kShowHeight + LCFloat(40), kScreenBounds.size.width - sliderX * 2, LCFloat(30))];
    self.slider.minimumValue = 1.0;
    self.slider.maximumValue = self.total;
    self.slider.minimumTrackTintColor = [UIColor blackColor];
    [self.slider addTarget:self action:@selector(didSlideWithSlider:) forControlEvents:UIControlEventValueChanged];
    [putView addSubview:self.slider];
    
    UILabel *changeLabel = [[UILabel alloc] init];
    changeLabel.text = @"兑换后请到“我的背包”中使用兑换券";
    changeLabel.font = [UIFont systemFontOfCustomeSize:14];
    changeLabel.textColor = [UIColor grayColor];
    CGSize changeSize = [changeLabel.text sizeWithCalcFont:changeLabel.font];
    changeLabel.frame = CGRectMake((kScreenBounds.size.width - changeSize.width) / 2, CGRectGetMaxY(self.slider.frame) + LCFloat(10), changeSize.width, changeSize.height);
    [putView addSubview:changeLabel];
    
    // 加减按钮
    CGFloat btnWidth = LCFloat(35);
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addButton.frame = CGRectMake(CGRectGetMaxX(self.slider.frame) + (sliderX - btnWidth) / 2, CGRectGetMinY(self.slider.frame) + (CGRectGetHeight(self.slider.frame) - btnWidth) / 2, btnWidth, btnWidth);
    [addButton setImage:[UIImage imageNamed:@"icon_snatch_add"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(didClickAddButton) forControlEvents:UIControlEventTouchUpInside];
    [putView addSubview:addButton];
    
    UIButton *reduceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    reduceButton.frame = CGRectMake((sliderX - btnWidth) / 2, CGRectGetMinY(addButton.frame), btnWidth, btnWidth);
    [reduceButton setImage:[UIImage imageNamed:@"icon_snatch_reduce"] forState:UIControlStateNormal];
    [reduceButton addTarget:self action:@selector(didClickReduceButton) forControlEvents:UIControlEventTouchUpInside];
    [putView addSubview:reduceButton];
    
    // 确认投入
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureButton setTitle:@"立即兑换" forState:UIControlStateNormal];
    sureButton.frame = CGRectMake(0, kShowHeight + kSelectHeight, kScreenBounds.size.width, kButtonHeight);
    [sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureButton addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
    [putView addSubview:sureButton];
    self.sureButton = sureButton;
    [self buttonEnable];
    
    [self showWithAnimationComplication:NULL];
}

- (void)buttonEnable {
    self.sureButton.backgroundColor = [UIColor blackColor];
    self.sureButton.enabled = YES;
}

- (void)buttonUnEnable {
    self.sureButton.backgroundColor = [UIColor lightGrayColor];
    self.sureButton.enabled = NO;
}

- (void)submitWithGoldCount:(void (^)(NSInteger))submitBlock {
    if (submitBlock) {
        self.goldBlcok = submitBlock;
    }
}

- (void)updateFrames {
    CGSize goldSize = [self.bambooLabel.text sizeWithCalcFont:self.bambooLabel.font];
    CGSize scaleSize = [self.numLabel.text sizeWithCalcFont:self.numLabel.font];
    
    self.bambooLabel.frame = CGRectMake(kScreenBounds.size.width / 2 - LCFloat(20) - goldSize.width, (kShowHeight - goldSize.height) / 2, goldSize.width, goldSize.height);
    self.numLabel.frame = CGRectMake(kScreenBounds.size.width / 2 + LCFloat(12), (kShowHeight - scaleSize.height) / 2, scaleSize.width, scaleSize.height);
}

- (void)updateGold {
    //当前 兑换的数量
    self.numLabel.text = [NSString stringWithFormat:@"%ld",(NSInteger)self.slider.value];
    self.bambooLabel.text = [NSString stringWithFormat:@"%ld",(long)(self.slider.value  * self.model.discountPrice)];
}

#pragma mark -- set

- (void)setTotal:(NSInteger)total {
    _total = total;
    self.slider.maximumValue = total;
}

- (void)setModel:(PDStoreActivityItem *)model {
    _model = model;
    
    self.numLabel.text = @"1";
    self.slider.minimumValue = 1;
    self.bambooLabel.text = [NSString stringWithFormat:@"%ld",(long)model.discountPrice];
    
    [self updateFrames];
}

#pragma mark -- 控件方法

- (void)didClickAddButton {
    self.slider.value++;
    [self.slider setValue:self.slider.value animated:YES];
    
    [self updateGold];
    [self updateFrames];
    
    if (self.slider.value > 0) {
        [self buttonEnable];
    }
}

- (void)didClickReduceButton {
    self.slider.value--;
    [self.slider setValue:self.slider.value animated:YES];
    
    [self updateGold];
    [self updateFrames];
    
    if (self.slider.value == 0) {
        [self buttonUnEnable];
    } else {
        [self buttonEnable];
    }
}

- (void)didClickButton {
    self.goldBlcok((NSInteger)self.slider.value);
    [self dismissWithAnimationComplication:NULL];
}

- (void)didSlideWithSlider:(UISlider *)slider {
    [self updateGold];
    [self updateFrames];
    
    if (slider.value == 0) {
        [self buttonUnEnable];
    } else {
        [self buttonEnable];
    }
}


@end
