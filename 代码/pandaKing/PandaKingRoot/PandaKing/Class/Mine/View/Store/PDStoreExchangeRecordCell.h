//
//  PDStoreExchangeRecordCell.h
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDStoreExchangeRecordItem.h"

/// 兑换记录cell
@interface PDStoreExchangeRecordCell : UITableViewCell
@property (nonatomic, strong) PDStoreExchangeRecordItem *model;

+ (CGFloat)cellHeight;
@end
