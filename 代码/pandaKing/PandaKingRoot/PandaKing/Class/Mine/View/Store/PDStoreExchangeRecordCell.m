//
//  PDStoreExchangeRecordCell.m
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDStoreExchangeRecordCell.h"

CGFloat const kSpace = 6;

@interface PDStoreExchangeRecordCell ()
@property (nonatomic, strong) PDImageView *avatarImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *numLabel;
@end

@implementation PDStoreExchangeRecordCell

+ (CGFloat)cellHeight {
    return LCFloat(50) + kSpace * 2;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self cellSetting];
    return self;
}

- (void)cellSetting {
    CGFloat avatarWidth = LCFloat(50);
    self.avatarImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, kSpace, avatarWidth, avatarWidth)];
    self.avatarImageView.layer.cornerRadius = avatarWidth / 2;
    self.avatarImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.avatarImageView];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = c2;
    [self.contentView addSubview:self.nameLabel];
    
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.dateLabel.textColor = [UIColor grayColor];
    [self.contentView addSubview:self.dateLabel];
    
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = self.dateLabel.font;
    self.numLabel.textColor = self.dateLabel.textColor;
    [self.contentView addSubview:self.numLabel];
}

- (void)setModel:(PDStoreExchangeRecordItem *)model {
    _model = model;
    
    self.nameLabel.text = model.member.nickname;
    self.dateLabel.text = [PDCenterTool dateFromTimestamp:model.redeemTime];
    self.numLabel.text = [NSString stringWithFormat:@" - %ld套",(long)model.quantity];
    [self.avatarImageView uploadImageWithAvatarURL:model.member.avatar placeholder:nil callback:NULL];
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - CGRectGetWidth(self.avatarImageView.frame) - LCFloat(12), [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGSize numSize = [self.numLabel.text sizeWithCalcFont:self.numLabel.font];
    
    CGFloat nameY = ([PDStoreExchangeRecordCell cellHeight] - nameSize.height - LCFloat(5) - dateSize.height) / 2;
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImageView.frame) + LCFloat(12), nameY, nameSize.width, nameSize.height);
    self.dateLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + LCFloat(5), dateSize.width, dateSize.height);
    self.numLabel.frame = CGRectMake(CGRectGetMaxX(self.dateLabel.frame), CGRectGetMinY(self.dateLabel.frame), numSize.width, numSize.height);
}

@end
