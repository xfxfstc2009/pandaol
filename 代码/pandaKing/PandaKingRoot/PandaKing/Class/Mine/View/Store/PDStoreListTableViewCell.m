//
//  PDStoreListTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDStoreListTableViewCell.h"

@interface PDStoreListTableViewCell ()
@property (nonatomic, strong) PDImageView *backgroundImageView;
@property (nonatomic, strong) UIView *descBar;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, assign) CGFloat descBarHeight;
@property (nonatomic, strong) UIImageView *iconImageView; // 价格小图标
@end

@implementation PDStoreListTableViewCell

+ (CGFloat)cellHeight {
    return 140;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    [self cellSetting];
    return self;
}

- (void)cellSetting {
    self.backgroundColor = [UIColor clearColor];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
   
    // background
    self.backgroundImageView = [[PDImageView alloc] initWithFrame:CGRectMake(storeListCellUnitHSpaceHeight * 2, storeListCellUnitVSpaceHeight, kScreenBounds.size.width - storeListCellUnitHSpaceHeight * 4, [PDStoreListTableViewCell cellHeight] - storeListCellUnitVSpaceHeight * 2)];
    self.backgroundImageView.userInteractionEnabled = YES;
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.backgroundImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.backgroundImageView];
    
    [self descBarSetting];
}

/** 名字描述➕价格*/
- (void)descBarSetting {
    _descBarHeight = 30;
    
    self.descBar = [[UIView alloc] initWithFrame:CGRectMake(10, CGRectGetHeight(self.backgroundImageView.frame) - _descBarHeight, CGRectGetWidth(self.backgroundImageView.frame) - 20, _descBarHeight)];
    self.descBar.backgroundColor = [UIColor colorWithWhite:1 alpha:0.8];
    [self.backgroundImageView addSubview:self.descBar];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = c3;
    [self.descBar addSubview:self.nameLabel];
    
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.priceLabel.textColor = c2;
    [self.descBar addSubview:self.priceLabel];
    
    self.iconImageView = [[UIImageView alloc] init];
    self.iconImageView.image = [UIImage imageNamed:@"icon_store_list_bamboo"];
    [self.descBar addSubview:self.iconImageView];
}

- (void)setModel:(PDStoreActivityItem *)model {
    _model = model;
    
    self.nameLabel.text = model.commodity.name;
    self.priceLabel.text = [NSString stringWithFormat:@"%ld",(long)model.discountPrice];
    [self.backgroundImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithRisqueUrl:model.commodity.coverPic] placeholder:nil callback:NULL];
    
    CGSize priceSize = [self.priceLabel.text sizeWithCalcFont:self.priceLabel.font];
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGRectGetWidth(self.descBar.frame) - 6 * 2 - priceSize.width - 12, [NSString contentofHeightWithFont:self.nameLabel.font])];
    
    self.nameLabel.frame = CGRectMake(6, (_descBarHeight - nameSize.height) / 2, nameSize.width, nameSize.height);
    self.priceLabel.frame = CGRectMake(CGRectGetWidth(self.descBar.frame) - 6 - priceSize.width, (_descBarHeight - priceSize.height) / 2, priceSize.width, priceSize.height);
    self.iconImageView.frame = CGRectMake(CGRectGetMinX(self.priceLabel.frame) - 1 - 10, (_descBarHeight - 10) / 2, 10, 10);
}

@end
