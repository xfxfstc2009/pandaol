//
//  PDStoreSheetView.h
//  PandaKing
//
//  Created by Cranz on 16/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSelectedSheetView.h"
#import "PDStoreActivityItem.h"

@interface PDStoreSheetView : PDSelectedSheetView
@property (nonatomic, strong) PDStoreActivityItem *model;
@property (nonatomic, assign) NSInteger total; // 总资产，slider的最大值
- (void)submitWithGoldCount:(void(^)(NSInteger count))submitBlock;
@end
