//
//  PDStoreRuleCell.m
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDStoreRuleCell.h"

#define KContentFont    [UIFont systemFontOfCustomeSize:13]
#define kSpace 10
@interface PDStoreRuleCell ()
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) NSMutableParagraphStyle *paragraph;
@end

@implementation PDStoreRuleCell

- (NSMutableParagraphStyle *)paragraph {
    if (!_paragraph) {
        _paragraph = [[NSMutableParagraphStyle alloc] init];
        CGSize size = [@"1." sizeWithCalcFont:self.contentLabel.font];
        _paragraph.headIndent = size.width;
    }
    return _paragraph;
}

+ (CGFloat)cellHeightWithContent:(NSString *)content {
    CGSize size = [content sizeWithCalcFont:KContentFont constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_leftMaxtter * 2, CGFLOAT_MAX)];
    return size.height + kSpace * 2;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    
    [self cellSetting];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

- (void)cellSetting {
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font = KContentFont;
    self.contentLabel.textColor = [UIColor grayColor];
    self.contentLabel.numberOfLines = 0;
    [self.contentView addSubview:self.contentLabel];
}

- (void)setContent:(NSString *)content {
    _content = content;
    
    self.contentLabel.text = content;
    
    CGSize textSize = [content sizeWithCalcFont:KContentFont constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_leftMaxtter * 2, CGFLOAT_MAX)];
    self.contentLabel.frame = CGRectMake(kTableViewSectionHeader_leftMaxtter, kSpace, textSize.width, textSize.height);
    
    NSAttributedString *textAtt = [[NSAttributedString alloc] initWithString:self.contentLabel.text attributes:@{NSParagraphStyleAttributeName: self.paragraph}];
    self.contentLabel.attributedText = textAtt;
}

@end
