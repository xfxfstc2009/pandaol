//
//  PDStoreProductDescCell.h
//  PandaKing
//
//  Created by Cranz on 17/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDStoreActivityItem.h"

/// 限购  正品  上新
@interface PDStoreProductDescCell : UITableViewCell
@property (nonatomic, strong) PDStoreActivityItem *model;

+ (CGFloat)cellHeight;
@end
