//
//  PDSelectedSheetView.h
//  PandaKing
//
//  Created by Cranz on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kTitleBarheight 35
@interface PDSelectedSheetView : UIView
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong) UIView *contentBar;
@property (nonatomic, strong) UIView *putView;
@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, assign) BOOL canTouchedCancle;    /**< 点击空白地方能否消失，默认YES*/
@property (nonatomic, assign) BOOL isShowing; /**< 是否已经弹起*/
/// 显示，隐藏 可以控制添加回调
- (void)showWithAnimationComplication:(void(^)())complication;
- (void)dismissWithAnimationComplication:(void(^)())complication;
/// 显示，隐藏 可以控制是否动画
- (void)showWithAnimated:(BOOL)animated;
- (void)dismissWithAnimated:(BOOL)animated;

/// 当消失时回出发回调
- (void)dismissWithActionBlock:(void(^)())complication;

@end
