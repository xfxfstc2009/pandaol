//
//  PDTopUpTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterTopUpItem.h"

/// 充值页面的单元格
@protocol PDTopUpTableViewCellDelegate;
@interface PDTopUpTableViewCell : UITableViewCell
@property (nonatomic, strong) PDCenterTopUpItem *item;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak)   id<PDTopUpTableViewCellDelegate> delegate;

+ (CGFloat)cellHeight;
@end

@protocol PDTopUpTableViewCellDelegate <NSObject>

- (void)topUpCell:(PDTopUpTableViewCell *)topUpCell didClickExChangeButtonAtIndexPath:(NSIndexPath *)indexPath;

@end