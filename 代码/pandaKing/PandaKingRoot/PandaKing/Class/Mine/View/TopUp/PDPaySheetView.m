//
//  PDPaySheetView.m
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPaySheetView.h"

#define kCellHeight LCFloat(50)
#define kButtonHeight 49

typedef void(^selectedBlock)(NSInteger type);
@interface PDPaySheetView ()
@property (nonatomic, strong) UIButton *selectedButton;
@property (nonatomic, copy)   selectedBlock selectedBlock;
@end

@implementation PDPaySheetView

+ (id)pay {
    return [[self alloc] init];
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    //
    UIView *putView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kCellHeight * 2 + kButtonHeight)];
    putView.backgroundColor = [UIColor whiteColor];
    self.putView = putView;
    self.title = @"请选择支付方式";
    
    // 支付方式
    UIView *aliPayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kCellHeight)];
    aliPayView.backgroundColor = [UIColor whiteColor];
    [putView addSubview:aliPayView];
    CGFloat imageHeight = LCFloat(36);
    
    UIImageView *alipayImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, (kCellHeight - imageHeight) / 2, imageHeight, imageHeight)];
    alipayImageView.image = [UIImage imageNamed:@"icon_topup_alipay"];
    [aliPayView addSubview:alipayImageView];
    
    UILabel *alipayLabel = [[UILabel alloc] init];
    alipayLabel.text = @"支付宝支付";
    alipayLabel.font = [UIFont systemFontOfCustomeSize:14];
    alipayLabel.textColor = [UIColor blackColor];
    CGSize alipaySize = [alipayLabel.text sizeWithCalcFont:alipayLabel.font];
    alipayLabel.frame = CGRectMake(CGRectGetMaxX(alipayImageView.frame) + LCFloat(10), (kCellHeight - alipaySize.height) / 2, alipaySize.width, alipaySize.height);
    [aliPayView addSubview:alipayLabel];
    
    CGFloat payButtonHeight = LCFloat(40);
    UIButton *alipayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    alipayButton.tag = 1;
    [alipayButton setFrame:CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - payButtonHeight, (kCellHeight - payButtonHeight) / 2, payButtonHeight, payButtonHeight)];
    [alipayButton setImage:[UIImage imageNamed:@"icon_pay_unselected"] forState:UIControlStateNormal];
    [alipayButton setImage:[UIImage imageNamed:@"icon_pay_selected"] forState:UIControlStateSelected];
    alipayButton.selected = YES;
    [alipayButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    [aliPayView addSubview:alipayButton];
    self.selectedButton = alipayButton;
    
    UIView *payline = [[UIView alloc] initWithFrame:CGRectMake(0, kCellHeight - .6, kScreenBounds.size.width, .6)];
    payline.backgroundColor = [UIColor lightGrayColor];
    [aliPayView addSubview:payline];
    
    
    // 微信支付
    UIView *wxpayView = [[UIView alloc] initWithFrame:CGRectMake(0, kCellHeight, kScreenBounds.size.width, kCellHeight)];
    wxpayView.backgroundColor = [UIColor whiteColor];
    [putView addSubview:wxpayView];
    
    UIImageView *wxpayImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, (kCellHeight - imageHeight) / 2, imageHeight, imageHeight)];
    wxpayImageView.image = [UIImage imageNamed:@"icon_topup_wxpay"];
    [wxpayView addSubview:wxpayImageView];
    
    UILabel *wxLabel = [[UILabel alloc] init];
    wxLabel.font = [UIFont systemFontOfCustomeSize:14];
    wxLabel.text = @"微信支付";
    CGSize wxSize = [wxLabel.text sizeWithCalcFont:wxLabel.font];
    wxLabel.frame = CGRectMake(CGRectGetMaxX(wxpayImageView.frame) + LCFloat(10), (kCellHeight - wxSize.height) / 2, wxSize.width, wxSize.height);
    [wxpayView addSubview:wxLabel];
    
    UIButton *wxButton = [UIButton buttonWithType:UIButtonTypeCustom];
    wxButton.tag = 2;
    [wxButton setFrame:CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - payButtonHeight, (kCellHeight - payButtonHeight) / 2, payButtonHeight, payButtonHeight)];
    [wxButton setImage:[UIImage imageNamed:@"icon_pay_unselected"] forState:UIControlStateNormal];
    [wxButton setImage:[UIImage imageNamed:@"icon_pay_selected"] forState:UIControlStateSelected];
    [wxButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    [wxpayView addSubview:wxButton];
    
    // 确定支付
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureButton setFrame:CGRectMake(0, 2 * kCellHeight, kScreenBounds.size.width, kButtonHeight)];
    [sureButton setTitle:@"确定支付" forState:UIControlStateNormal];
    sureButton.backgroundColor = c2;
    [sureButton addTarget:self action:@selector(didClickSureButton) forControlEvents:UIControlEventTouchUpInside];
    [putView addSubview:sureButton];
    
    [self showWithAnimationComplication:NULL];
}

- (void)payWithSelectedType:(void (^)(NSInteger))selectedBlock {
    if (selectedBlock) {
        self.selectedBlock = selectedBlock;
    }
}

- (void)didClickButton:(UIButton *)button {
    if (button != self.selectedButton) {
        self.selectedButton.selected = NO;
        button.selected = YES;
        self.selectedButton = button;
    }
}

- (void)didClickSureButton {
    self.selectedBlock(self.selectedButton.tag);
    [self dismissWithAnimationComplication:NULL];
}


@end
