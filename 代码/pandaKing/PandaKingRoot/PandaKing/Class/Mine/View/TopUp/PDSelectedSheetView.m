//
//  PDSelectedSheetView.m
//  PandaKing
//
//  Created by Cranz on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSelectedSheetView.h"

typedef void(^PDSelectedBlock)();
@interface PDSelectedSheetView ()

@property (nonatomic, strong) UIView *titleBar;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, assign) CGSize size;

@property (nonatomic, copy) PDSelectedBlock dismissBlock;
@end

@implementation PDSelectedSheetView

- (instancetype)init {
    self = [super initWithFrame:kScreenBounds];
    if (self) {
        [self __basicSetting];
        [self __pageSetting];
    }
    return self;
}

- (void)__basicSetting {
    _duration = .35;
    _canTouchedCancle = YES;
}

- (void)__pageSetting {
    self.backgroundColor = [UIColor clearColor];
    _size = self.frame.size;
    
    // 添加手势
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickBgView)];
    [self addGestureRecognizer:tap];
    
    // contentBar
    _contentBar = [[UIView alloc] init];
    _contentBar.backgroundColor = [UIColor clearColor];
    [self addSubview:_contentBar];
    
    // titleBar
    _titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _size.width, kTitleBarheight)];
    _titleBar.backgroundColor = c2;
    [_contentBar addSubview:_titleBar];
    
    // title label
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    _titleLabel.textColor = [UIColor whiteColor];
    [_titleBar addSubview:_titleLabel];
    
    // cancle button
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancleButton.frame = CGRectMake(_size.width - kTableViewSectionHeader_left - kTitleBarheight, 0, kTitleBarheight, kTitleBarheight);
    [cancleButton setImage:[UIImage imageNamed:@"icon_pay_cancle"] forState:UIControlStateNormal];
    [cancleButton addTarget:self action:@selector(didClickCancle) forControlEvents:UIControlEventTouchUpInside];
    [_titleBar addSubview:cancleButton];
}

- (void)addToWindow {
    NSEnumerator *windowEnnumtor = [UIApplication sharedApplication].windows.reverseObjectEnumerator;
    for (UIWindow *window in windowEnnumtor) {
        BOOL isOnMainScreen = window.screen == [UIScreen mainScreen];
        BOOL isVisible      = !window.hidden && window.alpha > 0;
        BOOL isLevelNormal  = window.windowLevel == UIWindowLevelNormal;
        
        if (isOnMainScreen && isVisible && isLevelNormal) {
            [window addSubview:self];
        }
    }
}

+ (CGFloat)titleBarHeight {
    return 35;
}

#pragma mark - set

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.titleLabel.text = title;
    CGSize titleSize = [title sizeWithCalcFont:self.titleLabel.font];
    self.titleLabel.frame = CGRectMake(kTableViewSectionHeader_left, (kTitleBarheight - titleSize.height) / 2, titleSize.width, titleSize.height);
}

- (void)setPutView:(UIView *)putView {
    _putView = putView;
    
    [_contentBar addSubview:putView];
    
    CGSize putSize = self.putView.frame.size;
    CGFloat y = _size.height - (kTitleBarheight + putSize.height);
    
    self.putView.frame = CGRectMake(0, CGRectGetMaxY(self.titleBar.frame), putSize.width, putSize.height);
    self.contentBar.frame = CGRectMake(0, _size.height, _size.width, _size.height - y);
}

#pragma mark - 控件方法

- (void)didClickCancle {
    [self dismissWithAnimationComplication:NULL];
}

- (void)didClickBgView {
    if (_canTouchedCancle) {
        [self dismissWithAnimationComplication:NULL];
    }
}

#pragma mark - Public

- (void)showWithAnimationComplication:(void (^)())complication {
    [self addToWindow];
    
    self.isShowing = YES;
    
    [UIView animateWithDuration:_duration animations:^{
        self.contentBar.frame = CGRectOffset(self.contentBar.frame, 0, -CGRectGetHeight(self.contentBar.frame));
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.65];
    } completion:^(BOOL finished) {
        if (complication) {
            complication();
        }
    }];
}

- (void)dismissWithAnimationComplication:(void (^)())complication {
    self.isShowing = NO;
    
    [UIView animateWithDuration:_duration animations:^{
        self.contentBar.frame = CGRectOffset(self.contentBar.frame, 0, CGRectGetHeight(self.contentBar.frame));
        self.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        if (complication) {
            complication();
        }
        if (self.dismissBlock) {
            self.dismissBlock();
        }
    }];
}

- (void)showWithAnimated:(BOOL)animated {
    [self addToWindow];
    
    self.isShowing = YES;
    
    [UIView animateWithDuration:animated? _duration : 0 animations:^{
        self.contentBar.frame = CGRectOffset(self.contentBar.frame, 0, -CGRectGetHeight(self.contentBar.frame));
        self.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.65];
    }];
}

- (void)dismissWithAnimated:(BOOL)animated {
    self.isShowing = NO;
    
    [UIView animateWithDuration:animated? _duration : 0 animations:^{
        self.contentBar.frame = CGRectOffset(self.contentBar.frame, 0, CGRectGetHeight(self.contentBar.frame));
        self.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        if (self.dismissBlock) {
            self.dismissBlock();
        }
    }];
}

- (void)dismissWithActionBlock:(void (^)())complication {
    if (complication) {
        self.dismissBlock = complication;
    }
}

@end
