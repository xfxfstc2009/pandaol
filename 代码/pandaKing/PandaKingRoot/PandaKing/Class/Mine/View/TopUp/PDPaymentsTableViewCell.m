//
//  PDPaymentsTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPaymentsTableViewCell.h"

static CGFloat const descFont = 14.;           /**< 内容的字体大小*/
static CGFloat const spaceVlineRight = 8;     /**< 竖线右边距离icon的距离*/
static CGFloat const spaceDateLeft = 15;       /**< 日期label的左右间隙*/
static CGFloat const iconWidth = 22.;
static CGFloat const descWidth = 180.;          /**< desc信息的约束宽度*/
//static CGFloat const labelSpace = 3; // 事件和金币的间隙
#define kDescTop ((kRowHeight - LCFloat(iconWidth))/2)
#define kRowHeight 44.

@interface PDPaymentsTableViewCell ()
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIView *vLine;
@property (nonatomic, strong) UIView *hLine;
@property (nonatomic, strong) UIImageView *dateBlockImageView;  /**< 竖线上的那块金黄色*/
//@property (nonatomic, strong) UIImageView *iconImageView;       /**< 事件图片*/
@property (nonatomic, strong) UILabel *descLabel;               /**< 事件描述*/
@property (nonatomic, strong) UILabel *numLabel;                /**< 收支数量*/
@property (nonatomic, strong) UIImageView *tagImageView;        /**< 竹子 | 金币*/
@end

@implementation PDPaymentsTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)pageSetting {
    // date
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.hidden = YES;
    self.dateLabel.textColor = c26;
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:14];
    [self.contentView addSubview:self.dateLabel];
    
    // 竖线
    self.vLine = [[UIView alloc] init];
    self.vLine.backgroundColor = c27;
    [self.contentView addSubview:self.vLine];
    
    // 金块
    self.dateBlockImageView = [[UIImageView alloc] init];
    self.dateBlockImageView.hidden = YES;
    self.dateBlockImageView.backgroundColor = self.dateLabel.textColor;
    [self.contentView addSubview:self.dateBlockImageView];
    
    // 事件icon
//    self.iconImageView = [[UIImageView alloc] init];
//    [self.contentView addSubview:self.iconImageView];
    
    // 事件
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:descFont];
    self.descLabel.numberOfLines = 0;
    self.descLabel.textColor = c3;
    [self.contentView addSubview:self.descLabel];
    
    // －100
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.numLabel.textColor = c26;
    [self.contentView addSubview:self.numLabel];
    
    // 竹子图标
    self.tagImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.tagImageView];
    
    // 横线
    self.hLine = [[UIView alloc] init];
    self.hLine.backgroundColor = self.vLine.backgroundColor;
    [self.contentView addSubview:self.hLine];
}

+ (CGFloat)cellHeightWithDesc:(PDMemberTopUpDetail *)detailModel {
    CGSize size = [detailModel.reason sizeWithCalcFont:[UIFont systemFontOfCustomeSize:descFont] constrainedToSize:CGSizeMake(LCFloat(descWidth), CGFLOAT_MAX)];
    CGFloat space = (kRowHeight - [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:descFont]]);
    CGFloat rowHeight = space + size.height;
    return rowHeight;
}

- (void)setDetailModel:(PDMemberTopUpDetail *)detailModel {
    _detailModel = detailModel;
    
    self.dateLabel.text = [PDCenterTool dateWithFormat:@"MM-dd" fromTimestamp:detailModel.logTime];
    
    /*
    if ([detailModel.source isEqualToString:@"buyGold"]) { //购买金币，没有购买竹子
        if (detailModel.deltaFee >= 0) {
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_add"];
        } else {
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_reduce"];
        }
    } else {
        if ([detailModel.source isEqualToString:@"invite"]) { // 邀请
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_invite"];
        } else if ([detailModel.source isEqualToString:@"invited"]) {// 被邀请
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_invited"];
        } else if ([detailModel.source isEqualToString:@"usepandaking"]) {// 打开一次App
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_usepandaking"];
        } else if ([detailModel.source isEqualToString:@"winmatches"]) { // 打比赛
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_winmatches"];
        } else if ([detailModel.source isEqualToString:@"goldExchange"]) { // 金币兑换
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_add"];
        } else if ([detailModel.source isEqualToString:@"bambooexchange"]) { // 竹子兑换
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_add"];
        } else if ([detailModel.source isEqualToString:@"bambooRedeemAct"]) { //竹子商城
            self.iconImageView.image = [UIImage imageNamed:@"icon_payment_bambooRedeemAct"];
        }
        else if ([detailModel.source isEqualToString:@"taskAward"]) { // 任务
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_task"];
        } else if ([detailModel.source isEqualToString:@"invitedMemFirstActivateLOLGameUser"]) {//任务:我邀请的-好友-如果绑定召唤师且通过认证
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_firstActivate"];
        } else if ([detailModel.source isEqualToString:@"treasure"]) { // 夺宝
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_treasure"];
        } else if ([detailModel.source isEqualToString:@"matchguess"]) { // 赛事竞猜
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_matchguess"];
        } else if ([detailModel.source isEqualToString:@"anchorguess"]) { // 主播竞猜
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_anchorguess"];
        } else if ([detailModel.source isEqualToString:@"lolchampionguess"]) { // 英雄猜
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_lolchampionguess"];
        } else if ([detailModel.source isEqualToString:@"matchguesscancel"]) { // 赛事竞猜取消
            
        } else if ([detailModel.source isEqualToString:@"anchorguesscancel"]) { // 主播竞猜取消
            
        } else if ([detailModel.source isEqualToString:@"lolchampionguesscancel"]) { // 英雄猜取消
            
        } else if ([detailModel.source isEqualToString:@"doneorder"]) { // 订单兑换（发货）
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_doneorder"];
        } else if ([detailModel.source isEqualToString:@"taskAward"]) {// 任务领奖
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_taskAward"];
        } else if ([detailModel.source isEqualToString:@"memberTransfersGoldToMember"]) {// 会员金币转账
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_memberTransfersGoldToMember"];
        } else if ([detailModel.source isEqualToString:@"memberTop"]) { // 排行榜
            self.iconImageView.image = [UIImage imageNamed:@"icon_payments_memberTop"];
        }
    }
    */
    
    
    self.descLabel.text = detailModel.reason;
    self.numLabel.text = (detailModel.deltaFee < 0)? [NSString stringWithFormat:@"%ld",(long)detailModel.deltaFee] : [NSString stringWithFormat:@"+%ld",(long)detailModel.deltaFee];
    
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(LCFloat(descWidth), CGFLOAT_MAX)];
    CGSize numSize = [self.numLabel.text sizeWithCalcFont:self.numLabel.font];
    CGFloat space = (kRowHeight - [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:descFont]]) / 2;
    
    self.dateLabel.frame = CGRectMake(LCFloat(spaceDateLeft), (kRowHeight - dateSize.height) / 2, dateSize.width, dateSize.height);
    self.vLine.frame = CGRectMake(CGRectGetMaxX(self.dateLabel.frame) + LCFloat(spaceDateLeft), 0, 1, descSize.height + space * 2);
    self.dateBlockImageView.frame = CGRectMake(CGRectGetMinX(self.vLine.frame) - 2, space, 4, descSize.height);
//    self.iconImageView.frame = CGRectMake(CGRectGetMaxX(self.vLine.frame) + LCFloat(spaceVlineRight), (kRowHeight - LCFloat(iconWidth)) / 2, LCFloat(iconWidth), LCFloat(iconWidth));
    self.descLabel.frame = CGRectMake(CGRectGetMaxX(self.vLine.frame) + LCFloat(spaceVlineRight), space, descSize.width, descSize.height);
    self.tagImageView.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - LCFloat(iconWidth), (kRowHeight - LCFloat(iconWidth)) / 2, LCFloat(iconWidth), LCFloat(iconWidth));
    self.numLabel.frame = CGRectMake(CGRectGetMinX(self.tagImageView.frame) - numSize.width - LCFloat(2.), (kRowHeight - numSize.height) / 2, numSize.width, numSize.height);
    self.hLine.frame = CGRectMake(CGRectGetMinX(self.descLabel.frame), [PDPaymentsTableViewCell cellHeightWithDesc:detailModel] - 1, kScreenBounds.size.width - CGRectGetMinX(self.dateBlockImageView.frame), 1);
}

- (void)setIsShowDate:(BOOL)isShowDate {
    _isShowDate = isShowDate;
    
    self.dateLabel.hidden = !isShowDate;
    self.dateBlockImageView.hidden = !isShowDate;
}

- (void)setIsLastEvent:(BOOL)isLastEvent {
    _isLastEvent = isLastEvent;
    
    self.hLine.frame = CGRectMake(CGRectGetMaxX(self.vLine.frame), [PDPaymentsTableViewCell cellHeightWithDesc:self.detailModel] - .5, kScreenBounds.size.width - CGRectGetMaxX(self.vLine.frame), .5);
}

- (void)setTagImage:(UIImage *)tagImage {
    _tagImage = tagImage;
    
    self.tagImageView.image = tagImage;
}

@end
