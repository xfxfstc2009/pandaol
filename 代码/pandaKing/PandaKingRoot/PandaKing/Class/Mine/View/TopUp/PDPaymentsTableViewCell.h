//
//  PDPaymentsTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMemberTopUpDetail.h"

/// 收支记录的单元格
@interface PDPaymentsTableViewCell : UITableViewCell
@property (nonatomic, assign) BOOL isShowDate;  /**< 如果是section的第一行就显示*/
@property (nonatomic, assign) BOOL isLastEvent; /**< 如果是一天当中的最后一条数据，就闭合横线*/
@property (nonatomic, strong) PDMemberTopUpDetail *detailModel;   
@property (nonatomic, strong) UIImage *tagImage;

+ (CGFloat)cellHeightWithDesc:(PDMemberTopUpDetail *)PDMemberTopUpDetail;
@end
