//
//  PDPaySheetView.h
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSelectedSheetView.h"

@interface PDPaySheetView : PDSelectedSheetView
+ (id)pay;
/** 1：支付宝；2:微信支付*/
- (void)payWithSelectedType:(void(^)(NSInteger type))selectedBlock;
@end
