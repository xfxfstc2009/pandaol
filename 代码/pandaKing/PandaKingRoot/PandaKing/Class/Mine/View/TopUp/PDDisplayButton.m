//
//  PDDisplayButton.m
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDDisplayButton.h"

@implementation PDDisplayButton

- (void)layoutSubviews {
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    CGFloat space_label_label = 5;
    CGFloat space_image_label = 10;
    CGFloat space_top = 18;
    CGFloat space_bottom = 12;
    
    CGSize titleSize = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}];
    self.titleLabel.frame = CGRectMake(((width - titleSize.width) > 0 ?(width - titleSize.width) : 0) / 2, height - space_bottom - titleSize.height, titleSize.width <= width ?titleSize.width : width, titleSize.height);
//    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    // 中间文本
    CGSize textSize = [self.textLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}];
    self.textLabel.frame = CGRectMake(0, height - space_bottom - titleSize.height - textSize.height - space_label_label, width, textSize.height);
    [self addSubview:self.textLabel];
    
    // 留给图片的距离
    CGFloat dis = height - titleSize.height - textSize.height - space_bottom - space_top - space_image_label - space_label_label;
    CGFloat imgeWidth = dis, imageHeight = imgeWidth;
    self.imageView.frame = CGRectMake((width - imgeWidth) / 2, space_top, imgeWidth, imageHeight);
}

- (UILabel *)textLabel {
    if (!_textLabel) {
        _textLabel = [[UILabel alloc] init];
        _textLabel.textAlignment = NSTextAlignmentCenter;
        _textLabel.font = [[UIFont systemFontOfCustomeSize:16] boldFont];
        _textLabel.textColor = self.titleLabel.textColor;
    }
    return _textLabel;
}

@end
