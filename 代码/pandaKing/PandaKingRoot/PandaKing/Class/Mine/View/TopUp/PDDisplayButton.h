//
//  PDDisplayButton.h
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDDisplayButton : UIButton
@property (nonatomic, strong) UILabel *textLabel; /**< 显示在中间行*/
@end
