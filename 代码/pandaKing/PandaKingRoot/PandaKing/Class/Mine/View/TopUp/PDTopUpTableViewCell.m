//
//  PDTopUpTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTopUpTableViewCell.h"

@interface PDTopUpTableViewCell ()
@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UILabel *prefixLabel; /**< 数字后缀*/
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UILabel *presentLabel; // 充值满就送
@property (nonatomic, strong) UIButton *exchangeButton;

@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, assign) CGFloat rowWidth;
@end

@implementation PDTopUpTableViewCell

+ (CGFloat)cellHeight {
    return 55.;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    _rowHeight = [PDTopUpTableViewCell cellHeight];
    _rowWidth  = kScreenBounds.size.width;

    CGFloat imageWidth = 28.;
    CGFloat buttonWidth = 70., buttonHeight = 30.;
    
    self.iconView = [[UIImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_leftMaxtter, (_rowHeight - imageWidth) / 2, imageWidth, imageWidth)];
    [self.contentView addSubview:self.iconView];
    
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = [UIFont systemFontOfCustomeSize:18];
    self.numLabel.textColor = c2;
    [self.contentView addSubview:self.numLabel];
    
    self.prefixLabel = [[UILabel alloc] init];
    self.prefixLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.prefixLabel.text = @"金币";
    self.prefixLabel.textColor = self.numLabel.textColor;
    [self.contentView addSubview:self.prefixLabel];
    
    self.presentLabel = [[UILabel alloc] init];
    self.presentLabel.font = [UIFont systemFontOfCustomeSize:11];
    self.presentLabel.textColor = c26;
    [self.contentView addSubview:self.presentLabel];
    
    self.exchangeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.exchangeButton.frame = CGRectMake(_rowWidth - kTableViewSectionHeader_leftMaxtter - buttonWidth, (_rowHeight - buttonHeight) / 2, buttonWidth, buttonHeight);
    self.exchangeButton.backgroundColor = c2;
    self.exchangeButton.layer.cornerRadius = 4;
    self.exchangeButton.clipsToBounds = YES;
    [self.exchangeButton setTitleColor:c16 forState:UIControlStateHighlighted];
    [self.exchangeButton setTitleColor:c1 forState:UIControlStateNormal];
    self.exchangeButton.titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    [self.exchangeButton addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.exchangeButton];
}


- (void)setItem:(PDCenterTopUpItem *)item {
    _item = item;

    self.iconView.image = [UIImage imageNamed:@"icon_center_gold"];
    self.numLabel.text = [PDCenterTool separateStringWithString:[NSString stringWithFormat:@"%ld", (long)item.gold]];
    [self.exchangeButton setTitle:[NSString stringWithFormat:@"¥%@",item.cash] forState:UIControlStateNormal];
    
    if (item.goldAwardForPay > 0) {
        NSString *presentGold = [PDCenterTool separateStringWithString:[NSString stringWithFormat:@"%ld",(long)item.goldAwardForPay]];
         self.presentLabel.text = [NSString stringWithFormat:@"(充即送 %@ 金币)",presentGold];
    }
    
    CGSize numSize = [self.numLabel.text sizeWithCalcFont:self.numLabel.font];
    CGSize prefixSize = [self.prefixLabel.text sizeWithCalcFont:self.prefixLabel.font];
    CGSize presentSize = [self.presentLabel.text sizeWithCalcFont:self.presentLabel.font];
    
    // 金币label的y
    CGFloat y = (_rowHeight - numSize.height - presentSize.height) / 2;
    
    self.numLabel.frame = CGRectMake(CGRectGetMaxX(self.iconView.frame) + 6, y, numSize.width, numSize.height);
    self.prefixLabel.frame = CGRectMake(CGRectGetMaxX(self.numLabel.frame) + 3, (CGRectGetMaxY(self.numLabel.frame) - prefixSize.height), prefixSize.width, prefixSize.height);
    self.presentLabel.frame = CGRectMake(CGRectGetMinX(self.numLabel.frame), CGRectGetMaxY(self.numLabel.frame), presentSize.width, presentSize.height);
}

#pragma mark -- 代理方法

- (void)didClickButton {
    if ([self.delegate respondsToSelector:@selector(topUpCell:didClickExChangeButtonAtIndexPath:)]) {
        [self.delegate topUpCell:self didClickExChangeButtonAtIndexPath:self.indexPath];
    }
}

@end
