//
//  PDCenterSnatchTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSnatchItem.h"

/// 我的 夺宝详情cell
typedef NS_ENUM(NSUInteger, PDProductSnatchType) {
    PDProductSnatchTypeProcess,     /**< 进行中*/
    PDProductSnatchTypeEnd,         /**< 已结束*/
    PDProductSnatchTypeWin,         /**< 已中奖*/
};
@interface PDCenterSnatchTableViewCell : UITableViewCell
@property (nonatomic, strong)  PDSnatchItem *item;
@property (nonatomic, strong) NSIndexPath *indexPath;

// 这些控件会根据给定的type做出相应调整
@property (nonatomic, strong) UILabel *leftDescLabel;
@property (nonatomic, strong) UILabel *rightDesclabel;


+ (CGFloat)cellHeight;
- (instancetype)initWithType:(PDProductSnatchType)type reuseIdentifier:(NSString *)reuseIdentifier;
@end
