//
//  PDProductSnatchCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDFindSnatchItem.h"

/// 发现种的TA的夺宝cell
@protocol PDProductSnatchCellDelegate;
typedef NS_ENUM(NSUInteger, PDProductSnatchType) {
    PDProductSnatchTypeProcess,     /**< 进行中*/
    PDProductSnatchTypeEnd,         /**< 已结束*/
    PDProductSnatchTypeWin,         /**< 已中奖*/
};
@interface PDProductSnatchCell : UITableViewCell
@property (nonatomic, strong)  PDFindSnatchItem *item;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, weak)   id<PDProductSnatchCellDelegate> delegate;  /**< 在有按钮时需要用到这个代理*/

// 这些控件会根据给定的type做出相应调整
@property (nonatomic, strong) UILabel *leftDescLabel;
@property (nonatomic, strong) UILabel *centerDescLabel;
@property (nonatomic, strong) UILabel *rightDesclabel;
@property (nonatomic, strong) UIImageView *signImageView;    /**< 特殊标示图*/


+ (CGFloat)cellHeight;
- (instancetype)initWithType:(PDProductSnatchType)type reuseIdentifier:(NSString *)reuseIdentifier;
- (void)setMoreButtonHidden;
@end


@protocol PDProductSnatchCellDelegate <NSObject>
- (void)productSnatchCell:(PDProductSnatchCell *)cell didClickMoreButtonAtIndexPath:(NSIndexPath *)indexPath;
@end