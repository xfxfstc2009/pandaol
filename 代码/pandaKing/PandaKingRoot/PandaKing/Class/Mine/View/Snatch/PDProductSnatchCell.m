//
//  PDProductSnatchCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProductSnatchCell.h"

@interface PDProductSnatchCell ()
@property (nonatomic, assign) PDProductSnatchType type;     /**< 需要展示的cell的类型*/
@property (nonatomic, strong) PDImageView *productImageView;
@property (nonatomic, strong) UILabel *nameLabel;           /**< 商品名字*/
@property (nonatomic, strong) UILabel *numLabel;            /**< 参与期号*/
@property (nonatomic, strong) UILabel *lotteryDateLabel;    /**< 开奖日期*/
@property (nonatomic, strong) UIView *line;

// 红字主要信息
@property (nonatomic, strong) UILabel *leftInfoLabel;
@property (nonatomic, strong) UILabel *centerInfoLabel;
@property (nonatomic, strong) UILabel *rightInfoLabel;

@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, assign) CGFloat rowWidth;
@property (nonatomic, assign) CGFloat showBarHeight;        /**< 底部展示信息bar的高度*/
@property (nonatomic, assign) CGFloat infoBarHeight;        /**< 上部分商品信息bar的高度*/
@property (nonatomic, strong) UIFont *font16;
@property (nonatomic, assign) CGFloat space_label_label_top;    /**< 上空隙*/
@property (nonatomic, assign) CGFloat space_label_label_bottom; /**< 下空隙*/
@property (nonatomic, assign) CGFloat space_label_label;    /**< 底部label和label之间的空隙*/
@end

@implementation PDProductSnatchCell

+ (CGFloat)cellHeight {
    return 120.;
}

- (instancetype)initWithType:(PDProductSnatchType)type reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _type = type;
        _rowHeight = [PDProductSnatchCell cellHeight];
        _rowWidth = kScreenBounds.size.width;
        _showBarHeight = 40.;
        _infoBarHeight = _rowHeight - _showBarHeight;
        _font16 = [UIFont systemFontOfCustomeSize:14];
        _space_label_label_top = 3;
        _space_label_label_bottom = 10;
        _space_label_label = LCFloat(5);
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    CGFloat imageWidth = 55.;
    self.productImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, (_infoBarHeight - imageWidth) / 2, imageWidth, imageWidth)];
    [self.contentView addSubview:self.productImageView];
    
    // 商品名字
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [[UIFont systemFontOfCustomeSize:16] boldFont];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    // 参与期号
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.numLabel.textColor = c28;
    [self.contentView addSubview:self.numLabel];
    
    // 开奖日期
    self.lotteryDateLabel = [[UILabel alloc] init];
    self.lotteryDateLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.lotteryDateLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.lotteryDateLabel];
    
    // 分割线
    self.line = [[UIView alloc] init];
    self.line.backgroundColor = c27;
    self.line.frame = CGRectMake(0, _infoBarHeight - .5, _rowWidth, .5);
    [self.contentView addSubview:self.line];
    
    if (_type == PDProductSnatchTypeProcess) {
        [self progressViewSetting];
        self.leftDescLabel.text = @"夺宝池共有";
        self.centerDescLabel.text = @"我的中奖率";
    } else if (_type == PDProductSnatchTypeEnd) {
        [self endViewSetting];
        // TA的夺宝中会有幸运中奖图式
        self.signImageView = [self signImageViewSetting];
        self.leftDescLabel.text = @"获奖者";
        self.rightDesclabel.text = @"投入竹子";
    } else {
        [self winViewSetting];
        self.leftDescLabel.text = @"总投入";
        self.rightDesclabel.text = @"我的中奖率：";
    }
}

- (void)setMoreButtonHidden {
    self.moreButton.hidden = YES;
}

#pragma mark -- type view setting
- (void)progressViewSetting {
    // 左侧：夺宝池共有 1588
    self.leftDescLabel = [self leftDescLabelSetting];
    self.leftInfoLabel = [self leftInfoLabelSetting];
    
    // 中间：我的中奖率：15％
    self.centerDescLabel = [self centerDescLabelSetting];
    self.centerInfoLabel = [self centerInfoLabelSetting];

    // 右侧：追加
    self.moreButton = [self moreButtonSetting];
    
    // TA的夺宝中会有幸运中奖图式
    self.signImageView = [self signImageViewSetting];
}

- (UIImageView *)signImageViewSetting {
    CGFloat imageWidth = LCFloat(60);
    CGFloat imageHeight = LCFloat(50);
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(_rowWidth - kTableViewSectionHeader_left - imageWidth, CGRectGetMinX(self.productImageView.frame), imageWidth, imageHeight)];
    imageView.hidden = YES;
    imageView.image = [UIImage imageNamed:@"icon_snatch_wined"];
    [self.contentView addSubview:imageView];
    return imageView;
}

- (void)endViewSetting {
    self.leftDescLabel = [self leftDescLabelSetting];
    self.leftInfoLabel = [self leftInfoLabelSetting];
    
    self.rightDesclabel = [self rightDesclabelSetting];
    self.rightInfoLabel = [self righeInfoLabelSetting];
}

- (void)winViewSetting {
    self.leftDescLabel = [self leftDescLabelSetting];
    self.leftInfoLabel = [self leftInfoLabelSetting];
    
    self.rightDesclabel = [self rightDesclabelSetting];
    self.rightInfoLabel = [self righeInfoLabelSetting];
}

- (UILabel *)leftDescLabelSetting {
    UILabel *leftDescLabel = [[UILabel alloc] init];
    leftDescLabel.font = _font16;
    leftDescLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:leftDescLabel];
    return leftDescLabel;
}

- (UILabel *)leftInfoLabelSetting {
    UILabel *leftInfoLabel = [[UILabel alloc] init];
    leftInfoLabel.font = _font16;
    leftInfoLabel.textColor = [UIColor redColor];
    [self.contentView addSubview:leftInfoLabel];
    return leftInfoLabel;
}

- (UILabel *)centerDescLabelSetting {
    UILabel *centerDescLabel = [[UILabel alloc] init];
    centerDescLabel.font = _font16;
    centerDescLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:centerDescLabel];
    return centerDescLabel;
}

- (UILabel *)centerInfoLabelSetting {
    UILabel *centerInfoLabel = [[UILabel alloc] init];
    centerInfoLabel.font = _font16;
    centerInfoLabel.textColor = [UIColor redColor];
    [self.contentView addSubview:centerInfoLabel];
    return centerInfoLabel;
}

- (UIButton *)moreButtonSetting {
    CGFloat buttonWidth = 80.;
    CGFloat buttonHeight = 30.;
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(_rowWidth - kTableViewSectionHeader_left - buttonWidth, (_showBarHeight - buttonHeight) / 2 + _infoBarHeight, buttonWidth, buttonHeight);
    moreButton.layer.borderColor = c26.CGColor;
    moreButton.layer.borderWidth = 1;
    [moreButton setTitleColor:c26 forState:UIControlStateNormal];
    moreButton.titleLabel.font = _font16;
    [moreButton addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:moreButton];
    return moreButton;
}

- (UILabel *)rightDesclabelSetting {
    UILabel *rightDescLabel = [[UILabel alloc] init];
    rightDescLabel.font = _font16;
    rightDescLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:rightDescLabel];
    return rightDescLabel;
}

- (UILabel *)righeInfoLabelSetting {
    UILabel *rightInfoLabel = [[UILabel alloc] init];
    rightInfoLabel.font = _font16;
    rightInfoLabel.textColor = [UIColor redColor];
    [self.contentView addSubview:rightInfoLabel];
    return rightInfoLabel;
}


#pragma mark -- 假的
- (void)setItem:(PDFindSnatchItem *)item {
    _item = item;
    
    [self.productImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithRisqueUrl:item.image] placeholder:nil callback:NULL];
    self.nameLabel.text = item.name;
    self.numLabel.text = [NSString stringWithFormat:@"参与期号：%@",item.phase];
    self.lotteryDateLabel.text = [NSString stringWithFormat:@"开奖时间：%@", [PDCenterTool dateFromTimestamp:item.drawTime]];
    self.signImageView.hidden = !item.isWin;
    
    if (_type == PDProductSnatchTypeProcess) {
        self.leftInfoLabel.text = item.totalGold;
        self.centerInfoLabel.text = item.goldCount;
        
    } else if (_type == PDProductSnatchTypeEnd) {
        self.leftInfoLabel.text = item.totalGold;
        self.rightInfoLabel.text = item.goldCount;
        
    } else {
        self.leftInfoLabel.text = @"1599";
        self.rightInfoLabel.text = @"16.2％";
    }
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font];
    CGSize numSize = [self.numLabel.text sizeWithCalcFont:self.numLabel.font];
    CGSize lotterySize = [self.lotteryDateLabel.text sizeWithCalcFont:self.lotteryDateLabel.font];
    
    
    // 上部分
    CGFloat space = (_infoBarHeight - nameSize.height - _space_label_label_top - numSize.height - _space_label_label_bottom - lotterySize.height) / 2;
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + LCFloat(10), space, nameSize.width, nameSize.height);
    self.numLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + _space_label_label_top, numSize.width, numSize.height);
    self.lotteryDateLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.numLabel.frame) + _space_label_label_bottom, lotterySize.width, lotterySize.height);
    
    // 下部分
    
    CGSize leftInfoSize = [self.leftInfoLabel.text sizeWithCalcFont:self.leftInfoLabel.font];
    CGSize rightInfoSize = [self.rightInfoLabel.text sizeWithCalcFont:self.rightInfoLabel.font];
    CGSize rightDescSize = [self.rightDesclabel.text sizeWithCalcFont:self.rightDesclabel.font];
    CGSize centerDescSize = [self.centerDescLabel.text sizeWithCalcFont:self.centerDescLabel.font];
    CGSize centerInfoSize = [self.centerInfoLabel.text sizeWithCalcFont:self.centerInfoLabel.font];
    CGFloat center_x = (_rowWidth - centerInfoSize.width - _space_label_label - centerDescSize.width) / 2;
    CGSize leftDescSize = [self.leftDescLabel.text sizeWithCalcFont:self.leftDescLabel.font];
    
    self.leftDescLabel.frame = CGRectMake(kTableViewSectionHeader_left, (_showBarHeight - leftDescSize.height) / 2 + _infoBarHeight, leftDescSize.width, leftDescSize.height);
    self.leftInfoLabel.frame = CGRectMake(CGRectGetMaxX(self.leftDescLabel.frame) + _space_label_label, (_showBarHeight - leftInfoSize.height) / 2 + _infoBarHeight, leftInfoSize.width, leftInfoSize.height);
    self.rightInfoLabel.frame = CGRectMake(_rowWidth - kTableViewSectionHeader_left - rightInfoSize.width, (_showBarHeight - rightInfoSize.height) / 2 + _infoBarHeight, rightInfoSize.width, rightInfoSize.height);
    self.rightDesclabel.frame = CGRectMake(CGRectGetMinX(self.rightInfoLabel.frame) - _space_label_label - rightDescSize.width, (_showBarHeight - rightDescSize.height) / 2 + _infoBarHeight, rightDescSize.width, rightDescSize.height);
    self.centerDescLabel.frame = CGRectMake(center_x, (_showBarHeight - centerDescSize.height) / 2 + _infoBarHeight, centerDescSize.width, centerDescSize.height);
    self.centerInfoLabel.frame = CGRectMake(CGRectGetMaxX(self.centerDescLabel.frame) + _space_label_label, (_showBarHeight - centerInfoSize.height) / 2 + _infoBarHeight, centerInfoSize.width, centerInfoSize.height);
    
}

- (void)didClickButton {
    if ([self.delegate respondsToSelector:@selector(productSnatchCell:didClickMoreButtonAtIndexPath:)]) {
        [self.delegate productSnatchCell:self didClickMoreButtonAtIndexPath:self.indexPath];
    }
}

@end
