//
//  PDProductSnatchCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterSnatchTableViewCell.h"

@interface PDCenterSnatchTableViewCell ()
@property (nonatomic, assign) PDProductSnatchType type;     /**< 需要展示的cell的类型*/
@property (nonatomic, strong) PDImageView *productImageView;
@property (nonatomic, strong) UILabel *nameLabel;           /**< 商品名字*/
@property (nonatomic, strong) UILabel *numLabel;            /**< 参与期号*/
/**
 * 中奖用户label
 */
@property (nonatomic, strong) UILabel *winerLabel;
@property (nonatomic, strong) UILabel *lotteryDateLabel;    /**< 开奖日期*/
@property (nonatomic, strong) UIView *line;

@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, assign) CGFloat rowWidth;
@property (nonatomic, assign) CGFloat showBarHeight;        /**< 底部展示信息bar的高度*/
@property (nonatomic, assign) CGFloat infoBarHeight;        /**< 上部分商品信息bar的高度*/
@property (nonatomic, strong) UIFont *font16;
/**
 * 中奖率
 */
@property (nonatomic, strong) UILabel *oddsLabel;

@end

@implementation PDCenterSnatchTableViewCell

+ (CGFloat)cellHeight {
    return 120.;
}

- (instancetype)initWithType:(PDProductSnatchType)type reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _type = type;
        _rowHeight = [PDCenterSnatchTableViewCell cellHeight];
        _rowWidth = kScreenBounds.size.width;
        _showBarHeight = 36.;
        _infoBarHeight = _rowHeight - _showBarHeight;
        _font16 = [UIFont systemFontOfCustomeSize:14];
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    CGFloat imageWidth = 55.;
    self.productImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, (_infoBarHeight - imageWidth) / 2, imageWidth, imageWidth)];
    self.productImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.productImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.productImageView];
    
    // 商品名字
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:16];
    self.nameLabel.textColor = c2;
    [self.contentView addSubview:self.nameLabel];
    
    // 参与期号
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.numLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.numLabel];

    // 中奖用户
    self.winerLabel = [[UILabel alloc] init];
    self.winerLabel.font = self.numLabel.font;
    self.winerLabel.textColor = self.numLabel.textColor;
    [self.contentView addSubview:self.winerLabel];
    
    // 开奖日期
    self.lotteryDateLabel = [[UILabel alloc] init];
    self.lotteryDateLabel.font = self.numLabel.font;
    self.lotteryDateLabel.textColor = self.numLabel.textColor;
    [self.contentView addSubview:self.lotteryDateLabel];
    
    self.oddsLabel = [[UILabel alloc] init];
    self.oddsLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.oddsLabel.textColor = c26;
    self.oddsLabel.textAlignment = NSTextAlignmentCenter;
    self.oddsLabel.numberOfLines = 2;
    [self.contentView addSubview:self.oddsLabel];
    
    // 分割线
    self.line = [[UIView alloc] init];
    self.line.backgroundColor = c27;
    self.line.frame = CGRectMake(0, _infoBarHeight - .5, _rowWidth, .5);
    [self.contentView addSubview:self.line];
    
    // 底部左侧
    self.leftDescLabel = [self leftDescLabelSetting];
    
    // 底部右侧
    self.rightDesclabel = [self rightDesclabelSetting];
}

- (UILabel *)leftDescLabelSetting {
    UILabel *leftDescLabel = [[UILabel alloc] init];
    leftDescLabel.font = _font16;
    leftDescLabel.textColor = c2;
    [self.contentView addSubview:leftDescLabel];
    return leftDescLabel;
}

- (UILabel *)rightDesclabelSetting {
    UILabel *rightDescLabel = [[UILabel alloc] init];
    rightDescLabel.font = _font16;
    rightDescLabel.textColor = c2;
    [self.contentView addSubview:rightDescLabel];
    return rightDescLabel;
}


#pragma mark - model
- (void)setItem:(PDSnatchItem *)item {
    _item = item;
    
    [self.productImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithRisqueUrl:item.image] placeholder:nil callback:NULL];
    self.nameLabel.text = item.name;
    self.numLabel.text = [NSString stringWithFormat:@"参与期号：%@",item.phase];
    if (item.winerId.length) {
        self.winerLabel.text = [NSString stringWithFormat:@"中奖用户：%@", item.winerNickName];
    } else {
        self.winerLabel.text = @"中奖用户：暂无";
    }
    
    self.lotteryDateLabel.text = [NSString stringWithFormat:@"开奖时间：%@", [PDCenterTool dateWithFormat:@"MM.dd HH:mm:ss" fromTimestamp:item.drawTime]];
    
    NSMutableAttributedString *oddsAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@％\n中奖率",item.odds]];
    [oddsAtt addAttribute:NSFontAttributeName value:[[UIFont systemFontOfCustomeSize:20] boldFont] range:NSMakeRange(0, item.odds.length + 1)];
    self.oddsLabel.attributedText = oddsAtt;
    
    // 我投入的份数
    self.leftDescLabel.text = [NSString stringWithFormat:@"我已投入%@份",@(item.portions)];
    self.rightDesclabel.text = [NSString stringWithFormat:@"好友帮抢%@份",@(item.helpPortions)];
    
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font];
    CGSize numSize = [self.numLabel.text sizeWithCalcFont:self.numLabel.font];
    CGSize winerSize = [self.winerLabel.text sizeWithCalcFont:self.winerLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - CGRectGetWidth(self.productImageView.frame) - LCFloat(10), [NSString contentofHeightWithFont:self.winerLabel.font])];
    CGSize lotterySize = [self.lotteryDateLabel.text sizeWithCalcFont:self.lotteryDateLabel.font];
    CGSize oddsSize = [self.oddsLabel.text sizeWithCalcFont:self.oddsLabel.font];
    
    // 上部分
    CGFloat space = (_infoBarHeight - nameSize.height - numSize.height - lotterySize.height - winerSize.height) / 5;
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + LCFloat(10), space, nameSize.width, nameSize.height);
    self.numLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + space, numSize.width, numSize.height);
    self.winerLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.numLabel.frame) + space, winerSize.width, winerSize.height);
    self.lotteryDateLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.winerLabel.frame) + space, lotterySize.width, lotterySize.height);
    self.oddsLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - oddsSize.width, CGRectGetMinY(self.numLabel.frame), oddsSize.width, oddsSize.height);
    
    // 下部分
    CGSize rightDescSize = [self.rightDesclabel.text sizeWithCalcFont:self.rightDesclabel.font];
    CGSize leftDescSize = [self.leftDescLabel.text sizeWithCalcFont:self.leftDescLabel.font];
    
    self.leftDescLabel.frame = CGRectMake(kTableViewSectionHeader_left, (_showBarHeight - leftDescSize.height) / 2 + _infoBarHeight, leftDescSize.width, leftDescSize.height);
    self.rightDesclabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - rightDescSize.width, (_showBarHeight - rightDescSize.height) / 2 + _infoBarHeight, rightDescSize.width, rightDescSize.height);
}

@end
