//
//  PDCenterLotteryAnchorCell.m
//  PandaKing
//
//  Created by Cranz on 16/11/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterLotteryAnchorCell.h"
#import "PDLotteryItemView.h"

@interface PDCenterLotteryAnchorCell ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *resultLabel;

@property (nonatomic, strong) PDLotteryItemView *pointView;
@property (nonatomic, strong) PDLotteryItemView *stateView;
@property (nonatomic, strong) PDLotteryItemView *goldView;

@property (nonatomic, strong) UIView *leftLine;
@property (nonatomic, strong) UIView *rightLine;

@property (nonatomic, assign) CGFloat minWidth;  /**< 左右两球默认大小，在内容超过限制时会跟着变大*/
@end

@implementation PDCenterLotteryAnchorCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (!self) {
        return nil;
    }
    
    _minWidth = LCFloat(60);
    
    [self cellSetting];
    
    return self;
}

- (void)cellSetting {
    // title
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.titleLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.titleLabel];
    
    // date
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.dateLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.dateLabel];
    
    // result
    self.resultLabel = [[UILabel alloc] init];
    self.resultLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.resultLabel];
    
    // 一塔point ／ 等待结果 ／ 金币
    self.pointView = [[PDLotteryItemView alloc] init];
    self.pointView.minItemSize = _minWidth;
    self.pointView.textFont = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.pointView];
    
    self.stateView = [[PDLotteryItemView alloc] init];
    self.stateView.minItemSize = _minWidth + LCFloat(30);
    self.stateView.textFont = [UIFont systemFontOfCustomeSize:16];
    [self.contentView addSubview:self.stateView];
    
    self.goldView = [[PDLotteryItemView alloc] init];
    self.goldView.textFont = [UIFont systemFontOfCustomeSize:13];
    self.goldView.minItemSize = _minWidth;
    [self.contentView addSubview:self.goldView];
    
    
    // line
    self.leftLine = [[UIView alloc] init];
    self.rightLine = [[UIView alloc] init];
    [self.contentView addSubview:self.leftLine];
    [self.contentView addSubview:self.rightLine];
}

- (void)setModel:(PDLotteryGameItem *)model {
    _model = model;
    
    self.titleLabel.text = model.title;
    self.dateLabel.text = [PDCenterTool dateFromTimestamp:model.dateTime];
    self.pointView.text = model.point;
    self.goldView.text = [NSString stringWithFormat:@"%ld\n金币",(long)model.goldCount];

    NSString *resultSuffix = @"";
    if ([model.state isEqualToString:@"waiting"]) {
        
        resultSuffix = @"暂无";
        self.stateView.text = @"等待结果";
        
        self.pointView.itemBgImage = self.stateView.itemBgImage = self.goldView.itemBgImage = [UIImage imageNamed:@"icon_lottery_wait_bg"];
        
        self.pointView.textColor = self.stateView.textColor = self.goldView.textColor = c26;
        
        self.leftLine.backgroundColor = self.rightLine.backgroundColor = c26;
        
    } else {
        
        NSString *result = model.result > 0? [NSString stringWithFormat:@"+%ld",(long)model.result] : [NSString stringWithFormat:@"%ld",model.result];
        resultSuffix = [NSString stringWithFormat:@"%@金币",result];
        
        if ([model.state isEqualToString:@"win"]) {
            self.stateView.text = @"预测成功";
            
            self.pointView.itemBgImage = self.stateView.itemBgImage = self.goldView.itemBgImage = [UIImage imageNamed:@"icon_lottery_success_bg"];
            
            self.pointView.textColor = self.stateView.textColor = self.goldView.textColor = c1;
            
            self.leftLine.backgroundColor = self.rightLine.backgroundColor = c26;
            
        } else {
            self.stateView.text = @"预测失败";
            
            self.pointView.itemBgImage = self.stateView.itemBgImage = self.goldView.itemBgImage = [UIImage imageNamed:@"icon_lottery_fail_bg"];
            
            self.pointView.textColor = self.stateView.textColor = self.goldView.textColor = c4;
            
            self.leftLine.backgroundColor = self.rightLine.backgroundColor = c27;
        }
    }
    
    NSMutableAttributedString *resultAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"竞猜结果:%@", resultSuffix]];
    [resultAtt addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(5, resultSuffix.length)];
    self.resultLabel.attributedText = [resultAtt copy];
    
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2, [NSString contentofHeightWithFont:self.titleLabel.font])];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGSize resultSize = [self.resultLabel.text sizeWithCalcFont:self.resultLabel.font];
    
    self.titleLabel.frame = CGRectMake(kTableViewSectionHeader_left, (self.row1 - titleSize.height) / 2, titleSize.width, titleSize.height);
    self.dateLabel.frame = CGRectMake(kTableViewSectionHeader_left, self.row1 + self.row2  +(self.row3 - dateSize.height) / 2, dateSize.width, dateSize.height);
    self.resultLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - resultSize.width, (self.row3 - resultSize.height) / 2 + self.row1 + self.row2, resultSize.width, resultSize.height);
    
    self.stateView.frame = CGRectMake((kScreenBounds.size.width - self.stateView.itemSize.width) / 2, (self.row2 - self.stateView.itemSize.height) / 2 + self.row1, self.stateView.itemSize.width, self.stateView.itemSize.height);
    self.pointView.frame = CGRectMake(CGRectGetMinX(self.stateView.frame) - LCFloat(45) - self.pointView.itemSize.width, (self.row2 - self.pointView.itemSize.height) / 2 + self.row1, self.pointView.itemSize.width, self.pointView.itemSize.height);
    self.goldView.frame = CGRectMake(CGRectGetMaxX(self.stateView.frame) + LCFloat(45), (self.row2 - self.goldView.itemSize.height) / 2 + self.row1, self.goldView.itemSize.width, self.goldView.itemSize.height);
    
    self.leftLine.frame = CGRectMake(CGRectGetMaxX(self.pointView.frame), (self.row2 - 1) / 2 + self.row1, CGRectGetMinX(self.stateView.frame) - CGRectGetMaxX(self.pointView.frame), 1);
    self.rightLine.frame = CGRectMake(CGRectGetMaxX(self.stateView.frame), CGRectGetMinY(self.leftLine.frame), CGRectGetMinX(self.goldView.frame) - CGRectGetMaxX(self.stateView.frame), 1);
}

@end
