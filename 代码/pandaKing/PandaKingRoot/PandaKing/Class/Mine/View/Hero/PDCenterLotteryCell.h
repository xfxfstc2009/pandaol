//
//  PDCenterLotteryCell.h
//  PandaKing
//
//  Created by Cranz on 16/10/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 我的竞猜基类cell
@interface PDCenterLotteryCell : UITableViewCell
@property (nonatomic, strong) UIView *line1;
@property (nonatomic, strong) UIView *line2;
@property (nonatomic, strong) UIImageView *bgImageView;

@property (nonatomic, assign) CGFloat row1;
@property (nonatomic, assign) CGFloat row2;
@property (nonatomic, assign) CGFloat row3;

+ (CGFloat)cellHeight;
@end
