//
//  PDCenterLotteryHeroDataView.h
//  PandaKing
//
//  Created by Cranz on 16/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryData.h"

/// 我的竞猜数据栏
@interface PDCenterLotteryHeroDataView : UIView
@property (nonatomic, strong) PDLotteryData *model;
@end
