//
//  PDLotteryItemView.m
//  PandaKing
//
//  Created by Cranz on 16/10/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryItemView.h"

@interface PDLotteryItemView ()
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) UILabel *textLabel;
@property (nonatomic, assign) CGSize size;
@end

@implementation PDLotteryItemView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self itemPageSetting];
    }
    return self;
}

- (void)itemPageSetting {
    self.bgImageView = [[UIImageView alloc] init];
    [self addSubview:self.bgImageView];
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.textAlignment = NSTextAlignmentCenter;
    self.textLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.textLabel.numberOfLines = 0;
    [self.bgImageView addSubview:self.textLabel];
}

- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    self.textLabel.textColor = textColor;
}

- (void)setText:(NSString *)text {
    _text = text;
    
    NSRange r;
    NSRange r_ = [text rangeOfString:@"\n"]; // 特殊标示的range
    r = NSMakeRange(r_.location + 1, text.length - (r_.location + 1)); // -5是因为其中一个是字符"\n"
    self.textLabel.text = text;
    
    if (_adjustsFontInNextLine) { // 第二行字体大小－2
        NSMutableAttributedString *textAtt = [[NSMutableAttributedString alloc] initWithString:text];
        [textAtt addAttribute:NSFontAttributeName value:[UIFont systemFontOfCustomeSize:self.textLabel.font.pointSize - 2] range:r];
        self.textLabel.attributedText = [textAtt copy];
    }
    
    CGSize textSize = [self.textLabel.text sizeWithCalcFont:self.textLabel.font];
    CGFloat max = MAX(textSize.width + LCFloat(15), textSize.height + LCFloat(15));
    if (_minItemSize == 0) {
        self.size = CGSizeMake(max, max);
    } else {
        self.size = CGSizeMake(MAX(max, _minItemSize), MAX(max, _minItemSize));
    }
    
    self.bgImageView.frame = CGRectMake(0, 0, self.size.width, self.size.height);
    self.textLabel.frame = self.bgImageView.bounds;
}

- (void)setItemBgImage:(UIImage *)itemBgImage {
    _itemBgImage = itemBgImage;
    
    self.bgImageView.image = itemBgImage;
}

- (void)setTextFont:(UIFont *)textFont {
    _textFont = textFont;
    
    self.textLabel.font = textFont;
}

- (CGSize)itemSize {
    return self.size;
}

@end
