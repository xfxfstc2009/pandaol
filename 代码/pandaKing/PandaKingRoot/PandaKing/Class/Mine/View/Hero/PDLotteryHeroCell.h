//
//  PDLotteryHeroCell.h
//  PandaKing
//
//  Created by Cranz on 16/10/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterLotteryCell.h"
#import "PDLotteryHeroItem.h"

/// 我的竞猜 － 英雄猜
@interface PDLotteryHeroCell : PDCenterLotteryCell
@property (nonatomic, strong) PDLotteryHeroItem *model;
@end
