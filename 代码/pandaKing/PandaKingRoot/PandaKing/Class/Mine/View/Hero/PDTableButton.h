//
//  PDTableButton.h
//  PandaKing
//
//  Created by Cranz on 16/11/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDTableButton : UIView
@property (nonatomic, assign) CGFloat offsetXOfArrow;
@property (nonatomic, assign) BOOL wannaToClickTempToDissmiss;

- (void)addItems:(NSArray <NSString *> *)itesName;
- (void)addItems:(NSArray <NSString *> *)itemsName exceptItem:(NSString *)itemName;
- (void)selectedAtIndexHandle:(void(^)(NSUInteger index, NSString *itemName))indexHandle;

- (void)show;
- (void)dismiss;
- (void)tempTapActionHandle:(void(^)())handle;
@end
