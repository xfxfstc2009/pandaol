//
//  PDLotteryItemView.h
//  PandaKing
//
//  Created by Cranz on 16/10/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDLotteryItemView : UIView
@property (nonatomic, assign) CGFloat minItemSize; /**< 最小的item尺寸*/
@property (nonatomic, strong) UIFont *textFont;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIImage *itemBgImage;
@property (nonatomic, assign) BOOL adjustsFontInNextLine; /** 第二行自动适应字体大小，默认NO*/
@property (nonatomic, assign, readonly) CGSize itemSize;
@end
