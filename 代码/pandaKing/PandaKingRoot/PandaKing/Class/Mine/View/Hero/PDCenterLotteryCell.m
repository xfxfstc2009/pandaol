//
//  PDCenterLotteryCell.m
//  PandaKing
//
//  Created by Cranz on 16/10/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterLotteryCell.h"

@interface PDCenterLotteryCell ()

@end

@implementation PDCenterLotteryCell

+ (CGFloat)cellHeight {
    return 180 + 20;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _row1 = 25;
        _row3 = 30 + 20;
        _row2 = [PDCenterLotteryCell cellHeight] - _row1 - _row3;
        [self lotteryPageSetting];
    }
    return self;
}

- (void)lotteryPageSetting {
    self.bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, _row1, kScreenBounds.size.width, [PDCenterLotteryCell cellHeight] - _row1 - _row3)];
    self.bgImageView.image = [UIImage imageNamed:@"icon_lottery_listview_bg"];
    [self.contentView addSubview:self.bgImageView];
    
    self.line1 = [[UIView alloc] initWithFrame:CGRectMake(0, _row1 - 1, kScreenBounds.size.width, 1)];
    self.line1.backgroundColor = c27;
    [self.contentView addSubview:self.line1];
    
    self.line2 = [[UIView alloc] initWithFrame:CGRectMake(0, [PDCenterLotteryCell cellHeight] - _row3, kScreenBounds.size.width, 1)];
    self.line2.backgroundColor = self.line1.backgroundColor;
    [self.contentView addSubview:self.line2];
}

@end
