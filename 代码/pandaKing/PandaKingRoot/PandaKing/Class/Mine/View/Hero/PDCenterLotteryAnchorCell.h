//
//  PDCenterLotteryAnchorCell.h
//  PandaKing
//
//  Created by Cranz on 16/11/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterLotteryCell.h"
#import "PDLotteryGameItem.h"

/// 我的竞猜 － 主播猜cell
@interface PDCenterLotteryAnchorCell : PDCenterLotteryCell
@property (nonatomic, strong) PDLotteryGameItem *model;
@end
