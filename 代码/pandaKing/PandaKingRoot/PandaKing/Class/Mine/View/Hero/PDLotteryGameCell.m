//
//  PDLotteryGameCell.m
//  PandaKing
//
//  Created by Cranz on 16/10/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameCell.h"
#import "PDLotteryItemView.h"

@interface PDLotteryGameCell ()
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, strong) PDLotteryItemView *pointView; // 观点
@property (nonatomic, strong) PDLotteryItemView *stateView; // 状态
@property (nonatomic, strong) PDLotteryItemView *goldView;  // 金币

@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *goldLabel; // 投注金币
@property (nonatomic, strong) UILabel *finishLabel; // 结束时间
@property (nonatomic, strong) UILabel *resultLabel; // 结果

@property (nonatomic, strong) UIColor *globeColor;  // 全局颜色
@property (nonatomic, strong) UIColor *lineColor;
@property (nonatomic, strong) UIView *leftline;
@property (nonatomic, strong) UIView *rightline;

@property (nonatomic, assign) CGFloat minWidth;  /**< 左右两球默认大小，在内容超过限制时会跟着变大*/
@end

@implementation PDLotteryGameCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    // title
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.titleLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.titleLabel];
    
    // date
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.dateLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.dateLabel];
    
    // gold
    self.goldLabel = [[UILabel alloc] init];
    self.goldLabel.font = self.dateLabel.font;
    self.goldLabel.textColor = self.dateLabel.textColor;
    [self.contentView addSubview:self.goldLabel];
    
    // finish
    self.finishLabel = [[UILabel alloc] init];
    self.finishLabel.font = self.dateLabel.font;
    self.finishLabel.textColor = self.dateLabel.textColor;
    [self.contentView addSubview:self.finishLabel];
    
    // result
    self.resultLabel = [[UILabel alloc] init];
    self.resultLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.resultLabel];
    
    // 一塔point ／ 等待结果 ／ 金币
    self.pointView = [[PDLotteryItemView alloc] init];
    self.pointView.textFont = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.pointView];
    
    self.stateView = [[PDLotteryItemView alloc] init];
    self.stateView.minItemSize = LCFloat(100);
    self.stateView.textFont = [UIFont systemFontOfCustomeSize:16];
    self.stateView.adjustsFontInNextLine = YES;
    [self.contentView addSubview:self.stateView];
    
    self.goldView = [[PDLotteryItemView alloc] init];
    self.goldView.textFont = [UIFont systemFontOfCustomeSize:13];
    self.goldView.adjustsFontInNextLine = YES;
    [self.contentView addSubview:self.goldView];
    
    // line
    self.leftline = [[UIView alloc] init];
    [self.contentView addSubview:self.leftline];
    
    self.rightline = [[UIView alloc] init];
    [self.contentView addSubview:self.rightline];
}

- (void)setGlobeColor:(UIColor *)globeColor {
    _globeColor = globeColor;
    
    self.pointView.textColor = globeColor;
    self.goldView.textColor = globeColor;
    self.stateView.textColor = globeColor;
}

- (void)setLineColor:(UIColor *)lineColor {
    _lineColor = lineColor;
    
    self.leftline.backgroundColor = lineColor;
    self.rightline.backgroundColor = lineColor;
}

- (void)setModel:(PDLotteryGameItem *)model {
    _model = model;
    
    self.titleLabel.text = model.title;
    self.dateLabel.text = [PDCenterTool dateWithFormat:@"MM-dd hh:mm" fromTimestamp:model.dateTime];
    self.pointView.text = model.point;
    self.goldView.text = [NSString stringWithFormat:@"%ld\n金币",(long)model.goldCount];
    
    // 赔率
    NSString *odds = [NSString stringWithFormat:@"赔率%.1lf",model.odds];
    
    // 金币
    NSString *gold = [NSString stringWithFormat:@"%ld金币",model.goldCount];
    NSMutableAttributedString *goldAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"投注:%@",gold]];
    [goldAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(3, gold.length)];
    self.goldLabel.attributedText = [goldAtt copy];

    NSString *result;
    NSString *finish;
    if ([model.state isEqualToString:@"waiting"]) {
        result = @"暂无";
        finish = @"暂无";
        self.resultLabel.text = [NSString stringWithFormat:@"竞猜结果:%@",result];
        self.stateView.text = [NSString stringWithFormat:@"等待结果\n%@",odds];
        self.globeColor = c26;
        self.lineColor = c26;
        self.stateView.itemBgImage = [UIImage imageNamed:@"icon_lottery_wait_bg"];
        self.goldView.itemBgImage = [UIImage imageNamed:@"icon_lottery_wait_bg"];
        self.pointView.itemBgImage = [UIImage imageNamed:@"icon_lottery_wait_bg"];
    } else if ([model.state isEqualToString:@"win"]) {
        result = [NSString stringWithFormat:@"+%ld",(long)model.result];
        finish = [PDCenterTool dateWithFormat:@"MM-dd hh:mm" fromTimestamp:model.finishTime];
        self.resultLabel.text = [NSString stringWithFormat:@"竞猜结果:%@金币",result];
        self.stateView.text = [NSString stringWithFormat:@"预测成功\n%@",odds];
        self.globeColor = [UIColor whiteColor];
        self.lineColor = c26;
        self.stateView.itemBgImage = [UIImage imageNamed:@"icon_lottery_success_bg"];
        self.goldView.itemBgImage = [UIImage imageNamed:@"icon_lottery_success_bg"];
        self.pointView.itemBgImage = [UIImage imageNamed:@"icon_lottery_success_bg"];
    } else {
        result = [NSString stringWithFormat:@"%ld",(long)model.result];
        finish = [PDCenterTool dateWithFormat:@"MM-dd hh:mm" fromTimestamp:model.finishTime];
        self.resultLabel.text = [NSString stringWithFormat:@"竞猜结果:%@金币",result];
        self.stateView.text = [NSString stringWithFormat:@"预测失败\n%@",odds];
        self.globeColor = c4;
        self.lineColor = c27;
        self.stateView.itemBgImage = [UIImage imageNamed:@"icon_lottery_fail_bg"];
        self.goldView.itemBgImage = [UIImage imageNamed:@"icon_lottery_fail_bg"];
        self.pointView.itemBgImage = [UIImage imageNamed:@"icon_lottery_fail_bg"];
    }
    
    self.finishLabel.text = finish;
    
    // 结果字体颜色处理
    NSMutableAttributedString *resultAtt = [[NSMutableAttributedString alloc] initWithString:self.resultLabel.text];
    if (model.result == 0) {
        [resultAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(5, result.length)];
    } else if (model.result > 0) {
        [resultAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(5, result.length + 2)];
    } else {
        [resultAtt addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(5, result.length + 2)];
    }

    self.resultLabel.attributedText = [resultAtt copy];
    
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGSize resultSize = [self.resultLabel.text sizeWithCalcFont:self.resultLabel.font];
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    CGSize finishSize = [self.finishLabel.text sizeWithCalcFont:self.finishLabel.font];
    
    CGFloat gold_y = (self.row3 - goldSize.height - 5 - resultSize.height) / 2;
    
    self.titleLabel.frame = CGRectMake(kTableViewSectionHeader_left, (self.row1 - titleSize.height) / 2, titleSize.width, titleSize.height);
    self.dateLabel.frame = CGRectMake(kTableViewSectionHeader_left, gold_y + self.row2 + self.row1, dateSize.width, dateSize.height);
    self.finishLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.dateLabel.frame) + 5, finishSize.width, finishSize.height);
    self.goldLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - goldSize.width, gold_y + self.row2 + self.row1, goldSize.width, goldSize.height);
    self.resultLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - resultSize.width, CGRectGetMaxY(self.goldLabel.frame) + 5, resultSize.width, resultSize.height);

    self.stateView.frame = CGRectMake((kScreenBounds.size.width - self.stateView.itemSize.width) / 2, (self.row2 - self.stateView.itemSize.height) / 2 + self.row1, self.stateView.itemSize.width, self.stateView.itemSize.height);
    self.pointView.frame = CGRectMake(CGRectGetMinX(self.stateView.frame) - LCFloat(45) - self.pointView.itemSize.width, (self.row2 - self.pointView.itemSize.height) / 2 + self.row1, self.pointView.itemSize.width, self.pointView.itemSize.height);
    self.goldView.frame = CGRectMake(CGRectGetMaxX(self.stateView.frame) + LCFloat(45), (self.row2 - self.goldView.itemSize.height) / 2 + self.row1, self.goldView.itemSize.width, self.goldView.itemSize.height);
    
    self.leftline.frame = CGRectMake(CGRectGetMaxX(self.pointView.frame), (self.row2 - 1) / 2 + self.row1, CGRectGetMinX(self.stateView.frame) - CGRectGetMaxX(self.pointView.frame), 1);
    self.rightline.frame = CGRectMake(CGRectGetMaxX(self.stateView.frame), CGRectGetMinY(self.leftline.frame), CGRectGetMinX(self.goldView.frame) - CGRectGetMaxX(self.stateView.frame), 1);
}

@end
