//
//  PDLotteryHeroCell.m
//  PandaKing
//
//  Created by Cranz on 16/10/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroCell.h"
#import "PDLotteryItemView.h"

@interface PDLotteryHeroCell ()
@property (nonatomic, strong) UIColor *globeColor;
@property (nonatomic, strong) UIColor *lineColor;

@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *issueLabel;
@property (nonatomic, strong) UILabel *goldLabel;
@property (nonatomic, strong) UILabel *resultLabel;
@property (nonatomic, strong) UILabel *finishLabel;
/**
 * 游戏图标
 */
@property (nonatomic, strong) UIImageView *gameIcon;

@property (nonatomic, strong) PDLotteryItemView *nameItemView;  /**< 名字长度*/
@property (nonatomic, strong) PDLotteryItemView *priceItemView; /**< 价格*/
@property (nonatomic, strong) PDLotteryItemView *rangeItemView; /**< 射程*/
@property (nonatomic, strong) PDLotteryItemView *stateItemView;

@property (nonatomic, strong) CAShapeLayer *nameLine;
@property (nonatomic, strong) CAShapeLayer *rangeLine;
@property (nonatomic, strong) CAShapeLayer *priceLine;
@end

@implementation PDLotteryHeroCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    // 期号
    self.issueLabel = [[UILabel alloc] init];
    self.issueLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.issueLabel];
    
    // date
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.dateLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.dateLabel];
    
    // 投注金币
    self.goldLabel = [[UILabel alloc] init];
    self.goldLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.goldLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.goldLabel];
    
    // 竞猜结果
    self.resultLabel = [[UILabel alloc] init];
    self.resultLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.resultLabel];
    
    // 开奖日期
    self.finishLabel = [[UILabel alloc] init];
    self.finishLabel.font = self.dateLabel.font;
    self.finishLabel.textColor = self.dateLabel.textColor;
    [self.contentView addSubview:self.finishLabel];
    
    // 名字长度
    self.nameItemView = [[PDLotteryItemView alloc] init];
    self.nameItemView.textFont = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.nameItemView];
    
    // 竞猜状态
    self.stateItemView = [[PDLotteryItemView alloc] init];
    self.stateItemView.textFont = [UIFont systemFontOfCustomeSize:15];
    self.stateItemView.minItemSize = LCFloat(100);
    self.stateItemView.adjustsFontInNextLine = YES;
    [self.contentView addSubview:self.stateItemView];
    
    // 射程
    self.rangeItemView = [[PDLotteryItemView alloc] init];
    self.rangeItemView.textFont = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.rangeItemView];
    
    // 价格
    self.priceItemView = [[PDLotteryItemView alloc] init];
    self.priceItemView.textFont = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.priceItemView];
    
    // 游戏图标
    self.gameIcon = [[UIImageView alloc] init];
    UIImage *image = [UIImage imageNamed:@"icon_mylottery_ow"];
    self.gameIcon.frame = CGRectMake(kTableViewSectionHeader_left, self.row1, image.size.width, image.size.height);
    [self.contentView addSubview:self.gameIcon];
    
    // 线
    self.nameLine = [CAShapeLayer layer];
    self.nameLine.lineWidth = 1;
    [self.contentView.layer addSublayer:self.nameLine];
    
    self.rangeLine = [CAShapeLayer layer];
    self.rangeLine.lineWidth = 1;
    [self.contentView.layer addSublayer:self.rangeLine];
    
    self.priceLine = [CAShapeLayer layer];
    self.priceLine.lineWidth = 1;
    [self.contentView.layer addSublayer:self.priceLine];
    
    [self setNameItemHidden:YES];
    [self setRangeItemHidden:YES];
    [self setPriceItemHidden:YES];
    
    [self.contentView bringSubviewToFront:self.nameItemView];
    [self.contentView bringSubviewToFront:self.stateItemView];
    [self.contentView bringSubviewToFront:self.rangeItemView];
    [self.contentView bringSubviewToFront:self.priceItemView];
}

- (void)calPathForLine:(CAShapeLayer *)line withView1Frame:(CGRect)frame1 view2Frame:(CGRect)frame2 {
    CGFloat r1 = frame1.size.width / 2;
    CGFloat r2 = frame2.size.width / 2;

    CGPoint center1 = CGPointMake(frame1.origin.x + r1, frame1.origin.y + r1);
    CGPoint center2 = CGPointMake(frame2.origin.x + r2, frame2.origin.y + r2);
    
    CGFloat xDis = ABS(center1.x - center2.x);
    CGFloat yDis = ABS(center1.y - center2.y);
    CGFloat l = sqrtf(xDis * xDis + yDis * yDis);
    CGPoint p1,p2;
    // 有4种可能性
    if (center2.x >= center1.x && center2.y < center1.y) {
        p2.x = center2.x - r2 / l * xDis;
        p2.y = center2.y + r2 / l * yDis;
        p1.x = center1.x + r1 / l * xDis;
        p1.y = center1.y - r1 / l * yDis;
    } else if (center2.x >= center1.x && center2.y >= center1.y) {
        p2.x = center2.x - r2 / l * xDis;
        p2.y = center2.y - r2 / l * yDis;
        p1.x = center1.x + r1 / l * xDis;
        p1.y = center1.y + r1 / l * yDis;
    } else if (center2.x < center1.x && center2.y < center1.y) {
        p2.x = center2.x + r2 / l * xDis;
        p2.y = center2.y + r2 / l * yDis;
        p1.x = center1.x - r1 / l * xDis;
        p1.y = center1.y - r1 / l * yDis;
    } else {
        p2.x = center2.x + r2 / l * xDis;
        p2.y = center2.y - r2 / l * yDis;
        p1.x = center1.x - r1 / l * xDis;
        p1.y = center1.y + r1 / l * yDis;
    }
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:p1];
    [path addLineToPoint:p2];
    line.path = path.CGPath;
}

- (void)setNameItemHidden:(BOOL)hidden {
    self.nameItemView.hidden = hidden;
    self.nameLine.hidden = hidden;
}

- (void)setRangeItemHidden:(BOOL)hidden {
    self.rangeItemView.hidden = hidden;
    self.rangeLine.hidden = hidden;
}

- (void)setPriceItemHidden:(BOOL)hidden {
    self.priceItemView.hidden = hidden;
    self.priceLine.hidden = hidden;
}

- (void)setLineColor:(UIColor *)lineColor {
    _lineColor = lineColor;
    
    self.nameLine.strokeColor = lineColor.CGColor;
    self.rangeLine.strokeColor = lineColor.CGColor;
    self.priceLine.strokeColor = lineColor.CGColor;
}

- (void)setGlobeColor:(UIColor *)globeColor {
    _globeColor = globeColor;
    
    self.stateItemView.textColor = self.globeColor;
    self.nameItemView.textColor = self.globeColor;
    self.rangeItemView.textColor = self.globeColor;
    self.priceItemView.textColor = self.globeColor;
}

- (void)setModel:(PDLotteryHeroItem *)model {
    _model = model;
    
    // 赔率
    NSString *odds = [NSString stringWithFormat:@"赔率%.1lf",model.odds];
    NSString *gold = [NSString stringWithFormat:@"%ld金币",(long)model.goldCount];
    NSString *finish;
    
    self.issueLabel.text = [NSString stringWithFormat:@"英雄猜第%@期",model.lotteryNum];
    self.dateLabel.text = [PDCenterTool dateWithFormat:@"MM-dd hh:mm" fromTimestamp:model.dateTime];
    self.goldLabel.text = [NSString stringWithFormat:@"投注:%@",gold];
    
    if ([model.state isEqualToString:@"waiting"]) {
        finish = @"暂无";
        self.resultLabel.text = [NSString stringWithFormat:@"竞猜结果:%@",@"暂无"];
        NSMutableAttributedString *resultAtt = [[NSMutableAttributedString alloc] initWithString:self.resultLabel.text];
        [resultAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(5, 2)];
        self.resultLabel.attributedText = [resultAtt copy];
        
        self.stateItemView.text = [NSString stringWithFormat:@"等待结果\n%@",odds];
        self.globeColor = c26;
        self.lineColor = c26;
        
        self.stateItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_wait_bg"];
        self.nameItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_wait_bg"];
        self.rangeItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_wait_bg"];
        self.priceItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_wait_bg"];
    } else {
        NSString *result = model.result > 0? [NSString stringWithFormat:@"+%ld",(long)model.result] : [NSString stringWithFormat:@"%ld",model.result];
        self.resultLabel.text = [NSString stringWithFormat:@"竞猜结果:%@金币",result];
        NSMutableAttributedString *resultAtt = [[NSMutableAttributedString alloc] initWithString:self.resultLabel.text];
        [resultAtt addAttribute:NSForegroundColorAttributeName value:model.result > 0? c26 : [UIColor redColor] range:NSMakeRange(5, result.length + 2)];
        self.resultLabel.attributedText = [resultAtt copy];
        
        finish = [PDCenterTool dateWithFormat:@"MM-dd hh:mm" fromTimestamp:model.finishTime];
        
        if ([model.state isEqualToString:@"lose"]) {
            self.stateItemView.text = [NSString stringWithFormat:@"预测失败\n%@",odds];
            self.globeColor = c4;
            self.lineColor = c27;

            self.stateItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_fail_bg"];
            self.nameItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_fail_bg"];
            self.rangeItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_fail_bg"];
            self.priceItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_fail_bg"];
        } else {
            self.stateItemView.text = [NSString stringWithFormat:@"预测成功\n%@",odds];
            self.globeColor = [UIColor whiteColor];
            self.lineColor = c26;
            
            self.stateItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_success_bg"];
            self.nameItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_success_bg"];
            self.rangeItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_success_bg"];
            self.priceItemView.itemBgImage = [UIImage imageNamed:@"icon_lottery_success_bg"];
        }
        
    }
    
    if (model.championGuessType == PDLotteryHeroTypeLol) {
        self.gameIcon.image = [UIImage imageNamed:@"icon_mylottery_lol"];
        /// 金币价格
        if (model.goldPrice == 0) { // 不选
            [self setPriceItemHidden:YES];
        } else if (model.goldPrice == 1) {
            self.priceItemView.text = @"3150及\n以下";
            [self setPriceItemHidden:NO];
        } else if (model.goldPrice == 2) {
            self.priceItemView.text = @"4800";
            [self setPriceItemHidden:NO];
        } else {
            self.priceItemView.text = @"6300及\n以上";
            [self setPriceItemHidden:NO];
        }
        
        /// 名字长度
        if (model.nameCount == 0) {
            [self setNameItemHidden:YES];
        } else if (model.nameCount == 1) {
            self.nameItemView.text = @"两个字\n及以下";
            [self setNameItemHidden:NO];
        } else if (model.nameCount == 2) {
            self.nameItemView.text = @"三个";
            [self setNameItemHidden:NO];
        } else {
            self.nameItemView.text = @"四个及\n以上";
            [self setNameItemHidden:NO];
        }
    } else if (model.championGuessType == PDLotteryHeroTypeDota2) {
        self.gameIcon.image = [UIImage imageNamed:@"icon_mylottery_dota2"];
        
        if (model.camp == 0) {//阵营
            [self setNameItemHidden:YES];
        } else if (model.camp == 1) {
            self.nameItemView.text = @"天辉";
            [self setNameItemHidden:NO];
        } else {
            self.nameItemView.text = @"夜魇";
            [self setNameItemHidden:NO];
        }
        
        if (model.mainProperty == 0) {
            [self setPriceItemHidden:YES];
        } else if (model.mainProperty == 1) {
            self.priceItemView.text = @"力量";
            [self setPriceItemHidden:NO];
        } else if (model.mainProperty == 2) {
            self.priceItemView.text = @"敏捷";
            [self setPriceItemHidden:NO];
        } else {
            self.priceItemView.text = @"智力";
            [self setPriceItemHidden:NO];
        }
    } else if (model.championGuessType == PDLotteryHeroTypeKing) {
        self.gameIcon.image = [UIImage imageNamed:@"icon_mylottery_king"];
        
        if (model.nameLength == 0) {
            [self setNameItemHidden:YES];
        } else if (model.nameLength == 1) {
            self.nameItemView.text = @"两个字\n及以下";
            [self setNameItemHidden:NO];
        } else if (model.nameLength == 2) {
            self.nameItemView.text = @"三个";
            [self setNameItemHidden:NO];
        } else {
            self.nameItemView.text = @"四个及\n以上";
            [self setNameItemHidden:NO];
        }
    }

    
    /// 射程
    if (model.distance == 0) {
        [self setRangeItemHidden:YES];
    } else if (model.distance == 1) {
        self.rangeItemView.text = @"远程";
        [self setRangeItemHidden:NO];
    } else {
        self.rangeItemView.text = @"近战";
        [self setRangeItemHidden:NO];
    }
    
    self.finishLabel.text = finish;
    
    NSMutableAttributedString *issueAtt = [[NSMutableAttributedString alloc] initWithString:self.issueLabel.text];
    [issueAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(4, model.lotteryNum.length)];
    self.issueLabel.attributedText = [issueAtt copy];
    
    NSMutableAttributedString *goldAtt = [[NSMutableAttributedString alloc] initWithString:self.goldLabel.text];
    [goldAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(3, gold.length)];
    self.goldLabel.attributedText = [goldAtt copy];
    
    CGSize issueSize = [self.issueLabel.text sizeWithCalcFont:self.issueLabel.font];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    CGSize resultSize = [self.resultLabel.text sizeWithCalcFont:self.resultLabel.font];
    CGSize finishSize = [self.finishLabel.text sizeWithCalcFont:self.finishLabel.font];
    
    // 投入和结果的label之间的距离
    CGFloat gold_y = (self.row3 - goldSize.height - resultSize.height - 5) / 2;
    
    self.issueLabel.frame = CGRectMake(kTableViewSectionHeader_left, (self.row1 - issueSize.height) / 2, issueSize.width, issueSize.height);
    self.dateLabel.frame = CGRectMake(kTableViewSectionHeader_left, gold_y + self.row1 + self.row2, dateSize.width, dateSize.height);
    self.finishLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.dateLabel.frame) + 5, finishSize.width, finishSize.height);
    self.goldLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - goldSize.width, gold_y + self.row2 + self.row1, goldSize.width, goldSize.height);
    self.resultLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - resultSize.width, CGRectGetMaxY(self.goldLabel.frame) + 5, resultSize.width, resultSize.height);
    self.stateItemView.frame = CGRectMake((kScreenBounds.size.width - self.stateItemView.itemSize.width) / 2, (self.row2 - self.stateItemView.itemSize.height) / 2 + self.row1, self.stateItemView.itemSize.width, self.stateItemView.itemSize.height);
    self.nameItemView.frame = CGRectMake(LCFloat(40), 20 + self.row1, self.nameItemView.itemSize.width, self.nameItemView.itemSize.height);
    self.rangeItemView.frame = CGRectMake(kScreenBounds.size.width - self.rangeItemView.itemSize.width - LCFloat(40), 10 + self.row1, self.rangeItemView.itemSize.width, self.rangeItemView.itemSize.height);
    self.priceItemView.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - self.priceItemView.itemSize.width - LCFloat(60), [PDLotteryHeroCell cellHeight] - self.row3 - 10 - self.priceItemView.itemSize.height, self.priceItemView.itemSize.width, self.priceItemView.itemSize.height);
    
    [self calPathForLine:self.nameLine withView1Frame:self.stateItemView.frame view2Frame:self.nameItemView.frame];
    [self calPathForLine:self.rangeLine withView1Frame:self.stateItemView.frame view2Frame:self.rangeItemView.frame];
    [self calPathForLine:self.priceLine withView1Frame:self.stateItemView.frame view2Frame:self.priceItemView.frame];
}

@end

