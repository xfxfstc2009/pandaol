//
//  PDCenterLotteryHeroDataView.m
//  PandaKing
//
//  Created by Cranz on 16/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterLotteryHeroDataView.h"
#import "PDDescView.h"

@interface PDCenterLotteryHeroDataView ()
@property (nonatomic, assign) CGSize size;
@property (nonatomic, strong) PDDescView *totalView;    // 竞猜场次
@property (nonatomic, strong) PDDescView *winView;  // 猜中次数
@property (nonatomic, strong) PDDescView *rewardView; // 累计收益
@end
@implementation PDCenterLotteryHeroDataView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    _size = self.frame.size;
}

- (void)pageSetting {
   
    for (int i = 0; i < 3; i++) {
        PDDescView *descView = [[PDDescView alloc] initWithFrame:CGRectMake(i * _size.width / 3, 0, _size.width / 3, _size.height)];
        descView.descColor = c26;
        descView.subDescColor = [UIColor whiteColor];
        if (i == 0) {
            descView.subDesc = @"竞猜场次";
            self.totalView = descView;
        } else if (i == 1) {
            descView.subDesc = @"猜中次数";
            self.winView = descView;
        } else {
            descView.subDesc = @"累计收益";
            self.rewardView = descView;
        }
        [self addSubview:descView];
        if (i == 0 || i == 1) {
            UIImageView *vLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_lottery_vline"]];
            vLine.frame = CGRectMake(_size.width / 3 * (i + 1), 0, 1, _size.height);
            [self addSubview:vLine];
        }
    }
    
    UIImageView *hline = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _size.width, 1)];
    hline.image = [UIImage imageNamed:@"icon_lottery_hline"];
    [self addSubview:hline];
}

- (void)setModel:(PDLotteryData *)model {
    _model = model;
    
    self.totalView.desc = [NSString stringWithFormat:@"%ld",(long)model.times];
    self.winView.desc = [NSString stringWithFormat:@"%ld",(long)model.winTimes];
    self.rewardView.desc = [NSString stringWithFormat:@"%ld",(long)model.profit];
}

@end
