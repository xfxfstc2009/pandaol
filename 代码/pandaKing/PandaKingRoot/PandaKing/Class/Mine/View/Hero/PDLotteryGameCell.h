//
//  PDLotteryGameCell.h
//  PandaKing
//
//  Created by Cranz on 16/10/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterLotteryCell.h"
#import "PDLotteryGameItem.h"

/// 我的竞猜 － 赛事竞猜
@interface PDLotteryGameCell : PDCenterLotteryCell
@property (nonatomic, strong) PDLotteryGameItem *model;
@end
