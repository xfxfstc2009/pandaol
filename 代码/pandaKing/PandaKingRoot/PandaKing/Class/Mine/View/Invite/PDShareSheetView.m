//
//  PDShareSheetView.m
//  PandaKing
//
//  Created by Cranz on 16/8/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDShareSheetView.h"

#define kShareViewCancleButton_height   40
#define kShareViewButton_top            20          //按钮顶部空隙
#define kShareViewButton_bottom         20          //按钮底部空隙
#define kShareViewButton_width          40
#define kShareViewButton_space          LCFloat(40) //按钮左右之间的空隙
#define kShareViewButton_dis            20          //按钮上下之间的间距
#define kShareViewButtonTilte_dis       5           // 按钮和文字之间的距离

#define kSheetViewTitle_height          30          // 标题的高度
#define kSheetViewButton_height         44          // 按钮的高度

typedef void(^clickBlock)(NSString *item);

@interface PDShareSheetView ()
@property (nonatomic, strong) UIView *bgView;       /**< 黑色半透明背景*/
@property (nonatomic, strong) UIColor *bgColor;
@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, copy)   clickBlock clickblock;
@property (nonatomic, copy) void (^sheetActions)(NSUInteger, NSString *);
@property (nonatomic, assign) NSTimeInterval duration;
@property (nonatomic, assign) CGFloat totalHeight;  /**< 按钮视图的总共高度*/
@end

@implementation PDShareSheetView

#pragma mark - 分享sheet

+ (PDShareSheetView *)shareWithItemArr:(NSArray *)itemArr imageArr:(NSArray *)imageArr clickComplication:(void (^)(NSString *))complication {
    return [[self alloc] initWithItemArr:itemArr imageArr:imageArr clickComplication:complication];
}

- (instancetype)initWithItemArr:(NSArray *)itemArr imageArr:(NSArray *)imageArr clickComplication:(void (^)(NSString *))complication {
    self = [super init];
    if (self) {
        self.bgColor = [[UIColor blackColor] colorWithAlphaComponent:.5];
        self.duration = 0.3f;
        self.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height);
        
        [self basicSettingWithItemArr:itemArr imageArr:imageArr clickComplication:complication];
    }
    return self;
}

- (void)basicSettingWithItemArr:(NSArray *)itemArr imageArr:(NSArray *)imageArr clickComplication:(void (^)(NSString *))complication {
    self.bgView = [[UIView alloc] initWithFrame:self.bounds];
    self.bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.bgView.backgroundColor = self.bgColor;
    [self addSubview:self.bgView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissBgView)];
    [self.bgView addGestureRecognizer:tap];
    
    self.buttonView = [[UIView alloc] init];
    self.buttonView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.buttonView];
    
    CGFloat button_space = (kScreenBounds.size.width - kShareViewButton_width * 4 - kShareViewButton_space * 3) / 2;
    UIFont *descFont = [UIFont systemFontOfCustomeSize:11];
    CGSize size = CGSizeZero;
    for (int i = 0;i < itemArr.count;i++) {
        NSString *title = itemArr[i];
        size = [title sizeWithCalcFont:descFont];
        
        UIButton *itemButton = [UIButton buttonWithType:UIButtonTypeCustom];
        itemButton.stringTag = title;
        [itemButton setFrame:CGRectMake(button_space + i % 4 * (kShareViewButton_width + kShareViewButton_space), kShareViewButton_top + ((int)(i / 4)) * (kShareViewButton_width + size.height + kShareViewButtonTilte_dis + kShareViewButton_dis), kShareViewButton_width, kShareViewButton_width)];
        [itemButton setBackgroundImage:[UIImage imageNamed:imageArr[i]] forState:UIControlStateNormal];
        [PDCenterTool setButtonBackgroundEffect:itemButton];
        itemButton.layer.cornerRadius = 2.f;
        itemButton.clipsToBounds = YES;
        [itemButton addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonView addSubview:itemButton];
        
        UILabel *descLabel = [[UILabel alloc] init];
        descLabel.text = title;
        descLabel.font = descFont;
        descLabel.textColor = [UIColor grayColor];
        CGPoint position = descLabel.layer.position;
        position.x = itemButton.layer.position.x;
        descLabel.layer.position = position;
        
        // 按钮的中心X
        CGFloat center_x = CGRectGetMinX(itemButton.frame) + kShareViewButton_width / 2;
        descLabel.frame = CGRectMake(center_x - size.width / 2, CGRectGetMaxY(itemButton.frame) + kShareViewButtonTilte_dis, size.width, size.height);
        [self.buttonView addSubview:descLabel];

    }
    
    // 1,2,3,4 -> 0
    // 5,6,7,8 -> 1
    // 9,10,11,12 -> 2
    int lines = (int)((itemArr.count - 1) / 4);
    for (int i = 1; i <= lines; i++) {
        UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, kShareViewButton_top + i * (kShareViewButton_width + [NSString contentofHeightWithFont:descFont] + kShareViewButtonTilte_dis + kShareViewButton_dis * 0.6), kScreenBounds.size.width, 1)];
        hLine.backgroundColor = c27;
        [self.buttonView addSubview:hLine];
    }
    
    CGFloat buttonView_height = kShareViewButton_top + kShareViewButton_bottom + (kShareViewButton_width + 2 + size.height) * (lines + 1) + lines * kShareViewButton_dis;
    
    UIView *sepView = [[UIView alloc] initWithFrame:CGRectMake(0, buttonView_height, kScreenBounds.size.width, 10)];
    sepView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.buttonView addSubview:sepView];
    
    buttonView_height += 10;
    
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancleButton setFrame:CGRectMake(0, buttonView_height, kScreenBounds.size.width, kShareViewCancleButton_height)];
    [cancleButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [PDCenterTool setButtonBackgroundEffect:cancleButton];
    cancleButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    [cancleButton addTarget:self action:@selector(cancleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonView addSubview:cancleButton];
    
    buttonView_height += kShareViewCancleButton_height;
    self.totalHeight = buttonView_height;
    self.buttonView.frame = CGRectMake(0, CGRectGetHeight(self.bgView.frame), kScreenBounds.size.width, buttonView_height);
    
    if (complication) {
        self.clickblock = complication;
    }
    
    NSEnumerator *windowEnnumtor = [UIApplication sharedApplication].windows.reverseObjectEnumerator;
    for (UIWindow *window in windowEnnumtor) {
        BOOL isOnMainScreen = window.screen == [UIScreen mainScreen];
        BOOL isVisible      = !window.hidden && window.alpha > 0;
        BOOL isLevelNormal  = window.windowLevel == UIWindowLevelNormal;
        
        if (isOnMainScreen && isVisible && isLevelNormal) {
            [window addSubview:self];
            [self show];
        }
    }
}

#pragma mark - 列表sheet

+ (PDShareSheetView *)showSheetWithTitle:(NSString *)title selContentArr:(NSArray *)contentArr clickComplication:(void (^)(NSUInteger, NSString *))complication {
    return [[self alloc] initWithTitle:title selContentArr:contentArr clickComplication:complication];
}

- (instancetype)initWithTitle:(NSString *)title selContentArr:(NSArray *)contentArr clickComplication:(void (^)(NSUInteger, NSString *))complication {
    self = [super init];
    if (self) {
        self.bgColor = [[UIColor blackColor] colorWithAlphaComponent:.5];
        self.duration = 0.3f;
        self.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height);
        [self setupSheetViewWithTitle:title selContentArr:contentArr clickComplication:complication];
    }
    return self;
}

- (void)setupSheetViewWithTitle:(NSString *)title selContentArr:(NSArray *)contentArr clickComplication:(void (^)(NSUInteger, NSString *))complication {
    self.bgView = [[UIView alloc] initWithFrame:self.bounds];
    self.bgView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.bgView.backgroundColor = self.bgColor;
    [self addSubview:self.bgView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissBgView)];
    [self.bgView addGestureRecognizer:tap];
    
    CGFloat titleHeight = 0;
    if (title) {
        titleHeight = kSheetViewTitle_height;
    }
    
    self.buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.bgView.frame), kScreenBounds.size.width, contentArr.count * kSheetViewButton_height + titleHeight + kShareViewCancleButton_height + 10)];
    self.buttonView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.buttonView];
    
    if (title) {
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.font = [UIFont systemFontOfCustomeSize:14];
        titleLabel.text = title;
        titleLabel.textColor = [UIColor lightGrayColor];
        CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
        titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, (kSheetViewTitle_height - titleSize.height) / 2, titleSize.width, titleSize.height);
        [self.buttonView addSubview:titleLabel];
    }
    
    for (int i = 0; i < contentArr.count; i++) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, titleHeight + i * kSheetViewButton_height, kScreenBounds.size.width, kSheetViewButton_height);
        [button setTag:i+999];
        button.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
        [button setTitle:contentArr[i] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [PDCenterTool setButtonBackgroundEffect:button];
        [button addTarget:self action:@selector(didClickSheetButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonView addSubview:button];
        
        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 1)];
        line.backgroundColor = c27;
        [button addSubview:line];
    }
    
    UIView *sepView = [[UIView alloc] initWithFrame:CGRectMake(0, titleHeight + contentArr.count * kSheetViewButton_height, kScreenBounds.size.width, 10)];
    sepView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.buttonView addSubview:sepView];
    
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancleButton setFrame:CGRectMake(0, CGRectGetMaxY(sepView.frame), kScreenBounds.size.width, kShareViewCancleButton_height)];
    [cancleButton setTitle:@"取消" forState:UIControlStateNormal];
    [cancleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [PDCenterTool setButtonBackgroundEffect:cancleButton];
    cancleButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    [cancleButton addTarget:self action:@selector(cancleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.buttonView addSubview:cancleButton];
    
    self.totalHeight = CGRectGetHeight(self.buttonView.frame);
    
    if (complication) {
        self.sheetActions = complication;
    }
    
    NSEnumerator *windowEnnumtor = [UIApplication sharedApplication].windows.reverseObjectEnumerator;
    for (UIWindow *window in windowEnnumtor) {
        BOOL isOnMainScreen = window.screen == [UIScreen mainScreen];
        BOOL isVisible      = !window.hidden && window.alpha > 0;
        BOOL isLevelNormal  = window.windowLevel == UIWindowLevelNormal;
        
        if (isOnMainScreen && isVisible && isLevelNormal) {
            [window addSubview:self];
            [self show];
        }
    }
}

#pragma mark - 手势

- (void)dismissBgView {
    [self dismiss];
}

#pragma mark - 控件方法
// share
- (void)buttonClick:(UIButton *)button {
    if (self.clickblock) {
        self.clickblock(button.stringTag);
    }
    [self dismiss];
}

- (void)cancleButtonClick:(UIButton *)button {
    [self dismiss];
}

// sheet
- (void)didClickSheetButton:(UIButton *)button {
    if (self.sheetActions) {
        self.sheetActions((button.tag-999), button.titleLabel.text);
    }
    [self dismiss];
}

#pragma mark - 动画

- (void)show {
    __block CGRect rect = self.buttonView.frame;
    [UIView animateWithDuration:self.duration animations:^{
        self.bgView.backgroundColor = self.bgColor;
        rect.origin.y -= self.totalHeight;
        self.buttonView.frame = rect;
    }];
}

- (void)dismiss {
    __block CGRect rect = self.buttonView.frame;
    [UIView animateWithDuration:self.duration animations:^{
        self.bgView.backgroundColor = [UIColor clearColor];
        rect.origin.y += self.totalHeight;
        self.buttonView.frame = rect;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
