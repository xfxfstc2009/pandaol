//
//  PDInviteLayer4.h
//  PandaKing
//
//  Created by Cranz on 17/1/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDInviteInfo.h"

@interface PDInviteLayer4 : UIView
@property (nonatomic, strong) PDInviteInfo *info;

- (void)didClickDetailActions:(void(^)())actions;
@end
