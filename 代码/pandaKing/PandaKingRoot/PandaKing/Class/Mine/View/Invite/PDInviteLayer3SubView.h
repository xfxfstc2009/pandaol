//
//  PDInviteLayer3SubView.h
//  PandaKing
//
//  Created by Cranz on 16/12/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDInviteLayer3SubView : UIView
- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title;
- (void)updateDataWithNumber:(NSInteger)number;
@end
