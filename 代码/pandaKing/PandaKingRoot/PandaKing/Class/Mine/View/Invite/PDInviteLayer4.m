//
//  PDInviteLayer4.m
//  PandaKing
//
//  Created by Cranz on 17/1/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDInviteLayer4.h"

@interface PDInviteLayer4 ()
@property (nonatomic, assign) CGSize size;
@property (nonatomic, strong) UILabel *descLabel; // 填写描述
@property (nonatomic, strong) NSMutableParagraphStyle *paragraph;
@property (nonatomic, strong) UIButton *detailButton;

@property (nonatomic, copy) void(^inviteLyaerBlock)();
@end

@implementation PDInviteLayer4

- (NSMutableParagraphStyle *)paragraph {
    if (!_paragraph) {
        _paragraph = [[NSMutableParagraphStyle alloc] init];
        CGSize size = [@"1、" sizeWithCalcFont:self.descLabel.font];
        _paragraph.headIndent = size.width;
        _paragraph.paragraphSpacing = 10;
    }
    return _paragraph;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        _size = frame.size;
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    self.backgroundColor = [UIColor whiteColor];
    
    // 邀请描述
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.descLabel.textColor = c5;
    self.descLabel.numberOfLines = 0;
    [self addSubview:self.descLabel];
    
    // 明细按钮
    self.detailButton = [UIButton buttonWithType:UIButtonTypeCustom];
    CGSize buttonSize = CGSizeMake(_size.width - 30 * 2, 44);
    self.detailButton.frame = CGRectMake(30, _size.height - 25 - buttonSize.height, buttonSize.width, buttonSize.height);
    self.detailButton.layer.cornerRadius = 3;
    self.detailButton.layer.masksToBounds = YES;
    self.detailButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    [self.detailButton setBackgroundImage:[UIImage imageWithRenderColor:c2 renderSize:buttonSize] forState:UIControlStateNormal];
    [self.detailButton setTitle:@"收益明细" forState:UIControlStateNormal];
    [self.detailButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self addSubview:self.detailButton];
    [self.detailButton addTarget:self action:@selector(didClickDetail) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didClickDetail {
    if (self.inviteLyaerBlock) {
        self.inviteLyaerBlock();
    }
}

- (void)didClickDetailActions:(void (^)())actions {
    if (actions) {
        self.inviteLyaerBlock = actions;
    }
}

- (void)setInfo:(PDInviteInfo *)info {
    _info = info;
    
    
    if (info.showContent) {
        NSAttributedString *descaAtt = [[NSAttributedString alloc] initWithString:info.showContent attributes:@{NSParagraphStyleAttributeName: self.paragraph}];
        self.descLabel.attributedText = [descaAtt copy];
    }
    
    CGFloat x = 25;
    CGSize descSize  = [self.descLabel.text boundingRectWithSize:CGSizeMake(_size.width - x * 2, CGFLOAT_MAX) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: self.descLabel.font, NSParagraphStyleAttributeName: self.paragraph} context:nil].size;
    self.descLabel.frame = CGRectMake(x, LCFloat(119/2)+6, descSize.width, descSize.height);
    
}

@end
