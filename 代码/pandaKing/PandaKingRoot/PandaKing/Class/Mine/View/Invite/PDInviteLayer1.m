//
//  PDInviteLayer1.m
//  PandaKing
//
//  Created by Cranz on 16/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInviteLayer1.h"
#import "PDQRTool.h"

@interface PDInviteLayer1 ()
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat textHeight; // 给描述文字溜出来的空隙

@property (nonatomic, strong) UIImageView *qrBgImageView;
@property (nonatomic, strong) UIImageView *qrImageView;
@property (nonatomic, strong) UILabel *descLabel;
@end

@implementation PDInviteLayer1

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    _size = frame.size;
    _textHeight = LCFloat(32);
    [self pageSetting];
    return self;
}

- (void)pageSetting {
    // 二维码背景图的宽度
    CGFloat qrWidth = _size.height - _textHeight;
    
    // 二维码背景
    self.qrBgImageView = [[UIImageView alloc] initWithFrame:CGRectMake((_size.width - qrWidth) / 2, 0, qrWidth, qrWidth)];
    self.qrBgImageView.image = [UIImage imageNamed:@"bg_invite_qr"];
    [self addSubview:self.qrBgImageView];
    
    // 二维码
    CGFloat qrSpace = 7;
    self.qrImageView = [[UIImageView alloc] initWithFrame:CGRectMake(qrSpace, qrSpace, qrWidth - qrSpace * 2, qrWidth - qrSpace * 2)];
    [self.qrBgImageView addSubview:self.qrImageView];
    
    // 描述label
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [[UIFont systemFontOfCustomeSize:14] boldFont];
    self.descLabel.textColor = c26;
    [self addSubview:self.descLabel];
}

- (void)setCode:(NSString *)code {
    _code = code;
    
    self.qrImageView.image = [PDQRTool showQRCodeImageWithString:code onColor:c2 offColor:c26];
    self.qrImageView.backgroundColor = c26;
    self.descLabel.text = @"扫码面对面邀请";
    
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    self.descLabel.frame = CGRectMake((_size.width - descSize.width) / 2, CGRectGetMaxY(self.qrBgImageView.frame) + (_textHeight - descSize.height) / 2, descSize.width, descSize.height);
}

@end
