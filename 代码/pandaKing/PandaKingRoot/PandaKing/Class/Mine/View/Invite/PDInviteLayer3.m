//
//  PDInviteLayer3.m
//  PandaKing
//
//  Created by Cranz on 16/12/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInviteLayer3.h"
#import "PDInviteLayer3SubView.h"

@interface PDInviteLayer3 ()
@property (nonatomic, assign) int layerHeight;
@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, strong) NSMutableArray *subViewArr;
@end

@implementation PDInviteLayer3

- (NSMutableArray *)subViewArr {
    if (!_subViewArr) {
        _subViewArr = [NSMutableArray array];
    }
    return _subViewArr;
}

- (instancetype)initWithFrame:(CGRect)frame titles:(NSArray *)titles {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    self.titles = titles;
    _size = frame.size;

    [self viewSetting];
    return self;
}

- (instancetype)addTitles:(NSArray *)titles {
    _layerHeight = LCFloat(238/2);
    return [self initWithFrame:CGRectMake(kTableViewSectionHeader_left, (kScreenBounds.size.height - _layerHeight) / 2, kScreenBounds.size.width - 2 * kTableViewSectionHeader_left, _layerHeight) titles:titles];
}

- (float)inviteLayerHeight {
    return _layerHeight;
}

#pragma mark - 界面

- (void)viewSetting {
    // 背景
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    backgroundImageView.image = [UIImage imageNamed:@"icon_share_layer3"];
    [self addSubview:backgroundImageView];
    
    for (int i = 0; i < _titles.count; i++) {
        CGFloat w = _size.width / _titles.count;
        PDInviteLayer3SubView *subView = [[PDInviteLayer3SubView alloc] initWithFrame:CGRectMake(w * i, 0, w, _size.height) title:_titles[i]];
        [self.subViewArr addObject:subView];
        [self addSubview:subView];
        if (i != 0) {
            UIView *l = [[UIView alloc] initWithFrame:CGRectMake(0, LCFloat(13), 1, _size.height- LCFloat(13) * 2)];
            l.backgroundColor = c28;
            [subView addSubview:l];
        }
    }
}

- (void)updateDataWithNumberArr:(NSArray *)arr {
    for (int i = 0; i < arr.count; i++) {
        PDInviteLayer3SubView *subView = _subViewArr[i];
        [subView updateDataWithNumber:[arr[i] integerValue]];
    }
}

@end
