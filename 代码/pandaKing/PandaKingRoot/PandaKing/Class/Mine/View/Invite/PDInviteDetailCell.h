//
//  PDInviteDetailCell.h
//  PandaKing
//
//  Created by Cranz on 16/12/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDInviteItem.h"

@interface PDInviteDetailCell : UITableViewCell
@property (nonatomic, strong) PDInviteItem *model;

+ (CGFloat)cellHeightWithModel:(PDInviteItem *)model;

- (instancetype)initWithConstraintWidth:(CGFloat)width reuseIdentifier:(NSString *)reuseIdentifier;

@end
