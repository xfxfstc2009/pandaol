//
//  PDInviteLayer1.h
//  PandaKing
//
//  Created by Cranz on 16/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 邀请码显示的那一层
@interface PDInviteLayer1 : UIView
@property (nonatomic, copy) NSString *code;
@end
