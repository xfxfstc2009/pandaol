//
//  PDInviteLayer3.h
//  PandaKing
//
//  Created by Cranz on 16/12/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 黑色浮层
@interface PDInviteLayer3 : UIView
@property (nonatomic, assign, readonly) float inviteLayerHeight;

- (instancetype)addTitles:(NSArray *)titles;
- (instancetype)initWithFrame:(CGRect)frame titles:(NSArray *)titles;

- (void)updateDataWithNumberArr:(NSArray *)arr;

@end
