//
//  PDInviteLayer2.h
//  PandaKing
//
//  Created by Cranz on 16/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDInviteInfo.h"

@class PDInviteLayer2;
@protocol PDInviteLayer2Delegate <NSObject>

- (void)inviteLayer:(PDInviteLayer2 *)layer didClickMoreButton:(UIButton *)button;
- (void)inviteLayer:(PDInviteLayer2 *)layer didClickSureButton:(UIButton *)button submitWithText:(NSString *)text;

@end
@interface PDInviteLayer2 : UIView
@property (nonatomic, weak) id<PDInviteLayer2Delegate> delegate;
@property (nonatomic, strong) PDInviteInfo *info;
@end
