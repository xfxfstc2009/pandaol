//
//  PDInviteLayer3SubView.m
//  PandaKing
//
//  Created by Cranz on 16/12/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInviteLayer3SubView.h"

@interface PDInviteLayer3SubView ()
@property (nonatomic, assign) CGSize size;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIImageView *line0;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, strong) UIImageView *arrowIcon;
@end

@implementation PDInviteLayer3SubView

- (instancetype)initWithFrame:(CGRect)frame title:(NSString *)title {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    _size = frame.size;
    [self viewSettingWithTitle:title];
    return self;
}

- (void)viewSettingWithTitle:(NSString *)title {
    // 标题
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:11];
    self.titleLabel.textColor = c4;
    self.titleLabel.text = title;
    [self addSubview:self.titleLabel];
    
    // 短线
    self.line0 = [[UIImageView alloc] init];
    self.line0.image = [UIImage imageNamed:@"icon_share_line0"];
    [self addSubview:self.line0];
    
    // 数字
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
    self.numberLabel.textColor = c26;
    self.numberLabel.text = @"0";
    [self addSubview:self.numberLabel];
    
    // 上箭头
    self.arrowIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_share_arrow"]];
    [self addSubview:self.arrowIcon];
    
    [self calFrames];
}

- (void)calFrames {
    // 设置frame
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font];
    CGSize lineSize = CGSizeMake(titleSize.width - 4, 2);
    CGSize numberSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font];
    CGSize arrowSize = self.arrowIcon.image.size;
    // 收入金币到短线的距离
    CGFloat s1 = LCFloat(6);
    // 短线到1000数字的距离 同 数字到箭头的距离
    CGFloat s2 = LCFloat(10);
    // 金币上 和 箭头下的距离
    CGFloat s3 = (_size.height - titleSize.height - numberSize.height - arrowSize.height - s1 - 2 * s2) / 2;
    
    self.titleLabel.frame = CGRectMake((_size.width - titleSize.width) / 2, s3, titleSize.width, titleSize.height);
    self.line0.frame = CGRectMake((_size.width - lineSize.width) / 2, CGRectGetMaxY(self.titleLabel.frame) + s1, lineSize.width, lineSize.height);
    self.numberLabel.frame = CGRectMake((_size.width - numberSize.width) / 2, CGRectGetMaxY(self.line0.frame) + s2, numberSize.width, numberSize.height);
    self.arrowIcon.frame = CGRectMake((_size.width - arrowSize.width) / 2, CGRectGetMaxY(self.numberLabel.frame) + s2, arrowSize.width, arrowSize.height);
}

- (void)updateDataWithNumber:(NSInteger)number {
    self.numberLabel.text = [PDCenterTool numberStringChange:number];
    
    CGSize numberSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font];
    self.numberLabel.frame = CGRectMake((_size.width - numberSize.width) / 2, self.numberLabel.frame.origin.y, numberSize.width, numberSize.height);
}

@end
