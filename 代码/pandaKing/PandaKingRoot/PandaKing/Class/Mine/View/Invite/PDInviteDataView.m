//
//  PDInviteDataView.m
//  PandaKing
//
//  Created by Cranz on 17/4/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDInviteDataView.h"
#import "PDDescView.h"

@interface PDInviteDataView ()
@property (nonatomic, strong) PDDescView *totalView;
@property (nonatomic, strong) PDDescView *paymentView;
@property (nonatomic, assign) CGSize size;
@end

@implementation PDInviteDataView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _size = frame.size;
        [self setupDataView];
    }
    return self;
}

- (void)setupDataView {
    UIImage *hLineImage = [UIImage imageNamed:@"icon_lottery_hline"];
    UIImageView *hLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, _size.width, 1)];
    hLine.image = hLineImage;
    [self addSubview:hLine];
    
    self.totalView = [[PDDescView alloc] initWithFrame:CGRectMake(0, 0, _size.width/2, _size.height)];
    self.totalView.descColor = c26;
    self.totalView.subDescColor = [UIColor whiteColor];
    self.totalView.subDesc = @"总收入";
    [self addSubview:self.totalView];

    self.paymentView = [[PDDescView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.totalView.frame), 0, _size.width / 2, _size.height)];
    self.paymentView.descColor = self.totalView.descColor;
    self.paymentView.subDescColor = self.totalView.subDescColor;
    self.paymentView.subDesc = @"奖励收益";
    [self addSubview:self.paymentView];
    
    UIImage *vLineImage = [UIImage imageNamed:@"icon_lottery_vline"];
    UIImageView *vLine = [[UIImageView alloc] initWithFrame:CGRectMake(_size.width / 2, 1, 1, _size.height-2)];
    vLine.image = vLineImage;
    [self addSubview:vLine];
}

- (void)setModel:(PDInviteList *)model {
    _model = model;
    
    self.totalView.desc = [NSString stringWithFormat:@"%@",[PDCenterTool numberStringChangeWithoutSpecialCharacter:model.totalCount]];
    self.paymentView.desc = [NSString stringWithFormat:@"%@",[PDCenterTool numberStringChangeWithoutSpecialCharacter:model.consumptionDrawCount]];
    
}

@end
