//
//  PDInviteLayer2.m
//  PandaKing
//
//  Created by Cranz on 16/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInviteLayer2.h"

@interface PDInviteLayer2 ()
@property (nonatomic, assign) CGSize size;
@property (nonatomic, strong) UIButton *moreButton; // 收益明晰
@property (nonatomic, strong) UIImageView *line1; // 长线
@property (nonatomic, strong) UILabel *descLabel; // 填写描述
@property (nonatomic, strong) CAShapeLayer *line2; // 虚线
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *sureButton;
@property (nonatomic, strong) UIImageView *nameImageView;
@property (nonatomic, strong) UILabel *nameLabel;

@property (nonatomic, strong) NSMutableParagraphStyle *paragraph;
@end

@implementation PDInviteLayer2

- (NSMutableParagraphStyle *)paragraph {
    if (!_paragraph) {
        _paragraph = [[NSMutableParagraphStyle alloc] init];
        CGSize size = [@"1." sizeWithCalcFont:self.descLabel.font];
        _paragraph.headIndent = size.width;
    }
    return _paragraph;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    _size = self.frame.size;
    self.backgroundColor = [[UIColor hexChangeFloat:@"000000"] colorWithAlphaComponent:0.78
    ];
    
    // 收益明晰按钮
    self.moreButton = [[UIButton alloc] init];
    self.moreButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    [self.moreButton setTitle:@"<< 收益明细 >>" forState:UIControlStateNormal];
    [self.moreButton setTitleColor:[UIColor hexChangeFloat:@"896a2d"] forState:UIControlStateNormal];
    CGSize titleSize = [self.moreButton.titleLabel.text sizeWithCalcFont:self.moreButton.titleLabel.font];
    self.moreButton.frame = CGRectMake((_size.width - titleSize.width) / 2, LCFloat(119/2), titleSize.width, LCFloat(58/2));
    [self.moreButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, self.moreButton.frame.size.height - titleSize.height, 0)];
    [self addSubview:self.moreButton];
    [self.moreButton addTarget:self action:@selector(didClickMore:) forControlEvents:UIControlEventTouchUpInside];
    
    // 长线
    self.line1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.moreButton.frame), _size.width, 2)];
    self.line1.image = [UIImage imageNamed:@"icon_share_line1"];
    [self addSubview:self.line1];
    
    // 邀请描述
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.descLabel.textColor = c5;
    self.descLabel.numberOfLines = 0;
    [self addSubview:self.descLabel];
    
    // 虚线
    self.line2 = [CAShapeLayer layer];
    self.line2.frame = CGRectMake(0, CGRectGetMaxY(self.line1.frame) + LCFloat(188/2), _size.width, 1);
    self.line2.backgroundColor = c2.CGColor;
//    self.line2.lineDashPattern = @[@5,@2];
//    self.line2.lineDashPhase = 0;
    [self.layer addSublayer:self.line2];
    
    // 填写 会根据是否已由邀请人而变化界面
    CGFloat x = LCFloat(50/2);
    CGFloat h = _size.height - CGRectGetMaxY(self.line2.frame); // 留给输入框和确定按钮的距离
    CGFloat s = LCFloat(48/2);
    CGFloat y = LCFloat(22/2) + CGRectGetMaxY(self.line2.frame);
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(x, y, LCFloat(620/2), LCFloat(88/2))];
    self.textField.backgroundColor = c2;
    self.textField.font = [UIFont systemFontOfCustomeSize:15];
    self.textField.textAlignment = NSTextAlignmentCenter;
    self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.textField.placeholder = @"填写好友邀请码获取福利";
    self.textField.textColor = c5;
    [self.textField setValue:c5 forKeyPath:@"_placeholderLabel.textColor"];
    self.textField.hidden = YES;
    [self.textField addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.textField];
    
    // 确定
    self.sureButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(self.textField.frame), CGRectGetMaxY(self.textField.frame) + s, CGRectGetWidth(self.textField.frame), LCFloat(88/2))];
    [self buttonUnEnabled];
    self.sureButton.hidden = YES;
    self.sureButton.backgroundColor = [UIColor hexChangeFloat:@"c39e53"];
    self.sureButton.titleLabel.font = [UIFont systemFontOfCustomeSize:17];
    [self.sureButton setTitle:@"确定" forState:UIControlStateNormal];
    [self.sureButton setTitleColor:[UIColor hexChangeFloat:@"6e5016"] forState:UIControlStateNormal];
    [PDCenterTool setButtonBackgroundEffect:self.sureButton];
    [self.sureButton addTarget:self action:@selector(didClickSure:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.sureButton];
    
    // 邀请人的名字
    self.nameImageView = [[UIImageView alloc] initWithFrame:CGRectMake(x, (h - LCFloat(110/2)) / 2 + CGRectGetMaxY(self.line2.frame), CGRectGetWidth(self.textField.frame), LCFloat(44))];
    self.nameImageView.hidden = YES;
    self.nameImageView.image = [UIImage imageNamed:@"icon_share_name_bg"];
    [self addSubview:self.nameImageView];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [[UIFont systemFontOfCustomeSize:17] boldFont];
    self.nameLabel.textColor = [UIColor hexChangeFloat:@"896a2d"];
    [self.nameImageView addSubview:self.nameLabel];
}

#pragma mark - 控件方法

- (void)didClickMore:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(inviteLayer:didClickMoreButton:)]) {
        [self.delegate inviteLayer:self didClickMoreButton:button];
    }
}

- (void)didClickSure:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(inviteLayer:didClickSureButton:submitWithText:)]) {
        [self.delegate inviteLayer:self didClickSureButton:button submitWithText:self.textField.text];
    }
}

- (void)didChangeValue:(UITextField *)textField {
    if (textField.text.length > 0) {
        [self buttonEnbled];
    } else {
        [self buttonUnEnabled];
    }
}

- (void)buttonEnbled {
//    self.sureButton.backgroundColor = [UIColor hexChangeFloat:@"c39e53"];
    self.sureButton.enabled = YES;
}

- (void)buttonUnEnabled {
//    self.sureButton.backgroundColor = [UIColor lightGrayColor];
    self.sureButton.enabled = NO;
}

#pragma mark - set

- (void)setInfo:(PDInviteInfo *)info {
    _info = info;
    
    self.nameLabel.text = [NSString stringWithFormat:@"我的邀请人:%@",info.nickname];
    self.descLabel.text = [NSString stringWithFormat:@"1.每成功邀请1个玩家注册盼达电竞，您可获得%ld金币奖励\n2.好友成功绑定并认证召唤师后，即可获得%ld竹子(竹子可在商城内直接兑换Q币、点卷等奖品)",(long)info.invitedAward,(long)info.activateGameUserAward];
    NSAttributedString *descaAtt = [[NSAttributedString alloc] initWithString:self.descLabel.text attributes:@{NSParagraphStyleAttributeName: self.paragraph}];
    self.descLabel.attributedText = [descaAtt copy];
    
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(_size.width - 10, CGFLOAT_MAX)];
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGRectGetWidth(self.nameImageView.frame) - 10, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.descLabel.frame = CGRectMake(5, (LCFloat(188/2) - descSize.height) / 2 + CGRectGetMaxY(self.line1.frame), descSize.width, descSize.height);
    self.nameLabel.frame = CGRectMake((CGRectGetWidth(self.nameImageView.frame) - nameSize.width) / 2, (CGRectGetHeight(self.nameImageView.frame) - nameSize.height) / 2, nameSize.width, nameSize.height);
    
    self.textField.hidden = info.hasInvited;
    self.sureButton.hidden = info.hasInvited;
    self.nameImageView.hidden = !info.hasInvited;
}

@end
