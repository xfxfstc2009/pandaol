//
//  PDInviteDataView.h
//  PandaKing
//
//  Created by Cranz on 17/4/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDInviteList.h"

@interface PDInviteDataView : UIView
@property (nonatomic, strong) PDInviteList *model;
@end
