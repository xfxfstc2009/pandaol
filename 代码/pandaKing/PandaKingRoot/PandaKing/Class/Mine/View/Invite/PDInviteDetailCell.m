//
//  PDInviteDetailCell.m
//  PandaKing
//
//  Created by Cranz on 16/12/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInviteDetailCell.h"
#import <objc/runtime.h>

#define kINVITE_SPACE 2
#define kINVITE_FONT [UIFont systemFontOfCustomeSize:14]

const char *constraintWidthKey = "constraintWidth";

@interface PDInviteDetailCell ()
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic) CGFloat width;
@end

@implementation PDInviteDetailCell

+ (CGFloat)cellHeightWithModel:(PDInviteItem *)model {

    NSString *type = @"";
    if ([model.source isEqualToString:@"firstActivateAfterInvitedTask"]) { // 竹子
        type = @"竹子";
    } else {
        type = @"金币";
    }
    
    NSNumber *cWidthNumber = objc_getAssociatedObject([self class], constraintWidthKey);
    CGSize contentSize = [[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@%@+%ld",model.reason, type,(long)model.deltaFee]] sizeWithCalcFont:kINVITE_FONT constrainedToSize:CGSizeMake(cWidthNumber.floatValue, CGFLOAT_MAX)];

    return contentSize.height + kINVITE_SPACE * 2;
}

- (instancetype)initWithConstraintWidth:(CGFloat)width reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        _width = width;
        [self cellSetting];
    }
    return self;
}

- (void)cellSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor clearColor];
    
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = kINVITE_FONT;
    self.dateLabel.textColor = c4;
    [self.contentView addSubview:self.dateLabel];
    
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font = kINVITE_FONT;
    self.contentLabel.textColor = c4;
    self.contentLabel.numberOfLines = 0;
    [self.contentView addSubview:self.contentLabel];
}

- (void)setModel:(PDInviteItem *)model {
    _model = model;
    
    NSString *date = [PDCenterTool dateWithFormat:@"yyyy-MM-dd" fromTimestamp:model.logTime];
    self.dateLabel.text = date;
    
    NSString *type = @"";
    if ([model.source isEqualToString:@"invitedMemFirstActivateLOLGameUser"]) { // 竹子
        type = @"竹子";
    } else {
        type = @"金币";
    }
    
    NSMutableAttributedString *reasonAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@%@+%ld",model.reason, type,(long)model.deltaFee]]];
    if ([model.reason containsString:@"【"] && [model.reason containsString:@"】"]) {
        NSRange r1 = [model.reason rangeOfString:@"【"];
        NSRange r2 = [model.reason rangeOfString:@"】"];
        NSRange r = NSMakeRange(r1.location, r2.location - r1.location + 1);
        [reasonAtt addAttribute:NSForegroundColorAttributeName value:c39 range:r];
    }
    
    self.contentLabel.attributedText = [reasonAtt copy];
    
    CGSize dateSize = [date sizeWithCalcFont:self.dateLabel.font];
    CGFloat cWidth = _width - dateSize.width - 12 * 2;
    CGSize contentSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(cWidth, CGFLOAT_MAX)];
    
    objc_setAssociatedObject([self class], constraintWidthKey, [NSNumber numberWithDouble:cWidth], OBJC_ASSOCIATION_RETAIN_NONATOMIC);

    self.dateLabel.frame = CGRectMake(12, kINVITE_SPACE, dateSize.width, dateSize.height);
    self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.dateLabel.frame), kINVITE_SPACE, contentSize.width, contentSize.height);
}

@end
