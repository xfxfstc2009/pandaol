//
//  PDShareSheetView.h
//  PandaKing
//
//  Created by Cranz on 16/8/6.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDShareSheetView : UIView
/**
 @param itemArr      传入名字
 @param imageArr     传入对应的图片名字
 @param complication 点击响应
 */
+ (PDShareSheetView *)shareWithItemArr:(NSArray *)itemArr
                              imageArr:(NSArray *)imageArr
                     clickComplication:(void(^)(NSString *item))complication;

/**
 * 列表sheet
 */
+ (PDShareSheetView *)showSheetWithTitle:(NSString *)title
                           selContentArr:(NSArray *)contentArr
                       clickComplication:(void(^)(NSUInteger index, NSString *item))complication;
@end
