//
//  PDCenterFriendTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterFriendItem.h"
#import "PDButton.h"

/// 关注、粉丝 cell
@protocol PDCenterFriendTableViewCellDelegate;
@interface PDCenterFriendTableViewCell : UITableViewCell
@property (nonatomic, strong) PDCenterFriendItem *model;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak)   id<PDCenterFriendTableViewCellDelegate> delegate;
@property (nonatomic, strong) PDButton *moreButton;

+ (CGFloat)cellHeight;
- (void)setNoneMoreButton;
@end

@protocol PDCenterFriendTableViewCellDelegate <NSObject>

- (void)tableViewCell:(PDCenterFriendTableViewCell *)cell didClickButtonAtIndexPath:(NSIndexPath *)indexPath;

@end
