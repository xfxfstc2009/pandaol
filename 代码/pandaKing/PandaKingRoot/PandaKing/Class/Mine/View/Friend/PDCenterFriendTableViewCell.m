//
//  PDCenterFriendTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterFriendTableViewCell.h"
#import "PDCenterLabel.h"

@interface PDCenterFriendTableViewCell ()
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) PDCenterLabel *ageLabel;
@property (nonatomic, strong) UIImageView *bindImageView; // 绑定
@property (nonatomic, strong) UIImageView *authImageView; // 认证
@property (nonatomic, strong) UILabel *signLabel; // 签名

@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, assign) CGFloat imageTop;
@property (nonatomic, assign) CGFloat imageRight;
@property (nonatomic, assign) CGFloat space_label;
@end

@implementation PDCenterFriendTableViewCell

+ (CGFloat)cellHeight {
    return LCFloat(75);
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    _rowHeight = [PDCenterFriendTableViewCell cellHeight];
    _imageTop = LCFloat(12);
    _imageRight = LCFloat(15);
    _space_label = LCFloat(3);
}

- (void)pageSetting {
    // header
    CGFloat imageWidth = _rowHeight - _imageTop * 2;
    self.headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, _imageTop, imageWidth, imageWidth)];
    self.headerImageView.layer.cornerRadius = imageWidth / 2;
    self.headerImageView.clipsToBounds = YES;
    self.headerImageView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.contentView addSubview:self.headerImageView];
    
    // name
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    // age
    self.ageLabel = [[PDCenterLabel alloc] init];
    self.ageLabel.textColor = [UIColor whiteColor];
    self.ageLabel.textFont = [UIFont systemFontOfCustomeSize:13];
    self.ageLabel.contentInsets = UIEdgeInsetsMake(2, 7, 2, 7);
    [self.contentView addSubview:self.ageLabel];

    // 绑定 icon
    self.bindImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.bindImageView];
    
    // 认证
    self.authImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.authImageView];
    
    // 距离
    self.signLabel = [[UILabel alloc] init];
    self.signLabel.textColor = [UIColor lightGrayColor];
    self.signLabel.font = [UIFont systemFontOfCustomeSize:12];
    [self.contentView addSubview:self.signLabel];
    
    // button
    CGFloat buttonWidth = LCFloat(44);
    CGFloat buttonHeight = _rowHeight - LCFloat(10);
    self.moreButton = [PDButton buttonWithType:UIButtonTypeCustom];
    self.moreButton.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.moreButton.frame = CGRectMake(kScreenBounds.size.width - buttonWidth - kTableViewSectionHeader_left, (_rowHeight - buttonHeight) / 2, buttonWidth, buttonHeight);
    self.moreButton.titleLabel.font = [UIFont systemFontOfCustomeSize:12];
    [self.moreButton setImageEdgeInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
    [self.moreButton addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.moreButton];
}

- (void)didClickButton {
    if ([self.delegate respondsToSelector:@selector(tableViewCell:didClickButtonAtIndexPath:)]) {
        [self.delegate tableViewCell:self didClickButtonAtIndexPath:self.indexPath];
    }
}

- (void)setNoneMoreButton {
    self.moreButton.hidden = YES;
}

- (void)setModel:(PDCenterFriendItem *)model {
    _model = model;
    
    self.nameLabel.text = model.friendInfo.nickname;
    self.ageLabel.text = [NSString stringWithFormat:@"%ld",(long)model.friendInfo.age];
    self.signLabel.text = model.friendInfo.signature;
    
    if ([model.friendInfo.gender isEqualToString:@"MALE"]) {
        self.ageLabel.image = [UIImage imageNamed:@"icon_friend_male"];
        self.ageLabel.backgroundColor = RGB(129, 212, 250, 1);
    } else if ([model.friendInfo.gender isEqualToString:@"FEMALE"]) {
        self.ageLabel.image = [UIImage imageNamed:@"icon_friend_female"];
        self.ageLabel.backgroundColor = RGB(243, 145, 178, 1);
    } else {
        self.ageLabel.backgroundColor = RGB(69, 69, 69, 1);
    }
    self.ageLabel.layer.cornerRadius = self.ageLabel.size.height / 2;
    self.ageLabel.clipsToBounds = YES;


    if (model.followed && !model.fans) {
        self.moreButton.imageView.image = [UIImage imageNamed:@"icon_friend_followed"];
        [self.moreButton setTitle:@"已关注" forState:UIControlStateNormal];
        [self.moreButton setEnabled:NO];
        [self.moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    } else if (model.followed && model.fans) {
        self.moreButton.imageView.image = [UIImage imageNamed:@"icon_friend_followeach"];
        [self.moreButton setTitle:@"互相关注" forState:UIControlStateNormal];
        [self.moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.moreButton setEnabled:NO];
    } else {
        self.moreButton.imageView.image = [UIImage imageNamed:@"icon_friend_addfollow"];
        [self.moreButton setTitle:@"加关注" forState:UIControlStateNormal];
        [self.moreButton setTitleColor:c26 forState:UIControlStateNormal];
        [self.moreButton setEnabled:YES];
    }
    
    // 是否绑定
    if (model.friendInfo.bindGameUser) {
        self.bindImageView.image = [UIImage imageNamed:@"icon_find_bind_on"];
    } else {
        self.bindImageView.image = [UIImage imageNamed:@"icon_find_bind_off"];
    }
    
    // 是否认证
    if (model.friendInfo.activateGameUser) {
        self.authImageView.image = [UIImage imageNamed:@"icon_find_auth_on"];
    } else {
        self.authImageView.image = [UIImage imageNamed:@"icon_find_auth_off"];
    }
    
    [self.headerImageView uploadImageWithURL:model.friendInfo.avatar placeholder:nil callback:NULL];
    
    CGFloat constraintWidth = kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - CGRectGetWidth(self.headerImageView.frame) - _imageRight * 2 - CGRectGetWidth(self.moreButton.frame);
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(constraintWidth, [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize ageSize = [self.ageLabel size];
    CGSize signSize = [self.signLabel.text sizeWithCalcFont:self.signLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - (CGRectGetMinX(self.moreButton.frame) - (CGRectGetMaxX(self.headerImageView.frame) + _imageRight)), [NSString contentofHeightWithFont:self.signLabel.font])];
    
    CGFloat y = CGRectGetMinY(self.headerImageView.frame);
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + _imageRight, y, nameSize.width, nameSize.height);
    self.ageLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + _space_label, ageSize.width, ageSize.height);
    self.bindImageView.frame = CGRectMake(CGRectGetMaxX(self.ageLabel.frame) + 2, CGRectGetMinY(self.ageLabel.frame), ageSize.height, ageSize.height);
    self.authImageView.frame = CGRectMake(CGRectGetMaxX(self.bindImageView.frame), CGRectGetMinY(self.bindImageView.frame), ageSize.height, ageSize.height);
    self.signLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.ageLabel.frame) + _space_label, signSize.width, signSize.height);
}

@end
