//
//  PDReceiveTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDReceiveTableViewCell.h"

@interface PDReceiveTableViewCell ()
@property (nonatomic, strong) UIImageView *imgView;
@end

@implementation PDReceiveTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self pageSetting];
    }
    return self;
}

+ (CGFloat)cellHeight {
    return 50.;
}

- (void)pageSetting {
    // 图片
    CGFloat imageHeight = 23;
    self.imgView = [[UIImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left * 2, ([PDReceiveTableViewCell cellHeight] - imageHeight) / 2, imageHeight, imageHeight)];
    [self.contentView addSubview:self.imgView];
    
    // 输入框
    CGFloat textHeight = 36.;
    CGFloat rowHeight = [PDReceiveTableViewCell cellHeight];
    UITextField *textField = [[UITextField alloc] init];
    textField.font = [UIFont systemFontOfCustomeSize:16];
    textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    textField.frame = CGRectMake(CGRectGetMaxX(self.imgView.frame) + LCFloat(12.), (rowHeight - textHeight) / 2, kScreenBounds.size.width - CGRectGetMinX(textField.frame) + kTableViewSectionHeader_left, textHeight);
    [self.contentView addSubview:textField];
    self.textField = textField;
}

- (void)setImg:(UIImage *)img {
    _img = img;
    
    self.imgView.image = img;
}

@end
