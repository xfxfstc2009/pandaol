//
//  PDReceiveTextCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDReceiveTextCell : UITableViewCell
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, strong) UITextField *textField;

+ (CGFloat)cellHeight;
@end
