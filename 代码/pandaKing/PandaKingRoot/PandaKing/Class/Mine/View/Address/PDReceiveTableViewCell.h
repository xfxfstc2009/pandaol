//
//  PDReceiveTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDReceiveTableViewCell : UITableViewCell
@property (nonatomic, strong) UIImage *img;
@property (nonatomic, strong) UITextField *textField;
+ (CGFloat)cellHeight;
@end
