//
//  PDReceiveAreaTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDReceiveAreaTableViewCell : UITableViewCell

@property (nonatomic, copy) NSString *name;

+ (CGFloat)cellHeight;
@end
