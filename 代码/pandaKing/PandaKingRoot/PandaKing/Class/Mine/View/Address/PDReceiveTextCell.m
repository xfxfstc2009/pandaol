//
//  PDReceiveTextCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDReceiveTextCell.h"

@interface PDReceiveTextCell ()
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIView *line;
@end

@implementation PDReceiveTextCell

+ (CGFloat)cellHeight {
    return 50;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _rowHeight = [PDReceiveTextCell cellHeight];
        
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    // desc
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:15];
    CGSize descSize = [@"最多四字" sizeWithCalcFont:self.descLabel.font];
    self.descLabel.frame = CGRectMake(kTableViewSectionHeader_left, (_rowHeight - descSize.height) / 2, descSize.width, descSize.height);
    self.descLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.descLabel];
    
    // line
    self.line = [[UIView alloc] init];
    self.line.backgroundColor = [UIColor lightGrayColor];
    self.line.frame = CGRectMake(CGRectGetMaxX(self.descLabel.frame) + LCFloat(12), 10, .5, (_rowHeight - 20));
    [self.contentView addSubview:self.line];
    
    // 输入
    CGFloat textHeight = 30;
    self.textField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.line.frame) + LCFloat(15), (_rowHeight - textHeight) / 2, kScreenBounds.size.width - CGRectGetMaxX(self.line.frame) - LCFloat(15) - kTableViewSectionHeader_left, textHeight)];
    self.textField.font = [UIFont systemFontOfCustomeSize:14];
    self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.contentView addSubview:self.textField];
}

- (void)setDesc:(NSString *)desc {
    _desc = desc;
    
    self.descLabel.text = desc;
}


@end
