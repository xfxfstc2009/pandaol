//
//  PDReceiveAreaTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDReceiveAreaTableViewCell.h"

@interface PDReceiveAreaTableViewCell ()
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *rightImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@end

@implementation PDReceiveAreaTableViewCell

+ (CGFloat)cellHeight {
    return 50;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _rowHeight = [PDReceiveAreaTableViewCell cellHeight];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    self.leftImageView = [[UIImageView alloc] init];
    self.leftImageView.image = [UIImage imageNamed:@"icon_backpack_fight"];
    [self.contentView addSubview:self.leftImageView];
    
    self.rightImageView = [[UIImageView alloc] init];
    self.rightImageView.image = [UIImage imageNamed:@"icon_backpack_pulldown"];
    [self.contentView addSubview:self.rightImageView];
}

- (void)setName:(NSString *)name {
    _name = name;
    
    self.nameLabel.text = name;
    CGSize nameSize = [name sizeWithCalcFont:self.nameLabel.font];
    CGSize imageSize = CGSizeMake(nameSize.height, nameSize.height);
    CGFloat space = LCFloat(15);
    
    self.nameLabel.frame = CGRectMake((kScreenBounds.size.width - nameSize.width) / 2, (_rowHeight - nameSize.height) / 2, nameSize.width, nameSize.height);
    self.leftImageView.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame) - space - imageSize.width, (_rowHeight - imageSize.height) / 2, imageSize.width, imageSize.height);
    self.rightImageView.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + space, (_rowHeight - imageSize.height) / 2, imageSize.width, imageSize.height);
}

@end
