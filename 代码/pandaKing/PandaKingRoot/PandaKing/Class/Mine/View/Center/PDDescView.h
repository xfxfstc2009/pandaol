//
//  PDDescView.h
//  PandaKing
//
//  Created by Cranz on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 上下两个label排布的View，在个人中心角色胜率、KDA、之类的地方用到
@interface PDDescView : UIView
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *subDesc;
@property (nonatomic, strong) UIColor *descColor;
@property (nonatomic, strong) UIColor *subDescColor;

/** 用这个方法创建*/
- (instancetype)initWithFrame:(CGRect)frame;
@end
