//
//  PDCenterLabel.h
//  PandaKing
//
//  Created by Cranz on 16/9/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PDCenterLabelStyle) {
    PDCenterLabelStyleDefault,  // 默认左图右侧文字
    PDCenterLabelStyleReverse,  // 左文字右图
};
/// 左图右侧文字，字体默认13
@interface PDCenterLabel : UIView
@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy)   NSString *text;
@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, strong) UIFont *textFont;
@property (nonatomic, assign) UIEdgeInsets contentInsets;
@property (nonatomic, assign) PDCenterLabelStyle style;

/** 在有图片和文字的时候，再来获取大小*/
- (CGSize)size;
@end
