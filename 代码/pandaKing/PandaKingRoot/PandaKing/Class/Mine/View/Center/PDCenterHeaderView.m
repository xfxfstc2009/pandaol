//
//  PDCenterHeaderView.m
//  PandaKing
//
//  Created by Cranz on 16/9/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterHeaderView.h"
#import "PDButton.h"
#import "PDCenterLabel.h"

@interface PDCenterHeaderView ()
@property (nonatomic, assign) CGFloat buttonBarHeight;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat imageWidth;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) NSArray *imageArr;

@property (nonatomic, strong) UIView *buttonBar;

@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UIView *headerBorderView;
//@property (nonatomic, strong) UILabel *idLabel;
@property (nonatomic, strong) PDCenterLabel *goldLabel;  /**< 金币*/
@property (nonatomic, strong) PDCenterLabel *bambooLabel;   /**< 竹子*/
@property (nonatomic, strong) UIView *line;

@property (nonatomic, assign) CGSize textSize;
//@property (nonatomic, assign) CGSize idSize;
@property (nonatomic, assign) CGSize goldSize;
@property (nonatomic, assign) CGSize bambooSize;
@end

@implementation PDCenterHeaderView

+ (CGFloat)headerViewHeight {
    return LCFloat(280);
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    _buttonBarHeight = LCFloat(60);
    _size = CGSizeMake(CGRectGetWidth(self.frame), [PDCenterHeaderView headerViewHeight]);
    _imageWidth = LCFloat(96);
    
    if ([AccountModel sharedAccountModel].isShenhe) { // 在审核中
        self.titleArr = @[@"我的卡包"];
    } else {
        self.titleArr = @[@"我的钱包",@"竞猜记录",@"夺宝记录"];
    }
    
    self.imageArr = @[@"icon_center_wallet",@"icon_center_lottery",@"icon_center_snatch"];
}

- (void)pageSetting {
    self.backgroundColor = [UIColor clearColor];
    
    self.buttonBar = [[UIView alloc] initWithFrame:CGRectMake(0, _size.height - _buttonBarHeight, _size.width, _buttonBarHeight)];
    self.buttonBar.backgroundColor = [UIColor clearColor];
    [self addSubview:self.buttonBar];
    
    for (int i = 0; i < self.titleArr.count; i++) {
        PDButton *button = [PDButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(_size.width / self.titleArr.count * i, 0, _size.width / self.titleArr.count, _buttonBarHeight);
        button.tag = i;
        button.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:(.1 + i * 0.1)];
        button.titleLabel.font = [UIFont systemFontOfCustomeSize:11];
        [button setTitle:self.titleArr[i] forState:UIControlStateNormal];
        [button setTitleColor:c6 forState:UIControlStateNormal];
        button.imageView.image = [UIImage imageNamed:self.imageArr[i]];
        
        [button addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonBar addSubview:button];
    }
    
    // 名字
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:18];
    self.nameLabel.textColor = c26;
    [self addSubview:self.nameLabel];
    
    // 头像
    UIView *headerBorderView = [[UIView alloc] init];
    headerBorderView.backgroundColor = [UIColor colorWithWhite:1 alpha:.2];
    headerBorderView.layer.cornerRadius = _imageWidth / 2;
    headerBorderView.clipsToBounds = YES;
    [self addSubview:headerBorderView];
    self.headerBorderView = headerBorderView;
    
    self.headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake(LCFloat(5), LCFloat(5), _imageWidth - LCFloat(10), _imageWidth - LCFloat(10))];
    self.headerImageView.backgroundColor = [UIColor whiteColor];
    self.headerImageView.layer.cornerRadius = (_imageWidth - LCFloat(10)) / 2;
    self.headerImageView.clipsToBounds = YES;
    self.headerImageView.userInteractionEnabled = YES;
    [headerBorderView addSubview:self.headerImageView];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickHeader:)];
    [self.headerImageView addGestureRecognizer:tap];
    
    // 金币
    self.goldLabel = [[PDCenterLabel alloc] init];
    self.goldLabel.image = [UIImage imageNamed:@"icon_center_gold"];
    [self addSubview:self.goldLabel];
    
    // 竖线
    self.line = [[UIView alloc] init];
    self.line.backgroundColor = c26;
    [self addSubview:self.line];
    
    // 竹子
    self.bambooLabel = [[PDCenterLabel alloc] init];
    self.bambooLabel.image = [UIImage imageNamed:@"icon_center_bamboo"];
    [self addSubview:self.bambooLabel];
}

#pragma mark - 代理方法

- (void)didClickButton:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(centerHeaderView:didClickButtonAtIndex:)]) {
        [self.delegate centerHeaderView:self didClickButtonAtIndex:button.tag];
    }
}

- (void)didClickHeader:(UITapGestureRecognizer *)tap {
    if ([self.delegate respondsToSelector:@selector(centerHeaderView:didClickRoleHeaderView:)]) {
        [self.delegate centerHeaderView:self didClickRoleHeaderView:(UIImageView *)tap.view];
    }
}

#pragma mark - 更新子视图frame

- (void)updateSubViews {
    // 除去按钮Bar的高度
    CGFloat height = CGRectGetHeight(self.frame) - _buttonBarHeight;
    
    // 顶部 和 底部高度不变
    
//    [self updateSubViewsWithOffsetY:0];
    
    //线和centerLabel之间的距离为15
    //centerLabel距离底部20
    //线宽1
    
    
    self.line.frame = CGRectMake((kScreenBounds.size.width - 1) / 2, height - LCFloat(20) - _goldSize.height + LCFloat(1), 1, _goldSize.height + LCFloat(2) * 2);
    self.goldLabel.frame = CGRectMake(CGRectGetMinX(self.line.frame) - LCFloat(15) - _goldSize.width, CGRectGetMinY(self.line.frame) + LCFloat(1), _goldSize.width, _goldSize.height);
    self.bambooLabel.frame = CGRectMake(CGRectGetMaxX(self.line.frame) + LCFloat(15), CGRectGetMinY(self.goldLabel.frame), _bambooSize.width, _bambooSize.height);
    
//    self.idLabel.frame = CGRectMake((_size.width - _idSize.width) / 2, CGRectGetMinY(self.line.frame) - LCFloat(5) - _idSize.height, _idSize.width, _idSize.height);
    
    self.nameLabel.frame = CGRectMake((_size.width - _textSize.width) / 2, CGRectGetMinY(self.line.frame) - LCFloat(5) - _textSize.height, _textSize.width, _textSize.height);
    self.headerBorderView.frame = CGRectMake((_size.width - _imageWidth) / 2, CGRectGetMinY(self.nameLabel.frame) - LCFloat(9) - _imageWidth, _imageWidth, _imageWidth);
    
}

- (void)updateSubViewsWithOffsetY:(CGFloat)offsetY {
//    self.nameLabel.frame = CGRectMake((_size.width - _textSize.width) / 2, CGRectGetMinY(self.line.frame) - LCFloat(5) - _textSize.height, _textSize.width, _textSize.height);
//    self.headerBorderView.frame = CGRectMake((_size.width - _imageWidth) / 2, CGRectGetMinY(self.nameLabel.frame) - LCFloat(9) - _imageWidth, _imageWidth, _imageWidth);
}

#pragma mark - set

- (void)setMyInfo:(PDCenterMyInfo *)myInfo {
    _myInfo = myInfo;
    
    self.nameLabel.text = myInfo.member.nickname;
    
    [self.headerImageView uploadImageWithURL:myInfo.member.avatar placeholder:nil callback:NULL];
    
    self.goldLabel.text = [PDCenterTool numberStringChangeWithoutSpecialCharacter:myInfo.gold];
    self.bambooLabel.text = [PDCenterTool numberStringChangeWithoutSpecialCharacter:myInfo.bamboo];
    
    self.textSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font];
    self.goldSize = [self.goldLabel size];
    self.bambooSize = [self.bambooLabel size];
    
    [self updateSubViews];
}

@end
