//
//  PDAddRoleTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDAddRoleTableViewCell.h"

@interface PDAddRoleTableViewCell ()
@property (nonatomic, strong) UIImageView *imgView;
@property (nonatomic, strong) UILabel *titleLabel;

@property (nonatomic, assign) CGFloat imgWidth;
@property (nonatomic, assign) CGFloat space;
@end

@implementation PDAddRoleTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    _imgWidth = 20;
    _space = 10;    // 图片距离label的空隙
    
    
    self.imgView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.imgView];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:16];
    self.textLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.titleLabel];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = title;
    
    CGSize size = [title sizeWithCalcFont:self.titleLabel.font];
    self.titleLabel.frame = CGRectMake((kScreenBounds.size.width - size.width) / 2 + _space, ([PDAddRoleTableViewCell cellHeight] - size.height) / 2, size.width, size.height);
    self.imgView.frame = CGRectMake(CGRectGetMinX(self.titleLabel.frame) - _space - _imgWidth, ([PDAddRoleTableViewCell cellHeight] - _imgWidth) / 2, _imgWidth, _imgWidth);
}

- (void)setImage:(UIImage *)image {
    _image = image;
    self.imgView.image = image;
}

+ (CGFloat)cellHeight {
    return 44;
}

@end
