//
//  PDAddRoleTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDAddRoleTableViewCell : UITableViewCell
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy)   NSString *title;
+ (CGFloat)cellHeight;
@end
