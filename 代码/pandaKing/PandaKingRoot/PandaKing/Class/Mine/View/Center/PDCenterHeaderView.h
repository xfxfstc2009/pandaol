//
//  PDCenterHeaderView.h
//  PandaKing
//
//  Created by Cranz on 16/9/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterMyInfo.h"

@protocol PDCenterHeaderViewDelegate;
@interface PDCenterHeaderView : UIView
@property (nonatomic, strong) PDCenterMyInfo *myInfo;
@property (nonatomic, weak) id<PDCenterHeaderViewDelegate> delegate;

/** 在拖动tableView的时候，更新子视图的frame*/
- (void)updateSubViewsWithOffsetY:(CGFloat)offsetY;
+ (CGFloat)headerViewHeight;
@end

@protocol PDCenterHeaderViewDelegate <NSObject>

- (void)centerHeaderView:(PDCenterHeaderView *)headerView didClickButtonAtIndex:(NSInteger)index;
- (void)centerHeaderView:(PDCenterHeaderView *)headerView didClickRoleHeaderView:(UIImageView *)imageView;

@end