//
//  PDDescView.m
//  PandaKing
//
//  Created by Cranz on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDDescView.h"

@interface PDDescView ()
@property (nonatomic, strong) UILabel *topLabel;
@property (nonatomic, strong) UILabel *bottomLabel;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat spaceLabel_label;
@end

@implementation PDDescView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    _size = self.frame.size;
    _spaceLabel_label = 2;
}

- (void)pageSetting {
    self.topLabel = [[UILabel alloc] init];
    self.topLabel.font = [UIFont systemFontOfSize:14];
    self.topLabel.textColor = [UIColor blackColor];
    self.topLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.topLabel];
    
    self.bottomLabel = [[UILabel alloc] init];
    self.bottomLabel.font = [UIFont systemFontOfSize:12];
    self.bottomLabel.textColor = [UIColor lightGrayColor];
    self.bottomLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.bottomLabel];
}

- (void)setDesc:(NSString *)desc {
    _desc = desc;
    self.topLabel.text = desc;
    
    CGSize topSize = [self.topLabel.text sizeWithCalcFont:self.topLabel.font constrainedToSize:CGSizeMake(_size.width, [NSString contentofHeightWithFont:self.topLabel.font])];
    CGSize bottomSize = [self.bottomLabel.text sizeWithCalcFont:self.bottomLabel.font constrainedToSize:CGSizeMake(_size.width, [NSString contentofHeightWithFont:self.bottomLabel.font])];
    CGFloat y = (_size.height - topSize.height - _spaceLabel_label - bottomSize.height) / 2;
    
    self.topLabel.frame = CGRectMake((_size.width - topSize.width) / 2, y, topSize.width, topSize.height);
    self.bottomLabel.frame = CGRectMake((_size.width - bottomSize.width) / 2, CGRectGetMaxY(self.topLabel.frame) + _spaceLabel_label, bottomSize.width, bottomSize.height);
}

- (void)setSubDesc:(NSString *)subDesc {
    _subDesc = subDesc;
    self.bottomLabel.text = subDesc;
    
    CGSize topSize = [self.topLabel.text sizeWithCalcFont:self.topLabel.font constrainedToSize:CGSizeMake(_size.width, [NSString contentofHeightWithFont:self.topLabel.font])];
    CGSize bottomSize = [self.bottomLabel.text sizeWithCalcFont:self.bottomLabel.font constrainedToSize:CGSizeMake(_size.width, [NSString contentofHeightWithFont:self.bottomLabel.font])];
    CGFloat y = (_size.height - CGRectGetHeight(self.topLabel.frame) - _spaceLabel_label - bottomSize.height) / 2;
    
    self.topLabel.frame = CGRectMake((_size.width - topSize.width) / 2, y, topSize.width, topSize.height);
    self.bottomLabel.frame = CGRectMake((_size.width - bottomSize.width) / 2, CGRectGetMaxY(self.topLabel.frame) + _spaceLabel_label, bottomSize.width, bottomSize.height);
}

- (void)setDescColor:(UIColor *)descColor {
    _descColor = descColor;
    
    self.topLabel.textColor = descColor;
}

- (void)setSubDescColor:(UIColor *)subDescColor {
    _subDescColor = subDescColor;
    
    self.bottomLabel.textColor = subDescColor;
}

@end
