//
//  PDCenterRoleTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterGameInfo.h"
/// 绑定的角色召唤师
@interface PDCenterRoleTableViewCell : UITableViewCell
@property (nonatomic, strong) PDCenterGameInfo *model;

+ (CGFloat)cellHeight;
@end
