//
//  PDButton.m
//  text
//
//  Created by Cranz on 16/9/9.
//  Copyright © 2016年 盼达. All rights reserved.
//

#import "PDButton.h"

@implementation PDButton

- (void)layoutSubviews {
    CGFloat width = self.frame.size.width;
    CGFloat height = self.frame.size.height;
    CGFloat space = 5, y;
    
    CGSize textSize = [self.titleLabel.text sizeWithAttributes:@{NSFontAttributeName:self.titleLabel.font}];
    CGSize imageSize = CGSizeMake(self.imageView.image.size.width, self.imageView.image.size.height);
    y = (height - textSize.height - imageSize.height - space) / 2;
    
    self.imageView.frame = CGRectMake((width - imageSize.width) / 2, y, imageSize.width, imageSize.height);
    self.titleLabel.frame = CGRectMake((width - textSize.width) / 2, CGRectGetMaxY(self.imageView.frame) + space, textSize.width, textSize.height);
    
}

@end
