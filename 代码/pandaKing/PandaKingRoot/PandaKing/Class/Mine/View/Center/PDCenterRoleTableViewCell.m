//
//  PDCenterRoleTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterRoleTableViewCell.h"
#import "PDDescView.h"

#define kCenterRoleHeightTop        LCFloat(65)
#define kCenterRoleHeightBottom     LCFloat(46)
@interface PDCenterRoleTableViewCell ()
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *areaLabel;   /**< 角色大区*/
@property (nonatomic, strong) UIImageView *activityImageView;   /**< 激活状态*/
@property (nonatomic, strong) PDDescView *rateView;     /**< 胜率*/
@property (nonatomic, strong) PDDescView *kdaView;      /**< KDA*/
@property (nonatomic, strong) PDDescView *levelView;    /**< 段位*/
@property (nonatomic, strong) PDDescView *numView;      /**< 场次*/

@property (nonatomic, assign) CGFloat imageTop;
@property (nonatomic, assign) CGFloat imageRight;   /**< 头像右侧*/
@end

@implementation PDCenterRoleTableViewCell

+ (CGFloat)cellHeight {
    return kCenterRoleHeightBottom + kCenterRoleHeightTop;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _imageTop = LCFloat(5);
    _imageRight = LCFloat(8);
}

- (void)pageSetting {
    // 左侧黑色小块
    CGFloat leftViewHeight = LCFloat(15);
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, leftViewHeight, 5, kCenterRoleHeightTop - leftViewHeight * 2)];
    leftView.backgroundColor = [UIColor blackColor];
    [self.contentView addSubview:leftView];
    
    // header
    CGFloat imageWidth = kCenterRoleHeightTop - 2 * _imageTop;
    self.headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake(20, _imageTop, imageWidth, imageWidth)];
    self.headerImageView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.headerImageView.layer.cornerRadius = imageWidth / 2;
    self.headerImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.headerImageView];
    
    // name
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    // area
    self.areaLabel = [[UILabel alloc] init];
    self.areaLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.areaLabel.textColor = [UIColor grayColor];
    [self.contentView addSubview:self.areaLabel];
    
    // 线
    UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, kCenterRoleHeightTop - .5, kScreenBounds.size.width, .5)];
    hLine.backgroundColor = c27;
    [self.contentView addSubview:hLine];
    
    // activityView
    CGFloat activityWidth = LCFloat(53);
    CGFloat activityHeight = LCFloat(39);
    self.activityImageView = [[UIImageView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width - activityWidth, 0, activityWidth, activityHeight)];
    [self.contentView addSubview:self.activityImageView];
    
    for (int i = 0; i < 4; i++) {
        CGFloat descViewWidth = kScreenBounds.size.width / 4;
        PDDescView *descView = [[PDDescView alloc] initWithFrame:CGRectMake(i * descViewWidth, kCenterRoleHeightTop, descViewWidth, kCenterRoleHeightBottom)];
        descView.descColor = RGB(95, 57, 23, 1);
        [self.contentView addSubview:descView];
        
        if (i == 0) {
            self.rateView = descView;
//            descView.subDesc = @"胜率";
        } else if (i == 1) {
            self.kdaView = descView;
//            descView.subDesc = @"KDA";
        } else if (i == 2) {
            self.levelView = descView;
//            descView.subDesc = @"段位";
        } else {
            self.numView = descView;
//            descView.subDesc = @"总场次";
        }
        
        if (i == 0) {
            continue;
        }
        
        // 三条竖线
        UIView *vLine = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMinX(descView.frame), 12 + kCenterRoleHeightTop, 1, kCenterRoleHeightBottom - 12 * 2)];
        vLine.backgroundColor = c27;
        [self.contentView addSubview:vLine];
    }
}

- (void)setModel:(PDCenterGameInfo *)model {
    _model = model;
    
    [self.headerImageView uploadImageWithAvatarURL:[PDCenterTool absoluteUrlWithRisqueUrl:model.memberLOLGameUser.avatar] placeholder:nil callback:NULL];
    self.nameLabel.text = model.memberLOLGameUser.lolGameUser.roleName;
    if ([model.memberLOLGameUser.lolGameUser.telecomLine isEqualToString:@"CT"]) {
        self.areaLabel.text = [NSString stringWithFormat:@"%@(电信)",model.memberLOLGameUser.lolGameUser.serverName];
    } else if ([model.memberLOLGameUser.lolGameUser.telecomLine isEqualToString:@"CM"]) {
        self.areaLabel.text = [NSString stringWithFormat:@"%@(网通)",model.memberLOLGameUser.lolGameUser.serverName];
    } else {
        self.areaLabel.text = [NSString stringWithFormat:@"%@(其他)",model.memberLOLGameUser.lolGameUser.serverName];
    }
//    self.activityImageView.image = model.memberLOLGameUser.enabled? [UIImage imageNamed:@"icon_center_avticity"] : [UIImage imageNamed:@"icon_center_unavticity"];
//    
    if ([model.memberLOLGameUser.state isEqualToString:@"activating"]){       // 激活中
        self.activityImageView.image = [UIImage imageNamed:@"icon_panda_activating"] ;
    } else if ([model.memberLOLGameUser.state isEqualToString:@"bind"]){      // 未绑定
        self.activityImageView.image = [UIImage imageNamed:@"icon_panda_activation_nor"] ;
    } else if ([model.memberLOLGameUser.state isEqualToString:@"activated"]){ // 已认证
        self.activityImageView.image = [UIImage imageNamed:@"icon_panda_activation_hlt"] ;
    }
    
    self.rateView.desc = [NSString stringWithFormat:@"%.1lf％",(model.stats.winRate * 100)];
    
    self.kdaView.desc = [NSString stringWithFormat:@"%.1lf",model.stats.kda.floatValue];
    switch (model.memberLOLGameUser.lolGameUser.grade) {
        case 255:
            self.levelView.desc = @"未定位";
            break;
        case 0:
            self.levelView.desc = @"最强王者";
            break;
        case 1:
            self.levelView.desc = @"钻石";
            break;
        case 2:
            self.levelView.desc = @"白金";
            break;
        case 3:
            self.levelView.desc = @"黄金";
            break;
        case 4:
            self.levelView.desc = @"白银";
            break;
        case 5:
            self.levelView.desc = @"青铜";
            break;
        case 6:
            self.levelView.desc = @"超凡大师";
            break;
        default:
            break;
    }
    
    switch (model.memberLOLGameUser.lolGameUser.gradeLevel) {
        case 1:
            self.levelView.desc = [NSString stringWithFormat:@"%@I",self.levelView.desc];
            break;
        case 2:
            self.levelView.desc = [NSString stringWithFormat:@"%@II",self.levelView.desc];
            break;
        case 3:
            self.levelView.desc = [NSString stringWithFormat:@"%@III",self.levelView.desc];
            break;
        case 4:
            self.levelView.desc = [NSString stringWithFormat:@"%@IV",self.levelView.desc];
            break;
        case 5:
            self.levelView.desc = [NSString stringWithFormat:@"%@V",self.levelView.desc];
            break;
        default:
            break;
    }
    
    self.numView.desc = [NSString stringWithFormat:@"%ld",(long)model.stats.gameCount];
    ///
    self.rateView.subDesc = @"胜率";
    self.kdaView.subDesc = @"KDA";
    self.levelView.subDesc = @"段位";
    self.numView.subDesc = @"总场次";
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(self.headerImageView.frame) - _imageRight * 2 - CGRectGetWidth(self.activityImageView.frame), [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize areaSize = [self.areaLabel.text sizeWithCalcFont:self.areaLabel.font];
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + _imageRight, CGRectGetMinY(self.headerImageView.frame) + LCFloat(5), nameSize.width, nameSize.height);
    self.areaLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), 5 + CGRectGetMaxY(self.nameLabel.frame), areaSize.width, areaSize.height);
}

@end


