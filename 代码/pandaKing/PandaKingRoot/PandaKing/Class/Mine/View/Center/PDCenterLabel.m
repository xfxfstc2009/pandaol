//
//  PDCenterLabel.m
//  PandaKing
//
//  Created by Cranz on 16/9/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterLabel.h"

@interface PDCenterLabel ()
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UIImageView *imageView;
@property (nonatomic, strong) UILabel *textLabel;

@property (nonatomic, assign) CGFloat space;
@end

@implementation PDCenterLabel

- (instancetype)init {
    self = [super init];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    _space = 5;
    
    self.backgroundImageView = [[UIImageView alloc] init];
    [self addSubview:self.backgroundImageView];
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.textLabel.textColor = c26;
    [self addSubview:self.textLabel];
    
    self.imageView = [[UIImageView alloc] init];
    [self addSubview:self.imageView];
}

- (void)layoutSubviews {
    self.backgroundImageView.frame = self.bounds;
}

- (void)setImage:(UIImage *)image {
    _image = image;
    self.imageView.image = image;
}

- (void)setText:(NSString *)text {
    _text = text;
    self.textLabel.text = text;
    
    CGSize size = [text sizeWithCalcFont:self.textLabel.font];
    CGSize imageSize = CGSizeMake(size.height, size.height);    // 图片实际比字大
    
    self.imageView.frame = CGRectMake(self.style == PDCenterLabelStyleDefault?self.contentInsets.left : self.contentInsets.left + size.width + _space, self.contentInsets.top, imageSize.width, imageSize.height);
    self.textLabel.frame = CGRectMake(self.style == PDCenterLabelStyleDefault?CGRectGetMaxX(self.imageView.frame) + _space : self.contentInsets.left, self.contentInsets.top, size.width, size.height);
}

- (void)setTextColor:(UIColor *)textColor {
    _textColor = textColor;
    
    self.textLabel.textColor = textColor;
}

- (void)setTextFont:(UIFont *)textFont {
    _textFont = textFont;
    self.textLabel.font = textFont;
    
    CGSize size = [self.textLabel.text sizeWithCalcFont:self.textLabel.font];
    CGSize imageSize = CGSizeMake(size.height, size.height);
    self.imageView.frame = CGRectMake(self.style == PDCenterLabelStyleDefault?self.contentInsets.left : self.contentInsets.left + size.width + _space, self.contentInsets.top, imageSize.width, imageSize.height);
    self.textLabel.frame = CGRectMake(self.style == PDCenterLabelStyleDefault?CGRectGetMaxX(self.imageView.frame) + _space : self.contentInsets.left, self.contentInsets.top, size.width, size.height);
}

- (void)setContentInsets:(UIEdgeInsets)contentInsets {
    _contentInsets = contentInsets;
    
    CGSize size = [self.textLabel.text sizeWithCalcFont:self.textLabel.font];
    CGSize imageSize = CGSizeMake(LCFloat(size.height), LCFloat(size.height));
    self.imageView.frame = CGRectMake(self.style == PDCenterLabelStyleDefault?self.contentInsets.left : self.contentInsets.left + size.width + _space, contentInsets.top, imageSize.width, imageSize.height);
    self.textLabel.frame = CGRectMake(self.style == PDCenterLabelStyleDefault?CGRectGetMaxX(self.imageView.frame) + _space : self.contentInsets.left, contentInsets.top, size.width, size.height);
}

- (void)setStyle:(PDCenterLabelStyle)style {
    _style = style;
    
    CGSize size = [self.textLabel.text sizeWithCalcFont:self.textLabel.font];
    CGSize imageSize = CGSizeMake(size.height, size.height);
    self.imageView.frame = CGRectMake(self.style == PDCenterLabelStyleDefault?self.contentInsets.left : self.contentInsets.left + size.width + _space, self.contentInsets.top, imageSize.width, imageSize.height);
    self.textLabel.frame = CGRectMake(self.style == PDCenterLabelStyleDefault?CGRectGetMaxX(self.imageView.frame) + _space : self.contentInsets.left, self.contentInsets.top, size.width, size.height);
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    _backgroundImage = backgroundImage;
    
    self.backgroundImageView.image = backgroundImage;
}

- (CGSize)size {
    return CGSizeMake(CGRectGetWidth(self.imageView.frame) + _space + CGRectGetWidth(self.textLabel.frame) + self.contentInsets.left + self.contentInsets.right, CGRectGetHeight(self.textLabel.frame) + self.contentInsets.top + self.contentInsets.bottom);
}


@end
