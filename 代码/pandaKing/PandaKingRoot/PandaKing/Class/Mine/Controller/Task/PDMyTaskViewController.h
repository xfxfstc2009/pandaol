//
//  PDMyTaskViewController.h
//  PandaKing
//
//  Created by Cranz on 16/12/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDCenterGameInfo.h"

/// 我的任务
@interface PDMyTaskViewController : AbstractViewController
@property (nonatomic, strong) PDCenterGameInfo *gameInfo; // 传递的游戏角色信息
@end
