//
//  PDMyTaskViewController.m
//  PandaKing
//
//  Created by Cranz on 16/12/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMyTaskViewController.h"
#import "PDCenterLabel.h"
#import "PDMyTaskHeaderView.h"
#import "PDMyTaskCell.h"
#import "PDTaskList.h"
#import "PDCenterMyInfo.h"
#import "PDMyTaskViewController+PDTaskManager.h"
#import "PDShopRootMainViewController.h" // 福利中心

@interface PDMyTaskViewController ()<UITableViewDataSource, UITableViewDelegate, PDMyTaskCellDelegate>
@property (nonatomic, strong) UIView *personalInfoView;
@property (nonatomic, strong) PDImageView *headerView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) PDCenterLabel *goldLabel;
@property (nonatomic, strong) UILabel *bambooLabel;
@property (nonatomic, strong) PDCenterLabel *tosnatchLabel; // 去夺宝
@property (nonatomic, assign) int tosnatchHeight;
@property (nonatomic, strong) UITableView *mainTableView;

// 任务model
@property (nonatomic, strong) PDTaskList *taskList;

@end

@implementation PDMyTaskViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor clearColor].CGColor;
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
    [self fetchMemberInfo];
}

- (void)basicSetting {
    self.barMainTitle = @"我的任务";
    self.tosnatchHeight = 35;
}

- (void)pageSetting {
    [self headerViewSetting];
    [self taskViewSetting];
}

- (void)headerViewSetting {
    // person info
    self.personalInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 100 + self.tosnatchHeight)];
    self.personalInfoView.backgroundColor = c2;
    [self.view addSubview:self.personalInfoView];
    UIImageView *imageBg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 100)];
    imageBg.image = [UIImage imageNamed:@"icon_tasnatch_bg"];
    [self.personalInfoView addSubview:imageBg];
    
    // header
    CGFloat imageWidth = 65;
    self.headerView = [[PDImageView alloc] initWithFrame:CGRectMake(25, (CGRectGetHeight(self.personalInfoView.frame) - self.tosnatchHeight - imageWidth) / 2, imageWidth, imageWidth)];
    self.headerView.layer.cornerRadius = imageWidth / 2;
    self.headerView.clipsToBounds = YES;
    [self.personalInfoView addSubview:self.headerView];
    
    // name
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [[UIFont systemFontOfCustomeSize:18] boldFont];
    self.nameLabel.textColor = c26;
    [self.personalInfoView addSubview:self.nameLabel];
    
    // 金币
    self.goldLabel = [[PDCenterLabel alloc] init];
    self.goldLabel.textFont = [UIFont systemFontOfCustomeSize:13];
    self.goldLabel.textColor = self.nameLabel.textColor;
    [self.personalInfoView addSubview:self.goldLabel];
    
    // 我的竹子 －>去夺宝
    UIImageView *bambooBgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 100, kScreenBounds.size.width, self.tosnatchHeight)];
    bambooBgImageView.userInteractionEnabled = YES;
    bambooBgImageView.image = [UIImage imageNamed:@"bg_task_tosnatch"];
    [self.personalInfoView addSubview:bambooBgImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didGoSnatch)];
    [bambooBgImageView addGestureRecognizer:tap];
    
    // 竹子
    self.bambooLabel = [[UILabel alloc] init];
    self.bambooLabel.textColor = c1;
    self.bambooLabel.font = [UIFont systemFontOfCustomeSize:14];
    [bambooBgImageView addSubview:self.bambooLabel];
    
    // 去夺宝
    self.tosnatchLabel = [[PDCenterLabel alloc] init];
    self.tosnatchLabel.style = PDCenterLabelStyleReverse;
    self.tosnatchLabel.textColor = c1;
    self.tosnatchLabel.textFont = [UIFont systemFontOfCustomeSize:14];
    [bambooBgImageView addSubview:self.tosnatchLabel];
}

- (void)taskViewSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.personalInfoView.frame), kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetHeight(self.personalInfoView.frame))];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        [weakSelf fetchData];
    }];
}

/** 更新label的frame*/
- (void)updateFrame {
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMinX(self.nameLabel.frame) - kTableViewSectionHeader_left, [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize goldSize = [self.goldLabel size];
    CGSize bambooSize = [self.bambooLabel.text sizeWithCalcFont:self.bambooLabel.font];
    CGSize tosnatchSize = [self.tosnatchLabel size];
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerView.frame) + LCFloat(15), CGRectGetMinX(self.headerView.frame) + 5, nameSize.width, nameSize.height);
    self.goldLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + 12, goldSize.width, goldSize.height);
    self.bambooLabel.frame = CGRectMake(kTableViewSectionHeader_left, (self.tosnatchHeight - bambooSize.height) / 2, bambooSize.width, bambooSize.height);
    self.tosnatchLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - tosnatchSize.width, (self.tosnatchHeight - tosnatchSize.height) / 2, tosnatchSize.width, tosnatchSize.height);
}

#pragma mark - 前往夺宝

- (void)didGoSnatch {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[PDMainTabbarViewController sharedController] setSelectedIndex:1 animated:YES];
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.taskList.dayLoopTask.items.count;
    } else if (section == 1) {
        return self.taskList.newbieTask.items.count;
    } else {
        return self.taskList.archieveTask.items.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"taskCellId";
    PDMyTaskCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[PDMyTaskCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.delegate = self;
    }
    cell.indexPath = indexPath;
    
    if (indexPath.section == 0) {
        if (indexPath.row < self.taskList.dayLoopTask.items.count) {
            PDTaskObject *taskObj = self.taskList.dayLoopTask.items[indexPath.row];
            cell.model = taskObj;
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row < self.taskList.newbieTask.items.count) {
            PDTaskObject *taskObj = self.taskList.newbieTask.items[indexPath.row];
            cell.model = taskObj;
        }
    } else {
        if (indexPath.row < self.taskList.archieveTask.items.count) {
            PDTaskObject *taskObj = self.taskList.archieveTask.items[indexPath.row];
            cell.model = taskObj;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (indexPath.row < self.taskList.dayLoopTask.items.count) {
            PDTaskObject *taskObj = self.taskList.dayLoopTask.items[indexPath.row];
            return [PDMyTaskCell cellHeightWithModel:taskObj];
        }
    } else if (indexPath.section == 1) {
        if (indexPath.row < self.taskList.newbieTask.items.count) {
            PDTaskObject *taskObj = self.taskList.newbieTask.items[indexPath.row];
            return [PDMyTaskCell cellHeightWithModel:taskObj];
        }
    } else {
        if (indexPath.row < self.taskList.archieveTask.items.count) {
            PDTaskObject *taskObj = self.taskList.archieveTask.items[indexPath.row];
            return [PDMyTaskCell cellHeightWithModel:taskObj];
        }
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewSectionHeader_height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *headerId = @"task_headerView_id";
    PDMyTaskHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerId];
    if (!headerView) {
        headerView = [[PDMyTaskHeaderView alloc] initWithReuseIdentifier:headerId];
    }
    NSString *name = @"";
    NSUInteger finished, total;
    if (section == 0) {
        name = @"每日任务";
        finished = self.taskList.dayLoopTask.doneCount;
        total = self.taskList.dayLoopTask.totalCount;
    } else if (section == 1) {
        name = @"新手任务";
        finished = self.taskList.newbieTask.doneCount;
        total = self.taskList.newbieTask.totalCount;
    } else {
        name = @"成就任务";
        finished = self.taskList.archieveTask.doneCount;
        total = self.taskList.archieveTask.totalCount;
    }
    [headerView updateTaskWithTitle:name finishedCount:finished totalCount:total];
    return headerView;
}

#pragma mark - taskCell delegate

- (void)taskCell:(PDMyTaskCell *)cell didClickMoreButton:(UIButton *)button {
    PDTaskObject *model = cell.model;
    if (model.done && !model.receiveAward) {
        [self fetchTaskAwardWithTaskId:model.ID taskType:model.type changeButtonTypeWithCell:cell];
    }
    if (!model.done) { // 去完成
        [self taskJumpManagerWithType:model.jumpType];
    }
}

#pragma mark - 网络请求

/** 获取任务*/
- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMyTask requestParams:nil responseObjectClass:[PDTaskList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDTaskList *taskList = (PDTaskList *)responseObject;
            weakSelf.taskList = taskList;
            [weakSelf.mainTableView reloadData];
            [weakSelf.mainTableView stopPullToRefresh];
        }
    }];
}

- (void)fetchMemberInfo {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:myInfo requestParams:nil responseObjectClass:[PDCenterMyInfo class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
    
        if (isSucceeded) {
            PDCenterMyInfo *myInfo = (PDCenterMyInfo *)responseObject;
            [weakSelf.headerView uploadImageWithAvatarURL:myInfo.member.avatar placeholder:nil callback:nil];
            weakSelf.nameLabel.text = myInfo.member.nickname;
            weakSelf.goldLabel.text = [NSString stringWithFormat:@"%ld",myInfo.gold];
            weakSelf.goldLabel.image = [UIImage imageNamed:@"icon_center_gold"];
            weakSelf.bambooLabel.text = [NSString stringWithFormat:@"我的竹子 %ld",myInfo.bamboo];
            weakSelf.tosnatchLabel.text = @"去夺宝";
            weakSelf.tosnatchLabel.image = [UIImage imageNamed:@"icon_task_arrow"];
            [weakSelf updateFrame];
            
            [AccountModel sharedAccountModel].memberInfo.gold = myInfo.gold;
            [AccountModel sharedAccountModel].memberInfo.bamboo = myInfo.bamboo;
        }
    }];
}

/** 领取奖励*/
- (void)fetchTaskAwardWithTaskId:(NSString *)taskId taskType:(NSString *)type changeButtonTypeWithCell:(PDMyTaskCell *)taskCell {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerTaskAward requestParams:@{@"id":taskId, @"type":type} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            // 改变用户的财富
            [weakSelf fetchMemberInfo];
            // 改变任务状态
            taskCell.model.receiveAward = YES;
            [taskCell changeButtonType:PDTaskButtonTypeFinished];
            [PDHUD showHUDSuccess:@"领取成功！"];
        }
    }];
}


@end
