//
//  PDCenterViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 个人中心主页
@interface PDCenterViewController : AbstractViewController

+ (instancetype)sharedController;


- (void)fetchMemberGameUser;
-(void)removeMemberGameUser;

- (void)clearMemberInfo;

@end
