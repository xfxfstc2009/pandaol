//
//  PDReceiveRealViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 填写实物收货地址控制器
@interface PDReceiveRealViewController : AbstractViewController
@property (nonatomic, copy) NSString *ID;   /**< 兑换券id*/
- (void)infoDidSubmitComplication:(void(^)())complication;
@end
