//
//  PDReceiveMobileViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDReceiveRealViewController.h"
#import "PDReceiveTextCell.h"
#import "PDCenterTool.h"
#import "PDPickView.h"

// 地域划分
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>

typedef void(^submitBlock)();
@interface PDReceiveRealViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, AMapSearchDelegate, UIPickerViewDelegate, UIPickerViewDataSource>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSArray *placeholderArr;
@property (nonatomic, strong) NSArray *descArr;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, assign) CGFloat buttonSpace;

@property (nonatomic, strong) UITextField *textField1;  /**< 收货人*/
@property (nonatomic, strong) UITextField *textField2;  /**< 手机*/
@property (nonatomic, strong) UITextField *textField3;  /**< 详细地址*/
@property (nonatomic, strong) UITextField *textField4;  /**< 备注*/

@property (nonatomic, assign) BOOL hasLength1;
@property (nonatomic, assign) BOOL hasLength2;
@property (nonatomic, assign) BOOL hasLength3;
@property (nonatomic, assign) BOOL hasAddress;

@property (nonatomic, assign) BOOL isNeedToScroll;  /**< 当键盘遮住的时候，就要滚动tableView*/

// 地理搜索
// 地区搜索
@property (nonatomic, strong) AMapSearchAPI *search;
@property (nonatomic, strong) NSMutableArray *provinceArr;
@property (nonatomic, strong) NSMutableArray *cityArr;
@property (nonatomic, strong) NSMutableArray *districtArr;
@property (nonatomic, strong) UIPickerView *addressPickerView;
@property (nonatomic, strong) PDReceiveTextCell *addressCell; /**< 用来改变detailText*/
@property (nonatomic, assign) NSInteger selectedSection;

@property (nonatomic, copy) submitBlock submitBlock;
@end

@implementation PDReceiveRealViewController

- (void)dealloc {
    [NOTIFICENTER removeObserver:self name:kKEYBOARD_HIDE object:nil];
    [NOTIFICENTER removeObserver:self name:kKEYBOARD_SHOW object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _basicSetting];
    [self _pageSetting];
    [self addNotification];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.textField1 becomeFirstResponder];
}

- (void)_basicSetting {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.barMainTitle = @"填写收货信息";
    
    self.placeholderArr = @[@"请输入收货人姓名",@"请输入手机账号",@"",@"街道、小区",@"备注信息"];
    self.descArr = @[@"收货人",@"手机账号",@"所在地区",@"详细地址",@"备注信息"];
    
    _buttonSpace = LCFloat(30.);
    _isNeedToScroll = NO;
}

- (void)_pageSetting {
    [self tableViewSetting];
    [self buttonSetting];
}

- (void)tableViewSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.mainTableView.rowHeight = [PDReceiveTextCell cellHeight];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
}

- (void)buttonSetting {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(kTableViewSectionHeader_left, [PDReceiveTextCell cellHeight] * 5 + kTableViewSectionHeader_height + _buttonSpace, kScreenBounds.size.width - kTableViewSectionHeader_left * 2, 48.);
    button.enabled = NO;
    button.backgroundColor = [UIColor lightGrayColor];
    [button setTitle:@"确定" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
    [self.mainTableView addSubview:button];
    self.button = button;
    
    CGFloat x = LCFloat(50);
    UILabel *markLabel = [[UILabel alloc] init];
    markLabel.text = @"备注：请认真核对键入信息，如错误填写一律不予补发，商品一经兑换概不退换";
    markLabel.textColor = [UIColor redColor];
    markLabel.numberOfLines = 0;
    markLabel.textAlignment = NSTextAlignmentCenter;
    markLabel.font = [UIFont systemFontOfCustomeSize:13];
    CGSize size = [markLabel.text sizeWithCalcFont:markLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - x * 2, CGFLOAT_MAX)];
    markLabel.frame = CGRectMake(x, CGRectGetMaxY(button.frame) + LCFloat(15), size.width, size.height);
    [self.mainTableView addSubview:markLabel];
}

- (void)addNotification {
    [NOTIFICENTER addObserver:self selector:@selector(showKeyBoard:) name:kKEYBOARD_SHOW object:nil];
    [NOTIFICENTER addObserver:self selector:@selector(hideKeyBoard:) name:kKEYBOARD_HIDE object:nil];
}

- (void)infoDidSubmitComplication:(void (^)())complication {
    if (complication) {
        self.submitBlock = complication;
    }
}

#pragma mark -
#pragma mark -- 键盘监听

- (void)showKeyBoard:(NSNotification *)noti {

    // 键盘弹起之后的rect
    CGRect rect = [[noti.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    
    if (kScreenBounds.size.height - 64 - kTableViewSectionHeader_height - 5 * [PDReceiveTextCell cellHeight] < rect.size.height) {
        _isNeedToScroll = YES;
    }
}

- (void)hideKeyBoard:(NSNotification *)noti {
    
}

#pragma mark -
#pragma mark -- 控件方法

- (void)didClickButton {
    [self fetchData];
}

- (void)didChangeValue:(UITextField *)textField {
    if (textField == self.textField1) {
        if (textField.text.length > 0) {
            _hasLength1 = YES;
            if (_hasLength2 && _hasLength3 && _hasAddress) {
                [self buttonEnabled];
            }
        } else {
            _hasLength1 = NO;
            [self buttonUnEnabled];
        }
    } else if (textField == self.textField2) {
        if (textField.text.length > 0) {
            _hasLength2 = YES;
            if (_hasLength1 && _hasLength3 && _hasAddress) {
                [self buttonEnabled];
            }
        } else {
            _hasLength2 = NO;
            [self buttonUnEnabled];
        }
    } else if (textField == self.textField3) {
        if (textField.text.length) {
            _hasLength3 = YES;
            if (_hasLength1 && _hasLength2 && _hasAddress) {
                [self buttonEnabled];
            }
        } else {
            _hasLength3 = NO;
            [self buttonUnEnabled];
        }
    } else {
        if (textField.text.length > 0 && _hasLength3 && _hasLength2 && _hasLength1 && _hasAddress) {
            [self buttonEnabled];
        } else {
            [self buttonUnEnabled];
        }
    }
}

- (void)buttonEnabled {
    self.button.enabled = YES;
    self.button.backgroundColor = [UIColor blackColor];
}

- (void)buttonUnEnabled {
    self.button.enabled = NO;
    self.button.backgroundColor = [UIColor lightGrayColor];
}

#pragma mark -
#pragma mark -- UITabelView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.descArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"receiveCellId";
    PDReceiveTextCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[PDReceiveTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textField.delegate = self;
        [cell.textField addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventEditingChanged];
    }
    cell.desc = [self.descArr objectAtIndex:indexPath.row];
    cell.textField.placeholder = self.placeholderArr[indexPath.row];
    
    if (indexPath.row == 0) {
        self.textField1 = cell.textField;
        
    } else if (indexPath.row == 1) {
        cell.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.textField2 = cell.textField;
    } else if (indexPath.row == 2) {
        cell.textField.text = @"省－市－区";
        cell.textField.enabled = NO;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        self.addressCell = cell;
    } else if (indexPath.row == 3) {
        self.textField3 = cell.textField;
    } else {
        self.textField4 = cell.textField;
        self.textField4.returnKeyType = UIReturnKeyDone;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.descArr.count - 1) {
        [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    CGFloat label_bottom = 3;
    UILabel *label = [[UILabel alloc] init];
    label.text = @"请填写相关物品的收货信息";
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfCustomeSize:13];
    label.textColor = [UIColor grayColor];
    CGSize size = [label.text sizeWithCalcFont:label.font];
    label.frame = CGRectMake(kTableViewSectionHeader_left, kTableViewSectionHeader_height - size.height - label_bottom, size.width, size.height);
    [headerView addSubview:label];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewSectionHeader_height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellDesc = [self.descArr objectAtIndex:indexPath.row];
    if ([cellDesc isEqualToString:@"所在地区"]) {
        [self.view endEditing:YES];
        [self getMapArea];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [PDCenterTool tableViewScrolledRemoveViscousCancleWithScrollView:scrollView sectionHeight:kTableViewSectionHeader_height];
}

#pragma mark -
#pragma mark -- UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.textField4) {
        [self.view endEditing:YES];
        return YES;
    } else {
        return NO;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
    if (_isNeedToScroll) {
        if (textField == self.textField2) {
            [self.mainTableView setContentOffset:CGPointMake(0, (kTableViewSectionHeader_height + [PDReceiveTextCell cellHeight])) animated:YES];
        } else if (textField == self.textField3) {
            [self.mainTableView setContentOffset:CGPointMake(0, (kTableViewSectionHeader_height + [PDReceiveTextCell cellHeight] * 2)) animated:YES];
        } else if (textField == self.textField4) {
            [self.mainTableView setContentOffset:CGPointMake(0, (kTableViewSectionHeader_height + [PDReceiveTextCell cellHeight] * 3)) animated:YES];
        }
    }
}

#pragma mark -
#pragma mark -- 获取地理划分

- (NSMutableArray *)provinceArr {
    if (!_provinceArr) {
        _provinceArr = [NSMutableArray array];
    }
    return _provinceArr;
}

- (NSMutableArray *)cityArr {
    if (!_cityArr) {
        _cityArr = [NSMutableArray array];
    }
    return _cityArr;
}

- (NSMutableArray *)districtArr {
    if (!_districtArr) {
        _districtArr = [NSMutableArray array];
    }
    return _districtArr;
}

- (AMapSearchAPI *)search {
    if (!_search) {
        _search = [[AMapSearchAPI alloc] init];
        _search.delegate = self;
    }
    return _search;
}

- (void)getMapArea {
    _selectedSection = -1; //-1 刷新第一个
    
    AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
    [self.search AMapDistrictSearch:dist];
    
    PDAddressPickView *pickerView = [[PDAddressPickView alloc] initWithTitle:@"所在城市"];
    self.addressPickerView = pickerView.pickerView;
    self.addressPickerView.delegate = self;
    self.addressPickerView.dataSource = self;
    
    __weak typeof(self) weakSelf = self;
    [pickerView clickSure:^{
        
        [weakSelf getAddressDetail];
        
        // 清空另两个数组， 否则会显示错误，因为我抢饮用了pickerView
        if (weakSelf.cityArr.count) {
            [weakSelf.cityArr removeAllObjects];
        }
        if (weakSelf.districtArr.count) {
            [weakSelf.districtArr removeAllObjects];
        }

    }];
}

- (void)getAddressDetail {
    NSDictionary *provinceDic = self.provinceArr.count? self.provinceArr[[self.addressPickerView selectedRowInComponent:0]]:@{@"":@""};
//    NSString *proCode = provinceDic.allKeys.firstObject;
    NSString *proName = provinceDic.allValues.firstObject;
    
    NSDictionary *cityDic = self.cityArr.count? self.cityArr[[self.addressPickerView selectedRowInComponent:1]]:@{@"":@""};
//    NSString *cityCode = cityDic.allKeys.firstObject;
    NSString *cityName = cityDic.allValues.firstObject;
    
    NSDictionary *districtDic = self.districtArr.count? self.districtArr[[self.addressPickerView selectedRowInComponent:2]]:@{@"":@""};
//    NSString *disCode = districtDic.allKeys.firstObject;
    NSString *disName = districtDic.allValues.firstObject;
    
    self.addressCell.textField.text = [NSString stringWithFormat:@"%@－%@－%@",proName, cityName, disName];
    
    _hasAddress = YES;
    if (_hasAddress && _hasLength1 && _hasLength2 && _hasLength3) {
        [self buttonEnabled];
    }
}

#pragma mark -- 搜索回调

- (void)onDistrictSearchDone:(AMapDistrictSearchRequest *)request response:(AMapDistrictSearchResponse *)response {
    if (!response) {
        return;
    }
    
    if (_selectedSection == -1) {
        if (self.provinceArr.count) {
            [self.provinceArr removeAllObjects];
        }
        for (AMapDistrict *dist in response.districts) {
            if (dist.districts.count > 0) {
                for (AMapDistrict *province in dist.districts) {
                    [self.provinceArr addObject:@{province.adcode: province.name}];
                }
            }
        }
        
        [self.addressPickerView reloadComponent:0];
        [self.addressPickerView selectRow:0 inComponent:0 animated:YES];
        return;
    }
    
    
    for (AMapDistrict *dist in response.districts) {
        if ([dist.level isEqualToString:@"province"]) {
            
            if (dist.districts.count > 0) {
                if (self.cityArr.count) {
                    [self.cityArr removeAllObjects];
                }
                for (AMapDistrict *city in dist.districts) {
                    [self.cityArr addObject:@{city.adcode: city.name}];
                }
            }
            
        } else if ([dist.level isEqualToString:@"city"]) {
            
            if (dist.districts.count > 0) {
                if (self.districtArr.count) {
                    [self.districtArr removeAllObjects];
                }
                for (AMapDistrict *district in dist.districts) {
                    [self.districtArr addObject:@{district.adcode: district.name}];
                }
            }
        }
    }
    
    
    if (_selectedSection == 0) {
        [self.addressPickerView reloadComponent:1];
        [self.addressPickerView selectRow:0 inComponent:1 animated:YES];
    } else if (_selectedSection == 1) {
        [self.addressPickerView reloadComponent:2];
        [self.addressPickerView selectRow:0 inComponent:2 animated:YES];
    } else if (_selectedSection == -1) {
        [self.addressPickerView reloadComponent:0];
    }
}

- (void)AMapSearchRequest:(id)request didFailWithError:(NSError *)error {
    DLog(@"AMapSearch Error: %@", error);
}

#pragma mark -
#pragma mark -- PickerView delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (component == 0) {
        return self.provinceArr.count;
    } else if (component == 1) {
        return self.cityArr.count;
    } else {
        return self.districtArr.count;
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    _selectedSection = component;
    if (component == 0) {
        if (self.provinceArr.count) {
            AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
            dist.keywords = [[[self.provinceArr objectAtIndex:row] allValues] firstObject];
            [self.search AMapDistrictSearch:dist];
        }
        
    } else if (component == 1) {
        if (self.cityArr.count) {
            AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
            dist.keywords = [[[self.cityArr objectAtIndex:row] allValues] firstObject];
            [self.search AMapDistrictSearch:dist];
        }
        
    } else {
        if (self.districtArr.count) {
            AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
            dist.keywords = [[[self.districtArr objectAtIndex:row] allValues] firstObject];
            [self.search AMapDistrictSearch:dist];
        }
        
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont systemFontOfCustomeSize:16];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    
    if (component == 0) {
        if (self.provinceArr.count) {
            label.text = [[self.provinceArr[row] allValues] firstObject];
        }
        
    } else if (component == 1) {
        if (self.cityArr.count) {
            label.text = [[self.cityArr[row] allValues] firstObject];
        }
        
    } else {
        if (self.districtArr.count) {
            label.text = [[self.districtArr[row] allValues] firstObject];
        }
        
    }
    
    return label;
}

#pragma mark -
#pragma mark -- 网络请求

- (void)fetchData {

    NSMutableString *address = [NSMutableString string];
    NSArray *strArr = [self.addressCell.textField.text componentsSeparatedByString:@"－"];
    for (NSString *subStr in strArr) {
        [address appendString:subStr];
    }
    [address appendString:self.textField3.text];
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:exchange requestParams:@{@"exchangeCategory":@"realItem", @"remark":self.textField4.text, @"receiver":self.textField1.text, @"telephone":self.textField2.text, @"address":address,@"cerId":self.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.submitBlock();
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

@end
