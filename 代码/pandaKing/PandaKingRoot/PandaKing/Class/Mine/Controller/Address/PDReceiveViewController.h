//
//  PDReceiveViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 收货信息控制器，当成基类，此为输入QQ，充值Q币
@interface PDReceiveViewController : AbstractViewController
@property (nonatomic, copy) NSString *ID;   /**< 兑换券id*/
- (void)infoDidSubmitComplication:(void(^)())complication;
@end
