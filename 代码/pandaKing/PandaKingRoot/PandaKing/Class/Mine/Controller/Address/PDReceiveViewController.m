//
//  PDReceiveViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDReceiveViewController.h"
#import "PDReceiveTextCell.h"
#import "PDCenterTool.h"

typedef void(^submitBlock)();

@interface PDReceiveViewController ()<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSArray *placeholderArr;
@property (nonatomic, strong) NSArray *descArr;
@property (nonatomic, strong) UIButton *button;
@property (nonatomic, assign) CGFloat buttonSpace;

@property (nonatomic, strong) UITextField *textField1;  /**< 第一个*/
@property (nonatomic, strong) UITextField *textField2;  /**< 第二个*/
@property (nonatomic, strong) UITextField *textField3;  /**< 第三个*/

@property (nonatomic, assign) BOOL hasLength1;
@property (nonatomic, assign) BOOL hasLength2;

@property (nonatomic, copy) submitBlock submitBlock;
@end

@implementation PDReceiveViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _basicSetting];
    [self _pageSetting];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.textField1 becomeFirstResponder];
}

- (void)_basicSetting {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.barMainTitle = @"填写收货信息";
    
    self.placeholderArr = @[@"请输入QQ账号",@"请再次确认QQ账号",@"备注信息"];
    self.descArr = @[@"QQ帐号",@"确认帐号",@"备注信息"];
    
    _buttonSpace = LCFloat(50.);
}

- (void)_pageSetting {
    [self tableViewSetting];
    [self buttonSetting];
}

- (void)tableViewSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.mainTableView.rowHeight = [PDReceiveTextCell cellHeight];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
}

- (void)buttonSetting {
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(kTableViewSectionHeader_left, [PDReceiveTextCell cellHeight] * 4 + kTableViewSectionHeader_height + _buttonSpace, kScreenBounds.size.width - kTableViewSectionHeader_left * 2, 48.);
    button.enabled = NO;
    button.backgroundColor = [UIColor lightGrayColor];
    [button setTitle:@"确定" forState:UIControlStateNormal];
    [button addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
    [self.mainTableView addSubview:button];
    self.button = button;
    
    CGFloat x = LCFloat(50);
    UILabel *markLabel = [[UILabel alloc] init];
    markLabel.text = @"备注：请认真核对键入信息，如错误填写一律不予补发，商品一经兑换概不退换";
    markLabel.textColor = [UIColor redColor];
    markLabel.numberOfLines = 0;
    markLabel.textAlignment = NSTextAlignmentCenter;
    markLabel.font = [UIFont systemFontOfCustomeSize:13];
    CGSize size = [markLabel.text sizeWithCalcFont:markLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - x * 2, CGFLOAT_MAX)];
    markLabel.frame = CGRectMake(x, CGRectGetMaxY(button.frame) + LCFloat(15), size.width, size.height);
    [self.mainTableView addSubview:markLabel];
}


#pragma mark -
#pragma mark -- 控件方法

- (void)didClickButton {
    if (![self.textField1.text isEqualToString:self.textField2.text]) {
        [PDHUD showHUDProgress:@"请确认两次QQ号输入相同！" diary:1.5];
        return;
    }
    [self fetchData];
}

- (void)didChangeValue:(UITextField *)textField {
    if (textField == self.textField1) {
        if (textField.text.length > 0) {
            _hasLength1 = YES;
            if (_hasLength2 && self.textField3.text.length) {
                [self buttonEnabled];
            } else {
                [self buttonUnEnabled];
            }
        } else {
            _hasLength1 = NO;
            [self buttonUnEnabled];
        }
    } else if (textField == self.textField2) {
        if (textField.text.length > 0) {
            _hasLength2 = YES;
            if (_hasLength1 && self.textField3.text.length) {
                [self buttonEnabled];
            } else {
                [self buttonUnEnabled];
            }
        } else {
            _hasLength2 = NO;
            [self buttonUnEnabled];
        }
    } else {
        if (textField.text.length > 0 && _hasLength1 && _hasLength2) {
            [self buttonEnabled];
        } else {
            [self buttonUnEnabled];
        }
    }
}

- (void)buttonEnabled {
    self.button.enabled = YES;
    self.button.backgroundColor = [UIColor blackColor];
}

- (void)buttonUnEnabled {
    self.button.enabled = NO;
    self.button.backgroundColor = [UIColor lightGrayColor];
}

#pragma mark -
#pragma mark -- UITabelView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.descArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"receiveCellId";
    PDReceiveTextCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[PDReceiveTextCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textField.delegate = self;
        [cell.textField addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventEditingChanged];
    }
    cell.desc = [self.descArr objectAtIndex:indexPath.row];
    cell.textField.placeholder = self.placeholderArr[indexPath.row];
    
    if (indexPath.row == 0) {
        cell.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.textField1 = cell.textField;
        
    } else if (indexPath.row == 1) {
        cell.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.textField2 = cell.textField;
    } else {
        cell.textField.returnKeyType = UIReturnKeyDone;
        self.textField3 = cell.textField;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.descArr.count - 1) {
        [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    CGFloat label_bottom = 3;
    UILabel *label = [[UILabel alloc] init];
    label.text = @"请填写相关物品的收货信息";
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfCustomeSize:13];
    label.textColor = [UIColor grayColor];
    CGSize size = [label.text sizeWithCalcFont:label.font];
    label.frame = CGRectMake(kTableViewSectionHeader_left, kTableViewSectionHeader_height - size.height - label_bottom, size.width, size.height);
    [headerView addSubview:label];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewSectionHeader_height;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [PDCenterTool tableViewScrolledRemoveViscousWithScrollView:scrollView sectionHeight:kTableViewSectionHeader_height];
}

- (void)infoDidSubmitComplication:(void (^)())complication {
    if (complication) {
        self.submitBlock = complication;
    }
}

#pragma mark -
#pragma mark -- UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.view endEditing:YES];
    return YES;
}

#pragma mark -
#pragma mark -- 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:exchange requestParams:@{@"exchangeCategory":@"qCoin", @"qq":self.textField1.text, @"remark": self.textField3.text,@"cerId":self.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.submitBlock();
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}


@end
