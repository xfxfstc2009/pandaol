//
//  PDCenterLotteryHeroViewController.h
//  PandaKing
//
//  Created by Cranz on 16/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSUInteger, PDCenterLotteryType) {
    PDCenterLotteryTypeHero,
    PDCenterLotteryTypeMatch,
};

/// 我的竞猜
@interface PDCenterLotteryHeroViewController : AbstractViewController
@property (nonatomic, assign) NSInteger selIndex;   /**< 选定的竞猜:0 英雄猜，1 赛事猜*/
@end
