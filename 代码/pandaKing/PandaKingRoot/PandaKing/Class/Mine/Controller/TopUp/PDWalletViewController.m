//
//  PDWalletViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDWalletViewController.h"
#import "PDTopUpViewController.h"
#import "CCZSegmentController.h"
#import "PDPaymentsViewController.h"
#import "PDBambooPaymentsViewController.h"

@interface PDWalletViewController ()
@property (nonatomic, strong) PDImageView *backgroundHeaderView;    /**< 背景头像图*/
@property (nonatomic, strong) UIView *topBar;
@property (nonatomic, assign) CGFloat topBarHeight;
@property (nonatomic, assign) CGFloat backgroundHeaderHeight;
@property (nonatomic, strong) UIColor *headerTitleColor;
@property (nonatomic, assign) CGFloat headerLeftSpace;      /**< 头像显示中lable左侧距离屏幕*/
@property (nonatomic, assign) CGFloat space_label_label;    /**< 上label和下label之间的间隙*/
@property (nonatomic, strong) UILabel *descLabel;           /**< 竹子资产标题*/
@property (nonatomic, strong) UILabel *bambooNumLabel;       /**< 竹子数量*/
@property (nonatomic, strong) UILabel *dailyDescLabel;      /**< 今日收益标题*/
@property (nonatomic, strong) UILabel *dailyInacomeLabel;    /**< 每日收益*/
@property (nonatomic, assign) CGFloat space_bottom;         /**< label距离底部的距离*/
@end

@implementation PDWalletViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self fetchData];
}

- (void)basicSetting {
    _topBarHeight = 44.;
    _backgroundHeaderHeight = LCFloat(220.);
    _headerTitleColor = c26;
    _headerLeftSpace = LCFloat(30.);
    _space_label_label = LCFloat(5.);
    _space_bottom = LCFloat(50.);
}

- (void)pageSetting {
    [self headerViewSetting];
    [self topBarSetting];
    [self paymentsViewsetting];
}

- (void)headerViewSetting {
    self.backgroundHeaderView = [[PDImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, _backgroundHeaderHeight)];
    __weak typeof(self) weakSelf = self;
    [self.backgroundHeaderView uploadImageWithURL:[Tool userDefaultGetWithKey:CustomerAvatar] placeholder:nil callback:^(UIImage *image) {
        weakSelf.backgroundHeaderView.image = [image applyDarkEffect];
        weakSelf.backgroundHeaderView.contentMode = UIViewContentModeScaleAspectFill;
    }];
    self.backgroundHeaderView.userInteractionEnabled = YES;
    [self.view addSubview:self.backgroundHeaderView];
    
    // 资产
    UILabel *descLabel = [[UILabel alloc] init];
    descLabel.text = @"总资产（金币）";
    descLabel.font = [UIFont systemFontOfCustomeSize:14];
    descLabel.textColor = _headerTitleColor;
    [self.backgroundHeaderView addSubview:descLabel];
    self.descLabel = descLabel;
    
    // 资产底下的数额
    self.bambooNumLabel = [[UILabel alloc] init];
    self.bambooNumLabel.font = [[UIFont systemFontOfCustomeSize:50] boldFont];
    self.bambooNumLabel.textColor = _headerTitleColor;
    self.bambooNumLabel.adjustsFontSizeToFitWidth = YES;
    [self.backgroundHeaderView addSubview:self.bambooNumLabel];
    
    // 今日收益
//    UILabel *dailyDescLabel = [[UILabel alloc] init];
//    dailyDescLabel.text = @"今日收益";
//    dailyDescLabel.textColor = _headerTitleColor;
//    dailyDescLabel.font = [UIFont systemFontOfCustomeSize:14];
//    [self.backgroundHeaderView addSubview:dailyDescLabel];
//    self.dailyDescLabel = dailyDescLabel;
    
    // 进入受益数额
    self.dailyInacomeLabel = [[UILabel alloc] init];
    self.dailyInacomeLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.dailyInacomeLabel.textColor = _headerTitleColor;
    [self.backgroundHeaderView addSubview:self.dailyInacomeLabel];
}

- (void)topBarSetting {
    self.topBar = [[UIView alloc] initWithFrame:CGRectMake(0, 20, kScreenBounds.size.width, _topBarHeight)];
    self.topBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.topBar];
    // 左侧按钮
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setTintColor:[UIColor whiteColor]];
    [leftButton setFrame:CGRectMake(0, 0, _topBarHeight, _topBarHeight)];
    [leftButton setImage:[UIImage imageNamed:@"icon_center_back"] forState:UIControlStateNormal];
    [leftButton addTarget:self action:@selector(didClickLeftButton) forControlEvents:UIControlEventTouchUpInside];
    [self.topBar addSubview:leftButton];
    
    // 标题
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.text = @"我的钱包";
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, (_topBarHeight  - titleSize.height) / 2, titleSize.width, titleSize.height);
    [self.topBar addSubview:titleLabel];
    
    // 右侧按钮
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setTitle:@"充值" forState:UIControlStateNormal];
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    rightButton.titleLabel.font = [UIFont systemFontOfCustomeSize:16];
    rightButton.frame = CGRectMake(kScreenBounds.size.width - ([@"充值" sizeWithCalcFont:rightButton.titleLabel.font].width + LCFloat(20)), 0, [@"充值" sizeWithCalcFont:rightButton.titleLabel.font].width + LCFloat(20), _topBarHeight);
    [rightButton addTarget:self action:@selector(didClickRightButton) forControlEvents:UIControlEventTouchUpInside];
    [self.topBar addSubview:rightButton];
}

- (void)paymentsViewsetting {
    PDPaymentsViewController *goldPaymentsViewController = [[PDPaymentsViewController alloc] init];
    PDBambooPaymentsViewController *bambooPaymentsViewController = [[PDBambooPaymentsViewController alloc] init];
    CCZSegmentController *segement = [[CCZSegmentController alloc] initWithFrame:CGRectMake(0, _backgroundHeaderHeight, kScreenBounds.size.width, kScreenBounds.size.height - _backgroundHeaderHeight) titles:@[@"金币收支",@"竹子记录"]];
    segement.containerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    segement.viewControllers = @[goldPaymentsViewController, bambooPaymentsViewController];
    [self addSegmentController:segement];
}

#pragma mark - 控件方法

- (void)didClickLeftButton {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didClickRightButton {
    
    PDTopUpViewController *topUpViewController = [[PDTopUpViewController alloc] init];
    [self  pushViewController:topUpViewController animated:YES];
}

#pragma mark - 网络请求

- (void)fetchData {
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberWallet requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            NSNumber *todaygold = responseObject[@"todaygold"];// 今日收益
            NSNumber *totalgold = responseObject[@"totalgold"];// 总资产
            
            weakSelf.bambooNumLabel.text = [self separateStringWithString:[NSString stringWithFormat:@"%ld",(long)totalgold.integerValue]];
            if (todaygold.intValue > 0) {
                weakSelf.dailyInacomeLabel.text = [NSString stringWithFormat:@"今日收益 +%ld",(long)todaygold.integerValue];
            } else {
                weakSelf.dailyInacomeLabel.text = [NSString stringWithFormat:@"今日收益 %ld",(long)todaygold.integerValue];
            }
            
            [weakSelf updateFrame];
        }
    }];

}

- (void)updateFrame {
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    CGSize bambooNumSize = [self.bambooNumLabel.text sizeWithCalcFont:self.bambooNumLabel.font];
    CGSize dailySize = [self.dailyInacomeLabel.text sizeWithCalcFont:self.dailyInacomeLabel.font];
//    CGSize dailyDescSize = [self.dailyDescLabel.text sizeWithCalcFont:self.dailyDescLabel.font];
    
    self.bambooNumLabel.frame = CGRectMake(_headerLeftSpace, _backgroundHeaderHeight - _space_bottom - bambooNumSize.height, kScreenBounds.size.width - _headerLeftSpace * 2, bambooNumSize.height);
    self.descLabel.frame = CGRectMake(_headerLeftSpace, CGRectGetMinY(self.bambooNumLabel.frame) - _space_label_label - descSize.height, descSize.width, descSize.height);
    self.dailyInacomeLabel.frame = CGRectMake(_headerLeftSpace, CGRectGetMaxY(self.bambooNumLabel.frame) + (_backgroundHeaderHeight - CGRectGetMaxY(self.bambooNumLabel.frame) - dailySize.height) / 2, dailySize.width, dailySize.height);
//    self.dailyDescLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - dailyDescSize.width, CGRectGetMinY(self.dailyInacomeLabel.frame) - LCFloat(2) - dailyDescSize.height, dailyDescSize.width, dailyDescSize.height);
}

#pragma mark - 分割

- (NSString *)separateStringWithString:(NSString *)targetString {
    if (targetString.length > 3) {
        NSString *newBalance = [targetString stringByReplacingOccurrencesOfString:@"," withString:@""];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSString *formattedBalance = [formatter stringFromNumber:[NSNumber numberWithInteger:[newBalance integerValue]]];
        return formattedBalance;
    } else {
        return targetString;
    }
}


@end
