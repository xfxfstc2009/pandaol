//
//  PDBambooPaymentsViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBambooPaymentsViewController.h"
#import "PDPaymentsTableViewCell.h"
#import "PDMemberTopUpNote.h"

@interface PDBambooPaymentsViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTabelView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) PDMemberTopUpNote *topUpNote;
@property (nonatomic, strong) NSMutableArray *itemArr;
@end

@implementation PDBambooPaymentsViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    _page = 1;
    _itemArr = [NSMutableArray array];
}

- (void)pageSetting {
    self.mainTabelView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height)];
    self.mainTabelView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTabelView.backgroundColor = [UIColor whiteColor];
    self.mainTabelView.delegate = self;
    self.mainTabelView.dataSource = self;
    self.mainTabelView.tableFooterView = [[UIView alloc] init];
    self.mainTabelView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.mainTabelView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainTabelView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.itemArr.count > 0) {
            [weakSelf.itemArr removeAllObjects];
        }
        [weakSelf fetchData];
    }];
    
    [self.mainTabelView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.topUpNote.hasNextPage) {
            weakSelf.page ++;
            [weakSelf fetchData];
        } else {
            [weakSelf.mainTabelView stopFinishScrollingRefresh];
        }
    }];
}

#pragma mark - UITabelView delegate

/** 后台返回的应该是不分开的，每条数据一个时间。我在拿到数据后将数据归类，同一天的分入同一个数组，每个数组一个section*/

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.itemArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.itemArr.count) {
        PDMemberTopUpItem *item = [self.itemArr objectAtIndex:section];
        return item.detail.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"paymentsCellId";
    PDPaymentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[PDPaymentsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    
    if (self.itemArr.count) {
        PDMemberTopUpItem *item = [self.itemArr objectAtIndex:indexPath.section];
        PDMemberTopUpDetail *detail = [item.detail objectAtIndex:indexPath.row];
        cell.detailModel = detail;
        cell.tagImage = [UIImage imageNamed:@"icon_center_bamboo"];
        
        if (indexPath.row == 0) {
            cell.isShowDate = YES;
        } else {
            cell.isShowDate = NO;
        }
        
        if (indexPath.row == item.detail.count - 1) {
            cell.isLastEvent = YES;
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemArr.count) {
        PDMemberTopUpItem *item = [self.itemArr objectAtIndex:indexPath.section];
        PDMemberTopUpDetail *detail = [item.detail objectAtIndex:indexPath.row];
        return [PDPaymentsTableViewCell cellHeightWithDesc:detail];
    } else {
        return 0;
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return kTableViewHeader_height;
    } else {
        return 0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [UIView new];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    return headerView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [PDCenterTool tableViewScrolledRemoveViscousCancleWithScrollView:scrollView sectionHeight:kTableViewHeader_height];
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberBamboo requestParams:@{@"pageNum":@(_page)} responseObjectClass:[PDMemberTopUpNote class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDMemberTopUpNote *note = (PDMemberTopUpNote *)responseObject;
            weakSelf.topUpNote = note;
            [weakSelf.itemArr addObjectsFromArray:note.items];
            [weakSelf dealArr:weakSelf.itemArr];
            [weakSelf.mainTabelView reloadData];
            if (weakSelf.itemArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTabelView.scrollEnabled = NO;
            } else {
                weakSelf.mainTabelView.scrollEnabled = YES;
            }
        }
        
        [weakSelf.mainTabelView stopFinishScrollingRefresh];
        [weakSelf.mainTabelView stopPullToRefresh];
    }];
}

#pragma mark - 处理数组

- (void)dealArr:(NSArray *)itemArr {
    
    for (int i = 0 ; i < itemArr.count; i++) {
        if (i == 0) {
            continue;
        }
        PDMemberTopUpItem *item0 = [self.itemArr objectAtIndex:i - 1];
        PDMemberTopUpItem *item = [self.itemArr objectAtIndex:i];
        if ([item.day isEqualToString:item0.day]) {
            [item0.detail addObjectsFromArray:item.detail];
            [self.itemArr removeObject:item];
        }
    }
}

#pragma mark - 显示无数据页

- (void)showNoDataPage {
    __weak typeof(self) weakSelf = self;
    [self.mainTabelView showPrompt:@"糟糕!暂无任何记录...QAQ" withImage:[UIImage imageNamed:@"icon_nodata_panda"] buttonTitle:@"点击刷新" clickBlock:^{
        [weakSelf.mainTabelView dismissPrompt];
        [weakSelf fetchData];
    }];
}

@end
