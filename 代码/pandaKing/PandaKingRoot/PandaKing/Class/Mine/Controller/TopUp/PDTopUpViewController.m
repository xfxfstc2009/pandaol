//
//  PDTempViewController.m
//  PandaKing
//
//  Created by Cranz on 16/11/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTopUpViewController.h"
#import "PDCenterLabel.h"
#import "PDTopUpTableViewCell.h"
#import "PDStoreRuleCell.h"
#import "PDCenterTopUpList.h"
#import "PDPaySheetView.h"
#import "PDWalletChangeList.h"
// 支付
#import "PDTopUpOrder.h"
#import "PDAlipayHandle.h"
#import "PDAlipayOrderInfo.h"
#import "PDWXPayHandle.h"

#define kBackgroundHeight  (LCFloat(260)-kBackgroundBeyondY)
#define kBackgroundBeyondY 64 // 头部背景图超出屏幕的y

@interface PDTopUpViewController ()<UITableViewDataSource, UITableViewDelegate, PDTopUpTableViewCellDelegate>
@property (nonatomic, strong) UIImageView *titleBar;
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UIImageView *vLine;
@property (nonatomic, strong) UILabel *goldLabel;    /**< 金币余额*/
@property (nonatomic, strong) UILabel *bambooLabel;     /**< 竹子余额*/
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, copy)   NSString *text; // 注意事项
@property (nonatomic, strong) PDCenterTopUpList *topUpList;

//刷新相关
@property (nonatomic, assign) CGFloat vLineY;
@property (nonatomic, assign) CGFloat vLineHeight;
@end

@implementation PDTopUpViewController

- (NSMutableArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

- (void)dealloc {
    [NOTIFICENTER removeObserver:self name:kWXPAY_NOTIFACTION object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self addNotification];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (void)basicSetting {
    self.text = @"1.金币是盼达电竞APP的付费货币，通过绑定游戏角色后打游戏和充值获取，可用于各类竞猜活动。\n2.竹子是盼达电竞的虚拟货币，通过任务和邀请新用户获得，可在APP内进行商品内购与参与夺宝活动，未来将有更多用途，敬请期待。";
}

- (void)pageSetting {
    [self titleBarSetting];
    [self topUpHeaderViewSetting];
    [self topUpTabelViewSetting];
}

- (void)titleBarSetting {
    self.titleBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    self.titleBar.userInteractionEnabled = YES;
    self.titleBar.image = [UIImage imageNamed:@"bg_wallet_top"];
    [self.view addSubview:self.titleBar];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"我的钱包";
    titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, (44 - titleSize.height) / 2 + 20, titleSize.width, titleSize.height);
    [self.titleBar addSubview:titleLabel];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(kTableViewSectionHeader_left, 20, 25, 38)];
    [backButton setImage:[UIImage imageNamed:@"icon_center_back"] forState:UIControlStateNormal];
    __weak typeof(self) weakSelf = self;
    [backButton buttonWithBlock:^(UIButton *button) {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.titleBar addSubview:backButton];
}

- (void)topUpHeaderViewSetting {
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kBackgroundHeight)];
    backgroundImageView.image = [UIImage imageNamed:@"bg_wallet_bottom"];
    backgroundImageView.userInteractionEnabled = YES;
    self.backgroundImageView = backgroundImageView;
    
    UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, kBackgroundHeight - 1, kScreenBounds.size.width, 1)];
    hLine.backgroundColor = c26;
    [backgroundImageView addSubview:hLine];
    
    UIButton *exchangeButton = [[UIButton alloc] init];
    [exchangeButton setTitleColor:c1 forState:UIControlStateNormal];
    [exchangeButton setTitle:@"金币兑换竹子" forState:UIControlStateNormal];
    exchangeButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    
    CGSize titleSize = [exchangeButton.titleLabel.text sizeWithCalcFont:exchangeButton.titleLabel.font];
    CGSize size = CGSizeMake(titleSize.width + 10 * 2, titleSize.height + 10 * 2);
    
    [exchangeButton setFrame:CGRectMake((kScreenBounds.size.width - size.width) / 2, kBackgroundHeight - LCFloat(20) - size.height, size.width, size.height)];
    exchangeButton.layer.borderColor = c1.CGColor;
    exchangeButton.layer.borderWidth = 1;
    exchangeButton.layer.cornerRadius = 3;
    exchangeButton.clipsToBounds = YES;
    [backgroundImageView addSubview:exchangeButton];
    [exchangeButton addTarget:self action:@selector(didClickGoldChangeToBamboo:) forControlEvents:UIControlEventTouchUpInside];
    
    //icon_lottery_vline
    _vLineY = LCFloat(25);
    _vLineHeight = CGRectGetMinY(exchangeButton.frame) - LCFloat(50);
    UIImageView *vLine = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_lottery_vline"]];
    vLine.frame = CGRectMake((kScreenBounds.size.width - 1) / 2, _vLineY, 1, _vLineHeight);
    [backgroundImageView addSubview:vLine];
    self.vLine = vLine;
    
    self.goldLabel = [[UILabel alloc] init];
    self.goldLabel.text = @"0";
    self.goldLabel.font = [[UIFont systemFontOfCustomeSize:30] boldFont];
    self.goldLabel.textColor = c1;
    [backgroundImageView addSubview:self.goldLabel];
    
    PDCenterLabel *goldDescLabel = [[PDCenterLabel alloc] init];
    goldDescLabel.textColor = c26;
    goldDescLabel.textFont = [[UIFont systemFontOfCustomeSize:13] boldFont];
    goldDescLabel.image = [UIImage imageNamed:@"icon_center_gold"];
    goldDescLabel.text = @"金币";
    [backgroundImageView addSubview:goldDescLabel];
    
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    CGSize goldDescSize = goldDescLabel.size;
    CGFloat y = CGRectGetMinY(vLine.frame) + (CGRectGetHeight(vLine.frame) - goldSize.height - 5 - goldDescSize.height) / 2;
    
    self.goldLabel.frame = CGRectMake((CGRectGetMinX(vLine.frame) - goldSize.width) / 2, y, goldSize.width, goldSize.height);
    goldDescLabel.frame = CGRectMake((CGRectGetMinX(vLine.frame) - goldDescSize.width) / 2, CGRectGetMaxY(self.goldLabel.frame) + 5, goldDescSize.width, goldDescSize.height);
    
    
    // 竹子
    self.bambooLabel = [[UILabel alloc] init];
    self.bambooLabel.text = @"0";
    self.bambooLabel.font = [[UIFont systemFontOfCustomeSize:30] boldFont];
    self.bambooLabel.textColor = c1;
    [backgroundImageView addSubview:self.bambooLabel];
    
    PDCenterLabel *bambooDescLabel = [[PDCenterLabel alloc] init];
    bambooDescLabel.textColor = goldDescLabel.textColor;
    bambooDescLabel.textFont = goldDescLabel.textFont;
    bambooDescLabel.image = [UIImage imageNamed:@"icon_center_bamboo"];
    bambooDescLabel.text = @"竹子";
    [backgroundImageView addSubview:bambooDescLabel];
    
    CGSize bambooSize = [self.bambooLabel.text sizeWithCalcFont:self.bambooLabel.font];
    CGSize bambooDescSize = bambooDescLabel.size;
    
    self.bambooLabel.frame = CGRectMake(CGRectGetMaxX(vLine.frame) + (CGRectGetMinX(vLine.frame) - bambooSize.width) / 2, y, bambooSize.width, bambooSize.height);
    bambooDescLabel.frame = CGRectMake(CGRectGetMaxX(vLine.frame) + (CGRectGetMinX(vLine.frame) - bambooDescSize.width) / 2, CGRectGetMaxY(self.bambooLabel.frame) + 5, bambooDescSize.width, bambooDescSize.height);
}

- (void)topUpTabelViewSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, kBackgroundBeyondY, kScreenBounds.size.width, kScreenBounds.size.height - kBackgroundBeyondY)];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.showsVerticalScrollIndicator = NO;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.tableHeaderView = self.backgroundImageView;
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        [weakSelf fetchData];
    }];
}

- (void)addNotification {
    [NOTIFICENTER addObserver:self selector:@selector(didReceivedWxPayNotification:) name:kWXPAY_NOTIFACTION object:nil];
}


#pragma mark -- UITabelView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return self.itemsArr.count;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
        NSString *cellId = @"topUpCellId";
        
        PDTopUpTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (!cell) {
            cell = [[PDTopUpTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            cell.delegate = self;
        }
        
        cell.indexPath = indexPath;
        
        if (self.itemsArr.count > indexPath.row) {
            cell.item = self.itemsArr[indexPath.row];
        }
    
        return cell;
        
    } else {
        
        NSString *cellId = @"warnCellId";
        
        PDStoreRuleCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (!cell) {
            cell = [[PDStoreRuleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        
        cell.content = self.text;
        
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [PDTopUpTableViewCell cellHeight];
    } else {
        return [PDStoreRuleCell cellHeightWithContent:self.text];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewSectionHeader_height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        
        UIView *sectionView = [[UIView alloc] init];
        sectionView.backgroundColor = BACKGROUND_VIEW_COLOR;
        
        UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 6, 3, kTableViewSectionHeader_height - 6 * 2)];
        blackView.backgroundColor = [UIColor blackColor];
        [sectionView addSubview:blackView];
        
        NSString *scale = [NSString stringWithFormat:@"(新用户首充多送%.0lf％金币)", self.topUpList.goldAwardPercentForFirstPay * 100];
        NSString *text = [NSString stringWithFormat:@"金币充值%@", scale];
        UILabel *label = [[UILabel alloc] init];
        label.text = text;
        label.font = [UIFont systemFontOfCustomeSize:11];
        label.textColor = [UIColor grayColor];
        CGSize size = [label.text sizeWithCalcFont:label.font];
        label.frame = CGRectMake(kTableViewSectionHeader_leftMaxtter, (kTableViewSectionHeader_height - size.height) / 2, size.width, size.height);
        [sectionView addSubview:label];
        
        NSMutableAttributedString *att = [[NSMutableAttributedString alloc] initWithString:text];
        [att addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4, scale.length)];
        label.attributedText = [att copy];
        
        return sectionView;
        
    } else {
        UIView *sectionView = [[UIView alloc] init];
        sectionView.backgroundColor = BACKGROUND_VIEW_COLOR;
        
        UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 6, 3, kTableViewSectionHeader_height - 6 * 2)];
        blackView.backgroundColor = [UIColor blackColor];
        [sectionView addSubview:blackView];
        
        UILabel *label = [[UILabel alloc] init];
        label.text = @"注意事项";
        label.font = [UIFont systemFontOfCustomeSize:11];
        label.textColor = [UIColor grayColor];
        CGSize size = [label.text sizeWithCalcFont:label.font];
        label.frame = CGRectMake(kTableViewSectionHeader_leftMaxtter, (kTableViewSectionHeader_height - size.height) / 2, size.width, size.height);
        [sectionView addSubview:label];
        
        return sectionView;
    }
}

#pragma mark - PDTopUpTableViewCellDelegate

- (void)topUpCell:(PDTopUpTableViewCell *)topUpCell didClickExChangeButtonAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.itemsArr.count > indexPath.row) {
        PDCenterTopUpItem *payItem = self.itemsArr[indexPath.row];
        
        PDPaySheetView *payView = [PDPaySheetView pay];
        payView.canTouchedCancle = NO;
        __weak typeof(self) weakSelf = self;
        [payView payWithSelectedType:^(NSInteger type) {
            [weakSelf fetchOrderWithType:type selectedItem:payItem];
        }];
        
    }
}

#pragma mark - 控件方法

/** 金币换竹子*/
- (void)didClickGoldChangeToBamboo:(UIButton *)button {
    // 获取兑换列表
    [self fetchExhcangeList];
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:walletPayList requestParams:nil responseObjectClass:[PDCenterTopUpList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        if (isSucceeded) {
            PDCenterTopUpList *topUpList = (PDCenterTopUpList *)responseObject;
            weakSelf.topUpList = topUpList;
            
            [weakSelf updateLabelNumberWithTopUpModel:topUpList];
            
            if (weakSelf.itemsArr.count) {
                [weakSelf.itemsArr removeAllObjects];
            }
            [weakSelf.itemsArr addObjectsFromArray:topUpList.items];
            [weakSelf.mainTableView reloadData];
        }
    }];
}

- (void)updateLabelNumberWithTopUpModel:(PDCenterTopUpList *)list {
    [AccountModel sharedAccountModel].memberInfo.gold =  list.goldBalance;
    
    self.goldLabel.text = [PDCenterTool numberStringChangeWithoutSpecialCharacter:list.goldBalance];
    self.bambooLabel.text = [PDCenterTool numberStringChangeWithoutSpecialCharacter:list.bambooBalance];
    
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    CGSize bambooSize=  [self.bambooLabel.text sizeWithCalcFont:self.bambooLabel.font];
    
    self.goldLabel.frame = CGRectMake((CGRectGetMinX(self.vLine.frame) - goldSize.width) / 2, self.goldLabel.frame.origin.y, goldSize.width, goldSize.height);
    self.bambooLabel.frame = CGRectMake(CGRectGetMaxX(self.vLine.frame) + (CGRectGetMinX(self.vLine.frame) - bambooSize.width) / 2, self.bambooLabel.frame.origin.y, bambooSize.width, bambooSize.height);
}

/** 请求订单 type: 1 -> 支付宝; 2 -> 微信支付*/
- (void)fetchOrderWithType:(NSInteger)type selectedItem:(PDCenterTopUpItem *)item {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerOrder requestParams:@{@"count":@(item.gold),@"money":item.cash} responseObjectClass:[PDTopUpOrder class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            
            PDTopUpOrder *topUpOrder = (PDTopUpOrder *)responseObject;
            
            if (type == 1) {
                PDPayOrderModel *order = [[PDPayOrderModel alloc] init];
                order.tradeNO = topUpOrder.ID;
                order.amount = [NSString stringWithFormat:@"%.2lf",topUpOrder.totalPrice.floatValue];
                order.productName = topUpOrder.orderName;
                order.productDescription = topUpOrder.orderName;
                PDAlipayHandle1 *alipayHandle1 = [PDAlipayHandle1 payHandle];
                alipayHandle1.notifyUrl = topUpOrder.alipayNotifyUrl;
                [alipayHandle1 payWithOrder:order payResultComplication:^(NSInteger code, NSString *errMsg) {
                    __strong typeof(weakSelf) strongSelf = weakSelf;
                    if (code == 9000) {
                        [strongSelf paySuccess];
                    } else {
                        [strongSelf payFail];
                    }
                }];
            } else {
                // 这个order可以用在支付宝最新版
                Order *order = [[Order alloc] init];
                order.biz_content = [BizContent new];
                order.biz_content.out_trade_no = topUpOrder.ID;
                order.biz_content.total_amount = [NSString stringWithFormat:@"%.2lf",topUpOrder.totalPrice.floatValue];
                order.biz_content.body = topUpOrder.orderName;
                
                PDWXPayHandle *wxpayHandle = [PDWXPayHandle payHandle];
                wxpayHandle.notifyUrl = topUpOrder.tenpayNotifyUrl;
                [wxpayHandle sendPayRequestWithOrder:order];
            }
            
        }
    }];
}

/** 竹子兑换金币*/
- (void)fetchGoldExchangeWithGold:(NSInteger)gold bamboo:(NSInteger)bamboo {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:walletExchange requestParams:@{@"goldCount":@(gold),@"bamCount":@(bamboo)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            [weakSelf fetchData];
            [PDHUD showHUDProgress:@"兑换成功！" diary:2];
        }
    }];
}

/** 获取兑换列表*/
- (void)fetchExhcangeList {
//    PDPropertyItem *item1 = [PDPropertyItem new];
//    item1.gold = 100000;
//    item1.bamboo = 30;
//    PDPropertyItem *item2 = [PDPropertyItem new];
//    item2.gold = 200000;
//    item2.bamboo = 40;
//    PDPropertyItem *item3 = [PDPropertyItem new];
//    item3.gold = 300000;
//    item3.bamboo = 50;
//    NSArray *arr = @[item1, item2, item3];
//    [self showExchangeAlertWithArr:arr];
//    return;
    
    __weak typeof(self) weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:walletChangeList requestParams:nil responseObjectClass:[PDWalletChangeList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            
            PDWalletChangeList *exchangeList = (PDWalletChangeList *)responseObject;
            
            [self showExchangeAlertWithArr:exchangeList.items];
            
        }
    }];
}

- (void)showExchangeAlertWithArr:(NSArray *)arr {
    
    __weak typeof(self) weakSelf = self;
    
    [[PDAlertView sharedAlertView] showAlertForWalletWithTitle:@"兑换竹子" cancleEnable:YES exchangeModelArr:arr selBlock:^(NSInteger index, PDPropertyItem *item) {
        
        [weakSelf fetchGoldExchangeWithGold:item.gold bamboo:item.bamboo];
        
    }];
    
}

#pragma mark -- 支付提示

- (void)paySuccess {
    // 支付成功
    [PDHUD showHUDProgress:@"支付成功！" diary:2];
    
    if ([self.delegate respondsToSelector:@selector(goldExchangeFinish:)]) {
        [self.delegate goldExchangeFinish:self];
    }
    
    [self fetchData];
}

- (void)payFail {
    // 支付失败
    [PDHUD showHUDProgress:@"支付失败！" diary:2];
}

#pragma mark -
#pragma mark -- 微信支付通知

- (void)didReceivedWxPayNotification:(NSNotification *)noti {
    NSDictionary *dic = noti.userInfo;
    if ([dic[@"result"] isEqualToString:@"success"]) {
        [self paySuccess];
    } else {
        [self payFail];
    }
}

@end
