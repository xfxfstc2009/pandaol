//
//  PDTopUpViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 充值控制器
@protocol PDTopUpViewControllerDelegate;
@interface PDTopUpViewController : AbstractViewController
@property (nonatomic, weak) id<PDTopUpViewControllerDelegate> delegate;
@end

@protocol PDTopUpViewControllerDelegate <NSObject>
/** 购买金币成功*/
@optional
- (void)goldExchangeFinish:(PDTopUpViewController *)topupViewController;

@end