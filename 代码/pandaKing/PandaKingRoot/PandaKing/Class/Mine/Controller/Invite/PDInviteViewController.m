
#import "PDInviteViewController.h"
#import "PDShareSheetView.h"
#import "PDOAuthLoginAndShareTool.h"
#import "PDInviteInfo.h"
#import "PDInviteLayer3.h"
#import "PDInviteLayer1.h"
#import "PDInviteLayer4.h"
#import "PDInviteDetailViewController.h"

@interface PDInviteViewController ()
@property (nonatomic, strong) PDInviteInfo *inviteInfo;
@property (nonatomic, strong) PDInviteLayer1 *inviteLayer1; // 邀请码那一层
@property (nonatomic, strong) PDInviteLayer4 *inviteLayer4; // 填写邀请码那一层
@property (nonatomic, strong) PDInviteLayer3 *inviteLayer3; // 黑色浮层
@property (nonatomic, strong) UIScrollView *backgroundView;
@property (nonatomic, strong) UIView *titleBar;

@end

@implementation PDInviteViewController


- (void)dealloc {
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKEYBOARD_HIDE object:nil];
//    [[NSNotificationCenter defaultCenter] removeObserver:self name:kKEYBOARD_SHOW object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
//    [self addNotification];
    [self pageSetting];
    [self fetchData:NULL];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)basicSetting {
    // 自定义的导航栏
    self.titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    self.titleBar.backgroundColor = c7;
    [self.view addSubview:self.titleBar];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"邀请有奖";
    titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, (44 - titleSize.height) / 2 + 20, titleSize.width, titleSize.height);
    [self.titleBar addSubview:titleLabel];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(kTableViewSectionHeader_left, 20, 31, 38)];
    [backButton setImage:[UIImage imageNamed:@"icon_center_back"] forState:UIControlStateNormal];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 3)];
    __weak typeof(self) weakSelf = self;
    [backButton buttonWithBlock:^(UIButton *button) {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.titleBar addSubview:backButton];
    
    // 分享
    UIButton *shareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [shareButton setFrame:CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - 31, 20, 31, 38)];
    [shareButton setImage:[UIImage imageNamed:@"icon_share_share"] forState:UIControlStateNormal];
    [shareButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 3)];
    [shareButton buttonWithBlock:^(UIButton *button) {
        [weakSelf share];
        [weakSelf.view endEditing:YES];
    }];
    [self.titleBar addSubview:shareButton];
    
    // 设置背景滚动
    self.backgroundView = [[UIScrollView alloc] initWithFrame:kScreenBounds];
    self.backgroundView.contentSize = CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.height);
    self.backgroundView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.backgroundView];
}

- (void)pageSetting {
    self.view.backgroundColor = c7;
    UIImage *backgroundImage = [UIImage imageNamed:@"icon_share_layer1_bg"];
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 64, kScreenBounds.size.width, kScreenBounds.size.width / (backgroundImage.size.width / backgroundImage.size.height))];
    backgroundImageView.image = backgroundImage;
    backgroundImageView.userInteractionEnabled = YES;
    [self.view addSubview:backgroundImageView];
    
    // 黑色浮层
    self.inviteLayer3 = [[PDInviteLayer3 alloc] addTitles:@[@"收入金币", @"收入竹子", @"邀请人数", @" 有效人数"]];
    [self.backgroundView addSubview:self.inviteLayer3];
    
    // 邀请二维码
    CGSize codeSize = CGSizeMake(LCFloat(408/2), LCFloat(408/2));
    self.inviteLayer1 = [[PDInviteLayer1 alloc] initWithFrame:CGRectMake((kScreenBounds.size.width - codeSize.width) / 2, 64 + LCFloat(5), codeSize.width, codeSize.height)];
    [self.backgroundView addSubview:self.inviteLayer1];
    
    // 填写邀请码
    self.inviteLayer4 = [[PDInviteLayer4 alloc] initWithFrame:CGRectMake(0, self.inviteLayer3.center.y, kScreenBounds.size.width, kScreenBounds.size.height / 2)];
    [self.backgroundView addSubview:self.inviteLayer4];
    
    [self.backgroundView bringSubviewToFront:self.inviteLayer3];
    [self.view bringSubviewToFront:self.backgroundView];
    [self.view bringSubviewToFront:self.titleBar];
    
    
    // 跳转
    __weak typeof(self) weakSelf = self;
    [self.inviteLayer4 didClickDetailActions:^{
        PDInviteDetailViewController *detailViewController = [[PDInviteDetailViewController alloc] init];
        [weakSelf pushViewController:detailViewController animated:YES];
    }];
}

//#pragma mark - 键盘监听
//
//- (void)addNotification {
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showKeyBoard:) name:kKEYBOARD_SHOW object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hideKeyBoard:) name:kKEYBOARD_HIDE object:nil];
//}
//
//- (void)showKeyBoard:(NSNotification *)noti {
//    NSTimeInterval duration = [[noti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    CGRect rect = [[noti.userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
//    
//    [UIView animateWithDuration:duration animations:^{
//        self.view.frame = CGRectMake(0, -rect.size.height, self.view.frame.size.width, self.view.bounds.size.height);
//        self.titleBar.frame = CGRectMake(0, rect.size.height, self.titleBar.frame.size.width, self.titleBar.frame.size.height);
//    }];
//}
//
//- (void)hideKeyBoard:(NSNotification *)noti {
//    NSTimeInterval duration = [[noti.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//    
//    [UIView animateWithDuration:duration animations:^{
//        self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.bounds.size.height);
//        self.titleBar.frame = CGRectMake(0, 0, self.titleBar.frame.size.width, self.titleBar.frame.size.height);
//    }];
//}
//
//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    [self.view endEditing:YES];
//}
//
#pragma mark - 分享

- (void)share {
    __weak typeof(self) weakSelf = self;
    [PDShareSheetView shareWithItemArr:@[@"QQ好友",@"微信好友",@"QQ空间",@"朋友圈"] imageArr:@[@"icon_share_qq",@"icon_share_wx",@"icon_share_qqzone",@"icon_share_wxzone"] clickComplication:^(NSString *item) {
        if ([item isEqualToString:@"QQ好友"]) {
            if ([TencentOAuth iphoneQQInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeQQFriend];
            } else {
                [PDHUD showHUDError:@"您未安装QQ手机客户端！"];
            }
        } else if ([item isEqualToString:@"微信好友"]) {
            if ([WXApi isWXAppInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeWXSession];
            } else {
                [PDHUD showHUDError:@"您未安装微信手机客户端！"];
            }
        } else if ([item isEqualToString:@"QQ空间"]) {
            if ([TencentOAuth iphoneQQInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeQQZone];
            } else {
                [PDHUD showHUDError:@"您未安装QQ手机客户端！"];
            }
        } else if ([item isEqualToString:@"朋友圈"]) {
            if ([WXApi isWXAppInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeWXTimeLine];
            } else {
                [PDHUD showHUDError:@"您未安装微信手机客户端！"];
            }
        }
    }];
}

- (void)shareWithType:(PD3RShareType)type {
    __weak typeof(self) weakSelf = self;
    PDImageView *iv = [[PDImageView alloc] init];
    NSString *picPath = [PDCenterTool absoluteUrlWithRisqueUrl:self.inviteInfo.config.pic];
    [iv uploadImageWithAvatarURL:picPath placeholder:nil callback:^(UIImage *image) {
        NSData *imgData = UIImageJPEGRepresentation(image, 1);
        if (imgData.length >= 32 * 1024) { // 判断字节数
            imgData = [PDCenterTool compressImage:image];
        }
        [PDOAuthLoginAndShareTool sharedWithType:type text:nil title:weakSelf.inviteInfo.config.title desc:weakSelf.inviteInfo.config.content thubImageData:imgData webUrl:weakSelf.inviteInfo.config.url];
    }];
}

#pragma mark - 网络请求

/**
 * 邀请页详细
 */
- (void)fetchData:(void(^)())fetchComplcation {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberInvite requestParams:nil responseObjectClass:[PDInviteInfo class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            PDInviteInfo *info = (PDInviteInfo *)responseObject;
            weakSelf.inviteInfo = info;
            
            // 更新数据
            weakSelf.inviteLayer1.code = [NSString stringWithFormat:@"%@",[PDCenterTool absoluteUrlWithRisqueUrl:[NSString stringWithFormat:@"%@%@",centerInviteCodeUrl,info.memberId]]];
            weakSelf.inviteLayer4.info = info;
            [weakSelf.inviteLayer3 updateDataWithNumberArr:@[@(info.totalGoldAward),@(info.totalBambooAward),@(info.invitedCount),@(info.activateGameUserCount)]];//收入金币、收入竹子、邀请人数、认证人数
            
            if (fetchComplcation) {
                fetchComplcation();
            }
        }
    }];
}

#pragma mark - 弹框

- (void)showAlert {
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"验证成功" conten:[NSString stringWithFormat:@"您已经成功获得%ld金币，并自动帮您关注好友【%@】",(long)self.inviteInfo.invitedAward, self.inviteInfo.nickname] isClose:NO btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissAllCompletion:NULL];
    }];
}

@end
