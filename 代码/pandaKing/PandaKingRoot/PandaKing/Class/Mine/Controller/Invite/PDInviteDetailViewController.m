//
//  PDInviteDetailViewController.m
//  PandaKing
//
//  Created by Cranz on 17/4/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDInviteDetailViewController.h"
#import "PDInviteDataView.h"
#import "PDInviteList.h"
#import "PDInviteDetailCell.h"

@interface PDInviteDetailViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UIView *topView;
/**
 * 横线下面的数据高度
 */
@property (nonatomic, assign) CGFloat dataViewHeight;
/**
 * 整一块黑色的高度
 */
@property (nonatomic, assign) CGFloat naviBgHeight;

@property (nonatomic, strong) PDInviteDataView *dataView;

@property (nonatomic, strong) UIView *tempView;
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSMutableArray *itemArr;

@property (nonatomic, assign) NSUInteger page;

@property (nonatomic, strong) PDInviteList *list;
@end

@implementation PDInviteDetailViewController

- (NSMutableArray *)itemArr {
    if (!_itemArr) {
        _itemArr = [NSMutableArray array];
    }
    return _itemArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupTopView];
    [self setupTableView];
    [self fetchDetail];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)setupTopView {
    _page = 1;
    
    _dataViewHeight = 128 / 2;
    _naviBgHeight = 64 + _dataViewHeight + 35/2;
    self.topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, _naviBgHeight)];
    self.topView.backgroundColor = c2;
    [self.view addSubview:self.topView];
    
    // 图片
    UIImage *bgImage = [UIImage imageNamed:@"icon_lottery_navi_bg"];
    UIImageView *bgImageView = [[UIImageView alloc] initWithImage:bgImage];
    bgImageView.frame = CGRectMake(0, _naviBgHeight - LCFloat(bgImage.size.height), kScreenBounds.size.width, LCFloat(bgImage.size.height));
    bgImageView.alpha = 0.3;
    [self.topView addSubview:bgImageView];
    
    // naviBar
    UIView *titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 20, kScreenBounds.size.width, 44)];
    titleBar.backgroundColor = [UIColor clearColor];
    [self.view addSubview:titleBar];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(kTableViewSectionHeader_left, (44 - 38) / 2, 25, 38)];
    [backButton setImage:[UIImage imageNamed:@"icon_center_back"] forState:UIControlStateNormal];
    __weak typeof(self) weakSelf = self;
    [backButton buttonWithBlock:^(UIButton *button) {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    [titleBar addSubview:backButton];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"收益明细";
    titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    [titleBar addSubview:titleLabel];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width)/2, (44 - titleSize.height)/2, titleSize.width, titleSize.height);
    
    self.dataView = [[PDInviteDataView alloc] initWithFrame:CGRectMake(0, _naviBgHeight - _dataViewHeight, kScreenBounds.size.width, _dataViewHeight)];
    [self.topView addSubview:self.dataView];
}

- (void)setupTableView {
    // 外框框
    UIView *tempImageView = [[UIView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, kTableViewHeader_height + CGRectGetMaxY(self.topView.frame), kScreenBounds.size.width - kTableViewSectionHeader_left * 2, kScreenBounds.size.height - CGRectGetMaxY(self.topView.frame) - kTableViewHeader_height * 2)];
    tempImageView.layer.borderColor = c4.CGColor;
    tempImageView.layer.borderWidth = 1;
    [self.view addSubview:tempImageView];
    self.tempView = tempImageView;
    
    self.mainTableView = [[UITableView alloc] initWithFrame:tempImageView.bounds style:UITableViewStyleGrouped];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    [tempImageView addSubview:self.mainTableView];
    
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.itemArr.count) {
            [weakSelf.itemArr removeAllObjects];
        }
        
        [weakSelf fetchDetail];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        
        if (weakSelf.list.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchDetail];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
        }
    }];
}

#pragma mark - UITabelView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"inviteCellid";
    PDInviteDetailCell *detailCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!detailCell) {
        detailCell = [[PDInviteDetailCell alloc] initWithConstraintWidth:self.mainTableView.frame.size.width reuseIdentifier:cellId];
    }
    
    if (self.itemArr.count > indexPath.row) {
        detailCell.model = self.itemArr[indexPath.row];
    }
    return detailCell; 
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemArr.count) {
        return [PDInviteDetailCell cellHeightWithModel:self.itemArr[indexPath.row]];
    } else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

#pragma mark - 网络请求

- (void)fetchDetail {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerInviteDetails requestParams:@{@"pageNum":@(_page)} responseObjectClass:[PDInviteList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDInviteList *inviteList = (PDInviteList *)responseObject;
            weakSelf.list = inviteList;
            [weakSelf.itemArr addObjectsFromArray:inviteList.items];
            
            [weakSelf updateData];
            [weakSelf.mainTableView reloadData];
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            [weakSelf.mainTableView stopPullToRefresh];
            
            if (weakSelf.itemArr.count == 0) {
                [weakSelf showNoData];
            } else {
                [weakSelf.tempView dismissPrompt];
            }
        }
    }];
}

- (void)updateData {
    self.dataView.model = self.list;
}

- (void)showNoData {
    [self.tempView showPrompt:@"糟糕！暂无数据" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:0 tapBlock:NULL];
}


@end
