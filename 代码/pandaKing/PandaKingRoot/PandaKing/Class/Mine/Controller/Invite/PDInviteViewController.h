//
//  PDInviteViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 邀请好友控制器
@interface PDInviteViewController : AbstractViewController

@end
