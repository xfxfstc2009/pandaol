//
//  PDStoreDetailViewController.h
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 商品详情
@interface PDStoreDetailViewController : AbstractViewController
@property (nonatomic, copy) NSString *ID; // 商品ID；
@end
