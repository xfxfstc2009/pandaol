//
//  PDStoreViewController.m
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDStoreViewController.h"
#import "PDBackpackViewController.h"
#import "PDStoreListTableViewCell.h"
#import "PDWalletViewController.h"
#import "PDStoreMyBambooCell.h"
#import "PDStoreDetailViewController.h"

// 列表model
#import "PDStoreActivityModel.h"

@interface PDStoreViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) PDStoreActivityModel *activityModel;
@end

@implementation PDStoreViewController

- (NSMutableArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchStoreList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor clearColor].CGColor;
}

- (void)basicSetting {
    self.barMainTitle = @"竹子商城";
    
    _page = 1;
    
    __weak typeof(self) weakSelf = self;
    [self rightBarButtonWithTitle:@"背包" barNorImage:nil barHltImage:nil action:^{
        PDBackpackViewController *backpackViewController = [[PDBackpackViewController alloc] init];
        [weakSelf.navigationController pushViewController:backpackViewController animated:YES];
    }];
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.mainTableView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        if (weakSelf.itemsArr.count > 0) {
            [weakSelf.itemsArr removeAllObjects];
        }
        weakSelf.page = 1;
        [weakSelf fetchStoreList];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.activityModel.listPage.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchStoreList];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
        }
    }];
}

#pragma mark -
#pragma mark -- UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else {
        return self.itemsArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSString *cellId = @"myBambooId";
        PDStoreMyBambooCell *myBambooCell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!myBambooCell) {
            myBambooCell = [[PDStoreMyBambooCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        myBambooCell.model = [NSString stringWithFormat:@"%ld",(long)self.activityModel.balance];
        return myBambooCell;
    } else {
        NSString *cellId = @"storeListId";
        PDStoreListTableViewCell *listCell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!listCell) {
            listCell = [[PDStoreListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        
        if (self.itemsArr.count > (indexPath.row)) {
            listCell.model = self.itemsArr[indexPath.row];
        }
        
        return listCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [PDStoreMyBambooCell cellHeight];
    } else {
        return [PDStoreListTableViewCell cellHeight];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return storeListCellUnitVSpaceHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        PDWalletViewController *walletViewConrtroller = [[PDWalletViewController alloc] init];
        [self.navigationController pushViewController:walletViewConrtroller animated:YES];
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
    } else {
        PDStoreActivityItem *item = self.itemsArr[indexPath.row];
        
        PDStoreDetailViewController *detailViewController = [[PDStoreDetailViewController alloc] init];
        detailViewController.ID = item.ID;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

#pragma mark -
#pragma mark -- 网络请求

- (void)fetchStoreList {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:storeList requestParams:@{@"pageNum":@(_page)} responseObjectClass:[PDStoreActivityModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            PDStoreActivityModel *model = (PDStoreActivityModel *)responseObject;
            weakSelf.activityModel = model;
            
            [weakSelf.itemsArr addObjectsFromArray:model.listPage.items];
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            [weakSelf.mainTableView stopPullToRefresh];
            [weakSelf.mainTableView reloadData];
        }
    }];
}

@end
