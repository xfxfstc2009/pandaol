//
//  PDStoreDetailViewController.m
//  PandaKing
//
//  Created by Cranz on 16/11/18.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDStoreDetailViewController.h"
#import "PDCycleScrollView.h"
#import "PDStoreProductDetailCell.h"
#import "PDStoreRuleCell.h"
#import "PDStoreExchangeRecordCell.h"
#import "PDTopUpViewController.h"
#import "PDStoreActivityItem.h"
#import "PDStoreExchangeRecordModel.h"
#import "PDStoreSheetView.h"
#import "PDBackpackViewController.h"
#import "PDStoreWebViewController.h"
#import "PDStoreProductDescCell.h"

@interface PDStoreDetailViewController ()<UITableViewDataSource, UITableViewDelegate, PDTopUpViewControllerDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) UIView *titleBar;
@property (nonatomic, strong) UIImageView *backButtonBg; // 返回按钮背景

@property (nonatomic, strong) PDCycleScrollView *bannerView;
@property (nonatomic, strong) NSMutableArray *itemsArr;

@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, assign) BOOL showButton;
@property (nonatomic, strong) PDSelectedSheetView *sheetView;

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) PDStoreActivityItem *activityItem;
@property (nonatomic, strong) PDStoreExchangeRecordModel  *listModel;
@property (nonatomic, assign) int page;
@end

@implementation PDStoreDetailViewController

- (NSMutableArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self basicSetting];
    [self pageSetting];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
}

- (void)basicSetting {
    _page = 1;
    self.text = @"1.礼品数量有限，先兑先得，兑完即止。\n2.兑换成功后，您可以到“我的背包”中使用礼券兑换礼品。\n3.礼品兑换仅作为回馈用户对盼达电竞的长期支持，不可用于兑换现金。\n4.本规则最终解释权归盼达电竞所有。";
}

- (void)pageSetting {
    [self bannerSetting];
    [self mainSetting];
    [self fetchDetail];
    [self fetchExchangeRecord];
}

- (void)mainSetting {
    // 自定义的导航栏
    self.titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    self.titleBar.alpha = 0;
    self.titleBar.backgroundColor = c2;
    [self.view addSubview:self.titleBar];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"商品详情";
    titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, (44 - titleSize.height) / 2 + 20, titleSize.width, titleSize.height);
    [self.titleBar addSubview:titleLabel];
    
    int backButtonWidth = 31; // 返回按钮（箭头）的宽
    int backButtonHeight = 38; //返回按钮（箭头）的高
    int backButtonBgWidth = 34; //返回按钮背景的宽
    UIImageView *backButtonBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_center_back"]];
    backButtonBg.frame = CGRectMake(kTableViewSectionHeader_left - (backButtonBgWidth - backButtonWidth) / 2, 20 + (44 - backButtonBgWidth) / 2, backButtonBgWidth, backButtonBgWidth);
    [self.view addSubview:backButtonBg];
    self.backButtonBg = backButtonBg;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(kTableViewSectionHeader_left, 20 + (44 - backButtonHeight) / 2, backButtonWidth, backButtonHeight)];
    [backButton setImage:[UIImage imageNamed:@"icon_center_back"] forState:UIControlStateNormal];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 3)];
    __weak typeof(self) weakSelf = self;
    [backButton buttonWithBlock:^(UIButton *button) {
        [PDHUD dismiss];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.view addSubview:backButton];
    
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 49) style:UITableViewStyleGrouped];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    self.mainTableView.tableHeaderView = self.bannerView;
    [self.view addSubview:self.mainTableView];
    
    // 刷新
    [self.mainTableView appendingPullToRefreshHandler:^{
        if (weakSelf.itemsArr.count > 0) {
            [weakSelf.itemsArr removeAllObjects];
        }
        weakSelf.page = 1;
        [weakSelf fetchDetail];
        [weakSelf fetchExchangeRecord];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.listModel.listPage.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchExchangeRecord];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
        }
    }];
    
    self.moreButton = [[UIButton alloc] init];
    self.moreButton.frame = CGRectMake(0, kScreenBounds.size.height - 49, kScreenBounds.size.width, 49);
    [self.moreButton setTitleColor:c1 forState:UIControlStateNormal];
    [self.moreButton setTitle:@"请稍后..." forState:UIControlStateNormal];
    self.moreButton.backgroundColor = [UIColor grayColor];
    self.moreButton.enabled = NO;
    self.moreButton.layer.shadowColor = [UIColor blackColor].CGColor;
    self.moreButton.layer.shadowOpacity = 0.5;
    self.moreButton.layer.shadowRadius = 3;
    self.moreButton.layer.shadowOffset = CGSizeMake(0, -1);
    [self.moreButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.moreButton];
    self.showButton = YES;
    
    [self.view bringSubviewToFront:self.titleBar];
    [self.view bringSubviewToFront:backButtonBg];
    [self.view bringSubviewToFront:backButton];
}

- (void)bannerSetting {
    self.bannerView = [[PDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(300))];
}

#pragma mark -- 兑换

- (void)didClickButton:(UIButton *)button {
    if (self.sheetView.isShowing) {
        return;
    }
    
    [PDHUD showHUDProgress:@"正在打开兑换窗口，请稍后..." diary:0];
    [self fetchGoldNumber];
}

- (void)showButtonAnimationWithAnimated:(BOOL)animated {
    [UIView animateWithDuration:animated? 0.1 : 0 animations:^{
        self.moreButton.frame = CGRectOffset(self.moreButton.frame, 0, -CGRectGetHeight(self.moreButton.frame));
    } completion:^(BOOL finished) {
        self.showButton = YES;
    }];
}

- (void)dismissButtonAnimationWithAnimated:(BOOL)animated {
    [UIView animateWithDuration:animated? 0.1 : 0 animations:^{
        self.moreButton.frame = CGRectOffset(self.moreButton.frame, 0, CGRectGetHeight(self.moreButton.frame));
    } completion:^(BOOL finished) {
        self.showButton = NO;
    }];
}

#pragma mark -- 按钮状态

- (void)buttonEnable {
    [self.moreButton setTitle:@"立即兑换" forState:UIControlStateNormal];
    self.moreButton.backgroundColor = c2;
    self.moreButton.enabled = YES;
}

/** 不再兑换时间*/
- (void)buttonNotTime {
    [self.moreButton setTitle:@"不在活动时间" forState:UIControlStateNormal];
    self.moreButton.backgroundColor = [UIColor grayColor];
    self.moreButton.enabled = NO;
}

- (void)buttonNotEnough {
    [self.moreButton setTitle:@"库存不足" forState:UIControlStateNormal];
    self.moreButton.backgroundColor = [UIColor grayColor];
    self.moreButton.enabled = NO;
}

#pragma mark -- UIscrollView delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    // 200为一个节点
    if (offsetY > 0) {
        self.titleBar.alpha = offsetY / 200;
        self.backButtonBg.alpha = (200 - offsetY) / 200;
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    } else {
        self.titleBar.alpha = 0;
        self.backButtonBg.alpha = 1;
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    }
}

#pragma mark -
#pragma mark -- UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0 || section == 1) {
        return 1;
    } else if (section == 2) {
        return 2;
    } else {
        return self.itemsArr.count + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        
        NSString *cellId = @"porductCellId";
        PDStoreProductDetailCell *detailCell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (detailCell == nil) {
            detailCell = [[PDStoreProductDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        
        detailCell.model = self.activityItem;
        
        return detailCell;
        
    } else if (indexPath.section == 1) {
        
        NSString *cellId = @"descCellId";
//        UITableViewCell *pageCell = [tableView dequeueReusableCellWithIdentifier:cellId];
//        if (!pageCell) {
//            pageCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
//            pageCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//        }
//        
//        pageCell.imageView.image = [UIImage imageNamed:@"icon_snatch_product_detail"];
//        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"图文详情(建议在wifi情况下观看)"];
//        [attributedString addAttributes:@{NSFontAttributeName: [UIFont systemFontOfCustomeSize:14], NSForegroundColorAttributeName:[UIColor grayColor]} range:NSMakeRange(4, 14)];
//        pageCell.textLabel.attributedText = [attributedString copy];
        
        PDStoreProductDescCell *descCell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!descCell) {
            descCell = [[PDStoreProductDescCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        descCell.model = self.activityItem;
        return descCell;
        
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            
            NSString *cellId = @"ruleCellDescId";
            UITableViewCell *ruleCell = [tableView dequeueReusableCellWithIdentifier:cellId];
            if (!ruleCell) {
                ruleCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
                ruleCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            
            ruleCell.imageView.image = [UIImage imageNamed:@"icon_store_rule"];
            ruleCell.textLabel.text = @"兑换规则";
            
            return ruleCell;
            
        } else {
            NSString *cellId = @"ruleCellId";
            PDStoreRuleCell *ruleCell = [tableView dequeueReusableCellWithIdentifier:cellId];
            if (!ruleCell) {
                ruleCell = [[PDStoreRuleCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            }
            
            ruleCell.content = self.text;
            return ruleCell;
        }
        
    } else {
        if (indexPath.row == 0) {
            
            NSString *cellId = @"recordCelId";
            UITableViewCell *recordCell = [tableView dequeueReusableCellWithIdentifier:cellId];
            if (!recordCell) {
                recordCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            }
            
            NSString *sales = [NSString stringWithFormat:@"(已兑换%ld套)",(long)self.activityItem.salesVolume];
            NSMutableAttributedString *recordAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"兑换记录 %@", sales]];
            [recordAtt addAttributes:@{NSFontAttributeName: [UIFont systemFontOfCustomeSize:12], NSForegroundColorAttributeName: c14} range:NSMakeRange(5, sales.length)];
            recordCell.textLabel.attributedText = [recordAtt copy];
            
            return recordCell;
            
        } else {
            
            NSString *cellId = @"exchangeCellId";
            PDStoreExchangeRecordCell *exchangeCell = [tableView dequeueReusableCellWithIdentifier:cellId];
            if (!exchangeCell) {
                exchangeCell  = [[PDStoreExchangeRecordCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            }
            if (indexPath.row - 1 < self.itemsArr.count) {
                exchangeCell.model = self.itemsArr[indexPath.row - 1];
            }
            
            return exchangeCell;
            
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [PDStoreProductDetailCell cellHeightWithModel:self.activityItem.commodity.name];
    } else if (indexPath.section == 1) {
        return [PDStoreProductDescCell cellHeight];
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            return 44;
        }
        return [PDStoreRuleCell cellHeightWithContent:self.text];
    } else {
        if (indexPath.row == 0) {
            return 36;
        }
        return [PDStoreExchangeRecordCell cellHeight];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 1) {
//        [tableView deselectRowAtIndexPath:indexPath animated:YES];
//        PDStoreWebViewController *webViewControler = [[PDStoreWebViewController alloc] init];
//        [webViewControler webDirectedWebUrl:[PDCenterTool absoluteUrlWithRisqueUrl:[NSString stringWithFormat:@"%@?id=%@",storeGraphicDetails,self.ID]]];
//        [self.navigationController pushViewController:webViewControler animated:YES];
//    }
//}

#pragma mark - PDTopUpViewControllerDelegate

- (void)goldExchangeFinish:(PDTopUpViewController *)topupViewController {
    [topupViewController.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 网络请求
/** 会员金币资产*/
- (void)fetchGoldNumber {
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberWallet requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }

        [PDHUD dismiss];
        if (isSucceeded) {
            
            NSNumber *totalbamboo = responseObject[@"totalbamboo"]; // 总资产
            
            // 如果余额不足，提示
            if (totalbamboo.integerValue < weakSelf.activityItem.discountPrice) {
                [weakSelf showNotEnough];
            } else {
                // 按钮缩下去
                if (weakSelf.showButton) {
                    [weakSelf dismissButtonAnimationWithAnimated:YES];
                }
                // 弹起弹窗
                [weakSelf showSelAlertWithBamboo:totalbamboo.integerValue];
            }
        }
    }];
}

/**< 兑换弹框*/
- (void)showSelAlertWithBamboo:(NSInteger)bamboo {
    PDStoreSheetView *sheetView = [[PDStoreSheetView alloc] init];
    self.sheetView = sheetView;
    sheetView.title = [NSString stringWithFormat:@"请选择兑换数量(库存%ld)", (long)self.activityItem.amount];
    sheetView.total = bamboo / (long)self.activityItem.discountPrice;
    sheetView.model = self.activityItem;
    
    __weak typeof(self) weakSelf = self;
    [sheetView submitWithGoldCount:^(NSInteger count) {
        [weakSelf fetchRedeemWithBammoo:(long)count * weakSelf.activityItem.discountPrice quantity:count];
    }];
    [weakSelf dismissSelectedViewWithMoreButtonChangtoNormal];
}

- (void)dismissSelectedViewWithMoreButtonChangtoNormal {
    __weak typeof(self) weakSelf = self;
    [self.sheetView dismissWithActionBlock:^{
        if (weakSelf.showButton == NO) {
            [weakSelf showButtonAnimationWithAnimated:YES];
        }
    }];
}

- (void)showNotEnough {
    __weak typeof(self) weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"余额不足" conten:@"您的竹子余额已经不足啦" isClose:NO btnArr:@[@"去兑换", @"取消"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissWithCompletion:NULL];
        if (buttonIndex == 0) {
            PDTopUpViewController *topUpViewController = [[PDTopUpViewController alloc] init];
            topUpViewController.delegate = weakSelf;
            [weakSelf pushViewController:topUpViewController animated:YES];
        }
    }];
}

/** 商品详情接口*/
- (void)fetchDetail {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:storeDetail requestParams:@{@"id":self.ID} responseObjectClass:[PDStoreActivityItem class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDStoreActivityItem *item = (PDStoreActivityItem *)responseObject;
            weakSelf.activityItem = item;
            
            [weakSelf updateBannerWithImages:item.commodity.pictures];
            [weakSelf updateMoreButtonState];
            
            [weakSelf.mainTableView reloadData];
        }
    }];
}

/** 更新轮播*/
- (void)updateBannerWithImages:(NSArray *)images {
    NSMutableArray *absolutePathArr = [NSMutableArray array];
    for (NSString *relativePath in images) {
        [absolutePathArr addObject:[PDCenterTool absoluteUrlWithRisqueUrl:relativePath]];
    }
    self.bannerView.imageURLStringsGroup = [absolutePathArr copy];
}

- (void)updateMoreButtonState {
    if (self.activityItem.amount <= 0) {
        [self buttonNotEnough];
    } else {
        if ([self.activityItem.state isEqualToString:@"going"]) {
            [self buttonEnable];
        } else {
            [self buttonNotTime];
        }
    }
}

/** 兑换记录*/
- (void)fetchExchangeRecord {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:storeExchangeRecord requestParams:@{@"id":self.ID, @"pageNum":@(_page)} responseObjectClass:[PDStoreExchangeRecordModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.listModel = (PDStoreExchangeRecordModel *)responseObject;
            [weakSelf.itemsArr addObjectsFromArray:weakSelf.listModel.listPage.items];
            
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            [weakSelf.mainTableView stopPullToRefresh];
            [weakSelf.mainTableView reloadSections:[[NSIndexSet alloc] initWithIndex:3] withRowAnimation:UITableViewRowAnimationNone];
        }
    }];
}

/** 请求兑换商品*/
- (void)fetchRedeemWithBammoo:(NSInteger)bamboo quantity:(NSInteger)quantity {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:storeRedeem requestParams:@{@"id":self.ID, @"bamCount":@(bamboo),@"quantity":@(quantity)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            // 刷新数据
            [weakSelf fetchDetail];
            
            if (weakSelf.itemsArr.count) {
                [weakSelf.itemsArr removeAllObjects];
            }
            [weakSelf fetchExchangeRecord];
            [weakSelf showRedmeeSuccess];
        }
    }];
}

- (void)showRedmeeSuccess {
    __weak typeof(self) weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"兑换成功" conten:@"您已成功兑换，现可到我的背包使用兑换券" isClose:NO btnArr:@[@"我的背包",@"不再提示"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissAllCompletion:NULL];
        if (buttonIndex == 0) {
            PDBackpackViewController *backpackViewController = [[PDBackpackViewController alloc] init];
            [weakSelf pushViewController:backpackViewController animated:YES];
        }
    }];
}

@end
