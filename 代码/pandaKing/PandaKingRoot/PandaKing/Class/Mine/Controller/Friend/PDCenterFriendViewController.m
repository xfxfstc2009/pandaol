//
//  PDCenterFriendViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterFriendViewController.h"
#import "PDCenterFriendTableViewCell.h"
#import "PDFindRootNearbyViewController.h"
#import "PDCenterFriendList.h"
#import "PDCenterPersonViewController.h"
#import "PDFindRootNearbyViewController.h"
#import <HTHorizontalSelectionList.h>

@interface PDCenterFriendViewController ()<UITableViewDataSource, UITableViewDelegate, PDCenterFriendTableViewCellDelegate, HTHorizontalSelectionListDelegate, HTHorizontalSelectionListDataSource>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) UIImageView *buttonBgImageView;
@property (nonatomic, assign) NSInteger followPage;
@property (nonatomic, assign) NSInteger fansPage;
@property (nonatomic, strong) PDCenterFriendList *followList;
@property (nonatomic, strong) PDCenterFriendList *fansList;
@property (nonatomic, strong) NSMutableArray *followsArr;
@property (nonatomic, strong) NSMutableArray *fansArr;
@property (nonatomic, strong) PDCenterFriendTableViewCell *friendCell;  /**< 点击添加的cell*/
@property (nonatomic, strong) HTHorizontalSelectionList *listSegment;
@property (nonatomic, strong) NSArray *segmentArr;
@end

@implementation PDCenterFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.listSegment setSelectedButtonIndex:self.selectedIndex animated:YES];
    
    if (self.selectedIndex == PDCenterSocialTypeFollow) {
        if (self.followsArr.count) {
            [self.followsArr removeAllObjects];
        }
        [self fetchFollowList];
    } else {
        if (self.fansArr.count) {
            [self.fansArr removeAllObjects];
        }
        [self fetchFansList];
    }
}

- (void)basicSetting {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    __weak typeof(self) weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_friend_add"] barHltImage:[UIImage imageNamed:@"icon_friend_add"] action:^{
        [weakSelf didClickAdd];
    }];
    
    _followPage = 1;
    _fansPage = 1;
    _followsArr = [NSMutableArray array];
    _fansArr = [NSMutableArray array];
}

- (void)pageSetting {
    [self segmentSetting];
    [self tableViewSetting];
}

- (void)segmentSetting {
    self.segmentArr = @[@"关注", @"粉丝"];
    
    self.listSegment = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake((kScreenBounds.size.width - LCFloat(150)) / 2., (LCFloat(44) - LCFloat(35)) / 2. ,LCFloat(150) , LCFloat(35))];
    self.listSegment.dataSource = self;
    self.listSegment.delegate = self;
    self.listSegment.backgroundColor = [UIColor clearColor];
    [self.listSegment setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    self.listSegment.selectionIndicatorColor = [UIColor colorWithCustomerName:@"白"];
    self.listSegment.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeHeavyBounce;
    self.listSegment.bottomTrimColor = [UIColor clearColor];
    [self.listSegment setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    
    self.listSegment.isNotScroll = YES;
    self.listSegment.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleButtonBorder;
    self.navigationItem.titleView = self.listSegment;

}

- (void)tableViewSetting {
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.tableFooterView = [UIView new];
    tableView.separatorColor = c27;
    [self.view addSubview:tableView];
    self.mainTableView = tableView;
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        if (weakSelf.listSegment.selectedButtonIndex == 0) {
            if (weakSelf.followsArr.count) {
                [weakSelf.followsArr removeAllObjects];
            }
            
            weakSelf.followPage = 1;
            [weakSelf fetchFollowList];
        } else {
            if (weakSelf.fansArr.count) {
                [weakSelf.fansArr removeAllObjects];
            }
            
            weakSelf.fansPage = 1;
            [weakSelf fetchFansList];
        }
        
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.listSegment.selectedButtonIndex == 0) {
            if (weakSelf.followList.hasNextPage) {
                weakSelf.followPage ++;
                [weakSelf fetchFollowList];
            } else {
                [weakSelf.mainTableView stopFinishScrollingRefresh];
                return ;
            }
            
        } else {
            if (weakSelf.fansList.hasNextPage) {
                weakSelf.fansPage ++;
                [weakSelf fetchFansList];
            } else {
                [weakSelf.mainTableView stopFinishScrollingRefresh];
                return;
            }
        }
    }];
}

#pragma mark - UITabelView delegatge

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.listSegment.selectedButtonIndex == 0) {
        return self.followsArr.count;
    } else {
        return self.fansArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *friendCellId = @"friendCellid";
    PDCenterFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:friendCellId];
    if (!cell) {
        cell = [[PDCenterFriendTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:friendCellId];
        cell.delegate = self;
    }
    cell.indexPath = indexPath;
    
    if (self.listSegment.selectedButtonIndex == 0) {
        if (self.followsArr.count) {
            cell.model = self.followsArr[indexPath.row];
        }
    } else {
        if (self.fansArr.count) {
            cell.model = self.fansArr[indexPath.row];
        }
    }
    
    if (self.listSegment.selectedButtonIndex == 0) {
        if (indexPath.row == self.followsArr.count - 1) { // 最后一行
            [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
        }
    } else {
        if (indexPath.row == self.fansArr.count - 1) { // 最后一行
            [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
        }
    }
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PDCenterFriendTableViewCell cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.listSegment.selectedButtonIndex == 0) { // 关注
        if (self.followsArr.count) {
            PDCenterFriendItem *item = [self.followsArr objectAtIndex:indexPath.row];
            NSString *memberId = item.friendInfo.ID;
            PDCenterPersonViewController *personViewController = [[PDCenterPersonViewController alloc] init];
            personViewController.transferMemberId = memberId;
            
            [self pushViewController:personViewController animated:YES];
        }
    } else {    // 粉丝
        if (self.fansArr.count) {
            PDCenterFriendItem *item = [self.fansArr objectAtIndex:indexPath.row];
            NSString *memberId = item.friendInfo.ID;
            PDCenterPersonViewController *personViewController = [[PDCenterPersonViewController alloc] init];
            personViewController.transferMemberId = memberId;
            [self pushViewController:personViewController animated:YES];
        }
    }
}

#pragma mark - UIscroll deldegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [PDCenterTool tableViewScrolledRemoveViscousCancleWithScrollView:scrollView sectionHeight:kTableViewHeader_height];
}

#pragma mark - 控件方法

- (void)didClickAdd {
    PDFindRootNearbyViewController *nearbyViewController = [[PDFindRootNearbyViewController alloc]init];
    [self.navigationController pushViewController:nearbyViewController animated:YES];
}

#pragma mark - PDCenterFriendTableViewCellDelegate

- (void)tableViewCell:(PDCenterFriendTableViewCell *)cell didClickButtonAtIndexPath:(NSIndexPath *)indexPath {
    self.friendCell = cell;
    if (self.listSegment.selectedButtonIndex == 0) {
        if (self.followsArr.count) {
            PDCenterFriendItem *friendItem = [self.followsArr objectAtIndex:indexPath.row];
            [self fetchAddFollow:friendItem];
        }
    } else {
        if (self.fansArr.count) {
            PDCenterFriendItem *friendItem = [self.fansArr objectAtIndex:indexPath.row];
            [self fetchAddFollow:friendItem];
        }
    }
    
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate

- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    if (index == 0) {
        if (self.followsArr.count) {
            [self.followsArr removeAllObjects];
        }
        [self fetchFollowList];
    } else {
        if (self.fansArr.count) {
            [self.fansArr removeAllObjects];
        }
        [self fetchFansList];
    }
}

#pragma mark - 网络请求

- (void)fetchFollowList {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMyfollowlist requestParams:@{@"pageNum":@(_followPage)} responseObjectClass:[PDCenterFriendList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDCenterFriendList *followList = (PDCenterFriendList *)responseObject;
            weakSelf.followList = followList;
            [weakSelf.followsArr addObjectsFromArray:followList.items];
            [weakSelf.mainTableView reloadData];
            [weakSelf.mainTableView dismissPrompt];
            if (weakSelf.followsArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
        
        [weakSelf.mainTableView stopPullToRefresh];
        [weakSelf.mainTableView stopFinishScrollingRefresh];
    }];
}

- (void)fetchFansList {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMyfanslist requestParams:@{@"pageNum":@(_fansPage)} responseObjectClass:[PDCenterFriendList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDCenterFriendList *fansList = (PDCenterFriendList *)responseObject;
            weakSelf.fansList = fansList;
            [weakSelf.fansArr addObjectsFromArray:fansList.items];
            [weakSelf.mainTableView reloadData];
            [weakSelf.mainTableView dismissPrompt];
            if (weakSelf.fansArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
        
        [weakSelf.mainTableView stopFinishScrollingRefresh];
        [weakSelf.mainTableView stopPullToRefresh];
    }];
}

- (void)fetchAddFollow:(PDCenterFriendItem *)memberItem {
    NSString *memberId = memberItem.friendInfo.ID;
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerAddFollow requestParams:@{@"followedMemberId":memberId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            memberItem.followed = YES;
            if (memberItem.followed && memberItem.fans) {
                weakSelf.friendCell.moreButton.imageView.image = [UIImage imageNamed:@"icon_friend_followeach"];
                [weakSelf.friendCell.moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [weakSelf.friendCell.moreButton setTitle:@"互相关注" forState:UIControlStateNormal];
            } else {
                weakSelf.friendCell.moreButton.imageView.image = [UIImage imageNamed:@"icon_friend_followed"];
                [weakSelf.friendCell.moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
                [weakSelf.friendCell.moreButton setTitle:@"已关注" forState:UIControlStateNormal];
            }
        }
    }];
}

#pragma mark - 显示无数据页

- (void)showNoDataPage {
    [self.mainTableView showPrompt:@"好孤单啊!一个朋友都没有...QAQ" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:0 tapBlock:NULL];
}

@end
