//
//  PDCenterFriendViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSUInteger ,PDCenterSocialType) {
    PDCenterSocialTypeFollow,
    PDCenterSocialTypeFans,
};

/// 我的撸友
@interface PDCenterFriendViewController : AbstractViewController
@property (nonatomic, assign) PDCenterSocialType selectedIndex;
@end
