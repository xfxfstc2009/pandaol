//
//  PDSnatchWinViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSnatchWinViewController.h"
#import "PDCenterSnatchTableViewCell.h"
#import "PDSnatchList.h"
#import "PDProductDetailViewController.h"
#import "PDShopRootMainViewController.h"

@interface PDSnatchWinViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) PDSnatchList *snatchList;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@end

@implementation PDSnatchWinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    _page = 1;
    _itemsArr = [NSMutableArray array];
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 64) style:UITableViewStyleGrouped];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.rowHeight = [PDCenterSnatchTableViewCell cellHeight];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.itemsArr.count) {
            [weakSelf.itemsArr removeAllObjects];
        }
        [weakSelf fetchData];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.snatchList.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchData];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            [weakSelf.mainTableView stopPullToRefresh];
            return ;
        }
    }];
}

#pragma mark - UITabelView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.itemsArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"winCellId";
    PDCenterSnatchTableViewCell *winCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!winCell) {
        winCell = [[PDCenterSnatchTableViewCell alloc] initWithType:PDProductSnatchTypeWin reuseIdentifier:cellId];
    }
    if (self.itemsArr.count) {
        winCell.item = self.itemsArr[indexPath.section];
    }

    return winCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemsArr.count) {
        PDSnatchItem *item = self.itemsArr[indexPath.section];
        PDProductDetailViewController *detailViewController = [[PDProductDetailViewController alloc] init];
        detailViewController.productId = item.ID;
        [self pushViewController:detailViewController animated:YES];
    }
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerSnatchWins requestParams:@{@"pageNum":@(_page)} responseObjectClass:[PDSnatchList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDSnatchList *list = (PDSnatchList *)responseObject;
            weakSelf.snatchList = list;
            [weakSelf.itemsArr addObjectsFromArray:list.items];
            [weakSelf.mainTableView reloadData];
            if (weakSelf.itemsArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
        
        [weakSelf.mainTableView stopPullToRefresh];
        [weakSelf.mainTableView stopFinishScrollingRefresh];
    }];
}

#pragma mark - 显示无数据页

- (void)showNoDataPage {
    __weak typeof(self) weakSelf = self;
    [self.mainTableView showPrompt:@"哎呀！你还没有命中过任何夺宝，要加油啊！骚年QAQ" withImage:[UIImage imageNamed:@"icon_nodata_panda"] buttonTitle:@"立即夺宝" clickBlock:^{
        if ([AccountModel sharedAccountModel].isShenhe){
            return ;
        }
        
        [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        [[PDMainTabbarViewController sharedController] setSelectedIndex:1 animated:YES];

    }];
}

@end
