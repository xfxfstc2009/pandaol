//
//  PDSnatchNoteViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSnatchNoteViewController.h"
#import "PDSnatchProgressViewController.h"
#import "PDSnatchEndViewController.h"
#import "PDSnatchWinViewController.h"
#import "CCZSegmentController.h"

@interface PDSnatchNoteViewController ()

@end

@implementation PDSnatchNoteViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
}

- (void)basicSetting {
    self.barMainTitle = @"夺宝记录";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)pageSetting {
    PDSnatchProgressViewController *progressViewController = [[PDSnatchProgressViewController alloc] init];
    PDSnatchEndViewController *endViewController = [[PDSnatchEndViewController alloc] init];
    PDSnatchWinViewController *winViewController = [[PDSnatchWinViewController alloc] init];
    
    CCZSegmentController *segment = [[CCZSegmentController alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height) titles:@[@"进行中",@"已结束",@"已中奖"]];
    segment.segmentView.segmentTintColor = c26;
    segment.segmentView.separateColor = segment.segmentView.segmentTintColor;
    segment.viewControllers = @[progressViewController, endViewController, winViewController];
    [self addSegmentController:segment];
}

@end
