//
//  PDSnatchEndViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 夺宝结束控制器
@interface PDSnatchEndViewController : AbstractViewController

@end
