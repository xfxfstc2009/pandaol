//
//  PDMineInformationFensiTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMineInformationFensiTableViewCell.h"

@interface PDMineInformationFensiTableViewCell()
@property (nonatomic,strong)UILabel *fensiDymicLabel;
@property (nonatomic,strong)UILabel *fensiFixedLabel;
@property (nonatomic,strong)UIButton *fensiButton;
@property (nonatomic,strong)UILabel *guanzhuDymicLabel;
@property (nonatomic,strong)UILabel *guanzhuFixedLabel;
@property (nonatomic,strong)UIButton *guanzhuButton;

@end

@implementation PDMineInformationFensiTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1.
    __weak typeof(self)weakSelf = self;
    [self createViewWithIndex:0 block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf= weakSelf;
        NSLog(@"1");
    }];
    
    [self createViewWithIndex:1 block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf= weakSelf;
        NSLog(@"2");
    }];
}


-(UIView *)createViewWithIndex:(NSInteger)index block:(void(^)())block{
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor clearColor];
    if (index == 0){
        bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width / 2., [PDMineInformationFensiTableViewCell calculationCellHeight]);
    } else {
        bgView.frame = CGRectMake(kScreenBounds.size.width / 2., 0, kScreenBounds.size.width / 2., [PDMineInformationFensiTableViewCell calculationCellHeight]);
    }
    [self addSubview:bgView];
    
    // 2. 创建label
    UILabel *dymicLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    dymicLabel.backgroundColor = [UIColor clearColor];
    dymicLabel.font = [UIFont systemFontOfCustomeSize:30.];
    dymicLabel.textAlignment = NSTextAlignmentCenter;
    dymicLabel.adjustsFontSizeToFitWidth = YES;
    if (index == 0){
        self.fensiDymicLabel = dymicLabel;
    } else {
        self.guanzhuDymicLabel = dymicLabel;
    }
    [bgView addSubview:dymicLabel];
    
    // 3. 创建fixed
    UILabel *fixedLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    fixedLabel.textAlignment = NSTextAlignmentCenter;
    if (index == 0){
        fixedLabel.text = @"粉丝";
    } else {
        fixedLabel.text = @"关注";
    }
    [bgView addSubview:fixedLabel];
    
    UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    actionButton.frame = bgView.bounds;
    __weak typeof(self)weakSelf = self;
    [actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    [bgView addSubview:actionButton];
    
    
    CGFloat margin = ([PDMineInformationFensiTableViewCell calculationCellHeight] - [NSString contentofHeightWithFont:dymicLabel.font] - [NSString contentofHeightWithFont:fixedLabel.font]) / 3.;
    dymicLabel.frame = CGRectMake(0, margin, kScreenBounds.size.width / 2., [NSString contentofHeightWithFont:dymicLabel.font]);
    
    fixedLabel.frame = CGRectMake(0, CGRectGetMaxY(dymicLabel.frame) + margin, kScreenBounds.size.width / 2., [NSString contentofHeightWithFont:fixedLabel.font]);
    
    
    return bgView;
}


-(void)setTransferSingleModel:(PDCenterMyInfo *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    self.fensiDymicLabel.text = [NSString stringWithFormat:@"%li",transferSingleModel.fans];
    self.guanzhuDymicLabel.text = [NSString stringWithFormat:@"%li",transferSingleModel.follow];

}

-(void)setTransferPersonSingleModel:(PDPersonSingleModel *)transferPersonSingleModel{
    _transferPersonSingleModel = transferPersonSingleModel;
    self.fensiDymicLabel.text = [NSString stringWithFormat:@"%@",transferPersonSingleModel.fans];
    self.guanzhuDymicLabel.text = [NSString stringWithFormat:@"%@",transferPersonSingleModel.follow];

}

+(CGFloat)calculationCellHeight{
    return LCFloat(80);
}

@end
