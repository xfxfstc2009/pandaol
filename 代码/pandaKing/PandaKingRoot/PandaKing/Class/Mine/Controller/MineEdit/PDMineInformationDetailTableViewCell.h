//
//  PDMineInformationDetailTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDMineInformationDetailTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;

@property (nonatomic,copy)NSString *transferInfo;

+(CGFloat)calculationCellHeightWithInfo:(NSString *)info;

@end
