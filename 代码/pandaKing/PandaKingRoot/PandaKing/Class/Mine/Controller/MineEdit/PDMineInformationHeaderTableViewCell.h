//
//  PDMineInformationHeaderTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterMyInfo.h"

@interface PDMineInformationHeaderTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDCenterMyInfo *transferSingleModel;


+(CGFloat)calculationCellHeight;
@end
