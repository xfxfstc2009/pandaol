//
//  PDMineInformationFensiTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterMyInfo.h"
#import "PDPersonSingleModel.h"

@interface PDMineInformationFensiTableViewCell : UITableViewCell

@property (nonatomic,strong)PDCenterMyInfo *transferSingleModel;
@property (nonatomic,strong)PDPersonSingleModel *transferPersonSingleModel;
+(CGFloat)calculationCellHeight;

@end
