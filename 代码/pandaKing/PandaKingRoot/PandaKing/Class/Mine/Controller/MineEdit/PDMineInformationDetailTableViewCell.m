//
//  PDMineInformationDetailTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMineInformationDetailTableViewCell.h"

@interface PDMineInformationDetailTableViewCell()
@property (nonatomic,strong)UILabel *descLabel;

@end

@implementation PDMineInformationDetailTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.descLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.descLabel.numberOfLines = 0;
    [self addSubview:self.descLabel];
}

#pragma mark - SET
-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferInfo:(NSString *)transferInfo{
    _transferInfo = transferInfo;
    self.descLabel.text = transferInfo;
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    if (descSize.height >= 2 * [NSString contentofHeightWithFont:self.descLabel.font]){
        self.descLabel.textAlignment = NSTextAlignmentLeft;
    } else {
        self.descLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    self.descLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), descSize.height);
}

+(CGFloat)calculationCellHeightWithInfo:(NSString *)info{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    CGSize descSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += descSize.height;
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end
