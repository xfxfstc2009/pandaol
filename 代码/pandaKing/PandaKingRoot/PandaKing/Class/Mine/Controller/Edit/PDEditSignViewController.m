//
//  PDEditSignViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDEditSignViewController.h"
#import "PDEditSignTableViewCell.h"
#import "PDCenterTool.h"

@interface PDEditSignViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) PDTextView *textView;
@end

@implementation PDEditSignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.textView becomeFirstResponder];
}

- (void)basicSetting {
    self.barMainTitle = @"个性签名";
    __weak typeof(self) weakSelf = self;
    [self rightBarButtonWithTitle:@"保存" barNorImage:nil barHltImage:nil action:^{
        if (weakSelf.textView.text.length == 0) {
            [PDHUD showHUDSuccess:@"输入不能为空!"];
            return;
        }
        [weakSelf fetchData];
    }];
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.rowHeight = [PDEditSignTableViewCell cellHeight];
    self.mainTableView.tableFooterView = [[UIView alloc] init];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
}

#pragma mark - UITabelView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"editSignId";
    PDEditSignTableViewCell *signCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!signCell) {
        signCell = [[PDEditSignTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        signCell.textView.text = self.transferInfo;
        signCell.textView.place = @"恕我直言，在座的各位都是渣渣～";
        signCell.textView.limitMax = 20;
        self.textView = signCell.textView;
        [PDCenterTool calculateCell:signCell leftSpace:0 rightSpace:0];
    }
    return signCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

#pragma mark -
#pragma mark -- 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:editSign requestParams:@{@"signature": self.textView.text} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            [weakSelf saveDelegate];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)saveDelegate {
    if ([self.delegate respondsToSelector:@selector(signViewController:didSavedSign:)]) {
        [self.delegate signViewController:self didSavedSign:self.textView.text];
    }
}

@end
