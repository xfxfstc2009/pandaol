//
//  PDEditNickNameViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDEditNickNameViewController.h"

@interface PDEditNickNameViewController ()

@end

@implementation PDEditNickNameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
}

- (void)basicSetting {
    self.barMainTitle = @"更改昵称";
    self.placeholder = @"请输入您的昵称(注意长度不得超过11个字符)";
    
    __weak typeof(self) weakSelf = self;
    [self saveWithInfoBlock:^(NSString *info) {
        if (info.length == 0) {
            [PDHUD showHUDSuccess:@"输入不能为空!"];
            return;
        }
        
        if (info.length > 11) {
            [PDHUD showHUDSuccess:@"昵称长度最长为11个字符"];
            return;
        }
        
        weakSelf.transferInfo = info;
        [weakSelf fetchDataWith:info];
        
    }];
}

- (void)fetchDataWith:(NSString *)info {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:editNickName requestParams:@{@"nickname": info} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            [AccountModel sharedAccountModel].memberInfo.nickname = info;
            [weakSelf saveDelegate];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)saveDelegate {
    if ([self.delegate respondsToSelector:@selector(userInfoViewController:didSavedWithInfo:atIndexOfName:)]) {
        [self.delegate userInfoViewController:self didSavedWithInfo:self.transferInfo atIndexOfName:@"昵称"];
    }
}

@end
