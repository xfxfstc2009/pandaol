//
//  PDEditViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDEditViewController.h"
#import "PDCenterTool.h"
#import "PDEditNickNameViewController.h"
#import "PDEditQQViewController.h"
#import "PDEditWeChatViewController.h"
#import "PDEditSignViewController.h"
#import "PDPickView.h"
#import "PDMemberInfoModel.h"

// 地域划分
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapSearchKit/AMapSearchKit.h>

static CGFloat const kEditingAvatarCell_height = 80;

@interface PDEditViewController ()<UITableViewDataSource, UITableViewDelegate, PDEditUserInfoViewControllerDelegate, AMapSearchDelegate, UIPickerViewDelegate, UIPickerViewDataSource, PDEditSignViewControllerDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSArray *sectionArr;
@property (nonatomic, strong) NSArray *sectionTitleArr;
@property (nonatomic, strong) PDMemberInfoModel *memberInfo;

// 地区搜索
@property (nonatomic, strong) AMapSearchAPI *search;
@property (nonatomic, strong) NSMutableArray *provinceArr;
@property (nonatomic, strong) NSMutableArray *cityArr;
@property (nonatomic, strong) NSMutableArray *districtArr;
@property (nonatomic, strong) UIPickerView *addressPickerView;
@property (nonatomic, strong) UITableViewCell *addressCell; /**< 用来改变detailText*/
@property (nonatomic, assign) NSInteger selectedSection;

// 生日
@property (nonatomic, strong) UIPickerView *birthPickerView;
@property (nonatomic, strong) NSArray *yearArr;
@property (nonatomic, strong) NSArray *monthArr;
@property (nonatomic, strong) NSArray *dayArr;
@property (nonatomic, strong) UITableViewCell *ageCell;

// 性别
@property (nonatomic, strong) UITableViewCell *genderCell;

// 头像
@property (nonatomic, strong) UITableViewCell *avatarCell;
@end

@implementation PDEditViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.barMainTitle = @"编辑资料";
    self.sectionArr = @[@[@"头像",@"昵称",@"性别",@"年龄",@"所在城市"],@[@"QQ",@"微信",@"个性签名"]];
    self.sectionTitleArr = @[@"个人资料",@"联系方式"];
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [[UIView alloc] init];
    self.mainTableView.separatorColor = c27;
    self.mainTableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.mainTableView];
}

#pragma mark -
#pragma mark -- UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.sectionArr[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"editCellId";
    NSString *sectionString = [self.sectionArr[indexPath.section] objectAtIndex:indexPath.row];
    UITableViewCell *editCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!editCell) {
        editCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
        editCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        if ([sectionString isEqualToString:@"头像"]) {
            CGFloat space = 10; // 头像距上下边距
            CGFloat right = 35; // 右侧预留空间
            CGFloat image_width = kEditingAvatarCell_height - 2 * space;
            PDImageView *avatarImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width - right - image_width, space, image_width, image_width)];
            avatarImageView.backgroundColor = BACKGROUND_VIEW_COLOR;
            avatarImageView.layer.cornerRadius = image_width / 2;
            avatarImageView.clipsToBounds = YES;
            avatarImageView.stringTag = @"avatarImageView";
            [editCell.contentView addSubview:avatarImageView];
            self.avatarCell = editCell;
        }
    }
    if ([sectionString isEqualToString:@"个性签名"]) {
        editCell.accessoryType = UITableViewCellAccessoryNone;
        [PDCenterTool calculateCell:editCell leftSpace:0 rightSpace:0];
    }
    
    // 数据展示
    PDImageView *avatarImageView = (PDImageView *)[editCell viewWithStringTag:@"avatarImageView"];
    [avatarImageView uploadImageWithURL:self.memberInfo.avatar placeholder:nil callback:NULL];
    
    if ([sectionString isEqualToString:@"昵称"]) {
        editCell.detailTextLabel.text = self.memberInfo.nickname;
    } else if ([sectionString isEqualToString:@"性别"]) {
        editCell.detailTextLabel.text = [self.memberInfo.gender isEqualToString:@"MALE"]? @"男" : @"女";
        self.genderCell = editCell;
    } else if ([sectionString isEqualToString:@"年龄"]) {
        editCell.detailTextLabel.text = [NSString stringWithFormat:@"%ld",(long)self.memberInfo.age];
        self.ageCell = editCell;
    } else if ([sectionString isEqualToString:@"所在城市"]) {
        editCell.detailTextLabel.text = [NSString stringWithFormat:@"%@-%@-%@",self.memberInfo.address.province.name.length? self.memberInfo.address.province.name : @"省",self.memberInfo.address.city.name.length? self.memberInfo.address.city.name :@"市" ,self.memberInfo.address.district.name.length? self.memberInfo.address.district.name : @"区"];
        self.addressCell = editCell;
    } else if ([sectionString isEqualToString:@"QQ"]) {
        editCell.detailTextLabel.text = self.memberInfo.qq;
    } else if ([sectionString isEqualToString:@"微信"]) {
        editCell.detailTextLabel.text = self.memberInfo.weChat;
    } else if ([sectionString isEqualToString:@"个性签名"]) {
        editCell.detailTextLabel.text = self.memberInfo.signature;
    }
    
    editCell.textLabel.text = sectionString;
    return editCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewSectionHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == 0) {
        return kEditingAvatarCell_height;
    } else {
        return 44;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    CGFloat label_bottom = 3;
    UILabel *label = [[UILabel alloc] init];
    label.text = self.sectionTitleArr[section];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfCustomeSize:13];
    label.textColor = [UIColor grayColor];
    CGSize size = [label.text sizeWithCalcFont:label.font];
    label.frame = CGRectMake(kTableViewSectionHeader_left, kTableViewSectionHeader_height - size.height - label_bottom, size.width, size.height);
    [headerView addSubview:label];
    
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *sectionString = [self.sectionArr[indexPath.section] objectAtIndex:indexPath.row];
    
    if ([sectionString isEqualToString:@"昵称"]) {
        PDEditNickNameViewController *nickNameViewController = [[PDEditNickNameViewController alloc] init];
        nickNameViewController.transferInfo = self.memberInfo.nickname;
        nickNameViewController.delegate = self;
        nickNameViewController.indexPath = indexPath;
        [self pushViewController:nickNameViewController animated:YES];
    } else if ([sectionString isEqualToString:@"性别"]) {
        [self changeGender];
    } else if ([sectionString isEqualToString:@"年龄"]) {
        [self birthPickerViewSetting];
    } else if ([sectionString isEqualToString:@"所在城市"]) {
        [self getMapArea];
    } else if ([sectionString isEqualToString:@"QQ"]) {
        PDEditQQViewController *qqViewController = [[PDEditQQViewController alloc] init];
        qqViewController.transferInfo = self.memberInfo.qq;
        qqViewController.delegate = self;
        qqViewController.indexPath = indexPath;
        [self pushViewController:qqViewController animated:YES];
    } else if ([sectionString isEqualToString:@"微信"]) {
        PDEditWeChatViewController *weChatViewController = [[PDEditWeChatViewController alloc] init];
        weChatViewController.transferInfo = self.memberInfo.weChat;
        weChatViewController.delegate = self;
        weChatViewController.indexPath = indexPath;
        [self pushViewController:weChatViewController animated:YES];
    } else if ([sectionString isEqualToString:@"个性签名"]) {
        PDEditSignViewController *signViewController = [[PDEditSignViewController alloc] init];
        signViewController.delegate = self;
        signViewController.indexPath = indexPath;
        signViewController.transferInfo = self.memberInfo.signature;
        [self pushViewController:signViewController animated:YES];
    } else if ([sectionString isEqualToString:@"头像"]) {
        __weak typeof(self) weakSelf = self;
        GWAssetsLibraryViewController *assetVC = [[GWAssetsLibraryViewController alloc]init];
        [assetVC selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if (selectedImgArr.count == 0) {
                return ;
            }
            [weakSelf fetchEditAvatar:selectedImgArr.firstObject];
        }];
        assetVC.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:assetVC animated:YES];
    }
}

- (void)changeGender {
    __weak typeof(self)weakSelf = self;
    [[PDAlertView sharedAlertView] showGenderAlertWithEdit:^(alertGenderType type) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (type == alertGenderTypeBoy) {
            [strongSelf fetchEditGender:@"MALE"];
        } else {
            [strongSelf fetchEditGender:@"FEMALE"];
        }
    }];
}

#pragma mark - PDEditUserInfoViewControllerDelegate

- (void)userInfoViewController:(PDEditUserInfoViewController *)viewController didSavedWithInfo:(NSString *)info atIndexOfName:(NSString *)name {
    if ([name isEqualToString:@"昵称"]) {
        self.memberInfo.nickname = info;
        [Tool userDefaulteWithKey:CustomerNickname Obj:info];
    } else if ([name isEqualToString:@"QQ"]) {
        self.memberInfo.qq = info;
    } else if ([name isEqualToString:@"微信"]) {
        self.memberInfo.weChat = info;
    }
    
    [self.mainTableView reloadRowsAtIndexPaths:@[viewController.indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -- sign delegate

- (void)signViewController:(PDEditSignViewController *)viewController didSavedSign:(NSString *)sign {
    self.memberInfo.signature = sign;
    [self.mainTableView reloadRowsAtIndexPaths:@[viewController.indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -- 生日

- (void)birthPickerViewSetting {
    PDAgePickView *birthPickerView = [[PDAgePickView alloc] initWithTitle:@"出生年月日"];
    self.birthPickerView = birthPickerView.pickerView;
    self.birthPickerView.delegate = self;
    self.birthPickerView.dataSource = self;
    
    __weak typeof(self) weakSelf = self;
    [birthPickerView clickSure:^{
        [weakSelf fetchEditAge];
    }];
    
    if (self.yearArr.count) { //防止重复创建
        return;
    }
    
    // 岂是年份
    NSUInteger startYear = 1950;

    // 当前的年份
    NSUInteger nowYear = [self curretYear];
    
    NSMutableArray *yearArr = [NSMutableArray array];
    for (NSUInteger y = startYear; y <= nowYear; y++) {
        NSString *s = [NSString stringWithFormat:@"%ld年",(long)y];
        [yearArr addObject:s];
    }
    
    NSMutableArray *monthArr = [NSMutableArray array];
    for (int m = 1; m < 13; m++) {
        NSString *month = [NSString stringWithFormat:@"%d月",m];
        [monthArr addObject:month];
    }
    
    NSMutableArray *dayArr = [NSMutableArray array];
    for (int d = 1; d < 32; d++) {
        NSString *day = [NSString stringWithFormat:@"%d号",d];
        [dayArr addObject:day];
    }
    
    self.yearArr = [yearArr copy];
    self.monthArr = [monthArr copy];
    self.dayArr = [dayArr copy];
}

- (NSUInteger)curretYear {
    // 当前的年份
    NSDate *date = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger flag = NSYearCalendarUnit;
    NSDateComponents *dateComponent = [calendar components:flag fromDate:date];
    NSUInteger nowYear = [dateComponent year];
    return nowYear;
}

#pragma mark -
#pragma mark -- 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberInfo requestParams:nil responseObjectClass:[PDMemberInfoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            PDMemberInfoModel *memerInfoModel = (PDMemberInfoModel *)responseObject;
            weakSelf.memberInfo = memerInfoModel;
            [weakSelf.mainTableView reloadData];
        }
    }];
}

- (void)fetchEditAvatar:(UIImage *)img {
    FetchFileModel *file = [[FetchFileModel alloc]init];
    file.uploadImage = img;
    file.keyName = @"avatar";
    FetchModel *fileModel = [[FetchModel alloc] init];
    fileModel.requestFileDataArr = @[file];
    __weak typeof(self)weakSelf = self;
    [fileModel fetchWithPath:editAvatar completionHandler:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            UIImageView *avatar = (UIImageView *)[strongSelf.avatarCell viewWithStringTag:@"avatarImageView"];
            [[PDPandaRootViewController sharedController] uploadUserAvatar:img];
            avatar.image = img;
        }
    }];
}

- (void)fetchEditAddress {
    NSDictionary *provinceDic = self.provinceArr.count? self.provinceArr[[self.addressPickerView selectedRowInComponent:0]]:@{@"":@""};
    NSString *proCode = provinceDic.allKeys.firstObject;
    NSString *proName = provinceDic.allValues.firstObject;
    
    NSDictionary *cityDic = self.cityArr.count? self.cityArr[[self.addressPickerView selectedRowInComponent:1]]:@{@"":@""};
    NSString *cityCode = cityDic.allKeys.firstObject;
    NSString *cityName = cityDic.allValues.firstObject;
    
    NSDictionary *districtDic = self.districtArr.count? self.districtArr[[self.addressPickerView selectedRowInComponent:2]]:@{@"":@""};
    NSString *disCode = districtDic.allKeys.firstObject;
    NSString *disName = districtDic.allValues.firstObject;
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:editAddress requestParams:@{@"province":proCode,@"provinceName": proName, @"city": cityCode, @"cityName": cityName, @"district": disCode, @"districtName": disName} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            self.addressCell.detailTextLabel.text = [NSString stringWithFormat:@"%@-%@-%@",proName, cityName, disName];
        }
    }];
    
}

- (void)fetchEditAge {
    NSString *year = self.yearArr[[self.birthPickerView selectedRowInComponent:0]];
//    NSString *month = self.monthArr[[self.birthPickerView selectedRowInComponent:1]];
//    NSString *day = self.dayArr[[self.birthPickerView selectedRowInComponent:2]];
    
    NSUInteger nowYear = [self curretYear];
    NSUInteger age = nowYear - year.integerValue + 1;

    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:editAge requestParams:@{@"age": @(age)} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.ageCell.detailTextLabel.text = [NSString stringWithFormat:@"%ld",(long)age];
        }
    }];
}

- (void)fetchEditGender:(NSString *)gender {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:editGender requestParams:@{@"gender":gender} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.genderCell.detailTextLabel.text = [gender isEqualToString:@"MALE"]? @"男" : @"女";
        }
    }];
}



#pragma mark -
#pragma mark -- 获取地域划分

- (NSMutableArray *)provinceArr {
    if (!_provinceArr) {
        _provinceArr = [NSMutableArray array];
    }
    return _provinceArr;
}

- (NSMutableArray *)cityArr {
    if (!_cityArr) {
        _cityArr = [NSMutableArray array];
    }
    return _cityArr;
}

- (NSMutableArray *)districtArr {
    if (!_districtArr) {
        _districtArr = [NSMutableArray array];
    }
    return _districtArr;
}

- (AMapSearchAPI *)search {
    if (!_search) {
        _search = [[AMapSearchAPI alloc] init];
        _search.delegate = self;
    }
    return _search;
}

- (void)getMapArea {
    _selectedSection = -1; //-1 刷新第一个
    
    AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
    [self.search AMapDistrictSearch:dist];
    
    PDAddressPickView *pickerView = [[PDAddressPickView alloc] initWithTitle:@"所在城市"];
    self.addressPickerView = pickerView.pickerView;
    self.addressPickerView.delegate = self;
    self.addressPickerView.dataSource = self;
    
    __weak typeof(self) weakSelf = self;
    [pickerView clickSure:^{
        [weakSelf fetchEditAddress];
        
        // 清空另两个数组， 否则会显示错误，因为我抢饮用了pickerView
        if (weakSelf.cityArr.count) {
            [weakSelf.cityArr removeAllObjects];
        }
        if (weakSelf.districtArr.count) {
            [weakSelf.districtArr removeAllObjects];
        }
    }];
}

#pragma mark -- 搜索回调

- (void)onDistrictSearchDone:(AMapDistrictSearchRequest *)request response:(AMapDistrictSearchResponse *)response {
    if (!response) {
        return;
    }
    
    if (_selectedSection == -1) {
        if (self.provinceArr.count) {
            [self.provinceArr removeAllObjects];
        }
        for (AMapDistrict *dist in response.districts) {
            if (dist.districts.count > 0) {
                for (AMapDistrict *province in dist.districts) {
                    [self.provinceArr addObject:@{province.adcode: province.name}];
                }
            }
        }
        
        [self.addressPickerView reloadComponent:0];
        [self.addressPickerView selectRow:0 inComponent:0 animated:YES];
        return;
    }
    
    
    for (AMapDistrict *dist in response.districts) {
        if ([dist.level isEqualToString:@"province"]) {
            
//            if (dist.districts.count > 0) {
                if (self.cityArr.count) {
                    [self.cityArr removeAllObjects];
                }
                for (AMapDistrict *city in dist.districts) {
                    [self.cityArr addObject:@{city.adcode: city.name}];
                }
//            }
            
        } else if ([dist.level isEqualToString:@"city"]) {
            
//            if (dist.districts.count > 0) {
                if (self.districtArr.count) {
                    [self.districtArr removeAllObjects];
                }
                for (AMapDistrict *district in dist.districts) {
                    [self.districtArr addObject:@{district.adcode: district.name}];
                }
//            }
        }
    }
    
    
    if (_selectedSection == 0) {
        [self.addressPickerView reloadComponent:1];
        [self.addressPickerView selectRow:0 inComponent:1 animated:YES];
    } else if (_selectedSection == 1) {
        [self.addressPickerView reloadComponent:2];
        [self.addressPickerView selectRow:0 inComponent:2 animated:YES];
    } else if (_selectedSection == -1) {
        [self.addressPickerView reloadComponent:0];
    }
}

- (void)AMapSearchRequest:(id)request didFailWithError:(NSError *)error {
    DLog(@"AMapSearch Error: %@", error);
}

#pragma mark -
#pragma mark -- PickerView delegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == self.addressPickerView) {
        if (component == 0) {
            return self.provinceArr.count;
        } else if (component == 1) {
            return self.cityArr.count;
        } else {
            return self.districtArr.count;
        }

    } else {
        if (component == 0) {
            return self.yearArr.count;
        } else if (component == 1) {
            return self.monthArr.count;
        } else {
            return self.dayArr.count;
        }
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (pickerView == self.addressPickerView) {
        _selectedSection = component;
        if (component == 0) {
            if (self.provinceArr.count) {
                AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
                dist.keywords = [[[self.provinceArr objectAtIndex:row] allValues] firstObject];
                [self.search AMapDistrictSearch:dist];
            }
        } else if (component == 1) {
            if (self.cityArr.count) {
                AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
                dist.keywords = [[[self.cityArr objectAtIndex:row] allValues] firstObject];
                [self.search AMapDistrictSearch:dist];
            }
        } else {
            if (self.districtArr.count) {
                AMapDistrictSearchRequest *dist = [[AMapDistrictSearchRequest alloc] init];
                dist.keywords = [[[self.districtArr objectAtIndex:row] allValues] firstObject];
                [self.search AMapDistrictSearch:dist];
            }
        }
        
    } else {
        if (component == 0) {
            [pickerView selectRow:0 inComponent:1 animated:YES];
            [pickerView selectRow:0 inComponent:2 animated:YES];
        } else if (component == 1) {
            [pickerView selectRow:0 inComponent:2 animated:YES];
        }
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont systemFontOfCustomeSize:16];
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = [UIColor blackColor];
    
    if (pickerView == self.addressPickerView) {
        if (component == 0) {
            if (self.provinceArr.count) {
                label.text = [[self.provinceArr[row] allValues] firstObject];
            }
        } else if (component == 1) {
            if (self.cityArr.count) {
                label.text = [[self.cityArr[row] allValues] firstObject];
            }
        } else {
            if (self.districtArr.count) {
                label.text = [[self.districtArr[row] allValues] firstObject];
            }
        }
        
    } else {
        if (component == 0) {
            label.text = self.yearArr[row];
        } else if (component == 1) {
            label.text = self.monthArr[row];
        } else {
            label.text = self.dayArr[row];
        }
    }
    
    return label;
}

@end
