//
//  PDEditUserInfoViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDEditUserInfoViewController.h"
#import "PDCenterTool.h"
#import "PDEditInfoTableViewCell.h"

typedef void(^infoBlcok)(NSString *info);
@interface PDEditUserInfoViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) PDEditInfoTableViewCell *editInfoCell;
@property (nonatomic, copy)   infoBlcok block;
@end

@implementation PDEditUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self _basicSetting];
    [self _pageSetting];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self.editInfoCell.textField becomeFirstResponder];
}

- (void)_basicSetting {
    __weak typeof(self) weakSelf = self;
    [self rightBarButtonWithTitle:@"保存" barNorImage:nil barHltImage:nil action:^{
        weakSelf.block(weakSelf.editInfoCell.textField.text);
    }];
}

- (void)_pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.rowHeight = [PDEditInfoTableViewCell cellHeight];
    self.mainTableView.tableFooterView = [[UIView alloc] init];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
}

- (void)saveWithInfoBlock:(void (^)(NSString *))infoBlcok {
    self.block = infoBlcok;
}

#pragma mark -
#pragma mark -- UITabelView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"editCellId";
    PDEditInfoTableViewCell *editInfoCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!editInfoCell) {
        editInfoCell = [[PDEditInfoTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        editInfoCell.selectionStyle = UITableViewCellSelectionStyleNone;
        editInfoCell.textField.placeholder = self.placeholder;
        editInfoCell.textField.text = self.transferInfo;
        self.editInfoCell = editInfoCell;
    }
    [PDCenterTool calculateCell:editInfoCell leftSpace:0 rightSpace:0];
    return editInfoCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

@end
