//
//  PDEditSignViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 编辑个人签名
@protocol PDEditSignViewControllerDelegate;
@interface PDEditSignViewController : AbstractViewController
@property (nonatomic, copy) NSString *transferInfo;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, weak) id<PDEditSignViewControllerDelegate> delegate;
@end

@protocol PDEditSignViewControllerDelegate <NSObject>

- (void)signViewController:(PDEditSignViewController *)viewController didSavedSign:(NSString *)sign;

@end