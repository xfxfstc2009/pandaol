//
//  PDEditWeChatViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDEditWeChatViewController.h"

@interface PDEditWeChatViewController ()

@end

@implementation PDEditWeChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
}

- (void)basicSetting {
    self.barMainTitle = @"微信账号";
    self.placeholder = @"请输入您的微信账号";
    
    __weak typeof(self) weakSelf = self;
    [self saveWithInfoBlock:^(NSString *info) {
        if (info.length == 0) {
            [PDHUD showHUDSuccess:@"输入不能为空!"];
            return;
        }
        weakSelf.transferInfo = info;
        [weakSelf fetchDataWith:info];
        
    }];
}

- (void)fetchDataWith:(NSString *)info {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:editWeChat requestParams:@{@"weChat": info} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            [weakSelf saveDelegate];
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

- (void)saveDelegate {
    if ([self.delegate respondsToSelector:@selector(userInfoViewController:didSavedWithInfo:atIndexOfName:)]) {
        [self.delegate userInfoViewController:self didSavedWithInfo:self.transferInfo atIndexOfName:@"微信"];
    }
}

@end
