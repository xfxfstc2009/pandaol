//
//  PDEditUserInfoViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"


@protocol PDEditUserInfoViewControllerDelegate;
/// 编辑资料基类控制器，有单行单元格
@interface PDEditUserInfoViewController : AbstractViewController
@property (nonatomic, copy)   NSString *transferInfo;
@property (nonatomic, copy)   NSString *placeholder;    /**< 输入框的placeholder*/
@property (nonatomic, weak) id<PDEditUserInfoViewControllerDelegate> delegate;
@property (nonatomic, strong) NSIndexPath *indexPath;

- (void)saveWithInfoBlock:(void(^)(NSString * info))infoBlcok;
@end


@protocol PDEditUserInfoViewControllerDelegate <NSObject>

- (void)userInfoViewController:(PDEditUserInfoViewController *)viewController didSavedWithInfo:(NSString *)info atIndexOfName:(NSString *)name;

@end