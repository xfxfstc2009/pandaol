//
//  PDCenterChangeBindCodeSendCell.h
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SJCodeButton.h"

@interface PDCenterChangeBindCodeSendCell : UITableViewCell
/**
 * 图形验证码
 */
@property (nonatomic, strong) UITextField *gradeImageTextField;
/**
 * 数字验证码
 */
@property (nonatomic, strong) UITextField *codeTextField;
@property (nonatomic, strong) SJCodeButton *codeButton;
@property (nonatomic, strong) UIButton *gradeButton;

- (instancetype)initWithPhoneNumber:(NSString *)number reuseIdentifier:(NSString *)reuseIdentifier;
- (void)didClickCodeButtonAction:(void(^)(SJCodeButton *button))action;
- (void)didClickGradeImageCodeButtonAction:(void(^)(UIButton *button))action;

- (void)clearTimer;
@end
