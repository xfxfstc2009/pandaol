//
//  PDImageCodeTextFieldCell.m
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDImageCodeTextFieldCell.h"

@interface PDImageCodeTextFieldCell ()
@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UIImage *iconImage;
@property (nonatomic, copy) NSString *placehold;
@property (nonatomic, copy) void(^codeAction)(SJCodeButton *);
@end

@implementation PDImageCodeTextFieldCell

- (instancetype)initWithLeftImage:(UIImage *)image placehold:(NSString *)placehold reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        _iconImage = image;
        _placehold = placehold;
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.iconView = [[UIImageView alloc] init];
    self.iconView.image = self.iconImage;
    [self.contentView addSubview:self.iconView];
    
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@30);
        make.left.equalTo(@30);
        make.width.equalTo(@(self.iconView.image.size.width));
        make.height.equalTo(@(self.iconView.image.size.height));
    }];
    
    self.textField = [[UITextField alloc] init];
    self.textField.placeholder = self.placehold;
    self.textField.font = [UIFont systemFontOfCustomeSize:14];
    self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.textField.keyboardType = UIKeyboardTypeNumberPad;
    self.textField.borderStyle = UITextBorderStyleNone;
    [self.contentView addSubview:self.textField];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.iconView);
        make.left.equalTo(self.iconView.mas_right).offset(15);
        make.height.equalTo(@35);
        make.right.equalTo(@(-120));
    }];
    
    self.codeButton = [SJCodeButton buttonWithType:UIButtonTypeCustom];
    [self.codeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.codeButton.backgroundColor = [UIColor blackColor];
    self.codeButton.titleLabel.font = [UIFont systemFontOfCustomeSize:12];
    [self.codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    self.codeButton.layer.cornerRadius = 5;
    self.codeButton.layer.masksToBounds = YES;
    [self.codeButton addTarget:self action:@selector(actionForCode:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.codeButton];
    
    [self.codeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@35);
        make.centerY.equalTo(self.textField);
        make.width.equalTo(@80);
        make.right.equalTo(@(-30));
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = c6;
    [self.contentView addSubview:line];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textField.mas_bottom).offset(3);
        make.left.equalTo(@14);
        make.right.equalTo(@(-14));
        make.height.equalTo(@0.5);
        make.bottom.equalTo(@0);
    }];
}

- (void)actionForCode:(SJCodeButton *)button {
    if (self.codeAction) {
        self.codeAction(button);
    }
}


- (void)didClickCodeButtonAction:(void (^)(SJCodeButton *))action {
    if (action) {
        self.codeAction = action;
    }
}

@end
