//
//  PDImageTextFieldCell.m
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDImageTextFieldCell.h"

@interface PDImageTextFieldCell ()
@property (nonatomic, strong) UIImageView *iconView;
@property (nonatomic, strong) UIImage *iconImage;
@property (nonatomic, copy) NSString *placehold;
@end

@implementation PDImageTextFieldCell

- (instancetype)initWithLeftImage:(UIImage *)image placehold:(NSString *)placehold reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        _iconImage = image;
        _placehold = placehold;
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.iconView = [[UIImageView alloc] init];
    self.iconView.image = self.iconImage;
    [self.contentView addSubview:self.iconView];
    
    [self.iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@30);
        make.left.equalTo(@30);
        make.width.equalTo(@(self.iconView.image.size.width));
        make.height.equalTo(@(self.iconView.image.size.height));
    }];
    
    self.textField = [[UITextField alloc] init];
    self.textField.placeholder = self.placehold;
    self.textField.font = [UIFont systemFontOfCustomeSize:14];
    self.textField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.textField.keyboardType = UIKeyboardTypeNumberPad;
    self.textField.borderStyle = UITextBorderStyleNone;
    [self.contentView addSubview:self.textField];
    
    [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.iconView);
        make.left.equalTo(self.iconView.mas_right).offset(15);
        make.height.equalTo(@35);
        make.right.equalTo(@(-30));
    }];
    
    UIView *line = [UIView new];
    line.backgroundColor = c6;
    [self.contentView addSubview:line];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.textField.mas_bottom).offset(3);
        make.left.equalTo(@14);
        make.right.equalTo(@(-14));
        make.height.equalTo(@0.5);
        make.bottom.equalTo(@0);
    }];
}

@end
