//
//  SJCodeButton.m
//  SJBicycle
//
//  Created by 金峰 on 2017/6/16.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import "SJCodeButton.h"

@interface SJCodeButton ()
@property (nonatomic, strong, readwrite) NSTimer *timer;
@property (nonatomic) NSTimeInterval t_countDown;
@end

@implementation SJCodeButton

- (NSTimer *)timer {
    if (!_timer) {
        _timer = [NSTimer timerWithTimeInterval:1 target:self selector:@selector(countDownStart) userInfo:nil repeats:YES];
        [[NSRunLoop currentRunLoop] addTimer:_timer forMode:[NSRunLoop currentRunLoop].currentMode];
    }
    return _timer;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    return self;
}

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (!self) {
        return nil;
    }
    
    return self;
}

- (void)countDownStart {
    _t_countDown--;
    if (_t_countDown == 0) {
        [self clearTimer];
        [self setTitle:@"获取验证码" forState:UIControlStateNormal];
        self.enabled = YES;
    } else {
        [self setTitle:[NSString stringWithFormat:@"%@S",@(_t_countDown)] forState:UIControlStateNormal];
    }
}

- (void)beginTimer {
    [self beginTimerFrom:60];
}

- (void)beginTimerFrom:(NSTimeInterval)t {
    self.enabled = NO;
    self.t_countDown = t;
    [self.timer fire];
}

- (void)clearTimer {
    [self.timer invalidate];
    self.timer = nil;
}

@end
