//
//  PDCenterSettingViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterSettingViewController.h"
#import "PDLoginUpdatePasswordViewController.h"
#import <pop/POP.h>
#import "PDCenterSettingThirdBindingViewController.h"
#import "PDTestVC.h"
#import "PDresetpasswordinfoModel.h"
#import "PDCustomerCenterViewController.h"

@interface PDCenterSettingViewController ()<UITableViewDataSource,UITableViewDelegate>{
    UILabel *diskLabel;
}
@property (nonatomic,strong)UITableView *settingTableView;
@property (nonatomic,strong)NSArray *sectionOfArr;
@property (nonatomic,strong)NSArray *iconArr;


@end

@implementation PDCenterSettingViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"设置";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    
    
    
#if DEBUG
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        self.sectionOfArr = @[@[@"修改密码",@"账号绑定",@"清除缓存",@"检查更新",@"快为盼达在线评分吧"],@[@"退出登录"],@[@"测试工具"]];
        self.iconArr = @[@[@"icon_center_setting_changePwd",@"icon_center_setting_binding",@"icon_center_setting_clean",@"icon_center_setting_version",@"icon_center_setting_grade"],@[@"icon_center_setting_logout"],@[@"icon_center_setting_binding"]];
    } else {
        self.sectionOfArr = @[@[@"清除缓存",@"检查更新"],@[@"登录"],@[@"测试工具"]];
        self.iconArr = @[@[@"icon_center_setting_clean",@"icon_center_setting_version"],@[@"icon_center_setting_logout"],@[@"icon_center_setting_logout"]];
    }
#else
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        if ([AccountModel sharedAccountModel].isShenhe){
            self.sectionOfArr = @[@[@"修改密码",@"账号绑定",@"清除缓存"],@[@"退出登录"]];
            self.iconArr = @[@[@"icon_center_setting_changePwd",@"icon_center_setting_binding",@"icon_center_setting_clean"],@[@"icon_center_setting_logout"]];
        } else {
            self.sectionOfArr = @[@[@"修改密码",@"账号绑定",@"清除缓存",@"检查更新",@"快为盼达在线评分吧"],@[@"退出登录"]];
            self.iconArr = @[@[@"icon_center_setting_changePwd",@"icon_center_setting_binding",@"icon_center_setting_clean",@"icon_center_setting_version",@"icon_center_setting_grade"],@[@"icon_center_setting_logout"]];
        }
    } else {
        if ([AccountModel sharedAccountModel].isShenhe){
            self.sectionOfArr = @[@[@"清除缓存"],@[@"登录"]];
            self.iconArr = @[@[@"icon_center_setting_clean"],@[@"icon_center_setting_logout"]];
        } else {
            self.sectionOfArr = @[@[@"清除缓存",@"检查更新"],@[@"登录"]];
            self.iconArr = @[@[@"icon_center_setting_clean",@"icon_center_setting_version"],@[@"icon_center_setting_logout"]];
        }
    }
#endif
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.settingTableView){
        self.settingTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.settingTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.settingTableView.delegate = self;
        self.settingTableView.dataSource = self;
        self.settingTableView.backgroundColor = [UIColor clearColor];
        self.settingTableView.showsVerticalScrollIndicator = NO;
        self.settingTableView.scrollEnabled = YES;
        self.settingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.settingTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.sectionOfArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.sectionOfArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellStyleDefault     ;
        
        // 1. icon
        PDImageView *iconImgView = [[PDImageView alloc]init];
        iconImgView.backgroundColor = [UIColor clearColor];
        iconImgView.frame = CGRectMake(LCFloat(11), (cellHeight - LCFloat(15)) / 2., LCFloat(15), LCFloat(15));
        iconImgView.stringTag = @"iconImgView";
        [cellWithRowOne addSubview:iconImgView];
        
        // 2. fixedLabel
        UILabel *fixedLabel = [[UILabel alloc]init];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
        fixedLabel.stringTag = @"fixedLabel";
        [cellWithRowOne addSubview:fixedLabel];
        
        UILabel *dymicLabel = [[UILabel alloc]init];
        dymicLabel.backgroundColor = [UIColor clearColor];
        dymicLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
        dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        dymicLabel.stringTag = @"dymicLabel";
        [cellWithRowOne addSubview:dymicLabel];
        
        // 3. 创建arrow
        PDImageView *arrowImageView = [[PDImageView alloc]init];
        arrowImageView.backgroundColor = [UIColor clearColor];
        arrowImageView.image = [UIImage imageNamed:@"icon_tool_arrow"];
        arrowImageView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(9), (cellHeight - LCFloat(15)) / 2., LCFloat(9), LCFloat(15));
        arrowImageView.stringTag = @"arrowImageView";
        [cellWithRowOne addSubview:arrowImageView];
    }
    // 1.
    PDImageView *iconImgView = (PDImageView *)[cellWithRowOne viewWithStringTag:@"iconImgView"];
    UILabel *fixedLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"fixedLabel"];
    PDImageView *arrowImgView = (PDImageView *)[cellWithRowOne viewWithStringTag:@"arrowImageView"];
    UILabel *dymicLabel = (UILabel *)[cellWithRowOne viewWithStringTag:@"dymicLabel"];
    
    iconImgView.image = [UIImage imageNamed: [[self.iconArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row]];
    fixedLabel.text = [[self.sectionOfArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    CGSize fixedSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeight)];
    fixedLabel.frame = CGRectMake(CGRectGetMaxX(iconImgView.frame) + LCFloat(11), 0, fixedSize.width, cellHeight);
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"清除缓存"] && indexPath.row == [self cellIndexPathRowWithcellData:@"清除缓存"]){
        dymicLabel.text = [PDImageView diskCount];
        dymicLabel.hidden = NO;
        diskLabel = dymicLabel;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"当前版本"] && indexPath.row == [self cellIndexPathRowWithcellData:@"当前版本"]){
        dymicLabel.text = [NSString stringWithFormat:@"V%@",[Tool appVersion]];
        dymicLabel.hidden = NO;
    }
    else {
        dymicLabel.hidden = YES;
    }
    CGSize contentOfDymic = [dymicLabel.text sizeWithCalcFont:dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, cellHeight)];
    dymicLabel.frame = CGRectMake(arrowImgView.orgin_x - LCFloat(3) - contentOfDymic.width, 0, contentOfDymic.width, cellHeight);
    
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退出登录"] && indexPath.row == [self cellIndexPathRowWithcellData:@"退出登录"]){
        arrowImgView.hidden = YES;
    } else {
        arrowImgView.hidden = NO;
    }
    
    
    return cellWithRowOne;
    
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *sectionString = self.sectionOfArr[indexPath.section][indexPath.row];
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"修改密码"] && indexPath.row == [self cellIndexPathRowWithcellData:@"修改密码"]){
        [self authorizeWithCompletionHandler:^{
            if (![[AccountModel sharedAccountModel] hasMemberLoggedIn]){
                PDLoginUpdatePasswordViewController *updatePwdVC = [[PDLoginUpdatePasswordViewController alloc]init];
                [self.navigationController pushViewController:updatePwdVC animated:YES];
            } else {
                PDLoginUpdatePasswordViewController *updatePwdVC = [[PDLoginUpdatePasswordViewController alloc]init];
                [self.navigationController pushViewController:updatePwdVC animated:YES];
            }
        }];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"清除缓存"] && indexPath.row == [self cellIndexPathRowWithcellData:@"清除缓存"]){
        NSString *content = [NSString stringWithFormat:@"清除当前App缓存将为您节省%@空间",[PDImageView diskCount]];
        __weak typeof(self)weakSelf = self;
        [[PDAlertView sharedAlertView] showAlertWithTitle:@"清除图片缓存" conten:content isClose:YES btnArr:@[@"清除",@"不清除"] buttonClick:^(NSInteger buttonIndex) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
            if (buttonIndex == 0){
                [strongSelf clearDiskManager];
            }
        }];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"账号绑定"] &&indexPath.row == [self cellIndexPathRowWithcellData:@"账号绑定"]){
        PDCenterSettingThirdBindingViewController *bindingVC = [[PDCenterSettingThirdBindingViewController alloc]init];
        [self.navigationController pushViewController:bindingVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"当前版本"] && indexPath.row == [self cellIndexPathRowWithcellData:@"当前版本"]){
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"联系我们"] && indexPath.row == [self cellIndexPathRowWithcellData:@"联系我们"]){
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:aboutus];
        [self.navigationController pushViewController:webViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"退出登录"] && indexPath.row == [self cellIndexPathRowWithcellData:@"退出登录"]){
        __weak typeof(self)weakSelf = self;
        [[AccountModel sharedAccountModel] logoutBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"退出登录" conten:@"您已退出登录" isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                [[PDAlertView sharedAlertView] dismissWithBlock:^{
                    [strongSelf loginManager];
                }];
            }];
        }];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"登录"] && indexPath.row == [self cellIndexPathRowWithcellData:@"登录"]){
        __weak typeof(self)weakSelf = self;
        [self showLoginViewControllerWithBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf loginManager];
        }];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"测试工具"] && indexPath.row == [self cellIndexPathRowWithcellData:@"测试工具"]){
        PDTestVC *testVC = [[PDTestVC alloc]init];
        [self.navigationController pushViewController:testVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"联系我们"] && indexPath.row == [self cellIndexPathRowWithcellData:@"联系我们"]){
        PDCustomerCenterViewController *customerViewController = [[PDCustomerCenterViewController alloc]init];
        [customerViewController hidesTabBarWhenPushed];
        [self.navigationController pushViewController:customerViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"检查更新"] && indexPath.row == [self cellIndexPathRowWithcellData:@"检查更新"]){
        __weak typeof(self)weakSelf = self;
        [[AccountModel sharedAccountModel]sendRequestToGetNewAppVersionWithBlock:^(NSString *version) {
            if (!weakSelf){
                return ;
            }
            if (version.length){
                
                [[PDAlertView sharedAlertView] showAlertWithTitle:@"检测到新版本" conten:[NSString stringWithFormat:@"检测最新到最新版本%@,当前版本%@",version,[Tool appVersion]] isClose:YES btnArr:@[@"立即更新",@"稍后再i说"] buttonClick:^(NSInteger buttonIndex) {
                    if (buttonIndex == 0){
                        [Tool openURLWithURL:appUrl];
                    }
                }];
            } else {
                [PDHUD showHUDSuccess:[NSString stringWithFormat:@"当前是最新版本:%@",[Tool appVersion]]];
            }
        }];
    } else if ([sectionString isEqualToString:@"快为盼达在线评分吧"]) {
        NSString *nsStringToOpen = [NSString  stringWithFormat: @"itms-apps://itunes.apple.com/app/id%@?action=write-review",kAPP_STORE_ID];//替换为对应的APPID
        NSURL *url = [NSURL URLWithString:nsStringToOpen];
        if ([[UIApplication sharedApplication] canOpenURL:url]) {
            [[UIApplication sharedApplication] openURL:url];
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.settingTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [[self.sectionOfArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([[self.sectionOfArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return  LCFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

#pragma mark - OtherManager
-(NSInteger)cellIndexPathSectionWithcellData:(NSString *)string{
    NSInteger cellIndexPathOfSection = -1;
    for (int i = 0 ; i < self.sectionOfArr.count ; i++){
        NSArray *dataTempArr = [self.sectionOfArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellIndexPathOfSection = i;
                    break;
                }
            }
        }
    }
    return cellIndexPathOfSection;
}

-(NSInteger )cellIndexPathRowWithcellData:(NSString *)string{
    NSInteger cellRow = -1;
    for (int i = 0 ; i < self.sectionOfArr.count ; i++){
        NSArray *dataTempArr = [self.sectionOfArr objectAtIndex:i];
        for (int j = 0 ; j <dataTempArr.count; j++){
            id item = [dataTempArr objectAtIndex:j];
            if ([item isKindOfClass:[NSString class]]){
                NSString *itemString = (NSString *)item;
                if ([itemString isEqualToString:string]){
                    cellRow = j;
                    break;
                }
            }
        }
    }
    return cellRow;
}


#pragma mark - Other Manager
-(void)clearDiskManager{
    __weak typeof(self)weakSelf = self;
    [PDImageView cleanImgCacheWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->diskLabel.text = [PDImageView diskCount];
    }];
}



-(void)loginManager{
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        self.sectionOfArr = @[@[@"修改密码",@"账号绑定",@"清除缓存",@"检查更新"],@[@"退出登录"]];
        self.iconArr = @[@[@"icon_center_setting_changePwd",@"icon_center_setting_binding",@"icon_center_setting_clean",@"icon_center_setting_version",@"icon_center_setting_link"],@[@"icon_center_setting_logout"]];
    } else {
        self.sectionOfArr = @[@[@"清除缓存",@"检查更新"],@[@"登录"]];
        self.iconArr = @[@[@"icon_center_setting_clean",@"icon_center_setting_version",@"icon_center_setting_link"],@[@"icon_center_setting_logout"]];
    }
    
    [self.settingTableView reloadData];
}


@end
