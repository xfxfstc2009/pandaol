//
//  PDChangeBindStateCell.m
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChangeBindStateCell.h"

@interface PDChangeBindStateCell ()
@property (nonatomic, strong) UIImageView *codeImageView;
@property (nonatomic, strong) UIImageView *mobileImageView;
@property (nonatomic, strong) UIImageView *finishImageView;
@property (nonatomic) PDChangeBindState state;
@end

@implementation PDChangeBindStateCell

- (instancetype)initWithState:(PDChangeBindState)state reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    
    self.state = state;
    [self setupCell];
    return self;
}

- (void)setupCell {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    UIView *line = [UIView new];
    line.backgroundColor = c6;
    [self.contentView addSubview:line];
    
    self.codeImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.codeImageView];
    
    self.mobileImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.mobileImageView];
    
    self.finishImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.finishImageView];
    
    if (self.state == PDChangeBindStateCode) {
        self.codeImageView.image = [UIImage imageNamed:@"icon_center_changebind_ok"];
        self.mobileImageView.image = [UIImage imageNamed:@"icon_center_changebind_none"];
        self.finishImageView.image = [UIImage imageNamed:@"icon_center_changebind_none"];
    } else if (self.state == PDChangeBindStateMobile) {
        self.codeImageView.image = [UIImage imageNamed:@"icon_center_changebind_ok"];
        self.mobileImageView.image = [UIImage imageNamed:@"icon_center_changebind_ok"];
        self.finishImageView.image = [UIImage imageNamed:@"icon_center_changebind_none"];
    } else {
        self.codeImageView.image = [UIImage imageNamed:@"icon_center_changebind_ok"];
        self.mobileImageView.image = [UIImage imageNamed:@"icon_center_changebind_ok"];
        self.finishImageView.image = [UIImage imageNamed:@"icon_center_changebind_ok"];
    }
    
    [self.codeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@15);
        make.left.equalTo(@85);
    }];
    
    [self.mobileImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.codeImageView);
        make.centerX.equalTo(self.contentView);
    }];
    
    [self.finishImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.codeImageView);
        make.right.equalTo(@(-85));
    }];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.codeImageView.mas_centerX);
        make.height.equalTo(@1);
        make.centerY.equalTo(self.codeImageView);
        make.right.equalTo(self.finishImageView.mas_centerX);
    }];
    
    UILabel *codeLabel = [[UILabel alloc] init];
    codeLabel.text = @"输入验证码";
    codeLabel.font = [UIFont systemFontOfCustomeSize:12];
    codeLabel.textColor = c28;
    [self.contentView addSubview:codeLabel];
    
    UILabel *mobileLabel = [[UILabel alloc] init];
    mobileLabel.text = @"更换手机号";
    mobileLabel.font = codeLabel.font;
    mobileLabel.textColor = codeLabel.textColor;
    [self.contentView addSubview:mobileLabel];
    
    UILabel *finishLabel = [[UILabel alloc] init];
    finishLabel.text = @"换绑完成";
    finishLabel.font = codeLabel.font;
    finishLabel.textColor = codeLabel.textColor;
    [self.contentView addSubview:finishLabel];
    
    [codeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.codeImageView.mas_bottom).offset(10);
        make.centerX.equalTo(self.codeImageView);
        make.bottom.equalTo(@(-15));
    }];
    
    [mobileLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(codeLabel);
        make.centerX.equalTo(self.mobileImageView);
    }];
    
    [finishLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(codeLabel);
        make.centerX.equalTo(self.finishImageView);
    }];
}


@end
