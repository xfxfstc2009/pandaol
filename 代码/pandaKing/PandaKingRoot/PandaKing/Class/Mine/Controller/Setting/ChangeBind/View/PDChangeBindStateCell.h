//
//  PDChangeBindStateCell.h
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PDChangeBindState) {
    PDChangeBindStateCode,
    PDChangeBindStateMobile,
    PDChangeBindStateFinish
};

@interface PDChangeBindStateCell : UITableViewCell
- (instancetype)initWithState:(PDChangeBindState)state reuseIdentifier:(NSString *)reuseIdentifier;


@end
