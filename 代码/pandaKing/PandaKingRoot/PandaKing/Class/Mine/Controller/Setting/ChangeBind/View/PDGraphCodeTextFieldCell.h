//
//  PDGraphCodeTextFieldCell.h
//  PandaKing
//
//  Created by Cranz on 17/11/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 图形验证码
@interface PDGraphCodeTextFieldCell : UITableViewCell
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) UIButton *codeButton;

- (instancetype)initWithLeftImage:(UIImage *)image
                        placehold:(NSString *)placehold
                  reuseIdentifier:(NSString *)reuseIdentifier;
- (void)didClickCodeButtonAction:(void(^)(UIButton *button))action;

@end
