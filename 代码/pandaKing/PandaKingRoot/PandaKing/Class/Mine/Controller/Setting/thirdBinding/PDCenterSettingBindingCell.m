//
//  PDCenterSettingBindingCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterSettingBindingCell.h"

@interface PDCenterSettingBindingCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *typeLabel;
@property (nonatomic,strong)UILabel *accountLabel;
@property (nonatomic,strong)UILabel *statusLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;

@end


@implementation PDCenterSettingBindingCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建icon
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    // 2. 创建名字
    self.typeLabel = [[UILabel alloc]init];
    self.typeLabel.backgroundColor = [UIColor clearColor];
    self.typeLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"] boldFont];
    [self addSubview:self.typeLabel];
    
    // 3. 创建箭头符号
    self.arrowImgView = [[PDImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    self.arrowImgView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    [self addSubview:self.arrowImgView];
    
    // 4. 创建绑定状态
    self.statusLabel = [[UILabel alloc]init];
    self.statusLabel.backgroundColor = [UIColor clearColor];
    self.statusLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.statusLabel];
    
    //5. 创建名字
    self.accountLabel = [[UILabel alloc]init];
    self.accountLabel.backgroundColor = [UIColor clearColor];
    self.accountLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.accountLabel];
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferType:(PDCenterSettingBindingCellType)transferType{
    _transferType = transferType;
}

-(void)setTransferSingleModel:(PDCenterSettingBindingSingleModel *)transferSingleModel{
    if (self.transferType == PDCenterSettingBindingCellQQ){
        self.iconImgView.image = [UIImage imageNamed:@"icon_accountbind_qq"];
        self.typeLabel.text = @"QQ";
        if (transferSingleModel.isBindQq){              // 已经绑定
            self.statusLabel.text = @"已绑定";
            self.statusLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            self.arrowImgView.hidden = YES;
        } else {
            self.statusLabel.text = @"立即绑定";
            self.statusLabel.textColor = c15;
            self.arrowImgView.hidden = NO;
        }
    } else if (self.transferType == PDCenterSettingBindingCellWechat){
        self.iconImgView.image = [UIImage imageNamed:@"icon_accountbind_wx"];
        self.typeLabel.text = @"微信";
        if (transferSingleModel.isBindWechat){              // 已经绑定
            self.statusLabel.text = @"已绑定";
            self.statusLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            self.arrowImgView.hidden = YES;
        } else {
            self.statusLabel.text = @"立即绑定";
            self.statusLabel.textColor = c15;
            self.arrowImgView.hidden = NO;
        }
    }
    
    
    // 1. icon
    self.iconImgView.frame = CGRectMake(LCFloat(11), (self.transferCellHeight - LCFloat(24)) / 2., LCFloat(24), LCFloat(24));
    // 2. name
    CGSize typeSize = [self.typeLabel.text sizeWithCalcFont:self.typeLabel.font  constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellHeight)];
    self.typeLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + LCFloat(11), 0, typeSize.width, self.transferCellHeight);
    
    // 3. 箭头
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(9), (self.transferCellHeight - LCFloat(15)) / 2., LCFloat(9), LCFloat(15));
    
    // 4. 名字
    CGSize statusSize = [self.statusLabel.text sizeWithCalcFont:self.statusLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellHeight)];
    self.statusLabel.frame = CGRectMake(self.arrowImgView.orgin_x - LCFloat(11) - statusSize.width, 0, statusSize.width, self.transferCellHeight);
}
@end

