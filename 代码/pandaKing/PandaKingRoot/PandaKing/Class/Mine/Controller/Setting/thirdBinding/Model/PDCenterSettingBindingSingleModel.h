//
//  PDCenterSettingBindingSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDCenterSettingBindingSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)BOOL isBindQq;                  /**< 是否绑定QQ*/
@property (nonatomic,assign)BOOL isBindWechat;              /**< 是否绑定微信*/
@property (nonatomic,assign)BOOL isBindPhone;               /**< 是否绑定手机*/
@property (nonatomic,copy)NSString *phone;               /**< 是否绑定手机*/

@end
