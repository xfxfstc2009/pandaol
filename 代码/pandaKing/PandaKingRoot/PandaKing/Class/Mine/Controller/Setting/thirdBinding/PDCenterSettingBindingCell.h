//
//  PDCenterSettingBindingCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterSettingBindingSingleModel.h"

typedef NS_ENUM(NSInteger,PDCenterSettingBindingCellType) {
    PDCenterSettingBindingCellQQ,
    PDCenterSettingBindingCellWechat,
};

@interface PDCenterSettingBindingCell : UITableViewCell

@property (nonatomic,assign)PDCenterSettingBindingCellType transferType;
@property (nonatomic,strong)PDCenterSettingBindingSingleModel *transferSingleModel;
@property (nonatomic,assign)CGFloat transferCellHeight;

@end
