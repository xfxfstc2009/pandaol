//
//  PDChangeBindViewController.h
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 换绑手机号
@interface PDChangeBindViewController : AbstractViewController
@property (nonatomic, copy) NSString *phoneNumber;
@end
