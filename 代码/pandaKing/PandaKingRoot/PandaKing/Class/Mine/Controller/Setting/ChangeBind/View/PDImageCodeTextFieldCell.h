//
//  PDImageCodeTextFieldCell.h
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SJCodeButton.h"

@interface PDImageCodeTextFieldCell : UITableViewCell
@property (nonatomic, strong) UITextField *textField;
@property (nonatomic, strong) SJCodeButton *codeButton;

- (instancetype)initWithLeftImage:(UIImage *)image
                        placehold:(NSString *)placehold
                  reuseIdentifier:(NSString *)reuseIdentifier;
- (void)didClickCodeButtonAction:(void(^)(SJCodeButton *button))action;
@end
