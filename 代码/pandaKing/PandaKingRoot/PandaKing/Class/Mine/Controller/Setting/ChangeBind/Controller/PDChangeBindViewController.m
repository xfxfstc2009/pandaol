//
//  PDChangeBindViewController.m
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChangeBindViewController.h"
#import "PDChangeBindStateCell.h"
#import "PDCenterChangeBindCodeSendCell.h"
#import "PDChangeBindNewMobileViewController.h"
#import <UIButton+WebCache.h>

@interface PDChangeBindViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *verButton;
@property (nonatomic, strong) PDCenterChangeBindCodeSendCell *codeCell;
@end

@implementation PDChangeBindViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    [self.codeCell clearTimer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barMainTitle = @"换绑手机";
    [self setupTableView];
    [self addNotification];
}

- (void)setupTableView {
    self.tableView = [PDCenterTool tableViewWithSuperView:self.view style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 50;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.tableView.tableFooterView = [self setupFooterView];
}

- (void)addNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionForChangeTextField:) name:UITextFieldTextDidChangeNotification object:nil];
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        PDChangeBindStateCell *stateCell = [tableView dequeueReusableCellWithIdentifier:@"statecellid"];
        if (!stateCell) {
            stateCell = [[PDChangeBindStateCell alloc] initWithState:PDChangeBindStateCode reuseIdentifier:@"statecellid"];
        }
        return stateCell;
    } else {
        PDCenterChangeBindCodeSendCell *codeCell = [tableView dequeueReusableCellWithIdentifier:@"codeCellId"];
        if (!codeCell) {
            codeCell = [[PDCenterChangeBindCodeSendCell alloc] initWithPhoneNumber:self.phoneNumber reuseIdentifier:@"codeCellId"];
            self.codeCell = codeCell;
            [self requestImageCodeWithButton:codeCell.gradeButton];
        }
        __weak typeof(self) weakSelf = self;
        [codeCell didClickCodeButtonAction:^(SJCodeButton *button) {
            [weakSelf requestCodeWithNumber:weakSelf.phoneNumber benginCodeButton:button];
        }];
        
        // 获取图形验证码
        [codeCell didClickGradeImageCodeButtonAction:^(UIButton *button) {
            [weakSelf requestImageCodeWithButton:button];
        }];
        return codeCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}


- (UIView *)setupFooterView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 80)];
    footerView.backgroundColor = [UIColor whiteColor];
    self.verButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [PDCenterTool button:self.verButton enable:NO];
    self.verButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    [self.verButton setTitle:@"验证后绑定新手机号" forState:UIControlStateNormal];
    [footerView addSubview:self.verButton];
    [self.verButton addTarget:self action:@selector(actionForBind:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.verButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(footerView);
        make.left.equalTo(@14);
        make.right.equalTo(@(-14));
        make.height.equalTo(@48);
    }];
    
    return footerView;
}

#pragma mark - 验证绑定手机号

- (void)actionForBind:(UIButton *)button {
    [self requestVerCode];
}

#pragma mark - 输入验证码通知

- (void)actionForChangeTextField:(NSNotification *)noti {
    UITextField *gradeTextField = self.codeCell.gradeImageTextField;
    UITextField *codeTextField = self.codeCell.codeTextField;
    
    if (gradeTextField.text.length > 4) {
        gradeTextField.text = [gradeTextField.text substringToIndex:4];
        return;
    }
    if (codeTextField.text.length > 4) {
        codeTextField.text = [codeTextField.text substringToIndex:4];
        return;
    }
    
    if (gradeTextField.text.length && codeTextField.text.length) {
        [PDCenterTool button:self.verButton enable:YES];
    } else {
        [PDCenterTool button:self.verButton enable:NO];
    }
}

#pragma mark - 网络请求

- (void)requestCodeWithNumber:(NSString *)number benginCodeButton:(SJCodeButton *)button {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:resetMsm requestParams:@{@"account":number, @"verifyCode":self.codeCell.gradeImageTextField.text} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (isSucceeded) {
            [PDHUD showHUDSuccess:@"验证码发送成功，请注意查收"];
            [button beginTimer];
        } else {
            NSUInteger code = [responseObject[@"code"] integerValue];
            if (code == 407 || code == 406) {
                [weakSelf requestImageCodeWithButton:weakSelf.codeCell.gradeButton];
            }
        }
    }];
}

/**
 * 获取图形验证码
 */
- (void)requestImageCodeWithButton:(UIButton *)button {
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberverifycode requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (isSucceeded) {
            NSString *img = responseObject[@"img"];
            [button sd_setBackgroundImageWithURL:[NSURL URLWithString:[PDCenterTool absoluteUrlWithRisqueUrl:img]] forState:UIControlStateNormal];
        }
    }];
}

/**
 * 验证手机号和输入的验证码
 */
- (void)requestVerCode {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:resetValidate requestParams:@{@"account":self.phoneNumber, @"code":self.codeCell.codeTextField.text} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (isSucceeded) {
            PDChangeBindNewMobileViewController *changeBind = [[PDChangeBindNewMobileViewController alloc] init];
            changeBind.oldPhoneNumber = weakSelf.phoneNumber;
            changeBind.oldCode = weakSelf.codeCell.codeTextField.text;
            [weakSelf.navigationController pushViewController:changeBind animated:YES];
        }
    }];
}

@end
