//
//  PDChangeBindNewMobileViewController.m
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChangeBindNewMobileViewController.h"
#import "PDChangeBindStateCell.h"
#import "PDImageTextFieldCell.h"
#import "PDImageCodeTextFieldCell.h"
#import "PDGraphCodeTextFieldCell.h"
#import <UIButton+WebCache.h>

@interface PDChangeBindNewMobileViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIButton *verButton;
@property (nonatomic, strong) UITextField *mobileTextField;
@property (nonatomic, strong) UITextField *codeTextField;
@property (nonatomic, strong) UITextField *gradeTextField;
@property (nonatomic, strong) SJCodeButton *codeButton;
@property (nonatomic, strong) UIButton *gradeButton;
@end

@implementation PDChangeBindNewMobileViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UITextFieldTextDidChangeNotification object:nil];
    [self.codeButton clearTimer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barMainTitle = @"换绑手机";
    [self setupTableView];
    [self addNotification];
}

- (void)setupTableView {
    self.tableView = [PDCenterTool tableViewWithSuperView:self.view style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.estimatedRowHeight = 50;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.tableFooterView = [self setupFooterView];
}

- (void)addNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(actionForChangeTextField:) name:UITextFieldTextDidChangeNotification object:nil];
}

#pragma mark - UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        PDChangeBindStateCell *stateCell = [tableView dequeueReusableCellWithIdentifier:@"statecellid"];
        if (!stateCell) {
            stateCell = [[PDChangeBindStateCell alloc] initWithState:PDChangeBindStateMobile reuseIdentifier:@"statecellid"];
        }
        return stateCell;
    } else {
        __weak typeof(self) weakSelf = self;
        if (indexPath.row == 0) {
            PDImageTextFieldCell *mobileCell = [tableView dequeueReusableCellWithIdentifier:@"codeCelId"];
            if (!mobileCell) {
                mobileCell = [[PDImageTextFieldCell alloc] initWithLeftImage:[UIImage imageNamed:@"icon_login_bindingInutr_phone"] placehold:@"请输入新手机号" reuseIdentifier:@"codeCelId"];
                self.mobileTextField = mobileCell.textField;
            }
            return mobileCell;
        } else if (indexPath.row == 1) {
            PDGraphCodeTextFieldCell *gradeCell = [tableView dequeueReusableCellWithIdentifier:@"gradeCellId"];
            if (!gradeCell) {
                gradeCell = [[PDGraphCodeTextFieldCell alloc] initWithLeftImage:[UIImage imageNamed:@"icon_login_mineCode"] placehold:@"请输入图形验证码" reuseIdentifier:@"gradeCellId"];
                self.gradeTextField = gradeCell.textField;
                self.gradeButton = gradeCell.codeButton;
                [self requestImageCodeWithButton:gradeCell.codeButton];
            }
            [gradeCell didClickCodeButtonAction:^(UIButton *button) {
                [weakSelf requestImageCodeWithButton:button];
            }];
            return gradeCell;
        } else {
            PDImageCodeTextFieldCell *cell = [tableView dequeueReusableCellWithIdentifier:@"imageCellId"];
            if (!cell) {
                cell = [[PDImageCodeTextFieldCell alloc] initWithLeftImage:[UIImage imageNamed:@"icon_changebind_code"] placehold:@"请输入验证码" reuseIdentifier:@"imageCellId"];
                self.codeTextField = cell.textField;
                self.codeButton = cell.codeButton;
                [PDCenterTool button:self.codeButton enable:NO];
            }
            [cell didClickCodeButtonAction:^(SJCodeButton *button) {
                [weakSelf requestCodeWithNumber:weakSelf.mobileTextField.text benginCodeButton:button];
            }];
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (UIView *)setupFooterView {
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 80)];
    footerView.backgroundColor = [UIColor whiteColor];
    self.verButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [PDCenterTool button:self.verButton enable:NO];
    self.verButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    [self.verButton setTitle:@"确认绑定" forState:UIControlStateNormal];
    [footerView addSubview:self.verButton];
    [self.verButton addTarget:self action:@selector(actionForBind:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.verButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(footerView);
        make.left.equalTo(@14);
        make.right.equalTo(@(-14));
        make.height.equalTo(@48);
    }];
    
    return footerView;
}

#pragma mark - 监听textField

- (void)actionForChangeTextField:(NSNotification *)noti {
    if (self.gradeTextField.text.length > 4) {
        self.gradeTextField.text = [self.gradeTextField.text substringToIndex:4];
        return;
    }
    if (self.codeTextField.text.length > 4) {
        self.codeTextField.text = [self.codeTextField.text substringToIndex:4];
        return;
    }
    
    if (self.mobileTextField.text.length) {
        [PDCenterTool button:self.codeButton enable:YES];
    } else {
        [PDCenterTool button:self.codeButton enable:NO];
    }
    
    if (self.mobileTextField.text.length && self.codeTextField.text.length && self.gradeTextField.text.length) {
        [PDCenterTool button:self.verButton enable:YES];
    } else {
        [PDCenterTool button:self.verButton enable:NO];
    }
}

#pragma mark - 确认绑定

- (void)actionForBind:(UIButton *)button {
    [self requestChangePhone];
}

/**
 * 跳往前面
 */
- (void)goBack {
    NSMutableArray *viewControllers = [self.navigationController.viewControllers mutableCopy];
    [viewControllers removeObjectsInRange:NSMakeRange(viewControllers.count - 2, 2)];
    [self.navigationController setViewControllers:viewControllers animated:YES];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"kChangeBindNotification" object:nil];
    
    UIAlertController *alter =[PDCenterTool showAlertWithTitle:nil message:@"您的手机账号换绑成功" buttonTitles:@[@"确认"] actions:nil];
    [self presentViewController:alter animated:YES completion:nil];
}

#pragma mark - 网络请求


/**
 * 获取图形验证码
 */
- (void)requestImageCodeWithButton:(UIButton *)button {
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberverifycode requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (isSucceeded) {
            NSString *img = responseObject[@"img"];
            [button sd_setBackgroundImageWithURL:[NSURL URLWithString:[PDCenterTool absoluteUrlWithRisqueUrl:img]] forState:UIControlStateNormal];
        }
    }];
}

/**
 * 发送验证码
 */
- (void)requestCodeWithNumber:(NSString *)number benginCodeButton:(SJCodeButton *)button {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:userRegSend requestParams:@{@"account":number, @"verifyCode":self.gradeTextField.text} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (isSucceeded) {
            [PDHUD showHUDSuccess:@"验证码发送成功，请注意查收"];
            [button beginTimer];
        } else {
            NSUInteger code = [responseObject[@"code"] integerValue];
            if (code == 407 || code == 406) {
                [weakSelf requestImageCodeWithButton:weakSelf.gradeButton];
            }
        }
    }];
}

- (void)requestChangePhone {
    __weak typeof(self)  weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:changeBindPhoneNumber requestParams:@{@"oldphone":self.oldPhoneNumber, @"oldphonecode":self.oldCode, @"newphone":self.mobileTextField.text, @"newphonecode":self.codeTextField.text} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (isSucceeded) {
            [weakSelf goBack];
        }
    }];
}

@end
