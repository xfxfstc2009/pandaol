//
//  PDImageTextFieldCell.h
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDImageTextFieldCell : UITableViewCell
@property (nonatomic, strong) UITextField *textField;

- (instancetype)initWithLeftImage:(UIImage *)image
                        placehold:(NSString *)placehold
                  reuseIdentifier:(NSString *)reuseIdentifier;

@end
