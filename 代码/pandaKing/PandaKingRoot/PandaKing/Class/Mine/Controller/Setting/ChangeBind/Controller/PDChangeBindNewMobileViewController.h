//
//  PDChangeBindNewMobileViewController.h
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDChangeBindNewMobileViewController : AbstractViewController
@property (nonatomic, copy) NSString *oldPhoneNumber;
@property (nonatomic, copy) NSString *oldCode;
@end
