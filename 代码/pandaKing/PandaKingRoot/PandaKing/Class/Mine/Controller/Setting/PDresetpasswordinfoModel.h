//
//  PDresetpasswordinfoModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDresetpasswordinfoModel : FetchModel

@property (nonatomic,copy)NSString *accountName;
@property (nonatomic,assign)BOOL isEnable;

@end
