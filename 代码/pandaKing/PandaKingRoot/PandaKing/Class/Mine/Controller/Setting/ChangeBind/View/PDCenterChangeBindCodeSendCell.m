///Users/panda/Desktop/PandaKingRoot/PandaKing.xcodeproj
//  PDCenterChangeBindCodeSendCell.m
//  PandaKing
//
//  Created by Cranz on 17/11/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDCenterChangeBindCodeSendCell.h"

@interface PDCenterChangeBindCodeSendCell ()
@property (nonatomic, strong) UILabel *stateLabel;
@property (nonatomic, strong) UILabel *numberLabel;
@property (nonatomic, copy) NSString *phoneNumber;
@property (nonatomic, copy) void(^codeAction)(SJCodeButton *);
@property (nonatomic, copy) void(^gradeAction)(UIButton *);
@end

@implementation PDCenterChangeBindCodeSendCell

- (void)clearTimer {
    [self.codeButton clearTimer];
}

- (instancetype)initWithPhoneNumber:(NSString *)number reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        _phoneNumber = number;
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.stateLabel = [[UILabel alloc] init];
    self.stateLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.stateLabel.text = @"已发送验证码短信到";
    self.stateLabel.textColor = c28;
    [self.contentView addSubview:self.stateLabel];
    
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.font = [[UIFont systemFontOfCustomeSize:24] boldFont];
    self.numberLabel.textColor = [UIColor blackColor];
    self.numberLabel.text = [PDCenterTool formatPhoneNumber:self.phoneNumber];
    [self.contentView addSubview:self.numberLabel];
    
    [self.stateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(@20);
        make.centerX.equalTo(self.contentView);
    }];
    
    [self.numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.stateLabel.mas_bottom).offset(10);
        make.centerX.equalTo(self.stateLabel);
    }];
    
    
    // 图形验证码部分
    UIImageView *iconView = [[UIImageView alloc] init];
    iconView.image = [UIImage imageNamed:@"icon_login_mineCode"];
    [self.contentView addSubview:iconView];
    
    [iconView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.numberLabel.mas_bottom).offset(40);
        make.left.equalTo(@30);
        make.width.equalTo(@(iconView.image.size.width));
        make.height.equalTo(@(iconView.image.size.height));
    }];
    
    self.gradeImageTextField = [[UITextField alloc] init];
    self.gradeImageTextField.placeholder = @"请输入图形验证码";
    self.gradeImageTextField.font = [UIFont systemFontOfCustomeSize:14];
    self.gradeImageTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.gradeImageTextField.keyboardType = UIKeyboardTypeDefault;
    self.gradeImageTextField.borderStyle = UITextBorderStyleNone;
    [self.contentView addSubview:self.gradeImageTextField];
    
    [self.gradeImageTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(iconView);
        make.left.equalTo(iconView.mas_right).offset(15);
        make.height.equalTo(@35);
        make.right.equalTo(@(-120));
    }];
    
    self.gradeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.gradeButton.backgroundColor = [UIColor lightGrayColor];
    [self.gradeButton addTarget:self action:@selector(actionForGradeCode:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.gradeButton];
    
    [self.gradeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@35);
        make.centerY.equalTo(self.gradeImageTextField);
        make.width.equalTo(@80);
        make.right.equalTo(@(-30));
    }];
    
    UIView *line0 = [UIView new];
    line0.backgroundColor = c6;
    [self.contentView addSubview:line0];
    
    [line0 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.gradeImageTextField.mas_bottom).offset(3);
        make.left.equalTo(@14);
        make.right.equalTo(@(-14));
        make.height.equalTo(@0.5);
    }];
    
    
    // 数字验证码部分
    UIImageView *leftView = [[UIImageView alloc] init];
    leftView.image = [UIImage imageNamed:@"icon_changebind_code"];
    [self.contentView addSubview:leftView];
    [leftView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(line0.mas_bottom).offset(30);
        make.left.equalTo(@30);
        make.width.equalTo(@(leftView.image.size.width));
        make.height.equalTo(@(leftView.image.size.height));
    }];

    self.codeTextField = [[UITextField alloc] init];
    self.codeTextField.placeholder = @"请输入验证码";
    self.codeTextField.font = [UIFont systemFontOfCustomeSize:14];
    self.codeTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.codeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.codeTextField.borderStyle = UITextBorderStyleNone;
    [self.contentView addSubview:self.codeTextField];
    
    [self.codeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(leftView);
        make.left.equalTo(leftView.mas_right).offset(15);
        make.right.equalTo(@(-120));
        make.height.equalTo(@35);
    }];
    
    self.codeButton = [SJCodeButton buttonWithType:UIButtonTypeCustom];
    [self.codeButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.codeButton.backgroundColor = [UIColor blackColor];
    self.codeButton.titleLabel.font = [UIFont systemFontOfCustomeSize:12];
    [self.codeButton setTitle:@"获取验证码" forState:UIControlStateNormal];
    self.codeButton.layer.cornerRadius = 5;
    self.codeButton.layer.masksToBounds = YES;
    [self.codeButton addTarget:self action:@selector(actionForCode:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.codeButton];
    
    [self.codeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@35);
        make.centerY.equalTo(self.codeTextField);
        make.width.equalTo(@80);
        make.right.equalTo(@(-30));
    }];

    
    UIView *line = [UIView new];
    line.backgroundColor = c6;
    [self.contentView addSubview:line];
    
    [line mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.codeTextField.mas_bottom).offset(3);
        make.left.equalTo(@14);
        make.right.equalTo(@(-14));
        make.height.equalTo(@0.5);
        make.bottom.equalTo(@(-40));
    }];
}

- (void)actionForCode:(SJCodeButton *)button {
    if (self.codeAction) {
        self.codeAction(button);
    }
}

- (void)actionForGradeCode:(UIButton *)button {
    if (self.gradeAction) {
        self.gradeAction(button);
    }
}

- (void)didClickCodeButtonAction:(void (^)(SJCodeButton *))action {
    if (action) {
        self.codeAction = action;
    }
}

- (void)didClickGradeImageCodeButtonAction:(void (^)(UIButton *))action {
    if (action) {
        self.gradeAction = action;
    }
}

@end
