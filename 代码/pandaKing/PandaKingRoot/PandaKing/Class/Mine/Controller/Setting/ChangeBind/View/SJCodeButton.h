//
//  SJCodeButton.h
//  SJBicycle
//
//  Created by 金峰 on 2017/6/16.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 验证码按钮
@interface SJCodeButton : UIButton
@property (nonatomic, strong, readonly) NSTimer *timer;
/**
 * 倒计时60秒
 */
- (void)beginTimer;
/**
 * 倒计时t秒
 */
- (void)beginTimerFrom:(NSTimeInterval)t;
/**
 * 清除定时器
 */
- (void)clearTimer;
@end
