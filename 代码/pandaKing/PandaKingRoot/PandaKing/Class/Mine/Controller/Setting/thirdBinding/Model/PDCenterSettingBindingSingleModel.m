//
//  PDCenterSettingBindingSingleModel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterSettingBindingSingleModel.h"

@implementation PDCenterSettingBindingSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end
