//
//  PDBackpackExchangeRuleViewController.m
//  PandaKing
//
//  Created by Cranz on 16/10/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBackpackExchangeRuleViewController.h"

@interface PDBackpackExchangeRuleViewController ()

@end

@implementation PDBackpackExchangeRuleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
}

- (void)pageSetting {
    self.barMainTitle = @"兑换规则";
    
    __weak typeof(self) weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_page_cancle"] barHltImage:[UIImage imageNamed:@"icon_page_cancle"] action:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}


@end
