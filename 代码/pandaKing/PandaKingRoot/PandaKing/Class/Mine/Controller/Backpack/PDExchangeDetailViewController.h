//
//  PDExchangeDetailViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDExchangeDetailViewController : AbstractViewController
@property (nonatomic, copy) NSString *orderId;
@end
