//
//  PDBackpackHadViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBackpackHadViewController.h"
#import "PDBackpackTableViewCell.h"
#import "PDBackpackHadModel.h"
#import "PDReceiveViewController.h"
#import "PDReceiveMobileViewController.h"
#import "PDReceiveRealViewController.h"
#import "PDReceiveInventedViewController.h"
#import "PDPresentListViewController.h"

@interface PDBackpackHadViewController ()<UITableViewDataSource, UITableViewDelegate, PDBackpackTableViewCellDelegate, PDPresentListViewControllerDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) PDBackpackHadModel *hadModel;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, strong) NSIndexPath *selIndexPath;
@end

@implementation PDBackpackHadViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    self.view.backgroundColor = [UIColor clearColor];
    _page = 1;
    _itemsArr = [NSMutableArray array];
    [PDHUD showHUDProgress:@"正在为您获取背包数据，请稍后..." diary:0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];    
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 64)];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mainTableView.rowHeight = [PDBackpackTableViewCell cellHeight];
    self.mainTableView.tableFooterView = [UIView new];
    [self.view addSubview:self.mainTableView];
    
    // 添加刷新
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.itemsArr.count) {
            [weakSelf.itemsArr removeAllObjects];
        }
        [weakSelf fetchData];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.hadModel.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchData];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            return ;
        }
    }];
}

- (void)submitSuccessAlert {
    self.page = 1;
    if (self.itemsArr.count) {
        [self.itemsArr removeAllObjects];
    }
    [self fetchData];
    
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"使用提示" conten:@"您的兑换申请已经提交，请耐心等待处理" isClose:NO btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissWithCompletion:NULL];
    }];
}

#pragma mark - PDPresentListViewControllerDelegate

- (void)presentViewControllerDidPresentCard {
    self.page = 1;
    if (self.itemsArr.count) {
        [self.itemsArr removeAllObjects];
    }
    [self fetchData];
}

#pragma mark - UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"hadCellId";
    PDBackpackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[PDBackpackTableViewCell alloc] initWithType:PDBackpackTableViewCellTypeHad reuseIdentifier:cellId];
        cell.delegate = self;
    }
    cell.indexPath = indexPath;
    if (self.itemsArr.count) {
        PDBackpackItemModel *itemModel = [self.itemsArr objectAtIndex:indexPath.row];
        cell.itemModel = itemModel;
    }
    return cell;
}

#pragma mark - PDBackpackTableViewCellDelegate

- (void)tableViewCell:(PDBackpackTableViewCell *)cell didClickButtonAtIndex:(NSInteger)index tableViewAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemsArr.count) {
        self.selIndexPath = indexPath;
        
        PDBackpackItemModel *item = self.itemsArr[indexPath.row];
        if (index == 0) { // 点击立即赠送 ，进入我的关注列表
            PDPresentListViewController *presentViewController = [[PDPresentListViewController alloc] init];
            presentViewController.item = item;
            presentViewController.delegate = self;
            [self pushViewController:presentViewController animated:YES];
        } else { // 点击立即使用
            __weak typeof(self) weakSelf = self;
            if ([item.exchangeCategory isEqualToString:@"realItem"]) {
                PDReceiveRealViewController *realViewController = [[PDReceiveRealViewController alloc] init];
                realViewController.ID = item.ID;
                [realViewController infoDidSubmitComplication:^{
                    [weakSelf submitSuccessAlert];
                }];
                [self pushViewController:realViewController animated:YES];
            } else if ([item.exchangeCategory isEqualToString:@"phoneMoney"]) {
                PDReceiveMobileViewController *mobileViewController = [[PDReceiveMobileViewController alloc] init];
                mobileViewController.ID = item.ID;
                [mobileViewController infoDidSubmitComplication:^{
                    [weakSelf submitSuccessAlert];
                }];
                [self pushViewController:mobileViewController animated:YES];
            } else if ([item.exchangeCategory isEqualToString:@"lolSkin"]) {
                PDReceiveInventedViewController *inventedViewController =[[PDReceiveInventedViewController alloc] init];
                inventedViewController.ID = item.ID;
                [inventedViewController infoDidSubmitComplication:^{
                    [weakSelf submitSuccessAlert];
                }];
                [self pushViewController:inventedViewController animated:YES];
            } else if ([item.exchangeCategory isEqualToString:@"qCoin"]) {
                PDReceiveViewController *qCoinViewController = [[PDReceiveViewController alloc] init];
                qCoinViewController.ID = item.ID;
                [qCoinViewController infoDidSubmitComplication:^{
                    [weakSelf submitSuccessAlert];
                }];
                [self pushViewController:qCoinViewController animated:YES];
            }
        }
    }
}

#pragma mark - PDPresentListViewControllerDelegate

- (void)presentViewControllerDidPresentCardWithController:(PDPresentListViewController *)viewController {
    self.page = 1;
    if (self.itemsArr.count) {
        [self.itemsArr removeAllObjects];
    }
    [self fetchData];
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:backpackHad requestParams:@{@"pageNum":@(_page)} responseObjectClass:[PDBackpackHadModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        [PDHUD dismiss];
        if (isSucceeded) {
            PDBackpackHadModel *hadModel = (PDBackpackHadModel *)responseObject;
            weakSelf.hadModel = hadModel;
            [weakSelf.itemsArr addObjectsFromArray:hadModel.items];
            [weakSelf.mainTableView reloadData];
            if (weakSelf.itemsArr.count == 0) {
                weakSelf.mainTableView.backgroundColor = [UIColor whiteColor];
                weakSelf.mainTableView.scrollEnabled = NO;
                [weakSelf showNoDataPage];
            } else {
                weakSelf.mainTableView.backgroundColor = [UIColor clearColor];
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
        [weakSelf.mainTableView stopFinishScrollingRefresh];
        [weakSelf.mainTableView stopPullToRefresh];
    }];
}

#pragma mark - 显示无数据页

- (void)showNoDataPage {
    __weak typeof(self) weakSelf = self;
    [self.mainTableView showPrompt:@"哎呀！背包中空空如也!_!~" withImage:[UIImage imageNamed:@"icon_nodata_panda"] buttonTitle:@"点击刷新" clickBlock:^{
        [weakSelf fetchData];
    }];
}

@end
