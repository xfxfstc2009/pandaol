//
//  PDBackpackUsedViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBackpackUsedViewController.h"
#import "PDBackpackTableViewCell.h"
#import "PDExchangeDetailViewController.h"
#import "PDBackpackUsedModel.h"

@interface PDBackpackUsedViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, strong) PDBackpackUsedModel *usedModel;
@end

@implementation PDBackpackUsedViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)basicSetting {
    self.view.backgroundColor = [UIColor clearColor];
    _page = 1;
    _itemsArr = [NSMutableArray array];
    [PDHUD showHUDProgress:@"正在为您获取背包数据，请稍后..." diary:0];
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 64)];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.mainTableView.rowHeight = [PDBackpackTableViewCell cellHeight];
    self.mainTableView.tableFooterView = [UIView new];
    [self.view addSubview:self.mainTableView];
    
    // 刷新
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        if (weakSelf.itemsArr.count) {
            [weakSelf.itemsArr removeAllObjects];
        }
        weakSelf.page = 1;
        [weakSelf fetchData];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.usedModel.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchData];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            return ;
        }
    }];
}

#pragma mark - UITableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"hadCellId";
    PDBackpackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[PDBackpackTableViewCell alloc] initWithType:PDBackpackTableViewCellTypeUsed reuseIdentifier:cellId];
    }
    cell.indexPath = indexPath;
    if (self.itemsArr.count) {
        PDBackpackUsedItemModel *itemModel = [self.itemsArr objectAtIndex:indexPath.row];
        cell.usedItemModel = itemModel;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemsArr.count) {
        PDExchangeDetailViewController *detailViewController = [[PDExchangeDetailViewController alloc] init];
        PDBackpackUsedItemModel *itemModel = [self.itemsArr objectAtIndex:indexPath.row];
        detailViewController.orderId = itemModel.ID;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:backpackUsed requestParams:@{@"pageNum": @(_page)} responseObjectClass:[PDBackpackUsedModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        [PDHUD dismiss];
        
        if (isSucceeded) {
            PDBackpackUsedModel *usedModel = (PDBackpackUsedModel *)responseObject;
            weakSelf.usedModel = usedModel;
            [weakSelf.itemsArr addObjectsFromArray:usedModel.items];
            [weakSelf.mainTableView reloadData];
            if (weakSelf.itemsArr.count == 0) {
                weakSelf.mainTableView.backgroundColor = [UIColor whiteColor];
                weakSelf.mainTableView.scrollEnabled = NO;
                [weakSelf showNoDataPage];
            } else {
                weakSelf.mainTableView.backgroundColor = [UIColor clearColor];
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
        
        [weakSelf.mainTableView stopFinishScrollingRefresh];
        [weakSelf.mainTableView stopPullToRefresh];
    }];
}

#pragma mark - 显示无数据页

- (void)showNoDataPage {
    __weak typeof(self) weakSelf = self;
    [self.mainTableView showPrompt:@"哎呀！背包中空空如也!_!~" withImage:[UIImage imageNamed:@"icon_nodata_panda"] buttonTitle:@"点击刷新" clickBlock:^{
        [weakSelf fetchData];
    }];
}

@end
