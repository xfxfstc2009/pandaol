//
//  PDExchangeDetailViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDExchangeDetailViewController.h"
#import "PDCenterTool.h"
#import "PDExchaneDetailCell.h"
#import "PDBackpackUsedItemModel.h"

@interface PDExchangeDetailViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) PDBackpackUsedItemModel *itemModel;
@end

@implementation PDExchangeDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    self.barMainTitle = @"兑换详情";
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
}

#pragma mark - UITabelView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else {
        if ([self.itemModel.exchangeCategory isEqualToString:@"realItem"] || [self.itemModel.exchangeCategory isEqualToString:@"lolSkin"]) {
             return 2;
        } else {
            return 1;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        NSString *cellId = @"cellId";
        PDExchaneDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[PDExchaneDetailCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
        }
        cell.model = self.itemModel;
        return cell;
    } else {
        NSString *cellId = @"cellId";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
            cell.detailTextLabel.textColor = c26;
        }
        
        if ([self.itemModel.exchangeCategory isEqualToString:@"realItem"]) {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"收货人";
                cell.detailTextLabel.text = self.itemModel.receiver;
            } else {
                cell.textLabel.text = @"联系电话";
                cell.detailTextLabel.text = self.itemModel.telephone;
            }
        } else if ([self.itemModel.exchangeCategory isEqualToString:@"phoneMoney"]) {
            cell.textLabel.text = @"手机账号";
            cell.detailTextLabel.text = self.itemModel.cellphone;
        } else if ([self.itemModel.exchangeCategory isEqualToString:@"lolSkin"]) {
            if (indexPath.row == 0) {
                cell.textLabel.text = @"收货QQ";
                cell.detailTextLabel.text = self.itemModel.qq;
            } else {
                cell.textLabel.text = @"大区信息";
                cell.detailTextLabel.text = self.itemModel.gameServerName;
            }
            
        } else {
            cell.textLabel.text = @"收货QQ";
            cell.detailTextLabel.text = self.itemModel.qq;
        }
        return cell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [PDExchaneDetailCell cellHeightWithModel:self.itemModel];
    } else {
        return 44;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:exchangeDetail requestParams:@{@"orderId": self.orderId} responseObjectClass:[PDBackpackUsedItemModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            weakSelf.itemModel = (PDBackpackUsedItemModel *)responseObject;
            [weakSelf.mainTableView reloadData];
        }
    }];
}

@end
