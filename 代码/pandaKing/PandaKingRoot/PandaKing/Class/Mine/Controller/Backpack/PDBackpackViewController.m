//
//  PDBackpackViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBackpackViewController.h"
#import "CCZSegmentController.h"
#import "PDBackpackHadViewController.h"
#import "PDBackpackUsedViewController.h"
#import "PDBackpackExchangeRuleViewController.h"

@interface PDBackpackViewController ()
@property (nonatomic, assign) CGFloat segementTopHeight;
@end

@implementation PDBackpackViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor clearColor].CGColor;
}

- (void)basicSetting {
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.barMainTitle = @"我的背包";
    _segementTopHeight = 20.;
    
    __weak typeof(self) weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_exchange_rule"] barHltImage:nil action:^{
        [weakSelf fetchData];
    }];
}

- (void)pageSetting {
    // 背景黑布
    UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 220.)];
    
    blackView.backgroundColor = c2;
    [self.view addSubview:blackView];
    
    // 上半背景图
    UIImageView *topImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, _segementTopHeight + 44, kScreenBounds.size.width, 220 - _segementTopHeight - 44)];
    topImageView.image = [UIImage imageNamed:@"icon_backpack_topbg"];
    [self.view addSubview:topImageView];
    
    PDBackpackHadViewController *hadViewController = [[PDBackpackHadViewController alloc] init];
    PDBackpackUsedViewController *usedViewController = [[PDBackpackUsedViewController alloc] init];
    CCZSegmentController *segement = [[CCZSegmentController alloc] initWithFrame:CGRectMake(0, _segementTopHeight, kScreenBounds.size.width, kScreenBounds.size.height - _segementTopHeight) titles:@[@"已拥有",@"已使用"]];
    segement.viewControllers = @[hadViewController, usedViewController];
    segement.segmentView.contentView.backgroundColor = [UIColor clearColor];
    [self addSegmentController:segement];
}

- (void)fetchData {
    // 兑换规则
    PDBackpackExchangeRuleViewController *exchangeRule = [[PDBackpackExchangeRuleViewController alloc] init];
    [exchangeRule webDirectedWebUrl:[PDCenterTool absoluteUrlWithRisqueUrl:backpackExchangeRule]];
    [self pushViewController:exchangeRule animated:YES];
}

@end
