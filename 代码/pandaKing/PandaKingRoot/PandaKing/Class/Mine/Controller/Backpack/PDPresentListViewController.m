//
//  PDPresentListViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPresentListViewController.h"
#import "PDCenterFriendTableViewCell.h"
#import "PDCenterFriendList.h"

@interface PDPresentListViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) UIImageView *buttonBgImageView;
@property (nonatomic, assign) NSInteger followPage;
@property (nonatomic, strong) PDCenterFriendList *followList;
@property (nonatomic, strong) NSMutableArray *followsArr;
@property (nonatomic, strong) UIButton *selectedButton;
@end

@implementation PDPresentListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchFollowList];
}

- (void)basicSetting {
    self.barMainTitle = @"选择好友";
    _followPage = 1;
    _followsArr = [NSMutableArray array];
}

- (void)pageSetting {
    [self tableViewSetting];
}

- (void)tableViewSetting {
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    tableView.backgroundColor = [UIColor clearColor];
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.tableFooterView = [UIView new];
    tableView.separatorColor = c27;
    [self.view addSubview:tableView];
    self.mainTableView = tableView;
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        if (weakSelf.followsArr.count) {
            [weakSelf.followsArr removeAllObjects];
        }
        
        weakSelf.followPage = 1;
        [weakSelf fetchFollowList];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.followList.hasNextPage) {
            weakSelf.followPage ++;
            [weakSelf fetchFollowList];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            return ;
        }
    }];
}

#pragma mark - UITabelView delegatge

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.followsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *friendCellId = @"friendCellid";
    PDCenterFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:friendCellId];
    if (!cell) {
        cell = [[PDCenterFriendTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:friendCellId];
    }
    cell.indexPath = indexPath;
    if (self.followsArr.count) {
        cell.model = self.followsArr[indexPath.row];
        [cell setNoneMoreButton];
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.followsArr.count - 1) { // 最后一行
        [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PDCenterFriendTableViewCell cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.followsArr.count) {
        PDCenterFriendItem *friendItem = self.followsArr[indexPath.row];
        
        __weak typeof(self) weakSelf = self;
        [[PDAlertView sharedAlertView] showAlertWithTitle:@"赠送提示" conten:[NSString stringWithFormat:@"确定给【%@】赠送【%@】吗？",friendItem.friendInfo.nickname,self.item.commodityName] isClose:NO btnArr:@[@"确定",@"取消"] buttonClick:^(NSInteger buttonIndex) {
            [JCAlertView dismissWithCompletion:NULL];
            if (buttonIndex == 0) {
                [weakSelf fetchPresentWithMemberId:friendItem.friendInfo.ID];
            }
        }];
    }
}

#pragma mark - UIscroll deldegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [PDCenterTool tableViewScrolledRemoveViscousCancleWithScrollView:scrollView sectionHeight:kTableViewHeader_height];
}

#pragma mark - 网络请求

- (void)fetchFollowList {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMyfollowlist requestParams:@{@"pageNum":@(_followPage)} responseObjectClass:[PDCenterFriendList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDCenterFriendList *followList = (PDCenterFriendList *)responseObject;
            weakSelf.followList = followList;
            [weakSelf.followsArr addObjectsFromArray:followList.items];
            [weakSelf.mainTableView reloadData];
            if (weakSelf.followsArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                [weakSelf.mainTableView dismissPrompt];
                weakSelf.mainTableView.scrollEnabled = YES;
            }
        }
        
        [weakSelf.mainTableView stopFinishScrollingRefresh];
        [weakSelf.mainTableView stopPullToRefresh];
    }];
}

- (void)fetchPresentWithMemberId:(NSString *)memberId {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:present requestParams:@{@"cerId":self.item.ID, @"receiveMemberId":memberId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            if ([weakSelf.delegate respondsToSelector:@selector(presentViewControllerDidPresentCardWithController:)]) {
                [weakSelf.delegate presentViewControllerDidPresentCardWithController:weakSelf];
            }
            [weakSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
}

#pragma mark - 显示无数据页

- (void)showNoDataPage {
    [self.mainTableView showPrompt:@"您还未关注过任何小伙伴-.-~ !!!" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:0 tapBlock:NULL];
}

@end
