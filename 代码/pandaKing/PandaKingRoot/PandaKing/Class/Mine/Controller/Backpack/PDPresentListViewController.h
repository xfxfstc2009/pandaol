//
//  PDPresentListViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDBackpackItemModel.h"

/// 赠送的好友列表
@protocol PDPresentListViewControllerDelegate;
@interface PDPresentListViewController : AbstractViewController
@property (nonatomic, strong) PDBackpackItemModel *item;   /**< 兑换卷id*/
@property (nonatomic, weak)   id<PDPresentListViewControllerDelegate> delegate;
@end

@protocol PDPresentListViewControllerDelegate <NSObject>

- (void)presentViewControllerDidPresentCardWithController:(PDPresentListViewController *)viewController;

@end