//
//  PDCenterViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/9.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterViewController.h"
#import "PDAddRoleTableViewCell.h"
#import "PDCenterTool.h"
#import "PDCenterHeaderView.h"
#import "PDEditViewController.h"
#import "PDWalletViewController.h"
#import "PDSnatchNoteViewController.h"
#import "PDBackpackViewController.h"
#import "PDCenterRoleTableViewCell.h"
#import "PDCenterFriendViewController.h"
#import "PDInviteViewController.h"
#import "PDZoneViewController.h"
#import "PDCenterMyInfo.h"
#import "PDCenterSettingViewController.h"                       // 设置

// 填写收货地址
#import "PDReceiveViewController.h"
#import "PDReceiveMobileViewController.h"
#import "PDReceiveRealViewController.h"
#import "PDReceiveInventedViewController.h"

// 我的召唤师
#import "PDCenterGameInfo.h"
#import "PDBindingInfoViewController.h"
#import "PDPandaRoleDetailViewController.h"               //  角色详情

#import "PDSelectedSheetView.h"

#import "PDInternetCafesRootViewController.h"               // 我的网吧

// 我的竞猜
#import "PDCenterLotteryHeroViewController.h"

// 商城
#import "PDStoreViewController.h"

// 联系我们
#import "PDCustomerCenterViewController.h"

// 我的任务
#import "PDMyTaskViewController.h"

@interface PDCenterViewController ()<UITableViewDataSource, UITableViewDelegate, PDCenterHeaderViewDelegate>
@property (nonatomic, strong) UITableView *mainTabelView;
@property (nonatomic, strong) NSArray *sectionArr;
@property (nonatomic, strong) NSArray *imageNameArr;

@property (nonatomic, assign) CGFloat sectionHeaderHeight;

@property (nonatomic, strong) PDImageView *roleImageView;   /**< 底部那个大头像*/
@property (nonatomic, strong) PDCenterHeaderView *centerHeaderView; /**< 个人信息面板*/
@property (nonatomic, strong) PDCenterGameInfo *gameInfo;

@property (nonatomic, assign) BOOL isRoleExit;  /**< 绑定的召唤师是否存在*/
@end

@implementation PDCenterViewController

+ (instancetype)sharedController{
    static PDCenterViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[PDCenterViewController alloc] init];
    });
    return _sharedAccountModel;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchMemberGameUser];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self fetchData];
    [self fetchTaskReceived];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self fetchMemberGameUser];
    
    if (![Tool userDefaultGetWithKey:NewFeature_Center].length){
        PDNewFeaturePageViewController *newFeaturePageViewController = [[PDNewFeaturePageViewController alloc]init];
        [newFeaturePageViewController showInView:self.parentViewController type:PDNewFeaturePageViewControllerTypeCenter];
    }
}

- (void)basicSetting {
    if ([AccountModel sharedAccountModel].isShenhe){ // 正在审核中
        self.sectionArr = @[@[@"我的撸友",@"附近网吧"],@[@"邀请有奖",@"联系我们",@"设置"]];
        self.imageNameArr = @[@[@"icon_center_friend",@"icon_center_netbar"],@[@"icon_center_invite",@"icon_center_contact",@"icon_center_setting"]];
    } else {
//        self.sectionArr = @[@[@"添加角色"],@[@"我的任务",@"我的竞猜",@"竹子商城",@"我的撸友",@"我的网吧"],@[@"邀请有奖",@"联系我们",@"设置"]];
//        self.imageNameArr = @[@[@"icon_center_add"],@[@"icon_center_task",@"icon_center_lottery",@"icon_center_store",@"icon_center_friend",@"icon_center_netbar"],@[@"icon_center_invite",@"icon_center_contact",@"icon_center_setting"]];
        self.sectionArr = @[@[@"我的背包",@"竹子商城",@"我的任务",@"邀请有奖"],@[@"附近网吧",@"设置"]];
        self.imageNameArr = @[@[@"icon_center_backpack",@"icon_center_store",@"icon_center_task",@"icon_center_invite"],@[@"icon_center_netbar",@"icon_center_setting"]];

    }
    
    _sectionHeaderHeight = 250;
    _isRoleExit = NO;
}

- (void)pageSetting {
    [self roleHeaderImageView];
    [self tabelViewSetting];
    [self headerViewSetting];
}

// 背景大图
- (void)roleHeaderImageView {
    self.roleImageView = [[PDImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [PDCenterHeaderView headerViewHeight])];
    self.roleImageView.userInteractionEnabled = YES;
    self.roleImageView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.roleImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:self.roleImageView];
}

- (void)tabelViewSetting {
    self.mainTabelView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.mainTabelView.backgroundColor = [UIColor clearColor];
    self.mainTabelView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTabelView.showsVerticalScrollIndicator = NO;
    self.mainTabelView.delegate = self;
    self.mainTabelView.dataSource = self;
    self.mainTabelView.tableFooterView = [[UIView alloc] init];
    self.mainTabelView.separatorColor = c27;
    [self.view addSubview:self.mainTabelView];
}

- (void)headerViewSetting {
    self.centerHeaderView = [[PDCenterHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [PDCenterHeaderView headerViewHeight])];
    self.centerHeaderView.delegate = self;
    self.mainTabelView.tableHeaderView = self.centerHeaderView;
}

#pragma mark - UITableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.sectionArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.sectionArr[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    NSString *centerRoleCellId = @"centerRoleCellId";
//    NSString *addRoleCellId = @"addRoleCellId";
    NSString *normalCellId = @"normalCellId";
    
    NSString *sectionString = [self.sectionArr[indexPath.section] objectAtIndex:indexPath.row];
    NSString *imageName = [self.imageNameArr[indexPath.section] objectAtIndex:indexPath.row];
//    if (indexPath.section == 0) {
//        
//        if (_isRoleExit) {
//            PDCenterRoleTableViewCell *centerRoleCell = [tableView dequeueReusableCellWithIdentifier:centerRoleCellId];
//            if (!centerRoleCell) {
//                centerRoleCell = [[PDCenterRoleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:centerRoleCellId];
//            }
//            centerRoleCell.model = self.gameInfo;
//            return centerRoleCell;
//        } else {
//            PDAddRoleTableViewCell *addRoleCell = [tableView dequeueReusableCellWithIdentifier:addRoleCellId];
//            if (addRoleCell == nil) {
//                addRoleCell = [[PDAddRoleTableViewCell alloc] initWithStyle:UITableViewCellStyleValue2 reuseIdentifier:addRoleCellId];
//            }
//            addRoleCell.title = sectionString;
//            addRoleCell.image = [UIImage imageNamed:imageName];
//            return addRoleCell;
//        }
//        
//    } else {
        UITableViewCell *normalCell = [tableView dequeueReusableCellWithIdentifier:normalCellId];
        if (normalCell == nil) {
            normalCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:normalCellId];
            normalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        normalCell.imageView.image = [UIImage imageNamed:imageName];
        normalCell.textLabel.text = sectionString;
        
        normalCell.detailTextLabel.textColor = [UIColor redColor];
        normalCell.detailTextLabel.font = [UIFont systemFontOfCustomeSize:12];
        
        return normalCell;
//    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return 0.01;
    }
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.section == 0) {
//        if (_isRoleExit) {
//            return [PDCenterRoleTableViewCell cellHeight];
//        } else {
//            return [PDAddRoleTableViewCell cellHeight];
//        }
//    } else {
        return [PDAddRoleTableViewCell cellHeight];
//    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *sectionString = [self.sectionArr[indexPath.section] objectAtIndex:indexPath.row];
    
//    if (indexPath.section == 0) {
//        if (self.isRoleExit){               // 跳转到角色详情
//            PDPandaRoleDetailViewController *bindingRoleDetailViewController = [[PDPandaRoleDetailViewController alloc]init];
//            bindingRoleDetailViewController.transferLolGameUserId = self.gameInfo.memberLOLGameUser.lolGameUser.gameUserId;
//            [bindingRoleDetailViewController hidesTabBarWhenPushed];
//            __weak typeof(self)weakSelf = self;
//            [bindingRoleDetailViewController bindingCancelWithBlock:^{
//                if (!weakSelf){
//                    return ;
//                }
//                __strong typeof(weakSelf)strongSelf = weakSelf;
//                strongSelf.isRoleExit = NO;
//                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//                [strongSelf.mainTabelView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationMiddle];
//                [[PDPandaRootViewController sharedController] removeAccountInfoSet];
//            }];
//            [self.navigationController pushViewController:bindingRoleDetailViewController animated:YES];
//            
//            // 激活
//            [bindingRoleDetailViewController activityInfoWithBlock:^(BindingStatus bindingStatus) {
//                if (!weakSelf){
//                    return ;
//                }
//                __strong typeof(weakSelf)strongSelf = weakSelf;
//                if (bindingStatus == BindingStatusActiviting){
//                    [strongSelf fetchMemberGameUser];
//                    [[PDPandaRootViewController sharedController] sendRequestToGetPersonalInfoHasAnimation:NO block:NULL];
//                }
//            }];
//        } else {
//            [self authorizeWithCompletionHandler:^{
//                PDBindingInfoViewController *bindingRoleDetailViewController = [[PDBindingInfoViewController alloc]init];
//                __weak typeof(self)weakSelf = self;
//                [bindingRoleDetailViewController bindingDidEndWithBlock:^(BOOL isbind, BOOL isActivity) {
//                    if (!weakSelf){
//                        return ;
//                    }
//                    __strong typeof(weakSelf)strongSelf = weakSelf;
//                    [strongSelf fetchMemberGameUser];
//                    [[PDPandaRootViewController sharedController] sendRequestToGetPersonalInfoHasAnimation:NO block:NULL];
//                }];
//                [bindingRoleDetailViewController hidesTabBarWhenPushed];
//                [self.navigationController pushViewController:bindingRoleDetailViewController animated:YES];
//            }];
//        }
//    } else {
        if ([sectionString isEqualToString:@"我的撸友"]) {
            PDCenterFriendViewController *friendViewController = [[PDCenterFriendViewController alloc] init];
            [self pushViewController:friendViewController animated:YES];
        } else if ([sectionString isEqualToString:@"附近网吧"]) {
            PDInternetCafesRootViewController *internetCafesRootViewController = [[PDInternetCafesRootViewController alloc]init];
            internetCafesRootViewController.transferInternetCafeType = InternetCafesTypeNearBy;
            [internetCafesRootViewController hidesTabBarWhenPushed];
            [self.navigationController pushViewController:internetCafesRootViewController animated:YES];
        } else if ([sectionString isEqualToString:@"邀请有奖"]) {
            PDInviteViewController *iniviteViewController = [[PDInviteViewController alloc] init];
            [iniviteViewController hidesTabBarWhenPushed];
            [self pushViewController:iniviteViewController animated:YES];
        } else if ([sectionString isEqualToString:@"设置"]) {
            PDCenterSettingViewController *centerSettingViewController = [[PDCenterSettingViewController alloc]init];
            [centerSettingViewController hidesTabBarWhenPushed];
            [self.navigationController pushViewController:centerSettingViewController animated:YES];
        } else if ([sectionString isEqualToString:@"我的竞猜"]) {
            PDCenterLotteryHeroViewController *heroViewController = [[PDCenterLotteryHeroViewController alloc] init];
            heroViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:heroViewController animated:YES];
        } else if ([sectionString isEqualToString:@"竹子商城"]) {
            PDStoreViewController *storeViewController = [[PDStoreViewController alloc] init];
            [self pushViewController:storeViewController animated:YES];
        } else if ([sectionString isEqualToString:@"联系我们"]) {
            PDCustomerCenterViewController *customerCenterViewController = [[PDCustomerCenterViewController alloc] init];
            [self pushViewController:customerCenterViewController animated:YES];
        } else if ([sectionString isEqualToString:@"我的任务"]) {
            PDMyTaskViewController *taskViewController = [[PDMyTaskViewController alloc] init];
            taskViewController.gameInfo = self.gameInfo;
            [self pushViewController:taskViewController animated:YES];
        } else if ([sectionString isEqualToString:@"我的背包"]) {
            PDBackpackViewController *backpackViewController = [[PDBackpackViewController alloc] init];
            [self pushViewController:backpackViewController animated:YES];
        }
//    }
}

- (void)clearMemberInfo {
    PDCenterMyInfo *info = [PDCenterMyInfo new];
    info.member.personId = @"";
    info.member.nickname = @"";
    info.member.nickname = @"";
    info.gold = 0;
    info.bamboo = 0;
    self.centerHeaderView.myInfo = info;
    self.roleImageView.image = [UIImage imageNamed:@"icon_nordata"];
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY <= 0) {
        CGFloat scale = (offsetY) * 2 / ([PDCenterHeaderView headerViewHeight]);
        self.roleImageView.transform = CGAffineTransformMakeScale(1 - scale, 1 - scale);
        [self.centerHeaderView updateSubViewsWithOffsetY:offsetY];
    } else {
        self.roleImageView.orgin_y = - offsetY / 2;
    }
}

#pragma mark - HeaderView delegate

- (void)centerHeaderView:(PDCenterHeaderView *)headerView didClickButtonAtIndex:(NSInteger)index {
    if (index == 0) {
#ifdef DEBUG
        PDWalletViewController *wallentViewController = [[PDWalletViewController alloc] init];
        [self pushViewController:wallentViewController animated:YES];
#else
        if ([AccountModel sharedAccountModel].isShenhe) {
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"提示" conten:@"金币不足，去分享吧，骚年！分享可以获得金币。" isClose:NO btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                [JCAlertView dismissAllCompletion:NULL];
            }];
        } else {
            PDWalletViewController *wallentViewController = [[PDWalletViewController alloc] init];
            [self pushViewController:wallentViewController animated:YES];
        }
#endif
        
    } else if (index == 1) {
        PDCenterLotteryHeroViewController *myLotteryViewController = [[PDCenterLotteryHeroViewController alloc] init];
        [self pushViewController:myLotteryViewController animated:YES];
    } else {
        PDSnatchNoteViewController *snatchNoteViewController = [[PDSnatchNoteViewController alloc] init];
        [self pushViewController:snatchNoteViewController animated:YES];
    }
}

- (void)centerHeaderView:(PDCenterHeaderView *)headerView didClickRoleHeaderView:(UIImageView *)imageView {
    PDEditViewController *editViewContorller = [[PDEditViewController alloc] init];
    [self pushViewController:editViewContorller animated:YES];
}

#pragma mark - 网络请求

- (void)fetchData {
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:myInfo requestParams:nil responseObjectClass:[PDCenterMyInfo class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (isSucceeded) {
            PDCenterMyInfo *myInfo = (PDCenterMyInfo *)responseObject;
            strongSelf.centerHeaderView.myInfo = myInfo;
            [strongSelf changeRoleImage:myInfo];
            [AccountModel sharedAccountModel].avatar = myInfo.member.avatar;
            [AccountModel sharedAccountModel].uid = myInfo.member.personId;
            [AccountModel sharedAccountModel].nickname = myInfo.member.nickname;
            [Tool userDefaulteWithKey:CustomerAvatar Obj:myInfo.member.avatar];
            [Tool userDefaulteWithKey:CustomerMemberId Obj:myInfo.member.personId];
            [Tool userDefaulteWithKey:CustomerNickname Obj:myInfo.member.nickname];
        }
    }];
}

- (void)changeRoleImage:(PDCenterMyInfo *)myInfo {
    __weak typeof(self) weakSelf = self;
    [self.roleImageView uploadImageWithURL:myInfo.member.avatar placeholder:nil callback:^(UIImage *image) {
        weakSelf.roleImageView.image = [image applyDarkEffect];
    }];
}

- (void)fetchMemberGameUser {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberGameUser requestParams:nil responseObjectClass:[PDCenterGameInfo class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            
            PDCenterGameInfo *gameInfo = (PDCenterGameInfo *)responseObject;
            weakSelf.gameInfo = gameInfo;
            if (gameInfo.stats) {
                _isRoleExit = YES;
            }
            if (weakSelf.mainTabelView){
                [weakSelf.mainTabelView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
    }];
}

- (void)removeMemberGameUser{
    _isRoleExit = NO;
    [self.mainTabelView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)fetchTaskReceived {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerTaskReceived requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            NSNumber *count = responseObject[@"count"];
            UITableViewCell *taskCell = [weakSelf.mainTabelView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:0]];
            if (count.intValue) {
                taskCell.detailTextLabel.text = @"您有未领取的福利";
            } else {
                taskCell.detailTextLabel.text = @"";
            }
        }
    }];
}

@end
