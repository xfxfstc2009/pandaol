//
//  PDWalletChangeList.h
//  PandaKing
//
//  Created by Cranz on 16/11/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPropertyItem.h"

@interface PDWalletChangeList : FetchModel
@property (nonatomic, strong) NSArray <PDPropertyItem> *items;
@property (nonatomic, assign) NSInteger goldBalance;
@property (nonatomic, assign) NSInteger bambooBalance;
@end
