//
//  PDMemberTopUpNote.h
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMemberTopUpItem.h"

/// 充值记录
@interface PDMemberTopUpNote : FetchModel
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) NSArray <PDMemberTopUpItem>*items;
@end
