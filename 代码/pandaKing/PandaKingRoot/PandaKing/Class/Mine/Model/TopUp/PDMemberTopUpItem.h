//
//  PDMemberTopUpItem.h
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMemberTopUpDetail.h"

@protocol PDMemberTopUpItem <NSObject>

@end

@interface PDMemberTopUpItem : FetchModel
@property (nonatomic, strong) NSMutableArray <PDMemberTopUpDetail>* detail;
@property (nonatomic, copy) NSString *day;
@end
