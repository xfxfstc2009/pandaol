//
//  PDMemberTopUpDetail.h
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDMemberTopUpDetail <NSObject>

@end

@interface PDMemberTopUpDetail : FetchModel
@property (nonatomic, assign) NSInteger deltaFee;   /**< 5 ／ －5*/
@property (nonatomic, assign) NSTimeInterval logTime;
@property (nonatomic, copy) NSString *reason;
@property (nonatomic, copy) NSString *source;
/**< 来源：
 invite 邀请
 invited 被邀请
 buybamboo 买竹子
 buyGold 买金币
 usepandaking 打开一次 熊猫王
 winmatches 打比赛，
 goldExchange 金币兑换...
 bambooexchange 竹子兑换
 bambooRedeemAct 竹子商城
 task 任务
 invitedMemFirstActivateLOLGameUser 我邀请的好友第一次帮顶召唤师...
 treasure 众筹夺宝
 matchguess 赛事猜
 anchorguess 主播竞猜...
 lolchampionguess 英雄猜
 matchguesscancel 赛事竞猜取消...
 anchorguesscancel 主播竞猜取消...
 lolchampionguesscancel 英雄猜取消...
 doneorder 订单兑现(发货)...
 taskAward 任务领奖...
 memberTransfersGoldToMember 会员金币转账...
 memberTop 排行榜
 */
@end
