//
//  PDCenterTopUpList.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDCenterTopUpItem.h"

/// 充值列表
@interface PDCenterTopUpList : FetchModel
@property (nonatomic, strong) NSArray <PDCenterTopUpItem>*items;
@property (nonatomic, assign) NSInteger bambooBalance;
@property (nonatomic, assign) NSInteger goldBalance;
@property (nonatomic, assign) CGFloat goldAwardPercentForFirstPay;//首充多送xx%金币
@end
