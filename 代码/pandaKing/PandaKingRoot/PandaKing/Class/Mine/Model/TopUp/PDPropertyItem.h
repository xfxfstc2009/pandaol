//
//  PDPropertyItem.h
//  PandaKing
//
//  Created by Cranz on 16/11/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 财产model
@protocol PDPropertyItem <NSObject>

@end

@interface PDPropertyItem : FetchModel
@property (nonatomic, assign) NSInteger gold;
@property (nonatomic, assign) NSInteger bamboo;
@end
