//
//  PDCenterTopUpItem.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDCenterTopUpItem <NSObject>
@end

@interface PDCenterTopUpItem : FetchModel
@property (nonatomic, copy) NSString *cash;   /**< 对应的钱*/
@property (nonatomic, assign) NSInteger gold;   /**< 金币*/
@property (nonatomic, assign) NSInteger goldAwardForPay;//充值即送xx金币


// temp ares
@property (nonatomic,assign)NSInteger payType;           /**< 1 支付宝 2 微信*/
@end
