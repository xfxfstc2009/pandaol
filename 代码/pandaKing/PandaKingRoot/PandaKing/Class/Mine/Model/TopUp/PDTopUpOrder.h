//
//  PDTopUpOrder.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 请求订单
@interface PDTopUpOrder : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *orderName;
@property (nonatomic, copy) NSString *totalPrice;
@property (nonatomic, copy) NSString *alipayNotifyUrl;
@property (nonatomic, copy) NSString *tenpayNotifyUrl;
@end
