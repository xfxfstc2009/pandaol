//
//  PDMemberInfoModel.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDEditAddressModel.h"

@interface PDMemberInfoModel : FetchModel
@property (nonatomic, strong) PDEditAddressModel *address;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *qq;
@property (nonatomic, copy) NSString *weChat;
@property (nonatomic, copy) NSString *signature;
@property (nonatomic, copy) NSString *gender;   /**< 性别=男 MALE|女 FEMALE*/
@property (nonatomic, copy) NSString *cellphone;    /**< 会员电话*/
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, assign) NSInteger age;
@property (nonatomic, assign) NSInteger registerTime;   /**< 注册时间*/
@property (nonatomic, assign) BOOL enable;  /**< 是否冻结true ｜ false*/
@property (nonatomic, assign) BOOL bindGameUser;
@property (nonatomic, assign) BOOL activateGameUser;
@end
