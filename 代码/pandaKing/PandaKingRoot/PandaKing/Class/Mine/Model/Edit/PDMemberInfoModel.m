//
//  PDMemberInfoModel.m
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMemberInfoModel.h"

@implementation PDMemberInfoModel
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"userId": @"id"};
}
@end
