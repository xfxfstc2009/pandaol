//
//  PDEditAddressModel.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDEditSubAddress.h"

@interface PDEditAddressModel : FetchModel
@property (nonatomic, strong) PDEditSubAddress *province;
@property (nonatomic, strong) PDEditSubAddress *city;
@property (nonatomic, strong) PDEditSubAddress *district;
@end
