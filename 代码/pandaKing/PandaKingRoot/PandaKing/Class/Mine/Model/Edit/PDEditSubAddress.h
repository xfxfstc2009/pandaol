//
//  PDEditSubAddress.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDEditSubAddress : FetchModel
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *code;
@end
