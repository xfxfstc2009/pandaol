//
//  PDLotteryData.h
//  PandaKing
//
//  Created by Cranz on 16/10/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 我的竞猜信息
@interface PDLotteryData : FetchModel
@property (nonatomic, assign) NSInteger times;
@property (nonatomic, assign) NSInteger winTimes;
@property (nonatomic, assign) NSInteger profit; /**< 累计收益*/
@end
