//
//  PDLotteryGameList.h
//  PandaKing
//
//  Created by Cranz on 16/10/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryGameItem.h"

@interface PDLotteryGameList : FetchModel
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSArray <PDLotteryGameItem>*gameList;
@property (nonatomic, assign) BOOL hasNextPage;
@end
