//
//  PDLotteryGameItem.h
//  PandaKing
//
//  Created by Cranz on 16/10/31.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
/*
state:
    waiting,//等待结果
    win,//预测成功
    lose;//预测失败
*/

@protocol PDLotteryGameItem <NSObject>

@end
@interface PDLotteryGameItem : FetchModel
@property (nonatomic, assign) NSTimeInterval dateTime;
@property (nonatomic, assign) NSInteger goldCount;
@property (nonatomic, copy)   NSString *ID;
@property (nonatomic, copy) NSString *team; /**< 队伍名字,  如果时主播猜，这个字断返回nil*/
@property (nonatomic, copy) NSString *state;//状态：等待结果 waiting，预测成功 win，预测失败 lose
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) BOOL isFinished;
@property (nonatomic, copy) NSString *point;
@property (nonatomic, assign) NSInteger result;
@property (nonatomic, assign) CGFloat odds; // 赔率
@property (nonatomic, assign) NSInteger finishTime; // 开奖时间
@end
