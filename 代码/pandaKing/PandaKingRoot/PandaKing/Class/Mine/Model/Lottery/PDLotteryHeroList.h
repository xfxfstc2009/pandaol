//
//  PDLotteryHeroList.h
//  PandaKing
//
//  Created by Cranz on 16/11/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryHeroItem.h"

@interface PDLotteryHeroList : FetchModel
@property (nonatomic, strong) NSArray<PDLotteryHeroItem> *gameList;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, assign) BOOL hasNextPage;
@end
