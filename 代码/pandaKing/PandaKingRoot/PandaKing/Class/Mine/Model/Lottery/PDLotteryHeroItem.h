//
//  PDLotteryHeroItem.h
//  PandaKing
//
//  Created by Cranz on 16/11/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDLotteryHeroItem <NSObject>

@end
 /**
 * @param goldPrice
 *            0:不选, 1:3150及以下, 2:4800, 3:6300及以上
 * @param distance
 *            0:不选, 1:远程, 2:近程
 * @param nameCount
 *            0:不选, 1:两个及以下, 2:三个, 3:四个及以上
 */

typedef NS_ENUM(NSUInteger, PDLotteryHeroType) {
    PDLotteryHeroTypeLol = 1, // lol
    PDLotteryHeroTypeDota2, // dota2
    PDLotteryHeroTypeKing, // 王者荣耀
};

@interface PDLotteryHeroItem : FetchModel
@property (nonatomic, assign) NSInteger result;
@property (nonatomic, assign) NSTimeInterval dateTime;
@property (nonatomic, assign) NSInteger goldCount;
@property (nonatomic, copy) NSString *lotteryNum;
@property (nonatomic, copy) NSString *state;
@property (nonatomic, assign) BOOL isFinished;
@property (nonatomic, assign) NSInteger goldPrice;
@property (nonatomic, assign) NSInteger distance;
@property (nonatomic, assign) NSInteger nameCount;
@property (nonatomic, assign) CGFloat odds; // 赔率
@property (nonatomic, assign) NSInteger finishTime; // 开奖时间
@property (nonatomic, assign) PDLotteryHeroType championGuessType;

//king
@property (nonatomic, assign) NSInteger  nameLength;
// dota2
@property (nonatomic, assign) NSInteger camp;
@property (nonatomic, assign) NSInteger mainProperty;

@end
