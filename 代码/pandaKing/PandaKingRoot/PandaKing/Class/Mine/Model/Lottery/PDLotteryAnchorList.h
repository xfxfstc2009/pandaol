//
//  PDLotteryAnchorList.h
//  PandaKing
//
//  Created by Cranz on 16/11/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryGameItem.h"

@interface PDLotteryAnchorList : FetchModel
@property (nonatomic, strong) NSArray <PDLotteryGameItem> *guessList;
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, assign) NSInteger page;
@end
