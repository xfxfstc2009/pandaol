//
//  PDNewbieTask.h
//  PandaKing
//
//  Created by Cranz on 17/1/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDTaskObject.h"

/// 新手任务
@interface PDNewbieTask : FetchModel
@property (nonatomic, assign) NSUInteger doneCount;
@property (nonatomic, assign) NSUInteger totalCount;
@property (nonatomic, strong) NSArray <PDTaskObject> *items;
@end
