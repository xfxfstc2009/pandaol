//
//  PDTaskList.h
//  PandaKing
//
//  Created by Cranz on 17/1/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDNewbieTask.h"
#import "PDArchieveTask.h"
#import "PDDayLoopTask.h"

/// 任务列表
@interface PDTaskList : FetchModel
@property (nonatomic, strong) PDNewbieTask *newbieTask;
@property (nonatomic, strong) PDArchieveTask *archieveTask;
@property (nonatomic, strong) PDDayLoopTask *dayLoopTask;
@end
