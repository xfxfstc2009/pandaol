//
//  PDTaskObject.h
//  PandaKing
//
//  Created by Cranz on 17/1/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDTaskAward.h"
/*
 "name":"登陆签到",
 "id":"586df3b81be82403c53224b4",
 "award":{
 "gold":100,
 "bamboo":0
 },
 "doneTimes":0,
 "times":1,
 "done":false,
 "receiveAward":false,
 "type":"dayLoopTask",
 "bsnsType":"winGuessContinue6"//业务类型
 */

// type: newbieTask(新手任务),archieveTask(成就任务),dayLoopTask(每日任务)

/// 任务的基类
@protocol PDTaskObject <NSObject>

@end

@interface PDTaskObject : FetchModel
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, strong) PDTaskAward *award;
@property (nonatomic, assign) NSUInteger doneTimes;
@property (nonatomic, assign) NSUInteger times;
@property (nonatomic, assign) BOOL done; // 是否做了
@property (nonatomic, assign) BOOL receiveAward; // 收到奖赏
@property (nonatomic, copy)   NSString *type; /// 任务类型
@property (nonatomic, copy)   NSString *bsnsType;
/**
 * 跳转类型
 */
@property (nonatomic, copy)   NSString *jumpType;

// 成就任务才有
@property (nonatomic, copy) NSString *archieveName;
@end

/*
 //每日任务
DayLoopTaskType {
    sign  // 签到
    joinGuess //参与一次竞猜
}
 
 
 //新手任务
 NewbieTaskType {
bindGameUser //绑定召唤师
activeGameUser //激活召唤师
firstStakeMatchGuess //参与一次赛事竞猜
firstStakeChampionGuess //参与一次英雄猜
follow //关注一位好友
firstStakeTreasureLottery //参与一次夺宝
bindPhone  //绑定手机号
bindWechat  //绑定微信
bindQQ //绑定QQ
winTreasureLottery  // 夺宝成功
}
 
 
 //成就任务
 ArchieveTaskType {
winGuessContinue6 //连续猜中6次竞猜
recharge100 //充值超过100
win10MillionByGuess //竞猜收益1000万
invite5FriendActive //邀请5个好友认证（激活）召唤师
invite1FriendActive //邀请1个好友认证（激活）召唤师
activedPlay10 //激活召唤师后完成10场比赛
bindedWinContinue6  //绑定后连胜6场
playWith5Friend //与5位好友游戏开黑
playWithAtLeast1Friend //与至少1位好友游戏开黑
enemyHasUser //对手 也有平台用户
}

*/

