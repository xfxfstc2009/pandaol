//
//  PDTaskAward.h
//  PandaKing
//
//  Created by Cranz on 17/1/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 任务奖励
@interface PDTaskAward : FetchModel
@property (nonatomic, assign) NSUInteger gold;
@property (nonatomic, assign) NSUInteger bamboo;
@end
