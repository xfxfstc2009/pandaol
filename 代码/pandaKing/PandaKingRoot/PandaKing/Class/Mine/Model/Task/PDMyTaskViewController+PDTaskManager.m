//
//  PDMyTaskViewController+PDTaskManager.m
//  PandaKing
//
//  Created by Cranz on 17/1/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMyTaskViewController+PDTaskManager.h"
#import "PDLotteryGameMainViewController.h"
#import "PDBindingInfoViewController.h" // 绑定
#import "PDPandaRoleDetailViewController.h" // 角色详情
#import "PDLotteryHeroViewController.h" // 英雄猜
#import "PDFindRootNearbyViewController.h" // 附近的人
#import "PDCenterSettingThirdBindingViewController.h" // 账号绑定
#import "PDTopUpViewController.h" // 充值
#import "PDInviteViewController.h" // 邀请好友
#import "PDShopRootMainViewController.h"
#import "PDScuffleEnterViewController.h" // lol大乱斗
#import "PDChaosFightRootNavController.h"
#import "PDChaosFightRootViewController.h"

/*
 //每日任务
 DayLoopTaskType {
 sign  // 签到
 joinGuess //参与一次竞猜
 }
 
 
 //新手任务
 NewbieTaskType {
 bindGameUser //绑定召唤师
 activeGameUser //激活召唤师
 firstStakeMatchGuess //参与一次赛事竞猜
 firstStakeChampionGuess //参与一次英雄猜
 follow1User //关注一位好友
 bindPhone  //绑定手机号
 bindWechat  //绑定微信
 bindQQ //绑定QQ
 }
 
 
 //成就任务
 ArchieveTaskType {
 winGuessContinue3 //连续猜中3次
 winGuessContinue6 //连续猜中6次竞猜
 activedWinContinue3 //认证召唤师后连胜3场
 activedWinContinue6 //认证召唤师后连胜6场
 invitedUser1Actived //1位受邀请用户认证召唤师
 invitedUser3Actived //3位受邀请用户认证召唤师
 invitedUser6Actived //6位受邀请用户认证召唤师
 invitedUser8Actived //8位受邀请用户认证召唤师
 activedPlay1Match  //认证召唤师后完成1场游戏
 recharge100 //充值超过100
 win10MillionByGuess //竞猜收益1000万
 playWith4ActivatedUser //与4位平台用户游戏开黑
 playWithAtLeast1ActivatedUser //与至少1位平台用户游戏开黑
 enemyHasAtLeast1ActivatedUser //游戏内对面有至少1位认证用户
 }
 
 */

@implementation PDMyTaskViewController (PDTaskManager)

- (void)taskJumpManagerWithType:(NSString *)type {
    /**
     * 赛事竞猜
     */
    if ([type isEqualToString:@"matchGuess"]) {
        [self goGameLottery];
    }
    /**
     * 英雄擦
     */
    else if ([type isEqualToString:@"championGuess"]) {
        [self goHeroLottery];
    }
    /**
     * 绑定账号
     */
    else if ([type isEqualToString:@"bindAccount"]) {
        [self goBindAccount];
    }
    /**
     * 绑定召唤师
     */
    else if ([type isEqualToString:@"bindGameUser"]) {
        [self goBindHero];
    }
    /**
     * 激活召唤师
     */
    else if ([type isEqualToString:@"activateGameUser"]) {
        [self goRoleDetail];
    }
    /**
     * 打野大乱斗
     */
    else if ([type isEqualToString:@"jungleGuess"]) {
        [self goChaosFight];
    }
    /**
     * lol大乱斗
     */
    else if ([type isEqualToString:@"lolCardsGame"]) {
        [self goLolScuffle];
    }
    /**
     * 充值
     */
    else if ([type isEqualToString:@"recharge"]) {
        [self goTopup];
    }
    /**
     * 邀请
     */
    else if ([type isEqualToString:@"invite"]) {
        [self goInvite];
    }
    /**
     * 前往英雄联盟客户端完成
     */
    else if ([type isEqualToString:@"playLOLGame"]) {
        [self showLolGameAlert];
    }
    /**
     * 娱乐界面
     */
    else if ([type isEqualToString:@"toEntertainment"]) {
        [PDHUD showHUDSuccess:@"请到我的娱乐界面参与！"];
    }
    /**
     * 野区分享
     */
    else if ([type isEqualToString:@"sharePost"]) {
        [self goPostBar];
    }
}

/**
 * 野区
 */
- (void)goPostBar {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[PDMainTabbarViewController sharedController] setSelectedIndex:2 animated:YES];
}

/**
 * 绑定账号
 */
- (void)goBindAccount {
    PDCenterSettingThirdBindingViewController *thirdViewController = [[PDCenterSettingThirdBindingViewController alloc] init];
    [self pushViewController:thirdViewController animated:YES];
}

/**
 * 绑定召唤师
 */
- (void)goBindHero {
    PDBindingInfoViewController *bindViewController = [[PDBindingInfoViewController alloc] init];
    [self pushViewController:bindViewController animated:YES];
}

/**
 * 前往召唤师详情
 */
- (void)goRoleDetail {
    if (self.gameInfo.memberLOLGameUser.lolGameUser.gameUserId.length) {
        PDPandaRoleDetailViewController *roleDetailViewController = [[PDPandaRoleDetailViewController alloc] init];
        roleDetailViewController.transferLolGameUserId = self.gameInfo.memberLOLGameUser.lolGameUser.gameUserId;
        [self pushViewController:roleDetailViewController animated:YES];
    } else {
        [self goBindHero];
    }
}

/** 去赛事猜*/
- (void)goGameLottery {
    __weak typeof(self) weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        if (!weakSelf){
            return ;
        }
        PDLotteryGameMainViewController *gameViewController = [[PDLotteryGameMainViewController alloc] init];
        [weakSelf pushViewController:gameViewController animated:YES];
    }];
}

/** 去英雄猜*/
- (void)goHeroLottery {
    __weak typeof(self) weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        if (!weakSelf){
            return ;
        }
        PDLotteryHeroViewController *heroViewController = [[PDLotteryHeroViewController alloc] init];
        [weakSelf pushViewController:heroViewController animated:YES];
    }];
}

/** 参与夺宝*/
- (void)goSnatch {
    [self.navigationController popToRootViewControllerAnimated:YES];
    [[PDMainTabbarViewController sharedController] setSelectedIndex:1 animated:YES];
}

/** 参与邀请*/
- (void)goInvite {
    PDInviteViewController *inviteViewController = [[PDInviteViewController alloc] init];
    [self pushViewController:inviteViewController animated:YES];
}

/** 去充值*/
- (void)goTopup {
    PDTopUpViewController *topupViewController = [[PDTopUpViewController alloc] init];
    [self pushViewController:topupViewController animated:YES];
}

/** 提示去玩lol*/
- (void)showLolGameAlert {
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"温馨提示" conten:@"请前往英雄联盟客户端完成" isClose:NO btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissAllCompletion:nil];
    }];
}

/**
 * lol大乱斗
 */
- (void)goLolScuffle {
    __weak typeof(self) weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        if (!weakSelf) {
            return ;
        }
        
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]) {
            PDScuffleEnterViewController *enterViewController = [[PDScuffleEnterViewController alloc] init];
            [strongSelf pushViewController:enterViewController animated:YES];
        }
    }];
}

/**
 * 打野大乱斗入口
 */
- (void)goChaosFight {
    __weak typeof(self) weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            PDChaosFightRootViewController *chaosViewController = [[PDChaosFightRootViewController alloc]init];
            PDNavigationController *nav = [[PDNavigationController alloc]initWithRootViewController:chaosViewController];
            [strongSelf.navigationController presentViewController:nav animated:YES completion:NULL];
        }
    }];
}

@end
