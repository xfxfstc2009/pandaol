//
//  PDMyTaskViewController+PDTaskManager.h
//  PandaKing
//
//  Created by Cranz on 17/1/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMyTaskViewController.h"

@interface PDMyTaskViewController (PDTaskManager)

- (void)taskJumpManagerWithType:(NSString *)type;

@end
