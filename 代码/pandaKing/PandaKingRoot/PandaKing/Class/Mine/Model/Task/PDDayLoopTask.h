//
//  PDDayLoopTask.h
//  PandaKing
//
//  Created by Cranz on 17/1/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDTaskObject.h"

/// 每日任务
@interface PDDayLoopTask : FetchModel
@property (nonatomic, assign) NSUInteger doneCount;
@property (nonatomic, assign) NSUInteger totalCount;
@property (nonatomic, strong) NSArray <PDTaskObject>* items;
@end
