//
//  PDCenterGameInfo.h
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDCenterGameInfoState.h"
#import "PDCenterGameLolInfo.h"

/// 我的召唤师详情 model
@interface PDCenterGameInfo : FetchModel
@property (nonatomic, strong) PDCenterGameInfoState *stats;
@property (nonatomic, strong) PDCenterGameLolInfo *memberLOLGameUser;
@end
