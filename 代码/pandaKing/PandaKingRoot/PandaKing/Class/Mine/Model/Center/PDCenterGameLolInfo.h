//
//  PDCenterGameLolInfo.h
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDCenterGameLolUserInfo.h"

@interface PDCenterGameLolInfo : FetchModel
@property (nonatomic,copy)  NSString *avatar;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *gameId;
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, assign) BOOL enabled;
@property (nonatomic, strong) PDCenterGameLolUserInfo *lolGameUser;

@property (nonatomic,copy)NSString *state;
@end
