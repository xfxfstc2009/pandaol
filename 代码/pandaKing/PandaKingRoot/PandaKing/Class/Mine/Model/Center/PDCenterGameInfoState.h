//
//  PDCenterGameInfoState.h
//  PandaKing
//
//  Created by on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDCenterGameInfoState : FetchModel
@property (nonatomic, assign) NSInteger gameCount;
@property (nonatomic, assign) NSInteger winGameCount;
@property (nonatomic, assign) NSInteger loseGameCount;
@property (nonatomic, assign) CGFloat winRate;/**< 胜率*/
@property (nonatomic, assign) NSInteger kill;
@property (nonatomic, assign) NSInteger death;
@property (nonatomic, copy)  NSString *assists;
@property (nonatomic, copy)  NSString *kda;
@property (nonatomic, assign) NSInteger threeKill;
@property (nonatomic, assign) NSInteger fourKill;
@property (nonatomic, assign) NSInteger fiveKill;
@property (nonatomic, assign) NSInteger superKill;
@end
