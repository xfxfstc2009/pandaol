//
//  PDCenterGameLolUserInfo.h
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// lolGameUser
@interface PDCenterGameLolUserInfo : FetchModel
@property (nonatomic, copy) NSString *gameUserId;
@property (nonatomic, copy) NSString *roleName;
@property (nonatomic, copy) NSString *serverName;   /**< 艾欧尼亚*/
@property (nonatomic, copy) NSString *telecomLine;  /**< 电信*/
@property (nonatomic, copy) NSString *gameServerId; /**< “1”*/
@property (nonatomic, assign) NSInteger grade;      /**< lol召唤师段位(0-6|255)如果=255，不用显示gradeLevel*/
@property (nonatomic, assign) NSInteger gradeLevel;
@property (nonatomic, assign) NSInteger fightingCapacity;
@property (nonatomic, copy) NSString *avatar;
@end
