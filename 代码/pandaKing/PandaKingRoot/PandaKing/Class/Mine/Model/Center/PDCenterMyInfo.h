//
//  PDCenterMyInfo.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPersonMemberSingleModel.h"

@interface PDCenterMyInfo : FetchModel
@property (nonatomic, assign) NSInteger gold;
@property (nonatomic, assign) NSInteger bamboo;
@property (nonatomic,assign)NSInteger fans;
@property (nonatomic,assign)NSInteger follow;
@property (nonatomic,assign)NSTimeInterval latestStakeTime;
@property (nonatomic,strong)PDPersonMemberSingleModel *member;

@end
