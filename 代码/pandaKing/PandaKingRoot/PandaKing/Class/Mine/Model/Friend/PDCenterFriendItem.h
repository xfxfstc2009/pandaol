//
//  PDCenterFriendItem.h
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDCenterFriendInfo.h"

@protocol PDCenterFriendItem <NSObject>

@end
@interface PDCenterFriendItem : FetchModel
@property (nonatomic, strong) PDCenterFriendInfo *friendInfo;
@property (nonatomic, assign) BOOL followed;
@property (nonatomic, assign) BOOL fans;
@end
