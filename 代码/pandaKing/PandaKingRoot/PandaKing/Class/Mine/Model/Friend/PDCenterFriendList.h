//
//  PDCenterFriendList.h
//  PandaKing
//
//  Created by Cranz on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDCenterFriendItem.h"

/// 我的撸友列表
@interface PDCenterFriendList : FetchModel
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, assign) NSInteger totalItemsCount;
@property (nonatomic, strong) NSArray <PDCenterFriendItem>*items;
@property (nonatomic, assign) BOOL hasPreviousPage;
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, assign) NSInteger pageCount;
@end
