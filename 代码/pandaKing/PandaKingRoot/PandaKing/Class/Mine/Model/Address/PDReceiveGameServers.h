//
//  PDReceiveGameServers.h
//  PandaKing
//
//  Created by Cranz on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDReceiveGameServerModel.h"

@interface PDReceiveGameServers : FetchModel
@property (nonatomic, strong) NSArray <PDReceiveGameServerModel> *gameservers;
@end
