//
//  PDReceiveGameServerModel.h
//  PandaKing
//
//  Created by Cranz on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"


@protocol PDReceiveGameServerModel <NSObject>

@end
@interface PDReceiveGameServerModel : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *telecomLine;
@end
