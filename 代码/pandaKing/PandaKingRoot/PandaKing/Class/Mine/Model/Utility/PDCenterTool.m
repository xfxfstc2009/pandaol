//
//  PDCenterTool.m
//  PandaKing
//
//  Created by Cranz on 16/7/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCenterTool.h"

@implementation PDCenterTool
+ (void)calculateCell:(UITableViewCell *)cell leftSpace:(CGFloat)left rightSpace:(CGFloat)right {
    cell.separatorInset = UIEdgeInsetsMake(0, left, 0, right);
    cell.layoutMargins = UIEdgeInsetsMake(0, left, 0, right);
    cell.preservesSuperviewLayoutMargins = NO;
}

+ (void)tableViewScrolledRemoveViscousWithScrollView:(UIScrollView *)scrollView sectionHeight:(CGFloat)sectionHeight {
    [self tableViewScrolledRemoveViscousCancleWithScrollView:scrollView sectionHeight:sectionHeight];
    
    if (scrollView.dragging == NO && scrollView.contentInset.top != 0) {
        [UIView animateWithDuration:.3 animations:^{
            scrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0);
        }];
    }
}

+ (void)tableViewScrolledRemoveViscousCancleWithScrollView:(UIScrollView *)scrollView sectionHeight:(CGFloat)sectionHeight {
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY <= sectionHeight && offsetY >= 0) {
        scrollView.contentInset = UIEdgeInsetsMake(- offsetY, 0, 0, 0);
    } else if (offsetY >= sectionHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(- sectionHeight, 0, 0, 0);
    }
}

+ (NSString *)dateFromTimestamp:(NSTimeInterval)stamp {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:stamp / 1000];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy.MM.dd HH:mm:ss";
    return [formatter stringFromDate:date];
}

+ (NSString *)dateWithFormat:(NSString *)dateFormat fromTimestamp:(NSTimeInterval)stamp {
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:stamp / 1000];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = dateFormat;
    return [formatter stringFromDate:date];
}


+ (NSString *)absoluteUrlWithRisqueUrl:(NSString *)url {
    if ([url hasPrefix:@"http"]) {
        return url;
    }
    if (JAVA_Port.length == 0) {
        return [NSString stringWithFormat:@"http://%@%@",JAVA_Host, url];
    } else {
        if ([url hasPrefix:@"/"]) {
          return [NSString stringWithFormat:@"http://%@:%@%@",JAVA_Host,JAVA_Port,url];
        } else {
            return [NSString stringWithFormat:@"http://%@:%@/%@",JAVA_Host,JAVA_Port,url];
        }
    }
}

+ (NSString *)absoluteUrlWithPostUrl:(NSString *)url {
    if ([url hasPrefix:@"http"]) {
        return url;
    }
    if (Jungle_Port.length == 0) {
        return [NSString stringWithFormat:@"http://%@%@",Jungle_Host, url];
    } else {
        return [NSString stringWithFormat:@"http://%@:%@%@",Jungle_Host,Jungle_Port,url];
    }
}

+ (NSString *)lotteryItemWithTag:(NSString *)tag atIndex:(int)i {
    if ([tag isEqualToString:@"goldPrice"]) {
        if (i == 1) {
            return @"3150及以下";
        } else if (i == 2) {
             return @"4800";
        } else {
             return @"6300及以上";
        }
    } else if ([tag isEqualToString:@"nickLength"]) {
        if (i == 1) {
             return @"两个及以下";
        } else if (i == 2) {
             return @"三个";
        } else {
             return @"四个及以上";
        }
    } else if ([tag isEqualToString:@"camp"]) {
        if (i == 1) {
            return @"天辉";
        } else {
            return @"夜魇";
        }
    } else if ([tag isEqualToString:@"nameLength"]) {
        if (i == 1) {
            return @"两个及以下";
        } else if (i == 2) {
            return @"三个";
        } else {
            return @"四个及以上";
        }
    } else if ([tag isEqualToString:@"far"]) {
        if (i == 1) {
            return @"远程";
        } else {
            return @"近战";
        }
    } else if ([tag isEqualToString:@"mainProperty"]) {
        if (i == 1) {
            return @"力量";
        } else if (i == 2) {
            return @"敏捷";
        } else {
            return @"智力";
        }
    } else {
        return nil;
    }
}

+ (NSString *)separateStringWithString:(NSString *)targetString {
    if (targetString.length > 3) {
        NSString *newBalance = [targetString stringByReplacingOccurrencesOfString:@"," withString:@""];
        NSNumberFormatter *formatter = [NSNumberFormatter new];
        [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSString *formattedBalance = [formatter stringFromNumber:[NSNumber numberWithInteger:[newBalance integerValue]]];
        return formattedBalance;
    } else {
        return targetString;
    }
}

+ (NSString *)numberStringChange:(NSInteger)number {
    if (number < 10000) {
        NSString *n = [NSString stringWithFormat:@"%ld", (long)number];
        return [self separateStringWithString:n];
    }
    
    return [NSString stringWithFormat:@"%.1f万", number / 10000.];
}

+ (NSString *)numberStringChangeWithoutSpecialCharacter:(NSInteger)number {
    if (number < 10000) {
        NSString *n = [NSString stringWithFormat:@"%ld", (long)number];
        return n;
    }
    
    return [NSString stringWithFormat:@"%.1f万", number / 10000.];
}

/**< 兑换券类型(phoneMoney|realItem|lolSkin|qCoin)*/
+ (NSString *)nameForCategory:(NSString *)category {
    if ([category isEqualToString:@"qCoin"]) {
        return @"[Q币]";
    } else if ([category isEqualToString:@"phoneMoney"]) {
        return @"[话费]";
    } else if ([category isEqualToString:@"realItem"]) {
        return @"[实物]";
    } else if ([category isEqualToString:@"lolSkin"]) {
        return @"[皮肤]";
    }
    return @"";
}

+ (void)setButtonBackgroundEffect:(UIButton *)button {
    [self setButtonBackgroundEffect:button effectColor:[UIColor lightGrayColor]];
}

+ (void)setButtonBackgroundEffect:(UIButton *)button effectColor:(UIColor *)color {
    [button setBackgroundImage:[UIImage imageWithRenderColor:color renderSize:button.frame.size] forState:UIControlStateHighlighted];
}

+ (void)saveUserInfoTExt:(NSDictionary **)ext {
    NSString *avatar = [Tool userDefaultGetWithKey:CustomerAvatar];
    if (avatar.length == 0) {
        avatar = @"icon_nordata"; // 占位头像
    }
    
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithDictionary:*ext];
    if ([Tool userDefaultGetWithKey:CustomerNickname].length) {
        [dic setValue:[Tool userDefaultGetWithKey:CustomerNickname] forKey:@"nickname"];
    } else {
        [dic setValue:[Tool userDefaultGetWithKey:CustomerMemberId] forKey:@"nickname"];
    }
    
    [dic setValue:avatar forKey:@"avatar"];
    *ext = [dic copy];
}

+ (void)firstShowGuide {
    if ([[NSUserDefaults standardUserDefaults] valueForKey:PDGUIDE_STATE] == nil || ![[[NSUserDefaults standardUserDefaults] valueForKey:PDGUIDE_STATE] isEqualToString:@"yes"]) {
        [[PDAlertView sharedAlertView] showScrollGuideViewWithDismissComplication:^{
            [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:PDGUIDE_STATE];
        }];
    }
}

+(void)firstShowGuideWithAmusement:(void(^)())block{
    if ([[NSUserDefaults standardUserDefaults] valueForKey:PDGUIDE_STATE] == nil || ![[[NSUserDefaults standardUserDefaults] valueForKey:PDGUIDE_STATE] isEqualToString:@"yes"]) {
        [[PDAlertView sharedAlertView] showScrollGuideViewWithDismissComplication:^{
            [[NSUserDefaults standardUserDefaults] setValue:@"yes" forKey:PDGUIDE_STATE];
            if (block){
                block();
            }
        }];
    }
}

+ (NSArray *)separateBgImgae:(UIImage *)bgImage {
    // 等比放大
    UIImage *newImage;
    CGSize imageSize = CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.height);
    UIGraphicsBeginImageContext(imageSize);
    [bgImage drawInRect:CGRectMake(0, 0, imageSize.width, imageSize.height)];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // 加高斯模糊
    UIImage *dimImage = [newImage boxblurImageWithBlur:0.6];
    
    // 上下分割图
    CGImageRef imageUpRef = CGImageCreateWithImageInRect(dimImage.CGImage, CGRectMake(0, 0, dimImage.size.width, dimImage.size.height / 2));
    CGImageRef imageDownRef = CGImageCreateWithImageInRect(dimImage.CGImage, CGRectMake(0, dimImage.size.height / 2, dimImage.size.width, newImage.size.height / 2));
    return @[[UIImage imageWithCGImage:imageUpRef], [UIImage imageWithCGImage:imageDownRef]];
}

+ (NSString *)jsonFromObjc:(id)dic {
    NSString *json = @"";
    NSData *data = [NSJSONSerialization dataWithJSONObject:dic options:NSJSONWritingPrettyPrinted error:nil];
    json = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    
    // 去掉空格和\n
    NSMutableString *mutStr = [NSMutableString stringWithString:json];
    NSRange range = {0, json.length};
    [mutStr replaceOccurrencesOfString:@" "
                            withString:@""
                               options:NSLiteralSearch range:range];
    NSRange range2 = {0, mutStr.length};
    [mutStr replaceOccurrencesOfString:@"\n"
                            withString:@""
                               options:NSLiteralSearch range:range2];
    return [mutStr copy];
}

+ (NSData *)compressImage:(UIImage *)image {
    UIImage *scaleImage = [UIImage scaleDown:image withSize:CGSizeMake(50, 50)];
    NSData * data = UIImageJPEGRepresentation(scaleImage, 1.0);
    if (data.length/1024 > 32) {
        data = UIImageJPEGRepresentation(scaleImage, 0.75);
    }
    return data;
}

+ (CGSize)sizeWithImageUrl:(NSString *)url {
    NSArray *sizeArr = [url componentsSeparatedByString:@"x"];
    if (sizeArr.count <= 2) {
        return CGSizeZero;
    }
    CGFloat width = [sizeArr[1] floatValue];
    CGFloat height = [sizeArr[2] floatValue];
    return CGSizeMake(width, height);
}

+ (UITableView *)tableViewWithSuperView:(UIView *)view {
    return [self tableViewWithSuperView:view style:UITableViewStyleGrouped];
}
+ (UITableView *)tableViewWithSuperView:(UIView *)view style:(UITableViewStyle)style {
    UITableView *tableView = [[UITableView alloc] initWithFrame:CGRectZero style:style];
    tableView.translatesAutoresizingMaskIntoConstraints = NO;
    tableView.showsVerticalScrollIndicator = NO;
    tableView.showsHorizontalScrollIndicator = NO;
    tableView.separatorColor = c6;
    [view addSubview:tableView];
    
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeTop multiplier:1.0 constant:0.0];
    NSLayoutConstraint *left = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0.0];
    NSLayoutConstraint *bottom = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeBottom multiplier:1.0 constant:0.0];
    NSLayoutConstraint *right = [NSLayoutConstraint constraintWithItem:tableView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:view attribute:NSLayoutAttributeRight multiplier:1.0 constant:0.0];
    [view addConstraints:@[top, left, bottom, right]];
    
    return tableView;
}

+ (NSString *)formatPhoneNumber:(NSString *)number {
    if (!number.length) return number;
    NSMutableString *mutString = [number mutableCopy];
    [mutString insertString:@" " atIndex:3];
    [mutString insertString:@" " atIndex:8];
    return [mutString copy];
}

+ (NSString *)formatChar4WithNumber:(NSString *)number {
    if (!number.length) return number;
    NSMutableString *numberTemp = [[self formatPhoneNumber:number] mutableCopy];
    [numberTemp replaceCharactersInRange:NSMakeRange(4, 4) withString:@"****"];
    return [numberTemp copy];
}

+ (void)button:(UIButton *)button enable:(BOOL)b {
    button.enabled = b;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    if (b == YES) {
        button.backgroundColor = [UIColor blackColor];
    } else {
        button.backgroundColor = [UIColor lightGrayColor];
    }
}

+ (UIAlertController *)showAlertWithTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray *)titles actions:(void (^)(NSInteger))actions {
    UIAlertController *alter = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    for (int i = 0; i < titles.count; ++i) {
        UIAlertAction *action = [UIAlertAction actionWithTitle:titles[i] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            !actions ?: actions(i);
        }];
        [alter addAction:action];
    }
    return alter;
}

@end
