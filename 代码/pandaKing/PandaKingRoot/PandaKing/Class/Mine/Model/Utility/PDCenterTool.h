//
//  PDCenterTool.h
//  PandaKing
//
//  Created by Cranz on 16/7/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDCenterTool : NSObject

#pragma mark - 单元格的分割线
+ (void)calculateCell:(UITableViewCell *)cell leftSpace:(CGFloat)left rightSpace:(CGFloat)right;

#pragma mark - scroll
/** 能够回滚*/
+ (void)tableViewScrolledRemoveViscousWithScrollView:(UIScrollView *)scrollView sectionHeight:(CGFloat)sectionHeight;
/** 没有回滚动画*/
+ (void)tableViewScrolledRemoveViscousCancleWithScrollView:(UIScrollView *)scrollView sectionHeight:(CGFloat)sectionHeight;

#pragma mark - 日期处理
/** 完整*/
+ (NSString *)dateFromTimestamp:(NSTimeInterval)stamp;
+ (NSString *)dateWithFormat:(NSString *)dateFormat fromTimestamp:(NSTimeInterval)stamp;

#pragma mark - 绝对路径的拼接
/** 相对路径变绝对路径*/
+ (NSString *)absoluteUrlWithRisqueUrl:(NSString *)url;
/**
 * 野区相关使用
 */
+ (NSString *)absoluteUrlWithPostUrl:(NSString *)url;

#pragma mark - 英雄猜竞猜分类名称
/** 这个是竞猜项每一项的信息*/
+ (NSString *)lotteryItemWithTag:(NSString *)tag atIndex:(int)i;

#pragma mark - 数字处理
/** 将数字分割成`,`格式 :1,000*/
+ (NSString *)separateStringWithString:(NSString *)targetString;
/** 小于1万的显示具体，并且是三位分隔；大于1万的精确到小数点后一位*/
+ (NSString *)numberStringChange:(NSInteger)number;
/** 不显示','与方法二类似*/
+ (NSString *)numberStringChangeWithoutSpecialCharacter:(NSInteger)number;

#pragma mark - 商品分类名称
/** 商品或者是活动的category的具体中文名｀qCoin｀ － Q币*/
+ (NSString *)nameForCategory:(NSString *)category;

#pragma mark - 按钮设置点击效果
+ (void)setButtonBackgroundEffect:(UIButton *)button;
+ (void)setButtonBackgroundEffect:(UIButton *)button effectColor:(UIColor *)color;

#pragma mark - 存头像和昵称,用于环信
+ (void)saveUserInfoTExt:(NSDictionary **)ext;

#pragma mark - 第一次显示引导弹框
+ (void)firstShowGuide;

#pragma mark - 上下分割一张图片－LOL大乱斗
+ (NSArray *)separateBgImgae:(UIImage *)bgImage;

#pragma mark - 字典转json字符串
+ (NSString *)jsonFromObjc:(id)dic;
#pragma mark - 用在分享的时候 将图片压缩到不可知的大小，微信分享图片大小不得超过32kb
+ (NSData *)compressImage:(UIImage *)image;

#pragma mark - 从图片路径中获取图片的size
+ (CGSize)sizeWithImageUrl:(NSString *)url;

+(void)firstShowGuideWithAmusement:(void(^)())block;

#pragma mark - 创建一个有约束的tableView
+ (UITableView *)tableViewWithSuperView:(UIView *)view;
+ (UITableView *)tableViewWithSuperView:(UIView *)view style:(UITableViewStyle)style;

/**
 * 格式化手机号
 */
+ (NSString *)formatPhoneNumber:(NSString *)number;

+ (NSString *)formatChar4WithNumber:(NSString *)number;

+ (void)button:(UIButton *)button enable:(BOOL)b;

+ (UIAlertController *)showAlertWithTitle:(NSString *)title message:(NSString *)message buttonTitles:(NSArray *)titles actions:(void(^)(NSInteger index))actions;

@end
