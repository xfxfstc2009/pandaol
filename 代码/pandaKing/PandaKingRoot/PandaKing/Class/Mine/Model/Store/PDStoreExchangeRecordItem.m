//
//  PDStoreExchangeRecordItem.m
//  PandaKing
//
//  Created by Cranz on 16/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDStoreExchangeRecordItem.h"

@implementation PDStoreExchangeRecordItem
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"ID":@"id"};
}
@end
