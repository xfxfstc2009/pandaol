//
//  PDStoreActivityList.h
//  PandaKing
//
//  Created by Cranz on 16/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDStoreActivityItem.h"

@interface PDStoreActivityList : FetchModel
@property (nonatomic, strong) NSArray <PDStoreActivityItem> *items;
@property (nonatomic, assign) BOOL hasNextPage;
@end
