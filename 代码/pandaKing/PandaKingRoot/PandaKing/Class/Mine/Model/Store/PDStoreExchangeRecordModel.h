//
//  PDStoreExchangeRecordModel.h
//  PandaKing
//
//  Created by Cranz on 16/11/23.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDStoreExchangeRecordList.h"

@interface PDStoreExchangeRecordModel : FetchModel
@property (nonatomic, strong) PDStoreExchangeRecordList *listPage;
@end
