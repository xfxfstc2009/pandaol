//
//  PDStoreActivityItem.h
//  PandaKing
//
//  Created by Cranz on 16/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDStoreActivityCommodity.h"

@protocol PDStoreActivityItem <NSObject>

@end

@interface PDStoreActivityItem : FetchModel
@property (nonatomic, copy) NSString *ID; //兑换活动id
@property (nonatomic, strong) PDStoreActivityCommodity *commodity;
@property (nonatomic, assign) CGFloat originalPrice;
@property (nonatomic, assign) CGFloat discountPrice;
@property (nonatomic, assign) NSInteger amount; // 库存
/**
 * 是否限购
 */
@property (nonatomic, assign) BOOL limit;
/**
 * 限购的数量
 */
@property (nonatomic, assign) NSUInteger limitAmount;
@property (nonatomic, assign) NSInteger salesVolume; // 销量
@property (nonatomic, assign) NSInteger publishTime; // 发布时间
@property (nonatomic, assign) NSInteger startTime;
@property (nonatomic, assign) NSInteger endTime;
/*
 if(state!=going)
 不在销售时间
 else if（amount<=0）
 库存不足
 
 * 未开始
 
 notStarted,
 
 * 进行中
 
 going,
 
 * 完成
 
 finished;
 */
@property (nonatomic, copy) NSString *state;
@property (nonatomic, assign) BOOL shelf;
@end
