//
//  PDStoreExchangeRecordItem.h
//  PandaKing
//
//  Created by Cranz on 16/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMemberInfoModel.h"

@protocol PDStoreExchangeRecordItem <NSObject>

@end

@interface PDStoreExchangeRecordItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, strong) PDMemberInfoModel *member;
@property (nonatomic, copy) NSString *bamRedeemActId; // 兑换id
@property (nonatomic, assign) NSInteger quantity; // 数量
@property (nonatomic, assign) NSTimeInterval redeemTime; // 兑换时间
@end
