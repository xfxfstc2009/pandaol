//
//  PDStoreActivityModel.h
//  PandaKing
//
//  Created by Cranz on 16/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDStoreActivityList.h"

@interface PDStoreActivityModel : FetchModel
@property (nonatomic, assign) NSInteger balance; //竹子余额
@property (nonatomic, strong) PDStoreActivityList *listPage;
@end
