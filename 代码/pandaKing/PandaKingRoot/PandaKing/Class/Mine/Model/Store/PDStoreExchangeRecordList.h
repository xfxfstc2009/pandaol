//
//  PDStoreExchangeRecordList.h
//  PandaKing
//
//  Created by Cranz on 16/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDStoreExchangeRecordItem.h"

@interface PDStoreExchangeRecordList : FetchModel
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, strong) NSArray <PDStoreExchangeRecordItem> *items;
@end
