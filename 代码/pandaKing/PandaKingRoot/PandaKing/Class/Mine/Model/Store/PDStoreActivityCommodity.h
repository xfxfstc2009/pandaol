//
//  PDStoreActivityCommodity.h
//  PandaKing
//
//  Created by Cranz on 16/11/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"


/*
 commodity":{
 "id":"2",
 "name":"50元Q币",
 "coverPic":"/po/upload/image/20161017/1476710983644005609.jpg",
 "pictures":[
 "/po/upload/image/20161017/1476710983644005609.jpg"
 ],
 "detail":"<p>常规夺宝奖品，每天持续不间断开奖，欢迎各位参与夺宝</p>",
 "value":50,
 "exchangeCategory":"qCoin",
 "createTime":1476711021350,
 "lastModifyTime":1476711021350,
 "down":false
 },
 */

@interface PDStoreActivityCommodity : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *coverPic;
@property (nonatomic, strong) NSArray *pictures;
@property (nonatomic, copy) NSString *detail;
@property (nonatomic, assign) NSInteger value;
@property (nonatomic, copy) NSString *exchangeCategory;
@property (nonatomic, assign) NSTimeInterval createTime;
@property (nonatomic, assign) NSTimeInterval lastModifyTime;
@property (nonatomic, assign) BOOL down;
@end
