//
//  PDSnatchItem.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDSnatchItem <NSObject>

@end

@interface PDSnatchItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, assign) NSInteger totalGold;  //所有会员总投入
@property (nonatomic, assign) NSInteger totalStake;  //当前会员总投入 | 中奖中是总金币数
@property (nonatomic, assign) NSTimeInterval drawTime;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, copy) NSString *phase;
@property (nonatomic, copy) NSString *odds;
@property (nonatomic, copy) NSString *winerId;
@property (nonatomic, copy) NSString *winerNickName;
@property (nonatomic, assign) BOOL isWin;   /**< 是否是中奖者*/

// 新加
@property (nonatomic, assign) NSInteger wagers; // 总投入的竹子数
@property (nonatomic, assign) NSInteger portions; // 份数

/**
 * 帮忙的份数
 */
@property (nonatomic, assign) NSUInteger helpPortions;
/**
 * 帮忙的竹子数
 */
@property (nonatomic, assign) NSUInteger helpWagers;
@end
