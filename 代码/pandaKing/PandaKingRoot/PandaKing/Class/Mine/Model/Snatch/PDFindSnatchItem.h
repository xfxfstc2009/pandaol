//
//  PDFindSnatchItem.h
//  PandaKing
//
//  Created by Cranz on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"


@protocol PDFindSnatchItem <NSObject>

@end
@interface PDFindSnatchItem : FetchModel
@property (nonatomic, copy) NSString *ID;   /**< 活动ID*/
@property (nonatomic, copy) NSString *goldCount;  /**< 当前会员总投入*/
@property (nonatomic, copy) NSString *totalGold;   /**< 所有会员总投入*/
@property (nonatomic, assign) NSTimeInterval drawTime;  /**< 开奖时间*/
@property (nonatomic, copy) NSString *name;    /**< 商品名*/
@property (nonatomic, copy) NSString *image;
@property (nonatomic, assign) BOOL isWin;
@property (nonatomic, copy) NSString *phase;  /**< 第几期*/
@end
