//
//  PDBackpackHadModel.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDBackpackItemModel.h"

/// 背包：已拥有
@interface PDBackpackHadModel : FetchModel
@property (nonatomic, assign) NSInteger totalItemsCount;
@property (nonatomic, assign) NSInteger pageItemsCount;
@property (nonatomic, strong) NSArray <PDBackpackItemModel>* items;
@property (nonatomic, assign) BOOL hasPreviousPage;
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, assign) NSInteger pageCount;
@end
