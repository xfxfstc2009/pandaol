//
//  PDBackpackItemModel.m
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBackpackItemModel.h"

@implementation PDBackpackItemModel
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"ID": @"id"};
}
@end
