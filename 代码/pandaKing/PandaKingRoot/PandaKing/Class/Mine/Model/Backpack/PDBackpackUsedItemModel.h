//
//  PDBackpackUsedItemModel.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDBackpackUsedItemModel <NSObject>

@end
@interface PDBackpackUsedItemModel : FetchModel
@property (nonatomic, copy) NSString *ID;   /**< 订单ID*/
@property (nonatomic, assign) NSInteger commodityId;    /**< 商品ID*/
@property (nonatomic, assign) NSInteger amount;
@property (nonatomic, copy) NSString *exchangeCategory;
@property (nonatomic, copy) NSString *commodityName;
@property (nonatomic, copy) NSString *commodityPic;
@property (nonatomic, copy) NSString *commodityTotalValue;  /**< 价值 可能是小数*/
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, assign) NSTimeInterval createTime;
@property (nonatomic, copy) NSString *sourceType;
@property (nonatomic, copy) NSString *sourceId;
@property (nonatomic, copy) NSString *doneTime;
@property (nonatomic, copy) NSString *state;    /**< NEEDDEAL | DONE*/
@property (nonatomic, copy) NSString *cellphone;
@property (nonatomic, copy) NSString *remark;   /**< 备注*/
@property (nonatomic, copy) NSString *qq;
@property (nonatomic, copy) NSString *gameServerName;
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *receiver;
@property (nonatomic, copy) NSString *telephone;
@end
