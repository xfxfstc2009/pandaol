//
//  PDBackpackUsedModel.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDBackpackUsedItemModel.h"

@interface PDBackpackUsedModel : FetchModel
@property (nonatomic, assign) NSInteger totalItemsCount;
@property (nonatomic, strong) NSArray <PDBackpackUsedItemModel> *items;
@property (nonatomic, assign) BOOL hasNextPage;
@end
