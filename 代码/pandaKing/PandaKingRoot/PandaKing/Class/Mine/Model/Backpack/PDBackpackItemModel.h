//
//  PDBackpackItemModel.h
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDBackpackItemModel <NSObject>

@end

@interface PDBackpackItemModel : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *commodityName;
@property (nonatomic, copy) NSString *exchangeCategory; /**< 兑换券类型(phoneMoney|realItem|lolSkin|qCoin)*/
@property (nonatomic, copy) NSString *exchangeCategoryName; /**< 兑换券类型显示（话费|实物|...）*/
@property (nonatomic, copy) NSString *commodityValue; /**< 兑换券商品价值*/
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, assign) NSTimeInterval createTime;
@end
