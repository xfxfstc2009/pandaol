//
//  PDBackpackUsedItemModel.m
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDBackpackUsedItemModel.h"

@implementation PDBackpackUsedItemModel
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"ID": @"id"};
}
@end
