//
//  PDInviteList.h
//  PandaKing
//
//  Created by Cranz on 16/12/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDInviteItem.h"

@interface PDInviteList : FetchModel
/**
 * 总收入
 */
@property (nonatomic, assign) NSUInteger totalCount;
/**
 * 提成收益
 */
@property (nonatomic, assign) NSUInteger consumptionDrawCount;
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, strong) NSArray <PDInviteItem> *items;
@end
