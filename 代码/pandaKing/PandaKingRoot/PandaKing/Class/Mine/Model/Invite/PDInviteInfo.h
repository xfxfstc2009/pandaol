//
//  PDInviteInfo.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDInviteDetail.h"

@interface PDInviteInfo : FetchModel
@property (nonatomic, copy) NSString *memberId; //我的推广码 |会员id
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, assign) NSInteger invitedCount;  /**< 我邀请的人数*/
@property (nonatomic, assign) NSInteger activateGameUserCount; /**< 已认证xx人*/
@property (nonatomic, assign) NSInteger activateGameUserAward; /**< 好友绑定并认证召唤师，更有xx竹子大奖*/
@property (nonatomic, assign) BOOL hasInvited; //是否已填写true|false
/**
 * 邀请详情描述
 */
@property (nonatomic, copy) NSString *showContent;
@property (nonatomic, assign) NSInteger invitedAward;  /**< 每成功邀请1人，可得xx金币*/
@property (nonatomic, assign) NSInteger totalGoldAward; /**< 共获得xx金币*/
@property (nonatomic, assign) NSInteger totalBambooAward; /**< 共获得xx竹子*/
@property (nonatomic, strong) PDInviteDetail *config;
/**
 * 抽成比例
 */
@property (nonatomic) CGFloat commissionRate;
@end
