//
//  PDInviteItem.h
//  PandaKing
//
//  Created by Cranz on 16/12/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/*
 "id":"222daf08-0be5-329f-9838-51e9a4bc7293",
 "accountId":"62b54efb-9f3d-397e-97af-8cc9b3b4dbc1",
 "memberId":"68973n",
 "deltaFee":1000,//增减金币/竹子数量
 "balance":1000,
 "logTime":1481082802377,//流水时间
 "reason":"通过【yaa】邀请",//流水原因
 "source":"invited"//流水类型
 
 
 invited 被邀请，加金币
 invite  邀请他人，加金币
 firstActivateAfterInvitedTask 被邀请的会员第一次认证召唤师，加竹子
 */

@protocol PDInviteItem <NSObject>

@end
@interface PDInviteItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *accountId;
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, assign) NSInteger deltaFee;
@property (nonatomic, assign) NSInteger balance;
@property (nonatomic, assign) NSTimeInterval logTime;
@property (nonatomic, copy) NSString *reason;
@property (nonatomic, copy) NSString *source;
@end
