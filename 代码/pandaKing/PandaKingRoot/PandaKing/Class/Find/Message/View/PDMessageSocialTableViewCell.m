//
//  PDMessageSocialTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageSocialTableViewCell.h"
#import "UILabel+PDCategory.h"

#define kTop LCFloat(10.)
#define kSpaceName_label LCFloat(10.)  // 名字到content的距离
#define kHeaderWidth LCFloat(50.)
#define KNameFont 15
#define kContentFont 13
#define kSpace_label LCFloat(8) /**< 控件之间的水平距离*/

@interface PDMessageSocialTableViewCell ()
@property (nonatomic, strong) PDImageView *headerImaegView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, strong) UILabel *dateLabel;
@end

@implementation PDMessageSocialTableViewCell

+ (CGFloat)cellHeightWithModel:(PDMessageSocialItem *)model {
    CGSize nameSize = [model.sourceMember.nickname sizeWithCalcFont:[UIFont systemFontOfCustomeSize:KNameFont] constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - kHeaderWidth - kSpace_label * 2 - [@"yyyy-MM-dd" sizeWithCalcFont:[UIFont systemFontOfCustomeSize:kContentFont]].width, [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:KNameFont]])];
    CGSize contentSize = [model.textContent sizeWithCalcFont:[UIFont systemFontOfCustomeSize:kContentFont] constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - kHeaderWidth - kSpace_label * 2, CGFLOAT_MAX)];
    
    CGFloat h = kTop * 2 + nameSize.height + contentSize.height + kSpaceName_label;
    if (h <= kTop * 2 + kHeaderWidth) {
        return kTop * 2 + kHeaderWidth;
    } else {
        return h;
    }
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)pageSetting {
    // 头像
    self.headerImaegView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, kTop, kHeaderWidth, kHeaderWidth)];
    self.headerImaegView.layer.cornerRadius = kHeaderWidth / 2;
    self.headerImaegView.clipsToBounds = YES;
    self.headerImaegView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.contentView addSubview:self.headerImaegView];
    
    // 名字
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:KNameFont];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    // 内容
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.font = [UIFont systemFontOfCustomeSize:kContentFont];
    self.contentLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.contentLabel];
    
    // 时间
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = self.contentLabel.font;
    self.dateLabel.textColor = self.contentLabel.textColor;
    [self.contentView addSubview:self.dateLabel];
}

#pragma mark - 控件

- (void)setModel:(PDMessageSocialItem *)model {
    _model = model;
    
    [self setReaded:model.read];
    self.nameLabel.text = model.sourceMember.nickname;
    self.contentLabel.text = model.textContent;
    self.dateLabel.text = [PDCenterTool dateWithFormat:@"yyyy-MM-dd" fromTimestamp:model.msgTime];
    [self.headerImaegView uploadImageWithAvatarURL:model.sourceMember.avatar placeholder:nil callback:NULL];
    
    self.contentLabel.constraintSize = CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - kHeaderWidth - kSpace_label * 2, CGFLOAT_MAX);
    
    CGSize dateSize = self.dateLabel.size;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:[UIFont systemFontOfCustomeSize:KNameFont] constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - kHeaderWidth - kSpaceName_label * 2 - dateSize.width, [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:KNameFont]])];
    CGSize contentSize = self.contentLabel.size;
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImaegView.frame) + kSpaceName_label, kTop, nameSize.width, nameSize.height);
    self.contentLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + kSpaceName_label, contentSize.width, contentSize.height);
    self.dateLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - dateSize.width, CGRectGetMaxY(self.nameLabel.frame) - dateSize.height, dateSize.width, dateSize.height);
}

- (void)setReaded {
    [self setReaded:YES];
}

@end
