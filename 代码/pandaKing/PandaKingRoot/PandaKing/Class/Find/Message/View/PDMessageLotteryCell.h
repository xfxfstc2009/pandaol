//
//  PDMessageLotteryCell.h
//  PandaKing
//
//  Created by Cranz on 16/11/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMessageLotteryItem.h"

@interface PDMessageLotteryCell : UITableViewCell
@property (nonatomic, strong) PDMessageLotteryItem *model;

- (void)setReaded;
+ (CGFloat)cellHeight;
@end
