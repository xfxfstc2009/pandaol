//
//  PDPostBarTypeCell.m
//  PandaKing
//
//  Created by Cranz on 17/5/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarTypeCell.h"

#define kICON_WIDTH LCFloat(50)

@interface PDPostBarTypeCell ()

@end

@implementation PDPostBarTypeCell

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        [self __setupCell];
    }
    return self;
}

- (void)__setupCell {
    self.icon = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 13, kICON_WIDTH, kICON_WIDTH)];
    self.icon.layer.cornerRadius = kICON_WIDTH / 2;
    self.icon.layer.masksToBounds = YES;
    [self.contentView addSubview:self.icon];
    
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.dateLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.dateLabel];
    
    self.levelLabel = [[UILabel alloc] init];
    self.levelLabel.font = [UIFont systemFontOfCustomeSize:11];
    self.levelLabel.textColor = [UIColor whiteColor];
    self.levelLabel.backgroundColor = c26;
    self.levelLabel.layer.cornerRadius = 2;
    self.levelLabel.clipsToBounds = YES;
    self.levelLabel.textAlignment = NSTextAlignmentCenter;
    [self.contentView addSubview:self.levelLabel];
}

- (void)resetPostType:(PDPostInfoType)type {
    self.type = type;
    if (type == PDPostInfoTypeReply) {
        self.levelLabel.hidden = NO;
    } else {
        self.levelLabel.hidden = YES;
    }
}

- (void)updateTypeCellsFrame {
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(_type == PDPostInfoTypeReply? (kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - kICON_WIDTH - 60) : (kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - kICON_WIDTH - 20), [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGSize levelSize = [self.levelLabel.text sizeWithCalcFont:self.levelLabel.font];
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.icon.frame) + 10, CGRectGetMinY(self.icon.frame) + 3, nameSize.width, nameSize.height);
    self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + 10, CGRectGetMaxY(self.nameLabel.frame) - levelSize.height, levelSize.width + 10, levelSize.height);
    self.dateLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + 5, dateSize.width, dateSize.height);
}

+ (CGFloat)cellHeight {
    return kICON_WIDTH + 26;
}

@end
