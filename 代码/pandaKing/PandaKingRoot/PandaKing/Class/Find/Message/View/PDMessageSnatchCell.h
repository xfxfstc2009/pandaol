//
//  PDMessageSnatchCell.h
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMessageSnatchItem.h"

@interface PDMessageSnatchCell : UITableViewCell
@property (nonatomic, strong) PDMessageSnatchItem *model;

- (void)setReaded;
+ (CGFloat)cellHeight;
@end
