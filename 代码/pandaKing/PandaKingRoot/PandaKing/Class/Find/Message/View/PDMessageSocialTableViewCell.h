//
//  PDMessageSocialTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageReadStateCell.h"
#import "PDMessageSocialItem.h"

/// 社交消息cell
@interface PDMessageSocialTableViewCell : PDMessageReadStateCell
@property (nonatomic, strong) PDMessageSocialItem *model;
@property (nonatomic, strong) NSIndexPath *indexPath;

+ (CGFloat)cellHeightWithModel:(PDMessageSocialItem *)model;
- (void)setReaded;
@end
