//
//  PDMessageSnatchCell.m
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageSnatchCell.h"

@interface PDMessageSnatchCell ()
@property (nonatomic, assign) CGFloat rowHeigth;
@property (nonatomic, assign) CGFloat spaceLabel_label;

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *contentLabel;
@property (nonatomic, strong) UIImageView *numIcon;
@property (nonatomic, strong) UILabel *numLabel;
@end

@implementation PDMessageSnatchCell

+ (CGFloat)cellHeight {
    return 70.;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    _rowHeigth = [PDMessageSnatchCell cellHeight];
    _spaceLabel_label = LCFloat(8.);
}

- (void)pageSetting {
    // 标题
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:16];
    self.titleLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.titleLabel];
    
    // 右上角 icon 图标
    self.numIcon = [[UIImageView alloc] init];
    self.numIcon.image = [UIImage imageNamed:@"icon_message_mark"];
    [self.contentView addSubview:self.numIcon];
    
    UILabel *numLabel = [[UILabel alloc] init];
    numLabel.text = @"1";
    numLabel.textColor = [UIColor whiteColor];
    numLabel.textAlignment = NSTextAlignmentCenter;
    numLabel.font = [UIFont systemFontOfCustomeSize:12];
    [self.numIcon addSubview:numLabel];
    self.numLabel = numLabel;
    
    // 内容
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.contentLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.contentLabel];
}

- (void)setModel:(PDMessageSnatchItem *)model {
    _model = model;
    
    self.titleLabel.text = model.msg.title;
    self.contentLabel.text = model.msg.textContent;
    self.numIcon.hidden = model.read;
    
    CGSize iconSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font];
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - LCFloat(5.) - iconSize.height, CGFLOAT_MAX)];
    CGSize contentSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2, CGFLOAT_MAX)];
    CGFloat height = [NSString contentofHeightWithFont:self.contentLabel.font];
    
    // 计算文本上下空隙
    CGFloat space = (_rowHeigth - titleSize.height - height - _spaceLabel_label) / 2;
    
    self.titleLabel.frame = CGRectMake(kTableViewSectionHeader_left, space, titleSize.width, titleSize.height);
    self.numIcon.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - iconSize.height, space, iconSize.height, iconSize.height);
    self.numLabel.frame = self.numIcon.bounds;
    self.contentLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.titleLabel.frame) + _spaceLabel_label, contentSize.width, height);
}

- (void)setReaded {
    self.numIcon.hidden = YES;
}

@end

