//
//  PDPostBarReplyCell.m
//  PandaKing
//
//  Created by Cranz on 17/5/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarReplyCell.h"
#import "PDPostBarReplyView.h"

@interface PDPostBarReplyCell ()
@property (nonatomic, strong) PDPostBarReplyView *replyView;
@end

@implementation PDPostBarReplyCell

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        _replyView = [[PDPostBarReplyView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.icon.frame) + 10, [PDPostBarContentCell cellHeight]  - 3, kScreenBounds.size.width - (CGRectGetMaxX(self.icon.frame) + 10), 27)];
        [self.contentView addSubview:_replyView];
    }
    return self;
}

- (void)setModel:(PDMessagePostItem *)model {
    _model = model;
    
    [self.icon uploadImageWithAvatarURL:model.sourceMemberAvatar placeholder:nil callback:nil];
    self.nameLabel.text = model.sourceMemberNickName;
    self.levelLabel.text = [NSString stringWithFormat:@"Lv.%u",model.sourceMemberLevel];
    self.dateLabel.text = model.replyTime;
    self.contentLabel.text = model.textContent;
    self.replyView.title = model.postOpStr;
    self.replyView.reply = model.postContent;
    [self setReaded:model.read];
    
    if ([model.actionType isEqualToString:@"postDeleted"]) { //帖子被删除
        [self resetPostType:PDPostInfoTypeSystem];
    } else if ([model.actionType isEqualToString:@"postLocked"]) { // 帖子被锁定
        [self resetPostType:PDPostInfoTypeSystem];
    } else if ([model.actionType isEqualToString:@"postGetUpvote"]) { // 帖子获得赞
        [self resetPostType:PDPostInfoTypeSystem];
    } else if ([model.actionType isEqualToString:@"postReplied"]) { // 回复
        [self resetPostType:PDPostInfoTypeReply];
    }
    
    [self updateTypeCellsFrame];
    [self updateContentCellsFrame];
    [self.replyView updateReplyFrame];
}

+ (CGFloat)cellHeight {
    return [super cellHeight] + 27 + 10;
}

@end
