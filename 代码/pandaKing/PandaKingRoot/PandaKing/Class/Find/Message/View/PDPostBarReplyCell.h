//
//  PDPostBarReplyCell.h
//  PandaKing
//
//  Created by Cranz on 17/5/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarContentCell.h"
#import "PDMessagePostItem.h"

/// 带有回复层的那一个cell
@interface PDPostBarReplyCell : PDPostBarContentCell
@property (nonatomic, strong) PDMessagePostItem *model;
@end
