//
//  PDMessageReadStateCell.m
//  PandaKing
//
//  Created by Cranz on 17/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMessageReadStateCell.h"

#define kPOINTIWDTH 10
@interface PDMessageReadStateCell ()
/**
 * 消息红点
 */
@property (nonatomic, strong) UIView *redView;
@end

@implementation PDMessageReadStateCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.redView = [[UIView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - kPOINTIWDTH, 12, kPOINTIWDTH, kPOINTIWDTH)];
    self.redView.backgroundColor = c26;
    self.redView.layer.cornerRadius = kPOINTIWDTH / 2;
    self.redView.clipsToBounds = YES;
    [self.contentView addSubview:self.redView];
}

- (void)setReadColor:(UIColor *)readColor {
    _readColor = readColor;
    
    self.redView.backgroundColor = readColor;
}

- (void)setReaded:(BOOL)isRead {
    self.redView.hidden = isRead;
}

@end
