//
//  PDPostBarTypeCell.h
//  PandaKing
//
//  Created by Cranz on 17/5/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMessageReadStateCell.h"

typedef NS_ENUM(NSUInteger, PDPostInfoType) {
    /**
     * 用户回复信息
     */
    PDPostInfoTypeReply,
    /**
     * 贴吧系统消息
     */
    PDPostInfoTypeSystem,
};

/// 信息类别：用户信息（带有登记）、帖子状态信息等
@interface PDPostBarTypeCell : PDMessageReadStateCell
/**
 * 图标部分
 */
@property (nonatomic, strong) PDImageView *icon;
/**
 * 名字标题部分
 */
@property (nonatomic, strong) UILabel *nameLabel;
/**
 * 时间部分
 */
@property (nonatomic, strong) UILabel *dateLabel;
/**
 * 等级部分
 */
@property (nonatomic, strong) UILabel *levelLabel;

@property (nonatomic) PDPostInfoType type;

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier;
- (void)resetPostType:(PDPostInfoType)type;

/**
 * 赋值之后屌用这个方法
 */
- (void)updateTypeCellsFrame;

+ (CGFloat)cellHeight;

@end
