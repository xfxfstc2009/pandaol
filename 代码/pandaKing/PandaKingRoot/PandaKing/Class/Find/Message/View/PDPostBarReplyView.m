//
//  PDPostBarReplyView.m
//  PandaKing
//
//  Created by Cranz on 17/5/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarReplyView.h"

@interface PDPostBarReplyView ()
/**
 * 某某回复；查看帖子
 */
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *replyLabel;
@property (nonatomic, strong) UIImageView *arrowIcon;
@end

@implementation PDPostBarReplyView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupView];
    }
    return self;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self setupView];
    }
    return self;
}

- (void)setupView {
    self.backgroundColor = [[UIColor lightGrayColor] colorWithAlphaComponent:0.2];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.textColor = c26;
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self addSubview:self.titleLabel];
    
    self.replyLabel = [[UILabel alloc] init];
    self.replyLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.replyLabel.textColor = [UIColor blackColor];
    [self addSubview:self.replyLabel];
    
    self.arrowIcon = [[UIImageView alloc] init];
    self.arrowIcon.image = [UIImage imageNamed:@"icon_arrow_right"];
    self.arrowIcon.frame = CGRectMake(self.frame.size.width - self.arrowIcon.image.size.width - kTableViewSectionHeader_left, (self.frame.size.height - self.arrowIcon.image.size.height) / 2, self.arrowIcon.image.size.width, self.arrowIcon.image.size.height);
     [self addSubview:self.arrowIcon];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = [NSString stringWithFormat:@"%@：",title];
}

- (void)setReply:(NSString *)reply {
    _reply = reply;
    self.replyLabel.text = reply;
}

- (void)updateReplyFrame {
    
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font];
    self.titleLabel.frame = CGRectMake(10, (self.frame.size.height - titleSize.height) / 2, titleSize.width, titleSize.height);
    
    CGFloat maxWidth = self.frame.size.width - CGRectGetWidth(self.arrowIcon.frame) - CGRectGetMaxX(self.titleLabel.frame);
    CGSize replySize = [self.replyLabel.text sizeWithCalcFont:self.replyLabel.font constrainedToSize:CGSizeMake(maxWidth + 2, [NSString contentofHeightWithFont:self.replyLabel.font])];
    self.replyLabel.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + 2, (self.frame.size.height - replySize.height) / 2, replySize.width, replySize.height);

}

@end
