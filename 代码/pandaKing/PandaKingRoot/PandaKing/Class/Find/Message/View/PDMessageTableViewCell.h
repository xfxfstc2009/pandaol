//
//  PDMessageTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageReadStateCell.h"
#import "PDMessageItem.h"

/// 系统消息
@interface PDMessageTableViewCell : PDMessageReadStateCell
@property (nonatomic, strong) PDMessageItem *model;

- (void)setReaded;
+ (CGFloat)cellHeight;
@end
