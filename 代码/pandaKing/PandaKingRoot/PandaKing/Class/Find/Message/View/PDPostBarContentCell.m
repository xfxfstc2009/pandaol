//
//  PDPostBarContentCell.m
//  PandaKing
//
//  Created by Cranz on 17/5/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarContentCell.h"
#import <objc/runtime.h>

@implementation PDPostBarContentCell

- (instancetype)initWithReuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithReuseIdentifier:reuseIdentifier];
    if (self) {
        [self setupContentCell];
    }
    return self;
}

- (void)setupContentCell {
    self.contentLabel = [[UILabel alloc] init];
    self.contentLabel.font = [UIFont systemFontOfCustomeSize:16];
    self.contentLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.contentLabel];
}

- (void)updateContentCellsFrame {
    CGSize contentSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left - CGRectGetMaxX(self.icon.frame) - 10, [NSString contentofHeightWithFont:self.contentLabel.font])];
    
    self.contentLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), [PDPostBarTypeCell cellHeight] - 3, contentSize.width, contentSize.height);
}

+ (CGFloat)cellHeight {
    return [super cellHeight] + [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:14]] + 13;
}

@end
