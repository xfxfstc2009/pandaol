//
//  PDMessageReadStateCell.h
//  PandaKing
//
//  Created by Cranz on 17/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 * 消息小红点
 */
@interface PDMessageReadStateCell : UITableViewCell
@property (nonatomic, strong) UIColor *readColor;
- (void)setReaded:(BOOL)isRead;
@end
