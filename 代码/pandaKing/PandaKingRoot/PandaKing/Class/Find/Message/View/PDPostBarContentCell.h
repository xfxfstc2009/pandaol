//
//  PDPostBarContentCell.h
//  PandaKing
//
//  Created by Cranz on 17/5/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDPostBarTypeCell.h"

@interface PDPostBarContentCell : PDPostBarTypeCell
@property (nonatomic, strong) UILabel *contentLabel;

- (void)updateContentCellsFrame;

@end
