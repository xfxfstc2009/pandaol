//
//  PDPostBarReplyView.h
//  PandaKing
//
//  Created by Cranz on 17/5/9.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDPostBarReplyView : UIControl
/**
 * 点击可查看
 */
@property (nonatomic, copy) NSString *title;
/**
 * 回复的内容
 */
@property (nonatomic, copy) NSString *reply;

- (void)updateReplyFrame;

@end
