//
//  PDMessageSystemViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 系统消息控制器
@interface PDMessageSystemViewController : AbstractViewController

@end
