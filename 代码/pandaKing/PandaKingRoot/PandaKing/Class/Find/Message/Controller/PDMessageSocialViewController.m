//
//  PDMessageSocialViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageSocialViewController.h"
#import "PDMessageSocialTableViewCell.h"
#import "PDMessageSocialList.h"
#import "PDCenterPersonViewController.h"
#import "PDBackpackViewController.h"
#import "CCZSegmentController.h"

@interface PDMessageSocialViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *messageArr;
@property (nonatomic, strong) PDMessageSocialList *messageList;
@property (nonatomic, strong) PDMessageSocialTableViewCell *messageCell;
@end

@implementation PDMessageSocialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    _page = 1;
    _messageArr = [NSMutableArray array];
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 64) style:UITableViewStyleGrouped];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    // 刷新
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.messageArr.count) {
            [weakSelf.messageArr removeAllObjects];
        }
        
        [weakSelf fetchData];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.messageList.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchData];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            return ;
        }
    }];
}

#pragma mark - UITabelView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messageArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"messageCellId";
    PDMessageSocialTableViewCell *messageCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!messageCell) {
        messageCell = [[PDMessageSocialTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    messageCell.indexPath = indexPath;
    if (self.messageArr.count) {
        messageCell.model = self.messageArr[indexPath.row];
    }
   
    return messageCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // 在最后一行的时候，将线闭合
    if (indexPath.row == self.messageArr.count - 1) {
        [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PDMessageSocialTableViewCell cellHeightWithModel:self.messageArr[indexPath.row]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PDMessageSocialTableViewCell *messgaeCell = [tableView cellForRowAtIndexPath:indexPath];
    self.messageCell = messgaeCell;
    
    if (self.messageArr.count) {
        PDMessageSocialItem *item = self.messageArr[indexPath.row];
        // 读消息
        if (!item.read) {
            [self fetchDetailSnatchWithItem:item];
        }
        
        if ([item.actionType isEqualToString:@"follow"]) { // 条用户主页
            PDCenterPersonViewController *centerPersonVC = [[PDCenterPersonViewController alloc]init];
            centerPersonVC.transferMemberId = item.sourceMember.ID;
            [centerPersonVC hidesTabBarWhenPushed];
            [self.navigationController pushViewController:centerPersonVC animated:YES];
            
            
        } else if ([item.actionType isEqualToString:@"cer"]) {  // 跳背包赠送
            PDBackpackViewController *backpackViewController = [[PDBackpackViewController alloc] init];
            [self pushViewController:backpackViewController animated:YES];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.messageArr.count) {
        PDMessageSocialItem *item = self.messageArr[indexPath.row];
        [self fetchDelete:item atIndexPath:indexPath];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [PDCenterTool tableViewScrolledRemoveViscousCancleWithScrollView:scrollView sectionHeight:kTableViewHeader_height];
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageSocial requestParams:@{@"pageNum":@(_page)} responseObjectClass:[PDMessageSocialList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            PDMessageSocialList *messageList = (PDMessageSocialList *)responseObject;
            weakSelf.messageList = messageList;
            [weakSelf.messageArr addObjectsFromArray:weakSelf.messageList.items];
            [weakSelf.mainTableView reloadData];
            if (weakSelf.messageArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
        
        [weakSelf.mainTableView stopPullToRefresh];
        [weakSelf.mainTableView stopFinishScrollingRefresh];
    }];
}

- (void)fetchDelete:(PDMessageSocialItem *)item atIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageDeleteSocial requestParams:@{@"msgId":item.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            
            // 已经读过就不需要再减了
            if (!item.read) {
                [weakSelf.segmentController reduceCurrentBadgeByNumber_1];
            }
            
            [weakSelf.messageArr removeObject:item];
            [weakSelf.mainTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            if (weakSelf.messageArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
    }];
}

- (void)fetchDetailSnatchWithItem:(PDMessageSocialItem *)item  {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageSocialRead requestParams:@{@"msgId":item.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            [weakSelf.messageCell setReaded];
            [weakSelf.segmentController reduceCurrentBadgeByNumber_1];
            item.read = YES;
        }
    }];
}

#pragma mark -- 显示无数据页

- (void)showNoDataPage {
    [self.mainTableView showPrompt:@"您还未收到过任何一条社交消息...QAQ" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:0 tapBlock:NULL];
}

@end
