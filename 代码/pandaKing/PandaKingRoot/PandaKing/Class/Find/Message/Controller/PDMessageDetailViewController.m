//
//  PDMessageDetailViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageDetailViewController.h"

@interface PDMessageDetailViewController ()
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIView *line;
@property (nonatomic, strong) UITextView *mainTextView; /**< 这个才显示文字*/
@end

@implementation PDMessageDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
}

- (void)basicSetting {
    self.barMainTitle = @"系统通知";
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)pageSetting {
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.text = self.item.msg.title;
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    self.titleLabel.textColor = [UIColor blackColor];
    [self.view addSubview:self.titleLabel];
    
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.text = [PDCenterTool dateFromTimestamp:self.item.msgTime];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.dateLabel.textColor = c26;
    [self.view addSubview:self.dateLabel];
    
    self.line = [[UIView alloc] init];
    self.line.backgroundColor = c26;
    [self.view addSubview:self.line];

    CGFloat x = LCFloat(35);
    CGFloat y = LCFloat(16);
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - x * 2, CGFLOAT_MAX)];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    self.titleLabel.frame = CGRectMake(x, y, titleSize.width, titleSize.height);
    self.dateLabel.frame = CGRectMake(x, CGRectGetMaxY(self.titleLabel.frame) + 5, dateSize.width, dateSize.height);
    self.line.frame = CGRectMake(0, y * 2 + titleSize.height + 5 + dateSize.height, kScreenBounds.size.width, 1);

    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:8];
    
    self.mainTextView = [[UITextView alloc] initWithFrame:CGRectMake(x, CGRectGetMaxY(self.line.frame) + 6, kScreenBounds.size.width - x * 2, kScreenBounds.size.height - 64 - CGRectGetMaxY(self.line.frame))];
    self.mainTextView.backgroundColor = [UIColor clearColor];
    self.mainTextView.editable = NO;
    [self.view addSubview:self.mainTextView];
    self.mainTextView.text = self.item.msg.textContent;
    NSMutableAttributedString *mainAttribute = [[NSMutableAttributedString alloc] initWithString:self.item.msg.textContent];
    [mainAttribute addAttributes:@{NSParagraphStyleAttributeName:paragraphStyle, NSFontAttributeName: [UIFont systemFontOfCustomeSize:16], NSForegroundColorAttributeName: [[UIColor blackColor] colorWithAlphaComponent:.8]} range:NSMakeRange(0, self.mainTextView.text.length)];
    self.mainTextView.attributedText = [mainAttribute copy];

}


@end
