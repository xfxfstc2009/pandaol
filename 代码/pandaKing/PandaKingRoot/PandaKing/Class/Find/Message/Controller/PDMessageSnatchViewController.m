//
//  PDMessageSystemViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageSnatchViewController.h"
#import "PDMessageSnatchCell.h"
#import "PDMessageSnatchList.h"
#import "PDProductDetailViewController.h"
#import "CCZSegmentController.h"
#import "PDShopRootMainViewController.h"
@interface PDMessageSnatchViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) PDMessageSnatchList *messageList;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *messageArr;
@property (nonatomic, strong) PDMessageSnatchCell *messageCell;
@end

@implementation PDMessageSnatchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    _page = 1;
    self.messageArr = [NSMutableArray array];
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 64) style:UITableViewStyleGrouped];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = [PDMessageSnatchCell cellHeight];
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    // 刷新
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.messageArr.count) {
            [weakSelf.messageArr removeAllObjects];
        }
        [weakSelf fetchData];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.messageList.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchData];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            return ;
        }
    }];
}

#pragma mark -
#pragma mark -- UITabelView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messageArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"messageCellId";
    PDMessageSnatchCell *messageCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!messageCell) {
        messageCell = [[PDMessageSnatchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    if (self.messageArr.count) {
        messageCell.model = self.messageArr[indexPath.row];
    }
    return messageCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.messageArr.count - 1) {
        [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PDMessageSnatchCell *messgaeCell = [tableView cellForRowAtIndexPath:indexPath];
    self.messageCell = messgaeCell;
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.messageArr.count) {
        // 已读
        PDMessageSnatchItem *item = self.messageArr[indexPath.row];
        if (!item.read) {
            [self fetchDetailSnatchWithItem:item];
        }
        
        // 跳活动详情
        PDProductDetailViewController *detailViewController = [[PDProductDetailViewController alloc] init];
        detailViewController.productId = item.lotteryId;
        [self pushViewController:detailViewController animated:YES];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.messageArr.count) {
        PDMessageSnatchItem *item = [self.messageArr objectAtIndex:indexPath.row];
        [self fetchDelete:item atIndexPath:indexPath];
    }
}

#pragma mark -- UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [PDCenterTool tableViewScrolledRemoveViscousCancleWithScrollView:scrollView sectionHeight:kTableViewHeader_height];
}

#pragma mark -
#pragma mark -- 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageSnatch requestParams:@{@"pageNum":@(_page)} responseObjectClass:[PDMessageSnatchList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDMessageSnatchList *list = (PDMessageSnatchList *)responseObject;
            weakSelf.messageList = list;
            [weakSelf.messageArr addObjectsFromArray:list.items];
            [weakSelf.mainTableView reloadData];
            if (weakSelf.messageArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
        
        [weakSelf.mainTableView stopFinishScrollingRefresh];
        [weakSelf.mainTableView stopPullToRefresh];
    }];
}

- (void)fetchDetailSnatchWithItem:(PDMessageSnatchItem *)item {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageSnatchRead requestParams:@{@"msgId":item.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            [weakSelf.messageCell setReaded];
            [weakSelf.segmentController reduceCurrentBadgeByNumber_1];
            item.read = YES;
        }
    }];
}

- (void)fetchDelete:(PDMessageSnatchItem *)item atIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageDeleteSnatch requestParams:@{@"msgId":item.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            
            // 已经读过就不需要再减了
            if (!item.read) {
                [weakSelf.segmentController reduceCurrentBadgeByNumber_1];
            }
            
            [weakSelf.messageArr removeObject:item];
            [weakSelf.mainTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            if (weakSelf.messageArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
    }];
}

#pragma mark -- 显示无数据页

- (void)showNoDataPage {
    __weak typeof(self) weakSelf = self;
    [self.mainTableView showPrompt:@"诶呀!您还未参与过夺宝啊...QAQ" withImage:[UIImage imageNamed:@"icon_nodata_panda"] buttonTitle:@"立即夺宝" clickBlock:^{
        if ([AccountModel sharedAccountModel].isShenhe){
            return ;
        }
        [weakSelf.navigationController popToRootViewControllerAnimated:YES];
        [[PDMainTabbarViewController sharedController] setSelectedIndex:1 animated:YES];
    }];
}

@end
