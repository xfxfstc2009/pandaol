//
//  PDMessageViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageViewController.h"
#import "CCZSegmentController.h"
#import "PDMessageSystemViewController.h" // 系统
#import "PDMessageSocialViewController.h" // 社交
#import "PDMessagePostBarViewController.h" // 野区

@interface PDMessageViewController ()
@property (nonatomic, strong) CCZSegmentController *segment;
@end

@implementation PDMessageViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchUnreadMessagesNumber];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 【新手引导】
    if (![Tool userDefaultGetWithKey:NewFeature_Message].length){
        PDNewFeaturePageViewController *newFeatureViewController = [[PDNewFeaturePageViewController alloc]init];
        [newFeatureViewController showInView:self.parentViewController type:PDNewFeaturePageViewControllerTypeMessage];
    }
}

- (void)basicSetting {
    self.barMainTitle = @"我的消息";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)pageSetting {
    PDMessageSystemViewController *systemViewController = [[PDMessageSystemViewController alloc] init];
    PDMessageSocialViewController *socialViewController = [[PDMessageSocialViewController alloc] init];
    PDMessagePostBarViewController *postViewController = [[PDMessagePostBarViewController alloc] init];
    
    CCZSegmentController *segement;
    if ([AccountModel sharedAccountModel].isShenhe) {
        segement = [[CCZSegmentController alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height) titles:@[@"系统",@"社交"]];
        segement.viewControllers = @[systemViewController, socialViewController];
    } else {
        segement = [[CCZSegmentController alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height) titles:@[@"系统",@"社交",@"野区"]];
        segement.viewControllers = @[systemViewController, socialViewController, postViewController];
    }
    
    segement.segmentView.segmentTintColor = c26;
    segement.containerView.scrollEnabled = NO;
    [self addSegmentController:segement];
    self.segment = segement;
}

#pragma mark - 请求未读消息数

- (void)fetchUnreadMessagesNumber {
    PDMessageState *state = [AccountModel sharedAccountModel].messageState;
    if ([AccountModel sharedAccountModel].isShenhe){
        [self.segment enumerateBadges:@[@(state.pandaMsgCount),@(state.socialMsgCount)]];
    } else {
        [self.segment enumerateBadges:@[@(state.pandaMsgCount),@(state.socialMsgCount),@(state.postMsgCount)]];
    }
}

@end
