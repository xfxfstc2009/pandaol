//
//  PDMessageLotteryViewController.m
//  PandaKing
//
//  Created by Cranz on 16/10/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageLotteryViewController.h"
#import "PDMessageLotteryCell.h"
#import "PDMessageLotteryList.h"
#import "PDCenterLotteryHeroViewController.h"
#import "PDLotteryHeroViewController.h"
#import "PDMessageDetailViewController.h"
#import "CCZSegmentController.h"
#import "PDWalletViewController.h"

// 赛事竞猜
#import "PDLotteryGameMainViewController.h"
#import "PDLotteryGameDetailMainViewController.h"

@interface PDMessageLotteryViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) PDMessageLotteryList *messageList;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *messageArr;
@property (nonatomic, strong) PDMessageLotteryCell *messageCell;

@end

@interface PDMessageLotteryViewController ()

@end

@implementation PDMessageLotteryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    _page = 1;
    self.messageArr = [NSMutableArray array];
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height- 64) style:UITableViewStyleGrouped];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = [PDMessageLotteryCell cellHeight];
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    // 刷新
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.messageArr.count) {
            [weakSelf.messageArr removeAllObjects];
        }
        [weakSelf fetchData];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.messageList.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchData];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            return ;
        }
    }];
}

#pragma mark - UITabelView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messageArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"messageCellId";
    PDMessageLotteryCell *messageCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!messageCell) {
        messageCell = [[PDMessageLotteryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    if (self.messageArr.count) {
        messageCell.model = self.messageArr[indexPath.row];
    }
    
    return messageCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.messageArr.count - 1) {
        [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PDMessageLotteryCell *messageCell = [tableView cellForRowAtIndexPath:indexPath];
    self.messageCell = messageCell;
    
    if (self.messageArr.count) {
        PDMessageLotteryItem *item = self.messageArr[indexPath.row];
        if (!item.read) {
             [self fetchReadMessage:item];
        }
        
        NSString *type = item.sourceType;
        if ([type isEqualToString:@"winLOLChampionGuess"]) {//直接打开英雄猜界面
            PDCenterLotteryHeroViewController *centerHeroLotteryViewController = [[PDCenterLotteryHeroViewController alloc] init];
            [self.navigationController pushViewController:centerHeroLotteryViewController animated:YES];
        } else if ([type isEqualToString:@"winMatchGuess"]) {
            PDCenterLotteryHeroViewController *centerHeroLotteryViewController = [[PDCenterLotteryHeroViewController alloc] init];
            centerHeroLotteryViewController.selIndex = 1;
            [self.navigationController pushViewController:centerHeroLotteryViewController animated:YES];
        } else if ([type isEqualToString:@"matchGuessStakeOff"]) {
            PDLotteryGameMainViewController *gameViewController = [[PDLotteryGameMainViewController alloc] init];
            PDLotteryGameDetailMainViewController *gameDetailViewController = [[PDLotteryGameDetailMainViewController alloc] init];
            gameDetailViewController.transferMatchId = item.sourceId;
            NSMutableArray *vcs = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
            [vcs addObjectsFromArray:@[gameViewController, gameDetailViewController]];
            [self.navigationController setViewControllers:[vcs copy] animated:YES];
        } else if ([type isEqualToString:@"cancelMatchGuess"]) {
            PDMessageItem *msgItem = [[PDMessageItem alloc] init];
            msgItem.msg.title = item.msg.title;
            msgItem.msg.textContent = item.msg.textContent;
            msgItem.msgTime = item.msgTime;
            PDMessageDetailViewController *detailViewController = [[PDMessageDetailViewController alloc] init];
            detailViewController.item = msgItem;
            [self.navigationController pushViewController:detailViewController animated:YES];
            detailViewController.barMainTitle = @"竞猜消息";
        } else if ([type isEqualToString:@"matchGuessMatchStart"]) { // 赛事猜-比赛开始->跳转到该赛事sourceId
            PDLotteryGameMainViewController *gameViewController = [[PDLotteryGameMainViewController alloc] init];
            PDLotteryGameDetailMainViewController *gameDetailViewController = [[PDLotteryGameDetailMainViewController alloc] init];
            gameDetailViewController.transferMatchId = item.sourceId;
            NSMutableArray *vcs = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
            [vcs addObjectsFromArray:@[gameViewController, gameDetailViewController]];
            [self.navigationController setViewControllers:[vcs copy] animated:YES];
        } else if ([type isEqualToString:@"lock"]) { // 赛事猜-锁盘->跳转到该赛事sourceId
            PDMessageItem *msgItem = [[PDMessageItem alloc] init];
            msgItem.msg.title = item.msg.title;
            msgItem.msg.textContent = item.msg.textContent;
            msgItem.msgTime = item.msgTime;
            PDMessageDetailViewController *detailViewController = [[PDMessageDetailViewController alloc] init];
            detailViewController.item = msgItem;
            [self pushViewController:detailViewController animated:YES];
            detailViewController.barMainTitle = @"竞猜消息";
        } else if ([type isEqualToString:@"unlock"]) { // 赛事猜-解锁->跳转到该赛事sourceId
            PDMessageItem *msgItem = [[PDMessageItem alloc] init];
            msgItem.msg.title = item.msg.title;
            msgItem.msg.textContent = item.msg.textContent;
            msgItem.msgTime = item.msgTime;
            PDMessageDetailViewController *detailViewController = [[PDMessageDetailViewController alloc] init];
            detailViewController.item = msgItem;
            [self pushViewController:detailViewController animated:YES];
            detailViewController.barMainTitle = @"竞猜消息";
        } else if ([type isEqualToString:@"drawLOLJungleGuess"]) { // 打野大作战-开奖->钱包流水
            PDWalletViewController *walletViewController = [[PDWalletViewController alloc] init];
            [self pushViewController:walletViewController animated:YES];
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.messageArr.count) {
        PDMessageLotteryItem *item = self.messageArr[indexPath.row];
        [self fetchDelete:item atIndexPath:indexPath];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [PDCenterTool tableViewScrolledRemoveViscousCancleWithScrollView:scrollView sectionHeight:kTableViewHeader_height];
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageLottery requestParams:@{@"pageNum":@(_page)} responseObjectClass:[PDMessageLotteryList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDMessageLotteryList *list = (PDMessageLotteryList *)responseObject;
            weakSelf.messageList = list;
            [weakSelf.messageArr addObjectsFromArray:list.items];
            [weakSelf.mainTableView reloadData];
            if (weakSelf.messageArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
        
        [weakSelf.mainTableView stopFinishScrollingRefresh];
        [weakSelf.mainTableView stopPullToRefresh];
    }];
}

- (void)fetchReadMessage:(PDMessageLotteryItem *)item {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageLotteryRead requestParams:@{@"msgId":item.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            [weakSelf.messageCell setReaded];
            [weakSelf.segmentController reduceCurrentBadgeByNumber_1];
            item.read = YES;
        }
    }];
}

- (void)fetchDelete:(PDMessageLotteryItem *)item atIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageDeleteLottery requestParams:@{@"msgId":item.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            
            // 已经读过就不需要再减了
            if (!item.read) {
                [weakSelf.segmentController reduceCurrentBadgeByNumber_1];
            }
            
            [weakSelf.messageArr removeObject:item];
            [weakSelf.mainTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            if (weakSelf.messageArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
    }];
}

#pragma mark -- 显示无数据页

- (void)showNoDataPage {
    [self.mainTableView showPrompt:@"您没有任何竞猜消息...QAQ" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:0 tapBlock:NULL];
}

@end
