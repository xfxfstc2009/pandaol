//
//  PDMessageSystemViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageSystemViewController.h"
#import "PDMessageTableViewCell.h"
#import "PDMessageList.h"
#import "PDMessageDetailViewController.h"
#import "PDProductDetailViewController.h" // 夺宝详情
#import "PDCenterLotteryHeroViewController.h" // 竞猜详情
#import "PDLotteryGameMainViewController.h"
#import "PDLotteryGameDetailMainViewController.h" // 赛事
#import "PDCenterLotteryHeroViewController.h" // 英雄猜
#import "PDWalletViewController.h" // 钱包流水
#import "CCZSegmentController.h"

@interface PDMessageSystemViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) PDMessageList *messageList;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *messageArr;
@property (nonatomic, strong) PDMessageTableViewCell *messageCell;
@end

@implementation PDMessageSystemViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    _page = 1;
    self.messageArr = [NSMutableArray array];
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 64) style:UITableViewStyleGrouped];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.rowHeight = [PDMessageTableViewCell cellHeight];
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    // 刷新
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.messageArr.count) {
            [weakSelf.messageArr removeAllObjects];
        }
        [weakSelf fetchData];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.messageList.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchData];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            return ;
        }
    }];
}

#pragma mark - UITabelView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.messageArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"messageCellId";
    PDMessageTableViewCell *messageCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!messageCell) {
        messageCell = [[PDMessageTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    if (self.messageArr.count) {
        messageCell.model = self.messageArr[indexPath.row];
    }
    
    return messageCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == self.messageArr.count - 1) {
        [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    PDMessageTableViewCell *messageCell = [tableView cellForRowAtIndexPath:indexPath];
    self.messageCell = messageCell;
    
    if (self.messageArr.count) {
        PDMessageItem *item = self.messageArr[indexPath.row];
        [self fetchReadMessage:item];
            // 跳详情
        [self jumpWithItem:item];
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.messageArr.count) {
        PDMessageItem *item = self.messageArr[indexPath.row];
        [self fetchDelete:item atIndexPath:indexPath];
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [PDCenterTool tableViewScrolledRemoveViscousCancleWithScrollView:scrollView sectionHeight:kTableViewHeader_height];
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageSystem requestParams:@{@"pageNum":@(_page)} responseObjectClass:[PDMessageList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDMessageList *list = (PDMessageList *)responseObject;
            weakSelf.messageList = list;
            [weakSelf.messageArr addObjectsFromArray:list.items];
            [weakSelf.mainTableView reloadData];
            if (weakSelf.messageArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
        
        [weakSelf.mainTableView stopFinishScrollingRefresh];
        [weakSelf.mainTableView stopPullToRefresh];
    }];
}

- (void)fetchReadMessage:(PDMessageItem *)item {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageSystemRead requestParams:@{@"id":item.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            [weakSelf.messageCell setReaded];
            if (item.read == NO) {
                [weakSelf.segmentController reduceCurrentBadgeByNumber_1];
                item.read = YES;
            }
        }
    }];
}

- (void)fetchDelete:(PDMessageItem *)item atIndexPath:(NSIndexPath *)indexPath {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageDeleteSystem requestParams:@{@"id":item.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            
            // 已经读过就不需要再减了
            if (!item.read) {
                [weakSelf.segmentController reduceCurrentBadgeByNumber_1];
            }
            
            [weakSelf.messageArr removeObject:item];
            [weakSelf.mainTableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationLeft];
            if (weakSelf.messageArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
    }];
}

#pragma mark - 显示无数据页

- (void)showNoDataPage {
    [self.mainTableView showPrompt:@"系统没给你发过任何消息...QAQ" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:0 tapBlock:NULL];
}

#pragma mark - 跳转

- (void)jumpWithItem:(PDMessageItem *)item {
    NSString *type = item.type;
    if ([type isEqualToString:@"publicNotice"]) {
        [self goMessageDetailWithItem:item];
    } else if ([type isEqualToString:@"treasure"]) {
        [self goSnatchWithItem:item];
    } else if ([type isEqualToString:@"drawMatchGuess"]) { // 赛事开奖
        [self goLotteryListWithType:PDCenterLotteryTypeMatch];
    } else if ([type isEqualToString:@"winMatchGuess"]) { // 赛事中奖
        [self goLotteryListWithType:PDCenterLotteryTypeMatch];
    } else if ([type isEqualToString:@"matchGuessStakeOff"]) { // 赛事猜-投注截至
        [self goMatchLotteryDetailWith:item];
    } else if ([type isEqualToString:@"matchGuessMatchStart"]) {// 赛事猜-比赛开始
        [self goMatchLotteryDetailWith:item];
    } else if ([type isEqualToString:@"cancelMatchGuess"]) { // 赛事猜-取消
        [self goMessageDetailWithItem:item];
    } else if ([type isEqualToString:@"winLOLChampionGuess"] || [type isEqualToString:@"winDota2ChampionGuess"] || [type isEqualToString:@"winKingChampionGuess"]) { // 英雄猜-中奖
        [self goLotteryListWithType:PDCenterLotteryTypeHero];
    } else if ([type isEqualToString:@"drawLOLChampionGuess"] || [type isEqualToString:@"drawDota2ChampionGuess"] || [type isEqualToString:@"drawKingChampionGuess"]) { // 英雄猜-开奖
        [self goHeroLottery];
    } else if ([type isEqualToString:@"drawLOLJungleGuess"] || [type isEqualToString:@"drawDota2JungleGuess"] || [type isEqualToString:@"drawKingJungleGuess"]) { // 打野大作战-开奖
        [self goWallet];
    } else if ([type isEqualToString:@"lock"]) { //  赛事猜-锁盘
        [self goMessageDetailWithItem:item];
    } else if ([type isEqualToString:@"unlock"]) { // 赛事猜-解锁
        [self goMessageDetailWithItem:item];
    } else if ([type isEqualToString:@"bind"]) {  // 绑定召唤师
        [self goMessageDetailWithItem:item];
    } else if ([type isEqualToString:@"sendDoneCommodityOrder"]) { // 兑换卷发货
        [self goMessageDetailWithItem:item];
    } else if ([type isEqualToString:@"otherBind"]) { // 召唤师被其他人绑定
        [self goMessageDetailWithItem:item];
    } else if ([type isEqualToString:@"gameUserActivate"]) { // 召唤师认证成功
        [self goMessageDetailWithItem:item];
    } else if ([type isEqualToString:@"invitedAward"]) { // 邀请好友得金币
        [self goWallet];
    } else if ([type isEqualToString:@"settle"]) { // 结算得金币
        [self goWallet];
    } else if ([type isEqualToString:@"notice"]) { // 通知
        [self goMessageDetailWithItem:item];
    } else if ([type isEqualToString:@"firstLogin"]) { // 注册奖励
        [self goWallet];
    }
}

/**
 * 去消息详情
 */
- (void)goMessageDetailWithItem:(PDMessageItem *)item {
    PDMessageDetailViewController *detailViewController = [[PDMessageDetailViewController alloc] init];
    detailViewController.item = item;
    [self pushViewController:detailViewController animated:YES];
}

/**
 * 去夺宝详情
 */
- (void)goSnatchWithItem:(PDMessageItem *)item {
    PDProductDetailViewController *detailViewController = [[PDProductDetailViewController alloc] init];
    detailViewController.productId = item.actionEntityId;
    [self pushViewController:detailViewController animated:YES];
}

/**
 * 去竞猜纪录
 */
- (void)goLotteryListWithType:(int)type {
    PDCenterLotteryHeroViewController *centerHeroLotteryViewController = [[PDCenterLotteryHeroViewController alloc] init];
    centerHeroLotteryViewController.selIndex = type;
    [self pushViewController:centerHeroLotteryViewController animated:YES];
}

/**
 * 去赛事猜详情
 */
- (void)goMatchLotteryDetailWith:(PDMessageItem *)item {
    PDLotteryGameMainViewController *gameViewController = [[PDLotteryGameMainViewController alloc] init];
    PDLotteryGameDetailMainViewController *gameDetailViewController = [[PDLotteryGameDetailMainViewController alloc] init];
    gameDetailViewController.transferMatchId = item.actionEntityId;
    NSMutableArray *vcs = [NSMutableArray arrayWithArray:self.navigationController.viewControllers];
    [vcs addObjectsFromArray:@[gameViewController, gameDetailViewController]];
    [self.navigationController setViewControllers:[vcs copy] animated:YES];
}

/**
 * 前往英雄猜
 */
- (void)goHeroLottery {
    PDCenterLotteryHeroViewController *centerHeroLotteryViewController = [[PDCenterLotteryHeroViewController alloc] init];
    [self pushViewController:centerHeroLotteryViewController animated:YES];
}

/**
 * 前往钱包看流水
 */
- (void)goWallet {
    PDWalletViewController *walletViewController = [[PDWalletViewController alloc] init];
    [self pushViewController:walletViewController animated:YES];
}

@end
