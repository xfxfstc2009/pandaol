//
//  PDMessageDetailViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDMessageItem.h"

/// 系统通知详情
@interface PDMessageDetailViewController : AbstractViewController
@property (nonatomic, strong) PDMessageItem *item;
@end
