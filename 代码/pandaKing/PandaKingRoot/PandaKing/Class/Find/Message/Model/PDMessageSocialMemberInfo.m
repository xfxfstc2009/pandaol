//
//  PDMessageSocialMemberInfo.m
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDMessageSocialMemberInfo.h"

@implementation PDMessageSocialMemberInfo
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"ID": @"id"};
}
@end
