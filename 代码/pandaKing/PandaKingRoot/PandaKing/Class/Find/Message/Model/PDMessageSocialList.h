//
//  PDMessageSocialList.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMessageSocialItem.h"

/// 社交消息列表
@interface PDMessageSocialList : FetchModel
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) NSArray <PDMessageSocialItem>*items;
@end
