//
//  PDMessageSocialItem.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMessageSocialMemberInfo.h"

@protocol PDMessageSocialItem <NSObject>
@end

@interface PDMessageSocialItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *receiveMemberId;
@property (nonatomic, copy) NSString *textContent;
@property (nonatomic, assign) NSTimeInterval msgTime;
@property (nonatomic, assign) BOOL read;
@property (nonatomic, copy) NSString *actionEntityId;
@property (nonatomic, copy) NSString *actionType;   /**< follow : 关注消息, cer : cer是赠送*/
@property (nonatomic, strong) PDMessageSocialMemberInfo *sourceMember;
@end
