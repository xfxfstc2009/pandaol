//
//  PDMessageState.h
//  PandaKing
//
//  Created by Cranz on 16/12/12.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDMessageState : FetchModel
@property (nonatomic, assign) BOOL hasUnReadMsg; // 有无未读消息
@property (nonatomic, assign) int pandaMsgCount; // 系统
@property (nonatomic, assign) int postMsgCount; // 野区
@property (nonatomic, assign) int socialMsgCount; // 社交
@end
