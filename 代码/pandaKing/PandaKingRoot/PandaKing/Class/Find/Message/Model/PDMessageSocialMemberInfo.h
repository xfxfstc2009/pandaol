//
//  PDMessageSocialMemberInfo.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDMessageSocialMemberInfo : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *qq;
@property (nonatomic, copy) NSString *weChat;
@property (nonatomic, copy) NSString *signature;
@property (nonatomic, copy) NSString *gender;   /**< MALE | */
@property (nonatomic, copy) NSString *cellphone;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, assign) NSInteger age;
@property (nonatomic, strong) NSArray *bindGames;
@property (nonatomic, assign) NSTimeInterval registerTime;
@property (nonatomic, assign) BOOL enable;
@property (nonatomic, copy) NSString *address;
@end
