//
//  PDMessageSnatchList.h
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMessageSnatchItem.h"

@interface PDMessageSnatchList : FetchModel
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) NSArray <PDMessageSnatchItem> *items;
@end
