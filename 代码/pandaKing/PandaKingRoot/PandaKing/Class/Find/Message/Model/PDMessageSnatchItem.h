//
//  PDMessageSnatchItem.h
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMessageSnatchMsg.h"

@protocol PDMessageSnatchItem <NSObject>
@end
@interface PDMessageSnatchItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *lotteryId;    /**< 活动期号*/
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, strong) PDMessageSnatchMsg *msg;
@property (nonatomic, assign) NSTimeInterval msgTime;
@property (nonatomic, assign) BOOL read;
@end
