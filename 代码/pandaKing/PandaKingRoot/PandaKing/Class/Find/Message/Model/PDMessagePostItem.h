//
//  PDMessagePostItem.h
//  PandaKing
//
//  Created by Cranz on 17/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDMessagePostItem <NSObject>

@end

/// 野区消息item
@interface PDMessagePostItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *sourceMemberId;
@property (nonatomic, copy) NSString *sourceMemberAvatar;
@property (nonatomic, copy) NSString *sourceMemberNickName;
@property (nonatomic, assign) unsigned int sourceMemberLevel;
@property (nonatomic, copy) NSString *receiveMemberId;
@property (nonatomic, copy) NSString *textContent;
@property (nonatomic, copy) NSString *actionType;
@property (nonatomic) NSInteger postId;
@property (nonatomic, assign) long msgTime;
@property (nonatomic, assign) BOOL read;
@property (nonatomic, copy) NSString *replyTime;
@property (nonatomic, copy) NSString *postContent;
@property (nonatomic, copy) NSString *postOpStr;
@property (nonatomic, copy) NSString *postType;
@property (nonatomic, copy) NSString *baseUrl;
@property (nonatomic, assign) BOOL me;
@end

/*<actionType>
 postDeleted,       帖子被删除
 postLocked,    帖子被锁定
 postGetUpvote,     帖子获得赞
 postReplied,       帖子获得回复
 */

