//
//  PDMessageLotteryItem.h
//  PandaKing
//
//  Created by Cranz on 16/11/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMessageLotteryMsg.h"

/*
 >>> sourceType <<<

 * 赛事猜-中奖

winMatchGuess,

 * 赛事猜-投注截至

matchGuessStakeOff,

 * 赛事猜-取消

cancelMatchGuess,

 * 英雄猜-中奖

winLOLChampionGuess,

 */
@protocol PDMessageLotteryItem <NSObject>

@end
@interface PDMessageLotteryItem : FetchModel
@property (nonatomic, copy) NSString *ID; //消息id
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, strong) PDMessageLotteryMsg *msg;
@property (nonatomic, assign) NSTimeInterval msgTime;
@property (nonatomic, assign) BOOL read;
@property (nonatomic, copy) NSString *sourceId; // 竞猜id
@property (nonatomic, copy) NSString *sourceType;
@end
