//
//  PDMessageSnatchMsg.h
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDMessageSnatchMsg : FetchModel
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *textContent;
@end
