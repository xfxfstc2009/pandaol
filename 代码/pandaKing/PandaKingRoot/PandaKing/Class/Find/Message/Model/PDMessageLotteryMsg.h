//
//  PDMessageLotteryMsg.h
//  PandaKing
//
//  Created by Cranz on 16/11/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDMessageLotteryMsg : FetchModel
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *textContent;
@end
