//
//  PDMessageLotteryList.h
//  PandaKing
//
//  Created by Cranz on 16/11/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMessageLotteryItem.h"

@interface PDMessageLotteryList : FetchModel
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, strong) NSArray<PDMessageLotteryItem> *items;
@end
