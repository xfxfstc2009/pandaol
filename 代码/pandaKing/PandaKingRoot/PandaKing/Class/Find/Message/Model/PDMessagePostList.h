//
//  PDMessagePostList.h
//  PandaKing
//
//  Created by Cranz on 17/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMessagePostItem.h"

/// 野区消息列表
@interface PDMessagePostList : FetchModel
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, assign) NSUInteger pageNum;
@property (nonatomic, strong) NSArray <PDMessagePostItem>*items;
@end
