//
//  PDMessageItem.h
//  PandaKing
//
//  Created by Cranz on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDMessageItem <NSObject>

@end

@class PDMessageText;
@interface PDMessageItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, strong) PDMessageText *msg;
@property (nonatomic, assign) NSTimeInterval msgTime;
/**
 * SystemMsgType,
 */
@property (nonatomic, copy) NSString *type;
@property (nonatomic, copy) NSString *actionEntityId;
@property (nonatomic, assign) BOOL read;
@end

@interface PDMessageText : FetchModel
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *textContent;
@end
/** <type>
 publicNotice,  公告
 treasure,  夺宝
 drawMatchGuess,    赛事猜-开奖
 winMatchGuess, 赛事猜-中奖
 matchGuessStakeOff,    赛事猜-投注截至
 matchGuessMatchStart,  赛事猜-比赛开始
 cancelMatchGuess,   赛事猜-取消
 winLOLChampionGuess,   英雄猜-中奖
 drawLOLChampionGuess,  英雄猜-开奖
 drawLOLJungleGuess,    打野大作战-开奖
 cancelAnchorGuess, 主播猜-取消
 winAnchorGuess,    主播猜-中奖
 anchorGuessCandidate,  候选主播出现
 anchorGuessFinal,   最终主播出现
 lock,  赛事猜-锁盘
 unlock,    赛事猜-解锁
 bind,  绑定召唤师
 sendDoneCommodityOrder,    兑换卷发货
 otherBind, 召唤师被其他人绑定
 gameUserActivate,   召唤师认证成功
 invitedAward,  邀请好友得金币
 settle,    结算得金币
 notice,    通知
 firstLogin,    注册奖励
 */