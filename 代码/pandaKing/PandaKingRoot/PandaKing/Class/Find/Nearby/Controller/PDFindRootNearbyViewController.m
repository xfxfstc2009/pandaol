//
//  PDFindRootNearbyViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFindRootNearbyViewController.h"
#import "HTHorizontalSelectionList.h"

#import "PDFindNearbyGamerCell.h"                   // 附近的人的cell
#import "PDFindNearbyGamerListModel.h"              // 获取附近的人
#import "PDCenterPersonViewController.h"            // 个人详情

@interface PDFindRootNearbyViewController()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView *gamerTableView;                    /**< 玩家tableView*/
@property (nonatomic,strong)NSMutableArray *gameMutableArr;                 /**< 玩家数据源*/
@property (nonatomic,assign)alertGenderType genderType;

@end

@implementation PDFindRootNearbyViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self mainTableView];
    
    
    
    // 获取当前附近信息
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetGamerInfo:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"附近";
    __weak typeof(self)weakSelf = self;
    [weakSelf rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_nearby_screen"] barHltImage:[UIImage imageNamed:@"icon_nearby_screen"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[PDAlertView sharedAlertView] showGenderAlertWithChoose:^(alertGenderType type) {
            strongSelf.genderType = type;
            self.gamerTableView.currentPage = @"1";
            [[PDAlertView sharedAlertView] dismissWithBlock:^{
                [strongSelf sendRequestToGetGamerInfo:YES];
            }];
        }];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.gameMutableArr = [NSMutableArray array];
    self.genderType = alertGenderTypeNormal;
}


#pragma mark - UITableView
-(void)mainTableView{
    if (!self.gamerTableView){
        self.gamerTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.gamerTableView.delegate = self;
        self.gamerTableView.dataSource = self;
        self.gamerTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.gamerTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.gamerTableView.showsVerticalScrollIndicator = YES;
        self.gamerTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.gamerTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.gamerTableView appendingFiniteScrollingPullToRefreshHandler:^{            // 上啦加载
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf =  weakSelf;
        [strongSelf sendRequestToGetGamerInfo:NO];
    }];
    
    [self.gamerTableView appendingPullToRefreshHandler:^{                       // 下啦刷新
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetGamerInfo:YES];
    }];
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.gameMutableArr.count;;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.gamerTableView){
        static NSString *cellIdentigyWithRowOne = @"cellIdentigyWithRowOne";
        PDFindNearbyGamerCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentigyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDFindNearbyGamerCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentigyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        cellWithRowOne.transferGameSingleModel = [self.gameMutableArr objectAtIndex:indexPath.row];
        __weak typeof(self)weakSelf = self;
        cellWithRowOne.linkBlock = ^(PDFindNearbyGamerSingleModel *transferGameSingleModel){
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToLinkWithId:transferGameSingleModel];
        };
        return cellWithRowOne;
    }
    return nil;
}


#pragma mark - UIUTableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PDCenterPersonViewController *centerPersonVC = [[PDCenterPersonViewController alloc]init];
    PDFindNearbyGamerSingleModel *personInfoModel = [self.gameMutableArr objectAtIndex:indexPath.row];
    centerPersonVC.transferMemberId = personInfoModel.friends.friendInfo.friendId;
    __weak typeof(self)weakSelf = self;
    [centerPersonVC personCenterCollectionBlock:^(NSString *personId,BOOL collection) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        for (PDFindNearbyGamerSingleModel *personInfoModel in strongSelf.gameMutableArr ){
            if ([personInfoModel.friends.friendInfo.friendId isEqualToString:personId]){
                personInfoModel.friends.followed = collection;
                [strongSelf.gamerTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                return;
            }
        }
    }];
    
    [centerPersonVC hidesTabBarWhenPushed];
    [self.navigationController pushViewController:centerPersonVC animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(10);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PDFindNearbyGamerCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.gamerTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else if ([indexPath row] == [self.gameMutableArr  count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeMiddle;
        }
        if ([self.gameMutableArr  count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
}


#pragma mark - 接口
-(void)sendRequestToGetGamerInfo:(BOOL)refresh{
    [PDHUD showHUDProgress:@"熊猫君正在搜寻附近的人……" diary:0];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:self.gamerTableView.currentPage forKey:@"start"];
    [params setObject:@([PDCurrentLocationManager sharedLocation].lat) forKey:@"latitude"];
    [params setObject:@([PDCurrentLocationManager sharedLocation].lng) forKey:@"longtitude"];

    if (self.genderType != alertGenderTypeNormal){
        [params setObject:(self.genderType == alertGenderTypeBoy ?@"MALE":@"FEMALE") forKey:@"gender"];
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:nearbyCustomer requestParams:params responseObjectClass:[PDFindNearbyGamerListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDFindNearbyGamerListModel *list = (PDFindNearbyGamerListModel *)responseObject;
            
            if (refresh){
                self.gamerTableView.currentPage = @"1";
                [strongSelf.gameMutableArr removeAllObjects];
            }
            
            if (list.items.count){
                [strongSelf.gameMutableArr addObjectsFromArray:list.items];
            }
            
            if (strongSelf.gameMutableArr.count){
                [strongSelf.gamerTableView dismissPrompt];
                [PDHUD showHUDProgress:@"熊猫君找到了好多小伙伴。" diary:2];
            } else {
                [strongSelf.gamerTableView showPrompt:@"没有附近的人" withImage: [UIImage imageNamed:@"bg_nearby_noHum"]  andImagePosition:PDPromptImagePositionTop tapBlock:^{
                    [strongSelf sendRequestToGetGamerInfo:YES];
                }];
                [PDHUD showHUDProgress:@"熊猫君找不到附近的小伙伴,换个姿势试试？" diary:2];
            }
            
            [strongSelf.gamerTableView reloadData];
            
            if ([strongSelf.gamerTableView.isXiaLa isEqualToString:@"YES"]){
                [strongSelf.gamerTableView stopPullToRefresh];
            } else {
                [strongSelf.gamerTableView stopFinishScrollingRefresh];
            }
            
            if (strongSelf.gamerTableView.contentSize.height < kScreenBounds.size.height - 64 - self.tabBarController.tabBar.size_height){
                [strongSelf.gamerTableView stopPullToRefresh];
                [strongSelf.gamerTableView stopFinishScrollingRefresh];
            }
        } else {
            [PDHUD showHUDProgress:@"熊猫君在寻找附近的小伙伴的过程中出现了错误" diary:2];
        }
    }];
}

#pragma mark - 关注
-(void)sendRequestToLinkWithId:(PDFindNearbyGamerSingleModel *)transferGameSingleModel{
    if (transferGameSingleModel.friends.followed){
        [PDHUD showHUDSuccess:@"请到详情中进行取消关注"];
        return;
    }
    
    NSString *alert = @"熊猫君正在拼命替你关注好友……";
    [PDHUD showHUDProgress:alert diary:0];
    
    NSString *userId = transferGameSingleModel.friends.friendInfo.friendId;
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerAddFollow requestParams:@{@"followedMemberId":userId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(self)strongSelf = weakSelf;
        if (isSucceeded){
            [PDHUD showHUDSuccess:@"好友关注成功"];
            transferGameSingleModel.friends.followed = YES;
            NSInteger index = [strongSelf.gameMutableArr indexOfObject:transferGameSingleModel];
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:index inSection:0];
            [strongSelf.gamerTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationBottom];
        } else {
            [PDHUD showHUDProgress:error.localizedDescription diary:2];
        }
    }];
}

@end
