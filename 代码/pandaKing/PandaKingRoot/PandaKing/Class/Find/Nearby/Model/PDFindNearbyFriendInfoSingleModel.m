//
//  PDFindNearbyFriendInfoSingleModel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFindNearbyFriendInfoSingleModel.h"

@implementation PDFindNearbyFriendInfoSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"friendId":@"id"};
}

@end
