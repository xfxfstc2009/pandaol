//
//  PDFindNearbyGamerListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDFindNearbyGamerSingleModel.h"
@interface PDFindNearbyGamerListModel : FetchModel


@property (nonatomic,assign)BOOL hasNextPage;              /**< 是否有下一页*/
@property (nonatomic,assign)BOOL hasPreviousPage;          /**< 是否有上一页*/
@property (nonatomic,strong)NSArray<PDFindNearbyGamerSingleModel> *items;
@property (nonatomic,assign)NSInteger pageCount;
@property (nonatomic,assign)NSInteger pageItemsCount;
@property (nonatomic,assign)NSInteger pageNum;
@property (nonatomic,assign)NSInteger totalItemsCount;
@end
