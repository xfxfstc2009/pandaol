//
//  PDFindNearbyFriendSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDFindNearbyFriendInfoSingleModel.h"

@interface PDFindNearbyFriendSingleModel : FetchModel
@property (nonatomic,assign)BOOL fans;                 /**< 他是不是我的粉丝*/
@property (nonatomic,assign)BOOL followed;             /**< 是否被我关注*/
@property (nonatomic,strong)PDFindNearbyFriendInfoSingleModel *friendInfo;

@end
