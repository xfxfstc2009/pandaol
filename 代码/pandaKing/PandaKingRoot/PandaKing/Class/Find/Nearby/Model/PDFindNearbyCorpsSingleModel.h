//
//  PDFindNearbyCorpsSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDFindNearbyCorpsSingleModel : FetchModel

@property (nonatomic,copy)NSString *locationName;               /**< 地区*/
@property (nonatomic,copy)NSString *avatar;                     /**< 图片*/
@property (nonatomic,copy)NSString *name;                       /**< 战队名字*/
@property (nonatomic,strong)NSArray *flagArr;                   /**< 标签数组*/
@property (nonatomic,copy)NSString *dymic;                      /**< 动态的内容*/
@property (nonatomic,copy)NSString *distance;                   /**< 距离*/
@property (nonatomic,copy)NSString *count;                      /**< 数量*/

@end
