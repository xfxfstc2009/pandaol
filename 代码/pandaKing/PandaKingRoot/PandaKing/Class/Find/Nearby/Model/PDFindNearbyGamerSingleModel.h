//
//  PDFindNearbyGamerSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDFindNearbyFriendSingleModel.h"
@protocol PDFindNearbyGamerSingleModel <NSObject>


@end

@interface PDFindNearbyGamerSingleModel : FetchModel

@property (nonatomic,copy)NSString *distance;                   /**< 距离*/
@property (nonatomic,strong)PDFindNearbyFriendSingleModel *friends;

@end
