//
//  PDFindNearbyGamerSingleModel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFindNearbyGamerSingleModel.h"

@implementation PDFindNearbyGamerSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"friends":@"friend"};
}

@end
