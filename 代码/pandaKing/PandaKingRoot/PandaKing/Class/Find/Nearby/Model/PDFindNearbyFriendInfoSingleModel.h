//
//  PDFindNearbyFriendInfoSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDFindNearbyFriendInfoSingleModel : FetchModel

@property (nonatomic,copy)NSString *address;                /**< 位置*/
@property (nonatomic,copy)NSString *age;                  /**< 年龄*/
@property (nonatomic,copy)NSString *avatar;                 /**< 头像*/
@property (nonatomic,strong)NSArray *bindGames;             /**< 绑定的游戏*/
@property (nonatomic,copy)NSString *cellphone;              /**< 号码*/
@property (nonatomic,assign)BOOL enable;
@property (nonatomic,copy)NSString *gender;
@property (nonatomic,copy)NSString *friendId;
@property (nonatomic,copy)NSString *nickname;
@property (nonatomic,copy)NSString *qq;
@property (nonatomic,assign)NSInteger registerTime;         /**< 注册时间*/
@property (nonatomic,copy)NSString *signature;              /**< 签名*/
@property (nonatomic,copy)NSString *weChat;                 /**< 微信*/

@property (nonatomic,assign)BOOL bindGameUser;              /**< 是否绑定*/
@property (nonatomic,assign)BOOL activateGameUser;          /**< 是否激活*/


@end
