//
//  PDPersonInfoHeaderView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPersonInfoHeaderView.h"

@interface PDPersonInfoHeaderView()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDImageView *bgImageView;
//@property (nonatomic,strong)
//@property (nonatomic,)

@end

@implementation PDPersonInfoHeaderView


-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.size_height);
    [self addSubview:self.bgView];

    
    CGSize finalSize = CGSizeMake(CGRectGetWidth(self.frame), self.size_height);
    CGFloat layerHeight = finalSize.height * 0.2;
    CAShapeLayer *layer = [CAShapeLayer layer];
    UIBezierPath *bezier = [UIBezierPath bezierPath];

    [bezier moveToPoint:CGPointMake(0, finalSize.height - layerHeight)];
    [bezier addLineToPoint:CGPointMake(0, finalSize.height - 1)];
    [bezier addLineToPoint:CGPointMake(finalSize.width, finalSize.height - 1)];
    [bezier addLineToPoint:CGPointMake(finalSize.width, finalSize.height - layerHeight)];
    [bezier addQuadCurveToPoint:CGPointMake(0,finalSize.height - layerHeight) controlPoint:CGPointMake(finalSize.width / 2, (finalSize.height - layerHeight) + 40)];
    [bezier closePath];
    layer.path = bezier.CGPath;
    

    layer.shadowColor = [[UIColor colorWithCustomerName:@"灰"] CGColor];
    layer.shadowOpacity = 3.0;
    layer.shadowOffset = CGSizeMake(-3.0f, -3.0f);
    layer.fillColor = [UIColor whiteColor].CGColor;
    [self.bgView.layer addSublayer:layer];
}




@end
