//
//  PDFindNearbyCorpsCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDFindNearbyCorpsSingleModel.h"
@interface PDFindNearbyCorpsCell : UITableViewCell

@property (nonatomic,strong)PDFindNearbyCorpsSingleModel *transferCorpsSingleModel;         /**< 传递的model*/
@property (nonatomic,assign)CGFloat transferCellHeight;

+(CGFloat)calculationCellHeight;

@end
