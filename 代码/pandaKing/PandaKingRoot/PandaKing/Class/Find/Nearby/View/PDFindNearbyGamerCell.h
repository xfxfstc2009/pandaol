//
//  PDFindNearbyGamerCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 附近的玩家
#import <UIKit/UIKit.h>
#import "PDFindNearbyGamerSingleModel.h"

typedef void(^block)(PDFindNearbyGamerSingleModel *transferGameSingleModel);

@interface PDFindNearbyGamerCell : UITableViewCell

@property (nonatomic,strong)PDFindNearbyGamerSingleModel *transferGameSingleModel;
@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,copy)block linkBlock;

+(CGFloat)calculationCellHeight;

@end
