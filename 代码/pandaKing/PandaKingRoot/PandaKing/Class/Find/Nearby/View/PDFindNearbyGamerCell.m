//
//  PDFindNearbyGamerCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFindNearbyGamerCell.h"

@interface PDFindNearbyGamerCell()
@property (nonatomic,strong)PDImageView *avatarImgView;             /**< 人头像*/
@property (nonatomic,strong)PDImageView *alphaImageView;            /**< 蒙层view*/
@property (nonatomic,strong)UILabel *nameLabel;                     /**< 名字label */
@property (nonatomic,strong)UILabel *descLabel;                     /**< 详情*/
@property (nonatomic,strong)PDImageView *locationIconImg;           /**< 位置img*/
@property (nonatomic,strong)UILabel *distanceLabel;                 /**< 距离*/
@property (nonatomic,strong)UIView *genderView;                     /**< 年龄view*/
@property (nonatomic,strong)PDImageView *genderIcon;                /**< 年龄的icon*/
@property (nonatomic,strong)UILabel *ageLabel;                      /**< 年龄的label*/
@property (nonatomic,strong)UIButton *linkButton;                   /**< 关注按钮*/
@property (nonatomic,strong)PDImageView *activityImgView;
@property (nonatomic,strong)PDImageView *bindingImgView;

@end

@implementation PDFindNearbyGamerCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    self.alphaImageView = [[PDImageView alloc]init];
    self.alphaImageView.backgroundColor = [UIColor clearColor];
    self.alphaImageView.image = [UIImage imageNamed:@"icon_nearby_alpha"];
    [self addSubview:self.alphaImageView];
    
    // 2. 创建label
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"]boldFont];
    [self addSubview:self.nameLabel];
    
    // 3. 创建descLabel
    self.descLabel = [[UILabel alloc]init];
    self.descLabel.backgroundColor = [UIColor clearColor];
    self.descLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.descLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.descLabel.numberOfLines = 0;
    [self addSubview:self.descLabel];
    
    // 4. 创建icon
    self.locationIconImg = [[PDImageView alloc]init];
    self.locationIconImg.backgroundColor = [UIColor clearColor];
    self.locationIconImg.image = [UIImage imageNamed:@"icon_nearby_location"];
    [self addSubview:self.locationIconImg];
    
    // 5. 创建location
    self.distanceLabel = [[UILabel alloc]init];
    self.distanceLabel.backgroundColor = [UIColor clearColor];
    self.distanceLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.distanceLabel.textColor = c15;
    [self addSubview:self.distanceLabel];
    
    // 6. 创建关注按钮
    self.linkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.linkButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [weakSelf.linkButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.linkBlock){
            strongSelf.linkBlock(strongSelf.transferGameSingleModel);
        }
    }];
    [self addSubview:self.linkButton];
    
    // 7. 创建年龄图标
    [self createViewLabelWithGender];
    
    // 8 . 创建激活图标
    self.activityImgView = [[PDImageView alloc]init];
    self.activityImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.activityImgView];
    
    // 9 .创建绑定
    self.bindingImgView = [[PDImageView alloc]init];
    self.bindingImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bindingImgView];
    
    // 10 . 重新修改frame
    
    
    
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferGameSingleModel:(PDFindNearbyGamerSingleModel *)transferGameSingleModel{
    _transferGameSingleModel = transferGameSingleModel;
    
//    // 1. 头像
    self.avatarImgView.frame = CGRectMake(LCFloat(11), LCFloat(5), (self.transferCellHeight - 2 * LCFloat(5)), (self.transferCellHeight - 2 * LCFloat(5)));
    [self.avatarImgView uploadImageWithURL:self.transferGameSingleModel.friends.friendInfo.avatar placeholder:nil callback:NULL];

    self.alphaImageView.frame = self.avatarImgView.frame;

    // 2. 创建名字
    NSString *tempString = transferGameSingleModel.friends.friendInfo.nickname;
    
    self.nameLabel.text = tempString;
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), self.avatarImgView.orgin_y,kScreenBounds.size.width - CGRectGetMaxX(self.avatarImgView.frame) - 100 , [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    // 3. 创建距离
    self.distanceLabel.text = [NSString stringWithFormat:@"距离%@",transferGameSingleModel.distance];
    self.distanceLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(10), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.distanceLabel.font]);
    
    
    // 4. 创建按钮
    self.linkButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(30), (self.transferCellHeight - LCFloat(30)) / 2., LCFloat(30), LCFloat(30));

//    icon_friend_followed
    if (transferGameSingleModel.friends.fans && transferGameSingleModel.friends.followed){          // 是否是粉丝
        [self.linkButton setImage:[UIImage imageNamed:@"icon_friend_followeach"] forState:UIControlStateNormal];
    } else {
        if (transferGameSingleModel.friends.followed){
            [self.linkButton setImage:[UIImage imageNamed:@"icon_friend_followed"] forState:UIControlStateNormal];
        } else {
            [self.linkButton setImage:[UIImage imageNamed:@"icon_friend_addfollow"] forState:UIControlStateNormal];
        }
    }
    
    // 5. 创建图标
    BOOL gender = [transferGameSingleModel.friends.friendInfo.gender isEqualToString:@"MALE"] ?YES :NO;
    [self autoUpdateInfoGender:gender Str:transferGameSingleModel.friends.friendInfo.age];
    
    // 6.
    if (self.transferGameSingleModel.friends.friendInfo.bindGameUser){
        self.bindingImgView.image = [UIImage imageNamed:@"icon_find_bind_on"];
    } else {
        self.bindingImgView.image = [UIImage imageNamed:@"icon_find_bind_off"];
    }
    if (self.transferGameSingleModel.friends.friendInfo.activateGameUser){
        self.activityImgView.image = [UIImage imageNamed:@"icon_find_auth_on"];
    } else {
        self.activityImgView.image = [UIImage imageNamed:@"icon_find_auth_off"];
    }
    
    
    // 3.1 获取margin
   CGFloat margin = (self.transferCellHeight - [NSString contentofHeightWithFont:self.nameLabel.font] - [NSString contentofHeightWithFont:self.distanceLabel.font] - self.genderView.size_height) / 4.;
    self.nameLabel.orgin_y = margin;
    self.genderView.orgin_x = self.nameLabel.orgin_x;
    self.genderView.orgin_y = CGRectGetMaxY(self.nameLabel.frame) + margin;
    
    self.bindingImgView.frame = CGRectMake(CGRectGetMaxX(self.genderView.frame) + LCFloat(11), 0, LCFloat(13), LCFloat(13));
    self.bindingImgView.center_y = self.genderView.center_y;
    
    self.activityImgView.frame = CGRectMake(CGRectGetMaxX(self.bindingImgView.frame) + LCFloat(11), 9, LCFloat(13), LCFloat(13));
    self.activityImgView.center_y = self.bindingImgView.center_y;
    
    self.distanceLabel.orgin_y = CGRectGetMaxY(self.genderView.frame) + margin;
    
    // 3.2locationIconImg
    self.locationIconImg.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + margin + (ABS([NSString contentofHeightWithFont:self.distanceLabel.font] - LCFloat(9))) / 2., LCFloat(9), LCFloat(9));
    
    self.locationIconImg.center_y = self.distanceLabel.center_y;
    
    self.distanceLabel.orgin_x = CGRectGetMaxX(self.locationIconImg.frame) + LCFloat(5);
    
}

-(void)createViewLabelWithGender{
    self.genderView = [[UIView alloc]init];
    self.genderView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.genderView];
    
    // 1. 创建性别图标
    self.genderIcon = [[PDImageView alloc]init];
    self.genderIcon.image = [UIImage imageNamed:@"icon_nearby_girl"];
    self.genderIcon.frame = CGRectMake(0, 0, LCFloat(8), LCFloat(8));
    [self.genderView addSubview:self.genderIcon];
    
    // 2. 创建label
    self.ageLabel = [[UILabel alloc]init];
    self.ageLabel.backgroundColor = [UIColor clearColor];
    self.ageLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.ageLabel.textColor = [UIColor whiteColor];
    [self.genderView addSubview:self.ageLabel];
}

-(void)autoUpdateInfoGender:(BOOL)gender Str:(NSString *)str{
    if (gender == YES){             // 男
        self.genderIcon.image = [UIImage imageNamed:@"icon_nearby_boy"];
        self.genderView.backgroundColor = [UIColor colorWithCustomerName:@"浅蓝"];
    } else {                        // 女
        self.genderIcon.image = [UIImage imageNamed:@"icon_nearby_girl"];
        self.genderView.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
    }
    
    self.ageLabel.text = str;
    CGSize contentOfAgeSize = [self.ageLabel.text sizeWithCalcFont:self.ageLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.ageLabel.font])];
    
    CGFloat width = contentOfAgeSize.width + LCFloat(5) * 2 + LCFloat(2) + self.genderIcon.size_width;
    CGFloat height = [NSString contentofHeightWithFont:self.ageLabel.font] + 2 * LCFloat(2);
    
    self.genderIcon.frame = CGRectMake(LCFloat(5), (height - self.genderIcon.size_height ) / 2., self.genderIcon.size_width, self.genderIcon.size_height);
    self.ageLabel.frame = CGRectMake(CGRectGetMaxX(self.genderIcon.frame) + LCFloat(2), LCFloat(2), contentOfAgeSize.width, [NSString contentofHeightWithFont:self.ageLabel.font]);
    self.genderView.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + LCFloat(11), self.nameLabel.orgin_y, width, height);
    
    // 修改frame
    self.genderView.clipsToBounds = YES;
    self.genderView.layer.cornerRadius = (MIN(self.genderView.size_width, self.genderView.size_height)) / 2.;
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(45);
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(10);
    return cellHeight;
}

@end
