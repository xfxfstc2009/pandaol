//
//  PDFindNearbyCorpsCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFindNearbyCorpsCell.h"

@interface PDFindNearbyCorpsCell()
@property (nonatomic,strong)UILabel *nameLabel;                 /**< 社区名*/
@property (nonatomic,strong)UIView *lineView;                   /**< 线*/
@property (nonatomic,strong)PDImageView *avatarView;            /**< 战队头像*/
@property (nonatomic,strong)UILabel *corpsLabel;                /**< 战队名字*/
@property (nonatomic,strong)UIView *flagView;                   /**< 标签view*/
@property (nonatomic,strong)UILabel *dymicLabel;                /**< 动态label*/
@property (nonatomic,strong)PDImageView *corpsCountImageView;   /**< 战队人数*/
@property (nonatomic,strong)UILabel *corpsCountLabel;           /**< 战队人数*/
@property (nonatomic,strong)UILabel *distanceLabel;             /**< 距离label*/

@end

@implementation PDFindNearbyCorpsCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 社区名字
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.nameLabel];
    
    // 2. 创建距离
    self.distanceLabel = [[UILabel alloc]init];
    self.distanceLabel.backgroundColor = [UIColor clearColor];
    self.distanceLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.distanceLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.distanceLabel];
    
    // 3. 创建line
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.lineView];
    
    // 4. 创建头像
    self.avatarView = [[PDImageView alloc]init];
    self.avatarView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarView];
    
    // 5. 创建名字
    self.corpsLabel = [[UILabel alloc]init];
    self.corpsLabel.backgroundColor = [UIColor clearColor];
    self.corpsLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"] boldFont];
    [self addSubview:self.corpsLabel];
    
    // 6. 创建标签图
    self.flagView = [[UIView alloc]init];
    self.flagView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.flagView];
    
    // 7.创建动态label
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.dymicLabel];
    
    // 8.创建img
    self.corpsCountImageView = [[PDImageView alloc]init];
    self.corpsCountImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.corpsCountImageView];
    
    // 9.创建战队人数
    self.corpsCountLabel = [[UILabel alloc]init];
    self.corpsCountLabel.backgroundColor = [UIColor clearColor];
    self.corpsCountLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.corpsCountLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight = 100;
    return cellHeight;
}
@end
