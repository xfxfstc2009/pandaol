//
//  PDFindRootViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFindRootViewController.h"
#import "PDFindRootNearbyViewController.h"              // 附近
#import <HTHorizontalSelectionList.h>
#import "PDFindInformationViewController.h"             // 资讯信息
#import "PDZoneViewController.h"                        // 圈子控制器
#import "PDMessageViewController.h"                     // 消息控制器
#import "UIView+PDBadgeNumber.h"

@class PDFindInformationViewController;

@interface PDFindRootViewController()<HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate,UIScrollViewDelegate,UINavigationControllerDelegate,UINavigationBarDelegate>
@property (nonatomic,strong)HTHorizontalSelectionList *segmentRootList;
@property (nonatomic,strong)NSArray *segmentArr;
@property (nonatomic,strong)UIScrollView *mainScrollView;

@property (nonatomic,strong)PDFindInformationViewController *findInformationViewController;         /**< 资讯控制器*/
@property (nonatomic,strong)PDZoneViewController *zoneViewController;                               /**< 圈子控制器*/
@property (nonatomic, strong) UIButton *rightButton;
@end

@implementation PDFindRootViewController

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    if (self.segmentRootList && self.mainScrollView){
        [self.segmentRootList setSelectedButtonIndex:[AccountModel sharedAccountModel].findType animated:YES];
        [self.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * [AccountModel sharedAccountModel].findType, 0) animated:NO];
    }
    
    [self fetchMessageState];
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor clearColor].CGColor;
}

+(instancetype)sharedController{
    static PDFindRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[PDFindRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self createSegmentList];
    [self pageSetting];
    [self createScrollView];
    [self createInformationController];                     // 添加资讯
    [self createCricleInformationController];               // 添加圈子

}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.segmentArr = @[@"发现",@"圈子"];
}


#pragma mark - pageSetting
-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
    self.rightButton = [weakSelf rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_panda_message_nor"] barHltImage:[UIImage imageNamed:@"icon_panda_message_nor"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDMessageViewController *messageViewController = [[PDMessageViewController alloc] init];
        [strongSelf pushViewController:messageViewController animated:YES];
        
        // 取消红点
        [strongSelf.rightButton clearNormalBadge];
        
    }];
}

#pragma mark - createSegmentList
-(void)createSegmentList{
    if (!self.segmentRootList){
        self.segmentRootList = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake((kScreenBounds.size.width - LCFloat(150)) / 2., (LCFloat(44) - LCFloat(35)) / 2. ,LCFloat(150) , LCFloat(35))];
        self.segmentRootList.delegate = self;
        self.segmentRootList.dataSource = self;
        self.segmentRootList.backgroundColor = [UIColor clearColor];
        [self.segmentRootList setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
        self.segmentRootList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"白"];
        self.segmentRootList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeHeavyBounce;
        self.segmentRootList.bottomTrimColor = [UIColor clearColor];
        [self.segmentRootList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];

        self.segmentRootList.isNotScroll = YES;
        self.segmentRootList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleButtonBorder;
        self.navigationItem.titleView = self.segmentRootList;
    }
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * index, 0) animated:YES];
    // 保存当前信息
    [AccountModel sharedAccountModel].findType = index;
}


#pragma mark - UIScrollView
-(void)createScrollView{
    self.mainScrollView = [[UIScrollView alloc]init];
    self.mainScrollView.frame = kScreenBounds;
    self.mainScrollView.delegate = self;
    self.mainScrollView.backgroundColor = [UIColor redColor];
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width , self.segmentArr.count);
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.showsVerticalScrollIndicator = NO;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.mainScrollView];
}

#pragma mark - 加载资讯
- (void)createInformationController{
    self.findInformationViewController = [[PDFindInformationViewController alloc] init];
    self.findInformationViewController.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.mainScrollView.size_height);
    [self.mainScrollView addSubview:self.findInformationViewController.view];
    [self addChildViewController:self.findInformationViewController];
}

#pragma mark - 加载圈子
-(void)createCricleInformationController {
    self.zoneViewController = [[PDZoneViewController alloc]init];
    self.zoneViewController.view.frame = CGRectMake(kScreenBounds.size.width, 0, kScreenBounds.size.width, kScreenBounds.size.height - 64 - 49);
    [self.mainScrollView addSubview:self.zoneViewController.view];
    [self addChildViewController:self.zoneViewController];
}

#pragma mark -- 读取消息盒子状态

- (void)fetchMessageState {
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageState requestParams:nil responseObjectClass:[PDMessageState class] succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDMessageState *state = (PDMessageState *)responseObject;
            // 重置
            [AccountModel sharedAccountModel].messageState = state;
            
            if (state.hasUnReadMsg) {
                [weakSelf.rightButton addNormalBadgeWithColor:[UIColor redColor] borderColor:[UIColor clearColor]];
            } else {
                [weakSelf.rightButton clearNormalBadge];
            }
        }
    }];
}

@end
