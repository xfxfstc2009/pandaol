//
//  PDZoneSearchMoreCell.m
//  PandaKing
//
//  Created by Cranz on 17/3/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDZoneSearchMoreCell.h"

@interface PDZoneSearchMoreCell ()
@property (nonatomic, strong) PDImageView *iconImageView;
@property (nonatomic, strong) UILabel *mainLabel;
@property (nonatomic, strong) UILabel *subLabel;
@end

@implementation PDZoneSearchMoreCell

+ (CGFloat)cellHeight {
    return 55;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    
    [self cellSetting];
    return self;
}

- (void)cellSetting {
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    CGFloat imageViewWidth = [PDZoneSearchMoreCell cellHeight] - 10 * 2;
    self.iconImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 10, imageViewWidth, imageViewWidth)];
    self.iconImageView.layer.cornerRadius = imageViewWidth / 2;
    self.iconImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.iconImageView];
    
    self.mainLabel = [[UILabel alloc] init];
    self.mainLabel.font = [UIFont systemFontOfCustomeSize:15];
    [self.contentView addSubview:self.mainLabel];
    
    self.subLabel = [[UILabel alloc] init];
    self.subLabel.font = [UIFont systemFontOfCustomeSize:11];
    self.subLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.subLabel];
}

- (void)setModel:(PDZoneSearchMoreModel *)model {
    _model = model;
    
    self.iconImageView.image = model.icon;
    self.mainLabel.text = model.mainTitle;
    self.subLabel.text = model.subTitle;
    
    CGSize mainSize = [self.mainLabel.text sizeWithCalcFont:self.mainLabel.font];
    CGSize subSize = [self.subLabel.text sizeWithCalcFont:self.subLabel.font];
    
    // 主标题和副标题之间的间隙
    CGFloat space = 4;
    CGFloat mainTitleY = ([PDZoneSearchMoreCell cellHeight] - mainSize.height - space - subSize.height) / 2;
    
    self.mainLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImageView.frame) + 10, mainTitleY, mainSize.width, mainSize.height);
    self.subLabel.frame = CGRectMake(CGRectGetMinX(self.mainLabel.frame), CGRectGetMaxY(self.mainLabel.frame) + space, subSize.width, subSize.height);
}

@end
