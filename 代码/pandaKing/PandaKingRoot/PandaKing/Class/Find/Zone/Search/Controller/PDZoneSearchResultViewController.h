//
//  PDZoneSearchResultViewController.h
//  PandaKing
//
//  Created by Cranz on 17/3/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 查找结果显示控制器
@interface PDZoneSearchResultViewController : AbstractViewController

@end
