//
//  PDZoneSearchingCell.h
//  PandaKing
//
//  Created by Cranz on 17/3/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDZoneSearchingCell : UITableViewCell

+ (CGFloat)cellHeight;

- (void)didSearchCellBecomeFirstRegister:(void(^)())complication;
@end
