//
//  PDZoneSearchMoreModel.m
//  PandaKing
//
//  Created by Cranz on 17/3/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDZoneSearchMoreModel.h"

@implementation PDZoneSearchMoreModel
+ (NSArray *)fetchData {
    PDZoneSearchMoreModel *model1 = [[self alloc] init];
    model1.icon = [UIImage imageNamed:@"icon_zone_search_nearby"];
    model1.mainTitle = @"附近的玩家";
    model1.subTitle = @"添加身边的朋友";
    
//    PDZoneSearchMoreModel *model2 = [[self alloc] init];
//    model2.icon = [UIImage imageNamed:@"icon_zone_search_recommend"];
//    model2.mainTitle = @"推荐玩家";
//    model2.subTitle = @"认识活跃的平台大神";
    
    return @[model1];
}
@end
