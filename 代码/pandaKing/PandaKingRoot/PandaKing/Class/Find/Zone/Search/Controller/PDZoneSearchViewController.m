//
//  PDZoneSearchViewController.m
//  PandaKing
//
//  Created by Cranz on 17/3/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDZoneSearchViewController.h"
#import "PDFindRootNearbyViewController.h"
#import "PDZoneSearchingCell.h"
#import "PDZoneSearchMoreCell.h"
#import "PDZoneSearchResultViewController.h"

@interface PDZoneSearchViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;

@property (nonatomic, strong) NSArray *moreArr;
@end

@implementation PDZoneSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
}

- (void)pageSetting {
    self.barMainTitle = @"查找好友";
    
    // 获取固定数据
    self.moreArr = [PDZoneSearchMoreModel fetchData];
    
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.separatorColor = c27;
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.tableFooterView = [UIView new];
    [self.view addSubview:self.mainTableView];
}

#pragma mark - UITableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else {
        return self.moreArr.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        static NSString *zoneSearchcCellId = @"zoneSearchcCellId";
        PDZoneSearchingCell *searchCell = [tableView dequeueReusableCellWithIdentifier:zoneSearchcCellId];
        if (!searchCell) {
            searchCell = [[PDZoneSearchingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:zoneSearchcCellId];
        }
        
        __weak typeof(self) weakSelf = self;
        [searchCell didSearchCellBecomeFirstRegister:^{
            [weakSelf goSearchResult];
        }];
        
        return searchCell;
    } else {
        static NSString *zoneMoreCellId = @"zoneMoreCellId";
        PDZoneSearchMoreCell *moreCell = [tableView dequeueReusableCellWithIdentifier:zoneMoreCellId];
        if (!moreCell) {
            moreCell = [[PDZoneSearchMoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:zoneMoreCellId];
        }
        moreCell.model = self.moreArr[indexPath.row];
        return moreCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [PDZoneSearchingCell cellHeight];
    } else {
        return [PDZoneSearchMoreCell cellHeight];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 1) {
        if (indexPath.row == 0) { // 附近的玩家
            [self GoNearBy];
        } else { // 推荐玩家
            
        }
    }
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];
}

- (void)GoNearBy {
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        PDFindRootNearbyViewController *nearPlayerViewController = [[PDFindRootNearbyViewController alloc] init];
        [self pushViewController:nearPlayerViewController animated:YES];
    } else {
        __weak typeof(self)weakSelf = self;
        [self showLoginViewControllerWithBlock:^{
            if (!weakSelf){
                return;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDFindRootNearbyViewController *nearPlayerViewController = [[PDFindRootNearbyViewController alloc] init];
            [strongSelf pushViewController:nearPlayerViewController animated:YES];
        }];
    }
}

#pragma mark - 跳转查找结果界面

- (void)goSearchResult {
    
    PDZoneSearchResultViewController *searchResultViewController = [[PDZoneSearchResultViewController alloc] init];
    PDNavigationController *navi = [[PDNavigationController alloc] initWithRootViewController:searchResultViewController];
    [self presentViewController:navi animated:NO completion:nil];
}

@end
