//
//  PDZoneSearchingCell.m
//  PandaKing
//
//  Created by Cranz on 17/3/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDZoneSearchingCell.h"

typedef void(^PDZoneSearchBlock)();

@interface PDZoneSearchingCell ()
@property (nonatomic, strong) UIButton *searchButton;
@property (nonatomic, copy) PDZoneSearchBlock searchBlock;
@end

@implementation PDZoneSearchingCell

+ (CGFloat)cellHeight {
    return 44;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    
    [self cellSetting];
    return self;
}

- (void)cellSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.searchButton = [[UIButton alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, ([PDZoneSearchingCell cellHeight] - 30) / 2, kScreenBounds.size.width - 2 * kTableViewSectionHeader_left, 30)];
    [self.searchButton setTitle:@" 终身ID/手机号/昵称" forState:UIControlStateNormal];
    self.searchButton.titleLabel.font = [UIFont systemFontOfCustomeSize:16];
    [self.searchButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
    [self.searchButton setImage:[UIImage imageNamed:@"icon_zone_search_search"] forState:UIControlStateNormal];
    [self.contentView addSubview:self.searchButton];
    
    [self.searchButton addTarget:self action:@selector(didClickSearch:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didClickSearch:(UIButton *)button {
    if (self.searchBlock) {
        self.searchBlock();
    }
}

- (void)didSearchCellBecomeFirstRegister:(void (^)())complication {
    if (complication) {
        self.searchBlock = complication;
    }
}


@end
