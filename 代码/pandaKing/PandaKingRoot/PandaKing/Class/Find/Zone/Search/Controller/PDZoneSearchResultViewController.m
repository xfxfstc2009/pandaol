//
//  PDZoneSearchResultViewController.m
//  PandaKing
//
//  Created by Cranz on 17/3/8.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDZoneSearchResultViewController.h"
#import "PDCenterFriendList.h"
#import "PDCenterFriendTableViewCell.h" // 搜索结果显示控制器
#import "PDZoneSearchMoreCell.h"
#import "PDFindRootNearbyViewController.h" // 附近的玩家
#import "PDCenterPersonViewController.h" // 用户主页

typedef NS_ENUM(NSUInteger, PDSearchResultType) {
    PDSearchResultTypeNone, // 一开始
    PDSearchResultTypeNotFind, // 未找到
    PDSearchResultTypeFinded, // 找到
};

@interface PDZoneSearchResultViewController ()<UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate, PDCenterFriendTableViewCellDelegate>
@property (nonatomic, strong) UIView *titleBar;
@property (nonatomic, strong) UISearchBar *searchBar;
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, assign) CGFloat searchBarWidth;
@property (nonatomic, assign) NSUInteger page;

@property (nonatomic, strong) PDCenterFriendList *searchList;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, assign) PDSearchResultType type;
@property (nonatomic, copy) NSString *currentText; // 当前显示的字

@property (nonatomic, assign) BOOL hasShow; // 已经显示了
@end

@implementation PDZoneSearchResultViewController

- (NSMutableArray *)itemsArr {
    if (!_itemsArr) {
        _itemsArr = [NSMutableArray array];
    }
    return _itemsArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self searchBarSetting];
    [self tableViewSetting];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    if (!self.hasShow) {
        [self showSearchBarWithAnimatied:YES];
    }
}

- (void)pageSetting {
    self.type = PDSearchResultTypeNone;
    self.hasShow = NO;
    
    self.titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    self.titleBar.backgroundColor = c2;
    [self.view addSubview:self.titleBar];
    
    // 右侧取消按钮
    NSString *title = @"取消";
    UIFont *font = [UIFont systemFontOfCustomeSize:16];
    CGSize titleSize = [title sizeWithCalcFont:font];
    CGSize buttonSize = CGSizeMake(titleSize.width, 40);
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancleButton.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - buttonSize.width, 20 + (44 - buttonSize.height) / 2, buttonSize.width, buttonSize.height);
    [cancleButton setTitle:title forState:UIControlStateNormal];
    cancleButton.titleLabel.font = font;
    [self.titleBar addSubview:cancleButton];
    
    self.searchBarWidth = kScreenBounds.size.width - kTableViewSectionHeader_left * 3 - buttonSize.width;
    
    [cancleButton addTarget:self action:@selector(didClickCancle:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)searchBarSetting {
    self.searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleBar.frame), kScreenBounds.size.width, 30)];
    self.searchBar.placeholder = @"终身ID/手机号/昵称";
    self.searchBar.delegate = self;
    self.searchBar.backgroundImage = [UIImage imageWithRenderColor:[UIColor clearColor] renderSize:self.searchBar.bounds.size];
    [self.view addSubview:self.searchBar];
}

- (void)tableViewSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.titleBar.frame), kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetHeight(self.titleBar.frame)) style:UITableViewStyleGrouped];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.separatorColor = c27;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self.view addSubview:self.mainTableView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.searchList.hasNextPage == YES) {
            weakSelf.page++;
            [weakSelf fetchDataWithText:weakSelf.currentText];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
        }
        
    }];
}

- (void)didClickCancle:(UIButton *)button {
    [PDHUD dismiss];
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - SearchBar 动画

- (void)showSearchBarWithAnimatied:(BOOL)animated {
    [UIView animateWithDuration:animated? 0.15 : 0 animations:^{
        self.searchBar.frame = CGRectMake(kTableViewSectionHeader_left, 20 + (44 - 30) / 2, self.searchBarWidth, 30);
    } completion:^(BOOL finished) {
        [self.searchBar becomeFirstResponder];
        self.hasShow = YES;
    }];
}

#pragma mark - UISearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    _page = 1;
    self.currentText = searchBar.text;
    [PDHUD showHUDProgress:@"正在搜索..." diary:0];
    // 在搜索之前清空数组
    if (self.itemsArr.count != 0) {
        [self.itemsArr removeAllObjects];
    }
    
    [self.view endEditing:YES];
    [self fetchDataWithText:searchBar.text];
}

#pragma mark - UITabelView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (_type == PDSearchResultTypeNone) {
        return 0;
    } else if (_type == PDSearchResultTypeNotFind) {
        return 2;
    } else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (_type == PDSearchResultTypeNone) {
        return 0;
    } else if (_type == PDSearchResultTypeNotFind) {
        return 1;
    } else {
        return self.itemsArr.count;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_type == PDSearchResultTypeNone) {
        return 0;
    } else if (_type == PDSearchResultTypeNotFind) {
        if (indexPath.section == 0) {
            return 80;
        } else {
            return [PDZoneSearchMoreCell cellHeight];
        }
    } else {
        return [PDCenterFriendTableViewCell cellHeight];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (_type == PDSearchResultTypeNone) {
        return nil;
    }
    if (_type == PDSearchResultTypeFinded) {
        NSString *searchHasResultCellId = @"searchHasResultCellId";
        PDCenterFriendTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:searchHasResultCellId];
        if (!cell) {
            cell = [[PDCenterFriendTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchHasResultCellId];
            cell.delegate = self;
        }
        
        cell.indexPath = indexPath;
        if (self.itemsArr.count > indexPath.row) {
            cell.model = self.itemsArr[indexPath.row];
        }
        return cell;
    } else { /// 没有结果，高度为80
        if (indexPath.section == 0) {
            NSString *searchNoneCellId = @"searchNoneCellId";
            UITableViewCell *noneCell = [tableView dequeueReusableCellWithIdentifier:searchNoneCellId];
            if (!noneCell) {
                noneCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchNoneCellId];
            
                // 创建label
                UILabel *label = [[UILabel alloc] init];
                label.stringTag = @"text_label";
                label.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
                label.textColor = [UIColor lightGrayColor];
                [noneCell.contentView addSubview:label];
            }
            
            UILabel *textLabel = (UILabel *)[noneCell viewWithStringTag:@"text_label"];
            textLabel.text = @"该用户不存在";
            CGSize textSize = [textLabel.text sizeWithCalcFont:textLabel.font];
            textLabel.frame = CGRectMake((kScreenBounds.size.width - textSize.width) / 2, (80 - textSize.height) / 2, textSize.width, textSize.height);
            
            return noneCell;
        } else {
            NSString *searchNearCellId = @"searchNearCellId";
            PDZoneSearchMoreCell *moreCell = [tableView dequeueReusableCellWithIdentifier:searchNearCellId];
            if (!moreCell) {
                moreCell = [[PDZoneSearchMoreCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:searchNearCellId];
            }
            moreCell.model = [[PDZoneSearchMoreModel fetchData] firstObject];
            return moreCell;
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.view endEditing:YES];
    
    if (_type == PDSearchResultTypeNotFind) {
        if (indexPath.section == 1) {
            [self GoNearBy];
        }
    } else { // 找到
        PDCenterFriendItem *friendItem = self.itemsArr[indexPath.row];
        PDCenterPersonViewController *personViewController = [[PDCenterPersonViewController alloc] init];
        personViewController.transferMemberId = friendItem.friendInfo.ID;
        [self pushViewController:personViewController animated:YES];
    }
}

- (void)GoNearBy {
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        PDFindRootNearbyViewController *nearPlayerViewController = [[PDFindRootNearbyViewController alloc] init];
        [self pushViewController:nearPlayerViewController animated:YES];
    } else {
        __weak typeof(self)weakSelf = self;
        [self showLoginViewControllerWithBlock:^{
            if (!weakSelf){
                return;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDFindRootNearbyViewController *nearPlayerViewController = [[PDFindRootNearbyViewController alloc] init];
            [strongSelf pushViewController:nearPlayerViewController animated:YES];
        }];
    }
}

#pragma mark - PDCenterFriendTableViewCellDelegate

- (void)tableViewCell:(PDCenterFriendTableViewCell *)cell didClickButtonAtIndexPath:(NSIndexPath *)indexPath {
    PDCenterFriendItem *friendItem = self.itemsArr[indexPath.row];
    [self addFollowWithMemberId:friendItem.friendInfo.ID indexPath:indexPath friendItem:friendItem];
}

#pragma mark - 网络请求
 // 17088742845
- (void)fetchDataWithText:(NSString *)text {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:zoneSearchPlayer requestParams:@{@"pageNum":@(_page),@"param":text} responseObjectClass:[PDCenterFriendList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        [PDHUD dismiss];
        [weakSelf.mainTableView stopFinishScrollingRefresh];
        
        if (isSucceeded) {
            weakSelf.searchList = (PDCenterFriendList *)responseObject;
            
            [weakSelf.itemsArr addObjectsFromArray:weakSelf.searchList.items];
            
            // 对搜索结果做个记录
            if (weakSelf.searchList.items.count == 0) {
                weakSelf.type = PDSearchResultTypeNotFind;
            } else {
                weakSelf.type = PDSearchResultTypeFinded;
            }
            
            [weakSelf.mainTableView reloadData];
        }
    }];
}

/** 添加关注*/
- (void)addFollowWithMemberId:(NSString *)memberId indexPath:(NSIndexPath *)indexPath friendItem:(PDCenterFriendItem *)item {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerAddFollow requestParams:@{@"followedMemberId": memberId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            // 修改关注状态
            item.followed = YES;
            
            // 更新cell
            [weakSelf.mainTableView beginUpdates];
            [weakSelf.mainTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [weakSelf.mainTableView endUpdates];
            
            [PDHUD showHUDSuccess:@"关注成功!"];
        }
    }];
}

@end
