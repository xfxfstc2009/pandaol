//
//  PDZoneAppFunctionModel.h
//  PandaKing
//
//  Created by Cranz on 17/5/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDZoneAppFunctionModel : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL enable;
@property (nonatomic, assign) BOOL jumpToUrl;
@property (nonatomic, copy) NSString *url;
@end
