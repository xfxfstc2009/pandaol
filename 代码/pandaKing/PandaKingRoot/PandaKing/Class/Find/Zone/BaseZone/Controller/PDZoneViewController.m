//
//  PDZoneViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDZoneViewController.h"
#import "PDButton.h"
#import "PDFindRootNearbyViewController.h"
#import "PDCenterFriendViewController.h"
#import "PDMessageViewController.h"
#import "PDZoneSearchViewController.h"
#import "PDZoneAppFunctionModel.h"

#import "PDShopRootMainViewController.h"

@interface PDZoneViewController ()
@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, strong) PDZoneAppFunctionModel *appFunc;
@end

@implementation PDZoneViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self setupRightItem];
    [self fetchBlackTeach];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self fetchMessageState];
}

- (void)basicSetting {
    self.barMainTitle = @"社交";
}

- (void)setupRightItem {
    if (self == [self.navigationController.viewControllers firstObject]){
        __weak typeof(self)weakSelf = self;
        self.tempMsgButton = [weakSelf rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_panda_message_nor"] barHltImage:[UIImage imageNamed:@"icon_panda_message_nor"] action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDMessageViewController *messageViewController = [[PDMessageViewController alloc]init];
            [messageViewController setHidesBottomBarWhenPushed:YES];
            [strongSelf.navigationController pushViewController:messageViewController animated:YES];
        }];
        [self.tempMsgButton sizeToFit];
    }
}

- (void)pageSetting {
    [self buttonViewSetting];
    [self tableViewSetting];
}

- (void)buttonViewSetting {
    self.buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, kTableViewHeader_height, kScreenBounds.size.width, 100)];
    self.buttonView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.buttonView];
    
    CGFloat buttonWidth = kScreenBounds.size.width / 3;
    for (int i = 0; i < 3; i++) {
        PDButton *button = [PDButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(i * buttonWidth, 0, buttonWidth, CGRectGetHeight(self.buttonView.frame));
        button.tag = i;
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
        if (i == 0) {
            [button setTitle:@"撸友" forState:UIControlStateNormal];
            button.imageView.image = [UIImage imageNamed:@"icon_zone_friend"];
        } else if (i == 1) {
            [button setTitle:@"黑科技" forState:UIControlStateNormal];
            button.imageView.image = [UIImage imageNamed:@"icon_zone_science"];
        } else {
            [button setTitle:@"查找" forState:UIControlStateNormal];
            button.imageView.image = [UIImage imageNamed:@"icon_zone_search"];
        }
        [button addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [self.buttonView addSubview:button];
    }
    
    __weak typeof(self) weakSelf = self;
    [[PDEMManager sharedInstance] getUnreadNumber:^(NSUInteger num) {
        weakSelf.tabBarItem.badgeValue = num == 0? nil : [NSString stringWithFormat:@"%@",@(num)];
    }];
}

- (void)tableViewSetting {
    // 聊天标题
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.buttonView.frame), kScreenBounds.size.width, kTableViewSectionHeader_height)];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.view addSubview:headerView];
    
    CGFloat label_bottom = 3;
    UILabel *label = [[UILabel alloc] init];
    label.text = @"聊天";
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont systemFontOfCustomeSize:13];
    label.textColor = [UIColor blackColor];
    CGSize size = [label.text sizeWithCalcFont:label.font];
    label.frame = CGRectMake(kTableViewSectionHeader_left, kTableViewSectionHeader_height - size.height - label_bottom, size.width, size.height);
    [headerView addSubview:label];
    
    // 消息列表
    PDEMListViewController *listViewController = [[PDEMManager sharedInstance] makeOneListViewCoontroller];
    listViewController.view.frame = CGRectMake(0, CGRectGetMaxY(headerView.frame), kScreenBounds.size.width, self.view.frame.size.height - CGRectGetMaxY(headerView.frame));
    [self.view addSubview:listViewController.view];
    [self addChildViewController:listViewController];
}

#pragma mark - 控件方法

- (void)didClickButton:(UIButton *)button {
    if (button.tag == 0) {
        PDCenterFriendViewController *followedViewController = [[PDCenterFriendViewController alloc] init];
        followedViewController.selectedIndex = PDCenterSocialTypeFollow;
        [self pushViewController:followedViewController animated:YES];
    } else if (button.tag == 1) { // 黑科技
        if (self.appFunc.enable == NO) {
            [PDHUD showHUDSuccess:@"敬请期待！"];
        }
    } else {
        PDZoneSearchViewController *searchViewController = [[PDZoneSearchViewController alloc] init];
        [self pushViewController:searchViewController animated:YES];
    }
}

- (void)fetchMessageState {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageState requestParams:nil responseObjectClass:[PDMessageState class] succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded) {
            PDMessageState *state = (PDMessageState *)responseObject;
            // 重置
            [AccountModel sharedAccountModel].messageState = state;
            
            if (state.hasUnReadMsg) {
                [strongSelf.tempMsgButton setImage:[UIImage imageNamed:@"icon_panda_message_has_nor"] forState:UIControlStateNormal];
                [strongSelf.tempMsgButton sizeToFit];
            } else {
                [strongSelf.tempMsgButton setImage:[UIImage imageNamed:@"icon_panda_message_nor"] forState:UIControlStateNormal];
                [strongSelf.tempMsgButton sizeToFit];
            }
        }
    }];
}

/**
 * 获取黑科技链接
 */
- (void)fetchBlackTeach {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:appFunctionZoneFind requestParams:@{@"code":@"blackTech"} responseObjectClass:[PDZoneAppFunctionModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.appFunc = (PDZoneAppFunctionModel *)responseObject;
        }
    }];
}


@end
