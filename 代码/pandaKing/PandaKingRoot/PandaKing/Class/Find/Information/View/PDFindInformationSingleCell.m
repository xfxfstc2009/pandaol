//
//  PDFindInformationSingleCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFindInformationSingleCell.h"
#import <pop/POP.h>
@interface PDFindInformationSingleCell()

@property (nonatomic,strong)UIView *bgView;                     /**< */
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)PDImageView *avatar;                /**< 头像*/
@property (nonatomic,strong)UILabel *titleLabel;                /**< 标题*/
@property (nonatomic,strong)UIView *iconView;                   /**< icon的View*/
@property (nonatomic,strong)UILabel *timeLabel;                 /**< 时间*/

@end

@implementation PDFindInformationSingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bgView];
    
    self.convertView = [[UIView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.clipsToBounds = YES;
    [self.bgView addSubview:self.convertView];
    
    // 1. 头像
    self.avatar = [[PDImageView alloc]init];
    self.avatar.backgroundColor = [UIColor clearColor];
    self.avatar.frame = self.convertView.bounds;
    self.avatar.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.convertView addSubview:self.avatar];
    
    // 2. 创建名字
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"]boldFont];
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self.bgView addSubview:self.titleLabel];
    
    // 3.创建nameView
    self.iconView = [[UIView alloc]init];
    self.iconView.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.iconView];
    
    // 4. 创建时间
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.timeLabel.textAlignment = NSTextAlignmentRight;
    self.timeLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self.bgView addSubview:self.timeLabel];
    
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferInformationSingleModel:(PDInformationSingleModel *)transferInformationSingleModel{
    _transferInformationSingleModel = transferInformationSingleModel;
    
    // 1. 创建view
    self.bgView.frame = CGRectMake(LCFloat(5), LCFloat(3), kScreenBounds.size.width - 2 * LCFloat(5), self.transferCellHeight - LCFloat(3) * 2);
    
    // 2. 头像
        self.convertView.frame = CGRectMake(LCFloat(4), LCFloat(4), LCFloat(135), self.bgView.size_height - 2 * LCFloat(4));
    
    // 1. 头像
    __weak typeof(self)weakSelf = self;
    [self.avatar uploadImageWithURL:transferInformationSingleModel.thumb placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (image){
          [strongSelf setTransferSingleAsset:image];
        }
    }];
    
    
    // 2. 名字
    CGFloat width = kScreenBounds.size.width - LCFloat(5) - CGRectGetMaxX(self.convertView.frame) - LCFloat(11);
    self.titleLabel.text = transferInformationSingleModel.title;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(5), LCFloat(5), width, titleSize.height);
    
    // 3. 创建iconView
    self.iconView.frame = CGRectMake(CGRectGetMaxX(self.convertView.frame) + LCFloat(5), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(5), width, 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]]);
    if (self.iconView){
        [self.iconView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        CGFloat margin = LCFloat(5);                        // 中间的宽度
        CGFloat maxWidth = width;
        CGFloat flagOriginX = LCFloat(0);
        CGFloat flagOriginY = LCFloat(0);
        if (transferInformationSingleModel.keywords.count){
            for (int i = 0 ; i < transferInformationSingleModel.keywords.count;i++){
                UILabel *flatLabel = [self createIconLabel:[transferInformationSingleModel.keywords objectAtIndex:i]];
                // 1. 获取剩下的宽度
                CGFloat width = maxWidth - flagOriginX;
                if (width >flatLabel.size_width){           // 不换行
                    flatLabel.frame = CGRectMake(flagOriginX, flagOriginY, flatLabel.size_width, flatLabel.size_height);
                    flagOriginX += flatLabel.size_width + margin;
                } else {                                    // 换行
                    flagOriginX = 0;
                    flagOriginY = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
                    flatLabel.frame = CGRectMake(flagOriginX, flagOriginY, flatLabel.size_width, flatLabel.size_height);
                    flagOriginX += flatLabel.size_width + margin;
                }
                [self.iconView addSubview:flatLabel];
                if (i == transferInformationSingleModel.keywords.count - 1){ // 最后一个
                    if (flatLabel.orgin_y == 0){
                        self.iconView.size_height = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
                    } else {
                        self.iconView.size_height =  2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
                    }
                }
            }
        }
    }
    
    // 3. 创建时间戳
    self.timeLabel.text = [NSDate getTimeWithString:(transferInformationSingleModel.publishTime / 1000.)];
    CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.timeLabel.font])];
    self.timeLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - timeSize.width, MAX(CGRectGetMaxY(self.iconView.frame) + LCFloat(5), self.transferCellHeight - LCFloat(5) - [NSString contentofHeightWithFont:self.timeLabel.font]), timeSize.width, [NSString contentofHeightWithFont:self.timeLabel.font]);

    
}

-(UILabel *)createIconLabel:(NSString *)title{
    UILabel *iconLabel = [[UILabel alloc]init];
    iconLabel.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    iconLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    iconLabel.text = title;
    iconLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    iconLabel.textAlignment = NSTextAlignmentCenter;
    CGSize contentOfSize = [title sizeWithCalcFont:iconLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:iconLabel.font])];
    iconLabel.frame = CGRectMake(LCFloat(3), LCFloat(2), contentOfSize.width + 2 * LCFloat(3), [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]]);
    return iconLabel;
}

+(CGFloat)calculationCellHeightWithModel:(PDInformationSingleModel *)transferInformationSingleModel{
    CGFloat cellHeight = 0 ;
    
    cellHeight += LCFloat(5);
    CGFloat width = kScreenBounds.size.width - LCFloat(5) - LCFloat(11) - LCFloat(135) - LCFloat(11);
    // 1. 标题
    CGSize titleSize = [transferInformationSingleModel.title sizeWithCalcFont:[[UIFont fontWithCustomerSizeName:@"正文"]boldFont] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += titleSize.height;
    cellHeight += LCFloat(5);
    // 2. iconView
    
    CGFloat margin = LCFloat(5);                        // 中间的宽度
    CGFloat maxWidth = width;
    CGFloat flagOriginX = LCFloat(0);
    CGFloat flagOriginY = LCFloat(0);
    CGFloat iconViewHeight = 0;
    if (transferInformationSingleModel.keywords.count){
        for (int i = 0 ; i < transferInformationSingleModel.keywords.count;i++){
            NSString *title = [transferInformationSingleModel.keywords objectAtIndex:i];
            CGSize contentOfSize = [title sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]])];
            CGFloat mainWidth = contentOfSize.width + 2 * LCFloat(3);
            // 1. 获取剩下的宽度
            CGFloat width = maxWidth - flagOriginX;
            if (width >mainWidth){           // 不换行
                flagOriginX += mainWidth + margin;
                iconViewHeight = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
            } else {                                    // 换行
                flagOriginX = 0;
                flagOriginY = [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
                flagOriginX += mainWidth + margin;
            }
            if (i == transferInformationSingleModel.keywords.count - 1){ // 最后一个
                if (flagOriginY == 0){
                    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
                } else {
                    cellHeight += 2 * [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
                }
            }
        }
    }

    // 3. 时间
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]];
    cellHeight += LCFloat(5);
    
    return MAX(cellHeight, LCFloat(5) * 2 + LCFloat(86));
}










#pragma mark - Animation
- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    [super setHighlighted:highlighted animated:animated];
    
    if (self.highlighted) {
        [self showBlunesManager1];
        
    } else {
        [self showBlunesManager];
    }
}

-(void)showBlunesManager{
    POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.duration           = 0.2f;
    scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(.95, .95)];
    [self.titleLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.timeLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.avatar pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

-(void)showBlunesManager1{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
    scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
    scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(6, 6)];
    scaleAnimation.springBounciness    = 1.f;
    [self.titleLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.timeLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    [self.avatar pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)startAnimationWithDelay:(CGFloat)delayTime {
        [self showBlunesManager1];
    [UIView animateWithDuration:1. delay:delayTime usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{

    } completion:^(BOOL finished) {
        [self showBlunesManager];
    }];
}





-(void)setTransferSingleAsset:(UIImage *)transferSingleAsset{
    
    CGImageRef posterImage = transferSingleAsset.CGImage;
    // 1. 获取当前图片高度
    size_t posterImageHeight = CGImageGetHeight(posterImage);
    CGFloat scale = posterImageHeight / LCFloat(135);
    self.avatar.image = [UIImage imageWithCGImage:posterImage scale:scale orientation:UIImageOrientationUp];
    
    // 基本尺寸参数
    CGSize boundsSize = self.convertView.bounds.size;
    CGFloat boundsWidth = boundsSize.width;
    CGFloat boundsHeight = boundsSize.height;
    
    CGSize imageSize = self.avatar.image.size;
    CGFloat imageWidth = imageSize.width;
    CGFloat imageHeight = imageSize.height;
    
    CGRect imageFrame = CGRectMake(0, (boundsHeight - boundsWidth) / 2., boundsWidth, imageHeight * boundsWidth / imageWidth);
    
    if (imageHeight > imageWidth){          // 图片高度大于图片宽度
        if (imageFrame.size.height < boundsHeight) {
            imageFrame.origin.y = floorf((boundsHeight - imageFrame.size.height) / 2.0);
        } else {
            imageFrame.origin.y = 0;
        }
    } else {                                // 图片宽度大于高度
        if (imageFrame.size.width < boundsWidth){
            imageFrame.origin.x = 0;
            imageFrame.size.width = (boundsHeight * 1.00 / imageFrame.size.height) *imageFrame.size.width;
            imageFrame.size.height = boundsHeight;
        } else {
            imageFrame.size.width = boundsWidth;
            imageFrame.size.height = (boundsWidth / imageFrame.size.width) * imageFrame.size.height;
            imageFrame.origin.x = - floorf((imageFrame.size.width - boundsWidth) / 2.0);
            imageFrame.origin.y = - floorf((imageFrame.size.height - boundsHeight) / 2.);
            
            if (imageFrame.size.height < boundsHeight){
                imageFrame.size.height = boundsHeight;
                imageFrame.size.width = (boundsHeight / imageFrame.size.height) * boundsWidth;
                imageFrame.origin.x = - floorf((imageFrame.size.width - boundsWidth) / 2.0);
                imageFrame.origin.y = - floorf((imageFrame.size.height - boundsHeight) / 2.);
            }
            
        }
    }
    self.avatar.frame = imageFrame;
}



@end
