//
//  PDFindInformationBannerCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFindInformationBannerCell.h"
#import "PDScrollView.h"

@interface PDFindInformationBannerCell()
@property (nonatomic,strong)PDScrollView *headerScrollView;

@end
@implementation PDFindInformationBannerCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.headerScrollView = [[PDScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(160))];
    [self.headerScrollView startTimerWithTime:1.5];
    self.headerScrollView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.headerScrollView];
}

-(void)setTransferSegmentModel:(PDInformationSegmentListModel *)transferSegmentModel{
    _transferSegmentModel = transferSegmentModel;
    NSMutableArray *bannerMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < transferSegmentModel.banners.count;i++){
        PDInformationBannerSingleModel *bannerSingleModel = [transferSegmentModel.banners objectAtIndex:i];
        PDScrollViewSingleModel *scrollSingleModel = [[PDScrollViewSingleModel alloc]init];
        scrollSingleModel.title = bannerSingleModel.text;
        scrollSingleModel.img = bannerSingleModel.pic;
        [bannerMutableArr addObject:scrollSingleModel];
    }
    self.headerScrollView.transferImArr = bannerMutableArr;
}

+(CGFloat)calculationCellHeiht{
    return LCFloat(160);
}

@end
