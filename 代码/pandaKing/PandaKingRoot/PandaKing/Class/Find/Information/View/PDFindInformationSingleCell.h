//
//  PDFindInformationSingleCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDInformationSingleModel.h"

@interface PDFindInformationSingleCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDInformationSingleModel *transferInformationSingleModel;

+(CGFloat)calculationCellHeightWithModel:(PDInformationSingleModel *)transferInformationSingleModel;


// animation
- (void)startAnimationWithDelay:(CGFloat)delayTime ;
@end
