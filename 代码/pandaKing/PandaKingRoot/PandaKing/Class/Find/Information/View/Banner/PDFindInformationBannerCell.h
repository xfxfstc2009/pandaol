//
//  PDFindInformationBannerCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDInformationSegmentListModel.h"

@interface PDFindInformationBannerCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDInformationSegmentListModel *transferSegmentModel;

+(CGFloat)calculationCellHeiht;

@end
