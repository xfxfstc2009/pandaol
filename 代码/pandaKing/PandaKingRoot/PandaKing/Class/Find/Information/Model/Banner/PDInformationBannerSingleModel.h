//
//  PDInformationBannerSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDInformationBannerSingleModel <NSObject>

@end

@interface PDInformationBannerSingleModel : FetchModel

/*
 actionEntityId = 218392636;
 actionType = treasure;
 id = 57ee3029a3c8a83f81de318c;
 pic = "/po/upload/image/20160927/1474941602886088549.jpg";
 position = discovery;
 text = "\U7535\U8111\U7535\U8111";
 */

@property (nonatomic,copy)NSString *actionEntityId;             /**< id*/
@property (nonatomic,copy)NSString *actionType;                 /**< 类型*/
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *pic;
@property (nonatomic,copy)NSString *position;
@property (nonatomic,copy)NSString *text;


@end
