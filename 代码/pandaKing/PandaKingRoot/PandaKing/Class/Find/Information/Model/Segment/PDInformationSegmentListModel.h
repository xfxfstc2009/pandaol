//
//  PDInformationSegmentListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDInformationSegmentSingleModel.h"
#import "PDInformationBannerSingleModel.h"
@interface PDInformationSegmentListModel : FetchModel

@property (nonatomic,strong)NSArray<PDInformationSegmentSingleModel>*cates;
@property (nonatomic,strong)NSArray<PDInformationBannerSingleModel> *banners;

@end
