//
//  PDInformationSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDInformationSegmentSingleModel.h"
@protocol PDInformationSingleModel <NSObject>


@end

@interface PDInformationSingleModel : FetchModel

@property (nonatomic,copy)NSString *informationId;              /**< 咨询Id*/
@property (nonatomic,copy)NSString *title;                      /**< 标题*/
@property (nonatomic,copy)NSString *summary;                    /**< 详情*/
@property (nonatomic,copy)NSString *thumb;                      /**< 头像*/
@property (nonatomic,strong)NSArray *keywords;                  /**< icon数组*/
@property (nonatomic,assign)NSTimeInterval publishTime;                /**< 发布时间*/
@property (nonatomic,assign)NSTimeInterval lastUpdateTime;              /**< 最后更新时间*/
@property (nonatomic,strong)PDInformationSegmentSingleModel *category; /**< 分组*/
/*
 {

 "summary": "相信不少的新手玩家们，在自己玩中路 AD，不是那么顺风顺水的时候，队友总会拿你的补刀来说事情。",
 "thumb": "http://i3.17173cdn.com/2fhnvk/YWxqaGBf/cms3/PfVutHbkCDAupvB.jpg",
 "keywords": [
 "补刀",
 "王者",
 "水平"
 ],
 "publishTime": 1474538053000,
 "lastUpdateTime": 1474538137000,
 "category": {
 "id": "2",
 "name": null
 }
 },
 */


@end
