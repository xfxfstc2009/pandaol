//
//  PDInformationSegmentSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDInformationSegmentSingleModel <NSObject>

@end

@interface PDInformationSegmentSingleModel : FetchModel

@property (nonatomic,copy)NSString *itemId;
@property (nonatomic,copy)NSString *name;

@property (nonatomic,copy)NSString *tableIndex;

@end
