//
//  PDInformationBannerSingleModel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInformationBannerSingleModel.h"

@implementation PDInformationBannerSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end
