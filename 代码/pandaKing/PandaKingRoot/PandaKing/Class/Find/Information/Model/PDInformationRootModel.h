//
//  PDInformationRootModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDInformationSingleModel.h"

@interface PDInformationRootModel : FetchModel

@property (nonatomic,strong)NSArray<PDInformationSingleModel> *items;

@end
