//
//  PDInformationSegmentSingleModel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDInformationSegmentSingleModel.h"

@implementation PDInformationSegmentSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"itemId":@"id"};
}

@end
