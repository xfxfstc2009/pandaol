//
//  PDInformationRootViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDInformationRootViewController.h"
#import "PDLotteryGameDetailBackgroundView.h"
#import <HTHorizontalSelectionList.h>

#import "PDInformationSegmentSingleModel.h"
#import "PDScrollView.h"                            // 头部的scroller
#import "PDFindInformationSingleCell.h"
#import "PDInformationSegmentListModel.h"
#import "PDInformationRootModel.h"
#import "PDProductDetailViewController.h"
#import "PDFindInformationDetailViewController.h"

#import "PDFindRootNearbyViewController.h"
#import "PDShopRootMainViewController.h"
#import "PDInformationRootViewController.h"

#define Information_Height LCFloat(244)
#define Information_Segment_Height LCFloat(44)

typedef NS_ENUM(NSInteger,informationNavAnimationType) {
    informationNavAnimationTypeStop,                    /**< 等待*/
    informationNavAnimationTypeStart,
    informationNavAnimationTypeWaiting,
};

@interface PDInformationRootViewController()<UITableViewDataSource,UITableViewDelegate,HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>{
    CGFloat _yOffset;
    informationNavAnimationType navAnimationType;
    CGPoint tempPoint;
}
@property (nonatomic,strong)PDLotteryGameDetailBackgroundView *mainBackgroundView;
// 【nav】
@property (nonatomic,strong)UIView *navigationView;
@property (nonatomic,strong)PDImageView *barMainTitleImageView;

// 【main】
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;         /**< segment*/


@property (nonatomic,strong)NSMutableArray *segmentMutableArr;              /**< segment数组*/
@property (nonatomic,strong)NSMutableDictionary *tableViewMutableDic;       /**< 列表的字典*/
@property (nonatomic,strong)NSMutableDictionary *dataSourceMutableDic;      /**< 数据源字典*/
@property (nonatomic,strong)NSMutableArray *currentMutableArr;              /**< 当前的数组*/
@property (nonatomic,strong)NSMutableDictionary *pointYMutableDic;          /**< 高度*/


@property (nonatomic,strong)PDScrollView *headerScrollView;                 /**< */
@property (nonatomic,strong)UIView *headerView;
@property (nonatomic,assign)CGFloat pointY;

@end

@implementation PDInformationRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createView];
    [self createHeaderView];                            // 3.
    [self interfaceManager];
    self.mainBackgroundView.transferNavView = [self createNavBar];
}

#pragma mark - InterfaceManager
-(void)interfaceManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendReqeustToGetSegmentInfoWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf hasTableAndManagerWithIndex:strongSelf.segmentList.selectedButtonIndex];
    }];
}

#pragma mark - pageSettikng
-(void)pageSetting{
    self.barMainTitle = @"资讯";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.segmentMutableArr = [NSMutableArray array];                        // 1. segment
    
    self.tableViewMutableDic = [NSMutableDictionary dictionary];            // 2. tableView
    self.dataSourceMutableDic = [NSMutableDictionary dictionary];           // 3. 数据源
    self.pointYMutableDic = [NSMutableDictionary dictionary];               // 4. point Y
    self.currentMutableArr = [NSMutableArray array];                        // 5. 当前数据源
}

#pragma mark 创建头部的view
-(void)createHeaderView{
    self.headerView = [[UIView alloc]init];
    self.headerView.backgroundColor = [UIColor clearColor];
    self.headerView.frame = CGRectMake(0, 0, kScreenBounds.size.width, Information_Height);
    _yOffset = self.headerView.center_y;
    [self.view addSubview:self.headerView];

    // 2. 创建北京scrollView
    self.headerScrollView = [[PDScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, Information_Height - Information_Segment_Height)];
    self.headerScrollView.backgroundColor = BACKGROUND_VIEW_COLOR;
    __weak typeof(self)weakSelf = self;
    [self.headerScrollView bannerImgTapManagerWithInfoblock:^(PDScrollViewSingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([singleModel.actionType isEqualToString:@"treasure"]){              // 夺宝活动
            PDProductDetailViewController *productDetailViewController = [[PDProductDetailViewController alloc]init];
            productDetailViewController.productId = singleModel.actionEntityId;
            [productDetailViewController hidesTabBarWhenPushed];
            [strongSelf.navigationController pushViewController:productDetailViewController animated:YES];
        } else if ([singleModel.actionType isEqualToString:@"news"]){           // 新闻资讯
            PDFindInformationDetailViewController *webViewController = [[PDFindInformationDetailViewController alloc]init];
            NSArray *ids = [singleModel.actionEntityId componentsSeparatedByString:@","];
            NSString *webUrl = InformationDetail([ids objectAtIndex:0], [ids objectAtIndex:1]);
            [webViewController webDirectedWebUrl:webUrl];
            webViewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:webViewController animated:YES];
        }
    }];
    [self.headerView addSubview:self.headerScrollView];
    
    // 3. 创建segment
    [self createSegment];
}


#pragma mark - createView
-(void)createView{
    self.mainBackgroundView = [[PDLotteryGameDetailBackgroundView alloc]initWithFrame:kScreenBounds];
    self.mainBackgroundView.transferHeaderHeight = Information_Height;
    [self.view addSubview:self.mainBackgroundView];
    self.mainBackgroundView.mainScrollView.delegate = self;
    self.mainBackgroundView.mainScrollView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.mainBackgroundView.mainScrollView.pagingEnabled = YES;
}

#pragma mark  创建navBar
-(UIView *)createNavBar{
    if (!self.navigationView){
        self.navigationView = [[UIView alloc]init];
        self.navigationView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        self.navigationView.clipsToBounds = YES;
        self.navigationView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 64);
        [self.view addSubview:self.navigationView];
        

        self.barMainTitleImageView = [[PDImageView alloc]init];
        self.barMainTitleImageView.image = [UIImage imageNamed:@"pandaol_logo_r"];
        self.barMainTitleImageView.backgroundColor = [UIColor clearColor];
        self.barMainTitleImageView.frame = CGRectMake((kScreenBounds.size.width - 227 /2.) / 2., self.navigationView.size_height , 227 /2., 21/2.);
        [self.navigationView addSubview:self.barMainTitleImageView];
        
    }
    return self.navigationView;
}


#pragma mark - Segment
-(void)createSegment{
    self.segmentList = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake(0, Information_Height - Information_Segment_Height, kScreenBounds.size.width, Information_Segment_Height)];
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    self.segmentList.selectionIndicatorColor = c15;
    self.segmentList.bottomTrimColor = [UIColor clearColor];
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.segmentList.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self.headerView addSubview:self.segmentList];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentMutableArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    PDInformationSegmentSingleModel *segmentItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
    return segmentItemSingleModel.name;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self.mainBackgroundView.mainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
    [self hasTableAndManagerWithIndex:index];
}

#pragma makr - 临时创建一个tableView
-(void)hasTableAndManagerWithIndex:(NSInteger)index{
    UITableView *currentMainTableView;
    // 2. 判断当前是否存在tableView
    if (![self.tableViewMutableDic.allKeys containsObject:[NSString stringWithFormat:@"%li",(long)index]]){            // 如果不存在
        if (self.currentMutableArr.count){
            [self.currentMutableArr removeAllObjects];
        }
        UITableView *tableView = [self createTableView];                            // 创建tableView
        tableView.orgin_x = index *kScreenBounds.size.width;                        // 修改frame
        CGFloat currentPointY = 0;
        if (self.pointY > Information_Height + 64){
            currentPointY = Information_Height - Information_Segment_Height;
        } else {
            currentPointY = self.pointY;
        }
        
        [tableView setContentOffset:CGPointMake(0, currentPointY)];                   //
        [self.tableViewMutableDic setObject:tableView forKey:[NSString stringWithFormat:@"%li",(long)index]];
        
        // 2. 走接口
        __weak typeof(self)weakSelf = self;
        PDInformationSegmentSingleModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
        studyItemSingleModel.tableIndex = [NSString stringWithFormat:@"%li",(long)index];
        [weakSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:NO];
        
        // 3.
        currentMainTableView = tableView;
        
        
    } else {            // 已经存在
        UITableView *currentTableView = (UITableView *)[self.tableViewMutableDic objectForKey:[NSString stringWithFormat:@"%li",(long)self.segmentList.selectedButtonIndex]];
        CGFloat currentPointY = 0;
        if (self.pointY > LCFloat(244) + 64){
            NSLog(@"self.pointY====>%.2f",self.pointY);
            NSLog(@"currentTableView.contentOffset.y====>%.2f",currentTableView.contentOffset.y);
            currentPointY = MIN(self.pointY, currentTableView.contentOffset.y) ;
            if (currentPointY > 0 && currentPointY < (Information_Height - Information_Segment_Height)){
                currentPointY = (Information_Height - Information_Segment_Height);
            }
            if (currentTableView.contentOffset.y == 0){
                currentPointY = (Information_Height - Information_Segment_Height);
            }
            
        } else {
            if (self.pointY > (Information_Height - Information_Segment_Height)){
                currentPointY = (Information_Height - Information_Segment_Height);
            } else {
                currentPointY = self.pointY;
            }
        }
        
        [currentTableView setContentOffset:CGPointMake(0, currentPointY) animated:YES];
        
        // 2. 走接口
        __weak typeof(self)weakSelf = self;
        PDInformationSegmentSingleModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
        [weakSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:YES];
        
        // 3. 获取当前tableview
        currentMainTableView = currentTableView;
    }
    
    // 动画
    CGPoint scrollViewDragPoint = currentMainTableView.contentOffset;
    
    [self.mainBackgroundView animateNavigationBar:scrollViewDragPoint maxHeight:(Information_Height - Information_Segment_Height- self.navigationView.size_height)];
}

#pragma mark - createTableView
-(UITableView *)createTableView{
    UITableView *tableView= [[UITableView alloc] initWithFrame:self.mainBackgroundView.mainScrollView.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.orgin_y = 0;
    tableView.size_height = self.mainBackgroundView.mainScrollView.size_height - self.tabBarController.tabBar.size_height;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.showsVerticalScrollIndicator = YES;
    tableView.backgroundColor = [UIColor clearColor];
    [self createTableHeadView:tableView];
    [self.mainBackgroundView.mainScrollView addSubview:tableView];
    
    __weak typeof(self)weakSelf = self;
    
    if (self.segmentMutableArr.count -1 >= self.segmentList.selectedButtonIndex && self.segmentList){
        PDInformationSegmentSingleModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:self.segmentList.selectedButtonIndex];
        // 1. 下拉刷新
        [tableView appendingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:NO];
        }];
        
        [tableView appendingFiniteScrollingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:NO];
        }];
    }
    
    return tableView;
}

#pragma makr - UITableView
-(void)createTableHeadView:(UITableView *)tableView{
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, Information_Height)];
    tableHeaderView.backgroundColor = [UIColor clearColor];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.tableHeaderView = tableHeaderView;
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.currentMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdengifyWithRowOne = @"cellIdentifyWithRowOne";
    PDFindInformationSingleCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDFindInformationSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = BACKGROUND_VIEW_COLOR;
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    // 1. 获取对应的tableView
    //    for (int i = 0 ;i < self.tableViewMutableDic.allKeys.count;i++){
    PDInformationSegmentSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    
    if (tableView == currentTableView){         // 获取当前的model
        
        // 1.获取当前tableView的key
        NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.itemId];
        PDInformationSingleModel *studyInfoSingleModel = [currentArr objectAtIndex:indexPath.row];
        cellWithRowOne.transferInformationSingleModel = studyInfoSingleModel;
    }
    //    }
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // 1. 获取对应的tableView
    PDInformationSegmentSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    
    if (tableView == currentTableView){         // 获取当前的model
        NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.itemId];
        PDInformationSingleModel *studyInfoSingleModel = [currentArr objectAtIndex:indexPath.row];
        
        PDFindInformationDetailViewController *webViewController = [[PDFindInformationDetailViewController alloc]init];
        NSString *webUrl = InformationDetail(studyInfoSingleModel.category.itemId, studyInfoSingleModel.informationId);
        [webViewController webDirectedWebUrl:webUrl];
        webViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:webViewController animated:YES];
    }
    //    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    PDInformationSegmentSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    
    if (tableView == currentTableView){         // 获取当前的model
        
        // 1.获取当前tableView的key
        NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.itemId];
        PDInformationSingleModel *studyInfoSingleModel = [currentArr objectAtIndex:indexPath.row];
        return [PDFindInformationSingleCell calculationCellHeightWithModel:studyInfoSingleModel];
    }
    return 44;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    static CGFloat initialDelay = 0.2f;
    static CGFloat stutter = 0.06f;
 
    if ([cell isKindOfClass:[PDFindInformationSingleCell class]]){
        PDFindInformationSingleCell *cardCell = (PDFindInformationSingleCell *)cell;
        [cardCell startAnimationWithDelay:initialDelay + ((indexPath.row) * stutter)];
    }
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.mainBackgroundView.mainScrollView){
        CGPoint point = scrollView.contentOffset;
        NSInteger page = point.x / kScreenBounds.size.width;
        [self.segmentList setSelectedButtonIndex:page animated:YES];
        
        [self hasTableAndManagerWithIndex:page];
        if (scrollView == self.mainBackgroundView.mainScrollView){
            navAnimationType = informationNavAnimationTypeStop;
        }
        return;
    }
    [self setTableViewContentOffsetWithTag:scrollView.tag contentOffset:scrollView.contentOffset.y];
    
    NSLog(@"减速停止");
    // 记录当前tableView的contentOfSet
    [self rememberPoint];
}

#pragma mark 记录当前的point
-(void)rememberPoint{
    UITableView *currentTableView = (UITableView *)[self.tableViewMutableDic objectForKey:[NSString stringWithFormat:@"%li",(long)self.segmentList.selectedButtonIndex]];
    [self.pointYMutableDic setObject:@(currentTableView.contentOffset.y) forKey:[NSString stringWithFormat:@"%li",(long)self.segmentList.selectedButtonIndex]];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    
    
    if (scrollView == self.mainBackgroundView.mainScrollView){              // 切换segment
        [self.mainBackgroundView animateNavigationBar:tempPoint maxHeight:(Information_Height - Information_Segment_Height- self.navigationView.size_height)];
    } else {                                                                // 上滑动
        if (navAnimationType == informationNavAnimationTypeStart){
            [self scrollViewDidScrollWithNavigationBar];
        }
        [self scrollViewDidScrollWithNavBar:scrollView.contentOffset.y];
        self.pointY = offsetY;
        
        CGFloat scale = 1.0;
        // 放大
        if (offsetY < 0) {
            
        } else if (offsetY > 0) { // 缩小
            scale = MAX(0.45, 1 - offsetY / self.headerScrollView.size_height);
            
            //            _userHead.transform = CGAffineTransformMakeScale(scale, scale);
        }
        
        if (scrollView.contentOffset.y > (Information_Height - Information_Segment_Height) - self.navigationView.size_height) {
            self.headerView.center = CGPointMake(self.headerView.center.x, _yOffset - (Information_Height - Information_Segment_Height) + self.navigationView.size_height);
            return;
        }
        CGFloat h = _yOffset - self.pointY;
        self.headerView.center = CGPointMake(self.headerView.center.x, h);
    }
}

// 开始拖动
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (scrollView == self.mainBackgroundView.mainScrollView){          //
        navAnimationType = informationNavAnimationTypeStop;
    } else {
        navAnimationType = informationNavAnimationTypeStart;
    }
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.mainBackgroundView.mainScrollView){          //
        navAnimationType = informationNavAnimationTypeStop;
    } else {
        navAnimationType = informationNavAnimationTypeStart;
    }
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView{
    if (scrollView == self.mainBackgroundView.mainScrollView){
        navAnimationType = informationNavAnimationTypeStop;
    }
}

-(void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset{
    if (scrollView == self.mainBackgroundView.mainScrollView){
        navAnimationType = informationNavAnimationTypeStop;
    }
}

#pragma mark - 【navigationBar 状态】
-(void)scrollViewDidScrollWithNavigationBar{
    PDInformationSegmentSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    CGPoint scrollViewDragPoint = currentTableView.contentOffset;
    tempPoint = scrollViewDragPoint;
    
    [self.mainBackgroundView animateNavigationBar:scrollViewDragPoint maxHeight:(Information_Height - Information_Segment_Height- self.navigationView.size_height)];
}


#pragma mark 完成拖拽
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([scrollView isEqual:self.mainBackgroundView.mainScrollView]) {
        return;
    }
    [self setTableViewContentOffsetWithTag:scrollView.tag contentOffset:scrollView.contentOffset.y];
    
    
    // 完成拖拽，记录当前的contentOfSet
    [self rememberPoint];
}

//设置tableView的偏移量
-(void)setTableViewContentOffsetWithTag:(NSInteger)tag contentOffset:(CGFloat)offset{
    
    CGFloat tableViewOffset = offset;
    
    if (offset > Information_Height + 64){
        tableViewOffset = Information_Height + 64;
    }
    
    // 1. 获取tableView
    NSInteger index = self.segmentList.selectedButtonIndex;
    // 2. 获取当前的tableView
    PDInformationSegmentSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex:index];
    //    NSString *key = [self.tableViewMutableDic.allKeys objectAtIndex:index];
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    [currentTableView setContentOffset:CGPointMake(0,currentTableView.contentOffset.y) animated:NO];
    NSLog(@"contentOffset_y=====>%.2f",currentTableView.contentOffset.y);
    NSLog(@"height=====>%.2f",currentTableView.contentSize.height);
}

#pragma mark - 【接口】
-(void)sendReqeustToGetSegmentInfoWithBlock:(void(^)())block{
    [[NetworkAdapter sharedAdapter] fetchWithPath:findSegmentList requestParams:nil responseObjectClass:[PDInformationSegmentListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (isSucceeded){
            PDInformationSegmentListModel *informationList = (PDInformationSegmentListModel *)responseObject;
            
            if(self.segmentMutableArr.count){
                [self.segmentMutableArr removeAllObjects];
            }
            [self.segmentMutableArr addObjectsFromArray:informationList.cates];
            if (self.segmentMutableArr.count > 2){
                self.segmentList.isNotScroll = NO;
            } else {
                self.segmentList.isNotScroll = YES;
            }
            
            
            // 修改banner
            NSMutableArray *bannerInfoArr = [NSMutableArray array];
            for (int i = 0 ; i < informationList.banners.count;i++){
                PDInformationBannerSingleModel *bannerSingleModel = [informationList.banners objectAtIndex:i];
                PDScrollViewSingleModel *singleModel = [[PDScrollViewSingleModel alloc]init];
                singleModel.title = bannerSingleModel.text;
                singleModel.img = bannerSingleModel.pic;
                singleModel.actionEntityId = bannerSingleModel.actionEntityId;
                singleModel.actionType = bannerSingleModel.actionType;
                [bannerInfoArr addObject:singleModel];
            }
            self.headerScrollView.transferImArr = bannerInfoArr;
            if (self.headerScrollView.transferImArr.count){
                [self.headerScrollView startTimerWithTime:6.];
            }
            // 重要
            self.mainBackgroundView.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.segmentMutableArr.count , self.mainBackgroundView.mainScrollView.size_height);
            [self.segmentList reloadData];
            if (block){
                block();
            }
        }
    }];
}

#pragma mark - 获取当前的item
-(void)sendRequestToGetInfoWithItemId:(PDInformationSegmentSingleModel *)item isHorizontal:(BOOL)horizontal{
    if (horizontal){
        return;
    }
    NSString *content = [NSString stringWithFormat:@"熊猫君正在寻找关于【%@】的资讯……",item.name];
    [PDHUD showHUDProgress:content diary:0];
    
    //1. 找到当前的tableView
    UITableView *currentTableView = (UITableView *)[self.tableViewMutableDic objectForKey:[NSString stringWithFormat:@"%li",(long)self.segmentList.selectedButtonIndex]];
    
    NSDictionary *params = @{@"catId":item.itemId,@"pageNum":currentTableView.currentPage};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:findInformation requestParams:params responseObjectClass:[PDInformationRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [PDHUD dismiss];
        if (isSucceeded){
            // 1.获取到当前model
            PDInformationRootModel *customerListModel = (PDInformationRootModel *)responseObject;
            // 2. 判断是否下啦
            if ([currentTableView.isXiaLa isEqualToString:@"YES"]){
                [strongSelf.dataSourceMutableDic removeObjectForKey:item.itemId];
            }
            // 3. 判断当前是否存在这样的数据源
            NSMutableArray *appendingMutableArr = [NSMutableArray array];
            if (![strongSelf.dataSourceMutableDic.allKeys containsObject:item.itemId]){          // 不存在
                [strongSelf.dataSourceMutableDic setObject:customerListModel.items forKey:item.itemId];
            } else {                                                                        // 存在
                // 1. 获取上一个数据源
                NSArray *lastArr = [strongSelf.dataSourceMutableDic objectForKey:item.itemId];
                // 2. 加入上一个数据源
                [appendingMutableArr addObjectsFromArray:lastArr];
                // 3. 加入新的数据源
                [appendingMutableArr addObjectsFromArray:customerListModel.items];
                // 4. 加入信息
                [strongSelf.dataSourceMutableDic setObject:appendingMutableArr forKey:item.itemId];
            }
            
            // 4. 获取当前的数据源
            NSArray *currentDataSourceArr = [strongSelf.dataSourceMutableDic objectForKey:item.itemId];
            
            if (strongSelf.currentMutableArr.count){
                [strongSelf.currentMutableArr removeAllObjects];
            }
            [strongSelf.currentMutableArr addObjectsFromArray:currentDataSourceArr];
            
            // dismiss
            if ([currentTableView.isXiaLa isEqualToString:@"YES"]){         // 下拉
                [currentTableView stopPullToRefresh];
            } else {
                [currentTableView stopFinishScrollingRefresh];
            }
            
            // placeholder
            
            [currentTableView reloadData];
            
        } else {
            
        }
    }];
}

// 1. 导航条增加文字
-(void)scrollViewDidScrollWithNavBar:(CGFloat)pointY{
    CGFloat fixedFloat = - self.navigationView.size_height + (self.navigationView.size_height - 20 - 14) / 2. + 5;
    CGFloat dymicFloat = self.mainBackgroundView.transferHeaderHeight - pointY + LCFloat(15);
    
    CATransform3D labelTransform = CATransform3DMakeTranslation(0, MAX(fixedFloat, dymicFloat) + LCFloat(15), 0);
    self.barMainTitleImageView.layer.transform = labelTransform;
    self.barMainTitleImageView.layer.zPosition = 20;
}

@end
