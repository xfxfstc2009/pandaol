//
//  PDFindInformationViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 
#import "AbstractViewController.h"
#import "PDFindRootViewController.h"

@class PDFindRootViewController;
@protocol PDFindInformationViewControllerDelegate <NSObject>

@optional
-(void)scrollToPageWithType:(FindType)type;

@end

@interface PDFindInformationViewController : AbstractViewController


@end
