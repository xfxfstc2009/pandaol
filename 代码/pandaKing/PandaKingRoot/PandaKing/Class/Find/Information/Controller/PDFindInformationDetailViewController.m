//
//  PDFindInformationDetailViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/10/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDFindInformationDetailViewController.h"
#import "PDPandaPersonCenterCell.h"

@interface PDFindInformationDetailViewController()
@property (nonatomic,strong)PDImageView *titleImgView;

@end

@implementation PDFindInformationDetailViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor clearColor].CGColor;
}


-(void)pageSetting{
    self.view.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.webView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    
    UIView *titleBgView = [[UIView alloc]init];
    titleBgView.backgroundColor = [UIColor clearColor];
    titleBgView.frame = self.navigationItem.titleView.bounds;
    self.navigationItem.titleView = titleBgView;
    
    self.titleImgView = [[PDImageView alloc]init];
    self.titleImgView.backgroundColor = [UIColor clearColor];
    UIImage *img = [UIImage imageNamed:@"pandaol_logo_r"];
    self.titleImgView.image = img;
    self.titleImgView.frame = CGRectMake((titleBgView.size_width - img.size.width / 2.) / 2. , (titleBgView.size_height - img.size.height / 2.) / 2., img.size.width / 2., img.size.height / 2.);
    [titleBgView addSubview:self.titleImgView];
}

@end
