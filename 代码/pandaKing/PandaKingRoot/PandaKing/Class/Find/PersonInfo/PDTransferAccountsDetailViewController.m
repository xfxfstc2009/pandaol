//
//  PDTransferAccountsDetailViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDTransferAccountsDetailViewController.h"
#import "PDTransferAccountListMainModel.h"
#import "PDTransferAccountDetailCell.h"

@interface PDTransferAccountsDetailViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *detailTableView;
@property (nonatomic,strong)NSMutableArray *detailMutableArr;

@end

@implementation PDTransferAccountsDetailViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendReqeustTogetDetailWithHornal:YES];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = self.transferPersonSingleModel.member.nickname;
    self.barSubTitle = self.transferPersonSingleModel.member.personId;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.detailMutableArr = [NSMutableArray array];
}

-(void)createTableView{
    if (!self.detailTableView){
        self.detailTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
        self.detailTableView.dataSource = self;
        self.detailTableView.delegate = self;
        [self.view addSubview:self.detailTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.detailTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendReqeustTogetDetailWithHornal:YES];
    }];

    [self.detailTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendReqeustTogetDetailWithHornal:NO];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.detailMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    PDTransferAccountListModel *transferAccountListModel = [self.detailMutableArr objectAtIndex:section];
    return transferAccountListModel.detail.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    PDTransferAccountDetailCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDTransferAccountDetailCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    PDTransferAccountListModel *transferAccountListModel = [self.detailMutableArr objectAtIndex:indexPath.section];
    cellWithRowOne.transferAccountSingleModel = [transferAccountListModel.detail objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PDTransferAccountDetailCell calculationCellHeight];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    UILabel *titleLabel = [[UILabel alloc]init];
    titleLabel.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(44));
    titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.textColor = [UIColor lightGrayColor];
    [headerView addSubview:titleLabel];
    
    PDTransferAccountListModel *infoModel = [self.detailMutableArr objectAtIndex:section];
    titleLabel.text = [NSString stringWithFormat:@"%@ 转出:%@ 收入:%@",infoModel.month,infoModel.expense,infoModel.income];
    
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.detailTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeHead;
        } else {
            separatorType = SeparatorTypeMiddle;
        }

        [cell addSeparatorLineWithType:separatorType];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(44);
}

#pragma mark - InterfaceManager
-(void)sendReqeustTogetDetailWithHornal:(BOOL)hornal{
    __weak typeof(self)weakSelf = self;
    
    if (hornal){
        self.detailTableView.currentPage = @"1";
    }
    
    NSDictionary *params = @{@"pageNum":self.detailTableView.currentPage,@"anotherMemberId":self.transferPersonSingleModel.member.personId};
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberaccountlogforxfegold requestParams:params responseObjectClass:[PDTransferAccountListMainModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDTransferAccountListMainModel *accountMainListModel = (PDTransferAccountListMainModel *)responseObject;
            if (hornal){                // 下拉
                [strongSelf.detailMutableArr removeAllObjects];
            }
            [strongSelf.detailMutableArr addObjectsFromArray:accountMainListModel.items];
            [strongSelf.detailTableView reloadData];
            
            if (strongSelf.detailMutableArr.count){
                [strongSelf.detailTableView dismissPrompt];
            } else {
                [strongSelf.detailTableView showPrompt:@"没有与该好友的赠送记录" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
        }
    }];
}

@end
