//
//  PDTransferAccountSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol  PDTransferAccountSingleModel<NSObject>



@end

@interface PDTransferAccountSingleModel : FetchModel

@property (nonatomic,copy)NSString *deltaFee;
@property (nonatomic,assign)NSTimeInterval recordTime;
@property (nonatomic,copy)NSString *remark;
@property (nonatomic,copy)NSString *source;

@end
