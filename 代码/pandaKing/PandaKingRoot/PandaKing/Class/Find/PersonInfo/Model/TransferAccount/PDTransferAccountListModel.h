//
//  PDTransferAccountListModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDTransferAccountSingleModel.h"

@protocol PDTransferAccountListModel <NSObject>

@end

@interface PDTransferAccountListModel : FetchModel

@property (nonatomic,strong)NSArray<PDTransferAccountSingleModel> *detail;
@property (nonatomic,copy)NSString *month;
@property (nonatomic,copy)NSString *income;
@property (nonatomic,copy)NSString *expense;

@end
