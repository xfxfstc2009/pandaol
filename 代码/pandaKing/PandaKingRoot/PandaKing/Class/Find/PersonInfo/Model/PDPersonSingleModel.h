//
//  PDPersonSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPersonMemberSingleModel.h"
#import "PDPersonImSingleModel.h"
@interface PDPersonSingleModel : FetchModel

@property (nonatomic,strong)PDPersonMemberSingleModel *member;
@property (nonatomic,assign)BOOL isFollow;                      /**< 是否关注*/
@property (nonatomic,copy)NSString *follow;                     /**< 关注数量*/
@property (nonatomic,copy)NSString *fans;                       /**< 关注的粉丝数量*/
@property (nonatomic,assign)NSTimeInterval latestStakeTime;     /**< 最后的投注时间*/
@property (nonatomic,copy)NSString *gameUserName;               /**< 游戏召唤师名字*/
@property (nonatomic,strong)PDPersonImSingleModel *im;          /**< IM*/
@property (nonatomic,copy)NSString *goldBalance;
@property (nonatomic,copy)NSString *bambooBalance;
@property (nonatomic,assign)NSInteger transferGoldLevel;

@end
