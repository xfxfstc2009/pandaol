//
//  PDPersonAddressNormalModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDPersonAddressNormalModel : FetchModel

@property (nonatomic,copy)NSString *name;

@end
