//
//  PDPersonAddressMainModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPersonAddressNormalModel.h"

@interface PDPersonAddressMainModel : FetchModel

@property (nonatomic,strong)PDPersonAddressNormalModel *city;
@property (nonatomic,strong)PDPersonAddressNormalModel *district;
@property (nonatomic,strong)PDPersonAddressNormalModel *province;



@end
