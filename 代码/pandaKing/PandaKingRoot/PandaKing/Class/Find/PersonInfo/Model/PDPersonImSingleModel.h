//
//  PDPersonImSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDPersonImSingleModel : FetchModel

@property (nonatomic,copy)NSString *userid;             /**< im的uid*/
@property (nonatomic,copy)NSString *password;           /**< im的密码*/

@end
