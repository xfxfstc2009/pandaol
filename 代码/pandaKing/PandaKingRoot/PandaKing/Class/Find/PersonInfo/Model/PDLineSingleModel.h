//
//  PDLineSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDLineSingleModel : FetchModel

@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *fixedStr;
@property (nonatomic,copy)NSString *dymmicStr;

@end
