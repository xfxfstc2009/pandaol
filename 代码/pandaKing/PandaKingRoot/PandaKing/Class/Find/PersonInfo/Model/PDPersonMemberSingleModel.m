//
//  PDPersonMemberSingleModel.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPersonMemberSingleModel.h"

@implementation PDPersonMemberSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"personId":@"id"};
}


-(BOOL)newGender{
    if ([self.gender isEqualToString:@"MALE"]){
        return YES;
    } else {
        return NO;
    }
}

@end
