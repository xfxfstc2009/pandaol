//
//  PDPersonMemberSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDPersonAddressNormalModel.h"
#import "PDPersonAddressMainModel.h"

@interface PDPersonMemberSingleModel : FetchModel

@property (nonatomic,copy)NSString *personId;                   /**< 用户编号*/
@property (nonatomic,copy)NSString *nickname;                   /**< 用户昵称*/
@property (nonatomic,copy)NSString *cellphone;                  /**< 联系号码*/
@property (nonatomic,copy)NSString *gender;                     /**< 性别*/
@property (nonatomic,assign)BOOL newGender;                     /**< 新性别*/
@property (nonatomic,copy)NSString *age;                        /**< 年龄*/
@property (nonatomic,copy)NSString *qq;                         /**< QQ*/
@property (nonatomic,copy)NSString *weChat;                     /**< 微信号*/
@property (nonatomic,copy)NSString *signature;                  /**< 签名*/
@property (nonatomic,copy)NSString *avatar;                     /**< 用户头像*/
@property (nonatomic,strong)NSArray *bindGames;                 /**< 绑定的游戏*/
@property (nonatomic,assign)NSInteger activateGameUser;         /**< 游戏*/
@property (nonatomic,strong)PDPersonAddressMainModel *address;
@property (nonatomic,assign)NSInteger bindGameUser;             /**< 绑定游戏玩家*/
@property (nonatomic,assign)NSInteger empiricValue;
@property (nonatomic,assign)BOOL enable;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)NSInteger level;


@end
