//
//  PDCenterPersonViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDCenterPersonViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferMemberId;                   /**< 传递过去的*/
@property (nonatomic, assign) BOOL isMe;

-(void)personCenterCollectionBlock:(void(^)(NSString *personId,BOOL collection))block;

@end
