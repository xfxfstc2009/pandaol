//
//  PDTransferAccountsDetailViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDPersonSingleModel.h"

@interface PDTransferAccountsDetailViewController : AbstractViewController

@property (nonatomic,strong)PDPersonSingleModel *transferPersonSingleModel;
@end
