//
//  PDTransferAccountsViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDTransferAccountsViewController.h"
#import "PDTransferAccountsHeaderCell.h"
#import "PDTransferAccountsDetailViewController.h"

@interface PDTransferAccountsViewController()<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>{
    UITextField *amountTextField;               /**< 金额*/
    UITextField *beizhuTextField;               /**< 备注*/
}
@property (nonatomic,strong)UITableView *zhuanzhangTableView;
@property (nonatomic,strong)NSArray *zhuanzhangArr;

@end

@implementation PDTransferAccountsViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    
}


-(void)pageSetting{
    self.barMainTitle = @"赠送";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"赠送记录" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDTransferAccountsDetailViewController *transferAccountDetailViewController = [[PDTransferAccountsDetailViewController alloc]init];
        transferAccountDetailViewController.transferPersonSingleModel = self.transferPersonSingleModel;
        [strongSelf.navigationController pushViewController:transferAccountDetailViewController animated:YES];
    }];
    
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.zhuanzhangArr = @[@[@"头部",@"金额",@"备注"],@[@"确认赠送"]];
}

-(void)createTableView{
    self.zhuanzhangTableView = [GWViewTool gwCreateTableViewRect:self.view.bounds];
    self.zhuanzhangTableView.dataSource = self;
    self.zhuanzhangTableView.delegate = self;
    [self.view addSubview:self.zhuanzhangTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.zhuanzhangArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.zhuanzhangArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellheight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if(indexPath.section == 0 && indexPath.row == 0){         // 头部
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDTransferAccountsHeaderCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDTransferAccountsHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            cellWithRowOne.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        }
        cellWithRowOne.transferCellHeight = cellheight;
        cellWithRowOne.transferPersonSingleModel = self.transferPersonSingleModel;

        return cellWithRowOne;
    } else if ((indexPath.section == 0 && indexPath.row == 1) || (indexPath.section == 0 && indexPath.row == 2)){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWInputTextFieldTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transfrCellHeight = cellheight;
        if (indexPath.row == 1){
            cellWithRowTwo.transferTitle = @"金额";
            cellWithRowTwo.transferPlaceholeder = @"请输入要输入金额";
            amountTextField = cellWithRowTwo.inputTextField;
            amountTextField.delegate = self;
            amountTextField.keyboardType = UIKeyboardTypeDecimalPad;
        } else if (indexPath.row == 2){
            cellWithRowTwo.transferTitle = @"备注";
            cellWithRowTwo.transferPlaceholeder = @"请输入要输入备注（20字以内）";
            beizhuTextField = cellWithRowTwo.inputTextField;
            beizhuTextField.delegate = self;
            beizhuTextField.keyboardType = UIKeyboardTypeDefault;
        }
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo textFieldDidChangeBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf adjustWithBtnStatus];
        }];
        
        return cellWithRowTwo;
        
    } else if (indexPath.section == 1 && indexPath.row == 0){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if(!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellheight;
        cellWithRowThr.transferTitle = @"确认赠送";
        [cellWithRowThr setButtonStatus:NO];
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf releaseTextField];
            [strongSelf sendRequestToZhuanzhangWithId:strongSelf.transferPersonSingleModel.member.personId amount:[strongSelf->amountTextField.text integerValue]  remark:strongSelf->beizhuTextField.text];
        }];
        return cellWithRowThr;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0){
        return LCFloat(160);
    } else{
        return LCFloat(44);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 1){
        return LCFloat(60);
    } else {
        return 0;
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.zhuanzhangTableView) {
        if (indexPath.section == 0){
            if(indexPath.row != 0){
                SeparatorType separatorType = SeparatorTypeMiddle;
                if ( [indexPath row] == 0) {
                    separatorType = SeparatorTypeHead;
                } else if ([indexPath row] == [[self.zhuanzhangArr objectAtIndex:indexPath.section] count] - 1) {
                    separatorType = SeparatorTypeBottom;
                } else {
                    separatorType = SeparatorTypeMiddle;
                }
                if ([[self.zhuanzhangArr objectAtIndex:indexPath.section] count] == 1) {
                    separatorType = SeparatorTypeSingle;
                }
                [cell addSeparatorLineWithType:separatorType];
            }
        }
    }
}

#pragma mark - IntefaceManager
-(void)sendRequestToManagerWithAnotherMemberId:(NSString *)anotherMemberId amount:(NSInteger)amount remark:(NSString *)remark{
    
    NSDictionary *params = @{@"anotherMemberId":anotherMemberId};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:requesttransfergoldtomem requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf sendRequestToZhuanzhangWithId:anotherMemberId amount:amount remark:remark];                   // 进行赠送
        }
    }];
}

-(void)sendRequestToZhuanzhangWithId:(NSString *)memberId amount:(NSInteger)amount remark:(NSString *)remark{
    __weak typeof(self)weakSelf = self;
    
    NSDictionary *params = @{@"anotherMemberId":memberId,@"amount":@(amount),@"remark":remark};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:transfergoldtomem requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [[PDAlertView sharedAlertView] showAlertWithTitle:@"赠送成功" conten:[NSString stringWithFormat:@"您已成功向【%@】赠送金币【%li】",strongSelf.transferPersonSingleModel.member.nickname,amount] isClose:YES btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                [[PDAlertView sharedAlertView] dismissAllWithBlock:^{
                    [strongSelf.navigationController popViewControllerAnimated:YES];
                }];
            }];
        }
    }];
}

-(void)setTransferPersonSingleModel:(PDPersonSingleModel *)transferPersonSingleModel{
    _transferPersonSingleModel = transferPersonSingleModel;
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]) {
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (textField == amountTextField){
        return YES;
    } else if (textField == beizhuTextField){
        if (toBeString.length > 20){
            textField.text = [toBeString substringToIndex:20];
            return NO;
        }
    }
    
    return YES;
}

-(void)adjustWithBtnStatus{
    GWButtonTableViewCell *btnCell = (GWButtonTableViewCell *)[self.zhuanzhangTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    if(amountTextField.text.length){
        [btnCell setButtonStatus:YES];
    } else {
        [btnCell setButtonStatus:NO];
    }
}

-(void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView{
    [self releaseTextField];
}

-(void)releaseTextField{
    if ([amountTextField isFirstResponder]){
        [amountTextField resignFirstResponder];
    } else if ([beizhuTextField isFirstResponder]){
        [beizhuTextField resignFirstResponder];
    }
}


@end
