//
//  PDCenterPersonViewHeaderCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDCenterPersonViewHeaderCell.h"

@interface PDCenterPersonViewHeaderCell()
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *levelLabel;
@property (nonatomic,strong)UILabel *idLabel;
@property (nonatomic,strong)PDImageView *goldImgView;
@property (nonatomic,strong)UILabel *goldLabel;
@property (nonatomic,strong)PDImageView *bambooImgView;
@property (nonatomic,strong)UILabel *bambooLabel;

@property (nonatomic,strong)UIView *genderView;
@property (nonatomic,strong)PDImageView *genderIcon;
@property (nonatomic,strong)UILabel *ageLabel;

@property (nonatomic,strong)UILabel *descLabel;

@property (nonatomic,strong)UIView *lineView;

@end

@implementation PDCenterPersonViewHeaderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = CGRectMake(LCFloat(11) * 4, LCFloat(11) * 2, LCFloat(60), LCFloat(60));
    [self addSubview:self.avatarImgView];
    
    // 2. 创建昵称
    self.nameLabel = [GWViewTool createLabelFont:@"标题" textColor:@"白"];
    self.nameLabel.font = [self.nameLabel.font boldFont];
    [self addSubview:self.nameLabel];
    
    // 3. 创建等级
    self.levelLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"白"];
    self.levelLabel.backgroundColor = [UIColor colorWithCustomerName:@"橙"];
    self.levelLabel.textAlignment = NSTextAlignmentCenter;
    self.levelLabel.font = [self.levelLabel.font boldFont];
    [self addSubview:self.levelLabel];
    
    // 4. 创建id
    self.idLabel = [GWViewTool createLabelFont:@"正文" textColor:@"浅灰"];
    [self addSubview:self.idLabel];
    
    // 5. 创建gold
    self.goldImgView = [[PDImageView alloc]init];
    self.goldImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.goldImgView];
    
    // 6. 创建金币
    self.goldLabel = [GWViewTool createLabelFont:@"提示" textColor:@"灰"];
    [self addSubview:self.goldLabel];
    
    // 7. 创建竹子
    self.bambooImgView = [[PDImageView alloc]init];
    self.bambooImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bambooImgView];
    
    // 8. 创建label
    self.bambooLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"灰"];
    [self addSubview:self.bambooLabel];
    
    [self createViewLabelWithGender];
    
    // 9. desc
    self.descLabel = [GWViewTool createLabelFont:@"正文" textColor:@"白"];
    self.descLabel.numberOfLines = 0;
    [self addSubview:self.descLabel];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"侧边分割线"];
    [self addSubview:self.lineView];
    
    self.guanzhuButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.guanzhuButton.backgroundColor = [UIColor clearColor];
    self.guanzhuButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    [self addSubview:self.guanzhuButton];
    
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferSingleModel:(PDCenterMyInfo *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    self.avatarImgView.frame = CGRectMake(LCFloat(11) * 4, LCFloat(11) * 2, LCFloat(60), LCFloat(60));
    [self.avatarImgView uploadImageWithAvatarURL:transferSingleModel.member.avatar placeholder:nil callback:NULL];
    
    // 2. title
    self.nameLabel.text = transferSingleModel.member.nickname;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + self.avatarImgView.orgin_x, self.avatarImgView.orgin_y, nameSize.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    // 3. level
    self.levelLabel.text = [NSString stringWithFormat:@"Lv %li",(long)transferSingleModel.member.level];
    CGSize levelSize = [self.levelLabel.text sizeWithCalcFont:self.levelLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.levelLabel.font])];
    CGFloat levelWidth = levelSize.width + 2 * LCFloat(7);
    CGFloat levelHeight = [NSString contentofHeightWithFont:self.levelLabel.font] + 2 * LCFloat(7);
    self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + LCFloat(11), 0, levelWidth, levelHeight);
    self.levelLabel.center_y = self.nameLabel.center_y;
    self.levelLabel.layer.cornerRadius = LCFloat(5);
    self.levelLabel.clipsToBounds = YES;
    
    // 4. 终生ID
    self.idLabel.text = [NSString stringWithFormat:@"终身ID：%@",transferSingleModel.member.personId];
    self.idLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), kScreenBounds.size.width - LCFloat(11) - self.nameLabel.orgin_x, [NSString contentofHeightWithFont:self.idLabel.font]);
    self.idLabel.adjustsFontSizeToFitWidth = YES;
    
    // 5. 创建gold
    self.goldImgView.image = [UIImage imageNamed:@"icon_center_gold"];
    self.goldImgView.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.idLabel.frame) + LCFloat(11), LCFloat(13), LCFloat(13));
    
    self.goldLabel.text = [NSString stringWithFormat:@"%li",self.transferSingleModel.gold];
    self.goldLabel.frame = CGRectMake(CGRectGetMaxX(self.goldImgView.frame) + LCFloat(7), 0, kScreenBounds.size.width - CGRectGetMaxX(self.goldImgView.frame) - LCFloat(11), [NSString contentofHeightWithFont:self.goldLabel.font]);
    self.goldLabel.center_y = self.goldImgView.center_y;
    
    // 6. 创建bamboo
    self.bambooImgView.image = [UIImage imageNamed:@"icon_center_bamboo"];
    self.bambooImgView.frame = CGRectMake(self.goldImgView.orgin_x, CGRectGetMaxY(self.goldLabel.frame) + LCFloat(11), LCFloat(13), LCFloat(13));
    
    self.bambooLabel.text = [NSString stringWithFormat:@"%li",self.transferSingleModel.bamboo];
    self.bambooLabel.frame = CGRectMake(self.goldLabel.orgin_x, 0, self.goldLabel.size_width, self.goldLabel.size_height);
    self.bambooLabel.center_y = self.bambooImgView.center_y;
    
    
    // 年纪
    BOOL gender = [transferSingleModel.member.gender isEqualToString:@"MALE"] ?YES :NO;
    [self autoUpdateInfoGender:gender Str:transferSingleModel.member.age];
    
    // desc
    self.descLabel.text = transferSingleModel.member.signature;
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    if (descSize.height >= 2 * [NSString contentofHeightWithFont:self.descLabel.font]){
        self.descLabel.textAlignment = NSTextAlignmentLeft;
    } else {
        self.descLabel.textAlignment = NSTextAlignmentCenter;
    }
    
    if (self.isMe){
        self.descLabel.frame = CGRectMake(LCFloat(11),CGRectGetMaxY(self.bambooImgView.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), descSize.height);
    }
    self.lineView.frame = CGRectMake(2 * LCFloat(11), CGRectGetMaxY(self.bambooImgView.frame) + LCFloat(11) / 2., kScreenBounds.size.width - 4 * LCFloat(11), .5f);
    
    self.guanzhuButton.hidden = YES;
}

-(void)setIsMe:(BOOL)isMe{
    _isMe = isMe;
}

#pragma mark - 创建性别
-(void)createViewLabelWithGender{
    self.genderView = [[UIView alloc]init];
    self.genderView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.genderView];
    
    // 1. 创建性别图标
    self.genderIcon = [[PDImageView alloc]init];
    self.genderIcon.image = [UIImage imageNamed:@"icon_nearby_girl"];
    self.genderIcon.frame = CGRectMake(0, 0, LCFloat(8), LCFloat(8));
    [self.genderView addSubview:self.genderIcon];
    
    // 2. 创建label
    self.ageLabel = [[UILabel alloc]init];
    self.ageLabel.backgroundColor = [UIColor clearColor];
    self.ageLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.ageLabel.textColor = [UIColor whiteColor];
    [self.genderView addSubview:self.ageLabel];
}

-(void)autoUpdateInfoGender:(BOOL)gender Str:(NSString *)str{
    if (gender == YES){             // 男
        self.genderIcon.image = [UIImage imageNamed:@"icon_nearby_boy"];
        self.genderView.backgroundColor = [UIColor colorWithCustomerName:@"浅蓝"];
    } else {                        // 女
        self.genderIcon.image = [UIImage imageNamed:@"icon_nearby_girl"];
        self.genderView.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
    }
    
    self.ageLabel.text = str;
    CGSize contentOfAgeSize = [self.ageLabel.text sizeWithCalcFont:self.ageLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.ageLabel.font])];
    
    CGFloat width = contentOfAgeSize.width + LCFloat(5) * 2 + LCFloat(2) + self.genderIcon.size_width;
    CGFloat height = [NSString contentofHeightWithFont:self.ageLabel.font] + 2 * LCFloat(2);
    
    self.genderIcon.frame = CGRectMake(LCFloat(5), (height - self.genderIcon.size_height ) / 2., self.genderIcon.size_width, self.genderIcon.size_height);
    self.ageLabel.frame = CGRectMake(CGRectGetMaxX(self.genderIcon.frame) + LCFloat(2), LCFloat(2), contentOfAgeSize.width, [NSString contentofHeightWithFont:self.ageLabel.font]);
    self.genderView.frame = CGRectMake(0, CGRectGetMaxY(self.avatarImgView.frame) + LCFloat(11), width, height);
    self.genderView.center_x = self.avatarImgView.center_x;
    
    // 修改frame
    self.genderView.clipsToBounds = YES;
    self.genderView.layer.cornerRadius = (MIN(self.genderView.size_width, self.genderView.size_height)) / 2.;
    
}


-(void)setTransferPersonSingleModel:(PDPersonSingleModel *)transferPersonSingleModel{
    _transferPersonSingleModel = transferPersonSingleModel;
    self.avatarImgView.frame = CGRectMake(LCFloat(11) * 4, LCFloat(11) * 2, LCFloat(60), LCFloat(60));
    [self.avatarImgView uploadImageWithAvatarURL:transferPersonSingleModel.member.avatar placeholder:nil callback:NULL];
    
    // 2. title
    self.nameLabel.text = transferPersonSingleModel.member.nickname;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + self.avatarImgView.orgin_x, self.avatarImgView.orgin_y, nameSize.width, [NSString contentofHeightWithFont:self.nameLabel.font]);
    
    // 3. level
    self.levelLabel.text = [NSString stringWithFormat:@"Lv %li",(long)transferPersonSingleModel.member.level];
    CGSize levelSize = [self.levelLabel.text sizeWithCalcFont:self.levelLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.levelLabel.font])];
    CGFloat levelWidth = levelSize.width + 2 * LCFloat(7);
    CGFloat levelHeight = [NSString contentofHeightWithFont:self.levelLabel.font] + 2 * LCFloat(7);
    self.levelLabel.frame = CGRectMake(CGRectGetMaxX(self.nameLabel.frame) + LCFloat(11), 0, levelWidth, levelHeight);
    self.levelLabel.center_y = self.nameLabel.center_y;
    self.levelLabel.layer.cornerRadius = LCFloat(5);
    self.levelLabel.clipsToBounds = YES;
    
    // 4. 终生ID
    self.idLabel.text = [NSString stringWithFormat:@"终身ID：%@",transferPersonSingleModel.member.personId];
    self.idLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(11), kScreenBounds.size.width - LCFloat(11) - self.nameLabel.orgin_x, [NSString contentofHeightWithFont:self.idLabel.font]);
    self.idLabel.adjustsFontSizeToFitWidth = YES;
    
    // 5. 创建gold
    self.goldImgView.image = [UIImage imageNamed:@"icon_center_gold"];
    self.goldImgView.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.idLabel.frame) + LCFloat(11), LCFloat(13), LCFloat(13));
    
    self.goldLabel.text = [NSString stringWithFormat:@"%@",transferPersonSingleModel.goldBalance];
    self.goldLabel.frame = CGRectMake(CGRectGetMaxX(self.goldImgView.frame) + LCFloat(7), 0, kScreenBounds.size.width - CGRectGetMaxX(self.goldImgView.frame) - LCFloat(11), [NSString contentofHeightWithFont:self.goldLabel.font]);
    self.goldLabel.center_y = self.goldImgView.center_y;
    
    // 6. 创建bamboo
    self.bambooImgView.image = [UIImage imageNamed:@"icon_center_bamboo"];
    self.bambooImgView.frame = CGRectMake(self.goldImgView.orgin_x, CGRectGetMaxY(self.goldLabel.frame) + LCFloat(11), LCFloat(13), LCFloat(13));
    
    self.bambooLabel.text = [NSString stringWithFormat:@"%@",transferPersonSingleModel.bambooBalance];
    self.bambooLabel.frame = CGRectMake(self.goldLabel.orgin_x, 0, self.goldLabel.size_width, self.goldLabel.size_height);
    self.bambooLabel.center_y = self.bambooImgView.center_y;
    
    
    // 年纪
    BOOL gender = [transferPersonSingleModel.member.gender isEqualToString:@"MALE"] ?YES :NO;
    [self autoUpdateInfoGender:gender Str:transferPersonSingleModel.member.age];
    
    // desc
    self.descLabel.text = transferPersonSingleModel.member.signature;
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    self.descLabel.textAlignment = NSTextAlignmentLeft;
    
    
    self.lineView.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.bambooImgView.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11) - self.nameLabel.orgin_x, .5f);
    
    if (self.isMe){
        self.descLabel.frame = CGRectMake(LCFloat(11),CGRectGetMaxY(self.bambooImgView.frame) + LCFloat(11), kScreenBounds.size.width - 2 * LCFloat(11), descSize.height);
    } else {
        CGFloat width = kScreenBounds.size.width - LCFloat(11) * 8 - 60 - 2 * LCFloat(11);
        self.descLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.lineView.frame) + LCFloat(11), width, descSize.height);
    }
    
    if (transferPersonSingleModel.isFollow){
        self.guanzhuButton.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
        [self.guanzhuButton setTitle:@"已关注" forState:UIControlStateNormal];
    } else {
        self.guanzhuButton.backgroundColor = [UIColor colorWithCustomerName:@"土"];
        [self.guanzhuButton setTitle:@"关注" forState:UIControlStateNormal];
    }
    CGSize guanzhuSize = [@"已关注" sizeWithCalcFont:self.guanzhuButton.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.guanzhuButton.titleLabel.font])];
    self.guanzhuButton.frame = CGRectMake(0, CGRectGetMaxY(self.genderView.frame) + LCFloat(11), guanzhuSize.width + 2 * LCFloat(11), [NSString contentofHeightWithFont:self.guanzhuButton.titleLabel.font] + LCFloat(7) * 2);
    self.guanzhuButton.layer.cornerRadius = LCFloat(7);
    self.guanzhuButton.clipsToBounds = YES;
    self.guanzhuButton.hidden = NO;
    self.guanzhuButton.center_x = self.genderView.center_x;
}

#pragma mark - 抛出我的高度
+(CGFloat)calculationCellHeightWithModel:(PDCenterMyInfo *)transferSingleModel{
    CGFloat cellHeight = 0;

    cellHeight += LCFloat(11) * 2;
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"标题"]];
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(13);
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(13);
    cellHeight += LCFloat(11);
    CGSize descSize = [transferSingleModel.member.signature sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(11), CGFLOAT_MAX)];
    cellHeight += descSize.height;
    cellHeight += LCFloat(11);
    
    return cellHeight;
}

#pragma mark - 抛出我的高度
+(CGFloat)calculationCellHeightWithPersonModel:(PDPersonSingleModel *)transferSingleModel{
    CGFloat cellHeight = 0;
    
    cellHeight += LCFloat(11) * 2;
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"标题"]];
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(13);
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(13);
    cellHeight += LCFloat(11);
    cellHeight += LCFloat(11);
    
    CGFloat width = kScreenBounds.size.width - LCFloat(11) * 8 - 60 - 2 * LCFloat(11);
    
    CGSize descSize = [transferSingleModel.member.signature sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += descSize.height;
    cellHeight += LCFloat(11);
    
    return cellHeight;
}


-(void)buttonWithGuanzhu:(BOOL)hasGuanzhu{
    __weak typeof(self)weakSelf = self;
    [Tool clickZanWithView:self.guanzhuButton block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (hasGuanzhu == YES){
            [strongSelf.guanzhuButton setTitle:@"已关注" forState:UIControlStateNormal];
            strongSelf.guanzhuButton.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
        } else {
            [strongSelf.guanzhuButton setTitle:@"关注" forState:UIControlStateNormal];
            strongSelf.guanzhuButton.backgroundColor = [UIColor colorWithCustomerName:@"土"];
        }
    }];
}

@end
