//
//  PDCenterPersonViewHeaderCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterMyInfo.h"
#import "PDPersonSingleModel.h"

@interface PDCenterPersonViewHeaderCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDCenterMyInfo *transferSingleModel;                /**< 我的召唤师*/
@property (nonatomic,assign)BOOL isMe;
@property (nonatomic,strong)PDPersonSingleModel *transferPersonSingleModel;
@property (nonatomic,strong)UIButton *guanzhuButton;

+(CGFloat)calculationCellHeightWithModel:(PDCenterMyInfo *)transferSingleModel;
+(CGFloat)calculationCellHeightWithPersonModel:(PDPersonSingleModel *)transferSingleModel;

-(void)buttonWithGuanzhu:(BOOL)hasGuanzhu;
@end
