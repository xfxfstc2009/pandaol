//
//  PDPersonalNormalCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPersonalNormalCell.h"

@interface PDPersonalNormalCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,strong)PDImageView *arrowImageView;
@property (nonatomic,strong)UIView *iconView;

@end


@implementation PDPersonalNormalCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建fixedLabel
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"]boldFont];
    [self addSubview:self.fixedLabel];
    
    // 2. 创建dymicLabel
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.dymicLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.dymicLabel];
    
    // 3. 创建箭头符号
    self.arrowImageView = [[PDImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    self.arrowImageView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    [self addSubview:self.arrowImageView];
    
    // 4. 创建view
    self.iconView = [[UIView alloc]init];
    self.iconView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferNormalCellType:(PDPersonalNormalCellType)transferNormalCellType{
    _transferNormalCellType = transferNormalCellType;
}



-(void)setTransferPersonSingleModel:(PDPersonSingleModel *)transferPersonSingleModel{
    _transferPersonSingleModel = transferPersonSingleModel;
    if (self.transferNormalCellType == PDPersonalNormalCellTypeQQ){
        self.fixedLabel.text = @"QQ";
        self.dymicLabel.text = transferPersonSingleModel.member.qq.length?transferPersonSingleModel.member.qq:@"用户暂未提交QQ";
        self.dymicLabel.hidden = NO;
        self.arrowImageView.hidden = YES;
    } else if (self.transferNormalCellType == PDPersonalNormalCellTypeWechat){
        self.fixedLabel.text = @"微信";
        self.dymicLabel.text = transferPersonSingleModel.member.weChat.length?transferPersonSingleModel.member.weChat:@"用户暂未提交微信";
        self.dymicLabel.hidden = NO;
        self.arrowImageView.hidden = YES;
    } else if (self.transferNormalCellType == PDPersonalNormalCellTypeTAGame){
        self.fixedLabel.text = @"TA的游戏";
        self.dymicLabel.hidden = YES;
        self.arrowImageView.hidden = YES;
    } else if (self.transferNormalCellType == PDPersonalNormalCellTypeTARole){
        self.fixedLabel.text = @"TA的召唤师";
        self.dymicLabel.text = transferPersonSingleModel.gameUserName.length? transferPersonSingleModel.gameUserName:@"该用户还未提交召唤师信息";
        self.arrowImageView.hidden = YES;
        self.dymicLabel.hidden = NO;
    } else if (self.transferNormalCellType == PDPersonalNormalCellTypeTeam){
        self.fixedLabel.text = @"TA的战队";
        self.arrowImageView.hidden = NO;
    } else if (self.transferNormalCellType == PDPersonalNormalCellTypeDuobao){
        self.fixedLabel.text = @"TA的夺宝";
        if (transferPersonSingleModel.latestStakeTime != 0){
            self.dymicLabel.text = [NSDate getTimeWithDuobaoString:(transferPersonSingleModel.latestStakeTime / 1000.)];
        }
        self.arrowImageView.hidden = NO;
    }
    
    CGSize fixedSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellHeight)];
    self.fixedLabel.frame = CGRectMake(LCFloat(11), 0, fixedSize.width, self.transferCellHeight);
    
    // 2. 创建文字
    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellHeight)];
    self.dymicLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - dymicSize.width, 0, dymicSize.width, self.transferCellHeight);
    
    // 3.  创建iconView
    if (self.iconView.subviews.count){
        [self.iconView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    self.iconView.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + LCFloat(11), 0, kScreenBounds.size.width - CGRectGetMaxX(self.fixedLabel.frame), self.transferCellHeight);
    
    if (self.arrowImageView.hidden == YES){
        self.iconView.size_width = kScreenBounds.size.width - CGRectGetMaxX(self.fixedLabel.frame);
    } else {
        self.iconView.size_width = self.arrowImageView.orgin_x - LCFloat(11) - CGRectGetMaxX(self.fixedLabel.frame);
    }
    
    CGFloat height = self.transferCellHeight - 2 * LCFloat(5);
    CGFloat width = self.transferCellHeight - 2 * LCFloat(5);
    CGFloat margin = LCFloat(10);
    if (self.transferNormalCellType == PDPersonalNormalCellTypeTAGame){         // 他的游戏

        
        for (int i = 0 ; i < transferPersonSingleModel.member.bindGames.count;i++){
            NSString *url = [transferPersonSingleModel.member.bindGames objectAtIndex:i];
            CGFloat origin_X = self.iconView.size_width - LCFloat(11) - (width + margin) * (i + 1);
            PDImageView *imageView = [[PDImageView alloc]init];
            imageView.frame = CGRectMake(origin_X, LCFloat(5), width, height);
            if ([url isEqualToString:@"LOL"]|| [url isEqualToString:@"lol"]){
                imageView.image = [UIImage imageNamed:@"icon_game_lol"];
            }
            [self.iconView addSubview:imageView];
        }
        
        self.iconView.backgroundColor = [UIColor clearColor];

        
    } else if (self.transferNormalCellType == PDPersonalNormalCellTypeTeam){    // 他的战队
        
    }
    
    // 4. 创建箭头
    self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(9), (self.transferCellHeight - LCFloat(15)) / 2., LCFloat(9), LCFloat(15));
    if (self.arrowImageView.hidden == NO){
        self.dymicLabel.orgin_x = self.arrowImageView.orgin_x - LCFloat(11) - self.dymicLabel.size_width;
    } else {
        self.dymicLabel.orgin_x = kScreenBounds.size.width - LCFloat(11) - self.dymicLabel.size_width;
    }
}


+(CGFloat)calculationCellHeight{
    return LCFloat(50);
}

@end
