//
//  PDLineCollectionView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLineCollectionView.h"
#import "LineLayout.h"
#import "PDLineCollectionCell.h"

@interface PDLineCollectionView()<UICollectionViewDelegateFlowLayout,UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong)UICollectionView *lineCollectionView;
@property (nonatomic,strong)NSArray *tempArr;

@end

@implementation PDLineCollectionView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame arr:(NSArray *)array{
    self = [super initWithFrame:frame];
    if (self){
        self.tempArr  = array;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    [self createCollectionView];
}

#pragma mark - createCollectionView
-(void)createCollectionView{
    LineLayout *layout = [[LineLayout alloc] init];
    
    self.lineCollectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
    self.lineCollectionView.backgroundColor = [UIColor clearColor];
    self.lineCollectionView.showsVerticalScrollIndicator = NO;
    self.lineCollectionView.delegate = self;
    self.lineCollectionView.dataSource = self;
    self.lineCollectionView.showsHorizontalScrollIndicator = NO;
    self.lineCollectionView.scrollsToTop = YES;
    self.lineCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self addSubview:self.lineCollectionView];
    
    [self.lineCollectionView registerClass:[PDLineCollectionCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.tempArr.count;
}

#pragma mark - UICollectionViewDelegate
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PDLineCollectionCell *cell = (PDLineCollectionCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify"  forIndexPath:indexPath];
    
    cell.transferSingleModel = [self.tempArr objectAtIndex:indexPath.row];
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(linelayoutItemWH,linelayoutItemWH);
}


#pragma mark - 刷新数据
-(void)uploadWithArr:(NSArray *)infoArr{
    self.tempArr  = infoArr;
    [self.lineCollectionView reloadData];
    CGFloat width = self.lineCollectionView.size_width / 2. - 30;
    [self.lineCollectionView setContentOffset:CGPointMake(width , 0) animated:YES];
}

@end
