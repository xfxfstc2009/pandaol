//
//  PDLineCollectionCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLineCollectionCell.h"
#import "PDLineSingleModel.h"

@interface PDLineCollectionCell()
@property (nonatomic,strong)PDImageView *avatarView;
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)UILabel *fixedLabel;

@end

@implementation PDLineCollectionCell

-(instancetype)initWithFrame:(CGRect)frame singleModel:(PDLineSingleModel *)singleModel{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 背景
    self.avatarView = [[PDImageView alloc]init];
    self.avatarView.backgroundColor = [UIColor whiteColor];
    self.avatarView.frame = CGRectMake((self.size_width - LCFloat(70)) / 2., (self.size_height - LCFloat(70)) / 2., LCFloat(70), LCFloat(70));
    self.avatarView.autoresizingMask = UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin;
    self.avatarView.layer.borderWidth = 1;
    self.avatarView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.avatarView.layer.cornerRadius = self.avatarView.size_height / 2.;
    self.avatarView.clipsToBounds = YES;
    [self addSubview:self.avatarView];
    
    // 3. 创建数字
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self.avatarView addSubview:self.numberLabel];
    
    // 2. 创建fixedlabel
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.fixedLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    [self.avatarView addSubview:self.fixedLabel];
    
    CGFloat margin = (self.avatarView.size_height - [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"提示"]] - [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]]) / 5.;
    self.numberLabel.frame = CGRectMake(0, 2 * margin, self.avatarView.size_width, [NSString contentofHeightWithFont:self.numberLabel.font]);
    self.fixedLabel.frame = CGRectMake(0, CGRectGetMaxY(self.numberLabel.frame) + margin, self.avatarView.size_width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
}

-(void)setTransferSingleModel:(PDLineSingleModel *)transferSingleModel{
    if (transferSingleModel){
        _transferSingleModel = transferSingleModel;
        
        if (self.transferSingleModel.avatar.length){                // 有头像的地方
            __weak typeof(self)weakSelf = self;
            [self.avatarView uploadImageWithAvatarURL:transferSingleModel.avatar placeholder:nil callback:^(UIImage *image) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (!image){
                    strongSelf.avatarView.image = [UIImage imageNamed:@"icon_nordata"];
                }
            }];
            self.avatarView.hidden = NO;
            self.fixedLabel.hidden = YES;
            self.numberLabel.hidden = YES;
        } else {
            self.avatarView.hidden = NO;
            self.avatarView.image = nil;
            self.fixedLabel.hidden = NO;
            self.numberLabel.hidden = NO;
            self.fixedLabel.text = transferSingleModel.fixedStr;
            self.numberLabel.text = transferSingleModel.dymmicStr;
        }
    }
}


@end
