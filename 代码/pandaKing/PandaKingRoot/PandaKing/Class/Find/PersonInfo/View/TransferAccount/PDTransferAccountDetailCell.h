//
//  PDTransferAccountDetailCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDTransferAccountSingleModel.h"

@interface PDTransferAccountDetailCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDTransferAccountSingleModel *transferAccountSingleModel;

+(CGFloat)calculationCellHeight;

@end
