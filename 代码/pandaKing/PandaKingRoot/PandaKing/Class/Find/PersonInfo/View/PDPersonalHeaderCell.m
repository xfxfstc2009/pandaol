//
//  PDPersonalHeaderCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPersonalHeaderCell.h"
#import "PDLineCollectionView.h"

@interface PDPersonalHeaderCell()
@property (nonatomic,strong)PDImageView *bgImageView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *idLabel;
@property (nonatomic,strong)UIView *floatView;                          /**< 浮层view*/
@property (nonatomic,strong)UILabel *contentLabel;                      /**< 详情label*/
@property (nonatomic,strong)CAShapeLayer *alphaLayer;

@property (nonatomic,strong)UIView *genderView;
@property (nonatomic,strong)PDImageView *genderIcon;
@property (nonatomic,strong)UILabel *ageLabel;

@property (nonatomic,strong)PDLineCollectionView *lineCollectionView;
@property (nonatomic,strong)UILabel *contentsLabel;

@end

@implementation PDPersonalHeaderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
        self.clipsToBounds = YES;
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1.创建背景图片
    self.bgImageView = [[PDImageView alloc]init];
    self.bgImageView.backgroundColor = [UIColor clearColor];
    self.bgImageView.clipsToBounds = YES;
    [self addSubview:self.bgImageView];
    
    // 2. 创建上面的浮层
    self.floatView = [[UIView alloc]init];
    self.floatView.backgroundColor = [UIColor clearColor];
    [self.bgImageView addSubview:self.floatView];

    // 2.1 创建背景的alpha
    self.alphaLayer = [CAShapeLayer layer];
    self.alphaLayer.shadowColor = [[UIColor colorWithCustomerName:@"灰"] CGColor];
    self.alphaLayer.shadowOpacity = 2.0;
    self.alphaLayer.shadowOffset = CGSizeMake(1.2f, 1.2f);
    self.alphaLayer.fillColor = [UIColor whiteColor].CGColor;
    [self.floatView.layer addSublayer:self.alphaLayer];
    
    // 3. 创建年龄view
    self.contentLabel = [[UILabel alloc]init];
    self.contentLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.contentLabel];

    
    // 4. 创建内容
    self.contentLabel = [[UILabel alloc]init];
    self.contentLabel.backgroundColor = [UIColor clearColor];
    self.contentLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.contentLabel.backgroundColor = [UIColor clearColor];
    self.contentLabel.numberOfLines = 0;
    self.contentLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    self.contentLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.contentLabel];

    // 5.创建年龄
    [self createViewLabelWithGender];
    
    // 6. 创建终生id
    self.idLabel = [[UILabel alloc]init];
    self.idLabel.backgroundColor = [UIColor clearColor];
    self.idLabel.textAlignment = NSTextAlignmentCenter;
    self.idLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.idLabel.textColor = [UIColor whiteColor];
    self.idLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.idLabel];
    
    self.lineCollectionView = [[PDLineCollectionView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(100))];
    self.lineCollectionView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(200));
    [self addSubview:self.lineCollectionView];
    
    // 7.创建内容
    self.contentsLabel = [[UILabel alloc]init];
    self.contentsLabel.backgroundColor = [UIColor clearColor];
    self.contentsLabel.textAlignment = NSTextAlignmentCenter;
    self.contentsLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.contentsLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.contentsLabel];
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferPersonSingleModel:(PDPersonSingleModel *)transferPersonSingleModel{
    _transferPersonSingleModel = transferPersonSingleModel;
    
    // 1. 背景图片
    self.bgImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.transferCellHeight);
    __weak typeof(self)weakSelf = self;
    [self.bgImageView uploadImageWithURL:transferPersonSingleModel.member.avatar placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (image){
            strongSelf.bgImageView.image = [image applyExtraLightEffectforDarkAres];
        } else {
            strongSelf.bgImageView.image = [[UIImage imageNamed:@"icon_nordata"] applyDarkEffect];
        }
    }];
    
    // 3. 创建签名
    self.contentLabel.text = transferPersonSingleModel.member.signature.length?transferPersonSingleModel.member.signature:@"TA很懒，什么都没有留下";
    CGSize contentSize = [self.contentLabel.text sizeWithCalcFont:self.contentLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * LCFloat(30), CGFLOAT_MAX)];
    CGFloat contentHeight = 0;
    if (contentSize.height > 3 * [NSString contentofHeightWithFont:self.contentLabel.font]){
        self.contentLabel.numberOfLines = 3;
        contentHeight = 3 * [NSString contentofHeightWithFont:self.contentLabel.font];
    } else {
        self.contentLabel.numberOfLines = 0;
        contentHeight = contentSize.height;
    }
    self.contentLabel.frame = CGRectMake(LCFloat(30), self.transferCellHeight - LCFloat(11) - contentHeight, kScreenBounds.size.width - 2 * LCFloat(30), contentHeight);
    
    // 2. 创建浮层
    self.floatView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.transferCellHeight);
    CGSize finalSize = CGSizeMake(kScreenBounds.size.width, self.transferCellHeight + 5);
    CGFloat layerHeight =  LCFloat(11) + contentHeight + LCFloat(11) + 60;
    UIBezierPath *bezier = [UIBezierPath bezierPath];
    
    [bezier moveToPoint:CGPointMake(0, finalSize.height - layerHeight)];
    [bezier addLineToPoint:CGPointMake(0, finalSize.height - 1)];
    [bezier addLineToPoint:CGPointMake(finalSize.width, finalSize.height - 1)];
    [bezier addLineToPoint:CGPointMake(finalSize.width, finalSize.height - layerHeight)];
    [bezier addQuadCurveToPoint:CGPointMake(0,finalSize.height - layerHeight) controlPoint:CGPointMake(finalSize.width / 2, (finalSize.height - layerHeight) + 70)];
    [bezier closePath];
    self.alphaLayer.path = bezier.CGPath;
    
    // 3. 年龄性别
    [self autoUpdateInfoGender:transferPersonSingleModel.member.newGender Str:transferPersonSingleModel.member.age];
    
    // 5.
    self.contentsLabel.text = [NSString stringWithFormat:@"粉丝 %@ | 关注 %@",transferPersonSingleModel.fans,transferPersonSingleModel.follow];
    self.contentsLabel.frame = CGRectMake(0, self.genderView.orgin_y - [NSString contentofHeightWithFont:self.contentsLabel.font], kScreenBounds.size.width, [NSString contentofHeightWithFont:self.contentsLabel.font]);
    
    // 4. 创建终生id
    self.idLabel.text = [NSString stringWithFormat:@"终身ID:%@",transferPersonSingleModel.member.personId];
//    CGSize contentOfSize = [self.idLabel.text sizeWithCalcFont:self.idLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.idLabel.font])];
    self.idLabel.frame = CGRectMake(0, self.contentsLabel.orgin_y - [NSString contentofHeightWithFont:self.idLabel.font], kScreenBounds.size.width, [NSString contentofHeightWithFont:self.idLabel.font]);
    // 5. 创建高度
    
    self.lineCollectionView.frame = CGRectMake(0, 0, kScreenBounds.size.width,self.transferCellHeight - layerHeight);
    if (transferPersonSingleModel){
        NSMutableArray *infoArr = [NSMutableArray array];
        for (int i = 0 ; i < 3; i ++){
            PDLineSingleModel *lineSingleModel = [[PDLineSingleModel alloc]init];
            if (i == 0){            // 关注
                lineSingleModel.avatar = @"";
                lineSingleModel.fixedStr = @"金币";
                lineSingleModel.dymmicStr = transferPersonSingleModel.goldBalance;
            } else if (i == 1){
                lineSingleModel.avatar = transferPersonSingleModel.member.avatar;
                lineSingleModel.fixedStr = @"";
                lineSingleModel.dymmicStr = @"";
            } else if (i == 2){
                lineSingleModel.avatar = @"";
                lineSingleModel.fixedStr = @"竹子";
                lineSingleModel.dymmicStr = transferPersonSingleModel.bambooBalance;
            }
            [infoArr addObject:lineSingleModel];
        }
        [self.lineCollectionView uploadWithArr:infoArr];
    }
}


#pragma mark - 创建性别
-(void)createViewLabelWithGender{
    self.genderView = [[UIView alloc]init];
    self.genderView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.genderView];
    
    // 1. 创建性别图标
    self.genderIcon = [[PDImageView alloc]init];
    self.genderIcon.image = [UIImage imageNamed:@"icon_nearby_girl"];
    self.genderIcon.frame = CGRectMake(0, 0, LCFloat(8), LCFloat(8));
    [self.genderView addSubview:self.genderIcon];
    
    // 2. 创建label
    self.ageLabel = [[UILabel alloc]init];
    self.ageLabel.backgroundColor = [UIColor clearColor];
    self.ageLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.ageLabel.textColor = [UIColor whiteColor];
    [self.genderView addSubview:self.ageLabel];
}

-(void)autoUpdateInfoGender:(BOOL)gender Str:(NSString *)str{
    if (gender == YES){             // 男
        self.genderIcon.image = [UIImage imageNamed:@"icon_nearby_boy"];
        self.genderView.backgroundColor = [UIColor colorWithCustomerName:@"浅蓝"];
    } else {                        // 女
        self.genderIcon.image = [UIImage imageNamed:@"icon_nearby_girl"];
        self.genderView.backgroundColor = [UIColor colorWithCustomerName:@"粉"];
    }
    
    self.ageLabel.text = str;
    CGSize contentOfAgeSize = [self.ageLabel.text sizeWithCalcFont:self.ageLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.ageLabel.font])];
    
    CGFloat width = contentOfAgeSize.width + LCFloat(5) * 2 + LCFloat(2) + self.genderIcon.size_width;
    CGFloat height = [NSString contentofHeightWithFont:self.ageLabel.font] + 2 * LCFloat(2);
    
    self.genderIcon.frame = CGRectMake(LCFloat(5), (height - self.genderIcon.size_height ) / 2., self.genderIcon.size_width, self.genderIcon.size_height);
    self.ageLabel.frame = CGRectMake(CGRectGetMaxX(self.genderIcon.frame) + LCFloat(2), LCFloat(2), contentOfAgeSize.width, [NSString contentofHeightWithFont:self.ageLabel.font]);
    self.genderView.frame = CGRectMake((kScreenBounds.size.width - width) / 2., self.contentLabel.orgin_y - LCFloat(40) - height, width, height);
    
    // 修改frame
    self.genderView.clipsToBounds = YES;
    self.genderView.layer.cornerRadius = (MIN(self.genderView.size_width, self.genderView.size_height)) / 2.;
    
}

+(CGFloat)calculationCellHeight{
    return LCFloat(300);
}

@end
