//
//  PDTransferAccountDetailCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/3.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDTransferAccountDetailCell.h"

@interface PDTransferAccountDetailCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *priceLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *successLabel;

@end

@implementation PDTransferAccountDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    // 2. 创建价格
    self.priceLabel = [GWViewTool createLabelFont:@"标题" textColor:@"黑"];
    self.priceLabel.font = [self.priceLabel.font boldFont];
    [self addSubview:self.priceLabel];
    
    // 3. 创建时间
    self.timeLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [self addSubview:self.timeLabel];
    
    // 4. successLabel
    self.successLabel = [GWViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    self.successLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.successLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferAccountSingleModel:(PDTransferAccountSingleModel *)transferAccountSingleModel{
    _transferAccountSingleModel = transferAccountSingleModel;
    // 1. 金币
    self.priceLabel.text = [NSString stringWithFormat:@"%@",transferAccountSingleModel.deltaFee];
    CGSize priceSize = [self.priceLabel.text sizeWithCalcFont:self.priceLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.priceLabel.font])];
    self.priceLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - priceSize.width, LCFloat(11), priceSize.width, [NSString contentofHeightWithFont:self.priceLabel.font]);
    
    // 2. success
    self.successLabel.text = @"转账成功";
    CGSize successSize = [self.successLabel.text sizeWithCalcFont:self.successLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.successLabel.font])];
    self.successLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - successSize.width, CGRectGetMaxY(self.priceLabel.frame) + LCFloat(11), successSize.width, [NSString contentofHeightWithFont:self.successLabel.font]);
    
    // 3. 转账
    CGFloat width = MIN(self.priceLabel.orgin_x, self.successLabel.orgin_x) - LCFloat(11) - LCFloat(11);
    self.titleLabel.text = transferAccountSingleModel.remark;
    self.titleLabel.frame = CGRectMake(LCFloat(11), LCFloat(11), width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.adjustsFontSizeToFitWidth = YES;
    
    // 4. 时间
    self.timeLabel.text = [NSDate getTimeGap:transferAccountSingleModel.recordTime / 1000.];
    CGSize timeSize = [self.timeLabel.text sizeWithCalcFont:self.timeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.timeLabel.font])];
    self.timeLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.titleLabel.frame) + LCFloat(11), timeSize.width, [NSString contentofHeightWithFont:self.timeLabel.font]);
    self.timeLabel.center_y = self.successLabel.center_y;
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"标题"]];
    cellHeight += LCFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(11);
    return cellHeight;
}

@end
