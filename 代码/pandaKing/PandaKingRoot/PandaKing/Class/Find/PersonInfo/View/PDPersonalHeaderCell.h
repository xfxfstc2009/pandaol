//
//  PDPersonalHeaderCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 个人中心头部
#import <UIKit/UIKit.h>
#import "PDPersonSingleModel.h"

@interface PDPersonalHeaderCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDPersonSingleModel *transferPersonSingleModel;         /**< 传递model*/

+(CGFloat)calculationCellHeight;

@end
