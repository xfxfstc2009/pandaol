//
//  PDLineCollectionView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLineSingleModel.h"
@interface PDLineCollectionView : UIView

-(instancetype)initWithFrame:(CGRect)frame arr:(NSArray *)array;

-(void)uploadWithArr:(NSArray *)infoArr;

@end
