//
//  PDLineCollectionCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLineSingleModel.h"

typedef NS_ENUM(NSInteger,PDLineCollectionCellType) {
    PDLineCollectionCellTypeImg,                            /**< 头像*/
    PDLineCollectionCellTypeTitle,                          /**< 文字*/
};

@interface PDLineCollectionCell : UICollectionViewCell

@property (nonatomic,strong)PDLineSingleModel *transferSingleModel;
@end
