//
//  PDPersonalNormalCell.h
//  PandaKing
//
//  Created by GiganticWhale on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPersonSingleModel.h"

typedef NS_ENUM(NSInteger,PDPersonalNormalCellType) {
    PDPersonalNormalCellTypeQQ,                             /**< QQ*/
    PDPersonalNormalCellTypeWechat,                         /**< wechat*/
    PDPersonalNormalCellTypeTAGame,                         /**< 他的游戏*/
    PDPersonalNormalCellTypeTARole,                         /**< 他的召唤师*/
    PDPersonalNormalCellTypeTeam,                           /**< 他的战队*/
    PDPersonalNormalCellTypeDuobao,                         /**< 他的夺宝*/
};


@interface PDPersonalNormalCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,assign)PDPersonalNormalCellType transferNormalCellType;
@property (nonatomic,strong)PDPersonSingleModel *transferPersonSingleModel;

+(CGFloat)calculationCellHeight;
@end
