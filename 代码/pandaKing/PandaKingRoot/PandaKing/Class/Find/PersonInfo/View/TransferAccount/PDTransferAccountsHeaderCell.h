//
//  PDTransferAccountsHeaderCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPersonSingleModel.h"


@interface PDTransferAccountsHeaderCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDPersonSingleModel *transferPersonSingleModel;

+(CGFloat)calculationCellHeight;

@end
