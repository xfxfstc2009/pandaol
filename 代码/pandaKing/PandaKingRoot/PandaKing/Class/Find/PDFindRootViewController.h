//
//  PDFindRootViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,FindType) {
    FindTypeInformstion = 0,                            /**< 发现资讯*/
    FindTypeCircle = 1,                                 /**< 发现圈子*/
};



@interface PDFindRootViewController : AbstractViewController

+(instancetype)sharedController;

@end
