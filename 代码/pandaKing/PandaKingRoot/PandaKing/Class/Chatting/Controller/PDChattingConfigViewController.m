//
//  PDChattingConfigViewController.m
//  PandaKing
//
//  Created by Cranz on 16/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChattingConfigViewController.h"
#import "PDChattingConfigMemberInfoCell.h"
#import "PDCenterPersonViewController.h"
#import "PDChattingConfigModel.h"

@interface PDChattingConfigViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) PDChattingConfigModel *configModel;
@end

@implementation PDChattingConfigViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    self.navigationController.navigationBar.layer.shadowColor = [UIColor clearColor].CGColor;
    self.barMainTitle = @"聊天设置";
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [[UIView alloc] init];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
}

#pragma mark -  UITabelViewdelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        NSString *cellId = @"memberCellId";
        PDChattingConfigMemberInfoCell *memberInfoCell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!memberInfoCell) {
            memberInfoCell = [[PDChattingConfigMemberInfoCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
        memberInfoCell.model = self.configModel;
        __weak typeof(self) weakSelf = self;
        [memberInfoCell chattingCellWithMore:^(UIButton *button) {
            [weakSelf fetchAddFollow];
        }];
        return memberInfoCell;
    } else {
        NSString *cellId = @"addToBlackCellId";
        UITableViewCell *addToBlackCell = [tableView dequeueReusableCellWithIdentifier:cellId];
        if (!addToBlackCell) {
            addToBlackCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            addToBlackCell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            // 加入黑名单switch
            UISwitch *blackSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(0, 0, 80, 30)];
            blackSwitch.on = [[PDEMManager sharedInstance] isMemberInBlackList:self.memberId];
            [blackSwitch addTarget:self action:@selector(didAddToBlackList:) forControlEvents:UIControlEventValueChanged];
            addToBlackCell.accessoryView = blackSwitch;
        }
        addToBlackCell.textLabel.text = @"加入黑名单";
        return addToBlackCell;
//            else {
//            NSString *cellId = @"clearCellId";
//            UITableViewCell *clearCacheCell = [tableView dequeueReusableCellWithIdentifier:cellId];
//            if (!clearCacheCell) {
//                clearCacheCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:cellId];
//                    clearCacheCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//            }
//            clearCacheCell.textLabel.text = @"清除聊天缓存";
//            clearCacheCell.detailTextLabel.text = [[PDIMManager sharedManager] getAllCacheSize];
//            return clearCacheCell;
//        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [PDChattingConfigMemberInfoCell cellHeight];
    }
    return 44;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1 && indexPath.row == 1) {
//        [[PDIMManager sharedManager] clearAllCache];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            [PDHUD showHUDSuccess:@"清除完成"];
//        });
//        [self.mainTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:1 inSection:1]] withRowAnimation:UITableViewRowAnimationAutomatic];
    } else if (indexPath.section == 0) {
        PDCenterPersonViewController *personViewController = [[PDCenterPersonViewController alloc] init];
        personViewController.transferMemberId = self.memberId;
        [self.navigationController pushViewController:personViewController animated:YES];
    }
}

#pragma mark - 控件方法

- (void)didAddToBlackList:(UISwitch *)blackSwitch {
    if (blackSwitch.on == YES) {
        [[PDEMManager sharedInstance] addOnePersonToBlackList:self.memberId complication:^(EMError *error) {
            [PDHUD showHUDSuccess:@"已成功将对方拉入黑名单"];
        }];
        
    } else {
        [[PDEMManager sharedInstance] deleteOnePersonFromBlackList:self.memberId complication:^(EMError *error) {
            [PDHUD showHUDSuccess:@"已成功将对方从黑名单中移除"];
        }];
    }
}

#pragma mark - 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:findChattingConfig requestParams:@{@"memberId":self.memberId} responseObjectClass:[PDChattingConfigModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            self.configModel = (PDChattingConfigModel *)responseObject;
            [weakSelf.mainTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
}

- (void)fetchAddFollow {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerAddFollow requestParams:@{@"followedMemberId":self.memberId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.configModel.isFollow = YES;
            [weakSelf.mainTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationAutomatic];
        }
    }];
}


@end
