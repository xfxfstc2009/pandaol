//
//  PDChattingConfigViewController.h
//  PandaKing
//
//  Created by Cranz on 16/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 聊天设置
@interface PDChattingConfigViewController : AbstractViewController
@property (nonatomic, copy) NSString *memberId;
@end
