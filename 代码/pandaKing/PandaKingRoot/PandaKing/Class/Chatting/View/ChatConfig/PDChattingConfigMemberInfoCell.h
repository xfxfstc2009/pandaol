//
//  PDChattingConfigMemberInfoCell.h
//  PandaKing
//
//  Created by Cranz on 16/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDChattingConfigModel.h"

@interface PDChattingConfigMemberInfoCell : UITableViewCell
@property (nonatomic, strong) PDChattingConfigModel *model;

+ (CGFloat)cellHeight;

- (void)chattingCellWithMore:(void(^)(UIButton *button))moreComplication;
@end
