//
//  PDChattingConfigMemberInfoCell.m
//  PandaKing
//
//  Created by Cranz on 16/12/8.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDChattingConfigMemberInfoCell.h"
#import "PDCenterLabel.h"
#import "PDButton.h"

typedef void(^PDMoreComplication)(UIButton *button);
@interface PDChattingConfigMemberInfoCell ()
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) PDCenterLabel *ageLabel;
@property (nonatomic, strong) PDCenterLabel *disLabel;
@property (nonatomic, strong) PDButton *moreButton;
@property (nonatomic, copy)   PDMoreComplication moreBlock;
@property (nonatomic, strong) UIImageView *bindImageView;
@property (nonatomic, strong) UIImageView *authImageView;
@end

@implementation PDChattingConfigMemberInfoCell

+ (CGFloat)cellHeight {
    return 90;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    
    [self cellSetting];
    return self;
}

- (void)cellSetting {
    _rowHeight = [PDChattingConfigMemberInfoCell cellHeight];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // 头像
    CGFloat y = 12; // 头像的上变局
    CGFloat headerWidth = _rowHeight - y * 2;
    self.headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, y, headerWidth, headerWidth)];
    self.headerImageView.layer.cornerRadius = headerWidth / 2;
    self.headerImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.headerImageView];
    
    // name
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:16];
    [self.contentView addSubview:self.nameLabel];
    
    // age
    self.ageLabel = [[PDCenterLabel alloc] init];
    self.ageLabel.textColor = c1;
    self.ageLabel.textFont = [UIFont systemFontOfCustomeSize:13];
    self.ageLabel.contentInsets = UIEdgeInsetsMake(2, 7, 2, 7);
    [self.contentView addSubview:self.ageLabel];
    
    // 绑定 icon
    self.bindImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.bindImageView];
    
    // 认证
    self.authImageView = [[UIImageView alloc] init];
    [self.contentView addSubview:self.authImageView];
    
    // 距离
    self.disLabel = [[PDCenterLabel alloc] init];
    self.disLabel.textColor = c14;
    self.disLabel.textFont = self.ageLabel.textFont;
    self.disLabel.image = [UIImage imageNamed:@"icon_nearby_location"];
    [self.contentView addSubview:self.disLabel];
    
    // button
    CGFloat buttonWidth = LCFloat(44);
    CGFloat buttonHeight = _rowHeight - LCFloat(32);
    self.moreButton = [PDButton buttonWithType:UIButtonTypeCustom];
    self.moreButton.titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.moreButton.frame = CGRectMake(kScreenBounds.size.width - buttonWidth - kTableViewSectionHeader_left, (_rowHeight - buttonHeight) / 2, buttonWidth, buttonHeight);
    self.moreButton.titleLabel.font = [UIFont systemFontOfCustomeSize:12];
    [self.moreButton setImageEdgeInsets:UIEdgeInsetsMake(15, 15, 15, 15)];
    [self.moreButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    [self.contentView addSubview:self.moreButton];
}

- (void)didClickButton:(UIButton *)button {
    self.moreBlock(button);
}

- (void)chattingCellWithMore:(void (^)(UIButton *))moreComplication {
    if (moreComplication) {
        self.moreBlock = moreComplication;
    }
}

#pragma mark -  set

- (void)setModel:(PDChattingConfigModel *)model {
    _model = model;
    
    [self.headerImageView uploadImageWithAvatarURL:model.member.avatar placeholder:nil callback:NULL];
    self.nameLabel.text = model.member.nickname;
    self.ageLabel.text = [NSString stringWithFormat:@"%ld",(long)model.member.age];
    
    if ([model.member.gender isEqualToString:@"FEMALE"]) {
        self.ageLabel.image = [UIImage imageNamed:@"icon_friend_female"];
        self.ageLabel.backgroundColor = RGB(243, 145, 178, 1);
    } else if ([model.member.gender isEqualToString:@"MALE"]) {
        self.ageLabel.image = [UIImage imageNamed:@"icon_friend_male"];
        self.ageLabel.backgroundColor = RGB(129, 212, 250, 1);
    } else {
        self.ageLabel.backgroundColor = RGB(69, 69, 69, 1);
    }
    self.ageLabel.layer.cornerRadius = self.ageLabel.size.height / 2;
    self.ageLabel.clipsToBounds = YES;
    
    if (model.member.bindGameUser) {
        self.bindImageView.image = [UIImage imageNamed:@"icon_find_bind_on"];
    } else {
        self.bindImageView.image = [UIImage imageNamed:@"icon_find_bind_off"];
    }
    
    if (model.member.activateGameUser) {
        self.authImageView.image = [UIImage imageNamed:@"icon_find_auth_on"];
    } else {
        self.authImageView.image = [UIImage imageNamed:@"icon_find_auth_off"];
    }
    
    self.disLabel.text = [NSString stringWithFormat:@"距离%ldm",(long)model.distance];
    
    if (model.isFollow) { // 已关注
        self.moreButton.imageView.image = [UIImage imageNamed:@"icon_friend_followed"];
        [self.moreButton setTitle:@"已关注" forState:UIControlStateNormal];
        [self.moreButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.moreButton setEnabled:NO];
    } else {
        self.moreButton.imageView.image = [UIImage imageNamed:@"icon_friend_addfollow"];
        [self.moreButton setTitle:@"加关注" forState:UIControlStateNormal];
        [self.moreButton setTitleColor:c14 forState:UIControlStateNormal];
        [self.moreButton setEnabled:YES];
    }

    CGFloat s = 8; // 名字到头像之间的距离
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(self.headerImageView.frame) - s - (kScreenBounds.size.width - CGRectGetMinX(self.moreButton.frame) - s), [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize ageSize = [self.ageLabel size];
    CGSize disSize = [self.disLabel size];
    CGFloat s1 = 5;
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + s, CGRectGetMinY(self.headerImageView.frame) + 3, nameSize.width, nameSize.height);
    self.ageLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + s1, ageSize.width, ageSize.height);
    self.bindImageView.frame = CGRectMake(CGRectGetMaxX(self.ageLabel.frame) + 2, CGRectGetMinY(self.ageLabel.frame), CGRectGetHeight(self.ageLabel.frame), CGRectGetHeight(self.ageLabel.frame));
    self.authImageView.frame = CGRectMake(CGRectGetMaxX(self.bindImageView.frame) + 2, CGRectGetMinY(self.ageLabel.frame), CGRectGetWidth(self.bindImageView.frame), CGRectGetWidth(self.bindImageView.frame));
    self.disLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.ageLabel.frame) + s1, disSize.width, disSize.height);
}

@end
