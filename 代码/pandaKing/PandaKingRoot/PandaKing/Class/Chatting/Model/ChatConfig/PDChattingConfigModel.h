//
//  PDChattingConfigModel.h
//  PandaKing
//
//  Created by Cranz on 16/12/11.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMemberInfoModel.h"

/*
 {
 "code": 200,
 "data": {
 "member": {
 "id": "ggggg",
 "nickname": "ghfghhf",
 "cellphone": "12222",
 "gender": "MALE",
 "age": 23,
 "qq": "11322323",
 "weChat": "2444444",
 "signature": "..",
 "avatar": "url",
 "bindGames": [
 "lol",
 "dota2"
 ],
 "goldBalance":0,//金币float
 "bambooBalance":0//竹子float
 "isFollow":true//是否关注
 },
 "isBindGameUser": true,//有无认证召唤师
 "isActivateGameUser": true,//有无认证召唤师
 "distance": 8788//double  距离
 }
 }
 */
@interface PDChattingConfigModel : FetchModel
@property (nonatomic, strong) PDMemberInfoModel *member;
@property (nonatomic, assign) BOOL isFollow; // 是否关注
@property (nonatomic, assign) CGFloat distance;
@end
