//
//  PDAmusementGameCell.m
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAmusementGameCell.h"
#import "PDAmusementGameHeaderView.h"

#define kMORE_HEIGHT 30

@interface PDAmusementGameCell ()
@property (nonatomic,strong) PDAmusementGameHeaderView *headerView;
@property (nonatomic, strong) UIImageView *iconImageView;
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, copy) void(^clickBlock)();
@end

@implementation PDAmusementGameCell
- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
- (void)createView{
    self.clipsToBounds = YES;
    
    UIImage *iconImage = [UIImage imageNamed:@"icon_lottery_sign"];
    self.iconImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, iconImage.size.width, iconImage.size.height)];
    self.iconImageView.image = iconImage;
    [self.contentView addSubview:self.iconImageView];
    
    self.moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.moreButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
    CGFloat buttonWidth = [@"更多" sizeWithCalcFont:self.moreButton.titleLabel.font].width + 20;
    [self.moreButton setFrame:CGRectMake(kScreenBounds.size.width - buttonWidth, 0, buttonWidth, kMORE_HEIGHT)];
    [self.moreButton setTitleColor:c26 forState:UIControlStateNormal];
    [self.moreButton setTitle:@"更多" forState:UIControlStateNormal];
    [self.contentView addSubview:self.moreButton];
    [self.moreButton addTarget:self action:@selector(didClickMoreButton) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setTransferGameRootSubSingleModel:(PDLoteryGameRootSubSingleModel *)transferGameRootSubSingleModel{
    _transferGameRootSubSingleModel = transferGameRootSubSingleModel;
    
    if (!self.headerView){
        self.headerView = [[PDAmusementGameHeaderView alloc]initWithFrame:CGRectMake(0, kMORE_HEIGHT, kScreenBounds.size.width, [PDAmusementGameHeaderView calculationCellHeightWithType:LotterGameHeaderTypeNormal] - LCFloat(8))];
    }
    self.headerView.transferType = LotterGameHeaderTypeNormal;
    self.headerView.transferLotteryCellType = LotteryCellTypeHome;
    self.headerView.userInteractionEnabled = NO;
    [self.contentView addSubview:self.headerView];
    
    self.headerView.transferGameRootSubSingleModel = self.transferGameRootSubSingleModel;
}

- (void)didClickMoreButton {
    if (self.clickBlock) {
        self.clickBlock();
    }
}

- (void)didClickMoreActions:(void (^)())actions {
    if  (actions) {
        self.clickBlock = actions;
    }
}

+ (CGFloat)cellHeight{
    return [PDAmusementGameHeaderView calculationCellHeightWithType:LotterGameHeaderTypeNormal] - LCFloat(8) + kMORE_HEIGHT;
}

-(void)cellFrameAutoSetting{
    [self.headerView frameSetting];
}

@end
