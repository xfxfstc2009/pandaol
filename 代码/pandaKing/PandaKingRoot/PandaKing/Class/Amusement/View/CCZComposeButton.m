//
//  CCZComposeButton.m
//  CCZComposeButton
//
//  Created by 金峰 on 2017/2/10.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import "CCZComposeButton.h"

static const CGFloat CCZComposeButtonSpace = 6;

@interface CCZComposeButton ()
@property (nonatomic, strong) UIImageView *tempBackgroundImageView;
@property (nonatomic, strong) UIImageView *leftImageView;
@property (nonatomic, strong) UIImageView *rightImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, assign, readwrite) CGFloat width;
@end

@implementation CCZComposeButton

- (UIImageView *)leftImageView {
    if (!_leftImageView) {
        _leftImageView = [[UIImageView alloc] init];
        [self addSubview:_leftImageView];
    }
    return _leftImageView;
}

- (UIImageView *)rightImageView {
    if (!_rightImageView) {
        _rightImageView = [[UIImageView alloc] init];
        [self addSubview:_rightImageView];
    }
    return _rightImageView;
}

- (instancetype)initWithFrame:(CGRect)frame leftImage:(UIImage *)leftImage rightImage:(UIImage *)rightImage {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    self.autoAdjustToWidth = YES;
    
    self.tempBackgroundImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.tempBackgroundImageView.clipsToBounds = YES;
    [self addSubview:self.tempBackgroundImageView];
    
    if (leftImage) {
        self.leftImage = leftImage;
        
        CGSize leftSize = leftImage.size;
        self.leftImageView.frame = CGRectMake(CCZComposeButtonSpace, (frame.size.height - leftSize.height) / 2, leftSize.width, leftSize.height);
        self.leftImageView.image = leftImage;
    }
    if (rightImage) {
        self.rightImage = rightImage;
        
        CGSize rightSize = rightImage.size;
        self.rightImageView.frame = CGRectMake(frame.size.width - CCZComposeButtonSpace - rightSize.width, (frame.size.height - rightSize.height) / 2, rightSize.width, rightSize.height);
        self.rightImageView.image = rightImage;
    }
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.font = [UIFont systemFontOfSize:12];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.titleLabel];
    
    return self;
}

- (void)setTitle:(NSString *)title {
    _title = title;
    [self updateTitleLabel];
}

- (void)updateTitleLabel {
    CGSize titleSize = [self.title sizeWithAttributes:@{NSFontAttributeName: self.titleLabel.font}];
    self.titleLabel.text = self.title;
    
    CGFloat wSpace = titleSize.width;
    
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.leftImageView.frame) + CCZComposeButtonSpace, (self.frame.size.height - titleSize.height) / 2, wSpace, titleSize.height);
    
    if (self.autoAdjustToWidth) {
        CGSize rightSize = self.rightImage.size;
        self.rightImageView.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + CCZComposeButtonSpace, (self.frame.size.height - rightSize.height) / 2, rightSize.width, rightSize.height);
        
        if (self.leftImage) {
            wSpace += CCZComposeButtonSpace * 2 + CGRectGetWidth(self.leftImageView.frame);
        } else {
            wSpace += CCZComposeButtonSpace;
        }
        
        if (self.rightImage) {
            wSpace += CCZComposeButtonSpace * 2 + CGRectGetWidth(self.rightImageView.frame);
        } else {
            wSpace += CCZComposeButtonSpace;
        }
    } else {        
        CGSize rightSize = self.rightImage.size;
        self.rightImageView.frame = CGRectMake(self.frame.size.width - CCZComposeButtonSpace - rightSize.width, (self.frame.size.height - rightSize.height) / 2, rightSize.width, rightSize.height);
        self.rightImageView.image = self.rightImage;
        
        wSpace = self.frame.size.width;
        
        // 留给title的宽度
        CGFloat titleWidth = wSpace - CCZComposeButtonSpace * 2;
        if (self.leftImage) {
            titleWidth -= CCZComposeButtonSpace + self.leftImage.size.width;
        }
        if (self.rightImage) {
            titleWidth -= CCZComposeButtonSpace + self.rightImage.size.width;
        }
        
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.leftImageView.frame) + CCZComposeButtonSpace, (self.frame.size.height - titleSize.height) / 2, titleWidth, titleSize.height);
    }
    
    self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y, wSpace, self.frame.size.height);
    self.tempBackgroundImageView.frame = self.bounds;
}

- (void)setLeftImage:(UIImage *)leftImage {
    _leftImage = leftImage;
    
    self.leftImageView.image = leftImage;
}

- (void)setRightImage:(UIImage *)rightImage {
    _rightImage = rightImage;
    
    self.rightImageView.image = rightImage;
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    _backgroundImage = backgroundImage;
    
    self.tempBackgroundImageView.image = backgroundImage;
}

- (void)setTitleColor:(UIColor *)titleColor {
    _titleColor = titleColor;
    
    self.titleLabel.textColor = titleColor;
}

- (void)setTitleFont:(UIFont *)titleFont {
    _titleFont = titleFont;
    
    self.titleLabel.font = titleFont;
    [self updateTitleLabel];
}

- (CGFloat)width {
    return self.frame.size.width;
}

@end
