//
//  PDAmusementPlayCell.h
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDAmusementPlayCell : UITableViewCell
@property (nonatomic, strong) UIImage *image;

+ (CGFloat)cellHeight;
@end
