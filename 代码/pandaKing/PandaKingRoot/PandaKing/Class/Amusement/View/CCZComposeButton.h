//
//  CCZComposeButton.h
//  CCZComposeButton
//
//  Created by 金峰 on 2017/2/10.
//  Copyright © 2017年 金峰. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

/// 图-字-图
@interface CCZComposeButton : UIControl

@property (nonatomic, strong, nullable) UIImage *backgroundImage;
@property (nonatomic, strong, nullable) UIImage *leftImage;
@property (nonatomic, strong, nullable) UIImage *rightImage;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, strong, nullable) UIColor *titleColor;
@property (nonatomic, strong, nullable) UIFont *titleFont;
@property (nonatomic) BOOL autoAdjustToWidth; /// 根据标题内容自动适应按钮的宽度，默认YES
@property (nonatomic, assign, readonly) CGFloat width;

- (instancetype)initWithFrame:(CGRect)frame
                    leftImage:(UIImage  * _Nullable)leftImage
                   rightImage:(UIImage * _Nullable)rightImage;

@end

NS_ASSUME_NONNULL_END
