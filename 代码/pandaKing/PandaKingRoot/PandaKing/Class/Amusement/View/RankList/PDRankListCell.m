//
//  PDRankListCell.m
//  PandaKing
//
//  Created by Cranz on 17/2/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRankListCell.h"

#define kRankWidth 38

@interface PDRankListCell ()
@property (nonatomic, strong) UILabel *rankLabel; // 名次
@property (nonatomic, strong) UIImageView *crownImageView; // 皇冠图标
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel; // 名字
@property (nonatomic, strong) UILabel *goldLabel; // 金币
@end

@implementation PDRankListCell

+ (CGFloat)cellHeight {
    return 50;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    [self setupCell];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

- (void)setupCell {
    // 排名
    self.rankLabel = [[UILabel alloc] init];
    self.rankLabel.textAlignment = NSTextAlignmentCenter;
    self.rankLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
    [self.contentView addSubview:self.rankLabel];
    
    // 头像
    self.headerImageView = [[PDImageView alloc] init];
    [self.contentView addSubview:self.headerImageView];
    
    // 皇冠
    self.crownImageView = [[UIImageView alloc] init];
    self.crownImageView.hidden = YES;
    [self.contentView addSubview:self.crownImageView];
    
    // 名字
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [[UIFont systemFontOfCustomeSize:14] boldFont];
    [self.contentView addSubview:self.nameLabel];
    
    // 金币
    self.goldLabel = [[UILabel alloc] init];
    self.goldLabel.font = [UIFont systemFontOfCustomeSize:12];
    [self.contentView addSubview:self.goldLabel];
}

- (void)setThModel:(PDRankThItem *)thModel {
    _thModel = thModel;
    
    self.rankLabel.text = [NSString stringWithFormat:@"%ld",thModel.index];
    // 第一名金色，第二名银色，第三名铜色，其他淡黑色
    if (thModel.index == 1) {
        self.rankLabel.textColor = self.nameLabel.textColor = self.goldLabel.textColor = c26;
        self.crownImageView.image = [UIImage imageNamed:@"icon_crown_gold"];
        self.crownImageView.hidden = NO;
    } else if (thModel.index == 2) {
        self.rankLabel.textColor = self.nameLabel.textColor = self.goldLabel.textColor = [UIColor lightGrayColor];
        self.crownImageView.image = [UIImage imageNamed:@"icon_crown_silver"];
        self.crownImageView.hidden = NO;
    } else if (thModel.index == 3) {
        self.rankLabel.textColor = self.nameLabel.textColor = self.goldLabel.textColor = c32;
        self.crownImageView.image = [UIImage imageNamed:@"icon_crown_copper"];
        self.crownImageView.hidden = NO;
    } else {
        self.rankLabel.textColor = self.nameLabel.textColor = self.goldLabel.textColor = c3;
        self.crownImageView.hidden = YES;
    }
    
    [self.headerImageView uploadImageWithAvatarURL:thModel.avatar placeholder:nil callback:nil];
    
    self.nameLabel.text = thModel.nickname;
    self.goldLabel.text = [NSString stringWithFormat:@"%@金币",[PDCenterTool numberStringChangeWithoutSpecialCharacter:thModel.gold]];
    
    CGSize rankSize = [self.rankLabel.text sizeWithCalcFont:self.rankLabel.font];
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    
    CGFloat imageWidth = 30; // 头像的宽度
    CGFloat imageHeight = 34; // 头像的高度，带帽子的
    
    self.rankLabel.frame = CGRectMake(0, ([PDRankListCell cellHeight] - rankSize.height) / 2, kRankWidth, rankSize.height);
    self.crownImageView.frame = CGRectMake(kRankWidth, ([PDRankListCell cellHeight] - imageWidth) / 2 - (imageHeight - imageWidth), imageWidth, imageHeight);
    self.headerImageView.frame = CGRectMake(kRankWidth, ([PDRankListCell cellHeight] - imageWidth) / 2, imageWidth, imageWidth);
    self.goldLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - goldSize.width, ([PDRankListCell cellHeight] - goldSize.height) / 2, goldSize.width, goldSize.height);
    
    CGFloat constaintWidth = kScreenBounds.size.width - CGRectGetMaxX(self.crownImageView.frame) - 10 * 2 - 60 - kTableViewSectionHeader_left;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(constaintWidth, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.crownImageView.frame) + 10, ([PDRankListCell cellHeight] - nameSize.height) / 2, nameSize.width, nameSize.height);
}

- (void)setCzModel:(PDRankCzItem *)czModel {
    _czModel = czModel;
    
    self.rankLabel.text = [NSString stringWithFormat:@"%ld",czModel.index];
    // 第一名金色，第二名银色，第三名铜色，其他淡黑色
    if (czModel.index == 1) {
        self.rankLabel.textColor = self.nameLabel.textColor = self.goldLabel.textColor = c26;
        self.crownImageView.image = [UIImage imageNamed:@"icon_crown_gold"];
        self.crownImageView.hidden = NO;
    } else if (czModel.index == 2) {
        self.rankLabel.textColor = self.nameLabel.textColor = self.goldLabel.textColor = [UIColor lightGrayColor];
        self.crownImageView.image = [UIImage imageNamed:@"icon_crown_silver"];
        self.crownImageView.hidden = NO;
    } else if (czModel.index == 3) {
        self.rankLabel.textColor = self.nameLabel.textColor = self.goldLabel.textColor = c32;
        self.crownImageView.image = [UIImage imageNamed:@"icon_crown_copper"];
        self.crownImageView.hidden = NO;
    } else {
        self.rankLabel.textColor = self.nameLabel.textColor = self.goldLabel.textColor = c3;
        self.crownImageView.hidden = YES;
    }
    
    [self.headerImageView uploadImageWithAvatarURL:czModel.avatar placeholder:nil callback:nil];
    
    self.nameLabel.text = czModel.nickname;
    
    if (czModel.goldAward != 0) { // 金币奖励
        self.goldLabel.text = [NSString stringWithFormat:@"奖励%@金币",[PDCenterTool numberStringChangeWithoutSpecialCharacter:czModel.goldAward]];
    } else { // 竹子奖励
        self.goldLabel.text = [NSString stringWithFormat:@"奖励%@竹子",[PDCenterTool numberStringChangeWithoutSpecialCharacter:czModel.bambooAward]];
    }
    
    CGSize rankSize = [self.rankLabel.text sizeWithCalcFont:self.rankLabel.font];
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    
    CGFloat imageWidth = 30; // 头像的宽度
    CGFloat imageHeight = 34; // 头像的高度，带帽子的
    
    self.rankLabel.frame = CGRectMake(0, ([PDRankListCell cellHeight] - rankSize.height) / 2, kRankWidth, rankSize.height);
    self.crownImageView.frame = CGRectMake(kRankWidth, ([PDRankListCell cellHeight] - imageWidth) / 2 - (imageHeight - imageWidth), imageWidth, imageHeight);
    self.headerImageView.frame = CGRectMake(kRankWidth, ([PDRankListCell cellHeight] - imageWidth) / 2, imageWidth, imageWidth);
    self.goldLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - goldSize.width, ([PDRankListCell cellHeight] - goldSize.height) / 2, goldSize.width, goldSize.height);
    
    CGFloat constaintWidth = kScreenBounds.size.width  - 60 - kTableViewSectionHeader_left - CGRectGetMaxX(self.crownImageView.frame) - 10 * 2;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(constaintWidth, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.crownImageView.frame) + 10, ([PDRankListCell cellHeight] - nameSize.height) / 2, nameSize.width, nameSize.height);
}

@end
