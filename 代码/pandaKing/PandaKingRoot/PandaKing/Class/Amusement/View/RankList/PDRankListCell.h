//
//  PDRankListCell.h
//  PandaKing
//
//  Created by Cranz on 17/2/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDRankThList.h"
#import "PDRankCzItem.h"

@interface PDRankListCell : UITableViewCell
@property (nonatomic, strong) PDRankThItem *thModel;
@property (nonatomic, strong) PDRankCzItem *czModel;

+ (CGFloat)cellHeight;
@end
