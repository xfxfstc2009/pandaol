//
//  PDRankListMoreCell.h
//  PandaKing
//
//  Created by Cranz on 17/2/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDRankYlItem.h"

/// 中间多一列的cell
@interface PDRankListMoreCell : UITableViewCell
@property (nonatomic, strong) PDRankYlItem *ylModel;

+ (CGFloat)cellHeight;
@end
