//
//  PDRankListMoreCell.m
//  PandaKing
//
//  Created by Cranz on 17/2/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRankListMoreCell.h"

#define kRankWidth 38

@interface PDRankListMoreCell ()
@property (nonatomic, strong) UILabel *rankLabel; // 名次
@property (nonatomic, strong) UIImageView *crownImageView; // 皇冠图标
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel; // 名字
@property (nonatomic, strong) UILabel *awardLabel; // 奖励
@property (nonatomic, strong) UILabel *moreLabel; // 更多 显示在中间
@end

@implementation PDRankListMoreCell

+ (CGFloat)cellHeight {
    return 50;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    [self setupCell];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    return self;
}

- (void)setupCell {
    // 排名
    self.rankLabel = [[UILabel alloc] init];
    self.rankLabel.textAlignment = NSTextAlignmentCenter;
    self.rankLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
    [self.contentView addSubview:self.rankLabel];
    
    // 头像
    self.headerImageView = [[PDImageView alloc] init];
    [self.contentView addSubview:self.headerImageView];
    
    // 皇冠
    self.crownImageView = [[UIImageView alloc] init];
    self.crownImageView.hidden = YES;
    [self.contentView addSubview:self.crownImageView];
    
    // 名字
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [[UIFont systemFontOfCustomeSize:14] boldFont];
    [self.contentView addSubview:self.nameLabel];
    
    // 奖励
    self.awardLabel = [[UILabel alloc] init];
    self.awardLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.awardLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.awardLabel];
    
    // 中间 更多
    self.moreLabel = [[UILabel alloc] init];
    self.moreLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.moreLabel.textAlignment = NSTextAlignmentRight;
    [self.contentView addSubview:self.moreLabel];
}

- (void)setYlModel:(PDRankYlItem *)ylModel {
    _ylModel = ylModel;
    
    self.rankLabel.text = [NSString stringWithFormat:@"%ld",ylModel.index];
//     第一名金色，第二名银色，第三名铜色，其他淡黑色
    if (ylModel.index == 1) {
        self.rankLabel.textColor = self.nameLabel.textColor = self.awardLabel.textColor = self.moreLabel.textColor = [UIColor hexChangeFloat:@"eac787"];
        self.crownImageView.image = [UIImage imageNamed:@"icon_crown_gold"];
        self.crownImageView.hidden = NO;
    } else if (ylModel.index == 2) {
        self.rankLabel.textColor = self.nameLabel.textColor = self.awardLabel.textColor = self.moreLabel.textColor = [UIColor hexChangeFloat:@"cccfd2"];
        self.crownImageView.image = [UIImage imageNamed:@"icon_crown_silver"];
        self.crownImageView.hidden = NO;
    } else if (ylModel.index == 3) {
        self.rankLabel.textColor = self.nameLabel.textColor = self.awardLabel.textColor = self.moreLabel.textColor = [UIColor hexChangeFloat:@"ba6e40"];
        self.crownImageView.image = [UIImage imageNamed:@"icon_crown_copper"];
        self.crownImageView.hidden = NO;
    } else {
        self.rankLabel.textColor = self.nameLabel.textColor = self.awardLabel.textColor = self.moreLabel.textColor = c3;
        self.crownImageView.hidden = YES;
    }
    
    [self.headerImageView uploadImageWithAvatarURL:ylModel.avatar placeholder:nil callback:nil];
    
    self.nameLabel.text = ylModel.nickname;
    
    if (ylModel.goldAward != 0) { // 金币奖励
        self.awardLabel.text = [NSString stringWithFormat:@"%@金币",[PDCenterTool numberStringChangeWithoutSpecialCharacter:ylModel.goldAward]];
    } else if (ylModel.bambooAward != 0) { // 竹子奖励
        self.awardLabel.text = [NSString stringWithFormat:@"%@竹子",[PDCenterTool numberStringChangeWithoutSpecialCharacter:ylModel.bambooAward]];
    } else { // 实物奖励
        self.awardLabel.text = ylModel.realItem;
    }
    
    self.moreLabel.text = [PDCenterTool numberStringChangeWithoutSpecialCharacter:ylModel.profit];
    
    CGSize rankSize = [self.rankLabel.text sizeWithCalcFont:self.rankLabel.font];
    CGSize awardSize = [self.awardLabel.text sizeWithCalcFont:self.awardLabel.font];
    CGSize moreSize = [self.moreLabel.text sizeWithCalcFont:self.moreLabel.font];
    
    CGFloat imageWidth = 30; // 头像的宽度
    CGFloat imageHeight = 34; // 头像的高度，带帽子的
    int awardWidth = 55; // 限制奖励的宽度是80
    int moreWidth = 55; // 限制中间更多的宽度是80
    int moreRightSpace = 25; // more和奖励之间的空隙
    
    self.rankLabel.frame = CGRectMake(0, ([PDRankListMoreCell cellHeight] - rankSize.height) / 2, kRankWidth, rankSize.height);
    self.crownImageView.frame = CGRectMake(kRankWidth, ([PDRankListMoreCell cellHeight] - imageWidth) / 2 - (imageHeight - imageWidth), imageWidth, imageHeight);
    self.headerImageView.frame = CGRectMake(kRankWidth, ([PDRankListMoreCell cellHeight] - imageWidth) / 2, imageWidth, imageWidth);
    self.awardLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - awardWidth, ([PDRankListMoreCell cellHeight] - awardSize.height) / 2, awardWidth, awardSize.height);
    self.moreLabel.frame = CGRectMake(CGRectGetMinX(self.awardLabel.frame) - moreRightSpace - moreWidth, ([PDRankListMoreCell cellHeight] - moreSize.height) / 2, moreWidth, moreSize.height);
    
    CGFloat constaintWidth = CGRectGetMinX(self.moreLabel.frame) - CGRectGetMaxX(self.crownImageView.frame) - 10;
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(constaintWidth, [NSString contentofHeightWithFont:self.nameLabel.font])];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.crownImageView.frame) + 10, ([PDRankListMoreCell cellHeight] - nameSize.height) / 2, nameSize.width, nameSize.height);
}

@end
