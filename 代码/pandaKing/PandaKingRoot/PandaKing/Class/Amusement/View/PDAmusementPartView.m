//
//  PDAmusementPartView.m
//  PandaKing
//
//  Created by Cranz on 17/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAmusementPartView.h"

/**
 * 一行最多的显示个数
 */
static int const maxRowItems = 3;
/**
 * item之间的空隙
 */
static CGFloat const itemSpace = 3;
static CGFloat const itemEdges = 2;
/**
 * 一行的高度
 */
#define itemRowHeight LCFloat(233)

/// 事先准备好6个容器，每次更新model数组的时候去显示这些容器视图
static int const itemsCapacity = 6;

@interface PDAmusementPartView ()
@property (nonatomic, strong) NSMutableArray *itemArr;
@property (nonatomic, strong) void(^clickBlock)(int, PDAmusementPartItem *);
@end

@implementation PDAmusementPartView

- (NSMutableArray *)itemArr {
    if (!_itemArr) {
        _itemArr = [NSMutableArray array];
    }
    return _itemArr;
}

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, itemRowHeight * 2)];
    if (self) {
        [self setupItemsView];
    }
    return self;
}

- (void)setupItemsView {
    /*
     0,1,2  0
     3,4,5  1
     */
    CGFloat itemWidth = (kScreenBounds.size.width - itemEdges * 2 - (maxRowItems - 1) * itemSpace) / maxRowItems;
    for (int i = 0; i < itemsCapacity; i++) {
        PDAmusementItemView *itemView = [[PDAmusementItemView alloc] initWithFrame:CGRectMake(itemEdges + (i % maxRowItems) * (itemWidth + itemSpace), itemEdges + (i / 3) * (itemRowHeight), itemWidth, itemRowHeight - itemEdges * 2)];
        itemView.tag = i + 999;
        [itemView addTarget:self action:@selector(didClickItem:) forControlEvents:UIControlEventTouchUpInside];
        [self.itemArr addObject:itemView];
        itemView.alpha = 0;
        [self addSubview:itemView];
    }
}

- (void)setItems:(NSArray *)items {
    _items = items;
    
    if (items.count > itemsCapacity) {
        DLog(@"娱乐版块多余额定数量");
        return;
    }
    
    if (items.count == 0) {
        [self showPrompt:@"该游戏下暂无娱乐项目" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:0 tapBlock:nil];
    } else {
        [self dismissPrompt];
    }
    
    for (int i = 0; i < items.count; i++) {
        PDAmusementPartItem *model = items[i];
        if (self.itemArr.count > i) {
            PDAmusementItemView *itemView = [self.itemArr objectAtIndex:i];
            itemView.model = model;
            [self showItemViewWithAnimation:itemView];
        }
    }
    
    // 需要去隐藏的个数
    NSUInteger hiddenCount = self.itemArr.count - items.count;
    for (NSUInteger i = 0; i < hiddenCount; i++) {
        PDAmusementItemView *itemView = self.itemArr[self.itemArr.count - 1 - i];
        [self hiddenItemViewWithAnimation:itemView];
    }
}

- (void)showItemViewWithAnimation:(PDAmusementItemView *)itemView {
    if (itemView.alpha == 0) {
        [UIView animateWithDuration:0.3 animations:^{
            itemView.alpha = 1;
        }];
    }
}

- (void)hiddenItemViewWithAnimation:(PDAmusementItemView *)itemView {
    if (itemView.alpha == 1) {
        [UIView animateWithDuration:0.3 animations:^{
            itemView.alpha = 0;
        }];
    }
}

#pragma mark - block

- (void)didClickItem:(PDAmusementItemView *)item {
    if (self.clickBlock) {
        self.clickBlock((int)(item.tag - 999),item.model);
    }
}

- (void)partViewClickActions:(void (^)(int, PDAmusementPartItem *))actions {
    if (actions) {
        self.clickBlock = actions;
    }
}

@end
