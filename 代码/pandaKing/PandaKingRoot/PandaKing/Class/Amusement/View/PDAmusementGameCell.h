//
//  PDAmusementGameCell.h
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLoteryGameRootSubSingleModel.h"

/// 娱乐中间赛事竞猜
@interface PDAmusementGameCell : UITableViewCell
@property (nonatomic,strong)PDLoteryGameRootSubSingleModel *transferGameRootSubSingleModel;

- (void)didClickMoreActions:(void(^)())actions;

+ (CGFloat)cellHeight;

-(void)cellFrameAutoSetting;
@end
