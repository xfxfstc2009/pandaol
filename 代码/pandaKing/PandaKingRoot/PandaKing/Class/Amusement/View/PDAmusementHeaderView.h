//
//  PDAmusementHeaderView.h
//  PandaKing
//
//  Created by Cranz on 17/2/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDAmusementHeaderView : UIView
@property (nonatomic, strong) UIButton *playButton;
- (void)headerViewDidClickButton:(void(^)())complication;
@end
