//
//  PDAmusementTrotingLeftView.h
//  PandaKing
//
//  Created by Cranz on 17/2/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

// 娱乐板块走马灯，左侧的公告文案会根据条目的type类型变化
// type: 公告(pub)；荣誉墙(honor)
@interface PDAmusementTrotingLeftView : UIView
- (void)updateNoticeWithType:(NSString *)type;
@end
