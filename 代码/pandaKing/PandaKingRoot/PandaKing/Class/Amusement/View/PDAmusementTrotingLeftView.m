//
//  PDAmusementTrotingLeftView.m
//  PandaKing
//
//  Created by Cranz on 17/2/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAmusementTrotingLeftView.h"

@interface PDAmusementTrotingLeftView ()
@property (nonatomic, copy) NSString *currentType; // 当前的显示类型
@property (nonatomic, strong) UILabel *currentLabel; // 正在显示的label

@property (nonatomic, strong) UILabel *pubLabel;
@property (nonatomic, strong) UILabel *honorLabel;
@property (nonatomic, assign) CGSize size;

@property (nonatomic, assign) CGFloat moveDis; // 每次要移动的距离
@property (nonatomic, assign) CGFloat labelHeight; // 显示的label高度
@end

@implementation PDAmusementTrotingLeftView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    _size = frame.size;
    [self pageSetting];
    return self;
}

- (void)pageSetting {
    self.backgroundColor = [UIColor whiteColor];
    self.clipsToBounds = YES;
    // 公告
    self.pubLabel = [self labelSetting];
    self.pubLabel.text = @"公告";
    self.pubLabel.backgroundColor = c26;
    CGSize pubSize = [self.pubLabel.text sizeWithCalcFont:self.pubLabel.font];
    CGSize changePubSize = CGSizeMake(pubSize.width + 2 * 3, pubSize.height + 2 * 1);
    self.pubLabel.frame = CGRectMake((_size.width - changePubSize.width) / 2, -changePubSize.height, changePubSize.width, changePubSize.height);
    
    // 荣誉墙
    self.honorLabel = [self labelSetting];
    self.honorLabel.text = @"荣誉墙";
    self.honorLabel.backgroundColor = [UIColor hexChangeFloat:@"00c5aa"];
    CGSize honorSize = [self.honorLabel.text sizeWithCalcFont:self.honorLabel.font];
    CGSize changeHonorSize = CGSizeMake(honorSize.width + 2 * 3, honorSize.height + 2 * 1);
    self.honorLabel.frame = CGRectMake((_size.width - changeHonorSize.width) / 2, -changeHonorSize.height, changeHonorSize.width, changeHonorSize.height);
    
    self.moveDis = changeHonorSize.height + (_size.height - changeHonorSize.height) / 2;
    self.labelHeight = changeHonorSize.height;
}

- (UILabel *)labelSetting {
    UILabel *textLabel = [[UILabel alloc] init];
    textLabel.font = [[UIFont systemFontOfCustomeSize:14] boldFont];
    textLabel.textColor = c1;
    textLabel.textAlignment = NSTextAlignmentCenter;
    textLabel.layer.cornerRadius = 2;
    textLabel.clipsToBounds = YES;
    [self addSubview:textLabel];
    return textLabel;
}

#pragma mark - 更新文案

- (void)updateNoticeWithType:(NSString *)type {
    if (![self.currentType isEqualToString:type]) {
        self.currentType = type;
        
        // 把目标往下滚，滚完后还原
        if ([type isEqualToString:@"pub"]) {
            [UIView animateWithDuration:0.8 animations:^{
                self.pubLabel.frame = CGRectOffset(self.pubLabel.frame, 0, self.moveDis);
                self.currentLabel.frame = CGRectOffset(self.currentLabel.frame, 0, self.moveDis);
            } completion:^(BOOL finished) {
                self.currentLabel.frame = CGRectOffset(self.currentLabel.frame, 0, -_size.height - self.labelHeight);
                self.currentLabel = self.pubLabel;
            }];
        } else {
            [UIView animateWithDuration:0.8 animations:^{
                self.honorLabel.frame = CGRectOffset(self.honorLabel.frame, 0, self.moveDis);
                self.currentLabel.frame = CGRectOffset(self.currentLabel.frame, 0, self.moveDis);
            } completion:^(BOOL finished) {
                self.currentLabel.frame = CGRectOffset(self.currentLabel.frame, 0, -_size.height - self.labelHeight);
                self.currentLabel = self.honorLabel;
            }];
        }
    }
}

@end
