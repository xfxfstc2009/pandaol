//
//  PDAmusementPartView.h
//  PandaKing
//
//  Created by Cranz on 17/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDAmusementItemView.h"

/// 娱乐板块的view
@interface PDAmusementPartView : UIView
@property (nonatomic, strong) NSArray *items;
- (void)partViewClickActions:(void(^)(int index, PDAmusementPartItem *item))actions;
@end
