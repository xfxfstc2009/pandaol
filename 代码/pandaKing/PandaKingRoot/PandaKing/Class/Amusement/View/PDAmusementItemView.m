//
//  PDAmusementItemView.m
//  PandaKing
//
//  Created by Cranz on 17/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAmusementItemView.h"

@interface PDAmusementItemView ()
@property (nonatomic, strong) PDImageView *imageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIView *signView;
@property (nonatomic, strong) UILabel *subTitleLabel;
/**
 * 图片的大小
 */
@property (nonatomic, assign) CGSize imageSize;
/**
 * 整个item 的大小
 */
@property (nonatomic, assign) CGSize itemSize;
@end

@implementation PDAmusementItemView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.itemSize = CGSizeMake(frame.size.width, frame.size.height);
        self.imageSize = CGSizeMake(self.itemSize.width, self.itemSize.height * 0.7);
        [self setupItemView];
    }
    return self;
}

- (void)setupItemView {
    self.imageView = [[PDImageView alloc] initWithFrame:CGRectMake(0, 0, self.imageSize.width, self.imageSize.height)];
    [self addSubview:self.imageView];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:15];
    [self addSubview:self.titleLabel];
    
    self.signView = [[UIView alloc] init];
    self.signView.layer.cornerRadius = 4/2;
    self.signView.layer.masksToBounds = YES;
    [self addSubview:self.signView];
    
    self.subTitleLabel = [[UILabel alloc] init];
    self.subTitleLabel.font = [UIFont systemFontOfCustomeSize:9];
    self.subTitleLabel.textColor = c3;
    [self addSubview:self.subTitleLabel];
}

- (void)setModel:(PDAmusementPartItem *)model {
    _model = model;
    if (model.cover.length){
        [self.imageView uploadAmusementPartWithImageCode:[NSString stringWithFormat:@"%@.%@",model.topic,model.cover] callback:nil];
    } else {
        [self.imageView uploadAmusementPartWithImageCode:[NSString stringWithFormat:@"%@.%@",model.topic,model.code] callback:nil];
    }
    
    self.titleLabel.text = model.title;
    self.signView.backgroundColor = [AccountModel sharedAccountModel].mainStyleColor;
    self.subTitleLabel.text = model.note;
    [self updateFrames];
}

- (void)updateFrames {
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font];
    CGSize subSize = [self.subTitleLabel.text sizeWithCalcFont:self.subTitleLabel.font];
    CGSize signSize = CGSizeMake(4, 4);
    CGFloat space = 6;
    // 标题的y坐标
    CGFloat y = (self.itemSize.height - self.imageSize.height - titleSize.height - subSize.height - space) / 2;
    
    self.titleLabel.frame = CGRectMake((self.itemSize.width - titleSize.width) / 2, y + CGRectGetMaxY(self.imageView.frame), titleSize.width, titleSize.height);
    self.subTitleLabel.frame = CGRectMake((self.itemSize.width - subSize.width) / 2 + signSize.width, CGRectGetMaxY(self.titleLabel.frame) + space, subSize.width, subSize.height);
    self.signView.frame = CGRectMake(CGRectGetMinX(self.subTitleLabel.frame) - signSize.width - 3, CGRectGetMinY(self.subTitleLabel.frame) + (CGRectGetHeight(self.subTitleLabel.frame) - signSize.height) / 2, signSize.width, signSize.height);
}

@end
