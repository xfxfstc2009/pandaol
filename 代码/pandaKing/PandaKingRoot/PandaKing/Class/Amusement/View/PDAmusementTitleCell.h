//
//  PDAmusementTitleCell.h
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDAmusementTitleCell : UITableViewCell
@property (nonatomic, strong) NSString *title;
/**
 * 左侧黑块适应字的高度，默认YES
 */
@property (nonatomic) BOOL blackViewAdjustToTitleHeight;

- (instancetype)initWithMoreButton:(NSString *)buttonTitle reuseIdentifier:(NSString *)reuseIdentifier;

- (void)titleCellDidClickMoreButton:(void(^)())complication;

+ (CGFloat)cellHeight;

@end
