//
//  PDAmusementPlayCell.m
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAmusementPlayCell.h"

@interface PDAmusementPlayCell ()
@property (nonatomic, strong) PDImageView *bigImageView;
@end

@implementation PDAmusementPlayCell

+ (CGFloat)cellHeight {
    return LCFloat(312/2+18);
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self
         setupCell];
    }
    
    return self;
}

- (void)setupCell {
    self.bigImageView = [[PDImageView alloc] initWithFrame:CGRectMake(9, 9, kScreenBounds.size.width - 18, [PDAmusementPlayCell cellHeight] - 18)];
    self.bigImageView.userInteractionEnabled = YES;
    [self.contentView addSubview:self.bigImageView];
}

- (void)setImage:(UIImage *)image {
    _image = image;
    self.bigImageView.image = image;
}

@end
