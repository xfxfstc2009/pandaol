//
//  PDAmusementFooterView.m
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAmusementFooterView.h"

@interface PDAmusementFooterView ()
@property (nonatomic, strong) NSArray *items;
@property (nonatomic, copy) PDAmusementFooterBlock block;
@end

@implementation PDAmusementFooterView

- (instancetype)initWithItems:(NSArray *)items {
    self = [super initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, ((items.count-1) / 2 + 1) * ([PDAmusementListCell cellSize].height + [PDAmusementListCell space]) + [PDAmusementListCell space])];
    if (self) {
        self.items = items;
        [self setupView];
    }
    return self;
}

- (void)setupView {
    for (int i = 0; i < self.items.count; i++) {
        PDAmusementListCell *listCell = [[PDAmusementListCell alloc] initWithFrame:CGRectMake(i % 2 == 0? [PDAmusementListCell space] : ([PDAmusementListCell cellSize].width + [PDAmusementListCell space] * 2), (i / 2) * ([PDAmusementListCell cellSize].height + [PDAmusementListCell space]) + [PDAmusementListCell space], [PDAmusementListCell cellSize].width, [PDAmusementListCell cellSize].height)];
        listCell.model = self.items[i];
        [self addSubview:listCell];
        listCell.tag = i + 999;
        
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClick:)];
        [listCell addGestureRecognizer:tap];
    }
}

- (void)didClick:(UITapGestureRecognizer *)tap {
    PDAmusementListCell *listcell = (PDAmusementListCell *)tap.view;
    NSInteger index = listcell.tag - 999;
    !self.block ?: self.block(index, self.items[index]);
}

- (void)footerViewDidClickComplication:(PDAmusementFooterBlock)complication
{
    if (complication) {
        self.block = complication;
    }
}

@end
