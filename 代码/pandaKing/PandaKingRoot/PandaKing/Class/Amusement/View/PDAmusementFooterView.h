//
//  PDAmusementFooterView.h
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDAmusementListCell.h"

typedef void(^PDAmusementFooterBlock)(NSInteger index, PDAmusementItem *item);
@interface PDAmusementFooterView : UIView
- (instancetype)initWithItems:(NSArray *)items;
- (void)footerViewDidClickComplication:(PDAmusementFooterBlock)complication;

@end
