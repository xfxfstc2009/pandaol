//
//  PDAmusementItemView.h
//  PandaKing
//
//  Created by Cranz on 17/5/18.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDAmusementPartItem.h"

/// 娱乐板块单个模块
@interface PDAmusementItemView : UIControl
@property (nonatomic, strong) PDAmusementPartItem *model;
@end
