//
//  PDAmusementTitleCell.m
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAmusementTitleCell.h"
#import "PDImageLabel.h"

typedef void(^PDAmusementTitleBlock)();
@interface PDAmusementTitleCell ()
@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UILabel *label;
@property (nonatomic, strong) PDImageLabel *moreButton;
@property (nonatomic, copy) PDAmusementTitleBlock block;
@end

@implementation PDAmusementTitleCell

+ (CGFloat)cellHeight {
    return LCFloat(40);
}

- (instancetype)initWithMoreButton:(NSString *)buttonTitle reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
    if (self) {
        if (buttonTitle) {
            _moreButton = [PDImageLabel imageLabelWithType:PDImageLabelTypeImageRight];
            _moreButton.text = buttonTitle;
            _moreButton.textColor = c26;
            _moreButton.image = [UIImage imageNamed:@"icon_home_arrow"];
            [self.contentView addSubview:_moreButton];
            [_moreButton updateFrames];
            
            _moreButton.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - _moreButton.tSize.width, ([PDAmusementTitleCell cellHeight] - _moreButton.tSize.height) / 2, _moreButton.tSize.width, _moreButton.tSize.height);
            
            [_moreButton addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
        }
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _blackViewAdjustToTitleHeight = YES;
        [self setupCell];
    }
    return self;
}

- (void)setupCell {
    self.leftView = [[UIView alloc] init];
    self.leftView.backgroundColor = c2;
    [self.contentView addSubview:self.leftView];
    
    self.label = [[UILabel alloc] init];
    self.label.textColor = c2;
    self.label.font = [UIFont systemFontOfCustomeSize:14];
    [self.contentView addSubview:self.label];
}

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.label.text = title;
    CGSize titleSize = [self.label.text sizeWithCalcFont:self.label.font];
    CGFloat blackHeight;
    if (_blackViewAdjustToTitleHeight == YES) {
        blackHeight = titleSize.height;
    } else {
        blackHeight = [PDAmusementTitleCell cellHeight] - 2 * LCFloat(6);
    }
    self.leftView.frame = CGRectMake(0, ([PDAmusementTitleCell cellHeight] - blackHeight) / 2, 5, blackHeight);
    self.label.frame = CGRectMake(CGRectGetMaxX(self.leftView.frame) + LCFloat(20), ([PDAmusementTitleCell cellHeight] - titleSize.height) / 2, titleSize.width, titleSize.height);
}

- (void)didClickButton {
    if (self.block) {
        self.block();
    }
}

- (void)titleCellDidClickMoreButton:(void (^)())complication {
    if (complication) {
        self.block = complication;
    }
}

@end
