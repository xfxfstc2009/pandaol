//
//  PDAmusementHeaderView.m
//  PandaKing
//
//  Created by Cranz on 17/2/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAmusementHeaderView.h"

typedef void(^PDAmusementHeaderBlock)();
@interface PDAmusementHeaderView ()
@property (nonatomic, copy) PDAmusementHeaderBlock block;
@end

@implementation PDAmusementHeaderView

+ (CGFloat)totalHeight {
    return LCFloat(200);
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [PDAmusementHeaderView totalHeight])];
    if (!self) {
        return nil;
    }
    [self setupHeaderView];
    return self;
}

- (void)setupHeaderView {
    self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.playButton setBackgroundImage:[UIImage imageNamed:@"img_panda_home_star"] forState:UIControlStateNormal];
    self.playButton.frame = CGRectMake(9, 9, kScreenBounds.size.width - 18, [PDAmusementHeaderView totalHeight] - 18);
    [self addSubview:self.playButton];
    
    [self.playButton addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didClickButton {
    if (self.block) {
        self.block();
    }
}

- (void)headerViewDidClickButton:(void (^)())complication {
    if (complication) {
        self.block = complication;
    }
}

@end
