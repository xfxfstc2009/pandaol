
//
//  PDAmusementListCell.m
//  PandaKing
//
//  Created by Cranz on 17/2/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAmusementListCell.h"

@interface PDAmusementListCell ()
@property (nonatomic, strong) PDImageView *bgImageView;
@property (nonatomic, strong) UILabel *mainLabel;
@property (nonatomic, strong) UILabel *subLabel;
@end

@implementation PDAmusementListCell

+ (CGSize)cellSize {
    CGFloat width = (kScreenBounds.size.width - kTableViewSectionHeader_left - [PDAmusementListCell space] * 2) / 2;
    return CGSizeMake(width, 173. / 345 * width);
}

+ (CGFloat)space {
    return 9;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    [self setupCell];
    return self;
}

- (void)setupCell {
    
    self.layer.cornerRadius = 2;
    self.layer.masksToBounds = YES;
    
    self.bgImageView = [[PDImageView alloc] initWithFrame:CGRectMake(0, 0, [PDAmusementListCell cellSize].width, [PDAmusementListCell cellSize].height)];
    [self.contentView addSubview:self.bgImageView];
    
    self.mainLabel = [[UILabel alloc] init];
    self.mainLabel.font = [[UIFont systemFontOfCustomeSize:17] boldFont];
    self.mainLabel.textColor = c1;
    [self.contentView addSubview:self.mainLabel];
    
    self.subLabel = [[UILabel alloc] init];
    self.subLabel.font = [[UIFont systemFontOfCustomeSize:11] boldFont];
    self.subLabel.textColor = self.mainLabel.textColor;
    self.subLabel.textAlignment = NSTextAlignmentCenter;
    self.subLabel.numberOfLines = 0;
    [self.contentView addSubview:self.subLabel];
}
 
- (void)setModel:(PDAmusementItem *)model {
    _model = model;
    
    self.mainLabel.text = model.mainTitle;
    self.bgImageView.image = model.image;
    
    NSMutableAttributedString *subAtt = [[NSMutableAttributedString alloc] initWithString:model.subTitle];
    [subAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(model.location, model.length)];
    self.subLabel.attributedText = [subAtt copy];
    
    [self updateLabelFrame];
}

- (void)updateLabelFrame {
    CGSize mainSize = [self.mainLabel.text sizeWithCalcFont:self.mainLabel.font];
    CGSize subSize = [self.subLabel.text sizeWithCalcFont:self.subLabel.font constrainedToSize:CGSizeMake(self.frame.size.width - 20 * 2, CGFLOAT_MAX)];
    
    CGFloat totalHeight = mainSize.height + subSize.height;
    CGFloat mainY = ([PDAmusementListCell cellSize].height - totalHeight) / 2;
    
    self.mainLabel.frame = CGRectMake(([PDAmusementListCell cellSize].width - mainSize.width) / 2, mainY, mainSize.width, mainSize.height);
    self.subLabel.frame = CGRectMake(([PDAmusementListCell cellSize].width - subSize.width) / 2, CGRectGetMaxY(self.mainLabel.frame), subSize.width, subSize.height);
}

@end
