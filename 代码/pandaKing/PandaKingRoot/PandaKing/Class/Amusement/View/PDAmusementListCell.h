//
//  PDAmusementListCell.h
//  PandaKing
//
//  Created by Cranz on 17/2/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDAmusementItem.h"

@interface PDAmusementListCell : UICollectionViewCell
@property (nonatomic, strong) PDAmusementItem *model;

+ (CGSize)cellSize;
+ (CGFloat)space;

@end
