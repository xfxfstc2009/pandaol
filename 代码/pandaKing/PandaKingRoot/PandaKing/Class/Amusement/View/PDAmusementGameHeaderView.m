//
//  PDAmusementGameHeaderView.m
//  PandaKing
//
//  Created by Cranz on 17/4/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDAmusementGameHeaderView.h"
#import "PDLotteryGameDetailHeaderStatusView.h"

@class PDLotteryGameDetailHeaderStatusView;

@interface PDAmusementGameHeaderView ()

@property (nonatomic,strong)UIView *timeBgView;
@property (nonatomic,strong)UILabel *headerTimeLabel;

@property (nonatomic,strong)UIView *lotteryView;                    /**< 竞猜view*/
@property (nonatomic,strong)PDImageView *leftIcon;                  /**< 左侧的icon*/
@property (nonatomic,strong)UILabel *leftLabel;                     /**< 左侧的名字*/

@property (nonatomic,strong)PDImageView *rightIcon;                 /**< 右侧的icon*/
@property (nonatomic,strong)UILabel *rightLabel;                    /**< 右侧的名字*/

@property (nonatomic,strong)UILabel *gameNameLabel;                 /**< 游戏名字*/
@property (nonatomic,strong)UILabel *gameTypeLabel;                 /**< 游戏类型*/

@property (nonatomic,strong)UILabel *leftRateLabel;                 /**< 左侧的倍率*/
@property (nonatomic,strong)UILabel *rightRateLabel;                /**< 右侧的倍率*/
@property (nonatomic,strong)UIView *gameStatusView;                 /**< 竞猜中背景*/
@property (nonatomic,strong)UILabel *gameStatusLabel;               /**< 游戏状态label */
@property (nonatomic,strong)UILabel *gameEndStatusLabel;               /**< 比赛label */

@property (nonatomic,strong)PDImageView *clockImgView;                     /**< 时间label*/
@property (nonatomic,strong)NSTimer *timer;

// 1.
@property (nonatomic,strong)UIView *lineView1;
@property (nonatomic,strong)UIView *lineView2;
@property (nonatomic,strong)UIView *lineView3;

@property (nonatomic,strong)UILabel *scoreLabel;
@end

@implementation PDAmusementGameHeaderView


-(instancetype)init{
    self = [super init];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.userInteractionEnabled = YES;
    
    // 3. 创建竞猜view
    self.lotteryView = [[UIView alloc]init];
    self.lotteryView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.lotteryView];
    
    
    // 1. 创建图片
    self.leftIcon = [[PDImageView alloc]init];
    self.leftIcon.backgroundColor = [UIColor clearColor];
    self.leftIcon.frame = CGRectMake(LCFloat(33), LCFloat(5), LCFloat(60), LCFloat(60));
    [self.lotteryView addSubview:self.leftIcon];
    
    // 1.1 创建leftLabel
    self.leftLabel = [[UILabel alloc]init];
    self.leftLabel.backgroundColor = [UIColor clearColor];
    self.leftLabel.textAlignment = NSTextAlignmentCenter;
    self.leftLabel.font = [[UIFont fontWithCustomerSizeName:@"小正文"]boldFont];
    self.leftLabel.frame = CGRectMake(self.leftIcon.orgin_x, CGRectGetMaxY(self.leftIcon.frame) + LCFloat(5), self.leftIcon.size_width , [NSString contentofHeightWithFont:self.leftLabel.font]);
    [self.lotteryView addSubview:self.leftLabel];
    
    // 2. 创建右侧的图片
    self.rightIcon = [[PDImageView alloc]init];
    self.rightIcon.backgroundColor = [UIColor clearColor];
    self.rightIcon.frame =CGRectMake(kScreenBounds.size.width - self.leftIcon.orgin_x - self.leftIcon.size_width, self.leftIcon.orgin_y, self.leftIcon.size_width, self.leftIcon.size_height);
    [self.lotteryView addSubview:self.rightIcon];
    
    // 2.1 创建rightLabel
    self.rightLabel = [[UILabel alloc]init];
    self.rightLabel.backgroundColor = [UIColor clearColor];
    self.rightLabel.textAlignment = NSTextAlignmentCenter;
    self.rightLabel.font = self.leftLabel.font;
    self.rightLabel.frame = CGRectMake(self.rightIcon.orgin_x, CGRectGetMaxY(self.rightIcon.frame) + LCFloat(5), self.rightIcon.size_width, [NSString contentofHeightWithFont:self.rightLabel.font]);
    [self.lotteryView addSubview:self.rightLabel];
    
    
    // 3. 创建名字
    self.gameNameLabel = [[UILabel alloc]init];
    self.gameNameLabel.backgroundColor = [UIColor clearColor];
    self.gameNameLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.gameNameLabel.textColor = c3;
    [self.lotteryView addSubview:self.gameNameLabel];
    
    // 4. 创建游戏赛制
    self.gameTypeLabel = [[UILabel alloc]init];
    self.gameTypeLabel.backgroundColor = [UIColor clearColor];
    self.gameTypeLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.gameTypeLabel.textColor = c3;
    [self.lotteryView addSubview:self.gameTypeLabel];
    
    // 6. 创建左侧的倍率
    self.leftRateLabel = [[UILabel alloc]init];
    self.leftRateLabel.backgroundColor = [UIColor clearColor];
    self.leftRateLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.leftRateLabel.textColor = c3;
    [self.lotteryView addSubview:self.leftRateLabel];
    
    // 7.创建右侧的倍率
    self.rightRateLabel = [[UILabel alloc]init];
    self.rightRateLabel.backgroundColor = [UIColor clearColor];
    self.rightRateLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.rightRateLabel.textColor = c3;
    [self.lotteryView addSubview:self.rightRateLabel];
    
    // 8.创建比赛开始状态
    self.gameStatusView = [[UIView alloc]init];
    self.gameStatusView.backgroundColor = [UIColor clearColor];
    [self.lotteryView addSubview:self.gameStatusView];
    
    self.gameStatusLabel = [[UILabel alloc]init];
    self.gameStatusLabel.backgroundColor = [UIColor clearColor];
    [self.gameStatusView addSubview:self.gameStatusLabel];
    
    
    // 10.创建比赛状态
    self.gameEndStatusLabel = [[UILabel alloc]init];
    self.gameEndStatusLabel.backgroundColor = [UIColor clearColor];
    self.gameEndStatusLabel.font = [UIFont systemFontOfCustomeSize:12.];
    self.gameEndStatusLabel.textColor = c3;
    self.gameEndStatusLabel.adjustsFontSizeToFitWidth = YES;
    [self.lotteryView addSubview:self.gameEndStatusLabel];
    
    self.lineView2 = [[UIView alloc]init];
    self.lineView2.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [self.lotteryView addSubview:self.lineView2];
    
    
    // 11. 比分label
    self.scoreLabel = [[UILabel alloc]init];
    self.scoreLabel.backgroundColor = [UIColor clearColor];
    self.scoreLabel.font = [[UIFont systemFontOfCustomeSize: 27.]boldFont];
    [self.lotteryView addSubview:self.scoreLabel];
    
    // 9. 创建tap
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self addGestureRecognizer:tapGesture];
    
    // 10 .创建时间
    self.clockImgView = [[PDImageView alloc]init];
    self.clockImgView.backgroundColor = [UIColor clearColor];
    self.clockImgView.image = [UIImage imageNamed:@"icon_lottery_list_clock"];
    self.clockImgView.frame = CGRectMake(0, 0, 9, 9);
    self.clockImgView.hidden = YES;
    [self.lotteryView addSubview:self.clockImgView];
}

-(void)setTransferIndex:(NSInteger)transferIndex{
    _transferIndex = transferIndex;
}


-(void)tapManager{
    if (self.delegate && [self.delegate respondsToSelector:@selector(foldTableViewHeaderDidSelectedWithIndex:)]){
        [self.delegate foldTableViewHeaderDidSelectedWithIndex:self.transferIndex];
    }
}

-(void)setTransferType:(LotterGameHeaderType)transferType{
    _transferType = transferType;
}

-(void)setTransferLotteryCellType:(LotteryCellType)transferLotteryCellType{
    _transferLotteryCellType = transferLotteryCellType;
}

-(void)setTransferGameRootSubSingleModel:(PDLoteryGameRootSubSingleModel *)transferGameRootSubSingleModel{
    _transferGameRootSubSingleModel = transferGameRootSubSingleModel;

    self.lotteryView.orgin_y = CGRectGetMaxY(self.timeBgView.frame);
    
    // 1. 左侧的头像
    [self.leftIcon uploadImageWithURL:transferGameRootSubSingleModel.teamLeft.icon placeholder:nil callback:NULL];
    
    // 1.1 左侧的名字
    self.leftLabel.text = transferGameRootSubSingleModel.teamLeft.name;
    CGSize leftLabelSize = [self.leftLabel.text sizeWithCalcFont:self.leftLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leftLabel.font])];
    self.leftLabel.frame = CGRectMake(0, CGRectGetMaxY(self.leftIcon.frame) + LCFloat(5), leftLabelSize.width, [NSString contentofHeightWithFont:self.leftLabel.font]);
    self.leftLabel.center_x = self.leftIcon.center_x;
    
    // 2. 右侧的头像
    [self.rightIcon uploadImageWithURL:transferGameRootSubSingleModel.teamRight.icon placeholder:nil callback:NULL];
    
    // 2.1 左侧的名字
    self.rightLabel.text = transferGameRootSubSingleModel.teamRight.name;
    CGSize rightLabelSize = [self.rightLabel.text sizeWithCalcFont:self.rightLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.rightLabel.font])];
    self.rightLabel.frame = CGRectMake(0, self.leftLabel.orgin_y, rightLabelSize.width, [NSString contentofHeightWithFont:self.rightLabel.font]);
    self.rightLabel.center_x = self.rightIcon.center_x;
    
    // 3. 线条
    self.lineView2.frame = CGRectMake(0, CGRectGetMaxY(self.leftLabel.frame) + LCFloat(15) / 2., kScreenBounds.size.width, .5f);
    
    // 4. 创建左侧倍率
    PDLotteryGameSubListSingleModel *topSingleModel = [transferGameRootSubSingleModel.lotteryDetailList firstObject];
    self.leftRateLabel.text = [NSString stringWithFormat:@"胜%.2f倍",topSingleModel.lotteryLeft.rate];
    CGSize leftRateSize = [self.leftRateLabel.text sizeWithCalcFont:self.leftRateLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leftRateLabel.font])];
    self.leftRateLabel.frame = CGRectMake(0, CGRectGetMaxY(self.lineView2.frame) + LCFloat(5), leftRateSize.width, [NSString contentofHeightWithFont:self.leftRateLabel.font]);
    self.leftRateLabel.center_x = self.leftIcon.center_x;
    
    // 5, 创建右侧的倍率
    self.rightRateLabel.text = [NSString stringWithFormat:@"胜%.2f倍",topSingleModel.lotteryRight.rate];
    CGSize rightRateSize = [self.rightRateLabel.text sizeWithCalcFont:self.rightRateLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.rightRateLabel.font])];
    self.rightRateLabel.frame = CGRectMake(0, CGRectGetMaxY(self.lineView2.frame) + LCFloat(5), rightRateSize.width, [NSString contentofHeightWithFont:self.rightRateLabel.font]);
    self.rightRateLabel.center_x = self.rightIcon.center_x;
    
    // 6. 赛事名字
    self.gameNameLabel.text = [NSString stringWithFormat:@"%@",transferGameRootSubSingleModel.matchName];
    CGSize gameNameSize = [self.gameNameLabel.text sizeWithCalcFont:self.gameNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gameNameLabel.font])];
    
    CGFloat gameNameWidth = gameNameSize.width > self.rightIcon.orgin_x - LCFloat(11) - CGRectGetMaxX(self.leftIcon.frame) - LCFloat(11) ? self.rightIcon.orgin_x - LCFloat(11) - CGRectGetMaxX(self.leftIcon.frame) - LCFloat(11):gameNameSize.width;
    
    self.gameNameLabel.frame = CGRectMake(CGRectGetMaxX(self.leftIcon.frame) + LCFloat(11), LCFloat(13),gameNameWidth , [NSString contentofHeightWithFont:self.gameNameLabel.font]);
    self.gameNameLabel.center_x = kScreenBounds.size.width / 2.;
    
    // 7.赛制
    self.gameTypeLabel.text = transferGameRootSubSingleModel.matchFormat;
    CGSize gameSize = [self.gameTypeLabel.text sizeWithCalcFont:self.gameTypeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gameTypeLabel.font])];
    self.gameTypeLabel.frame = CGRectMake(0, CGRectGetMaxY(self.gameNameLabel.frame) + LCFloat(7.5), gameSize.width, [NSString contentofHeightWithFont:self.gameTypeLabel.font]);
    self.gameTypeLabel.center_x = kScreenBounds.size.width / 2.;
    
    // 8. 创建竞猜中按钮
    self.gameStatusView.frame = CGRectMake(0, CGRectGetMaxY(self.gameTypeLabel.frame) + LCFloat(10), LCFloat(59), LCFloat(21));
    self.gameStatusView.backgroundColor = c26;
    self.gameStatusView.clipsToBounds = YES;
    self.gameStatusView.layer.cornerRadius = MIN(self.gameStatusView.size_height, self.gameStatusView.size_width) / 2.;
    self.gameStatusView.center_x = kScreenBounds.size.width / 2.;
    
    // 9. title
    self.gameStatusLabel.text = @"竞猜中";
    self.gameStatusLabel.textAlignment = NSTextAlignmentCenter;
    self.gameStatusLabel.frame = self.gameStatusView.bounds;
    self.gameStatusLabel.textColor = [UIColor whiteColor];
    self.gameStatusLabel.font = [UIFont systemFontOfCustomeSize:11.];
    
    // 10 . 比赛中
    if ([self.transferGameRootSubSingleModel.matchStatus isEqualToString:@"over"]){                  //比赛结束
        [self timeStatusWithType:LotteryGameDetailStatusEnd];
    } else if ([self.transferGameRootSubSingleModel.matchStatus isEqualToString:@"inPlay"]){
        [self timeStatusWithType:LotteryGameDetailStatusGaming];
    } else if ([self.transferGameRootSubSingleModel.matchStatus isEqualToString:@"unbegun"]){
        [self timeStatusWithType:LotteryGameDetailStatusWillBegin];
    } else {
        [self timeStatusWithType:LotteryGameDetailStatusNormal];
    }

}


+(CGFloat)calculationCellHeightWithType:(LotterGameHeaderType)type {         // 传递类型
    CGFloat cellHeight = 0 ;
    cellHeight += LCFloat(23);
    cellHeight += LCFloat(5);
    cellHeight += LCFloat(60);
    cellHeight += LCFloat(5);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]];
    cellHeight += LCFloat(7.5);
    cellHeight += LCFloat(49) / 2.;
    cellHeight -= LCFloat(8);
    return cellHeight;
}

-(void)timeStatusWithType:(LotteryGameDetailStatus)type{
    if (type == LotteryGameDetailStatusWillBegin){              // 比赛未开始
        
        NSString *statusStr = [NSDate getLotteryTimeDistance:self.transferGameRootSubSingleModel.gameStartTempTime / 1000.];
        self.gameEndStatusLabel.text = statusStr;
        
        [self startTimer];
        self.clockImgView.hidden = NO;
        
        self.scoreLabel.hidden = YES;
        self.gameTypeLabel.hidden = NO;
        self.gameStatusView.hidden = NO;
        self.gameStatusLabel.hidden = NO;
        self.gameStatusLabel.text = @"竞猜中";
        
    } else if (type == LotteryGameDetailStatusEnd){             // 比赛结束
        [self stopTimer];
        self.clockImgView.hidden = YES;
        self.gameEndStatusLabel.text = @"已结束";
        
        // 比分
        self.scoreLabel.text = [NSString stringWithFormat:@"%@:%@",self.transferGameRootSubSingleModel.teamLeft.gameEnd,self.transferGameRootSubSingleModel.teamRight.gameEnd];
        CGSize contentOfScoreSize = [@"880:100" sizeWithCalcFont:self.scoreLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.scoreLabel.font])];
        self.scoreLabel.backgroundColor = [UIColor clearColor];
        self.scoreLabel.textAlignment = NSTextAlignmentCenter;
        self.scoreLabel.frame = CGRectMake(0, 0, contentOfScoreSize.width + 2 * LCFloat(5), [NSString contentofHeightWithFont:self.scoreLabel.font]);
        self.scoreLabel.adjustsFontSizeToFitWidth = YES;
        self.scoreLabel.center_x = kScreenBounds.size.width / 2.;
        
        CGFloat marginHeight = self.lineView2.orgin_y - CGRectGetMaxY(self.gameNameLabel.frame);
        CGFloat center_y = marginHeight / 2. + CGRectGetMaxY(self.gameNameLabel.frame);
        self.gameNameLabel.backgroundColor = [UIColor clearColor];
        self.scoreLabel.backgroundColor = [UIColor clearColor];
        self.scoreLabel.center_y = center_y;
        
        self.scoreLabel.textColor = c26;
        
        self.scoreLabel.hidden = NO;
        self.gameTypeLabel.hidden = YES;
        self.gameStatusView.hidden = YES;
        self.gameStatusLabel.hidden = YES;
    } else if (type == LotteryGameDetailStatusGaming){          // 比赛中
        [self stopTimer];
        self.clockImgView.hidden = YES;
        self.gameEndStatusLabel.text = @"已开始";
        
        self.scoreLabel.hidden = YES;
        self.gameTypeLabel.hidden = NO;
        self.gameStatusView.hidden = NO;
        self.gameStatusLabel.hidden = NO;
        self.gameStatusLabel.text = @"比赛中";
    }
    
    CGSize contentOfSize = [self.gameEndStatusLabel.text sizeWithCalcFont:self.gameEndStatusLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gameEndStatusLabel.font])];
    self.gameEndStatusLabel.frame = CGRectMake(0, 0, contentOfSize.width, [NSString contentofHeightWithFont:self.gameEndStatusLabel.font]);
    self.gameEndStatusLabel.center_x = kScreenBounds.size.width / 2.;
    self.gameEndStatusLabel.center_y = self.leftRateLabel.center_y;
    
    self.clockImgView.orgin_x = self.gameEndStatusLabel.orgin_x - LCFloat(3) - LCFloat(9);
    self.clockImgView.center_y = self.gameEndStatusLabel.center_y;
}

-(void)startTimer{
    if (!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
    }
}


-(void)daojishiManager{
    //    PDLotteryGameSubListSingleModel *singleModel = [self.transferGameRootSubSingleModel.gameStartTempTime  firstObject];
    NSString *statusStr = [NSDate getLotteryTimeDistance:(self.transferGameRootSubSingleModel.gameStartTempTime / 1000.)];
    if ([statusStr isEqualToString:@"比赛中"]){
        self.gameEndStatusLabel.text = @"已开始";
        [self stopTimer];
    } else {
        self.gameEndStatusLabel.text = statusStr;
    }
}

-(void)stopTimer{
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}

-(void)frameSetting{
    // 1.
    self.leftIcon.frame = CGRectMake(LCFloat(33), LCFloat(5), LCFloat(60), LCFloat(60));
    // 2.
    CGSize leftLabelSize = [self.leftLabel.text sizeWithCalcFont:self.leftLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leftLabel.font])];
    self.leftLabel.frame = CGRectMake(0, CGRectGetMaxY(self.leftIcon.frame) + LCFloat(5), leftLabelSize.width, [NSString contentofHeightWithFont:self.leftLabel.font]);
    self.leftLabel.center_x = self.leftIcon.center_x;
    // 3.
    self.rightIcon.frame =CGRectMake(kScreenBounds.size.width - self.leftIcon.orgin_x - self.leftIcon.size_width, self.leftIcon.orgin_y, self.leftIcon.size_width, self.leftIcon.size_height);
    // 4.
    CGSize rightLabelSize = [self.rightLabel.text sizeWithCalcFont:self.rightLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.rightLabel.font])];
    self.rightLabel.frame = CGRectMake(0, self.leftLabel.orgin_y, rightLabelSize.width, [NSString contentofHeightWithFont:self.rightLabel.font]);
    self.rightLabel.center_x = self.rightIcon.center_x;
    // 3. 线条
    self.lineView2.frame = CGRectMake(0, CGRectGetMaxY(self.leftLabel.frame) + LCFloat(15) / 2., kScreenBounds.size.width, .5f);
    
    // 5.
    CGSize gameNameSize = [self.gameNameLabel.text sizeWithCalcFont:self.gameNameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gameNameLabel.font])];
    
    CGFloat gameNameWidth = gameNameSize.width > self.rightIcon.orgin_x - LCFloat(11) - CGRectGetMaxX(self.leftIcon.frame) - LCFloat(11) ? self.rightIcon.orgin_x - LCFloat(11) - CGRectGetMaxX(self.leftIcon.frame) - LCFloat(11):gameNameSize.width;
    
    self.gameNameLabel.frame = CGRectMake(CGRectGetMaxX(self.leftIcon.frame) + LCFloat(11), LCFloat(13),gameNameWidth , [NSString contentofHeightWithFont:self.gameNameLabel.font]);
    self.gameNameLabel.center_x = kScreenBounds.size.width / 2.;

    // 7.
    CGSize gameSize = [self.gameTypeLabel.text sizeWithCalcFont:self.gameTypeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gameTypeLabel.font])];
    self.gameTypeLabel.frame = CGRectMake(0, CGRectGetMaxY(self.gameNameLabel.frame) + LCFloat(7.5), gameSize.width, [NSString contentofHeightWithFont:self.gameTypeLabel.font]);
    self.gameTypeLabel.center_x = kScreenBounds.size.width / 2.;

    // 8.
    self.gameStatusView.frame = CGRectMake(0, CGRectGetMaxY(self.gameTypeLabel.frame) + LCFloat(10), LCFloat(59), LCFloat(21));
    self.gameStatusView.center_x = kScreenBounds.size.width / 2.;
    
    // 9.
}
@end
