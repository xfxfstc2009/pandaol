//
//  PDAmusementNoticeModel.h
//  PandaKing
//
//  Created by Cranz on 17/2/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDAmusementNoticeModel <NSObject>

@end

@interface PDAmusementNoticeModel : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, assign) NSTimeInterval createTime;
@property (nonatomic, assign) NSTimeInterval notifyTime;
@property (nonatomic, copy) NSString *type;
@end

/**
 type:
 pub,  公告
 honor 荣誉墙
 */