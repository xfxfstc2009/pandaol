//
//  PDAmusementPartItem.h
//  PandaKing
//
//  Created by Cranz on 17/5/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDAmusementPartItem <NSObject>

@end

@interface PDAmusementPartItem : FetchModel
/**
 * 标题
 */
@property (nonatomic, copy) NSString *title;
/**
 * 副标题
 */
@property (nonatomic, copy) NSString *note;
/**
 * lol，dota2，king
 */
@property (nonatomic, copy) NSString *code;
@property (nonatomic, copy) NSString *topic;
/**
 * 是否开放 NO：弹出hud
 */
@property (nonatomic, assign) BOOL enable;

@property (nonatomic,copy)NSString *cover;
@end
