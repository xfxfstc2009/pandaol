//
//  PDAmusementPartList.h
//  PandaKing
//
//  Created by Cranz on 17/5/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDAmusementPartItem.h"

@interface PDAmusementPartList : FetchModel
@property (nonatomic, strong) NSArray <PDAmusementPartItem> *posterlist;
@end
