//
//  PDRankThItem.h
//  PandaKing
//
//  Created by Cranz on 17/2/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRankListItem.h"

@protocol PDRankThItem <NSObject>

@end

/// 土豪榜item
@interface PDRankThItem : PDRankListItem
@property (nonatomic, assign) NSUInteger gold;
@end
