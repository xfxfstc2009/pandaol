//
//  PDRankYlItem.h
//  PandaKing
//
//  Created by Cranz on 17/2/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRankListItem.h"

@protocol PDRankYlItem <NSObject>

@end

/// 盈利Item
@interface PDRankYlItem : PDRankListItem
@property (nonatomic, assign) NSUInteger goldAward;
@property (nonatomic, assign) NSUInteger bambooAward;
@property (nonatomic, copy) NSString *realItem; //实物奖励
@property (nonatomic, assign) NSUInteger profit; // 盈利
@end
