//
//  PDRankListItem.h
//  PandaKing
//
//  Created by Cranz on 17/2/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 土豪榜单人
@interface PDRankListItem : FetchModel
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, assign) NSUInteger index;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *memberId;
@end
