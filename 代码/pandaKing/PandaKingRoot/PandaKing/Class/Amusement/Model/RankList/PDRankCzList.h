//
//  PDRankCzList.h
//  PandaKing
//
//  Created by Cranz on 17/2/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRankListInfo.h"
#import "PDRankCzItem.h"

@interface PDRankCzList : PDRankListInfo
@property (nonatomic, assign) NSUInteger recharge; // 充值
@property (nonatomic, strong) NSArray <PDRankCzItem> *items;
@end
