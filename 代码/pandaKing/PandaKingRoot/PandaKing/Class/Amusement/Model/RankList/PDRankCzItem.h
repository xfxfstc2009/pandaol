//
//  PDRankCzItem.h
//  PandaKing
//
//  Created by Cranz on 17/2/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRankListItem.h"

@protocol PDRankCzItem <NSObject>

@end

/// 充值榜item
@interface PDRankCzItem : PDRankListItem
@property (nonatomic, assign) NSUInteger goldAward; // 金币奖励
@property (nonatomic, assign) NSUInteger bambooAward;// 竹子奖励
@end
