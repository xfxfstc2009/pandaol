//
//  PDRankYlList.h
//  PandaKing
//
//  Created by Cranz on 17/2/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRankListInfo.h"
#import "PDRankYlItem.h"

@interface PDRankYlList : PDRankListInfo
@property (nonatomic, assign) NSUInteger profit; // 我的盈利
@property (nonatomic, strong) NSArray <PDRankYlItem> *items;
@end
