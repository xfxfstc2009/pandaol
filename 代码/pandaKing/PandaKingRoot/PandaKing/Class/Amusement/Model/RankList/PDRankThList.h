//
//  PDRankThList.h
//  PandaKing
//
//  Created by Cranz on 17/2/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDRankListInfo.h"
#import "PDRankThItem.h"

/// 土豪榜
@interface PDRankThList : PDRankListInfo
@property (nonatomic, assign) NSUInteger gold;
@property (nonatomic, strong) NSArray<PDRankThItem> *items;
@end
