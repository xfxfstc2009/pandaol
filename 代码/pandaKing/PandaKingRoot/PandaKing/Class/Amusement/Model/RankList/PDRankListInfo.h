//
//  PDRankListInfo.h
//  PandaKing
//
//  Created by Cranz on 17/2/21.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 排行榜基类
@interface PDRankListInfo : FetchModel
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, assign) NSUInteger ranking;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *avatar;
@end
