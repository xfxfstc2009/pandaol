//
//  PDAmusementNoticeList.h
//  PandaKing
//
//  Created by Cranz on 17/2/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDAmusementNoticeModel.h"

@interface PDAmusementNoticeList : FetchModel
@property (nonatomic, strong) NSArray <PDAmusementNoticeModel> *items;
@end
