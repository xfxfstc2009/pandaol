//
//  PDAmusementItem.h
//  PandaKing
//
//  Created by Cranz on 17/2/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDAmusementItem : NSObject
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, copy)   NSString *mainTitle;
@property (nonatomic, copy)   NSString *subTitle;
@property (nonatomic, assign) int location; // 起始位置
@property (nonatomic, assign) int length; // 长度
@property (nonatomic, strong) NSArray *rangeArr;
@property (nonatomic, strong) UIColor *renderColor;
@end
