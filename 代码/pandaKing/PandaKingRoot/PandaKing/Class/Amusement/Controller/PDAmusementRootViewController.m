//
//  PDAmusementRootViewController.m
//  PandaKing
//
//  Created by Cranz on 17/2/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "CCZComposeButton.h"
#import "PDTopUpViewController.h"
#import "PDAmusementTitleCell.h"
#import "PDAmusementPlayCell.h"
#import "PDAmusementFooterView.h"
#import "PDRankRootViewController.h"
#import "PDLotteryGameMainViewController.h"
#import "PDLotteryGameDetailMainViewController.h"
#import "PDLotteryHeroViewController.h"
#import "PDChaosFightRootNavController.h"
#import "PDChaosFightRootViewController.h"

#import "CCZTrotingLabel.h"
#import "PDAmusementTrotingLeftView.h"

// 公告
#import "PDAmusementNoticeList.h"

// 赛事精彩cell
#import "PDAmusementGameCell.h"
#import "PDLoteryGameRootListModel.h"

// LOL英雄猜
#import "PDScuffleViewController.h" // 大乱斗
#import "PDScuffleEnterViewController.h" // 大乱斗入口

// 邀请有奖
#import "PDInviteViewController.h"
// 娱乐底部模块分类
#import "PDAmusementPartView.h"
#import "PDAmusementPartList.h"

#import "PDFirstChooseGameTypeViewController.h"             // 第一次进行选择游戏
#import "PDGradientNavBar.h"
#import "PDAppleShenheModel.h"
#import "PDInformationRootViewController.h"

#import "PDYuezhanRootViewController.h"                     // 约战
#import "PDJuediYuezhanRootViewController.h"
#import "PDJuediYuezhanSearchViewController.h"
#import "PDRegisterIsEighteenViewController.h"

@interface PDAmusementRootViewController ()<UITableViewDataSource, UITableViewDelegate>{
    BOOL hasGoinChaosFight;
}
@property (nonatomic, strong) UIView *topBar;
@property (nonatomic, strong) CCZComposeButton *goldComposeButton;
@property (nonatomic, strong) CCZComposeButton *rankComposeButton;
@property (nonatomic, strong) UITableView *mainTableView;

// 赛事数组
@property (nonatomic, strong) NSMutableArray *lotteryMutableArr;

@property (nonatomic, strong) CCZTrotingLabel *trotingLabel;
/**
 * 娱乐底部
 */
@property (nonatomic, strong) PDAmusementPartView *partView;
@end

@implementation PDAmusementRootViewController

#pragma mark - shared
+(instancetype)sharedController{
    static PDAmusementRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[PDAmusementRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

-(void)directToChongzhiManager{
    PDTopUpViewController *chongzhiViewController = [[PDTopUpViewController alloc]init];
    [chongzhiViewController hidesTabBarWhenPushed];
    [self.navigationController pushViewController:chongzhiViewController animated:YES];
}

- (NSMutableArray *)lotteryMutableArr {
    if (!_lotteryMutableArr) {
        _lotteryMutableArr = [NSMutableArray array];
    }
    return _lotteryMutableArr;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [[PDMainTabbarViewController sharedController] setBarHidden:NO animated:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        [self fetchMemberTotalGold];                                                // 获取当前的金币
    }
    
    [self sendRequestToGetGuessList];                                               // 获取竞猜信息
    [self.trotingLabel walk];                                                       // 荣誉墙
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupBar];
    [self setupTrotingLabel];
    [self setupTabelView];
    [self listeningRotating];                                                       // 监听设备旋转
    
    // 1. 判断是否审核
    __weak typeof(self)weakSelf = self;
    [self sendRequestToGetShenheWithBlock:^{
        if (!weakSelf){
            return ;
        }
        if ([AccountModel sharedAccountModel].isShenhe){
            
            PDPandaRootViewController *pandaOLRootViewController = [[PDPandaRootViewController alloc]init];
            PDNavigationController *amusementRootNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
            [amusementRootNav setViewControllers:@[pandaOLRootViewController]];
            [[PDMainTabbarViewController sharedController] setViewController:amusementRootNav atIndex:0];
            
            PDInformationRootViewController *intormationViewController = [[PDInformationRootViewController alloc]init];
            PDNavigationController *intormationViewNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
            [intormationViewNav setViewControllers:@[intormationViewController]];
            [[PDMainTabbarViewController sharedController] setViewController:intormationViewNav atIndex:1];
            
            return;
        }
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            [self showLogin];
        }
    }];
}

- (void)setupBar {
    // 跳转钱包
    UIImage *backgroundImage = [UIImage imageNamed:@"bg_amusement_button"];
    self.goldComposeButton = [[CCZComposeButton alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 20 + (44 - 30) / 2, 0, 30) leftImage:[UIImage imageNamed:@"icon_amusement_gold"] rightImage:[UIImage imageNamed:@"icon_amusement_topup"]];
    self.goldComposeButton.title = @"0";
    self.goldComposeButton.titleFont = [[UIFont systemFontOfCustomeSize:13] boldFont];
    self.goldComposeButton.backgroundImage = backgroundImage;
    [self.goldComposeButton addTarget:self action:@selector(didClickLeftButton) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UIButton *avatarButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.avatarButton = avatarButton;
    [avatarButton setFrame:CGRectMake(0, 0, LCFloat(30), LCFloat(30))];
//    [avatarButton sizeThatFits:CGSizeMake(LCFloat(30), LCFloat(30))];
    [avatarButton buttonWithBlock:^(UIButton *button) {
        [[RESideMenu shareInstance] presentLeftMenuViewController];
    }];
    UIBarButtonItem *avatarItem = [[UIBarButtonItem alloc] initWithCustomView:self.avatarButton];
    UIBarButtonItem *goldItem = [[UIBarButtonItem alloc] initWithCustomView:self.goldComposeButton];
    self.navigationItem.leftBarButtonItems = @[avatarItem,goldItem];
    
    // 排行榜
    self.rankComposeButton = [[CCZComposeButton alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, 20 + (44 - 30) / 2, 0, 30) leftImage:[UIImage imageNamed:@"icon_amusement_rank"] rightImage:nil];
    self.rankComposeButton.title = @"排行榜";
    self.rankComposeButton.titleFont = self.goldComposeButton.titleFont;
    self.rankComposeButton.backgroundImage = backgroundImage;
    self.rankComposeButton.frame = CGRectMake(kScreenBounds.size.width - self.rankComposeButton.width - kTableViewSectionHeader_left, self.rankComposeButton.frame.origin.y, self.rankComposeButton.width, 35);
    [self.rankComposeButton addTarget:self action:@selector(didClickRightButton) forControlEvents:UIControlEventTouchUpInside];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.rankComposeButton];
}

- (void)setupTrotingLabel {
    self.trotingLabel = [[CCZTrotingLabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.topBar.frame), kScreenBounds.size.width, 55)];
    self.trotingLabel.backgroundColor = [UIColor whiteColor];
    self.trotingLabel.repeatTextArr = YES;
    self.trotingLabel.pause = 1.5;
    self.trotingLabel.type = CCZWalkerTypeDescend;
    self.trotingLabel.textColor = [UIColor blackColor];
    [self.view addSubview:self.trotingLabel];
    
    PDAmusementTrotingLeftView *leftView = [[PDAmusementTrotingLeftView alloc] initWithFrame:CGRectMake(0, 0, 60, 55)];
    self.trotingLabel.leftView = leftView;
    
    [self.trotingLabel trotingWithAttribute:^(CCZTrotingAttribute * _Nonnull attribute) {
        [leftView updateNoticeWithType:attribute.userInfo[@"type"]];
    }];
}

- (void)setupTabelView {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.trotingLabel.frame), kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(self.trotingLabel.frame) - 49 - 64) style:UITableViewStyleGrouped];
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];//
    
    self.partView = [[PDAmusementPartView alloc] init];
    self.mainTableView.tableFooterView = self.partView;
    __weak typeof(self) weakSelf = self;
    [self.partView partViewClickActions:^(int index, PDAmusementPartItem *item) {
        if (item.enable == YES) {
            if ([item.code isEqualToString:@"lolJungle"] || [item.code isEqualToString:@"dota2Jungle"] || [item.code isEqualToString:@"kingJungle"]) {
                [weakSelf goChaosFight];
            } else if ([item.code isEqualToString:@"matchGuess"]){ // 赛事竞猜
                [weakSelf goMatchGame];
            } else if ([item.code isEqualToString:@"lolChampionGuess"] || [item.code isEqualToString:@"dota2ChampionGuess"] || [item.code isEqualToString:@"kingChampionGuess"]) { // 英雄猜
                [weakSelf gotoHeroGuess];
            } else if ([item.code isEqualToString:@"lolCards"] || [item.code isEqualToString:@"dota2Cards"] || [item.code isEqualToString:@"kingCards"]) { // 英雄杀
                [weakSelf gotoHeroScuffle];
            } else if ([item.code isEqualToString:@"help"]) { // 新手教程
                PDWebViewController *webViewController = [[PDWebViewController alloc] init];
                [webViewController webDirectedWebUrl:amusementTheGuide];
                [weakSelf pushViewController:webViewController animated:YES];
            } else if ([item.code isEqualToString:@"invite"]) { // 邀请
                PDInviteViewController *inviteViewController = [[PDInviteViewController alloc] init];
                [weakSelf pushViewController:inviteViewController animated:YES];
            } else if ([item.code isEqualToString:@"lolCombat"]){       // 约站
                PDYuezhanRootViewController *yuezhanRootViewController = [[PDYuezhanRootViewController alloc]init];
                [self.navigationController pushViewController:yuezhanRootViewController animated:YES];
                return;
            } else if ([item.code isEqualToString:@"pubgCombat"]){
                PDJuediYuezhanRootViewController *juediYuezhanViewController = [[PDJuediYuezhanRootViewController alloc]init];
                [self.navigationController pushViewController:juediYuezhanViewController animated:YES];
            } else if ([item.code isEqualToString:@"pubgSearchbattle"]){
                PDJuediYuezhanSearchViewController *searchViewController = [[PDJuediYuezhanSearchViewController alloc]init];
                [self.navigationController pushViewController:searchViewController animated:YES];
            } else if ([item.code isEqualToString:@"pubgEquipmentData"]){
                PDWebViewController *webViewController = [[PDWebViewController alloc]init];
                NSString *webUrl = [NSString stringWithFormat:@"http://%@:%@/po/pd_web/data.html",JAVA_Host,JAVA_Port];
                [webViewController webDirectedWebUrl:webUrl];
                [self.navigationController pushViewController:webViewController animated:YES];
            }
        } else {
            [PDHUD showHUDSuccess:@"敬请期待!"];
            return ;
        }
    }];
}

/**
 * 赛事猜
 */
- (void)goMatchGame {
    __weak typeof(self) weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        PDLotteryGameMainViewController *lotteryGameViewController = [[PDLotteryGameMainViewController alloc]init];
        [strongSelf.navigationController pushViewController:lotteryGameViewController animated:YES];
    }];
}

/**
 * 英雄猜入口
 */
- (void)gotoHeroGuess {
    __weak typeof(self) weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]) {
            PDLotteryHeroViewController *heroViewController = [[PDLotteryHeroViewController alloc] init];
            [strongSelf pushViewController:heroViewController animated:YES];
        }
    }];
}

/**
 * 英雄杀入口
 */
- (void)gotoHeroScuffle {
    __weak typeof(self) weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        if (!weakSelf) {
            return ;
        }
        
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]) {
            PDScuffleEnterViewController *enterViewController = [[PDScuffleEnterViewController alloc] init];
            [strongSelf pushViewController:enterViewController animated:YES];
        }
    }];
}

/**
 * 打野大乱斗入口
 */
- (void)goChaosFight {
    __weak typeof(self) weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            PDChaosFightRootViewController *chaosViewController = [[PDChaosFightRootViewController alloc]init];
            strongSelf->hasGoinChaosFight = YES;
            PDNavigationController *nav = [[PDNavigationController alloc]initWithRootViewController:chaosViewController];
            [strongSelf.navigationController presentViewController:nav animated:YES completion:NULL];
        }
    }];
}

#pragma mark - 控件方法
- (void)didClickLeftButton {
    PDTopUpViewController *topupViewController = [[PDTopUpViewController alloc] init];
    topupViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:topupViewController animated:YES];
}

- (void)didClickRightButton {
    PDRegisterIsEighteenViewController *regVC = [[PDRegisterIsEighteenViewController alloc]init];
    [self.navigationController pushViewController:regVC animated:YES];
    
    return;
    
    PDRankRootViewController *rankViewController = [[PDRankRootViewController alloc] init];
    rankViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:rankViewController animated:YES];
}
#pragma mark - TableView Delagte

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *gameCellID = @"amusemntGameCellID";
    PDAmusementGameCell *gameCell = [tableView dequeueReusableCellWithIdentifier:gameCellID];
    if (!gameCell) {
        gameCell = [[PDAmusementGameCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:gameCellID];
    }
    gameCell.transferGameRootSubSingleModel = [self.lotteryMutableArr firstObject];
    __weak typeof(self) weakSelf = self;
    [gameCell didClickMoreActions:^{
        [weakSelf goMatchGame];
    }];
    return gameCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self.lotteryMutableArr firstObject] == nil) {
        return 0;
    }
    return [PDAmusementGameCell cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    PDLoteryGameRootSubSingleModel *subSingleModel = [self.lotteryMutableArr firstObject];
    
    if (subSingleModel.matchId.length == 0 || !subSingleModel.matchId) {
        return;
    }
    
    PDLotteryGameDetailMainViewController *detailViewController = [[PDLotteryGameDetailMainViewController alloc] init];
    detailViewController.transferMatchId = subSingleModel.matchId;
    [self pushViewController:detailViewController animated:YES];
}

#pragma mark - 网络请求

/** 会员金币资产*/
- (void)fetchMemberTotalGold {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberWallet requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        if (isSucceeded) {
            NSNumber *totalgold = responseObject[@"totalgold"]; // 总资产
            [AccountModel sharedAccountModel].memberInfo.gold = [totalgold integerValue];
            weakSelf.goldComposeButton.title = [NSString stringWithFormat:@"%@",totalgold];
        }
    }];
}

/**
 * 第一次进来的时候要主动去获取一下公告
 */
- (void)fetchNotices {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:amusementNotice requestParams:nil responseObjectClass:[PDAmusementNoticeList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDAmusementNoticeList *list = (PDAmusementNoticeList *)responseObject;
            NSMutableArray *modelArr = [NSMutableArray array];
            for (PDAmusementNoticeModel *model in list.items) {
                CCZTrotingAttribute *att = [[CCZTrotingAttribute alloc] init];
                att.text = model.content;
                att.userInfo = @{@"type":model.type};
                [modelArr addObject:att];
            }
            [weakSelf.trotingLabel removeAllAttributes];
            [weakSelf.trotingLabel addTrotingAttributes:[modelArr copy]];
        }
    }];
}

/**
 * 娱乐模块列表
 */
- (void)fetchParts {
    __weak typeof(self) weakSelf = self;
    NSDictionary *param ;
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        param = @{@"gameType":@"lol"};
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        param = @{@"gameType":@"dota2"};
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        param = @{@"gameType":@"king"};
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePubg) {
        param = @{@"gameType":@"pubg"};
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeCS) {
        param = @{@"gameType":@"cs"};
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:amusementPartList requestParams:param responseObjectClass:[PDAmusementPartList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) return;
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded) {
            PDAmusementPartList *partList = (PDAmusementPartList *)responseObject;
            strongSelf.partView.items = partList.posterlist;
        }
    }];
}

- (void)updateAmusementItems {
    [self fetchParts];
}

#pragma mark - 获取竞猜信息
-(void)sendRequestToGetGuessList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:memberGuessMatchguess requestParams:nil responseObjectClass:[PDLoteryGameRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDLoteryGameRootListModel *lotteryList = (PDLoteryGameRootListModel *)responseObject;
            
            
            for (int i = 0 ; i < lotteryList.lotteryList.count;i++){
                // 1. 获取当前所有的赛事
                PDLoteryGameRootSingleModel *lotteryGameRootSingleModel = [lotteryList.lotteryList objectAtIndex:i];
                for (int j = 0 ; j < lotteryGameRootSingleModel.subList.count;j++){
                    PDLoteryGameRootSubSingleModel *subSingleModel = [lotteryGameRootSingleModel.subList objectAtIndex:j];
                    subSingleModel.gameStartTempTime = [NSDate getNSTimeIntervalWithCurrent] + subSingleModel.matchLeftTime;
                    if (j == 0){
                        subSingleModel.currentTempTime = lotteryGameRootSingleModel.dateTime;
                    } else {
                        subSingleModel.currentTempTime = -1;
                    }
                    
                    for (int k = 0 ; k < subSingleModel.lotteryDetailList.count;k++){
                        PDLotteryGameSubListSingleModel *subListSingleModel = [subSingleModel.lotteryDetailList objectAtIndex:k];
                        subListSingleModel.endTime = [NSDate getNSTimeIntervalWithCurrent] + subListSingleModel.leftTime;
                    }
                }
                
                
                if (i == 0) {
                    if (strongSelf.lotteryMutableArr.count) {
                        [strongSelf.lotteryMutableArr removeAllObjects];
                    }
                    [strongSelf.lotteryMutableArr addObjectsFromArray:lotteryGameRootSingleModel.subList];
                }
            }
            
            
            // 进行刷新
            [strongSelf.mainTableView beginUpdates];
            [strongSelf.mainTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:0 inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
            [strongSelf.mainTableView endUpdates];
        }
    }];
}

#pragma mark - 添加Socket

- (void)socketDidBackData:(id)responseObject {
    [super socketDidBackData:responseObject];
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *resp = (NSDictionary *)responseObject;
        NSDictionary *data = resp[@"data"];
        NSNumber *type = data[@"type"];
        if (type.intValue == 910) {
            NSArray *items = data[@"items"];
            NSMutableArray *modelArr = [NSMutableArray array];
            for (NSDictionary *obj in items) {
                PDAmusementNoticeModel *model = [[PDAmusementNoticeModel alloc] init];
                model.ID = obj[@"id"];
                model.content = obj[@"content"];
                model.createTime = [obj[@"createTime"] integerValue];
                model.notifyTime = [obj[@"notifyTime"] integerValue];
                model.type = obj[@"type"];
                CCZTrotingAttribute *att = [[CCZTrotingAttribute alloc] init];
                att.text = model.content;
                att.userInfo = @{@"type":model.type};
                [modelArr addObject:att];
            }
            
            [self.trotingLabel removeAllAttributes];
            [self.trotingLabel addTrotingAttributes:[modelArr copy]];
        }
    }
}

-(void)showNewFeature {
    __weak typeof(self)weakSelf = self;
    [PDCenterTool firstShowGuideWithAmusement:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf showLogin];
    }];
}

-(void)showLogin{
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^{
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){             // 登录了
            PDSliderViewController *sliderViewController = (PDSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
            [sliderViewController sliderLoginManager];
            [strongSelf fetchNotices];
            [strongSelf fetchParts];
            [strongSelf fetchMemberTotalGold];
            [strongSelf sendRequestToGetGuessList];
            // 签到
//            [[AccountModel sharedAccountModel] sendRequestToQiandao];
        }
    }];
}

-(void)showFirstChooseGameInfo{
    if ([AccountModel sharedAccountModel].isShenhe){
        return;
    }
    
    if (![Tool userDefaultGetWithKey:FirstChooseGame].length){
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            PDFirstChooseGameTypeViewController *firstChooseGame = [[PDFirstChooseGameTypeViewController alloc]init];
            __weak typeof(self)weakSelf = self;
            [firstChooseGame chooseGameBlock:^{
                if (!weakSelf){
                    return ;
                }
                PDSliderViewController *sliderViewController = (PDSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
                [sliderViewController.bottomView firstChooseGameManager];
                [sliderViewController uploadTabbarIconsManager];
            }];
            PDNavigationController *zoneNav = [[PDNavigationController alloc] initWithNavigationBarClass:[PDGradientNavBar class] toolbarClass:nil];
            [zoneNav setViewControllers:@[firstChooseGame]];
            [self presentViewController:zoneNav animated:YES completion:NULL];
        });
    }
}

#pragma mark - 其他

- (BOOL)shouldAutorotate{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskLandscapeLeft | UIInterfaceOrientationMaskLandscapeRight;
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

#pragma mark - 监听设备旋转通知
- (void)listeningRotating{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDeviceOrientationChange) name:UIDeviceOrientationDidChangeNotification object:nil ];
}


/**
 *  屏幕方向发生变化会调用这里
 */
- (void)onDeviceOrientationChange {
    UIDeviceOrientation orientation             = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = (UIInterfaceOrientation)orientation;
    switch (interfaceOrientation) {
        case UIInterfaceOrientationPortraitUpsideDown:{
            NSLog(@"【Down】");
        }
            break;
        case UIInterfaceOrientationPortrait:{
            if (hasGoinChaosFight){
                if ([self.lotteryMutableArr firstObject] == nil) {
                    return ;
                }
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                    [self.mainTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    PDSliderViewController *sliderViewController = (PDSliderViewController *)[RESideMenu shareInstance].leftMenuViewController;
                    [sliderViewController.sliderTableView reloadData];
                    hasGoinChaosFight = NO;
                });
            }
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:{
            NSLog(@"【Left】");
        }
            break;
        case UIInterfaceOrientationLandscapeRight:{
            NSLog(@"【Right】");
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - 审核标记
-(void)sendRequestToGetShenheWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"version":[Tool appVersion]};
    [[NetworkAdapter sharedAdapter] fetchWithPath:shenhe requestParams:params responseObjectClass:[PDAppleShenheModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!
            weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDAppleShenheModel *shenheModel = (PDAppleShenheModel *)responseObject;
            [AccountModel sharedAccountModel].isShenhe = shenheModel.enable;
            [AccountModel sharedAccountModel].auth = shenheModel.auth;
#ifdef DEBUG        // 测试           // 【JAVA】
//            [AccountModel sharedAccountModel].isShenhe = NO;
//            [AccountModel sharedAccountModel].auth = YES;
#else               // 线上
            
#endif
        }
        if (block){
            block();
        }
    }];
}
@end
