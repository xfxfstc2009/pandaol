//
//  PDRankRootViewController.h
//  PandaKing
//
//  Created by Cranz on 17/2/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 排行榜容器控制器
@interface PDRankRootViewController : AbstractViewController

@end
