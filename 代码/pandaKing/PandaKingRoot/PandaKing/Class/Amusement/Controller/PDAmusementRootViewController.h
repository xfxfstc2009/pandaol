//
//  PDAmusementRootViewController.h
//  PandaKing
//
//  Created by Cranz on 17/2/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 娱乐根视图控制器
@interface PDAmusementRootViewController : AbstractViewController

+(instancetype)sharedController;

@property (nonatomic,strong)UIButton *avatarButton;


-(void)directToChongzhiManager;
-(void)showNewFeature;                          /**< 显示新手引导*/

/**
 * 每次重选游戏后去更新
 */
- (void)updateAmusementItems;
-(void)showFirstChooseGameInfo;
@end
