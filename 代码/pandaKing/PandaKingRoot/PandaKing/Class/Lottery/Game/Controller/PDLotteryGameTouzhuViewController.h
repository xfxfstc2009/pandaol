//
//  PDLotteryGameTouzhuViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 投注控制器
#import "AbstractViewController.h"
#import "PDTouzhuDetailModel.h"

@interface PDLotteryGameTouzhuViewController : AbstractViewController

// 相关属性
@property (nonatomic,assign) BOOL isHasGesture;                             // 判断是否包含手势
@property (nonatomic,strong)PDTouzhuDetailModel *transferTouzhuDetailModel;     /**< 传递过去的投注model*/

@property (nonatomic, assign) NSUInteger memberMaximalGold; // 用户当前最大金币数额

- (void)showInView:(UIViewController *)viewController;
- (void)dismissFromView:(UIViewController *)viewController;
- (void)hideParentViewControllerTabbar:(UIViewController *)viewController;              // 毛玻璃效果

-(void)touzhuBtnClickManager:(void(^)(NSString *gold))block;

@property (nonatomic,assign)BOOL isGuider;                      // 是否引导


@end
