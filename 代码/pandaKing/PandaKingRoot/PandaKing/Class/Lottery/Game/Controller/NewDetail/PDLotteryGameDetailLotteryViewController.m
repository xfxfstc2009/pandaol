//
//  PDLotteryGameDetailLotteryViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameDetailLotteryViewController.h"
#import "PDNewDetailLotteryCellTableViewCell.h"
#import "PDTopUpViewController.h"
#import "PDLotteryGameTouzhuViewController.h"
#import "PDTouzhuDetailSuccessModel.h"


static char loginKey;
@interface PDLotteryGameDetailLotteryViewController ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,assign)BOOL hasWindowShow;         /**< 判断弹窗是否弹出*/
@end

@implementation PDLotteryGameDetailLotteryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{

}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.lotteryMutableArr = [NSMutableArray array];
}

#pragma mark - createTableView
-(void)createTableView{
    if(!self.lotteryTableView){
        self.lotteryTableView= [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.lotteryTableView.delegate = self;
        self.lotteryTableView.dataSource = self;
        self.lotteryTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.lotteryTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.lotteryTableView.showsVerticalScrollIndicator = YES;
        self.lotteryTableView.backgroundColor = [UIColor clearColor];
        [self.view addSubview:self.lotteryTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.lotteryTableView appendingPullToRefreshHandler:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(scrollViewDelegateManager:andDrop:)]){
            [strongSelf.delegate scrollViewDelegateManager:strongSelf.lotteryTableView andDrop:YES];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [strongSelf.lotteryTableView stopPullToRefresh];
        });
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.lotteryMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString*cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    PDNewDetailLotteryCellTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDNewDetailLotteryCellTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    PDMatchguessinfoGuessListRootSingleModel *guessSingleModel = [self.lotteryMutableArr objectAtIndex:indexPath.row];
    cellWithRowOne.transferLotteryGameSingleModel = guessSingleModel;
    
    // 【进行投注】
    __weak typeof(self)weakSelf = self;
    [cellWithRowOne actionClickToTouzhuWithLeft:^(BOOL isLeft) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf authorizeWithCompletionHandler:^{
            if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
                void(^block)() = objc_getAssociatedObject(strongSelf, &loginKey);
                if (block){
                    block();
                }
                
                if ([AccountModel sharedAccountModel].matchGuess3 == NO){
                    [self showGuiderManager];
                    return ;
                }
                
                [strongSelf sendRequestToStakeGuessWithTeamLeft:isLeft andModel:guessSingleModel];
            }
        }];
    }];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PDNewDetailLotteryCellTableViewCell calculationCellHeight];
}


#pragma mark - 请求竞猜
-(void)sendRequestToStakeGuessWithTeamLeft:(BOOL)isLeft andModel:(PDMatchguessinfoGuessListRootSingleModel *)guessSingleModel{
    // 先去获取用户的金币数
    __weak typeof(self) weakSelf = self;
    [self fetchMemberPropertyComplicationHandle:^(NSUInteger maxerGold) {
        [weakSelf fetchStakeGuessWithMemberGold:maxerGold isTeamLeft:isLeft andModel:guessSingleModel];
    }];
}

- (void)fetchStakeGuessWithMemberGold:(NSUInteger)gold isTeamLeft:(BOOL)isLeft andModel:(PDMatchguessinfoGuessListRootSingleModel *)guessSingleModel{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"guessId":guessSingleModel.guessId,@"onPointA":@(isLeft)};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:requeststakematchguess requestParams:params responseObjectClass:[PDTouzhuDetailModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDTouzhuDetailModel *touzhuDetailModel = (PDTouzhuDetailModel *)responseObject;
            touzhuDetailModel.jingcaiName = guessSingleModel.guessName;
            if (isLeft){
                touzhuDetailModel.teamName = guessSingleModel.teamLeft.point;
            } else {
                touzhuDetailModel.teamName = guessSingleModel.teamRight.point;
            }
            if (!strongSelf.hasWindowShow){
                strongSelf.hasWindowShow = YES;
                PDLotteryGameTouzhuViewController *lotteryGameTouzhuViewController = [[PDLotteryGameTouzhuViewController alloc]init];
                lotteryGameTouzhuViewController.isHasGesture = YES;
                lotteryGameTouzhuViewController.hasCancelSocket = YES;
                [lotteryGameTouzhuViewController showInView:strongSelf.parentViewController];
                lotteryGameTouzhuViewController.memberMaximalGold = gold;
                lotteryGameTouzhuViewController.transferTouzhuDetailModel = touzhuDetailModel;
                [lotteryGameTouzhuViewController touzhuBtnClickManager:^(NSString *gold) {
                    strongSelf.hasWindowShow = NO;
                    if ([gold isEqualToString:@"0"]){
                        [PDHUD showHUDError:@"请选择竞猜金币"];
                        return ;
                    } else if ([gold isEqualToString:@"-1"]){
                        return;
                    }
                    [strongSelf sendRequestToTouzhuGuessWithGold:gold team:isLeft peilv:touzhuDetailModel.oddsOnPoint name:touzhuDetailModel.teamName andModel:guessSingleModel];
                }];
            }
        }
    }];
}

//  去竞猜
-(void)sendRequestToTouzhuGuessWithGold:(NSString *)gold team:(BOOL)isTeamLeft peilv:(CGFloat)peilv name:(NSString *)teamName andModel:(PDMatchguessinfoGuessListRootSingleModel *)guessSingleModel{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"guessId":guessSingleModel.guessId,@"gold":gold,@"onPointA":@(isTeamLeft),@"odds":@(peilv)};
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:stakematchguess requestParams:params responseObjectClass:[PDTouzhuDetailSuccessModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDTouzhuDetailSuccessModel *touzhuSuccessModel = (PDTouzhuDetailSuccessModel *)responseObject;
            if (touzhuSuccessModel.isChange){           // 竞猜倍率改变
                NSString *title = [NSString stringWithFormat:@"注意，由于您在竞猜页面停留过长时间，事实倍率已经变化，现在赔率为\n 【%f->%f】\n 是否选择继续竞猜",touzhuSuccessModel.invalidOdds,touzhuSuccessModel.currentOdds];
                [[PDAlertView sharedAlertView] showAlertWithTitle:@"竞猜提醒" conten:title isClose:NO btnArr:@[@"继续",@"取消"] buttonClick:^(NSInteger buttonIndex) {
                    [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
                    if (buttonIndex == 0){          // 继续
                        [strongSelf sendRequestToTouzhuGuessWithGold:gold team:isTeamLeft peilv:touzhuSuccessModel.currentOdds name:teamName andModel:guessSingleModel];
                    }
                }];
            } else {                                    // 竞猜倍率没变
                
                NSString *infoTitle = @"";
                if (isTeamLeft){
                    infoTitle = guessSingleModel.teamLeft.point;
                } else {
                    infoTitle = guessSingleModel.teamRight.point;
                }
                [strongSelf touzhuWithTitle:infoTitle model:touzhuSuccessModel];
            }
        }
    }];
}

-(void)touzhuWithTitle:(NSString *)title model:(PDTouzhuDetailSuccessModel *)touzhuSuccessModel{
    __weak typeof(self)weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"竞猜成功" conten:[NSString stringWithFormat:@"您已成功竞猜【%@】",title] isClose:NO btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
        
    }];
}

/** 余额不足*/
- (void)showNotEnough {
    __weak typeof(self) weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"余额不足" conten:@"最低竞猜金额为100金币" isClose:NO btnArr:@[@"去充值", @"取消"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissWithCompletion:NULL];
        if (buttonIndex == 0) {
            PDTopUpViewController *topUpViewController = [[PDTopUpViewController alloc] init];
            [weakSelf pushViewController:topUpViewController animated:YES];
        }
    }];
}

#pragma mark -- 获取用户的资产
- (void)fetchMemberPropertyComplicationHandle:(void(^)(NSUInteger maxerGold))complicationHandle {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberWallet requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        if (isSucceeded) {
            NSNumber *totalgold = responseObject[@"totalgold"]; // 总资产
            if (totalgold.integerValue < 100) { // 去充值
                [weakSelf showNotEnough];
            } else {
                if (complicationHandle) {
                    complicationHandle(totalgold.integerValue);
                }
            }
        }
    }];
}


#pragma mark - UIScrollViewDelegate

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (self.delegate &&[self.delegate respondsToSelector:@selector(scrollViewDelegateManager:andDrop:)]){
        [self.delegate scrollViewDelegateManager:scrollView andDrop:NO];
    }
}

//-(void)lotteryscrollViewWillBeginDragging:(UIScrollView *)scrollView
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (self.delegate &&[self.delegate respondsToSelector:@selector(lotteryscrollViewWillBeginDragging:)]){
        [self.delegate lotteryscrollViewWillBeginDragging:scrollView];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate; {
    if (self.delegate &&[self.delegate respondsToSelector:@selector(lotteryscrollViewDidEndDragging:)]){
        [self.delegate lotteryscrollViewDidEndDragging:scrollView];
    }
}



//-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
//    if (self.delegate &&[self.delegate respondsToSelector:@selector(scrollViewDelegateManager:andDrop:)]){
//        [self.delegate scrollViewDelegateManager:scrollView andDrop:NO];
//    }
//}
//
-(void)loginSuccessManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &loginKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



#pragma mark - 显示新手引导
-(void)showGuiderManager{
    return;
    PDLotteryGameTouzhuViewController *lotteryGameTouzhuViewController = [[PDLotteryGameTouzhuViewController alloc]init];
    lotteryGameTouzhuViewController.isHasGesture = YES;
    lotteryGameTouzhuViewController.hasCancelSocket = YES;

    PDMatchguessinfoGuessListRootSingleModel *guessListRootSingleModel =  [self.lotteryMutableArr objectAtIndex:0];
    
    PDTouzhuDetailModel *touzhuDetailModel = [[PDTouzhuDetailModel alloc]init];
    touzhuDetailModel.teamName = guessListRootSingleModel.teamLeft.teamName;
    touzhuDetailModel.oddsOnPoint = 100;
    touzhuDetailModel.memberMaxStakeGold = 1000;
    touzhuDetailModel.balance = 1000;

    lotteryGameTouzhuViewController.transferTouzhuDetailModel = touzhuDetailModel;
    lotteryGameTouzhuViewController.memberMaximalGold = 100;
    lotteryGameTouzhuViewController.isGuider = YES;
    [lotteryGameTouzhuViewController showInView:self.parentViewController];
    
    __weak typeof(self)weakSelf = self;
    [lotteryGameTouzhuViewController touzhuBtnClickManager:^(NSString *gold) {
        if (!weakSelf){
            return ;
        }
        if ([gold isEqualToString:@"-1"]){
            return;
        }
        [[PDAlertView sharedAlertView] showAlertWithTitle:@"完成教学" conten:@"恭喜您完成【赛事猜】教学，马上竞猜试试吧！" isClose:YES btnArr:@[@"立即竞猜"] buttonClick:^(NSInteger buttonIndex) {
            [[PDAlertView sharedAlertView] dismissAllWithBlock:NULL];
        }];
    }];
}





#pragma mark - 通过坐标转换到当前
-(void)getCurrentLocationToManager:(CGPoint)point{
    //1.
    NSInteger index = point.y / [PDNewDetailLotteryCellTableViewCell calculationCellHeight];
    
//    PDNewDetailLotteryCellTableViewCell *cellWithRowOne = [self.lotteryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    
    BOOL left = YES;
    if (point.x <= kScreenBounds.size.width / 3.){
        left = YES;
    } else if (point.x >= kScreenBounds.size.width * 2 / 3){
        left = NO;
    } else {
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    [weakSelf authorizeWithCompletionHandler:^{
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            void(^block)() = objc_getAssociatedObject(weakSelf, &loginKey);
            if (block){
                block();
            }
            
//            if ([AccountModel sharedAccountModel].matchGuess3 == NO){
//                [self showGuiderManager];
//                return ;
//            }
            PDMatchguessinfoGuessListRootSingleModel *guessSingleModel = [self.lotteryMutableArr objectAtIndex:index];
            if ([guessSingleModel.guessState isEqualToString:@"finished"]){
                return;
            } else if ([guessSingleModel.guessState isEqualToString:@"stakeoff"]){
                [PDHUD showHUDError:@"竞猜未开始，暂不能投注"];
                return;
            }
            [weakSelf sendRequestToStakeGuessWithTeamLeft:left andModel:guessSingleModel];
        }
    }];

    
    
}




@end
