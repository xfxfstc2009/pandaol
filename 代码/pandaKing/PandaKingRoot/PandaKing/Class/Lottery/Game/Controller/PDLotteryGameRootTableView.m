//
//  PDLotteryGameRootTableView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameRootTableView.h"

typedef NS_ENUM(NSInteger,PDLotteryGameState) {
    PDLotteryGameStateFlod,                       /**< 收缩状态*/
    PDLotteryGameStateShow,                       /**< 显示状态*/
};


@interface PDLotteryGameRootTableView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSMutableArray *statusArray;


@end

@implementation PDLotteryGameRootTableView

-(instancetype)init{
    self = [super init];
    if (self){
        [self arrayWithInit];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self arrayWithInit];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style {
    self = [super initWithFrame:frame style:style];
    if (self) {
        [self arrayWithInit];
    }
    return self;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit {                         // 设置代理
    self.delegate = self;
    self.dataSource = self;
    if (self.style == UITableViewStylePlain) {
        self.tableFooterView = [[UIView alloc] init];
    }
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onChangeStatusBarOrientationNotification:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
}

// 数据初始化
-(NSMutableArray *)statusArray {
    if (!_statusArray) {
        _statusArray = [NSMutableArray array];
    }
    if (_statusArray.count) {
        if (_statusArray.count > self.numberOfSections) {
            [_statusArray removeObjectsInRange:NSMakeRange(self.numberOfSections - 1, _statusArray.count - self.numberOfSections)];
        }else if (_statusArray.count < self.numberOfSections) {
            for (NSInteger i = self.numberOfSections - _statusArray.count; i < self.numberOfSections; i++) {
                [_statusArray addObject:[NSNumber numberWithInteger:PDLotteryGameStateFlod]];
            }
        }
    }else{
        for (NSInteger i = 0; i < self.numberOfSections; i++) {
            [_statusArray addObject:[NSNumber numberWithInteger:PDLotteryGameStateFlod]];
        }
    }
    return _statusArray;
}

#pragma mark - 折叠&打开
-(void)onChangeStatusBarOrientationNotification:(NSNotification *)notification {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self reloadData];
    });
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.foldDelegate && [self.foldDelegate respondsToSelector:@selector(numberOfSectionForFlodTableView:)]) {
        return [self.foldDelegate numberOfSectionForFlodTableView:self];
    }else{
        return self.numberOfSections;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (((NSNumber *)self.statusArray[section]).integerValue == PDLotteryGameStateShow) {
        if (self.foldDelegate && [self.foldDelegate respondsToSelector:@selector(foldTableView:numberOfRowInSection:)]) {
            return [self.foldDelegate foldTableView:self numberOfRowInSection:section];
        }
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.foldDelegate && [self.foldDelegate respondsToSelector:@selector(foldTableView:cellForRowAtIndexPath:)]){
        return [self.foldDelegate foldTableView:tableView cellForRowAtIndexPath:indexPath];
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.foldDelegate && [self.foldDelegate respondsToSelector:@selector(foldTableView:heightForHeaderInSection:)]) {
        return [self.foldDelegate foldTableView:self heightForHeaderInSection:section];
    }else{
        return self.sectionHeaderHeight;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.foldDelegate && [self.foldDelegate respondsToSelector:@selector(foldTableView:heightForRowAtIndexPath:)]) {
        return [self.foldDelegate foldTableView:self heightForRowAtIndexPath:indexPath];
    }else{
        return self.rowHeight;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.foldDelegate && [self.foldDelegate respondsToSelector:@selector(foldTableView:didSelectRowAtIndexPath:)]) {
        [self.foldDelegate foldTableView:self didSelectRowAtIndexPath:indexPath];
    }
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (self.foldDelegate && [self.foldDelegate respondsToSelector:@selector(foldTableView:viewForHeaderInSection:)]){
        return [self.foldDelegate foldTableView:self viewForHeaderInSection:section];
    }
    return [[UIView alloc]init];
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (self.style == UITableViewStylePlain) {
        return 0;
    }else{
        return 0.001;
    }
}


-(void)foldFoldingSectionHeaderTappedAtIndex:(NSInteger)index {
    BOOL currentIsOpen = ((NSNumber *)self.statusArray[index]).boolValue;
    
    [self.statusArray replaceObjectAtIndex:index withObject:[NSNumber numberWithBool:!currentIsOpen]];
    
    NSInteger numberOfRow = [self.foldDelegate foldTableView:self numberOfRowInSection:index];
    NSMutableArray *rowArray = [NSMutableArray array];
    if (numberOfRow) {
        for (NSInteger i = 0; i < numberOfRow; i++) {
            [rowArray addObject:[NSIndexPath indexPathForRow:i inSection:index]];
        }
    }
    if (rowArray.count) {
        if (currentIsOpen) {
            [self deleteRowsAtIndexPaths:[NSArray arrayWithArray:rowArray] withRowAnimation:UITableViewRowAnimationTop];
        }else{
            [self insertRowsAtIndexPaths:[NSArray arrayWithArray:rowArray] withRowAnimation:UITableViewRowAnimationTop];

            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:index];
            [self scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.foldDelegate && [self.foldDelegate respondsToSelector:@selector(foldTableView:willDisplayCell:forRowAtIndexPath:)]) {
        [self.foldDelegate foldTableView:tableView willDisplayCell:cell forRowAtIndexPath:indexPath];
    }
}


@end
