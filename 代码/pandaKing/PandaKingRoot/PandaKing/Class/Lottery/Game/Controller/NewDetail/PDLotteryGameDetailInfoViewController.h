//
//  PDLotteryGameDetailInfoViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 【详情】
#import "AbstractViewController.h"
#import "PDMatchguessinfoSingleModel.h"
#import "PDMatchguessinfoGuessListSingleModel.h"

@protocol PDLotteryGameDetailInfoViewControllerDelegate <NSObject>

-(void)scrollViewDelegateWithDetailManager:(UIScrollView *)scrollView andDrop:(BOOL)drop;

@end

@interface PDLotteryGameDetailInfoViewController : AbstractViewController
@property (nonatomic,strong)UITableView *detailTableView;
@property (nonatomic,strong)NSMutableArray *detailMutableArr;

@property(nonatomic,strong)PDMatchguessinfoSingleModel *transferMatchSingleModel;
@property(nonatomic,strong)PDMatchguessinfoGuessListSingleModel *transferGuessMainSingleModel;

@property (nonatomic,weak)id<PDLotteryGameDetailInfoViewControllerDelegate> delegate;


@end
