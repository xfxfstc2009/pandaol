//
//  PDLotteryGameTouzhuViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameTouzhuViewController.h"
#import <objc/runtime.h>


#define SheetViewHeight       LCFloat(128 + 49)


static NSString *btnClickKey;
@interface PDLotteryGameTouzhuViewController(){
    
}
@property (nonatomic,weak)UIViewController *showViewController;
@property (nonatomic,strong)UIView *backgrondView;
@property (nonatomic,strong)UIImageView *shareView;
@property (nonatomic,strong) UIImage *backgroundImage;                      // 背景图片

@property (nonatomic,strong)UIView *topBlackView;
@property (nonatomic,strong)UILabel *dymicLabel;


// 1. fixed金币
@property (nonatomic,strong)UIView *topWriteView;                   /**< 上面的白色的view*/
@property (nonatomic,strong)UILabel *fixedGoldLabel;                /**< 金币固定*/
@property (nonatomic,strong)UILabel *dymicGoldLabel;                /**< 动态的金币*/
@property (nonatomic,strong)UIView *lineView1;                      /**< 竖线*/
@property (nonatomic,strong)UILabel *fixedMabeLabel;                /**< 可能获得金币*/
@property (nonatomic,strong)UILabel *dymicMabeLabel;                /**< 动态可能获得金币*/
@property (nonatomic,strong)UIView *lineView2;                      /**< 横线view*/
@property (nonatomic,strong)UILabel *fixedLabel;                    /**< 固定的label*/

@property (nonatomic,strong)UISlider *slider;                       /**< slider*/
@property (nonatomic,strong)UIButton *leftBtn;                      /**< 左侧的按钮*/
@property (nonatomic,strong)UIButton *rightBtn;                     /**< 右侧的按钮*/
@property (nonatomic,strong)UIButton *determineButton;

@end


@implementation PDLotteryGameTouzhuViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self createSheetView];                      // 加载Sheetview
    [self createDismissButton];                  // 创建dismissButton
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([AccountModel sharedAccountModel].matchGuess3 == NO){
        [self guiderManager];                        // 新手引导
    }
}

#pragma mark - 创建sheetView
-(void)createSheetView{
    self.view.backgroundColor = [UIColor clearColor];
    if (IS_IOS7_LATER){
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = YES;
    }
    
    // 创建背景色
    self.backgrondView = [[UIView alloc]initWithFrame:self.view.bounds];
    self.backgrondView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:.65f];
    [self.view addSubview:self.backgrondView];
    [self backgroundColorFadeInOrOutFromValue:.0f toValue:1.0f];
    
    [self createSharetView];
    
}

#pragma mark - 创建view
-(void)createSharetView{
    CGFloat viewHeight = SheetViewHeight;
    CGRect screenRect = [[UIScreen mainScreen]bounds];
    // ShareViewShow
    _shareView = [[UIImageView alloc]initWithFrame:CGRectMake(0, screenRect.size.height, screenRect.size.width, viewHeight)];
    _shareView.clipsToBounds = YES;
    _shareView.image = self.backgroundImage;
    _shareView.userInteractionEnabled = YES;
    _shareView.contentMode = UIViewContentModeBottom;
    _shareView.backgroundColor = [UIColor colorWithRed:246/256. green:246/256. blue:246/256. alpha:.9];
    [self.view addSubview:_shareView];
    
    // 1. 创建上面的view
    self.topBlackView = [[UIView alloc]init];
    self.topBlackView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.topBlackView.frame = CGRectMake(0, 0, kScreenBounds.size.width , [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小提示"]] + 2 * LCFloat(4));
    [_shareView addSubview:self.topBlackView];
    
    // 2. 创建竞猜
    UILabel *fixedLabel = [[UILabel alloc]init];
    fixedLabel.backgroundColor = [UIColor clearColor];
    fixedLabel.text = @"竞猜";
    fixedLabel.textColor = [UIColor whiteColor];
    fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    CGSize fixedSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:fixedLabel.font])];
    fixedLabel.frame = CGRectMake(LCFloat(11), 0, fixedSize.width, self.topBlackView.size_height);
    [self.topBlackView addSubview:fixedLabel];
    
    // 3. 创建比赛内容信息
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.text = @"**-胜利-赔率**";
    if (self.transferTouzhuDetailModel){
        self.dymicLabel.text = [NSString stringWithFormat:@"%@-赔率%.2f",self.transferTouzhuDetailModel.teamName,self.transferTouzhuDetailModel.oddsOnPoint];
    }
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.dymicLabel.textColor = c26;
//    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLabel.font])];
    self.dymicLabel.frame = CGRectMake(CGRectGetMaxX(fixedLabel.frame), fixedLabel.orgin_y, kScreenBounds.size.width - CGRectGetMaxX(fixedLabel.frame), self.topBlackView.size_height);
    [self.topBlackView addSubview:self.dymicLabel];
    
    
    self.topWriteView = [self createMainView];
    self.topWriteView.orgin_y = CGRectGetMaxY(self.topBlackView.frame);
    self.topWriteView.backgroundColor = [UIColor whiteColor];
    [_shareView addSubview:self.topWriteView];
    
    
    _shareView.size_height = CGRectGetMaxY(self.topWriteView.frame) + LCFloat(49);
}

#pragma mark 创建dismissButton
-(void)createDismissButton{
    // Dismiss_Button
    self.determineButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.determineButton.frame = CGRectMake(0, _shareView.frame.size.height - LCFloat(49), _shareView.frame.size.width, LCFloat(49));
    [self.determineButton setTitle:@"确定投入" forState:UIControlStateNormal];
    [self.determineButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.determineButton.titleLabel.font = [UIFont systemFontOfSize:LCFloat(18.)];
    __weak typeof(self)weakSelf = self;
    [self.determineButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf) strongSelf = weakSelf;
        void(^block)(NSString *gold) = objc_getAssociatedObject(strongSelf, &btnClickKey);
        if (block){
            block(strongSelf.dymicGoldLabel.text);
        }
        [strongSelf sheetViewDismiss];
    }];
    self.determineButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [_shareView addSubview:self.determineButton];
    
    // Gesture
    if (self.isHasGesture){
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backgrondView addGestureRecognizer:tapGestureRecognizer];
    }
}

#pragma mark - actionClick
-(void)sheetViewDismiss{
    void(^block)(NSString *gold) = objc_getAssociatedObject(self, &btnClickKey);
    if (block){
        block(@"-1");
    }
    [self dismissFromView:_showViewController];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backgrondView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak PDLotteryGameTouzhuViewController *weakVC = self;
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.2f animations:^{
        weakVC.shareView.frame = CGRectMake(0, screenHeight, weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    } completion:^(BOOL finished) {
        [weakVC willMoveToParentViewController:nil];
        [weakVC.view removeFromSuperview];
        [weakVC removeFromParentViewController];
    }];
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    __weak PDLotteryGameTouzhuViewController *weakVC = self;
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];
    
    NSInteger screenHeight = [[UIScreen mainScreen] bounds].size.height;
    [UIView animateWithDuration:0.3f animations:^{
        weakVC.shareView.frame = CGRectMake(weakVC.shareView.frame.origin.x, screenHeight-_shareView.bounds.size.height-(IS_IOS7_LATER ? 0 : 20), weakVC.shareView.bounds.size.width, weakVC.shareView.bounds.size.height);
    }];
}


- (void)hideParentViewControllerTabbar:(UIViewController *)viewController{              // 毛玻璃效果
    self.backgroundImage = [[UIImage screenShoot:viewController.view] applyExtraLightEffect];
}


#pragma mark - 创建上面的view
-(UIView *)createMainView{
    // 1.topWriteView
    self.topWriteView = [[UIView alloc]init];
    self.topWriteView.backgroundColor = [UIColor redColor];
    self.topWriteView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 100);
    
    // 2.创建金币
    self.fixedGoldLabel = [[UILabel alloc]init];
    self.fixedGoldLabel.backgroundColor = [UIColor clearColor];
    self.fixedGoldLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.fixedGoldLabel.textColor = c28;
    self.fixedGoldLabel.text = @"金币";
    self.fixedGoldLabel.frame = CGRectMake(LCFloat(14), (LCFloat(85) - LCFloat(32)) / 2., LCFloat(51), LCFloat(32));
    self.fixedGoldLabel.textAlignment = NSTextAlignmentCenter;
    self.fixedGoldLabel.backgroundColor = c31;
    [self.topWriteView addSubview:self.fixedGoldLabel];
    
    // 3.创建动态的label
    self.dymicGoldLabel = [[UILabel alloc]init];
    self.dymicGoldLabel.backgroundColor = [UIColor clearColor];
    self.dymicGoldLabel.text = @"0";
    self.dymicGoldLabel.textColor = c3;
    self.dymicGoldLabel.font = [[UIFont fontWithCustomerSizeName:@"小正文"]boldFont];
    self.dymicGoldLabel.frame = CGRectMake(kScreenBounds.size.width / 2. - LCFloat(11) - LCFloat(13) - 100, self.fixedGoldLabel.orgin_y, 100, [NSString contentofHeightWithFont:self.dymicGoldLabel.font]);
    self.dymicGoldLabel.center_y = self.fixedGoldLabel.center_y;
    self.dymicGoldLabel.textAlignment = NSTextAlignmentRight;
    [self.topWriteView addSubview:self.dymicGoldLabel];
    
    // 4. 创建竖线
    self.lineView1 = [[UIView alloc]init];
    self.lineView1.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    self.lineView1.frame = CGRectMake(kScreenBounds.size.width / 2. - .5f, 0, 1, LCFloat(85));
    [self.topWriteView addSubview:self.lineView1];
    
    // 5. 创建可能获得的金币
    self.fixedMabeLabel = [[UILabel alloc]init];
    self.fixedMabeLabel.backgroundColor = [UIColor clearColor];
    self.fixedMabeLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.fixedMabeLabel.textColor = c28;
    self.fixedMabeLabel.text = @"可能获得金币";
    CGSize contentOfFixedSize = [@"可能获" sizeWithCalcFont:self.fixedMabeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedMabeLabel.font])];
    self.fixedMabeLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(14) - contentOfFixedSize.width, 0, contentOfFixedSize.width, 2 * [NSString contentofHeightWithFont:self.fixedMabeLabel.font]);
    self.fixedMabeLabel.numberOfLines = 2;
    self.fixedMabeLabel.adjustsFontSizeToFitWidth = YES;
    self.fixedMabeLabel.center_y = self.fixedGoldLabel.center_y;
    [self.topWriteView addSubview:self.fixedMabeLabel];
    
    // 6. 创建动态的金币
    self.dymicMabeLabel = [[UILabel alloc]init];
    self.dymicMabeLabel.backgroundColor = [UIColor clearColor];
    self.dymicMabeLabel.font = [UIFont systemFontOfCustomeSize:60.];
    self.dymicMabeLabel.textColor = c3;
    self.dymicMabeLabel.textAlignment = NSTextAlignmentRight;
    CGFloat width = self.fixedMabeLabel.orgin_x - LCFloat(13) - kScreenBounds.size.width / 2. - LCFloat(13);
    self.dymicMabeLabel.frame = CGRectMake(self.fixedMabeLabel.orgin_x - LCFloat(13) - width, 0, width, [NSString contentofHeightWithFont:self.dymicMabeLabel.font]);
    self.dymicMabeLabel.text = @"0";
    self.dymicMabeLabel.adjustsFontSizeToFitWidth = YES;
    self.dymicMabeLabel.center_y = self.fixedMabeLabel.center_y;
    [self.topWriteView addSubview:self.dymicMabeLabel];
    
    
    // 7.创建线
    self.lineView2 = [[UIView alloc]init];
    self.lineView2.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    self.lineView2.frame = CGRectMake(0, CGRectGetMaxY(self.lineView1.frame), kScreenBounds.size.width, 1);
    [self.topWriteView addSubview:self.lineView2];
    
    // 1. 创建左侧按钮
    self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftBtn.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.leftBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.slider setValue:(--strongSelf.slider.value) animated:YES];
        [strongSelf updateLabelInfo];
    }];
    [self.leftBtn setImage:[UIImage imageNamed:@"icon_snatch_reduce"] forState:UIControlStateNormal];
    self.leftBtn.frame = CGRectMake(LCFloat(17), CGRectGetMaxY(self.lineView2.frame) + LCFloat(31), LCFloat(16), LCFloat(16));
    [self.topWriteView addSubview:self.leftBtn];
    
    // 2. 创建右侧按钮
    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightBtn.backgroundColor = [UIColor clearColor];
    [self.rightBtn setImage:[UIImage imageNamed:@"icon_snatch_add"] forState:UIControlStateNormal];
    self.rightBtn.frame = CGRectMake(kScreenBounds.size.width - LCFloat(17) - LCFloat(16), self.leftBtn.orgin_y, LCFloat(16), LCFloat(16));
    [self.rightBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.slider setValue:(++strongSelf.slider.value) animated:YES];
        [strongSelf updateLabelInfo];
    }];
    [self.topWriteView addSubview:self.rightBtn];
    
    // 8.创建
    self.slider = [[UISlider alloc] init];
    self.slider.minimumValue = 0;
    self.slider.maximumValue = 0;
    if (self.transferTouzhuDetailModel){
        self.slider.maximumValue = MIN(self.transferTouzhuDetailModel.balance / 100, self.transferTouzhuDetailModel.memberMaxStakeGold / 100);
    }
    self.slider.minimumTrackTintColor = c26;
    [self.slider addTarget:self action:@selector(didSlideWithSlider:) forControlEvents:UIControlEventValueChanged];
    [self.topWriteView addSubview:self.slider];
    self.slider.frame = CGRectMake(CGRectGetMaxX(self.leftBtn.frame) + LCFloat(17), 0, self.rightBtn.orgin_x - LCFloat(17) - CGRectGetMaxX(self.leftBtn.frame) - LCFloat(17), LCFloat(30));
    self.slider.center_y = self.rightBtn.center_y;

    
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.fixedLabel.textAlignment = NSTextAlignmentCenter;
    self.fixedLabel.textColor = c28;
    self.fixedLabel.frame =CGRectMake(0,  CGRectGetMaxY(self.slider.frame) + LCFloat(37), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.fixedLabel.font]);
    [self.topWriteView addSubview:self.fixedLabel];
    self.fixedLabel.text = @"奖金按照您竞猜时的赔率兑付";
    
    
    self.topWriteView.size_height = CGRectGetMaxY(self.fixedLabel.frame) + LCFloat(30) ;
    
    return self.topWriteView;
}

-(void)didSlideWithSlider:(UISlider *)sender{
    [self updateLabelInfo];
    
    if (sender.value == 0) {
//        self.dymicGoldLabel.text = slider.value;
    } else {
        
    }
}

-(void)updateLabelInfo{
    // 金币
    self.dymicGoldLabel.text = [NSString stringWithFormat:@"%li",(long)self.slider.value * 100];
    
    // 可能获得
    NSInteger selectedValue = self.slider.value;
    self.dymicMabeLabel.text = [NSString stringWithFormat:@"%li",(long)(selectedValue * self.transferTouzhuDetailModel.oddsOnPoint * 100)];
    if ([self.dymicGoldLabel.text isEqualToString:@"0"]){
        self.dymicMabeLabel.text = @"0";
        [self.slider setValue:0 animated:YES];
    }
}

-(void)touzhuBtnClickManager:(void(^)(NSString *gold))block{
    objc_setAssociatedObject(self, &btnClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setTransferTouzhuDetailModel:(PDTouzhuDetailModel *)transferTouzhuDetailModel{
    _transferTouzhuDetailModel = transferTouzhuDetailModel;
    
    if (self.dymicLabel){
        self.dymicLabel.text = [NSString stringWithFormat:@"%@-%@-赔率%.2f",transferTouzhuDetailModel.jingcaiName, transferTouzhuDetailModel.teamName,transferTouzhuDetailModel.oddsOnPoint];
    }
    if (self.slider){
        // 需要结合用户当前最的金币数额做显示
        CGFloat max;
        if (self.transferTouzhuDetailModel.memberMaxStakeGold == 0) {
            max = self.memberMaximalGold;
        } else  {
            max = MIN(self.transferTouzhuDetailModel.memberMaxStakeGold, self.memberMaximalGold);
        }
        
//        CGFloat max = self.transferTouzhuDetailModel.memberMaxStakeGold == 0 ? 100000000000:self.transferTouzhuDetailModel.memberMaxStakeGold;
        self.slider.maximumValue = MIN(self.transferTouzhuDetailModel.balance / 100, max / 100);
    }
}


-(void)guiderManager{
    if (self.isGuider){
        PDGuideRootViewController *guideRootViewController = [[PDGuideRootViewController alloc]init];
        // 1. 添加消息
        PDGuideSingleModel *remindGuideSingleModel = [[PDGuideSingleModel alloc]init];
        remindGuideSingleModel.text = @"'点击向右滑动到底进行投注吧'";
        remindGuideSingleModel.foundationType = foundationTypeRect;
        UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
        CGRect convertFrame = [self.shareView convertRect:self.topWriteView.frame toView:keyWindow];
        remindGuideSingleModel.transferView = self.topWriteView;
        remindGuideSingleModel.transferShowFrame = convertFrame;
        remindGuideSingleModel.tempSlider = self.slider;
        CGRect slideRect = [self.topWriteView convertRect:self.slider.frame toView:keyWindow];
        remindGuideSingleModel.transferPositionFrame =  slideRect;
        remindGuideSingleModel.guideType = GuideGuesterTypeSwipe;
        remindGuideSingleModel.transferGuideType = GuideTypeLotteryGame;

        // 2. 添加按钮
        PDGuideSingleModel *actionButtonModel = [[PDGuideSingleModel alloc]init];
        actionButtonModel.text = @"'点击确定吧'";
        actionButtonModel.foundationType = foundationTypeCircle;
        CGRect actionButtonFrame = [self.shareView convertRect:self.determineButton.frame toView:keyWindow];
        actionButtonModel.transferShowFrame = actionButtonFrame;
        actionButtonModel.tempActionButton = self.determineButton;
        [guideRootViewController showInView:self.parentViewController withViewActionArr:@[remindGuideSingleModel,actionButtonModel]];
        
        // 添加滑动成功
        __weak typeof(self)weakSelf = self;
        [guideRootViewController lotteryDetailTouzhuManagerBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.topWriteView.frame = CGRectMake(0, CGRectGetMaxY(self.topBlackView.frame), kScreenBounds.size.width, self.topWriteView.size_height);
            [strongSelf.shareView addSubview:strongSelf.topWriteView];
        }];
        
        // 点击确定
        [guideRootViewController lotteryDetailTouzhuActionManagerBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = self;
            [[AccountModel sharedAccountModel]guestSuccessWithType:GuestTypeMatchGuess3 block:NULL];

            void(^block)(NSString *gold) = objc_getAssociatedObject(strongSelf, &btnClickKey);
            if (block){
                block(strongSelf.dymicGoldLabel.text);
            }
            [strongSelf sheetViewDismiss];
        }];
    }
}

@end
