//
//  PDLotteryGameDetailRootViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameDetailRootViewController.h"
#import <HTHorizontalSelectionList.h>                           // segment
#import "PDNewLotteryDetailHeaderView.h"                        // 头部内容

#import "PDLotteryGameDetailInfoViewController.h"               // 详情
#import "PDLotteryGameDetailLotteryViewController.h"            // 竞猜
// Model
#import "PDMatchguessinfoSingleModel.h"                         //
#import "PDBaseSocketRootModel.h"
#import "PDLotteryGameDetailRecordHeaderView.h"
#import "PDNewLotteryDetailBottomView.h"                        // 底部内容
#import "PDCenterLotteryHeroViewController.h"
#import "PDTopUpViewController.h"
#import "PDLotteryHeroChatRoomViewController.h"
#import "PDLotteryGameDetailMainViewController.h"

#define headImgViewHeight LCFloat(160)
#define headViewHeight LCFloat(204)

@interface PDLotteryGameDetailRootViewController ()<HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate,UIScrollViewDelegate,PDNetworkAdapterDelegate,PDLotteryGameDetailLotteryViewControllerDelegate,PDLotteryGameDetailInfoViewControllerDelegate,PDTopUpViewControllerDelegate>{
    PDMatchguessinfoSingleModel *matchRootModel;
    PDMatchguessinfoGuessListSingleModel *guessListSingleModel;
    
}
@property (nonatomic,strong)UIScrollView*mainScrollView;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)NSMutableArray *segmentMutableArr;              /**< segment数组*/

//1.创建下面的内容scrollview
@property (nonatomic,strong)UIScrollView *infoScrollView;                   /**<scrollView*/
//2.创建头部内容
@property (nonatomic,strong)PDNewLotteryDetailHeaderView *headerView;

// 3.创建控制器
@property (nonatomic,strong)PDLotteryGameDetailLotteryViewController *lotteryController;
@property (nonatomic,strong)PDLotteryGameDetailInfoViewController *detailController;
@property (nonatomic,strong)PDLotteryHeroChatRoomViewController *convertsationViewController;

// 4. 跳转到PDNewLotteryDetailBottomView
@property (nonatomic,strong)PDNewLotteryDetailBottomView *bottomView;

@property (nonatomic,strong)UIButton *rightTempBtn;                 /**< 右侧按钮*/
@property (nonatomic,strong)UIButton *leftTempBtn;                  /**< 左侧按钮*/
@end

@implementation PDLotteryGameDetailRootViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createScrollView];
    [self createBottomView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [NetworkAdapter sharedAdapter].delegate = self;
    [self interfaceManager];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self sendRequestToOutDetailPage];
    [self.headerView stopTimer];
}

#pragma mark - InterfaceManager
-(void)interfaceManager{
    // 1. 获取头部信息
    [self sendRequestToGetGameDetail];
    // 2. 获取竞猜列表
    [self sendRequestToGetGameLotteryDetail];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"赛事";
    __weak typeof(self)weakSelf = self;
    self.rightTempBtn = [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_lottery_detail_remind_nor"] barHltImage:[UIImage imageNamed:@"icon_lottery_detail_remind_nor"] action:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        PDLotteryGameDetailMainViewController *newController = [[PDLotteryGameDetailMainViewController alloc]init];
        newController.transferMatchId = self.transferMatchId;
        [strongSelf.navigationController pushViewController:newController animated:YES];
        
        
        
        
        
        return;
        if (strongSelf->matchRootModel.remind){         // 取消
            [strongSelf sendRequestToCancelRawremind];
        } else {                                   // 增加
            [strongSelf sendRequestToAddRawremind];
        }
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.segmentMutableArr = [NSMutableArray array];
    NSArray *segmentTempArr = @[@"竞猜",@"详情",@"聊天"];
    [self.segmentMutableArr addObjectsFromArray:segmentTempArr];
}

#pragma mark - createScrollView
-(void)createScrollView{
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        self.mainScrollView.frame = self.view.bounds;
        self.mainScrollView.delegate = self;
        self.mainScrollView.backgroundColor = [UIColor whiteColor];
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, self.view.size_height + headImgViewHeight);
        self.mainScrollView.pagingEnabled = NO;
        self.mainScrollView.alwaysBounceVertical = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:self.mainScrollView];
    }
    
    // 1. 创建头部内容
    [self createHeaderView];
    // 2. 创建segment
    [self createSegmentList];
    // 3. 创建内容scrollView
    [self createInfoScrollView];
}

#pragma mark - 创建头部内容
-(void)createHeaderView{
    if (!self.headerView){
        self.headerView= [[PDNewLotteryDetailHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, headImgViewHeight)];
        self.headerView.backgroundColor = [UIColor clearColor];
        [self.mainScrollView addSubview:self.headerView];
    }
}

#pragma mark - 创建segment
-(void)createSegmentList{
    if (!self.segmentList){
        // 3. 创建segment
        self.segmentList = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake(0, CGRectGetMaxY(self.headerView.frame), kScreenBounds.size.width, LCFloat(44))];
        self.segmentList.delegate = self;
        self.segmentList.dataSource = self;
        [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
        self.segmentList.selectionIndicatorColor = c15;
        self.segmentList.bottomTrimColor = [UIColor clearColor];
        self.segmentList.isNotScroll = YES;
        [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        self.segmentList.backgroundColor = [UIColor colorWithCustomerName:@"白"];
        [self.mainScrollView addSubview:self.segmentList];
    }
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentMutableArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentMutableArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    if (index == 2){
        if (![[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            [self authorizeWithCompletionHandler:^{
                if ([[AccountModel sharedAccountModel]hasLoggedIn]){            // 已经登录
                    [self.infoScrollView setContentOffset:CGPointMake(kScreenBounds.size.width* index, 0) animated:YES];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self createIMControllerWithIMId:matchRootModel.imGroupId];
                        [self.bottomView sendRequestToGetInfo];
                        [self.mainScrollView setContentOffset:CGPointMake(0, headImgViewHeight) animated:YES];
                    });
                    
                    // 首页进行更新数据
                    [[PDPandaRootViewController sharedController] sendRequestToGetPersonalInfoHasAnimation:NO block:NULL];
                    return ;
                }
            }];
        } else {
            [self.mainScrollView setContentOffset:CGPointMake(0, headImgViewHeight) animated:YES];
            [self.infoScrollView setContentOffset:CGPointMake(kScreenBounds.size.width* index, 0) animated:YES];
        }
        [UIView animateWithDuration:.5f animations:^{
            self.bottomView.orgin_y = kScreenBounds.size.height - 64;
        } completion:^(BOOL finished) {
            self.bottomView.orgin_y = kScreenBounds.size.height - 64;
        }];
    } else {
        [self.infoScrollView setContentOffset:CGPointMake(kScreenBounds.size.width* index, 0) animated:YES];
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            [UIView animateWithDuration:.5f animations:^{
                self.bottomView.orgin_y = kScreenBounds.size.height - 64 - self.bottomView.size_height;
            } completion:^(BOOL finished) {
                self.bottomView.orgin_y = kScreenBounds.size.height - 64 - self.bottomView.size_height;
            }];
        }
        

//        [self.convertsationViewController.messageInputView resignFirstResponder];
    }
}

#pragma mark - 创建内容scrollView
-(void)createInfoScrollView{
    if (!self.infoScrollView){
        self.infoScrollView = [[UIScrollView alloc]init];
        self.infoScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, self.mainScrollView.contentSize.height - headViewHeight - 64);
        self.infoScrollView.delegate = self;
        self.infoScrollView.backgroundColor = [UIColor clearColor];
        self.infoScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 3,self.infoScrollView.size_height + 1);
//        self.infoScrollView.pagingEnabled = YES;
        self.infoScrollView.alwaysBounceVertical = YES;
        self.infoScrollView.scrollEnabled = NO;
   
        self.infoScrollView.showsVerticalScrollIndicator = NO;
        self.infoScrollView.showsHorizontalScrollIndicator = NO;
        [self.mainScrollView addSubview:self.infoScrollView];
    }
    __weak typeof(self)weakSelf = self;
    [self.infoScrollView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }];
    
    [self createController];
}

#pragma mark - 创建view
-(void)createController{
    [self createLotteryController];
    [self createDetailController];
}


#pragma mark - 加载竞猜
- (void)createLotteryController{
    self.lotteryController = [[PDLotteryGameDetailLotteryViewController alloc] init];
    self.lotteryController.hasCancelSocket = YES;
    self.lotteryController.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.infoScrollView.size_height);
    self.lotteryController.delegate = self;
    [self.infoScrollView addSubview:self.lotteryController.view];
    [self addChildViewController:self.lotteryController];
    
    __weak typeof(self)weakSelf = self;
    [self.lotteryController loginSuccessManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            [strongSelf.bottomView sendRequestToGetInfo];
            
            [UIView animateWithDuration:.5f animations:^{
                strongSelf.bottomView.orgin_y = strongSelf.view.size_height  - strongSelf.bottomView.size_height;
            } completion:^(BOOL finished) {
                strongSelf.bottomView.orgin_y = strongSelf.view.size_height  - strongSelf.bottomView.size_height;
            }];
            //1. 修改这里的tableView高度
            strongSelf.lotteryController.lotteryTableView.size_height = strongSelf.infoScrollView.size_height - strongSelf.bottomView.size_height;
            // 2. 修改详情的tableView高度
            strongSelf.detailController.detailTableView.size_height = strongSelf.infoScrollView.size_height - strongSelf.bottomView.size_height;
            
            // 首页进行更新数据
            [[PDPandaRootViewController sharedController] sendRequestToGetPersonalInfoHasAnimation:NO block:NULL];
        }
    }];
}

#pragma mark - 加载详情
-(void)createDetailController {
    self.detailController = [[PDLotteryGameDetailInfoViewController alloc]init];
    self.detailController.view.frame = CGRectMake(kScreenBounds.size.width, 0, kScreenBounds.size.width, self.infoScrollView.size_height);
    [self.infoScrollView addSubview:self.detailController.view];
    [self addChildViewController:self.detailController];
    self.detailController.delegate = self;
    self.detailController.hasCancelSocket = YES;
}

#pragma mark - 创建背景的view
-(void)createBottomView{
    if (!self.bottomView){
        self.bottomView = [[PDNewLotteryDetailBottomView alloc]initWithFrame:CGRectMake(0, self.view.size_height - 64, kScreenBounds.size.width, LCFloat(50))];
        self.bottomView.backgroundColor = [UIColor whiteColor];
        self.bottomView.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
        self.bottomView.layer.shadowOpacity = 2.0;
        self.bottomView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
        __weak typeof(self)weakSelf = self;
        [self.bottomView directToMyLottery:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDCenterLotteryHeroViewController *centerLotteryViewController = [[PDCenterLotteryHeroViewController alloc]init];
            centerLotteryViewController.hasCancelSocket = YES;
            centerLotteryViewController.selIndex = 1;
            [strongSelf.navigationController pushViewController:centerLotteryViewController animated:YES];
        }];
        
        [self.bottomView directToRecharge:^{            // 充值
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDTopUpViewController *topupViewController = [[PDTopUpViewController alloc]init];
            topupViewController.delegate = self;
            [strongSelf.navigationController pushViewController:topupViewController animated:YES];
        }];
        [self.view addSubview:self.bottomView];
        [self bottonRequestManager];
    }
}

-(void)bottonRequestManager{
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        [self.bottomView sendRequestToGetInfo];
        
        [UIView animateWithDuration:.5f animations:^{
            self.bottomView.orgin_y = self.view.size_height  - self.bottomView.size_height;
        } completion:^(BOOL finished) {
            self.bottomView.orgin_y = self.view.size_height  - self.bottomView.size_height;
        }];
        //1. 修改这里的tableView高度
        self.lotteryController.lotteryTableView.size_height = self.infoScrollView.size_height - self.bottomView.size_height;
        // 2. 修改详情的tableView高度
        self.detailController.detailTableView.size_height = self.infoScrollView.size_height - self.bottomView.size_height;
    }
}


-(void)createIMControllerWithIMId:(NSString *)imId{
    if(!self.convertsationViewController){
        if (![[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            return;
        }
        PDEMMessageViewController *chatRoomViewController = [[PDEMManager sharedInstance] conversationWithChatroom:imId];
        if(chatRoomViewController == nil){
            return;
        }
    
        chatRoomViewController.view.backgroundColor = UURandomColor;
        chatRoomViewController.view.frame = CGRectMake(kScreenBounds.size.width * 2, 0, kScreenBounds.size.width, self.infoScrollView.size_height);
        [self addChildViewController:chatRoomViewController];
        [self.infoScrollView addSubview:chatRoomViewController.view];
    }
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    if (scrollView == self.mainScrollView){
        if (self.mainScrollView.contentOffset.y >= headImgViewHeight){
            self.mainScrollView.scrollEnabled = NO;
        } else {
            self.mainScrollView.scrollEnabled = YES;
        }
    } else if (scrollView == self.infoScrollView){
        
    }
}



#pragma mark - 接口
#pragma mark - 接口 - 进入赛事猜获取信息
-(void)sendRequestToGetGameLotteryDetail{
    NSDictionary *params = @{@"token":[AccountModel sharedAccountModel].token,@"type":@"matchguessdetail",@"matchId":self.transferMatchId};
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:params socket:[NetworkAdapter sharedAdapter].loginSocket];
}

#pragma mark - 接口- 退出赛事猜
-(void)sendRequestToOutDetailPage{
    NSDictionary *params = @{@"token":[AccountModel sharedAccountModel].token,@"type":@"matchguessquitdetail",@"matchId":self.transferMatchId};
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:params socket:[NetworkAdapter sharedAdapter].loginSocket];
}

-(void)socketDidBackData:(id)responseObject{
    PDBaseSocketRootModel *responseObjectModel = [[PDBaseSocketRootModel  alloc] initWithJSONDict:[responseObject objectForKey:@"data"]];
    if (responseObjectModel.type == socketType950){
        guessListSingleModel = responseObjectModel.items;
        
        // 1. 【竞猜】
        if (!self.lotteryController.lotteryMutableArr){
            self.lotteryController.lotteryMutableArr = [NSMutableArray array];
        } else {
            [self.lotteryController.lotteryMutableArr removeAllObjects];
        }
        [self.lotteryController.lotteryMutableArr addObjectsFromArray:guessListSingleModel.guessList];
        [self.lotteryController.lotteryTableView reloadData];
        
        // 2. 【竞猜记录】
        if (!self.detailController.detailMutableArr){
            self.detailController.detailMutableArr = [NSMutableArray array];
        } else {
            [self.detailController.detailMutableArr removeAllObjects];
        }
        [self.detailController.detailMutableArr addObjectsFromArray:guessListSingleModel.stakeList];
        self.detailController.transferGuessMainSingleModel = guessListSingleModel;
        if (self.detailController.detailTableView){
            [self.detailController.detailTableView reloadData];
        }
        
        // 3. 修改分数
        [self.headerView changeScoreWithStr:[NSString stringWithFormat:@"%li:%li",guessListSingleModel.teamAScore,guessListSingleModel.teamBScore]];
        // 4.
        [self.headerView changeJoinWithStr:[NSString stringWithFormat:@"%li人参与，总投注%li",guessListSingleModel.totalJoinCount,guessListSingleModel.totalGoldCount]];
        
        // 5. 增加引导
        if ([AccountModel sharedAccountModel].matchGuess2 == NO){
            [self addGestureManager];
        }
    }
}

#pragma mark - 获取当前赛事详情
-(void)sendRequestToGetGameDetail{
    if (!self.transferMatchId){
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:matchguessinfo requestParams:@{@"matchId":self.transferMatchId} responseObjectClass:[PDMatchguessinfoSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->matchRootModel = (PDMatchguessinfoSingleModel *)responseObject;
        // 1. 创建回话
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            [strongSelf createIMControllerWithIMId:strongSelf->matchRootModel.imGroupId];
        }
        // 2. 添加头部内容
        strongSelf.headerView.transferRootModel = strongSelf->matchRootModel;
        // 3. 传递比赛信息
        strongSelf.detailController.transferMatchSingleModel = strongSelf ->matchRootModel;
        
        // 4. 修改barMainTitle
        strongSelf.barMainTitle = [NSString stringWithFormat:@"%@ vs %@",strongSelf->matchRootModel.teamLeft.teamName,strongSelf->matchRootModel.teamRight.teamName];
        
        // 5. 开始时间
        strongSelf->matchRootModel.gameStartTempTime = [NSDate getNSTimeIntervalWithCurrent] + strongSelf->matchRootModel.matchLeftTime;
    }];
}

#pragma mark 添加开奖提醒
-(void)sendRequestToAddRawremind{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:drawremind requestParams:@{@"id":self.transferMatchId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [PDHUD showShimmeringString:@"添加开奖提醒"];
            strongSelf -> matchRootModel.remind = YES;
            [strongSelf remindBtnStatusChange];
        }
    }];
}

#pragma mark 取消开奖提醒
-(void)sendRequestToCancelRawremind{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:removedrawremind requestParams:@{@"id":self.transferMatchId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [PDHUD showShimmeringString:@"取消开奖提醒"];
            strongSelf -> matchRootModel.remind = NO;
            [strongSelf remindBtnStatusChange];
        }
    }];
}

#pragma mark - OtherManager
-(void)remindBtnStatusChange{    // 1.
    [Tool clickZanWithView:self.rightTempBtn block:^{
        if (matchRootModel.remind){          // 点开
            [self.rightTempBtn setImage:[UIImage imageNamed:@"icon_lottery_detail_remind_sel"] forState:UIControlStateNormal];
        } else {
            [self.rightTempBtn setImage:[UIImage imageNamed:@"icon_lottery_detail_remind_nor"] forState:UIControlStateNormal];
        }
    }];
}


-(void)scrollViewDelegateWithDetailManager:(UIScrollView *)scrollView andDrop:(BOOL)drop{
    if(drop){
        [self.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    } else {
        [self.mainScrollView setContentOffset:CGPointMake(0, headImgViewHeight) animated:YES];
    }
}

-(void)scrollViewDelegateManager:(UIScrollView *)scrollView andDrop:(BOOL)drop{
    if(drop){
        [self.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    } else {
        [self.mainScrollView setContentOffset:CGPointMake(0, headImgViewHeight) animated:YES];
    }
}

#pragma mark - 新手引导
-(void)addGestureManager{
    return;
    PDGuideRootViewController *guideRootViewController = [[PDGuideRootViewController alloc]init];
    // 1. 添加消息
    PDGuideSingleModel *remindGuideSingleModel = [[PDGuideSingleModel alloc]init];
    remindGuideSingleModel.text = @"'点击这里可添加竞猜提醒哟'";
    remindGuideSingleModel.foundationType = foundationTypeCircle;
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    CGRect convertFrame = [self.navigationController.navigationBar convertRect:self.rightTempBtn.frame toView:keyWindow];
    remindGuideSingleModel.transferShowFrame = CGRectMake(convertFrame.origin.x - LCFloat(11), convertFrame.origin.y - LCFloat(11), convertFrame.size.width + 2 * LCFloat(11), convertFrame.size.height + 2 * LCFloat(11));
    
    // 2. 添加聊天
    PDGuideSingleModel *messageGuideSingleModel = [[PDGuideSingleModel alloc]init];
    messageGuideSingleModel.text = @"'这里可以和别的小伙伴讨论赛事哟'";
    messageGuideSingleModel.foundationType = foundationTypeCircle;
    messageGuideSingleModel.transferShowFrame = CGRectMake(kScreenBounds.size.width - self.segmentList.size_width / 3., self.segmentList.orgin_y + 64, self.segmentList.size_width / 3. - LCFloat(11), self.segmentList.size_height);
    
    // 3. 添加竞猜
    PDGuideSingleModel *shengfuSingleModel = [[PDGuideSingleModel alloc]init];
    shengfuSingleModel.text = @"这个竞猜投左边简直不要太稳了";
    
    PDNewDetailLotteryCellTableViewCell *cell = (PDNewDetailLotteryCellTableViewCell *)[self.lotteryController.lotteryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    CGRect lotteryRect = [self.lotteryController.lotteryTableView convertRect:cell.frame toView:keyWindow];
    shengfuSingleModel.foundationType = foundationTypeRect;
    shengfuSingleModel.transferShowFrame = lotteryRect;
    CGRect lotteryPosationRect = [cell convertRect:cell.leftButton.frame toView:keyWindow];
    shengfuSingleModel.transferPositionFrame = lotteryPosationRect;
    shengfuSingleModel.guideType = GuideGuesterTypeTap;
    
    // show
    // 详情
    [guideRootViewController lotteryDetailManagerBlock:^{
        
    }];
    [guideRootViewController showInView:self.parentViewController withViewActionArr:@[remindGuideSingleModel,messageGuideSingleModel,shengfuSingleModel]];
}

#pragma mark - 充值成功
- (void)goldExchangeFinish:(PDTopUpViewController *)topupViewController{
    [self.bottomView sendRequestToGetInfo];
}

@end
