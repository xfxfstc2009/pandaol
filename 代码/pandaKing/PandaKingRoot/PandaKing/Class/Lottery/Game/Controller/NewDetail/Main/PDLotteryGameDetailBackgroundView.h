//
//  PDLotteryGameDetailBackgroundView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/17.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDScrollView.h"

typedef NS_ENUM(NSInteger,BackgroundPageType) {
    BackgroundPageTypeBackgroundImage,
    BackgroundPageTypeBanner,
};

@interface PDLotteryGameDetailBackgroundView : UIView

@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)PDScrollView *headerScrollView;



#pragma mark -初始化
@property (nonatomic,assign)CGFloat transferBackgroundImgHeight;                /**< 背景图片的高度*/
@property (nonatomic,strong)UIImage *transferBackgroundImg;                     /**< 背景图片*/
@property (nonatomic,copy)NSString *transferBackgroundUrl;                      /**< 传递的背景图片url*/

@property (nonatomic,assign)CGFloat transferHeaderHeight;                       /**< 顶部的高度*/

// 传过来的nav
@property (nonatomic,strong)UIView *transferNavView;                            /**< 传递过来的navBar*/

@property (nonatomic,assign)BackgroundPageType transferBackgroundType;          /**< 类型*/

@property (nonatomic,assign)BOOL isShowNavAnimation;

-(instancetype)initWithFrame:(CGRect)frame pageType:(BackgroundPageType)pageType;

// 【NavigationBar 动画】
-(void)animateNavigationBar:(CGPoint)currentPoint maxHeight:(CGFloat)maxHeight;
@end
