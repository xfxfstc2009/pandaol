//
//  PDLotteryGameDetailMainViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDLotteryGameDetailMainViewController.h"
#import "PDLotteryGameDetailBackgroundView.h"
#import "PDNewLotteryDetailHeaderView.h"
#import "HTHorizontalSelectionList.h"
#import "PDLotteryGameDetailLotteryViewController.h"
#import "PDLotteryHeroChatRoomViewController.h"
#import "PDLotteryGameDetailInfoViewController.h"
#import "PDNewLotteryDetailBottomView.h"
#import "PDCenterLotteryHeroViewController.h"
#import "PDTopUpViewController.h"

#define kHeader_Height LCFloat(250.)
#define kTeamIcon_Max_Size LCFloat(30)

typedef NS_ENUM(NSInteger,navBarAnimationType) {
    navBarAnimationNo = 0,                      /**< 不动画*/
    navBarAnimationYes,                         /**< 动画*/
    navBarAnimationWaiting,                     /**< 等待*/
};

@interface PDLotteryGameDetailMainViewController()<HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate,PDLotteryGameDetailLotteryViewControllerDelegate,PDLotteryGameDetailInfoViewControllerDelegate,UIScrollViewDelegate,PDNetworkAdapterDelegate,PDTopUpViewControllerDelegate,UIGestureRecognizerDelegate>{
    PDMatchguessinfoSingleModel *matchRootModel;
    PDMatchguessinfoGuessListSingleModel *guessListSingleModel;
    
    CGRect leftConvertTempRect;
    CGRect rightConvertTempRect;
    navBarAnimationType navBarAnimation;
}

@property (nonatomic,strong)PDLotteryGameDetailBackgroundView *lotteryGameDetailBackgroundView;
@property (nonatomic,strong)PDNewLotteryDetailHeaderView *headerView;
@property (nonatomic,strong)UIView *navigationView;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)NSMutableArray *segmentMutableArr;

@property (nonatomic,strong)UIScrollView *infoScrollView;
@property (nonatomic,strong)PDLotteryGameDetailLotteryViewController *lotteryController;
@property (nonatomic,strong)PDLotteryGameDetailInfoViewController *detailController;
@property (nonatomic,strong)PDLotteryHeroChatRoomViewController *convertsationViewController;

// 4. 跳转到PDNewLotteryDetailBottomView
@property (nonatomic,strong)PDNewLotteryDetailBottomView *bottomView;

// nav
@property (nonatomic,strong)UIButton *tempRightButton;
@property (nonatomic,strong)UIButton *tempLeftButton;
@property (nonatomic,strong)UIButton *navLeftBackButton;
@property (nonatomic,strong)UIButton *navRightMsgButton;

@property (nonatomic,strong)LTMorphingLabel *teamLeftTitleLabel;                    // 队伍左边
@property (nonatomic,strong)LTMorphingLabel *baraMainTitleLabel;
@property (nonatomic,strong)LTMorphingLabel *teamRightTitleLabel;                   // 队伍右边

@property (nonatomic,strong)LTMorphingLabel *gameTimeLabel;                         // 赛事时间
@property (nonatomic,strong)LTMorphingLabel *gameStatusLabel;                       // 状态类别


@property (nonatomic,strong)PDImageView *leftIconImgView;
@property (nonatomic,strong)PDImageView *rightIconImgView;

@property (nonatomic,strong)UILabel *barTitleLabel;

@end

@implementation PDLotteryGameDetailMainViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
    [NetworkAdapter sharedAdapter].delegate = self;
}


-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createView];
    [self createMenuView];
    [self createBottomView];
    [self interfaceManager];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [NetworkAdapter sharedAdapter].delegate = self;
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self sendRequestToOutDetailPage];
    [self.headerView stopTimer];
}

#pragma mark - InterfaceManager
-(void)interfaceManager{
    // 1. 获取头部信息
    [self sendRequestToGetGameDetail];
    // 2. 获取竞猜列表
    [self sendRequestToGetGameLotteryDetail];
}

#pragma mark - pageSetting
-(void)pageSetting{

}

#pragma mark - createMenuView
-(void)createMenuView{
    self.tempLeftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.tempLeftButton.frame = CGRectMake(LCFloat(11), 20, 20, 44);
    [self.tempLeftButton setImage:[UIImage imageNamed:@"icon_main_back"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.tempLeftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.view addSubview:self.tempLeftButton];
    
    self.tempRightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.tempRightButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - 20, 20, 20, 44);
    [self.tempRightButton setImage:[UIImage imageNamed:@"icon_lottery_detail_remind_nor"] forState:UIControlStateNormal];
    [self.tempRightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf->matchRootModel.remind){         // 取消
            [strongSelf sendRequestToCancelRawremind];
        } else {                                   // 增加
            [strongSelf sendRequestToAddRawremind];
        }
    }];
    [self.view addSubview:self.tempRightButton];
    
    //
    self.barTitleLabel = [[UILabel alloc]init];
    self.barTitleLabel.textAlignment = NSTextAlignmentCenter;
    self.barTitleLabel.frame = CGRectMake(CGRectGetMaxX(self.tempLeftButton.frame), 20, self.tempRightButton.orgin_x - CGRectGetMaxX(self.tempLeftButton.frame), 44);
    self.barTitleLabel.alpha = 1;
    self.barTitleLabel.textColor = [UIColor whiteColor];
    [self.view addSubview:self.barTitleLabel];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.segmentMutableArr = [NSMutableArray array];
    NSArray *segmentTempArr = @[@"竞猜",@"详情",@"聊天"];
    [self.segmentMutableArr addObjectsFromArray:segmentTempArr];
}

#pragma mark - createView
-(void)createView{
    self.lotteryGameDetailBackgroundView = [[PDLotteryGameDetailBackgroundView alloc]initWithFrame:kScreenBounds];
    self.lotteryGameDetailBackgroundView.transferBackgroundImg = [UIImage imageNamed:@"panda_rootBgView"];
    self.lotteryGameDetailBackgroundView.transferHeaderHeight = kHeader_Height;
    [self.view addSubview:self.lotteryGameDetailBackgroundView];
    self.lotteryGameDetailBackgroundView.transferNavView = [self createNavBar];
    self.lotteryGameDetailBackgroundView.isShowNavAnimation = YES;
    self.lotteryGameDetailBackgroundView.mainScrollView.delegate = self;

    [self createHeaderView];
    [self createSegmentList];
}

#pragma mark - 创建头部内容
-(void)createHeaderView{
    if (!self.headerView){
        self.headerView= [[PDNewLotteryDetailHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kHeader_Height)];
        self.headerView.backgroundColor = [UIColor clearColor];
        [self.lotteryGameDetailBackgroundView.mainScrollView addSubview:self.headerView];
    }
    [self createLotteryIconImgView];
}

#pragma mark  创建navBar
-(UIView *)createNavBar{
    if (!self.navigationView){
        self.navigationView = [[UIView alloc]init];
        self.navigationView.backgroundColor = [UIColor clearColor];
        self.navigationView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 64);
        [self.view addSubview:self.navigationView];
        
        // 2. 创建消息按钮
        self.navLeftBackButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.navLeftBackButton setImage:[UIImage imageNamed:@"icon_main_back"] forState:UIControlStateNormal];
        self.navLeftBackButton.backgroundColor = UURandomColor;
        __weak typeof(self)weakSelf = self;
        [self.navLeftBackButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }];
        self.navLeftBackButton.frame = CGRectMake(LCFloat(11), 20, self.tempLeftButton.size_width, 44);
        [self.navigationView addSubview:self.navLeftBackButton];
        
        // 3. 创建PandaOL
        self.navRightMsgButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.navRightMsgButton setImage:[UIImage imageNamed:@"icon_lottery_detail_remind_nor"] forState:UIControlStateNormal];
//        self.navRightMsgButton.backgroundColor = [UIColor redColor];
        self.navRightMsgButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) -  self.tempRightButton.size_width, 20, self.tempRightButton.size_width, 44);
        [self.navigationView addSubview:self.navRightMsgButton];
        
        [self.navRightMsgButton buttonWithBlock:^(UIButton *button) {
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (strongSelf->matchRootModel.remind){         // 取消
                [strongSelf sendRequestToCancelRawremind];
            } else {                                   // 增加
                [strongSelf sendRequestToAddRawremind];
            }
        }];
        
        // 4.barMainTitle
        self.baraMainTitleLabel = [[LTMorphingLabel alloc]init];
        self.baraMainTitleLabel.backgroundColor = [UIColor clearColor];
        self.baraMainTitleLabel.textColor = [UIColor whiteColor];
        [self.navigationView addSubview:self.baraMainTitleLabel];
        self.baraMainTitleLabel.morphingEffect = LTMorphingEffectAnvil;
        self.baraMainTitleLabel.font = BAR_MAIN_TITLE_FONT;
        self.baraMainTitleLabel.text = @"";
        NSString *barMainInfo = @" : ";
        [self.navigationView addSubview:self.baraMainTitleLabel];
        CGSize barMainTitleSize = [barMainInfo sizeWithCalcFont:self.baraMainTitleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.baraMainTitleLabel.font])];
        self.baraMainTitleLabel.frame = CGRectMake((kScreenBounds.size.width - barMainTitleSize.width) / 2., 20, barMainTitleSize.width, 44);
        
        // 5. teamLeft
        self.teamLeftTitleLabel = [[LTMorphingLabel alloc]init];
        self.teamLeftTitleLabel.backgroundColor = [UIColor clearColor];
        self.teamLeftTitleLabel.textColor = [UIColor whiteColor];
        [self.navigationView addSubview:self.teamLeftTitleLabel];
        self.teamLeftTitleLabel.morphingEffect = LTMorphingEffectScale;
        self.teamLeftTitleLabel.font = BAR_MAIN_TITLE_FONT;
        self.teamLeftTitleLabel.text = @"";
        

        // 6. teamRight
        self.teamRightTitleLabel = [[LTMorphingLabel alloc]init];
        self.teamRightTitleLabel.backgroundColor = [UIColor clearColor];
        self.teamRightTitleLabel.textColor = [UIColor whiteColor];
        [self.navigationView addSubview:self.teamRightTitleLabel];
        self.teamRightTitleLabel.font = BAR_MAIN_TITLE_FONT;
        self.teamRightTitleLabel.morphingEffect = LTMorphingEffectScale;
        self.teamRightTitleLabel.text = @"";
        
        // 7.gameTimeLabel
        self.gameTimeLabel = [[LTMorphingLabel alloc]init];
        self.gameTimeLabel.backgroundColor = [UIColor clearColor];
        self.gameTimeLabel.textColor = [UIColor whiteColor];
        [self.navigationView addSubview:self.gameTimeLabel];
        self.gameTimeLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
        self.gameTimeLabel.morphingEffect = LTMorphingEffectScale;
        self.gameTimeLabel.text = @"";
        
        // 8.statusLabel
        self.gameStatusLabel = [[LTMorphingLabel alloc]init];
        self.gameStatusLabel.backgroundColor = [UIColor clearColor];
        self.gameStatusLabel.textColor = [UIColor whiteColor];
        [self.navigationView addSubview:self.gameStatusLabel];
        self.gameStatusLabel.font = BAR_MAIN_TITLE_FONT;
        self.gameStatusLabel.morphingEffect = LTMorphingEffectBurn;
        self.gameStatusLabel.textAlignment = NSTextAlignmentCenter;
        self.gameStatusLabel.adjustsFontSizeToFitWidth = YES;
        self.gameStatusLabel.text = @"";
        
    }
    return self.navigationView;
}

#pragma mark - createSegment
-(void)createSegmentList{
    if (!self.segmentList){
        // 3. 创建segment
        self.segmentList = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake(0, kHeader_Height, kScreenBounds.size.width, LCFloat(44))];
        self.segmentList.delegate = self;
        self.segmentList.dataSource = self;
        [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
        self.segmentList.selectionIndicatorColor = c15;
        self.segmentList.bottomTrimColor = [UIColor clearColor];
        self.segmentList.isNotScroll = YES;
        [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        self.segmentList.backgroundColor = [UIColor colorWithCustomerName:@"白"];
        [self.view addSubview:self.segmentList];
    }
    
    [self createInfoScrollView];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentMutableArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentMutableArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self segmentSelectedManager:index];
}

-(void)segmentSelectedManager:(NSInteger)index{
    if (index == 2){
        if (![[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            [self authorizeWithCompletionHandler:^{
                if ([[AccountModel sharedAccountModel]hasLoggedIn]){            // 已经登录
                    [self.infoScrollView setContentOffset:CGPointMake(kScreenBounds.size.width* index, 0) animated:YES];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [self createIMControllerWithIMId:matchRootModel.imGroupId];
                        [self.bottomView sendRequestToGetInfo];
                        [self.lotteryGameDetailBackgroundView.mainScrollView setContentOffset:CGPointMake(0, kHeader_Height) animated:YES];
                    });
                    
                    // 首页进行更新数据
                    [[PDPandaRootViewController sharedController] sendRequestToGetPersonalInfoHasAnimation:NO block:NULL];
                    return ;
                }
            }];
        } else {
            [self.lotteryGameDetailBackgroundView.mainScrollView setContentOffset:CGPointMake(0, kHeader_Height) animated:YES];
            [self.infoScrollView setContentOffset:CGPointMake(kScreenBounds.size.width* index, 0) animated:YES];
        }
        [UIView animateWithDuration:.5f animations:^{
            self.bottomView.orgin_y = kScreenBounds.size.height;
        } completion:^(BOOL finished) {
            self.bottomView.orgin_y = kScreenBounds.size.height;
        }];
    } else {
        [self.infoScrollView setContentOffset:CGPointMake(kScreenBounds.size.width* index, 0) animated:YES];
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            [UIView animateWithDuration:.5f animations:^{
                self.bottomView.orgin_y = kScreenBounds.size.height  - self.bottomView.size_height;
            } completion:^(BOOL finished) {
                self.bottomView.orgin_y = kScreenBounds.size.height  - self.bottomView.size_height;
            }];
        }
        
        [self.convertsationViewController releaseKeyboard];
    }

}

#pragma mark - createDetailView

-(void)createInfoScrollView{
    if (!self.infoScrollView){
        self.infoScrollView = [[UIScrollView alloc]init];
        self.infoScrollView.frame = CGRectMake(0, kHeader_Height + self.segmentList.size_height, kScreenBounds.size.width, kScreenBounds.size.height - self.navigationView.size_height - self.segmentList.size_height);
        self.infoScrollView.delegate = self;
        self.infoScrollView.backgroundColor = [UIColor clearColor];
        self.infoScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 3,self.infoScrollView.size_height);
        self.infoScrollView.alwaysBounceVertical = YES;
        self.infoScrollView.scrollEnabled = YES;
        self.infoScrollView.pagingEnabled = YES;
        self.infoScrollView.userInteractionEnabled = NO;
        
        self.infoScrollView.showsVerticalScrollIndicator = NO;
        self.infoScrollView.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:self.infoScrollView];
    }
    __weak typeof(self)weakSelf = self;
    [self.infoScrollView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.lotteryGameDetailBackgroundView.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }];
    
    [self createLotteryController];
    [self createDetailController];
}

#pragma mark - createLotteryIconImgView
-(void)createLotteryIconImgView{
    self.leftIconImgView = [[PDImageView alloc]init];
    self.leftIconImgView.backgroundColor = [UIColor clearColor];
    self.leftIconImgView.frame = CGRectMake(0, 0, LCFloat(60), LCFloat(60));
    [self.view addSubview:self.leftIconImgView];
    
    self.rightIconImgView = [[PDImageView alloc]init];
    self.rightIconImgView.backgroundColor = [UIColor clearColor];
    self.rightIconImgView.frame = CGRectMake(0, 0, LCFloat(60), LCFloat(60));
    [self.view addSubview:self.rightIconImgView];
}



#pragma mark - 加载竞猜
- (void)createLotteryController{
    self.lotteryController = [[PDLotteryGameDetailLotteryViewController alloc] init];
    self.lotteryController.hasCancelSocket = YES;
    self.lotteryController.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.infoScrollView.size_height);
    self.lotteryController.delegate = self;
    [self.infoScrollView addSubview:self.lotteryController.view];
    [self addChildViewController:self.lotteryController];
    
    __weak typeof(self)weakSelf = self;
    [self.lotteryController loginSuccessManagerBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            if (!strongSelf.bottomView.nickName.text.length){
                [strongSelf.bottomView sendRequestToGetInfo];
            }
            
            [UIView animateWithDuration:.5f animations:^{
                strongSelf.bottomView.orgin_y = strongSelf.view.size_height  - strongSelf.bottomView.size_height;
            } completion:^(BOOL finished) {
                strongSelf.bottomView.orgin_y = strongSelf.view.size_height  - strongSelf.bottomView.size_height;
            }];
            //1. 修改这里的tableView高度
            strongSelf.lotteryController.lotteryTableView.size_height = strongSelf.infoScrollView.size_height - strongSelf.bottomView.size_height;
            // 2. 修改详情的tableView高度
            strongSelf.detailController.detailTableView.size_height = strongSelf.infoScrollView.size_height - strongSelf.bottomView.size_height;
            
            // 首页进行更新数据
#warning    // 首页进行更新数据 忘记为什么要这样写了。暂时先隐藏。
//            [[PDPandaRootViewController sharedController] sendRequestToGetPersonalInfoHasAnimation:NO block:NULL];
        }
    }];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(handleSingleTap:)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

#pragma mark - 加载详情
-(void)createDetailController {
    self.detailController = [[PDLotteryGameDetailInfoViewController alloc]init];
    self.detailController.view.frame = CGRectMake(kScreenBounds.size.width, 0, kScreenBounds.size.width, self.infoScrollView.size_height);
    [self.infoScrollView addSubview:self.detailController.view];
    [self addChildViewController:self.detailController];
    self.detailController.delegate = self;
    self.detailController.hasCancelSocket = YES;
}

#pragma mark - 创建背景的view
-(void)createBottomView{
    if (!self.bottomView){
        self.bottomView = [[PDNewLotteryDetailBottomView alloc]initWithFrame:CGRectMake(0, self.view.size_height - 64, kScreenBounds.size.width, LCFloat(50))];
        self.bottomView.backgroundColor = [UIColor whiteColor];
        self.bottomView.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
        self.bottomView.layer.shadowOpacity = 2.0;
        self.bottomView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
        __weak typeof(self)weakSelf = self;
        [self.bottomView directToMyLottery:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDCenterLotteryHeroViewController *centerLotteryViewController = [[PDCenterLotteryHeroViewController alloc]init];
            centerLotteryViewController.hasCancelSocket = YES;
            centerLotteryViewController.selIndex = 1;
            [strongSelf.navigationController pushViewController:centerLotteryViewController animated:YES];
        }];
        
        [self.bottomView directToRecharge:^{            // 充值
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDTopUpViewController *topupViewController = [[PDTopUpViewController alloc]init];
            topupViewController.delegate = self;
            [strongSelf.navigationController pushViewController:topupViewController animated:YES];
        }];
        [self.view addSubview:self.bottomView];
        [self bottonRequestManager];
    }
}

-(void)bottonRequestManager{
    if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
        [self.bottomView sendRequestToGetInfo];
        
        [UIView animateWithDuration:.5f animations:^{
            self.bottomView.orgin_y = self.view.size_height  - self.bottomView.size_height;
        } completion:^(BOOL finished) {
            self.bottomView.orgin_y = self.view.size_height  - self.bottomView.size_height;
        }];
        //1. 修改这里的tableView高度
        self.lotteryController.lotteryTableView.size_height = self.infoScrollView.size_height - self.bottomView.size_height;
        // 2. 修改详情的tableView高度
        self.detailController.detailTableView.size_height = self.infoScrollView.size_height - self.bottomView.size_height;
    } else {
        self.bottomView.orgin_y = self.view.size_height;
    }
}


-(void)createIMControllerWithIMId:(NSString *)imId{
    if(!self.convertsationViewController){
        if (![[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            return;
        }
        PDEMMessageViewController *chatRoomViewController = [[PDEMManager sharedInstance] conversationWithChatroom:imId];
        if(chatRoomViewController == nil){
            return;
        }
        
        chatRoomViewController.view.backgroundColor = UURandomColor;
        chatRoomViewController.view.frame = CGRectMake(kScreenBounds.size.width * 2, 0, kScreenBounds.size.width, self.infoScrollView.size_height);
        [self addChildViewController:chatRoomViewController];
        [self.infoScrollView addSubview:chatRoomViewController.view];
    }
}

#pragma mark - 接口
#pragma mark - 接口 - 进入赛事猜获取信息
-(void)sendRequestToGetGameLotteryDetail{
    NSDictionary *params = @{@"token":[AccountModel sharedAccountModel].token,@"type":@"matchguessdetail",@"matchId":self.transferMatchId};
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:params socket:[NetworkAdapter sharedAdapter].loginSocket];
}

#pragma mark - 接口- 退出赛事猜
-(void)sendRequestToOutDetailPage{
    NSDictionary *params = @{@"token":[AccountModel sharedAccountModel].token,@"type":@"matchguessquitdetail",@"matchId":self.transferMatchId};
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:params socket:[NetworkAdapter sharedAdapter].loginSocket];
}

-(void)socketDidBackData:(id)responseObject{
    PDBaseSocketRootModel *responseObjectModel = [[PDBaseSocketRootModel  alloc] initWithJSONDict:[responseObject objectForKey:@"data"]];
    if (responseObjectModel.type == socketType950){
        guessListSingleModel = responseObjectModel.items;
        
        // 1. 【竞猜】
        if (!self.lotteryController.lotteryMutableArr){
            self.lotteryController.lotteryMutableArr = [NSMutableArray array];
        } else {
            [self.lotteryController.lotteryMutableArr removeAllObjects];
        }
        [self.lotteryController.lotteryMutableArr addObjectsFromArray:guessListSingleModel.guessList];
        [self.lotteryController.lotteryTableView reloadData];
        
        // 2. 【竞猜记录】
        if (!self.detailController.detailMutableArr){
            self.detailController.detailMutableArr = [NSMutableArray array];
        } else {
            [self.detailController.detailMutableArr removeAllObjects];
        }
        [self.detailController.detailMutableArr addObjectsFromArray:guessListSingleModel.stakeList];
        self.detailController.transferGuessMainSingleModel = guessListSingleModel;
        if (self.detailController.detailTableView){
            [self.detailController.detailTableView reloadData];
        }
        
        // 3. 修改分数
        [self.headerView changeScoreWithStr:[NSString stringWithFormat:@"%li:%li",(long)guessListSingleModel.teamAScore,guessListSingleModel.teamBScore]];
        // 4.
        [self.headerView changeJoinWithStr:[NSString stringWithFormat:@"%li人参与，总投注%@",guessListSingleModel.totalJoinCount,[Tool transferWithInfoNumber:guessListSingleModel.totalGoldCount]]];
        
        // 5. 增加引导
        if ([AccountModel sharedAccountModel].matchGuess2 == NO){
            [self addGestureManager];
        }
        
        // 6 .修改比分
        
        // 5. 计算宽度
        NSString *teamLeftStr = [NSString stringWithFormat:@"%li",(long)guessListSingleModel.teamAScore];
        CGSize teamLeftSize = [teamLeftStr sizeWithCalcFont:self.teamLeftTitleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.teamLeftTitleLabel.font])];
        self.teamLeftTitleLabel.frame = CGRectMake(self.baraMainTitleLabel.orgin_x - teamLeftSize.width, 20, teamLeftSize.width, 44);
        
        // 6. 计算宽度2
        NSString *teamRightStr = [NSString stringWithFormat:@"%li",guessListSingleModel.teamBScore];
        CGSize teamRightSize = [teamRightStr sizeWithCalcFont:self.teamRightTitleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.teamRightTitleLabel.font])];
        self.teamRightTitleLabel.frame = CGRectMake(CGRectGetMaxX(self.baraMainTitleLabel.frame), 20, teamRightSize.width, 44);
    }
}

#pragma mark - 获取当前赛事详情
-(void)sendRequestToGetGameDetail{
    if (!self.self.transferMatchId){
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:matchguessinfo requestParams:@{@"matchId":self.transferMatchId} responseObjectClass:[PDMatchguessinfoSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->matchRootModel = (PDMatchguessinfoSingleModel *)responseObject;
        // 0.修改是否提醒
        if (strongSelf->matchRootModel.remind){
            [strongSelf remindBtnStatusChange];
        }
        
        strongSelf.barTitleLabel.text = [NSString stringWithFormat:@"%@VS%@",strongSelf->matchRootModel.teamLeft.teamName,strongSelf->matchRootModel.teamRight.teamName];
    
        // 1. 修改背景
        strongSelf.lotteryGameDetailBackgroundView.transferBackgroundUrl = strongSelf->matchRootModel.gameBackgroundPic;

        // 1. 创建回话
        if ([[AccountModel sharedAccountModel] hasMemberLoggedIn]){
            [strongSelf createIMControllerWithIMId:strongSelf->matchRootModel.imGroupId];
        }
        // 2. 添加头部内容
        strongSelf.headerView.transferRootModel = strongSelf->matchRootModel;
        
        // 2.2
        [strongSelf.leftIconImgView uploadImageWithURL:strongSelf->matchRootModel.teamLeft.icon placeholder:nil callback:NULL];
        CGRect leftConvertFrame = [strongSelf.headerView convertRect:strongSelf.headerView.leftTeamImgView.frame fromView:[UIApplication sharedApplication].keyWindow];
        strongSelf->leftConvertTempRect = leftConvertFrame;
        strongSelf.leftIconImgView.frame = leftConvertFrame;
        
        [strongSelf.rightIconImgView uploadImageWithURL:strongSelf->matchRootModel.teamRight.icon placeholder:nil callback:NULL];
        CGRect rightConvertFrame = [strongSelf.headerView convertRect:strongSelf.headerView.rightTeamImgView.frame fromView:[UIApplication sharedApplication].keyWindow];
        strongSelf->rightConvertTempRect = rightConvertFrame;
        strongSelf.rightIconImgView.frame = rightConvertFrame;
        
        
        // 3. 传递比赛信息
        strongSelf.detailController.transferMatchSingleModel = strongSelf ->matchRootModel;
        
        // 5. 开始时间
        strongSelf->matchRootModel.gameStartTempTime = [NSDate getNSTimeIntervalWithCurrent] + strongSelf->matchRootModel.matchLeftTime;
        
        // 5. 计算宽度
        NSString *teamLeftStr = [NSString stringWithFormat:@"%li",(long)guessListSingleModel.teamAScore];
        CGSize teamLeftSize = [teamLeftStr sizeWithCalcFont:self.teamLeftTitleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.teamLeftTitleLabel.font])];
        self.teamLeftTitleLabel.frame = CGRectMake(self.baraMainTitleLabel.orgin_x - teamLeftSize.width, 20, teamLeftSize.width, 44);
        
        // 6. 计算宽度2
        NSString *teamRightStr = [NSString stringWithFormat:@"%li",guessListSingleModel.teamBScore];
        CGSize teamRightSize = [teamRightStr sizeWithCalcFont:self.teamRightTitleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.teamRightTitleLabel.font])];
        self.teamRightTitleLabel.frame = CGRectMake(CGRectGetMaxX(self.baraMainTitleLabel.frame), 20, teamRightSize.width, 44);
        
        // 7. 游戏时间
        NSString *gameTimeStr = [NSDate getTimeWithLotteryString:strongSelf->matchRootModel.matchStartTime / 1000.];
        self.gameTimeLabel.text = gameTimeStr;
        CGSize gameTimeSize = [gameTimeStr sizeWithCalcFont:self.gameTimeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gameTimeLabel.font])];
        self.gameTimeLabel.frame = CGRectMake((kScreenBounds.size.width - gameTimeSize.width) / 2., 20, gameTimeSize.width, [NSString contentofHeightWithFont:self.gameTimeLabel.font]);

        // 8.比赛状态
        NSString *gameStatusStr = @"";
        if ([strongSelf->matchRootModel.matchStatus isEqualToString:@"unbegun"]){                   // 比赛未开始
            gameStatusStr = @"竞猜中";
            strongSelf.gameTimeLabel.hidden = NO;
            strongSelf.gameStatusLabel.hidden = NO;
            strongSelf.teamLeftTitleLabel.hidden = YES;
            strongSelf.teamRightTitleLabel.hidden = YES;
            strongSelf.baraMainTitleLabel.hidden = YES;
        } else if ([strongSelf->matchRootModel.matchStatus isEqualToString:@"inPlay"]){             // 比赛中
            gameStatusStr = @"比赛中";
            strongSelf.gameTimeLabel.hidden = NO;
            strongSelf.gameStatusLabel.hidden = NO;
            strongSelf.teamLeftTitleLabel.hidden = YES;
            strongSelf.teamRightTitleLabel.hidden = YES;
            strongSelf.baraMainTitleLabel.hidden = YES;
        } else if ([strongSelf->matchRootModel.matchStatus isEqualToString:@"over"]){               // 比赛结束
            gameStatusStr = @"已结束";
            strongSelf.gameTimeLabel.hidden = YES;
            strongSelf.gameStatusLabel.hidden = YES;
            strongSelf.teamLeftTitleLabel.hidden = NO;
            strongSelf.teamRightTitleLabel.hidden = NO;
            strongSelf.baraMainTitleLabel.hidden = NO;
        }
        self.gameStatusLabel.text = gameStatusStr;
        self.gameStatusLabel.frame = CGRectMake(self.gameTimeLabel.orgin_x, CGRectGetMaxY(self.gameTimeLabel.frame), self.gameTimeLabel.size_width, [NSString contentofHeightWithFont:self.gameStatusLabel.font]);
        
    }];
}

#pragma mark 添加开奖提醒
-(void)sendRequestToAddRawremind{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:drawremind requestParams:@{@"id":self.transferMatchId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [PDHUD showShimmeringString:@"添加开奖提醒"];
            strongSelf -> matchRootModel.remind = YES;
            [strongSelf remindBtnStatusChange];
        }
    }];
}

#pragma mark 取消开奖提醒
-(void)sendRequestToCancelRawremind{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:removedrawremind requestParams:@{@"id":self.transferMatchId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [PDHUD showShimmeringString:@"取消开奖提醒"];
            strongSelf -> matchRootModel.remind = NO;
            [strongSelf remindBtnStatusChange];
        }
    }];
}

#pragma mark - OtherManager
-(void)remindBtnStatusChange{    // 1.
    [Tool clickZanWithView:self.navRightMsgButton block:^{
        if (matchRootModel.remind){          // 点开
            [self.navRightMsgButton setImage:[UIImage imageNamed:@"icon_lottery_detail_remind_sel"] forState:UIControlStateNormal];
            [self.tempRightButton setImage:[UIImage imageNamed:@"icon_lottery_detail_remind_sel"] forState:UIControlStateNormal];
        } else {
            [self.navRightMsgButton setImage:[UIImage imageNamed:@"icon_lottery_detail_remind_nor"] forState:UIControlStateNormal];
            [self.tempRightButton setImage:[UIImage imageNamed:@"icon_lottery_detail_remind_nor"] forState:UIControlStateNormal];
        }
    }];
}


#pragma mark - 新手引导
-(void)addGestureManager{
    return;
    PDGuideRootViewController *guideRootViewController = [[PDGuideRootViewController alloc]init];
    // 1. 添加消息
    PDGuideSingleModel *remindGuideSingleModel = [[PDGuideSingleModel alloc]init];
    remindGuideSingleModel.text = @"'点击这里可添加竞猜提醒哟'";
    remindGuideSingleModel.foundationType = foundationTypeCircle;
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    CGRect convertFrame = [self.view convertRect:self.tempRightButton.frame toView:keyWindow];
    remindGuideSingleModel.transferShowFrame = CGRectMake(convertFrame.origin.x - LCFloat(11), convertFrame.origin.y - LCFloat(11), convertFrame.size.width + 2 * LCFloat(11), convertFrame.size.height + 2 * LCFloat(11));
    
    // 2. 添加聊天
    PDGuideSingleModel *messageGuideSingleModel = [[PDGuideSingleModel alloc]init];
    messageGuideSingleModel.text = @"'这里可以和别的小伙伴讨论赛事哟'";
    messageGuideSingleModel.foundationType = foundationTypeCircle;
    messageGuideSingleModel.transferShowFrame = CGRectMake(kScreenBounds.size.width - self.segmentList.size_width / 3., self.segmentList.orgin_y, self.segmentList.size_width / 3. - LCFloat(11), self.segmentList.size_height);
    
    // 3. 添加竞猜
    PDGuideSingleModel *shengfuSingleModel = [[PDGuideSingleModel alloc]init];
    shengfuSingleModel.text = @"这个竞猜投左边简直不要太稳了";
    
    PDNewDetailLotteryCellTableViewCell *cell = (PDNewDetailLotteryCellTableViewCell *)[self.lotteryController.lotteryTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    CGRect lotteryRect = [self.lotteryController.lotteryTableView convertRect:cell.frame toView:keyWindow];
    shengfuSingleModel.foundationType = foundationTypeRect;
    shengfuSingleModel.transferShowFrame = lotteryRect;
    CGRect lotteryPosationRect = [cell convertRect:cell.leftButton.frame toView:keyWindow];
    shengfuSingleModel.transferPositionFrame = lotteryPosationRect;
    shengfuSingleModel.guideType = GuideGuesterTypeTap;
    
    // 详情
    [guideRootViewController lotteryDetailManagerBlock:^{
        
    }];
    [guideRootViewController showInView:self.parentViewController withViewActionArr:@[remindGuideSingleModel,messageGuideSingleModel,shengfuSingleModel]];
}

#pragma mark - 充值成功
- (void)goldExchangeFinish:(PDTopUpViewController *)topupViewController{
    [self.bottomView sendRequestToGetInfo];
}







#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGPoint point = scrollView.contentOffset;
    if(scrollView == self.lotteryGameDetailBackgroundView.mainScrollView){          // 背部的scrollView
        [self scrollviewDidScrollWithSegmentList:point];
        [self scrollviewDidScrollWithAnimation:point];
        
        if (point.y < 10){
            self.barTitleLabel.alpha = 1;
        } else {
            self.barTitleLabel.alpha = 0;
        }
        
    } else if (scrollView == self.infoScrollView){                                  // 存放资讯的scrollView
        NSLog(@"infoScrollView");
    }  else {
        NSLog(@"tableView");
    }
    [self scrollviewDidScrollWithUserActivity:point];
}

-(void)scrollviewDidScrollWithSegmentList:(CGPoint)point{
    if(point.y >= kHeader_Height - self.segmentList.size_height - 20){
        self.segmentList.orgin_y = 64;
        self.infoScrollView.orgin_y = CGRectGetMaxY(self.segmentList.frame);
    } else {
        self.segmentList.orgin_y = kHeader_Height - point.y;
        self.infoScrollView.orgin_y = CGRectGetMaxY(self.segmentList.frame);
    }
}

-(void)scrollviewDidScrollWithUserActivity:(CGPoint)point{
    if(point.y >= kHeader_Height - self.segmentList.size_height - 20){
        self.lotteryGameDetailBackgroundView.mainScrollView.scrollEnabled = NO;
        self.infoScrollView.scrollEnabled = NO;
        self.infoScrollView.userInteractionEnabled = YES;
        
        [self smartScrollViewEndingWithPoint:point];
    } else {
        self.lotteryGameDetailBackgroundView.mainScrollView.scrollEnabled = YES;
        self.infoScrollView.scrollEnabled = YES;
        self.infoScrollView.userInteractionEnabled = NO;
    }
}

#pragma mark 上边左右的两个标签缩放
-(void)scrollviewDidScrollWithAnimation:(CGPoint)point{
    // 1. 计算左侧的frame
    if(point.y < 0){            // 往下拉
        self.leftIconImgView.orgin_y = leftConvertTempRect.origin.y - point.y;
        self.rightIconImgView.orgin_y = rightConvertTempRect.origin.y - point.y;
        [self navBarToNormal];
    } else {                    // 往上拉
        // 1. 计算移动的高度
        CGFloat move_Height = leftConvertTempRect.origin.y + leftConvertTempRect.size.height / 2. - (20 + (44 - kTeamIcon_Max_Size) / 2.);
        CGFloat move_Width_Left = self.teamLeftTitleLabel.orgin_x - kTeamIcon_Max_Size / 2. - (leftConvertTempRect.origin.x + leftConvertTempRect.size.width / 2.);
        CGFloat move_Width_right = rightConvertTempRect.origin.x - CGRectGetMaxX(self.teamRightTitleLabel.frame) - kTeamIcon_Max_Size / 2.;
        if (point.y < move_Height){
            // 2. 计算比例
            CGFloat zoom =  (point.y / move_Height);
            if (zoom >= 1){
                zoom = 1;
            }
            CGFloat size = leftConvertTempRect.size.width - (leftConvertTempRect.size.width - kTeamIcon_Max_Size) * zoom;
            CGFloat origin_y = - point.y - self.leftIconImgView.size_height + leftConvertTempRect.origin.y + leftConvertTempRect.size.height;
            CGFloat origin_x_left = leftConvertTempRect.origin.x + move_Width_Left * zoom;
            CGFloat origin_x_right = rightConvertTempRect.origin.x - move_Width_right * zoom;
            self.leftIconImgView.frame = CGRectMake(origin_x_left, origin_y, size, size);
            self.rightIconImgView.frame = CGRectMake(origin_x_right, origin_y, size, size);
            
            self.headerView.alpha = 1 - zoom;
            
        } else {
            if (navBarAnimation == navBarAnimationNo){          // 没播放过动画
                navBarAnimation = navBarAnimationYes;
            }
        }
        if (navBarAnimation == navBarAnimationYes){
            if ([matchRootModel.matchStatus isEqualToString:@"unbegun"]){                   // 比赛未开始
                NSString *gameTimeStr = [NSDate getTimeWithLotteryString:matchRootModel.matchStartTime / 1000.];
                self.gameTimeLabel.text = gameTimeStr;
                self.gameStatusLabel.text = @"竞猜中";
            } else if ([matchRootModel.matchStatus isEqualToString:@"inPlay"]){             // 比赛中
                NSString *gameTimeStr = [NSDate getTimeWithLotteryString:matchRootModel.matchStartTime / 1000.];
                self.gameTimeLabel.text = gameTimeStr;
                self.gameStatusLabel.text = @"比赛中";
            } else if ([matchRootModel.matchStatus isEqualToString:@"over"]){               // 比赛结束
                self.teamLeftTitleLabel.text = [NSString stringWithFormat:@"%li",guessListSingleModel.teamAScore];
                self.teamRightTitleLabel.text = [NSString stringWithFormat:@"%li",guessListSingleModel.teamBScore];
                self.baraMainTitleLabel.text = @" : ";

            }
            navBarAnimation = navBarAnimationWaiting;
        }
    }
}

-(void)smartScrollViewEndingWithPoint:(CGPoint)point{
    CGFloat height =  kHeader_Height - self.segmentList.size_height - 20;
    if(point.y >= height){
        if(point.y < 0){            // 往下拉
            self.leftIconImgView.orgin_y = leftConvertTempRect.origin.y - point.y;
            self.rightIconImgView.orgin_y = rightConvertTempRect.origin.y - point.y;
        } else {                    // 往上拉
            CGFloat move_Width_Left = self.teamLeftTitleLabel.orgin_x - kTeamIcon_Max_Size / 2. - (leftConvertTempRect.origin.x + leftConvertTempRect.size.width / 2.);
            CGFloat move_Width_right = rightConvertTempRect.origin.x - CGRectGetMaxX(self.teamRightTitleLabel.frame) - kTeamIcon_Max_Size / 2.;
            // 2. 计算比例
            CGFloat zoom =  1;
            CGFloat size = leftConvertTempRect.size.width - (leftConvertTempRect.size.width - kTeamIcon_Max_Size) * zoom;
            CGFloat origin_y = 20 + (44 - LCFloat(30)) / 2.;
            CGFloat origin_x_left = leftConvertTempRect.origin.x + move_Width_Left * zoom - LCFloat(5);
            CGFloat origin_x_right = rightConvertTempRect.origin.x - move_Width_right * zoom + LCFloat(5);
            [UIView animateWithDuration:.1f animations:^{
                self.leftIconImgView.frame = CGRectMake(origin_x_left, origin_y, size, size);
                self.rightIconImgView.frame = CGRectMake(origin_x_right, origin_y, size, size);
            }];
            
            self.headerView.alpha = 1 - zoom;
        }
    }
}

#pragma mark - 赛事投注详情
-(void)scrollViewDelegateWithDetailManager:(UIScrollView *)scrollView andDrop:(BOOL)drop{
    if(drop){
        self.lotteryGameDetailBackgroundView.mainScrollView.scrollEnabled = YES;
        self.infoScrollView.scrollEnabled = YES;
        self.infoScrollView.userInteractionEnabled = NO;
        [self.lotteryGameDetailBackgroundView.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        [self navBarToNormal1];
    }
}


#pragma mark - 竞猜列表
- (void)scrollViewDelegateManager:(UIScrollView *)scrollView andDrop:(BOOL)drop{
    if (drop){
        self.lotteryGameDetailBackgroundView.mainScrollView.scrollEnabled = YES;
        self.infoScrollView.scrollEnabled = YES;
        self.infoScrollView.userInteractionEnabled = NO;
        [self.lotteryGameDetailBackgroundView.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        [self navBarToNormal1];
    }
}

#pragma mark - Other
-(void)navBarToNormal{
    navBarAnimation = navBarAnimationNo;
    self.teamLeftTitleLabel.text = @"";
    self.teamRightTitleLabel.text = @"";
    self.baraMainTitleLabel.text = @"";
    self.gameStatusLabel.text = @"";
    self.gameTimeLabel.text = @"";
}

-(void)navBarToNormal1{
    navBarAnimation = navBarAnimationYes;
    self.teamLeftTitleLabel.text = @"";
    self.teamRightTitleLabel.text = @"";
    self.baraMainTitleLabel.text = @"";
    self.gameStatusLabel.text = @"";
    self.gameTimeLabel.text = @"";
}


- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

-(void)handleSingleTap:(UITapGestureRecognizer *)sender{
    CGPoint point = [sender locationInView:self.lotteryController.lotteryTableView];
    CGPoint segmentPoint = [sender locationInView:self.segmentList];
    
    if (point.y > 0){
        if (self.segmentList.selectedButtonIndex == 0){             // 竞猜
            [self.lotteryController getCurrentLocationToManager:point];
        }
    } else {
        
    }
    
    // segment
    if (segmentPoint.y > 0 && segmentPoint.y < self.segmentList.size_height){
        CGFloat segmentWidth = self.segmentList.size_width / 3.;
        NSInteger segmentIndex = segmentPoint.x / segmentWidth;
        [self.segmentList setSelectedButtonIndex:segmentIndex animated:YES];
        [self segmentSelectedManager:segmentIndex];
    }
}

@end
