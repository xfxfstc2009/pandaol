//
//  PDLotteryGameDetailRecordHeaderView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 1. 头部的view
#import <UIKit/UIKit.h>
#import "PDMatchguessinfoSingleModel.h"



@interface PDLotteryGameDetailRecordHeaderView : UIView

@property (nonatomic,strong)PDMatchguessinfoSingleModel *transferRootModel;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)UIButton *leftButton;

-(void)changeHeaderType;
@end
