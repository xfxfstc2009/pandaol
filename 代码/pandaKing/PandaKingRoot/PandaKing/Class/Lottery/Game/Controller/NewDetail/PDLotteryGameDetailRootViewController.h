//
//  PDLotteryGameDetailRootViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDLotteryGameDetailRootViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferMatchId;

@property (nonatomic,assign)CGRect transferLeftRect;
@property (nonatomic,assign)CGRect transferRightRect;


@end
