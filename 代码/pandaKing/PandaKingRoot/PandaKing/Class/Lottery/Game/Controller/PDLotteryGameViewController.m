//
//  PDLotteryGameViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameViewController.h"
#import "PDLotteryGameMainHeaderView.h"
#import "PDLotteryGameRootTableView.h"

// 【cell】
#import "PDLotteryGameSubViewCell.h"

// 【Model】
#import "PDLoteryGameRootListModel.h"
#import "PDLotteryGameMenuListModel.h"

// 【Controller】
#import "PDLotteryGameDetailRootViewController.h"

// test
#import "PDLotteryGameTouzhuViewController.h"
#import "PDLotteryGameDetailRootViewController.h"



@interface PDLotteryGameViewController()<PDLotteryGameRootTableViewDelegate,PDLotteryGameMainHeaderViewDelegate>{
    BOOL arrowIsOpen;
}
@property (nonatomic,strong)PDLotteryGameRootTableView *lotteryGameTableView;              /**< 赛事猜tableView*/
@property (nonatomic,strong)NSMutableArray *gameMainMutableArr;             /**< 主main*/
@property (nonatomic,strong)NSMutableArray *gameSubTempMutableArr;          /**< 临时使用*/
@property (nonatomic,strong)UIView *barHeaderView;                          /**< navBar view*/

@property (nonatomic,strong)NSMutableArray *menuMutableArr;                  /**< menu数组*/

// titleView
@property (nonatomic,strong)UIView *titleView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)PDImageView *arrowImgView;
@property (nonatomic,strong)UIButton *tapButton;

@property (nonatomic,strong)NSString *tempGameId;               /**< 判断是否有选定*/

@end

@implementation PDLotteryGameViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetMatchList];
    
    [weakSelf sendRequestToGetGameMenuList];        // 获取赛事menu
    
}

-(void)createHeaderView{
    self.barHeaderView = [[UIView alloc]init];
    self.barHeaderView.backgroundColor = [UIColor clearColor];
    
}

-(void)pageSetting{
    self.barMainTitle = @"赛事猜";
    __weak typeof(self)weakSelf = self;
    [weakSelf leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_page_cancle"] barHltImage:[UIImage imageNamed:@"icon_page_cancle"] action:^{
        if (self != [self.navigationController.viewControllers firstObject]){
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self dismissViewControllerAnimated:YES completion:NULL];
        }
    }];
    
    // right
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_lottery_rule"] barHltImage:[UIImage imageNamed:@"icon_lottery_rule"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:gzgz];
        [strongSelf.navigationController pushViewController:webViewController animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.gameMainMutableArr = [NSMutableArray array];
    self.gameSubTempMutableArr = [NSMutableArray array];
    self.menuMutableArr = [NSMutableArray array];                       // menu  数组
    
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.lotteryGameTableView) {
        self.lotteryGameTableView = [[PDLotteryGameRootTableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.lotteryGameTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.lotteryGameTableView.backgroundColor = [UIColor clearColor];
        self.lotteryGameTableView.showsVerticalScrollIndicator = NO;
        self.lotteryGameTableView.scrollEnabled = YES;
        self.lotteryGameTableView.foldDelegate = self;
        self.lotteryGameTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.lotteryGameTableView];
    }
    
    __weak typeof(self)weakSelf = self;
    // 上拉加载
    [weakSelf.lotteryGameTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetMatchList];
    }];
    
    // 下拉刷新
    [weakSelf.lotteryGameTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetMatchList];
    }];
    
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionForFlodTableView:(UITableView *)tableView{
    return self.gameSubTempMutableArr.count;
}

-(NSInteger)foldTableView:(UITableView *)tableView numberOfRowInSection:(NSInteger)section{
    PDLoteryGameRootSubSingleModel *singleModel = [self.gameSubTempMutableArr objectAtIndex:section];
    return  singleModel.lotteryDetailList.count;
}

-(UITableViewCell *)foldTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
    PDLotteryGameSubViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
    if (!cellWithRowThr){
        cellWithRowThr = [[PDLotteryGameSubViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowThr.backgroundColor = c8;
    }
    cellWithRowThr.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    
    PDLoteryGameRootSubSingleModel *singleModel = [self.gameSubTempMutableArr objectAtIndex:indexPath.section];
    PDLotteryGameSubListSingleModel *subListSingleModel = [singleModel.lotteryDetailList objectAtIndex:indexPath.row];
    cellWithRowThr.transferLotteryGameListSingleModel = subListSingleModel;
    return cellWithRowThr;
}


#pragma mark - UITableViewDelegate
-(void)foldTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    PDLoteryGameRootSubSingleModel *singleModel = [self.gameSubTempMutableArr objectAtIndex:indexPath.section];
    PDLotteryGameSubListSingleModel *subListSingleModel = [singleModel.lotteryDetailList objectAtIndex:indexPath.row];
    
    PDLotteryGameDetailRootViewController *lotteryGameDetailViewController = [[PDLotteryGameDetailRootViewController alloc]init];
    lotteryGameDetailViewController.transferMatchId = subListSingleModel.ID;
    [self.navigationController pushViewController:lotteryGameDetailViewController animated:YES];
}

-(UIView *)foldTableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    PDLotteryGameMainHeaderView *headerView = [[PDLotteryGameMainHeaderView alloc]init];
    headerView.transferIndex = section;
    PDLoteryGameRootSubSingleModel *singleModel = [self.gameSubTempMutableArr objectAtIndex:section];
    if (singleModel.currentTempTime != -1){
        headerView.transferType = LotterGameHeaderTypeTime;
    } else {
        headerView.transferType = LotterGameHeaderTypeNormal;
    }
    headerView.transferGameRootSubSingleModel = singleModel;
    headerView.delegate = self;
    
    return headerView;
}

-(CGFloat)foldTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    PDLoteryGameRootSubSingleModel *singleModel = [self.gameSubTempMutableArr objectAtIndex:section];
    if (singleModel.currentTempTime != -1){
        return [PDLotteryGameMainHeaderView calculationCellHeightWithType:LotterGameHeaderTypeTime];
    } else {
        return [PDLotteryGameMainHeaderView calculationCellHeightWithType:LotterGameHeaderTypeNormal];
    }
}

-(CGFloat)foldTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return LCFloat(52);
}

#pragma mark - 
-(void)foldTableViewHeaderDidSelectedWithIndex:(NSInteger)index{
    
    PDLoteryGameRootSubSingleModel *singleModel = [self.gameSubTempMutableArr objectAtIndex:index];
    PDLotteryGameDetailRootViewController *lotteryViewController = [[PDLotteryGameDetailRootViewController alloc]init];
    lotteryViewController.transferMatchId = singleModel.matchId;
    [self.navigationController pushViewController:lotteryViewController animated:YES];
    
//    [self.lotteryGameTableView foldFoldingSectionHeaderTappedAtIndex:index];            // 折叠，展示
}


-(void)foldTableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.lotteryGameTableView) {
        [cell addSeparatorLineWithTypeWithAres:SeparatorTypeBottom andUseing:@"record"];
    }
}

#pragma mark - 获取当前赛事列表
-(void)sendRequestToGetMatchList{
    __weak typeof(self)weakSelf = self;

    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    [dic setValue:self.lotteryGameTableView.currentPage forKey:@"page"];
    [dic setValue:@"5" forKey:@"size"];
    if (self.tempGameId.length){
        [dic setValue:self.tempGameId forKey:@"gameId"];
    }
    
    //【HUD】
    [PDHUD showHUDProgress:@"正在获取当前赛事列表…" diary:0];
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:matchguesslist requestParams:dic responseObjectClass:[PDLoteryGameRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDLoteryGameRootListModel *lotteryGameRootListModel = (PDLoteryGameRootListModel *)responseObject;
            
            if ([strongSelf.lotteryGameTableView.isXiaLa isEqualToString:@"YES"]){
                [strongSelf.gameMainMutableArr removeAllObjects];
                [strongSelf.gameSubTempMutableArr removeAllObjects];
            }
            
            [strongSelf.gameMainMutableArr addObjectsFromArray:lotteryGameRootListModel.lotteryList];
            
            for (int i = 0 ; i < lotteryGameRootListModel.lotteryList.count;i++){
                // 1. 获取当前所有的赛事
                PDLoteryGameRootSingleModel *lotteryGameRootSingleModel = [lotteryGameRootListModel.lotteryList objectAtIndex:i];
                for (int j = 0 ; j < lotteryGameRootSingleModel.subList.count;j++){
                    PDLoteryGameRootSubSingleModel *subSingleModel = [lotteryGameRootSingleModel.subList objectAtIndex:j];
                    subSingleModel.gameStartTempTime = [NSDate getNSTimeIntervalWithCurrent] + subSingleModel.matchLeftTime;
                    if (j == 0){
                        subSingleModel.currentTempTime = lotteryGameRootSingleModel.dateTime;
                    } else {
                        subSingleModel.currentTempTime = -1;
                    }
                    
                    for (int k = 0 ; k < subSingleModel.lotteryDetailList.count;k++){
                        PDLotteryGameSubListSingleModel *subListSingleModel = [subSingleModel.lotteryDetailList objectAtIndex:k];
                        subListSingleModel.endTime = [NSDate getNSTimeIntervalWithCurrent] + subListSingleModel.leftTime;
                    }
                }
                [strongSelf.gameSubTempMutableArr addObjectsFromArray:lotteryGameRootSingleModel.subList];
            }
            [strongSelf.lotteryGameTableView reloadData];
            
            if ([strongSelf.lotteryGameTableView.isXiaLa isEqualToString:@"YES"]){
                [strongSelf.lotteryGameTableView stopPullToRefresh];
            } else {
                [strongSelf.lotteryGameTableView stopFinishScrollingRefresh];
            }
            
            // show
            if (!strongSelf.gameSubTempMutableArr.count){
                NSString *prompt = [NSString stringWithFormat:@"暂时没有关于【%@】的竞猜",strongSelf.titleLabel.text];
                [strongSelf.lotteryGameTableView showPrompt:prompt withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            } else {
                [strongSelf.lotteryGameTableView dismissPrompt];
            }
            
            // 添加新手引导
            if ([AccountModel sharedAccountModel].matchGuess == NO){
                [strongSelf showGesture];
            }
            
        } else {
            [strongSelf.lotteryGameTableView showPrompt:@"服务器开了点小差，重新点击试试？" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:^{
                [strongSelf sendRequestToGetMatchList];
            }];
        }
    }];
}

#pragma mark - 获取赛事猜目录
-(void)sendRequestToGetGameMenuList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:gamelist requestParams:nil responseObjectClass:[PDLotteryGameMenuListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.menuMutableArr removeAllObjects];
            PDLotteryGameMenuListModel *listModel = (PDLotteryGameMenuListModel *)responseObject;
            
            PDLotteryGameMenuSingleModel *lotteryGameAllSingleModel = [[PDLotteryGameMenuSingleModel alloc]init];
            lotteryGameAllSingleModel.ID = @"-1";
            lotteryGameAllSingleModel.shortName = @"LOL";
            lotteryGameAllSingleModel.fullName = @"全部竞猜";
            lotteryGameAllSingleModel.enabled = YES;
            [strongSelf.menuMutableArr addObject:lotteryGameAllSingleModel];
            [strongSelf.menuMutableArr addObjectsFromArray:listModel.game];
        }
    }];
}


#pragma mark - 显示新手引导
-(void)showGesture{
    return;
    
    if (!self.menuMutableArr.count){
        return;
    }
    
    if (!self.gameSubTempMutableArr.count){
        return;
    }
    
    PDGuideRootViewController *guideRootViewController = [[PDGuideRootViewController alloc]init];
    PDGuideSingleModel *singleModel = [[PDGuideSingleModel alloc]init];

    singleModel.transferShowFrame = CGRectMake(0, 64, kScreenBounds.size.width,[PDLotteryGameMainHeaderView calculationCellHeightWithType:LotterGameHeaderTypeTime]);
    singleModel.guideType = GuideGuesterTypeTap;
    singleModel.text = @"点击一场竞猜中的赛事";
    singleModel.foundationType = foundationTypeRect;
    
    __weak typeof(self)weakSelf = self;
    [guideRootViewController actionClicDismisWithGameLotteryManager:^{                  // 【进行跳转】
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
//     【跳转到详情】
        PDLoteryGameRootSubSingleModel *singleModel = [strongSelf.gameSubTempMutableArr objectAtIndex:0];
        PDLotteryGameDetailRootViewController *lotteryViewController = [[PDLotteryGameDetailRootViewController alloc]init];
        lotteryViewController.transferMatchId = singleModel.matchId;
        [strongSelf.navigationController pushViewController:lotteryViewController animated:YES];
        [guideRootViewController lotteryRootManagerBlock:^{
            
        }];
    }];
    [guideRootViewController showInView:self.parentViewController withViewActionArr:@[singleModel]];
    
}

@end
