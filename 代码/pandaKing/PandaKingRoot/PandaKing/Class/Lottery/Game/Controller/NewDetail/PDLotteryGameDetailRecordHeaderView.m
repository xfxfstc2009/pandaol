//
//  PDLotteryGameDetailRecordHeaderView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameDetailRecordHeaderView.h"
#import "PDLotteryGameDetailHeaderStatusView.h"


@interface PDLotteryGameDetailRecordHeaderView()
// bg
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)UIView *alphaView;

// left
@property (nonatomic,strong)PDImageView *leftImgView;
@property (nonatomic,strong)UILabel *leftMaxLabel;
@property (nonatomic,strong)UILabel *leftTeamfixedLabel;
@property (nonatomic,strong)UILabel *leftRateLabel;
@property (nonatomic,strong)UILabel *leftRateFixedLabel;
@property (nonatomic,strong)PDImageView *lineView;

// right
@property (nonatomic,strong)PDImageView *rightImgView;
@property (nonatomic,strong)UILabel *rightMaxLabel;
@property (nonatomic,strong)UILabel *rightRateLabel;
@property (nonatomic,strong)UILabel *rightRateFixedLabel;
@property (nonatomic,strong)UILabel *rightTeamFixedLabel;

// center
@property (nonatomic,strong)UILabel *joinLabel;
@property (nonatomic,strong)PDLotteryGameDetailHeaderStatusView *statusView;

@end

@implementation PDLotteryGameDetailRecordHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建背景图片
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgImgView];
    
    // 2. 创建顶部的alpha
    self.alphaView = [[UIView alloc]init];
    self.alphaView.backgroundColor = [UIColor blackColor];
    self.alphaView.alpha = .4f;
    [self addSubview:self.alphaView];
    
    // 3. 创建左侧队伍
    self.leftImgView = [[PDImageView alloc]init];
    self.leftImgView.backgroundColor = [UIColor clearColor];
    self.leftImgView.frame = CGRectMake(LCFloat(33), 0, LCFloat(60), LCFloat(60));
    [self addSubview:self.leftImgView];
    
    
    // 5. 创建左侧上线
    self.leftMaxLabel = [[UILabel alloc]init];
    self.leftMaxLabel.backgroundColor = [UIColor clearColor];
    self.leftMaxLabel.textAlignment = NSTextAlignmentCenter;
    self.leftMaxLabel.textColor = c26;
    self.leftMaxLabel.font = [UIFont systemFontOfCustomeSize:10.];
    [self addSubview:self.leftMaxLabel];
    self.leftMaxLabel.frame = CGRectMake(0, 0, self.leftImgView.size_width, [NSString contentofHeightWithFont:self.leftMaxLabel.font]);
    
    // 6. 创建右侧的view
    self.rightImgView = [[PDImageView alloc]init];
    self.rightImgView.backgroundColor = [UIColor clearColor];
    self.rightImgView.frame = CGRectMake(kScreenBounds.size.width - self.leftImgView.size_width - self.leftImgView.orgin_x, self.leftImgView.orgin_y, self.leftImgView.size_width, self.leftImgView.size_height);
    [self addSubview:self.rightImgView];
    
    // 7. 创建右侧竞猜按钮
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(self.rightImgView.orgin_x, self.leftButton.orgin_y, self.leftButton.size_width, self.leftButton.size_height);
    [self.rightButton setTitle:@"竞猜" forState:UIControlStateNormal];
    self.rightButton.titleLabel.font = [UIFont systemFontOfCustomeSize:11.];
    self.rightButton.clipsToBounds = YES;
    [self.rightButton setTitleColor:c26 forState:UIControlStateNormal];
    self.rightButton.layer.cornerRadius = MIN(self.leftButton.size_width, self.leftButton.size_height) / 2.;
    self.rightButton.layer.borderColor = c26.CGColor;
    self.rightButton.layer.borderWidth = 1.;
    [self addSubview:self.rightButton];
    
    // 8.创建右侧上线
    self.rightMaxLabel = [[UILabel alloc]init];
    self.rightMaxLabel.backgroundColor = [UIColor clearColor];
    self.rightMaxLabel.textAlignment = NSTextAlignmentCenter;
    self.rightMaxLabel.textColor = c26;
    self.rightMaxLabel.font = [UIFont systemFontOfCustomeSize:10.];
    [self addSubview:self.rightMaxLabel];
    self.rightMaxLabel.frame = CGRectMake(0, 0, self.leftImgView.size_width, [NSString contentofHeightWithFont:self.leftMaxLabel.font]);
    
    // 9 . 创建lineView
    self.lineView = [[PDImageView alloc]init];
    self.lineView.frame = CGRectMake(kScreenBounds.size.width / 2. - LCFloat(2), 0, LCFloat(1), LCFloat(60));
    self.lineView.image = [UIImage imageNamed:@"icon_lottery_detail_cutLine"];
    [self addSubview:self.lineView];
    
    // 10.创建左侧获得
    self.leftTeamfixedLabel = [[UILabel alloc]init];
    self.leftTeamfixedLabel.backgroundColor = [UIColor clearColor];
    self.leftTeamfixedLabel.textColor = c1;
    self.leftTeamfixedLabel.adjustsFontSizeToFitWidth = YES;
    self.leftTeamfixedLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.leftTeamfixedLabel];
    self.leftTeamfixedLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    
    // 11. 创建左侧赔率
    self.leftRateLabel = [[UILabel alloc]init];
    self.leftRateLabel.backgroundColor = [UIColor clearColor];
    self.leftRateLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.leftRateLabel.textColor = c26;
    [self addSubview:self.leftRateLabel];
    
    // 12. 创建右侧
    self.rightTeamFixedLabel = [[UILabel alloc]init];
    self.rightTeamFixedLabel.backgroundColor = [UIColor clearColor];
    self.rightTeamFixedLabel.textColor = c1;
    self.rightTeamFixedLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.rightTeamFixedLabel];
    self.rightTeamFixedLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    
    // 13. 创建右侧赔率
    self.rightRateLabel = [[UILabel alloc]init];
    self.rightRateLabel.backgroundColor = [UIColor clearColor];
    self.rightRateLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.rightRateLabel.textColor = c26;
    [self addSubview:self.rightRateLabel];
    
    // 14. 创建参与人数
    self.joinLabel = [[UILabel alloc]init];
    self.joinLabel.backgroundColor = [UIColor clearColor];
    self.joinLabel.frame = CGRectMake(0, self.size_height - LCFloat(15) - [NSString contentofHeightWithFont:self.joinLabel.font], 0, [NSString contentofHeightWithFont:self.joinLabel.font]);
    self.joinLabel.center_x = kScreenBounds.size.width / 2.;
    [self addSubview:self.joinLabel];
    self.joinLabel.font = [UIFont systemFontOfCustomeSize:11.];
    self.joinLabel.textColor = c26;
    
    // 15. 创建倒计时
    self.statusView = [[PDLotteryGameDetailHeaderStatusView alloc]initWithFrame:CGRectMake(0, self.joinLabel.orgin_y - LCFloat(7) - LCFloat(15), LCFloat(59), LCFloat(15))];
    self.statusView.center_x = kScreenBounds.size.width / 2.;
    [self addSubview:self.statusView];
    
    // 17. 创建左侧赔率
    self.leftRateFixedLabel = [[UILabel alloc]init];
    self.leftRateFixedLabel.backgroundColor = [UIColor clearColor];
    self.leftRateFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.leftRateFixedLabel.textColor = c8;
    [self addSubview:self.leftRateFixedLabel];
    
    
    // 18. 创建右侧赔率
    self.rightRateFixedLabel = [[UILabel alloc]init];
    self.rightRateFixedLabel.backgroundColor = [UIColor clearColor];
    self.rightRateFixedLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.rightRateFixedLabel.textColor = c8;
    [self addSubview:self.rightRateFixedLabel];
}

-(void)setTransferRootModel:(PDMatchguessinfoSingleModel *)transferRootModel{
    _transferRootModel = transferRootModel;
    [self.bgImgView uploadImageWithURL:transferRootModel.gameBackgroundPic placeholder:nil callback:NULL];
    
    // 1.左侧
    [self.leftImgView uploadImageWithURL:transferRootModel.teamLeft.icon placeholder:nil callback:NULL];
    // 2.右侧
    [self.rightImgView uploadImageWithURL:transferRootModel.teamRight.icon placeholder:nil callback:NULL];
    
    // 2. 创建上线
    self.leftMaxLabel.hidden = YES;
    self.leftMaxLabel.text = [NSString stringWithFormat:@"上限:%@",@"1111"];
    CGSize leftMaxSize = [self.leftMaxLabel.text sizeWithCalcFont:self.leftMaxLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leftMaxLabel.font])];
    self.leftMaxLabel.frame = CGRectMake(0, CGRectGetMaxY(self.leftButton.frame) + LCFloat(5), leftMaxSize.width, [NSString contentofHeightWithFont:self.leftMaxLabel.font]);
    self.leftMaxLabel.center_x = self.leftImgView.center_x;

    
    // 3. 创建右侧上限
    self.rightMaxLabel.hidden =YES;
    self.rightMaxLabel.text = [NSString stringWithFormat:@"上限:%@",@"11"];
    CGSize rightMaxSize = [self.rightMaxLabel.text sizeWithCalcFont:self.rightMaxLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.rightMaxLabel.font])];
    self.rightMaxLabel.frame = CGRectMake(0, self.leftMaxLabel.orgin_y, rightMaxSize.width, [NSString contentofHeightWithFont:self.rightMaxLabel.font]);
    self.rightMaxLabel.center_x = self.rightImgView.center_x;
    

    // 4. 创建左侧名字
    CGFloat teamWidth = self.rightImgView.orgin_x - kScreenBounds.size.width / 2. - LCFloat(11);
    
    self.leftTeamfixedLabel.text = [NSString stringWithFormat:@"%@",transferRootModel.teamLeft.teamName];
    //    CGSize teamFixedSize = [self.leftTeamfixedLabel.text sizeWithCalcFont:self.leftTeamfixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leftTeamfixedLabel.font])];
    self.leftTeamfixedLabel.frame = CGRectMake(kScreenBounds.size.width / 2. - LCFloat(11) - teamWidth, self.leftImgView.orgin_y + LCFloat(11), teamWidth, [NSString contentofHeightWithFont:self.leftTeamfixedLabel.font]);
    
    // 5. 创建右侧名字
    self.rightTeamFixedLabel.text = [NSString stringWithFormat:@"%@",transferRootModel.teamRight.teamName];
    

    
    //    CGSize teamRightSize = [self.rightTeamFixedLabel.text sizeWithCalcFont:self.rightTeamFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.rightTeamFixedLabel.font])];
    self.rightTeamFixedLabel.frame = CGRectMake(kScreenBounds.size.width / 2. + LCFloat(11), self.leftTeamfixedLabel.orgin_y, teamWidth, [NSString contentofHeightWithFont:self.rightTeamFixedLabel.font]);


//    // 7. 创建赔率
//    self.leftRateLabel.text = [NSString stringWithFormat:@"%.2f",transferRootModel.teamLeft.rate];
//    CGSize leftRateSize = [self.leftRateLabel.text sizeWithCalcFont:self.leftRateLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leftRateLabel.font])];
//    self.leftRateLabel.frame = CGRectMake(kScreenBounds.size.width / 2. - LCFloat(11) - leftRateSize.width, CGRectGetMaxY(self.leftTeamfixedLabel.frame) + LCFloat(8), leftRateSize.width, [NSString contentofHeightWithFont:self.leftRateLabel.font]);
//
//    self.leftRateFixedLabel.text = @"赔率";
//    CGSize leftRateFixedSize = [self.leftRateFixedLabel.text sizeWithCalcFont:self.leftRateFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leftRateFixedLabel.font])];
//    self.leftRateFixedLabel.frame = CGRectMake(self.leftRateLabel.orgin_x - LCFloat(6) - leftRateFixedSize.width, self.leftRateLabel.center_y - [NSString contentofHeightWithFont:self.leftTeamfixedLabel.font] / 2., leftRateFixedSize.width, [NSString contentofHeightWithFont:self.leftTeamfixedLabel.font]);
//    
//    // 8.创建右侧赔率
//    self.rightRateLabel.text = [NSString stringWithFormat:@"%.2f",transferRootModel.teamRight.rate];
//    CGSize rightRateSize = [self.rightRateLabel.text sizeWithCalcFont:self.rightRateLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.rightRateLabel.font])];
//    self.rightRateLabel.frame = CGRectMake(kScreenBounds.size.width / 2. + LCFloat(11), self.leftRateLabel.orgin_y, rightRateSize.width, [NSString contentofHeightWithFont:self.rightRateLabel.font]);
//    
//    // 8.1
//    self.rightRateFixedLabel.text = @"赔率";
//    CGSize rightRateFixedSize = [self.rightRateFixedLabel.text sizeWithCalcFont:self.rightRateFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.rightTeamFixedLabel.font])];
//    self.rightRateFixedLabel.frame = CGRectMake(CGRectGetMaxX(self.rightRateLabel.frame) + LCFloat(6), self.leftRateFixedLabel.orgin_y, rightRateFixedSize.width, [NSString contentofHeightWithFont:self.rightRateFixedLabel.font]);
//    
//    LotteryGameDetailStatus status = -1;
//    if([transferRootModel.lotteryStatus isEqualToString:@"staking"]){           // 可竞猜
//        status = LotteryGameDetailStatusWillBegin;
//        
//        [self.leftButton setTitleColor:c26 forState:UIControlStateNormal];
//        [self.leftButton setTitle:@"竞猜" forState:UIControlStateNormal];
//        self.leftButton.layer.borderColor = c26.CGColor;
//        self.leftButton.userInteractionEnabled = YES;
//        
//        
//        
//        [self.rightButton setTitleColor:c26 forState:UIControlStateNormal];
//        [self.rightButton setTitle:@"竞猜" forState:UIControlStateNormal];
//        self.rightButton.layer.borderColor = c26.CGColor;
//        self.rightButton.userInteractionEnabled = YES;
//        
//        self.rightButton.hidden = NO;
//        self.leftButton.hidden = NO;
//        self.leftMaxLabel.hidden = NO;
//        self.rightMaxLabel.hidden = NO;
//        
//        [self changeHeaderType];
//        
//    } else if ([transferRootModel.lotteryStatus isEqualToString:@"stakeoff"]){      // 竞猜截至
//        status = LotteryGameDetailStatusGaming;
//        
//        [self.leftButton setTitle:@"竞猜截止" forState:UIControlStateNormal];
//        [self.leftButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
//        self.leftButton.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
//        self.leftButton.userInteractionEnabled = NO;
//        
//        [self.rightButton setTitle:@"竞猜截止" forState:UIControlStateNormal];
//        [self.rightButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
//        self.rightButton.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
//        self.rightButton.userInteractionEnabled = NO;
//        
//        self.rightButton.hidden = NO;
//        self.leftButton.hidden = NO;
//        self.leftMaxLabel.hidden = NO;
//        self.rightMaxLabel.hidden = NO;
//        
//    } else if ([transferRootModel.lotteryStatus isEqualToString:@"finished"]){          // 已结算
//        status = LotteryGameDetailStatusEnd;
//        
//        self.rightButton.hidden = YES;
//        self.leftButton.hidden = YES;
//        self.leftMaxLabel.hidden = YES;
//        self.rightMaxLabel.hidden = YES;
//    } else {
//        status = LotteryGameDetailStatusNormal;
//    }
//    
//    //    if([transferRootModel.matchStatus isEqualToString:@"unbegun"]){   //  未开始
//    //
//    //    } else if ([transferRootModel.matchStatus isEqualToString:@"inPlay"]){          // 比赛已经开始
//    //
//    //    } else if ([transferRootModel.matchStatus isEqualToString:@"over"]){            // 比赛结束
//    //        status = LotteryGameDetailStatusEnd;
//    //    } else {
//    //
//    //    }
//    //
//    
//    NSTimeInterval timeInterval = (transferRootModel.endTime / 1000.);
//    [self.statusView startCutDown:timeInterval type:status block:^{
//        NSLog(@"比赛结束");
//    }];
//    
//    if ([transferRootModel.matchStatus isEqualToString:@"over"] && [transferRootModel.lotteryStatus isEqualToString:@"finished"]){
//        [self.statusView showGameSettlement];
//    }
//    
//    
//    // 如果上限==0 就可以无限
//    if(transferRootModel.teamLeft.bettingMax == 0){
//        self.leftMaxLabel.hidden = YES;
//    } else {
//        self.leftMaxLabel.hidden = NO;
//    }
//    
//    if(transferRootModel.teamRight.bettingMax == 0){
//        self.rightMaxLabel.hidden = YES;
//    } else {
//        self.rightMaxLabel.hidden = NO;
//    }
}




-(void)changeHeaderType{
//    if (self.transferRootModel.teamLeft.hasStake){
//        [self.leftButton setTitle:@"追加" forState:UIControlStateNormal];
//    } else {
//        [self.leftButton setTitle:@"竞猜" forState:UIControlStateNormal];
//    }
//    
//    if (self.transferRootModel.teamRight.hasStake){
//        [self.rightButton setTitle:@"追加" forState:UIControlStateNormal];
//    } else {
//        [self.rightButton setTitle:@"竞猜" forState:UIControlStateNormal];
//    }
}

#pragma mark - 修改参与人数
-(void)updateJoinHum:(NSInteger)humCount moneyCount:(NSInteger)moneyCount{
    // 6. 创建join
    self.joinLabel.text = [NSString stringWithFormat:@"%li人参与，总竞猜%li",humCount,moneyCount];
    CGSize joinSize = [self.joinLabel.text sizeWithCalcFont:self.joinLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.joinLabel.font])];
    self.joinLabel.frame = CGRectMake(0, self.size_height - LCFloat(15) - [NSString contentofHeightWithFont:self.joinLabel.font], joinSize.width, [NSString contentofHeightWithFont:self.joinLabel.font]);
    self.joinLabel.center_x = kScreenBounds.size.width / 2.;
}
@end
