//
//  PDLotteryGameDetailLotteryViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 【竞猜列表】
#import "AbstractViewController.h"
#import "PDNewDetailLotteryCellTableViewCell.h"

@protocol PDLotteryGameDetailLotteryViewControllerDelegate <NSObject>

- (void)scrollViewDelegateManager:(UIScrollView *)scrollView andDrop:(BOOL)drop;

-(void)lotteryscrollViewWillBeginDragging:(UIScrollView *)scrollView;
-(void)lotteryscrollViewDidEndDragging:(UIScrollView *)scrollView;

@end

@interface PDLotteryGameDetailLotteryViewController : AbstractViewController
@property (nonatomic,strong)NSMutableArray*lotteryMutableArr;
@property (nonatomic,strong)UITableView *lotteryTableView;

@property (nonatomic,weak)id<PDLotteryGameDetailLotteryViewControllerDelegate> delegate;
-(void)loginSuccessManagerBlock:(void(^)())block;

#pragma mark - 通过坐标转换到当前
-(void)getCurrentLocationToManager:(CGPoint)point;
@end
