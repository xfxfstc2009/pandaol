//
//  PDLotteryGameDetailBackgroundView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/17.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDLotteryGameDetailBackgroundView.h"
#import "DKLiveBlurView.h"
#import "PDScrollView.h"
@interface PDLotteryGameDetailBackgroundView()<UIScrollViewDelegate>
@property (nonatomic,strong)DKLiveBlurView *backgroundImageView;
@property (nonatomic,strong)UIView *alphaView;
@property (nonatomic,strong)PDImageView *backgroundImgView;

@end

@implementation PDLotteryGameDetailBackgroundView

-(instancetype)init{
    self = [super init];
    if (self){
        
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame pageType:(BackgroundPageType)pageType{
    self = [super initWithFrame:frame];
    if(self){
        _transferBackgroundType = pageType;
        self.transferBackgroundImgHeight = 380;
        [self initialize];
    }
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if(self){
        self.transferBackgroundImgHeight = 380;
        [self initialize];
    }
    return self;
}

#pragma mark - createView
-(void)initialize{
    if (self.transferBackgroundType == BackgroundPageTypeBanner){           // banner
        [self setupBanner];               // 创建背景
    } else if (self.transferBackgroundType == BackgroundPageTypeBackgroundImage){
        [self setupBackgroundImgView];               // 创建背景
    }
    [self setupMainScrollView];
    self.autoresizesSubviews = YES;

}


#pragma mark 创建背景图片
-(void)setupBackgroundImgView{
    self.backgroundImageView = [[DKLiveBlurView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, self.transferBackgroundImgHeight)];
    self.backgroundImageView.backgroundColor = [UIColor clearColor];
    self.backgroundImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.backgroundImageView.clipsToBounds = YES;
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.backgroundImageView];
    self.backgroundImageView.image = self.transferBackgroundImg;
    
    // 2. 创建阴影
    self.alphaView = [[UIView alloc]init];
    self.alphaView.backgroundColor = c7;
    self.alphaView.alpha = .5f;
    self.alphaView.frame = self.backgroundImageView.bounds;
    [self.backgroundImageView addSubview:self.alphaView];
    
    self.backgroundImgView = [[PDImageView alloc]init];
    self.backgroundImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.backgroundImgView.size_height);
    
}

#pragma mark - 创建scrollview
-(void)setupMainScrollView{
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        self.mainScrollView.frame = kScreenBounds;
        self.mainScrollView.delegate = self;
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.height + self.transferHeaderHeight);
        self.mainScrollView.pagingEnabled = NO;
        self.mainScrollView.alwaysBounceVertical = NO;
        self.mainScrollView.showsVerticalScrollIndicator = YES;
        self.mainScrollView.showsHorizontalScrollIndicator = YES;
        [self addSubview:self.mainScrollView];
        
        
        // 添加观察者
        void *context = (__bridge void *)self;
        [self.mainScrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:context];

    }
}

-(void)setTransferBackgroundImg:(UIImage *)transferBackgroundImg{
    _transferBackgroundImg = transferBackgroundImg;
    self.backgroundImageView.originalImage = transferBackgroundImg;
    [self frameRectUpdate];
}

-(void)setTransferBackgroundImgHeight:(CGFloat)transferBackgroundImgHeight{
    _transferBackgroundImgHeight = transferBackgroundImgHeight;
    [self frameRectUpdate];
}

-(void)setTransferHeaderHeight:(CGFloat)transferHeaderHeight{
    _transferHeaderHeight = transferHeaderHeight;
    [self frameRectUpdate];
}

-(void)setTransferBackgroundUrl:(NSString *)transferBackgroundUrl{
    _transferBackgroundUrl = transferBackgroundUrl;
    __weak typeof(self)weakSelf = self;
    if (self.backgroundImgView){
        [self.backgroundImgView uploadImageWithURL:transferBackgroundUrl placeholder:nil callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.backgroundImageView.originalImage = image;
        }];
    }
}

-(void)frameRectUpdate{
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.height + self.transferHeaderHeight);
    self.backgroundImageView.size_height = self.transferBackgroundImgHeight;
}


-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context{
    if (context != (__bridge void *)self)
    {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
        return;
    }
    
    if ((object == self.mainScrollView) && ([keyPath isEqualToString:@"contentOffset"] == YES))
    {
        if (self.isShowNavAnimation){
            [self scrollViewDidScrollWithOffset:self.mainScrollView.contentOffset.y];
        }
        return;
    }
}

- (void)scrollViewDidScrollWithOffset:(CGFloat)scrollOffset {
    
    CGPoint scrollViewDragPoint = self.mainScrollView.contentOffset;
    
    // navBar变色
    [self animateNavigationBar:scrollOffset draggingPoint:scrollViewDragPoint];
    // 2. 背景图片变色
    [self scrollViewDidScrollWithBackground:scrollOffset];
//
//    if (self.delegate && [self.delegate respondsToSelector:@selector(detailsPage:scrollViewWithScrollOffset:)]){
//        [self.delegate detailsPage:self scrollViewWithScrollOffset:scrollOffset];
//    }
//    NSLog(@"%.2f",scrollViewDragPoint.y);
}



#pragma mark - navigation
- (void)animateNavigationBar:(CGFloat)scrollOffset draggingPoint:(CGPoint)scrollViewDragPoint {
    CGFloat alpha_margin = scrollOffset / self.transferHeaderHeight;
    if (alpha_margin > 1){
        alpha_margin = 1;
    }
    _transferNavView.alpha = alpha_margin;
}

-(void)animateNavigationBar:(CGPoint)currentPoint maxHeight:(CGFloat)maxHeight{
//    NSLog(@"currentPoint======>%.2f",currentPoint.y);
//    NSLog(@"maxHeight======>%.2f",maxHeight);
    CGFloat alpha_margin = currentPoint.y / maxHeight;
    if (alpha_margin > 1){
        alpha_margin = 1;
    }
    _transferNavView.alpha = alpha_margin;
}

-(void)setTransferNavView:(UIView *)transferNavView{
    if (_transferNavView == transferNavView){
        return;
    }
    _transferNavView = transferNavView;
    _transferNavView.alpha = 0.f;
}


-(void)dealloc{
    [self.mainScrollView removeObserver:self forKeyPath:@"contentOffset"];
}

-(void)scrollViewDidScrollWithBackground:(CGFloat)pointY{
    [self.backgroundImageView setBlurLevel:(pointY / self.transferHeaderHeight + .3f)];
}

#pragma mark - 资讯页面使用
-(void)setupBanner{
    // 2. 创建北京scrollView
    self.headerScrollView = [[PDScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(200))];
    self.headerScrollView.backgroundColor = BACKGROUND_VIEW_COLOR;
    __weak typeof(self)weakSelf = self;
    [self.headerScrollView bannerImgTapManagerWithInfoblock:^(PDScrollViewSingleModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
//        if ([singleModel.actionType isEqualToString:@"treasure"]){              // 夺宝活动
//            PDProductDetailViewController *productDetailViewController = [[PDProductDetailViewController alloc]init];
//            productDetailViewController.productId = singleModel.actionEntityId;
//            [productDetailViewController hidesTabBarWhenPushed];
//            [strongSelf.navigationController pushViewController:productDetailViewController animated:YES];
//        } else if ([singleModel.actionType isEqualToString:@"news"]){           // 新闻资讯
//            PDFindInformationDetailViewController *webViewController = [[PDFindInformationDetailViewController alloc]init];
//            NSArray *ids = [singleModel.actionEntityId componentsSeparatedByString:@","];
//            NSString *webUrl = InformationDetail([ids objectAtIndex:0], [ids objectAtIndex:1]);
//            [webViewController webDirectedWebUrl:webUrl];
//            webViewController.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:webViewController animated:YES];
//        }
    }];
    [self addSubview:self.headerScrollView];

}


@end
