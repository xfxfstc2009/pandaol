//
//  PDLotteryGameDetailMainViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDLotteryGameDetailMainViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferMatchId;

@property (nonatomic,assign)CGRect transferLeftRect;
@property (nonatomic,assign)CGRect transferRightRect;
@property (nonatomic,strong)UIImage *transferLeftImg;
@property (nonatomic,strong)UIImage *transferRightImg;


@end
