//
//  PDLotteryGameDetailInfoViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameDetailInfoViewController.h"
#import "PDLotteryGameDetailProgressCell.h"
#import "PDLotteryDetailNormalTableViewCell.h"

@interface PDLotteryGameDetailInfoViewController ()<UITableViewDelegate,UITableViewDataSource>

@end

@implementation PDLotteryGameDetailInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createTableView];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = [UIColor clearColor];
}

#pragma mark - createTableView
-(void)createTableView{
    if(!self.detailTableView){
        self.detailTableView= [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
        self.detailTableView.delegate = self;
        self.detailTableView.dataSource = self;
        self.detailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.detailTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.detailTableView.showsVerticalScrollIndicator = YES;
        self.detailTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
        [self.view addSubview:self.detailTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.detailTableView appendingPullToRefreshHandler:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.delegate && [strongSelf.delegate respondsToSelector:@selector(scrollViewDelegateWithDetailManager:andDrop:)]){
            [strongSelf.delegate scrollViewDelegateWithDetailManager:strongSelf.detailTableView andDrop:YES];
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.1f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [strongSelf.detailTableView stopPullToRefresh];
        });
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        return 1;
    } else {
        return self.detailMutableArr.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PDLotteryGameDetailProgressCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if(!cellWithRowThr){
            cellWithRowThr = [[PDLotteryGameDetailProgressCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferRootModel = self.transferMatchSingleModel;
        cellWithRowThr.transferMainModel = self.transferGuessMainSingleModel;
        return cellWithRowThr;
    } else {
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        PDLotteryDetailNormalTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[PDLotteryDetailNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.transferCellheight = cellHeight;
        cellWithRowFour.index = indexPath.row;
        PDMatchguessinfoGuessStakeListSingleModel *stakeListSingleModel = [self.detailMutableArr objectAtIndex:indexPath.row];
        cellWithRowFour.transferDetailNormalModel = stakeListSingleModel;
        return cellWithRowFour;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        return [PDLotteryGameDetailProgressCell calculationCellHeight];
    } else {
        return [PDLotteryDetailNormalTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(24);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    if(section == 0){
        UIView *tagView = [[UIView alloc]init];
        tagView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
        tagView.frame = CGRectMake(0, LCFloat(3), LCFloat(3), LCFloat(24) - LCFloat(6));
        [headerView addSubview:tagView];
        
        UILabel *fixedLabel = [[UILabel alloc]init];
        fixedLabel.backgroundColor = [UIColor clearColor];
        fixedLabel.font = [UIFont systemFontOfCustomeSize:11.];
        fixedLabel.text = @"竞猜概况";
        fixedLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        fixedLabel.frame = CGRectMake(CGRectGetMaxX(tagView.frame) + LCFloat(11), 0, kScreenBounds.size.width, LCFloat(24));
        [headerView addSubview:fixedLabel];
    } else {
        UILabel *label1 = [[UILabel alloc]init];
        label1.backgroundColor = [UIColor clearColor];
        label1.font = [UIFont systemFontOfCustomeSize:11.];
        label1.text = @"排名";
        label1.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        label1.textAlignment = NSTextAlignmentCenter;
        label1.frame = CGRectMake(0, 0, [PDLotteryDetailNormalTableViewCell numberWidth] + 2 * LCFloat(11), LCFloat(24));
        [headerView addSubview:label1];
        
        // 2. 创建投注金额
        UILabel *jineLabel = [[UILabel alloc]init];
        
        jineLabel.backgroundColor = [UIColor clearColor];
        jineLabel.font = [UIFont systemFontOfCustomeSize:11.];
        jineLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        jineLabel.text = @"投注金额";
        CGSize jineSize = [jineLabel.text sizeWithCalcFont:jineLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:jineLabel.font])];
        jineLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - jineSize.width, 0, jineSize.width, LCFloat(24));
        [headerView addSubview:jineLabel];
        
        // 3. 创建用户
        UILabel *userLabel = [[UILabel alloc]init];
        userLabel.backgroundColor = [UIColor clearColor];
        userLabel.frame = CGRectMake(CGRectGetMaxX(label1.frame) + LCFloat(30), 0, 100, LCFloat(24));
        userLabel.font = [UIFont systemFontOfCustomeSize:11.];
        userLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        userLabel.text = @"用户";
        [headerView addSubview:userLabel];
    }
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.detailTableView) {
        if (indexPath.section == 1){
            SeparatorType separatorType = SeparatorTypeMiddle;
            if ( [indexPath row] == 0) {
                separatorType = SeparatorTypeHead;
            } else if ([indexPath row] == [self.detailMutableArr count] - 1) {
                separatorType = SeparatorTypeBottom;
            } else {
                separatorType = SeparatorTypeMiddle;
            }
            if ([self.detailMutableArr count] == 1) {
                separatorType = SeparatorTypeSingle;
            }
            [cell addSeparatorLineWithType:separatorType];
        }
    }
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    if (self.delegate && [self.delegate respondsToSelector:@selector(scrollViewDelegateWithDetailManager: andDrop:)]){
        [self.delegate scrollViewDelegateWithDetailManager:scrollView andDrop:NO];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = LCFloat(24);
    if (scrollView.contentOffset.y <= sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

-(void)setTransferMatchSingleModel:(PDMatchguessinfoSingleModel *)transferMatchSingleModel{
    _transferMatchSingleModel = transferMatchSingleModel;
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:0];
    [self.detailTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
}
@end
