//
//  PDLotteryGameRootTableView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PDLotteryGameRootTableViewDelegate <NSObject>

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionForFlodTableView:(UITableView *)tableView;                               /**< 显示多少个section*/
-(NSInteger)foldTableView:(UITableView *)tableView numberOfRowInSection:(NSInteger)section;         /**< 一组显示多少个row */
-(UITableViewCell *)foldTableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;  /**< cell*/
#pragma mark - UITableViewDelegate
-(CGFloat)foldTableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section;           /**< 返回高度*/
-(CGFloat)foldTableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath;
-(void)foldTableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath ;
-(UIView *)foldTableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section;
-(void)foldTableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath;
@end

@interface PDLotteryGameRootTableView : UITableView

@property (nonatomic,weak)id<PDLotteryGameRootTableViewDelegate> foldDelegate;              /**< 代理*/
-(void)foldFoldingSectionHeaderTappedAtIndex:(NSInteger)index ;
@end
