//
//  PDLotteryGameMainViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/15.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDLotteryGameMainViewController.h"
#import "PDLotteryGameMainHeaderView.h"
#import "PDLotteryGameRootTableView.h"
#import "HTHorizontalSelectionList.h"
#import "PDInformationSegmentSingleModel.h"
#import "PDLotteryGameMenuListModel.h"
#import "PDLoteryGameRootListModel.h"
#import "PDLotteryGameSubViewCell.h"
#import "PDLotteryGameDetailRootViewController.h"
#import "PDLotteryGameMainHeaderCell.h"
#import "PDLotteryGameDetailMainViewController.h"

@interface PDLotteryGameMainViewController()<UITableViewDelegate,UITableViewDataSource,HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate,UIScrollViewDelegate>{

}
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)NSMutableArray *segmentMutableArr;

@property (nonatomic,strong)UIScrollView *tableViewMainScrollView;          /**< scrollView*/
@property (nonatomic,strong)NSMutableDictionary *tableViewMutableDic;       /**< 列表的字典*/
@property (nonatomic,strong)NSMutableDictionary *dataSourceMutableDic;      /**< 数据源字典*/
@property (nonatomic,strong)NSMutableArray *currentMutableArr;              /**< 当前的数组*/

@end

@implementation PDLotteryGameMainViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createSegment];
    [self createBaseScrollView];
    [self InterfaceManager];
}

#pragma mark - InterfaceManager
-(void)InterfaceManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetSegmentList];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"赛事竞猜";
    self.navigationController.navigationBar.layer.shadowColor = [[UIColor clearColor] CGColor];
    __weak typeof(self)weakSelf = self;
    
    // right
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_lottery_rule"] barHltImage:[UIImage imageNamed:@"icon_lottery_rule"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWebUrl:gzgz];
        [strongSelf.navigationController pushViewController:webViewController animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.segmentMutableArr = [NSMutableArray array];
    self.tableViewMutableDic = [NSMutableDictionary dictionary];            // 2. tableView
    self.dataSourceMutableDic = [NSMutableDictionary dictionary];           // 3. 数据源
    self.currentMutableArr = [NSMutableArray array];                        // 5. 当前数据源
}


#pragma mark - Segment
-(void)createSegment{
    self.segmentList = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(44))];
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    self.segmentList.selectionIndicatorColor = c15;
    self.segmentList.bottomTrimColor = [UIColor clearColor];
    self.segmentList.isNotScroll = YES;
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.segmentList.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self.view addSubview:self.segmentList];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentMutableArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    PDLotteryGameMenuSingleModel *segmentItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
    return segmentItemSingleModel.fullName;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self.tableViewMainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
    [self hasTableAndManagerWithIndex:index];
}

#pragma mark - createScrollView
-(void)createBaseScrollView{
    if (!self.tableViewMainScrollView){
        self.tableViewMainScrollView = [[UIScrollView alloc]init];
        self.tableViewMainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(self.segmentList.frame));
        self.tableViewMainScrollView.delegate = self;
        self.tableViewMainScrollView.backgroundColor = [UIColor clearColor];
        self.tableViewMainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width , self.tableViewMainScrollView.size_height);
        self.tableViewMainScrollView.pagingEnabled = YES;
        self.tableViewMainScrollView.alwaysBounceVertical = NO;
        self.tableViewMainScrollView.showsVerticalScrollIndicator = NO;
        self.tableViewMainScrollView.showsHorizontalScrollIndicator = NO;
        [self.view addSubview:self.tableViewMainScrollView];
    }
}

#pragma mark- UITableViewDataSource
#pragma makr - 临时创建一个tableView
-(void)hasTableAndManagerWithIndex:(NSInteger)index{
    // 2. 判断当前是否存在tableView
    __weak typeof(self)weakSelf = self;
    if (![self.tableViewMutableDic.allKeys containsObject:[NSString stringWithFormat:@"%li",(long)index]]){            // 如果不存在
        if (self.currentMutableArr.count){
            [self.currentMutableArr removeAllObjects];
        }
        UITableView *tableView = [self createTableView];                            // 创建tableView
        tableView.orgin_x = index *kScreenBounds.size.width;                        // 修改frame
        
        [self.tableViewMutableDic setObject:tableView forKey:[NSString stringWithFormat:@"%li",(long)index]];
        
        PDLotteryGameMenuSingleModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
        studyItemSingleModel.tableIndex = [NSString stringWithFormat:@"%li",(long)index];
        [weakSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:NO];
    } else {            // 已经存在
        PDLotteryGameMenuSingleModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
        [weakSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:YES];
    }
}

-(UITableView *)createTableView{
    UITableView *tableView= [[UITableView alloc] initWithFrame:self.tableViewMainScrollView.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.size_height = self.tableViewMainScrollView.size_height;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.showsVerticalScrollIndicator = YES;
    tableView.backgroundColor = [UIColor clearColor];
    [self.tableViewMainScrollView addSubview:tableView];
    
    __weak typeof(self)weakSelf = self;
    PDLotteryGameMenuSingleModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:self.segmentList.selectedButtonIndex];
    // 下拉刷新
    [tableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:YES];
    }];
    
    [tableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithItemId:studyItemSingleModel isHorizontal:NO];
    }];
    
    return tableView;
}


#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.currentMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    PDLoteryGameRootSingleModel *lotteryWithDateModel = [self.currentMutableArr objectAtIndex:section];
    return lotteryWithDateModel.subList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    PDLotteryGameMainHeaderCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDLotteryGameMainHeaderCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    
    PDLotteryGameMenuSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    if (tableView == currentTableView){         // 获取当前的model
        // 1.获取当前tableView的key
        NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.ID];
        PDLoteryGameRootSingleModel *lotteryWithDateModel = [currentArr objectAtIndex:indexPath.section];
        if (lotteryWithDateModel.subList.count){
            PDLoteryGameRootSubSingleModel *lotteryGameRootSubSingleModel = [lotteryWithDateModel.subList objectAtIndex:indexPath.row];
            if (indexPath.row == 0){
                cellWithRowOne.transferType = LotterGameHeaderTypeTime;
            } else {
                cellWithRowOne.transferType = LotterGameHeaderTypeNormal;
            }
            cellWithRowOne.transferGameRootSubSingleModel = lotteryGameRootSubSingleModel;
        }
    }
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
   
    PDLotteryGameMenuSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    if (tableView == currentTableView){         // 获取当前的model
        // 1.获取当前tableView的key
        NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.ID];
        PDLoteryGameRootSingleModel *lotteryWithDateModel = [currentArr objectAtIndex:indexPath.section];
        if (lotteryWithDateModel.subList.count){
//            PDLoteryGameRootSubSingleModel *lotteryGameRootSubSingleModel = [lotteryWithDateModel.subList objectAtIndex:indexPath.row];
            if (indexPath.row == 0){
                return [PDLotteryGameMainHeaderCell calculationCellHeightWithType:LotterGameHeaderTypeTime];
            } else {
                return [PDLotteryGameMainHeaderCell calculationCellHeightWithType:LotterGameHeaderTypeNormal];
            }
        }
        return [PDLotteryGameMainHeaderCell calculationCellHeightWithType:LotterGameHeaderTypeTime];
    }
    return [PDLotteryGameMainHeaderCell calculationCellHeightWithType:LotterGameHeaderTypeTime];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    PDLotteryGameMenuSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    if (tableView == currentTableView){         // 获取当前的model
        // 1.获取当前tableView的key
        NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.ID];
        PDLoteryGameRootSingleModel *lotteryWithDateModel = [currentArr objectAtIndex:indexPath.section];
        if (lotteryWithDateModel.subList.count){
            PDLoteryGameRootSubSingleModel *lotteryGameRootSubSingleModel = [lotteryWithDateModel.subList objectAtIndex:indexPath.row];
            PDLotteryGameDetailMainViewController *lotteryGameDetailMainViewController = [[PDLotteryGameDetailMainViewController alloc]init];
            lotteryGameDetailMainViewController.transferMatchId = lotteryGameRootSubSingleModel.matchId;
            [self.navigationController pushViewController:lotteryGameDetailMainViewController animated:YES];
            return;
        }
    }
}

#pragma mark - InterfaceManager
-(void)sendRequestToGetSegmentList{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:gamelist requestParams:nil responseObjectClass:[PDLotteryGameMenuListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.segmentMutableArr removeAllObjects];
            PDLotteryGameMenuListModel *listModel = (PDLotteryGameMenuListModel *)responseObject;
            [strongSelf.segmentMutableArr addObjectsFromArray:listModel.game];
            [strongSelf.segmentList reloadData];
            strongSelf.tableViewMainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * strongSelf.segmentMutableArr.count, self.tableViewMainScrollView.size_height);
            
            if (strongSelf.segmentMutableArr.count){
                // 1. 获取下面的内容
                [strongSelf hasTableAndManagerWithIndex:0];
                [strongSelf.view dismissPrompt];
            } else {
                [strongSelf.view showPrompt:@"当前没有竞猜项，请稍后再试" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
        }
    }];
}

#pragma mark 获取下面的赛事
-(void)sendRequestToGetInfoWithItemId:(PDLotteryGameMenuSingleModel *)itemSingleModel isHorizontal:(BOOL)isHorizontal{
 
    __weak typeof(self)weakSelf = self;
    //1. 找到当前的tableView
    UITableView *currentTableView = (UITableView *)[self.tableViewMutableDic objectForKey:[NSString stringWithFormat:@"%li",(long)self.segmentList.selectedButtonIndex]];
    
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:currentTableView.currentPage forKey:@"page"];
//    [params setValue:@"5" forKey:@"size"];
    if (![itemSingleModel.ID isEqualToString:@"-1"]){
        [params setValue:itemSingleModel.ID forKey:@"gameId"];
    }
    
    //【HUD】
    NSString *info = [NSString stringWithFormat:@"正在获取【%@】赛事列表…",itemSingleModel.fullName];
    [PDHUD showHUDProgress:info diary:0];
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:matchguesslist requestParams:params responseObjectClass:[PDLoteryGameRootListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [PDHUD dismiss];
        if (isSucceeded){
            // 1.获取到当前model
            PDLoteryGameRootListModel *customerListModel = (PDLoteryGameRootListModel *)responseObject;
            // 2. 判断是否下啦
            if (isHorizontal){
                [strongSelf.dataSourceMutableDic removeObjectForKey:itemSingleModel.ID];
            }
            
            
            for (int i = 0 ; i < customerListModel.lotteryList.count;i++){
                // 1. 获取当前所有的赛事
                PDLoteryGameRootSingleModel *lotteryGameRootSingleModel = [customerListModel.lotteryList objectAtIndex:i];
                for (int j = 0 ; j < lotteryGameRootSingleModel.subList.count;j++){
                    PDLoteryGameRootSubSingleModel *subSingleModel = [lotteryGameRootSingleModel.subList objectAtIndex:j];
                    subSingleModel.gameStartTempTime = [NSDate getNSTimeIntervalWithCurrent] + subSingleModel.matchLeftTime;
                    if (j == 0){
                        subSingleModel.currentTempTime = lotteryGameRootSingleModel.dateTime;
                    } else {
                        subSingleModel.currentTempTime = -1;
                    }
                    
                    for (int k = 0 ; k < subSingleModel.lotteryDetailList.count;k++){
                        PDLotteryGameSubListSingleModel *subListSingleModel = [subSingleModel.lotteryDetailList objectAtIndex:k];
                        subListSingleModel.endTime = [NSDate getNSTimeIntervalWithCurrent] + subListSingleModel.leftTime;
                    }
                }
            }
            
            // 3. 判断
            NSMutableArray *appendingMutableArr = [NSMutableArray array];
            if (![strongSelf.dataSourceMutableDic.allKeys containsObject:itemSingleModel.ID]){          // 【不存在】
                [strongSelf.dataSourceMutableDic setObject:customerListModel.lotteryList forKey:itemSingleModel.ID];
            } else {                                                                        // 存在
                // 2.获取已经存在的数据源
                NSArray *lastArr = [strongSelf.dataSourceMutableDic objectForKey:itemSingleModel.ID];
                if (customerListModel.lotteryList.count > 1){      // 需要拆分
                    // 1. 获取当前第一条的数据
                    PDLoteryGameRootSingleModel *currentLotteryGameRootSingleModel = [customerListModel.lotteryList firstObject];
                    // 3. 获取已经存在的最后一条数据源
                    PDLoteryGameRootSingleModel *lastLotteryGameRootSingleModel = [lastArr lastObject];
                    // 4. 判断是不是同一天的数据
                    if (currentLotteryGameRootSingleModel.dateTime == lastLotteryGameRootSingleModel.dateTime){     // 是同一天
                        NSMutableArray *tempArr = [NSMutableArray array];
                        [tempArr addObjectsFromArray:lastLotteryGameRootSingleModel.subList];
                        [tempArr addObjectsFromArray:currentLotteryGameRootSingleModel.subList];
                        lastLotteryGameRootSingleModel.subList = (NSArray<PDLoteryGameRootSubSingleModel> *)tempArr;
                        // 移除上一个最后一个对象
                        NSMutableArray *tempMutableArr = [NSMutableArray arrayWithArray:lastArr];
                        [tempMutableArr removeLastObject];
                        [tempMutableArr addObject:lastLotteryGameRootSingleModel];
                        // 添加现在数据
                        NSMutableArray *tempCurrentMutableArr = [NSMutableArray arrayWithArray:customerListModel.lotteryList];
                        [tempCurrentMutableArr removeObjectAtIndex:0];
                        [tempMutableArr addObjectsFromArray:tempCurrentMutableArr];
                        
                        [strongSelf.dataSourceMutableDic setObject:tempMutableArr forKey:itemSingleModel.ID];
                    } else {                                                                                        // 不是同一天
                        [appendingMutableArr addObjectsFromArray:lastArr];
                        [appendingMutableArr addObjectsFromArray:customerListModel.lotteryList];
                        [strongSelf.dataSourceMutableDic setObject:appendingMutableArr forKey:itemSingleModel.ID];
                    }
                } else {                                           // 不需要拆分
                    if (customerListModel.lotteryList.count == 1){
                        PDLoteryGameRootSingleModel *currentLotteryGameRootSingleModel = [customerListModel.lotteryList firstObject];
                        [appendingMutableArr addObjectsFromArray:lastArr];
                        [appendingMutableArr addObject:currentLotteryGameRootSingleModel];
                        [strongSelf.dataSourceMutableDic setObject:appendingMutableArr forKey:itemSingleModel.ID];
                    }
                }
            }

            // 4. 获取当前的数据源
            NSArray *currentDataSourceArr = [strongSelf.dataSourceMutableDic objectForKey:itemSingleModel.ID];
            
            if (strongSelf.currentMutableArr.count){
                [strongSelf.currentMutableArr removeAllObjects];
            }
            [strongSelf.currentMutableArr addObjectsFromArray:currentDataSourceArr];
            
            // dismiss
            [currentTableView stopPullToRefresh];
            [currentTableView stopFinishScrollingRefresh];
            
            // placeholder
            
            [currentTableView reloadData];
            
            if (!strongSelf.currentMutableArr.count){
                [currentTableView showPrompt:@"当前没有该赛事信息" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            } else {
                [currentTableView dismissPrompt];
            }
        }
    }];
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.tableViewMainScrollView){
        CGPoint point = scrollView.contentOffset;
        NSInteger page = point.x / kScreenBounds.size.width;
        [self.segmentList setSelectedButtonIndex:page animated:YES];
        
        [self hasTableAndManagerWithIndex:page];
        
        return;
    }
}


#pragma mark - 显示新手引导
-(void)showGesture{
    return;
    if (!self.segmentMutableArr.count){
        return;
    }
    
    PDLotteryGameMenuSingleModel *itemSingleModel = [self.segmentMutableArr objectAtIndex:0];
    NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.ID];

    if (!currentArr.count){
        return;
    }
    
    
    PDGuideRootViewController *guideRootViewController = [[PDGuideRootViewController alloc]init];
    PDGuideSingleModel *singleModel = [[PDGuideSingleModel alloc]init];
    
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    PDLotteryGameMainHeaderCell *cell = [currentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0  ]];
    
    CGRect leftConvertFrame = [currentTableView convertRect:cell.headerView.frame fromView:[UIApplication sharedApplication].keyWindow];
    
    
    singleModel.transferShowFrame = CGRectMake(0,ABS(leftConvertFrame.origin.y) , kScreenBounds.size.width,[PDLotteryGameMainHeaderView calculationCellHeightWithType:LotterGameHeaderTypeTime]);
    singleModel.guideType = GuideGuesterTypeTap;
    singleModel.text = @"点击一场竞猜中的赛事";
    singleModel.foundationType = foundationTypeRect;
    
    __weak typeof(self)weakSelf = self;
    [guideRootViewController actionClicDismisWithGameLotteryManager:^{                  // 【进行跳转】
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        //     【跳转到详情】
        
        PDLotteryGameMenuSingleModel *itemSingleModel = [strongSelf.segmentMutableArr objectAtIndex: strongSelf.segmentList.selectedButtonIndex];
        UITableView *currentTableView = [strongSelf.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
        if (currentTableView){         // 获取当前的model
            // 1.获取当前tableView的key
            NSArray *currentArr = [strongSelf.dataSourceMutableDic objectForKey:itemSingleModel.ID];
            if (!currentArr.count){
                return;
            }
            PDLoteryGameRootSingleModel *lotteryWithDateModel = [currentArr objectAtIndex:0];
            if (lotteryWithDateModel.subList.count){
                PDLoteryGameRootSubSingleModel *lotteryGameRootSubSingleModel = [lotteryWithDateModel.subList objectAtIndex:0];
                PDLotteryGameDetailMainViewController *lotteryGameDetailMainViewController = [[PDLotteryGameDetailMainViewController alloc]init];
                lotteryGameDetailMainViewController.transferMatchId = lotteryGameRootSubSingleModel.matchId;
                [strongSelf.navigationController pushViewController:lotteryGameDetailMainViewController animated:YES];
                
                [guideRootViewController lotteryRootManagerBlock:^{
                    
                }];
                return;
            }
        }
    }];
    [guideRootViewController showInView:self.parentViewController withViewActionArr:@[singleModel]];
    
}

@end
