//
//  PDLotteryGameMainHeaderCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/15.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLoteryGameRootSubSingleModel.h"
#import "PDLotteryGameMainHeaderView.h"

@interface PDLotteryGameMainHeaderCell : UITableViewCell

@property (nonatomic,strong)PDLotteryGameMainHeaderView *headerView;
@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,assign)LotterGameHeaderType transferType;
@property (nonatomic,strong)PDLoteryGameRootSubSingleModel *transferGameRootSubSingleModel;     /**< 其他类型的Model*/


+(CGFloat)calculationCellHeightWithType:(LotterGameHeaderType)type;

@end
