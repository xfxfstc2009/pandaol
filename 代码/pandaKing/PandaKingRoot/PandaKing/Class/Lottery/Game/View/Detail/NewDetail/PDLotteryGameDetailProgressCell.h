//
//  PDLotteryGameDetailProgressCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 竞猜情况里面的progress;

#import <UIKit/UIKit.h>
#import "PDMatchguessinfoGuessListSingleModel.h"
#import "PDMatchguessinfoSingleModel.h"

@interface PDLotteryGameDetailProgressCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDMatchguessinfoGuessListSingleModel *transferMainModel;
@property (nonatomic,strong)PDMatchguessinfoSingleModel *transferRootModel;
+(CGFloat)calculationCellHeight;

@end
