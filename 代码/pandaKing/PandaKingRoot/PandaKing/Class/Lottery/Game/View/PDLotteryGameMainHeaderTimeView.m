//
//  PDLotteryGameMainHeaderTimeView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameMainHeaderTimeView.h"

@interface PDLotteryGameMainHeaderTimeView()
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)PDImageView *timeImgView;
@property (nonatomic,strong)NSTimer *timer;

@end

@implementation PDLotteryGameMainHeaderTimeView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.timeImgView = [[PDImageView alloc]init];
    self.timeImgView.backgroundColor = [UIColor clearColor];
    self.timeImgView.image = [UIImage imageNamed:@""];
    [self addSubview:self.timeImgView];
}



//-(instancetype)initWithFrame:(CGRect)frame daojishi:(NSInteger)daojishi withBlock:(void(^)())block{
//    self = [super initWithFrame:frame];
//    if (self){
//        _timeInteger = daojishi;
//        objc_setAssociatedObject(self, &PDCountDownButtonKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
//        [self createView];
//    }
//    return self;
//}
//
//-(void)createView{
//    // 1.  创建label
//    self.dymicLabel = [[UILabel alloc]init];
//    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
//    self.dymicLabel.textAlignment = NSTextAlignmentCenter;
//    self.dymicLabel.frame = self.bounds;
//    [self smsLabelTypeManager:YES];
//    [self addSubview:self.dymicLabel];
//    
//    
//    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
//    self.button.frame = self.bounds;
//    [self addSubview:self.button];
//    __weak typeof(self)weakSelf = self;
//    
//    [self.button buttonWithBlock:^(UIButton *button) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        void(^block)() = objc_getAssociatedObject(strongSelf, &PDCountDownButtonKey);
//        if(block){
//            block();
//        }
//    }];
//}
//
//-(void)startTimer{
//    [self smsLabelTypeManager:NO];
//    self.timeInteger = 60;
//    if (!self.timer){
//        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
//    }
//}
//
//#pragma mark - 倒计时方法
//-(void)daojishiManager{
//    --self.timeInteger;
//    if (self.timeInteger < 0){
//        [self smsLabelTypeManager:YES];
//        if (self.timer){
//            [self.timer invalidate];
//            self.timer = nil;
//        }
//    } else {
//        [self smsLabelTypeManager:NO];
//        self.dymicLabel.attributedText = [Tool rangeLabelWithContent:[NSString stringWithFormat:@"验证码%lis",(long)self.timeInteger]  hltContentArr:@[[NSString stringWithFormat:@"%li",(long)self.timeInteger]] hltColor:c9 normolColor:c3];
//    }
//}
//
//-(void)smsLabelTypeManager:(BOOL)type{
//    if (type == YES){
//        self.button.enabled = YES;
//        self.dymicLabel.textColor = c3;
//        self.dymicLabel.text = @"";
//        self.dymicLabel.attributedText = [Tool rangeLabelWithContent:[NSString stringWithFormat:@"获取验证码"] hltContentArr:@[] hltColor:c3 normolColor:c3];
//    } else {
//        self.button.enabled = NO;
//    }
//}
//


@end
