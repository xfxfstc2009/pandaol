//
//  PDCustomerNavBarView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/12.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDCustomerNavBarView.h"

@interface PDCustomerNavBarView()
@property (nonatomic,strong)UIButton *navLeftButton;
@property (nonatomic,strong)UIButton *navRightButton;
@property (nonatomic,strong)UILabel  *titleLabel;

@end

@implementation PDCustomerNavBarView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        
    }
    return self;
}

-(instancetype) init{
    self = [super init];
    if (self){
    
    }
    return self;
}

- (nonnull UIButton *)leftBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock {
    if (!self.navLeftButton){
        self.navLeftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:self.navLeftButton];
    }
    
    self.navLeftButton.backgroundColor = [UIColor clearColor];
    self.navLeftButton.frame = CGRectMake(0, 20, LCFloat(60), 44);
    [self.navLeftButton setTitle:title forState:UIControlStateNormal];
    [self.navLeftButton setImage:norImage forState:UIControlStateNormal];
    [self.navLeftButton setImage:hltImage forState:UIControlStateHighlighted];
    __weak typeof(self)weakSelf = self;
    [self.navLeftButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        if(actionBlock){
            actionBlock();
        }
    }];
    return self.navLeftButton;
}

- (nonnull UIButton *)rightBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock{
    if (!self.navRightButton){
        self.navRightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:self.navRightButton];
    }
    
    self.navRightButton.backgroundColor = [UIColor clearColor];
    self.navRightButton.frame = CGRectMake(kScreenBounds.size.width - 60, 20, 60, 44);
    [self.navRightButton setTitle:title forState:UIControlStateNormal];
    [self.navRightButton setImage:norImage forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.navRightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (actionBlock){
            actionBlock();
        }
    }];
    [self.navRightButton setImage:hltImage forState:UIControlStateHighlighted];
    return self.navRightButton;
}

-(void)setBarMainTitle:(NSString *)barMainTitle{
    if (!self.titleLabel){
        self.titleLabel = [[UILabel alloc]init];
        [self addSubview:self.titleLabel];
    }
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.titleLabel.text = barMainTitle;
    self.titleLabel.frame = CGRectMake(60, 20, kScreenBounds.size.width - 2 * LCFloat(60), 44);
}
@end
