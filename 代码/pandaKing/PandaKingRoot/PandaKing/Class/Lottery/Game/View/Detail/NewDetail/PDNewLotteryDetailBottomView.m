//
//  PDNewLotteryDetailBottomView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNewLotteryDetailBottomView.h"
#import "PDCenterMyInfo.h"


static char directToLotteryInfoKey;
static char directToRechargeKey;
@interface PDNewLotteryDetailBottomView()
@property (nonatomic,strong)PDImageView *avatar;
@property (nonatomic,strong)PDImageView*moneyImg;
@property (nonatomic,strong)UILabel *moneyCountLabel;
@property (nonatomic,strong)UILabel *getLabel;
@property (nonatomic,strong)UIButton *lotteryButton;
@property (nonatomic,strong)UIButton *leftButton;
@end

@implementation PDNewLotteryDetailBottomView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.avatar = [[PDImageView alloc]init];
    self.avatar.backgroundColor = [UIColor clearColor];
    self.avatar.frame = CGRectMake(LCFloat(11), LCFloat(5), self.size_height - LCFloat(5) * 2, self.size_height - 2 * LCFloat(5));
    self.avatar.clipsToBounds = YES;
    self.avatar.layer.cornerRadius = self.avatar.size_height / 2;
    self.avatar.layer.borderColor = c26.CGColor;
    self.avatar.layer.borderWidth = .5f;
    [self addSubview:self.avatar];
    
    // 2. 创建账户明爱
    self.nickName = [[UILabel alloc]init];
    self.nickName.backgroundColor = [UIColor clearColor];
    self.nickName.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.nickName.frame = CGRectMake(CGRectGetMaxX(self.avatar.frame) + LCFloat(11), self.avatar.orgin_y, kScreenBounds.size.width / 2. - CGRectGetMaxX(self.avatar.frame) - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.nickName.font]);
    [self addSubview:self.nickName];
    
    // 3.创建钱包
    self.moneyImg = [[PDImageView alloc]init];
    self.moneyImg.backgroundColor = [UIColor clearColor];
    self.moneyImg.image = [UIImage imageNamed:@"icon_center_gold"];
    self.moneyImg.frame = CGRectMake(self.nickName.orgin_x, CGRectGetMaxY(self.nickName.frame) + LCFloat(11), LCFloat(13), LCFloat(13));
    [self addSubview:self.moneyImg];
    
    // 4. 创建钱
    self.moneyCountLabel = [[UILabel alloc]init];
    self.moneyCountLabel.backgroundColor = [UIColor clearColor];
    self.moneyCountLabel.textColor = c26;
    self.moneyCountLabel.adjustsFontSizeToFitWidth = YES;
    self.moneyCountLabel.font = [UIFont systemFontOfCustomeSize:11.];
    [self addSubview:self.moneyCountLabel];
    
    // 5. 创建竞猜按钮
    self.lotteryButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.lotteryButton.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self.lotteryButton setTitle:@"我的竞猜" forState:UIControlStateNormal];
    self.lotteryButton.frame = CGRectMake(kScreenBounds.size.width / 2., 0, kScreenBounds.size.width / 2., self.size_height);
    __weak typeof(self)weakSelf = self;
    [self.lotteryButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &directToLotteryInfoKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.lotteryButton];
    
    // 6. 创建左侧按钮
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.backgroundColor = [UIColor clearColor];
    self.leftButton.frame = CGRectMake(0, 0, kScreenBounds.size.width / 2., self.size_height);
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &directToRechargeKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.leftButton];
    
    // 7.创建获取
    self.getLabel = [[UILabel alloc]init];
    self.getLabel.backgroundColor = c26;
    self.getLabel.textAlignment = NSTextAlignmentCenter;
    self.getLabel.textColor = [UIColor whiteColor];
    self.getLabel.font = [UIFont systemFontOfCustomeSize:11.];
    self.getLabel.text = @"+获取";
    [self addSubview:self.getLabel];
}


-(void)directToMyLottery:(void(^)())block{
    objc_setAssociatedObject(self, &directToLotteryInfoKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)directToRecharge:(void(^)())block{
    objc_setAssociatedObject(self, &directToRechargeKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)sendRequestToGetInfo{
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:myInfo requestParams:nil responseObjectClass:[PDCenterMyInfo class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDCenterMyInfo *gameInfo = (PDCenterMyInfo *)responseObject;
            // 1. 头像
            [self.avatar uploadImageWithURL:gameInfo.member.avatar placeholder:nil callback:NULL];
            // 2. 名字
            self.nickName.text = gameInfo.member.nickname;
            // 3. gold
            self.moneyCountLabel.text = [NSString stringWithFormat:@"%li",gameInfo.gold];
            CGSize moneySize = [self.moneyCountLabel.text sizeWithCalcFont:self.moneyCountLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.moneyCountLabel.font])];
            self.moneyCountLabel.frame = CGRectMake(CGRectGetMaxX(self.moneyImg.frame) + LCFloat(4), 0, moneySize.width, [NSString contentofHeightWithFont:self.moneyCountLabel.font]);
            self.moneyCountLabel.center_y = self.moneyImg.center_y;
            
            // 4. 获取
            CGSize getSize = [self.getLabel.text sizeWithCalcFont:self.getLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.getLabel.font])];
            self.getLabel.frame = CGRectMake(CGRectGetMaxX(self.moneyCountLabel.frame) + LCFloat(3), 0, getSize.width + 2 * LCFloat(3), [NSString contentofHeightWithFont:self.getLabel.font]);
            self.getLabel.layer.cornerRadius = MIN(self.getLabel.size_height, self.getLabel.size_width) / 2.;
            self.getLabel.clipsToBounds = YES;
            self.getLabel.center_y = self.moneyCountLabel.center_y;
        }
    }];
}

@end
