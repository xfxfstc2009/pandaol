//
//  PDLotteryGameMainHeaderCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/15.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDLotteryGameMainHeaderCell.h"
#import "PDLotteryGameMainHeaderView.h"

@interface PDLotteryGameMainHeaderCell()

@end

@implementation PDLotteryGameMainHeaderCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.headerView = [[PDLotteryGameMainHeaderView alloc]init];
    self.headerView.userInteractionEnabled = NO;
    [self addSubview:self.headerView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.headerView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.transferCellHeight);
}

-(void)setTransferGameRootSubSingleModel:(PDLoteryGameRootSubSingleModel *)transferGameRootSubSingleModel{
    _transferGameRootSubSingleModel = transferGameRootSubSingleModel;
    self.headerView.transferGameRootSubSingleModel = transferGameRootSubSingleModel;
}

-(void)setTransferType:(LotterGameHeaderType)transferType{
    _transferType = transferType;
    self.headerView.transferType = transferType;
}

+(CGFloat)calculationCellHeightWithType:(LotterGameHeaderType)type{
    return [PDLotteryGameMainHeaderView calculationCellHeightWithType:type];
}

@end
