//
//  PDLotteryDetailNormalTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryGameDetailBettingDetailModel.h"
#import "PDMatchguessinfoGuessStakeListSingleModel.h"
@interface PDLotteryDetailNormalTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellheight;
@property (nonatomic,strong)PDMatchguessinfoGuessStakeListSingleModel *transferDetailNormalModel;
@property (nonatomic,assign)NSInteger index;

+(CGFloat)calculationCellHeight;

+(CGFloat)numberWidth;
@end

