//
//  PDNewLotteryDetailBottomView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDNewLotteryDetailBottomView : UIView

-(void)directToMyLottery:(void(^)())block;
-(void)directToRecharge:(void(^)())block;

@property (nonatomic,strong)UILabel *nickName;
-(void)sendRequestToGetInfo;

@end
