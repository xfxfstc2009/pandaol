//
//  PDLotteryGameMainHeaderView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLoteryGameRootSubSingleModel.h"
#import "PDLoteryGameRootSingleModel.h"

typedef NS_ENUM(NSInteger,LotterGameHeaderType) {
    LotterGameHeaderTypeTime = 1,
    LotterGameHeaderTypeNormal = 0,
};

typedef NS_ENUM(NSInteger,LotteryCellType) {
    LotteryCellTypeNor = 0,
    LotteryCellTypeHome = 1,
};

@protocol PDLotteryGameMainHeaderViewDelegate <NSObject>

-(void)foldTableViewHeaderDidSelectedWithIndex:(NSInteger)index;

@end

@interface PDLotteryGameMainHeaderView : UIView

@property (nonatomic,weak)id<PDLotteryGameMainHeaderViewDelegate> delegate;
@property (nonatomic,assign)NSInteger transferIndex;
@property (nonatomic,assign)LotterGameHeaderType transferType;
@property (nonatomic,assign)LotteryCellType transferLotteryCellType;                            /**< 首页的赛事竞猜*/
@property (nonatomic,strong)PDLoteryGameRootSubSingleModel *transferGameRootSubSingleModel;     /**< 其他类型的Model*/


+(CGFloat)calculationCellHeightWithType:(LotterGameHeaderType)type;
@end
