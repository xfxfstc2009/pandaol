//
//  PDNewDetailLotteryCellTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMatchguessinfoGuessListRootSingleModel.h"

@interface PDNewDetailLotteryCellTableViewCell : UITableViewCell

@property (nonatomic,strong)PDMatchguessinfoGuessListRootSingleModel *transferLotteryGameSingleModel;
@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UIButton *rightButton;


+(CGFloat)calculationCellHeight;

-(void)actionClickToTouzhuWithLeft:(void(^)(BOOL isLeft))block;

@end
