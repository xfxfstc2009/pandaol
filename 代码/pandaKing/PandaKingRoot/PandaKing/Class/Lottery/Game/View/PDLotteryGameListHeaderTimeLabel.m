//
//  PDLotteryGameListHeaderTimeLabel.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameListHeaderTimeLabel.h"

@interface PDLotteryGameListHeaderTimeLabel()
@property (nonatomic,strong)UILabel *timeLabel;

@end

@implementation PDLotteryGameListHeaderTimeLabel

-(instancetype)init{
    self = [super init];
    if (self){
        [self createLabel];
    }
    return self;
}

#pragma mark - createLabel
-(void)createLabel{
    self.timeLabel = [[UILabel alloc]init];
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    
    [self addSubview:self.timeLabel];
    
}


@end
