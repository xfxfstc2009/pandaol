//
//  PDLotteryGameDetailHeaderStatusView.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,LotteryGameDetailStatus) {
    LotteryGameDetailStatusWillBegin,               /**< 比赛未开始*/
    LotteryGameDetailStatusGaming,                  /**< 比赛中*/
    LotteryGameDetailStatusEnd,                     /**< 比赛开始*/
    LotteryGameDetailStatusNormal,
};

@interface PDLotteryGameDetailHeaderStatusView : UIView

-(void)startCutDown:(NSTimeInterval)cutDown type:(LotteryGameDetailStatus)type block:(void(^)())block;
-(void)showGameSettlement;              /**< 用来显示已结算*/

@end
