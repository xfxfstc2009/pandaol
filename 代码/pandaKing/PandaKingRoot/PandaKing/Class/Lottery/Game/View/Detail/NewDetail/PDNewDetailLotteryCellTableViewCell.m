//
//  PDNewDetailLotteryCellTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNewDetailLotteryCellTableViewCell.h"

@interface PDNewDetailLotteryCellTableViewCell()
@property (nonatomic,strong)UIView *topView;
@property (nonatomic,strong)UIView *topFlagView;
@property (nonatomic,strong)UILabel *topFlagLabel;

@property (nonatomic,strong)UIView *bottomView;

@property (nonatomic,strong)UILabel *leftTeamLabel;
@property (nonatomic,strong)UILabel *leftProgressLabel;
@property (nonatomic,strong)UILabel *rightTeamLabel;
@property (nonatomic,strong)UILabel *rightProgressLabel;
@property (nonatomic,strong)UILabel *lotteryFlagLabel;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *leftHasLabel;
@property (nonatomic,strong)UILabel *rightHasLabel;

@property (nonatomic,strong)UIView *leftLine;
@property (nonatomic,strong)UIView *rightLine;

@property (nonatomic,strong)PDImageView *clockImgView;
@property (nonatomic,strong)PDImageView *clockImgView2;
@property (nonatomic,strong)UILabel *zhongLabel;

@property (nonatomic,strong)UILabel *countLabel;                    /**< 多少人参与，总投注***万*/
@end

static char actionTouzhuKey;
@implementation PDNewDetailLotteryCellTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建左侧的label
    self.topView = [[UIView alloc]init];
    self.topView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.topView.frame = CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(30));
    [self addSubview:self.topView];
    
    // 2 创建标签
    self.topFlagView = [[UIView alloc]init];
    self.topFlagView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.topFlagView.frame = CGRectMake(0, LCFloat(5), LCFloat(6), self.topView.size_height - 2 * LCFloat(5));
    [self.topView addSubview:self.topFlagView];
    
    // 3.创建标签label
    self.topFlagLabel = [[UILabel alloc]init];
    self.topFlagLabel.backgroundColor = [UIColor clearColor];
    self.topFlagLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.topFlagLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    [self.topView addSubview:self.topFlagLabel];
    self.topFlagLabel.frame = CGRectMake(CGRectGetMaxX(self.topFlagView.frame) + LCFloat(11), 0, kScreenBounds.size.width - CGRectGetMaxX(self.topFlagView.frame) - LCFloat(11), self.topView.size_height);

    // 4. 创建bottomVirw
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    [self addSubview:self.bottomView];
    
    CGFloat width = kScreenBounds.size.width / 3.;
    // 5. 创建左侧的队伍label
    self.leftTeamLabel = [[UILabel alloc]init];
    self.leftTeamLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.leftTeamLabel.frame = CGRectMake(0, LCFloat(11), width, [NSString contentofHeightWithFont:self.leftTeamLabel.font]);
    self.leftTeamLabel.textAlignment = NSTextAlignmentCenter;
    self.leftTeamLabel.textColor = c26;
    [self.bottomView addSubview:self.leftTeamLabel];
    
    // 6. 创建左侧的赔率
    self.leftProgressLabel = [[UILabel alloc]init];
    self.leftProgressLabel.backgroundColor = [UIColor clearColor];
    self.leftProgressLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.leftProgressLabel.frame = CGRectMake(0, CGRectGetMaxY(self.leftTeamLabel.frame), self.leftTeamLabel.size_width, [NSString contentofHeightWithFont:self.leftTeamLabel.font]);
    self.leftProgressLabel.textColor = c26;
    self.leftProgressLabel.textAlignment = NSTextAlignmentCenter;
    [self.bottomView addSubview:self.leftProgressLabel];
    
    // 7. 创建右侧的赔率
    self.rightTeamLabel = [[UILabel alloc]init];
    self.rightTeamLabel.backgroundColor = [UIColor clearColor];
    self.rightTeamLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
    self.rightTeamLabel.frame = CGRectMake(kScreenBounds.size.width - width, self.leftTeamLabel.orgin_y, width, [NSString contentofHeightWithFont:self.rightTeamLabel.font]);
    self.rightTeamLabel.textColor = c26;
    self.rightTeamLabel.textAlignment = NSTextAlignmentCenter;
    [self.bottomView addSubview:self.rightTeamLabel];
    
    // 8. 创建右侧的赔率
    self.rightProgressLabel = [[UILabel alloc]init];
    self.rightProgressLabel.backgroundColor = [UIColor clearColor];
    self.rightProgressLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.rightProgressLabel.textColor = c26;
    self.rightProgressLabel.textAlignment = NSTextAlignmentCenter;
    self.rightProgressLabel.frame = CGRectMake(self.rightTeamLabel.orgin_x, CGRectGetMaxY(self.rightTeamLabel.frame), self.rightTeamLabel.size_width, [NSString contentofHeightWithFont:self.rightProgressLabel.font]);
    [self.bottomView addSubview:self.rightProgressLabel];
    
    // 9.创建标签
    self.lotteryFlagLabel = [[UILabel alloc]init];
    self.lotteryFlagLabel.backgroundColor = [UIColor clearColor];
    self.lotteryFlagLabel.textAlignment = NSTextAlignmentCenter;
    self.lotteryFlagLabel.frame = CGRectMake(CGRectGetMaxX(self.leftTeamLabel.frame), 0, self.rightTeamLabel.orgin_x - CGRectGetMaxX(self.leftTeamLabel.frame), LCFloat(50));
    [self.bottomView addSubview:self.lotteryFlagLabel];
    
    // 10 创建线条
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    self.lineView.frame = CGRectMake(0, CGRectGetMaxY(self.lotteryFlagLabel.frame), kScreenBounds.size.width, .5f);
    [self.bottomView addSubview:self.lineView];
    
    // 11.创建底部
    self.leftHasLabel = [[UILabel alloc]init];
    self.leftHasLabel.backgroundColor = [UIColor clearColor];
    self.leftHasLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.leftHasLabel.textAlignment = NSTextAlignmentCenter;
    [self.bottomView addSubview:self.leftHasLabel];
    
    // 12. 创建右侧底部
    self.rightHasLabel = [[UILabel alloc]init];
    self.rightHasLabel.backgroundColor = [UIColor clearColor];
    self.rightHasLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.rightHasLabel.textAlignment = NSTextAlignmentCenter;
    [self.bottomView addSubview:self.rightHasLabel];

    // 13.左边的线
    self.leftLine = [[UIView alloc]init];
    self.leftLine.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    self.leftLine.frame = CGRectMake(CGRectGetMaxX(self.leftTeamLabel.frame), 0, .5f, self.lineView.orgin_y);
    [self.bottomView addSubview:self.leftLine];
    
    // 14. 创建右侧的view
    self.rightLine = [[UIView alloc]init];
    self.rightLine.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    self.rightLine.frame = CGRectMake(self.rightTeamLabel.orgin_x - .5f, 0, .5f, self.lineView.orgin_y);
    [self.bottomView addSubview:self.rightLine];
    
    // 15. 创建左边按钮
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BOOL isLeft) = objc_getAssociatedObject(strongSelf, &actionTouzhuKey);
        if (block){
            block(YES);
        }
    }];
    [self addSubview:self.leftButton];
    
    // 16. 创建右侧按钮
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.backgroundColor = [UIColor clearColor];
    [self.rightButton buttonWithBlock:^(UIButton *button) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(BOOL isLeft) = objc_getAssociatedObject(strongSelf, &actionTouzhuKey);
        if (block){
            block(NO);
        }
    }];
    [self addSubview:self.rightButton];
    
    // 17.创建左边的clock
    self.clockImgView = [[PDImageView alloc]init];
    self.clockImgView.backgroundColor = [UIColor clearColor];
    self.clockImgView.image = [UIImage imageNamed:@"icon_liveshow_clock"];
    [self addSubview:self.clockImgView];
    
    // 18. 创建右边的clock
    self.clockImgView2 = [[PDImageView alloc]init];
    self.clockImgView2.backgroundColor = [UIColor clearColor];
    self.clockImgView2.image = [UIImage imageNamed:@"icon_liveshow_clock"];
    [self addSubview:self.clockImgView2];
    
    // 19. 创建中
    self.zhongLabel = [self createZhongLabel];
    [self addSubview:self.zhongLabel];
    self.zhongLabel.hidden = YES;
    
    // 11. countLabel
    self.countLabel = [[UILabel alloc]init];
    self.countLabel.backgroundColor = [UIColor clearColor];
    self.countLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.countLabel.textAlignment = NSTextAlignmentCenter;
    self.countLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.countLabel.adjustsFontSizeToFitWidth = YES;
    [self.bottomView addSubview:self.countLabel];
    
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferLotteryGameSingleModel:(PDMatchguessinfoGuessListRootSingleModel *)transferLotteryGameSingleModel{
    _transferLotteryGameSingleModel = transferLotteryGameSingleModel;
    
    // 1. top
    self.topFlagLabel.text = transferLotteryGameSingleModel.guessName;
    
    // 2. 创建bottom
    self.bottomView.frame = CGRectMake(0, CGRectGetMaxY(self.topView.frame), kScreenBounds.size.width, LCFloat(75));

    // 3. 创建左侧
    self.leftTeamLabel.text = transferLotteryGameSingleModel.teamLeft.point;
    self.leftProgressLabel.text = [NSString stringWithFormat:@"%.2f",transferLotteryGameSingleModel.teamLeft.odds];
    
    // 右侧
    self.rightTeamLabel.text = transferLotteryGameSingleModel.teamRight.point;
    self.rightProgressLabel.text = [NSString stringWithFormat:@"%.2f",transferLotteryGameSingleModel.teamRight.odds];
    
    self.lotteryFlagLabel.text = transferLotteryGameSingleModel.guessName;
    self.lotteryFlagLabel.adjustsFontSizeToFitWidth = YES;
    
    self.leftHasLabel.text =[NSString stringWithFormat: @"已投%@",[Tool transferWithInfoNumber:transferLotteryGameSingleModel.teamLeft.myStakeCount] ];
    self.leftHasLabel.frame = CGRectMake(0, CGRectGetMaxY(self.lineView.frame), self.leftTeamLabel.size_width, self.bottomView.size_height - CGRectGetMaxY(self.lineView.frame));
    self.rightHasLabel.text =[NSString stringWithFormat: @"已投%@",[Tool transferWithInfoNumber:transferLotteryGameSingleModel.teamRight.myStakeCount]];
    self.rightHasLabel.frame = CGRectMake(self.rightTeamLabel.orgin_x, CGRectGetMaxY(self.lineView.frame), self.leftTeamLabel.size_width, self.bottomView.size_height - CGRectGetMaxY(self.lineView.frame));

    // 4.左侧按钮
    self.leftButton.frame = CGRectMake(0, self.bottomView.orgin_y, self.leftTeamLabel.size_width, self.lotteryFlagLabel.size_height);
    // 5. 右侧按钮
    self.rightButton.frame = CGRectMake(kScreenBounds.size.width - self.rightTeamLabel.size_width, self.bottomView.orgin_y, self.rightTeamLabel.size_width, self.leftButton.size_height);
 
    // 6. 总投已投
    self.countLabel.frame = CGRectMake(CGRectGetMaxX(self.leftButton.frame), self.rightHasLabel.orgin_y, self.rightButton.orgin_x - CGRectGetMaxX(self.leftButton.frame), self.rightHasLabel.size_height);
    self.countLabel.text = [NSString stringWithFormat:@"%li人参与，总投注%@",transferLotteryGameSingleModel.guessTotalJoinCount,[Tool transferWithInfoNumber:transferLotteryGameSingleModel.guessTotalGoldCount]];
    
    // 6.
    
    if ([self.transferLotteryGameSingleModel.guessState isEqualToString:@"stakeoff"]){          // 关闭
        [self.leftButton setImage:[UIImage imageNamed:@"icon_liveshow_clock"] forState:UIControlStateNormal];
        self.leftButton.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
        self.leftButton.alpha = .7f;
        [self.rightButton setImage:[UIImage imageNamed:@"icon_liveshow_clock"] forState:UIControlStateNormal];
        self.rightButton.backgroundColor = [UIColor colorWithCustomerName:@"灰"];
        self.rightButton.alpha = .7f;
        self.leftButton.userInteractionEnabled = NO;
        self.rightButton.userInteractionEnabled = NO;
    } else if ([self.transferLotteryGameSingleModel.guessState isEqualToString:@"finished"]){
        if (transferLotteryGameSingleModel.teamLeft.isWin){
            self.leftTeamLabel.textColor = c26;
            self.leftProgressLabel.textColor = c26;
            self.rightProgressLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            self.rightTeamLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            self.zhongLabel.frame = CGRectMake(CGRectGetMaxX(self.leftButton.frame) - LCFloat(5) - self.zhongLabel.size_width, self.leftButton.orgin_y + (self.leftButton.size_height - self.zhongLabel.size_height) / 2., self.zhongLabel.size_width, self.zhongLabel.size_height);
        
        } else {
            self.leftTeamLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            self.leftProgressLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            self.rightTeamLabel.textColor = c26;
            self.rightProgressLabel.textColor = c26;
            
            self.zhongLabel.frame = CGRectMake(self.rightButton.orgin_x + LCFloat(5), self.leftButton.orgin_y + (self.leftButton.size_height - self.zhongLabel.size_height) / 2., self.zhongLabel.size_width, self.zhongLabel.size_height);
        }
        self.zhongLabel.hidden = NO;
        self.leftButton.userInteractionEnabled = NO;
        self.rightButton.userInteractionEnabled = NO;
    } else {
        [self.rightButton setImage:nil forState:UIControlStateNormal];
        [self.rightButton setImage:nil forState:UIControlStateNormal];
        self.leftButton.userInteractionEnabled = YES;
        self.rightButton.userInteractionEnabled = YES;
    }
}


+(CGFloat)calculationCellHeight{
    CGFloat cellheight = LCFloat(30);
    cellheight += LCFloat(75);
    return cellheight;
}

-(void)actionClickToTouzhuWithLeft:(void(^)(BOOL isLeft))block{
    objc_setAssociatedObject(self, &actionTouzhuKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(UILabel *)createZhongLabel{
    self.zhongLabel = [[UILabel alloc]init];
    self.zhongLabel.backgroundColor = c26;
    self.zhongLabel.font = [[UIFont fontWithCustomerSizeName:@"正文"] boldFont];
    self.zhongLabel.layer.cornerRadius = LCFloat(2);
    self.zhongLabel.clipsToBounds = YES;
    self.zhongLabel.text = @"中";
    self.zhongLabel.textColor = [UIColor whiteColor];
    self.zhongLabel.textAlignment = NSTextAlignmentCenter;
    CGSize zhongSize = [self.zhongLabel.text sizeWithCalcFont:self.zhongLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.zhongLabel.font])];
    self.zhongLabel.bounds = CGRectMake(0, 0, MAX(zhongSize.width + 2 * LCFloat(3), [NSString contentofHeightWithFont:self.zhongLabel.font] + 2 * LCFloat(2)), MAX(zhongSize.width + 2 * LCFloat(3), [NSString contentofHeightWithFont:self.zhongLabel.font] + 2 * LCFloat(2)));
    return self.zhongLabel;
}
@end
