//
//  PDNewLotteryDetailHeaderView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNewLotteryDetailHeaderView.h"


static CGFloat navHeight = 64;
@interface PDNewLotteryDetailHeaderView()

@property (nonatomic,strong)UIView *alphaView;
@property (nonatomic,strong)UILabel *startTimeLabel;

@property (nonatomic,strong)UILabel *leftTeamNameLabel;
@property (nonatomic,strong)UILabel *scoreLabel;
@property (nonatomic,strong)UILabel *rightTeamLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel*joinLabel;
@property (nonatomic,strong)UILabel *statusLabel;
@property (nonatomic,strong)NSTimer *downCountTimer;


@end

@implementation PDNewLotteryDetailHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1.创建背景
    self.bgImgView = [[UIView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.size_height);
    [self addSubview:self.bgImgView];
    
    // 2. 创建上层的透明view
//    self.alphaView = [[UIView alloc]init];
//    self.alphaView.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
//    self.alphaView.alpha = .6f;
//    [self addSubview:self.alphaView];
    
    // 3. 创建开始时间
    self.startTimeLabel = [[UILabel alloc]init];
    self.startTimeLabel.backgroundColor = [UIColor clearColor];
    self.startTimeLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.startTimeLabel.textAlignment = NSTextAlignmentCenter;
    self.startTimeLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    [self addSubview:self.startTimeLabel];
    
    // 4. 创建左侧的图片
    self.leftTeamImgView = [[PDImageView alloc]init];
    self.leftTeamImgView.backgroundColor = [UIColor clearColor];
    self.leftTeamImgView.frame = CGRectMake(0, 0, LCFloat(60), LCFloat(60));
    [self addSubview:self.leftTeamImgView];
    
    // 5. 创建左侧名字
    self.leftTeamNameLabel = [[UILabel alloc]init];
    self.leftTeamNameLabel.backgroundColor = [UIColor clearColor];
    self.leftTeamNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.leftTeamNameLabel.textAlignment = NSTextAlignmentCenter;
    self.leftTeamNameLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.leftTeamNameLabel];
    
    // 6. 创建右侧
    self.rightTeamImgView = [[PDImageView alloc]init];
    self.rightTeamImgView.backgroundColor = [UIColor clearColor];
    self.rightTeamImgView.frame = CGRectMake(0, 0, LCFloat(60), LCFloat(60));
    [self addSubview:self.rightTeamImgView];
    
    // 7.创建右侧名字
    self.rightTeamLabel = [[UILabel alloc]init];
    self.rightTeamLabel.backgroundColor = [UIColor clearColor];
    self.rightTeamLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.rightTeamLabel.textAlignment = NSTextAlignmentCenter;
    self.rightTeamLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.rightTeamLabel];
    
    // 8. 创建比分
    self.scoreLabel = [[UILabel alloc]init];
    self.scoreLabel.backgroundColor = [UIColor clearColor];
    self.scoreLabel.font = [UIFont systemFontOfCustomeSize:30];
    self.scoreLabel.adjustsFontSizeToFitWidth = YES;
    self.scoreLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.scoreLabel];
    
    // 9. 创建timelabel
    self.joinLabel = [[UILabel alloc]init];
    self.joinLabel.backgroundColor = [UIColor clearColor];
    self.joinLabel.textAlignment = NSTextAlignmentCenter;
    self.joinLabel.textColor = c26;
    self.joinLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self addSubview:self.joinLabel];
    
    // 10 . statusLabel
    self.statusLabel = [[UILabel alloc]init];
    self.statusLabel.backgroundColor = [UIColor clearColor];
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    self.statusLabel.adjustsFontSizeToFitWidth = YES;
    self.statusLabel.textColor = c26;
    self.statusLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    self.statusLabel.layer.borderColor = c26.CGColor;
    self.statusLabel.layer.borderWidth = 1.f;
    [self addSubview:self.statusLabel];
}

-(void)setTransferRootModel:(PDMatchguessinfoSingleModel *)transferRootModel{
    _transferRootModel = transferRootModel;
    
    // 1. 创建背景
//    self.bgImgView.frame = self.bounds;
//    __weak typeof(self)weakSelf = self;
//    [self.bgImgView uploadImageWithURL:transferRootModel.gameBackgroundPic placeholder:nil callback:^(UIImage *image) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf maskHeaderImage:image];
//    }];
    
//     2.透明
//    self.alphaView.frame = self.bgImgView.bounds;
    
    // 2. 创建时间
    self.startTimeLabel.text = [NSDate getTimeWithLotteryString:transferRootModel.matchStartTime / 1000.];
    self.startTimeLabel.frame = CGRectMake(0, navHeight + LCFloat(22), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.startTimeLabel.font]);
    
    // 3. 创建左边图片
//    [self.leftTeamImgView uploadImageWithURL:transferRootModel.teamLeft.icon placeholder:nil callback:NULL];
    self.leftTeamImgView.hidden = YES;
    self.leftTeamImgView.frame = CGRectMake(LCFloat(22), CGRectGetMaxY(self.startTimeLabel.frame) , LCFloat(60), LCFloat(60));

    // 4. 左侧名字
    self.leftTeamNameLabel.text = transferRootModel.teamLeft.teamName;
    self.leftTeamNameLabel.adjustsFontSizeToFitWidth = YES;
    self.leftTeamNameLabel.frame = CGRectMake(self.leftTeamImgView.orgin_x, CGRectGetMaxY(self.leftTeamImgView.frame), self.leftTeamImgView.size_width, [NSString contentofHeightWithFont:self.leftTeamNameLabel.font]);
    
    // 5. 创建右侧图片
//    [self.rightTeamImgView uploadImageWithURL:transferRootModel.teamRight.icon placeholder:nil callback:NULL];
    self.rightTeamImgView.hidden = YES;
    self.rightTeamImgView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(22) - self.leftTeamImgView.size_width, self.leftTeamImgView.orgin_y,self.leftTeamImgView.size_width , self.leftTeamImgView.size_height);
    
    // 1. 创建右侧的文字
    self.rightTeamLabel.text = transferRootModel.teamRight.teamName;
    self.rightTeamLabel.adjustsFontSizeToFitWidth = YES;
    self.rightTeamLabel.frame = CGRectMake(self.rightTeamImgView.orgin_x, CGRectGetMaxY(self.rightTeamImgView.frame), self.rightTeamImgView.size_width, [NSString contentofHeightWithFont:self.rightTeamLabel.font]);
    
    // 7. 创建比分
    self.scoreLabel.frame = CGRectMake(CGRectGetMaxX(self.leftTeamImgView.frame), 0, self.rightTeamImgView.orgin_x - CGRectGetMaxX(self.leftTeamImgView.frame), [NSString contentofHeightWithFont:self.scoreLabel.font]);
    self.scoreLabel.textColor = [UIColor whiteColor];
    self.scoreLabel.center_y = self.leftTeamImgView.center_y;
    
    // 8. 创建参与人数
    self.joinLabel.frame =CGRectMake(0, CGRectGetMaxY(self.leftTeamNameLabel.frame), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.joinLabel.font]);
    
    // 9. 创建 self.startTimeLabel
    if ([transferRootModel.matchStatus isEqualToString:@"unbegun"]){            // 显示倒计时
        if (!self.downCountTimer){
            self.downCountTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
        }
    } else if ([transferRootModel.matchStatus isEqualToString:@"inPlay"]){      // 显示比赛中
        self.statusLabel.text = @"比赛中";
    } else if ([transferRootModel.matchStatus isEqualToString:@"over"]){        // 显示已结束
        self.statusLabel.text = @"已结束";
    }
    
    CGSize statusSize = [@"已结束" sizeWithCalcFont:self.statusLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.statusLabel.font])];
    CGFloat width = statusSize.width + 2 * LCFloat(15);
    CGFloat height = [NSString contentofHeightWithFont:self.statusLabel.font] + 2 * LCFloat(2);
    self.statusLabel.frame = CGRectMake((kScreenBounds.size.width - width) / 2. , self.joinLabel.orgin_y - LCFloat(6) - height, width, height);
    
    self.statusLabel.layer.cornerRadius = MIN(self.statusLabel.size_height, self.statusLabel.size_width) / 2.;
    
}

-(void)changeScoreWithStr:(NSString *)str{
    self.scoreLabel.text = str;
}

-(void)changeJoinWithStr:(NSString *)str{
    self.joinLabel.text = str;
}

#pragma mark - 
+(CGFloat)calculationCellHeight{
    return LCFloat(160);
}

-(void)daojishiManager{
    NSString *statusStr = [NSDate getLotteryTimeDistance:self.transferRootModel.gameStartTempTime / 1000];
    if ([statusStr isEqualToString:@"比赛中"]){
        self.statusLabel.text = @"已截止";
        [self stopTimer];
    } else {
        self.statusLabel.text = statusStr;
    }
}



-(void)stopTimer{
    if (self.downCountTimer){
        [self.downCountTimer invalidate];
        self.downCountTimer = nil;
    }
}

//-(void)maskHeaderImage:(UIImage *)image{
//    [self.bgImgView mdInflateAnimatedFromPoint:CGPointMake(kScreenBounds.size.width / 2., self.bgImgView.size_height / 2.) backgroundColor:[UIColor colorWithCustomerName:@"白"] duration:.3f completion:^{
//        self.bgImgView.image = image;
//    }];
//}

@end
