//
//  PDCustomerNavBarView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/1/12.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDCustomerNavBarView : UIView

@property (nonatomic,copy,nonnull)NSString *barMainTitle;

- (nonnull UIButton *)leftBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock ;

- (nonnull UIButton *)rightBarButtonWithTitle:(nullable NSString *)title barNorImage:(nullable UIImage *)norImage barHltImage:(nullable UIImage *)hltImage action:(nullable void(^)(void))actionBlock;
@end
