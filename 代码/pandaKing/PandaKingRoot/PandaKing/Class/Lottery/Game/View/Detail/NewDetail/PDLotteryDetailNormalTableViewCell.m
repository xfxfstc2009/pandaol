//
//  PDLotteryDetailNormalTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryDetailNormalTableViewCell.h"

@interface PDLotteryDetailNormalTableViewCell()
@property (nonatomic,strong)UILabel *numberLabel;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *goldLabel;

@end

@implementation PDLotteryDetailNormalTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. number
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.font = [UIFont fontWithCustomerSizeName:@"标题"];
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    self.numberLabel.textColor = c26;
    [self addSubview:self.numberLabel];
    
    // 2. 创建头像
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.avatarImgView];
    
    // 3. 创建名字
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.nameLabel];
    
    // 4. 创建金币
    self.goldLabel = [[UILabel alloc]init];
    self.goldLabel.backgroundColor = [UIColor clearColor];
    self.goldLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.goldLabel];
}

-(void)setTransferCellheight:(CGFloat)transferCellheight{
    _transferCellheight = transferCellheight;
}

-(void)setTransferDetailNormalModel:(PDMatchguessinfoGuessStakeListSingleModel *)transferDetailNormalModel{
    _transferDetailNormalModel = transferDetailNormalModel;
    
    // 1.数字
    self.numberLabel.text = [NSString stringWithFormat:@"%li",self.index];
    CGSize numberSize = [@"20" sizeWithCalcFont:self.numberLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellheight)];
    self.numberLabel.frame = CGRectMake(LCFloat(11), 0, numberSize.width, self.transferCellheight);
    self.numberLabel.adjustsFontSizeToFitWidth = YES;
    self.numberLabel.textAlignment = NSTextAlignmentCenter;
    
    // 2. 创建头像
    self.avatarImgView.frame = CGRectMake(CGRectGetMaxX(self.numberLabel.frame) + LCFloat(11), LCFloat(5), (self.transferCellheight - 2 * LCFloat(5)), (self.transferCellheight - 2 * LCFloat(5)));
    [self.avatarImgView uploadImageWithAvatarURL:transferDetailNormalModel.avatar placeholder:nil callback:NULL];
    
    // 4. 创建金币
    NSString *goldStr =  [NSString stringWithFormat:@"%@金币",[Tool transferWithInfoNumber:transferDetailNormalModel.stakeCount]];
    self.goldLabel.attributedText = [Tool rangeLabelWithContent:goldStr hltContentArr:@[@"金币"] hltColor:[UIColor colorWithCustomerName:@"黑"] normolColor:c26];
    CGSize goldSize = [goldStr sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, self.transferCellheight)];
    self.goldLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - goldSize.width, 0, goldSize.width, self.transferCellheight);
    
    // 3. 创建名字
    self.nameLabel.text = transferDetailNormalModel.nickname;
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11), 0, self.goldLabel.orgin_x - (CGRectGetMaxX(self.avatarImgView.frame) + LCFloat(11)), self.transferCellheight);
    self.nameLabel.adjustsFontSizeToFitWidth = YES;
    
}

+(CGFloat)calculationCellHeight{
   return LCFloat(44);
}

-(void)setIndex:(NSInteger)index{
    _index = index + 1;
}

+(CGFloat)numberWidth{
    CGSize numberSize = [@"20" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"标题"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, 20)];
    return numberSize.width;
}

@end
