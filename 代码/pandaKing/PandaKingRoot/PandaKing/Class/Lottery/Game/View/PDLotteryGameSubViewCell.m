//
//  PDLotteryGameSubViewCell.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameSubViewCell.h"
#import "PDLotteryGameDetailHeaderStatusView.h"

@interface PDLotteryGameSubViewCell()
@property (nonatomic,strong)UILabel *leftRateLabel;
@property (nonatomic,strong)UILabel *leftSubLabel;
@property (nonatomic,strong)PDImageView *leftVictory;
@property (nonatomic,strong)UILabel *rightRateLabel;
@property (nonatomic,strong)UILabel *rightSubLabel;
@property (nonatomic,strong)PDImageView *rightVictory;
@property (nonatomic,strong)UILabel *gameTargetLabel;
@property (nonatomic,strong)UILabel *gameTimeLabel;                 /**< 游戏时间label*/
@property (nonatomic,strong)PDImageView *arrowImageView;

// timer
@property (nonatomic,strong)NSTimer *timer;
@property (nonatomic,strong)PDImageView *clockImgView;              /**< 时间图片*/

@end

@implementation PDLotteryGameSubViewCell


-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    // 1. 创建左侧的倍率
    self.leftRateLabel = [[UILabel alloc]init];
    self.leftRateLabel.backgroundColor = [UIColor clearColor];
    self.leftRateLabel.textColor = c26;
    self.leftRateLabel.textAlignment = NSTextAlignmentCenter;
    self.leftRateLabel.adjustsFontSizeToFitWidth = YES;
    self.leftRateLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];
    [self addSubview:self.leftRateLabel];
    
    // 2. 创建左侧的sublabel
    self.leftSubLabel = [[UILabel alloc]init];
    self.leftSubLabel.backgroundColor = [UIColor clearColor];
    self.leftSubLabel.textAlignment = NSTextAlignmentCenter;
    self.leftSubLabel.textColor = c28;
    self.leftSubLabel.adjustsFontSizeToFitWidth = YES;
    self.leftSubLabel.font = [UIFont systemFontOfCustomeSize:11.];
    [self addSubview:self.leftSubLabel];
    
    // 3. 创建左侧胜利标志
    self.leftVictory = [[PDImageView alloc]init];
    self.leftVictory.backgroundColor = [UIColor clearColor];
    self.leftVictory.image = [UIImage imageNamed:@"icon_lottery_list_victory"];
    self.leftVictory.frame = CGRectMake(0, 0, LCFloat(17), LCFloat(17));
    [self addSubview:self.leftVictory];
    
    // 4. 创建右侧倍率label
    self.rightRateLabel = [[UILabel alloc]init];
    self.rightRateLabel.backgroundColor = [UIColor clearColor];
    self.rightRateLabel.textColor = c26;
    self.rightRateLabel.textAlignment = NSTextAlignmentCenter;
    self.rightRateLabel.adjustsFontSizeToFitWidth = YES;
    self.rightRateLabel.font = [UIFont fontWithCustomerSizeName:@"小提示"];

    [self addSubview:self.rightRateLabel];
    
    // 5. 创建右侧的sublabel
    self.rightSubLabel = [[UILabel alloc]init];
    self.rightSubLabel.backgroundColor = [UIColor clearColor];
    self.rightSubLabel.textAlignment = NSTextAlignmentCenter;
    self.rightSubLabel.textColor = c28;
    self.rightSubLabel.adjustsFontSizeToFitWidth = YES;
    self.rightSubLabel.font = [UIFont systemFontOfCustomeSize:11.];
    [self addSubview:self.rightSubLabel];
    
    // 6. 创建右侧的胜利标志
    self.rightVictory = [[PDImageView alloc]init];
    self.rightVictory.backgroundColor = [UIColor clearColor];
    self.rightVictory.image = [UIImage imageNamed:@"icon_lottery_list_victory"];
    self.rightVictory.frame = CGRectMake(0, 0, LCFloat(17), LCFloat(17));
    [self addSubview:self.rightVictory];
    
       // 8. 创建比赛类别
    self.gameTargetLabel = [[UILabel alloc]init];
    self.gameTargetLabel.backgroundColor = [UIColor clearColor];
    self.gameTargetLabel.adjustsFontSizeToFitWidth = YES;
    self.gameTargetLabel.clipsToBounds = YES;
    self.gameTargetLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.gameTargetLabel];
    
    // 7. 创建比赛时间
    self.gameTimeLabel = [[UILabel alloc]init];
    self.gameTimeLabel.backgroundColor = [UIColor clearColor];
    self.gameTimeLabel.textAlignment = NSTextAlignmentCenter;
    self.gameTimeLabel.font = [UIFont systemFontOfCustomeSize:11.];
    self.gameTimeLabel.textColor = c28;
    self.gameTimeLabel.adjustsFontSizeToFitWidth = YES;
    [self addSubview:self.gameTimeLabel];
    
    // 9. 创建箭头
    self.arrowImageView = [[PDImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    self.arrowImageView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    [self addSubview:self.arrowImageView];
    
    // 10.创建时间ImgView
    self.clockImgView = [[PDImageView alloc]init];
    self.clockImgView.backgroundColor = [UIColor clearColor];
    self.clockImgView.hidden = YES;
    self.clockImgView.image = [UIImage imageNamed:@"icon_lottery_list_clock"];
    self.clockImgView.frame = CGRectMake(0, 0, LCFloat(9), LCFloat(9));
    [self addSubview:self.clockImgView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
    self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - LCFloat(11) - LCFloat(9), (self.transferCellHeight - LCFloat(9)) / 2., LCFloat(9), LCFloat(15));
}

-(void)setTransferLotteryGameListSingleModel:(PDLotteryGameSubListSingleModel *)transferLotteryGameListSingleModel{
    _transferLotteryGameListSingleModel = transferLotteryGameListSingleModel;
    // 1. 左侧倍率
    self.leftRateLabel.text = [NSString stringWithFormat:@"%.2f倍",transferLotteryGameListSingleModel.lotteryLeft.rate];
    
    CGSize leftRateSize = [self.leftRateLabel.text sizeWithCalcFont:self.leftRateLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.leftRateLabel.font])];
    self.leftRateLabel.frame = CGRectMake((kScreenBounds.size.width - self.arrowImageView.orgin_x), LCFloat(13), leftRateSize.width, [NSString contentofHeightWithFont:self.leftSubLabel.font]);
    CGFloat margin_center_x = LCFloat(33) + LCFloat(60) / 2.;
    self.leftRateLabel.center_x = margin_center_x;
    
    // 2. 创建胜利
    self.leftSubLabel.text = transferLotteryGameListSingleModel.lotteryLeft.subTitle;
    self.leftSubLabel.frame = CGRectMake(self.leftRateLabel.orgin_x, CGRectGetMaxY(self.leftRateLabel.frame) + LCFloat(5), self.leftRateLabel.size_width, [NSString contentofHeightWithFont:self.leftSubLabel.font]);
    
    // 3. 创建右侧
    self.rightRateLabel.text = [NSString stringWithFormat:@"%.2f倍",transferLotteryGameListSingleModel.lotteryRight.rate];
    self.rightRateLabel.frame = CGRectMake(self.arrowImageView.orgin_x - self.leftRateLabel.size_width, self.leftRateLabel.orgin_y, self.leftRateLabel.size_width, self.leftRateLabel.size_height);
    self.rightRateLabel.center_x = kScreenBounds.size.width - self.leftRateLabel.center_x;
    
    // 3.1 创建右侧是否胜利
    self.rightSubLabel.text = transferLotteryGameListSingleModel.lotteryRight.subTitle;
    self.rightSubLabel.frame = CGRectMake(self.rightRateLabel.orgin_x, self.leftSubLabel.orgin_y, self.leftSubLabel.size_width, self.leftSubLabel.size_height);
    
    // 4.  创建比赛类别
    self.gameTargetLabel.text = transferLotteryGameListSingleModel.lotteryTarget;
    self.gameTargetLabel.font = [UIFont systemFontOfCustomeSize:11.];
    self.gameTargetLabel.frame = CGRectMake(kScreenBounds.size.width / 2. - LCFloat(59) / 2., LCFloat(5), LCFloat(59), LCFloat(21));
    self.gameTargetLabel.layer.cornerRadius = MIN(self.gameTargetLabel.size_width, self.gameTargetLabel.size_height) / 2.;
    
    self.gameTargetLabel.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    self.gameTargetLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    if([transferLotteryGameListSingleModel.lotteryStatus isEqualToString:@"staking"]){          //显示倒计时
        [self timeStatusWithType:LotteryGameDetailStatusWillBegin];
    } else if ([transferLotteryGameListSingleModel.lotteryStatus isEqualToString:@"stakeoff"]){
        [self timeStatusWithType:LotteryGameDetailStatusGaming];                // 比赛中
    } else if ([transferLotteryGameListSingleModel.lotteryStatus isEqualToString:@"finished"]){
        [self timeStatusWithType:LotteryGameDetailStatusEnd];
        
        self.gameTargetLabel.backgroundColor = [UIColor colorWithCustomerName:@"白"];
        self.gameTargetLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
        self.gameTargetLabel.layer.borderWidth = 1;
        self.gameTargetLabel.layer.borderColor = [UIColor colorWithCustomerName:@"灰"].CGColor;
    } else {
        [self timeStatusWithType:LotteryGameDetailStatusNormal];
    }
    
    
    // 6 .是否胜利图标
    self.leftVictory.frame = CGRectMake(0, (self.transferCellHeight - LCFloat(17)) / 2., LCFloat(17), LCFloat(17));
    self.leftVictory.center_x = (kScreenBounds.size.width / 2. - self.leftSubLabel.center_x) / 2. + self.leftSubLabel.center_x;
    
    self.rightVictory.frame = self.leftVictory.frame;
    self.rightVictory.center_x = kScreenBounds.size.width - self.leftVictory.center_x;
    
    if (self.transferLotteryGameListSingleModel.lotteryLeft.isVictory){
        self.leftVictory.hidden = NO;
        self.rightVictory.hidden = YES;
    } else if (self.transferLotteryGameListSingleModel.lotteryRight.isVictory){
        self.leftVictory.hidden = YES;
        self.rightVictory.hidden = NO;
    } else {
        self.leftVictory.hidden = YES;
        self.rightVictory.hidden = YES;
    }
}

-(void)timeStatusWithType:(LotteryGameDetailStatus)type{
    if (type == LotteryGameDetailStatusWillBegin){              // 比赛未开始
        NSString *statusStr = [NSDate getLotteryTimeDistance:self.transferLotteryGameListSingleModel.endTime / 1000.];
        self.gameTimeLabel.text = statusStr;
        
        [self startTimer];
        self.clockImgView.hidden = NO;
    } else if (type == LotteryGameDetailStatusEnd){             // 比赛结束
        [self stopTimer];
        self.clockImgView.hidden = YES;
        self.gameTimeLabel.text = @"已结算";
    } else if (type == LotteryGameDetailStatusGaming){          // 比赛中
        [self stopTimer];
        self.clockImgView.hidden = YES;
        self.gameTimeLabel.text = @"已截止";
    } else {
        [self stopTimer];
        self.clockImgView.hidden = YES;
        self.gameTimeLabel.text = @"竞猜中";

    }
    
    // 1. 比赛类别
    CGFloat margin = (self.transferCellHeight - self.gameTargetLabel.size_height - self.gameTimeLabel.size_height) / 3.;
    self.gameTargetLabel.orgin_y = margin;

    // 2. 比赛时间
    CGSize contentOfSize = [self.gameTimeLabel.text sizeWithCalcFont:self.gameTimeLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.gameTimeLabel.font])];
    self.gameTimeLabel.frame = CGRectMake(0, 0, contentOfSize.width, [NSString contentofHeightWithFont:self.gameTimeLabel.font]);
    self.gameTimeLabel.center_x = kScreenBounds.size.width / 2.;
    // 2.1 计算高度
    CGFloat center_time_h = (self.transferCellHeight - CGRectGetMaxY(self.gameTargetLabel.frame)) / 2. + CGRectGetMaxY(self.gameTargetLabel.frame);
    self.gameTimeLabel.center_y = center_time_h;
    
    self.clockImgView.orgin_x = self.gameTimeLabel.orgin_x - LCFloat(3) - LCFloat(9);
    self.clockImgView.center_y = self.gameTimeLabel.center_y;
}

-(void)startTimer{
    if (!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
    }
}


-(void)daojishiManager{
    NSString *statusStr = [NSDate getLotteryTimeDistance:self.transferLotteryGameListSingleModel.endTime / 1000.];
    if ([statusStr isEqualToString:@"比赛中"]){
        self.gameTimeLabel.text = @"已截止";
        [self stopTimer];
    } else {
        self.gameTimeLabel.text = statusStr;
    }
}

-(void)stopTimer{
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}

@end
