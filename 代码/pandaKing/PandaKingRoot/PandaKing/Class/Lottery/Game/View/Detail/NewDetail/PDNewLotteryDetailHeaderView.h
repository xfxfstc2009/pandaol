//
//  PDNewLotteryDetailHeaderView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDMatchguessinfoSingleModel.h"
#import "DKLiveBlurView.h"

@interface PDNewLotteryDetailHeaderView : UIView


@property (nonatomic,strong)PDMatchguessinfoSingleModel *transferRootModel;
@property (nonatomic,strong)UIView *bgImgView;

@property (nonatomic,strong)PDImageView *leftTeamImgView;
@property (nonatomic,strong)PDImageView *rightTeamImgView;

// 1. 修改比分
-(void)changeScoreWithStr:(NSString *)str;
// 2. 修改参与人数
-(void)changeJoinWithStr:(NSString *)str;

+(CGFloat)calculationCellHeight;

-(void)stopTimer;
@end
