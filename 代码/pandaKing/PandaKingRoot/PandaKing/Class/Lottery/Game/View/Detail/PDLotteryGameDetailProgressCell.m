//
//  PDLotteryGameDetailProgressCell.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameDetailProgressCell.h"

@interface PDLotteryGameDetailProgressCell()
@property (nonatomic,strong)UILabel *fixedLeftLabel;                /**< 左侧*/
@property (nonatomic,strong)UILabel *fixedRightLabel;               /**< 右侧*/
@property (nonatomic,strong)UILabel *dymicLeftLabel;                /**< 动态左侧*/
@property (nonatomic,strong)UILabel *dymicRightLabel;               /**< 动态右侧*/

@property (nonatomic,strong)UIView *progressView;
@property (nonatomic,strong)UIView *leftProView;
@property (nonatomic,strong)UIView *rightProView;
@end

@implementation PDLotteryGameDetailProgressCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建左侧label
    self.fixedLeftLabel = [[UILabel alloc]init];
    self.fixedLeftLabel.backgroundColor = [UIColor clearColor];
    self.fixedLeftLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.fixedLeftLabel.textColor = c3;
    [self addSubview:self.fixedLeftLabel];
    
    // 2. 创建右侧
    self.fixedRightLabel = [[UILabel alloc]init];
    self.fixedRightLabel.backgroundColor = [UIColor clearColor];
    self.fixedRightLabel.font = [UIFont fontWithCustomerSizeName:@"提示"];
    self.fixedRightLabel.textAlignment = NSTextAlignmentRight;
    self.fixedRightLabel.textColor = c3;
    [self addSubview:self.fixedRightLabel];
    
    // 4. 创建支持率
    self.dymicLeftLabel = [[UILabel alloc]init];
    self.dymicLeftLabel.backgroundColor = [UIColor clearColor];
    self.dymicLeftLabel.font = [UIFont systemFontOfCustomeSize:11.];
    self.dymicLeftLabel.textColor = c29;
    [self addSubview:self.dymicLeftLabel];
    
    // 5. 创建右侧支持率
    self.dymicRightLabel = [[UILabel alloc]init];
    self.dymicRightLabel.backgroundColor = [UIColor  clearColor];
    self.dymicRightLabel.font = [UIFont systemFontOfCustomeSize:11.];
    self.dymicRightLabel.textColor = c30;
    self.dymicRightLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.dymicRightLabel];
    
    
    // 3. 创建progressView
    self.progressView = [[UIView alloc]init];
    self.progressView.backgroundColor = [UIColor lightGrayColor];
    self.progressView.frame = CGRectMake(LCFloat(19), 0, kScreenBounds.size.width - 2 * LCFloat(19), LCFloat(4));
    [self addSubview:self.progressView];
    
    // 3.1 创建pro1
    self.leftProView = [[UIView alloc]init];
    self.leftProView.backgroundColor = c29;
    self.leftProView.frame = CGRectMake(0, 0, 0, self.progressView.size_height);
    [self.progressView addSubview:self.leftProView];
    
    // 3.2 创建pro2
    self.rightProView = [[UIView alloc]init];
    self.rightProView.backgroundColor = c30;
    self.rightProView.frame = CGRectMake(self.progressView.size_width, 0, 0, self.progressView.size_height);
    [self.progressView addSubview:self.rightProView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferGameDetailRootModel:(PDLotteryGameDetailRootModel *)transferGameDetailRootModel{
    _transferGameDetailRootModel = transferGameDetailRootModel;
    self.fixedLeftLabel.text = [NSString stringWithFormat:@"%@",transferGameDetailRootModel.teamLeft.wagerName];
    
    CGSize contentOfFixedLeftSize = [self.fixedLeftLabel.text sizeWithCalcFont:self.fixedLeftLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLeftLabel.font])];
    self.fixedLeftLabel.frame = CGRectMake(LCFloat(19), LCFloat(8), contentOfFixedLeftSize.width, [NSString contentofHeightWithFont:self.fixedLeftLabel.font]);

    // 3. 创建右侧
    self.fixedRightLabel.text = [NSString stringWithFormat:@"%@",transferGameDetailRootModel.teamRight.wagerName];
    CGSize contentOfFixedRightSize = [self.fixedRightLabel.text sizeWithCalcFont:self.fixedRightLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedRightLabel.font])];
    self.fixedRightLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(19) - contentOfFixedRightSize.width, self.fixedLeftLabel.orgin_y, contentOfFixedRightSize.width, [NSString contentofHeightWithFont:self.fixedRightLabel.font]);
    
    // 2. 创建progress
    self.progressView.frame = CGRectMake(LCFloat(19), CGRectGetMaxY(self.fixedLeftLabel.frame) + LCFloat(6.5), kScreenBounds.size.width - 2 * LCFloat(19), LCFloat(4));
    
    // 4. 创建支持率
    self.dymicLeftLabel.text = [NSString stringWithFormat:@"%li%%支持率",transferGameDetailRootModel.teamLeft.support];
    CGSize dymicLeftLabelSize = [self.dymicLeftLabel.text sizeWithCalcFont:self.dymicLeftLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLeftLabel.font])];
    self.dymicLeftLabel.frame = CGRectMake(LCFloat(19), CGRectGetMaxY(self.progressView.frame) + LCFloat(4), dymicLeftLabelSize.width, [NSString contentofHeightWithFont:self.dymicLeftLabel.font]);
    
    // 5. 创建支持率
    self.dymicRightLabel.text = [NSString stringWithFormat:@"%li%%支持率",transferGameDetailRootModel.teamRight.support];
    CGSize dymicRightLabelSize = [self.dymicRightLabel.text sizeWithCalcFont:self.dymicRightLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicRightLabel.font])];
    self.dymicRightLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(19) - dymicRightLabelSize.width , CGRectGetMaxY(self.progressView.frame) + LCFloat(4), dymicRightLabelSize.width, [NSString contentofHeightWithFont:self.dymicRightLabel.font]);
    
    
    // 6. 左右支持率

    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:3.5f animations:^{
            self.leftProView.size_width = self.progressView.size_width * transferGameDetailRootModel.teamLeft.support / 100.;
            self.rightProView.frame = CGRectMake(self.progressView.size_width, 0, - self.progressView.size_width *transferGameDetailRootModel.teamRight.support / 100., self.progressView.size_height);
        }];
    });
}


+(CGFloat)calculationCellHeight{
    return LCFloat(54);
}

@end

