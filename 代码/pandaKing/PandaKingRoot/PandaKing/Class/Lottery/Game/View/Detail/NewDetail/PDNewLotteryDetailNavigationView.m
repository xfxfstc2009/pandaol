//
//  PDNewLotteryDetailNavigationView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDNewLotteryDetailNavigationView.h"

@interface PDNewLotteryDetailNavigationView()
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation PDNewLotteryDetailNavigationView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    
}

@end
