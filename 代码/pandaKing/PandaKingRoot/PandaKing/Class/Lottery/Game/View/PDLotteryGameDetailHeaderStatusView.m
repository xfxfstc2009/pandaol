//
//  PDLotteryGameDetailHeaderStatusView.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameDetailHeaderStatusView.h"

static char statusKey;

@interface PDLotteryGameDetailHeaderStatusView()
@property (nonatomic,strong)UIView *statusBgView;
@property (nonatomic,strong)UILabel *statusLabel;
@property (nonatomic,strong)NSTimer *timer;

@property (nonatomic,assign)NSTimeInterval tempTime;

@end

@implementation PDLotteryGameDetailHeaderStatusView

-(instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建
    self.statusBgView = [[UIView alloc]init];
    self.statusBgView.backgroundColor = c26;
    self.statusBgView.frame = self.bounds;
    self.statusBgView.clipsToBounds = YES;
    self.statusBgView.layer.cornerRadius = MIN(self.statusBgView.size_height, self.statusBgView.size_width) / 2.;
    [self addSubview:self.statusBgView];
    
    // 2. 创建时间
    self.statusLabel = [[UILabel alloc]init];
    self.statusLabel.backgroundColor = [UIColor clearColor];
    self.statusLabel.font = [UIFont systemFontOfCustomeSize:10];
    self.statusLabel.frame = self.bounds;
    self.statusLabel.adjustsFontSizeToFitWidth = YES;
    self.statusLabel.textAlignment = NSTextAlignmentCenter;
    self.statusLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.statusLabel];
}

-(void)startCutDown:(NSTimeInterval)cutDown type:(LotteryGameDetailStatus)type block:(void(^)())block{
    if (type == LotteryGameDetailStatusEnd){                // 比赛结束
        self.statusLabel.text = @"已结算";
        [self stopTimer];
    } else if (type == LotteryGameDetailStatusGaming){      // 比赛中
        self.statusLabel.text = @"已截止";
        [self stopTimer];
    } else if (type == LotteryGameDetailStatusWillBegin){       // 比赛未开始
        _tempTime = cutDown;
        objc_setAssociatedObject(self, &statusKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
        [self startTimer];                      // 开启计时器
    } else if (type == LotteryGameDetailStatusNormal){
        self.statusLabel.text = @"竞猜中";
        [self stopTimer];
    }
}

-(void)showGameSettlement{
    [self stopTimer];
    self.statusLabel.text = @"已结算";
    self.statusLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    self.statusBgView.backgroundColor = [UIColor clearColor];
    self.statusBgView.layer.borderColor = [UIColor colorWithCustomerName:@"浅灰"].CGColor;
    self.statusBgView.layer.borderWidth = 1.;
}

-(void)startTimer{
    if (!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
    }
}

-(void)daojishiManager{
    NSString *statusStr = [NSDate getLotteryTimeDistance:self.tempTime];
    if ([statusStr isEqualToString:@"比赛中"]){
        self.statusLabel.text = @"比赛中";
        [self stopTimer];
    } else {
        self.statusLabel.text = statusStr;
    }
}

-(void)stopTimer{
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}

@end
