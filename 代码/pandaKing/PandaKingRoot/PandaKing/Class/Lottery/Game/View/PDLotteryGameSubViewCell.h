//
//  PDLotteryGameSubViewCell.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryGameSubListSingleModel.h"

@interface PDLotteryGameSubViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDLotteryGameSubListSingleModel *transferLotteryGameListSingleModel;            // 
@end
