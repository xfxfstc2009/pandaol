//
//  PDLoteryGameRootSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLoteryGameRootSubSingleModel.h"

@protocol PDLoteryGameRootSingleModel <NSObject>

@end


@interface PDLoteryGameRootSingleModel : FetchModel

@property (nonatomic,assign)NSTimeInterval dateTime;                                   /**< 比赛时间*/
@property (nonatomic,strong)NSArray<PDLoteryGameRootSubSingleModel> *subList;          /**< 次级*/

@end
