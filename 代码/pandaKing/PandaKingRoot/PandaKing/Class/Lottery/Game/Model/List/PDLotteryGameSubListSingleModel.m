//
//  PDLotteryGameSubListSingleModel.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameSubListSingleModel.h"

@implementation PDLotteryGameSubListSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end
