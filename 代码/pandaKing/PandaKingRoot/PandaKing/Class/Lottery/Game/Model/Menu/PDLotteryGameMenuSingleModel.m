//
//  PDLotteryGameMenuSingleModel.m
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryGameMenuSingleModel.h"

@implementation PDLotteryGameMenuSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"ID":@"id"};
}

@end
