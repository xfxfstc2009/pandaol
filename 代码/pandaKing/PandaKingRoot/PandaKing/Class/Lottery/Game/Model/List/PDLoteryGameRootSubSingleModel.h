//
//  PDLoteryGameRootSubSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryGameSubListSingleModel.h"
#import "PDLotteryGameTeamSingleModel.h"

@protocol PDLoteryGameRootSubSingleModel <NSObject>

@end

@interface PDLoteryGameRootSubSingleModel : FetchModel


@property (nonatomic,copy)NSString *matchName;                            /**< 比赛名字*/
@property (nonatomic,copy)NSString *matchFormat;                          /**< 赛制*/
@property (nonatomic,copy)NSString *matchId;

// team
@property (nonatomic,strong)PDLotteryGameTeamSingleModel *teamLeft;
@property (nonatomic,strong)PDLotteryGameTeamSingleModel *teamRight;
@property (nonatomic,assign)NSTimeInterval matchLeftTime;
@property (nonatomic,copy)NSString *matchStatus;

@property (nonatomic,strong)NSArray<PDLotteryGameSubListSingleModel> *lotteryDetailList;   /**< 竞猜列表*/


// temp内部调用
@property (nonatomic,assign)NSTimeInterval currentTempTime;

//比赛开始时间
@property (nonatomic,assign)NSTimeInterval gameStartTempTime;
@end
