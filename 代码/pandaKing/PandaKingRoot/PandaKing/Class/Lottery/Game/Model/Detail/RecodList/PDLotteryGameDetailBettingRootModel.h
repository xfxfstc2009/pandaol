//
//  PDLotteryGameDetailBettingRootModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryGameDetailBettingDetailModel.h"

@interface PDLotteryGameDetailBettingRootModel : FetchModel

@property (nonatomic,strong)NSArray<PDLotteryGameDetailBettingDetailModel> * takeList;

//@property (nonatomic,strong)NSArray<PDLotteryGameDetailBettingDetailModel> * gameStart;
//
//@property (nonatomic,strong)NSArray<PDLotteryGameDetailBettingDetailModel> * betEnd;

@end
