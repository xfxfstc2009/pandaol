//
//  PDTouzhuDetailModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDTouzhuDetailModel : FetchModel

@property (nonatomic,assign)CGFloat oddsOnPoint;            /**< 该队赔率*/
@property (nonatomic,assign)CGFloat memberMaxStakeGold;     /**< 该队最大投注*/
@property (nonatomic,assign)CGFloat balance;                /**< 会员金币余额*/

// temp
@property (nonatomic,copy)NSString *teamName;
@property (nonatomic,copy)NSString *jingcaiName;            /**< 竞猜名字*/

@end
