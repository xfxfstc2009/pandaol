//
//  PDMatchguessinfoGuessListSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMatchguessinfoGuessStakeListSingleModel.h"
#import "PDMatchguessinfoGuessListRootSingleModel.h"

@interface PDMatchguessinfoGuessListSingleModel : FetchModel

@property (nonatomic,assign)NSInteger mainGuessPointBSupportRate;           /**< 右队-主竞猜*/
@property (nonatomic,assign)NSInteger totalJoinCount;                       /**< **人参与*/
@property (nonatomic,assign)NSInteger totalGoldCount;                       /**< 总投注*/
@property (nonatomic,assign)NSInteger teamAScore;                           /**< 左队-比赛总分*/
@property (nonatomic,assign)NSInteger teamBScore;                           /**< 右队-比赛总分*/
@property (nonatomic,assign)NSInteger mainGuessPointASupportRate;           /**< 左队-主竞猜-支持率*/

@property (nonatomic,strong)NSArray<PDMatchguessinfoGuessStakeListSingleModel> *stakeList;
@property (nonatomic,strong)NSArray<PDMatchguessinfoGuessListRootSingleModel> *guessList;



@end
