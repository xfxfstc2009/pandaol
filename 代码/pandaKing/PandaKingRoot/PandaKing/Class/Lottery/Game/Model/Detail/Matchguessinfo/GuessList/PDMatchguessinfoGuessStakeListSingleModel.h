//
//  PDMatchguessinfoGuessStakeListSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDMatchguessinfoGuessStakeListSingleModel <NSObject>

@end

@interface PDMatchguessinfoGuessStakeListSingleModel : FetchModel

@property (nonatomic,assign)NSInteger stakeCount;
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,copy)NSString *memberId;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *nickname;


@end
