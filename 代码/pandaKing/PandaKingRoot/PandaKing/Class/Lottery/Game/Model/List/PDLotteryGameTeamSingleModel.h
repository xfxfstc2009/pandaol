//
//  PDLotteryGameTeamSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDLotteryGameTeamSingleModel : FetchModel

@property (nonatomic,copy) NSString *icon;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *gameEnd;

@end
