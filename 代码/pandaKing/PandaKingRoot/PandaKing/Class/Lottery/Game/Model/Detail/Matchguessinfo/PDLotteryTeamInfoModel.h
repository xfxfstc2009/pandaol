//
//  PDLotteryTeamInfoModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"



@interface PDLotteryTeamInfoModel : FetchModel

@property (nonatomic,copy)NSString *icon;
@property (nonatomic,copy)NSString *teamName;

@end
