//
//  PDLotteryGameMenuListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryGameMenuSingleModel.h"

@interface PDLotteryGameMenuListModel : FetchModel

@property (nonatomic,strong)NSArray<PDLotteryGameMenuSingleModel > *game;

@end
