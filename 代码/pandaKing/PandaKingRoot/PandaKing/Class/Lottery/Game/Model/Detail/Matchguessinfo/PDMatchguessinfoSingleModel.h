//
//  PDMatchguessinfoSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryTeamInfoModel.h"

@interface PDMatchguessinfoSingleModel : FetchModel
@property(nonatomic,assign)NSTimeInterval matchStartTime;       /**< 比赛开始时间*/
@property (nonatomic,assign)NSTimeInterval matchLeftTime;       /**< 比赛开始剩余时间*/
@property (nonatomic,copy)NSString *matchStatus;                /**< 比赛状态*/
@property (nonatomic,copy)NSString *matchId;                    /**< 比赛id*/
@property (nonatomic,copy)NSString *imGroupId;                  /**< 讨论组*/
@property (nonatomic,copy)NSString *gameBackgroundPic;          /**< 背景图片*/
@property (nonatomic,assign)BOOL remind;                        /**< 是否提醒*/
@property (nonatomic,copy)NSString *mainGuessId;
@property (nonatomic,strong)PDLotteryTeamInfoModel *teamLeft;
@property (nonatomic,strong)PDLotteryTeamInfoModel *teamRight;


// temp
@property (nonatomic,assign)NSTimeInterval gameStartTempTime;

@end