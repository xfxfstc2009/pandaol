//
//  PDMatchguessinfoGuessListTeamSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDMatchguessinfoGuessListTeamSingleModel : FetchModel


@property (nonatomic,copy)NSString *teamName;
@property (nonatomic,assign)CGFloat odds;
@property (nonatomic,assign)NSInteger myStakeCount;
@property (nonatomic,copy)NSString*point;
@property (nonatomic,assign)BOOL isWin;



@end
