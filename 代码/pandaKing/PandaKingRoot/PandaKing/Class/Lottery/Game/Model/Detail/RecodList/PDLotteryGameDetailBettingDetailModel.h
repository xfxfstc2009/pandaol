//
//  PDLotteryGameDetailBettingDetailModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/25.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDLotteryGameDetailBettingDetailModel <NSObject>

@end

@interface PDLotteryGameDetailBettingDetailModel : FetchModel

@property (nonatomic,copy)NSString *teamIcon;
@property (nonatomic,assign)NSInteger supportCount;
@property (nonatomic,copy)NSString *personId;
@property (nonatomic,copy)NSString *avatar;
@property (nonatomic,copy)NSString *supportTeam;

// temp
@property (nonatomic,assign)BOOL isEnd;
@end
