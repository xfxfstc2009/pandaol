//
//  PDLotteryGameSubListSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLoteryGameSubSingleModel.h"

@protocol PDLotteryGameSubListSingleModel <NSObject>


@end

@interface PDLotteryGameSubListSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *lotteryTarget;              /**< 竞猜标的*/
@property (nonatomic,copy)NSString *lotteryStatus;              /**< 竞猜状态*/
@property (nonatomic,copy)NSString *matchStatus;                /**< 比赛当前状态*/
@property (nonatomic,assign)NSTimeInterval leftTime;            /**< 时间*/

// temp
@property (nonatomic,assign)NSTimeInterval endTime;             /**< 结束时间*/

@property (nonatomic,strong)PDLoteryGameSubSingleModel *lotteryLeft;
@property (nonatomic,strong)PDLoteryGameSubSingleModel *lotteryRight;

@end
