//
//  PDLotteryGameMenuSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/28.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDLotteryGameMenuSingleModel <NSObject>

@end

@interface PDLotteryGameMenuSingleModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *shortName;
@property (nonatomic,copy)NSString *fullName;
@property (nonatomic,assign)BOOL enabled;
@property (nonatomic,copy)NSString *backgroundPic;

@property (nonatomic,copy)NSString *tableIndex;

@end
