//
//  PDLoteryGameRootListModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLoteryGameRootSingleModel.h"

@interface PDLoteryGameRootListModel : FetchModel

@property (nonatomic,strong)NSArray<PDLoteryGameRootSingleModel>*lotteryList;



@end
