//
//  PDTouzhuDetailSuccessModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/11/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryGameDetailBettingDetailModel.h"
@interface PDTouzhuDetailSuccessModel : FetchModel

@property (nonatomic,strong)PDLotteryGameDetailBettingDetailModel *myStake;
@property (nonatomic,assign)BOOL isChange;                                      /**< 是否变化投注倍率*/
@property (nonatomic,assign)CGFloat invalidOdds;                /**< 就赔率*/
@property (nonatomic,assign)CGFloat currentOdds;                /**< 心赔率*/
@end
