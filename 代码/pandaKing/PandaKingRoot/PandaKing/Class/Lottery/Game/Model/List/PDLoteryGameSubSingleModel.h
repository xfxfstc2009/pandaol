//
//  PDLoteryGameSubSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 2016/10/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDLoteryGameSubSingleModel <NSObject>


@end

@interface PDLoteryGameSubSingleModel : FetchModel

@property (nonatomic,assign)CGFloat rate;                   /**< 倍率*/
@property (nonatomic,copy)NSString *subTitle;               /**< 胜利、获得*/
@property (nonatomic,assign)BOOL isVictory;                 /**< 判断是否胜利*/



@end
