//
//  PDLotteryGameDetailSegmentSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDLotteryGameDetailSegmentSingleModel : FetchModel

@property (nonatomic,copy)NSString *itemId;
@property (nonatomic,copy)NSString *itemName;

@property (nonatomic,copy)NSString *tableIndex;
@end
