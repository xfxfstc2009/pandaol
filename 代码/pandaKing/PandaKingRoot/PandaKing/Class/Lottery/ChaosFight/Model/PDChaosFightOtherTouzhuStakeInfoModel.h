//
//  PDChaosFightOtherTouzhuStakeInfoModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDChaosFightOtherTouzhuStakeInfoModel <NSObject>


@end

@interface PDChaosFightOtherTouzhuStakeInfoModel : FetchModel

@property (nonatomic,assign)NSInteger gold ;
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,assign)NSInteger target;
@property (nonatomic,copy)NSString *memberId;

@end
