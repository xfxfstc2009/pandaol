//
//  PDChaosFightGooutSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/30.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDChaosFightGooutSingleModel : FetchModel


@property (nonatomic,assign)NSInteger gold;
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,copy)NSString *memberId;

@property (nonatomic,copy)NSString *nickName;
@property (nonatomic,copy)NSString *avatar;

@end
