//
//  PDChaosFightLotteryModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDChaosFightLotteryModel : FetchModel

@property (nonatomic,assign)NSTimeInterval currentTime;
@property (nonatomic,assign)NSTimeInterval drawTime;
@property (nonatomic,copy)NSString *lotteryId;
@end
