//
//  PDChaosFightKaijiangRootModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDChaosFightWinningGuessSingleModel.h"
#import "PDChaosFightGuessItemsRoleSingleModel.h"

@interface PDChaosFightKaijiangRootModel : FetchModel

@property (nonatomic,copy)NSString *lotteryId;      /**< 竞猜ID*/
@property (nonatomic,copy)NSString *lotteryNum;     /**< 竞猜编号*/
@property (nonatomic,assign)NSTimeInterval lotteryDayTime;      /**< 时间*/
@property (nonatomic,strong)NSArray *pointResultItems;              /**< 怪物卡片*/
@property (nonatomic,strong)NSArray<PDChaosFightWinningGuessSingleModel> * winningGuessItems;
@property (nonatomic,strong)NSArray<PDChaosFightGuessItemsRoleSingleModel> * guessItemRs;


@end
