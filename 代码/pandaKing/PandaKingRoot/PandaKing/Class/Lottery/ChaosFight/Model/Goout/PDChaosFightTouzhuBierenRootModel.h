//
//  PDChaosFightTouzhuBierenRootModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDChaosFightTouzhuBierenSingleModel.h"

@interface PDChaosFightTouzhuBierenRootModel : FetchModel

@property (nonatomic,strong)PDChaosFightTouzhuBierenSingleModel *stakeInfo;
@property (nonatomic,strong)PDChaosFightGooutSingleModel *member;
@property (nonatomic,strong)PDChaosFightTouzhuSingleModel *guessItemInfo;


@end
