//
//  PDChaosFightOtherTouzhuSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/7.
//  Copyright © 2017年 PandaOL. All rights reserved.
//
// 其他人投注
#import "FetchModel.h"
#import "PDChaosFightMembersSingleModel.h"
#import "PDChaosFightGuessItemSingleModel.h"
#import "PDChaosFightOtherTouzhuStakeInfoModel.h"


@interface PDChaosFightOtherTouzhuSingleModel : FetchModel

@property (nonatomic,strong)PDChaosFightMembersSingleModel *member;
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *guessItemInfo;
@property (nonatomic,strong)PDChaosFightOtherTouzhuStakeInfoModel *stakeInfo;
@property (nonatomic,strong)PDChaosFightGuessItemMainModel *guessMainInfo;
@property (nonatomic,copy)NSString *guessId;
@end
