//
//  PDChaosFightPointSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDChaosFightPointSingleModel : FetchModel

@property (nonatomic,assign)NSInteger num;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,assign)NSInteger lotteryDayTime;

@end
