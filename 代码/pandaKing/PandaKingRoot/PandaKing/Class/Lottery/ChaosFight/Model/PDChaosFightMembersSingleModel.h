//
//  PDChaosFightMembersSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDChaosFightMembersSingleModel <NSObject>


@end

@interface PDChaosFightMembersSingleModel : FetchModel

@property (nonatomic,assign)NSInteger gold;
@property (nonatomic,copy)NSString *nickName;
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,copy)NSString *memberId;
@property (nonatomic,copy)NSString *avatar;

@end
