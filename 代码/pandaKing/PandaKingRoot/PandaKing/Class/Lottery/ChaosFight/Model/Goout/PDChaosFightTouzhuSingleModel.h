//
//  PDChaosFightTouzhuSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDChaosFightTouzhuView.h"

@interface PDChaosFightTouzhuSingleModel : FetchModel

@property (nonatomic,assign)NSInteger memberStake;
@property (nonatomic,assign)NSInteger odds;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,assign)NSInteger totalStake;
@property (nonatomic,assign)chaosTouzhuType target;

@end
