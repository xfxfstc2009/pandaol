//
//  PDChaosFightHistorySingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDChaosFightHistorySingleModel <NSObject>


@end

@interface PDChaosFightHistorySingleModel : FetchModel

@property (nonatomic,strong)NSArray *pointBResultItems;
@property (nonatomic,strong)NSArray *pointAResultItems;
@property (nonatomic,copy)NSString *winItemName;
@property (nonatomic,strong)NSArray *winningAnyoneGuessItems;
@property (nonatomic,strong)NSArray *winningOverallGuessItems;
@property (nonatomic,copy)NSString *num;

@end
