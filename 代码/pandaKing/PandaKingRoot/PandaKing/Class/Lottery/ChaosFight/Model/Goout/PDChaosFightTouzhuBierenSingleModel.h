//
//  PDChaosFightTouzhuBierenSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/31.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDChaosFightTouzhuBierenSingleModel : FetchModel

/*
 "stakeInfo":{"gold":100,"index":1,"target":10},"member":{"gold":1.0216808E8,"index":1,"memberId":"cne925"},"guessItemInfo":{"memberStake":22900,"odds":45.0,"name":"平手","type":"tie","totalStake":24000,"target":10},"type":976}}
 */
@property (nonatomic,assign)NSInteger gold;
@property (nonatomic,assign)NSInteger index;
@property (nonatomic,assign)NSInteger target;

@end
