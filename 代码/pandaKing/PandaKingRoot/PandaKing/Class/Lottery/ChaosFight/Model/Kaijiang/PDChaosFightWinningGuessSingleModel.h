//
//  PDChaosFightWinningGuessSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/1.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDChaosFightWinningGuessSingleModel <NSObject>

@end

@interface PDChaosFightWinningGuessSingleModel : FetchModel

@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *type;
@property (nonatomic,assign)NSInteger odds;
@property (nonatomic,assign)NSInteger order;


@end
