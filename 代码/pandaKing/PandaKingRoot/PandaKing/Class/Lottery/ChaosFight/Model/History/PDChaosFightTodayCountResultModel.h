//
//  PDChaosFightTodayCountResultModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDChaosFightTodayCountResultModel : FetchModel

@property (nonatomic,assign)NSInteger pointAWin;
@property (nonatomic,assign)NSInteger pointBWin;
@property (nonatomic,assign)NSInteger tie;


@property (nonatomic,assign)NSInteger anyoneDragonet;
@property (nonatomic,assign)NSInteger anyoneDragon;

@property (nonatomic,assign)NSInteger anyoneDoubleKill;
@property (nonatomic,assign)NSInteger anyoneDoubleNull;

@property (nonatomic,assign)NSInteger overallOne;
@property (nonatomic,assign)NSInteger overallTwo;
@property (nonatomic,assign)NSInteger overallThree;
@property (nonatomic,assign)NSInteger overallFour;

@property (nonatomic,assign)NSInteger anyoneMaster;             // 主宰
@property (nonatomic,assign)NSInteger anyoneTyrant;             // 暴君

@property (nonatomic,assign)NSInteger anyoneBlackDragon;             // 黑龙
@property (nonatomic,assign)NSInteger anyoneMeatMountain;             // 肉山

@end
