//
//  PDChaosFightGuessModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDChaosFightGuessModel : FetchModel

@property (nonatomic,copy)NSString *roomNum;
@property (nonatomic,copy)NSString *guessId;
@property (nonatomic,assign)NSInteger totalMemberCount;

@end
