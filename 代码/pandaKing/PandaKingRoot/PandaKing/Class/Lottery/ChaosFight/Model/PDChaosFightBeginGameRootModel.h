//
//  PDChaosFightBeginGameRootModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDChaosFightGuessModel.h"
#import "PDChaosFightMembersSingleModel.h"
#import "PDChaosFightLotteryModel.h"
#import "PDChaosFightGuessItemMainModel.h"
#import "PDChaosFightOtherTouzhuStakeInfoModel.h"

typedef NS_ENUM(NSInteger,PDChaosFightBeginGameType) {
    PDChaosFightBeginGameBegin = 971,                           /**< 比赛开始*/
    PDChaosFightBeginGameTouzhu = 972,                          /**< 投注*/
};

@interface PDChaosFightBeginGameRootModel : FetchModel

@property (nonatomic,strong)PDChaosFightGuessModel *guess;                          /**< 房间信息*/
@property (nonatomic,strong)NSArray<PDChaosFightMembersSingleModel> *members;       /**< 角色信息*/
@property (nonatomic,assign)NSInteger memberMaxDayStake;                            /**< 最高投注*/
@property (nonatomic,assign)NSInteger memberDayStake;                               /**< 当前已投注*/

@property (nonatomic,strong)PDChaosFightLotteryModel *lottery;                      /**< 比赛信息*/
@property (nonatomic,strong)PDChaosFightGuessItemMainModel *guessItemInfo;          /**< 比例*/
@property (nonatomic,assign)PDChaosFightBeginGameType type;
@property (nonatomic,strong)NSArray<PDChaosFightOtherTouzhuStakeInfoModel>  *stakeInfos;

@end
