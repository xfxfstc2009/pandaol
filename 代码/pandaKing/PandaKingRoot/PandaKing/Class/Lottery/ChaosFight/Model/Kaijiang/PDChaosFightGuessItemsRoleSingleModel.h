//
//  PDChaosFightGuessItemsRoleSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/1.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDChaosFightGuessItemsRoleSingleModel <NSObject>


@end

@interface PDChaosFightGuessItemsRoleSingleModel : FetchModel

@property (nonatomic,copy)NSString *memberId;
@property (nonatomic,copy)NSString *guessItemType;
@property (nonatomic,assign)NSInteger winGold;
@property (nonatomic,assign)NSTimeInterval playTime;


@end
