//
//  PDChaosFightGuessItemSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDChaosFightGuessItemSingleModel : FetchModel

@property (nonatomic,assign)NSInteger memberStake;            /**< 我的投注*/
@property (nonatomic,copy)NSString *odds;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,assign)NSInteger target;
@property (nonatomic,assign)NSInteger totalStake;
@property (nonatomic,copy)NSString *type;

@end
