//
//  PDChaosFightGuessItemMainModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDChaosFightGuessItemSingleModel.h"

@interface PDChaosFightGuessItemMainModel : FetchModel

//
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *pointBWin;                /**< 儿童节*/
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *pointAWin;                /**< 托儿所*/
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *tie;                      /**< 平局*/

@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *overallOne;               /**< 1只*/
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *overallTwo;               /**< 2只*/
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *overallThree;             /**< 3只*/
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *overallFour;              /**< 4只*/


@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *anyoneDragonet;           /**< 小龙*/
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *anyoneDragon;             /**< 大龙*/

@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *anyoneMeatMountain;           /**< 肉山*/
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *anyoneBlackDragon;             /**< 黑龙*/

@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *anyoneTyrant;           /**< 暴君*/
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *anyoneMaster;             /**< 主宰*/


@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *anyoneDoubleKill;         /**< 双杀*/
@property (nonatomic,strong)PDChaosFightGuessItemSingleModel *anyoneDoubleNull;         /**< 双控*/





@end
