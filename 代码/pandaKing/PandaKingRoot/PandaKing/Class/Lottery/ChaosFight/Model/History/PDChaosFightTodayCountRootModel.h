//
//  PDChaosFightTodayCountRootModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/6.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDChaosFightTodayCountResultModel.h"

@interface PDChaosFightTodayCountRootModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)NSTimeInterval dayTime;
@property (nonatomic,strong)PDChaosFightTodayCountResultModel *resultToCount;


@end
