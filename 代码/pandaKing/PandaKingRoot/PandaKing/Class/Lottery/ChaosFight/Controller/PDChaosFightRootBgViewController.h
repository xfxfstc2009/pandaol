//
//  PDChaosFightRootBgViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/17.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDChaosFightTouzhuView.h"
#import "PDChaosFightBeginGameRootModel.h"
#import "PDChaosFightTouzhuSingleModel.h"
//#import "PDAlertView+PDChaosFight.h"

typedef NS_ENUM(NSInteger,touzhuCountType) {
    touzhuCountType100,
    touzhuCountType1000,
    touzhuCountType10000,
    touzhuCountType50000,
    touzhuCountType100000,
    touzhuCountTypeALL,                 /**< 全下*/
};

typedef NS_ENUM(NSInteger,touzhuGameType) {
    touzhuGameTypeNormal = 0,                       /**< 可投注icon_chaosFight_top_title*/
    touzhuGameTypeReadyGo = 1,                       /**< readyGo icon_chaosFight_start*/
    touzhuGameTypeEnd = 2,                         /**< 结束不可投注 icon_chaosFight_ending*/
    touzhuGameTypeNoMoney = 3,                      /**< 结束不可投注 没钱*/
    touzhuGameTypeShangxian = 4,                    /**< 达到投注上线*/
};

@interface PDChaosFightRootBgViewController : AbstractViewController

@property (nonatomic,strong)PDChaosFightTouzhuView *rightBottomTable1;
@property (nonatomic,strong)PDChaosFightTouzhuView *rightBottomTable2;
@property (nonatomic,strong)PDChaosFightTouzhuView *rightBottomTable3;
@property (nonatomic,strong)PDChaosFightTouzhuView *rightBottomTable4;

@property (nonatomic,strong)PDChaosFightTouzhuView *leftBottomTable1;
@property (nonatomic,strong)PDChaosFightTouzhuView *leftBottomTable2;
@property (nonatomic,strong)PDChaosFightTouzhuView *leftBottomTable3;
@property (nonatomic,strong)PDChaosFightTouzhuView *leftBottomTable4;

@property (nonatomic,strong)PDChaosFightTouzhuView *topTable1;
@property (nonatomic,strong)PDChaosFightTouzhuView *topTable2;
@property (nonatomic,strong)PDChaosFightTouzhuView *topTable3;
@property (nonatomic,strong)NSMutableArray *topPriceMutableArr;

@property (nonatomic,assign)touzhuGameType transferGameType;
@property (nonatomic,assign)BOOL hasGoout;

@property (nonatomic,assign)BOOL hasCanTouzhu;
@property (nonatomic,assign)NSInteger myCountGold;
@property (nonatomic,assign)touzhuCountType currentTouzhuType;

-(void)actionClickWithType:(chaosTouzhuType)type block:(void(^)())block;
@property (nonatomic,strong)PDChaosFightBeginGameRootModel *singleModel;


#pragma mark - 更新房间信息
-(void)updateHouseInfo:(PDChaosFightBeginGameRootModel *)singleModel;
-(void)updateHouseTotalCount:(NSInteger)count;                                  // 更新总数
#pragma mark - 下注
-(void)actionXiazhuManagerWithBlock:(void(^)(chaosTouzhuType type,PDChaosFightTouzhuView *touzhuView,NSInteger gold))block;

#pragma mark - 移除
-(void)dismissRootManager;

#pragma mark - 退出窗口
-(void)gooutManagerWithBlock:(void(^)())block;

#pragma mark - 有用户退出
-(void)goOutMemberIndex:(NSInteger)index;
#pragma mark 玩家进入方法
-(void)goInMemberSingleModel:(PDChaosFightGooutSingleModel *)index;
#pragma mark - 投注
-(void)touzhuViewManagerWithView:(PDChaosFightTouzhuView *)view money:(NSInteger)memberStake block:(void(^)(touzhuGameType touzhuGameType))block;
-(void)touzhuSocketBaseBackManager:(PDChaosFightTouzhuSingleModel *)moneySingleModel;                                   // socket更新
-(void)touzhuWithOtherGamer:(PDChaosFightOtherTouzhuSingleModel *)mainSingleModel;
-(void)touzhuWillGoInGame:(PDChaosFightBeginGameRootModel *)singleModel;
#pragma mark - 开奖动画
-(void)kaijiangAnimationManager:(PDChaosFightKaijiangRootModel *)kaijiangSingleModel block:(void(^)())block;
#pragma mark - 显示弹框
-(void)showAlertToCount;
#pragma mark - 创建去充值
-(void)showAlertToChongzhiBlcok:(void(^)())block;
#pragma mark - 创建下注上线
-(void)showAlertTopShangxian;
#pragma mark - 被提出
-(void)showAlertBeitichuWithBlock:(void(^)())block;

#pragma mark - 统计按钮点击
-(void)tongjiBtnClickBlock:(void(^)())block;
#pragma mark - 倒计时
-(void)changeTimerImgWithInteger:(NSInteger)num;
#pragma mark - 开始动画
-(void)challengeGuaishouAnimationWithBlock:(void(^)())block;
#pragma mark - 修改我的金币
-(void)changeMyCoinWithMoney:(NSInteger)money;
#pragma mark - Action- 充值
-(void)actionClickToChongzhiManagerBlock:(void(^)())block;
#pragma mark - 赚钱
-(void)mineWinMangerWithCoin:(NSInteger)coin;

#pragma mark - 修改图片
-(void)changeTitleImgType:(touzhuGameType)type;
-(void)winWithCoinRootManager:(PDChaosFightKaijiangRootModel *)winModel block:(void(^)())block;
-(void)dismissCard;
#pragma mark - 修改赔率
-(void)changeGuessItemInfoWithModel:(PDChaosFightBeginGameRootModel *)jungleGuessInfo;
@end
