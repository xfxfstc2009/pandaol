//
//  PDChaosFightRootBgViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/17.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightRootBgViewController.h"
#import "PDChaosFightTouzhuView.h"
#import "PDChaosFightRoleTableViewCell.h"
#import "PDChaosCardView.h"
#import "ZCAudioTool.h"
#import "PDChaosCoinImgView.h"
#import "PDChaosReadyGoView.h"
#import "PDHeroView.h"
#import "PDChaosVSView.h"
#import "PDChaosBackgroundView.h"
#import "PDChaosFightPopView.h"
#import "PDChaosFightWinLabel.h"
#import "PDChaosFightPopUserAvatarView.h"

// 卡片
typedef NS_ENUM(NSInteger,killGuaishouType) {
    killGuaishouType1,
    killGuaishouType2,
    killGuaishouType3,
    killGuaishouType4,
};


static char actionClickWithTypeKey;
static char gooutManagerWithBlockKey;
static char tongjiBtnClickBlockKey;
@interface PDChaosFightRootBgViewController ()<UITableViewDataSource,UITableViewDelegate>{
    BOOL bottom_page_two;
}
@property (nonatomic,strong)PDImageView *backgroundImgView;
@property (nonatomic,strong)UIButton *backButton;

// 在线人数
@property (nonatomic,strong)PDImageView *onLineBgView;                  /**< 在线背景*/
@property (nonatomic,strong)UILabel *onlineLabel;                       /**< 在线人数*/
@property (nonatomic,strong)UILabel *onlineHouseLabel;                  /**< 在线房间*/

// 左侧的卡片
@property (nonatomic,strong)UITableView *leftTableView;
@property (nonatomic,strong)NSMutableArray *leftMutableArr;
// 右侧
@property (nonatomic,strong)UITableView *rightTableView;
@property (nonatomic,strong)NSMutableArray *rightMutableArr;

// 底部
@property (nonatomic,strong)PDImageView *bottomView;
@property (nonatomic,strong)PDImageView *userAvatarImgView;
@property (nonatomic,strong)PDImageView *userTopAvatarImgView;
@property (nonatomic,strong)PDImageView *nickNamebgView;
@property (nonatomic,strong)UIButton *chongzhiButton;

@property (nonatomic,strong)UILabel *nickNameLabel;
@property (nonatomic,strong)PDImageView *moneybgView;
@property (nonatomic,strong)PDImageView *coinImgView;
@property (nonatomic,strong)PDImageView *coinAddImgView;
@property (nonatomic,strong)UILabel *coinLabel;
@property (nonatomic,strong)UIButton *oneButton;
@property (nonatomic,strong)UIButton *twoButton;
@property (nonatomic,strong)UIButton *threeButton;
@property (nonatomic,strong)UIButton *fourButton;
@property (nonatomic,strong)UIButton *fivButton;
@property (nonatomic,strong)UIButton *sixButton;
@property (nonatomic,strong)UIButton *cutButton;                    /**< 切换按钮*/
@property (nonatomic,strong)UIScrollView *mainScrollView;           /**< scrollView*/
@property (nonatomic,strong)UIButton *countButton;

// 创建标题
@property (nonatomic,strong)PDImageView *titleImgView;
@property (nonatomic,strong)PDImageView *timeImgView;

// 创建牌桌
@property (nonatomic,strong)PDImageView *topCardTableImgView;

// 左侧
@property (nonatomic,strong)PDImageView *leftCardTableImgView;
@property (nonatomic,strong)PDImageView *leftCardtopTitleImgView;               // 底下的投注文字

// 右侧
@property (nonatomic,strong)PDImageView *rightCardTableImgView;
@property (nonatomic,strong)PDImageView *rightCardtopTitleImgView;               // 底下的投注文字

// 卡片1
@property (nonatomic,strong)PDChaosCardView *leftCard1;
@property (nonatomic,strong)PDChaosCardView *leftCard2;
@property (nonatomic,strong)PDChaosCardView *rightCard1;
@property (nonatomic,strong)PDChaosCardView *rightCard2;
@property (nonatomic,strong)PDChaosBackgroundView *animationBgView;

//
@property (nonatomic,assign)NSInteger goldCount;

@property (nonatomic,strong)ZCAudioTool *audioTool;

@property (nonatomic,strong)NSMutableArray *touzhuButtonArr;

@property (nonatomic,strong)PDChaosReadyGoView *readyGoView;

@property (nonatomic,strong)PDChaosFightMembersSingleModel *mineMemberSingleModel;

@property (nonatomic,strong)PDChaosFightWinLabel *mineWinGoldLabel;

@property (nonatomic,strong)UIButton *guizeButton;                              /**< 规则按钮*/

@end

@implementation PDChaosFightRootBgViewController

-(void)dealloc{
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    for (int i = 0 ; i < keyWindow.subviews.count;i++){
        UIView *view = [keyWindow.subviews objectAtIndex:i];
        NSLog(@"%@",view);
    }
    NSLog(@"%li",keyWindow.subviews.count);
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    
    [self dismissRootManager];
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    for (int i = 0 ; i < keyWindow.subviews.count;i++){
        UIView *view = [keyWindow.subviews objectAtIndex:i];
        if ([view isKindOfClass:[PDChaosCardView class]]){
            [view removeFromSuperview];
            view = nil;
        }
    }
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithBaseInit];                       // 1. 数据初始化
    [self createBackgroundView];                    // 2. 创建背景音乐
    [self createBackButton];                        // 3. 创建返回按钮
    [self createOnLineView];                        // 4. 创建在线view
    [self gameStartAudio];                          // 5. 创建入场音乐
    [self createBottomView];                        // 6. 创建底部信息
    [self createTableView];                         // 7. 创建左右的tableView
    [self createTitleImgView];                      // 8. 创建顶部的view
    [self createCardTable];                         // 9. 创建牌桌
    [self viewLayerSetting];
}

-(void)gameStartAudio{
    if (!self.audioTool){
        self.audioTool = [[ZCAudioTool alloc]init];
    }
    
    [self.audioTool playSoundWithSoundName:@"mic_gameDidStart.wav"];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 自动布局frame
    [self auSetupPriceView];
}

#pragma mark - arrayWithInit
-(void)arrayWithBaseInit{
    self.topPriceMutableArr = [NSMutableArray array];
}

#pragma mark - 1.创建背景
-(void)createBackgroundView{
    self.backgroundImgView = [[PDImageView alloc]init];
    self.backgroundImgView.backgroundColor = [UIColor clearColor];
    
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        self.backgroundImgView.image = [UIImage imageNamed:@"icon_chaosFight_main_bg"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        self.backgroundImgView.image = [UIImage imageNamed:@"icon_chaosFight_main_bg_dota2"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        self.backgroundImgView.image = [UIImage imageNamed:@"icon_chaosFight_main_bg_king"];
    }
    
    self.backgroundImgView.frame = self.view.bounds;
    [self.view addSubview:self.backgroundImgView];
}

#pragma mark - 2. 创建返回按钮
-(void)createBackButton{
    self.backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.backButton.backgroundColor = [UIColor clearColor];
    __weak typeof(self)weakSelf = self;
    [self.backButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &gooutManagerWithBlockKey);
        if (block){
            block();
        }
    }];
    [self.backButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_back"] forState:UIControlStateNormal];
    self.backButton.backgroundColor = [UIColor clearColor];
    self.backButton.frame = CGRectMake(11, 11, 40, 40);
    [self.view addSubview:self.backButton];
}

-(void)gooutManagerWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &gooutManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 1.创建在线View
-(void)createOnLineView{
    self.onLineBgView = [[PDImageView alloc]init];
    self.onLineBgView.backgroundColor = [UIColor clearColor];
    self.onLineBgView.image = [Tool stretchImageWithName:@"icon_luandou_guize"];
    self.onLineBgView.frame = CGRectMake(kScreenBounds.size.width - 11 - 41, 9, 41, 41);
    [self.view addSubview:self.onLineBgView];
    self.onLineBgView.userInteractionEnabled = YES;
    
    self.guizeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.guizeButton.frame = self.onLineBgView.bounds;
    __weak typeof(self)weakSelf = self;
    [self.guizeButton buttonWithBlock:^(UIButton *button) {                 // 规则
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf showAlertWithGuize];
    }];
    [self.onLineBgView addSubview:self.guizeButton];
    
//    CGFloat margin = 3;
//    CGFloat height = (self.onLineBgView.size_height - 3 * margin) / 2.;
//    self.onlineLabel = [PDViewTool gameLabelFont:@"小提示" textColor:@"金"];
//    self.onlineLabel.text = @"0000人在线";
//    self.onlineLabel.adjustsFontSizeToFitWidth = YES;
//    self.onlineLabel.textAlignment = NSTextAlignmentCenter;
//    self.onlineLabel.textColor = [UIColor hexChangeFloat:@"fad679"];
//    self.onlineLabel.font = [self.onlineLabel.font boldFont];
//    self.onlineLabel.frame = CGRectMake(margin, margin, self.onLineBgView.size_width - 2 * margin, height);
//    [self.onLineBgView addSubview:self.onlineLabel];
//    
//    self.onlineHouseLabel = [PDViewTool gameLabelFont:@"小提示" textColor:@"金"];
//    self.onlineHouseLabel.text = @"当前房间000";
//    self.onlineHouseLabel.adjustsFontSizeToFitWidth = YES;
//    self.onlineHouseLabel.font = [self.onlineHouseLabel.font boldFont];
//    self.onlineHouseLabel.textAlignment = NSTextAlignmentCenter;
//    self.onlineHouseLabel.textColor = [UIColor hexChangeFloat:@"f27b4d"];
//    self.onlineHouseLabel.frame = CGRectMake(margin, CGRectGetMaxY(self.onlineLabel.frame) + margin, self.onlineLabel.size_width, height);
//    [self.onLineBgView addSubview:self.onlineHouseLabel];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.leftMutableArr){
        self.leftMutableArr = [NSMutableArray array];
    }
    if (!self.rightMutableArr){
        self.rightMutableArr = [NSMutableArray array];
    }
    CGFloat height_main = (self.bottomView.orgin_y - CGRectGetMaxY(self.backButton.frame) + 8 + 2);
    
    if (!self.leftTableView){
        self.leftTableView = [PDViewTool gwCreateTableViewRect:CGRectMake(11, CGRectGetMaxY(self.backButton.frame) + 2, 50, height_main)];
        self.leftTableView.delegate = self;
        self.leftTableView.dataSource = self;
        self.leftTableView.scrollEnabled = NO;
        [self.view addSubview:self.leftTableView];
    }
    if (!self.rightTableView){
        self.rightTableView = [PDViewTool gwCreateTableViewRect:CGRectMake(kScreenBounds.size.width - 11 - self.leftTableView.size_width, self.leftTableView.orgin_y, self.leftTableView.size_width, self.leftTableView.size_height)];
        self.rightTableView.dataSource = self;
        self.rightTableView.delegate = self;
        self.rightTableView.scrollEnabled = NO;
        [self.view addSubview:self.rightTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.leftTableView){
        return self.leftMutableArr.count;
    } else if (tableView == self.rightTableView){
        return self.rightMutableArr.count;
    }
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    // 动画
    static CGFloat initialDelay = 0.2f;
    static CGFloat stutter = 0.1f;
    
    if (tableView == self.leftTableView){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        PDChaosFightRoleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[PDChaosFightRoleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        cellWithRowOne.transferCellWidth = tableView.size_width;
        
        NSInteger index = indexPath.section;
        NSInteger yIndex = self.leftMutableArr.count - 1;
        if (index <= yIndex){
            PDChaosFightMembersSingleModel *singleModel = [self.leftMutableArr objectAtIndex:indexPath.section];
            cellWithRowOne.transferSingleModel = singleModel;
            if (singleModel.memberId.length){
                [cellWithRowOne memberGoinHouse:initialDelay + ((indexPath.section) * stutter) model:singleModel];
            }
        }
        
        return cellWithRowOne;
    } else if (tableView == self.rightTableView){
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        PDChaosFightRoleTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[PDChaosFightRoleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
        cellWithRowTwo.transferCellWidth = tableView.size_width;
        
        if (tableView == self.rightTableView){
            NSInteger index = indexPath.section;
            NSInteger yIndex = self.rightMutableArr.count - 1;
            if (index <= yIndex){
                PDChaosFightMembersSingleModel *singleModel = [self.rightMutableArr objectAtIndex:indexPath.section];
                cellWithRowTwo.transferSingleModel = singleModel;
                if (singleModel.memberId.length){
                    [cellWithRowTwo memberGoinHouse:initialDelay + ((indexPath.section) * stutter) model:singleModel];
                }
            }
        }
        return cellWithRowTwo;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    PDChaosFightPopUserAvatarView *avatarVC = [[PDChaosFightPopUserAvatarView  alloc]initWithFrame:CGRectMake((kScreenBounds.size.width - 200) / 2., (kScreenBounds.size.height - 200) / 2., 200, 200)];
    if (tableView == self.leftTableView){
        NSInteger index = indexPath.section;
        NSInteger yIndex = self.leftMutableArr.count - 1;
        if (index <= yIndex){
            PDChaosFightMembersSingleModel *singleModel = [self.leftMutableArr objectAtIndex:indexPath.section];
            if (singleModel.nickName.length && singleModel.avatar.length){
                [avatarVC viewShowWithAvatar:singleModel];
            }
            return;
        }
    } else if (tableView == self.rightTableView){
        NSInteger index = indexPath.section;
        NSInteger yIndex = self.rightMutableArr.count - 1;
        if (index <= yIndex){
            PDChaosFightMembersSingleModel *singleModel = [self.rightMutableArr objectAtIndex:indexPath.section];
            if (singleModel.nickName.length && singleModel.avatar.length){
                [avatarVC viewShowWithAvatar:singleModel];
            }
            return;
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.leftTableView){
        CGFloat mainHeight = self.bottomView.orgin_y - CGRectGetMaxY(self.backButton.frame) - 2 - 8;
        CGFloat singleHeight = (mainHeight - 4 * 5) / 4.;
        return singleHeight;
    } else {
        CGFloat mainHeight = self.bottomView.orgin_y - CGRectGetMaxY(self.onLineBgView.frame) - 8;
        CGFloat singleHeight = (mainHeight - 4 * 5) / 4.;
        return singleHeight;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}


-(void)viewLayerSetting{
    if (kScreenBounds.size.height < 375){
        // 1. 底部
        self.bottomView.frame = CGRectMake(0, kScreenBounds.size.height - 50, kScreenBounds.size.width, 50);
        self.userAvatarImgView.frame = CGRectMake(CFFloat(15), CFFloat(7), 50 - 2 * CFFloat(7), 50 - 2 * CFFloat(7));
        self.userTopAvatarImgView.frame = CGRectMake(CFFloat(15), CFFloat(7), 50 - 2 * CFFloat(11), 50 - 2 * CFFloat(11));
        self.userTopAvatarImgView.center = self.userAvatarImgView.center;
        CGFloat margin = CFFloat(5);
         CGFloat height = (self.bottomView.size_height - 3 * margin) / 2.;
        self.nickNamebgView.frame = CGRectMake(CGRectGetMaxX(self.userAvatarImgView.frame) + CFFloat(11), self.userAvatarImgView.orgin_y, 130,height);
        self.nickNameLabel.frame = CGRectMake(11, 0, self.nickNamebgView.size_width - 2 * 11, self.nickNamebgView.size_height);
        self.moneybgView.frame = CGRectMake(self.nickNamebgView.orgin_x, CGRectGetMaxY(self.nickNamebgView.frame) + margin, self.nickNamebgView.size_width, self.nickNamebgView.size_height);
        self.coinImgView.frame = CGRectMake(CFFloat(4), 0, self.moneybgView.size_height - 2 * CFFloat(3), self.moneybgView.size_height -  2 * CFFloat(3));
        self.coinAddImgView.frame = CGRectMake(self.moneybgView.size_width - 4 - (self.moneybgView.size_height - 2 * CFFloat(3)), CFFloat(3), self.moneybgView.size_height - 2 * CFFloat(3), self.moneybgView.size_height - 2 * CFFloat(3));
        self.countButton.frame = CGRectMake(kScreenBounds.size.width - 11 - 75 , (self.bottomView.size_height - (self.bottomView.size_height - 2 * CFFloat(3))) / 2., 75, self.bottomView.size_height - 2 * CFFloat(3));
        CGFloat lastBottomWidth = self.countButton.orgin_x - CFFloat(11) - CGRectGetMaxX(self.nickNamebgView.frame) - CFFloat(11);
        
        CGSize bottomBtnSize = CGSizeMake((self.bottomView.size_height - 2 * CFFloat(3))  * 1.0 * 111 / 52, (self.bottomView.size_height - 2 * CFFloat(3)));
//        if (lastBottomWidth > 3 *bottomBtnSize.width){
//            self.oneButton.frame = CGRectMake(CGRectGetMaxX(self.nickNamebgView.frame) + CFFloat(11), CFFloat(3), bottomBtnSize.width, bottomBtnSize.height);
//            self.twoButton.frame = CGRectMake(CGRectGetMaxX(self.oneButton.frame), CFFloat(3), bottomBtnSize.width, bottomBtnSize.height);
//            self.threeButton.frame = CGRectMake(CGRectGetMaxX(self.twoButton.frame), CFFloat(3), bottomBtnSize.width, bottomBtnSize.height);
//        } else {
//            CGFloat bottomBtnMargin = 5;
//            CGFloat bottomBtnSingleWidth = (lastBottomWidth - 4 * bottomBtnMargin) / 3.;
//            CGFloat bottomBtnSigleHeight = (bottomBtnSingleWidth  * 1.0 * 52 / 111.);
//            self.oneButton.frame = CGRectMake(CGRectGetMaxX(self.nickNamebgView.frame) + bottomBtnMargin, (self.bottomView.size_height - bottomBtnSigleHeight) / 2., bottomBtnSingleWidth, bottomBtnSigleHeight);
//            self.twoButton.frame = CGRectMake(CGRectGetMaxX(self.oneButton.frame) + bottomBtnMargin, (self.bottomView.size_height - bottomBtnSigleHeight) / 2., bottomBtnSingleWidth, bottomBtnSigleHeight);
//            self.threeButton.frame = CGRectMake(CGRectGetMaxX(self.twoButton.frame) + bottomBtnMargin, (self.bottomView.size_height - bottomBtnSigleHeight) / 2., bottomBtnSingleWidth, bottomBtnSigleHeight);
//        }
        
        // 1. count
        self.countButton.frame = CGRectMake(kScreenBounds.size.width - 5 - self.countButton.size_width, 7 + (self.bottomView.size_height - 7 - self.countButton.size_height) / 2., self.countButton.size_width, self.countButton.size_height);
        
        self.cutButton.frame = CGRectMake(self.countButton.orgin_x - 5 - self.cutButton.size_width, self.countButton.orgin_y, self.cutButton.size_width, self.countButton.size_height);
        
        self.mainScrollView.frame = CGRectMake(CGRectGetMaxX(self.nickNamebgView.frame), 7, self.cutButton.orgin_x - CGRectGetMaxX(self.nickNamebgView.frame), self.bottomView.size_height - 7);
        
        CGFloat btn_margin = 3;
        CGFloat btn_width = (self.mainScrollView.size_width - 4 * margin) / 3.;
        CGFloat btn_height = (btn_width * 1.f * 51 / 111);
        for (int i = 0 ; i < 6 ; i ++){
            CGFloat origin_x = btn_margin + i % 3 * (btn_margin + btn_width);
            CGFloat origin_y = 0;
            if (i / 3 == 0){
                origin_y = (self.mainScrollView.size_height - btn_height) / 2.;
            } else {
                origin_y = (self.mainScrollView.size_height  - btn_height) / 2. + self.mainScrollView.size_height;
            }
            if (i == 0){
                self.oneButton.frame = CGRectMake(origin_x, origin_y, btn_width, btn_height);
            } else if (i == 1){
                self.twoButton.frame = CGRectMake(origin_x, origin_y, btn_width, btn_height);
            } else if (i == 2){
                self.threeButton.frame = CGRectMake(origin_x, origin_y, btn_width, btn_height);
            } else if (i == 3){
                self.fourButton.frame = CGRectMake(origin_x, origin_y, btn_width, btn_height);
            } else if (i == 4){
                self.fivButton.frame = CGRectMake(origin_x, origin_y, btn_width, btn_height);
            } else if (i == 5){
                self.sixButton.frame = CGRectMake(origin_x, origin_y, btn_width, btn_height);
            }
        }
        
        

        
        
        
        
        // closeBtn
        self.backButton.frame = CGRectMake(CFFloat(11), CFFloat(11), CFFloat(40),CFFloat(40));
        // rightRole
        CGFloat onLineBgViewMargin = 3;
        CGFloat onLineBgViewheight = (self.onLineBgView.size_height - 3 * onLineBgViewMargin) / 2.;

        self.onLineBgView.frame = CGRectMake(kScreenBounds.size.width - CFFloat(11) - CFFloat(41), self.backButton.orgin_y, CFFloat(41), CFFloat(41));
//        self.onlineLabel.frame = CGRectMake(onLineBgViewMargin, onLineBgViewMargin, self.onLineBgView.size_width - 2 * onLineBgViewMargin, onLineBgViewheight);
//        self.onlineHouseLabel.frame = CGRectMake(onLineBgViewMargin, CGRectGetMaxY(self.onlineLabel.frame) + onLineBgViewMargin, self.onlineLabel.size_width, self.onlineLabel.size_height);
        
        self.guizeButton.frame = self.onLineBgView.bounds;
        
        // tableView
        CGFloat leftTableView_height_main = (self.bottomView.orgin_y - CGRectGetMaxY(self.backButton.frame) + 8 + 2);
        
        CGFloat tamp_table_height = 60 * 4 + 4 * 5;
        CGFloat tamp_table_width = 50 * 1.0 * leftTableView_height_main / tamp_table_height;
        
        self.leftTableView.frame = CGRectMake(CFFloat(11), CGRectGetMaxY(self.backButton.frame) + 2, tamp_table_width, leftTableView_height_main);
        CGFloat rightTableView_height_main = (self.bottomView.orgin_y - CGRectGetMaxY(self.onLineBgView.frame) - 8);
        
        self.rightTableView.frame = CGRectMake(kScreenBounds.size.width - CFFloat(11) - self.leftTableView.size_width, CGRectGetMaxY(self.onLineBgView.frame) + 2, self.leftTableView.size_width, rightTableView_height_main);
        [self.leftTableView reloadData];
        [self.rightTableView reloadData];
        
        
        // 牌桌
        CGFloat table_margin = 11;
        CGFloat table_width = self.rightTableView.orgin_x - CGRectGetMaxX(self.leftTableView.frame);
        CGFloat single_table_Width = (table_width - 5 * table_margin) / 2.;
        // 牌桌背景
        self.leftCardTableImgView.frame = CGRectMake(CGRectGetMaxX(self.leftTableView.frame) + 2 * table_margin, self.bottomView.orgin_y - table_margin - single_table_Width / 1.9, single_table_Width, single_table_Width / 1.9);
        self.rightCardTableImgView.frame = CGRectMake(CGRectGetMaxX(self.leftCardTableImgView.frame) + table_margin, self.leftCardTableImgView.orgin_y, self.leftCardTableImgView.size_width, self.leftCardTableImgView.size_height);
        self.topCardTableImgView.frame = CGRectMake(self.leftCardTableImgView.orgin_x, CGRectGetMaxY(self.timeImgView.frame) + 10, table_width - 4 * table_margin, self.leftCardTableImgView.orgin_y - table_margin - (CGRectGetMaxY(self.timeImgView.frame) + 10));

        self.leftCardtopTitleImgView.frame = CGRectMake(20, 25, 35, 37);
        
        CGFloat table_margin_wid = CFFloat(15);
        CGFloat table_margin_height = CFFloat(15);
        CGFloat table_singl_width = (self.leftCardTableImgView.size_width - CGRectGetMaxX(self.leftCardtopTitleImgView.frame) - 3 * table_margin_wid) / 2.;
        CGFloat table_single_height = (self.leftCardTableImgView.size_height - 3 * table_margin_height) / 2.;

        self.leftBottomTable1.frame = CGRectMake(CGRectGetMaxX(self.leftCardtopTitleImgView.frame) + table_margin_wid, table_margin_height, table_singl_width, table_single_height);
        self.leftBottomTable2.frame = CGRectMake(CGRectGetMaxX(self.leftBottomTable1.frame) + table_margin_wid, self.leftBottomTable1.orgin_y, self.leftBottomTable1.size_width, self.leftBottomTable1.size_height);
        self.leftBottomTable3.frame = CGRectMake(self.leftBottomTable1.orgin_x, CGRectGetMaxY(self.leftBottomTable1.frame) + table_margin_height, self.leftBottomTable1.size_width, self.leftBottomTable1.size_height);
        self.leftBottomTable4.frame = CGRectMake(self.leftBottomTable2.orgin_x, self.leftBottomTable3.orgin_y, self.leftBottomTable1.size_width, self.leftBottomTable1.size_height);
        
        self.rightBottomTable1.frame = CGRectMake(CGRectGetMaxX(self.rightCardtopTitleImgView.frame) + table_margin_wid, table_margin_height, table_singl_width, table_single_height);
        self.rightBottomTable2.frame = CGRectMake(CGRectGetMaxX(self.rightBottomTable1.frame) + table_margin_wid, self.rightBottomTable1.orgin_y, self.rightBottomTable1.size_width, self.rightBottomTable1.size_height);
        self.rightBottomTable3.frame = CGRectMake(self.rightBottomTable1.orgin_x, CGRectGetMaxY(self.rightBottomTable1.frame) + table_margin_height, self.rightBottomTable1.size_width, self.rightBottomTable1.size_height);
        self.rightBottomTable4.frame = CGRectMake(self.leftBottomTable2.orgin_x, self.leftBottomTable3.orgin_y, self.rightBottomTable1.size_width, self.rightBottomTable1.size_height);
    }
    
    [self createBottonLeftTableView];
    [self createBottonRightTableView];
    [self createBottonTopTableView];
    [self touzhuViewAddToMutableArr];
}

#pragma mark - 统计
-(void)createBottomView{
    self.bottomView = [[PDImageView alloc]init];
    self.bottomView.backgroundColor = [UIColor clearColor];
    self.bottomView.image = [Tool stretchImageWithName:@"icon_chaosFight_bottom_bg"];
    self.bottomView.frame = CGRectMake(0, kScreenBounds.size.height - 60, kScreenBounds.size.width, 60);
    self.bottomView.userInteractionEnabled = YES;
    [self.view addSubview:self.bottomView];
    
    // 2. 创建头像
    self.userAvatarImgView = [[PDImageView alloc]init];
    self.userAvatarImgView.backgroundColor = [UIColor clearColor];
    self.userAvatarImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_bottom_role_bg"];
    self.userAvatarImgView.frame = CGRectMake(15, 7, 60 - 2 * 7, 60 - 2 * 7);
    [self.bottomView addSubview:self.userAvatarImgView];
    
    // 2.1 创建上层的头像
    self.userTopAvatarImgView = [[PDImageView alloc]init];
    self.userTopAvatarImgView.backgroundColor = [UIColor clearColor];
    self.userTopAvatarImgView.frame = CGRectMake(15, 7, 60 - 2 * 11, 60 - 2 * 11);
    [self.bottomView addSubview:self.userTopAvatarImgView];
    [self.userTopAvatarImgView uploadImageWithAvatarURL:[AccountModel sharedAccountModel].memberInfo.avatar placeholder:nil callback:NULL];
    self.userTopAvatarImgView.center = self.userAvatarImgView.center;
    
    // 3. 创建阴影view
    CGFloat margin = 5;
    CGFloat height = (self.bottomView.size_height - 3 * margin) / 2.;
    self.nickNamebgView = [[PDImageView alloc]init];
    self.nickNamebgView.backgroundColor = [UIColor clearColor];
    self.nickNamebgView.image = [Tool stretchImageWithName:@"icon_chaosFight_bottom_scr"];
    self.nickNamebgView.frame = CGRectMake(CGRectGetMaxX(self.userAvatarImgView.frame) + 11, margin, 130, height);
    [self.bottomView addSubview:self.nickNamebgView];
    
    // 3.1 创建nickNameLabel
    self.nickNameLabel = [[UILabel alloc]init];
    self.nickNameLabel.backgroundColor = [UIColor clearColor];
    self.nickNameLabel.font = [UIFont systemFontOfSize:15.];
    self.nickNameLabel.adjustsFontSizeToFitWidth = YES;
    self.nickNameLabel.frame = CGRectMake(11, 0, self.nickNamebgView.size_width - 2 * 11, self.nickNamebgView.size_height);
    self.nickNameLabel.textAlignment = NSTextAlignmentCenter;
    self.nickNameLabel.textColor = [UIColor whiteColor];
    self.nickNameLabel.text = @"";
    [self.nickNamebgView addSubview:self.nickNameLabel];
    
    // 3.2
    self.moneybgView = [[PDImageView alloc]init];
    self.moneybgView.backgroundColor = [UIColor clearColor];
    self.moneybgView.image = [Tool stretchImageWithName:@"icon_chaosFight_bottom_scr"];
    self.moneybgView.frame = CGRectMake(self.nickNamebgView.orgin_x, CGRectGetMaxY(self.nickNamebgView.frame) + margin, self.nickNamebgView.size_width, self.nickNamebgView.size_height);
    [self.bottomView addSubview:self.moneybgView];
    
    // 3.3 金币
    self.coinImgView = [[PDImageView alloc]init];
    self.coinImgView.backgroundColor = [UIColor clearColor];
    self.coinImgView.image = [UIImage imageNamed:@"icon_chaosFight_coin"];
    self.coinImgView.frame = CGRectMake(4, 0, 19, 20);
    self.coinImgView.center_y = self.moneybgView.size_height / 2.;
    [self.moneybgView addSubview:self.coinImgView];
    
    // 3.4
    self.coinAddImgView = [[PDImageView alloc]init];
    self.coinAddImgView.backgroundColor = [UIColor clearColor];
    self.coinAddImgView.image = [UIImage imageNamed:@"icon_chaosFight_add"];
    self.coinAddImgView.frame = CGRectMake(self.moneybgView.size_width - 4 - 17, 0, 17, 17);
    self.coinAddImgView.center_y = self.coinImgView.center_y;
    [self.moneybgView addSubview:self.coinAddImgView];
    
    // 3.5
    self.coinLabel = [[UILabel alloc]init];
    self.coinLabel.backgroundColor = [UIColor clearColor];
    self.coinLabel.adjustsFontSizeToFitWidth = YES;
    self.coinLabel.textColor = [UIColor whiteColor];
    self.coinLabel.text = @"0";
    self.coinLabel.frame = CGRectMake(CGRectGetMaxX(self.coinImgView.frame) + 4, 0, self.coinAddImgView.orgin_x - 4 - (CGRectGetMaxX(self.coinImgView.frame) + 4), self.moneybgView.size_height);
    self.coinLabel.textAlignment = NSTextAlignmentCenter;
    self.coinLabel.font = [UIFont systemFontOfSize:15.];
    [self.moneybgView addSubview:self.coinLabel];
    
    // 4. 创建统计按钮
    self.countButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.countButton.frame = CGRectMake(kScreenBounds.size.width - 11 - 75, (self.bottomView.size_height - 52) / 2. + 7, 75, 52);
    __weak typeof(self)weakSelf = self;
    [self.countButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_tongji"] forState:UIControlStateNormal];
    [self.countButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &tongjiBtnClickBlockKey);
        if (block){
            block();
        }
    }];
    self.countButton.backgroundColor = [UIColor clearColor];
    [self.bottomView addSubview:self.countButton];
    
    // 5. 创建切换按钮
    self.cutButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.cutButton.frame = CGRectMake(self.countButton.orgin_x - 11 - 60, self.countButton.orgin_y, 60, 52);
    [self.cutButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_change_nor"] forState:UIControlStateNormal];
    [self.cutButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->bottom_page_two = !strongSelf->bottom_page_two;
        if (strongSelf->bottom_page_two == YES){
            [strongSelf.mainScrollView setContentOffset:CGPointMake(0, strongSelf.mainScrollView.size_height) animated:YES];
            [strongSelf.cutButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_change_hlt"] forState:UIControlStateNormal];
        } else {
            [strongSelf.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            [strongSelf.cutButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_change_nor"] forState:UIControlStateNormal];
        }
    }];
    self.cutButton.backgroundColor = [UIColor clearColor];
    [self.bottomView addSubview:self.cutButton];
    
    // 6. 创建scrollview
    self.mainScrollView = [[UIScrollView alloc]init];
    self.mainScrollView.frame = CGRectMake(CGRectGetMaxX(self.nickNamebgView.frame) + 11, 0, self.cutButton.orgin_x - 11 - CGRectGetMaxX(self.nickNamebgView.frame) - 11, self.bottomView.size_height);
    self.mainScrollView.userInteractionEnabled = YES;
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    [self.bottomView addSubview:self.mainScrollView];
    
    CGFloat mainScrollView_w = self.mainScrollView.size_width;
    CGFloat singleBtn_w = (mainScrollView_w - 4 * 11) / 3.;
    
    for (int i = 0 ; i < 6;i++){
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.userInteractionEnabled = YES;
        [button buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            if (i == 0){
                [strongSelf touzhuManagerWithType:touzhuCountType100];
            } else if (i == 1){
                [strongSelf touzhuManagerWithType:touzhuCountType1000];
            } else if (i == 2){
                [strongSelf touzhuManagerWithType:touzhuCountType10000];
            } else if (i == 3){
                [strongSelf touzhuManagerWithType:touzhuCountType50000];
            } else if (i == 4){
                [strongSelf touzhuManagerWithType:touzhuCountType100000];
            } else if (i == 5){
                [strongSelf touzhuManagerWithType:touzhuCountTypeALL];
            }
        }];
        
        CGFloat btn_width = 0;
        CGFloat btn_height = 0;
        
        if (singleBtn_w < 111){
            btn_width = singleBtn_w;
            btn_height = 52 * singleBtn_w / 111;
        } else {
            btn_width = 111;
            btn_height = 52;
        }
        
        CGFloat margin_height = (self.bottomView.size_height - 7 - btn_height) / 2.;
        CGFloat origin_x = 11 + i % 3 * (11 + btn_width);
        CGFloat origin_y = 0;
        if (i / 3 == 0){
            origin_y = margin_height + 7;
        } else {
            origin_y = self.mainScrollView.size_height + margin_height + 7;
        }
        
        button.frame = CGRectMake(origin_x, origin_y, btn_width, btn_height);
        [self.mainScrollView addSubview:button];
        
        if (i == 0){
            [button setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100_nor"] forState:UIControlStateNormal];
            self.oneButton = button;
        } else if (i == 1){
            [button setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_1000_nor"] forState:UIControlStateNormal];
            self.twoButton = button;
        } else if (i == 2){
            [button setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_10000_nor"] forState:UIControlStateNormal];
            self.threeButton = button;
        } else if (i == 3){
            [button setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_50000_nor"] forState:UIControlStateNormal];
            self.fourButton = button;
        } else if (i == 4){
            [button setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100000_nor"] forState:UIControlStateNormal];
            self.fivButton = button;
        } else if (i == 5){
            [button setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_all_nor"] forState:UIControlStateNormal];
            self.sixButton = button;
        }
    }
    
    [self touzhuManagerWithType:touzhuCountType100];
    
    // 创建按钮
    self.chongzhiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.chongzhiButton.frame = CGRectMake(self.nickNamebgView.orgin_x, 0, CGRectGetMaxX(self.nickNamebgView.frame) - self.nickNamebgView.orgin_x, self.bottomView.size_height);
    [self.bottomView addSubview:self.chongzhiButton];
    
    // 创建胜利label
    [self createMineWinLabel];
}

#pragma mark - 创建标题view
-(void)createTitleImgView{
    self.titleImgView = [[PDImageView alloc]init];
    self.titleImgView.frame = CGRectMake((kScreenBounds.size.width - 126) / 2., 11, 126, 23);
    self.titleImgView.image = [UIImage imageNamed:@"icon_chaosFight_top_title"];
    self.titleImgView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.titleImgView];
    
    self.timeImgView = [[PDImageView alloc]init];
    self.timeImgView.backgroundColor = [UIColor clearColor];
    self.timeImgView.frame = CGRectMake((kScreenBounds.size.width - 45/2.) / 2., CGRectGetMaxY(self.titleImgView.frame) + 20, 45/2., 45/2.);
    [self.view addSubview:self.timeImgView];
}

-(void)changeTitleImgType:(touzhuGameType)type{
    _transferGameType = type;
    
    [UIView animateWithDuration:.5f animations:^{
        self.titleImgView.alpha = 0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.5f animations:^{
            self.titleImgView.alpha = 1;
            if (type == touzhuGameTypeNormal){              // 可投注
                self.titleImgView.image = [UIImage imageNamed:@"icon_chaosFight_start"];
            } else if (type == touzhuGameTypeReadyGo){      // 即将开始
                self.titleImgView.image = [UIImage imageNamed:@"icon_chaosFight_start"];
            } else if (type == touzhuGameTypeEnd){          // 无法投注
                self.titleImgView.image = [UIImage imageNamed:@"icon_chaosFight_ending"];
                
            }
        } completion:^(BOOL finished) {
            
        }];
    }];
}

#pragma mark - 创建牌桌
-(void)createCardTable{
    CGFloat margin = 11;
    CGFloat width = self.rightTableView.orgin_x - CGRectGetMaxX(self.leftTableView.frame);
    CGFloat singleWidth = (width - 5 * margin) / 2.;
    self.leftCardTableImgView = [[PDImageView alloc]init];
    self.leftCardTableImgView.frame = CGRectMake(CGRectGetMaxX(self.leftTableView.frame) + 2 * margin, self.bottomView.orgin_y - margin - singleWidth / 1.7, singleWidth, singleWidth / 1.7);
    self.leftCardTableImgView.backgroundColor = [UIColor clearColor];
    self.leftCardTableImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bg2"];
    [self.view addSubview:self.leftCardTableImgView];
    self.leftCardTableImgView.userInteractionEnabled = YES;
    // 添加内容页
    
    self.rightCardTableImgView = [[PDImageView alloc]init];
    self.rightCardTableImgView.frame = CGRectMake(CGRectGetMaxX(self.leftCardTableImgView.frame) + margin, self.leftCardTableImgView.orgin_y, self.leftCardTableImgView.size_width, self.leftCardTableImgView.size_height);
    self.rightCardTableImgView.backgroundColor = [UIColor clearColor];
    self.rightCardTableImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bg2"];
    [self.view addSubview:self.rightCardTableImgView];
    self.rightCardTableImgView.userInteractionEnabled = YES;
    

    self.topCardTableImgView = [[PDImageView alloc]init];
    self.topCardTableImgView.backgroundColor = [UIColor clearColor];
    self.topCardTableImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bg3"];
    self.topCardTableImgView.frame = CGRectMake(self.leftCardTableImgView.orgin_x, CGRectGetMaxY(self.timeImgView.frame) + 10, width - 4 * margin, self.leftCardTableImgView.orgin_y - margin - (CGRectGetMaxY(self.timeImgView.frame) + 10));
    [self.view addSubview:self.topCardTableImgView];
    
    self.topCardTableImgView.userInteractionEnabled = YES;
    
}

-(void)createBottonLeftTableView{
    self.leftCardtopTitleImgView = [[PDImageView alloc]init];
    self.leftCardtopTitleImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_anyone"];
    self.leftCardtopTitleImgView.frame = CGRectMake(20, 25, 35, 37);
    [self.leftCardTableImgView addSubview:self.leftCardtopTitleImgView];

    CGFloat margin_wid = 15;
    CGFloat margin_height = 15;
    CGFloat width = (self.leftCardTableImgView.size_width - CGRectGetMaxX(self.leftCardtopTitleImgView.frame) - 3 * margin_wid) / 2.;
    CGFloat height = (self.leftCardTableImgView.size_height - 3 * margin_height) / 2.;
    
    self.leftBottomTable1 = [[PDChaosFightTouzhuView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.leftCardtopTitleImgView.frame) + margin_wid, margin_height, width, height)];
    self.leftBottomTable1.transferType = chaosTouzhuTypeShuangsha;
    [self.leftCardTableImgView addSubview:self.leftBottomTable1];
    
    self.leftBottomTable2 = [[PDChaosFightTouzhuView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.leftBottomTable1.frame) + margin_wid, self.leftBottomTable1.orgin_y, self.leftBottomTable1.size_width, self.leftBottomTable1.size_height)];
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        self.leftBottomTable2.transferType = chaosTouzhuTypeXiaolong;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        self.leftBottomTable2.transferType = chaosTouzhuTypeTyrant;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        self.leftBottomTable2.transferType = chaosTouzhuTypeBlackDragonet;
    }
    
    [self.leftCardTableImgView addSubview:self.leftBottomTable2];
    
    self.leftBottomTable3 = [[PDChaosFightTouzhuView alloc]initWithFrame:CGRectMake(self.leftBottomTable1.orgin_x, CGRectGetMaxY(self.leftBottomTable1.frame) + margin_height, self.leftBottomTable1.size_width, self.leftBottomTable1.size_height)];
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        self.leftBottomTable3.transferType = chaosTouzhuTypeDalong;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        self.leftBottomTable3.transferType = chaosTouzhuTypeMaster;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        self.leftBottomTable3.transferType = chaosTouzhuTypeMeatMountain;
    }
    [self.leftCardTableImgView addSubview:self.leftBottomTable3];
    
    self.leftBottomTable4 = [[PDChaosFightTouzhuView alloc] initWithFrame:CGRectMake(self.leftBottomTable2.orgin_x, self.leftBottomTable3.orgin_y, self.leftBottomTable1.size_width, self.leftBottomTable1.size_height)];
    self.leftBottomTable4.transferType = chaosTouzhuTypeShuangkong;
    [self.leftCardTableImgView addSubview:self.leftBottomTable4];
    
    self.leftCardtopTitleImgView.center_y = self.leftBottomTable1.center_y;
}

-(void)createBottonRightTableView{
    self.rightCardtopTitleImgView = [[PDImageView alloc]init];
    self.rightCardtopTitleImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_allGame"];
    self.rightCardtopTitleImgView.frame = CGRectMake(20, 25, 35, 17);
    [self.rightCardTableImgView addSubview:self.rightCardtopTitleImgView];
    
    CGFloat margin_wid = 15;
    CGFloat margin_height = 15;
    CGFloat width = (self.rightCardTableImgView.size_width - CGRectGetMaxX(self.rightCardtopTitleImgView.frame) - 3 * margin_wid) / 2.;
    CGFloat height = (self.rightCardTableImgView.size_height - 3 * margin_height) / 2.;
    
    self.rightBottomTable1 = [[PDChaosFightTouzhuView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.rightCardtopTitleImgView.frame) + margin_wid, margin_height, width, height)];
    self.rightBottomTable1.transferType = chaosTouzhuTypeYizhi;
    [self.rightCardTableImgView addSubview:self.rightBottomTable1];
    
    self.rightBottomTable2 = [[PDChaosFightTouzhuView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.rightBottomTable1.frame) + margin_wid, self.rightBottomTable1.orgin_y, self.rightBottomTable1.size_width, self.rightBottomTable1.size_height)];
    self.rightBottomTable2.transferType = chaosTouzhuTypeLiangzhi;
    [self.rightCardTableImgView addSubview:self.rightBottomTable2];
    
    self.rightBottomTable3 = [[PDChaosFightTouzhuView alloc]initWithFrame:CGRectMake(self.rightBottomTable1.orgin_x, CGRectGetMaxY(self.rightBottomTable1.frame) + margin_height, self.rightBottomTable1.size_width, self.rightBottomTable1.size_height)];
    self.rightBottomTable3.transferType = chaosTouzhuTypeSanzhi;
    [self.rightCardTableImgView addSubview:self.rightBottomTable3];
    
    self.rightBottomTable4 = [[PDChaosFightTouzhuView alloc] initWithFrame:CGRectMake(self.rightBottomTable2.orgin_x, self.rightBottomTable3.orgin_y, self.rightBottomTable1.size_width, self.rightBottomTable1.size_height)];
    self.rightBottomTable4.transferType = chaosTouzhuTypeSizhi;
    [self.rightCardTableImgView addSubview:self.rightBottomTable4];
    
    self.rightCardtopTitleImgView.center_y = self.rightBottomTable1.center_y;
}

-(void)createBottonTopTableView{
    CGFloat top_margin = 10;
    CGFloat width_margin = 15;
    CGFloat width = (self.topCardTableImgView.size_width - 6 * width_margin) / 3.;
    
    CGRect rect1;
    CGRect rect2;
    CGRect rect3;

    CGFloat top_table_width = (self.topCardTableImgView.size_width - 6 * width_margin) / 3.;
    
    if (kScreenBounds.size.height < 375){

        rect1 = CGRectMake(width_margin, top_margin, top_table_width, self.topCardTableImgView.size_height - 2 * top_margin);
        rect2 = CGRectMake(CGRectGetMaxX(self.topTable1.frame) + width_margin * 2, top_margin, top_table_width, self.topCardTableImgView.size_height - 2 * top_margin);
        rect3 = CGRectMake(CGRectGetMaxX(self.topTable2.frame) + width_margin * 2, top_margin, top_table_width, self.topCardTableImgView.size_height - 2 * top_margin);
    } else {
        rect1 = CGRectMake(width_margin, top_margin, width, self.topCardTableImgView.size_height - 2 * top_margin);
        rect2 = CGRectMake(CGRectGetMaxX(self.topTable1.frame) + width_margin * 2, top_margin, width, self.topCardTableImgView.size_height - 2 * top_margin);
        rect3 = CGRectMake(CGRectGetMaxX(self.topTable2.frame) + width_margin * 2, top_margin, width, self.topCardTableImgView.size_height - 2 * top_margin);
    }
    
    self.topTable1 = [[PDChaosFightTouzhuView alloc]initWithFrame:rect1];
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        self.topTable1.transferType = chaosTouzhuTypeTuoersuo;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        self.topTable1.transferType = chaosTouzhuTypeLangRen;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        self.topTable1.transferType = chaosTouzhuTypeXiahoudun;
    }
    
    [self.topCardTableImgView addSubview:self.topTable1];

    
    if (kScreenBounds.size.height < 375){
        rect2 = CGRectMake(CGRectGetMaxX(self.topTable1.frame) + width_margin * 2, top_margin, top_table_width, self.topCardTableImgView.size_height - 2 * top_margin);
    } else {
        rect2 = CGRectMake(CGRectGetMaxX(self.topTable1.frame) + width_margin * 2, top_margin, width, self.topCardTableImgView.size_height - 2 * top_margin);
    }
    self.topTable2 = [[PDChaosFightTouzhuView alloc]initWithFrame:rect2];
    self.topTable2.transferType = chaosTouzhuTypePingshou;
    [self.topCardTableImgView addSubview:self.topTable2];
    
    if (kScreenBounds.size.height < 375){
        rect3 = CGRectMake(CGRectGetMaxX(self.topTable2.frame) + width_margin * 2, top_margin, top_table_width, self.topCardTableImgView.size_height - 2 * top_margin);
    } else {
        rect3 = CGRectMake(CGRectGetMaxX(self.topTable2.frame) + width_margin * 2, top_margin, width, self.topCardTableImgView.size_height - 2 * top_margin);
    }
    self.topTable3 = [[PDChaosFightTouzhuView alloc]initWithFrame:rect3];
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
    self.topTable3.transferType = chaosTouzhuTypeertongjie;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        self.topTable3.transferType = chaosTouzhuTypeMori;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        self.topTable3.transferType = chaosTouzhuTypeChengyaojin;
    }
    [self.topCardTableImgView addSubview:self.topTable3];
}

-(void)touzhuViewAddToMutableArr{
    if (!self.topPriceMutableArr){
        self.topPriceMutableArr = [NSMutableArray array];
    }
    if (![self.topPriceMutableArr containsObject:self.topTable1]){
        [self.topPriceMutableArr addObject:self.topTable1];
    }
    if (![self.topPriceMutableArr containsObject:self.topTable2]){
        [self.topPriceMutableArr addObject:self.topTable2];
    }
    if (![self.topPriceMutableArr containsObject:self.topTable3]){
        [self.topPriceMutableArr addObject:self.topTable3];
    }
    if (![self.topPriceMutableArr containsObject:self.leftBottomTable1]){
        [self.topPriceMutableArr addObject:self.leftBottomTable1];
    }
    if (![self.topPriceMutableArr containsObject:self.leftBottomTable2]){
        [self.topPriceMutableArr addObject:self.leftBottomTable2];
    }
    if (![self.topPriceMutableArr containsObject:self.leftBottomTable3]){
        [self.topPriceMutableArr addObject:self.leftBottomTable3];
    }
    if (![self.topPriceMutableArr containsObject:self.leftBottomTable4]){
        [self.topPriceMutableArr addObject:self.leftBottomTable4];
    }
    if (![self.topPriceMutableArr containsObject:self.rightBottomTable1]){
        [self.topPriceMutableArr addObject:self.rightBottomTable1];
    }
    if (![self.topPriceMutableArr containsObject:self.rightBottomTable2]){
        [self.topPriceMutableArr addObject:self.rightBottomTable2];
    }
    if (![self.topPriceMutableArr containsObject:self.rightBottomTable3]){
        [self.topPriceMutableArr addObject:self.rightBottomTable3];
    }
    if (![self.topPriceMutableArr containsObject:self.rightBottomTable4]){
        [self.topPriceMutableArr addObject:self.rightBottomTable4];
    }
}

#pragma mark - 自动布局顶部的价格
-(void)auSetupPriceView{
    if (self.topPriceMutableArr.count){
        for (PDChaosFightTouzhuView *view in self.topPriceMutableArr){
            [view auSetupFrame];
        }
    }
}

#pragma mark - 创建图片
-(void)animationWithGuaishouManagerWithType:(killGuaishouType)type strIndex:(NSString *)index  block:(void(^)())block{

    // 2. 获取
    if (self.hasGoout){
        [self dismissCard];
        return;
    }
    
    CGFloat margin = 80;
    CGFloat height = kScreenBounds.size.height - 2 * margin;
    CGFloat width = height / 56 * 42;
    
    PDChaosCardView *cardView;
    if (type == killGuaishouType1){         // 第一只怪兽
        cardView = self.leftCard1;
    } else if (type == killGuaishouType2){      // 第二只怪兽
        cardView = self.rightCard1;
    } else if (type == killGuaishouType3){      // 第三只怪兽
        cardView = self.leftCard2;
    } else if (type == killGuaishouType4){      // 第四只怪兽
        cardView = self.rightCard2;
    }
    
    UIImage *img;
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        img = [UIImage imageNamed:[NSString stringWithFormat:@"icon_chaosFight_guaishou_%@",index]];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        img = [UIImage imageNamed:[NSString stringWithFormat:@"icon_chaosFight_guaishou_dota_%@",index]];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        img = [UIImage imageNamed:[NSString stringWithFormat:@"icon_chaosFight_guaishou_king_%@",index]];
    }
    NSString *indexStr = [NSString stringWithFormat:@"%@",index];
    if ([indexStr isEqualToString:@"0"]){
        NSInteger numberCard = random() % 9 + 1;
        img = [UIImage imageNamed:[NSString stringWithFormat:@"icon_chaosFight_guaishou_%li",(long)numberCard]];
        
        if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
            img = [UIImage imageNamed:[NSString stringWithFormat:@"icon_chaosFight_guaishou_%li",(long)numberCard]];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
            img = [UIImage imageNamed:[NSString stringWithFormat:@"icon_chaosFight_guaishou_dota_%li",(long)numberCard]];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
            img = [UIImage imageNamed:[NSString stringWithFormat:@"icon_chaosFight_guaishou_king_%li",(long)numberCard]];
        }
    }
    
    if (!cardView){
        cardView = [[PDChaosCardView alloc]initWithFrame:CGRectMake((kScreenBounds.size.width - width) / 2.,margin , width, height)];
        cardView.alpha = 0;
        cardView.transferImg = img;
        UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
        [keyWindow addSubview:cardView];
    } else {
        cardView.alpha = 0;
        cardView.transferImg = img;
        cardView.transferRect = CGRectMake((kScreenBounds.size.width - width) / 2.,margin , width, height);
    }
    
    // 1. 获取卡片1的目标地点
    CGFloat targetMargin = 5;
    CGFloat targetHeight = self.topCardTableImgView.orgin_y - CGRectGetMaxY(self.titleImgView.frame) - 2 * targetMargin;
    CGFloat targetWidth = targetHeight / 56 * 42;
    
    CGRect targetRect;
    if (type == killGuaishouType1){
        self.leftCard1 = cardView;
        targetRect = CGRectMake(self.timeImgView.orgin_x - 11 - targetWidth, CGRectGetMaxY(self.titleImgView.frame) + targetMargin, targetWidth, targetHeight);
    } else if (type == killGuaishouType2){
        self.rightCard1 = cardView;
        targetRect = CGRectMake(CGRectGetMaxX(self.timeImgView.frame) + 11 , CGRectGetMaxY(self.titleImgView.frame) + targetMargin, targetWidth, targetHeight);
    } else if (type == killGuaishouType3){
        self.leftCard2 = cardView;
        targetRect = CGRectMake(self.leftCard1.orgin_x - 11 - targetWidth, CGRectGetMaxY(self.titleImgView.frame) + targetMargin, targetWidth, targetHeight);
    } else if (type == killGuaishouType4){
        self.rightCard2 = cardView;
        targetRect = CGRectMake(CGRectGetMaxX(self.rightCard1.frame) + 11 , CGRectGetMaxY(self.titleImgView.frame) + targetMargin, targetWidth, targetHeight);
    }
    
    PDChaosCardViewType cardType = PDChaosCardViewTypeKill;
    NSString *mainIndex = [NSString stringWithFormat:@"%@",index];
    if ([mainIndex isEqualToString:@"0"]){
        cardType = PDChaosCardViewTypeMiss;
    } else {
        cardType = PDChaosCardViewTypeKill;
    }
    
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:.7f animations:^{
        cardView.alpha = 1;
    } completion:^(BOOL finished) {
      
        [cardView animationWithType:cardType block:^{
            if (!weakSelf){
                return ;
            }
    
            [UIView animateWithDuration:.3f animations:^{
                cardView.transferRect = targetRect;
            } completion:^(BOOL finished) {
                if ([mainIndex isEqualToString:@"0"]){
                    cardView.transferImg = [UIImage imageNamed:@"icon_chaosFight_guaishou_0"];
                }
                if (block){
                    block();
                }
            }];
        }];
    }];
}

#pragma mark - 怪兽卡片进行消失
-(void)guaishouCardDismiss{
    [UIView animateWithDuration:.5f animations:^{
        self.leftCard1.alpha = 0;
        self.leftCard1.orgin_y -= 30;
        self.leftCard2.alpha = 0;
        self.leftCard2.orgin_y -= 30;
        self.rightCard1.alpha = 0;
        self.rightCard1.orgin_y -= 30;
        self.rightCard2.alpha = 0;
        self.rightCard2.orgin_y -= 30;
    } completion:^(BOOL finished) {
        [self.leftCard1 removeFromSuperview];
        [self.leftCard2 removeFromSuperview];
        [self.rightCard1 removeFromSuperview];
        [self.rightCard2 removeFromSuperview];
        self.leftCard1 = nil;
        self.leftCard2 = nil;
        self.rightCard1 = nil;
        self.rightCard2 = nil;
    }];
}

-(void)dismissCard{
    [self.leftCard1 removeFromSuperview];
    [self.leftCard2 removeFromSuperview];
    [self.rightCard1 removeFromSuperview];
    [self.rightCard2 removeFromSuperview];
    self.leftCard1 = nil;
    self.leftCard2 = nil;
    self.rightCard1 = nil;
    self.rightCard2 = nil;
}

#pragma mark - 创建topWindow
-(void)showAlertToCount{
    CGFloat xWidth = kScreenBounds.size.width - 2 * 100;
    CGFloat yHeight = kScreenBounds.size.height - 2 * 60;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    PDChaosFightPopView *poplistview = [[PDChaosFightPopView alloc] initWithFrame:CGRectMake(100, yOffset, xWidth, yHeight)];
    [poplistview showChaosViewWithType:dayeAlertTypeTongji];
}

#pragma mark - 显示规则
-(void)showAlertWithGuize{
    CGFloat xWidth = kScreenBounds.size.width - 2 * 100;
    CGFloat yHeight = kScreenBounds.size.height - 2 * 60;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    PDChaosFightPopView *poplistview = [[PDChaosFightPopView alloc] initWithFrame:CGRectMake(100, yOffset, xWidth, yHeight)];
    [poplistview showChaosViewWithType:dayeAlertTypeGuize];
}

#pragma mark - 创建去充值
-(void)showAlertToChongzhiBlcok:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    CGFloat xWidth = kScreenBounds.size.width - 2 * 100;
    CGFloat yHeight = kScreenBounds.size.height - 2 * 60;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    PDChaosFightPopView *poplistview = [[PDChaosFightPopView alloc] initWithFrame:CGRectMake(100, yOffset, xWidth, yHeight)];
    [poplistview showNoYueToChongzhiWithBlock:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    
}
#pragma mark - 创建下注上线
-(void)showAlertTopShangxian{
    __weak typeof(self)weakSelf = self;
    CGFloat xWidth = kScreenBounds.size.width - 2 * 100;
    CGFloat yHeight = kScreenBounds.size.height - 2 * 60;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    PDChaosFightPopView *poplistview = [[PDChaosFightPopView alloc] initWithFrame:CGRectMake(100, yOffset, xWidth, yHeight)];
    [poplistview showShangxianWithBlock:^{
        if (!weakSelf){
            return ;
        }
    }];
}

#pragma mark - 被提出
-(void)showAlertBeitichuWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    CGFloat xWidth = kScreenBounds.size.width - 2 * 100;
    CGFloat yHeight = kScreenBounds.size.height - 2 * 60;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    PDChaosFightPopView *poplistview = [[PDChaosFightPopView alloc] initWithFrame:CGRectMake(100, yOffset, xWidth, yHeight)];
    [poplistview showTichuWithBlock:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
        
    }];
}



-(void)actionXiazhuManagerWithBlock:(void(^)(chaosTouzhuType type,PDChaosFightTouzhuView *touzhuView,NSInteger gold))block{
    __weak typeof(self)weakSelf = self;
    // 1. 托儿索胜利
    [self.topTable1 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (block){
            if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
                block(chaosTouzhuTypeTuoersuo,strongSelf.topTable1,strongSelf.goldCount);
            } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
                block(chaosTouzhuTypeLangRen,strongSelf.topTable1,strongSelf.goldCount);
            } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
                block(chaosTouzhuTypeXiahoudun,strongSelf.topTable1,strongSelf.goldCount);
            }
        }
    }];
    
    // 2. 平局
    [self.topTable2 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
         
        if (block){
            block(chaosTouzhuTypePingshou,strongSelf.topTable2,strongSelf.goldCount);
        }
    }];
    
    // 3. 创建儿童节胜利
    [self.topTable3 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
         
        if (block){
            if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
                block(chaosTouzhuTypeertongjie,strongSelf.topTable3,strongSelf.goldCount);
            } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
                block(chaosTouzhuTypeMori,strongSelf.topTable3,strongSelf.goldCount);
            } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
                block(chaosTouzhuTypeChengyaojin,strongSelf.topTable3,strongSelf.goldCount);
            }
        }
        
    }];
    
    // 4. 创建双杀
    [self.leftBottomTable1 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
         
        if (block){
            block(chaosTouzhuTypeShuangsha,strongSelf.leftBottomTable1,strongSelf.goldCount);
        }
    }];
    
    // 5. 创建小龙
    [self.leftBottomTable2 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
         
        if (block){
            if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
                block(chaosTouzhuTypeXiaolong,strongSelf.leftBottomTable2,strongSelf.goldCount);
            } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
                block(chaosTouzhuTypeBlackDragonet,strongSelf.leftBottomTable2,strongSelf.goldCount);
            } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
                block(chaosTouzhuTypeTyrant,strongSelf.leftBottomTable2,strongSelf.goldCount);
            }
        }
    }];
    
    // 6. 创建大龙
    [self.leftBottomTable3 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        
        if (block){
            if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
                block(chaosTouzhuTypeDalong,strongSelf.leftBottomTable3,strongSelf.goldCount);
            } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
                block(chaosTouzhuTypeMeatMountain,strongSelf.leftBottomTable3,strongSelf.goldCount);
            } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
                block(chaosTouzhuTypeMaster,strongSelf.leftBottomTable3,strongSelf.goldCount);
            }
        }
    }];
    
    // 7 .双空
    [self.leftBottomTable4 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
         
        if (block){
            block(chaosTouzhuTypeShuangkong,strongSelf.leftBottomTable4,strongSelf.goldCount);
        }
    }];
    
    // 8.一只
    [self.rightBottomTable1 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
         
        if (block){
            block(chaosTouzhuTypeYizhi,strongSelf.rightBottomTable1,strongSelf.goldCount);
        }
    }];
    
    // 9. 两只
    [self.rightBottomTable2 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
         
        if (block){
            block(chaosTouzhuTypeLiangzhi,strongSelf.rightBottomTable2,strongSelf.goldCount);
        }
    }];
    
    // 10. 三只
    [self.rightBottomTable3 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
         
        if (block){
            block(chaosTouzhuTypeSanzhi,strongSelf.rightBottomTable3,strongSelf.goldCount);
        }
    }];
    
    // 11 .四只
    [self.rightBottomTable4 actionClickManagerWithGold:self.goldCount callback:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
         
        if (block){
            block(chaosTouzhuTypeSizhi,strongSelf.rightBottomTable4,strongSelf.goldCount);
        }
    }];
}

#pragma mark - 投注方法
-(void)actionClickWithType:(chaosTouzhuType)type block:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithTypeKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)touzhuViewManagerWithView:(PDChaosFightTouzhuView *)view money:(NSInteger)memberStake block:(void(^)(touzhuGameType touzhuGameType))block{
    if (self.transferGameType == touzhuGameTypeEnd){
        [PDHUD showHUDError:@"暂时投注，请等待开奖结束。"];
        return;
    }
    
    if ((self.myCountGold < memberStake) || memberStake == 0){
        if (block){
            block(touzhuGameTypeNoMoney);
        }
        return;
    }

    if (self.singleModel.memberMaxDayStake != 0){
        if ((self.singleModel.memberDayStake + memberStake) > self.singleModel.memberMaxDayStake){
            if (block){
                block(touzhuGameTypeShangxian);
            }
            return;
        }
    } else {                        // 上限 = 0  没有上限
        
        NSLog(@"上限 = 0");
    }
    
    NSInteger stake = [AccountModel sharedAccountModel].accountCountDayStake - [AccountModel sharedAccountModel].accountMemberDayStake ;
    
    if (memberStake > stake){
        if (block){
            block(touzhuGameTypeShangxian);
        }
        return;
    }
    // 1. 增加自己投注的钱
    [AccountModel sharedAccountModel].accountMemberDayStake += memberStake;
    // 2. 其他操作
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    
    CGRect convertFrame;
    if (view == self.topTable1 || view == self.topTable2 || view == self.topTable3){
        convertFrame = [self.topCardTableImgView convertRect:view.frame toView:keyWindow];
    } else if (view == self.leftBottomTable1 || view == self.leftBottomTable2 || view == self.leftBottomTable3 || view == self.leftBottomTable4){
        convertFrame = [self.leftCardTableImgView convertRect:view.frame toView:keyWindow];
    } else if (view == self.rightBottomTable1 || view == self.rightBottomTable2 || view == self.rightBottomTable3 || view == self.rightBottomTable4){
        convertFrame = [self.rightCardTableImgView convertRect:view.frame toView:keyWindow];
    }
    
    CGRect beginFrame = [self.moneybgView convertRect:self.coinImgView.frame toView:keyWindow];
    NSInteger coinCount = 0;
    if (memberStake == 100){                  // 1个金币
        coinCount = 1;
    } else if (memberStake == 1000){          // 2个金币
        coinCount = 2;
    } else if (memberStake == 10000){         // 3 个金币
        coinCount = 3;
    } else {
        coinCount = 3;
    }
    
    for (int i = 0 ; i < coinCount;i++){
        CGFloat origin_x_random = convertFrame.origin.x + 3 + (arc4random() % ((long)convertFrame.size.width - 20));
        CGFloat origin_y_random = convertFrame.origin.y + 3 + (arc4random() % ((long)convertFrame.size.height - 20));
        
        PDChaosCoinImgView *coinImgView = [[PDChaosCoinImgView alloc]initWithFrame:CGRectMake(beginFrame.origin.x + beginFrame.size.width / 2., beginFrame.origin.y + beginFrame.size.height / 2., 19, 20)];
        coinImgView.image = [UIImage imageNamed:@"icon_chaosFight_coin"];
        [self.view addSubview:coinImgView];
        [UIView animateWithDuration:1.f animations:^{
            coinImgView.frame = CGRectMake(origin_x_random, origin_y_random, coinImgView.size_width, coinImgView.size_height);
        } completion:^(BOOL finished) {
            coinImgView.frame = CGRectMake(origin_x_random, origin_y_random, coinImgView.size_width, coinImgView.size_height);
        }];
    }
    
    [self.audioTool playSoundWithSoundName:@"mic_gold.wav"];
    
    if (block){
        block(touzhuGameTypeNormal);
    }
}

-(void)touzhuSocketBaseBackManager:(PDChaosFightTouzhuSingleModel *)moneySingleModel{
    PDChaosFightTouzhuView *view = [self touzhuViewManager:moneySingleModel];
    
    // 增加上面的文字
    view.currentCountPrice = moneySingleModel.totalStake;
    view.currentMyPrice = moneySingleModel.memberStake;
}

-(PDChaosFightTouzhuView *)touzhuViewManager:(PDChaosFightTouzhuSingleModel *)singleModel{
    PDChaosFightTouzhuView *touzhuView;
    
    if (singleModel.target == chaosTouzhuTypeTuoersuo){
        touzhuView = self.topTable1;
    } else if (singleModel.target == chaosTouzhuTypePingshou){
        touzhuView = self.topTable2;
    } else if (singleModel.target == chaosTouzhuTypeertongjie){
        touzhuView = self.topTable3;
    } else if (singleModel.target == chaosTouzhuTypeShuangsha){
        touzhuView = self.leftBottomTable1;
    } else if (singleModel.target == chaosTouzhuTypeXiaolong){
        touzhuView = self.leftBottomTable2;
    } else if (singleModel.target == chaosTouzhuTypeDalong){
        touzhuView = self.leftBottomTable3;
    } else if (singleModel.target == chaosTouzhuTypeShuangkong){
        touzhuView = self.leftBottomTable4;
    } else if (singleModel.target == chaosTouzhuTypeYizhi){
        touzhuView = self.rightBottomTable1;
    } else if (singleModel.target == chaosTouzhuTypeLiangzhi){
        touzhuView = self.rightBottomTable2;
    } else if (singleModel.target == chaosTouzhuTypeSanzhi){
        touzhuView = self.rightBottomTable3;
    } else if (singleModel.target == chaosTouzhuTypeSizhi){
        touzhuView = self.rightBottomTable4;
    }
    return touzhuView;
}



-(void)touzhuWithOtherGamer:(PDChaosFightOtherTouzhuSingleModel *)singleModel{
    PDChaosFightTouzhuView *touzhuView;
    // 1. 获取目标的viewRect
    if (singleModel.stakeInfo.target == chaosTouzhuTypeTuoersuo){
        touzhuView = self.topTable1;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.pointAWin.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.pointAWin.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
    } else if (singleModel.stakeInfo.target == chaosTouzhuTypePingshou){
        touzhuView = self.topTable2;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.tie.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.tie.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
    } else if (singleModel.stakeInfo.target == chaosTouzhuTypeertongjie){
        touzhuView = self.topTable3;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.pointBWin.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.pointBWin.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
        
    } else if (singleModel.stakeInfo.target == chaosTouzhuTypeShuangsha){
        touzhuView = self.leftBottomTable1;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.anyoneDoubleKill.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.anyoneDoubleKill.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
    } else if (singleModel.stakeInfo.target == chaosTouzhuTypeXiaolong){
        touzhuView = self.leftBottomTable2;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.anyoneDragonet.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.anyoneDragonet.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
    } else if (singleModel.stakeInfo.target == chaosTouzhuTypeDalong){
        touzhuView = self.leftBottomTable3;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.anyoneDragon.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.anyoneDragon.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
    } else if (singleModel.stakeInfo.target == chaosTouzhuTypeShuangkong){
        touzhuView = self.leftBottomTable4;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.anyoneDoubleNull.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.anyoneDoubleNull.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
    } else if (singleModel.stakeInfo.target == chaosTouzhuTypeYizhi){
        touzhuView = self.rightBottomTable1;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.overallOne.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.overallOne.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
    } else if (singleModel.stakeInfo.target == chaosTouzhuTypeLiangzhi){
        touzhuView = self.rightBottomTable2;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.overallTwo.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.overallTwo.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
    } else if (singleModel.stakeInfo.target == chaosTouzhuTypeSanzhi){
        touzhuView = self.rightBottomTable3;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.overallThree.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.overallThree.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
    } else if (singleModel.stakeInfo.target == chaosTouzhuTypeSizhi){
        touzhuView = self.rightBottomTable4;
        if (singleModel.guessMainInfo){
            touzhuView.currentCountPrice = singleModel.guessMainInfo.overallFour.totalStake;
            touzhuView.currentMyPrice = singleModel.guessMainInfo.overallFour.memberStake;
        } else if (singleModel.guessItemInfo){
            touzhuView.currentCountPrice = singleModel.guessItemInfo.totalStake;
        }
    }
    
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    
    CGRect targetViewRect;
    if (touzhuView == self.topTable1 || touzhuView == self.topTable2 || touzhuView == self.topTable3){
        targetViewRect = [self.topCardTableImgView convertRect:touzhuView.frame toView:keyWindow];
    } else if (touzhuView == self.leftBottomTable1 || touzhuView == self.leftBottomTable2 || touzhuView == self.leftBottomTable3 || touzhuView == self.leftBottomTable4){
        targetViewRect = [self.leftCardTableImgView convertRect:touzhuView.frame toView:keyWindow];
    } else if (touzhuView == self.rightBottomTable1 || touzhuView == self.rightBottomTable2 || touzhuView == self.rightBottomTable3 || touzhuView == self.rightBottomTable4){
        targetViewRect = [self.rightCardTableImgView convertRect:touzhuView.frame toView:keyWindow];
    }
    
    // 2. 获取金币数量
    NSInteger coinCount = 0;
    if (singleModel.stakeInfo.gold >= 10000){
        coinCount = 3;
    } else if (singleModel.stakeInfo.gold >= 1000){
        coinCount = 2;
    } else {
        coinCount = 1;
    }
    
    // 3. 寻找 当前目标
    CGRect targetRect;
    if ([singleModel.member.memberId isEqualToString:[AccountModel sharedAccountModel].memberId] || [singleModel.stakeInfo.memberId isEqualToString:[AccountModel sharedAccountModel].memberId]){
        targetRect = [self.moneybgView convertRect:self.coinImgView.frame toView:keyWindow];
    } else {
        NSString *memberId = singleModel.stakeInfo.memberId;
        NSInteger index = -1;
        UITableView *targetTableView;
        PDChaosFightMembersSingleModel *targetModel ;
        BOOL hasLeft = NO;
        for (int i = 0 ; i < self.leftMutableArr.count;i++){
            PDChaosFightMembersSingleModel *tempSingleModel = [self.leftMutableArr objectAtIndex:i];
            if ([tempSingleModel.memberId isEqualToString:memberId]){
                index = i;
                targetTableView = self.leftTableView;
                targetModel = tempSingleModel;
                targetModel.gold = singleModel.member.gold;
                hasLeft = YES;
                break;
            }
        }
        if (!hasLeft){
            for (int i = 0 ; i < self.rightMutableArr.count;i++){
                PDChaosFightMembersSingleModel *tempSingleModel = [self.rightMutableArr objectAtIndex:i];
                if ([tempSingleModel.memberId isEqualToString:memberId]){
                    index = i;
                    targetTableView = self.rightTableView;
                    targetModel = tempSingleModel;
                    targetModel.gold = singleModel.member.gold;
                }
            }
        }
        
        PDChaosFightRoleTableViewCell *cellWithRowOne = (PDChaosFightRoleTableViewCell *)[targetTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]];
        cellWithRowOne.transferSingleModel = targetModel;
        CGRect convertFrame = [targetTableView convertRect:cellWithRowOne.frame toView:keyWindow];
        targetRect = CGRectMake(convertFrame.origin.x + (convertFrame.size.width - 20) / 2., convertFrame.origin.y + (convertFrame.size.height - 19) / 2., 20, 19);
    }
    
    for (int i = 0 ; i < coinCount;i++){
        CGFloat origin_x_random = targetViewRect.origin.x + 3 + (arc4random() % ((long)targetViewRect.size.width - 20));
        CGFloat origin_y_random = targetViewRect.origin.y + 3 + (arc4random() % ((long)targetViewRect.size.height - 20));
        CGRect beginFrame = targetRect;
        PDChaosCoinImgView *coinImgView = [[PDChaosCoinImgView alloc]initWithFrame:beginFrame];
        coinImgView.image = [UIImage imageNamed:@"icon_chaosFight_coin"];
        [self.view addSubview:coinImgView];
        
        [UIView animateWithDuration:1.f animations:^{
            coinImgView.frame = CGRectMake(origin_x_random, origin_y_random, 20, 19);
        } completion:^(BOOL finished) {
            coinImgView.frame = CGRectMake(origin_x_random, origin_y_random, 20, 19);
        }];
    }
    
    [self.audioTool playSoundWithSoundName:@"mic_gold.wav"];
}

-(void)touzhuWillGoInGame:(PDChaosFightBeginGameRootModel *)singleModel{
    PDChaosFightOtherTouzhuSingleModel * infoModel = [[PDChaosFightOtherTouzhuSingleModel alloc]init];
    for (int i = 0 ; i < singleModel.stakeInfos.count;i++){
        infoModel.stakeInfo = [singleModel.stakeInfos objectAtIndex:i];
        infoModel.guessMainInfo = singleModel.guessItemInfo;
        [self touzhuWithOtherGamer:infoModel];
    }
}


#pragma mark - 清除比赛脏数据
-(void)cleanGameData{
    // 1. 清除金钱图片
    for (UIView *view in self.view.subviews){
        if ([view isKindOfClass:[PDChaosCoinImgView class]]){
            [UIView animateWithDuration:.5f animations:^{
                view.orgin_y -= 20;
                view.alpha = 0;
            } completion:^(BOOL finished) {
                [view removeFromSuperview];
            }];
        }
    }
    // 2. 清除上面的数字
    for (PDChaosFightTouzhuView *view in self.topPriceMutableArr){
        [UIView animateWithDuration:.5f animations:^{
            view.countPriceLabel.orgin_y -= 20;
            view.myTouzhuLabel.orgin_y -= 20;
            view.countPriceLabel.alpha = 0;
            view.myTouzhuLabel.alpha = 0;
        } completion:^(BOOL finished) {
            view.currentCountPrice = 0;
            view.currentMyPrice = 0;
            [view auSetupFrame];
        }];
    }
}

-(void)touzhuManagerWithType:(touzhuCountType)type{
    self.currentTouzhuType = type;
    if (type == touzhuCountType100){
        [self.oneButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100_hlt"] forState:UIControlStateNormal];
        [self.twoButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_1000_nor"] forState:UIControlStateNormal];
        [self.threeButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_10000_nor"] forState:UIControlStateNormal];
        [self.fourButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_50000_nor"] forState:UIControlStateNormal];
        [self.fivButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100000_nor"] forState:UIControlStateNormal];
        [self.sixButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_all_nor"] forState:UIControlStateNormal];
        self.goldCount = 100;
    } else if (type == touzhuCountType1000){
        [self.oneButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100_nor"] forState:UIControlStateNormal];
        [self.twoButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_1000_hlt"] forState:UIControlStateNormal];
        [self.threeButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_10000_nor"] forState:UIControlStateNormal];
        [self.fourButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_50000_nor"] forState:UIControlStateNormal];
        [self.fivButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100000_nor"] forState:UIControlStateNormal];
        [self.sixButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_all_nor"] forState:UIControlStateNormal];
        self.goldCount = 1000;
    } else if (type == touzhuCountType10000){
        [self.oneButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100_nor"] forState:UIControlStateNormal];
        [self.twoButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_1000_nor"] forState:UIControlStateNormal];
        [self.threeButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_10000_hlt"] forState:UIControlStateNormal];
        [self.fourButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_50000_nor"] forState:UIControlStateNormal];
        [self.fivButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100000_nor"] forState:UIControlStateNormal];
        [self.sixButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_all_nor"] forState:UIControlStateNormal];

        self.goldCount = 10000;
    } else if (type == touzhuCountType50000){
        [self.oneButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100_nor"] forState:UIControlStateNormal];
        [self.twoButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_1000_nor"] forState:UIControlStateNormal];
        [self.threeButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_10000_nor"] forState:UIControlStateNormal];
        [self.fourButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_50000_hlt"] forState:UIControlStateNormal];
        [self.fivButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100000_nor"] forState:UIControlStateNormal];
        [self.sixButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_all_nor"] forState:UIControlStateNormal];
        self.goldCount = 50000;
    } else if (type == touzhuCountType100000){
        [self.oneButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100_nor"] forState:UIControlStateNormal];
        [self.twoButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_1000_nor"] forState:UIControlStateNormal];
        [self.threeButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_10000_nor"] forState:UIControlStateNormal];
        [self.fourButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_50000_nor"] forState:UIControlStateNormal];
        [self.fivButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100000_hlt"] forState:UIControlStateNormal];
        [self.sixButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_all_nor"] forState:UIControlStateNormal];
        self.goldCount = 100000;
    } else if (type == touzhuCountTypeALL){
        [self.oneButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100_nor"] forState:UIControlStateNormal];
        [self.twoButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_1000_nor"] forState:UIControlStateNormal];
        [self.threeButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_10000_nor"] forState:UIControlStateNormal];
        [self.fourButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_50000_nor"] forState:UIControlStateNormal];
        [self.fivButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_100000_nor"] forState:UIControlStateNormal];
        [self.sixButton setImage:[UIImage imageNamed:@"icon_chaosFight_bottom_all_hlt"] forState:UIControlStateNormal];
        
        self.goldCount = self.myCountGold /100 * 100;
    }
}


#pragma mark - 更新信息
-(void)updateHouseInfo:(PDChaosFightBeginGameRootModel *)singleModel{
    // 0.更新当前投注的钱
    
    // 1.修改房间信息
    self.onlineLabel.text = [NSString stringWithFormat:@"%li人在线",singleModel.guess.totalMemberCount];
    self.onlineHouseLabel.text = [NSString stringWithFormat:@"当前房间%@",singleModel.guess.roomNum];
    
    // 2. 修改左右用户
    NSMutableArray *tempArr = [NSMutableArray arrayWithArray:singleModel.members];
    for (int i = 0 ; i < tempArr.count;i++){
        PDChaosFightMembersSingleModel *memberSingleModel = [tempArr objectAtIndex:i];
        if ([memberSingleModel.memberId isEqualToString:[AccountModel sharedAccountModel].memberId]){
            // 2.1 保存我的钱
            self.myCountGold = memberSingleModel.gold;
            
            self.mineMemberSingleModel = memberSingleModel;
            self.nickNameLabel.text = self.mineMemberSingleModel.nickName;

            self.coinLabel.text = [NSString stringWithFormat:@"%li",self.myCountGold];
            [tempArr removeObject:memberSingleModel];
            break;
        }
    }
    
    // 3. 更新当前投注的钱
    if (self.currentTouzhuType == touzhuCountTypeALL){
         self.goldCount = self.myCountGold /100 * 100;
    }
    
    [self.leftMutableArr removeAllObjects];
    [self.rightMutableArr removeAllObjects];
    if (tempArr.count >= 4){
        for (int i = 0 ; i < 4;i++){
            PDChaosFightMembersSingleModel *memberSingleModel = [tempArr objectAtIndex:i];
            [self.leftMutableArr addObject:memberSingleModel];
            [tempArr removeObject:memberSingleModel];
        }
        
        for (int i = 0; i < tempArr.count;i++){
            PDChaosFightMembersSingleModel *memberSingleModel = [tempArr objectAtIndex:i];
            [self.rightMutableArr addObject:memberSingleModel];
        }
    }
    
    [self.leftTableView reloadData];
    [self.rightTableView reloadData];
}

#pragma mark 更新房间人数
-(void)updateHouseTotalCount:(NSInteger)count{
    self.onlineLabel.text = [NSString stringWithFormat:@"%li人在线",count];
}


-(void)dismissRootManager{
    // 1. 移除投注界面的window层
    for (UIView *view in self.leftCardTableImgView.subviews) {
        if ([view isKindOfClass:[PDChaosFightTouzhuView class]]){
            PDChaosFightTouzhuView *mainView = (PDChaosFightTouzhuView *)view;
            [mainView dismissKeyWindowManager];
        }
    }
    for (UIView *view in self.rightCardTableImgView.subviews){
        if ([view isKindOfClass:[PDChaosFightTouzhuView class]]){
            PDChaosFightTouzhuView *mainView = (PDChaosFightTouzhuView *)view;
            [mainView dismissKeyWindowManager];
        }
    }
    for (UIView *view in self.topCardTableImgView.subviews){
        if ([view isKindOfClass:[PDChaosFightTouzhuView class]]){
            PDChaosFightTouzhuView *mainView = (PDChaosFightTouzhuView *)view;
            [mainView dismissKeyWindowManager];
        }
    }
    
    //  2. 移除ReadyGo动画
    [self.animationBgView dismissManager];
    
    // 3. 移除界面的卡片
    [self.topTable1 dismissWinManagerBlock:NULL];
    [self.topTable2 dismissWinManagerBlock:NULL];
    [self.topTable3 dismissWinManagerBlock:NULL];
    [self.rightBottomTable1 dismissWinManagerBlock:NULL];
    [self.rightBottomTable2 dismissWinManagerBlock:NULL];
    [self.rightBottomTable3 dismissWinManagerBlock:NULL];
    [self.rightBottomTable4 dismissWinManagerBlock:NULL];
    [self.leftBottomTable1 dismissWinManagerBlock:NULL];
    [self.leftBottomTable2 dismissWinManagerBlock:NULL];
    [self.leftBottomTable3 dismissWinManagerBlock:NULL];
    [self.leftBottomTable4 dismissWinManagerBlock:NULL];
    
    // 4. 移除上面的卡片
    [self guaishouCardDismiss];
    // 5. 移除所有的金币
    [self cleanGameData];
    // 6. 移除
    
}

#pragma mark 玩家出去方法
-(void)goOutMemberIndex:(NSInteger)index{
    BOOL isLeft = YES;
    NSInteger newIndex = -1;
    if (index / 4 == 0){                // 左侧
        newIndex = index % 4;
        isLeft = YES;
    } else {                            // 右侧
        newIndex = index - 4;
        isLeft = NO;
    }
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:newIndex];
    
    PDChaosFightMembersSingleModel *singleMemberModel = [[PDChaosFightMembersSingleModel alloc]init];
    if (isLeft){
        [self.leftMutableArr replaceObjectAtIndex:newIndex withObject:singleMemberModel];
        
        PDChaosFightRoleTableViewCell *cellWithRowOne = [self.leftTableView cellForRowAtIndexPath:indexPath];
        [cellWithRowOne memberGooutHouse:0];
    } else {
        [self.rightMutableArr replaceObjectAtIndex:newIndex withObject:singleMemberModel];
        PDChaosFightRoleTableViewCell *cellWithRowOne = [self.rightTableView cellForRowAtIndexPath:indexPath];
        [cellWithRowOne memberGooutHouse:0];
    }
}

#pragma mark 玩家进入方法
-(void)goInMemberSingleModel:(PDChaosFightGooutSingleModel *)model{
    BOOL isLeft = YES;
    NSInteger newIndex = -1;
    
    NSInteger otherGamerIndex = model.index;
    NSInteger myIndex = self.mineMemberSingleModel.index;
    
    if (otherGamerIndex > myIndex){
        if ((otherGamerIndex - 1) / 4 == 0){                // 左侧
            newIndex = (otherGamerIndex - 1) % 4;
            isLeft = YES;
        } else {                            // 右侧
            newIndex = (otherGamerIndex - 1) - 4;
            isLeft = NO;
        }
    } else {
        if (otherGamerIndex / 4 == 0){                // 左侧
            newIndex = otherGamerIndex % 4;
            isLeft = YES;
        } else {                            // 右侧
            newIndex = otherGamerIndex - 4;
            isLeft = NO;
        }
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:newIndex];
    PDChaosFightMembersSingleModel *memberSingleModel = [[PDChaosFightMembersSingleModel alloc]init];
    memberSingleModel.gold = model.gold;
    memberSingleModel.nickName = model.nickName;
    memberSingleModel.memberId = model.memberId;
    memberSingleModel.avatar = model.avatar;

    NSMutableArray *tempMemberMutableArr = [NSMutableArray arrayWithArray:self.singleModel.members];
    
    NSInteger modelIndex = model.index;
    NSInteger mineIndex = self.mineMemberSingleModel.index;
    
    if (modelIndex > mineIndex){
        memberSingleModel.index = modelIndex;
        [tempMemberMutableArr replaceObjectAtIndex:modelIndex withObject:memberSingleModel];
    } else {
        memberSingleModel.index = modelIndex;
        [tempMemberMutableArr replaceObjectAtIndex:modelIndex withObject:memberSingleModel];
    }
    self.singleModel.members = [tempMemberMutableArr copy];
    
    if (isLeft){
        [self.leftMutableArr replaceObjectAtIndex:newIndex withObject:memberSingleModel];
        PDChaosFightRoleTableViewCell *cellWithRowOne = [self.leftTableView cellForRowAtIndexPath:indexPath];
        cellWithRowOne.transferSingleModel = memberSingleModel;
        if (memberSingleModel.memberId.length){
            [cellWithRowOne memberGoinHouse:0 model:memberSingleModel];
        }
    } else {
        [self.rightMutableArr replaceObjectAtIndex:newIndex withObject:memberSingleModel];
        PDChaosFightRoleTableViewCell *cellWithRowOne = [self.rightTableView cellForRowAtIndexPath:indexPath];
        cellWithRowOne.transferSingleModel = memberSingleModel;
        if (memberSingleModel.memberId.length){
            [cellWithRowOne memberGoinHouse:0 model:memberSingleModel];
        }
    }
}

//#pragma mark - 开奖方法
//-(void)kaijiangAnimationManager:(PDChaosFightKaijiangRootModel *)kaijiangSingleModel{
//    // 1. 打怪动画
//    [self animationKillGuaishouRootManagerWithModel:kaijiangSingleModel];
//}

#pragma mark  1.打怪动画
-(void)challengeGuaishouAnimationWithBlock:(void(^)())block{
    if (!self.animationBgView){
        self.animationBgView = [[PDChaosBackgroundView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
        self.animationBgView.userInteractionEnabled = NO;
        [self.view addSubview:self.animationBgView];
    }
    
    [self.animationBgView startAnimationWithFinish:^{
        if (block){
            block();
        }
    }];
}

-(void)kaijiangAnimationManager:(PDChaosFightKaijiangRootModel *)kaijiangSingleModel block:(void(^)())block{
    self.leftCard1.alpha = 0;
    self.leftCard2.alpha = 0;
    self.rightCard1.alpha = 0;
    self.rightCard2.alpha = 0;
    
    NSString *info1 = [kaijiangSingleModel.pointResultItems objectAtIndex:1];
    NSString *info2 = [kaijiangSingleModel.pointResultItems objectAtIndex:2];
    NSString *info3 = [kaijiangSingleModel.pointResultItems objectAtIndex:0];
    NSString *info4 = [kaijiangSingleModel.pointResultItems objectAtIndex:3];

    // 1. 杀第一只怪兽
    __weak typeof(self)weakSelf = self;
    [self animationWithGuaishouManagerWithType:killGuaishouType1 strIndex:info1 block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf animationWithGuaishouManagerWithType:killGuaishouType2 strIndex:info2 block:^{
            [strongSelf animationWithGuaishouManagerWithType:killGuaishouType3 strIndex:info3 block:^{
                [strongSelf animationWithGuaishouManagerWithType:killGuaishouType4 strIndex:info4 block:^{
                    [strongSelf animationInsertQizi:kaijiangSingleModel block:^{
                        if (block){
                            block();
                        }
                    }];
                }];
            }];
        }];
    }];
}

#pragma mark - 插旗子
-(void)animationInsertQizi:(PDChaosFightKaijiangRootModel *)kaijiangSingleModel block:(void(^)())block{
    // 2. 获取
    if (self.hasGoout){
        for (UIView *view in self.leftCardTableImgView.subviews) {
            if ([view isKindOfClass:[PDChaosFightTouzhuView class]]){
                PDChaosFightTouzhuView *mainView = (PDChaosFightTouzhuView *)view;
                [mainView removeFromWindow];
            }
        }
        for (UIView *view in self.rightCardTableImgView.subviews){
            if ([view isKindOfClass:[PDChaosFightTouzhuView class]]){
                PDChaosFightTouzhuView *mainView = (PDChaosFightTouzhuView *)view;
                [mainView removeFromWindow];
            }
        }
        for (UIView *view in self.topCardTableImgView.subviews){
            if ([view isKindOfClass:[PDChaosFightTouzhuView class]]){
                PDChaosFightTouzhuView *mainView = (PDChaosFightTouzhuView *)view;
                [mainView removeFromWindow];
            }
        }
        return;
    }
    
    for (PDChaosFightWinningGuessSingleModel *singleModel in kaijiangSingleModel.winningGuessItems){
        PDChaosFightTouzhuView *touzhuView;
        if (singleModel.order == chaosTouzhuTypeTuoersuo){
            touzhuView = self.topTable1;
        } else if (singleModel.order == chaosTouzhuTypePingshou){
            touzhuView = self.topTable2;
        } else if (singleModel.order == chaosTouzhuTypeertongjie){
            touzhuView = self.topTable3;
        } else if (singleModel.order == chaosTouzhuTypeShuangsha){
            touzhuView = self.leftBottomTable1;
        } else if (singleModel.order == chaosTouzhuTypeXiaolong){
            touzhuView = self.leftBottomTable2;
        } else if (singleModel.order == chaosTouzhuTypeDalong){
            touzhuView = self.leftBottomTable3;
        } else if (singleModel.order == chaosTouzhuTypeShuangkong){
            touzhuView = self.leftBottomTable4;
        } else if (singleModel.order == chaosTouzhuTypeYizhi){
            touzhuView = self.rightBottomTable1;
        } else if (singleModel.order == chaosTouzhuTypeLiangzhi){
            touzhuView = self.rightBottomTable2;
        } else if (singleModel.order == chaosTouzhuTypeSanzhi){
            touzhuView = self.rightBottomTable3;
        } else if (singleModel.order == chaosTouzhuTypeSizhi){
            touzhuView = self.rightBottomTable4;
        }
        
        [touzhuView showWinManagerWithBlock:^{
            
        }];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (block){
            block();
        }
    });
}

-(void)changeTimerImgWithInteger:(NSInteger)num{
    self.timeImgView.image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_chaosFight_other_time_%li",num]];
}

#pragma mark - 统计按钮点击
-(void)tongjiBtnClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &tongjiBtnClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 增加金币动画
-(void)winWithCoinRootManager:(PDChaosFightKaijiangRootModel *)winModel block:(void(^)())block{
    for (int i = 0 ; i < winModel.guessItemRs.count;i++){
        PDChaosFightGuessItemsRoleSingleModel *roleSingleModel = [winModel.guessItemRs objectAtIndex:i];
        PDChaosFightTouzhuView *tempView;
        if ([roleSingleModel.guessItemType isEqualToString:@"pointAWin"]){                      // 托儿所胜利
            tempView = self.topTable1;
        } else if ([roleSingleModel.guessItemType isEqualToString:@"tie"]){                     // 平手
            tempView = self.topTable2;
        } else if ([roleSingleModel.guessItemType isEqualToString:@"pointBWin"]){               // 儿童节胜利
            tempView = self.topTable3;
        } else if ([roleSingleModel.guessItemType isEqualToString:@"anyoneDoubleKill"]){        // 双杀
            tempView = self.leftBottomTable1;
        } else if ([roleSingleModel.guessItemType isEqualToString:@"anyoneDragonet"]){          // 小龙
            tempView = self.leftBottomTable2;
        } else if ([roleSingleModel.guessItemType isEqualToString:@"anyoneDragon"]){            // 大龙
            tempView = self.leftBottomTable3;
        } else if ([roleSingleModel.guessItemType isEqualToString:@"anyoneDoubleNull"]){        // 双空
            tempView = self.leftBottomTable4;
        } else if ([roleSingleModel.guessItemType isEqualToString:@"overallOne"]){              // 一只
            tempView = self.rightBottomTable1;
        } else if ([roleSingleModel.guessItemType isEqualToString:@"overallTwo"]){              // 2只
            tempView = self.rightBottomTable2;
        } else if ([roleSingleModel.guessItemType isEqualToString:@"overallThree"]){            // 3只
            tempView = self.rightBottomTable3;
        } else if ([roleSingleModel.guessItemType isEqualToString:@"overallFour"]){             // 4只
            tempView = self.rightBottomTable4;
        }
        [self winCoinManagerFromView:tempView withModel:roleSingleModel block:^{

        }];
    }
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1. * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (block){
            block();
        }
    });
}

-(void)winCoinManagerFromView:(PDChaosFightTouzhuView *)view withModel:(PDChaosFightGuessItemsRoleSingleModel *)winModel block:(void(^)())block{
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    
    // 1. 获取开始的view 的位置
    CGRect fromViewRect;
    if (view == self.topTable1 || view == self.topTable2 || view == self.topTable3){
        fromViewRect = [self.topCardTableImgView convertRect:view.frame toView:keyWindow];
    } else if (view == self.leftBottomTable1 || view == self.leftBottomTable2 || view == self.leftBottomTable3 || view == self.leftBottomTable4){
        fromViewRect = [self.leftCardTableImgView convertRect:view.frame toView:keyWindow];
    } else if (view == self.rightBottomTable1 || view == self.rightBottomTable2 || view == self.rightBottomTable3 || view == self.rightBottomTable4){
        fromViewRect = [self.rightCardTableImgView convertRect:view.frame toView:keyWindow];
    }
    
    // 2. 获取金币数量
    NSInteger coinCount = 0;
    if (winModel.winGold >= 100000){                  // 1个金币
        coinCount = 4;
    } else if (winModel.winGold >= 10000){          // 2个金币
        coinCount = 3;
    } else if (winModel.winGold >= 1000){         // 3 个金币
        coinCount = 2;
    } else if (winModel.winGold >= 100){
        coinCount = 1;
    }
    
    // 3. 寻找 当前目标
    CGRect targetRect;
    PDChaosFightRoleTableViewCell *cellWithRowOne;
    if ([winModel.memberId isEqualToString:[AccountModel sharedAccountModel].memberId]){
        targetRect = [self.moneybgView convertRect:self.coinImgView.frame toView:keyWindow];
    } else {
        NSString *memberId = winModel.memberId;
        NSInteger index = -1;
        UITableView *targetTableView;
        BOOL hasLeft = NO;
        for (int i = 0 ; i < self.leftMutableArr.count;i++){
            PDChaosFightMembersSingleModel *singleModel = [self.leftMutableArr objectAtIndex:i];
            if ([singleModel.memberId isEqualToString:memberId]){
                index = i;
                targetTableView = self.leftTableView;
                hasLeft = YES;
                break;
            }
        }
        if (!hasLeft){
            for (int i = 0 ; i < self.rightMutableArr.count;i++){
                PDChaosFightMembersSingleModel *singleModel = [self.rightMutableArr objectAtIndex:i];
                if ([singleModel.memberId isEqualToString:memberId]){
                    index = i;
                    targetTableView = self.rightTableView;
                }
            }
        }

        
        cellWithRowOne = (PDChaosFightRoleTableViewCell *)[targetTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:index]];
        CGRect convertFrame = [targetTableView convertRect:cellWithRowOne.frame toView:keyWindow];
        targetRect = convertFrame;
    }
    
    for (int i = 0 ; i < coinCount;i++){
        CGFloat origin_x_random = fromViewRect.origin.x + 3 + (arc4random() % ((long)fromViewRect.size.width - 20));
        CGFloat origin_y_random = fromViewRect.origin.y + 3 + (arc4random() % ((long)fromViewRect.size.height - 20));
        CGRect beginFrame = CGRectMake(origin_x_random, origin_y_random, 20, 19);
        PDChaosCoinImgView *coinImgView = [[PDChaosCoinImgView alloc]initWithFrame:beginFrame];
        coinImgView.image = [UIImage imageNamed:@"icon_chaosFight_coin"];
        [self.view addSubview:coinImgView];
     
        [UIView animateWithDuration:1.f animations:^{
            coinImgView.frame = CGRectMake(targetRect.origin.x + targetRect.size.width / 2., targetRect.origin.y + targetRect.size.height / 2., coinImgView.size_width, coinImgView.size_height);
        } completion:^(BOOL finished) {
            coinImgView.frame = CGRectMake(targetRect.origin.x + targetRect.size.width / 2., targetRect.origin.y + targetRect.size.height / 2., coinImgView.size_width, coinImgView.size_height);
            // 1. 显示加钱动画
            [cellWithRowOne winWithAddGoldAnimation:winModel.winGold];
            // 2. 账户信息
            if ([winModel.memberId isEqualToString:[AccountModel sharedAccountModel].memberId]){
                self.myCountGold += winModel.winGold;
                [self mineWinMangerWithCoin:winModel.winGold];
//                [self changeMyCoinWithMoney:[AccountModel sharedAccountModel].gold];
            } else {
            
            }
        }];
    }
    
    [self.audioTool playSoundWithSoundName:@"mic_line.wav"];
}

#pragma mark - 修改我的金币
-(void)changeMyCoinWithMoney:(NSInteger)money{
    self.coinLabel.text = [NSString stringWithFormat:@"%li",(long)money];
}

#pragma mark - Action
-(void)actionClickToChongzhiManagerBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    if (self.chongzhiButton){
        [self.chongzhiButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            if (block){
                block();
            }
        }];
    }
}

-(void)createMineWinLabel{
    if (!self.mineWinGoldLabel){
        self.mineWinGoldLabel = [[PDChaosFightWinLabel alloc]init];
        self.mineWinGoldLabel.textColor = [UIColor whiteColor];
        self.mineWinGoldLabel.font = [[UIFont systemFontOfSize:15.]boldFont];
        self.mineWinGoldLabel.layer.shadowColor = [UIColor hexChangeFloat:@"7b3c0a"].CGColor;
        self.mineWinGoldLabel.layer.shadowOffset = CGSizeMake(3,2.);
        self.mineWinGoldLabel.layer.shadowOpacity = 0.6;
        self.mineWinGoldLabel.layer.shadowRadius = 1.0;
        [self.bottomView addSubview:self.mineWinGoldLabel];
    }
}

-(void)mineWinMangerWithCoin:(NSInteger)coin{
    [self createMineWinLabel];

    self.mineWinGoldLabel.frame = CGRectMake(self.moneybgView.orgin_x + self.moneybgView.size_width / 2., self.moneybgView.orgin_y, self.coinLabel.size_width, [NSString contentofHeightWithFont:self.coinLabel.font]);
    self.mineWinGoldLabel.textAlignment = NSTextAlignmentCenter;
    self.mineWinGoldLabel.text = [NSString stringWithFormat:@"+%@",[Tool transferWithInfoNumber:coin]];
    self.mineWinGoldLabel.alpha = 1;
    [UIView animateWithDuration:.8f animations:^{
        self.mineWinGoldLabel.orgin_y = 10;
    } completion:^(BOOL finished) {
        self.mineWinGoldLabel.alpha = 0;
    }];
}


#pragma mark - 修改赔率
-(void)changeGuessItemInfoWithModel:(PDChaosFightBeginGameRootModel *)jungleGuessInfo{
    // 1. 左上1
    [self.topTable1 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.pointAWin.odds]];
    // 2. 中间
    [self.topTable2 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.tie.odds]];
    // 3. 右边
    [self.topTable3 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.pointBWin.odds]];
    // 左下1
    [self.leftBottomTable1 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.anyoneDoubleKill.odds]];
    // 左下4
    [self.leftBottomTable4 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.anyoneDoubleNull.odds]];
    // 右下1
    [self.rightBottomTable1 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.overallOne.odds]];
    // 右下2
    [self.rightBottomTable2 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.overallTwo.odds]];
    // 右下3
    [self.rightBottomTable3 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.overallThree.odds]];
    // 右下3
    [self.rightBottomTable4 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.overallFour.odds]];
    
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        [self.leftBottomTable2 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.anyoneDragonet.odds]];
        [self.leftBottomTable3 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.anyoneDragon.odds]];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        [self.leftBottomTable2 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.anyoneBlackDragon.odds]];
        [self.leftBottomTable3 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.anyoneMeatMountain.odds]];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        [self.leftBottomTable2 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.anyoneTyrant.odds]];
        [self.leftBottomTable3 changePeilv:[NSString stringWithFormat:@"%@倍",jungleGuessInfo.guessItemInfo.anyoneMaster.odds]];
    }
}
@end
