//
//  PDChaosFightPagePaySingleCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterTopUpItem.h"

@interface PDChaosFightPagePaySingleCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,assign)CGFloat transferCellWidth;
@property (nonatomic,strong)PDCenterTopUpItem *transferSingleModel;

-(void)actionClickMangerWithBlock:(void(^)(PDCenterTopUpItem *singleModel))block;

+(CGFloat)calculationCellHeightWithModel:(PDCenterTopUpItem *)singleModel;

@end
