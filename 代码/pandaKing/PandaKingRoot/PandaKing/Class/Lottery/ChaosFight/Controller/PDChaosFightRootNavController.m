//
//  PDChaosFightRootNavController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightRootNavController.h"

@interface PDChaosFightRootNavController()<UINavigationControllerDelegate,UINavigationBarDelegate>

@end

@implementation PDChaosFightRootNavController

-(instancetype)initWithRootViewController:(UIViewController *)rootViewController{
    self = [super initWithRootViewController:rootViewController];
    if (self){
        [self.navigationBar setBarStyle:UIBarStyleBlack];
        self.navigationBar.tintColor = [UIColor blackColor];
    }
    return self;
}
//
//-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
//    return [self.topViewController shouldAutorotateToInterfaceOrientation:toInterfaceOrientation];
//}
//
//// 6.0 之后系统调用方法
//-(BOOL)shouldAutorotate{
//    //    系统会调用跟视图的旋转控制方法，所以我们将跟视图将控制条件交给顶层视图（顶层视图即我们需要控制的视图）
//    //    系统调用该方法
//    return NO;
//}
//
//-(UIInterfaceOrientationMask)supportedInterfaceOrientations {
//    return UIInterfaceOrientationMaskLandscapeLeft;
//}
//
//-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation{
//    return UIInterfaceOrientationLandscapeLeft;
//}

@end
