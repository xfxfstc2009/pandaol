//
//  PDChaosFightPopUserAvatarView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightPopUserAvatarView.h"

@interface PDChaosFightPopUserAvatarView()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UIControl *dismissControl;
@property (nonatomic,strong)PDChaosFightMembersSingleModel *singleModel;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *nickNameLabel;

@end

@implementation PDChaosFightPopUserAvatarView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self pageSetting];
        [self createDismissBackgroundView];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.frame = self.bounds;
    self.bgView.userInteractionEnabled = YES;
    [self addSubview:self.bgView];
    
    self.avatarImgView = [[PDImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    self.avatarImgView.frame = self.bgView.bounds;
    [self.bgView addSubview:self.avatarImgView];
    
    self.nickNameLabel = [[UILabel alloc]init];
    self.nickNameLabel.backgroundColor = [UIColor clearColor];
    self.nickNameLabel.font = [UIFont systemFontOfSize:14.];
    self.nickNameLabel.textAlignment = NSTextAlignmentRight;
    self.nickNameLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    self.nickNameLabel.layer.shadowColor = [UIColor hexChangeFloat:@"7b3c0a"].CGColor;
    self.nickNameLabel.layer.shadowOffset = CGSizeMake(3,2.);
    self.nickNameLabel.backgroundColor = [UIColor clearColor];
    self.nickNameLabel.text = @"";
    self.nickNameLabel.layer.shadowOpacity = 0.6;
    self.nickNameLabel.layer.shadowRadius = 1.0;
    self.nickNameLabel.frame = CGRectMake(11, self.bgView.size_height - [NSString contentofHeightWithFont:self.nickNameLabel.font] - 5, self.bgView.size_width - 2 * 11, [NSString contentofHeightWithFont:self.nickNameLabel.font]);
    [self.bgView addSubview:self.nickNameLabel];
}

#pragma mark - popView
- (void)fadeIn {
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)fadeOut {
    [UIView animateWithDuration:.4 animations:^{
        self.transform = CGAffineTransformMakeScale(1, 1);
        self.alpha = 0.0;
        self.dismissControl.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.dismissControl removeFromSuperview];
            [self removeFromSuperview];
        }
    }];
}


-(void)viewShowWithAvatar:(PDChaosFightMembersSingleModel *)singleModel{
    self.singleModel = singleModel;
    UIWindow *keywindow = [self lastWindow];
    [keywindow addSubview:self.dismissControl];
    [keywindow addSubview:self];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,  keywindow.bounds.size.height/2.0f);
    
    // 修改用户头像
    [self.avatarImgView uploadImageWithURL:singleModel.avatar placeholder:nil callback:NULL];
    
    // nick
    self.nickNameLabel.text = singleModel.nickName;
    [self fadeIn];
}

- (UIWindow *)lastWindow {
    NSArray *windows = [UIApplication sharedApplication].windows;
    for(UIWindow *window in [windows reverseObjectEnumerator]) {
        
        if ([window isKindOfClass:[UIWindow class]] &&
            CGRectEqualToRect(window.bounds, [UIScreen mainScreen].bounds))
            
            return window;
    }
    
    return [UIApplication sharedApplication].keyWindow;
}



-(void)viewShowTopView:(UIView *)topView{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self.dismissControl];
    [[UIApplication sharedApplication].keyWindow insertSubview:self belowSubview:topView];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,  keywindow.bounds.size.height/2.0f);
    
    [self fadeIn];
}

- (void)viewDismiss {
    [self fadeOut];
}

-(void)pageSetting{
    self.layer.cornerRadius = 10.0f;
    self.clipsToBounds = true;
    self.userInteractionEnabled = YES;
}

-(void)createDismissBackgroundView{
    self.dismissControl = [[UIControl alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.dismissControl.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.5];
    [self.dismissControl addTarget:self action:@selector(viewDismiss) forControlEvents:UIControlEventTouchUpInside];
}


@end
