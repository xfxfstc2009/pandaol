//
//  PDChaosFightPagePaySingleCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightPagePaySingleCell.h"


static char actionClickMangerWithBlockKey;
@interface PDChaosFightPagePaySingleCell()
@property (nonatomic,strong)PDImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)UIButton *payButton;
@property (nonatomic,strong)UIView *lineView;

@end

@implementation PDChaosFightPagePaySingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    
    PDImageView *bgImgView = [[PDImageView alloc]init];
    bgImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_chongzhi_cell"];
    self.backgroundView = bgImgView;
    
    self.iconImgView = [[PDImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
    
    self.titleLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.titleLabel.textColor = [UIColor hexChangeFloat:@"7d492a"];
    self.titleLabel.font = [UIFont systemFontOfSize:15.];
    [self addSubview:self.titleLabel];
    
    self.descLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.descLabel.font = [UIFont systemFontOfSize:12.];
    self.descLabel.textColor = [UIColor hexChangeFloat:@"7d492a"];
    [self addSubview:self.descLabel];
    
    self.payButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.payButton.backgroundColor = [UIColor clearColor];
    self.payButton.titleLabel.font = [UIFont systemFontOfSize:12.];
    [self addSubview:self.payButton];
    [self.payButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_chongzhi_btn"] forState:UIControlStateNormal];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferCellWidth:(CGFloat)transferCellWidth{
    _transferCellWidth = transferCellWidth;
}

-(void)setTransferSingleModel:(PDCenterTopUpItem *)transferSingleModel{
    // 1. icon
    self.iconImgView.image = [UIImage imageNamed:@"icon_chaosFight_chongzhi_goldicon"];
    self.iconImgView.frame = CGRectMake(14, (self.transferCellHeight - 29) / 2., 23, 29);
    
    // title
    self.titleLabel.text = [NSString stringWithFormat:@"%li金币",transferSingleModel.gold];
    
    if (transferSingleModel.goldAwardForPay != 0){
        self.descLabel.text = [NSString stringWithFormat:@"送%li",transferSingleModel.goldAwardForPay];
        self.descLabel.hidden = NO;
    } else {
        self.descLabel.hidden = YES;
    }
    if (self.descLabel.hidden == NO){
        CGFloat margin = (self.transferCellHeight - [NSString contentofHeightWithFont:self.titleLabel.font] - [NSString contentofHeightWithFont:self.descLabel.font]) / 3.;
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + 11, margin,100 , [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.descLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame), self.titleLabel.size_width, [NSString contentofHeightWithFont:self.descLabel.font]);
    } else {
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + 11, 0, 100, self.transferCellHeight);
    }
    
    // btn
    self.payButton.frame = CGRectMake(self.transferCellWidth - 62 - 11, (self.transferCellHeight - 29) / 2., 62, 29);
    [self.payButton setTitle:[NSString stringWithFormat:@"%@元",transferSingleModel.cash] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.payButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(PDCenterTopUpItem *singleModel) = objc_getAssociatedObject(strongSelf, &actionClickMangerWithBlockKey);
        if (block){
            block(transferSingleModel);
        }
    }];
    
}

-(void)actionClickMangerWithBlock:(void(^)(PDCenterTopUpItem *singleModel))block{
    objc_setAssociatedObject(self, &actionClickMangerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellHeightWithModel:(PDCenterTopUpItem *)singleModel{
    return 60;
}

@end
