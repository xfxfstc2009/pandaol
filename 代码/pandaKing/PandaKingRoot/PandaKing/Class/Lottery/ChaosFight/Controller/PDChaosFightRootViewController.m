//
//  PDChaosFightRootViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/17.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightRootViewController.h"
#import "PDBaseSocketRootModel.h"
#import "PDChaosFightTouzhuSingleModel.h"
#import "PDChaosFightKaijiangRootModel.h"
#import "PDChaosFightPopView.h"
#import "PDChaosFightPopChongzhiView.h"
#import "PDChaosFightPopChongzhiPayView.h"
#import "PDTopUpOrder.h"
#import "PDPayOrderModel.h"
#import "PDAlipayHandle.h"
#import "PDWXPayHandle.h"

@interface PDChaosFightRootViewController ()<PDNetworkAdapterDelegate>{
    NSInteger memberCount;
    BOOL hasNext;

}

@property (nonatomic,strong)NSTimer *timer;
@property (nonatomic,assign)NSInteger timerIndex;

@end

@implementation PDChaosFightRootViewController

-(void)dealloc{
    NSLog(@"释放");
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self screenToAutoPortrait];
    [self statusBarHiden:NO];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.hasGoout = NO;
    [self statusBarHiden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([NetworkAdapter sharedAdapter]){
        [NetworkAdapter sharedAdapter].delegate = self;
    }
    [self sendRequestToGetInfo];                            // 1. 进入房间
    [self touzhuManager];                                   // 2. 声明投注
    [self goOutHouseManagerWithBlock:NULL];                 // 3. 退出房间方法
    [self tongjiManager];                                   // 4. 点击统计按钮方法
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self chongzhiManager];                     // 充值
    [self listeningRotating];                   // 监听设备旋转
    [self addObserverAndNotification];          // 设备前台后台
}

#pragma mark - 1. 进入房间
-(void)sendRequestToGetInfo{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([AccountModel sharedAccountModel].token.length){
        [params setObject:[AccountModel sharedAccountModel].token forKey:@"token"];
    }
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){     // LOL
        [params setObject:@"loljungleguess" forKey:@"type"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){    // Dota2
        [params setObject:@"dota2jungleguess" forKey:@"type"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){      // 王者荣耀
        [params setObject:@"kingjungleguess" forKey:@"type"];
    }
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:params socket:[NetworkAdapter sharedAdapter].loginSocket];
}

#pragma mark - 2. 投注方法
-(void)touzhuManager{
    __weak typeof(self)weakSelf = self;
    [self actionXiazhuManagerWithBlock:^(chaosTouzhuType type, PDChaosFightTouzhuView *touzhuView, NSInteger gold) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (!strongSelf.singleModel.guess.guessId.length){
            [PDHUD showHUDProgress:@"暂时还不能投注，请等待" diary:2];
            return;
        }
        [strongSelf touzhuSmartWithView:touzhuView money:gold type:type];
    }];
}

-(void)touzhuSmartWithView:(PDChaosFightTouzhuView *)touzhuView money:(NSInteger)gold type:(chaosTouzhuType )type{
    __weak typeof(self)weakSelf = self;
    [self touzhuViewManagerWithView:touzhuView money:gold block:^(touzhuGameType touzhuGameType) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (touzhuGameType == touzhuGameTypeNoMoney){               // 没钱
            [strongSelf touzhuNoMoneyManager];
            return ;
        } else if (touzhuGameType == touzhuGameTypeShangxian){      // 到达投注上线
            [strongSelf showAlertTopShangxian];
            [PDHUD showErrorWithStatus:[NSString stringWithFormat:@"剩余可投注:%li",self.singleModel.memberMaxDayStake - self.singleModel.memberDayStake]];
            return;
        }
        // 1.修改当前的钱
        if (strongSelf.myCountGold > gold){
            strongSelf.myCountGold -= gold;
        }

        // 2.
        [strongSelf sendRequestToTouzhuGold:gold type:type];
    }];
}

-(void)touzhuNoMoneyManager{
    __weak typeof(self)weakSelf = self;
    [self showAlertToChongzhiBlcok:^{
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDChaosFightPopChongzhiView *chongzhiOneView = [[PDChaosFightPopChongzhiView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width - 2 * 100, kScreenBounds.size.height - 2 * 30)];
        
        [chongzhiOneView actionClickWithBlock:^(PDCenterTopUpItem *singleModel) {
            [chongzhiOneView viewDismiss];
            [strongSelf chongzhiToPay:singleModel];
        }];
        [chongzhiOneView viewShow];
    }];
    
}

#pragma mark - 去充值
-(void)chongzhiManager{
    __weak typeof(self)weakSelf = self;
    [self actionClickToChongzhiManagerBlock:^{          // 充值按钮点击
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDChaosFightPopChongzhiView *chongzhiOneView = [[PDChaosFightPopChongzhiView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width - 2 * 100, kScreenBounds.size.height - 2 * 30)];
        
        [chongzhiOneView actionClickWithBlock:^(PDCenterTopUpItem *singleModel) {
            [chongzhiOneView viewDismiss];
            [strongSelf chongzhiToPay:singleModel];
        }];
        [chongzhiOneView viewShow];
    }];
}

-(void)chongzhiToPay:(PDCenterTopUpItem *)singleModel{
    PDChaosFightPopChongzhiPayView *chongzhiPayView = [[PDChaosFightPopChongzhiPayView alloc]initWithFrame:CGRectMake(0, 0, 260, 300)];
    __weak typeof(self)weakSelf = self;
    [chongzhiPayView actionClickToPayWithModel:^(PDCenterTopUpItem *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf fetchOrderWithType:singleModel.payType selectedItem:singleModel];
        [chongzhiPayView viewDismiss];
    }];
    [chongzhiPayView viewShowWithModel:singleModel];
}

#pragma mark - topay 
- (void)fetchOrderWithType:(NSInteger)type selectedItem:(PDCenterTopUpItem *)item {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerOrder requestParams:@{@"count":@(item.gold),@"money":item.cash} responseObjectClass:[PDTopUpOrder class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded) {
            
            PDTopUpOrder *topUpOrder = (PDTopUpOrder *)responseObject;
            
            if (type == 1) {
                PDPayOrderModel *order = [[PDPayOrderModel alloc] init];
                order.tradeNO = topUpOrder.ID;
                order.amount = [NSString stringWithFormat:@"%.2lf",topUpOrder.totalPrice.floatValue];
                order.productName = topUpOrder.orderName;
                order.productDescription = topUpOrder.orderName;
                PDAlipayHandle1 *alipayHandle1 = [PDAlipayHandle1 payHandle];
                alipayHandle1.notifyUrl = topUpOrder.alipayNotifyUrl;
                [alipayHandle1 payWithOrder:order payResultComplication:^(NSInteger code, NSString *errMsg) {
                    if (code == 9000) {
                        [PDHUD showHUDSuccess:@"支付成功"];
                        strongSelf.myCountGold = strongSelf.myCountGold + (item.gold + item.goldAwardForPay);
                        [strongSelf changeMyCoinWithMoney:strongSelf.myCountGold];
                        return ;
                    } else {
                        [PDHUD showHUDError:@"支付失败"];
                    }
                }];
            } else {
                // 这个order可以用在支付宝最新版
                Order *order = [[Order alloc] init];
                order.biz_content = [BizContent new];
                order.biz_content.out_trade_no = topUpOrder.ID;
                order.biz_content.total_amount = [NSString stringWithFormat:@"%.2lf",topUpOrder.totalPrice.floatValue];
                order.biz_content.body = topUpOrder.orderName;
                
                PDWXPayHandle *wxpayHandle = [PDWXPayHandle payHandle];
                wxpayHandle.notifyUrl = topUpOrder.tenpayNotifyUrl;
                [wxpayHandle sendPayRequestWithOrder:order];
            }
            
        }
    }];
}


#pragma mark  2.1投注接口
-(void)sendRequestToTouzhuGold:(NSInteger)gold type:(chaosTouzhuType)type{
    
    NSString *typeStr = @"";
    if (type == chaosTouzhuTypeTuoersuo){               // 托儿所胜利
        typeStr = @"pointAWin";
    } else if (type == chaosTouzhuTypePingshou){            // 平手
        typeStr = @"tie";
    } else if (type == chaosTouzhuTypeertongjie){           // 儿童节
        typeStr = @"pointBWin";
    } else if (type == chaosTouzhuTypeShuangsha){           // 双杀
        typeStr = @"anyoneDoubleKill";
    } else if (type == chaosTouzhuTypeShuangkong){          // 双空
        typeStr = @"anyoneDoubleNull";
    } else if (type == chaosTouzhuTypeYizhi){               // 一只
        typeStr = @"overallOne";
    } else if (type == chaosTouzhuTypeLiangzhi){            // 2只
        typeStr = @"overallTwo";
    } else if (type == chaosTouzhuTypeSanzhi){              // 3只
        typeStr = @"overallThree";
    } else if (type == chaosTouzhuTypeSizhi){               // 4只
        typeStr = @"overallFour";
    } else if (type == chaosTouzhuTypeXiaolong){            // 小龙
        typeStr = @"anyoneDragonet";
    } else if (type == chaosTouzhuTypeDalong){              // 大龙
        typeStr = @"anyoneDragon";
    } else if (type == chaosTouzhuTypeBlackDragonet){       // 黑龙
        typeStr = @"anyoneBlackDragon";
    } else if (type == chaosTouzhuTypeTyrant){              // 暴君
        typeStr = @"anyoneTyrant";
    } else if (type == chaosTouzhuTypeMeatMountain){        // 肉山
        typeStr = @"anyoneMeatMountain";
    } else if (type == chaosTouzhuTypeMaster){              // 主宰
        typeStr = @"anyoneMaster";
    } else if (type == chaosTouzhuTypeLangRen){             // 狼人
        typeStr = @"pointAWin";
    } else if (type == chaosTouzhuTypeMori){                // 末日
        typeStr = @"pointBWin";
    } else if (type == chaosTouzhuTypeXiahoudun){              // 暴君
        typeStr = @"pointAWin";
    } else if (type == chaosTouzhuTypeChengyaojin){         // 程咬金
        typeStr = @"pointBWin";
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        [params setObject:@"kingstakejungleguess" forKey:@"type"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        [params setObject:@"dota2stakejungleguess" forKey:@"type"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        [params setObject:@"lolstakejungleguess" forKey:@"type"];
    }
    if ([AccountModel sharedAccountModel].token.length){
        [params setObject:[AccountModel sharedAccountModel].token forKey:@"token"];
    }
    [params setObject:self.singleModel.guess.guessId forKey:@"guessId"];
    [params setObject:typeStr forKey:@"guessItemType"];
    [params setObject:@(gold) forKey:@"gold"];
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:params socket:[NetworkAdapter sharedAdapter].loginSocket];
}

#pragma mark - 3. 退出房间
-(void)goOutHouseManagerWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [self gooutManagerWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf gooutSingleManager:^{
            if (block){
                block();
            }
        }];
    }];
}

-(void)gooutSingleManager:(void(^)())block{
    
    [self stopTomer];
    if (self.singleModel.guess.guessId.length){
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
            [params setObject:@"kingquitjungleguess" forKey:@"type"];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
            [params setObject:@"dota2quitjungleguess" forKey:@"type"];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
            [params setObject:@"lolquitjungleguess" forKey:@"type"];
        }
        [params setObject:[AccountModel sharedAccountModel].token forKey:@"token"];
        [params setObject:self.singleModel.guess.guessId forKey:@"guessId"];
        [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:params socket:[NetworkAdapter sharedAdapter].loginSocket];
    }
    self.hasGoout = YES;
    [self dismissRootManager];
    [self dismissCard];
    [self dismissViewControllerAnimated:YES completion:^{
        if (block){
            block();
        }
    }];
}


-(void)socketDidBackData:(id)responseObject{
    __weak typeof(self)weakSelf = self;
    if ([responseObject isKindOfClass:[NSDictionary class]]){
//        if ([[responseObject allKeys] containsObject:@"code"]){
//            NSString *infoCode = [NSString stringWithFormat:@"%li",[[responseObject objectForKey:@"code"] integerValue]];
//            NSString *infoMsg = [NSString stringWithFormat:@"%@",[responseObject objectForKey:@"msg"]];
//            if ([infoCode isEqualToString:@"501"]){
//                if ([infoMsg isEqualToString:@"您已达到该次竞猜投注上限"]){
//                    
//                } else {
//                    [self gooutSingleManager:NULL];
//                }
//                
//                return;
//            }
//        }
        PDBaseSocketRootModel *baseSocket = [[PDBaseSocketRootModel alloc] initWithJSONDict:[responseObject objectForKey:@"data"]];
        if (baseSocket.type == socketType970 || baseSocket.type == socketType980 || baseSocket.type == socketType990){                                                              // 【人数变化】
            [self updateHouseTotalCount:baseSocket.totalMemberCount];
        } else if (baseSocket.type == socketType971 || baseSocket.type == socketType981 || baseSocket.type == socketType991){                                                       // 【人刚进来获取数据】
            hasNext = NO;
            self.singleModel = baseSocket.jungleGuessInfo;
            
            [self updateHouseInfo:self.singleModel];
            // 2.  开启计时器
            [self startTime];
            // 4. 修改头部内容
            [self changeTitleImgType:touzhuGameTypeNormal];
            // 5. 历史投注信息
            [self touzhuWillGoInGame:self.singleModel];
            // 6. 保存到我当前的投注
            [AccountModel sharedAccountModel].accountMemberDayStake = self.singleModel.memberDayStake;
            if(self.singleModel.memberMaxDayStake == 0){
                [AccountModel sharedAccountModel].accountCountDayStake = 10000000000000000;
            } else {
                [AccountModel sharedAccountModel].accountCountDayStake = self.singleModel.memberMaxDayStake;
            }
            
            // 7. 修改赔率
            [self changeGuessItemInfoWithModel:baseSocket.jungleGuessInfo];
        } else if (baseSocket.type == socketType972 || baseSocket.type == socketType982 || baseSocket.type == socketType992){                                                       // 【玩家进入】
            [self hasMemberGoinMember:baseSocket.member];
        } else if (baseSocket.type == socketType973 || baseSocket.type == socketType983 || baseSocket.type == socketType993){
            
        } else if (baseSocket.type == socketType974 || baseSocket.type == socketType984 || baseSocket.type == socketType994){                                                       // 【人退出】
            [self hasMemberLogoutWithModel:baseSocket.member];
        } else if (baseSocket.type == socketType975 || baseSocket.type == socketType985 || baseSocket.type == socketType995){                                                       // 【自己投注方法】
            PDChaosFightTouzhuSingleModel *touzhuSingleModel = baseSocket.guessItem;
            [self touzhuSocketBackManager:touzhuSingleModel];
        } else if (baseSocket.type == socketType976 || baseSocket.type == socketType986 || baseSocket.type == socketType996){                                                       // 【别人下注】
            
            PDChaosFightOtherTouzhuSingleModel *mainSingleModel = baseSocket.stakeData;
            if ([mainSingleModel.guessId isEqualToString:self.singleModel.guess.guessId]){
                [self touzhuWithOtherGamer:mainSingleModel];
            }
        } else if (baseSocket.type == socketType979 || baseSocket.type == socketType989 || baseSocket.type == socketType999){                           // 开奖
            if (hasNext == YES){
                return;
            }
            [self changeTitleImgType:touzhuGameTypeEnd];        // 1. 修改当前状态
            
            __weak typeof(self)weakSelf = self;
            [self challengeGuaishouAnimationWithBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf socket9791WithModel:baseSocket.drawJungleLotteryResult];
            }];
        } else if (baseSocket.type == socketType978 || baseSocket.type == socketType988 || baseSocket.type == socketType998){               // 长时间不动踢出
            [self showAlertBeitichuWithBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf gooutSingleManager:NULL];
            }];
        }  else if (baseSocket.type == socketType501){
            [PDHUD showConnectionErr:baseSocket.msg];
        }
    }
}

-(void)socket9791WithModel:(PDChaosFightKaijiangRootModel *)drawJungleLotteryResult{
    PDChaosFightKaijiangRootModel *kaijiangSingleModel = drawJungleLotteryResult;
    __weak typeof(self)weakSelf = self;
    [weakSelf kaijiangAnimationManager:kaijiangSingleModel block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf socket9792WithModel:kaijiangSingleModel];
    }];
}

-(void)socket9792WithModel:(PDChaosFightKaijiangRootModel *)drawJungleLotteryResult{
    __weak typeof(self)weakSelf = self;
    [weakSelf winWithCoinRootManager:drawJungleLotteryResult block:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 1. 清空上面的view
        [strongSelf dismissRootManager];
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.4f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            // 2. 下一把
            [weakSelf nextGame];
            strongSelf->hasNext = YES;
        });
    }];
}


#pragma mark - 继续下一把
-(void)nextGame{
    NSLog(@"【请求下一吧】");
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        [params setObject:@"kingnextjungleguess" forKey:@"type"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        [params setObject:@"dota2nextjungleguess" forKey:@"type"];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        [params setObject:@"lolnextjungleguess" forKey:@"type"];
    }
    [params setObject:[AccountModel sharedAccountModel].token forKey:@"token"];
    [[NetworkAdapter sharedAdapter] socketFetchModelWithJavaRequestParams:params socket:[NetworkAdapter sharedAdapter].loginSocket];
}


#pragma mark - 有用户退出
-(void)hasMemberLogoutWithModel:(PDChaosFightGooutSingleModel *)member{
    NSMutableArray *tempArr = [NSMutableArray arrayWithArray:self.singleModel.members];
    for (PDChaosFightMembersSingleModel *memberSingleModel in self.singleModel.members){
        if ([memberSingleModel.memberId isEqualToString:[AccountModel sharedAccountModel].memberId]){
            [tempArr removeObject:memberSingleModel];
            break;
        }
    }
    
    for (int i = 0 ; i < tempArr.count;i++){
        PDChaosFightMembersSingleModel *singleMemberModel = [tempArr objectAtIndex:i];
        if ([singleMemberModel.memberId isEqualToString:member.memberId]){
             [self goOutMemberIndex:i];
        }
    }
}

#pragma mark - 玩家进入
-(void)hasMemberGoinMember:(PDChaosFightGooutSingleModel *)member{
    [self goInMemberSingleModel:member];
}

#pragma mark - 投注方法
-(void)touzhuSocketBackManager:(PDChaosFightTouzhuSingleModel *)touzhuManager{
    [self touzhuSocketBaseBackManager:touzhuManager];
    // 减少自己的金币
    [self changeMyCoinWithMoney:self.myCountGold];
}

#pragma mark - 比赛开始
#pragma mark - startTime
-(void)startTime{
    if (!self.timer){
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
        NSRunLoop *currentRunLoop = [NSRunLoop currentRunLoop];
        [currentRunLoop addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    NSInteger time = (self.singleModel.lottery.drawTime - self.singleModel.lottery.currentTime) / 1000.;
    self.timerIndex = time;
}

-(void)timeChange{
    self.timerIndex--;
    [self changeTimerImgWithInteger:self.timerIndex];
    if (self.timerIndex <= -10 && hasNext == NO){
        [self nextGame];
    }
}

#pragma mark - 停止计时器
-(void)stopTomer{
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}

-(void)tongjiManager{
    __weak typeof(self)weakSelf = self;
    [self tongjiBtnClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf chaosInfoAlert];
        return;
//        [strongSelf showAlertToCount];
    }];
}



#pragma mark - 旋转
- (BOOL)shouldAutorotate{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {      //viewController所支持的全部旋转方向
    return UIInterfaceOrientationMaskLandscapeRight;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {     //viewController初始显示的方向
    return UIInterfaceOrientationLandscapeRight;
}


-(void)chaosInfoAlert{
    CGFloat xWidth = kScreenBounds.size.width - 2 * 100;
    CGFloat yHeight = kScreenBounds.size.height - 2 * 60;
    CGFloat yOffset = (self.view.bounds.size.height - yHeight)/2.0f;
    PDChaosFightPopView *poplistview = [[PDChaosFightPopView alloc] initWithFrame:CGRectMake(100, yOffset, xWidth, yHeight)];
    [poplistview showChaosViewWithType:dayeAlertTypeTongji];
}

/**
 *  全屏按钮事件
 *
 *  @param sender 全屏Button
 */
- (void)fullScreenAction
{
    UIDeviceOrientation orientation             = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = (UIInterfaceOrientation)orientation;
    switch (interfaceOrientation) {
            
        case UIInterfaceOrientationPortraitUpsideDown:{
            [self interfaceOrientation:UIInterfaceOrientationPortrait];
        }
            break;
        case UIInterfaceOrientationPortrait:{
            [self interfaceOrientation:UIInterfaceOrientationLandscapeRight];
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:{
            [self interfaceOrientation:UIInterfaceOrientationPortrait];
        }
            break;
        case UIInterfaceOrientationLandscapeRight:{
            [self interfaceOrientation:UIInterfaceOrientationPortrait];
        }
            break;
            
        default:
            break;
    }
}

-(void)screenToAutoPortrait{
    [self interfaceOrientation:UIInterfaceOrientationPortrait];
}

/**
 *  屏幕方向发生变化会调用这里
 */
- (void)onDeviceOrientationChange {
    UIDeviceOrientation orientation             = [UIDevice currentDevice].orientation;
    UIInterfaceOrientation interfaceOrientation = (UIInterfaceOrientation)orientation;
    switch (interfaceOrientation) {
        case UIInterfaceOrientationPortraitUpsideDown:{
            NSLog(@"【Down】");
        }
            break;
        case UIInterfaceOrientationPortrait:{
            NSLog(@"【Portrait】");
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:{
            NSLog(@"【Left】");
        }
            break;
        case UIInterfaceOrientationLandscapeRight:{
            NSLog(@"【Right】");
        }
            break;
            
        default:
            break;
    }
}

/**
 *  强制屏幕转屏
 *
 *  @param orientation 屏幕方向
 */
- (void)interfaceOrientation:(UIInterfaceOrientation)orientation
{
    // arc下
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector             = NSSelectorFromString(@"setOrientation:");
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
        [invocation setSelector:selector];
        [invocation setTarget:[UIDevice currentDevice]];
        int val                  = orientation;
        // 从2开始是因为0 1 两个参数已经被selector和target占用
        [invocation setArgument:&val atIndex:2];
        [invocation invoke];
    }
    if (orientation == UIInterfaceOrientationLandscapeRight || orientation == UIInterfaceOrientationLandscapeLeft) {
        // 设置横屏
        [self setOrientationLandscape];
        
    }else if (orientation == UIInterfaceOrientationPortrait) {
        // 设置竖屏
        [self setOrientationPortrait];
        
    }
}

/**
 *  设置横屏的约束
 */
- (void)setOrientationLandscape
{
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
        // 亮度view加到window最上层
    
}

/**
 *  设置竖屏的约束
 */
- (void)setOrientationPortrait
{
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:YES];
 }


#pragma mark - 监听设备旋转通知
- (void)listeningRotating{
    [[UIDevice currentDevice] beginGeneratingDeviceOrientationNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onDeviceOrientationChange) name:UIDeviceOrientationDidChangeNotification object:nil ];
}

#pragma mark - 观察者、通知
- (void)addObserverAndNotification {
    // app退到后台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterBackground) name:UIApplicationWillResignActiveNotification object:nil];
    // app进入前台
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appDidEnterPlayGround) name:UIApplicationDidBecomeActiveNotification object:nil];
}

-(void)appDidEnterBackground{
    NSLog(@"后台");
}

-(void)appDidEnterPlayGround{
    NSLog(@"前台");
}


// 【状态栏隐藏和显示】
-(void)statusBarHiden:(BOOL)hiden{
    UIScrollView *statusBar = [[UIApplication sharedApplication] valueForKey:@"statusBarWindow"];
    [UIView animateWithDuration:.2f animations:^{
        if (hiden){
            statusBar.orgin_y = -20;
        } else {
            statusBar.orgin_y = 0;
        }
    }];
}

@end
