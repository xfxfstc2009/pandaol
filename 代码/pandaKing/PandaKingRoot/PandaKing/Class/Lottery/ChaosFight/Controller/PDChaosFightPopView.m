//
//  PDChaosFightPopView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/17.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightPopView.h"
#import "PDChaosTodayCountCell.h"
#import "PDChaosFightHistoryListModel.h"
#import "PDChaosLishiCountCell.h"
#import "PDChaosFightGuizeTableViewCell.h"


@interface PDChaosFightPopView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UIControl *dismissControl;
@property (nonatomic,strong)NSMutableArray *segmentMutableArr;
@property (nonatomic,strong)NSMutableArray *historyMutableArr;
@property (nonatomic,strong)PDImageView *backgroundImageView;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)UITableView *countTableView;
@property (nonatomic,strong)UITableView *rightTableView;
@property (nonatomic,strong)PDChaosFightTodayCountRootModel *transferSingleModel;


@end

@implementation PDChaosFightPopView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self pageSetting];
        [self createDismissBackgroundView];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor whiteColor];
    self.bgView.frame = self.bounds;
    self.bgView.userInteractionEnabled = YES;
    [self addSubview:self.bgView];
}

-(void)showChaosViewWithType:(dayeAlertType)type{
    self.segmentMutableArr = [NSMutableArray array];
    self.historyMutableArr = [NSMutableArray array];
    
    _transferDayeType = type;
    // 1.
    [self createBgImgView];
    [self viewShow];
}


- (UIWindow *)lastWindow
{
    NSArray *windows = [UIApplication sharedApplication].windows;
    for(UIWindow *window in [windows reverseObjectEnumerator]) {
        
        if ([window isKindOfClass:[UIWindow class]] &&
            CGRectEqualToRect(window.bounds, [UIScreen mainScreen].bounds))
            
            return window;
    }
    
    return [UIApplication sharedApplication].keyWindow;
}




-(void)showChaosViewWithTopView:(UIView *)topView{
    self.segmentMutableArr = [NSMutableArray array];
    self.historyMutableArr = [NSMutableArray array];
    
    // 1.
    [self createBgImgView];
    [self viewShowTopView:topView];
}

#pragma mark - createbgView
-(PDImageView *)createBgImgView{
    if (!self.backgroundImageView){
        self.backgroundImageView = [[PDImageView alloc]init];
        self.backgroundImageView.image = [Tool stretchImageWithName:@"icon_chaosFight_alert_today_bg"];
        self.backgroundImageView.frame = self.bounds;
        self.backgroundImageView.userInteractionEnabled = YES;
        [self addSubview:self.backgroundImageView];
    }
    
    UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_close"] forState:UIControlStateNormal];
    
    
    // 1. 创建标签
    [self createSegmentList];
    
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        self.mainScrollView.frame = CGRectMake(20, CGRectGetMaxY(self.leftButton.frame) + 10, self.backgroundImageView.size_width - 2 * 20, self.backgroundImageView.size_height - CGRectGetMaxY(self.leftButton.frame) - 10 - 11);
        self.mainScrollView.delegate = self;
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.size_width * 2 , self.mainScrollView.size_height);
        self.mainScrollView.scrollEnabled = NO;
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.alwaysBounceVertical = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        [self.backgroundImageView addSubview:self.mainScrollView];
        // 1. 创建table
        [self createTableView];
        // 2. 创建web
        [self createWebView];
        
        if (self.transferDayeType == dayeAlertTypeGuize){
            [self.mainScrollView setContentOffset:CGPointMake(self.mainScrollView.size_width * 1, 0)];
            
            self.leftButton.hidden = YES;
            [self.rightButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment2_hlt"] forState:UIControlStateNormal];
            self.rightButton.center_x = self.mainScrollView.center_x;
            
        } else if (self.transferDayeType == dayeAlertTypeTongji){
            [self.mainScrollView setContentOffset:CGPointMake(self.mainScrollView.size_width * 0, 0)];
            
            self.rightButton.hidden = YES;
            [self.leftButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment1_hlt"] forState:UIControlStateNormal];
            self.leftButton.center_x = self.mainScrollView.center_x;
        }
        
    }
//    [self.mainScrollView setContentOffset:CGPointMake(0, 0)];
    
    [self sendRequestToGetInfo];
    [self sendRequestToGetCurrentData];
    
    
    //
    
    
    
    
    return self.backgroundImageView;
}

#pragma mark - createSegment
-(void)createSegmentList{
    CGFloat origin_x = (self.backgroundImageView.size_width - 2 * 103) / 2.;
    self.leftButton = [self createButton];
    self.leftButton.frame = CGRectMake(origin_x, 11, 103, 30);
    [self.leftButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment1_hlt"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    self.leftButton.userInteractionEnabled = NO;
//    [self.leftButton buttonWithBlock:^(UIButton *button) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf buttonClickManager:0];
//    }];
    
    [self.backgroundImageView addSubview:self.leftButton];
    
    self.rightButton = [self createButton];
    self.rightButton.frame = CGRectMake(CGRectGetMaxX(self.leftButton.frame), self.leftButton.orgin_y, self.leftButton.size_width, self.leftButton.size_height);
    [self.rightButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment2_nor"] forState:UIControlStateNormal];
    self.rightButton.userInteractionEnabled = NO;
//    [self.rightButton buttonWithBlock:^(UIButton *button) {
//        if (!weakSelf){
//            return ;
//        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
//        [strongSelf buttonClickManager:1];
//    }];
    
    [self.backgroundImageView addSubview:self.rightButton];
}

-(void)buttonClickManager:(NSInteger)btnIndex{
    if (btnIndex == 0){
        [self.leftButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment1_hlt"] forState:UIControlStateNormal];
        [self.rightButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment2_nor"] forState:UIControlStateNormal];
        [self.mainScrollView setContentOffset:CGPointMake(self.mainScrollView.size_width * 0, 0) animated:YES];
        
    } else {
        [self.leftButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment1_nor"] forState:UIControlStateNormal];
        [self.rightButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_segment2_hlt"] forState:UIControlStateNormal];
        [self.mainScrollView setContentOffset:CGPointMake(self.mainScrollView.size_width * 1, 0) animated:YES];
    }
}

-(UIButton *)createButton{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor clearColor];
    button.frame = CGRectMake(0, 0, 103, 30);
    return button;
}

#pragma mark - 创建TableView
-(void)createTableView{
    if (!self.countTableView){
        self.countTableView = [GWViewTool gwCreateTableViewRect:CGRectMake(0, 0, self.mainScrollView.size_width, self.mainScrollView.size_height)];
        self.countTableView.dataSource = self;
        self.countTableView.delegate = self;
        [self.mainScrollView addSubview:self.countTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0){
        return 1;
    } else {
        return self.historyMutableArr.count;
    }
}

#pragma mark - UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if(tableView == self.countTableView){
        if (indexPath.section == 0){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            PDChaosTodayCountCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[PDChaosTodayCountCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferCellWidth = self.countTableView.size_width;
            cellWithRowOne.transferSingleModel = self.transferSingleModel;
            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            PDChaosLishiCountCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[PDChaosLishiCountCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferCellWidth = tableView.size_width;
            if (indexPath.row == self.historyMutableArr.count - 1){
                cellWithRowTwo.transferIndex = -1;
                
            } else {
                cellWithRowTwo.transferIndex = indexPath.row;
            }
            cellWithRowTwo.transferHistorySingleModel = [self.historyMutableArr objectAtIndex:indexPath.row];
            
            return cellWithRowTwo;
        }
    } else {
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        PDChaosFightGuizeTableViewCell*cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[PDChaosFightGuizeTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferWidth = tableView.size_width;
        return cellWithRowThr;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.countTableView){
        if (indexPath.section == 0){
            return [PDChaosTodayCountCell calculationCellHeight];
        } else {
            return 30;
        }
    } else {
        return [PDChaosFightGuizeTableViewCell calculationCellHeightWithWidth:self.rightTableView.size_width];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (tableView == self.countTableView){
        if (section == 1){
            return 20;
        } else {
            return 0;
        }
    } else {
        return 0;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (tableView == self.countTableView){
        UIView *headerView = [[UIView alloc]init];
        headerView.backgroundColor = [UIColor clearColor];
        headerView.frame = CGRectMake(0, 0, tableView.size_width, 20);
        
        UILabel *titleLabel = [GWViewTool createLabelFont:@"正文" textColor:@"金"];
        titleLabel.font = [UIFont systemFontOfSize:12.];
        titleLabel.frame = CGRectMake(11, 0, tableView.size_width, 20);
        if (section == 0){
            titleLabel.text = @"今日统计";
        } else if (section == 1){
            titleLabel.text = @"最近胜负";
        }
        titleLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
        [headerView addSubview:titleLabel];
        
        return headerView;
    } else{
        return nil;
    }
}

#pragma mark - 创建webView
-(void)createWebView{
    if (!self.rightTableView){
        self.rightTableView = [GWViewTool gwCreateTableViewRect:CGRectMake(self.mainScrollView.size_width, 0, self.mainScrollView.size_width, self.mainScrollView.size_height)];
        self.rightTableView.dataSource = self;
        self.rightTableView.delegate = self;
        [self.mainScrollView addSubview:self.rightTableView];
    }
}

//去掉UItableview headerview黏性(sticky)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat sectionHeaderHeight = 20;
    if (scrollView.contentOffset.y <= sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
        scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
    } else if (scrollView.contentOffset.y >= sectionHeaderHeight) {
        scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
    }
}

#pragma mark - 接口
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    NSString *path = @"";
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        path = todayloljunglestatistic;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        path = todaydota2junglestatistic;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        path = todaykingjunglestatistic;
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithPath:path requestParams:nil responseObjectClass:[PDChaosFightTodayCountRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if(isSucceeded){
            strongSelf.transferSingleModel = (PDChaosFightTodayCountRootModel *)responseObject;
            [strongSelf sendRequestToGetCurrentData];
        }
    }];
}

#pragma mark - 获取当前数据
-(void)sendRequestToGetCurrentData{
    __weak typeof(self)weakSelf = self;
    NSString *path = @"";
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        path = todayloljungledraw;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        path = todaydota2jungledraw;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
        path = todaykingjungledraw;
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:path requestParams:nil responseObjectClass:[PDChaosFightHistoryListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDChaosFightHistoryListModel *historyListModel = (PDChaosFightHistoryListModel *)responseObject;
            [strongSelf.historyMutableArr addObjectsFromArray:historyListModel.drawList];
            [strongSelf.countTableView reloadData];
        }
    }];
}








#pragma mark - 显示充值方法
-(void)showNoYueToChongzhiWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    PDImageView *chongzhiView = [self createChongzhiViewWithActionClick:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
        [self viewDismiss];
    }];
    
    [self addSubview:chongzhiView];
    [self viewShow];
}


#pragma mark - 创建充值View
-(PDImageView *)createChongzhiViewWithActionClick:(void(^)())block{
    PDImageView *backgroundImageView = [[PDImageView alloc]init];
    backgroundImageView.image = [Tool stretchImageWithName:@"icon_chaosFight_alert_touzhuFail_background"];
    backgroundImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * 100, kScreenBounds.size.height - 2 * 60);
    backgroundImageView.userInteractionEnabled = YES;
    
    PDImageView *noyueImgView = [[PDImageView alloc]init];
    noyueImgView.image = [UIImage imageNamed:@"icon_chaosFight_alert_noyue"];
    
    //    363 / 136
    CGFloat noyueHeight = (136 * (backgroundImageView.size_width - 2 * 11)) * 1.f / 363.;
    
    noyueImgView.frame = CGRectMake(11,11,backgroundImageView.size_width - 2 * 11, noyueHeight);
    noyueImgView.center_x = backgroundImageView.size_width / 2.;
    [backgroundImageView addSubview:noyueImgView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    __weak typeof(self)weakSelf = self;
    [btn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    btn.frame = CGRectMake(0, 0, 130, 38);
    btn.center_x = backgroundImageView.size_width / 2.;
    
    btn.orgin_y = (backgroundImageView.size_height - noyueHeight - 11 - 38) / 2. + 11 + noyueHeight;
    
    [btn setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_tozhongzhi"] forState:UIControlStateNormal];
    [backgroundImageView addSubview:btn];
    
    return backgroundImageView;
}



#pragma mark - 显示到达上线方法
-(void)showShangxianWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    PDImageView *chongzhiView = [self showDaodashangxianWithBlock:^{
        if (!weakSelf){
            return ;
        }
        [self viewDismiss];
        if (block){
            block();
        }
    }];
    [self addSubview:chongzhiView];
    [self viewShow];
}


-(PDImageView *)showDaodashangxianWithBlock:(void(^)())block{
    PDImageView *backgroundImageView = [[PDImageView alloc]init];
    backgroundImageView.image = [Tool stretchImageWithName:@"icon_chaosFight_alert_touzhuFail_background"];
    backgroundImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * 100, kScreenBounds.size.height - 2 * 60);
    backgroundImageView.userInteractionEnabled = YES;
    
    PDImageView *noyueImgView = [[PDImageView alloc]init];
    noyueImgView.image = [UIImage imageNamed:@"icon_chaosFight_alert_touzhuFail_shangxian"];
    
    //    363 / 136
    CGFloat noyueHeight = (136 * (backgroundImageView.size_width - 2 * 11)) * 1.f / 363.;
    
    noyueImgView.frame = CGRectMake(11,11,backgroundImageView.size_width - 2 * 11, noyueHeight);
    noyueImgView.center_x = backgroundImageView.size_width / 2.;
    [backgroundImageView addSubview:noyueImgView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    __weak typeof(self)weakSelf = self;
    [btn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    btn.frame = CGRectMake(0, 0, 130, 38);
    btn.center_x = backgroundImageView.size_width / 2.;
    
    btn.orgin_y = (backgroundImageView.size_height - noyueHeight - 11 - 38) / 2. + 11 + noyueHeight;
    
    [btn setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_iknow"] forState:UIControlStateNormal];
    [backgroundImageView addSubview:btn];
    
    return backgroundImageView;
}



#pragma mark - 现实被踢出
-(void)showTichuWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    PDImageView *chongzhiView = [self showBeitichuImgWithBlock:^{
        if (!weakSelf){
            return ;
        }
        [self viewDismiss];
        if (block){
            block();
        }
    }];
    
    [self addSubview:chongzhiView];
    [self viewShow];
}




-(PDImageView *)showBeitichuImgWithBlock:(void(^)())block{
    PDImageView *backgroundImageView = [[PDImageView alloc]init];
    backgroundImageView.image = [Tool stretchImageWithName:@"icon_chaosFight_alert_touzhuFail_background"];
    backgroundImageView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * 100, kScreenBounds.size.height - 2 * 60);
    backgroundImageView.userInteractionEnabled = YES;
    
    PDImageView *noyueImgView = [[PDImageView alloc]init];
    noyueImgView.image = [UIImage imageNamed:@"icon_chaosFight_alert_changshijianbucaozuo"];
    
    //    363 / 136
    CGFloat noyueHeight = (136 * (backgroundImageView.size_width - 2 * 11)) * 1.f / 363.;
    
    noyueImgView.frame = CGRectMake(11,11,backgroundImageView.size_width - 2 * 11, noyueHeight);
    noyueImgView.center_x = backgroundImageView.size_width / 2.;
    [backgroundImageView addSubview:noyueImgView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    __weak typeof(self)weakSelf = self;
    [btn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    btn.frame = CGRectMake(0, 0, 130, 38);
    btn.center_x = backgroundImageView.size_width / 2.;
    
    btn.orgin_y = (backgroundImageView.size_height - noyueHeight - 11 - 38) / 2. + 11 + noyueHeight;
    
    [btn setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_alert_iknow"] forState:UIControlStateNormal];
    [backgroundImageView addSubview:btn];
    
    return backgroundImageView;
}








#pragma mark - popView
- (void)fadeIn {
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)fadeOut {
    [UIView animateWithDuration:.4 animations:^{
        self.transform = CGAffineTransformMakeScale(1, 1);
        self.alpha = 0.0;
        self.dismissControl.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.dismissControl removeFromSuperview];
            [self removeFromSuperview];
        }
    }];
}

- (void)viewShow {
    UIWindow *keywindow = [self lastWindow];
    [keywindow addSubview:self.dismissControl];
    [keywindow addSubview:self];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,  keywindow.bounds.size.height/2.0f);
    
    [self fadeIn];
}

-(void)viewShowTopView:(UIView *)topView{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self.dismissControl];
    [[UIApplication sharedApplication].keyWindow insertSubview:self belowSubview:topView];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,  keywindow.bounds.size.height/2.0f);
    
    [self fadeIn];
}

- (void)viewDismiss {
    [self fadeOut];
}

-(void)pageSetting{
    self.layer.cornerRadius = 10.0f;
    self.clipsToBounds = true;
    self.userInteractionEnabled = YES;
}

-(void)createDismissBackgroundView{
    self.dismissControl = [[UIControl alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.dismissControl.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.5];
    [self.dismissControl addTarget:self action:@selector(viewDismiss) forControlEvents:UIControlEventTouchUpInside];
}


@end
