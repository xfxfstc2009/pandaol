//
//  PDChaosFightPopChongzhiPayView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCenterTopUpItem.h"

@interface PDChaosFightPopChongzhiPayView : UIView

-(void)viewShowWithModel:(PDCenterTopUpItem *)singleModel;
-(void)viewDismiss;

-(void)actionClickToPayWithModel:(void(^)(PDCenterTopUpItem *singleModel))block;

@end
