//
//  PDChaosFightPopChongzhiView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightPopChongzhiView.h"
#import "PDChaosFightPagePaySingleCell.h"
#import "PDCenterTopUpList.h"

static char actionClickWithBlockKey;
@interface PDChaosFightPopChongzhiView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)UILabel *shouchongLabel;
@property (nonatomic,strong)PDImageView *imgView1;
@property (nonatomic,strong)PDImageView *imgView2;
@property (nonatomic,strong)PDImageView *imgView3;

@property (nonatomic,strong)UIControl *dismissControl;
@property (nonatomic,strong)UITableView *payTableView;
@property (nonatomic,strong)NSMutableArray *payMutableArr;
@end

@implementation PDChaosFightPopChongzhiView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        if (!self.payMutableArr){
            self.payMutableArr = [NSMutableArray array];
        }
        [self pageSetting];
        [self createDismissBackgroundView];
        [self createView];
        [self sendRequestTogetChongzhiList];                    //
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.frame = self.bounds;
    self.bgView.userInteractionEnabled = YES;
    [self addSubview:self.bgView];
    
    // 1.
    self.imgView1 = [[PDImageView alloc]init];
    self.imgView1.backgroundColor = [UIColor clearColor];
    self.imgView1.image = [UIImage imageNamed:@"icon_chaosFight_chongzhi_one_top"];
    [self.bgView addSubview:self.imgView1];
    
    self.imgView2 = [[PDImageView alloc]init];
    self.imgView2.backgroundColor = [UIColor clearColor];
    self.imgView2.userInteractionEnabled = YES;
    [self.bgView addSubview:self.imgView2];
    
    self.imgView3 = [[PDImageView alloc]init];
    self.imgView3.backgroundColor = [UIColor clearColor];
    self.imgView3.image = [UIImage imageNamed:@"icon_chaosFight_chongzhi_one_bottom"];
    [self.bgView addSubview:self.imgView3];
    
    // 1. 计算
    // 440 63
    CGFloat top_height = 63 * self.size_width / 440;
    self.imgView1.frame = CGRectMake(0, 0, self.size_width, top_height);
    
    // bottom
    CGFloat bottom_height = 30 * self.size_width / 440;
    self.imgView3.frame = CGRectMake(0, self.size_height - bottom_height, self.size_width, bottom_height);
    
    // normal
    self.imgView2.frame = CGRectMake(0, CGRectGetMaxY(self.imgView1.frame), self.size_width, self.imgView3.orgin_y - CGRectGetMaxY(self.imgView1.frame));
    self.imgView2.image = [UIImage imageNamed:@"icon_chaosFight_chongzhi_one_normal"];
    
    self.shouchongLabel = [[UILabel alloc]init];
    self.shouchongLabel.font = [UIFont systemFontOfSize:12.];
    self.shouchongLabel.textColor = [UIColor hexChangeFloat:@"de6c2b"];
    [self.imgView2 addSubview:self.shouchongLabel];
    self.shouchongLabel.frame = CGRectMake(33, 0, self.imgView2.size_width - 2 * 33, [NSString contentofHeightWithFont:self.shouchongLabel.font]);
    
    self.payTableView = [PDViewTool gwCreateTableViewRect:self.imgView2.bounds];
    self.payTableView.frame = CGRectMake(20, CGRectGetMaxY(self.shouchongLabel.frame), self.imgView2.size_width - 2 * 20, self.imgView2.size_height - CGRectGetMaxY(self.shouchongLabel.frame));
    self.payTableView.dataSource = self;
    self.payTableView.delegate = self;
    self.payTableView.userInteractionEnabled = YES;
    [self.imgView2 addSubview:self.payTableView];
    
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.payMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    PDChaosFightPagePaySingleCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[PDChaosFightPagePaySingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferCellWidth = tableView.size_width;
    cellWithRowOne.transferSingleModel = [self.payMutableArr objectAtIndex:indexPath.section];
    __weak typeof(self)weakSelf = self;
    [cellWithRowOne actionClickMangerWithBlock:^(PDCenterTopUpItem *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(PDCenterTopUpItem *singleModel) = objc_getAssociatedObject(strongSelf, &actionClickWithBlockKey);
        if (block){
            block(singleModel);
        }
    }];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 5;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [PDChaosFightPagePaySingleCell calculationCellHeightWithModel:nil];
}


#pragma mark - popView
- (void)fadeIn {
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)fadeOut {
    [UIView animateWithDuration:.4 animations:^{
        self.transform = CGAffineTransformMakeScale(1, 1);
        self.alpha = 0.0;
        self.dismissControl.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.dismissControl removeFromSuperview];
            [self removeFromSuperview];
        }
    }];
}


-(void)viewShow{
    UIWindow *keywindow = [self lastWindow];
    [keywindow addSubview:self.dismissControl];
    [keywindow addSubview:self];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,  keywindow.bounds.size.height/2.0f);
    
    [self fadeIn];
}

- (UIWindow *)lastWindow {
    NSArray *windows = [UIApplication sharedApplication].windows;
    for(UIWindow *window in [windows reverseObjectEnumerator]) {
        
        if ([window isKindOfClass:[UIWindow class]] &&
            CGRectEqualToRect(window.bounds, [UIScreen mainScreen].bounds))
            
            return window;
    }
    
    return [UIApplication sharedApplication].keyWindow;
}



-(void)viewShowTopView:(UIView *)topView{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self.dismissControl];
    [[UIApplication sharedApplication].keyWindow insertSubview:self belowSubview:topView];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,  keywindow.bounds.size.height/2.0f);
    
    [self fadeIn];
}

- (void)viewDismiss {
    [self fadeOut];
}

-(void)pageSetting{
    self.layer.cornerRadius = 10.0f;
    self.clipsToBounds = true;
    self.userInteractionEnabled = YES;
}

-(void)createDismissBackgroundView{
    self.dismissControl = [[UIControl alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.dismissControl.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.5];
    [self.dismissControl addTarget:self action:@selector(viewDismiss) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - 获取充值列表数据
-(void)sendRequestTogetChongzhiList{
    __weak typeof(self) weakSelf = self;

    [[NetworkAdapter sharedAdapter] fetchWithPath:walletPayList requestParams:nil responseObjectClass:[PDCenterTopUpList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDCenterTopUpList *list = (PDCenterTopUpList *)responseObject;
        [strongSelf.payMutableArr addObjectsFromArray:list.items];
        [strongSelf.payTableView reloadData];
        
        strongSelf.shouchongLabel.text = [NSString stringWithFormat:@"首充多送 %li%% 金币",(long)(list.goldAwardPercentForFirstPay * 100)];
    }];
}

-(void)actionClickWithBlock:(void(^)(PDCenterTopUpItem *singleModel))block{
    objc_setAssociatedObject(self, &actionClickWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
