//
//  PDChaosFightPopChongzhiPayView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightPopChongzhiPayView.h"

static char actionClickToPayWithModelKey;

@interface PDChaosFightPopChongzhiPayView()
@property (nonatomic,strong)UIView *bgView;
@property (nonatomic,strong)PDImageView *imgView1;
@property (nonatomic,strong)PDImageView *imgView2;
@property (nonatomic,strong)PDImageView *imgView3;
@property (nonatomic,strong)UIControl *dismissControl;
@property (nonatomic,strong)UITableView *payTableView;

@property (nonatomic,strong)UIButton *alipayButton;
@property (nonatomic,strong)UIButton *wechatButton;
@property (nonatomic,strong)PDCenterTopUpItem *tempSingleModel;
@end

@implementation PDChaosFightPopChongzhiPayView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self pageSetting];
        [self createDismissBackgroundView];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.frame = self.bounds;
    self.bgView.userInteractionEnabled = YES;
    [self addSubview:self.bgView];
    
    // img1
    // 1.
    self.imgView1 = [[PDImageView alloc]init];
    self.imgView1.backgroundColor = [UIColor clearColor];
    self.imgView1.image = [UIImage imageNamed:@"icon_chaosFight_chongzhi_pay_top"];
    [self.bgView addSubview:self.imgView1];
    
    self.imgView2 = [[PDImageView alloc]init];
    self.imgView2.backgroundColor = [UIColor clearColor];
    self.imgView2.userInteractionEnabled = YES;
    [self.bgView addSubview:self.imgView2];
    
    self.imgView3 = [[PDImageView alloc]init];
    self.imgView3.backgroundColor = [UIColor clearColor];
    self.imgView3.image = [UIImage imageNamed:@"icon_chaosFight_chongzhi_pay_bottom"];
    [self.bgView addSubview:self.imgView3];
    
    // 1. 计算
    // 440 63
    CGFloat top_height = 66 * self.size_width / 260;
    self.imgView1.frame = CGRectMake(0, 0, self.size_width, top_height);
    
    // bottom
    CGFloat bottom_height = 30 * self.size_width / 260;
    self.imgView3.frame = CGRectMake(0, self.size_height - bottom_height, self.size_width, bottom_height);
    
    // normal
    self.imgView2.frame = CGRectMake(0, CGRectGetMaxY(self.imgView1.frame), self.size_width, self.imgView3.orgin_y - CGRectGetMaxY(self.imgView1.frame));
    self.imgView2.image = [UIImage imageNamed:@"icon_chaosFight_chongzhi_pay_normal"];
 
    // alipay
    
    CGFloat margin = (self.imgView2.size_height - 2 * 45) / 3.;
    
    __weak typeof(self)weakSelf = self;
    self.alipayButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.alipayButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_chongzhi_pay_alipay"] forState:UIControlStateNormal];
    self.alipayButton.frame = CGRectMake((self.size_width - 111) / 2., margin, 111, 45);
    [self.alipayButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempSingleModel.payType = 1;
        void(^block)(PDCenterTopUpItem *singleModel) = objc_getAssociatedObject(strongSelf, &actionClickToPayWithModelKey);
        if (block){
            block(strongSelf.tempSingleModel);
        }
    }];
    [self.imgView2 addSubview:self.alipayButton];
    
    self.wechatButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.wechatButton setBackgroundImage:[UIImage imageNamed:@"icon_chaosFight_chongzhi_pay_wechat"] forState:UIControlStateNormal];
    self.wechatButton.frame = CGRectMake(self.alipayButton.orgin_x, CGRectGetMaxY(self.alipayButton.frame) + margin, self.alipayButton.size_width, self.alipayButton.size_height);
    [self.wechatButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.tempSingleModel.payType = 2;
        void(^block)(PDCenterTopUpItem *singleModel) = objc_getAssociatedObject(strongSelf, &actionClickToPayWithModelKey);
        if (block){
            block(strongSelf.tempSingleModel);
        }
    }];
    [self.imgView2 addSubview:self.wechatButton];
}

#pragma mark - popView
- (void)fadeIn {
    self.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.alpha = 0;
    [UIView animateWithDuration:.35 animations:^{
        self.alpha = 1;
        self.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)fadeOut {
    [UIView animateWithDuration:.4 animations:^{
        self.transform = CGAffineTransformMakeScale(1, 1);
        self.alpha = 0.0;
        self.dismissControl.backgroundColor = [UIColor clearColor];
    } completion:^(BOOL finished) {
        if (finished) {
            [self.dismissControl removeFromSuperview];
            [self removeFromSuperview];
        }
    }];
}


-(void)viewShowWithModel:(PDCenterTopUpItem *)singleModel{
    _tempSingleModel = singleModel;
    
    UIWindow *keywindow = [self lastWindow];
    [keywindow addSubview:self.dismissControl];
    [keywindow addSubview:self];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,  keywindow.bounds.size.height/2.0f);
    
    [self fadeIn];
}

- (UIWindow *)lastWindow {
    NSArray *windows = [UIApplication sharedApplication].windows;
    for(UIWindow *window in [windows reverseObjectEnumerator]) {
        
        if ([window isKindOfClass:[UIWindow class]] &&
            CGRectEqualToRect(window.bounds, [UIScreen mainScreen].bounds))
            
            return window;
    }
    
    return [UIApplication sharedApplication].keyWindow;
}



-(void)viewShowTopView:(UIView *)topView{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [keywindow addSubview:self.dismissControl];
    [[UIApplication sharedApplication].keyWindow insertSubview:self belowSubview:topView];
    
    self.center = CGPointMake(keywindow.bounds.size.width/2.0f,  keywindow.bounds.size.height/2.0f);
    
    [self fadeIn];
}

- (void)viewDismiss {
    [self fadeOut];
}

-(void)pageSetting{
    self.layer.cornerRadius = 10.0f;
    self.clipsToBounds = true;
    self.userInteractionEnabled = YES;
}

-(void)createDismissBackgroundView{
    self.dismissControl = [[UIControl alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.dismissControl.backgroundColor = [UIColor colorWithRed:.16 green:.17 blue:.21 alpha:.5];
    [self.dismissControl addTarget:self action:@selector(viewDismiss) forControlEvents:UIControlEventTouchUpInside];
}

-(void)actionClickToPayWithModel:(void(^)(PDCenterTopUpItem *singleModel))block{
    objc_setAssociatedObject(self, &actionClickToPayWithModelKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
