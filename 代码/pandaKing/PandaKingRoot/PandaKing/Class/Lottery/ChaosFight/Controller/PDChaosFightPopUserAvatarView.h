//
//  PDChaosFightPopUserAvatarView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/2.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDChaosFightMembersSingleModel.h"

@interface PDChaosFightPopUserAvatarView : UIView

-(void)viewShowWithAvatar:(PDChaosFightMembersSingleModel *)singleModel;
-(void)viewDismiss;

@end
