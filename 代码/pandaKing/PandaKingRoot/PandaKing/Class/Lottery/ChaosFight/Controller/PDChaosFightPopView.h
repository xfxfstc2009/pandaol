//
//  PDChaosFightPopView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/17.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,dayeAlertType) {
    dayeAlertTypeGuize = 1,                         /**< 打野规则*/
    dayeAlertTypeTongji = 2,                        /**< 打野统计*/
};

@interface PDChaosFightPopView : UIView

@property (nonatomic,assign)dayeAlertType transferDayeType;


-(void)viewShow;
-(void)viewDismiss;
-(void)viewShowTopView:(UIView *)topView;
-(void)showChaosViewWithType:(dayeAlertType)type;               // 传递类型
-(void)showChaosViewWithTopView:(UIView *)topView;
// 显示充值
-(void)showNoYueToChongzhiWithBlock:(void(^)())block;
#pragma mark - 显示到达上线方法
-(void)showShangxianWithBlock:(void(^)())block;
#pragma mark - 被踢出
-(void)showTichuWithBlock:(void(^)())block;

@end
