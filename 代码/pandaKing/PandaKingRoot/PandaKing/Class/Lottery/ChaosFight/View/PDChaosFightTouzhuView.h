//
//  PDChaosFightTouzhuView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,chaosTouzhuType) {
    chaosTouzhuTypeShuangsha = 1,                   /**< 双杀*/
    chaosTouzhuTypeXiaolong = 2,                    /**< 小龙*/
    chaosTouzhuTypeDalong = 3,                      /**< 大龙*/
    chaosTouzhuTypeShuangkong = 4,                  /**< 双空*/
    chaosTouzhuTypeYizhi = 5,                       /**< 一只*/
    chaosTouzhuTypeLiangzhi = 6,                    /**< 两只*/
    chaosTouzhuTypeSanzhi = 7,                      /**< 三只*/
    chaosTouzhuTypeSizhi = 8,                       /**< 四只*/
    chaosTouzhuTypeTuoersuo = 9,                    /**< 托儿所*/
    chaosTouzhuTypePingshou = 10,                   /**< 平手*/
    chaosTouzhuTypeertongjie = 11,                  /**< 儿童节*/
    chaosTouzhuTypeBlackDragonet = 12,              /**< Dota2 黑龙*/
    chaosTouzhuTypeMeatMountain = 13,               /**< Dota2 肉山*/
    chaosTouzhuTypeTyrant = 14,                     /**< King 暴君*/
    chaosTouzhuTypeMaster = 15,                     /**< King 主宰*/
    chaosTouzhuTypeXiahoudun = 16,                  /**< 夏侯惇*/
    chaosTouzhuTypeChengyaojin = 17,                /**< 程咬金*/
    chaosTouzhuTypeLangRen = 18,                    /**< 狼人*/
    chaosTouzhuTypeMori = 19,                       /**< 末日*/
};


@interface PDChaosFightTouzhuView : UIView

@property (nonatomic,assign)chaosTouzhuType transferType;
-(void)actionClickManagerWithGold:(NSInteger)gold callback:(void(^)())block;
-(void)auSetupFrame;

@property (nonatomic,assign)NSInteger currentCountPrice;
@property (nonatomic,assign)NSInteger currentMyPrice;
@property (nonatomic,strong)UILabel *countPriceLabel;
@property (nonatomic,strong)UILabel *myTouzhuLabel;

-(void)dismissKeyWindowManager;
-(void)showWinManagerWithBlock:(void(^)())block;                        /**< 显示胜利牌子*/
-(void)dismissWinManagerBlock:(void(^)())block;                         /**< 消失胜利牌子*/

#pragma mark - 移除到window
-(void)removeFromWindow;
-(void)outMainSettingFrame:(CGRect)frame;

-(void)changePeilv:(NSString *)str;
@end
