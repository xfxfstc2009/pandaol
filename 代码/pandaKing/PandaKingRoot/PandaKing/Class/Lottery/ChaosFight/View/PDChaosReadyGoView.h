//
//  PDChaosReadyGoView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDChaosReadyGoView : UIView

-(void)startAnimation;

@end
