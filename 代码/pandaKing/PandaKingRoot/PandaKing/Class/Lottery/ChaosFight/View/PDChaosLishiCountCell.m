//
//  PDChaosLishiCountCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosLishiCountCell.h"

@interface PDChaosLishiCountCell()
@property (nonatomic,strong)PDImageView *bgView;
@property (nonatomic,strong)UILabel *qihaoLabel;                /**< 期号*/
@property (nonatomic,strong)UILabel *shengLabel;                /**< 胜利人*/
@property (nonatomic,strong)UILabel *tuoersuoLabel;             /**< 托儿所*/
@property (nonatomic,strong)UILabel *ertongjieLabel;            /**< 儿童节*/

@end

@implementation PDChaosLishiCountCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

#pragma createView
-(void)createView{
    self.bgView = [[PDImageView alloc]init];
    [self addSubview:self.bgView];
    self.backgroundView = self.bgView;
    
    
    self.qihaoLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.qihaoLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    self.qihaoLabel.font = [UIFont systemFontOfSize:11.];
    [self addSubview:self.qihaoLabel];
    
    self.shengLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.shengLabel.font = [UIFont systemFontOfSize:11.];
    self.shengLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.shengLabel];
    
    self.tuoersuoLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.tuoersuoLabel.font = [UIFont systemFontOfSize:11.];
    self.tuoersuoLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.tuoersuoLabel];
    
    self.ertongjieLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.ertongjieLabel.font = [UIFont systemFontOfSize:11.];
    self.ertongjieLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.ertongjieLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferCellWidth:(CGFloat)transferCellWidth{
    _transferCellWidth = transferCellWidth;
}

-(void)setTransferIndex:(NSInteger)transferIndex{
    _transferIndex = transferIndex;
}

-(void)setTransferHistorySingleModel:(PDChaosFightHistorySingleModel *)transferHistorySingleModel{
    _transferHistorySingleModel = transferHistorySingleModel;
    
    self.bgView.frame = CGRectMake(0, 0, self.transferCellWidth, self.transferCellHeight);
    if (self.transferIndex == 0){
        self.bgView.image = [Tool stretchImageWithName:@"icon_chaosFight_alert_history_top"];
    } else if (self.transferIndex == -1){
        self.bgView.image = [Tool stretchImageWithName:@"icon_chaosFight_alert_history_bottom"];
    } else {
        self.bgView.image = [Tool stretchImageWithName:@"icon_chaosFight_alert_history_normal"];
    }
    
    CGFloat width = (self.transferCellWidth - 5 * 11) / 4.;
    self.qihaoLabel.text = [NSString stringWithFormat:@"%@期",transferHistorySingleModel.num];

    self.qihaoLabel.frame = CGRectMake(11, 0, width, self.transferCellHeight);
    
    //
    self.shengLabel.text = [NSString stringWithFormat:@"%@",transferHistorySingleModel.winItemName];
    self.shengLabel.frame = CGRectMake(CGRectGetMaxX(self.qihaoLabel.frame), 0, width, self.transferCellHeight);
    
    //  tuoersuo
    NSString *title1 = @"";
    for (NSString *str in transferHistorySingleModel.winningAnyoneGuessItems){
        title1 = [title1 stringByAppendingString:[NSString stringWithFormat:@"%@  ",str]];
    }
    
    self.tuoersuoLabel.text = title1;
//    [NSString stringWithFormat:@"托儿索(%@ - %@)",[transferHistorySingleModel.pointAResultItems objectAtIndex:0],[transferHistorySingleModel.pointAResultItems objectAtIndex:1]];
    self.tuoersuoLabel.frame = CGRectMake(CGRectGetMaxX(self.shengLabel.frame), 0, width, self.transferCellHeight);
    
    // 儿童节
    
    NSString *title2 = @"";
    for (NSString *str2 in transferHistorySingleModel.winningOverallGuessItems){
         title2 = [title2 stringByAppendingString:[NSString stringWithFormat:@"%@  ",str2]];
    }
    
    self.ertongjieLabel.text = title2;
//    [NSString stringWithFormat:@"儿童劫(%@ - %@)",[transferHistorySingleModel.pointBResultItems objectAtIndex:0],[transferHistorySingleModel.pointBResultItems objectAtIndex:1]];
    self.ertongjieLabel.frame = CGRectMake(CGRectGetMaxX(self.tuoersuoLabel.frame), 0, width , self.transferCellHeight);
    
    
    
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += LCFloat(11);
    return cellHeight;
}


@end
