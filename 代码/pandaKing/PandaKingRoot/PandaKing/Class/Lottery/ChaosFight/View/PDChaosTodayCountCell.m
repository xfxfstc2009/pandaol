//
//  PDChaosTodayCountCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosTodayCountCell.h"

@interface PDChaosTodayCountCell()
@property (nonatomic,strong)PDImageView *bgView;
@property (nonatomic,strong)UILabel *tuoersuoLabel;             /**< 托儿所胜*/
@property (nonatomic,strong)UILabel *pingjuLabel;               /**< 平局*/
@property (nonatomic,strong)UILabel *ertongjieLabel;            /**< 儿童劫胜*/
@property (nonatomic,strong)UILabel *shuangshaLabel;            /**< 托儿所胜*/
@property (nonatomic,strong)UILabel *xiaolongLabel;             /**< 双杀*/
@property (nonatomic,strong)UILabel *yizhiLabel;                /**< 1只*/
@property (nonatomic,strong)UILabel *erzhiLabel;                /**< 2只*/
@property (nonatomic,strong)UILabel *sanzhiLabel;               /**< 3只*/
@property (nonatomic,strong)UILabel *sizhiLabel;                /**< 4只*/
@property (nonatomic,strong)UILabel *dalongLabel;               /**< 大龙*/
@property (nonatomic,strong)UILabel *shuangkongLabel;           /**< 双空*/
@end

@implementation PDChaosTodayCountCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.backgroundColor = [UIColor clearColor];
    self.bgView = [[PDImageView alloc]init];
    self.bgView.image = [Tool stretchImageWithName:@"icon_chaosFight_alert_info_background"];
    self.backgroundView = self.bgView;
    
    // 1. 托儿所
    self.tuoersuoLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.tuoersuoLabel.font = [UIFont systemFontOfSize:11.];
    self.tuoersuoLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.tuoersuoLabel];
    // 2. 平局
    self.pingjuLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.pingjuLabel.font = [UIFont systemFontOfSize:11.];
    self.pingjuLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.pingjuLabel];
    // 3. 儿童劫
    self.ertongjieLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.ertongjieLabel.font = [UIFont systemFontOfSize:11.];
    self.ertongjieLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.ertongjieLabel];
    // 4. 创建双杀
    self.shuangkongLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.shuangkongLabel.font = [UIFont systemFontOfSize:11.];
    self.shuangkongLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.shuangkongLabel];
    // 5. 创建小龙
    self.xiaolongLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.xiaolongLabel.font = [UIFont systemFontOfSize:11.];
    self.xiaolongLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.xiaolongLabel];
    // 6. 创建1只
    self.yizhiLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.yizhiLabel.font = [UIFont systemFontOfSize:11.];
    self.yizhiLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.yizhiLabel];
    //7. 创建2只
    self.erzhiLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.erzhiLabel.font = [UIFont systemFontOfSize:11.];
    self.erzhiLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.erzhiLabel];
    //8. 创建3只
    self.sanzhiLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.sanzhiLabel.font = [UIFont systemFontOfSize:11.];
    self.sanzhiLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.sanzhiLabel];
    //9. 创建4只
    self.sizhiLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.sizhiLabel.font = [UIFont systemFontOfSize:11.];
    self.sizhiLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.sizhiLabel];
    //10.创建大龙
    self.dalongLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.dalongLabel.font = [UIFont systemFontOfSize:11.];
    self.dalongLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.dalongLabel];
    
    self.shuangshaLabel = [PDViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.shuangshaLabel.font = [UIFont systemFontOfSize:11.];
    self.shuangshaLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
    [self addSubview:self.shuangshaLabel];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferCellWidth:(CGFloat)transferCellWidth{
    _transferCellWidth = transferCellWidth;
}

-(void)setTransferSingleModel:(PDChaosFightTodayCountRootModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    CGFloat widthOne = (self.transferCellWidth - 4 * 11) / 3.;
    if([AccountModel sharedAccountModel].gameType == GameTypePVP){
        self.tuoersuoLabel.text = [NSString stringWithFormat:@"夏侯惇胜:%li",transferSingleModel.resultToCount.pointAWin];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        self.tuoersuoLabel.text = [NSString stringWithFormat:@"托儿索胜:%li",transferSingleModel.resultToCount.pointAWin];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        self.tuoersuoLabel.text = [NSString stringWithFormat:@"狼人胜:%li",transferSingleModel.resultToCount.pointAWin];
    }
    
    self.tuoersuoLabel.frame = CGRectMake(11, 11, widthOne, [NSString contentofHeightWithFont:self.tuoersuoLabel.font]);
    
    // 2. 平局
    self.pingjuLabel.text = [NSString stringWithFormat:@"平局:%li",transferSingleModel.resultToCount.tie];
    self.pingjuLabel.frame = CGRectMake(CGRectGetMaxX(self.tuoersuoLabel.frame) + 11, self.tuoersuoLabel.orgin_y, widthOne, self.tuoersuoLabel.size_height);
    
    // 3. 儿童节胜利
    
    
    if([AccountModel sharedAccountModel].gameType == GameTypePVP){
        self.ertongjieLabel.text = [NSString stringWithFormat:@"程咬金胜:%li",transferSingleModel.resultToCount.pointBWin];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        self.ertongjieLabel.text = [NSString stringWithFormat:@"儿童劫胜:%li",transferSingleModel.resultToCount.pointBWin];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        self.ertongjieLabel.text = [NSString stringWithFormat:@"末日胜:%li",transferSingleModel.resultToCount.pointBWin];
    }
    
    self.ertongjieLabel.frame = CGRectMake(CGRectGetMaxX(self.pingjuLabel.frame) + 11, self.tuoersuoLabel.orgin_y, widthOne, self.tuoersuoLabel.size_height);
    
    // 4. 双杀
    CGFloat widthTwo = (self.transferCellWidth - 5 * 11) / 4.;
    self.shuangshaLabel.text = [NSString stringWithFormat:@"双杀:%li",transferSingleModel.resultToCount.anyoneDoubleKill];
    self.shuangshaLabel.frame = CGRectMake(11, CGRectGetMaxY(self.tuoersuoLabel.frame) + 11, widthTwo,self.tuoersuoLabel.size_height);
    
    // 5. 小龙
    if([AccountModel sharedAccountModel].gameType == GameTypePVP){
        self.xiaolongLabel.text = [NSString stringWithFormat:@"暴君:%li",transferSingleModel.resultToCount.anyoneMaster];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        self.xiaolongLabel.text = [NSString stringWithFormat:@"小龙:%li",transferSingleModel.resultToCount.anyoneDragonet];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        self.xiaolongLabel.text = [NSString stringWithFormat:@"黑龙:%li",transferSingleModel.resultToCount.anyoneBlackDragon];
    }
    
    self.xiaolongLabel.frame = CGRectMake(CGRectGetMaxX(self.shuangshaLabel.frame) + 11, self.shuangshaLabel.orgin_y, widthTwo, self.shuangshaLabel.size_height);
    
    // 6. 一只
    self.yizhiLabel.text = [NSString stringWithFormat:@"1只:%li",transferSingleModel.resultToCount.overallOne];
    self.yizhiLabel.frame = CGRectMake(CGRectGetMaxX(self.xiaolongLabel.frame) + 11, self.shuangshaLabel.orgin_y, widthTwo, self.shuangshaLabel.size_height);
    
    // 7. 2只
    self.erzhiLabel.text = [NSString stringWithFormat:@"2只:%li",transferSingleModel.resultToCount.overallTwo];
    self.erzhiLabel.frame = CGRectMake(CGRectGetMaxX(self.yizhiLabel.frame) + 11, self.shuangshaLabel.orgin_y, widthTwo, self.shuangshaLabel.size_height);
    
    // 8. 大龙
    // 5. 小龙
    if([AccountModel sharedAccountModel].gameType == GameTypePVP){
        self.dalongLabel.text = [NSString stringWithFormat:@"主宰:%li",transferSingleModel.resultToCount.anyoneMaster];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
        self.dalongLabel.text = [NSString stringWithFormat:@"大龙:%li",transferSingleModel.resultToCount.anyoneDragon];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
        self.dalongLabel.text = [NSString stringWithFormat:@"肉山:%li",transferSingleModel.resultToCount.anyoneMeatMountain];
    }
    
    self.dalongLabel.frame = CGRectMake(11, CGRectGetMaxY(self.shuangshaLabel.frame) + 11, widthTwo, self.shuangshaLabel.size_height);
    
    // 9. 双空
    self.shuangkongLabel.text = [NSString stringWithFormat:@"双空:%li",transferSingleModel.resultToCount.anyoneDoubleNull];
    self.shuangkongLabel.frame = CGRectMake(CGRectGetMaxX(self.dalongLabel.frame) + 11, self.dalongLabel.orgin_y, widthTwo, self.dalongLabel.size_height);
    
    // 10. 3 只
    self.sanzhiLabel.text = [NSString stringWithFormat:@"3只:%li",transferSingleModel.resultToCount.overallThree];
    self.sanzhiLabel.frame = CGRectMake(CGRectGetMaxX(self.shuangkongLabel.frame) + 11, self.shuangkongLabel.orgin_y, widthTwo, self.shuangkongLabel.size_height);
    
    self.sizhiLabel.text = [NSString stringWithFormat:@"4只:%li",transferSingleModel.resultToCount.overallFour];
    self.sizhiLabel.frame = CGRectMake(CGRectGetMaxX(self.sanzhiLabel.frame) + 11, self.sanzhiLabel.orgin_y, self.sanzhiLabel.size_width, self.sanzhiLabel.size_height);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += 11 * 4;
    cellHeight += [NSString contentofHeightWithFont:[UIFont systemFontOfSize:11.]] * 3;
    return cellHeight;
}




@end
