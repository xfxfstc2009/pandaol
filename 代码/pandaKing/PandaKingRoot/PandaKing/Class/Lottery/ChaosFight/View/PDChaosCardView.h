//
//  PDChaosCardView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,PDChaosCardViewType) {
    PDChaosCardViewTypeMiss,
    PDChaosCardViewTypeKill,
};

@interface PDChaosCardView : UIView

@property (nonatomic,strong)UIImage *transferImg;
@property (nonatomic,assign)CGRect transferRect;

-(void)animationWithType:(PDChaosCardViewType)type block:(void(^)())block;

-(void)removeView;

@end
