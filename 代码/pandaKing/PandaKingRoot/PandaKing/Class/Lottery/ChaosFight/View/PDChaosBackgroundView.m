//
//  PDChaosBackgroundView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosBackgroundView.h"
#import "PDChaosCardView.h"
#import "PDHeroView.h"
#import "PDChaosVSView.h"
#import "ZCAudioTool.h"

@interface PDChaosBackgroundView()
@property (nonatomic,strong)PDImageView *backgroundView;
// 卡片1
@property (nonatomic,strong)PDChaosCardView *leftCard1;
@property (nonatomic,strong)PDChaosCardView *leftCard2;
@property (nonatomic,strong)PDChaosCardView *rightCard1;
@property (nonatomic,strong)PDChaosCardView *rightCard2;
@property (nonatomic,strong)PDHeroView *leftHeroImgView;
@property (nonatomic,strong)PDHeroView *rightHeroImgView;
@property (nonatomic,strong)PDChaosVSView *vsImgView;

@property (nonatomic,strong)    ZCAudioTool *audioTool;
@end

@implementation PDChaosBackgroundView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.backgroundView = [[PDImageView alloc]init];
    self.backgroundView.frame = CGRectMake(0, (kScreenBounds.size.height - 189)/2., kScreenBounds.size.width, 189);
    self.backgroundView.image = [Tool stretchImageWithName:@"icon_chaosFight_animation_line"];
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    [keyWindow addSubview:self.backgroundView];
    
    self.audioTool = [[ZCAudioTool alloc]init];
}

#pragma mark - animation
-(void)startAnimationWithFinish:(void(^)())block{
    self.backgroundView.alpha = 0;
    [UIView animateWithDuration:.2f animations:^{
        self.backgroundView.alpha = 1;
    } completion:^(BOOL finished) {
        [self challengeGuaishouAnimation1Block:^{
            if (block){
                block();
            }
        }];
    }];
}


-(void)challengeGuaishouAnimation1Block:(void(^)())block{
    // 左边的英雄
    if (!self.leftHeroImgView){
        self.leftHeroImgView = [[PDHeroView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
        self.leftHeroImgView.userInteractionEnabled = NO;
        if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
            self.leftHeroImgView.transferType = heroTypeYasuo;
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
            self.leftHeroImgView.transferType = heroTypeXiahoudun;
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
            self.leftHeroImgView.transferType = heroTypeLangren;
        }
        
        [self addSubview:self.leftHeroImgView];
    }
    __weak typeof(self)weakSelf = self;
    [self.leftHeroImgView starAnimationWithType:self.leftHeroImgView.transferType finished:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.leftHeroImgView dismissAnimationWithType:strongSelf.leftHeroImgView.transferType];
    }];
    
    
    // 添加VS
    if (!self.vsImgView){
        self.vsImgView = [[PDChaosVSView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
        self.vsImgView.userInteractionEnabled = NO;
        [self addSubview:self.vsImgView];
    }
    [self.vsImgView startAnimationFinish:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
    
    // 右边的英雄
    if (!self.rightHeroImgView){
        self.rightHeroImgView = [[PDHeroView alloc]initWithFrame:CGRectMake(0, 0, 100, 100)];
        self.rightHeroImgView.userInteractionEnabled = NO;
        if ([AccountModel sharedAccountModel].gameType == GameTypeLoL){
            self.rightHeroImgView.transferType = heroTypeJie;
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP){
            self.rightHeroImgView.transferType = heroTypeChengyaojin;
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2){
            self.rightHeroImgView.transferType = heroTypeMori;
        }
        [self addSubview:self.rightHeroImgView];
    }
    [self.rightHeroImgView starAnimationWithType:self.rightHeroImgView.transferType finished:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.rightHeroImgView dismissAnimationWithType:strongSelf.rightHeroImgView.transferType];
    }];
    
    [self.audioTool playAudioWithAudioName:@"mic_gameStart.wav"];
    
    // 背景
    [UIView animateWithDuration:.7f animations:^{
        self.backgroundView.alpha = 0;
    } completion:^(BOOL finished) {
        
    }];
}


#pragma mark - 移除方法
-(void)dismissManager{
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    for (UIView *view in keyWindow.subviews) {
        if ([view isKindOfClass:[PDImageView class]]){
            [view removeFromSuperview];
        }
    }
    if (self.vsImgView){
        [self.vsImgView dismissManager];
        self.vsImgView = nil;
    }
    
}

@end
