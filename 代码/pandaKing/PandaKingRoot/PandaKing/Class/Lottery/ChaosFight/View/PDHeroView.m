//
//  PDHeroView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDHeroView.h"

@interface PDHeroView()
@property (nonatomic,strong)PDImageView *heroImgView;

@end

@implementation PDHeroView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    if (!self.heroImgView){
        self.heroImgView = [[PDImageView alloc]init];
        self.heroImgView.frame = self.bounds;
        self.heroImgView.backgroundColor = [UIColor clearColor];
        self.heroImgView.alpha = 0;
        UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
        [keyWindow addSubview:self.heroImgView];
    }
}

-(void)setTransferType:(heroType)transferType{
    _transferType = transferType;
    [self lazyUploadType:transferType];
}

-(void)lazyUploadType:(heroType)transferType{
    if (transferType == heroTypeJie){
        self.heroImgView.image = [UIImage imageNamed:@"icon_chaosFight_animation_jie"];
        self.heroImgView.frame = CGRectMake(- 245, (kScreenBounds.size.height - 175) / 2., 245, 175);
    } else if (transferType == heroTypeYasuo){
        self.heroImgView.image = [UIImage imageNamed:@"icon_chaosFight_animation_yasuo"];
        self.heroImgView.frame = CGRectMake(kScreenBounds.size.width + 245, (kScreenBounds.size.height - 175) / 2., 245, 175);
    } else if (transferType == heroTypeChengyaojin){        // 程咬金
        self.heroImgView.image = [UIImage imageNamed:@"icon_chaosFight_animation_chengyaojin"];
        self.heroImgView.frame = CGRectMake(- 245, (kScreenBounds.size.height - 175) / 2., 245, 175);
    } else if (transferType == heroTypeXiahoudun){          // 夏侯惇
        self.heroImgView.image = [UIImage imageNamed:@"icon_chaosFight_animation_xiahoudun"];
        self.heroImgView.frame = CGRectMake(kScreenBounds.size.width + 245, (kScreenBounds.size.height - 175) / 2., 245, 175);
    } else if (transferType == heroTypeMori){               // 末日
        self.heroImgView.image = [UIImage imageNamed:@"icon_chaosFight_animation_mori"];
        self.heroImgView.frame = CGRectMake(- 245, (kScreenBounds.size.height - 175) / 2., 245, 175);
    } else if (transferType == heroTypeLangren){            // 狼人
        self.heroImgView.image = [UIImage imageNamed:@"icon_chaosFight_animation_langren"];
        self.heroImgView.frame = CGRectMake(kScreenBounds.size.width + 245, (kScreenBounds.size.height - 175) / 2., 245, 175);
    }
}

-(void)starAnimationWithType:(heroType)heroType finished:(void(^)())block{
    // 1. 懒加载
    [self createView];
    // 2.1
    [self lazyUploadType:heroType];
    // 2. animation
    [UIView animateWithDuration:.3f animations:^{
        self.heroImgView.alpha = 1;
        if (self.transferType == heroTypeJie){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. + 92 + 50;
        } else if (self.transferType == heroTypeYasuo){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. - 50 - 245;
        } else if (self.transferType == heroTypeXiahoudun){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. - 50 - 245;
        } else if (self.transferType == heroTypeChengyaojin){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. + 92 + 50;
        } else if (self.transferType == heroTypeLangren){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. - 50 - 245;
        } else if (self.transferType == heroTypeMori){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. + 92 + 50;
        }
    } completion:^(BOOL finished) {
        if (block){
            block();
        }
    }];
}

-(void)dismissAnimationWithType:(heroType)heroType{
    [UIView animateWithDuration:1.f animations:^{
        self.heroImgView.alpha = .7f;
        if (self.transferType == heroTypeJie){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. + 92 + 150;
        } else if (self.transferType == heroTypeYasuo){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. - 50 - 345;
        } else if (self.transferType == heroTypeXiahoudun){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. - 50 - 345;
        } else if (self.transferType == heroTypeChengyaojin){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. + 92 + 150;
        } else if (self.transferType == heroTypeLangren){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. - 50 - 345;
        } else if (self.transferType == heroTypeMori){
            self.heroImgView.orgin_x = (kScreenBounds.size.width - 92) / 2. + 92 + 150;
        }

    } completion:^(BOOL finished) {
        [self dismissAnimationWithType1:heroType];
    }];
}

-(void)dismissAnimationWithType1:(heroType)heroType{
    [UIView animateWithDuration:.7f animations:^{
        self.heroImgView.alpha = 0;
        if (self.transferType == heroTypeJie){
            self.heroImgView.orgin_x = - 245;
        } else if (self.transferType == heroTypeYasuo){
            self.heroImgView.orgin_x = kScreenBounds.size.width + 245;
        } else if (self.transferType == heroTypeXiahoudun){
            self.heroImgView.orgin_x = kScreenBounds.size.width + 245;
        } else if (self.transferType == heroTypeChengyaojin){
            self.heroImgView.orgin_x = - 245;
        } else if (self.transferType == heroTypeLangren){
            self.heroImgView.orgin_x = kScreenBounds.size.width + 245;
        } else if (self.transferType == heroTypeMori){
            self.heroImgView.orgin_x = - 245;
        }
    } completion:^(BOOL finished) {
        [self.heroImgView removeFromSuperview];
        self.heroImgView = nil;
    }];
}

-(void)removeView{
    if (self.heroImgView){
        [self.heroImgView removeFromSuperview];
    }
}
@end
