//
//  PDChaosVSView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDChaosVSView : UIView
@property (nonatomic,strong)PDImageView *vsImgView;

-(void)startAnimationFinish:(void(^)())block;
-(void)dismissAnimation:(void(^)())block;
-(void)dismissManager;
@end
