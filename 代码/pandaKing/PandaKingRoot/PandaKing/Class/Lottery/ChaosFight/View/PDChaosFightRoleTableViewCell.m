//
//  PDChaosFightRoleTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightRoleTableViewCell.h"
#import "PDCoinView.h"
#import "PDChaosFightWinLabel.h"

@interface PDChaosFightRoleTableViewCell()
@property (nonatomic,strong)PDImageView *bgImgView;

// view1
@property (nonatomic,strong)PDImageView *firstView;
@property (nonatomic,strong)PDImageView *avatarImgView;
@property (nonatomic,strong)UILabel *moneyLabel;

// view2;
@property (nonatomic,strong)PDImageView *secView;
@property (nonatomic,strong)PDImageView *norAvatarImgView;

@property (nonatomic,strong)PDCoinView *coinView;

@property (nonatomic,strong)PDChaosFightWinLabel *winGoldLabel;

//@property (nonatomic,strong)PDChaosFightMembersSingleModel *tempSingleModel;

@end

@implementation PDChaosFightRoleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.bgImgView];
    
    
    // 【view1】
    self.firstView = [[PDImageView alloc]init];
    [self.bgImgView addSubview:self.firstView];
    
    // 2. img
    self.avatarImgView = [[PDImageView alloc]init];
    [self.firstView addSubview:self.avatarImgView];
    
    self.moneyLabel = [GWViewTool createLabelFont:@"小标题" textColor:@"金"];
    self.moneyLabel.textAlignment = NSTextAlignmentCenter;
    self.moneyLabel.textColor = c26;
    self.moneyLabel.backgroundColor = [UIColor clearColor];
    self.moneyLabel.font = [[UIFont systemFontOfSize:14.]boldFont];
    self.moneyLabel.adjustsFontSizeToFitWidth = YES;
    [self.firstView addSubview:self.moneyLabel];

    // 【view2】
    self.secView = [[PDImageView alloc]init];
    [self.bgImgView addSubview:self.secView];
    
    self.winGoldLabel = [[PDChaosFightWinLabel alloc]init];
    self.winGoldLabel.textColor = [UIColor whiteColor];
    self.winGoldLabel.layer.shadowColor = [UIColor hexChangeFloat:@"7b3c0a"].CGColor;
    self.winGoldLabel.layer.shadowOffset = CGSizeMake(3,2.);
    self.winGoldLabel.layer.shadowOpacity = 0.6;
    self.winGoldLabel.layer.shadowRadius = 1.0;
    self.winGoldLabel.font = [[UIFont systemFontOfSize:15.]boldFont];
    [self addSubview:self.winGoldLabel];
    
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferCellWidth:(CGFloat)transferCellWidth{
    _transferCellWidth = transferCellWidth;
    
    // 1.bgimgView
    self.bgImgView.frame = CGRectMake(0, 0, self.transferCellWidth, self.transferCellHeight);
    
    // 2.
    self.firstView.image = [Tool stretchImageWithName:@"icon_chaosFight_role_bottom"];
    self.firstView.frame = self.bgImgView.bounds;
    
    // 3.
    self.secView.image = [UIImage imageNamed:@"icon_chaosFight_other_avatar"];
    self.secView.frame = self.bgImgView.bounds;
}


-(void)modelInfoWithModel:(PDChaosFightMembersSingleModel *)transferSingleModel{
    self.avatarImgView.center_x = self.bgImgView.size_width / 2.;
    
    // label
    self.moneyLabel.text = @"";
    
    // 1. 计算高度
    CGFloat avatarHeight = self.transferCellHeight * .7f;
    CGFloat avatarMainHeight = MIN(avatarHeight, self.transferCellWidth) - 2 * 5;
    CGFloat margin_x = (self.transferCellWidth - avatarMainHeight ) / 2.;
    self.avatarImgView.frame = CGRectMake(margin_x, 5, avatarMainHeight, avatarMainHeight);
    
    self.moneyLabel.frame = CGRectMake(5, self.transferCellHeight * .7f, self.transferCellWidth - 2 * 5, self.transferCellHeight * .3f - 5);
    self.moneyLabel.backgroundColor = [UIColor clearColor];
    
    if (transferSingleModel.memberId.length){
        // 1. 金钱
//        if (transferSingleModel.gold > 0){
            self.moneyLabel.text = [NSString stringWithFormat:@"%@",[Tool transferWithInfoNumber:transferSingleModel.gold]];
//        } else {
//            self.moneyLabel.text = @"0";
//        }
        
        // 2.头像
        if (transferSingleModel.avatar.length){
            [self.avatarImgView uploadImageWithURL:transferSingleModel.avatar placeholder:nil callback:NULL];
        }
    }
    
    if (!self.coinView){
        self.coinView = [[PDCoinView alloc]initWithPrimaryView:self.secView andSecondaryView:self.firstView inFrame:self.bgImgView.bounds];
        self.coinView.backgroundColor = [UIColor clearColor];
        [self.bgImgView addSubview:self.coinView];
    }

}

-(void)setTransferSingleModel:(PDChaosFightMembersSingleModel *)transferSingleModel{
     self.moneyLabel.text = [NSString stringWithFormat:@"%@",[Tool transferWithInfoNumber:transferSingleModel.gold]];
    [self modelInfoWithModel:transferSingleModel];
}


-(void)winWithAddGoldAnimation:(NSInteger)coin{
    if (!self.winGoldLabel){
        self.winGoldLabel = [[PDChaosFightWinLabel alloc]init];
        self.winGoldLabel.textColor = [UIColor whiteColor];
        self.winGoldLabel.font = [[UIFont systemFontOfSize:15.]boldFont];
        self.winGoldLabel.layer.shadowColor = [UIColor hexChangeFloat:@"7b3c0a"].CGColor;
        self.winGoldLabel.layer.shadowOffset = CGSizeMake(3,2.);
        self.winGoldLabel.layer.shadowOpacity = 0.6;
        self.winGoldLabel.layer.shadowRadius = 1.0;
        [self addSubview:self.winGoldLabel];
    }
    self.winGoldLabel.frame = CGRectMake(0, 10, self.bgImgView.size_width, [NSString contentofHeightWithFont:self.winGoldLabel.font]);
    self.winGoldLabel.textAlignment = NSTextAlignmentCenter;
    self.winGoldLabel.text = [NSString stringWithFormat:@"+%@",[Tool transferWithInfoNumber:coin]];
    self.winGoldLabel.alpha = 1;
    [UIView animateWithDuration:.4f animations:^{
        self.winGoldLabel.orgin_y = 0;
    } completion:^(BOOL finished) {
        self.winGoldLabel.alpha = 0;
    }];
}


#pragma mark - Animation
-(void)animationManager{

}

- (void)startAnimationWithDelay:(CGFloat)delayTime {
    
}

-(void)memberGoinHouse:(CGFloat)delayTime model:(PDChaosFightMembersSingleModel *)transferSingleModel{
    self.transferSingleModel = transferSingleModel;
    if (transferSingleModel.memberId.length){
        [self modelInfoWithModel:transferSingleModel];
        [self.avatarImgView uploadImageWithURL:transferSingleModel.avatar placeholder:nil callback:NULL];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.coinView){
            [self.coinView changerHasMemberInfoManager];
        }
    });
}

-(void)memberGooutHouse:(CGFloat)delayTime {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.coinView){
            [self.coinView changeNormalInfoManager];
        }
    });
}

@end
