//
//  PDChaosFightTouzhuView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightTouzhuView.h"

static char actionClickManagerWithGoldKey;
@interface PDChaosFightTouzhuView()
@property (nonatomic,strong)PDImageView *bgImgView;
@property (nonatomic,strong)PDImageView *topImgView;
@property (nonatomic,strong)UILabel *bottomLabel;
@property (nonatomic,strong)UIButton *clickBtn;
@property (nonatomic,strong)PDImageView *myTouzhuBgView;
@property (nonatomic,strong)PDImageView *winImgView;


@end

@implementation PDChaosFightTouzhuView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    //
    self.bgImgView = [[PDImageView alloc]init];
    self.bgImgView.backgroundColor = [UIColor clearColor];
    self.bgImgView.frame = self.bounds;
    self.bgImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bottom_bg"];
    [self addSubview:self.bgImgView];
    
    self.topImgView = [[PDImageView alloc]init];
    self.topImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.topImgView];
    
    self.bottomLabel = [[UILabel alloc]init];
    self.bottomLabel.font = [[UIFont systemFontOfSize:13.]boldFont];
    self.bottomLabel.textAlignment = NSTextAlignmentCenter;
    self.bottomLabel.textColor = [UIColor hexChangeFloat:@"b45b26"];
    [self addSubview:self.bottomLabel];
    
    // 创建btn
    self.clickBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.clickBtn.backgroundColor = [UIColor clearColor];
    self.clickBtn.frame = self.bounds;
    [self addSubview:self.clickBtn];
    __weak typeof(self)weakSelf = self;
    [self.clickBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickManagerWithGoldKey);
        if (block){
            block();
        }
    }];
    
    // 创建label
    [self createKeyWindowLabel];
}

#pragma mark - createKeyWindowLabel
-(void)createKeyWindowLabel{
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    if (!self.countPriceLabel){
        self.countPriceLabel = [[UILabel alloc]init];
        self.countPriceLabel.font = [UIFont systemFontOfSize:15.];
        self.countPriceLabel.textColor = [UIColor whiteColor];
        self.countPriceLabel.backgroundColor = [UIColor clearColor];
        self.countPriceLabel.textAlignment = NSTextAlignmentCenter;
        self.countPriceLabel.layer.shadowColor = [UIColor hexChangeFloat:@"7b3c0a"].CGColor;
        self.countPriceLabel.layer.shadowOffset = CGSizeMake(3,2.);
        self.countPriceLabel.layer.shadowOpacity = 0.6;
        self.countPriceLabel.layer.shadowRadius = 1.0;
        self.countPriceLabel.text = @"";
        [keyWindow addSubview:self.countPriceLabel];
    }
    if (!self.myTouzhuBgView){
        self.myTouzhuBgView = [[PDImageView alloc]init];
        self.myTouzhuBgView.frame = CGRectMake(0, self.bgImgView.size_height - 16, self.bgImgView.size_width, 16);
        self.myTouzhuBgView.alpha = 0.f;
        self.myTouzhuBgView.image = [Tool stretchImageWithName:@"icon_chaosFight_mytouzhu"];
        [self.bgImgView addSubview:self.myTouzhuBgView];
    }
    if (!self.myTouzhuLabel){
        self.myTouzhuLabel = [[UILabel alloc]init];
        self.myTouzhuLabel.font = [UIFont systemFontOfSize:13.];
        self.myTouzhuLabel.textColor = c26;
        self.myTouzhuLabel.textAlignment = NSTextAlignmentCenter;
        self.myTouzhuLabel.text = @"";
        [keyWindow addSubview:self.myTouzhuLabel];
    }
    
    [self auSetupFrame];
}

-(void)auSetupFrame{
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    CGRect convertRect = [self.superview convertRect:self.frame toView:keyWindow];
    self.countPriceLabel.frame = CGRectMake(convertRect.origin.x, convertRect.origin.y - 10, convertRect.size.width, [NSString contentofHeightWithFont:self.countPriceLabel.font]);
    
    self.myTouzhuLabel.frame = CGRectMake(convertRect.origin.x, convertRect.origin.y + convertRect.size.height - 16, self.countPriceLabel.size_width, 16);
}

-(void)outMainSettingFrame:(CGRect)frame{
    self.bounds = CGRectMake(0, 0, frame.size.width, frame.size.height);
    self.bgImgView.frame = self.bounds;
    self.bgImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bottom_bg"];

    self.clickBtn.frame = self.bounds;
    
    CGFloat height_margin = (self.size_height - 13 - [NSString contentofHeightWithFont:self.bottomLabel.font]) / 3.;
    self.topImgView.frame = CGRectMake((self.size_width - 27) / 2., height_margin, 27, 13);
    
    self.bottomLabel.frame = CGRectMake(0, CGRectGetMaxY(self.topImgView.frame) + height_margin, self.size_width, [NSString contentofHeightWithFont:self.bottomLabel.font]);

}


-(void)setTransferType:(chaosTouzhuType)transferType{
    _transferType = transferType;
    CGFloat height_margin = (self.size_height - 13 - [NSString contentofHeightWithFont:self.bottomLabel.font]) / 3.;
    self.topImgView.frame = CGRectMake((self.size_width - 27) / 2., height_margin, 27, 13);
    
    self.bottomLabel.frame = CGRectMake(0, CGRectGetMaxY(self.topImgView.frame) + height_margin, self.size_width, [NSString contentofHeightWithFont:self.bottomLabel.font]);
    
    if (transferType == chaosTouzhuTypeShuangsha){              // 双杀
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_shuangsha"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeXiaolong){
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_xiaolong"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeDalong){
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_dalong"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeShuangkong){
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_shuangkong"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeYizhi){
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_yizhi"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeLiangzhi){
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_erzhi"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeSanzhi){
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_sanzhi"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeSizhi){
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_sizhi"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeBlackDragonet){           // 黑龙
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_heilong"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeMeatMountain){            // 肉山
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_roushan"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeMaster){                  // 主宰
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_zhuzai"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeTyrant){                  // 暴君
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_baojun"];
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeTuoersuo){                // 托儿所
        self.bgImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bg1"];
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_tuoersuo"];
        self.bottomLabel.text = @"0倍";
        self.topImgView.frame = CGRectMake((self.size_width - CFFloat(75)) / 2., self.topImgView.orgin_y, CFFloat(75),CFFloat(19));
    } else if (transferType == chaosTouzhuTypePingshou){                // 平手
        self.bgImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bg1"];
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_pingshou"];
        self.topImgView.frame = CGRectMake((self.size_width - CFFloat(37)) / 2., self.topImgView.orgin_y, CFFloat(37), CFFloat(18));
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeertongjie){               // 儿童节
        self.bgImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bg1"];
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_ertongjie"];
        self.topImgView.frame = CGRectMake((self.size_width - CFFloat(75)) / 2., self.topImgView.orgin_y, CFFloat(75), CFFloat(19));
        self.bottomLabel.text = @"0倍";
    } else if (transferType == chaosTouzhuTypeChengyaojin){             // 程咬金
        self.bgImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bg1"];
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_chengyaojin"];
        self.bottomLabel.text = @"0倍";
        self.topImgView.frame = CGRectMake((self.size_width - CFFloat(75)) / 2., self.topImgView.orgin_y, CFFloat(75),CFFloat(19));
    } else if (transferType == chaosTouzhuTypeXiahoudun){               // 夏侯惇
        self.bgImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bg1"];
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_xiahoudun"];
        self.bottomLabel.text = @"0倍";
        self.topImgView.frame = CGRectMake((self.size_width - CFFloat(75)) / 2., self.topImgView.orgin_y, CFFloat(75),CFFloat(19));
    } else if (transferType == chaosTouzhuTypeLangRen){                 // 狼人
        self.bgImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bg1"];
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_langren"];
        self.bottomLabel.text = @"0倍";
        self.topImgView.frame = CGRectMake((self.size_width - CFFloat(57)) / 2., self.topImgView.orgin_y, CFFloat(57),CFFloat(19));
    } else if (transferType == chaosTouzhuTypeMori){                    // 末日
        self.bgImgView.image = [Tool stretchImageWithName:@"icon_chaosFight_table_bg1"];
        self.topImgView.image = [UIImage imageNamed:@"icon_chaosFight_table_mori"];
        self.bottomLabel.text = @"0倍";
        self.topImgView.frame = CGRectMake((self.size_width - CFFloat(56)) / 2., self.topImgView.orgin_y, CFFloat(56),CFFloat(19));
    }
}

-(void)setCurrentCountPrice:(NSInteger)currentCountPrice{
    [self createKeyWindowLabel];
    _currentCountPrice = currentCountPrice;
    if (currentCountPrice >0){
        if (currentCountPrice >= 10000){
            self.countPriceLabel.text = [NSString stringWithFormat:@"%.2f万",currentCountPrice * 1. / 10000];
        } else {
            self.countPriceLabel.text = [NSString stringWithFormat:@"%li",(long)currentCountPrice];
        }
        self.countPriceLabel.alpha = 1;
    } else {
        self.countPriceLabel.alpha = 0;
    }
}

-(void)setCurrentMyPrice:(NSInteger)currentMyPrice{
    [self createKeyWindowLabel];
    _currentMyPrice = currentMyPrice;
    if (_currentMyPrice > 0){
        if (_currentMyPrice >= 10000){
            self.myTouzhuLabel.text = [NSString stringWithFormat:@"%.2f万",_currentMyPrice * 1. / 10000];
        } else {
            self.myTouzhuLabel.text = [NSString stringWithFormat:@"%li",(long)_currentMyPrice];
        }
        self.myTouzhuLabel.alpha = .8f;
        self.myTouzhuBgView.alpha = .6f;
    } else {
        self.myTouzhuLabel.alpha = 0;
        self.myTouzhuBgView.alpha = 0;
    }
}

-(void)actionClickManagerWithGold:(NSInteger)gold callback:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickManagerWithGoldKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)dismissKeyWindowManager{
    [self.winImgView removeFromSuperview];
    [self.myTouzhuBgView removeFromSuperview];
    [self.myTouzhuLabel removeFromSuperview];
    [self.countPriceLabel removeFromSuperview];

    
    self.winImgView = nil;
    self.myTouzhuBgView = nil;
    self.countPriceLabel = nil;
    self.myTouzhuLabel = nil;
}

-(void)removeFromWindow{
    [self.winImgView removeFromSuperview];
    [self.myTouzhuBgView removeFromSuperview];
    [self.countPriceLabel removeFromSuperview];
    [self.myTouzhuLabel removeFromSuperview];
    
    self.winImgView = nil;
    self.myTouzhuBgView = nil;
    self.countPriceLabel = nil;
    self.myTouzhuLabel = nil;
}

-(void)showWinManagerWithBlock:(void(^)())block{
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    CGRect convertRect = [self.superview convertRect:self.frame toView:keyWindow];
    if (!self.winImgView){
        self.winImgView = [[PDImageView alloc]init];
        self.winImgView.backgroundColor = [UIColor clearColor];
        self.winImgView.image = [UIImage imageNamed:@"icon_chaosFight_bottom_win"];;
        self.winImgView.frame = CGRectMake(0, 0, 75, 53);
        CGFloat width = 75 * self.size_height / 53;
        self.winImgView.frame = CGRectMake(0, 0, width, MIN(53, self.size_height));
        
        
        self.winImgView.center_x = convertRect.origin.x + convertRect.size.width / 2.;
        self.winImgView.alpha = 0.;
        self.winImgView.orgin_y -= 100;
        [keyWindow addSubview:self.winImgView];
    }
    [UIView animateWithDuration:.5f animations:^{
        self.winImgView.orgin_y = convertRect.origin.y;
        self.winImgView.alpha = 1.;
    } completion:^(BOOL finished) {
        if (block){
            block();
        }
    }];
}

-(void)dismissWinManagerBlock:(void(^)())block{
        [self removeFromWindow];
    [UIView animateWithDuration:.5f animations:^{
        self.winImgView.alpha = 0;
    } completion:^(BOOL finished) {

        [self.winImgView removeFromSuperview];
        self.winImgView = nil;
        if (block){
            block();
        }
    }];
}

-(void)changePeilv:(NSString *)str{
    self.bottomLabel.text = str;
}
@end
