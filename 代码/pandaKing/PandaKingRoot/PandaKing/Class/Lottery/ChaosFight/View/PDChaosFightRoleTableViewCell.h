//
//  PDChaosFightRoleTableViewCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDChaosFightMembersSingleModel.h"

@interface PDChaosFightRoleTableViewCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,strong)PDChaosFightMembersSingleModel *transferSingleModel;
@property (nonatomic,assign)CGFloat transferCellWidth;

#pragma mark - 有人进入房间
-(void)memberGoinHouse:(CGFloat)delayTime model:(PDChaosFightMembersSingleModel *)transferSingleModel;
-(void)memberGooutHouse:(CGFloat)delayTime;
-(void)winWithAddGoldAnimation:(NSInteger)coin;


@end
