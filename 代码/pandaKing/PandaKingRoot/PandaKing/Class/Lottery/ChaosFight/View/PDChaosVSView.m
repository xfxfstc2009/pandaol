//
//  PDChaosVSView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosVSView.h"

@interface PDChaosVSView()
@property (nonatomic,strong)LTMorphingLabel *vsLabel;
@property (nonatomic,strong)PDImageView *goImgView;
@end

@implementation PDChaosVSView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    if (!self.vsImgView){
        self.vsImgView = [[PDImageView alloc]init];
        self.vsImgView.frame = self.bounds;
        self.vsImgView.alpha = 0;
        self.vsImgView.image = [UIImage imageNamed:@"vs"];
        self.vsImgView.frame = CGRectMake((kScreenBounds.size.width - 92) / 2., (kScreenBounds.size.height - 65) / 2. - 20, 92, 65);
        [keyWindow addSubview:self.vsImgView];
    }
    if (!self.vsLabel){
        self.vsLabel = [[LTMorphingLabel alloc] initWithFrame:CGRectZero];
        self.vsLabel.backgroundColor = [UIColor clearColor];
        self.vsLabel.font = BAR_MAIN_TITLE_FONT;
        self.vsLabel.adjustsFontSizeToFitWidth = YES;
        self.vsLabel.textColor = [UIColor clearColor];
        self.vsLabel.textAlignment = NSTextAlignmentCenter;
        self.vsLabel.morphingEffect = LTMorphingEffectAnvil;
        self.vsLabel.frame = CGRectMake((kScreenBounds.size.width - 92) / 2., (kScreenBounds.size.height - 65) / 2., 92, 65);
        self.vsLabel.text = @"";
        [keyWindow addSubview:self.vsLabel];
    }
    
    if (!self.goImgView){
        self.goImgView = [[PDImageView alloc]init];
        self.goImgView.frame = self.bounds;
        self.goImgView.alpha = 0;
        self.goImgView.image = [UIImage imageNamed:@"icon_chaosFight_ready_go"];
        self.goImgView.frame = CGRectMake((kScreenBounds.size.width - 92) / 2., (kScreenBounds.size.height - 53) / 2., 92, 53);
        [keyWindow addSubview:self.goImgView];
    }
}

-(NSString *)getRandomText{
    NSString *info = @"";
    NSInteger x = random() % 20;
    for (int i = 0 ; i< x;i++){
        info = [info stringByAppendingString:@" "];
    }
    return info;
}

-(void)startAnimationFinish:(void(^)())block{
    // 1. 懒加载
    [self createView];
    // 2.
    NSString *info = [self getRandomText];
    if ([info isEqualToString:self.vsLabel.text]){
        [self startAnimationFinish:NULL];
        return;
    }
    self.vsLabel.text = info;
    
    [UIView animateWithDuration:.5f animations:^{
        self.vsImgView.alpha = 1;
        self.vsImgView.orgin_y = (kScreenBounds.size.height - 65) / 2.;
    } completion:^(BOOL finished) {
        self.vsImgView.orgin_y = (kScreenBounds.size.height - 65) / 2.;
        __weak typeof(self)weakSelf = self;
        [self dismissAnimation:^{
            if (!weakSelf){
                return ;
            }
            if (block){
                block();
            }
        }];
    }];
}


-(void)dismissAnimation:(void(^)())block{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.2f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:.2f animations:^{
            if (self.vsImgView){
                self.vsImgView.alpha = 0;
            }
        } completion:^(BOOL finished) {
            [self.vsLabel removeFromSuperview];
            [self.vsImgView removeFromSuperview];
            self.vsLabel = nil;
            self.vsImgView = nil;
            __weak typeof(self)weakSelf = self;
            [self showGoManagerWithBlock:^{
                if (!weakSelf){
                    return ;
                }
                if(block){
                    block();
                }
            }];
        }];
    });
}

-(void)showGoManagerWithBlock:(void(^)())block{
    self.goImgView.alpha = 1;
    [UIView animateWithDuration:1.f animations:^{
        self.goImgView.alpha = 0;
        self.goImgView.frame = CGRectMake((kScreenBounds.size.width - 92 * 3) / 2., (kScreenBounds.size.height - 53 * 3) / 2., 92 * 3, 53 * 3);
    } completion:^(BOOL finished) {
        [self.goImgView removeFromSuperview];
        self.goImgView = nil;
        if (block){
            block();
        }
    }];
}

-(void)dismissManager{
    if (self.vsImgView){
        [self.vsLabel removeFromSuperview];
        [self.vsImgView removeFromSuperview];
        [self.goImgView removeFromSuperview];
        self.vsLabel = nil;
        self.vsImgView = nil;
        self.goImgView = nil;
    }
}
@end
