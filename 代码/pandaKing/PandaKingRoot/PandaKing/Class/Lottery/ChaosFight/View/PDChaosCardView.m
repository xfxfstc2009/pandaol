//
//  PDChaosCardView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosCardView.h"
#import "ZCAudioTool.h"

@interface PDChaosCardView()
@property (nonatomic,strong)PDImageView * guaishouImgView;
@property (nonatomic,strong)PDImageView *missImgView;
@property (nonatomic,strong)PDImageView *killImgView;
@property (nonatomic,strong)ZCAudioTool *audioTool;

@end

@implementation PDChaosCardView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.guaishouImgView = [[PDImageView alloc]init];
    self.guaishouImgView.frame = self.bounds;
    [self addSubview:self.guaishouImgView];
    
    self.missImgView = [[PDImageView alloc]init];
    self.missImgView.backgroundColor = [UIColor clearColor];
    self.missImgView.frame = CGRectMake((self.size_width - 79) / 2., 30, 79, 35);
    self.missImgView.alpha = 0;
    self.missImgView.image = [UIImage imageNamed:@"icon_chaosFight_miss"];
    [self addSubview:self.missImgView];
    
    self.killImgView = [[PDImageView alloc]init];
    self.killImgView.backgroundColor = [UIColor clearColor];
    self.killImgView.frame = self.bounds;
    self.killImgView.alpha = 0;
    self.killImgView.image = [UIImage imageNamed:@"icon_chaosFight_guaishou_line"];
    [self addSubview:self.killImgView];
    
    self.audioTool = [[ZCAudioTool alloc]init];
}

-(void)setTransferImg:(UIImage *)transferImg{
    _transferImg = transferImg;
    self.guaishouImgView.image = transferImg;
}

-(void)setTransferRect:(CGRect)transferRect{
    _transferRect = transferRect;
    self.frame = transferRect;
    self.guaishouImgView.frame = self.bounds;
}

-(void)animationWithType:(PDChaosCardViewType)type block:(void(^)())block{
    if (type == PDChaosCardViewTypeKill){
        self.killImgView.alpha = 1;
        [self.audioTool playSoundWithSoundName:@"mic_daguai.wav"];
        [UIView animateWithDuration:.5f animations:^{
            self.killImgView.alpha = 0;
        } completion:^(BOOL finished) {
            if (block){
                block();
            }
        }];
    } else if (type == PDChaosCardViewTypeMiss){
        self.missImgView.alpha = 1;
        self.killImgView.alpha = 1;
        [self.audioTool playSoundWithSoundName:@"mic_miss.wav"];
        [UIView animateWithDuration:.5f animations:^{
            self.missImgView.alpha = 0;
            self.killImgView.alpha = 0;
        } completion:^(BOOL finished) {
            if (block){
                block();
            }
        }];
    }
}

-(void)removeView{
   
}
@end
