//
//  PDChaosTodayCountCell.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDChaosFightTodayCountRootModel.h"

@interface PDChaosTodayCountCell : UITableViewCell

@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,assign)CGFloat transferCellWidth;
@property (nonatomic,strong)PDChaosFightTodayCountRootModel *transferSingleModel;

+(CGFloat)calculationCellHeight;

@end
