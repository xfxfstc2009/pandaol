//
//  PDHeroView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/27.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger,heroType) {
    heroTypeYasuo,                      /**< 亚索*/
    heroTypeJie,                        /**< 劫*/
    heroTypeXiahoudun,                  /**< 夏侯惇*/
    heroTypeChengyaojin,                /**< 程咬金*/
    heroTypeMori,                       /**< 末日*/
    heroTypeLangren,                    /**< 狼人*/
};

@interface PDHeroView : UIView

@property (nonatomic,assign)heroType transferType;

-(void)starAnimationWithType:(heroType)heroType finished:(void(^)())block;
-(void)dismissAnimationWithType:(heroType)heroType;


-(void)removeView;
@end
