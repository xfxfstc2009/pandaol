//
//  PDChaosFightGuizeTableViewCell.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/4/10.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosFightGuizeTableViewCell.h"

@interface PDChaosFightGuizeTableViewCell()
@property (nonatomic,strong)UILabel *infoLabel;


@end

@implementation PDChaosFightGuizeTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.infoLabel = [GWViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.infoLabel.font = [UIFont systemFontOfSize:13.];
    self.infoLabel.numberOfLines = 0;
    self.infoLabel.text = @"打野大作战火热上线！选择你支持的选手，赢得更多奖励哦~\n基本规则(以英雄联盟为例)：\n每场比赛托儿索和儿童劫将会各自出击2次进行打野，托儿索先出击，每次出击将会有10种不同的结果分别为：没打到（0）、F6（1200）、三狼（1320）、石甲虫（1445）、峡谷迅捷蟹（755）、魔沼蛙（1600）、蓝BUFF（1300）、红BUFF（1800）、小龙（4940）、大龙（11540），括号内数值代表野怪的分数。玩家可以根据公开信息选择支持的阵营和区域。\n当打野结果产生时，根据结果按照下注区域的倍数奖励获胜的玩家。\n任一区域：两位选手中任何一位得到区域内的结果，玩家将获得对应倍数的奖励。\n全场区域：全场累计得到区域内的结果，玩家将获得对应倍数的奖励。\n当两位选手最终积分相同时，视作平手。\n名词解释：\n双杀：任意一人一局比赛内连续2次斩杀同一只野怪\n双空：任意一人一局比赛内连续2次没有斩杀到野怪\n小龙：任意一人一局比赛内斩杀至少1只小龙\n大龙：任意一人一局比赛内斩杀至少1只大龙\n1只：全场比赛总共斩杀1只野怪\n2只：全场比赛总共斩杀2只野怪\n3只：全场比赛总共斩杀3只野怪\n4只：全场比赛总共斩杀4只野怪\n每日投入有上限。请设计合理的策略进行打野大作战吧！";
    [self addSubview:self.infoLabel];
    self.infoLabel.textColor = [UIColor hexChangeFloat:@"75501b"];
}

-(void)setTransferWidth:(CGFloat)transferWidth{
    _transferWidth = transferWidth;
    CGSize contentOfSize = [self.infoLabel.text sizeWithCalcFont:self.infoLabel.font constrainedToSize:CGSizeMake(transferWidth - 2 * 11, CGFLOAT_MAX)];
    self.infoLabel.frame = CGRectMake(11, 0, transferWidth - 2 * 11, contentOfSize.height);
}

+(CGFloat)calculationCellHeightWithWidth:(CGFloat)width{
    CGFloat cellHeight = 0;
    NSString *info = @"打野大作战火热上线！选择你支持的选手，赢得更多奖励哦~\n基本规则(以英雄联盟为例)：\n每场比赛托儿索和儿童劫将会各自出击2次进行打野，托儿索先出击，每次出击将会有10种不同的结果分别为：没打到（0）、F6（1200）、三狼（1320）、石甲虫（1445）、峡谷迅捷蟹（755）、魔沼蛙（1600）、蓝BUFF（1300）、红BUFF（1800）、小龙（4940）、大龙（11540），括号内数值代表野怪的分数。玩家可以根据公开信息选择支持的阵营和区域。\n当打野结果产生时，根据结果按照下注区域的倍数奖励获胜的玩家。\n任一区域：两位选手中任何一位得到区域内的结果，玩家将获得对应倍数的奖励。\n全场区域：全场累计得到区域内的结果，玩家将获得对应倍数的奖励。\n当两位选手最终积分相同时，视作平手。\n名词解释：\n双杀：任意一人一局比赛内连续2次斩杀同一只野怪\n双空：任意一人一局比赛内连续2次没有斩杀到野怪\n小龙：任意一人一局比赛内斩杀至少1只小龙\n大龙：任意一人一局比赛内斩杀至少1只大龙\n1只：全场比赛总共斩杀1只野怪\n2只：全场比赛总共斩杀2只野怪\n3只：全场比赛总共斩杀3只野怪\n4只：全场比赛总共斩杀4只野怪\n每日投入有上限。请设计合理的策略进行打野大作战吧！";
    CGSize contentOfSize = [info sizeWithCalcFont:[UIFont systemFontOfSize:13.] constrainedToSize:CGSizeMake(width - 2 * 11, CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    return cellHeight;
}
@end
