//
//  PDChaosBackgroundView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/28.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDChaosBackgroundView : UIView


-(void)startAnimationWithFinish:(void(^)())block;

-(void)dismissManager;
@end
