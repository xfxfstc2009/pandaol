//
//  PDChaosReadyGoView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDChaosReadyGoView.h"
#import "ZCAudioTool.h"
@interface PDChaosReadyGoView()
@property (nonatomic,strong)PDImageView *readyGoImgView;
@property (nonatomic,strong)ZCAudioTool *audioTool;

@end

@implementation PDChaosReadyGoView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.readyGoImgView = [[PDImageView alloc]init];
    self.readyGoImgView.backgroundColor = [UIColor redColor];
    self.readyGoImgView.image = [UIImage imageNamed:@""];
    self.readyGoImgView.frame = self.bounds;
    self.readyGoImgView.alpha = 0;
    [self  addSubview:self.readyGoImgView];
    
    self.audioTool = [[ZCAudioTool alloc]init];
}

-(void)startAnimation{
    [self.audioTool playSoundWithSoundName:@"5375.wav"];
    
    
    [UIView animateWithDuration:.5f animations:^{
        self.orgin_x = (kScreenBounds.size.width - 200) / 2.;
        self.alpha = 1;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:.5f animations:^{
            self.orgin_x = (kScreenBounds.size.width - 200) / 4 * 3;
            self.alpha = 0;
        } completion:^(BOOL finished) {
            self.orgin_x = -200;
        }];
    }];
}

@end
