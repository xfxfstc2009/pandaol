//
//  PDTestWindowViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/3/30.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDTestWindowViewController.h"

@implementation PDTestWindowViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
}

#pragma mark - pageSetting
-(void)pageSetting{
    __weak typeof(self)weakSelf=  self;
        [self leftBarButtonWithTitle:@"返回" barNorImage:nil barHltImage:nil action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
        }];
}
-(void)dealloc{
    NSLog(@"123");
    
    
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    NSLog(@"%@",keyWindow.subviews);
}
@end
