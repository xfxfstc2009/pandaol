//
//  PDLotterySelectedGoldView.m
//  PandaKing
//
//  Created by Cranz on 16/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotterySelectedGoldView.h"
#import "PDSelectedSheetView.h"

typedef void(^tempBlock)();
typedef void(^goldBlock)(NSInteger count);
@interface PDLotterySelectedGoldView ()
@property (nonatomic, strong) UIView *titleBar;
@property (nonatomic, strong) UIView *contentView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat contentHeight;

@property (nonatomic, copy)   tempBlock backBlock;
@property (nonatomic, copy)   tempBlock cancleBlock;
@property (nonatomic, copy)   goldBlock goldBlock;

// 主要内容
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UIView *vline1;
@property (nonatomic, strong) UIView *hline1;
@property (nonatomic, strong) UILabel *goldLabel; // 投入总金额
@property (nonatomic, strong) UIView *vline2;
@property (nonatomic, strong) UIView *hline2;
@property (nonatomic, strong) UILabel *rewardDescLabel;
@property (nonatomic, strong) UILabel *rewardLabel;// 获得的金额
@property (nonatomic, strong, readwrite) CCZSlider *slider;
@property (nonatomic, strong) UIButton *moreButton;

// 三模块的高度
@property (nonatomic, assign) CGFloat row1;
@property (nonatomic, assign) CGFloat row2;
@property (nonatomic, assign) CGFloat row3;
@end
@implementation PDLotterySelectedGoldView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    _size = self.frame.size;
    _contentHeight = _size.height - kTitleBarheight;
}

- (void)pageSetting {
    [self titleBarSetting];
    [self contentViewSetting];
}

- (void)titleBarSetting {
    _titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _size.width, kTitleBarheight)];
    _titleBar.backgroundColor = c2;
    [self addSubview:_titleBar];
    
    UIButton *backButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, kTitleBarheight, kTitleBarheight)];
    [backButton setImage:[UIImage imageNamed:@"icon_center_back"] forState:UIControlStateNormal];
    [backButton addTarget:self action:@selector(didClickBack) forControlEvents:UIControlEventTouchUpInside];
    [_titleBar addSubview:backButton];
    
    // cancle button
    UIButton *cancleButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancleButton.frame = CGRectMake(_size.width - kTableViewSectionHeader_left - kTitleBarheight, 0, kTitleBarheight, kTitleBarheight);
    [cancleButton setImage:[UIImage imageNamed:@"icon_pay_cancle"] forState:UIControlStateNormal];
    [cancleButton addTarget:self action:@selector(didClickCancle) forControlEvents:UIControlEventTouchUpInside];
    [_titleBar addSubview:cancleButton];
    
    _titleLabel = [[UILabel alloc] init];
    _titleLabel.font = [UIFont systemFontOfCustomeSize:13];
    _titleLabel.textColor = [UIColor whiteColor];
    [_titleBar addSubview:_titleLabel];
}

- (void)contentViewSetting {
    _contentView = [[UIView alloc] initWithFrame:CGRectMake(0, kTitleBarheight, _size.width, _size.height - kTitleBarheight)];
    _contentView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_contentView];
    
    // 确定按钮
    UIButton *moreButton = [[UIButton alloc] initWithFrame:CGRectMake(0, _contentHeight - 44, kScreenBounds.size.width, 44)];
    [moreButton setTitle:@"确定" forState:UIControlStateNormal];
    [moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(didClickSure) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:moreButton];
    self.moreButton = moreButton;
    [self buttonUnEnable];
    
    CGFloat rowUnitSpace = (_contentHeight - 44) / 6; // 1:2:3
    _row1 = rowUnitSpace;
    _row2 = rowUnitSpace * 2;
    _row3 = rowUnitSpace * 3;
    
    // 第一行
    UILabel *subDescLabel = [[UILabel alloc] init];
    subDescLabel.text = @"投注内容";
    subDescLabel.font = [UIFont systemFontOfCustomeSize:14];
    subDescLabel.textColor = c4;
    CGSize subDescSize = [subDescLabel.text sizeWithCalcFont:subDescLabel.font];
    subDescLabel.frame = CGRectMake(kTableViewSectionHeader_left, (_row1 - subDescSize.height) / 2, subDescSize.width, subDescSize.height);
    [_contentView addSubview:subDescLabel];
    
    UIView *vline1 = [[UIView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left * 2 + subDescSize.width, 0, 1, _row1)];
    vline1.backgroundColor = c27;
    [_contentView addSubview:vline1];
    self.vline1 = vline1;
    
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.descLabel.textColor = c26;
    self.descLabel.adjustsFontSizeToFitWidth = YES;
    [_contentView addSubview:self.descLabel];
    
    self.hline1 = [[UIView alloc] initWithFrame:CGRectMake(0, _row1 - 1, _size.width, 1)];
    self.hline1.backgroundColor = vline1.backgroundColor;
    [_contentView addSubview:self.hline1];
    
    // 第二行
    UILabel *goldDescLabel = [[UILabel alloc] init];
    goldDescLabel.text = @"竞猜金额(金币)";
    goldDescLabel.textAlignment = NSTextAlignmentCenter;
    goldDescLabel.font = [UIFont systemFontOfCustomeSize:13];
    goldDescLabel.textColor = c4;
    goldDescLabel.backgroundColor = BACKGROUND_VIEW_COLOR;
    CGSize goldDescSize = [goldDescLabel.text sizeWithCalcFont:goldDescLabel.font];
    CGSize goldDescMakeSize = CGSizeMake(goldDescSize.width + 4, 20 + goldDescSize.height);
    goldDescLabel.frame = CGRectMake(kTableViewSectionHeader_left, (_row2 - goldDescMakeSize.height) / 2 + _row1, goldDescMakeSize.width, goldDescMakeSize.height);
    [_contentView addSubview:goldDescLabel];
    
    self.goldLabel = [[UILabel alloc] init];
    self.goldLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.goldLabel.textColor = c4;
    [_contentView addSubview:self.goldLabel];
    
    self.vline2 = [[UIView alloc] initWithFrame:CGRectMake(_size.width / 2, _row1, 1, _row2)];
    self.vline2.backgroundColor = vline1.backgroundColor;
    [_contentView addSubview:self.vline2];
    
    UILabel *rewardDescLabel = [[UILabel alloc] init];
    rewardDescLabel.font = [UIFont systemFontOfCustomeSize:13];
    rewardDescLabel.textColor = c4;
    rewardDescLabel.numberOfLines = 0;
    rewardDescLabel.text = @"可能的奖金\n数额(金币)";
    CGSize rewardSize = [rewardDescLabel.text sizeWithCalcFont:rewardDescLabel.font];
    rewardDescLabel.frame = CGRectMake(_size.width - kTableViewSectionHeader_left - rewardSize.width, _row1 + (_row2 - rewardSize.height) / 2, rewardSize.width, rewardSize.height);
    [_contentView addSubview:rewardDescLabel];
    self.rewardDescLabel = rewardDescLabel;
    
    self.rewardLabel = [[UILabel alloc] init];
    self.rewardLabel.font = [UIFont systemFontOfCustomeSize:22];
    self.rewardLabel.textColor = [UIColor blackColor];
    [_contentView addSubview:self.rewardLabel];
    
    self.hline2 = [[UIView alloc] initWithFrame:CGRectMake(0, _row1 + _row2 - 1, _size.width, 1)];
    self.hline2.backgroundColor = vline1.backgroundColor;
    [_contentView addSubview:self.hline2];
    
    // 第三行
    // 滑杆
    // 滑杆的最大值会随着会员的总金币数变化
    CGFloat sliderX = LCFloat(60);
    self.slider = [[CCZSlider alloc] initWithFrame:CGRectMake(sliderX, _row1 + _row2 + LCFloat(40), kScreenBounds.size.width - sliderX * 2, LCFloat(30))];
    self.slider.minimumValue = 0;
    self.slider.divisionValue = 100;
    self.slider.minimumTrackTintColor = [UIColor blackColor];
    [self.slider addTarget:self action:@selector(heroSliderDidSlider:) forControlEvents:UIControlEventValueChanged];
    [_contentView addSubview:self.slider];
    
    UILabel *changeLabel = [[UILabel alloc] init];
    changeLabel.text = @"奖金数额会随着金币投入数额的量而变化";
    changeLabel.font = [UIFont systemFontOfCustomeSize:14];
    changeLabel.textColor = c4;
    CGSize changeSize = [changeLabel.text sizeWithCalcFont:changeLabel.font];
    changeLabel.frame = CGRectMake((_size.width - changeSize.width) / 2, CGRectGetMaxY(self.slider.frame) + LCFloat(10), changeSize.width, changeSize.height);
    [_contentView addSubview:changeLabel];
    
    // 加减按钮
    CGFloat btnWidth = LCFloat(35);
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addButton.frame = CGRectMake(CGRectGetMaxX(self.slider.frame) + (sliderX - btnWidth) / 2, CGRectGetMinY(self.slider.frame) + (CGRectGetHeight(self.slider.frame) - btnWidth) / 2, btnWidth, btnWidth);
    [addButton setImage:[UIImage imageNamed:@"icon_snatch_add"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(didClickAddButton) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:addButton];
    
    UIButton *reduceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    reduceButton.frame = CGRectMake((sliderX - btnWidth) / 2, CGRectGetMinY(addButton.frame), btnWidth, btnWidth);
    [reduceButton setImage:[UIImage imageNamed:@"icon_snatch_reduce"] forState:UIControlStateNormal];
    [reduceButton addTarget:self action:@selector(didClickReduceButton) forControlEvents:UIControlEventTouchUpInside];
    [_contentView addSubview:reduceButton];
}

- (void)buttonEnable {
    self.moreButton.enabled = YES;
    self.moreButton.backgroundColor = [UIColor blackColor];
}

- (void)buttonUnEnable {
    self.moreButton.enabled = NO;
    self.moreButton.backgroundColor = [UIColor lightGrayColor];
}

- (void)updateGold {
    self.goldLabel.text = [NSString stringWithFormat:@"%ld",(long)self.slider.value];
    self.rewardLabel.text = [NSString stringWithFormat:@"%ld",(long)(self.slider.value * self.model.odds.floatValue)];
    
}

- (void)updateFrames {
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    CGSize rewardSize = [self.rewardLabel.text sizeWithCalcFont:self.rewardLabel.font];
    
    self.goldLabel.frame = CGRectMake(_size.width / 2 - LCFloat(15) - goldSize.width, (_row2 - goldSize.height) / 2 + _row1, goldSize.width, goldSize.height);
    self.rewardLabel.frame = CGRectMake(CGRectGetMinX(self.rewardDescLabel.frame) - LCFloat(8) - rewardSize.width, (_row2 - rewardSize.height) / 2 + _row1, rewardSize.width, rewardSize.height);
}

- (void)backWithBackBlock:(void (^)())backBlock {
    if (backBlock) {
        self.backBlock = backBlock;
    }
}

- (void)cancleWithCancleBlock:(void (^)())cancleBlock {
    if (cancleBlock) {
        self.cancleBlock = cancleBlock;
    }
}

- (void)submitWithGoldCount:(void (^)(NSInteger))submitBlock {
    if (submitBlock) {
        self.goldBlock = submitBlock;
    }
}

#pragma mark - set

- (void)setTitle:(NSString *)title {
    _title = title;
    
    _titleLabel.text = title;
    CGSize titleSize = [title sizeWithCalcFont:_titleLabel.font];
    _titleLabel.frame = CGRectMake((_size.width - titleSize.width) / 2, (kTitleBarheight - titleSize.height) / 2, titleSize.width, titleSize.height);
}

- (void)setModel:(PDLotteryHeroLotteryInfo *)model {
    _model = model;
    
    self.slider.maximumValue = model.gold / 100 * 100;
    
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        self.descLabel.text = [NSString stringWithFormat:@"%@、%@、%@(赔率%@)",model.goldPrice,model.far,model.nickLength,model.odds];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        self.descLabel.text = [NSString stringWithFormat:@"%@、%@、%@(赔率%@)",model.dota2Camp,model.dota2Far,model.dota2MainProperty,model.odds];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        self.descLabel.text = [NSString stringWithFormat:@"%@、%@(赔率%@)",model.pvpFar,model.pvpNameLength,model.odds];
    }
    
    self.goldLabel.text = @"0";
    self.rewardLabel.text = @"0";
    
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(self.vline1.frame), [NSString contentofHeightWithFont:self.descLabel.font])];
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    CGSize rewardSize = [self.rewardLabel.text sizeWithCalcFont:self.rewardLabel.font];
    
    self.descLabel.frame = CGRectMake(CGRectGetMaxX(self.vline1.frame) + kTableViewSectionHeader_left, (_row1 - descSize.height) / 2, descSize.width, descSize.height);
    self.goldLabel.frame = CGRectMake(_size.width / 2 - LCFloat(15) - goldSize.width, (_row2 - goldSize.height) / 2 + _row1, goldSize.width, goldSize.height);
    self.rewardLabel.frame = CGRectMake(CGRectGetMinX(self.rewardDescLabel.frame) - LCFloat(8) - rewardSize.width, (_row2 - rewardSize.height) / 2 + _row1, rewardSize.width, rewardSize.height);
}

- (void)setFalseGold:(NSUInteger)falseGold {
    _falseGold = falseGold;
    
    [self.slider setValue:falseGold animated:YES];
    
    [self updateGold];
    [self updateFrames];
    [self buttonEnable];
}

#pragma mark - 控件方法

- (void)didClickBack {
    if (self.backBlock) {
        self.backBlock();
    }
}

- (void)didClickCancle {
    if (self.cancleBlock) {
        self.cancleBlock();
    }
}

- (void)didClickSure {
    if (self.goldBlock) {
      self.goldBlock(self.slider.value);
    }
}

- (void)heroSliderDidSlider:(CCZSlider *)slider {
    [self updateGold];
    [self updateFrames];
    
    if (slider.value == 0) {
        [self buttonUnEnable];
    } else {
        [self buttonEnable];
    }
}

- (void)didClickAddButton {
    if (self.slider.value + 100 > self.model.gold) {
        return;
    }
    self.slider.value += 100;
    [self.slider setValue:self.slider.value animated:YES];
    
    [self updateGold];
    [self updateFrames];
    
    if (self.slider.value > 0) {
        [self buttonEnable];
    }
}

- (void)didClickReduceButton {
    self.slider.value -= 100;
    [self.slider setValue:self.slider.value animated:YES];
    
    [self updateGold];
    [self updateFrames];
    
    if (self.slider.value == 0) {
        [self buttonUnEnable];
    } else {
        [self buttonEnable];
    }
}

@end
