//
//  PDLotterySelectView.h
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSelectedSheetView.h"
#import "PDLotterySelectedButton.h"
#import "PDLotterySelectedGoldView.h"

@protocol PDLotterySelectViewDelegate;
@interface PDLotterySelectView : PDSelectedSheetView
@property (nonatomic, copy) NSString *buttonTitle;
@property (nonatomic, weak) id<PDLotterySelectViewDelegate> delegate;

@property (nonatomic, strong) PDLotterySelectedGoldView *goldView; //选择金币页

@property (nonatomic, strong) PDLottryButtonModel *selectedRow1Model;
@property (nonatomic, strong) PDLottryButtonModel *selectedRow2Model;
@property (nonatomic, strong) PDLottryButtonModel *selectedRow3Model;

@property (nonatomic, assign) BOOL isSelected;  /**< 判断是否至少选了一个选项*/

+ (CGFloat)viewHeight;
- (void)changeToInput;
- (void)changeToSelected;
- (void)dismissGoldView;

/// 主动去指定选中某行某一个
- (void)setSelectedAtRow:(NSInteger)row index:(NSInteger)index;
- (PDLotterySelectedButton *)buttonAtRow:(NSInteger)row index:(NSInteger)index;

@end

@protocol PDLotterySelectViewDelegate <NSObject>
/** 
 1、一进去是一个都未选中
 2、在未选中的情况下的值是0
 3、没点击一次按钮就会触发下面的回调，反应三行选择情况
 */
- (void)lotterySelectView:(PDLotterySelectView *)view didSelectedAtRow1:(NSInteger)index1 row2:(NSInteger)index2 row3:(NSInteger)index3;
/** 
 点击下一步
 */
- (void)lotterySelectView:(PDLotterySelectView *)view didClickNextButton:(UIButton *)button;
@end