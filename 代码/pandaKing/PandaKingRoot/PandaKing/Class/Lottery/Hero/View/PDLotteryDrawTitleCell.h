//
//  PDLotteryDrawTitleCell.h
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

/// 开奖记录
@interface PDLotteryDrawTitleCell : UITableViewCell
@property (nonatomic, copy) NSString *leftDesc;
@property (nonatomic, copy) NSString *centerDesc;
@property (nonatomic, copy) NSString *rightDesc;

+ (CGFloat)cellHeight;
@end
