//
//  PDLotteryJoiningCell.h
//  PandaKing
//
//  Created by Cranz on 16/11/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryHeroDetail.h"

@interface PDLotteryJoiningCell : UITableViewCell
@property (nonatomic, strong) PDLotteryHeroDetail *model;
+ (CGFloat)cellHeight;
@end
