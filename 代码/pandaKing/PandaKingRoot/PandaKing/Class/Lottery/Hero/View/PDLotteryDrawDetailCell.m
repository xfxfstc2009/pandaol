//
//  PDLotteryDrawDetailCell.m
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryDrawDetailCell.h"

@interface PDLotteryDrawDetailCell ()
@property (nonatomic, strong) UILabel *issueLabel;  // 期号
@property (nonatomic, strong) UILabel *numLabel;    /**< 开奖号码*/

@property (nonatomic, assign) CGFloat rowHeight;
@end

@implementation PDLotteryDrawDetailCell

+ (CGFloat)cellHeight {
    return 44;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        _rowHeight = [PDLotteryDrawDetailCell cellHeight];
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    self.issueLabel = [[UILabel alloc] init];
    self.issueLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.issueLabel.textColor = c28;
    [self.contentView addSubview:self.issueLabel];
    
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = self.issueLabel.font;
    self.numLabel.textColor = c9;
    [self.contentView addSubview:self.numLabel];
    
    CGFloat imagewidth = 34;
    CGFloat space = ([@"幸运英雄" sizeWithCalcFont:[UIFont systemFontOfCustomeSize:14]].width - imagewidth) / 2;
    CGFloat x = kScreenBounds.size.width - kTableViewSectionHeader_left - space - imagewidth;
    self.headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake(x, (_rowHeight - imagewidth) / 2, imagewidth, imagewidth)];
    self.headerImageView.userInteractionEnabled = YES;
    self.headerImageView.layer.cornerRadius = imagewidth / 2;
    self.headerImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.headerImageView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickHero)];
    [self.headerImageView addGestureRecognizer:tap];
}

#pragma mark - 控件方法

- (void)didClickHero {
    if ([self.delegate respondsToSelector:@selector(lotteryCell:didClickHeroImageView:atRow:)]) {
        [self.delegate lotteryCell:self didClickHeroImageView:self.headerImageView atRow:self.indexPath.row - 1];
    }
}

#pragma mark - set

- (void)setModel:(PDLotteryLotteriesItem *)model {
    _model = model;
    
    self.issueLabel.text = [NSString stringWithFormat:@"第%ld期",(long)model.shishicaiInfo.num];
    if (model.draw) { // 开奖了
        self.numLabel.text = [NSString stringWithFormat:@"%@",model.shishicaiInfo.drawCode];
        if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
            [self.headerImageView uploadImageWithAvatarURL:[PDCenterTool absoluteUrlWithRisqueUrl:self.model.winPandaLOLChampion.champion.avatar] placeholder:nil callback:NULL];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
            [self.headerImageView uploadImageWithAvatarURL:[PDCenterTool absoluteUrlWithRisqueUrl:self.model.winPandaDota2Champion.champion.avatar] placeholder:nil callback:NULL];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
            [self.headerImageView uploadImageWithAvatarURL:[PDCenterTool absoluteUrlWithRisqueUrl:self.model.winPandaKingChampion.champion.avatar] placeholder:nil callback:NULL];
        }
        
    } else {
        self.numLabel.text = @"正在开奖中...";
        self.headerImageView.image = [UIImage imageNamed:@"icon_hero_wait"];
    }
    
    
    CGSize issueSize = [self.issueLabel.text sizeWithCalcFont:self.issueLabel.font];
    CGSize numSize = [self.numLabel.text sizeWithCalcFont:self.numLabel.font];
    
    self.issueLabel.frame = CGRectMake(kTableViewSectionHeader_left + LCFloat(5), (_rowHeight - issueSize.height) / 2, issueSize.width, issueSize.height);
    self.numLabel.frame = CGRectMake((kScreenBounds.size.width - numSize.width) / 2, (_rowHeight - numSize.height) / 2, numSize.width, numSize.height);
}

@end
