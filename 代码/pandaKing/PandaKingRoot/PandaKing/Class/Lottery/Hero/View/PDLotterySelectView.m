//
//  PDLotterySelectView.m
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotterySelectView.h"

static const int descViewHeight   = 26;
static const int buttonViewHeight = 60;

@interface PDLotterySelectView ()
@property (nonatomic, assign) CGFloat descViewHeight;
@property (nonatomic, assign) CGFloat buttonViewHeight;

@property (nonatomic, strong) PDLotterySelectedButton *selectedRow1Button;
@property (nonatomic, strong) PDLotterySelectedButton *selectedRow2Button;
@property (nonatomic, strong) PDLotterySelectedButton *selectedRow3Button;
@property (nonatomic, strong) UIButton *nextButton;

@end

@implementation PDLotterySelectView

+ (CGFloat)viewHeight {
    return (descViewHeight + buttonViewHeight) * 3 + 44 + kTitleBarheight;
}

- (PDLotterySelectedGoldView *)goldView {
    if (!_goldView) {
        _goldView = [[PDLotterySelectedGoldView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width, kScreenBounds.size.height - [PDLotterySelectView viewHeight], kScreenBounds.size.width, [PDLotterySelectView viewHeight])];
        [self addSubview:_goldView];
        _goldView.title = @"确定竞猜";
    }
    __weak typeof(self) weakSelf = self;
    [_goldView cancleWithCancleBlock:^{
        [UIView animateWithDuration:weakSelf.duration animations:^{
            _goldView.frame = CGRectOffset(_goldView.frame, 0, _goldView.frame.size.height);
        }];
        [weakSelf dismissWithAnimated:YES];
    }];
    [_goldView backWithBackBlock:^{
        [weakSelf changeToSelected];
    }];
    return _goldView;
}

- (instancetype)init {
    self = [super init];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    self.selectedRow1Model = [[PDLottryButtonModel alloc] init];
    self.selectedRow2Model = [[PDLottryButtonModel alloc] init];
    self.selectedRow3Model = [[PDLottryButtonModel alloc] init];
    
    _descViewHeight = descViewHeight;
    _buttonViewHeight = buttonViewHeight;
}

- (void)pageSetting {
    self.canTouchedCancle = NO;
    
    UIView *putView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, (_descViewHeight + _buttonViewHeight) * 3 + 49)];
    putView.backgroundColor = [UIColor whiteColor];
    self.putView = putView;
    
    // 金币
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        UIView *goldView = [self descViewSettingWithTitle:@"金币价格"];
        goldView.frame = CGRectMake(0, 0, kScreenBounds.size.width, _descViewHeight);
        // 金币 － 选项
        UIView *goldButtonView = [self buttonViewSettingWithTitleArr:@[@"3150及以下",@"4800",@"6300及以上"] atRow:0];
        goldButtonView.frame = CGRectMake(0, CGRectGetMaxY(goldView.frame), kScreenBounds.size.width, _buttonViewHeight);
        // 远程
        UIView *disView = [self descViewSettingWithTitle:@"远程or近战"];
        disView.frame = CGRectMake(0, CGRectGetMaxY(goldButtonView.frame), kScreenBounds.size.width, _descViewHeight);
        // 远程 － 选择
        UIView *disButtonView = [self buttonViewSettingWithTitleArr:@[@"远程", @"近战"] atRow:1];
        disButtonView.frame = CGRectMake(0, CGRectGetMaxY(disView.frame), kScreenBounds.size.width, _buttonViewHeight);
        // 称呼字数
        UIView *lengthView = [self descViewSettingWithTitle:@"称呼字数"];
        lengthView.frame = CGRectMake(0, CGRectGetMaxY(disButtonView.frame), kScreenBounds.size.width, _descViewHeight);
        // 称呼字数 － 选择
        UIView *lengthButtonView = [self buttonViewSettingWithTitleArr:@[@"两个及以下",@"三个",@"四个及以上"] atRow:2];
        lengthButtonView.frame = CGRectMake(0, CGRectGetMaxY(lengthView.frame), kScreenBounds.size.width, _buttonViewHeight);
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        UIView *goldView = [self descViewSettingWithTitle:@"阵营"];
        goldView.frame = CGRectMake(0, 0, kScreenBounds.size.width, _descViewHeight);
        // 金币 － 选项
        UIView *goldButtonView = [self buttonViewSettingWithTitleArr:@[@"天辉",@"夜魇"] atRow:0];
        goldButtonView.frame = CGRectMake(0, CGRectGetMaxY(goldView.frame), kScreenBounds.size.width, _buttonViewHeight);
        // 远程
        UIView *disView = [self descViewSettingWithTitle:@"远程or近战"];
        disView.frame = CGRectMake(0, CGRectGetMaxY(goldButtonView.frame), kScreenBounds.size.width, _descViewHeight);
        // 远程 － 选择
        UIView *disButtonView = [self buttonViewSettingWithTitleArr:@[@"远程", @"近战"] atRow:1];
        disButtonView.frame = CGRectMake(0, CGRectGetMaxY(disView.frame), kScreenBounds.size.width, _buttonViewHeight);
        // 称呼字数
        UIView *lengthView = [self descViewSettingWithTitle:@"主属性"];
        lengthView.frame = CGRectMake(0, CGRectGetMaxY(disButtonView.frame), kScreenBounds.size.width, _descViewHeight);
        // 称呼字数 － 选择
        UIView *lengthButtonView = [self buttonViewSettingWithTitleArr:@[@"力量",@"敏捷",@"智力"] atRow:2];
        lengthButtonView.frame = CGRectMake(0, CGRectGetMaxY(lengthView.frame), kScreenBounds.size.width, _buttonViewHeight);
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        // 远程
        UIView *disView = [self descViewSettingWithTitle:@"远程or近战"];
        disView.frame = CGRectMake(0, 0, kScreenBounds.size.width, _descViewHeight);
        // 远程 － 选择
        UIView *disButtonView = [self buttonViewSettingWithTitleArr:@[@"远程", @"近战"] atRow:0];
        disButtonView.frame = CGRectMake(0, CGRectGetMaxY(disView.frame), kScreenBounds.size.width, _buttonViewHeight);
        // 称呼字数
        UIView *lengthView = [self descViewSettingWithTitle:@"称呼字数"];
        lengthView.frame = CGRectMake(0, CGRectGetMaxY(disButtonView.frame), kScreenBounds.size.width, _descViewHeight);
        // 称呼字数 － 选择
        UIView *lengthButtonView = [self buttonViewSettingWithTitleArr:@[@"两个及以下",@"三个",@"四个及以上"] atRow:1];
        lengthButtonView.frame = CGRectMake(0, CGRectGetMaxY(lengthView.frame), kScreenBounds.size.width, _buttonViewHeight);
    }
    
    UIButton *nextButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nextButton.frame = CGRectMake(0, (_descViewHeight + _buttonViewHeight) * 3, kScreenBounds.size.width, 49);
    nextButton.backgroundColor = [UIColor lightGrayColor];
    nextButton.enabled = NO;
    [nextButton setTitle:@"下一步" forState:UIControlStateNormal];
    [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [nextButton addTarget:self action:@selector(didClickNextButton:) forControlEvents:UIControlEventTouchUpInside];
    [putView addSubview:nextButton];
    self.nextButton = nextButton;
    
    [self showWithAnimationComplication:NULL];
}

- (UIView *)descViewSettingWithTitle:(NSString *)title {
    UIView *descView = [[UIView alloc] init];
    descView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.putView addSubview:descView];
    UILabel *descLabel = [[UILabel alloc] init];
    descLabel.text = title;
    descLabel.textColor = [UIColor grayColor];
    descLabel.font = [UIFont systemFontOfCustomeSize:12];
    CGSize goldSize = [descLabel.text sizeWithCalcFont:descLabel.font];
    descLabel.frame = CGRectMake(kTableViewSectionHeader_left, (_descViewHeight - goldSize.height) / 2, goldSize.width, goldSize.height);
    [descView addSubview:descLabel];
    return descView;
}

- (UIView *)buttonViewSettingWithTitleArr:(NSArray *)titles atRow:(NSInteger)row {
    
    CGFloat buttonSpace = LCFloat(20);
    CGFloat buttonWidth = (kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - 2 * buttonSpace) / 3;
    CGFloat buttonHeight = 32;
    
    UIView *buttonView = [[UIView alloc] init];
    buttonView.tag = row;
    for (int i = 1; i <= titles.count; i++) {
        PDLotterySelectedButton *button = [[PDLotterySelectedButton alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left + (i - 1) * (buttonSpace + buttonWidth), (_buttonViewHeight - buttonHeight) / 2, buttonWidth, buttonHeight)];
        [button setTitle:titles[i-1] forState:UIControlStateNormal];
        PDLottryButtonModel *indexModel = [[PDLottryButtonModel alloc] init];
        indexModel.index = i;
        indexModel.row = row;
        button.indexModel = indexModel;
        [button addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
        [buttonView addSubview:button];
    }
    [self.putView addSubview:buttonView];
    return buttonView;
}

- (void)setButtonTitle:(NSString *)buttonTitle {
    _buttonTitle = buttonTitle;
    
    [self.nextButton setTitle:buttonTitle forState:UIControlStateNormal];
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    
    if (isSelected == NO) {
        self.nextButton.enabled = NO;
        self.nextButton.backgroundColor = [UIColor lightGrayColor];
    } else {
        self.nextButton.enabled = YES;
        self.nextButton.backgroundColor = [UIColor blackColor];
    }
}

#pragma mark - 控件方法

- (PDLotterySelectedButton *)buttonAtRow:(NSInteger)row index:(NSInteger)index {
    UIView *buttonView = [self.putView viewWithTag:row];
    for (UIView *subview in buttonView.subviews) {
        if ([subview isKindOfClass:[PDLotterySelectedButton class]]) {
            PDLotterySelectedButton *button = (PDLotterySelectedButton *)subview;
            return button;
        }
    }
    return nil;
}

- (void)setSelectedAtRow:(NSInteger)row index:(NSInteger)index {
    [self didClickButton:[self buttonAtRow:row index:index]];
}

- (void)didClickButton:(PDLotterySelectedButton *)button {
    
    if (button.indexModel.row == 0) {
        if (self.selectedRow1Button != button) {
            self.selectedRow1Button.selected = NO;
        }
        
        button.selected = !button.selected;
        [self.selectedRow1Button changeButtonState];
        self.selectedRow1Button = button;
        [button changeButtonState];
        
        if (button.selected) {
            self.selectedRow1Model.row = 0;
            self.selectedRow1Model.index = button.indexModel.index;
        } else {
            self.selectedRow1Model.row = 0;
            self.selectedRow1Model.index = 0;
        }
    } else if (button.indexModel.row == 1) {
        if (self.selectedRow2Button != button) {
            self.selectedRow2Button.selected = NO;
        }
        button.selected = !button.selected;
        [self.selectedRow2Button changeButtonState];
        self.selectedRow2Button = button;
        [button changeButtonState];
        
        if (button.selected) {
            self.selectedRow2Model.row = 0;
            self.selectedRow2Model.index = button.indexModel.index;
        } else {
            self.selectedRow2Model.row = 0;
            self.selectedRow2Model.index = 0;
        }
    } else if (button.indexModel.row == 2) {
        if (self.selectedRow3Button != button) {
            self.selectedRow3Button.selected = NO;
        }
        button.selected = !button.selected;
        [self.selectedRow3Button changeButtonState];
        self.selectedRow3Button = button;
        [button changeButtonState];
        
        if (button.selected) {
            self.selectedRow3Model.row = 0;
            self.selectedRow3Model.index = button.indexModel.index;
        } else {
            self.selectedRow3Model.row = 0;
            self.selectedRow3Model.index = 0;
        }
    }
    
    
    // 至少得选一个
    if (self.selectedRow1Model.index == 0 && self.selectedRow2Model.index == 0 && self.selectedRow3Model.index == 0) {
        self.isSelected = NO;
    } else {
        self.isSelected = YES;
    }
    
    if ([self.delegate respondsToSelector:@selector(lotterySelectView:didSelectedAtRow1:row2:row3:)]) {
        [self.delegate lotterySelectView:self didSelectedAtRow1:self.selectedRow1Model.index row2:self.selectedRow2Model.index row3:self.selectedRow3Model.index];
    }
}

- (void)didClickNextButton:(UIButton *)button {
    if ([self.delegate respondsToSelector:@selector(lotterySelectView:didClickNextButton:)]) {
        [self.delegate lotterySelectView:self didClickNextButton:button];
    }
}

#pragma mark - 视图切换
/** 切换到投入*/
- (void)changeToInput {
    [UIView animateWithDuration:.3 animations:^{
        self.contentBar.frame = CGRectOffset(self.contentBar.frame, -kScreenBounds.size.width, 0);
        self.goldView.frame = CGRectOffset(self.goldView.frame, -kScreenBounds.size.width, 0);
    }];
}
/** 切换到选择*/
- (void)changeToSelected {
    [UIView animateWithDuration:.3 animations:^{
        self.contentBar.frame = CGRectOffset(self.contentBar.frame, kScreenBounds.size.width, 0);
        self.goldView.frame = CGRectOffset(self.goldView.frame, kScreenBounds.size.width, 0);
    }];
}

#pragma mark - 移除goldView

- (void)dismissGoldView {
    [UIView animateWithDuration:self.duration animations:^{
        _goldView.frame = CGRectOffset(_goldView.frame, 0, _goldView.frame.size.height);
    }];
    [self dismissWithAnimated:YES];
}

@end
