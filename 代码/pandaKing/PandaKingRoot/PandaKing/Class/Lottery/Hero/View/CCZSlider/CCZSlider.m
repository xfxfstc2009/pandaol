//
//  CCZSlider.m
//  CCZSlider
//
//  Created by 金峰 on 2016/11/5.
//  Copyright © 2016年 金峰. All rights reserved.
//

#import "CCZSlider.h"

@interface CCZSlider ()
@property (nonatomic, strong) UIView *scaleView;
@end

@implementation CCZSlider

- (instancetype)init {
   self = [super init];
    if (self) {
        [self addChange];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self addChange];
    }
    return self;
}

- (UIView *)scaleView {
    if (!_scaleView) {
        _scaleView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.frame) / 2 + 1 + _scaleOffsetY, CGRectGetWidth(self.frame), 30)];
        _scaleView.backgroundColor = [UIColor clearColor];
        
        int division = _divisionValue == 0? 0.5 : _divisionValue;
        int d = (int)self.maximumValue / division;
        CGFloat w = CGRectGetWidth(self.frame);
        CGFloat cw = w * ((d * division / self.maximumValue));
        for (int i = 0; i <= d; i++) {
            CALayer *line = [CALayer layer];
            line.backgroundColor = self.scaleColor? self.scaleColor.CGColor : [UIColor blackColor].CGColor;
            [_scaleView.layer addSublayer:line];
            
            if (_scaleVale != 0 && i * division % _scaleVale == 0) {
                line.frame = CGRectMake(i * (cw / d) - 0.5, 0, 1, 15);
            } else {
                line.frame = CGRectMake(i * (cw / d) - 0.5, 0, 1, 10);
            }
        }
    }
    return _scaleView;
}

- (void)addChange {
    [self addTarget:self action:@selector(sliderValueDidChange) forControlEvents:UIControlEventValueChanged];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
// 在视图被渲染的时候才执行
- (void)drawRect:(CGRect)rect {
    NSAssert(_scaleVale == 0 || (_scaleVale % _divisionValue) == 0, @"刻度值scaleValue设定必须为divisionValue的倍数！");
    NSAssert(_scaleVale == 0 || _scaleVale > _divisionValue, @"刻度值scaleValue必须大于分度值divisionValue");
    
    if (_scale == YES) {
        [self addSubview:self.scaleView];
        [self sendSubviewToBack:self.scaleView];
    }
}

- (void)sliderValueDidChange {
    [self setValue:(int)self.value / _divisionValue * _divisionValue animated:YES];
}

#pragma mark -- set

- (void)setScale:(BOOL)scale {
    _scale = scale;
    
    if (scale) {
        _scaleView.hidden = NO;
    } else {
        _scaleView.hidden = YES;
    }
}

@end
