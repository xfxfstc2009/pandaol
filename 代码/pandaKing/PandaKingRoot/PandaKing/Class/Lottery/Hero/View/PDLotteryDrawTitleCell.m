//
//  PDLotteryDrawTitleCell.m
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryDrawTitleCell.h"

@interface PDLotteryDrawTitleCell ()
@property (nonatomic, strong) UILabel *leftLabel;
@property (nonatomic, strong) UILabel *centerLabel;
@property (nonatomic, strong) UILabel *rightLabel;

@property (nonatomic, strong) UIFont *font;
@property (nonatomic, strong) UIColor *textColor;
@end

@implementation PDLotteryDrawTitleCell

+ (CGFloat)cellHeight {
    return 32;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _font = [[UIFont systemFontOfCustomeSize:14] boldFont];
        _textColor = c3;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self pageSetting];;
    }
    return self;
}

- (void)pageSetting {
    self.leftLabel = [self labelSetting];
    self.centerLabel = [self labelSetting];
    self.rightLabel = [self labelSetting];
}

- (UILabel *)labelSetting {
    UILabel *label = [[UILabel alloc] init];
    label.font = _font;
    label.textColor = _textColor;
    [self.contentView addSubview:label];
    return label;
}

- (void)setLeftDesc:(NSString *)leftDesc {
    _leftDesc = leftDesc;
    self.leftLabel.text = leftDesc;
    
    CGSize size = [leftDesc sizeWithCalcFont:_font];
    self.leftLabel.frame = CGRectMake(kTableViewSectionHeader_left, ([PDLotteryDrawTitleCell cellHeight] - size.height) / 2, size.width, size.height);
}

- (void)setCenterDesc:(NSString *)centerDesc {
    _centerDesc = centerDesc;
    self.centerLabel.text = centerDesc;
    
    CGSize size = [centerDesc sizeWithCalcFont:_font];
    self.centerLabel.frame = CGRectMake((kScreenBounds.size.width - size.width) / 2, ([PDLotteryDrawTitleCell cellHeight] - size.height) / 2, size.width, size.height);
}

- (void)setRightDesc:(NSString *)rightDesc {
    _rightDesc = rightDesc;
    self.rightLabel.text = rightDesc;
    
    CGSize size = [rightDesc sizeWithCalcFont:_font];
    self.rightLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - size.width, ([PDLotteryDrawTitleCell cellHeight] - size.height) / 2, size.width, size.height);
}

@end
