//
//  PDLotteryHistoryPieCell.h
//  HistoryDemo
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryDailyStatisticList.h"

/// 饼状图的cell
@interface PDLotteryHistoryPieCell : UICollectionViewCell
@property (nonatomic, strong) PDLotteryDailyStatisticList *model;
@end
