//
//  PDLotteryHeroParticipationView.h
//  PandaKing
//
//  Created by Cranz on 16/12/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryHeroDetail.h"

@interface PDLotteryHeroParticipationView : UIView
@property (nonatomic, strong) PDLotteryHeroDetail *model;
+ (CGFloat)cellHeight;
@end
