//
//  PDLotteryTimeLabel.m
//  PandaKing
//
//  Created by Cranz on 16/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryTimeLabel.h"

#define kDAY    86400
#define kHOUR   3600
#define kMINUTE 60

typedef void(^resultBlock)(CGSize size, NSString *dateString);
@interface PDLotteryTimeLabel ()
@property (nonatomic, weak) NSTimer *timer;
@property (nonatomic, copy) NSString *timeStr;
@property (nonatomic, copy) resultBlock block;
@property (nonatomic, copy) resultBlock textBlock;
@property (nonatomic, assign) NSTimeInterval timestamp; /**< 校准后的未来某一天*/
@end

@implementation PDLotteryTimeLabel

- (void)clearTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (instancetype)init {
    self = [super init];
    if (self && !_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
        NSRunLoop *currentRunLoop = [NSRunLoop currentRunLoop];
        [currentRunLoop addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return self;
}

- (void)timeChange {
    NSInteger surplusTimeStamp = (NSInteger)(_timestamp - [NSDate date].timeIntervalSince1970);
    
    self.text = [self getTimeDescWithTimeStamp:surplusTimeStamp];
    CGSize size = [self.text sizeWithAttributes:@{NSFontAttributeName: self.font}];
    CGSize textSize = [self.timeStr sizeWithAttributes:@{NSFontAttributeName: self.font}];
    if (self.block){
        self.block(size, self.text);
    }
    if (self.textBlock) {
        self.textBlock(textSize, self.timeStr);
    }
}

- (NSString *)getTimeDescWithTimeStamp:(NSInteger)timeStamp {
    
    NSString *timeDesc = @"";
    
    if (timeStamp <= 0) {
        self.timeStr = @"00:00";
        timeDesc = [NSString stringWithFormat:@"%@ %@",self.prefixStr? self.prefixStr : @"",self.timeStr];
        self.text = timeDesc;
        CGSize size = [timeDesc sizeWithAttributes:@{NSFontAttributeName: self.font}];
        if (self.block){
            self.block(size, timeDesc);
        }
        
        return timeDesc;
    } else {
//        int days_ = (int)(timeStamp / kDAY);
        int hours_ = (int)(timeStamp % kDAY / kHOUR);
        int minutes_ = (int)((timeStamp % kHOUR) / kMINUTE);
        int seconds_ = (int)(timeStamp % kMINUTE);
        if (hours_ > 0) {
            self.timeStr = [NSString stringWithFormat:@"%02d:%02d:%02d",hours_ , minutes_,seconds_];
            timeDesc = [NSString stringWithFormat:@"%@ %@",self.prefixStr? self.prefixStr : @"",self.timeStr];
        } else {
            self.timeStr = [NSString stringWithFormat:@"%02d:%02d",minutes_,seconds_];
            timeDesc = [NSString stringWithFormat:@"%@ %@",self.prefixStr? self.prefixStr : @"", self.timeStr];
        }
        
        return timeDesc;
    }
}

- (void)countDownWithTimeNow:(NSTimeInterval)n timeEnd:(NSTimeInterval)e resultBlock:(void (^)(CGSize, NSString *))resultBlock {
    if (!resultBlock) {
        return;
    }
    self.block = resultBlock;
    
    _timestamp = e / n * ([NSDate date].timeIntervalSince1970);
    
    self.text = [self getTimeDescWithTimeStamp:(NSInteger)(_timestamp - [NSDate date].timeIntervalSince1970)];
    CGSize size = [self.text sizeWithAttributes:@{NSFontAttributeName: self.font}];
    self.block(size, self.text);
}

- (void)timeLabelTextBlock:(void (^)(CGSize ,NSString *))textBlock {
    if (textBlock) {
        self.textBlock = textBlock;
    }
}

@end
