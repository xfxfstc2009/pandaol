//
//  PDLotteryHistoryPieCell.m
//  HistoryDemo
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import "PDLotteryHistoryPieCell.h"
#import "CCZShapeView.h"
#import "PDLotteryPercentView.h"

@interface PDLotteryHistoryPieCell ()
@property (nonatomic, strong) CCZShapeView *shapeView;
@property (nonatomic, strong) PDLotteryPercentView *percentView1;
@property (nonatomic, strong) PDLotteryPercentView *percentView2;
@property (nonatomic, strong) PDLotteryPercentView *percentView3;
@property (nonatomic, assign) CGSize size;
@end

@implementation PDLotteryHistoryPieCell

- (PDLotteryPercentView *)percentView1 {
    if (!_percentView1) {
        _percentView1 = [[PDLotteryPercentView alloc] initWithFrame:CGRectMake(0, _size.width + LCFloat(20), _size.width, [PDLotteryPercentView viewHeight])];
        [self addSubview:_percentView1];
    }
    return _percentView1;
}

- (PDLotteryPercentView *)percentView2 {
    if (!_percentView2) {
        _percentView2 = [[PDLotteryPercentView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.percentView1.frame) + LCFloat(2), _size.width, [PDLotteryPercentView viewHeight])];
        [self addSubview:_percentView2];
    }
    return _percentView2;
}

- (PDLotteryPercentView *)percentView3 {
    if (!_percentView3) {
        _percentView3 = [[PDLotteryPercentView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.percentView2.frame) + LCFloat(2), _size.width, [PDLotteryPercentView viewHeight])];
        [self addSubview:_percentView3];
    }
    return _percentView3;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    _size = frame.size;
//    [self pageSetting];
    return self;
}

- (void)pageSetting {
    
}

- (void)setModel:(PDLotteryDailyStatisticList *)model {
    if (_model) {
        return;
    }
    
    _model = model;
    
    if (!self.shapeView) {
        self.shapeView = [[CCZShapeView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.width) itemsNum:model.itemList.count];
        if (model.itemList.count == 2) {
           self.shapeView.colors = @[c20,c19];
        } else {
            self.shapeView.colors = @[c20,c19,c18];
        }
        [self.contentView addSubview:self.shapeView];
    }
    
    self.shapeView.desc = model.itemTag;
    
    NSMutableArray *arr = [NSMutableArray arrayWithCapacity:model.itemList.count];
    for (int i = 0; i < model.itemList.count; i++) {
        PDLotteryDailyStatisticItem *item = model.itemList[i];
        [arr addObject:@(item.percent.floatValue)];
        
        if (item.op == 1) {
            PDLotteryPercentModel *model = [[PDLotteryPercentModel alloc] init];
            model.tagColor = c20;
            model.percent = item.percent.floatValue;
            model.count = item.count;
            
            if ([self.model.itemTag isEqualToString:@"goldPrice"]) {
                model.desc = @"3150及以下";
            } else if ([self.model.itemTag isEqualToString:@"nickLength"]) {
                model.desc = @"两个及以下";
            } else if ([self.model.itemTag isEqualToString:@"far"]) {
                model.desc = @"远程";
            } else if ([self.model.itemTag isEqualToString:@"camp"]) {
                model.desc = @"天辉";
            } else if ([self.model.itemTag isEqualToString:@"mainProperty"]) {
                model.desc = @"力量";
            } else if ([self.model.itemTag isEqualToString:@"nameLength"]) {
                model.desc = @"两个及以下";
            }
            
            self.percentView1.model = model;
        } else if (item.op == 2) {
            PDLotteryPercentModel *model = [[PDLotteryPercentModel alloc] init];
            model.tagColor = c19;
            model.percent = item.percent.floatValue;
            model.count = item.count;
            if ([self.model.itemTag isEqualToString:@"goldPrice"]) {
                model.desc = @"4800";
            } else if ([self.model.itemTag isEqualToString:@"nickLength"]) {
                model.desc = @"三个";
            } else if ([self.model.itemTag isEqualToString:@"far"]) {
                model.desc = @"近战";
            } else if ([self.model.itemTag isEqualToString:@"camp"]) {
                model.desc = @"夜魇";
            } else if ([self.model.itemTag isEqualToString:@"mainProperty"]) {
                model.desc = @"敏捷";
            } else if ([self.model.itemTag isEqualToString:@"nameLength"]) {
                model.desc = @"三个";
            }
            
            self.percentView2.model = model;
        } else {
            PDLotteryPercentModel *model = [[PDLotteryPercentModel alloc] init];
            model.tagColor = c18;
            model.percent = item.percent.floatValue;
            model.count = item.count;
            if ([self.model.itemTag isEqualToString:@"goldPrice"]) {
                model.desc = @"6300及以上";
            } else if ([self.model.itemTag isEqualToString:@"nickLength"]) {
                model.desc = @"四个及以上";
            } else if ([self.model.itemTag isEqualToString:@"mainProperty"]) {
                model.desc = @"智力";
            } else if ([self.model.itemTag isEqualToString:@"nameLength"]) {
                model.desc = @"四个及以上";
            }
            
            self.percentView3.model = model;
        }
    }
    
    __weak typeof(self) weakSelf = self;
    [self.shapeView setItemsPercentage:[arr copy] lineStrokeComplication:^(NSArray *arr) {
        for (int i = 0; i < arr.count; i ++) {
            [weakSelf drawLineWithPointCenter:[arr[i] CGPointValue] atIndex:i];
        }
    }];
}

- (void)drawLineWithPointCenter:(CGPoint)p atIndex:(int)i {
    // 小白点
    UIView *redView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 5)];
    redView.backgroundColor = [UIColor whiteColor];
    redView.layer.cornerRadius = 5 / 2;
    redView.clipsToBounds = YES;
    redView.center = p;
    [self.contentView addSubview:redView];
    
    // 划线
    CGPoint p2 = CGPointZero, p3 = CGPointZero;
    CGFloat b = 0; // 这个是来调整正负的
    if (p.x > self.shapeView.c.x && p.y > self.shapeView.c.y) {
        p2 = CGPointMake(p.x + LCFloat(10), p.y + LCFloat(10));
        p3 = CGPointMake(p2.x + LCFloat(70), p2.y);
        b = -1;
    } else if (p.x > self.shapeView.c.x && p.y <= self.shapeView.c.y) {
        p2 = CGPointMake(p.x + LCFloat(10), p.y - LCFloat(10));
        p3 = CGPointMake(p2.x + LCFloat(70), p2.y);
        b = -1;
    } else if (p.x <= self.shapeView.c.x && p.y > self.shapeView.c.y) {
        p2 = CGPointMake(p.x - LCFloat(10), p.y + LCFloat(10));
        p3 = CGPointMake(p2.x - LCFloat(70), p2.y);
        b = 0;
    } else {
        p2 = CGPointMake(p.x - LCFloat(10), p.y - LCFloat(10));
        p3 = CGPointMake(p2.x - LCFloat(70), p2.y);
        b = 0;
    }
    
    UIBezierPath *path = [UIBezierPath bezierPath];
    [path moveToPoint:p];
    [path addLineToPoint:p2];
    [path addLineToPoint:p3];
    
    CAShapeLayer *line = [CAShapeLayer layer];
    line.path = path.CGPath;
    line.lineWidth = 1;
    line.strokeColor = c1.CGColor;
    line.fillColor = [UIColor clearColor].CGColor;
    [self.contentView.layer addSublayer:line];
    
    // 线上的desc
    UILabel *topLabel = [[UILabel alloc] init];
    PDLotteryDailyStatisticItem *item = self.model.itemList[i];
    topLabel.text = [NSString stringWithFormat:@"%d％",(int)(item.percent.floatValue * 100)];
    topLabel.font = [UIFont systemFontOfCustomeSize:10];
    topLabel.textColor = c1;
    CGSize topSize = [topLabel.text sizeWithCalcFont:topLabel.font];
    topLabel.frame = CGRectMake(p3.x + b * topSize.width, p3.y - 1.5 - topSize.height, topSize.width, topSize.height);
    [self.contentView addSubview:topLabel];
    
    UILabel *bottomLabel = [[UILabel alloc] init];
    bottomLabel.text = [PDCenterTool lotteryItemWithTag:self.model.itemTag atIndex:i + 1];
    bottomLabel.font = topLabel.font;
    bottomLabel.textColor = c1;
    CGSize bottomSize = [bottomLabel.text sizeWithCalcFont:topLabel.font];
    bottomLabel.frame = CGRectMake(p3.x + b * bottomSize.width, p3.y + 1.5, bottomSize.width, bottomSize.height);
    [self.contentView addSubview:bottomLabel];
}

@end
