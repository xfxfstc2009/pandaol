//
//  CCZBrowseView.m
//  HistoryDemo
//
//  Created by Cranz on 16/11/11.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import "CCZBrowseView.h"
#import "PDLotteryHistoryPieCell.h"

#define kCCZBROSCREEN_BOUNDS [[UIScreen mainScreen] bounds]

#pragma mark -
#pragma mark -- 自定义的layout布局

@interface CCZFlowLayout : UICollectionViewFlowLayout
@property (nonatomic, assign) CGFloat s;
@end

@implementation CCZFlowLayout

- (instancetype)initWithScale:(CGFloat)scale {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _s = scale;
    return self;
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds {
    return YES;
}

-(NSArray *)layoutAttributesForElementsInRect:(CGRect)rect {
    
    NSArray *allAttributes = [super layoutAttributesForElementsInRect:rect];
    
    CGRect greenRect;
    greenRect.origin = self.collectionView.contentOffset;
    greenRect.size = self.collectionView.bounds.size;

    CGFloat greenCenter = CGRectGetMidX(greenRect);
    
    CGFloat scale_ = _s - 1;
    for (UICollectionViewLayoutAttributes *attr in allAttributes) {
        CGFloat redCenter = attr.center.x;
        CGFloat distance = greenCenter-redCenter;
        CGFloat zoom = distance / (kCCZBROSCREEN_BOUNDS.size.width / 2);
        if (ABS(distance) <= (kCCZBROSCREEN_BOUNDS.size.width / 2)) {
            CGFloat scale = 1 + scale_ * (1 - ABS(zoom));
            attr.transform3D = CATransform3DMakeScale(scale, scale, 1);
        }
    }

    return allAttributes;
}

@end


#define kBROWSE_Y (50)
#define kCELL_IDENTIFIER @"browsecellid"

typedef void(^indexBlock)(NSUInteger index);
@interface CCZBrowseView()<UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) UIImageView *backgroundImageView;

@property (nonatomic, assign) CGSize mainSize;
@property (nonatomic, assign) Class cls;
@property (nonatomic, copy)   indexBlock indexBlock_;
@end

@implementation CCZBrowseView

- (UICollectionView *)mainView {
    if (!_mainView) {
        
        _scale = _scale == 0? 1 : _scale;
        
        CCZFlowLayout *layout = [[CCZFlowLayout alloc] initWithScale:_scale];
        layout.itemSize = (_itemSize.width == 0 && _itemSize.height == 0)? CGSizeMake(_mainSize.height - 2 * kBROWSE_Y, _mainSize.height - 2 * kBROWSE_Y) : _itemSize;
        layout.minimumLineSpacing = _itemSpace;
        layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
        layout.sectionInset = UIEdgeInsetsMake(15, _leftRightSpace, 0, _leftRightSpace);
        _mainView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:layout];
        _mainView.backgroundColor = [UIColor clearColor];
        _mainView.delegate = self;
        _mainView.dataSource = self;
        _mainView.showsHorizontalScrollIndicator = NO;
        _mainView.showsVerticalScrollIndicator = NO;
        [_mainView registerClass:[_cls class] forCellWithReuseIdentifier:kCELL_IDENTIFIER];
    }
    return _mainView;
}

- (id)initBrowseViewWithFrame:(CGRect)frame registerCustomCell:(Class)cellCls {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    _cls = cellCls;
    _mainSize = self.frame.size;
    
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self addSubview:self.backgroundImageView];
    return self;
}

- (void)selectedAtIndex:(void (^)(NSUInteger))indexBlock {
    if (indexBlock) {
        _indexBlock_ = indexBlock;
    }
}

- (void)setBrowseSelectedAtIndex:(NSUInteger)index {
    if (!_wannaToTapScrollItem && index == 0) {
        return;
    }
    
    [self.mainView setContentOffset:CGPointMake(index * (_itemSize.width + _itemSpace) - (_mainSize.width - _itemSize.width) / 2 + _leftRightSpace, 0) animated:YES];
}

- (void)didMoveToSuperview {
    [self addSubview:self.mainView];
}

#pragma mark -- ##### UICollectionViewDelegate #####

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _itemNum;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PDLotteryHistoryPieCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCELL_IDENTIFIER forIndexPath:indexPath];
    
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        if (indexPath.row == 0) {
            cell.model = self.model.goldPrice;
        } else if (indexPath.row == 1) {
            cell.model = self.model.far;
        } else {
            cell.model = self.model.nickLength;
        }
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        if (indexPath.row == 0) {
            cell.model = self.model.camp;
        } else if (indexPath.row == 1) {
            cell.model = self.model.far;
        } else {
            cell.model = self.model.mainProperty;
        }
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        if (indexPath.row == 0) {
            cell.model = self.model.far;
        } else if (indexPath.row == 1) {
            cell.model = self.model.nameLength;
        }
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_indexBlock_) {
        _indexBlock_(indexPath.row);
    }
    
    if (!_wannaToTapScrollItem) {
        return;
    }
    
    [self setBrowseSelectedAtIndex:indexPath.row];
}

- (void)setModel:(PDLotteryDailyStatisticModel *)model {
    _model = model;
    
    [self.mainView reloadData];
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    _backgroundImage = backgroundImage;
    
    self.backgroundImageView.image = backgroundImage;
}

@end

