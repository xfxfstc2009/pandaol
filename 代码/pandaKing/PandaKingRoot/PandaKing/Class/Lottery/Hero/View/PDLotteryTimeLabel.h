//
//  PDLotteryTimeLabel.h
//  PandaKing
//
//  Created by Cranz on 16/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDLotteryTimeLabel : UILabel
@property (nonatomic, copy) NSString *prefixStr;

/** 传入服务器中未来某一天和当前时间，进行校准后再计算倒计时*/
- (void)countDownWithTimeNow:(NSTimeInterval)n timeEnd:(NSTimeInterval)e resultBlock:(void(^)(CGSize size, NSString *dateString))resultBlock;
/** 每次更新显示都会触发这个回调*/
- (void)timeLabelTextBlock:(void(^)(CGSize textSize,NSString *text))textBlock;

- (void)clearTimer;
@end
