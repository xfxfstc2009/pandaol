//
//  PDLotteryJoinerCell.h
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryJoinerItem.h"
#import "PDLotteryHeroDota2JoinerItem.h"
#import "PDLotteryHeroPvpJoinerItem.h"

@interface PDLotteryJoinerCell : UITableViewCell
@property (nonatomic, strong) PDLotteryJoinerItem *model;
@property (nonatomic, strong) PDLotteryHeroDota2JoinerItem *dota2Item;
@property (nonatomic, strong) PDLotteryHeroPvpJoinerItem *pvpItem;

+ (CGFloat)cellHeight;
@end
