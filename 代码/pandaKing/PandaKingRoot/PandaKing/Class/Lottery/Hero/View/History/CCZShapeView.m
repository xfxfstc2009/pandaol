//
//  CCZShapeView.m
//  HistoryDemo
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import "CCZShapeView.h"

static CGFloat const startAngle = 0;
static CGFloat const lineWidth = 15;
static NSTimeInterval const duration = 1;
typedef void(^comBlcok)(NSArray *arr);
@interface CCZShapeView ()
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UILabel *subDescLabel;
@property (nonatomic, assign) NSUInteger num;
@property (nonatomic, strong) CABasicAnimation *lineAnim;
@property (nonatomic, strong) NSMutableArray *layerArr;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) int index; // 执行到第几个
@property (nonatomic, copy)   comBlcok resultBlock;
@property (nonatomic, strong) NSArray *psercentArr;

// 画弧相关
@property (nonatomic, assign) CGFloat startAngle;

// 化纤
@property (nonatomic, strong) NSMutableArray *offsetArr;
@end

@implementation CCZShapeView

- (NSMutableArray *)offsetArr {
    if (!_offsetArr) {
        _offsetArr = [NSMutableArray array];
    }
    return _offsetArr;
}

- (CABasicAnimation *)lineAnim {
    if (!_lineAnim) {
        _lineAnim = [CABasicAnimation animationWithKeyPath:@"strokeEnd"];
        _lineAnim.duration = duration;
        _lineAnim.fromValue = @0;
        _lineAnim.toValue = @1;
        _lineAnim.delegate = self;
    }
    return _lineAnim;
}

- (instancetype)initWithFrame:(CGRect)frame itemsNum:(NSUInteger)num {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    [self pageSetting];
    
    self.r = self.frame.size.width / 2 - lineWidth / 2;
    self.c = CGPointMake(_r + lineWidth / 2, _r + lineWidth / 2);
    self.num = num;
    self.size = frame.size;
    self.startAngle = startAngle;
    
    self.layerArr = [NSMutableArray arrayWithCapacity:num];
    for (int i = 0; i < num; i++) {
        CAShapeLayer *layer = [CAShapeLayer layer];
        layer.lineWidth = lineWidth;
        [self.layer addSublayer:layer];
        [self.layerArr addObject:layer];
    }
    
    return self;
}

- (void)pageSetting {
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.descLabel.textColor = c1;
    [self addSubview:self.descLabel];
    
    self.subDescLabel = [[UILabel alloc] init];
    self.subDescLabel.font = [UIFont systemFontOfCustomeSize:8];
    self.subDescLabel.textColor = c1;
    [self addSubview:self.subDescLabel];
}

- (void)setItemsPercentage:(NSArray *)percentArr lineStrokeComplication:(void(^)(NSArray *arr))complication {
    
    self.index = 0;
    self.psercentArr = percentArr;
    if (complication) {
        self.resultBlock = complication;
    }
    
    [self startLineStrokeAnimAtIndex:self.index];
}

- (void)startLineStrokeAnimAtIndex:(int)i {
    CGFloat occupy = [self.psercentArr[i] floatValue];
    UIBezierPath *path = [self pathWithStartAngle:_startAngle endAngleOccupy:occupy];
    CAShapeLayer *layer = self.layerArr[i];
    
    layer.strokeColor = [self.colors[i] CGColor];
    layer.fillColor = [UIColor clearColor].CGColor;
    layer.path = path.CGPath;
    [layer addAnimation:self.lineAnim forKey:nil];
    
    CGFloat a = _startAngle + M_PI * 2 * occupy / 2;
    CGPoint p = CGPointMake(_r * cos(a) + _size.width / 2, _r * sin(a) + _size.height / 2);
    [self.offsetArr addObject:[NSValue valueWithCGPoint:p]];
 
    _startAngle += M_PI * 2 * occupy;
}

- (UIBezierPath *)pathWithStartAngle:(CGFloat)s endAngleOccupy:(CGFloat)o {
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:self.c radius:self.r startAngle:s endAngle:o * M_PI * 2 + s clockwise:YES];
    return path;
}

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    
    if (_index == _num - 1) {
        // 画院结束
        
        if (self.resultBlock) {
            self.resultBlock([self.offsetArr copy]);
        }
        return;
    }
    
    self.index++;
    [self startLineStrokeAnimAtIndex:self.index];
}

#pragma mark -- set

- (void)setDesc:(NSString *)desc {
    _desc = desc;
    
    if ([desc isEqualToString:@"far"]) {
        self.descLabel.text = @"近远程";
        self.subDescLabel.text = @"Attack distance";
    } else if ([desc isEqualToString:@"goldPrice"]) {
        self.descLabel.text = @"金币价格";
        self.subDescLabel.text = @"Gold price";
    } else if ([desc isEqualToString:@"mainProperty"]) {
        self.descLabel.text = @"主属性";
        self.subDescLabel.text = @"Main property";
    } else if ([desc isEqualToString:@"nameLength"]) {
        self.descLabel.text = @"名字长度";
        self.subDescLabel.text = @"Name length";
    } else if ([desc isEqualToString:@"nickLength"]) {
        self.descLabel.text = @"称呼长度";
        self.subDescLabel.text = @"Nickname length";
    } else if ([desc isEqualToString:@"camp"]) {
        self.descLabel.text = @"阵营";
        self.subDescLabel.text = @"Camp";
    }
    
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    CGSize subdescSize = [self.subDescLabel.text sizeWithCalcFont:self.subDescLabel.font];
    CGFloat labelSpace = LCFloat(3);
    CGFloat width = _size.width;
    CGFloat y = (width - (descSize.height + labelSpace + subdescSize.height)) / 2;
    
    self.descLabel.frame = CGRectMake((width - descSize.width) / 2, y, descSize.width, descSize.height);
    self.subDescLabel.frame = CGRectMake((width - subdescSize.width) / 2, CGRectGetMaxY(self.descLabel.frame) + labelSpace, subdescSize.width, subdescSize.height);
}


@end
