//
//  PDLotterySelectedButton.m
//  PandaKing
//
//  Created by Cranz on 16/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotterySelectedButton.h"

@implementation PDLotterySelectedButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.titleLabel.font = [UIFont systemFontOfCustomeSize:14];
        [self setTitleColor:c4 forState:UIControlStateNormal];
        [self setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
        [self changeButtonState];
    }
    return self;
}

- (void)changeButtonState {
    if (self.selected == YES) {
        self.backgroundColor = c26;
        self.layer.borderWidth = 0;
    } else {
        self.backgroundColor = [UIColor whiteColor];
        self.layer.borderColor = c4.CGColor;
        self.layer.borderWidth = 1;
    }
}

@end
