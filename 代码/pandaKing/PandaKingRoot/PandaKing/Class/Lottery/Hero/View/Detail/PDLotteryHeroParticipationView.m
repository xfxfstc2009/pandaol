//
//  PDLotteryHeroParticipationView.m
//  PandaKing
//
//  Created by Cranz on 16/12/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroParticipationView.h"

@interface PDLotteryHeroParticipationView ()
@property (nonatomic, strong) UILabel *dailyJoinerLabel; /**< 今日参与*/
@property (nonatomic, strong) UILabel *goldPoolLabel; /**< 竞猜池*/
@end

@implementation PDLotteryHeroParticipationView

+ (CGFloat)cellHeight {
    return 44;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    [self pageSetting];
    return self;
}

- (void)pageSetting {
    self.backgroundColor = c1;
    
    self.dailyJoinerLabel = [[UILabel alloc] init];
    self.dailyJoinerLabel.font = [UIFont systemFontOfCustomeSize:14];
    [self addSubview:self.dailyJoinerLabel];
    
    self.goldPoolLabel = [[UILabel alloc] init];
    self.goldPoolLabel.font = self.dailyJoinerLabel.font;
    [self addSubview:self.goldPoolLabel];
}

- (void)setModel:(PDLotteryHeroDetail *)model {
    _model = model;
    
    NSMutableAttributedString *dailyAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"今日竞猜人数:%ld",(long)model.joiners]];
    [dailyAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(7, [NSString stringWithFormat:@"%ld",(long)model.joiners].length)];
    self.dailyJoinerLabel.attributedText = [dailyAtt copy];
    
    NSString *totalGold = [PDCenterTool numberStringChange:model.totalGold];
    NSMutableAttributedString *coinAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"今日竞猜池累计:%@金币",totalGold]];
    [coinAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(8, totalGold.length)];
    self.goldPoolLabel.attributedText = [coinAtt copy];
    
    CGSize dailySize = [self.dailyJoinerLabel.text sizeWithCalcFont:self.dailyJoinerLabel.font];
    CGSize coinSize = [self.goldPoolLabel.text sizeWithCalcFont:self.goldPoolLabel.font];
    
    self.dailyJoinerLabel.frame = CGRectMake(kTableViewSectionHeader_left, ([PDLotteryHeroParticipationView cellHeight] - dailySize.height) / 2, dailySize.width, dailySize.height);
    self.goldPoolLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - coinSize.width, ([PDLotteryHeroParticipationView cellHeight] - coinSize.height) / 2, coinSize.width, coinSize.height);
}


@end
