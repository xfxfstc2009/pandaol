//
//  CCZSlider.h
//  CCZSlider
//
//  Created by 金峰 on 2016/11/5.
//  Copyright © 2016年 金峰. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCZSlider : UISlider
@property (nonatomic, assign) BOOL scale; /**< 是否显示刻度，默认分度值是0.5*/
@property (nonatomic, strong) UIColor *scaleColor;
@property (nonatomic, assign) CGFloat scaleOffsetY;
@property (nonatomic, assign) int divisionValue;   /**< 分度值，也就是最小刻度*/
@property (nonatomic, assign) int scaleVale;    /**< 刻度值*/
@end
