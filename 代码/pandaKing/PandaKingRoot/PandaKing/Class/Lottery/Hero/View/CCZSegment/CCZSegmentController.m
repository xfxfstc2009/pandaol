//
//  CCZSegmentController.m
//  CCZSegmentController
//
//  Created by 金峰 on 2016/12/15.
//  Copyright © 2016年 金峰. All rights reserved.
//

#import "CCZSegmentController.h"

typedef NS_ENUM(NSUInteger, CCZScrollRectPosition) {
    CCZScrollRectPositionOrigin, // 在原始位置
    CCZScrollRectPositionAcross, // 在中间段位置
    CCZScrollRectPositionTarget, // 到达目标位置
};

typedef void(^CCZViewControllerIndexBlock)(NSUInteger, UIButton *, UIViewController *);
typedef void(^CCZScrollComplicatuionBlock)();
#define kSCREENBOUNDS [[UIScreen mainScreen] bounds]
@interface CCZSegmentController ()
@property (nonatomic, strong, readwrite) UIViewController *currentViewController;
@property (nonatomic, strong, readwrite) CCZSegmentView *segmentView;
@property (nonatomic, strong, readwrite) UIScrollView *containerView;
@property (nonatomic, readwrite) NSUInteger index;
@property (nonatomic, strong) NSArray *titles;
@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGPoint viewOrigin; // 记录原始位置
@property (nonatomic, assign) CGSize offsetSize;        /**< 这个属性是用在badge上的偏移，width是_buttonSpace,height是titleLabel的y*/
@property (nonatomic, copy)   CCZViewControllerIndexBlock indexBlock;
@property (nonatomic, copy)   CCZScrollComplicatuionBlock originBlock;
@property (nonatomic, copy)   CCZScrollComplicatuionBlock targetBlock;
/// animation
@property (nonatomic, strong) UIScrollView *backgroundScrollView;
@property (nonatomic, assign) CCZScrollRectPosition position;
@property (nonatomic, assign) CGFloat scrollOffset;
@property (nonatomic, strong) NSMutableArray *registScrollViewKvoArr;
@end

@implementation CCZSegmentController

- (NSMutableArray *)registScrollViewKvoArr {
    if (!_registScrollViewKvoArr) {
        _registScrollViewKvoArr = [NSMutableArray array];
    }
    return _registScrollViewKvoArr;
}

- (void)dealloc {
    for (UIScrollView *registScrollView in _registScrollViewKvoArr) {
        [registScrollView removeObserver:self forKeyPath:@"contentOffset"];
    }
}

+ (instancetype)segmentControllerWithTitles:(NSArray<NSString *> *)titles {
    return [[self alloc] initWithFrame:CGRectMake(0, 0, kSCREENBOUNDS.size.width, kSCREENBOUNDS.size.height) titles:titles];
}

- (instancetype)initWithFrame:(CGRect)frame titles:(NSArray *)titles titleFont:(UIFont *)font contentEdgeSpace:(CGFloat)space {
    self = [super init];
    if (!self || titles.count == 0) {
        return nil;
    }
    
    _titles = titles;
    _size = frame.size;
    _viewOrigin = frame.origin;
    _pagingEnabled = YES;
    _bounces = NO;
    self.position = CCZScrollRectPositionOrigin;
    self.view.frame = frame;
    
    [self backgroundViewSetting];
    [self segmentPageSettingWithTitleFont:font contentEdgeSpace:space];
    [self containerViewSetting];
    
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame titles:(NSArray *)titles {
    return [self initWithFrame:frame titles:titles titleFont:[UIFont systemFontOfSize:16] contentEdgeSpace:0];
}

- (void)backgroundViewSetting {
    _backgroundScrollView = [[UIScrollView alloc] initWithFrame:self.view.bounds];
    _backgroundScrollView.backgroundColor = [UIColor clearColor];
    _backgroundScrollView.contentSize = _size;
    _backgroundScrollView.delegate = self;
    _backgroundScrollView.showsHorizontalScrollIndicator = NO;
    _backgroundScrollView.showsVerticalScrollIndicator = NO;
    _backgroundScrollView.bounces = NO;
    [self.view addSubview:_backgroundScrollView];
}

- (void)segmentPageSettingWithTitleFont:(UIFont *)font contentEdgeSpace:(CGFloat)space {
    _segmentView = [[CCZSegmentView alloc] initWithFrame:CGRectMake(0, 0, _size.width, 0) titles:_titles titleFont:font contentEdgeSpace:space];
    __weak typeof(self) weakSelf = self;
    [_segmentView selectedAtIndex:^(NSUInteger index, UIButton * _Nonnull button) {
        [weakSelf moveToViewControllerAtIndex:index];
    }];
    [_backgroundScrollView addSubview:_segmentView];
    UIButton *button = _segmentView.buttons.firstObject;
    
    _offsetSize = CGSizeMake(_segmentView.buttonSpace, (_segmentView.segmentHeight - [@"ccz" sizeWithAttributes:@{NSFontAttributeName: button.titleLabel.font}].height) / 2);
}

- (void)containerViewSetting {
    _containerView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, _segmentView.segmentHeight, _size.width, _size.height - _segmentView.segmentHeight)];
    _containerView.backgroundColor = [UIColor clearColor];
    _containerView.showsVerticalScrollIndicator = NO;
    _containerView.showsHorizontalScrollIndicator = NO;
    _containerView.delegate = self;
    _containerView.pagingEnabled = _pagingEnabled;
    _containerView.bounces = _bounces;
    [_backgroundScrollView addSubview:_containerView];
}

#pragma mark - scrollView delegate

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == _containerView) {
        NSInteger index = round(scrollView.contentOffset.x / _size.width);
        
        // 移除不足一页的操作
        if (index != self.index) {
            [self setSelectedAtIndex:index];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    if (scrollView == _containerView) {
        CGFloat offsetX = scrollView.contentOffset.x;
        
        [_segmentView adjustOffsetXToFixIndicatePosition:offsetX];
    }
    if (scrollView == _backgroundScrollView) {
        CGFloat offsetY = scrollView.contentOffset.y;

        if (offsetY == 0) {
            self.position = CCZScrollRectPositionOrigin;
            if (self.originBlock) {
                self.originBlock();
            }
        }
        if (offsetY == _scrollOffset) {
            self.position = CCZScrollRectPositionTarget;
            if (self.targetBlock) {
                self.targetBlock();
            }
        }
    }
}

#pragma mark - index

- (void)setSelectedAtIndex:(NSUInteger)index {
    [_segmentView setSelectedAtIndex:index];
}

- (void)moveToViewControllerAtIndex:(NSUInteger)index {
    [self scrollContainerViewToIndex:index];
    
    UIViewController *targetViewController = self.viewControllers[index];
    if ([self.childViewControllers containsObject:targetViewController] || !targetViewController) {
        return;
    }
    
    [self updateFrameChildViewController:targetViewController atIndex:index];
}

- (void)selectedAtIndex:(void (^)(NSUInteger, UIButton * _Nonnull, UIViewController * _Nonnull))indexBlock {
    if (indexBlock) {
        _indexBlock = indexBlock;
    }
}

- (void)updateFrameChildViewController:(UIViewController *)childViewController atIndex:(NSUInteger)index {
    childViewController.view.frame = CGRectOffset(CGRectMake(0, 0, _containerView.frame.size.width, _containerView.frame.size.height), index * _size.width, 0);
    [self addChildViewController:childViewController];
    [_containerView addSubview:childViewController.view];
    
    // scroll 控制
    if (!childViewController.registScrollView) {
        return;
    }
    [childViewController.registScrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
    [self.registScrollViewKvoArr addObject:childViewController.registScrollView];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    UIScrollView *registScrollView = self.currentViewController.registScrollView;
    if ([keyPath isEqualToString:@"contentOffset"] && object == registScrollView && !registScrollView.decelerating && registScrollView.dragging) {
        
        CGPoint offset = [change[NSKeyValueChangeNewKey] CGPointValue];
        CGFloat offsetY = offset.y;
        
        if (self.position == CCZScrollRectPositionOrigin && offsetY >= 0) {
            [self.backgroundScrollView setContentOffset:CGPointMake(0, _scrollOffset) animated:YES];
        }
        if (self.position == CCZScrollRectPositionTarget && offsetY <= 0) {
            [self.backgroundScrollView setContentOffset:CGPointZero animated:YES];
        }
    }
}

- (void)segmentScrollToTargetComplication:(void (^)())scrollComplication {
    if (scrollComplication) {
        self.targetBlock = scrollComplication;
    }
}

- (void)segmentScrollToOriginComplication:(void (^)())scrollComplication {
    if (scrollComplication) {
        self.originBlock = scrollComplication;
    }
}

#pragma mark - scroll

- (void)scrollContainerViewToIndex:(NSUInteger)index {
    [UIView animateWithDuration:_segmentView.duration delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [_containerView setContentOffset:CGPointMake(index * _size.width, 0)];
    } completion:^(BOOL finished) {
        if (_indexBlock) {
            _indexBlock(index, _segmentView.selectedButton, self.currentViewController);
        }
    }];
}

#pragma mark - set

- (void)setViewControllers:(NSArray *)viewControllers {
    _viewControllers = viewControllers;
    _containerView.contentSize = CGSizeMake(viewControllers.count * _size.width, _size.height - _segmentView.segmentHeight);
    
    // 默认加入第一个控制器
    UIViewController *firstViewController = self.viewControllers.firstObject;
    [self updateFrameChildViewController:firstViewController atIndex:0];
}

- (void)setPagingEnabled:(BOOL)pagingEnabled {
    _pagingEnabled = pagingEnabled;
    
    self.containerView.pagingEnabled = pagingEnabled;
}

- (void)setBounces:(BOOL)bounces {
    _bounces = bounces;
    
    self.containerView.bounces = bounces;
}

#pragma mark - get

- (NSUInteger)index {
    return self.segmentView.index;
}

- (UIViewController *)currentViewController {
    return self.viewControllers[self.index];
}

#pragma mark - scroll rect

- (void)setHeaderView:(UIView *)headerView {
    _headerView = headerView;
    
    CGSize headerSize = headerView.frame.size;
    headerView.frame = CGRectMake(0, 0, _size.width, headerSize.height);
    [_backgroundScrollView addSubview:self.headerView];
    
    _scrollOffset = round(headerSize.height);
    _backgroundScrollView.contentSize = CGSizeMake(_size.width, _size.height + headerSize.height);
    _segmentView.frame = CGRectOffset(self.segmentView.frame, 0, headerSize.height);
    _containerView.frame = CGRectOffset(self.containerView.frame, 0, headerSize.height);
}

- (void)scrollToTarget {
    if (self.position == CCZScrollRectPositionOrigin) {
        [self.backgroundScrollView setContentOffset:CGPointMake(0, _scrollOffset) animated:YES];
    }
}

- (void)scrollToOrigin {
    if (self.position == CCZScrollRectPositionTarget) {
        [self.backgroundScrollView setContentOffset:CGPointZero animated:YES];
    }
}

#pragma mark - #! 分类(UIView)

- (void)enumerateBadges:(NSArray<NSNumber *> *)badges {
    [badges enumerateObjectsUsingBlock:^(NSNumber * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        UIButton *button = self.segmentView.buttons[idx];
        [button addNumberBadge:obj.integerValue badgeOffsetSize:_offsetSize color:_segmentView.segmentTintColor borderColor:_segmentView.contentView.backgroundColor];
    }];
}

- (void)addCurrentBadgeByNumber_1 {
    [self.segmentView.selectedButton addNumber_1];
}

- (void)reduceCurrentBadgeByNumber_1 {
    [self.segmentView.selectedButton reduceNumber_1];
}

- (void)clearAllBadges {
    [_segmentView.buttons enumerateObjectsUsingBlock:^(UIButton *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj clearNumberBadge];
    }];
}

- (void)clearCurrentBadge {
    [self.segmentView.selectedButton clearNumberBadge];
}

@end

#pragma mark - #! 分类(UIViewController)

#import <objc/runtime.h>

const char *CCZRegistScrollViewKey = "ccz_registScrollView_key";
@implementation UIViewController(CCZSegment)
@dynamic segmentController, registScrollView;

- (CCZSegmentController *)segmentController {
    if ([self.parentViewController isKindOfClass:[CCZSegmentController class]] && self.parentViewController) {
        return (CCZSegmentController *)self.parentViewController;
    }
    return nil;
}

- (void)setRegistScrollView:(UIScrollView *)registScrollView {
    objc_setAssociatedObject(self, CCZRegistScrollViewKey, registScrollView, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (UIScrollView *)registScrollView {
    return objc_getAssociatedObject(self, CCZRegistScrollViewKey);
}

- (void)addSegmentController:(CCZSegmentController *)segment {
    if (self == segment) {
        return;
    }
    
    [self.view addSubview:segment.view];
    [self addChildViewController:segment];
}

@end
