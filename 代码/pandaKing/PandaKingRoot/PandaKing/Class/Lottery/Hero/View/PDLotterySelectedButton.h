//
//  PDLotterySelectedButton.h
//  PandaKing
//
//  Created by Cranz on 16/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLottryButtonModel.h"

@interface PDLotterySelectedButton : UIButton
@property (nonatomic, strong) PDLottryButtonModel *indexModel;
- (void)changeButtonState;
@end
