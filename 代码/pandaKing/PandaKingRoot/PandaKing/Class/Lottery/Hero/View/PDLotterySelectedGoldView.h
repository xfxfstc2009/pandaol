//
//  PDLotterySelectedGoldView.h
//  PandaKing
//
//  Created by Cranz on 16/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryHeroLotteryInfo.h"
#import "CCZSlider.h"

/// 投入金币页
@interface PDLotterySelectedGoldView : UIView
@property (nonatomic, strong) PDLotteryHeroLotteryInfo *model;
@property (nonatomic, copy)  NSString *title;
@property (nonatomic, strong, readonly) CCZSlider *slider;

/** 确定*/
- (void)submitWithGoldCount:(void(^)(NSInteger count))submitBlock;
/** 返回*/
- (void)backWithBackBlock:(void(^)())backBlock;
/**< 取消*/
- (void)cancleWithCancleBlock:(void(^)())cancleBlock;

/// 新手引导的时候传入的假的金额
@property (nonatomic, assign) NSUInteger falseGold;

@end
