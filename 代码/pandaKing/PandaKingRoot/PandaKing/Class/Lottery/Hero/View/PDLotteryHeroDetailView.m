//
//  PDLotteryHeroDetailView.m
//  PandaKing
//
//  Created by Cranz on 16/10/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroDetailView.h"

@interface PDLotteryHeroDetailView ()
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) PDImageView *heroImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *descLabel;

@property (nonatomic, assign) CGSize size;
@end

@implementation PDLotteryHeroDetailView

- (instancetype)initWithFrame:(CGRect)frame {
    CGSize heroCardSize = CGSizeMake(LCFloat(478/2), [AccountModel sharedAccountModel].gameType == GameTypeDota2? LCFloat(300) : LCFloat(778/2));
    self = [super initWithFrame:CGRectMake(frame.origin.x - heroCardSize.width / 2, frame.origin.y - heroCardSize.height / 2, heroCardSize.width, heroCardSize.height)];
    if (self) {
        _size = self.frame.size;
        self.style = PDLotteryHeroDetailViewShowStyleTop;
        [self heroDetailViewSetting];
    }
    return self;
}

- (void)heroDetailViewSetting {
    self.bgImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    
    CGSize heroSize = CGSizeZero;
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        self.bgImageView.image = [UIImage imageNamed:@"icon_hero_showpic"];
        heroSize = CGSizeMake(LCFloat(569/2) *3./5, LCFloat(569/2));
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        self.bgImageView.image = [UIImage imageNamed:@"icon_hero_dota2_showpic"];
        heroSize = CGSizeMake(LCFloat(170), LCFloat(170) * 3.47 / 3.);
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        self.bgImageView.image = [UIImage imageNamed:@"icon_hero_king_showpic"];
        heroSize = CGSizeMake(LCFloat(569/2) *3./5, LCFloat(569/2));
    }
    
    self.bgImageView.layer.cornerRadius = 2;
    self.bgImageView.clipsToBounds = YES;
    self.bgImageView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.bgImageView.layer.shadowOpacity = 0.8;
    self.bgImageView.layer.shadowOffset = CGSizeMake(1, 1);
    [self addSubview:self.bgImageView];
    
    self.heroImageView = [[PDImageView alloc] initWithFrame:CGRectMake((_size.width - heroSize.width) / 2, LCFloat(45), heroSize.width, heroSize.height)];
    self.heroImageView.image = [UIImage imageNamed:@"1"];
    [self.bgImageView addSubview:self.heroImageView];
    
    // 英雄名字
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [[UIFont systemFontOfCustomeSize:16] boldFont];
    self.nameLabel.textColor = c26;
    [self.bgImageView addSubview:self.nameLabel];
    
    // 竞猜描述信息
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.descLabel.textColor = c21;
    [self.bgImageView addSubview:self.descLabel];
}

- (void)setStyle:(PDLotteryHeroDetailViewShowStyle)style {
    _style = style;
    
    if (style == PDLotteryHeroDetailViewShowStyleTop) {
        self.layer.anchorPoint = CGPointMake(1, 1);
    } else {
        self.layer.anchorPoint = CGPointMake(1, 0);
    }
}

- (void)setModel:(PDLotteryLotteriesItem *)model {
    _model = model;
    
    if (model.draw == YES) {
        if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
            [self.heroImageView uploadHeroLotteryWithGameType:[AccountModel sharedAccountModel].gameType imageCode:[NSString stringWithFormat:@"%ld",model.winPandaLOLChampion.num] callback:nil];
            self.nameLabel.text = [NSString stringWithFormat:@"%ld·%@",model.winPandaLOLChampion.num,model.winPandaLOLChampion.champion.nick];
            NSString *nameLength = @"";
            NSString *goldPrice = @"";
            NSString *far = @"";
            if (model.winPandaLOLChampion.champion.nick.length >= 4) {
                nameLength = @"四个及以上";
            } else if (model.winPandaLOLChampion.champion.nick.length == 3) {
                nameLength = @"三个";
            } else {
                nameLength = @"两个及以下";
            }
            if (model.winPandaLOLChampion.champion.goldPrice >= 6300) {
                goldPrice = @"6300及以上";
            } else if (model.winPandaLOLChampion.champion.goldPrice == 4800) {
                goldPrice = @"4800";
            } else {
                goldPrice = @"3150及以下";
            }
            if (model.winPandaLOLChampion.champion.far == YES) {
                far = @"远程";
            } else {
                far = @"近战";
            }
            self.descLabel.text = [NSString stringWithFormat:@"%@-%@-%@",nameLength,goldPrice,far];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
            [self.heroImageView uploadHeroLotteryWithGameType:[AccountModel sharedAccountModel].gameType imageCode:[NSString stringWithFormat:@"%ld",model.winPandaDota2Champion.num] callback:nil];
            self.nameLabel.text = [NSString stringWithFormat:@"%ld·%@",model.winPandaDota2Champion.num,model.winPandaDota2Champion.champion.nick];
            NSString *nameLength = @"";
            NSString *camp = @"";
            NSString *far = @"";
            if (model.winPandaDota2Champion.champion.nick.length >= 4) {
                nameLength = @"四个及以上";
            } else if (model.winPandaDota2Champion.champion.nick.length == 3) {
                nameLength = @"三个";
            } else {
                nameLength = @"两个及以下";
            }
            if ([model.winPandaDota2Champion.champion.camp isEqualToString:@""]) {
                camp = @"夜魇";
            } else {
                camp = @"天辉";
            }
            if (model.winPandaDota2Champion.champion.far == YES) {
                far = @"远程";
            } else {
                far = @"近战";
            }
            self.descLabel.text = [NSString stringWithFormat:@"%@-%@-%@",camp,far,nameLength];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
            [self.heroImageView uploadHeroLotteryWithGameType:[AccountModel sharedAccountModel].gameType imageCode:[NSString stringWithFormat:@"%ld",model.winPandaKingChampion.num] callback:nil];
            self.nameLabel.text = [NSString stringWithFormat:@"%ld·%@",model.winPandaKingChampion.num,model.winPandaKingChampion.champion.name];
            NSString *nameLength = @"";
            NSString *far = @"";
            if (model.winPandaKingChampion.champion.name.length >= 4) {
                nameLength = @"四个及以上";
            } else if (model.winPandaKingChampion.champion.name.length == 3) {
                nameLength = @"三个";
            } else if (model.winPandaKingChampion.champion.name.length < 3) {
                nameLength = @"三个以下";
            }
            
            if (model.winPandaKingChampion.champion.far == YES) {
                far = @"远程";
            } else {
                far = @"近战";
            }
            self.descLabel.text = [NSString stringWithFormat:@"%@-%@",far,nameLength];
        }
        
    } else {
        self.nameLabel.font = self.descLabel.font;
        self.nameLabel.textColor = self.descLabel.textColor;
        self.heroImageView.image = [UIImage imageNamed:@"icon_herobg_wait"];
        self.nameLabel.text = @"时时彩开奖官方延迟60－120秒";
        self.descLabel.text = @"请耐心等待...";
    }
    
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font];
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    
    self.nameLabel.frame = CGRectMake((_size.width - nameSize.width) / 2, CGRectGetMaxY(self.heroImageView.frame) + LCFloat(19/2), nameSize.width, nameSize.height);
    self.descLabel.frame = CGRectMake((_size.width - descSize.width) / 2, CGRectGetMaxY(self.nameLabel.frame) + LCFloat(16/2 - 4), descSize.width, descSize.height);
}

@end
