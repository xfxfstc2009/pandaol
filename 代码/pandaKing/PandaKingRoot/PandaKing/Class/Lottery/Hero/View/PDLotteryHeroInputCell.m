//
//  PDLotteryHeroInputCell.m
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroInputCell.h"

#define kSpace_label 2
#define kSpaceLabel_image 4
#define kSpace_image 5  // 图片距离cell的边距
#define kFont [UIFont systemFontOfCustomeSize:14]
@interface PDLotteryHeroInputCell ()
@property (nonatomic, strong) UIView *leftView;
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) UILabel *poolLabel;   /**< 夺宝池*/
@property (nonatomic, strong) UILabel *rewardLabel;    /**< 奖金*/
@end

@implementation PDLotteryHeroInputCell

+ (CGFloat)cellHeight {
    CGSize size = [@"一行" sizeWithCalcFont:kFont];
    return size.height * 2 + kSpaceLabel_image * 2 + kSpace_label + kSpace_image * 2;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    [self pageSetting];
    return self;
}

- (void)pageSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    // 左边的黑块
    CGFloat imageHeight = [PDLotteryHeroInputCell cellHeight] - 8;
    CGFloat leftViewHeight = imageHeight - 8;
    
    self.leftView = [[UIView alloc] init];
    self.leftView.frame = CGRectMake(0, ([PDLotteryHeroInputCell cellHeight] - leftViewHeight) / 2, 5, leftViewHeight);
    self.leftView.backgroundColor = [UIColor blackColor];
    [self.contentView addSubview:self.leftView];
    
    self.bgImageView = [[UIImageView alloc] init];
    self.bgImageView.frame = CGRectMake(LCFloat(20), ([PDLotteryHeroInputCell cellHeight] - imageHeight) / 2, kScreenBounds.size.width - LCFloat(20) * 2, imageHeight);
    self.bgImageView.image = [UIImage imageNamed:@"icon_snatch_bg"];
    [self.contentView addSubview:self.bgImageView];
    
    // 本期竞猜池已有
    self.poolLabel = [[UILabel alloc] init];
    self.poolLabel.font = kFont;
    self.poolLabel.textColor = [UIColor whiteColor];
    [self.bgImageView addSubview:self.poolLabel];
    
    // 您已投入1竹子可能奖金176
    self.rewardLabel = [[UILabel alloc] init];
    self.rewardLabel.font = self.poolLabel.font;
    self.rewardLabel.textColor = [UIColor whiteColor];
    [self.bgImageView addSubview:self.rewardLabel];
}

- (void)setModel:(PDLotteryHeroDetail *)model {
    _model = model;
    
    NSString *totalGold = [NSString stringWithFormat:@"%ld",model.currentDrawGold];
    NSString *memberGold = [NSString stringWithFormat:@"%ld",model.memberGold];
    NSMutableAttributedString *poolAttributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"本期竞猜池已有%@金币", totalGold]];
    [poolAttributeString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(7, totalGold.length)];
    self.poolLabel.attributedText = poolAttributeString;
    NSMutableAttributedString *rateAttributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"您已投入%@金币", memberGold]];
    [rateAttributeString addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4, memberGold.length)];
    self.rewardLabel.attributedText = rateAttributeString;
    
    CGSize poolSize = [self.poolLabel.text sizeWithCalcFont:self.poolLabel.font];
    CGSize rateSize = [self.rewardLabel.text sizeWithCalcFont:self.rewardLabel.font];
    
    self.poolLabel.frame = CGRectMake((CGRectGetWidth(self.bgImageView.frame) - poolSize.width) / 2, kSpaceLabel_image, poolSize.width, poolSize.height);
    self.rewardLabel.frame = CGRectMake((CGRectGetWidth(self.bgImageView.frame) - rateSize.width) / 2, CGRectGetMaxY(self.poolLabel.frame) + kSpace_label, rateSize.width, rateSize.height);
}

@end
