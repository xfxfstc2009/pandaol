//
//  PDLotteryHeroInputCell.h
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryHeroDetail.h"

/// 投入金币池
@interface PDLotteryHeroInputCell : UITableViewCell
@property (nonatomic, strong) PDLotteryHeroDetail *model;

+ (CGFloat)cellHeight;
@end
