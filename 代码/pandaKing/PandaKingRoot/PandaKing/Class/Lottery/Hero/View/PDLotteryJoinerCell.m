//
//  PDLotteryJoinerCell.m
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryJoinerCell.h"

@interface PDLotteryJoinerCell ()
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *descLabel;   // 英雄信息
@property (nonatomic, strong) UILabel *inputLabel;
@property (nonatomic, strong) UILabel *dateLabel;

@property (nonatomic, assign) CGFloat rowHeight;
@end
@implementation PDLotteryJoinerCell

+ (CGFloat)cellHeight {
    return 109 / 2;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        _rowHeight = [PDLotteryJoinerCell cellHeight];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    CGFloat imageWidth = 42;
    self.headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, (_rowHeight - imageWidth) / 2, imageWidth, imageWidth)];
    self.headerImageView.layer.cornerRadius = imageWidth / 2;
    self.headerImageView.layer.masksToBounds = YES;
    [self.contentView addSubview:self.headerImageView];
    
    // name
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = c3;
    [self.contentView addSubview:self.nameLabel];
    
    // 英雄描述
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.descLabel.textColor = c4;
    [self.contentView addSubview:self.descLabel];
    
    // 投入
    self.inputLabel = [[UILabel alloc] init];
    self.inputLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.inputLabel.textColor = c4;
    [self.contentView addSubview:self.inputLabel];
    
    // date
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.dateLabel.textColor = c4;
    [self.contentView addSubview:self.dateLabel];
}

- (void)updateFrames {
    CGFloat space = LCFloat(12);
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - descSize.width - space * 2 - CGRectGetWidth(self.headerImageView.frame), nameSize.height)];
    CGSize inputSize = [self.inputLabel.text sizeWithCalcFont:self.inputLabel.font];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGFloat vSpace = (_rowHeight - nameSize.height - inputSize.height) / 3;
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + space, vSpace, nameSize.width, nameSize.height);
    self.descLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - descSize.width, CGRectGetMaxY(self.nameLabel.frame) - descSize.height, descSize.width, descSize.height);
    self.inputLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + vSpace, inputSize.width, inputSize.height);
    self.dateLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - dateSize.width, CGRectGetMaxY(self.descLabel.frame) + vSpace, dateSize.width, dateSize.height);
}

- (void)setModel:(PDLotteryJoinerItem *)model {
    _model = model;
    
    self.nameLabel.text = model.member.nickname;
    self.descLabel.text = [NSString stringWithFormat:@"%@-%@-%@",model.goldPrice,model.far,model.nickLength];
    self.inputLabel.text = [NSString stringWithFormat:@"投入金币%ld个",model.stakeInfo.gold];
    self.dateLabel.text = [PDCenterTool dateFromTimestamp:model.stakeInfo.stakeTime];
    [self.headerImageView uploadImageWithAvatarURL:model.member.avatar placeholder:nil callback:NULL];
    
    NSMutableAttributedString *inputAtt = [[NSMutableAttributedString alloc] initWithString:self.inputLabel.text];
    [inputAtt addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4, [NSString stringWithFormat:@"%ld",model.stakeInfo.gold].length)];
    self.inputLabel.attributedText = [inputAtt copy];
    
    [self updateFrames];
}

- (void)setDota2Item:(PDLotteryHeroDota2JoinerItem *)dota2Item {
    _dota2Item = dota2Item;
    
    self.nameLabel.text = dota2Item.member.nickname;
    self.descLabel.text = [NSString stringWithFormat:@"%@-%@-%@",dota2Item.camp,dota2Item.far,dota2Item.mainProperty];
    self.inputLabel.text = [NSString stringWithFormat:@"投入金币%ld个",dota2Item.stakeInfo.gold];
    self.dateLabel.text = [PDCenterTool dateFromTimestamp:dota2Item.stakeInfo.stakeTime];
    [self.headerImageView uploadImageWithAvatarURL:dota2Item.member.avatar placeholder:nil callback:NULL];
    
    NSMutableAttributedString *inputAtt = [[NSMutableAttributedString alloc] initWithString:self.inputLabel.text];
    [inputAtt addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4, [NSString stringWithFormat:@"%ld",dota2Item.stakeInfo.gold].length)];
    self.inputLabel.attributedText = [inputAtt copy];
    
    [self updateFrames];
}

- (void)setPvpItem:(PDLotteryHeroPvpJoinerItem *)pvpItem {
    _pvpItem = pvpItem;
    
    self.nameLabel.text = pvpItem.member.nickname;
    self.descLabel.text = [NSString stringWithFormat:@"%@-%@",pvpItem.far,pvpItem.nameLength];
    self.inputLabel.text = [NSString stringWithFormat:@"投入金币%ld个",pvpItem.stakeInfo.gold];
    self.dateLabel.text = [PDCenterTool dateFromTimestamp:pvpItem.stakeInfo.stakeTime];
    [self.headerImageView uploadImageWithAvatarURL:pvpItem.member.avatar placeholder:nil callback:NULL];
    
    NSMutableAttributedString *inputAtt = [[NSMutableAttributedString alloc] initWithString:self.inputLabel.text];
    [inputAtt addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(4, [NSString stringWithFormat:@"%ld",pvpItem.stakeInfo.gold].length)];
    self.inputLabel.attributedText = [inputAtt copy];
    
    [self updateFrames];
}

@end
