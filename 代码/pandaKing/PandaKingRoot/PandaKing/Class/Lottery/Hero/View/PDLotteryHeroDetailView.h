//
//  PDLotteryHeroDetailView.h
//  PandaKing
//
//  Created by Cranz on 16/10/26.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryLotteriesItem.h"

typedef NS_ENUM(NSInteger, PDLotteryHeroDetailViewShowStyle) {
    PDLotteryHeroDetailViewShowStyleTop, // 向上弹起
    PDLotteryHeroDetailViewShowStyleDown,// 向下弹出
};
@interface PDLotteryHeroDetailView : UIView
@property (nonatomic, strong) PDLotteryLotteriesItem *model;
@property (nonatomic, assign) PDLotteryHeroDetailViewShowStyle style;
@end
