//
//  PDLotteryHeroHistoryCell.m
//  PandaKing
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroHistoryCell.h"

@interface PDLotteryHeroHistoryCell ()
@property (nonatomic, strong) UILabel *drawLabel;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, assign) CGFloat rowHeight;
@end
@implementation PDLotteryHeroHistoryCell

+ (CGFloat)cellHeight {
    return 74 / 2;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (!self) {
        return nil;
    }
    _rowHeight = [PDLotteryHeroHistoryCell cellHeight];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    [self pageSetting];
    return self;
}

- (void)pageSetting {
    self.drawLabel = [[UILabel alloc] init];
    self.drawLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.drawLabel.textColor = c28;
    [self.contentView addSubview:self.drawLabel];
    
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = self.drawLabel.font;
    self.descLabel.textColor = self.descLabel.textColor;
    [self.contentView addSubview:self.descLabel];
}

- (void)setModel:(PDLotteryHeroDailyItem *)model {
    _model = model;
    
    self.drawLabel.text = [NSString stringWithFormat:@"第%ld期",(long)model.lotteryNum];
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        self.descLabel.text = [NSString stringWithFormat:@"%ld·%@-%@-%@-%@",model.winNum,model.nick,model.goldPrice,model.far,model.nickLength];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        self.descLabel.text = [NSString stringWithFormat:@"%ld·%@-%@-%@-%@",model.winNum,model.nick,model.camp,model.far,model.mainProperty];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        self.descLabel.text = [NSString stringWithFormat:@"%ld·%@-%@-%@",model.winNum,model.name,model.far,model.nameLength];
    }

    CGSize drawSize = [self.drawLabel.text sizeWithCalcFont:self.drawLabel.font];
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    
    self.drawLabel.frame = CGRectMake(20, (_rowHeight - drawSize.height) / 2, drawSize.width, drawSize.height);
    self.descLabel.frame = CGRectMake(CGRectGetMaxX(self.drawLabel.frame) + LCFloat(20), (_rowHeight - descSize.height) / 2, descSize.width, descSize.height);
}

@end
