//
//  PDLotteryPercentView.h
//  PandaKing
//
//  Created by Cranz on 16/11/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryPercentModel.h"

@interface PDLotteryPercentView : UIView
@property (nonatomic, strong) PDLotteryPercentModel *model;
+ (CGFloat)viewHeight;
@end
