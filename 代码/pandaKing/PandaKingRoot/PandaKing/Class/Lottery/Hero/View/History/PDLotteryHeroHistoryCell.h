//
//  PDLotteryHeroHistoryCell.h
//  PandaKing
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryHeroDailyItem.h"

@interface PDLotteryHeroHistoryCell : UITableViewCell
@property (nonatomic, strong) PDLotteryHeroDailyItem *model;

+ (CGFloat)cellHeight;
@end
