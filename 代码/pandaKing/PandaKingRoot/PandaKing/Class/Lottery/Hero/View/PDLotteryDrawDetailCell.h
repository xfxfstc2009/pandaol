//
//  PDLotteryDrawDetailCell.h
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryLotteriesItem.h"

/// 开奖期号详情
@protocol PDLotteryDrawDetailCellDelegate;
@interface PDLotteryDrawDetailCell : UITableViewCell
@property (nonatomic, strong) PDLotteryLotteriesItem *model;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, weak) id<PDLotteryDrawDetailCellDelegate> delegate;

+ (CGFloat)cellHeight;
@end

@protocol PDLotteryDrawDetailCellDelegate <NSObject>

- (void)lotteryCell:(PDLotteryDrawDetailCell *)cell didClickHeroImageView:(UIImageView *)heroImageView atRow:(NSInteger)row;
@end