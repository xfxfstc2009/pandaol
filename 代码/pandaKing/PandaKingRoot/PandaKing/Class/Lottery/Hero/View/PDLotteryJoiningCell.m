//
//  PDLotteryJoiningCell.m
//  PandaKing
//
//  Created by Cranz on 16/11/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryJoiningCell.h"

@interface PDLotteryJoiningCell ()
@property (nonatomic, strong) UILabel *dailyJoinerLabel; /**< 今日参与*/
@property (nonatomic, strong) UILabel *goldPoolLabel; /**< 竞猜池*/
//@property (nonatomic, strong) UILabel *drawJoinerLabel; /**< 当前参与*/
@end

@implementation PDLotteryJoiningCell

+ (CGFloat)cellHeight {
    return 44;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    [self pageSetting];
    return self;
}

- (void)pageSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    self.dailyJoinerLabel = [[UILabel alloc] init];
    self.dailyJoinerLabel.font = [UIFont systemFontOfCustomeSize:14];
    [self.contentView addSubview:self.dailyJoinerLabel];
    
    self.goldPoolLabel = [[UILabel alloc] init];
    self.goldPoolLabel.font = self.dailyJoinerLabel.font;
    [self.contentView addSubview:self.goldPoolLabel];
    
//    self.drawJoinerLabel = [[UILabel alloc] init];
//    self.drawJoinerLabel.font = self.dailyJoinerLabel.font;
//    [self.contentView addSubview:self.drawJoinerLabel];
}

- (void)setModel:(PDLotteryHeroDetail *)model {
    _model = model;
    
    NSMutableAttributedString *dailyAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"今日竞猜人数:%ld",(long)model.joiners]];
    [dailyAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(7, [NSString stringWithFormat:@"%ld",(long)model.joiners].length)];
    self.dailyJoinerLabel.attributedText = [dailyAtt copy];
    
    NSMutableAttributedString *coinAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"今日竞猜池累计:%ld金币",(long)model.totalGold]];
    [coinAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(8, [NSString stringWithFormat:@"%ld",(long)model.totalGold].length)];
    self.goldPoolLabel.attributedText = [coinAtt copy];

//    NSMutableAttributedString *joinerAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"当前参与人数:%ld",(long)model.lotteryJoiners]];
//    [coinAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(7, [NSString stringWithFormat:@"%ld",(long)model.lotteryJoiners].length)];
//    self.drawJoinerLabel.attributedText = [joinerAtt copy];
    
    CGSize dailySize = [self.dailyJoinerLabel.text sizeWithCalcFont:self.dailyJoinerLabel.font];
    CGSize coinSize = [self.goldPoolLabel.text sizeWithCalcFont:self.goldPoolLabel.font];
//    CGSize joinerSize = [self.drawJoinerLabel.text sizeWithCalcFont:self.drawJoinerLabel.font];
    
//    CGFloat spaceLabel = 5;
//    CGFloat space = ([PDLotteryJoiningCell cellHeight] - spaceLabel - dailySize.height - joinerSize.height) / 2;
    
    self.dailyJoinerLabel.frame = CGRectMake(kTableViewSectionHeader_left, ([PDLotteryJoiningCell cellHeight] - dailySize.height) / 2, dailySize.width, dailySize.height);
    self.goldPoolLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - coinSize.width, ([PDLotteryJoiningCell cellHeight] - coinSize.height) / 2, coinSize.width, coinSize.height);
//    self.drawJoinerLabel.frame = CGRectMake(kTableViewSectionHeader_left, CGRectGetMaxY(self.dailyJoinerLabel.frame) + spaceLabel, joinerSize.width, joinerSize.height);
}

@end
