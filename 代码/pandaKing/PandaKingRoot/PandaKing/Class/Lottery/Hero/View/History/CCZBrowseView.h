//
//  CCZBrowseView.h
//  HistoryDemo
//
//  Created by Cranz on 16/11/11.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryDailyStatisticModel.h"

@interface CCZBrowseView : UIView
@property (nonatomic, strong) UICollectionView *mainView;
@property (nonatomic, strong) PDLotteryDailyStatisticModel *model;
@property (nonatomic, assign) NSUInteger itemNum;
@property (nonatomic, strong) UIImage *backgroundImage;
@property (nonatomic, assign) CGSize itemSize;
@property (nonatomic, assign) CGFloat offsetY;
@property (nonatomic, assign) CGFloat scale; /**< 缩放比例*/
@property (nonatomic, assign) CGFloat itemSpace;
@property (nonatomic, assign) CGFloat leftRightSpace;
@property (nonatomic, assign) BOOL wannaToTapScrollItem;

- (instancetype)initBrowseViewWithFrame:(CGRect)frame registerCustomCell:(Class)cellCls;
- (void)setBrowseSelectedAtIndex:(NSUInteger)index;
- (void)selectedAtIndex:(void(^)(NSUInteger index))indexBlock;
@end