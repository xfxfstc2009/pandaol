//
//  CCZShapeView.h
//  HistoryDemo
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 Cranz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CCZShapeView : UIView
@property (nonatomic, strong) NSArray *colors;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, assign) CGFloat r;
@property (nonatomic, assign) CGPoint c;

- (instancetype)initWithFrame:(CGRect)frame itemsNum:(NSUInteger)num;
- (void)setItemsPercentage:(NSArray *)percentArr lineStrokeComplication:(void(^)(NSArray *arr))complication;
@end
