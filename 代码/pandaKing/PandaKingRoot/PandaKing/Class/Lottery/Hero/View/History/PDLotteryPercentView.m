//
//  PDLotteryPercentView.m
//  PandaKing
//
//  Created by Cranz on 16/11/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryPercentView.h"

@interface PDLotteryPercentView ()
@property (nonatomic, strong) UIView *tagColorView;
@property (nonatomic, strong) UILabel *descLabel;
@property (nonatomic, strong) UILabel *percentLabel;
@property (nonatomic, assign) CGSize size;
@end

@implementation PDLotteryPercentView

+ (CGFloat)viewHeight {
    return [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13]];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    
    _size = frame.size;
    [self pageSetting];
    return self;
}

- (void)pageSetting {
    CGFloat tagWidth = LCFloat(12);
    self.tagColorView = [[UIView alloc] initWithFrame:CGRectMake(0, (_size.height - tagWidth) / 2, tagWidth, tagWidth)];
    self.tagColorView.layer.cornerRadius = tagWidth / 2;
    self.tagColorView.clipsToBounds = YES;
    [self addSubview:self.tagColorView];
    
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:10];
    self.descLabel.textColor = c1;
    [self addSubview:self.descLabel];
    
    self.percentLabel = [[UILabel alloc] init];
    self.percentLabel.font = self.descLabel.font;
    self.percentLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.percentLabel];
}

- (void)setModel:(PDLotteryPercentModel *)model {
    _model = model;
    
    self.tagColorView.backgroundColor = model.tagColor;
    self.descLabel.text = model.desc;

    self.percentLabel.text = [NSString stringWithFormat:@"%ld次",(long)model.count];
    
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    CGSize percentSize = [self.percentLabel.text sizeWithCalcFont:self.percentLabel.font];
    
    self.descLabel.frame = CGRectMake(CGRectGetMaxX(self.tagColorView.frame) + LCFloat(8), (_size.height - descSize.height) / 2, descSize.width, descSize.height);
    self.percentLabel.frame = CGRectMake(_size.width - percentSize.width, (_size.height - percentSize.height) / 2, percentSize.width, percentSize.height);
}

@end
