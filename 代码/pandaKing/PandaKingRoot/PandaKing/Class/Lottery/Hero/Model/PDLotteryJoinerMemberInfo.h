//
//  PDLotteryJoinerMemberInfo.h
//  PandaKing
//
//  Created by Cranz on 16/11/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDLotteryJoinerMemberInfo : FetchModel
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *userId;
@property (nonatomic, copy) NSString *nickname;
@end
