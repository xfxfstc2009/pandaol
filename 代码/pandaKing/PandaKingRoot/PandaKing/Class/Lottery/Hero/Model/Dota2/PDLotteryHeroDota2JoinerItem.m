//
//  PDLotteryHeroDota2JoinerItem.m
//  PandaKing
//
//  Created by Cranz on 17/5/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroDota2JoinerItem.h"

@implementation PDLotteryHeroDota2JoinerItem

- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"ID":@"id"};
}

- (NSString *)camp {
    if ([_camp isEqualToString:@"1"]) {
        return @"天辉";
    } else if ([_camp isEqualToString:@"2"]) {
        return @"夜魇";
    } else {
        return @"";
    }
}

- (NSString *)far {
    if ([_far isEqualToString:@"1"]) {
        return @"远程";
    } else if ([_far isEqualToString:@"2"]) {
        return @"近战";
    } else {
        return @"";
    }
}

- (NSString *)mainProperty {
    if ([_mainProperty isEqualToString:@"1"]) {
        return @"力量";
    } else if ([_mainProperty isEqualToString:@"2"]) {
        return @"敏捷";
    } else if ([_mainProperty isEqualToString:@"3"]) {
        return @"智力";
    } else {
        return @"";
    }
}

@end
