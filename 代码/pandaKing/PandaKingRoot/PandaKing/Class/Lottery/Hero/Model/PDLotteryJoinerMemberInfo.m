//
//  PDLotteryJoinerMemberInfo.m
//  PandaKing
//
//  Created by Cranz on 16/11/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryJoinerMemberInfo.h"

@implementation PDLotteryJoinerMemberInfo
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"userId":@"id"};
}
@end
