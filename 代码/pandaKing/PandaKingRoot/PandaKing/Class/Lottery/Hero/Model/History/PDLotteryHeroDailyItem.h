//
//  PDLotteryHeroDailyItem.h
//  PandaKing
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDLotteryHeroDailyItem <NSObject>

@end
@interface PDLotteryHeroDailyItem : FetchModel
@property (nonatomic, copy) NSString *nick;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSUInteger winNum;
@property (nonatomic, assign) NSUInteger lotteryNum;
@property (nonatomic, copy) NSString *goldPrice;
@property (nonatomic, copy) NSString *far;
@property (nonatomic, copy) NSString *nickLength;
// dota2
@property (nonatomic, copy) NSString *camp;
@property (nonatomic, copy) NSString *mainProperty;
// pvp
@property (nonatomic, copy) NSString *nameLength;
@end
