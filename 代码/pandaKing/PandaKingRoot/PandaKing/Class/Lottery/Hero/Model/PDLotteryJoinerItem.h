//
//  PDLotteryJoinerItem.h
//  PandaKing
//
//  Created by Cranz on 16/11/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryJoinerMemberInfo.h"
#import "PDLotteryJoinerStakeInfo.h"

@protocol PDLotteryJoinerItem <NSObject>

@end

/// 英雄猜lol
@interface PDLotteryJoinerItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, assign) NSInteger lotteryNum;
@property (nonatomic, strong) PDLotteryJoinerMemberInfo *member;
@property (nonatomic, strong) PDLotteryJoinerStakeInfo  *stakeInfo;

@property (nonatomic, copy) NSString *goldPrice;
@property (nonatomic, copy) NSString *far;
@property (nonatomic, copy) NSString *nickLength;
@end
