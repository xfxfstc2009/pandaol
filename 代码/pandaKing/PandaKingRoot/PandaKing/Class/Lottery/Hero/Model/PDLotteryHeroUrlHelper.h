//
//  PDLotteryHeroUrlHelper.h
//  PandaKing
//
//  Created by Cranz on 17/5/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDLotteryHeroUrlHelper : NSObject
/**
 * 最新一期
 */
+ (NSString *)lastDetailUrlExchange;
/**
 * 投注
 */
+ (NSString *)inputUrlExchange;
/**
 * 开奖纪录
 */
+ (NSString *)lotteriesRecordUrlExchange;
/**
 * 投注纪录
 */
+ (NSString *)inputRecordUrlExchange;
/**
 * 投注排行
 */
+ (NSString *)rankListUrlExchange;
/**
 * 今日开奖列表
 */
+ (NSString *)historyListUrlExhchange;
/**
 * 今日统计
 */
+ (NSString *)statisticUrlExchange;
/**
 * 获取赔率
 */
+ (NSString *)getOddsUrlExchange;
/**
 * 我的竞猜
 */
+ (NSString *)myLotteryListUrlExchange;
@end
