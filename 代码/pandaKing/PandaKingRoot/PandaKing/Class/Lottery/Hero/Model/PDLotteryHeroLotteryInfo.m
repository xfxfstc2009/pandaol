//
//  PDLotteryHeroLotteryInfo.m
//  PandaKing
//
//  Created by Cranz on 16/11/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroLotteryInfo.h"

@implementation PDLotteryHeroLotteryInfo
- (NSString *)goldPrice {
    if ([_goldPrice isEqualToString:@"1"]) {
        return @"3150及以下";
    } else if ([_goldPrice isEqualToString:@"2"]) {
        return @"4800";
    } else if ([_goldPrice isEqualToString:@"3"]) {
        return @"6300及以上";
    } else {
        return @"";
    }
}

- (NSString *)far {
    if ([_far isEqualToString:@"1"]) {
        return @"远程";
    } else if ([_far isEqualToString:@"2"]) {
        return @"近战";
    } else {
        return @"";
    }
}

- (NSString *)nickLength {
    if ([_nickLength isEqualToString:@"1"]) {
        return @"两个及以下";
    } else if ([_nickLength isEqualToString:@"2"]) {
        return @"三个";
    } else if ([_nickLength isEqualToString:@"3"]) {
        return @"四个及以上";
    } else {
        return @"";
    }
}

- (NSString *)dota2Camp {
    if ([_dota2Camp isEqualToString:@"1"]) {
        return @"天辉";
    } else if ([_dota2Camp isEqualToString:@"2"]) {
        return @"夜魇";
    } else {
        return @"";
    }
}

- (NSString *)dota2Far {
    if ([_dota2Far isEqualToString:@"1"]) {
        return @"远程";
    } else if ([_dota2Far isEqualToString:@"2"]) {
        return @"近程";
    } else {
        return @"";
    }
}

- (NSString *)dota2MainProperty {
    if ([_dota2MainProperty isEqualToString:@"1"]) {
        return @"力量";
    } else if ([_dota2MainProperty isEqualToString:@"2"]) {
        return @"敏捷";
    } else if ([_dota2MainProperty isEqualToString:@"3"]) {
        return @"智力";
    } else {
        return @"";
    }
}

- (NSString *)pvpFar {
    if ([_pvpFar isEqualToString:@"1"]) {
        return @"远程";
    } else if ([_pvpFar isEqualToString:@"2"]) {
        return @"近程";
    } else {
        return @"";
    }
}

- (NSString *)pvpNameLength {
    if ([_pvpNameLength isEqualToString:@"1"]) {
        return @"两个及以下";
    } else if ([_pvpNameLength isEqualToString:@"2"]) {
        return @"三个";
    } else if ([_pvpNameLength isEqualToString:@"3"]) {
        return @"四个及以上";
    } else {
        return @"";
    }
}

@end
