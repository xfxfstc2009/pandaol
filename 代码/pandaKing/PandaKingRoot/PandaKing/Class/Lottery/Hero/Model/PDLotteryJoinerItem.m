//
//  PDLotteryJoinerItem.m
//  PandaKing
//
//  Created by Cranz on 16/11/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryJoinerItem.h"

@implementation PDLotteryJoinerItem
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"ID":@"id"};
}

- (NSString *)goldPrice {
    if ([_goldPrice isEqualToString:@"1"]) {
        return @"3150及以下";
    } else if ([_goldPrice isEqualToString:@"2"]) {
        return @"4800";
    } else if ([_goldPrice isEqualToString:@"3"]) {
        return @"6300及以上";
    } else {
        return @"";
    }
}

- (NSString *)far {
    if ([_far isEqualToString:@"1"]) {
        return @"远程";
    } else if ([_far isEqualToString:@"2"]) {
        return @"近战";
    } else {
        return @"";
    }
}

- (NSString *)nickLength {
    if ([_nickLength isEqualToString:@"1"]) {
        return @"两个及以下";
    } else if ([_nickLength isEqualToString:@"2"]) {
        return @"三个";
    } else if ([_nickLength isEqualToString:@"3"]) {
        return @"四个及以上";
    } else {
        return @"";
    }
}
@end
