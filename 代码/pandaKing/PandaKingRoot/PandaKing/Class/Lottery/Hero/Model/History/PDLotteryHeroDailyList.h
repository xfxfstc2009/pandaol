//
//  PDLotteryHeroDailyList.h
//  PandaKing
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryHeroDailyItem.h"

@interface PDLotteryHeroDailyList : FetchModel
@property (nonatomic, strong) NSArray <PDLotteryHeroDailyItem>*drawList;
@property (nonatomic, assign) BOOL hasNextPage;
@end
