//
//  PDLotteryLotteriesItem.h
//  PandaKing
//
//  Created by Cranz on 16/11/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryShishicaiModel.h"
#import "PDLotteryLolChampion.h" // lol
#import "PDLotteryHeroPvpChampion.h" // 王者荣耀
#import "PDLotterHeroDota2Champion.h" // dota2

@protocol PDLotteryLotteriesItem <NSObject>

@end
@interface PDLotteryLotteriesItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, assign) BOOL draw; /**< 是否开奖*/
@property (nonatomic, assign) NSTimeInterval drawTime;
@property (nonatomic, strong) PDLotteryShishicaiModel *shishicaiInfo;
@property (nonatomic, strong) PDLotteryLolChampion *winPandaLOLChampion;//"<null>"
@property (nonatomic, strong) PDLotteryHeroPvpChampion *winPandaKingChampion;
@property (nonatomic, strong) PDLotterHeroDota2Champion *winPandaDota2Champion;
@end
