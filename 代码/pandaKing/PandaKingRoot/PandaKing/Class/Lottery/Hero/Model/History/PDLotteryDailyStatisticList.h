//
//  PDLotteryDailyStatisticList.h
//  PandaKing
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryDailyStatisticItem.h"

@interface PDLotteryDailyStatisticList : FetchModel
@property (nonatomic, copy) NSString *itemTag;//goldPrice,nickLength,far
@property (nonatomic, assign) NSUInteger totalCount;
@property (nonatomic, strong) NSArray<PDLotteryDailyStatisticItem> *itemList;
@end
