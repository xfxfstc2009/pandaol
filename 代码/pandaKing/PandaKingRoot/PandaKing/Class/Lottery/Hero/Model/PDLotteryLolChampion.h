//
//  PDLotteryLolChampion.h
//  PandaKing
//
//  Created by Cranz on 16/11/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryLolHeroInfo.h"

@interface PDLotteryLolChampion : FetchModel
@property (nonatomic, strong) PDLotteryLolHeroInfo *champion;
@property (nonatomic, assign) NSInteger num;
@end
