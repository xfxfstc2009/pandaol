//
//  PDLotteryHeroPvpJoinerItem.h
//  PandaKing
//
//  Created by Cranz on 17/5/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryHeroPvpStakeInfo.h"
#import "PDLotteryJoinerMemberInfo.h"

@protocol PDLotteryHeroPvpJoinerItem <NSObject>

@end

@interface PDLotteryHeroPvpJoinerItem : FetchModel
@property (nonatomic, copy) NSString *nameLength;
@property (nonatomic, copy) NSString *far;

@property (nonatomic, strong) PDLotteryHeroPvpStakeInfo *stakeInfo;
@property (nonatomic, strong) PDLotteryJoinerMemberInfo *member;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, assign) NSInteger lotteryNum;
@end
