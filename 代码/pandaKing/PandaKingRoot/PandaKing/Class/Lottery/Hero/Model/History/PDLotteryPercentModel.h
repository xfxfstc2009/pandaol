//
//  PDLotteryPercentModel.h
//  PandaKing
//
//  Created by Cranz on 16/11/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDLotteryPercentModel : NSObject
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, assign) CGFloat percent;
@property (nonatomic, strong) UIColor *tagColor;
@property (nonatomic, assign) NSInteger count;
@end
