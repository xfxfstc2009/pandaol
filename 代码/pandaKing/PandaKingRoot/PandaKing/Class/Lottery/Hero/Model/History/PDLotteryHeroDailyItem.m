//
//  PDLotteryHeroDailyItem.m
//  PandaKing
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroDailyItem.h"

@implementation PDLotteryHeroDailyItem
- (NSString *)goldPrice {
    if ([_goldPrice isEqualToString:@"1"]) {
        return @"3150及以下";
    } else if ([_goldPrice isEqualToString:@"2"]) {
        return @"4800";
    } else if ([_goldPrice isEqualToString:@"3"]) {
        return @"6300及以上";
    } else {
        return @"";
    }
}

- (NSString *)far {
    if ([_far isEqualToString:@"1"]) {
        return @"远程";
    } else if ([_far isEqualToString:@"2"]) {
        return @"近战";
    } else {
        return @"";
    }
}

- (NSString *)nickLength {
    if ([_nickLength isEqualToString:@"1"]) {
        return @"两个及以下";
    } else if ([_nickLength isEqualToString:@"2"]) {
        return @"三个";
    } else if ([_nickLength isEqualToString:@"3"]) {
        return @"四个及以上";
    } else {
        return @"";
    }
}

- (NSString *)camp {
    if ([_camp isEqualToString:@"1"]) {
        return @"天辉";
    } else if ([_camp isEqualToString:@"2"]) {
        return @"夜魇";
    } else {
        return @"";
    }
}

- (NSString *)mainProperty {
    if ([_mainProperty isEqualToString:@"1"]) {
        return @"力量";
    } else if ([_mainProperty isEqualToString:@"2"]) {
        return @"敏捷";
    } else if ([_mainProperty isEqualToString:@"3"]) {
        return @"智力";
    } else {
        return @"";
    }
}

- (NSString *)nameLength {
    if ([_nameLength isEqualToString:@"1"]) {
        return @"两个及以下";
    } else if ([_nameLength isEqualToString:@"2"]) {
        return @"三个";
    } else if ([_nameLength isEqualToString:@"3"]) {
        return @"四个及以上";
    } else {
        return @"";
    }
}

@end
