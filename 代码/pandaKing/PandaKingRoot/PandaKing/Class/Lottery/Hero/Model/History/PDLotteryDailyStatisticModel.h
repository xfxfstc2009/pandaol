//
//  PDLotteryDailyStatisticModel.h
//  PandaKing
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryDailyStatisticList.h"

/**
 LOL: goldPrice,nickLength,far
 DOTA2:camp,far,mainProperty
 PVP:far,nameLength
 */
@interface PDLotteryDailyStatisticModel : FetchModel
@property (nonatomic, strong) PDLotteryDailyStatisticList *goldPrice;
@property (nonatomic, strong) PDLotteryDailyStatisticList *nickLength;
@property (nonatomic, strong) PDLotteryDailyStatisticList *far;
@property (nonatomic, strong) PDLotteryDailyStatisticList *camp;
@property (nonatomic, strong) PDLotteryDailyStatisticList *mainProperty;
@property (nonatomic, strong) PDLotteryDailyStatisticList *nameLength;
@end
