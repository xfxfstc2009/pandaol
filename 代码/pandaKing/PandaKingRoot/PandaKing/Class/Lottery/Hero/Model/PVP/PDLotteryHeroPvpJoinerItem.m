//
//  PDLotteryHeroPvpJoinerItem.m
//  PandaKing
//
//  Created by Cranz on 17/5/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroPvpJoinerItem.h"

@implementation PDLotteryHeroPvpJoinerItem
- (NSDictionary *)modelKeyJSONKeyMapper {
    return @{@"ID":@"id"};
}

- (NSString *)far {
    if ([_far isEqualToString:@"1"]) {
        return @"远程";
    } else if ([_far isEqualToString:@"2"]) {
        return @"近战";
    } else {
        return @"";
    }
}

- (NSString *)nameLength {
    if ([_nameLength isEqualToString:@"1"]) {
        return @"两个及以下";
    } else if ([_nameLength isEqualToString:@"2"]) {
        return @"三个";
    } else if ([_nameLength isEqualToString:@"3"]) {
        return @"四个及以上";
    } else {
        return @"";
    }
}

@end
