//
//  PDLotterHeroDota2Champion.h
//  PandaKing
//
//  Created by Cranz on 17/5/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryHeroDota2HeroInfo.h"

@interface PDLotterHeroDota2Champion : FetchModel
@property (nonatomic, strong) PDLotteryHeroDota2HeroInfo *champion;
@property (nonatomic, assign) NSUInteger num;
@end
