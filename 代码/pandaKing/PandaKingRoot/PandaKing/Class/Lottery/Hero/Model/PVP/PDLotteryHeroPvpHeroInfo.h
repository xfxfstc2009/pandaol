//
//  PDLotteryHeroPvpHeroInfo.h
//  PandaKing
//
//  Created by Cranz on 17/5/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDLotteryHeroPvpHeroInfo : FetchModel
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *ID;
//male
@property (nonatomic, copy) NSString *gender;
@property (nonatomic, assign) BOOL far;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *showPic;
@end
