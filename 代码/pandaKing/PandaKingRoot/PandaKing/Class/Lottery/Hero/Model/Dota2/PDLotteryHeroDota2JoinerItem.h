//
//  PDLotteryHeroDota2JoinerItem.h
//  PandaKing
//
//  Created by Cranz on 17/5/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryJoinerMemberInfo.h"
#import "PDLotteryHeroDota2StakeInfo.h"

@protocol PDLotteryHeroDota2JoinerItem <NSObject>

@end

@interface PDLotteryHeroDota2JoinerItem : FetchModel
@property (nonatomic, copy) NSString *camp;
@property (nonatomic, copy) NSString *far;
@property (nonatomic, copy) NSString *mainProperty;

@property (nonatomic, copy) NSString *ID;
@property (nonatomic, assign) NSInteger lotteryNum;
@property (nonatomic, strong) PDLotteryJoinerMemberInfo *member;
@property (nonatomic, strong) PDLotteryHeroDota2StakeInfo *stakeInfo;
@end
