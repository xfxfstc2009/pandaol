//
//  PDLotteryHeroDota2HeroInfo.h
//  PandaKing
//
//  Created by Cranz on 17/5/26.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDLotteryHeroDota2HeroInfo : FetchModel
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *camp;
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, assign) BOOL far;
@property (nonatomic, copy) NSString *mainProperty;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *nick;
@property (nonatomic, copy) NSString *showPic;
@end
