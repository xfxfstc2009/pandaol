//
//  PDLotteryHeroPvpJoinerList.h
//  PandaKing
//
//  Created by Cranz on 17/5/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryHeroPvpJoinerItem.h"

@interface PDLotteryHeroPvpJoinerList : FetchModel
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, strong) NSArray<PDLotteryHeroPvpJoinerItem> *items;
@end
