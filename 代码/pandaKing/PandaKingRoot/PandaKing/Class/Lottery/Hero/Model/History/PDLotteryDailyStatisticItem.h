//
//  PDLotteryDailyStatisticItem.h
//  PandaKing
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDLotteryDailyStatisticItem <NSObject>

@end
@interface PDLotteryDailyStatisticItem : FetchModel
@property (nonatomic, assign) int op; //选项
@property (nonatomic, assign) int count;
@property (nonatomic, copy) NSString *percent; // 百分比
@end
