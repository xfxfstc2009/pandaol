//
//  PDLotteryHeroLotteryInfo.h
//  PandaKing
//
//  Created by Cranz on 16/11/2.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 用来包装投注信息
@interface PDLotteryHeroLotteryInfo : FetchModel
// lol
@property (nonatomic, copy) NSString *goldPrice;
@property (nonatomic, copy) NSString *far;
@property (nonatomic, copy) NSString *nickLength;
// dota2
@property (nonatomic, copy) NSString *dota2Camp;
@property (nonatomic, copy) NSString *dota2Far;
@property (nonatomic, copy) NSString *dota2MainProperty;
// 王者荣耀
@property (nonatomic, copy) NSString *pvpNameLength;
@property (nonatomic, copy) NSString *pvpFar;

@property (nonatomic, copy) NSString *odds; /**< 赔率*/
@property (nonatomic, assign) NSInteger gold;   /**< 会员金币资产*/
@property (nonatomic, assign) NSInteger input; /**< 投注金币*/

// 投注的编号
@property (nonatomic, assign) NSInteger selIndex1; // 第一行
@property (nonatomic, assign) NSInteger selIndex2; // 2
@property (nonatomic, assign) NSInteger selIndex3; // 3
@end

