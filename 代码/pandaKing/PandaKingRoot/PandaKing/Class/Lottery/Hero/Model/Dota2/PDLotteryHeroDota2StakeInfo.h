//
//  PDLotteryHeroDota2StakeInfo.h
//  PandaKing
//
//  Created by Cranz on 17/5/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDLotteryHeroDota2StakeInfo : FetchModel
@property (nonatomic, assign) BOOL chosen;
@property (nonatomic, assign) BOOL draw;
@property (nonatomic, assign) NSTimeInterval drawTime;
@property (nonatomic, assign) NSUInteger gold;
@property (nonatomic, copy) NSString *odds;
@property (nonatomic, assign) NSTimeInterval stakeTime;
@property (nonatomic, assign) NSUInteger winGold;
@end
