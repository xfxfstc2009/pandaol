//
//  PDLottryButtonModel.h
//  PandaKing
//
//  Created by Cranz on 16/10/24.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PDLottryButtonModel : NSObject
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) NSInteger row;
@end
