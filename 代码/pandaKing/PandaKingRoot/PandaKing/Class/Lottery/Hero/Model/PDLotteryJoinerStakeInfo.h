//
//  PDLotteryJoinerStakeInfo.h
//  PandaKing
//
//  Created by Cranz on 16/11/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDLotteryJoinerStakeInfo : FetchModel
@property (nonatomic, assign) BOOL draw; /**< 是否开奖*/
@property (nonatomic, assign) NSInteger gold;
@property (nonatomic, copy) NSString *odds;
@property (nonatomic, assign) NSTimeInterval stakeTime;
@property (nonatomic, assign) BOOL win; /**< 是否中奖*/
@property (nonatomic, assign) NSInteger winGold;
@end
