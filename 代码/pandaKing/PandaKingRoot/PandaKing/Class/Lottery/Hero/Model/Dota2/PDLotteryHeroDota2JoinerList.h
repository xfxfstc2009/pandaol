//
//  PDLotteryHeroDota2JoinerList.h
//  PandaKing
//
//  Created by Cranz on 17/5/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryHeroDota2JoinerItem.h"

@interface PDLotteryHeroDota2JoinerList : FetchModel
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, strong) NSArray<PDLotteryHeroDota2JoinerItem> *items;
@end
