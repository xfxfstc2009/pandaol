//
//  PDLotteryHeroDetail.h
//  PandaKing
//
//  Created by Cranz on 16/11/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/**
 draw:期号
 currentTime:服务器当前时间
 leftTime:服务器距离开奖剩余时间
 joiners:今日参与人数
 totalGold:今日竞猜池累积金币
 currentDrawGold:本期竞猜池已有金币
 memberGold:会员本期投入
 */
@interface PDLotteryHeroDetail : FetchModel
@property (nonatomic, assign) NSInteger draw;
@property (nonatomic, assign) NSTimeInterval currentTime;
@property (nonatomic, assign) NSTimeInterval leftTime;
@property (nonatomic, assign) NSInteger joiners;
@property (nonatomic, assign) NSInteger totalGold;
@property (nonatomic, assign) NSInteger currentDrawGold;
@property (nonatomic, assign) NSInteger memberGold;
@property (nonatomic, assign) NSInteger dayTime;
@property (nonatomic, assign) NSInteger firstStakeTime; /**< 第一次投注时间*/
@property (nonatomic, copy) NSString *imGroupId; // 聊天室id
@end
