//
//  PDLotteryHeroUrlHelper.m
//  PandaKing
//
//  Created by Cranz on 17/5/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroUrlHelper.h"

@implementation PDLotteryHeroUrlHelper
/**
 * 最新一期
 */
+ (NSString *)lastDetailUrlExchange {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        return lotteryHeroLolLastedDetail;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        return lotteryHeroDota2LastedDetail;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        return lotteryHeroPvpLastedDetail;
    } else {
        return nil;
    }
}

/**
 * 投注
 */
+ (NSString *)inputUrlExchange {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        return lotteryHeroLolInput;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        return lotteryHeroDota2Input;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        return lotteryHeroPvpInput;
    } else {
        return nil;
    }
}

/**
 * 开奖纪录
 */
+ (NSString *)lotteriesRecordUrlExchange {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        return lotteryHeroLolLotteriesRecord;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        return lotteryHeroDota2LotteriesRecord;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        return lotteryHeroPvpLotteriesRecord;
    } else {
        return nil;
    }
}

/**
 * 投注纪录
 */
+ (NSString *)inputRecordUrlExchange {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        return lotteryHeroLolInputRecord;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        return lotteryHeroDota2InputRecord;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        return lotteryHeroPvpInputRecord;
    } else {
        return nil;
    }
}

/**
 * 投注排行
 */
+ (NSString *)rankListUrlExchange {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        return lotteryHeroLolRankList;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        return lotteryHeroDota2RankList;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        return lotteryHeroPvpRankList;
    } else {
        return nil;
    }
}
/**
 * 今日开奖列表
 */
+ (NSString *)historyListUrlExhchange {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        return lotteryHeroLolDailyHistory;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        return lotteryHeroDota2HistoryList;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        return lotteryHeroPvpHistoryList;
    } else {
        return nil;
    }
}
/**
 * 今日统计
 */
+ (NSString *)statisticUrlExchange {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        return lotteryHeroLolDailyStatistics;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        return lotteryHeroDota2HistoryStatistics;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        return lotteryHeroPvpHistoryStatistics;
    } else {
        return nil;
    }
}
/**
 * 获取赔率
 */
+ (NSString *)getOddsUrlExchange {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        return lotteryHeroLolGetGuessOdds;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        return lotteryHeroDota2GetGuessOdds;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        return lotteryHeroPvpGetGuessOdds;
    } else {
        return nil;
    }
}

/**
 * 我的竞猜
 */
+ (NSString *)myLotteryListUrlExchange {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        return centerHeroLottery;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        return centerHeroDota2Lottery;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        return centerHeroKingLottery;
    } else {
        return nil;
    }
}

@end
