//
//  PDLotteryLolHeroInfo.h
//  PandaKing
//
//  Created by Cranz on 16/11/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDLotteryLolHeroInfo : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *nick;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *showPic;// 英雄的展示图
@property (nonatomic, copy) NSString *title;
@property (nonatomic, assign) NSInteger goldPrice;
@property (nonatomic, assign) BOOL far;
@end
