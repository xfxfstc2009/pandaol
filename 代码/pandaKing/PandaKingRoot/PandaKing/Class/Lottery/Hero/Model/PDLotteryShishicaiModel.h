//
//  PDLotteryShishicaiModel.h
//  PandaKing
//
//  Created by Cranz on 16/11/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 英雄猜实时猜model
@interface PDLotteryShishicaiModel : FetchModel
@property (nonatomic, assign) NSTimeInterval dayTime;
@property (nonatomic, copy)   NSString *drawCode;
@property (nonatomic, assign) NSTimeInterval drawTime;
@property (nonatomic, assign) NSInteger num;    /**< 期号*/
@property (nonatomic, assign) NSInteger valid;
@end
