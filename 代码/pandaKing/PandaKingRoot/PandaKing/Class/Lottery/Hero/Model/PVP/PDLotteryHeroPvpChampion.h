//
//  PDLotteryHeroPvpChampion.h
//  PandaKing
//
//  Created by Cranz on 17/5/23.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDLotteryHeroPvpHeroInfo.h"

@interface PDLotteryHeroPvpChampion : FetchModel
@property (nonatomic, strong) PDLotteryHeroPvpHeroInfo *champion;
@property (nonatomic, assign) NSUInteger num;
@end
