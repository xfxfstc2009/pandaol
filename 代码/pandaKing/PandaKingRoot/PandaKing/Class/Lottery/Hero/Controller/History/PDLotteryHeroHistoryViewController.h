//
//  PDLotteryHeroHistoryViewController.h
//  PandaKing
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 英雄猜历史分析控制器
@interface PDLotteryHeroHistoryViewController : AbstractViewController

@end
