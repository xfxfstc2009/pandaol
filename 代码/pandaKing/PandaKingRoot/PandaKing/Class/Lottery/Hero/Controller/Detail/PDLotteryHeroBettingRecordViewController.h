//
//  PDLotteryHeroBettingRecordViewController.h
//  PandaKing
//
//  Created by Cranz on 16/12/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDLotteryHeroDetail.h"

/// 投注记录
@interface PDLotteryHeroBettingRecordViewController : UIViewController
@property (nonatomic, strong) PDLotteryHeroDetail *heroDetail;
@property (nonatomic, assign) CGFloat buttonHeight;

- (void)startToUpdateData:(PDLotteryHeroDetail *)heroDetail;
@end
