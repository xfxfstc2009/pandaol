//
//  PDLotteryHeroRankListViewController.h
//  PandaKing
//
//  Created by Cranz on 17/2/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 英雄猜竞猜排行榜
@interface PDLotteryHeroRankListViewController : UIViewController
@property (nonatomic, assign) CGFloat buttonHeight;

- (void)updateRankList;
@end
