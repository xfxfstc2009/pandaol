//
//  PDLotteryHeroChatRoomViewController.m
//  PandaKing
//
//  Created by Cranz on 16/12/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroChatRoomViewController.h"
#import "CCZSegmentController.h"

static const int kTempHeight = 32; // 留给在线人数的高度
static const int kBaseCount = 83; // 虚假的在线人数
@interface PDLotteryHeroChatRoomViewController ()
@property (nonatomic, strong) UILabel *memberCountLabel;
@property (nonatomic, strong) EMChatroom *myRoom;
@property (nonatomic, strong) PDEMMessageViewController *chatRoomViewController;
@end

@implementation PDLotteryHeroChatRoomViewController

- (void)dealloc {
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self addChattingRoomView];
}

- (void)addChattingRoomView {
    self.chatRoomViewController = [[PDEMManager sharedInstance] conversationWithChatroom:self.imGroupId];
    [self renderChatViewWithChatroomViewController:self.chatRoomViewController];
}

- (void)renderChatViewWithChatroomViewController:(PDEMMessageViewController *)chatroomViewController {
    [self addChildViewController:self.chatRoomViewController];
    [self.view addSubview:self.chatRoomViewController.view];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    // 在线人数显示
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kTempHeight)];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(headerView.frame) - 1, kScreenBounds.size.width, 1)];
    line2.backgroundColor = c27;
    [headerView addSubview:line2];

    self.chatRoomViewController.tableView.frame = CGRectMake(self.chatRoomViewController.tableView.frame.origin.x, self.chatRoomViewController.tableView.frame.origin.y + kTempHeight, self.chatRoomViewController.tableView.frame.size.width, self.chatRoomViewController.tableView.frame.size.height - kTempHeight);
    [self.chatRoomViewController.view addSubview:headerView];
    // 群聊公告
    UILabel *descLabel = [[UILabel alloc] init];
    descLabel.text = @"欢迎进入讨论区请文明发言";
    descLabel.font = [UIFont systemFontOfCustomeSize:14];
    descLabel.textColor = [UIColor grayColor];
    CGSize descSize = [descLabel.text sizeWithCalcFont:descLabel.font];
    descLabel.frame = CGRectMake(kTableViewSectionHeader_left, (kTempHeight - descSize.height) / 2, descSize.width, descSize.height);
    [headerView addSubview:descLabel];
    
    // 人数
    self.memberCountLabel = [[UILabel alloc] init];
    self.memberCountLabel.font = descLabel.font;
    self.memberCountLabel.textColor = c26;
    [headerView addSubview:self.memberCountLabel];
    
    self.registScrollView = self.chatRoomViewController.tableView;
    
    __weak typeof(self) weakSelf = self;
    [[PDEMManager sharedInstance] chatroomChangeInfo:^(EMChatroom *room) {
        [weakSelf getChatroomInfoWithRoom:room];
    }];
    
    dispatch_async(dispatch_get_global_queue(0, 0), ^{
        EMChatroom *room = [[EMClient sharedClient].roomManager fetchChatroomInfo:self.imGroupId includeMembersList:NO error:nil];
        self.chatRoomViewController.transferRoom = room;
        dispatch_sync(dispatch_get_main_queue(), ^{
            [self getChatroomInfoWithRoom:room];
        });
    });
}

- (void)viewWillAppear {

}

- (void)getChatroomInfoWithRoom:(EMChatroom *)room {
    self.memberCountLabel.text = [NSString stringWithFormat:@"在线人数%ld人",room.membersCount == 0? kBaseCount : room.membersCount + kBaseCount];
    CGSize countSize = [self.memberCountLabel.text sizeWithCalcFont:self.memberCountLabel.font];
    self.memberCountLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - countSize.width, (kTempHeight - countSize.height) / 2, countSize.width, countSize.height);
}

- (void)releaseKeyboard{
    EaseChatToolbar *toolBar = (EaseChatToolbar *)self.chatRoomViewController.chatToolbar;
    if ([toolBar.inputTextView isFirstResponder]){
        [toolBar.inputTextView resignFirstResponder];
    }
}

@end
