//
//  PDLotteryHeroBettingRecordViewController.m
//  PandaKing
//
//  Created by Cranz on 16/12/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroBettingRecordViewController.h"
#import "CCZSegmentController.h"
#import "PDLotteryJoinersList.h" // lol
#import "PDLotteryHeroDota2JoinerList.h" // dota2
#import "PDLotteryHeroPvpJoinerList.h" // 王者荣耀
#import "PDLotteryJoinerCell.h"
// 根据不同游戏转换url的路径
#import "PDLotteryHeroUrlHelper.h"


@interface PDLotteryHeroBettingRecordViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, strong) NSMutableArray *joinerArr;
@property (nonatomic, strong) PDLotteryJoinersList *joinersList;
@property (nonatomic, strong) PDLotteryHeroDota2JoinerList *joinersDota2List;
@property (nonatomic, strong) PDLotteryHeroPvpJoinerList *joinersPvpList;
@property (nonatomic, strong) UILabel *textLabel;
@end

@implementation PDLotteryHeroBettingRecordViewController

- (void)dealloc {
    
}

- (NSMutableArray *)joinerArr {
    if (!_joinerArr) {
        _joinerArr = [NSMutableArray array];
    }
    return _joinerArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self headerViewSetting];
    [self pageSetting];
    [self updateDateWithDetail:self.heroDetail];
    [self fetchInputRecord];
}

- (void)headerViewSetting {
    CGFloat contentHeight = 30;
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, contentHeight)];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.view addSubview:headerView];
    
//    UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kTableViewHeader_height)];
//    tempView.backgroundColor = BACKGROUND_VIEW_COLOR;
//    [headerView addSubview:tempView];
    
//    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, kTableViewHeader_height, kScreenBounds.size.width, 1)];
//    line1.backgroundColor = c27;
//    [headerView addSubview:line1];
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(headerView.frame) - 1, kScreenBounds.size.width, 1)];
    line2.backgroundColor = c27;
    [headerView addSubview:line2];
    
    UILabel *textLabel = [[UILabel alloc] init];
    textLabel.font = [UIFont systemFontOfCustomeSize:14];
    textLabel.textColor = c3;
    [headerView addSubview:textLabel];
    self.textLabel = textLabel;
}

- (void)pageSetting {
    // 立即竞猜
    _page = 1;
    
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 30, kScreenBounds.size.width, self.view.frame.size.height - 30 - _buttonHeight - 64 - _buttonHeight)];
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.tableFooterView = [[UIView alloc] init];
    self.mainTableView.rowHeight = [PDLotteryJoinerCell cellHeight];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    self.registScrollView = self.mainTableView;
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.joinerArr.count) {
            [weakSelf.joinerArr removeAllObjects];
        }
        [weakSelf fetchInputRecord];
    }];
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.joinersList.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchInputRecord];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
        }
    }];
}

#pragma mark - UITableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.joinerArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *joinerCellId = @"joinerCellId";
    PDLotteryJoinerCell *joinerCell = [tableView dequeueReusableCellWithIdentifier:joinerCellId];
    if (!joinerCell) {
        joinerCell = [[PDLotteryJoinerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:joinerCellId];
    }
    if (self.joinerArr.count > indexPath.row) {
        if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
            joinerCell.model = [self.joinerArr objectAtIndex:indexPath.row];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
            joinerCell.pvpItem = [self.joinerArr objectAtIndex:indexPath.row];
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
            joinerCell.dota2Item = [self.joinerArr objectAtIndex:indexPath.row];
        }
    }
    
    if (indexPath.row % 2 == 0) {
        joinerCell.backgroundColor = c33;
    } else {
        joinerCell.backgroundColor = c1;
    }
    
    return joinerCell;
}

#pragma mark - 网路请求

- (void)updateDateWithDetail:(PDLotteryHeroDetail *)heroDetail {
    NSString *dateStr;
    dateStr = [PDCenterTool dateWithFormat:@"yyyy-MM-dd HH:mm:ss" fromTimestamp:heroDetail.firstStakeTime];
    
//    NSMutableAttributedString *leftAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"竞猜记录(自%@开始)", dateStr]];
//    [leftAtt addAttributes:@{NSFontAttributeName: [UIFont systemFontOfCustomeSize:13], NSForegroundColorAttributeName: [UIColor grayColor]} range:NSMakeRange(4, 5 + dateStr.length)];
//    self.textLabel.attributedText = [leftAtt copy];
    self.textLabel.text = @"竞猜记录";
    
    CGSize textSize = [self.textLabel.text sizeWithCalcFont:self.textLabel.font];
    self.textLabel.frame = CGRectMake(kTableViewSectionHeader_left, (32 - textSize.height) / 2, textSize.width, textSize.height);
}

- (void)startToUpdateData:(PDLotteryHeroDetail *)heroDetail {
    self.heroDetail = heroDetail;
    
    if (self.mainTableView == nil) {
        return;
    }
    
    _page = 1;
    if (self.joinerArr.count) {
        [self.joinerArr removeAllObjects];
    }
    [self updateDateWithDetail:self.heroDetail];
    [self fetchInputRecord];
}

- (void)fetchInputRecord {
    __weak typeof(self) weakSelf = self;
    Class cls;
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        cls = [PDLotteryJoinersList class];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        cls = [PDLotteryHeroPvpJoinerList class];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        cls = [PDLotteryHeroDota2JoinerList class];
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDLotteryHeroUrlHelper inputRecordUrlExchange] requestParams:@{@"pageNum":@(_page)} responseObjectClass:cls succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
                PDLotteryJoinersList *list = (PDLotteryJoinersList *)responseObject;
                weakSelf.joinersList = list;
                [weakSelf.joinerArr addObjectsFromArray:list.items];
            } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
                PDLotteryHeroPvpJoinerList *list = (PDLotteryHeroPvpJoinerList *)responseObject;
                weakSelf.joinersPvpList = list;
                [weakSelf.joinerArr addObjectsFromArray:list.items];
            } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
                PDLotteryHeroDota2JoinerList *list = (PDLotteryHeroDota2JoinerList *)responseObject;
                weakSelf.joinersDota2List = list;
                [weakSelf.joinerArr addObjectsFromArray:list.items];
            }
            
            [weakSelf.mainTableView reloadData];
            [weakSelf.mainTableView stopPullToRefresh];
            [weakSelf.mainTableView stopFinishScrollingRefresh];
        }
    }];
}



@end
