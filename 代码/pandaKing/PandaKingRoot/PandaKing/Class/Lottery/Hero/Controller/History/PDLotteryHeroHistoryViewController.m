//
//  PDLotteryHeroHistoryViewController.m
//  PandaKing
//
//  Created by Cranz on 16/11/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroHistoryViewController.h"
#import "PDLotteryHeroHistoryCell.h"
#import "CCZBrowseView.h"
#import "PDLotteryHistoryPieCell.h"
#import "PDLotteryHeroDailyList.h"
#import "PDLotteryDailyStatisticModel.h"
#import "PDLotteryHeroUrlHelper.h"

CGFloat const pieViewHeight = 693/2 + 20;
@interface PDLotteryHeroHistoryViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) PDLotteryHeroDailyList *list;
@property (nonatomic, strong) CCZBrowseView *browseView;
@property (nonatomic, strong) UIImageView *titleBar;
@end

@implementation PDLotteryHeroHistoryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchDailyHistory];
    [self fetchDailyStatistic];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)basicSetting {
    self.itemsArr = [NSMutableArray array];
    _page = 1;
}

- (void)pageSetting {
    self.titleBar = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    self.titleBar.backgroundColor = c2;
    self.titleBar.userInteractionEnabled = YES;
    self.titleBar.image = [UIImage imageNamed:@"icon_lottery_history_up"];
    [self.view addSubview:self.titleBar];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(kTableViewSectionHeader_left, 20 + (44 - 38) / 2, 25, 38)];
    [backButton setImage:[UIImage imageNamed:@"icon_center_back"] forState:UIControlStateNormal];
    __weak typeof(self) weakSelf = self;
    [backButton buttonWithBlock:^(UIButton *button) {
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.titleBar addSubview:backButton];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"历史分析";
    titleLabel.font = [[UIFont systemFontOfCustomeSize:18] boldFont];
    titleLabel.textColor = [UIColor whiteColor];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, (44 - titleSize.height) / 2 + 20, titleSize.width, titleSize.height);
    [self.titleBar addSubview:titleLabel];
    
    CCZBrowseView *browseView = [[CCZBrowseView alloc] initBrowseViewWithFrame:CGRectMake(0, 64, kScreenBounds.size.width, LCFloat(pieViewHeight) - 64) registerCustomCell:[PDLotteryHistoryPieCell class]];
    browseView.backgroundColor = c2;
    browseView.itemNum = 0;
    browseView.scale = 1.2;
    browseView.itemSpace = LCFloat(110);
    browseView.leftRightSpace = LCFloat(110);
    browseView.itemSize = CGSizeMake(LCFloat(130), LCFloat(170 + 60));
    browseView.backgroundImage = [UIImage imageNamed:@"icon_lottery_history_bg"];
//    browseView.wannaToTapScrollItem = YES;
    [self.view addSubview:browseView];
    self.browseView = browseView;
    
    //开奖公布
    UILabel *openLabel = [[UILabel alloc] init];
    openLabel.text = @"今日开奖分布";
    openLabel.font = [[UIFont systemFontOfCustomeSize:17] boldFont];
    openLabel.textColor = c26;
    CGSize openSize = [openLabel.text sizeWithCalcFont:openLabel.font];
    openLabel.frame = CGRectMake((kScreenBounds.size.width - openSize.width) / 2, CGRectGetHeight(browseView.frame) - openSize.height - LCFloat(5), openSize.width, openSize.height);
    [browseView addSubview:openLabel];
    
    UIImageView *bg2 = [[UIImageView alloc] initWithFrame:browseView.mainView.bounds];
    bg2.image = [UIImage imageNamed:@"icon_lottery_history_bottom"];
    bg2.userInteractionEnabled = YES;
    [browseView insertSubview:bg2 belowSubview:browseView.mainView];
    
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, LCFloat(pieViewHeight) + 9, kScreenBounds.size.width, kScreenBounds.size.height - LCFloat(pieViewHeight))];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    [self.mainTableView appendingPullToRefreshHandler:^{
        if (weakSelf.itemsArr.count) {
            [weakSelf.itemsArr removeAllObjects];
        }
        
        weakSelf.page = 1;
        [weakSelf fetchDailyHistory];
    }];
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.list.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchDailyHistory];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
        }
    }];
    
    [self.view bringSubviewToFront:self.titleBar];
}

#pragma mark - UITablewView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.itemsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"historyCellId";
    PDLotteryHeroHistoryCell *historyCell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!historyCell) {
        historyCell = [[PDLotteryHeroHistoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    if (self.itemsArr.count > indexPath.row) {
        historyCell.model = self.itemsArr[indexPath.row];
    }
    
    if (indexPath.row % 2 == 1) {
        historyCell.backgroundColor = c33;
    } else {
        historyCell.backgroundColor = BACKGROUND_VIEW_COLOR;
    }
    
    return historyCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PDLotteryHeroHistoryCell cellHeight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 32;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel *subLabel = [[UILabel alloc] init];
    subLabel.text = @"开奖期数";
    subLabel.textColor = c3;
    subLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
    CGSize subSize = [subLabel.text sizeWithCalcFont:subLabel.font];
    subLabel.frame = CGRectMake(kTableViewSectionHeader_left, (32 - subSize.height) / 2, subSize.width, subSize.height);
    [headerView addSubview:subLabel];
    
    UILabel *mainLabel = [[UILabel alloc] init];
    mainLabel.text = @"幸运英雄";
    mainLabel.textColor = c3;
    mainLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
    CGSize mainSize = [mainLabel.text sizeWithCalcFont:mainLabel.font];
    mainLabel.frame = CGRectMake(CGRectGetMaxX(subLabel.frame) + (kScreenBounds.size.width - kTableViewSectionHeader_left - subSize.width - mainSize.width) / 2, (32 - mainSize.height) / 2, mainSize.width, mainSize.height);
    [headerView addSubview:mainLabel];
    return headerView;
}

#pragma mark - 网络请求

- (void)fetchDailyStatistic {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDLotteryHeroUrlHelper statisticUrlExchange] requestParams:nil responseObjectClass:[PDLotteryDailyStatisticModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            if (([AccountModel sharedAccountModel].gameType == GameTypeLoL) || ([AccountModel sharedAccountModel].gameType == GameTypeDota2)) {
                 weakSelf.browseView.itemNum = 3;
            } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
                 weakSelf.browseView.itemNum = 2;
            }
           
            PDLotteryDailyStatisticModel *statisticModel = (PDLotteryDailyStatisticModel *)responseObject;
            weakSelf.browseView.model = statisticModel;
            [weakSelf.browseView setBrowseSelectedAtIndex:1];
        }
    }];
}

- (void)fetchDailyHistory {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDLotteryHeroUrlHelper historyListUrlExhchange] requestParams:@{@"page":@(_page)} responseObjectClass:[PDLotteryHeroDailyList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDLotteryHeroDailyList *list = (PDLotteryHeroDailyList *)responseObject;
            weakSelf.list = list;
            [weakSelf.itemsArr addObjectsFromArray:list.drawList];
            [weakSelf.mainTableView reloadData];
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            [weakSelf.mainTableView stopPullToRefresh];
        }
    }];
}

@end
