//
//  PDLotteryHeroLotteryRecordsViewController.h
//  PandaKing
//
//  Created by Cranz on 16/12/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDLotteryDrawDetailCell.h"
#import "PDLotteryHeroDetail.h"

/// 开奖纪录控制器
@interface PDLotteryHeroLotteryRecordsViewController : UIViewController
@property (nonatomic, strong) PDLotteryDrawDetailCell *detailCell; // 正在开奖的那个cell
@property (nonatomic, assign) CGFloat buttonHeight;

- (void)startToUpdateData:(PDLotteryHeroDetail *)heroDetail fetchComplication:(void(^)())complication;
@end
