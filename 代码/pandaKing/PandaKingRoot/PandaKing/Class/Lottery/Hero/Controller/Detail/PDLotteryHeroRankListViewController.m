//
//  PDLotteryHeroRankListViewController.m
//  PandaKing
//
//  Created by Cranz on 17/2/20.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroRankListViewController.h"
#import "PDRankListCell.h"
#import "CCZSegmentController.h"
#import "PDRankThList.h"
// 根据不同游戏转换url的路径
#import "PDLotteryHeroUrlHelper.h"

@interface PDLotteryHeroRankListViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) NSMutableArray *rankerArr;
@property (nonatomic, strong) UITableView *mainTableView;
@end

@implementation PDLotteryHeroRankListViewController

- (void)dealloc {
    
}

- (NSMutableArray *)rankerArr {
    if (!_rankerArr) {
        _rankerArr = [NSMutableArray array];
    }
    return _rankerArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self headerViewSetting];
    [self pageSetting];
    [self fetchRankList];
}

- (void)headerViewSetting {
    CGFloat contentHeight = 32;
    
    UIView *sectionView = [[UIView alloc] init];
    sectionView.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    // 排名
    UILabel *pmLabel = [self labelCopy];
    pmLabel.text = @"排名";
    UILabel *yhmLabel = [self labelCopy];
    yhmLabel.text = @"用户名";
    UILabel *jbsLabel = [self labelCopy];
    jbsLabel.text = @"投注金额";
    
    CGSize pmSize = [pmLabel.text sizeWithCalcFont:pmLabel.font];
    CGSize yhmSize = [yhmLabel.text sizeWithCalcFont:yhmLabel.font];
    CGSize jbsSize = [jbsLabel.text sizeWithCalcFont:jbsLabel.font];
    
    pmLabel.frame = CGRectMake(kTableViewSectionHeader_left, (contentHeight - pmSize.height) / 2, pmSize.width, pmSize.height);
    yhmLabel.frame = CGRectMake(CGRectGetMaxX(pmLabel.frame) + LCFloat(40), (contentHeight - yhmSize.height) / 2, yhmSize.width, yhmSize.height);
    jbsLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - 10 - jbsSize.width, (contentHeight - jbsSize.height) / 2, jbsSize.width, jbsSize.height);
    
    [sectionView addSubview:pmLabel];
    [sectionView addSubview:yhmLabel];
    [sectionView addSubview:jbsLabel];
    
    [self.view addSubview:sectionView];
}

- (UILabel *)labelCopy {
    UILabel *label = [[UILabel alloc] init];
    label.font = [UIFont systemFontOfCustomeSize:12];
    label.textColor = RGB(153, 153, 153, 1);
    return label;
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 30, kScreenBounds.size.width, self.view.frame.size.height - 30 - _buttonHeight - 64 - _buttonHeight)];
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.tableFooterView = [[UIView alloc] init];
    self.mainTableView.rowHeight = [PDRankListCell cellHeight];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    self.registScrollView = self.mainTableView;
}

- (void)updateRankList {
    [self fetchRankList];
}

#pragma mark - UITableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.rankerArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellId = @"lotteryHeroRankListId";
    PDRankListCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
    if (!cell) {
        cell = [[PDRankListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
    }
    cell.thModel = self.rankerArr[indexPath.row];
    return cell;
}

#pragma mark - 网络请求

- (void)fetchRankList {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDLotteryHeroUrlHelper rankListUrlExchange] requestParams:nil responseObjectClass:[PDRankThList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        
        if (isSucceeded) {
            PDRankThList *list = (PDRankThList *)responseObject;
            if (weakSelf.rankerArr.count) {
                [weakSelf.rankerArr removeAllObjects];
            }
            [weakSelf.rankerArr addObjectsFromArray:list.items];
            [weakSelf.mainTableView reloadData];
        }
    }];
}


@end
