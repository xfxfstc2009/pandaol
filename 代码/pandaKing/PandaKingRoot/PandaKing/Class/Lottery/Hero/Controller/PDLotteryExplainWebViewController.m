//
//  PDLotteryExplainWebViewController.m
//  PandaKing
//
//  Created by Cranz on 16/11/4.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryExplainWebViewController.h"

@interface PDLotteryExplainWebViewController ()

@end

@implementation PDLotteryExplainWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.barMainTitle = @"英雄猜规则";
    [self webPageSetting];
}

- (void)webPageSetting {
    __weak typeof(self) weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_center_back"] barHltImage:[UIImage imageNamed:@"icon_center_back"] action:^{
        [weakSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
    }];
}

@end
