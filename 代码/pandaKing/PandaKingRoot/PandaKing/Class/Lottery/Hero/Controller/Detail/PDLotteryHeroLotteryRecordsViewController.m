//
//  PDLotteryHeroLotteryRecordsViewController.m
//  PandaKing
//
//  Created by Cranz on 16/12/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLotteryHeroLotteryRecordsViewController.h"
#import "CCZSegmentController.h"
#import "PDLotteryHeroDetailView.h"
#import "PDLotteryLotteriesList.h"
// 根据不同游戏转换url的路径
#import "PDLotteryHeroUrlHelper.h"

typedef void(^PDFetchComplication)();
@interface PDLotteryHeroLotteryRecordsViewController ()<UITableViewDataSource, UITableViewDelegate, PDLotteryDrawDetailCellDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) NSMutableArray *drawArr;  // 开奖记录数组
// 弹出英雄详情
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) PDLotteryHeroDetailView *heroDetailView;
@property (nonatomic, strong) UIImageView *headerImageView;

@property (nonatomic, assign) NSUInteger page;
@property (nonatomic, strong) PDLotteryLotteriesList *lotteriesList;
@property (nonatomic, strong) PDLotteryHeroDetail *heroDetail; // 本期英雄猜详情

@property (nonatomic, copy) PDFetchComplication fetchComplication;
@end

@implementation PDLotteryHeroLotteryRecordsViewController

- (void)dealloc {
    
}

- (NSMutableArray *)drawArr {
    if (!_drawArr) {
        _drawArr = [NSMutableArray array];
    }
    return _drawArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self headerViewSetting];
    [self pageSetting];
}

- (void)headerViewSetting {
    
    CGFloat contentHeight = 30;
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, contentHeight)];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    UILabel *leftLabel = [self labelSetting];
    UILabel *centerLabel = [self labelSetting];
    UILabel *rightLabel = [self labelSetting];
    
//    UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kTableViewHeader_height)];
//    tempView.backgroundColor = BACKGROUND_VIEW_COLOR;
//    [headerView addSubview:tempView];
    
//    UIView *line1 = [[UIView alloc] initWithFrame:CGRectMake(0, kTableViewHeader_height, kScreenBounds.size.width, 1)];
//    line1.backgroundColor = c27;
//    [headerView addSubview:line1];
    
    UIView *line2 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(headerView.frame) - 1, kScreenBounds.size.width, 1)];
    line2.backgroundColor = c27;
    [headerView addSubview:line2];
    
    leftLabel.text = @"开奖期数";
    centerLabel.text = @"开奖号码";
    rightLabel.text = @"幸运英雄";
    
    CGSize leftSize = [leftLabel.text sizeWithCalcFont:leftLabel.font];
    CGSize centerSize = [centerLabel.text sizeWithCalcFont:centerLabel.font];
    CGSize rightSize = [rightLabel.text sizeWithCalcFont:rightLabel.font];
    
    leftLabel.frame = CGRectMake(kTableViewSectionHeader_left, (contentHeight - leftSize.height) / 2, leftSize.width, leftSize.height);
    centerLabel.frame = CGRectMake((kScreenBounds.size.width - centerSize.width) / 2, (contentHeight - centerSize.height) / 2, centerSize.width, centerSize.height);
    rightLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - rightSize.width, (contentHeight - rightSize.height) / 2, rightSize.width, rightSize.height);
    [headerView addSubview:leftLabel];
    [headerView addSubview:centerLabel];
    [headerView addSubview:rightLabel];
    
    [self.view addSubview:headerView];
}

- (UILabel *)labelSetting {
    UILabel *label = [[UILabel alloc] init];
    label.font = [[UIFont systemFontOfCustomeSize:14] boldFont];
    label.textColor = c3;
    return label;
}

- (void)pageSetting {
    // 立即竞猜
    _page = 1;
    
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 30, kScreenBounds.size.width, self.view.frame.size.height - 30 - _buttonHeight - 64 - _buttonHeight)];
    self.mainTableView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainTableView.dataSource = self;
    self.mainTableView.delegate = self;
    self.mainTableView.tableFooterView = [[UIView alloc] init];
    self.mainTableView.rowHeight = [PDLotteryDrawDetailCell cellHeight];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    self.registScrollView = self.mainTableView;
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.drawArr.count) {
            [weakSelf.drawArr removeAllObjects];
        }
        [weakSelf fetchLotteriesRecord];
    }];
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.lotteriesList.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchLotteriesRecord];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
        }
    }];
}

#pragma mark - UITableView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.drawArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *detailCellId = @"detailCellId";
    PDLotteryDrawDetailCell *detailCell = [tableView dequeueReusableCellWithIdentifier:detailCellId];
    if (!detailCell) {
        detailCell = [[PDLotteryDrawDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:detailCellId];
        detailCell.delegate = self;
    }
    detailCell.indexPath = indexPath;
    if (self.drawArr.count > indexPath.row) {
        detailCell.model = self.drawArr[indexPath.row];
    }
    if (indexPath.row % 2 == 0) {
        detailCell.backgroundColor = c33;
    } else {
        detailCell.backgroundColor = c1;
    }
    if (indexPath.row == 0) {
        self.detailCell = detailCell;
    }
    return detailCell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
}

#pragma mark - PDLotteryDrawDetailCellDelegate

- (void)lotteryCell:(PDLotteryDrawDetailCell *)cell didClickHeroImageView:(UIImageView *)heroImageView atRow:(NSInteger)row {
    // 判断tableView的偏移量，英雄弹出视图的高度为778/2 ＝ 389
//    CGFloat offsetY = self.mainTableView.contentOffset.y;
    CGRect rect = [cell convertRect:heroImageView.frame toView:[UIApplication sharedApplication].delegate.window];
    CGFloat heroHeight = 0;
    if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        heroHeight = LCFloat(300);
    } else {
        heroHeight = LCFloat(389);
    }
    
    PDLotteryHeroDetailViewShowStyle style = PDLotteryHeroDetailViewShowStyleTop;
    
    CGFloat d = heroHeight - rect.origin.y;
    if (d > 0) {
        style = PDLotteryHeroDetailViewShowStyleDown;
    }
    
    [self showHeroLotteryDetailWithHeroModel:cell.model atCurrentHeroHeader:heroImageView withFrame:rect showStyle:style];
    
//    float t = .3;
//    if (d < (heroHeight / 2) && d >= 0) {
//        [self.mainTableView setContentOffset:CGPointMake(0, offsetY - d) animated:YES];
//        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(t * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
//            CGRect rect = [cell convertRect:heroImageView.frame toView:self.view.window];
//            [self showHeroLotteryDetailWithHeroModel:cell.model atCurrentHeroHeader:heroImageView withFrame:rect showStyle:style];
//        });
//        
//    } else if (d < heroHeight && d >= (heroHeight / 2)) {
//        style = PDLotteryHeroDetailViewShowStyleDown;
//        [self showHeroLotteryDetailWithHeroModel:cell.model atCurrentHeroHeader:heroImageView withFrame:rect showStyle:style];
//    } else {
//        [self showHeroLotteryDetailWithHeroModel:cell.model atCurrentHeroHeader:heroImageView withFrame:rect showStyle:style];
//    }
}

#pragma mark - 点击头像弹出的英雄信息

- (void)showHeroLotteryDetailWithHeroModel:(PDLotteryLotteriesItem *)model atCurrentHeroHeader:(UIImageView *)heroImageView withFrame:(CGRect)rect showStyle:(PDLotteryHeroDetailViewShowStyle)style {
    
    UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:kScreenBounds];
    bgImageView.image = [[UIImage screenShoot:[UIApplication sharedApplication].delegate.window] bulrNaviBarWithBlur:2];
    bgImageView.alpha = 0;
    bgImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didClickBgScreen:)];
    [bgImageView addGestureRecognizer:tap];
    self.bgImageView = bgImageView;
    
    PDLotteryHeroDetailView *heroDetailView = [[PDLotteryHeroDetailView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y, 0, 0)];
    heroDetailView.style = style;
    heroDetailView.layer.shadowColor = [UIColor blackColor].CGColor;
    heroDetailView.layer.shadowOpacity = 0.5;
    heroDetailView.layer.shadowRadius = 3;
    heroDetailView.layer.shadowOffset = CGSizeMake(1, 1);
    heroDetailView.transform = CGAffineTransformMakeScale(0, 0);
    heroDetailView.model = model;
    [bgImageView addSubview:heroDetailView];
    self.heroDetailView = heroDetailView;
    
    UIImageView *headerImageView = [[UIImageView alloc] initWithFrame:rect];
    headerImageView.image = heroImageView.image;
    headerImageView.layer.cornerRadius = rect.size.width / 2;
    headerImageView.clipsToBounds = YES;
    [bgImageView addSubview:headerImageView];
    self.headerImageView = headerImageView;
    
    [self addToWindowWithSubView:bgImageView];
}

- (void)addToWindowWithSubView:(UIView *)subView {
    NSEnumerator *windowEnnumtor = [UIApplication sharedApplication].windows.reverseObjectEnumerator;
    for (UIWindow *window in windowEnnumtor) {
        BOOL isOnMainScreen = window.screen == [UIScreen mainScreen];
        BOOL isVisible      = !window.hidden && window.alpha > 0;
        BOOL isLevelNormal  = window.windowLevel == UIWindowLevelNormal;
        
        if (isOnMainScreen && isVisible && isLevelNormal) {
            [window addSubview:subView];
            [self showHeroDetail];
        }
    }
}

- (void)didClickBgScreen:(UITapGestureRecognizer *)tap {
    [self dismissHeroDetail];
}

- (void)showHeroDetail {
    [UIView animateWithDuration:.25 animations:^{
        self.bgImageView.alpha = 1;
        self.heroDetailView.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

- (void)dismissHeroDetail {
    [UIView animateWithDuration:.25 animations:^{
        self.bgImageView.alpha = 0;
        self.heroDetailView.transform = CGAffineTransformMakeScale(0.01, 0.01);
    } completion:^(BOOL finished) {
        [self.bgImageView removeFromSuperview];
    }];
}

#pragma mark - 更新数据

- (void)startToUpdateData:(PDLotteryHeroDetail *)heroDetail fetchComplication:(void (^)())complication {
    if (complication) {
        self.fetchComplication = complication;
    }
    
    self.heroDetail = heroDetail;
    
    if (self.mainTableView == nil) {
        return;
    }
    
    _page = 1;
    if (self.drawArr.count) {
        [self.drawArr removeAllObjects];
    }
    [self fetchLotteriesRecord];
}

/** 开奖记录*/
- (void)fetchLotteriesRecord {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDLotteryHeroUrlHelper lotteriesRecordUrlExchange] requestParams:@{@"pageNum":@(_page), @"dayTime":@(self.heroDetail.dayTime)} responseObjectClass:[PDLotteryLotteriesList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDLotteryLotteriesList *list = (PDLotteryLotteriesList *)responseObject;
            weakSelf.lotteriesList = list;
            [weakSelf.drawArr addObjectsFromArray:list.items];
            [weakSelf.mainTableView reloadData];
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            [weakSelf.mainTableView stopPullToRefresh];
            
            // 因为不延迟的话detailCell取值为空
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                if (weakSelf.fetchComplication) {
                    weakSelf.fetchComplication();
                }
            });
        }
    }];
}



@end
