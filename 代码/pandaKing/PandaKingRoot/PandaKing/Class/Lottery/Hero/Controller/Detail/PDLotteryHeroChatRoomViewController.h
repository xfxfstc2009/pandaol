//
//  PDLotteryHeroChatRoomViewController.h
//  PandaKing
//
//  Created by Cranz on 16/12/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

// 在获取聊天室ID的时候可能会因为网络原因延迟获取，这个时候，假如用户切换到竞猜室聊天，此时群聊控制器是创建不出来的，因此是个nil

#import "AbstractViewController.h"

/// 英雄猜聊天室
@interface PDLotteryHeroChatRoomViewController : UIViewController
@property (nonatomic, copy) NSString *imGroupId; // 传入的群聊id


/*
 
1、去一下消息界面就会出现聊天室不灵的问题 ！！！
 
2、重新登录也会出问题
 
 原因：在创建聊天室控制器的时候会做个判断，就是假如已经在这个聊天室中了，就会主动给你退出去，那么我在创建的时候就先推出这个聊天室
 */

//- (void)roomIdGetComplicationBackToMainThread:(void(^)())complication;

/** 外界主动通知视图出现*/
- (void)viewWillAppear;

-(void)releaseKeyboard;

@end
