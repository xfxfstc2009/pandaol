//
//  PDLotteryHeroViewController.m
//  PandaKing
//
//  Created by Cranz on 16/10/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//


/*
 1、有两个socket通知：一个是告诉我有新的一期生成，我去刷新上面的lasted和下面的记录；二是告诉我上一期开奖了（开奖有2分钟延迟），我去更新动画同时刷新下面的列表
 2、竞猜记录有两种刷新，一是自己点到那个按钮上才去刷新，线判断是否已存在数据，存在就不去刷新；二是在自己竞猜之后会出触发刷新
 3、立即竞猜后刷新竞猜详情，以及竞猜记录
 */

#import "PDLotteryHeroViewController.h"
#import "PDLotteryExplainWebViewController.h"
#import "iCarousel.h"
#import "PDLotteryTimeLabel.h"

// 今日竞猜人数
#import "PDLotteryHeroParticipationView.h"
// 投入
#import "PDLotteryHeroPoolDetailView.h"

// model
#import "PDLotteryHeroDetail.h" // 主页详情

// 投注及开奖通知视图
#import "PDPopViewManager.h"
#import "PDPopViewModel.h"
#import "PDPopMessageView.h"

// SpreadButton
#import "CCZSpreadButton.h"
#import "PDLotteryHeroHistoryViewController.h"
#import "PDCenterLotteryHeroViewController.h"

#import "CCZSegmentController.h"
// 子模块
#import "PDLotteryHeroLotteryRecordsViewController.h"
#import "PDLotteryHeroBettingRecordViewController.h"
#import "PDLotteryHeroChatRoomViewController.h"
#import "PDLotteryHeroRankListViewController.h"

// 选择投注
#import "PDLotterySelectView.h"
#import "PDTopUpViewController.h"

// 新手指引 v2
#import "CCZMaskView.h"
// 根据不同游戏转换url的路径
#import "PDLotteryHeroUrlHelper.h"

@interface PDLotteryHeroViewController ()<iCarouselDataSource, iCarouselDelegate, PDLotterySelectViewDelegate>
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UIView *titleBar; // 自定义导航栏
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, assign) BOOL showTimeCount; // 是否在标题显示倒计时

@property (nonatomic, strong) CCZSegmentController *segment;
@property (nonatomic, strong) PDLotteryHeroChatRoomViewController *chatRoomViewController;

/// 滚动区域
@property (nonatomic, strong) PDImageView *backgroundImageView; // 背景大熊猫
@property (nonatomic, strong) PDLotteryTimeLabel *timerLabel;
/**
 * 官方描述
 */
@property (nonatomic, strong) UILabel *descLabel;
/// iCarouse
@property (nonatomic, strong) iCarousel *heroCarousel;
@property (nonatomic, assign) CGFloat iCarouselHeight;
@property (nonatomic, assign) CGFloat iCarouselBottomTempHeight;

/// 悬浮按钮
@property (nonatomic, strong) CCZSpreadButton *spreadButton;

/// 详情低下的两个cell
@property (nonatomic, strong) PDLotteryHeroParticipationView *participationView; // 今日竞猜
@property (nonatomic, strong) PDLotteryHeroPoolDetailView *poolView; // 投入

/// 英雄卡片数量
@property (nonatomic, assign) int heroImages;

/// 纪录model
@property (nonatomic, strong) PDLotteryHeroDetail *heroDetail; // 本期英雄猜详情

/// 投注
@property (nonatomic, strong) PDLotterySelectView *lotterySelectedView; // 投注弹窗
@property (nonatomic, strong) PDLotteryHeroLotteryInfo *lotteryInfo; // 记录用户竞猜信息
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, assign, getter=isShowButton) BOOL showButton;
@property (nonatomic, assign) CGFloat buttonHegiht; // 投注按钮高度

/// socket相关
@property (nonatomic, strong) PDPopViewManager *popManager;

@end

@implementation PDLotteryHeroViewController

- (PDLotteryHeroLotteryInfo *)lotteryInfo {
    if (!_lotteryInfo) {
        _lotteryInfo = [[PDLotteryHeroLotteryInfo alloc] init];
    }
    return _lotteryInfo;
}

#pragma mark - initial

- (void)dealloc {
    [self.spreadButton removeFromSuperview];
    self.spreadButton = nil;
    [PDHUD dismiss];
    [self.timerLabel clearTimer];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self contentScrollViewSetting];
    [self cellsSetting];
    [self titleBarSetting];
    [self addSubModule];
    [self addBettingButton];
    [self addSpreadButton];
    [self fetchLastedDetailWithList1Update:YES list2Update:YES];
    [self addPopViewManager];
}

- (void)basicSetting {
    _iCarouselBottomTempHeight = LCFloat(42);
    _iCarouselHeight = LCFloat(565 / 2);
    _buttonHegiht = 49;
}

#pragma mark - 添加 socket 展示相关

- (void)addPopViewManager {
    PDPopViewManager *popManager = [[PDPopViewManager alloc] init];
    popManager.pause = 2;
    [popManager registViewClass:[PDPopMessageView class] inView:self.view];
    [popManager registViewModelKey:@"model"];
    self.popManager = popManager;
}

#pragma mark - 自定义导航栏

- (void)titleBarSetting {
    self.titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    self.titleBar.backgroundColor = c2;
    self.titleBar.alpha = 0;
    [self.view addSubview:self.titleBar];
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(kTableViewSectionHeader_left, 20 + (44 - 38) / 2, 31, 38)];
    [backButton setImage:[UIImage imageNamed:@"icon_page_cancle"] forState:UIControlStateNormal];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 3)];
    __weak typeof(self) weakSelf = self;
    [backButton buttonWithBlock:^(UIButton *button) {
        [weakSelf.navigationController popViewControllerAnimated:YES];
        [weakSelf dismissViewControllerAnimated:YES completion:NULL];
        [[PDEMManager sharedInstance] exitChatroomWithChatroom:weakSelf.heroDetail.imGroupId];
    }];
    [self.view addSubview:backButton];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    self.titleLabel = titleLabel;
    titleLabel.font = [[UIFont systemFontOfCustomeSize:18] boldFont];
    titleLabel.textColor = [UIColor whiteColor];
    [self changeTitleToNormal];
    [self.view addSubview:titleLabel];
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [rightButton setFrame:CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - 38, 20 + (44 - 38) / 2, 38, 38)];
    [rightButton setImage:[UIImage imageNamed:@"icon_lotteryhero_rule"] forState:UIControlStateNormal];
    [rightButton buttonWithBlock:^(UIButton *button) {
        // 规则
        PDLotteryExplainWebViewController *webViewController = [[PDLotteryExplainWebViewController alloc] init];
        [webViewController webDirectedWebUrl:[PDCenterTool absoluteUrlWithRisqueUrl:lotteryHeroLolRule]];
        PDNavigationController *navi = [[PDNavigationController alloc] initWithRootViewController:webViewController];
        navi.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [weakSelf presentViewController:navi animated:YES completion:NULL];
        
    }];
    [self.view addSubview:rightButton];
    
    [self.view bringSubviewToFront:self.titleBar];
    [self.view bringSubviewToFront:backButton];
    [self.view bringSubviewToFront:titleLabel];
    [self.view bringSubviewToFront:rightButton];
}

#pragma mark - 头部视图

- (void)contentScrollViewSetting {
    // 背景图
    self.backgroundImageView = [[PDImageView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64 + _iCarouselHeight)];
    self.backgroundImageView.image = [[UIImage imageNamed:@"icon_lotteryhero_bg"] applyDarkEffect];
    self.backgroundImageView.userInteractionEnabled = YES;
    [self.view addSubview:self.backgroundImageView];
    
    iCarousel *heroCarousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, _iCarouselHeight)];
    heroCarousel.type = iCarouselTypeRotary;
    heroCarousel.autoscroll = 0.2;
    heroCarousel.delegate = self;
    heroCarousel.dataSource = self;
    self.heroCarousel = heroCarousel;
    
    UIImageView *maskImageViewTop = [[UIImageView alloc] initWithFrame:CGRectMake(0, -64, kScreenBounds.size.width, 64)];
    maskImageViewTop.image = [UIImage imageNamed:@"icon_hero_topbg"];
    maskImageViewTop.userInteractionEnabled = YES;
    [self.heroCarousel.contentView addSubview:maskImageViewTop];
    
    UIImageView *maskImageView = [[UIImageView alloc] initWithFrame:self.heroCarousel.bounds];
    maskImageView.image = [UIImage imageNamed:@"icon_hero_downbg"];
    maskImageView.userInteractionEnabled = YES;
    [self.heroCarousel.contentView addSubview:maskImageView];
    
    //倒计时
    self.timerLabel = [[PDLotteryTimeLabel alloc] init];
    self.timerLabel.font = [UIFont systemFontOfCustomeSize:15];
    self.timerLabel.textColor = c26;
    [self.heroCarousel.contentView addSubview:self.timerLabel];
    
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.descLabel.textColor = c26;
    self.descLabel.text = @"开奖结果完全取自时时彩，100％公平公正公开。";
    [self.heroCarousel.contentView addSubview:self.descLabel];
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    self.descLabel.frame = CGRectMake((kScreenBounds.size.width - descSize.width) / 2, 10, descSize.width, descSize.height);
}

- (void)cellsSetting {
    self.participationView = [[PDLotteryHeroParticipationView alloc] initWithFrame:CGRectMake(0, _iCarouselHeight, kScreenBounds.size.width, [PDLotteryHeroParticipationView cellHeight])];
    self.poolView = [[PDLotteryHeroPoolDetailView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.participationView.frame), kScreenBounds.size.width, [PDLotteryHeroPoolDetailView cellHeight])];
    
    CGFloat height = _iCarouselHeight + [PDLotteryHeroParticipationView cellHeight] + [PDLotteryHeroPoolDetailView cellHeight];
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, height)];
    
    [self.headerView addSubview:self.heroCarousel];
    [self.headerView addSubview:self.participationView];
    [self.headerView addSubview:self.poolView];
}

#pragma mark - 三个子模块

- (void)addSubModule {
    
    // 开奖纪录
    PDLotteryHeroLotteryRecordsViewController *lotteryRecordsViewController = [[PDLotteryHeroLotteryRecordsViewController alloc] init];
    lotteryRecordsViewController.buttonHeight = _buttonHegiht;
    
    // 投注记录
    PDLotteryHeroBettingRecordViewController *bettingRecordsViewController = [[PDLotteryHeroBettingRecordViewController alloc] init];
    bettingRecordsViewController.buttonHeight = _buttonHegiht;
    
    // 竞猜排行
    PDLotteryHeroRankListViewController *rankListViewController = [[PDLotteryHeroRankListViewController alloc] init];
    rankListViewController.buttonHeight = _buttonHegiht;
    
    // 聊天室
    PDLotteryHeroChatRoomViewController *chattingRoomViewController = [[PDLotteryHeroChatRoomViewController alloc] init];
    self.chatRoomViewController = chattingRoomViewController;
    
    CCZSegmentController *segment = [[CCZSegmentController alloc] initWithFrame:CGRectMake(0, 64, kScreenBounds.size.width, kScreenBounds.size.height - 64) titles:@[@"开奖记录", @"竞猜记录", @"竞猜排行", @"竞猜讨论"]];
    segment.containerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    segment.viewControllers = @[lotteryRecordsViewController, bettingRecordsViewController, rankListViewController, chattingRoomViewController];
    [self addSegmentController:segment];
    self.segment = segment;
    // 头部视图
    segment.headerView = self.headerView;
    
    [segment.segmentView setButtonEnabledAtIndex:3 enabled:NO];
    
    __weak typeof(self) weakSelf = self;
    [segment segmentScrollToOriginComplication:^{
        weakSelf.titleBar.alpha = 0;
        
        [weakSelf changeTitleToNormal];
        
    }];
    [segment segmentScrollToTargetComplication:^{
        weakSelf.titleBar.alpha = 1;
        
        // 当滚到头部的时候需要把倒计时展示出来
        [weakSelf changeTitleToTimeLabel];
    }];
    
    // 在聊天室界面是没有按钮的
    [segment selectedAtIndex:^(NSUInteger index, UIButton * _Nonnull button, UIViewController * _Nonnull viewController) {
        
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (index == 3) {
            if (strongSelf.isShowButton == NO) {
                return;
            }
            [strongSelf.chatRoomViewController viewWillAppear];
            [strongSelf dismissButtonAnimationWithAnimated:YES];
            [viewController.segmentController scrollToTarget];
        } else {
            [strongSelf.view endEditing:YES];
            
            if (strongSelf.isShowButton == YES) {
                return;
            }
            [strongSelf showButtonAnimationWithAnimated:YES];
        }
    }];
}

#pragma mark - 标题变化

- (void)changeTitleToTimeLabel {
    self.showTimeCount = YES;
    self.titleLabel.textColor = c26;
    __weak typeof(self) weakSelf = self;
    [self.timerLabel timeLabelTextBlock:^(CGSize textSize, NSString *text) {
        if (!weakSelf.showTimeCount) {
            return;
        }
        weakSelf.titleLabel.text = [NSString stringWithFormat:@"幸运英雄出现 %@",text];
        CGSize titleSize = [weakSelf.titleLabel.text sizeWithCalcFont:weakSelf.titleLabel.font];
        weakSelf.titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, (44 - titleSize.height) / 2 + 20, titleSize.width, titleSize.height);
    }];
}

- (void)changeTitleToNormal {
    self.showTimeCount = NO;
    self.titleLabel.text = @"英雄猜";
    self.titleLabel.textColor = c1;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font];
    self.titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, (44 - titleSize.height) / 2 + 20, titleSize.width, titleSize.height);
}

#pragma mark - 添加投注按钮

- (void)addBettingButton {
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(0, kScreenBounds.size.height - _buttonHegiht, kScreenBounds.size.width, _buttonHegiht);
    moreButton.backgroundColor = c2;
    moreButton.layer.shadowColor = [UIColor blackColor].CGColor;
    moreButton.layer.shadowOpacity = 0.5;
    moreButton.layer.shadowRadius = 3;
    moreButton.layer.shadowOffset = CGSizeMake(0, -1);
    [moreButton setTitle:@"立即竞猜" forState:UIControlStateNormal];
    [moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    [PDCenterTool setButtonBackgroundEffect:moreButton];
    [self.view addSubview:moreButton];
    self.moreButton = moreButton;
    self.showButton = YES;
    [self.view bringSubviewToFront:moreButton];
}

- (void)didClickButton:(UIButton *)button {
    if (self.lotterySelectedView.isShowing) {
        return;
    }
    
    [PDHUD showHUDProgress:@"请稍后，正在为您获取本次竞猜数据..." diary:0];
    self.moreButton.enabled = NO;
    // 请求用户金币数额
    [self fetchMemberTotalGold];
}

/// 按钮有两个动画，显示和隐藏 对应属性showButton
- (void)showButtonAnimationWithAnimated:(BOOL)animated {
    [UIView animateWithDuration:animated? 0.1 : 0 animations:^{
        self.moreButton.frame = CGRectMake(0, kScreenBounds.size.height - _buttonHegiht, kScreenBounds.size.width, _buttonHegiht);
    } completion:^(BOOL finished) {
        if (finished) {
            self.showButton = YES;
        }
    }];
}

- (void)dismissButtonAnimationWithAnimated:(BOOL)animated {
    [UIView animateWithDuration:animated? 0.1 : 0 animations:^{
        self.moreButton.frame = CGRectMake(0, kScreenBounds.size.height, kScreenBounds.size.width, _buttonHegiht);
    } completion:^(BOOL finished) {
        if (finished) {
            self.showButton = NO;
        }
    }];
}

#pragma mark - iCarousel Delegate

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
       return 133;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        return 113;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        return 65;
    } else {
        return 0;
    }
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(nullable UIView *)view {
    PDImageView *heroImageView = (PDImageView *)view;
    if (!heroImageView) {
        CGFloat heroHeight = _iCarouselHeight - _iCarouselBottomTempHeight * 2;
        CGFloat heroWidth = 0;
        if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
            heroWidth = 3. / 5 * heroHeight;
        } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
            heroWidth = 3. / 3.47 * heroHeight;
        } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
            heroWidth = 3. / 5 * heroHeight;
        }
        heroImageView = [[PDImageView alloc] initWithFrame:CGRectMake(0, 0, heroWidth, heroHeight)];
    }

    [heroImageView uploadHeroLotteryWithGameType:[AccountModel sharedAccountModel].gameType imageCode:[NSString stringWithFormat:@"%li",index+1] callback:nil];
    return heroImageView;
}

#pragma mark - 添加弹出按钮
- (void)addSpreadButton {
    CGFloat buttonWidth = 60;
    CCZSpreadButton *spreadButton = [CCZSpreadButton spreadButtonWithCapacity:2];
    spreadButton.frame = CGRectMake(20, kScreenBounds.size.height * 0.8, buttonWidth, buttonWidth);
    spreadButton.normalImage = [UIImage imageNamed:@"btn_lottery_normal"];
    spreadButton.selImage = [UIImage imageNamed:@"btn_lottery_sel"];
    spreadButton.images = @[@"btn_lottery_mylottery",@"btn_lottery_history"];
    spreadButton.wannaToClickTempDismiss = YES;
    spreadButton.autoAdjustToFitSubItemsPosition = YES;
    spreadButton.spreadButtonOpenViscousity = YES;
    [self.view addSubview:spreadButton];
    [self.view bringSubviewToFront:spreadButton];
    self.spreadButton = spreadButton;
    
    __weak typeof(self) weakSelf = self;
    [spreadButton spreadButtonDidClickItemAtIndex:^(NSUInteger index) {
        if (index == 0) {
            PDCenterLotteryHeroViewController *myLotteryViewController = [[PDCenterLotteryHeroViewController alloc] init];
            [weakSelf pushViewController:myLotteryViewController animated:YES];
        }
        else if (index == 1) {
            PDLotteryHeroHistoryViewController *lotteryHistoryViewController = [[PDLotteryHeroHistoryViewController alloc] init];
            [weakSelf pushViewController:lotteryHistoryViewController animated:YES];
        }
    }];
}

#pragma mark - PDLotterySelectViewDelegate

- (void)lotterySelectView:(PDLotterySelectView *)view didSelectedAtRow1:(NSInteger)index1 row2:(NSInteger)index2 row3:(NSInteger)index3 {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        // model赋值 -> 竞猜信息q
        self.lotteryInfo.goldPrice = [NSString stringWithFormat:@"%ld",(long)index1];
        self.lotteryInfo.far = [NSString stringWithFormat:@"%ld",(long)index2];
        self.lotteryInfo.nickLength = [NSString stringWithFormat:@"%ld",(long)index3];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        self.lotteryInfo.dota2Camp = [NSString stringWithFormat:@"%ld",(long)index1];
        self.lotteryInfo.dota2Far = [NSString stringWithFormat:@"%ld",(long)index2];
        self.lotteryInfo.dota2MainProperty = [NSString stringWithFormat:@"%ld",(long)index3];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        self.lotteryInfo.pvpFar = [NSString stringWithFormat:@"%@",@(index1)];
        self.lotteryInfo.pvpNameLength = [NSString stringWithFormat:@"%@",@(index2)];
    }
    
    self.lotteryInfo.selIndex1 = index1;
    self.lotteryInfo.selIndex2 = index2;
    self.lotteryInfo.selIndex3 = index3;
    
    if (view.isSelected == YES) {
        view.buttonTitle = @"下一步";
        [self fetchGuessOddsSel1:index1 sel2:index2 sel3:index3];
    } else {
        view.buttonTitle = @"请至少选择一个选项进行竞猜";
    }
}

- (void)lotterySelectView:(PDLotterySelectView *)view didClickNextButton:(UIButton *)button {
    if (view.isSelected) { // 至少选了一个
        view.goldView.model = self.lotteryInfo;
        [view changeToInput];
    }
}

#pragma mark - 网络请求

/** 竞猜详情， 在拿到数据的时候会向下透传*/
- (void)fetchLastedDetailWithList1Update:(BOOL)u1 list2Update:(BOOL)u2 {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDLotteryHeroUrlHelper lastDetailUrlExchange] requestParams:nil responseObjectClass:[PDLotteryHeroDetail class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDLotteryHeroDetail *heroDetail = (PDLotteryHeroDetail *)responseObject;
            weakSelf.heroDetail = heroDetail;
            
            // 更新开奖纪录
            if (u1) {
                [weakSelf updateLotteryRecords];
            }
            
            if (u2) {
                // 更新投注记录
                PDLotteryHeroBettingRecordViewController *bettingViewController = weakSelf.segment.viewControllers[1];
                [bettingViewController startToUpdateData:heroDetail];
            }
            
            // 传递群聊id
            if (!weakSelf.chatRoomViewController.imGroupId) {
                weakSelf.chatRoomViewController.imGroupId = heroDetail.imGroupId;
                [weakSelf updateSegmentButtonEnbled];
            }
            
            // 更新近日竞猜
            weakSelf.participationView.model = heroDetail;
            weakSelf.poolView.model = heroDetail;
            
            // 更新倒计时
            [weakSelf updateTime];
        }
    }];
}

/// 拿到roomId后进行界面的绘制
- (void)updateSegmentButtonEnbled {
    [self.segment.segmentView setButtonEnabledAtIndex:3 enabled:YES];
}

- (void)updateLotteryRecords {
    __weak typeof(self) weakSelf = self;
    PDLotteryHeroLotteryRecordsViewController *lotteryRecordsViewController = self.segment.viewControllers.firstObject;
    [lotteryRecordsViewController startToUpdateData:self.heroDetail fetchComplication:^{
        /// 请求完成之后才处理新手指引，注意别重复弹出引导
        [weakSelf requestGuide_s1];
    }];
}

- (void)updateTime {
    __weak typeof(self) weakSelf = self;
    
    self.timerLabel.prefixStr = [NSString stringWithFormat:@"距离第%ld期幸运英雄出现还有 ",(long)self.heroDetail.draw];
    [self.timerLabel countDownWithTimeNow:self.heroDetail.currentTime timeEnd:(self.heroDetail.currentTime + self.heroDetail.leftTime) resultBlock:^(CGSize size, NSString *dateString) {
        weakSelf.timerLabel.frame = CGRectMake((kScreenBounds.size.width - size.width) / 2, weakSelf.iCarouselHeight - weakSelf.iCarouselBottomTempHeight + (weakSelf.iCarouselBottomTempHeight - size.height) / 2, size.width, size.height);
    }];
}

#pragma mark - 获取会员资产

- (void)fetchMemberTotalGold {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberWallet requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        
        [PDHUD dismiss];
        weakSelf.moreButton.enabled = YES;
        if (isSucceeded) {
            NSNumber *totalgold = responseObject[@"totalgold"]; // 总资产
            weakSelf.lotteryInfo.gold = totalgold.integerValue;
            if (totalgold.integerValue < 100) { // 去充值
#ifdef DEBUG
                [weakSelf showNotEnough];
#else
                if ([AccountModel sharedAccountModel].isShenhe) {
                    [[PDAlertView sharedAlertView] showAlertWithTitle:@"提示" conten:@"金币不足，去分享吧，骚年！分享可以获得金币。" isClose:NO btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                        [JCAlertView dismissAllCompletion:NULL];
                    }];
                } else {
                    [weakSelf showNotEnough];
                }
#endif
                
            } else {
                // 按钮缩下去
                if (weakSelf.isShowButton) {
                    [weakSelf dismissButtonAnimationWithAnimated:YES];
                }
                
                // 1、先向后台请求数据，会拿到一些数据
                PDLotterySelectView *selectedView = [[PDLotterySelectView alloc] init];
                selectedView.title = [NSString stringWithFormat:@"第%ld期幸运英雄竞猜",(long)weakSelf.heroDetail.draw];
                selectedView.buttonTitle = @"请至少选择一个选项进行竞猜";
                selectedView.delegate = weakSelf;
                weakSelf.lotterySelectedView = selectedView;
                [weakSelf submitToInput];
                [weakSelf dismissSelectedViewWithMoreButtonChangtoNormal];
                
                // 第二个新手指导
                [weakSelf requestGuide_s2];
            }
        }
    }];
}

- (void)dismissSelectedViewWithMoreButtonChangtoNormal {
    __weak typeof(self) weakSelf = self;
    [self.lotterySelectedView dismissWithActionBlock:^{
        if (weakSelf.isShowButton == NO) {
            [weakSelf showButtonAnimationWithAnimated:YES];
        }
    }];
}

/** 获取赔率*/
- (void)fetchGuessOddsSel1:(NSInteger)sel1 sel2:(NSInteger)sel2 sel3:(NSInteger)sel3 {
    NSDictionary *param;
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        param = @{@"goldPrice":@(sel1),@"far":@(sel2),@"nickLength":@(sel3)};
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        param = @{@"camp":@(sel1),@"far":@(sel2),@"mainProperty":@(sel3)};
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        param = @{@"far":@(sel1),@"nameLength":@(sel2)};
    }
    
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDLotteryHeroUrlHelper getOddsUrlExchange] requestParams:param responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.lotteryInfo.odds = responseObject[@"odds"];
            weakSelf.lotterySelectedView.buttonTitle = [NSString stringWithFormat:@"下一步(赔率%@)",responseObject[@"odds"]];
        }
    }];
}

/** 确认竞猜*/
- (void)fetchInputWithLotteryInfo:(PDLotteryHeroLotteryInfo *)lotteryInfo {
    __weak typeof(self) weakSelf = self;
    NSDictionary *param;
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        param = @{@"goldPrice":@(lotteryInfo.selIndex1),@"far":@(lotteryInfo.selIndex2),@"nickLength":@(lotteryInfo.selIndex3),@"gold":@(lotteryInfo.input)};
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        param = @{@"camp":@(lotteryInfo.selIndex1),@"far":@(lotteryInfo.selIndex2),@"mainProperty":@(lotteryInfo.selIndex3),@"gold":@(lotteryInfo.input)} ;
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        param = @{@"far":@(lotteryInfo.selIndex1),@"nameLength":@(lotteryInfo.selIndex2),@"gold":@(lotteryInfo.input)};
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:[PDLotteryHeroUrlHelper inputUrlExchange] requestParams:param responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        [PDHUD dismiss];
        if (isSucceeded) {
            
            // 竞猜成功提示
            [weakSelf successToBet];
            
            // 刷新最新竞猜详情
            [weakSelf fetchLastedDetailWithList1Update:NO list2Update:YES];
            
            // 自己投注的时候才去更新排行榜
            PDLotteryHeroRankListViewController *rankListViewControler = weakSelf.segment.viewControllers[2];
            [rankListViewControler updateRankList];
        }
    }];
}

/// 确认竞猜
#pragma mark - 确认竞猜

- (void)submitToInput {
    __weak typeof(self) weakSelf = self;
    [self.lotterySelectedView.goldView submitWithGoldCount:^(NSInteger count) {
        weakSelf.lotteryInfo.input = count;
        [PDHUD showHUDProgress:@"正在获取投注信息，请稍后..." diary:0];
        [weakSelf fetchInputWithLotteryInfo:weakSelf.lotteryInfo];
        [weakSelf.lotterySelectedView dismissGoldView];
    }];
}

#pragma mark - 提示弹框
/** 成功竞猜*/
- (void)successToBet {
    if ([AccountModel sharedAccountModel].gameType == GameTypeLoL) {
        [[PDAlertView sharedAlertView] showAlertWithTitle:@"竞猜成功" conten:[NSString stringWithFormat:@"您已成功竞猜LOL英雄猜第%ld期(%@-%@-%@)%ld金币，祝您好运。",(long)self.heroDetail.draw,self.lotteryInfo.goldPrice,self.lotteryInfo.far,self.lotteryInfo.nickLength, (long)self.lotteryInfo.input] isClose:NO btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
            [JCAlertView dismissAllCompletion:NULL];
        }];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypeDota2) {
        [[PDAlertView sharedAlertView] showAlertWithTitle:@"竞猜成功" conten:[NSString stringWithFormat:@"您已成功竞猜Dota2英雄猜第%ld期(%@-%@-%@)%ld金币，祝您好运。",(long)self.heroDetail.draw,self.lotteryInfo.dota2Camp,self.lotteryInfo.dota2Far,self.lotteryInfo.dota2MainProperty, (long)self.lotteryInfo.input] isClose:NO btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
            [JCAlertView dismissAllCompletion:NULL];
        }];
    } else if ([AccountModel sharedAccountModel].gameType == GameTypePVP) {
        [[PDAlertView sharedAlertView] showAlertWithTitle:@"竞猜成功" conten:[NSString stringWithFormat:@"您已成功竞猜王者荣耀英雄猜第%ld期(%@-%@)%ld金币，祝您好运。",(long)self.heroDetail.draw,self.lotteryInfo.pvpFar,self.lotteryInfo.pvpNameLength, (long)self.lotteryInfo.input] isClose:NO btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
            [JCAlertView dismissAllCompletion:NULL];
        }];
    }
}

/** 竞猜失败*/
- (void)faiToBet {
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"竞猜失败" conten:@"LOL英雄猜竞猜暂停啦，请您稍等一会儿哦..." isClose:NO btnArr:@[@"我知道了"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissAllCompletion:NULL];
    }];
}

/** 余额不足*/
- (void)showNotEnough {
    __weak typeof(self) weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"余额不足" conten:@"最低竞猜金额为100金币" isClose:NO btnArr:@[@"去充值", @"取消"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissWithCompletion:NULL];
        if (buttonIndex == 0) {
            PDTopUpViewController *topUpViewController = [[PDTopUpViewController alloc] init];
            [weakSelf pushViewController:topUpViewController animated:YES];
        }
    }];
}

- (void)showFinishGuide {
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"完成教学" conten:@"恭喜您完成英雄猜教学，马上竞猜试试吧！" isClose:NO btnArr:@[@"立即竞猜"] buttonClick:^(NSInteger buttonIndex) {
        [JCAlertView dismissAllCompletion:NULL];
    }];
}

#pragma mark - Socket delegate

- (void)socketDidBackData:(id)responseObject {
    [super socketDidBackData:responseObject];
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *resp = (NSDictionary *)responseObject;
        NSDictionary *data = resp[@"data"];
        NSNumber *type = data[@"type"];
        int typeInt = [type intValue];
        if (typeInt == 805 || typeInt == 825 || typeInt == 845) { // 投入金币
            PDPopViewModel *model = [[PDPopViewModel alloc] init];
            model.avatar = data[@"avatar"];
            model.nickname = data[@"nickname"];
            model.gold = data[@"gold"];
            [self.popManager popViewWithModel:model];
        } else if (typeInt == 807 || typeInt == 827 || typeInt == 847) { // 开奖
            NSArray *winNumsArr = data[@"winNums"];
            NSNumber *lastNum = winNumsArr.lastObject;
            [self startToHeroShowAnimationAtIndex:lastNum.intValue];
        } else if (typeInt == 806 || typeInt == 826 || typeInt == 846) { // 有新一期
            // 拉取最新一期数据
            [self fetchLastedDetailWithList1Update:NO list2Update:YES];
        }
    }
}

#pragma mark - 竞猜出结果 -> 英雄飞出动画

- (void)startToHeroShowAnimationAtIndex:(int)index {
    // 加速转动2s后，减速
    self.heroCarousel.autoscroll = 5;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.heroCarousel.autoscroll = 0.2;
        [self heroImageViewMoveAnimationAtIndex:index];
    });
}

- (void)heroImageViewMoveAnimationAtIndex:(int)index {
    iCarousel *ic;
    for (UIView *subView in self.segment.headerView.subviews) {
        if ([subView isKindOfClass:[iCarousel class]]) {
            ic = (iCarousel *)subView;
            break;
        }
    }
    CGRect icRect = [self.headerView convertRect:ic.frame toView:self.view];
    
    PDImageView *heroImageView = [[PDImageView alloc] initWithFrame:CGRectMake((kScreenBounds.size.width - self.heroCarousel.itemWidth) / 2, (_iCarouselHeight - CGRectGetHeight(self.heroCarousel.currentItemView.frame)) / 2 + icRect.origin.y, self.heroCarousel.itemWidth, CGRectGetHeight(self.heroCarousel.currentItemView.frame))];
    [heroImageView uploadHeroLotteryWithGameType:[AccountModel sharedAccountModel].gameType imageCode:[NSString stringWithFormat:@"%d",index] callback:nil];
    [self.view addSubview:heroImageView];
    
    PDLotteryHeroLotteryRecordsViewController *lotterRecordsViewController = self.segment.viewControllers.firstObject;
    
    [UIView animateWithDuration:1.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        heroImageView.transform = CGAffineTransformMakeScale(1.3, 1.3);
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:1.5 delay:0.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
            CGRect headerRect = [lotterRecordsViewController.detailCell convertRect:lotterRecordsViewController.detailCell.headerImageView.frame toView:self.view];
            heroImageView.frame = headerRect;
            
            heroImageView.transform = CGAffineTransformMakeScale(headerRect.size.width / heroImageView.frame.size.width, headerRect.size.height / heroImageView.frame.size.height);
        } completion:^(BOOL finished) {
            [heroImageView removeFromSuperview];
            [lotterRecordsViewController startToUpdateData:self.heroDetail fetchComplication:NULL];
        }];
    }];
}

#pragma mark - 新手指引

- (void)startGuideshow:(void(^)())block {
    CCZMaskView *maskView = [[CCZMaskView alloc] init];
    maskView.shadowAvailable = YES;
    
    // 步骤
    // 0.狂英雄
    CCZMaskStep *step1 = [[CCZMaskStep alloc] init];
    CGRect icRect = [self.headerView convertRect:self.heroCarousel.frame toView:self.view];
    step1.rect = CGRectMake((kScreenBounds.size.width - self.heroCarousel.itemWidth) / 2, (_iCarouselHeight - CGRectGetHeight(self.heroCarousel.currentItemView.frame)) / 2 + icRect.origin.y, self.heroCarousel.itemWidth, CGRectGetHeight(self.heroCarousel.currentItemView.frame));
    step1.radius = 30;
    step1.text = @"“系统每隔一段时间会根据时时彩的开奖情况计算出一位幸运英雄，玩家猜中幸运英雄的各个属性即可获得奖励，可组合竞猜”";
    
    // 1.悬浮按钮
    CCZMaskStep *step2 = [[CCZMaskStep alloc] init];
    step2.rect = self.spreadButton.frame;
    step2.radius = CGRectGetWidth(self.spreadButton.frame) / 2;
    step2.text = @"“点击这里可以查看历史分析和我的竞猜”";
    
    // 2.查看英雄属性
    CCZMaskStep *step3 = [[CCZMaskStep alloc] init];
    PDLotteryHeroLotteryRecordsViewController *lotterRecordsViewController = self.segment.viewControllers.firstObject;
    CGRect headerRect = [lotterRecordsViewController.detailCell convertRect:lotterRecordsViewController.detailCell.headerImageView.frame toView:self.view];
    step3.rect = headerRect;
    step3.radius = headerRect.size.width / 2;
    step3.text = @"“点击这里可以查看英雄属性哦”";
    
    // 3.点击按钮
    CCZMaskStep *step4 = [[CCZMaskStep alloc] init];
    step4.rect = self.moreButton.frame;
    step4.radius = self.moreButton.frame.size.height / 2;
    step4.text = @"“点击投注试试吧”";
    
    
    [maskView addSteps:@[step1, step2, step3, step4]];
    [maskView showMask];
    
    __weak typeof(self) weakSelf = self;
    [maskView nextComplicationHandle:^(NSUInteger index, BOOL isFinished, __kindof UIGestureRecognizer * _Nonnull gestureRecognizer) {
        if (index == 3) {
            
            // 提示点击按钮
            UIImage *fingerImage = [UIImage imageNamed:@"tap_gesture"];
            UIImageView *fingerImageView = [[UIImageView alloc] initWithImage:fingerImage];
            fingerImageView.frame = CGRectMake(weakSelf.moreButton.center.x + LCFloat(30), weakSelf.moreButton.center.y - 10, fingerImage.size.width, fingerImage.size.height);
            
            // 为小手指添加动画
            [UIView animateWithDuration:0.55 delay:0.3 options:UIViewAnimationOptionRepeat | UIViewKeyframeAnimationOptionAutoreverse animations:^{
                fingerImageView.frame = CGRectOffset(fingerImageView.frame, 0, -15);
            } completion:NULL];
            [maskView addSubGuideView:fingerImageView];
        }
        
        if (isFinished) {
            [weakSelf didClickButton:weakSelf.moreButton];
        }
    } dismissComplicationHandle:block];
}

- (void)startGuideshow2:(void(^)())block {
    CCZMaskView *maskView = [[CCZMaskView alloc] init];
    maskView.shadowAvailable = YES;
    
    // 0.弹起选择框
    CCZMaskStep *step1 = [[CCZMaskStep alloc] init];
    step1.rect = CGRectMake(self.lotterySelectedView.contentBar.frame.origin.x, self.lotterySelectedView.contentBar.frame.origin.y, kScreenBounds.size.width, kScreenBounds.size.height - self.lotterySelectedView.contentBar.frame.origin.y);
    step1.radius = 80;
    step1.text = @"“投注这期的英雄属性吧”";
    
    
    // 1.点击选项1
    
    
    // 1.点击选项2
    CCZMaskStep *step2 = [[CCZMaskStep alloc] init];
    step2.rect = CGRectMake(0, CGRectGetHeight(self.lotterySelectedView.frame) - 49, CGRectGetWidth(self.lotterySelectedView.frame), 49);
    step2.radius = step2.rect.size.height / 2;
    step2.text = @"“点击下一步投注金币吧”";
    
    // 2.下一步
    CCZMaskStep *step3 = [[CCZMaskStep alloc] init];
    step3.rect = CGRectMake(self.lotterySelectedView.contentBar.frame.origin.x, self.lotterySelectedView.contentBar.frame.origin.y, kScreenBounds.size.width, kScreenBounds.size.height - self.lotterySelectedView.contentBar.frame.origin.y);
    step3.radius = 80;
    step3.type = CCZMaskStepGestureRecognizerTypePan;
    step3.text = @"“点击向右滑动到底进行投注吧”";
    
    // 3.确定
    CCZMaskStep *step4 = [[CCZMaskStep alloc] init];
    step4.rect = CGRectMake(0, CGRectGetHeight(self.lotterySelectedView.frame) - 49, CGRectGetWidth(self.lotterySelectedView.frame), 49);
    step4.radius = step4.rect.size.height / 2;
    step4.text = @"“点击确定吧”";
    
    [maskView addSteps:@[step1, step2, step3, step4]];
    [maskView showMask];
    
    __weak typeof(self) weakSelf = self;
    [maskView nextComplicationHandle:^(NSUInteger index, BOOL isFinished, __kindof UIGestureRecognizer * _Nonnull gestureRecognizer) {
        // 指导点击按钮
        if (index == 0) {
            // 提示点击按钮
            UIImage *fingerImage = [UIImage imageNamed:@"tap_gesture"];
            UIImageView *fingerImageView = [[UIImageView alloc] initWithImage:fingerImage];
            PDLotterySelectedButton *lotteryButton = [weakSelf.lotterySelectedView buttonAtRow:1 index:2];
            CGRect buttonRect = [lotteryButton.superview convertRect:lotteryButton.frame toView:weakSelf.view];
            
            fingerImageView.frame = CGRectMake(buttonRect.origin.x + buttonRect.size.width / 2, buttonRect.origin.y + buttonRect.size.height, fingerImage.size.width, fingerImage.size.height);
            
            // 为小手指添加动画
            [UIView animateWithDuration:0.55 delay:0.3 options:UIViewAnimationOptionRepeat | UIViewKeyframeAnimationOptionAutoreverse animations:^{
                fingerImageView.frame = CGRectOffset(fingerImageView.frame, 0, -15);
            } completion:NULL];
            [maskView addSubGuideView:fingerImageView];
        } else if (index == 1) {
            [weakSelf.lotterySelectedView setSelectedAtRow:1 index:2];
        } else if (index == 2) {
            weakSelf.lotterySelectedView.goldView.model = weakSelf.lotteryInfo;
            
            [weakSelf.lotterySelectedView changeToInput];
            
            // 提示点击按钮
            UIImage *fingerImage = [UIImage imageNamed:@"tap_gesture"];
            UIImageView *fingerImageView = [[UIImageView alloc] initWithImage:fingerImage];
            CCZSlider *slider = weakSelf.lotterySelectedView.goldView.slider;
            CGRect rect = [slider.superview convertRect:slider.frame toView:weakSelf.view];
            fingerImageView.frame = CGRectMake(rect.origin.x + fingerImage.size.width / 2, rect.origin.y, fingerImage.size.width, fingerImage.size.height);
            
            // 为小手指添加动画
            [UIView animateWithDuration:0.55 delay:0.3 options:UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse animations:^{
                fingerImageView.frame = CGRectOffset(fingerImageView.frame, 15, 0);
            } completion:NULL];
            [maskView addSubGuideView:fingerImageView];
        } else if (index == 3) {
        
        }
        
        if (isFinished) {
            [weakSelf.lotterySelectedView dismissGoldView];
        }
    } dismissComplicationHandle:^{
        [weakSelf showFinishGuide];
        block();
    }];
    
    [maskView valueForGestureRecognizer:^(NSUInteger index,NSValue * _Nonnull grv) {
        if (index == 3) {
            CGFloat x = grv.CGPointValue.x;
            NSUInteger i = x >= 0? x : 0;
            weakSelf.lotterySelectedView.goldView.slider.maximumValue = 100;
            weakSelf.lotterySelectedView.goldView.falseGold = i;
        }
    }];
}

- (void)requestGuide_s1 {
    if (![Tool userDefaultGetWithKey:championGuess1].length) {
        [self startGuideshow:^{
            [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeChampionGuess1 block:nil];
        }];
    }
}

- (void)requestGuide_s2 {
    if (![Tool userDefaultGetWithKey:championGuess2].length) {
        [self startGuideshow2:^{
            [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeChampionGuess2 block:nil];
        }];
    }
}
@end
