//
//  PDShopRootSubViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDShopRootSubViewController : AbstractViewController

@property (nonatomic,assign)CGFloat transferHeight;

@end
