//
//  PDPayResultViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPayResultViewController.h"
#import "PDSnatchNoteViewController.h"
#import "PDPayResultTableViewCell.h"
#import "PDShareSheetView.h"
#import "PDOAuthLoginAndShareTool.h"

@interface PDPayResultViewController ()<UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) UIView *headerView;
@property (nonatomic, strong) UITableView *mainTabelView;
@property (nonatomic, strong) UIButton *moreButton;

// 分享的内容
@property (nonatomic, copy) NSString *shareTitle;
@property (nonatomic, copy) NSString *url;
@property (nonatomic, copy) NSString *content;

@end

@implementation PDPayResultViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchShareInfo];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.navigationController.navigationBar.layer.shadowColor = [UIColor clearColor].CGColor;
}

- (void)basicSetting {
    self.barMainTitle = @"支付结果";
    
    __weak typeof(self) weakSelf = self;
    [self rightBarButtonWithTitle:@"夺宝记录" barNorImage:nil barHltImage:nil action:^{
        PDSnatchNoteViewController *noteViewController = [[PDSnatchNoteViewController alloc] init];
        [weakSelf pushViewController:noteViewController animated:YES];
    }];
    
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_center_back"] barHltImage:[UIImage imageNamed:@"icon_center_back"] action:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)pageSetting {
    [self headerViewSetting];
    [self tableViewSetting];
    [self buttonSetting];
}

- (void)buttonSetting {
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = CGRectMake(20, kScreenBounds.size.height - 64 - 44 - 44, kScreenBounds.size.width - 20 * 2, 44);
    moreButton.layer.cornerRadius = 4;
    moreButton.clipsToBounds = YES;
    moreButton.titleLabel.font = [UIFont systemFontOfCustomeSize:16];
    if (self.canHelp == YES) {
        moreButton.enabled = NO;
        [moreButton setTitle:@"邀请好友帮我抢（帮抢越多中奖率越高）" forState:UIControlStateNormal];
    } else {
        [moreButton setTitle:@"继续夺宝" forState:UIControlStateNormal];
    }
    
    [moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    moreButton.backgroundColor = [UIColor blackColor];
    [moreButton addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:moreButton];
    self.moreButton = moreButton;
}

- (void)headerViewSetting {
    self.headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(140))];
    self.headerView.backgroundColor = c2;
    [self.view addSubview:self.headerView];
    
    CGFloat imageWidth = LCFloat(50);
    CGFloat space = 6;
    UIImageView *successImageView = [[UIImageView alloc] initWithFrame:CGRectMake((kScreenBounds.size.width - imageWidth) / 2, LCFloat(25), imageWidth, imageWidth)];
    successImageView.image = [UIImage imageNamed:@"icon_snatch_success"];
    [self.headerView addSubview:successImageView];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"参与夺宝成功";
    titleLabel.textColor = c26;
    titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, CGRectGetMaxY(successImageView.frame) + space, titleSize.width, titleSize.height);
    [self.headerView addSubview:titleLabel];
    
    UILabel *subTitleLabel = [[UILabel alloc] init];
    subTitleLabel.text = @"请等待系统为您揭晓";
    subTitleLabel.textColor = c6;
    subTitleLabel.font = [UIFont systemFontOfCustomeSize:12];
    CGSize subSize = [subTitleLabel.text sizeWithCalcFont:subTitleLabel.font];
    subTitleLabel.frame = CGRectMake((kScreenBounds.size.width - subSize.width) / 2, CGRectGetMaxY(titleLabel.frame) + space, subSize.width, subSize.height);
    [self.headerView addSubview:subTitleLabel];
}

- (void)tableViewSetting {
    self.mainTabelView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.headerView.frame), kScreenBounds.size.width, self.view.bounds.size.height - CGRectGetHeight(self.headerView.frame) - 64 - 88 - 10) style:UITableViewStyleGrouped];
    self.mainTabelView.backgroundColor = [UIColor clearColor];
    self.mainTabelView.delegate = self;
    self.mainTabelView.dataSource = self;
    self.mainTabelView.tableFooterView = [UIView new];
    self.mainTabelView.separatorColor = c27;
    [self.view addSubview:self.mainTabelView];
}

#pragma mark - 控件方法

- (void)didClickButton {
    if (self.canHelp == NO) {
        [self.navigationController popViewControllerAnimated:YES];
        return;
    }
    
    // 弹出分享
    __weak typeof(self) weakSelf = self;
    [PDShareSheetView shareWithItemArr:@[@"微信好友", @"朋友圈"] imageArr:@[@"icon_share_wx", @"icon_share_wxzone"] clickComplication:^(NSString *item) {
        if ([item isEqualToString:@"微信好友"]) {
            if ([WXApi isWXAppInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeWXSession];
            } else {
                [PDHUD showHUDError:@"您未安装微信手机客户端！"];
            }
        } else if ([item isEqualToString:@"朋友圈"]) {
            if ([WXApi isWXAppInstalled]) {
                [weakSelf shareWithType:PD3RShareTypeWXTimeLine];
            } else {
                [PDHUD showHUDError:@"您未安装微信手机客户端！"];
            }
        }
    }];
}

- (void)shareWithType:(PD3RShareType)type {
    PDImageView *downloadImageView = [PDImageView new];
    
    __weak typeof(self) weakSelf = self;
    [downloadImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithRisqueUrl:self.stake.commodityImage] placeholder:nil callback:^(UIImage *image) {
        NSData *imgData = UIImageJPEGRepresentation(image, 1);
        if (imgData.length >= 32 * 1024) { // 判断字节数
            imgData = [PDCenterTool compressImage:image];
        }
        
        [PDOAuthLoginAndShareTool sharedWithType:type text:nil title:weakSelf.shareTitle desc:weakSelf.content thubImageData:imgData webUrl:weakSelf.url];
    }];
    
}

#pragma mark - UITabelView delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"normalCellId"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"normalCellId"];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont systemFontOfCustomeSize:13];

        // 成功参与 (左侧)
        NSMutableAttributedString *textAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"成功参与1件商品"]];
        [textAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(4, 1)];
        cell.textLabel.attributedText = [textAtt copy];
        
        NSString *portions = [NSString stringWithFormat:@"%ld",self.stake.portion];
        NSString *str = [NSString stringWithFormat:@"x%@份(%ld竹子)",portions, self.stake.wager];
        NSMutableAttributedString *attribute = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName: [UIFont systemFontOfCustomeSize:13]}];
        [attribute addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(1, portions.length)];
        cell.detailTextLabel.attributedText = attribute;
        return cell;
    } else {
        NSString *payCellId = @"payCellId";
        PDPayResultTableViewCell *resultCell = [tableView dequeueReusableCellWithIdentifier:payCellId];
        if (!resultCell) {
            resultCell = [[PDPayResultTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:payCellId];
        }
        resultCell.model = self.stake;
        return resultCell;
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [PDCenterTool calculateCell:cell leftSpace:0 rightSpace:0];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        return 35;
    } else {
        return [PDPayResultTableViewCell cellHeightWithModel:self.stake];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

#pragma mark - 网络请求

- (void)fetchShareInfo {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchShareHelp requestParams:@{@"lotteryId":self.stake.ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.shareTitle = responseObject[@"title"];
            weakSelf.content = responseObject[@"content"];
            weakSelf.url = responseObject[@"url"];
            
            weakSelf.moreButton.enabled = YES;
        }
    }];
}

@end
