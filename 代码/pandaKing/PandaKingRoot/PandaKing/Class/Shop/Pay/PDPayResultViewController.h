//
//  PDPayResultViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDSnatchStakeModel.h"

/// 支付结果控制器
@interface PDPayResultViewController : AbstractViewController
@property (nonatomic, strong) PDSnatchStakeModel *stake;
/**
 * 能否分享
 */
@property (nonatomic) BOOL canHelp;
@end
