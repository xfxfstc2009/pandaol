//
//  PDShopRootMainHeaderView.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDShopRootMainHeaderView.h"
#import "PDShopRootMainHeaderSingleView.h"
#import "PDShopRootMemassetRootSingleModel.h"


static char actionClickWithLeftKey;
static char actionClickWithRightKey;
@interface PDShopRootMainHeaderView()
@property (nonatomic,strong)PDShopRootMainHeaderSingleView *leftView;
@property (nonatomic,strong)PDShopRootMainHeaderSingleView *rightView;

@end

@implementation PDShopRootMainHeaderView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
        [self sendRequestTogetMemasset];
    }
    return self;
}

#pragma mark-createView
-(void)createView{
    self.leftView = [[PDShopRootMainHeaderSingleView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width / 2., [PDShopRootMainHeaderSingleView calculationCellHeight])];
    self.leftView.transferIcon = [UIImage imageNamed:@"icon_shop_root_bamboo"];
    self.leftView.transferTitle = @"我的竹子";
    self.leftView.transferDesc = @"0";
    [self addSubview:self.leftView];
    
    UIButton *btn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn1.backgroundColor = [UIColor clearColor];
    btn1.frame = self.leftView.bounds;
    [self.leftView addSubview:btn1];
    __weak typeof(self)weakSelf = self;
    [btn1 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithLeftKey);
        if (block){
            block();
        }
    }];
    
    
    
    self.rightView = [[PDShopRootMainHeaderSingleView alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.leftView.frame) , 0, kScreenBounds.size.width / 2., [PDShopRootMainHeaderSingleView calculationCellHeight])];
    self.rightView.transferIcon = [UIImage imageNamed:@"icon_shop_root_beibao"];
    self.rightView.transferTitle = @"我的背包";
    self.rightView.transferDesc = @"可使用卡券0张";
    [self addSubview:self.rightView];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    btn2.backgroundColor = [UIColor clearColor];
    btn2.frame = self.rightView.bounds;
    [self.rightView addSubview:btn2];
    [btn2 buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithRightKey);
        if (block){
            block();
        }
    }];
}

+(CGFloat)calculationCellHeight{
    return [PDShopRootMainHeaderSingleView calculationCellHeight];
}


-(void)sendRequestTogetMemasset{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:entertainmentMemasset requestParams:nil responseObjectClass:[PDShopRootMemassetRootSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDShopRootMemassetRootSingleModel *infoModel = (PDShopRootMemassetRootSingleModel *)responseObject;
            // 我的竹子
            strongSelf.leftView.transferDesc = [NSString stringWithFormat:@"%li",infoModel.totalBamboo];
            // 我的卡券
            strongSelf.rightView.transferDesc = [NSString stringWithFormat:@"可使用卡券%li张",infoModel.cerNotUsedCount];
        }
    }];
}

-(void)actionClickWithLeft:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithLeftKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
-(void)actionClickWithRight:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithRightKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
