//
//  PDShopRootMemassetRootSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDShopRootMemassetRootSingleModel <NSObject>

@end

@interface PDShopRootMemassetRootSingleModel : FetchModel

@property (nonatomic,assign)NSInteger totalBamboo;
@property (nonatomic,assign)NSInteger totalGold;
@property (nonatomic,assign)NSInteger cerNotUsedCount;
@end
