//
//  PDShopRootMainHeaderView.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDShopRootMainHeaderView : UIView

+(CGFloat)calculationCellHeight;

-(void)actionClickWithLeft:(void(^)())block;
-(void)actionClickWithRight:(void(^)())block;
@end
