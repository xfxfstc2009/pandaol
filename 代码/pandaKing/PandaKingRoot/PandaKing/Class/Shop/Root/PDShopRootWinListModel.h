//
//  PDShopRootWinListModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDShopRootWinSingleModel.h"
@interface PDShopRootWinListModel : FetchModel

@property (nonatomic,strong)NSArray<PDShopRootWinSingleModel> *items;

@end
