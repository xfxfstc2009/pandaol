//
//  PDShopRootWinSingleModel.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/22.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDShopRootWinSingleModel <NSObject>

@end

@interface PDShopRootWinSingleModel : FetchModel

@property(nonatomic,copy)NSString *treasure;
@property(nonatomic,copy)NSString *winner;

@end
