//
//  PDProductDetailWebViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDWebViewController.h"

/// 商品图文详情
@interface PDProductDetailWebViewController : PDWebViewController

@end
