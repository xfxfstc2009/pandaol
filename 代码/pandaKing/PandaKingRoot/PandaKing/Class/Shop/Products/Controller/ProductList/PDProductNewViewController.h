//
//  PDProductNewViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 最新揭晓控制器>>>>>>>>>>> 弃用
@protocol PDProductNewViewControllerDelegate;
@interface PDProductNewViewController : AbstractViewController
@property (nonatomic, strong) UICollectionView *mainCollectionView;
@property (nonatomic, weak) id<PDProductNewViewControllerDelegate> delegate;
@end

@protocol PDProductNewViewControllerDelegate <NSObject>
@optional
- (void)newViewController:(PDProductNewViewController *)viewController didScrollWithOffset:(CGFloat)offsetY;

@end