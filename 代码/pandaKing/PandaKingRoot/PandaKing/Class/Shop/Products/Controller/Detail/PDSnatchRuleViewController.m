//
//  PDSnatchRuleViewController.m
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSnatchRuleViewController.h"

@interface PDSnatchRuleViewController ()

@end

@implementation PDSnatchRuleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)pageSetting {
    self.barMainTitle = @"夺宝规则";
    
    __weak typeof(self) weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_page_cancle"] barHltImage:[UIImage imageNamed:@"icon_page_cancle"] action:^{
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
}

@end
