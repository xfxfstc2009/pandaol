//
//  PDProductAllViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProductNewViewController.h"
#import "PDProductCollectionViewCell.h"
#import "PDProductDetailViewController.h"
#import "PDSnatchProductList.h"

@interface PDProductNewViewController ()<UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, strong) PDSnatchProductList *list;
@property (nonatomic, weak) NSTimer *timer;
@end

@implementation PDProductNewViewController

- (void)dealloc {
    [_timer invalidate];
    _timer = nil;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    _page = 1;
    _itemsArr = [NSMutableArray array];
    _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(didChangeTime) userInfo:nil repeats:YES];
}

- (void)pageSetting {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.itemSize = [PDProductCollectionViewCell cellSize];
    layout.sectionInset = UIEdgeInsetsMake([PDProductCollectionViewCell space], [PDProductCollectionViewCell space], [PDProductCollectionViewCell space], [PDProductCollectionViewCell space]);
    self.mainCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - 44) collectionViewLayout:layout];
    self.mainCollectionView.backgroundColor = [UIColor clearColor];
    self.mainCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainCollectionView.alwaysBounceVertical = YES;
//    self.mainCollectionView.scrollEnabled = NO;
//    self.mainCollectionView.bounces = NO;
    self.mainCollectionView.delegate = self;
    self.mainCollectionView.dataSource = self;
    [self.mainCollectionView registerClass:[PDProductCollectionViewCell class] forCellWithReuseIdentifier:@"productCellId"];
    [self.view addSubview:self.mainCollectionView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainCollectionView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.itemsArr.count) {
            [weakSelf.itemsArr removeAllObjects];
        }
        [weakSelf fetchData];
    }];
    
    [self.mainCollectionView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.list.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchData];
        } else {
            [weakSelf.mainCollectionView stopFinishScrollingRefresh];
            return ;
        }
    }];
}

#pragma mark -
#pragma mark -- UICollectionViewDelegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.itemsArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PDProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"productCellId" forIndexPath:indexPath];
    if (self.itemsArr.count) {
        cell.model = self.itemsArr[indexPath.row];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemsArr.count) {
        PDProductDetailViewController *detailViewController = [[PDProductDetailViewController alloc] init];
        PDSnatchProductItem *item = self.itemsArr[indexPath.row];
        detailViewController.productId = item.ID;
        [self pushViewController:detailViewController animated:YES];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if ([self.delegate respondsToSelector:@selector(newViewController:didScrollWithOffset:)]) {
        [self.delegate newViewController:self didScrollWithOffset:scrollView.contentOffset.y];
    }
}

#pragma mark -
#pragma mark -- 网络请求
/** 获取全部商品*/
- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchNewList requestParams:@{@"pageNum":@(_page)} responseObjectClass:[PDSnatchProductList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDSnatchProductList *list = (PDSnatchProductList *)responseObject;
            weakSelf.list = list;
            [weakSelf.itemsArr addObjectsFromArray:list.items];
            [weakSelf.mainCollectionView reloadData];
            if (weakSelf.itemsArr.count == 0) {
                weakSelf.mainCollectionView.scrollEnabled = NO;
                [weakSelf showNoDataPage];
            } else {
                weakSelf.mainCollectionView.scrollEnabled = YES;
                [weakSelf.mainCollectionView dismissPrompt];
            }
        }
        
        [weakSelf.mainCollectionView stopFinishScrollingRefresh];
        [weakSelf.mainCollectionView stopPullToRefresh];
    }];
}

#pragma mark -- 显示无数据页

- (void)showNoDataPage {
    __weak typeof(self) weakSelf = self;
    [self.mainCollectionView showPrompt:@"小的们正在紧张的准备夺宝活动0...0~ !!!" withImage:[UIImage imageNamed:@"icon_nodata_panda"] buttonTitle:@"点击刷新" clickBlock:^{
        [weakSelf fetchData];
    }];
}

#pragma mark -- 全局定时

- (void)didChangeTime {
    if (self.itemsArr.count) {
        for (PDSnatchProductItem *item in self.itemsArr) {
            item.leftTime -= 1000;
        }
    }
}

@end
