//
//  PDWinnerListViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 往期中奖名单控制器
@interface PDWinnerListViewController : AbstractViewController
@property (nonatomic, copy) NSString *ID;   /**< 本期夺宝ID*/
@end
