//
//  PDProductAllViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 所有奖品列表控制器>>>>>>>>>>> 弃用
@protocol PDProductAllViewControllerDelegate;

@interface PDProductAllViewController : AbstractViewController
@property (nonatomic, weak) id<PDProductAllViewControllerDelegate> delegate;
@property (nonatomic, strong) UICollectionView *mainCollectionView;
@end
@protocol PDProductAllViewControllerDelegate <NSObject>
@optional
- (void)allViewController:(PDProductAllViewController *)viewController didScrollWithOffset:(CGFloat)offsetY;

@end