//
//  PDProductDetailWebViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProductDetailWebViewController.h"

@interface PDProductDetailWebViewController ()

@end

@implementation PDProductDetailWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    self.barMainTitle = @"图文详情";
}


@end
