//
//  PDProductDetailViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProductDetailViewController.h"
#import "PDProductDetailView.h"
#import "PDProductDetailRateTableViewCell.h"
#import "PDProductDetailPartUserCell.h"
#import "PDProductDetailPriceWinnerCell.h"
#import "PDWinnerListViewController.h"
#import "PDSnatchSheetView.h"
#import "PDProductDetailWebViewController.h"    // 图文详情
#import "PDSnatchRecordList.h"
#import "PDSnatchActivityDetail.h"
#import "PDTopUpViewController.h"
#import "PDSnatchStakeModel.h"
#import "PDPayResultViewController.h"
#import "PDSnatchActivityWinnerModel.h"
#import "PDCenterPersonViewController.h"
#import "PDSnatchRuleViewController.h"
// 弹出消息
#import "PDPopViewManager.h"
#import "PDPopViewModel.h"
#import "PDPopMessageView.h"

@interface PDProductDetailViewController ()<UITableViewDataSource, UITableViewDelegate, PDTopUpViewControllerDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, strong) PDProductDetailView *detailView;
// 投注按钮
@property (nonatomic, strong) UIButton *moreButton; /**< 立即投入按钮*/
@property (nonatomic, assign) BOOL showButton;

@property (nonatomic, strong) UIView *buttonView;
@property (nonatomic, strong) UIView *titleBar; /**< 自定义的导航栏bar*/
@property (nonatomic, strong) UIImageView *backButtonBg; // 返回按钮背景

// 参与记录
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, strong) PDSnatchRecordList *recordList;

// 夺宝活动详情
@property (nonatomic, strong) PDSnatchActivityDetail *activityDetail;

@property (nonatomic, strong) PDSnatchSheetView *sheetView;

// pop
@property (nonatomic, strong) PDPopViewManager *popManager;

// 倒计时结束了是否更新过
@property (nonatomic) BOOL isUpdated;
@end

@implementation PDProductDetailViewController

- (void)dealloc {
    [self.detailView clearTimer];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchSnatchDetail];
    [self fetchJoinRecord];
    [self addPopViewManager];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
}

- (void)addPopViewManager {
    PDPopViewManager *popManager = [[PDPopViewManager alloc] init];
    popManager.pause = 2;
    [popManager registViewClass:[PDPopMessageView class] inView:self.view];
    [popManager registViewModelKey:@"model"];
    self.popManager = popManager;
}

- (void)basicSetting {
    _page = 1;
    _itemsArr = [NSMutableArray array];
}

- (void)pageSetting {
    // 自定义的导航栏
    self.titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, 64)];
    self.titleBar.alpha = 0;
    self.titleBar.backgroundColor = c2;
    [self.view addSubview:self.titleBar];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"夺宝详情";
    titleLabel.font = [UIFont systemFontOfCustomeSize:18];
    titleLabel.textColor = [UIColor whiteColor];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake((kScreenBounds.size.width - titleSize.width) / 2, (44 - titleSize.height) / 2 + 20, titleSize.width, titleSize.height);
    [self.titleBar addSubview:titleLabel];
    
    int backButtonWidth = 31; // 返回按钮（箭头）的宽
    int backButtonHeight = 38; //返回按钮（箭头）的高
    int backButtonBgWidth = 34; //返回按钮背景的宽
    UIImageView *backButtonBg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_center_back"]];
    backButtonBg.frame = CGRectMake(kTableViewSectionHeader_left - (backButtonBgWidth - backButtonWidth) / 2, 20 + (44 - backButtonBgWidth) / 2, backButtonBgWidth, backButtonBgWidth);
    [self.view addSubview:backButtonBg];
    self.backButtonBg = backButtonBg;
    
    UIButton *backButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [backButton setFrame:CGRectMake(kTableViewSectionHeader_left, 20 + (44 - backButtonHeight) / 2, backButtonWidth, backButtonHeight)];
    [backButton setImage:[UIImage imageNamed:@"icon_center_back"] forState:UIControlStateNormal];
    [backButton setImageEdgeInsets:UIEdgeInsetsMake(0, 3, 0, 3)];
    __weak typeof(self) weakSelf = self;
    [backButton buttonWithBlock:^(UIButton *button) {
        [weakSelf fetchExit];
        [weakSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.view addSubview:backButton];
    
    // 立即投入按钮
    UIView *buttonView = [[UIView alloc] initWithFrame:CGRectMake(0, kScreenBounds.size.height - 49, kScreenBounds.size.width, 49)];
    buttonView.backgroundColor = [UIColor whiteColor];
    buttonView.layer.shadowColor = [UIColor blackColor].CGColor;
    buttonView.layer.shadowOpacity = 0.5;
    buttonView.layer.shadowRadius = 3;
    buttonView.layer.shadowOffset = CGSizeMake(0, -1);
    [self.view addSubview:buttonView];
    self.buttonView = buttonView;
    
    UIButton *moreButton = [UIButton buttonWithType:UIButtonTypeCustom];
    moreButton.frame = buttonView.bounds;
    moreButton.backgroundColor = c2;
    [moreButton setTitle:@"立即投入" forState:UIControlStateNormal];
    [moreButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [moreButton addTarget:self action:@selector(didClickButton:) forControlEvents:UIControlEventTouchUpInside];
    [PDCenterTool setButtonBackgroundEffect:moreButton];
    [buttonView addSubview:moreButton];
    self.moreButton = moreButton;
    self.showButton = YES;
    
    self.detailView = [[PDProductDetailView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, LCFloat(260))];
    [self.detailView countdownToEndComplication:^{
        if (!weakSelf.isUpdated) {
            [weakSelf fetchSnatchDetail];
        }
    }];
    
    self.mainTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetHeight(moreButton.frame)) style:UITableViewStyleGrouped];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.recordList.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchJoinRecord];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            return ;
        }
    }];
    
    [self.view bringSubviewToFront:self.titleBar];
    [self.view bringSubviewToFront:self.buttonView];
    [self.view bringSubviewToFront:backButtonBg];
    [self.view bringSubviewToFront:backButton];
}

#pragma mark - Socket Delegate

- (void)socketDidBackData:(id)responseObject {
    [super socketDidBackData:responseObject];
    
    if ([responseObject isKindOfClass:[NSDictionary class]]) {
        NSDictionary *resp = (NSDictionary *)responseObject;
        NSDictionary *data = resp[@"data"];
        NSNumber *type = data[@"type"];
        if (type.intValue == 800) { // 投注
            PDPopViewModel *model = [[PDPopViewModel alloc] init];
            model.avatar = data[@"avatar"];
            model.nickname = data[@"nickname"];
            model.ID = data[@"id"];
            model.portion = data[@"portion"];
            model.wager = data[@"wager"];
            
            if ([model.ID isEqualToString:self.productId]) {
                [self.popManager popViewWithModel:model];
                
                // 刷新夺宝池数据
                [self fetchSnatchDetail];
            }
        } else if (type.intValue == 801) { // 有人中奖
            [self fetchSnatchDetail];
        } else if (type.intValue == 808) { // 帮投通知
            PDPopViewModel *model = [[PDPopViewModel alloc] init];
            model.avatar = data[@"helpedAvatar"];
            model.nickname = data[@"helpedNickname"];
            model.ID = data[@"id"];
            model.portion = data[@"portion"];
            model.wager = data[@"wager"];
            
            if ([model.ID isEqualToString:self.productId]) {
                [self.popManager popViewWithModel:model];
                
                // 刷新夺宝池数据
                [self fetchSnatchDetail];
            }
        }
    }
}

#pragma mark - 控件方法

/**
 * 点击立即投注
 */
- (void)didClickButton:(UIButton *)button {
    
    if (self.sheetView.isShowing) {
        return;
    }
    
    if (self.activityDetail.draw == NO) {
        
        [self fetchMemberTotalGold];
        
    } else {
        // 去请求新一期的id
        [self fetchNextActivity];
    }
}

- (void)showButtonAnimationWithAnimated:(BOOL)animated {
    [UIView animateWithDuration:animated? 0.1 : 0 animations:^{
        self.buttonView.frame = CGRectOffset(self.buttonView.frame, 0, -CGRectGetHeight(self.buttonView.frame));
    } completion:^(BOOL finished) {
        self.showButton = YES;
    }];
}

- (void)dismissButtonAnimationWithAnimated:(BOOL)animated {
    [UIView animateWithDuration:animated? 0.1 : 0 animations:^{
        self.buttonView.frame = CGRectOffset(self.buttonView.frame, 0, CGRectGetHeight(self.buttonView.frame));
    } completion:^(BOOL finished) {
        self.showButton = NO;
    }];
}

#pragma mark - UITabelView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 3;
    } else {
        return self.itemsArr.count + 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.section == 0) {
        
        // 结束的话显示winner
        if (self.activityDetail.draw == YES) {
            
            NSString *winnerCellId = @"winnerCellId";
            PDProductDetailPriceWinnerCell *winnerCell = [tableView dequeueReusableCellWithIdentifier:winnerCellId];
            if (!winnerCell) {
                winnerCell = [[PDProductDetailPriceWinnerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:winnerCellId];
            }
            winnerCell.model = self.activityDetail.winner;
            if (self.activityDetail.winner.memberId.length == 0) {
                winnerCell.hidden = YES;
            }
            return winnerCell;
            
        } else { // 投注数据
            NSString *rateCellID = @"rateCellId";
            PDProductDetailRateTableViewCell *rateCell = [tableView dequeueReusableCellWithIdentifier:rateCellID];
            if (!rateCell) {
                rateCell = [[PDProductDetailRateTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:rateCellID];
            }
            rateCell.model = self.activityDetail;
            return rateCell;
        }
        
    } else if (indexPath.section == 1) {
        NSString *normalCellID = @"normalCellID";
        UITableViewCell *normalCell = [tableView dequeueReusableCellWithIdentifier:normalCellID];
        if (!normalCell) {
            normalCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:normalCellID];
            normalCell.textLabel.font = [UIFont systemFontOfCustomeSize:16];
            normalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
        
        if (indexPath.row == 0) {
            normalCell.imageView.image = [UIImage imageNamed:@"icon_snatch_product_detail"];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:@"图文详情(建议在wifi情况下观看)"];
            [attributedString addAttributes:@{NSFontAttributeName: [UIFont systemFontOfCustomeSize:14], NSForegroundColorAttributeName:[UIColor lightGrayColor]} range:NSMakeRange(4, 14)];
            normalCell.textLabel.attributedText = attributedString;
        } else if (indexPath.row == 1) {
            normalCell.imageView.image = [UIImage imageNamed:@"icon_snatch_winnerlist"];
            normalCell.textLabel.text = @"往期中奖名单";
        } else {
            normalCell.imageView.image = [UIImage imageNamed:@"icon_snatch_rule"];
            normalCell.textLabel.text = @"夺宝规则";
        }
        
        return normalCell;
    } else {
        
        if (indexPath.row == 0) {
            NSString *partUserCellDescId = @"partUserCellDescId";
            UITableViewCell *partUserCell = [tableView dequeueReusableCellWithIdentifier:partUserCellDescId];
            if (!partUserCell) {
                partUserCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:partUserCellDescId];
                partUserCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            [PDCenterTool calculateCell:partUserCell leftSpace:0 rightSpace:0];
            NSString *startDate = [NSString stringWithFormat:@"参与记录(自%@开始)",[PDCenterTool dateWithFormat:@"MM-dd HH:mm:ss" fromTimestamp:self.recordList.startTime]];
            NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:startDate attributes:@{NSForegroundColorAttributeName:[UIColor lightGrayColor]}];
            [attributedString addAttribute:NSFontAttributeName value:[UIFont systemFontOfCustomeSize:12] range:NSMakeRange(4, startDate.length - 4)];
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, 4)];
            partUserCell.textLabel.attributedText = attributedString;
            partUserCell.detailTextLabel.textColor = c26;
            partUserCell.detailTextLabel.text = [NSString stringWithFormat:@"当前参与:%ld人",(long)self.recordList.joinCount];
            return partUserCell;
        } else {
            NSString *partUserCellId = @"partUserCellId";
            PDProductDetailPartUserCell *partCell = [tableView dequeueReusableCellWithIdentifier:partUserCellId];
            if (!partCell) {
                partCell = [[PDProductDetailPartUserCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:partUserCellId];
            }
            if (indexPath.row - 1 < self.itemsArr.count) {
                partCell.model = self.itemsArr[indexPath.row - 1];
            }
            return partCell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return kTableViewHeader_height;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.01;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (self.activityDetail.draw == YES) {
            if (self.activityDetail.winner.memberId.length > 0) {
                return [PDProductDetailPriceWinnerCell cellHeight];
            } else {
                return 0;
            }
            
        } else {
            return [PDProductDetailRateTableViewCell cellHeight];
        }
    } else if (indexPath.section == 1) {
        return 50;
    } else {
        if (indexPath.row == 0) {
            return 40;
        } else {
            return [PDProductDetailPartUserCell cellHeight];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        
        if (indexPath.row == 0) {   // 图文详情
            [self fetchProductDetail];
        } else if (indexPath.row == 1) {    // 往期中奖
            PDWinnerListViewController *winnerListViewController = [[PDWinnerListViewController alloc] init];
            winnerListViewController.ID = self.productId;
            [self pushViewController:winnerListViewController animated:YES];
        } else {
            // 夺宝规则
            [self fetchSnatchRule];
        }
        
    } else if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            return;
        } else {
            PDSnatchRecordItem *item = self.itemsArr[indexPath.row - 1];
            NSString *memberId = item.ID;
            PDCenterPersonViewController *personViewController = [[PDCenterPersonViewController alloc] init];
            personViewController.transferMemberId = memberId;
            if ([memberId isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:CustomerMemberId]]) {
                personViewController.isMe = YES;
            }
            [self pushViewController:personViewController animated:YES];
        }
    }
}

#pragma mark - UIScrollView delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    // 200为一个节点
    if (offsetY > 0) {
        self.titleBar.alpha = offsetY / 200;
        self.backButtonBg.alpha = (200 - offsetY) / 200;
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:NO];
    } else {
        self.titleBar.alpha = 0;
        self.backButtonBg.alpha = 1;
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault animated:NO];
    }
    if (offsetY < 0) {
        self.mainTableView.contentOffset = CGPointMake(0, 0);
    }
}

#pragma mark - 网络请求

/** 请求图文详情*/
- (void)fetchProductDetail {
    NSString *url = [NSString stringWithFormat:@"http://%@:%@%@?id=%@",JAVA_Host,JAVA_Port,snatchProductImagetextDetail,self.productId];
    PDProductDetailWebViewController *webViewController = [[PDProductDetailWebViewController alloc] init];
    [webViewController webDirectedWebUrl:url];
    [self pushViewController:webViewController animated:YES];
}

/** 参与记录*/
- (void)fetchJoinRecord {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchJoinRecord requestParams:@{@"id":self.productId, @"pageNum":@(_page)} responseObjectClass:[PDSnatchRecordList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDSnatchRecordList *list = (PDSnatchRecordList *)responseObject;
            weakSelf.recordList = list;
            [weakSelf.itemsArr addObjectsFromArray:list.items];
            [weakSelf.mainTableView reloadData];
            [weakSelf.mainTableView stopFinishScrollingRefresh];
        }
    }];
}

/** 夺宝活动详情*/
- (void)fetchSnatchDetail {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchProductDetail requestParams:@{@"id":self.productId} responseObjectClass:[PDSnatchActivityDetail class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDSnatchActivityDetail *detail = (PDSnatchActivityDetail *)responseObject;
            // 有下一期，更新倒计时的重置bool值
            weakSelf.isUpdated = YES;
            
            weakSelf.activityDetail = detail;
            weakSelf.detailView.model = detail;
            weakSelf.detailView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [PDProductDetailView viewHeightWithModel:detail]);
            weakSelf.mainTableView.tableHeaderView = self.detailView;
            [weakSelf.mainTableView reloadData];
            [weakSelf updateEventButton];
        }
    }];
}

/** 投入竹子份数*/
- (void)fetchStakeWithBannerPartCount:(NSInteger)count {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchStake requestParams:@{@"phaseId":self.productId,@"portion":@(count)} responseObjectClass:[PDSnatchStakeModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        if (isSucceeded) {
            PDSnatchStakeModel *stakeModel = (PDSnatchStakeModel *)responseObject;
            if (stakeModel.goldenough) {
                PDPayResultViewController *resultViewController = [[PDPayResultViewController alloc] init];
                resultViewController.stake = stakeModel;
                resultViewController.canHelp = weakSelf.activityDetail.canHelp;
                [weakSelf pushViewController:resultViewController animated:YES];
                
                // 更新数据
                [weakSelf fetchSnatchDetail];
                weakSelf.page = 1;
                if (weakSelf.itemsArr.count > 0) {
                    [weakSelf.itemsArr removeAllObjects];
                }
                [weakSelf fetchJoinRecord];
            } else {
                [weakSelf showNotEnough];
            }
        }
    }];
}

/** 会员金币资产*/
- (void)fetchMemberTotalGold {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:centerMemberWallet requestParams:nil responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }

        if (isSucceeded) {
            NSNumber *totalbamboo = responseObject[@"totalbamboo"]; // 总资产
            // 计算竹子的份数
            NSUInteger portion = totalbamboo.integerValue / weakSelf.activityDetail.wagersPerPortion;
            
            // 如果余额不足，提示
            if (portion == 0) {
#ifdef DEBUG
                [weakSelf showNotEnough];
#else
                if ([AccountModel sharedAccountModel].isShenhe) {
                    [[PDAlertView sharedAlertView] showAlertWithTitle:@"提示" conten:@"金币不足，去分享吧，骚年！分享可以获得金币。" isClose:NO btnArr:@[@"确定"] buttonClick:^(NSInteger buttonIndex) {
                        [JCAlertView dismissAllCompletion:NULL];
                    }];
                } else {
                    [weakSelf showNotEnough];
                }
#endif
            } else {
                // 按钮缩下去
                if (weakSelf.showButton) {
                    [weakSelf dismissButtonAnimationWithAnimated:YES];
                }
                
                PDSnatchSheetView *snatchView = [[PDSnatchSheetView alloc] init];
                snatchView.model = weakSelf.activityDetail;
                snatchView.totalGold = MIN(portion, weakSelf.activityDetail.maxMemberPortion == 0? portion : weakSelf.activityDetail.maxMemberPortion);
                weakSelf.sheetView = snatchView;
                [weakSelf submitInput:snatchView];
                [weakSelf dismissSelectedViewWithMoreButtonChangtoNormal];
            }
        }
    }];
}

- (void)dismissSelectedViewWithMoreButtonChangtoNormal {
    __weak typeof(self) weakSelf = self;
    [self.sheetView dismissWithActionBlock:^{
        if (weakSelf.showButton == NO) {
            [weakSelf showButtonAnimationWithAnimated:YES];
        }
    }];
}

- (void)submitInput:(PDSnatchSheetView *)snatchView {
    __weak typeof(self)weakSelf = self;
    [snatchView submitWithGoldCount:^(NSInteger count) {
        // 投入竹子份数
        [weakSelf fetchStakeWithBannerPartCount:count];
    }];
}

- (void)showNotEnough {
    __weak typeof(self) weakSelf = self;
    [[PDAlertView sharedAlertView] showAlertWithTitle:@"余额不足" conten:@"您的竹子余额已经不足啦，快去兑换或者邀请更多好友吧~" isClose:NO btnArr:@[@"去兑换", @"取消"] buttonClick:^(NSInteger buttonIndex) {                                                                                                                                                                 [JCAlertView dismissWithCompletion:NULL];
        if (buttonIndex == 0) {
            PDTopUpViewController *topUpViewController = [[PDTopUpViewController alloc] init];
            topUpViewController.delegate = weakSelf;
            [weakSelf pushViewController:topUpViewController animated:YES];
        }
    }];
}

- (void)fetchNextActivity {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchNextId requestParams:@{@"id":self.productId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }

        if (isSucceeded) {
            NSString *productId = responseObject[@"id"];
            PDProductDetailViewController *detailViewController = [[PDProductDetailViewController alloc] init];
            detailViewController.hidesBottomBarWhenPushed = YES;
            detailViewController.productId = productId;
            NSMutableArray *viewControllers = [weakSelf.navigationController.viewControllers mutableCopy];
            [viewControllers removeLastObject];
            [viewControllers addObject:detailViewController];
            [weakSelf.navigationController setViewControllers:[viewControllers copy] animated:YES];
        }
    }];
}

- (void)fetchExit {
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchExit requestParams:@{@"id":self.productId} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
    }];
}

- (void)fetchSnatchRule {
    PDSnatchRuleViewController *rule = [[PDSnatchRuleViewController alloc] init];
    [rule webDirectedWebUrl:[PDCenterTool absoluteUrlWithRisqueUrl:snatchRule]];
    [self pushViewController:rule animated:YES];
}

#pragma mark - PDTopUpViewControllerDelegate

- (void)goldExchangeFinish:(PDTopUpViewController *)topupViewController {
    [topupViewController.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 更新立即投入按钮

- (void)updateEventButton {
    if (self.activityDetail.draw == YES) {
        if (self.activityDetail.hasNextLottery) {   // 有下一期
            CGFloat height = self.moreButton.frame.size.height;
            self.moreButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(100), 0, LCFloat(100), height);
            
            UILabel *textLabel = [[UILabel alloc] init];
            textLabel.text = @"新一期活动正在火爆进行中...";
            textLabel.font = [UIFont systemFontOfCustomeSize:14];
            CGSize textSize = [textLabel.text sizeWithCalcFont:textLabel.font];
            textLabel.frame = CGRectMake(kTableViewSectionHeader_left, (height - textSize.height) / 2, textSize.width, textSize.height);
            [self.buttonView addSubview:textLabel];
        } else {    // 没有下一期
            CGFloat height = self.moreButton.frame.size.height;
            self.moreButton.backgroundColor = [UIColor lightGrayColor];
            self.moreButton.frame = CGRectMake(kScreenBounds.size.width - LCFloat(100), 0, LCFloat(100), height);
            self.moreButton.enabled = NO;
            
            UILabel *textLabel = [[UILabel alloc] init];
            textLabel.text = @"活动火爆，该奖品已经断货";
            textLabel.font = [UIFont systemFontOfCustomeSize:14];
            CGSize textSize = [textLabel.text sizeWithCalcFont:textLabel.font];
            textLabel.frame = CGRectMake(kTableViewSectionHeader_left, (height - textSize.height) / 2, textSize.width, textSize.height);
            [self.buttonView addSubview:textLabel];
        }
    }
}


@end
