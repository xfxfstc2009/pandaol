//
//  PDWinnerListViewController.m
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDWinnerListViewController.h"
#import "PDWinnerListTableViewCell.h"
#import "PDCenterTool.h"
#import "PDCenterPersonViewController.h"
#import "PDSnatchWinerList.h"

#define kWinnerTabelViewSectionHeight 25
@interface PDWinnerListViewController ()<UITableViewDataSource ,UITableViewDelegate>
@property (nonatomic, strong) UITableView *mainTableView;
@property (nonatomic, assign) NSInteger page;
@property (nonatomic, strong) NSMutableArray *itemsArr;
@property (nonatomic, strong) PDSnatchWinerList *winnerList;
@end

@implementation PDWinnerListViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self fetchData];
}

- (void)basicSetting {
    self.barMainTitle = @"往期中奖名单";
    _page = 1;
    _itemsArr = [NSMutableArray array];
}

- (void)pageSetting {
    self.mainTableView = [[UITableView alloc] initWithFrame:self.view.bounds];
    self.mainTableView.backgroundColor = [UIColor clearColor];
    self.mainTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainTableView.delegate = self;
    self.mainTableView.dataSource = self;
    self.mainTableView.tableFooterView = [UIView new];
    self.mainTableView.separatorColor = c27;
    [self.view addSubview:self.mainTableView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainTableView appendingPullToRefreshHandler:^{
        weakSelf.page = 1;
        if (weakSelf.itemsArr.count) {
            [weakSelf.itemsArr removeAllObjects];
        }
        [weakSelf fetchData];
    }];
    
    [self.mainTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.winnerList.hasNextPage) {
            weakSelf.page++;
            [weakSelf fetchData];
        } else {
            [weakSelf.mainTableView stopFinishScrollingRefresh];
            return ;
        }
    }];
}

#pragma mark -
#pragma mark -- UITableView delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.itemsArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.itemsArr.count) {
        PDSnatchWinerListItem *item = self.itemsArr[section];
        return item.detail.count;
    } else {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *winnerCellId = @"winnerCellId";
    PDWinnerListTableViewCell *winnerListCell = [tableView dequeueReusableCellWithIdentifier:winnerCellId];
    if (!winnerListCell) {
        winnerListCell = [[PDWinnerListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:winnerCellId];
    }
    
    if (self.itemsArr.count) {
        PDSnatchWinerListItem *item = self.itemsArr[indexPath.section];
        winnerListCell.model = item.detail[indexPath.row];
    }
    
    return winnerListCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [PDWinnerListTableViewCell cellHeight];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    PDSnatchWinerListItem *item = self.itemsArr.count? self.itemsArr[section] : nil;
    
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = BACKGROUND_VIEW_COLOR;
    
    UILabel *dateLabel = [[UILabel alloc] init];
    dateLabel.text = [PDCenterTool dateWithFormat:@"yyyy-MM-dd" fromTimestamp:item.day];
    dateLabel.font = [UIFont systemFontOfCustomeSize:13];
    dateLabel.textColor = [UIColor grayColor];
    CGSize dateSize = [dateLabel.text sizeWithCalcFont:dateLabel.font];
    dateLabel.frame = CGRectMake(kTableViewSectionHeader_left, kWinnerTabelViewSectionHeight - dateSize.height, dateSize.width, dateSize.height);
    [headerView addSubview:dateLabel];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return kWinnerTabelViewSectionHeight;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.itemsArr.count) {
        PDSnatchWinerListItem *item = self.itemsArr[indexPath.section];
        PDSnatchWinnerDetail *detail = item.detail[indexPath.row];
        PDCenterPersonViewController *personalHomepageViewController = [[PDCenterPersonViewController alloc] init];
        personalHomepageViewController.transferMemberId = detail.ID;
        if ([detail.ID isEqualToString:[[NSUserDefaults standardUserDefaults] objectForKey:CustomerMemberId]]) {
            personalHomepageViewController.isMe = YES;
        }
        [self pushViewController:personalHomepageViewController animated:YES];
    }
    
}

#pragma mark -
#pragma mark -- 网络请求

- (void)fetchData {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchWinerList requestParams:@{@"id":self.ID, @"pageNum":@(_page)} responseObjectClass:[PDSnatchWinerList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            PDSnatchWinerList *list = (PDSnatchWinerList *)responseObject;
            weakSelf.winnerList = list;
            [weakSelf.itemsArr addObjectsFromArray:list.items];
            [weakSelf.mainTableView reloadData];
            if (weakSelf.itemsArr.count == 0) {
                [weakSelf showNoDataPage];
                weakSelf.mainTableView.scrollEnabled = NO;
            } else {
                weakSelf.mainTableView.scrollEnabled = YES;
                [weakSelf.mainTableView dismissPrompt];
            }
        }
        
        [weakSelf.mainTableView stopFinishScrollingRefresh];
        [weakSelf.mainTableView stopPullToRefresh];
    }];
}

#pragma mark -- 显示无数据页

- (void)showNoDataPage {
    [self.mainTableView showPrompt:@"暂无中奖名单!" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:0 tapBlock:NULL];
}

@end
