//
//  PDProductDetailViewController.h
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 夺宝详情
@interface PDProductDetailViewController : AbstractViewController
@property (nonatomic, copy) NSString *productId;    /**< 夺宝活动ID*/
@end
