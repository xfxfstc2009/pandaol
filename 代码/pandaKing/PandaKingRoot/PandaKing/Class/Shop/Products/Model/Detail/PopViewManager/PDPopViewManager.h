//
//  PDPopViewManager.h
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface PDPopViewManager : NSObject
@property (nonatomic, assign) NSTimeInterval pause; /**< 设置暂停的时间，默认1s*/
@property (nonatomic, assign) NSTimeInterval duration;  /**< 动画的时间，默认1.2s*/
/**
 在父视图中注册
 */
- (void)registViewClass:(Class)viewClass inView:(UIView *)view;
/**
 注册需要更新的属性的key
 */
- (void)registViewModelKey:(NSString *)modelKey;
/**
 视图数据更新
 */
- (void)popViewWithModel:(id)model;
/**
 开始监听，默认创建好的时候就开始监听。在停止监听后需要主动启动
 */
- (void)startMonitor;
/**
 停止监听，更新model的值将不起作用
 */
- (void)stopMonitor;

@end
