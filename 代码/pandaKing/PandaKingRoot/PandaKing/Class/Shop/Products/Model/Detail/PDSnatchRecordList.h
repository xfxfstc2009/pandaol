//
//  PDSnatchRecordList.h
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDSnatchRecordItem.h"

@interface PDSnatchRecordList : FetchModel
@property (nonatomic, assign) NSInteger joinCount;  /**< 参与人数*/
@property (nonatomic, assign) NSTimeInterval startTime;
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) NSArray <PDSnatchRecordItem>*items;
@end
