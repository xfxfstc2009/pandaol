//
//  PDSnatchWinnerDetail.h
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDSnatchWinnerDetail <NSObject>

@end

@interface PDSnatchWinnerDetail : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, assign) NSInteger winnerStakePortions;
@property (nonatomic, assign) NSTimeInterval stayTime;
@property (nonatomic, copy) NSString *num;    /**< 夺宝期号*/
@end
