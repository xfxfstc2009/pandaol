//
//  PDPopViewModel.h
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDPopViewModel : FetchModel
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *ID;   /**< 期号*/
@property (nonatomic, strong) NSNumber *gold;
@property (nonatomic, strong) NSNumber *portion;// 投入份数
@property (nonatomic, strong) NSNumber *wager;// 总投入的竹子

// 新增帮投
@property (nonatomic, copy) NSString *helpedAvatar;
@property (nonatomic, copy) NSString *helpNickname;
@end
