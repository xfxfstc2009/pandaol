//
//  PDSnatchProductList.h
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDSnatchProductItem.h"

@interface PDSnatchProductList : FetchModel
@property (nonatomic, assign) BOOL hasNextPage;
@property (nonatomic, assign) NSInteger pageNum;
@property (nonatomic, strong) NSArray <PDSnatchProductItem>*items;
@end
