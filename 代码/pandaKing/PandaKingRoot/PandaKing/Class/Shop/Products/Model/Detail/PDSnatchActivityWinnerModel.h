//
//  PDSnatchActivityWinnerModel.h
//  PandaKing
//
//  Created by Cranz on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDSnatchActivityWinnerModel : FetchModel
@property (nonatomic, copy) NSString *memberId;
@property (nonatomic, copy) NSString *nickname;
@property (nonatomic, copy) NSString *avatar;
@property (nonatomic, assign) NSInteger winnerStakeWagers; // 总投入的竹子数
@property (nonatomic, assign) NSInteger winnerStakePortions;
@property (nonatomic, assign) NSTimeInterval drawTime;
@property (nonatomic, assign) NSInteger winPortionNum;
@end
