//
//  PDSnatchProductItem.h
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDSnatchProductItem <NSObject>

@end
@interface PDSnatchProductItem : FetchModel
@property (nonatomic, copy) NSString *ID;   /**< 商品ID*/
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *image;
@property (nonatomic, assign) NSInteger joinedCount;    /**< 参与人数*/
@property (nonatomic, assign) NSTimeInterval drawTime;  /**< 开奖时间戳*/
@property (nonatomic, assign) NSTimeInterval leftTime;
@property (nonatomic, assign) NSTimeInterval currentTime;
@property (nonatomic, assign) NSInteger position;   /**< 位置 前4*/
@property (nonatomic, assign) NSTimeInterval openTime; /**< 转化后的真正的开奖时间*/
@property (nonatomic, assign) int progress; // 开奖进度
@end