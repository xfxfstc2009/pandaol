//
//  PDSnatchProductBannerItems.h
//  PandaKing
//
//  Created by Cranz on 16/10/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDSnatchProductBannerModel.h"

@interface PDSnatchProductBannerItems : FetchModel
@property (nonatomic, strong) NSArray <PDSnatchProductBannerModel> *items;
@end
