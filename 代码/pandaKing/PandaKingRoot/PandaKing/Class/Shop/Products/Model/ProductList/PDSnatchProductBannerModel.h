//
//  PDSnatchProductBannerModel.h
//  PandaKing
//
//  Created by Cranz on 16/10/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDSnatchProductBannerModel <NSObject>

@end
@interface PDSnatchProductBannerModel : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *text;
@property (nonatomic, copy) NSString *pic;
@property (nonatomic, copy) NSString *actionType;//treasure 夺宝活动,news 新闻资讯
@property (nonatomic, copy) NSString *actionEntityId;//夺宝活动:活动id；news:catid,id.
@end
