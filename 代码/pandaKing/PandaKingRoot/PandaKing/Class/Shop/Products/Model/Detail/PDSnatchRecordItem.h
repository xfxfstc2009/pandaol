//
//  PDSnatchRecordItem.h
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDSnatchRecordItem <NSObject>

@end

@interface PDSnatchRecordItem : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *avatar;
/**
 * 投注份数
 */
@property (nonatomic, assign) NSInteger portionCount;
/**
 * 是否是其他人帮投
 */
@property (nonatomic, assign) BOOL help;
@property (nonatomic, assign) NSTimeInterval stayTime;  /**< 投注时间*/
@end
