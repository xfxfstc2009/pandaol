//
//  PDSnatchStakeModel.h
//  PandaKing
//
//  Created by Cranz on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

/// 投入金币返回的model
@interface PDSnatchStakeModel : FetchModel
/**
 * 夺宝活动id
 */
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *stakeCount;   /**< 投入*/
/**
 * 商品id
 */
@property (nonatomic, copy) NSString *commodityId;
@property (nonatomic, copy) NSString *commodityName;
@property (nonatomic, copy) NSString *commodityImage;
@property (nonatomic, copy) NSString *drawNum;  /**< 参与期号*/
@property (nonatomic, assign) BOOL goldenough;  /**< 余额足：才有其他数据*/
@property (nonatomic, assign) NSInteger wager; // 总投入的竹子
@property (nonatomic, assign) NSInteger portion; // 投入份数
@property (nonatomic, strong) NSArray *portionNums; // 夺宝号码数组
@end
