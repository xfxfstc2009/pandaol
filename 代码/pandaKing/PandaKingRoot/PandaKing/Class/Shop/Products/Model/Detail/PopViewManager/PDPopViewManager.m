//
//  PDPopViewManager.m
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPopViewManager.h"

@interface PDPopViewManager ()
@property (nonatomic, assign) Class viewClass;
@property (nonatomic, strong) UIView *superView;
@property (nonatomic, copy)   NSString *modelKey;
@property (nonatomic, strong) NSMutableArray *viewsArr; /**< 存放view的容器*/
@property (nonatomic, strong) NSMutableArray *showArr;  /**< 当前在superView上展示的views*/
@property (nonatomic, assign) BOOL monitoring;  /**< 监听状态，默认YES*/
@end

@implementation PDPopViewManager

- (instancetype)init {
    self = [super init];
    if (self) {
        [self basicSetting];
    }
    return self;
}

- (void)basicSetting {
    _pause = 1;
    _duration = 1.2;
    _viewsArr = [NSMutableArray array];
    _showArr  = [NSMutableArray array];
    _monitoring = NO;
}

#pragma mark -- 注册视图相关信息

- (void)registViewClass:(Class)viewClass inView:(UIView *)view {
    _viewClass = viewClass;
    _superView = view;
}

- (void)registViewModelKey:(NSString *)modelKey {
    _modelKey = modelKey;
    [self startMonitor];
}

#pragma mark -- 更新视图信息 -> model

- (void)popViewWithModel:(id)model {
    if (!_monitoring) { // 监听状态判断
        return;
    }
    
    if (_showArr.count > 0) { // 此时superView上正在展示动画
        for (UIView *viewObj in _showArr) {
            [UIView animateWithDuration:_duration animations:^{
                viewObj.frame = CGRectOffset(viewObj.frame, 0, viewObj.frame.size.height + 2);
            }];
        }
    }
    
    [self updateViewWithModel:model];
}

- (void)updateViewWithModel:(id)model {
    
    // 创建将要展示的视图，并且model赋值
    UIView *viewObj = [[_viewClass alloc] init];
    [_viewsArr addObject:viewObj];
    [viewObj setValue:model forKey:_modelKey];
    // 重新布局
    CGSize viewSize = viewObj.frame.size;
    viewObj.frame = CGRectMake(- viewSize.width, 80, viewSize.width, viewSize.height);
    
    [self showView];
}

#pragma mark -- 视图显示隐藏

- (void)showView {
    
    UIView *viewObj = _viewsArr.lastObject;
    [_superView addSubview:viewObj];
    [_showArr addObject:viewObj];
    
    // 有两个动画，一个是弹出来的，另一个是渐隐的动画
    CABasicAnimation *popAnim = [CABasicAnimation animationWithKeyPath:@"transform.translation.x"];
    popAnim.delegate = self;
    popAnim.duration = _duration;
    popAnim.fromValue = @0;
    popAnim.toValue = @(viewObj.frame.size.width);
    popAnim.removedOnCompletion = NO;
    popAnim.fillMode = kCAFillModeForwards;
    [viewObj.layer addAnimation:popAnim forKey:@"popAnimation"];
}

- (void)dismissView {
    UIView *viewObj = _viewsArr.firstObject;
    [_viewsArr removeObject:viewObj];
    [UIView animateWithDuration:_duration delay:_pause options:UIViewAnimationOptionCurveEaseIn animations:^{
        viewObj.alpha = 0;
    } completion:^(BOOL finished) {
        [viewObj removeFromSuperview];
        [_showArr removeObject:viewObj];
    }];
}

#pragma mark -- CAAnimationDelegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    [self dismissView];
}

#pragma mark -- 监听控制

- (void)startMonitor {
    _monitoring = YES;
}

- (void)stopMonitor {
    _monitoring = NO;
}

@end
