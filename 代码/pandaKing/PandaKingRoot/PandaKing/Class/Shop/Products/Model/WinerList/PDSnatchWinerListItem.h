//
//  PDSnatchWinerListItem.h
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDSnatchWinnerDetail.h"


@protocol PDSnatchWinerListItem <NSObject>
@end
@interface PDSnatchWinerListItem : FetchModel
@property (nonatomic, strong) NSArray <PDSnatchWinnerDetail>*detail;
@property (nonatomic, assign) NSTimeInterval day;
@end
