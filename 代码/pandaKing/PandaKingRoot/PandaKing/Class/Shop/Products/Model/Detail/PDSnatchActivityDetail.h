//
//  PDSnatchActivityDetail.h
//  PandaKing
//
//  Created by Cranz on 16/9/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDSnatchActivityWinnerModel.h"

/// 夺宝活动详情model
@interface PDSnatchActivityDetail : FetchModel
@property (nonatomic, copy) NSString *ID;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *price;
@property (nonatomic, assign) NSInteger drawNum;
@property (nonatomic, copy) NSString *totalGoldCount;//夺宝池金币总数
@property (nonatomic, assign) NSInteger memberStakeCount;//当前会员本期已经投入的份数
@property (nonatomic, copy) NSString *odds; /**< 中奖几率*/
@property (nonatomic, strong) NSArray *images;
@property (nonatomic, assign) NSTimeInterval leftTime;
@property (nonatomic, assign) NSTimeInterval currentTime;
@property (nonatomic, assign) NSInteger joinedCount;//本期参与人数
@property (nonatomic, assign) NSTimeInterval startTime;//开奖时间戳
@property (nonatomic, assign) BOOL draw;    /**< 是否结束*/
@property (nonatomic, assign) BOOL hasNextLottery;  /**< 是否有下一期*/
@property (nonatomic, strong) PDSnatchActivityWinnerModel *winner;

// 新加入的字断
@property (nonatomic, copy) NSString *category;
@property (nonatomic, assign) NSInteger maxPortion; // 总需多少份
@property (nonatomic, assign) NSInteger totalPortion; // 总共投入的份数
@property (nonatomic, assign) NSInteger wagersPerPortion; // 1人份=多少竹子
@property (nonatomic, assign) NSInteger maxMemberPortion; // 用户此次夺宝能投的最大份数，需要和用户的竹子数做比较

// 新加夺宝分享
@property (nonatomic, assign) BOOL canHelp;
@end
