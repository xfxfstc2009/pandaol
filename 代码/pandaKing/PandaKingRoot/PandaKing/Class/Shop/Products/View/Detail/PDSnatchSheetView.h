//
//  PDSnatchSheetView.h
//  PandaKing
//
//  Created by Cranz on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSelectedSheetView.h"
#import "PDSnatchActivityDetail.h"

/// 立即投入弹出的提示框
@interface PDSnatchSheetView : PDSelectedSheetView
@property (nonatomic, strong) PDSnatchActivityDetail *model;
@property (nonatomic, assign) NSInteger totalGold; // 传入进来的竹子份数，gold是财富的意思
- (void)submitWithGoldCount:(void(^)(NSInteger count))submitBlock;
@end
