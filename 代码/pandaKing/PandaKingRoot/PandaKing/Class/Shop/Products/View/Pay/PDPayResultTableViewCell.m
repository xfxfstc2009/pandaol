//
//  PDPayResultTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPayResultTableViewCell.h"

#define kLabelSpace LCFloat(5) // 名字、旗号之间的距离
#define kImageTop LCFloat(10)  // 图片的顶部高度
#define kImageRight LCFloat(11) // 图片右侧的距离
#define imageWidth LCFloat(60)
#define kTextFont [UIFont systemFontOfCustomeSize:12]

@interface PDPayResultTableViewCell ()
@property (nonatomic, strong) PDImageView *productImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *drawLabel;   /**< 期号*/
@property (nonatomic, strong) UILabel *IDLabel; /**< 每一份夺宝编号标题*/
@property (nonatomic, strong) UILabel *numberLabel; // 编号串
@end

@implementation PDPayResultTableViewCell

+ (CGFloat)cellHeightWithModel:(PDSnatchStakeModel *)model {
    CGSize nameSize = [model.commodityName sizeWithCalcFont:[UIFont systemFontOfCustomeSize:14] constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - imageWidth - kImageRight, CGFLOAT_MAX)];
    CGFloat drawHeight = [NSString contentofHeightWithFont:kTextFont];
    NSMutableString *numString = [NSMutableString string];
    for (int i = 0; i < model.portionNums.count; i++) {
        if (i == model.portionNums.count - 1) {
            [numString appendFormat:@"%@",model.portionNums[i]];
        } else {
            [numString appendFormat:@"%@ ",model.portionNums[i]];
        }
    }
    CGSize numberSize = [[numString copy] sizeWithCalcFont:kTextFont constrainedToSize:[PDPayResultTableViewCell constraintSize]];
    return kImageTop * 2 + kLabelSpace * 2 + nameSize.height + drawHeight + numberSize.height;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)pageSetting {
    self.productImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, kImageTop, imageWidth, imageWidth)];
    self.productImageView.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self.contentView addSubview:self.productImageView];
    
    //name
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.nameLabel];
    
    //draw
    self.drawLabel = [[UILabel alloc] init];
    self.drawLabel.font = kTextFont;
    self.drawLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.drawLabel];
    
    // 夺宝号码
    self.IDLabel = [[UILabel alloc] init];
    self.IDLabel.font = kTextFont;
    self.IDLabel.textColor = self.drawLabel.textColor;
    [self.contentView addSubview:self.IDLabel];
    
    // 编号列表
    self.numberLabel = [[UILabel alloc] init];
    self.numberLabel.font = kTextFont;
    self.numberLabel.textColor = self.drawLabel.textColor;
    self.numberLabel.numberOfLines = 0;
    [self.contentView addSubview:self.numberLabel];
}

- (void)setModel:(PDSnatchStakeModel *)model {
    _model = model;
    
    [self.productImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithRisqueUrl:model.commodityImage] placeholder:nil callback:NULL];
    self.nameLabel.text = model.commodityName;
    self.drawLabel.text = [NSString stringWithFormat:@"参与期号：%@",model.drawNum];
    self.IDLabel.text = @"夺宝号码：";
    NSMutableString *numString = [NSMutableString string];
    for (int i = 0; i < model.portionNums.count; i++) {
        if (i == model.portionNums.count - 1) {
            [numString appendFormat:@"%@",model.portionNums[i]];
        } else {
            [numString appendFormat:@"%@ ",model.portionNums[i]];
        }
    }
    self.numberLabel.text = numString;
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - kTableViewSectionHeader_left * 2 - CGRectGetWidth(self.productImageView.frame) - kImageRight, CGFLOAT_MAX)];
    CGSize drawSize = [self.drawLabel.text sizeWithCalcFont:self.drawLabel.font];
    CGSize IDSize = [self.IDLabel.text sizeWithCalcFont:self.IDLabel.font];
    CGSize numberSize = [self.numberLabel.text sizeWithCalcFont:self.numberLabel.font constrainedToSize:[PDPayResultTableViewCell constraintSize]];
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.productImageView.frame) + kImageRight, CGRectGetMinY(self.productImageView.frame), nameSize.width, nameSize.height);
    self.drawLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + kLabelSpace, drawSize.width, drawSize.height);
    self.IDLabel.frame = CGRectMake(CGRectGetMinX(self.drawLabel.frame), CGRectGetMaxY(self.drawLabel.frame) + kLabelSpace, IDSize.width, IDSize.height);
    self.numberLabel.frame = CGRectMake(CGRectGetMaxX(self.IDLabel.frame), CGRectGetMinY(self.IDLabel.frame), numberSize.width, numberSize.height);
}

+ (CGSize)constraintSize {
    CGSize spaceSize = [@"123456789" sizeWithCalcFont:kTextFont];
    int numSpace = 10;
    // 留给号码串的空隙，一行固定显示三个
    return CGSizeMake(spaceSize.width * 3 + numSpace * 2, CGFLOAT_MAX);
}

@end
