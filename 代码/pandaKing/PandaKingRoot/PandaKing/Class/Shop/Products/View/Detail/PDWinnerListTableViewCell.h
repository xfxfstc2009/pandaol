//
//  PDWinnerListTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSnatchWinnerDetail.h"

@interface PDWinnerListTableViewCell : UITableViewCell
@property (nonatomic, strong) PDSnatchWinnerDetail *model;

+ (CGFloat)cellHeight;
@end
