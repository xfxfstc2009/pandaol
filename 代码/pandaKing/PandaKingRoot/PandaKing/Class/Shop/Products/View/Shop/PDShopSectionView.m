//
//  PDShopSectionView.m
//  PandaKing
//
//  Created by Cranz on 16/11/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDShopSectionView.h"


@implementation PDShopSectionView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self pageSetting];
        [self titleBarSetting];
    }
    return self;
}

- (void)pageSetting {
    PDCycleScrollView *scrollView = [PDCycleScrollView cycleScrollViewWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kBannerHeight) delegate:self placeholderImage:[UIImage imageNamed:@"icon_nordata"]];
    scrollView.backgroundColor = BACKGROUND_VIEW_COLOR;
    scrollView.pageControlAliment = SDCycleScrollViewPageContolAlimentRight;
    [self addSubview:scrollView];
    self.bannerScrollView = scrollView;
}

- (void)titleBarSetting {
    UIView *titleBar = [[UIView alloc] initWithFrame:CGRectMake(0, kBannerHeight, kScreenBounds.size.width, KTitleHeight)];
    titleBar.backgroundColor = c1;
    [self addSubview:titleBar];
    
    // 黑色小块
    CGFloat temp_y = 6;
    UIView *tempView = [[UIView alloc] initWithFrame:CGRectMake(0, temp_y, 5, KTitleHeight - temp_y * 2)];
    tempView.backgroundColor = c7;
    [titleBar addSubview:tempView];
    
    // 标题
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.text = @"夺宝";
    titleLabel.textColor = c7;
    titleLabel.font = [[UIFont systemFontOfCustomeSize:16] boldFont];
    CGSize titleSize = [titleLabel.text sizeWithCalcFont:titleLabel.font];
    titleLabel.frame = CGRectMake(CGRectGetMaxX(tempView.frame) + 10, (KTitleHeight - titleSize.height) / 2, titleSize.width, titleSize.height);
    [titleBar addSubview:titleLabel];
}

@end
