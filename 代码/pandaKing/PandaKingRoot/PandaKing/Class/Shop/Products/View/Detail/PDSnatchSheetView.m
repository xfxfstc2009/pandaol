//
//  PDSnatchSheetView.m
//  PandaKing
//
//  Created by Cranz on 16/9/27.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDSnatchSheetView.h"

#define kShowHeight   LCFloat(80)
#define kButtonHeight 44
#define kSelectHeight LCFloat(120)

typedef void(^submitGoldBlock)(NSInteger count);
@interface PDSnatchSheetView ()
@property (nonatomic, strong) UILabel *goldLabel;
@property (nonatomic, strong) UILabel *scaleLabel;  /**< 中奖比例*/
@property (nonatomic, strong) UILabel *changeLabel;
@property (nonatomic, strong) UISlider *slider;
@property (nonatomic, copy)   submitGoldBlock goldBlcok;
@property (nonatomic, strong) UIButton *sureButton;
@end

@implementation PDSnatchSheetView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    self.title = @"夺宝有风险，参与需谨慎";
    self.canTouchedCancle = NO;
    
    UIView *putView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kShowHeight + kButtonHeight + kSelectHeight)];
    putView.backgroundColor = [UIColor whiteColor];
    self.putView = putView;
    
    // 金币 desc
    UILabel *goldDescLabel = [[UILabel alloc] init];
    goldDescLabel.text = @"人份";
    goldDescLabel.textColor = [UIColor grayColor];
    goldDescLabel.backgroundColor = BACKGROUND_VIEW_COLOR;
    goldDescLabel.font = [UIFont systemFontOfCustomeSize:14];
    goldDescLabel.textAlignment = NSTextAlignmentCenter;
    CGSize goldDescSize = [goldDescLabel.text sizeWithCalcFont:goldDescLabel.font];
    goldDescLabel.frame = CGRectMake(kTableViewSectionHeader_left, (kShowHeight - goldDescSize.height - LCFloat(20)) / 2, goldDescSize.width + LCFloat(30), goldDescSize.height + LCFloat(20));
    [putView addSubview:goldDescLabel];
    
    self.goldLabel = [[UILabel alloc] init];
    self.goldLabel.font = [UIFont systemFontOfCustomeSize:18];
    [putView addSubview:self.goldLabel];
    
    // 竖线
    UIView *vLine = [[UIView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width / 2 - .5, 0, 1, kShowHeight)];
    vLine.backgroundColor = c27;
    [putView addSubview:vLine];
    
    // 比例
    self.scaleLabel = [[UILabel alloc] init];
    self.scaleLabel.font = [[UIFont systemFontOfCustomeSize:22] boldFont];
    self.scaleLabel.textColor = [UIColor blackColor];
    [putView addSubview:self.scaleLabel];
    
    // 比例 － desc
    UILabel *scaleDescLabel = [[UILabel alloc] init];
    scaleDescLabel.text = @"物品的\n中奖率";
    scaleDescLabel.textColor = [UIColor grayColor];
    scaleDescLabel.numberOfLines = 0;
    scaleDescLabel.font = [UIFont systemFontOfCustomeSize:14];
    CGSize scaleDescSize = [scaleDescLabel.text sizeWithCalcFont:scaleDescLabel.font];
    scaleDescLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - scaleDescSize.width, (kShowHeight - scaleDescSize.height) / 2, scaleDescSize.width, scaleDescSize.height);
    [putView addSubview:scaleDescLabel];
    
    // 横线
    UIView *hLine = [[UIView alloc] initWithFrame:CGRectMake(0, kShowHeight, kScreenBounds.size.width, 1)];
    hLine.backgroundColor = c27;
    [putView addSubview:hLine];
    
    // 滑杆
    // 滑杆的最大值会随着会员的总金币数变化
    CGFloat sliderX = LCFloat(60);
    self.slider = [[UISlider alloc] initWithFrame:CGRectMake(sliderX, kShowHeight + LCFloat(40), kScreenBounds.size.width - sliderX * 2, LCFloat(30))];
    self.slider.minimumValue = 0;
    self.slider.maximumValue = self.totalGold;
    self.slider.minimumTrackTintColor = c26;
    [self.slider addTarget:self action:@selector(didSlideWithSlider:) forControlEvents:UIControlEventValueChanged];
    [putView addSubview:self.slider];
    
    UILabel *changeLabel = [[UILabel alloc] init];
    changeLabel.font = [UIFont systemFontOfCustomeSize:14];
    changeLabel.textColor = [UIColor grayColor];
    [putView addSubview:changeLabel];
    self.changeLabel = changeLabel;
    
    // 加减按钮
    CGFloat btnWidth = LCFloat(35);
    UIButton *addButton = [UIButton buttonWithType:UIButtonTypeCustom];
    addButton.frame = CGRectMake(CGRectGetMaxX(self.slider.frame) + (sliderX - btnWidth) / 2, CGRectGetMinY(self.slider.frame) + (CGRectGetHeight(self.slider.frame) - btnWidth) / 2, btnWidth, btnWidth);
    [addButton setImage:[UIImage imageNamed:@"icon_snatch_add"] forState:UIControlStateNormal];
    [addButton addTarget:self action:@selector(didClickAddButton) forControlEvents:UIControlEventTouchUpInside];
    [putView addSubview:addButton];
    
    UIButton *reduceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    reduceButton.frame = CGRectMake((sliderX - btnWidth) / 2, CGRectGetMinY(addButton.frame), btnWidth, btnWidth);
    [reduceButton setImage:[UIImage imageNamed:@"icon_snatch_reduce"] forState:UIControlStateNormal];
    [reduceButton addTarget:self action:@selector(didClickReduceButton) forControlEvents:UIControlEventTouchUpInside];
    [putView addSubview:reduceButton];
    
    // 确认投入
    UIButton *sureButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureButton setTitle:@"立即投入" forState:UIControlStateNormal];
    sureButton.frame = CGRectMake(0, kShowHeight + kSelectHeight, kScreenBounds.size.width, kButtonHeight);
    [sureButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sureButton addTarget:self action:@selector(didClickButton) forControlEvents:UIControlEventTouchUpInside];
    [putView addSubview:sureButton];
    self.sureButton = sureButton;
    [self buttonUnEnable];
    
    [self showWithAnimationComplication:NULL];
}

- (void)buttonEnable {
    self.sureButton.backgroundColor = [UIColor blackColor];
    self.sureButton.enabled = YES;
}

- (void)buttonUnEnable {
    self.sureButton.backgroundColor = [UIColor lightGrayColor];
    self.sureButton.enabled = NO;
}

- (void)submitWithGoldCount:(void (^)(NSInteger))submitBlock {
    if (submitBlock) {
        self.goldBlcok = submitBlock;
    }
}

- (void)updateFrames {
    CGSize goldSize = [self.goldLabel.text sizeWithCalcFont:self.goldLabel.font];
    CGSize scaleSize = [self.scaleLabel.text sizeWithCalcFont:self.scaleLabel.font];
    
    self.goldLabel.frame = CGRectMake(kScreenBounds.size.width / 2 - LCFloat(20) - goldSize.width, (kShowHeight - goldSize.height) / 2, goldSize.width, goldSize.height);
    self.scaleLabel.frame = CGRectMake(kScreenBounds.size.width / 2 + LCFloat(12), (kShowHeight - scaleSize.height) / 2, scaleSize.width, scaleSize.height);
}

- (void)updateGold {
    //当前 投入的份数
    NSString *currentGold = [NSString stringWithFormat:@"%ld",(NSInteger)self.slider.value];
    self.goldLabel.text = currentGold;
    
    CGFloat scale;
    if (currentGold.intValue == 0) {
        scale = self.model.odds.floatValue / 100;
    } else {
        scale = (currentGold.floatValue + self.model.memberStakeCount) / (self.model.totalPortion + currentGold.intValue);
    }
    
    self.scaleLabel.text = [NSString stringWithFormat:@"%.2lf％",scale * 100];
}

#pragma mark -- set

- (void)setTotalGold:(NSInteger)totalGold {
    _totalGold = totalGold;
    self.slider.maximumValue = totalGold;
}

- (void)setModel:(PDSnatchActivityDetail *)model {
    _model = model;
    
    self.goldLabel.text = @"0";
    self.scaleLabel.text = [NSString stringWithFormat:@"%.2lf％",model.odds.floatValue];
    self.changeLabel.text = [NSString stringWithFormat:@"1人份=%ld竹子",model.wagersPerPortion];
    
    // 兑换比例由于不会经常变。因此提出来直接计算掉就好
    CGSize changeSize = [self.changeLabel.text sizeWithCalcFont:self.changeLabel.font];
    self.changeLabel.frame = CGRectMake((kScreenBounds.size.width - changeSize.width) / 2, CGRectGetMaxY(self.slider.frame) + LCFloat(10), changeSize.width, changeSize.height);
    
    [self updateFrames];
}

#pragma mark -- 控件方法

- (void)didClickAddButton {
    self.slider.value++;
    [self.slider setValue:self.slider.value animated:YES];
    
    [self updateGold];
    [self updateFrames];
    
    if (self.slider.value > 0) {
        [self buttonEnable];
    }
}

- (void)didClickReduceButton {
    self.slider.value--;
    [self.slider setValue:self.slider.value animated:YES];
    
    [self updateGold];
    [self updateFrames];
    
    if (self.slider.value == 0) {
        [self buttonUnEnable];
    } else {
        [self buttonEnable];
    }
}

- (void)didClickButton {
    self.goldBlcok((NSInteger)self.slider.value);
    [self dismissWithAnimationComplication:NULL];
}

- (void)didSlideWithSlider:(UISlider *)slider {
    [self updateGold];
    [self updateFrames];
    
    if (slider.value == 0) {
        [self buttonUnEnable];
    } else {
        [self buttonEnable];
    }
}

@end
