//
//  PDProductDetailView.m
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProductDetailView.h"
#import "PDCycleScrollView.h"
#import "UILabel+PDCategory.h"
#import "PDTimerLabel.h"

#define kScrollHeight (LCFloat(300))
#define kSpaceName_label LCFloat(10)
#define kTop LCFloat(10)
#define kBottom LCFloat(12)
#define kNameFont  [[UIFont systemFontOfCustomeSize:18] boldFont]
#define kPriceFont [[UIFont systemFontOfCustomeSize:17] boldFont]

typedef void(^PDProductBlock)();

@interface PDProductDetailView ()
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *issueLabel;  /**< 期号*/
@property (nonatomic, strong) PDTimerLabel *dateLabe;

@property (nonatomic, strong) PDCycleScrollView *scrollView;

@property (nonatomic, copy) PDProductBlock countdownBlock;
@end

@implementation PDProductDetailView

+ (CGFloat)viewHeightWithModel:(PDSnatchActivityDetail *)model {
    CGSize nameSize = [model.name sizeWithCalcFont:kNameFont constrainedToSize:CGSizeMake(kScreenBounds.size.width - 12 * 2, CGFLOAT_MAX)];
    return nameSize.height + kTop + kSpaceName_label + [NSString contentofHeightWithFont:kPriceFont] + kBottom + kScrollHeight;
}

- (void)clearTimer {
    [self.dateLabe clearTimer];
}

- (void)countdownToEndComplication:(void (^)())complication {
    if (complication) {
        self.countdownBlock = complication;
    }
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    self.backgroundColor = [UIColor whiteColor];
}

- (void)pageSetting {
    // 上面scrollView
    PDCycleScrollView *scrollView = [[PDCycleScrollView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScrollHeight)];
    scrollView.backgroundColor = BACKGROUND_VIEW_COLOR;
    scrollView.bannerImageViewContentMode = UIViewContentModeScaleAspectFill;
    scrollView.placeholderImage = [UIImage imageNamed:@"icon_nordata"];
    [self addSubview:scrollView];
    self.scrollView = scrollView;
    
    // 下部分
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = kNameFont;
    self.nameLabel.textColor = [UIColor blackColor];
    self.nameLabel.numberOfLines = 0;
    self.nameLabel.constraintSize = CGSizeMake(kScreenBounds.size.width - 12 * 2, CGFLOAT_MAX);
    [self addSubview:self.nameLabel];
    
    self.priceLabel = [[UILabel alloc] init];
    self.priceLabel.font = kPriceFont;
    self.priceLabel.textColor = [UIColor redColor];
    [self addSubview:self.priceLabel];
    
    self.issueLabel = [[UILabel alloc] init];
    self.issueLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.issueLabel.backgroundColor = RGB(229, 210, 195, 1);
    self.issueLabel.textColor = RGB(95, 57, 23, 1);
    self.issueLabel.layer.cornerRadius = 2.5;
    self.issueLabel.clipsToBounds = YES;
    self.issueLabel.textAlignment = NSTextAlignmentCenter;
    [self addSubview:self.issueLabel];
    
    self.dateLabe = [[PDTimerLabel alloc] init]; 
    self.dateLabe.font = [UIFont systemFontOfCustomeSize:14];
    self.dateLabe.textColor = [UIColor grayColor];
    self.dateLabe.prefixDesc = @"剩余时间:";
    [self addSubview:self.dateLabe];
}

- (void)setModel:(PDSnatchActivityDetail *)model {
    _model = model;
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@%@",[PDCenterTool nameForCategory:model.category], model.name];
    self.priceLabel.text = [NSString stringWithFormat:@"%@¥",model.price];
    self.issueLabel.text = [NSString stringWithFormat:@"第%ld期",(long)model.drawNum];
    
    NSMutableArray *imageArr = [NSMutableArray array];
    for (NSString *pre in model.images) {
        [imageArr addObject:[PDCenterTool absoluteUrlWithRisqueUrl:pre]];
    }
    self.scrollView.imageURLStringsGroup = [imageArr copy];
    
    CGSize nameSize = self.nameLabel.size;
    CGSize priceSize = self.priceLabel.size;
    CGSize issureSize = self.issueLabel.size;
    CGSize dataSize = self.dateLabe.size;
    
    self.nameLabel.frame = CGRectMake(12, kTop + kScrollHeight, nameSize.width, nameSize.height);
    self.priceLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + kSpaceName_label, priceSize.width, priceSize.height);
    self.issueLabel.frame = CGRectMake(CGRectGetMaxX(self.priceLabel.frame) + LCFloat(20), CGRectGetMaxY(self.priceLabel.frame) - issureSize.height, issureSize.width + LCFloat(10) * 2, issureSize.height);
    
    self.dateLabe.frame = CGRectMake(self.frame.size.width - 12 - dataSize.width, CGRectGetMaxY(self.priceLabel.frame) - dataSize.height, dataSize.width, dataSize.height);

    __weak typeof(self) weakSelf = self;
    [self.dateLabe countDownWithTimeNow:(model.currentTime / 1000) timeEnd:(model.startTime / 1000) resultBlock:^(CGSize size, NSString *dateString, NSTimeInterval t) {
        if (t <= -1) {
            !weakSelf.countdownBlock ?: weakSelf.countdownBlock();
        }
        
        // 如果已经开奖，显示已揭晓
        if (model.draw == YES) {
            weakSelf.dateLabe.text = @"已揭晓";
            size = [weakSelf.dateLabe.text sizeWithCalcFont:weakSelf.dateLabe.font];
        }
        
        weakSelf.dateLabe.frame = CGRectMake(weakSelf.frame.size.width - 12 - size.width, CGRectGetMaxY(weakSelf.priceLabel.frame) - size.height, size.width, size.height);
    }];
}

@end
