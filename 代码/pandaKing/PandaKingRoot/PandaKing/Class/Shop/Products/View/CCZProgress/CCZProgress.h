//
//  CCZProgress.h
//  TEST
//
//  Created by 金峰 on 2016/12/26.
//  Copyright © 2016年 金峰. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface CCZProgress : UIView
- (instancetype)initWithFrame:(CGRect)frame;

@property (nonatomic, readonly) CGFloat progress;
@property (nonatomic, strong, nullable) UIColor* progressColor; // 默认blue
@property (nonatomic, strong, nullable) UIColor* trackColor; // 默认淡灰色 ededed
@property (nonatomic) BOOL cornerToClips; // 默认YES

- (void)setProgress:(CGFloat)progress animated:(BOOL)animated;

@end
NS_ASSUME_NONNULL_END
