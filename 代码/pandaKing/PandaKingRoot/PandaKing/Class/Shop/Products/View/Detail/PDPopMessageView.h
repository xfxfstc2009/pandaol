//
//  PDPopMessageView.h
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDPopViewModel.h"

/// 弹出提示view
@interface PDPopMessageView : UIView
@property (nonatomic, strong) PDPopViewModel *model;
@end
