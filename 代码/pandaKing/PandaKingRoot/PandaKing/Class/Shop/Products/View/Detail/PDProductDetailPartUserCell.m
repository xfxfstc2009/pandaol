//
//  PDProductDetailPartUserCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProductDetailPartUserCell.h"

#define kSpaceImage_label LCFloat(10)
#define kSpace_label LCFloat(6)

@interface PDProductDetailPartUserCell ()
@property (nonatomic, assign) CGFloat imageTop;
@property (nonatomic, assign) CGFloat imageHeight;

@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *bambooLabel; /**< 投入的竹子*/
@property (nonatomic, strong) UILabel *dateLabel;
@end

@implementation PDProductDetailPartUserCell

+ (CGFloat)cellHeight {
    return LCFloat(50) + 10;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    _imageTop = 5;
    _imageHeight = LCFloat(50.);
}

- (void)pageSetting {
    self.headerImageView = [[PDImageView alloc] init];
    self.headerImageView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.headerImageView.layer.cornerRadius = _imageHeight / 2;
    self.headerImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.headerImageView];
    
    // name
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.nameLabel.textColor = [UIColor blackColor];
    self.nameLabel.numberOfLines = 0;
    [self.contentView addSubview:self.nameLabel];
    
    // 投入竹子
    self.bambooLabel = [[UILabel alloc] init];
    self.bambooLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.bambooLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.bambooLabel];
    
    // 日期
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.dateLabel.textColor = self.bambooLabel.textColor;
    [self.contentView addSubview:self.dateLabel];
}

- (void)setModel:(PDSnatchRecordItem *)model {
    _model = model;
    
    self.nameLabel.text = model.name;
    NSString *portions = [NSString stringWithFormat:@"%ld",model.portionCount];
    
    if (model.help == YES) {
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"参与%@人份(好友帮抢)",portions]];
        [attributeString addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(2, portions.length)];
        self.bambooLabel.attributedText = attributeString;
    } else {
        NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"参与%@人份",portions]];
        [attributeString addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(2, portions.length)];
        self.bambooLabel.attributedText = attributeString;
    }
    
    self.dateLabel.text = [PDCenterTool dateWithFormat:@"MM.dd HH:mm:ss" fromTimestamp:model.stayTime];
    [self.headerImageView uploadImageWithURL:model.avatar placeholder:nil callback:NULL];
    
    // 名字最多只有8个字
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - _imageHeight - kTableViewSectionHeader_left * 2 - kSpaceImage_label - dateSize.width, [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize bambooSize = [self.bambooLabel.text sizeWithCalcFont:self.bambooLabel.font];
    
    self.headerImageView.frame = CGRectMake(kTableViewSectionHeader_left, _imageTop, _imageHeight, _imageHeight);
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + kSpaceImage_label, CGRectGetMinY(self.headerImageView.frame) + 5, nameSize.width, nameSize.height);
    self.bambooLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + kSpace_label, bambooSize.width, bambooSize.height);
    self.dateLabel.frame = CGRectMake(kScreenBounds.size.width - kTableViewSectionHeader_left - dateSize.width, CGRectGetMinY(self.nameLabel.frame), dateSize.width, dateSize.height);
}

@end
