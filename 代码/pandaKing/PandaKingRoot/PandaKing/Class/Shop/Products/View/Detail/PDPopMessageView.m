//
//  PDPopMessageView.m
//  PandaKing
//
//  Created by Cranz on 16/10/10.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDPopMessageView.h"

@interface PDPopMessageView ()
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *textLabel;

@property (nonatomic, assign) CGSize size;
@end

@implementation PDPopMessageView

- (instancetype)init {
    if (self = [super initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width - LCFloat(70), LCFloat(32))]) {
        _size = self.frame.size;
        [self pageSetting];
    }
    return self;
}

- (void)pageSetting {
    self.bgImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    CGFloat top = 10; // 顶端盖高度
    CGFloat bottom = 10; // 底端盖高度
    CGFloat left = 10; // 左端盖宽度
    CGFloat right = 10; // 右端盖宽度
    UIEdgeInsets insets = UIEdgeInsetsMake(top, left, bottom, right);
    UIImage *image = [UIImage imageNamed:@"icon_popview_bg"];
    // 指定为拉伸模式，伸缩后重新赋值
    image = [image resizableImageWithCapInsets:insets resizingMode:UIImageResizingModeStretch];
    self.bgImageView.image = image;
    [self addSubview:self.bgImageView];
    
    CGFloat imageWidth = LCFloat(30);
    self.headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake(LCFloat(12), (_size.height - imageWidth) / 2, imageWidth, imageWidth)];
    self.headerImageView.layer.cornerRadius = imageWidth / 2;
    self.headerImageView.clipsToBounds = YES;
    [self addSubview:self.headerImageView];
    
    self.textLabel = [[UILabel alloc] init];
    self.textLabel.font = [UIFont systemFontOfCustomeSize:13];
    self.textLabel.textColor = c26;
    [self addSubview:self.textLabel];
}

- (void)setModel:(PDPopViewModel *)model {
    _model = model;
    
    NSString *name = @"";
    NSString *text = @"";
    
    if (model.helpNickname.length) { // 此条记录为帮投
        name = model.helpNickname;
        text = [NSString stringWithFormat:@"帮%@投注%@份(%@竹子)",model.nickname, model.portion,model.wager];
    } else {
        name = model.nickname;
        if ([NSString stringWithFormat:@"%@",model.gold].length && model.gold.integerValue) {
            text = [NSString stringWithFormat:@"投入%@金币",model.gold];
        } else {
            text = [NSString stringWithFormat:@"投入%@份(%@竹子)",model.portion,model.wager];
        }
    }

    self.textLabel.text = [NSString stringWithFormat:@"%@ %@",name, text];
    
    CGSize textSize = [self.textLabel.text sizeWithCalcFont:self.textLabel.font constrainedToSize:CGSizeMake(_size.width - LCFloat(30), [NSString contentofHeightWithFont:self.textLabel.font])];
    self.textLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + LCFloat(5), (_size.height - textSize.height) / 2, textSize.width, textSize.height);
    [self.headerImageView uploadImageWithAvatarURL:model.avatar placeholder:nil callback:NULL];
}

@end
