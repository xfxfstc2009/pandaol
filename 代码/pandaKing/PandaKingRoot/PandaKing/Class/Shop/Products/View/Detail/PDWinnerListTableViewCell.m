//
//  PDWinnerListTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDWinnerListTableViewCell.h"

#define kNameFont [UIFont systemFontOfCustomeSize:14]
#define kIPFont   [UIFont systemFontOfCustomeSize:13]
#define kDateFont [UIFont systemFontOfCustomeSize:12]
#define kSpace_label LCFloat(3)
#define kSpaceTop    LCFloat(12) /// label距离cell边的间距
@interface PDWinnerListTableViewCell ()
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UIView *lineLeft;
@property (nonatomic, strong) UIView *lineRight;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *IPLabel; /**< 注意这个改成期号*/
@property (nonatomic, strong) UILabel *IDLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIImageView *dateIcon;
@property (nonatomic, strong) UILabel *numLabel;
@property (nonatomic, strong) UILabel *descLabel;

@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, assign) CGFloat spaceHeader_line; /**< 头像右侧距离线的间距*/
@end

@implementation PDWinnerListTableViewCell

+ (CGFloat)cellHeight {
    CGSize nameSize = [@"1" sizeWithCalcFont:kNameFont];
    CGSize IPSize = [@"1" sizeWithCalcFont:kIPFont];
    CGSize IDSize = [@"1" sizeWithCalcFont:kIPFont];
    CGSize dateSize = [@"1" sizeWithCalcFont:kDateFont];
    return kSpaceTop * 2 + nameSize.height + IPSize.height + IDSize.height + dateSize.height + kSpace_label * 3;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    _rowHeight = [PDWinnerListTableViewCell cellHeight];
    _spaceHeader_line = LCFloat(12);
}

- (void)pageSetting {
    // header
    CGFloat headerTop = kSpaceTop + 5;
    CGFloat headerWidth = _rowHeight - 2 * headerTop;
    self.headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake(kTableViewSectionHeader_left, headerTop, headerWidth, headerWidth)];
    self.headerImageView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.headerImageView.layer.cornerRadius = headerWidth / 2;
    self.headerImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.headerImageView];
    
    // line left
    self.lineLeft = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + _spaceHeader_line, kSpaceTop, .5, _rowHeight - kSpaceTop * 2)];
    self.lineLeft.backgroundColor = c26;
    [self.contentView addSubview:self.lineLeft];
    
    // name
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = kNameFont;
    self.nameLabel.textColor = c26;
    [self.contentView addSubview:self.nameLabel];
    
    // ip ********* 不显示IP地址，变成期号
    self.IPLabel = [[UILabel alloc] init];
    self.IPLabel.font = kIPFont;
    self.IPLabel.textColor = [UIColor lightGrayColor];
    [self.contentView addSubview:self.IPLabel];
    
    // id
    self.IDLabel = [[UILabel alloc] init];
    self.IDLabel.font = kIPFont;
    self.IDLabel.textColor = self.IPLabel.textColor;
    [self.contentView addSubview:self.IDLabel];
    
    // date icon
    self.dateIcon = [[UIImageView alloc] init];
    self.dateIcon.image = [UIImage imageNamed:@"icon_snatch_lastdate"];
    [self.contentView addSubview:self.dateIcon];
    
    // date
    self.dateLabel = [[UILabel alloc] init];
    self.dateLabel.font = kIPFont;
    self.dateLabel.textColor = self.IPLabel.textColor;
    [self.contentView addSubview:self.dateLabel];
    
    // line right
    self.lineRight = [[UIView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width - CGRectGetMaxX(self.lineLeft.frame), CGRectGetMinY(self.lineLeft.frame), CGRectGetWidth(self.lineLeft.frame), CGRectGetHeight(self.lineLeft.frame))];
    self.lineRight.backgroundColor = self.lineLeft.backgroundColor;
    [self.contentView addSubview:self.lineRight];
    
    // num
    self.numLabel = [[UILabel alloc] init];
    self.numLabel.font = [[UIFont systemFontOfCustomeSize:17] boldFont];
    self.numLabel.textColor = c26;
    self.numLabel.adjustsFontSizeToFitWidth = YES;
    [self.contentView addSubview:self.numLabel];
    
    // 投入竹子
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.text = @"购买份数";
    self.descLabel.font = [UIFont systemFontOfCustomeSize:13];
    [self.contentView addSubview:self.descLabel];
}

- (void)setModel:(PDSnatchWinnerDetail *)model {
    _model = model;
    
    
    self.nameLabel.text = model.name;
    self.IPLabel.text = [NSString stringWithFormat:@"夺宝期号：%@",model.num];
    self.IDLabel.text = [NSString stringWithFormat:@"ID：%@",model.ID];
    self.dateLabel.text = [NSString stringWithFormat:@"%@",[PDCenterTool dateWithFormat:@"MM-dd HH:mm:ss" fromTimestamp:model.stayTime]];
    self.numLabel.text = [NSString stringWithFormat:@"%ld",(long)model.winnerStakePortions];
    [self.headerImageView uploadImageWithURL:model.avatar placeholder:nil callback:NULL];
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:kNameFont constrainedToSize:CGSizeMake(CGRectGetMinX(self.lineRight.frame) - CGRectGetMaxX(self.lineLeft.frame) - _spaceHeader_line, [NSString contentofHeightWithFont:self.nameLabel.font])];
    CGSize ipSize = [self.IPLabel.text sizeWithCalcFont:kIPFont];
    CGSize IDSize = [self.IDLabel.text sizeWithCalcFont:kIPFont];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:kIPFont];
    CGSize dateIconSize = CGSizeMake(dateSize.height, dateSize.height);
    CGSize numSize = [self.numLabel.text sizeWithCalcFont:self.numLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(self.lineRight.frame), [NSString contentofHeightWithFont:self.numLabel.font])];
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    CGFloat right = kScreenBounds.size.width - CGRectGetMaxX(self.lineRight.frame); // 右侧留出来的距离
    // 竹子数量上方距离cell边距
    CGFloat numTop = (_rowHeight - numSize.height - descSize.height - 2) / 2;
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.lineLeft.frame) + _spaceHeader_line, kSpaceTop, nameSize.width, nameSize.height);
    self.IPLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + kSpace_label, ipSize.width, ipSize.height);
    self.IDLabel.frame = CGRectMake(CGRectGetMinX(self.IPLabel.frame), CGRectGetMaxY(self.IPLabel.frame) + kSpace_label, IDSize.width, IDSize.height);
    self.dateIcon.frame = CGRectMake(CGRectGetMinX(self.IDLabel.frame), CGRectGetMaxY(self.IDLabel.frame) + kSpace_label, dateIconSize.width, dateIconSize.height);
    self.dateLabel.frame = CGRectMake(CGRectGetMaxX(self.dateIcon.frame) + 5, CGRectGetMinY(self.dateIcon.frame), dateSize.width, dateSize.height);
    self.numLabel.frame = CGRectMake((right - numSize.width) / 2 + CGRectGetMaxX(self.lineRight.frame), numTop, numSize.width, numSize.height);
    self.descLabel.frame = CGRectMake((right - descSize.width) / 2 + CGRectGetMaxX(self.lineRight.frame), CGRectGetMaxY(self.numLabel.frame) + 2, descSize.width, descSize.height);
}

@end
