//
//  PDTitleBar.h
//  Layer.Mask
//
//  Created by Cranz on 16/12/27.
//  Copyright © 2016年 盼达. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface PDTitleBar : UIView<UIScrollViewDelegate>
@property (nonatomic, strong, nullable) UIImage *backgroundImage;
@property (nonatomic, strong) UIScrollView *registScrollView;
@property (nonatomic, copy)   NSString *title;
@property (nonatomic, getter=isProcess) BOOL process; // 默认YES

- (void)leftButtonImage:(UIImage *)image clickComplication:(void(^)(UIButton *))complication;
- (void)rightButtonImage:(UIImage *)image clickComplication:(void(^)(UIButton *))complication;

@end
NS_ASSUME_NONNULL_END
