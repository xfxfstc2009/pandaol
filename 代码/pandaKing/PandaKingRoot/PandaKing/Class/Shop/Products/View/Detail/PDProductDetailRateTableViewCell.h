//
//  PDProductDetailRateTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSnatchActivityDetail.h"

/// 第一个section的cell 显示自己的中奖几率
@interface PDProductDetailRateTableViewCell : UITableViewCell
@property (nonatomic, strong) PDSnatchActivityDetail *model;

+ (CGFloat)cellHeight;
@end
