//
//  CCZProgress.m
//  TEST
//
//  Created by 金峰 on 2016/12/26.
//  Copyright © 2016年 金峰. All rights reserved.
//

#import "CCZProgress.h"

static NSTimeInterval const duration = 0.5;
@interface CCZProgress ()
@property (nonatomic, strong) UIView *progressLayer;
@property (nonatomic, readwrite) CGFloat progress;
@end

@implementation CCZProgress


- (UIView *)progressLayer {
    if (!_progressLayer) {
        _progressLayer = [[UIView alloc] init];
        _progressLayer.backgroundColor = c26;
        [self addSubview:_progressLayer];
    }
    return _progressLayer;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (!self) {
        return nil;
    }
    [self progressSetting];
    return self;
}

- (void)progressSetting {
    self.clipsToBounds = YES;
    self.cornerToClips = YES;
    self.backgroundColor = c27;
    self.progressLayer.frame = CGRectMake(0, 0, 0, self.frame.size.height);
}

#pragma mark - progress

- (void)setProgress:(CGFloat)progress animated:(BOOL)animated {
    if (progress > 1) {
        progress = 1;
    }
    if (progress < 0) {
        progress = 0;
    }
    self.progress = progress;
    [UIView animateWithDuration:animated? duration : 0 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.progressLayer.frame = CGRectMake(0, 0, progress * self.frame.size.width, self.frame.size.height);
    } completion:nil];
}

#pragma mark - set

- (void)setTrackColor:(UIColor *)trackColor {
    _trackColor = trackColor;
    self.backgroundColor = trackColor;
}

- (void)setProgressColor:(UIColor *)progressColor {
    _progressColor = progressColor;
    self.progressLayer.backgroundColor = progressColor;
}

- (void)setCornerToClips:(BOOL)cornerToClips {
    _cornerToClips = cornerToClips;
    if (cornerToClips) {
        self.layer.cornerRadius = self.frame.size.height / 2;
        self.progressLayer.layer.cornerRadius = self.layer.cornerRadius;
        self.progressLayer.layer.masksToBounds = YES;
    } else {
        self.layer.cornerRadius = 0;
        self.progressLayer.layer.cornerRadius = self.layer.cornerRadius;
        self.progressLayer.layer.masksToBounds = NO;
    }
}

@end
