//
//  PDProductCollectionViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSnatchProductItem.h"
#import "PDStoreActivityItem.h"
#import "PDLeftTimerLabel.h"

@interface PDProductCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) PDSnatchProductItem *model;
@property (nonatomic, assign) BOOL needToShowMsec;
@property (nonatomic, strong) PDStoreActivityItem *transferProductSingleModel;
@property (nonatomic, strong) PDLeftTimerLabel *dateLabel;   /**< 倒计时*/

/** item之间的空隙*/
+ (CGFloat)space;
+ (CGSize)cellSize;

-(void)timerFinishBlock:(void(^)())block;

-(void)cleanTimeLabel;
@end
