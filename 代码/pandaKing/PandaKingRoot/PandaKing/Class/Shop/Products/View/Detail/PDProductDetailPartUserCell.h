//
//  PDProductDetailPartUserCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSnatchRecordItem.h"

/// 参与用户
@interface PDProductDetailPartUserCell : UITableViewCell
@property (nonatomic, strong) PDSnatchRecordItem *model;

+ (CGFloat)cellHeight;
@end
