//
//  PDProductDetailPriceWinnerCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSnatchActivityWinnerModel.h"

/// 获奖者cell
@interface PDProductDetailPriceWinnerCell : UITableViewCell
@property (nonatomic, strong) PDSnatchActivityWinnerModel *model;

+ (CGFloat)cellHeight;
@end
