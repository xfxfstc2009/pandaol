//
//  PDProductDetailPriceWinnerCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProductDetailPriceWinnerCell.h"

#define kBgImageTop 4
#define kHeaderImageTop LCFloat(15)
#define kSpaceName_label LCFloat(5)     // 获奖者名字和投入的竹子间距
#define kSpaceBamboo_label LCFloat(16)  // 投入竹子和拥护IP间距
#define kSpaceIP_label  LCFloat(2)      // 用户ip、id、时间的间隙
#define KNameFont   [[UIFont systemFontOfCustomeSize:14] boldFont]
#define kBambooFont [UIFont systemFontOfCustomeSize:13]
#define kIPFont     [UIFont systemFontOfCustomeSize:12]

@interface PDProductDetailPriceWinnerCell ()
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) PDImageView *headerImageView;
@property (nonatomic, strong) UILabel *nameLabel;
@property (nonatomic, strong) UILabel *bambooLabel;
@property (nonatomic, strong) UILabel *userIdLabel;
@property (nonatomic, strong) UILabel *snatchIdLabel; // 夺宝号码
@property (nonatomic, strong) UILabel *dateLabel;

@property (nonatomic, strong) UIColor *textColor;
@property (nonatomic, assign) CGFloat rowHeight;
@property (nonatomic, assign) CGFloat bgImageTop;
@property (nonatomic, assign) CGFloat spaceImage_label; /**< 头像到右侧label的间距*/
@end

@implementation PDProductDetailPriceWinnerCell

+ (CGFloat)cellHeight {
    CGFloat nameHeight = [NSString contentofHeightWithFont:KNameFont];
    CGFloat bambooHeight = [NSString contentofHeightWithFont:kBambooFont];
    CGFloat ipHeight = [NSString contentofHeightWithFont:kIPFont];
    
    return kHeaderImageTop * 2 + nameHeight + kSpaceName_label + bambooHeight + kSpaceBamboo_label + ipHeight * 3 + kSpaceIP_label + kBgImageTop * 2;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    _rowHeight = [PDProductDetailPriceWinnerCell cellHeight];
    _bgImageTop = kBgImageTop;
    _textColor = RGB(95, 57, 23, 1);
    _spaceImage_label = LCFloat(12);
}

- (void)pageSetting {
    // 左侧黑块
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 8, 5, 40)];
    leftView.backgroundColor = [UIColor blackColor];
    [self.contentView addSubview:leftView];
    
    CGFloat bgImageHeight = _rowHeight - _bgImageTop * 2;
    self.bgImageView = [[UIImageView alloc] initWithFrame:CGRectMake(LCFloat(40), _bgImageTop, kScreenBounds.size.width - LCFloat(40) * 2, bgImageHeight)];
    self.bgImageView.image = [UIImage imageNamed:@"icon_snatch_winnerbg"];
    [self.contentView addSubview:self.bgImageView];
    
    // header
    CGFloat header_x = LCFloat(15);
    CGFloat header_width = LCFloat(40);
    self.headerImageView = [[PDImageView alloc] initWithFrame:CGRectMake(header_x, kHeaderImageTop, header_width, header_width)];
    self.headerImageView.layer.cornerRadius = header_width / 2;
    self.headerImageView.clipsToBounds = YES;
    self.headerImageView.backgroundColor = [UIColor redColor];
    [self.bgImageView addSubview:self.headerImageView];
    
    // name
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.font = KNameFont;
    self.nameLabel.textColor = _textColor;
    [self.bgImageView addSubview:self.nameLabel];
    
    // 竹子
    self.bambooLabel = [[UILabel alloc] init];
    self.bambooLabel.font = [kBambooFont boldFont];
    self.bambooLabel.textColor = _textColor;
    [self.bgImageView addSubview:self.bambooLabel];
    
    // ip
    self.snatchIdLabel = [self smallLabel];
    
    // id
    self.userIdLabel = [self smallLabel];
    
    // date
    self.dateLabel = [self smallLabel];
    
}

- (UILabel *)smallLabel {
    UILabel *label = [[UILabel alloc] init];
    label.font = kIPFont;
    label.textColor = _textColor;
    [self.bgImageView addSubview:label];
    return label;
}

- (void)setModel:(PDSnatchActivityWinnerModel *)model {
    _model = model;
    
    self.nameLabel.text = [NSString stringWithFormat:@"获奖者：%@",model.nickname];
    self.bambooLabel.text = [NSString stringWithFormat:@"购买%ld份(%ld竹子)",model.winnerStakePortions,model.winnerStakeWagers];
    self.snatchIdLabel.text = [NSString stringWithFormat:@"夺宝号码:%ld",model.winPortionNum];
    self.userIdLabel.text = [NSString stringWithFormat:@"唯一ID:%@(用户唯一标识)",model.memberId];
    self.dateLabel.text = [NSString stringWithFormat:@"揭晓时间:%@",[PDCenterTool dateWithFormat:@"yyyy.MM.dd hh:mm" fromTimestamp:model.drawTime]];
    [self.headerImageView uploadImageWithAvatarURL:model.avatar placeholder:nil callback:NULL];
    
    CGSize nameSize = [self.nameLabel.text sizeWithCalcFont:self.nameLabel.font];
    CGSize bambooSize = [self.bambooLabel.text sizeWithCalcFont:self.bambooLabel.font];
    CGSize snatchIdSize = [self.snatchIdLabel.text sizeWithCalcFont:self.snatchIdLabel.font];;
    CGSize userIdSize = [self.userIdLabel.text sizeWithCalcFont:self.userIdLabel.font];
    CGSize dateSize = [self.dateLabel.text sizeWithCalcFont:self.dateLabel.font];
    
    self.nameLabel.frame = CGRectMake(CGRectGetMaxX(self.headerImageView.frame) + _spaceImage_label, CGRectGetMinY(self.headerImageView.frame), nameSize.width, nameSize.height);
    self.bambooLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.nameLabel.frame) + kSpaceName_label, bambooSize.width, bambooSize.height);
    self.snatchIdLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.bambooLabel.frame) + kSpaceBamboo_label, snatchIdSize.width, snatchIdSize.height);
    self.userIdLabel.frame = CGRectMake(CGRectGetMinX(self.nameLabel.frame), CGRectGetMaxY(self.snatchIdLabel.frame) + kSpaceIP_label, userIdSize.width, userIdSize.height);
    self.dateLabel.frame = CGRectMake(CGRectGetMinX(self.userIdLabel.frame), CGRectGetMaxY(self.userIdLabel.frame) + kSpaceIP_label, dateSize.width, dateSize.height);
}

@end
