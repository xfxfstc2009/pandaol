//
//  CCZTrotingAttribute.h
//  CCZTrotView
//
//  Created by 金峰 on 16/9/25.
//  Copyright © 2016年 金峰. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCZTrotingAttribute : NSObject
@property (nonatomic, copy)   NSString *text;
@property (nonatomic, strong) NSAttributedString *attribute;
@property (nonatomic, strong) NSDictionary *userInfo; // 做容器使用
@end
