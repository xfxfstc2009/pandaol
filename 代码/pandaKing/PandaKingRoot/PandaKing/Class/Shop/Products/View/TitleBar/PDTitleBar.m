//
//  PDTitleBar.m
//  Layer.Mask
//
//  Created by Cranz on 16/12/27.
//  Copyright © 2016年 盼达. All rights reserved.
//

#import "PDTitleBar.h"

typedef void(^PDButtonBlock)(UIButton *);
@interface PDTitleBar ()
@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, assign) CGSize size;
@property (nonatomic) float location; // 默认200
@property (nonatomic, assign) float buttonWidth; // 宽高相等
@property (nonatomic, strong) UIButton *leftButton;
@property (nonatomic, strong) UIButton *rightButton;
@property (nonatomic, copy)   PDButtonBlock leftBlock;
@property (nonatomic, copy)   PDButtonBlock rightBlock;
@end

@implementation PDTitleBar

- (instancetype)init {
    return [self initWithFrame:CGRectZero];
}

- (instancetype)initWithFrame:(CGRect)frame {
    _size = [UIScreen mainScreen].bounds.size;
    self = [super initWithFrame:CGRectMake(0, 0, _size.width, 64)];
    if (!self) {
        return nil;
    }
    [self barSetting];
    return self;
}

- (void)barSetting {
    self.buttonWidth = 38;
    self.location = 200;
    self.alpha = 0;
    self.backgroundColor = c2;
    self.process = YES;
    
    // 背景
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    [self addSubview:self.backgroundImageView];
    
    self.titleLabel = [[UILabel alloc] init];
    self.titleLabel.font = [UIFont systemFontOfSize:18];
    self.titleLabel.textColor = [UIColor whiteColor];
    [self addSubview:self.titleLabel];
}

- (void)leftButtonImage:(UIImage *)image clickComplication:(void (^)(UIButton * _Nonnull))complication {
    if (!self.leftButton) {
        self.leftButton = [[UIButton alloc] initWithFrame:CGRectMake(11, 20 + (44 - _buttonWidth) / 2, _buttonWidth, _buttonWidth)];
        [self.leftButton addTarget:self action:@selector(didClickLeftButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.leftButton];
    }
    [self.leftButton setImage:image forState:UIControlStateNormal];
    
    if (complication) {
        self.leftBlock = complication;
    }
}

- (void)rightButtonImage:(UIImage *)image clickComplication:(void (^)(UIButton * _Nonnull))complication {
    if (!self.rightButton) {
        self.rightButton = [[UIButton alloc] initWithFrame:CGRectMake(_size.width - 11 - _buttonWidth, 20 + (44 - _buttonWidth) / 2, _buttonWidth, _buttonWidth)];
        [self.rightButton addTarget:self action:@selector(didClickRightButton:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.rightButton];
    }
    [self.rightButton setImage:image forState:UIControlStateNormal];
    
    if (complication) {
        self.rightBlock = complication;
    }
}

#pragma mark - 按钮方法

- (void)didClickLeftButton:(UIButton *)button {
    self.leftBlock(button);
}

- (void)didClickRightButton:(UIButton *)button {
    self.rightBlock(button);
}

#pragma mark - scroll

- (void)scrollWithOffsetY:(CGFloat)y {
    CGFloat offsetY = y;
    // 200为一个节点
    if (self.process) {
        if (offsetY > 0) {
            self.alpha = offsetY / self.location;
        } else {
            self.alpha = 0;
        }
    } else {
        if (offsetY > self.location) {
            self.alpha = 1;
        } else {
            self.alpha = 0;
        }
    }
}

#pragma mark - set

- (void)setTitle:(NSString *)title {
    _title = title;
    
    self.titleLabel.text = title;
    
    CGSize titleSize = [title sizeWithAttributes:@{NSFontAttributeName: self.titleLabel.font}];
    self.titleLabel.frame = CGRectMake((_size.width - titleSize.width) / 2, 20 + (44 - titleSize.height) / 2, titleSize.width, titleSize.height);
}

- (void)setBackgroundImage:(UIImage *)backgroundImage {
    _backgroundImage = backgroundImage;
    
    self.backgroundImageView.image = backgroundImage;
}

- (void)setRegistScrollView:(UIScrollView *)registScrollView {
    _registScrollView = registScrollView;
    
    [registScrollView addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew context:nil];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    UIScrollView *registScrollView = self.registScrollView;
    if ([keyPath isEqualToString:@"contentOffset"] && object == registScrollView) {
        
        CGPoint offset = [change[NSKeyValueChangeNewKey] CGPointValue];
        [self scrollWithOffsetY:offset.y];
    }
}

- (void)dealloc {
    [self removeObserver:self forKeyPath:@"contentOffset"];
}

@end
