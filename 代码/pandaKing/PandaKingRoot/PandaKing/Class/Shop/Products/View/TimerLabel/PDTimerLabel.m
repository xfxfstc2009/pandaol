//
//  PDTimerLabel.m
//  PandaKing
//
//  Created by Cranz on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDTimerLabel.h"

#define kDAY    86400
#define kHOUR   3600
#define kMINUTE 60

typedef void(^resultBlock)(CGSize, NSString *, NSTimeInterval);
@interface PDTimerLabel ()
@property (nonatomic, weak) NSTimer *timer;
@property (nonatomic, copy) resultBlock block;
/**
 * 第一次结束
 */
@property (nonatomic) BOOL isEnded;
@property (nonatomic, assign) NSTimeInterval timestamp; /**< 校准后的未来某一天*/
@end

@implementation PDTimerLabel

- (void)clearTimer {
    [self.timer invalidate];
    self.timer = nil;
}

- (instancetype)init {
    self = [super init];
    if (self && !_timer) {
        _isEnded = NO;
        _timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
        NSRunLoop *currentRunLoop = [NSRunLoop currentRunLoop];
        [currentRunLoop addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return self;
}

- (void)timeChange {
    NSInteger surplusTimeStamp = (NSInteger)(_timestamp - [NSDate date].timeIntervalSince1970);
    
    self.text = [self getTimeDescWithTimeStamp:surplusTimeStamp];
    CGSize size = [self.text sizeWithAttributes:@{NSFontAttributeName: self.font}];
    if (self.block) {
        self.block(size, self.text, surplusTimeStamp);
    }
}

- (NSString *)getTimeDescWithTimeStamp:(NSInteger)timeStamp {

    NSString *timeDesc = @"";
    
    if (timeStamp <= 0) {
        timeDesc = @"已揭晓";
        return timeDesc;
    } else {
        _isEnded = NO;
        
        int days_ = (int)(timeStamp / kDAY);
        int hours_ = (int)(timeStamp % kDAY / kHOUR);
        int minutes_ = (int)((timeStamp % kHOUR) / kMINUTE);
        int seconds_ = (int)(timeStamp % kMINUTE);

        if (timeStamp < kDAY) {
            timeDesc = [NSString stringWithFormat:@"%@%02d:%02d:%02d",self.prefixDesc,hours_,minutes_,seconds_];
        } else {
            timeDesc = [NSString stringWithFormat:@"%@%d天%02d小时%02d分",self.prefixDesc,days_,hours_,minutes_];
        }
        return timeDesc;
    }
}

- (void)countDownWithTimeNow:(NSTimeInterval)n timeEnd:(NSTimeInterval)e resultBlock:(void (^)(CGSize, NSString *, NSTimeInterval))resultBlock {
    if (!resultBlock) {
        return;
    }
    
    if (resultBlock){
        self.block = resultBlock;
    }
    
    _timestamp = e / n * ([NSDate date].timeIntervalSince1970);
    
    self.text = [self getTimeDescWithTimeStamp:(NSInteger)(_timestamp - [NSDate date].timeIntervalSince1970)];
    CGSize size = [self.text sizeWithAttributes:@{NSFontAttributeName: self.font}];
    if (self.block){
        self.block(size, self.text, _timestamp);
    }
}


@end