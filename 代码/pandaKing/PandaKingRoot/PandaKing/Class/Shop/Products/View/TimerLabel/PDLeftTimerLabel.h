//
//  PDLeftTimerLabel.h
//  PandaKing
//
//  Created by Cranz on 16/10/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PDLeftTimerLabel : UILabel
@property (nonatomic, assign) BOOL needToShowMsec/** 需要显示毫秒级*/;

-(void)timerClean;
-(void)timerStart;
/** 传入到未来某一天的剩余时间戳*/
- (void)countDownWithTimestamp:(NSTimeInterval)timestamp resultBlock:(void(^)(CGSize size, NSString *dateString))resultBlock;
/** 传入未来开奖那天的时间戳*/
- (void)countDownWithOpenTime:(NSTimeInterval)timestamp resultBlock:(void(^)(CGSize size, NSString *dateString))resultBlock;


/**
 * 倒计时结束的回调
 */
- (void)countdownFinished:(void(^)())finished;
@end
