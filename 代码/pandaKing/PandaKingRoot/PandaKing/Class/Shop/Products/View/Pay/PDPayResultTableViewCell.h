//
//  PDPayResultTableViewCell.h
//  PandaKing
//
//  Created by Cranz on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSnatchStakeModel.h"

@interface PDPayResultTableViewCell : UITableViewCell
@property (nonatomic, strong) PDSnatchStakeModel *model;

+ (CGFloat)cellHeightWithModel:(PDSnatchStakeModel *)model;
@end
