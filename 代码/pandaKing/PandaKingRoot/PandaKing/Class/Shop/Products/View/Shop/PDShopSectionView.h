//
//  PDShopSectionView.h
//  PandaKing
//
//  Created by Cranz on 16/11/7.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDCycleScrollView.h"

#define kBannerHeight LCFloat(248)
#define KTitleHeight  LCFloat(44)
/// 活动集合视图SectionView
@interface PDShopSectionView : UICollectionReusableView<SDCycleScrollViewDelegate>
@property (nonatomic, strong) PDCycleScrollView *bannerScrollView;
@end
