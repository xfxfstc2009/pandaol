//
//  PDLeftTimerLabel.m
//  PandaKing
//
//  Created by Cranz on 16/10/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDLeftTimerLabel.h"

// 以s * 0.1记
#define kDAY    (kHOUR * 24)
#define kHOUR   (kMINUTE * 60)
#define kMINUTE (kSECOND * 60)
#define kSECOND 1000

typedef void(^resultBlock)(CGSize size, NSString *dateString);
@interface PDLeftTimerLabel ()
@property (nonatomic, weak) NSTimer *timer;
@property (nonatomic, copy) resultBlock block;
@property (nonatomic, assign) NSTimeInterval timestamp; /**< 校准后的未来某一天*/
@property (nonatomic, copy) void(^finishedBlock)();
@end

@implementation PDLeftTimerLabel


-(void)timerClean{
    if (self.timer){
        [self.timer invalidate];
        self.timer = nil;
    }
}

-(void)timerStart{
    if (!self.timer){
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
        NSRunLoop *currentRunLoop = [NSRunLoop currentRunLoop];
        [currentRunLoop addTimer:_timer forMode:NSRunLoopCommonModes];
    }
}

- (instancetype)init {
    self = [super init];
    if (self && !_timer) {
        _timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(timeChange) userInfo:nil repeats:YES];
        NSRunLoop *currentRunLoop = [NSRunLoop currentRunLoop];
        [currentRunLoop addTimer:_timer forMode:NSRunLoopCommonModes];
    }
    return self;
}

- (void)countDownWithTimestamp:(NSTimeInterval)timestamp resultBlock:(void (^)(CGSize, NSString *))resultBlock {
    if (resultBlock) {
        self.block = resultBlock;
    }
    
    _timestamp = timestamp;
    
    self.text = [self getTimeDescWithTimeStamp:(NSInteger)timestamp];
    CGSize size = [self.text sizeWithAttributes:@{NSFontAttributeName: self.font}];
    self.block(size, self.text);
}

- (void)timeChange {
    self.text = [self getTimeDescWithTimeStamp:_timestamp];
    CGSize size = [self.text sizeWithAttributes:@{NSFontAttributeName: self.font}];
    if (self.block) {
        self.block(size, self.text);
    }
    
    _timestamp -= 100;
}

- (NSString *)getTimeDescWithTimeStamp:(NSInteger)timeStamp {

    NSString *timeDesc = @"";
    
    if (timeStamp <= 0) {
        timeDesc = @"已揭晓";
        self.text = timeDesc;
        CGSize size = [timeDesc sizeWithAttributes:@{NSFontAttributeName: self.font}];
        if (self.block) {
            self.block(size, timeDesc);
        }
        if (self.finishedBlock) {
            self.finishedBlock();
        }
        return timeDesc;
    } else {
        int days_ = (int)(timeStamp / kDAY);
        int hours_ = (int)(timeStamp % kDAY / kHOUR);
        int minutes_ = (int)((timeStamp % kHOUR) / kMINUTE);
        int seconds_ = (int)(timeStamp % kMINUTE / kSECOND);
        int ms_ = (int)(timeStamp % kSECOND / 100);
        if (timeStamp > kDAY) {
            timeDesc = [NSString stringWithFormat:@"%d天%02d小时%02d分",days_,hours_,minutes_];
        } else if (timeStamp <= kDAY && timeStamp > kHOUR) {
            timeDesc = [NSString stringWithFormat:@"%02d:%02d:%02d",hours_,minutes_,seconds_];
        } else {
            timeDesc = _needToShowMsec? [NSString stringWithFormat:@"%02d:%02d:%02d",minutes_,seconds_,ms_] : [NSString stringWithFormat:@"%02d:%02d:%02d",hours_,minutes_,seconds_];
        }

        return timeDesc;
    }
}

#pragma mark - 传入未来时间

- (void)countDownWithOpenTime:(NSTimeInterval)timestamp resultBlock:(void (^)(CGSize, NSString *))resultBlock {
    if (resultBlock) {
        self.block = resultBlock;
    }
    
    NSDate *date = [NSDate date];
    _timestamp = timestamp - date.timeIntervalSince1970 * kSECOND;
    
    self.text = [self getTimeDescWithTimeStamp:(NSInteger)timestamp];
    CGSize size = [self.text sizeWithAttributes:@{NSFontAttributeName: self.font}];
    self.block(size, self.text);
}

#pragma mark - 倒计时结束

- (void)countdownFinished:(void (^)())finished {
    if (finished) {
        self.finishedBlock = finished;
    }
}

@end
