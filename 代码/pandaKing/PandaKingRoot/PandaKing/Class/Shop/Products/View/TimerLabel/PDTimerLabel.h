//
//  PDTimerLabel.h
//  PandaKing
//
//  Created by Cranz on 16/9/30.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PDTimerLabel : UILabel
@property (nonatomic, copy) NSString *prefixDesc;// 前缀描述

/** 传入服务器中未来某一天和当前时间，进行校准后再计算倒计时*/
- (void)countDownWithTimeNow:(NSTimeInterval)n timeEnd:(NSTimeInterval)e resultBlock:(void(^)(CGSize size, NSString *dateString, NSTimeInterval t))resultBlock;

- (void)clearTimer;
@end
