//
//  PDProductCollectionViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/19.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProductCollectionViewCell.h"
#import "PDLeftTimerLabel.h"
#import "CCZProgress.h"

static char timerFinishBlockKey;
#define kContentHeight LCFloat(100)

@interface PDProductCollectionViewCell ()
@property (nonatomic, strong) PDImageView *productImageView;
@property (nonatomic, strong) UILabel *descLabel;   /**< 商品描述*/
@property (nonatomic, strong) UILabel *partLabel; // 开奖进度
@property (nonatomic, strong) CCZProgress *progress;

@property (nonatomic, assign) CGSize size;
@property (nonatomic, assign) CGFloat space;
@property (nonatomic, assign) CGFloat progressHeight;
@end

@implementation PDProductCollectionViewCell

+ (CGFloat)space {
    return 5;
}

+ (CGSize)cellSize {
    CGFloat width = kScreenBounds.size.width / 2 - 2 * [PDProductCollectionViewCell space];
    CGSize itemSize = CGSizeMake((kScreenBounds.size.width - LCFloat(35)) / 2, width * 143.5 / 180 + kContentHeight);
    return itemSize;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self basicSetting];
        [self pageSetting];
    }
    return self;
}

- (void)basicSetting {
    _size = [PDProductCollectionViewCell cellSize];
    self.backgroundColor = [UIColor whiteColor];
    _space = 2;
    _progressHeight = LCFloat(8);
}

- (void)pageSetting {
    // 图片
    // 图片距离边框也有1个点的距离
    self.productImageView = [[PDImageView alloc] initWithFrame:CGRectMake(_space, _space, _size.width - _space * 2, _size.height - _space - kContentHeight)];
    self.productImageView.contentMode = UIViewContentModeScaleAspectFill;
    self.productImageView.clipsToBounds = YES;
    [self.contentView addSubview:self.productImageView];
    
    // 描述
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
    self.descLabel.numberOfLines = 2;
    self.descLabel.textAlignment = NSTextAlignmentLeft;
    self.descLabel.textColor = [UIColor blackColor];
    [self.contentView addSubview:self.descLabel];
    
    // 开奖进度
    self.partLabel = [[UILabel alloc] init];
    self.partLabel.font = [UIFont systemFontOfCustomeSize:12];
    self.partLabel.textColor = [UIColor grayColor];
    [self.contentView addSubview:self.partLabel];
    
    // 进度条
    self.progress = [[CCZProgress alloc] initWithFrame:CGRectMake(0, 0, 0, _progressHeight)];
    [self.contentView addSubview:self.progress];
}

-(void)setTransferProductSingleModel:(PDStoreActivityItem *)transferProductSingleModel{
    _transferProductSingleModel = transferProductSingleModel;
    
    // 1.
    [self.productImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithRisqueUrl:transferProductSingleModel.commodity.coverPic] placeholder:nil callback:NULL];
    
    // 2. title
    self.descLabel.text = transferProductSingleModel.commodity.name;
    CGSize contentOfSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.descLabel.font])];
    if (contentOfSize.height >= 2 * [NSString contentofHeightWithFont:self.descLabel.font]){
        self.descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.productImageView.frame) + LCFloat(11), self.size_width - 2 * LCFloat(11), 2 * [NSString contentofHeightWithFont:self.descLabel.font]);
        self.descLabel.numberOfLines = 2;
    } else {
        self.descLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.productImageView.frame) + LCFloat(11), self.size_width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.descLabel.font]);
        self.descLabel.numberOfLines = 1;
    }
    
    // 3. 创建价格
    self.partLabel.font = [[UIFont fontWithCustomerSizeName:@"标题"]boldFont];
    self.partLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    self.partLabel.text = [NSString stringWithFormat:@"%ld 竹子",(long)transferProductSingleModel.discountPrice];
    self.partLabel.frame = CGRectMake(LCFloat(11), CGRectGetMaxY(self.productImageView.frame) + 2 * LCFloat(11) + 2 * [NSString contentofHeightWithFont:self.descLabel.font], self.size_width - 2 * LCFloat(11), [NSString contentofHeightWithFont:self.partLabel.font]);
    
}


- (void)setModel:(PDSnatchProductItem *)model {
    _model = model;
    
    // 开奖进度
    NSString *odds = [NSString stringWithFormat:@"%d％",model.progress];
    NSMutableAttributedString *partAtt = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"开奖进度%@", odds]];
    [partAtt addAttribute:NSForegroundColorAttributeName value:c26 range:NSMakeRange(4, odds.length)];
    self.partLabel.attributedText = [partAtt copy];
    
    self.descLabel.text = model.name;
    [self.productImageView uploadImageWithURL:[PDCenterTool absoluteUrlWithRisqueUrl:model.image] placeholder:nil callback:NULL];
    
    CGSize partSize = [self.partLabel.text sizeWithCalcFont:self.partLabel.font constrainedToSize:CGSizeMake(_size.width, [NSString contentofHeightWithFont:self.partLabel.font])];
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(_size.width - _space * 2, [NSString contentofHeightWithFont:self.descLabel.font] * 2)];
    
    CGFloat dateHeight = [NSString contentofHeightWithFont:[[UIFont systemFontOfCustomeSize:15] boldFont]];
    
    // 进度条和开奖进度之间的间隙
    CGFloat s1 = LCFloat(3);
    
    // 进度条的高度
    CGFloat ph = _progressHeight;
    
    self.descLabel.frame = CGRectMake((_size.width - descSize.width) / 2, CGRectGetMaxY(self.productImageView.frame) + LCFloat(2), descSize.width, descSize.height);

    // 时间和开奖进度 == 开奖进度和名称的间隔
    CGFloat space = (_size.height - CGRectGetMinY(self.descLabel.frame) - [NSString contentofHeightWithFont:self.descLabel.font] * 2 - dateHeight - (partSize.height + s1 + ph)) / 3;
    
    self.partLabel.frame = CGRectMake((_size.width - partSize.width) / 2, _size.height - space * 2 - dateHeight - partSize.height, partSize.width, partSize.height);

    self.progress.frame = CGRectMake(_space + LCFloat(2), CGRectGetMinY(self.partLabel.frame) - s1 - ph, _size.width - (_space + LCFloat(2)) * 2, ph);
    
    [self.progress setProgress:model.progress / 100. animated:NO];
    
     __weak typeof(self)weakSelf = self;
    if (!self.dateLabel){
        // 时间
        self.dateLabel = [[PDLeftTimerLabel alloc] init];
        self.dateLabel.font = [[UIFont systemFontOfCustomeSize:15] boldFont];
        self.dateLabel.textColor = [UIColor redColor];
        [self.contentView addSubview:self.dateLabel];
        
        [self.dateLabel countdownFinished:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)() = objc_getAssociatedObject(strongSelf, &timerFinishBlockKey);
            if (block){
                block();
            }
        }];
        self.dateLabel.needToShowMsec = self.needToShowMsec;
        
    }
    
    [self.dateLabel countDownWithOpenTime:model.openTime resultBlock:^(CGSize size, NSString *dateString) {
        if (!weakSelf.dateLabel){
            return;
        }
        
        CGSize dateSize = [weakSelf.dateLabel.text sizeWithCalcFont:weakSelf.dateLabel.font constrainedToSize:CGSizeMake(_size.width, [NSString contentofHeightWithFont:weakSelf.dateLabel.font])];
        weakSelf.dateLabel.frame = CGRectMake((_size.width - dateSize.width) / 2, weakSelf.size.height - dateSize.height - space, dateSize.width, dateSize.height);
    }];
}

-(void)timerFinishBlock:(void(^)())block{
    objc_setAssociatedObject(self, &timerFinishBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)cleanTimeLabel{
    [self.dateLabel timerClean];
    [self.dateLabel removeFromSuperview];
    self.dateLabel = nil;
}
@end
