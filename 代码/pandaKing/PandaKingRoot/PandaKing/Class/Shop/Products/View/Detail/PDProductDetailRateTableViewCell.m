//
//  PDProductDetailRateTableViewCell.m
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDProductDetailRateTableViewCell.h"
#import "CCZProgress.h"

#define kSpace 6
#define kSpaceLabel_progress 3
#define kFont [UIFont systemFontOfCustomeSize:13]
#define kBgImageHeight LCFloat(33)
#define kProgressHeight LCFloat(8)
@interface PDProductDetailRateTableViewCell ()
@property (nonatomic, strong) UIImageView *bgImageView;
@property (nonatomic, strong) UILabel *descLabel; // 1人份 = 10竹子
@property (nonatomic, strong) UILabel *totalLabel; // 总份数
@property (nonatomic, strong) UILabel *leftLabel; // 剩余份数
@property (nonatomic, strong) UILabel *inputLabel;  //我的投入
@property (nonatomic, strong) CCZProgress *progress;
@end

@implementation PDProductDetailRateTableViewCell

+ (CGFloat)cellHeight {
    return [NSString contentofHeightWithFont:kFont] * 2 + kSpace * 3 + kSpaceLabel_progress * 2 + kBgImageHeight + kProgressHeight;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [self pagesetting];
    }
    return self;
}

- (void)pagesetting {
    // 背景黑条
    self.bgImageView = [[UIImageView alloc] init];
    self.bgImageView.image = [UIImage imageNamed:@"icon_snatch_bg"];
    [self.contentView addSubview:self.bgImageView];
    
    // 1人份 = 10竹子
    self.descLabel = [[UILabel alloc] init];
    self.descLabel.font = kFont;
    self.descLabel.textColor = c26;
    [self.contentView addSubview:self.descLabel];
    
    // 总需499人份
    self.totalLabel = [[UILabel alloc] init];
    self.totalLabel.font = self.descLabel.font;
    self.totalLabel.textColor = [UIColor grayColor];
    [self.contentView addSubview:self.totalLabel];
    
    //  还需277人份
    self.leftLabel = [[UILabel alloc] init];
    self.leftLabel.font = self.descLabel.font;
    self.leftLabel.textColor = self.descLabel.textColor;
    [self.contentView addSubview:self.leftLabel];

    // 我的投入 和 0.01%中奖率
    self.inputLabel = [[UILabel alloc] init];
    self.inputLabel.font = [UIFont systemFontOfCustomeSize:14];
    self.inputLabel.textColor = c26;
    [self.bgImageView addSubview:self.inputLabel];
    
    // 进度条
    self.progress = [[CCZProgress alloc] initWithFrame:CGRectMake(0, 0, 0, kProgressHeight)];
    [self.contentView addSubview:self.progress];
}

- (void)setModel:(PDSnatchActivityDetail *)model {
    _model = model;
    
    NSString *odds = [NSString stringWithFormat:@"%.2f％",floorf(model.odds.floatValue * 100) / 100];
    self.descLabel.text = [NSString stringWithFormat:@"1人份=%ld竹子",(long)model.wagersPerPortion];
    self.totalLabel.text = [NSString stringWithFormat:@"总需%ld人份",model.maxPortion];
    self.leftLabel.text = [NSString stringWithFormat:@"还需%ld人份",model.maxPortion - model.totalPortion];
    self.inputLabel.text = [NSString stringWithFormat:@"您已投入%ld人份 中奖率%@",model.memberStakeCount,odds];
    
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font];
    CGSize totalSize = [self.totalLabel.text sizeWithCalcFont:self.totalLabel.font];
    CGSize leftSize = [self.leftLabel.text sizeWithCalcFont:self.leftLabel.font];
    CGSize inputSize = [self.inputLabel.text sizeWithCalcFont:self.inputLabel.font];
    
    self.descLabel.frame = CGRectMake(LCFloat(20), kSpace, descSize.width, descSize.height);
    self.progress.frame = CGRectMake(CGRectGetMinX(self.descLabel.frame), CGRectGetMaxY(self.descLabel.frame) + kSpaceLabel_progress, kScreenBounds.size.width - LCFloat(20) * 2, kProgressHeight);
    self.totalLabel.frame = CGRectMake(CGRectGetMinX(self.descLabel.frame), CGRectGetMaxY(self.progress.frame) + kSpaceLabel_progress, totalSize.width, totalSize.height);
    self.leftLabel.frame = CGRectMake(kScreenBounds.size.width - LCFloat(20) - leftSize.width, CGRectGetMinY(self.totalLabel.frame), leftSize.width, leftSize.height);
    self.bgImageView.frame = CGRectMake(CGRectGetMinX(self.descLabel.frame), CGRectGetMaxY(self.totalLabel.frame) + kSpace, kScreenBounds.size.width - LCFloat(20) * 2, kBgImageHeight);
    self.inputLabel.frame = CGRectMake((CGRectGetWidth(self.bgImageView.frame) - inputSize.width) / 2, (kBgImageHeight - inputSize.height) / 2, inputSize.width, inputSize.height);
    
    // 先设置progress的frame
    float floatPortion = (float)model.totalPortion / model.maxPortion;
    if (model.maxPortion == 0) {
        [self.progress setProgress:0 animated:YES];
    } else {
        [self.progress setProgress:floatPortion animated:YES];
    }
}

@end
