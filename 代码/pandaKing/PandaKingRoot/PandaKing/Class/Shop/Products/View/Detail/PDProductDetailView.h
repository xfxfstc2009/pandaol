//
//  PDProductDetailView.h
//  PandaKing
//
//  Created by Cranz on 16/9/20.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PDSnatchActivityDetail.h"

@interface PDProductDetailView : UIView
@property (nonatomic, strong) PDSnatchActivityDetail *model;

+ (CGFloat)viewHeightWithModel:(PDSnatchActivityDetail *)model;
/**
 * 倒计时结束时的回调
 */
- (void)countdownToEndComplication:(void(^)())complication;

- (void)clearTimer;
@end
