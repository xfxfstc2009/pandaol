//
//  PDOrderProductDetailModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDOrderProductDetailModel : FetchModel

@property (nonatomic,copy)NSString *img;
@property (nonatomic,copy)NSString *product;

@end
