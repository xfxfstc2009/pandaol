//
//  MMHCustomKeyboard.m
//  MamHao
//
//  Created by SmartMin on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHCustomKeyboard.h"
#import "ZCAudioTool.h"
@interface MMHCustomKeyboard()
@property (nonatomic,strong)NSMutableArray *allNumberButtons;
@end

@implementation MMHCustomKeyboard

#pragma mark arrayWithInit
-(NSMutableArray *)allNumberButtons{
    if (_allNumberButtons == nil){
        _allNumberButtons = [NSMutableArray array];
    }
    return _allNumberButtons;
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        self.backgroundColor = [UIColor whiteColor];
        //1. 键盘添加按钮
        [self createKeyboardButtons];
    }
    return self;
}

#pragma mark create KeyBoard Buttons
-(void)createKeyboardButtons{
    for (int i = 0; i < 12;i++){
        // 创建按钮
        UIButton *singleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self addSubview:singleButton];
        [singleButton addTarget:self action:@selector(playTock) forControlEvents:UIControlEventTouchDown];
        [singleButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [singleButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        [singleButton setBackgroundImage:[UIImage imageNamed:@"number_bg"] forState:UIControlStateNormal];
        
        if (i == 9){                // 隐藏按钮
            [singleButton setTitle:@"隐藏" forState:UIControlStateNormal];
            singleButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
            [singleButton addTarget:self action:@selector(hidenButtonClick) forControlEvents:UIControlEventTouchUpInside];
            
        } else if (i == 10){        // 0
            [singleButton setTitle:@"0" forState:UIControlStateNormal];
            singleButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:kScreenBounds.size.width * 0.06875];
            singleButton.tag = 0;
            [singleButton addTarget:self action:@selector(numberButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [self.allNumberButtons addObject:singleButton];
        } else if (i == 11) {  // 删除按钮
            [singleButton setTitle:@"删除" forState:UIControlStateNormal];
            singleButton.titleLabel.font = [UIFont systemFontOfSize:kScreenBounds.size.width * 0.046875];
            [singleButton addTarget:self action:@selector(deleteBtnClick) forControlEvents:UIControlEventTouchUpInside];
        } else {
            [singleButton setTitle:[NSString stringWithFormat:@"%d",i + 1] forState:UIControlStateNormal];
            singleButton.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:kScreenBounds.size.width * 0.06875];
            singleButton.tag = i + 1;
            [singleButton addTarget:self action:@selector(numberButtonClick:) forControlEvents:UIControlEventTouchUpInside];
            [self.allNumberButtons addObject:singleButton];
        }
    }
}

#pragma mark - Layout
-(void)layoutSubviews{
    [super layoutSubviews];
    // 定义总列数
    NSInteger totalCol = 3;
    
    // 定义间距
    CGFloat pad = kScreenBounds.size.width * 0.015625;
    
    // 定义x y w h
    CGFloat x;
    CGFloat y;
    CGFloat w = kScreenBounds.size.width * 0.3125;
    CGFloat h = kScreenBounds.size.width * 0.14375;
    
    // 列数 行数
    NSInteger row;
    NSInteger col;
    for (int i = 0; i < 12; i++) {
        row = i / totalCol;
        col = i % totalCol;
        x = pad + col * (w + pad);
        y = row * (h + pad) + pad;
        UIButton *btn = self.subviews[i];
        btn.frame = CGRectMake(x, y, w, h);
    }
}



#pragma mark -actionClick
-(void)deleteButtonClick{               //  删除按钮
    if ([self.keyboardDelegate respondsToSelector:@selector(customKeyboardDeleteButtonClick)]){
        [self.keyboardDelegate customKeyboardDeleteButtonClick];
        [[NSNotificationCenter defaultCenter]postNotificationName:keyboardDeleteButtonClick object:self];
    }
}

-(void)hidenButtonClick {                // 隐藏按钮
    if ([self.keyboardDelegate respondsToSelector:@selector(customKeyboardConfirmButtonClick)]){
        [self.keyboardDelegate customKeyboardConfirmButtonClick];
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:keyboardConfirmButtonClick object:self];
}

-(void)numberButtonClick:(UIButton *)sender {
    UIButton *numberButton = (UIButton *)sender;
    if ([self.keyboardDelegate respondsToSelector:@selector(customKayboard:numberButtonClick:)]){
        [self.keyboardDelegate customKayboard:self numberButtonClick:numberButton.tag];
    }
    NSMutableDictionary *userInfoDic = [NSMutableDictionary dictionary];
    userInfoDic[keyboardNumbersKey] = @(numberButton.tag);
    [[NSNotificationCenter defaultCenter] postNotificationName:keyboardNumbersButtonClick object:self userInfo:userInfoDic];
}

/** 删除按钮点击 */
- (void)deleteBtnClick {
    if ([self.keyboardDelegate respondsToSelector:@selector(customKeyboardConfirmButtonClick)]) {
        [self.keyboardDelegate customKeyboardConfirmButtonClick];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:keyboardDeleteButtonClick object:self];
}

/** 确定按钮点击 */
- (void)okBtnClick {
    if ([self.keyboardDelegate respondsToSelector:@selector(customKeyboardConfirmButtonClick)]) {
        [self.keyboardDelegate customKeyboardConfirmButtonClick];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:keyboardConfirmButtonClick object:self];
}

-(void)playTock{
    ZCAudioTool *audioTool = [[ZCAudioTool alloc] initSystemSoundWithName:@"Tock" SoundType:@"caf"];
    [audioTool play];
}

@end
