//
//  MMHCustomKeyboard.h
//  MamHao
//
//  Created by SmartMin on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//
// 自定义键盘
#import <UIKit/UIKit.h>

static NSString *keyboardDeleteButtonClick = @"keyboardDeleteButtonClick";
static NSString *keyboardConfirmButtonClick = @"keyboardConfirmButtonClick";
static NSString *keyboardNumbersButtonClick = @"keyboardNumbersButtonClick";
static NSString *keyboardNumbersKey = @"keyboardNumbersKey";


@class MMHCustomKeyboard;
@protocol MMHCustomKeyboardDelegate <NSObject>

@optional
-(void)customKayboard:(MMHCustomKeyboard *)keyboard numberButtonClick:(NSInteger)number;            // 数字按钮点击
-(void)customKeyboardDeleteButtonClick;                                                             // 删除按钮点击
-(void)customKeyboardConfirmButtonClick;                                                            // 确认按钮点击

@end

@interface MMHCustomKeyboard : UIView
@property (nonatomic,assign)id<MMHCustomKeyboardDelegate>keyboardDelegate;                          // 键盘代理
@end
