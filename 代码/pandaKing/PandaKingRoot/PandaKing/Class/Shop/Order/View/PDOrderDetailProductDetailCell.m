//
//  PDOrderDetailProductDetailCell.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDOrderDetailProductDetailCell.h"

@interface PDOrderDetailProductDetailCell()
@property (nonatomic,strong)PDImageView *proImageView;              /**< 商品头像*/
@property (nonatomic,strong)UILabel *proNameLabel;                  /**< 商品名称*/
@property (nonatomic,strong)UILabel *priceLabel;                    /**< 商品价格*/
@property (nonatomic,strong)UILabel *numberLabel;                   /**< 商品数量*/

@end

@implementation PDOrderDetailProductDetailCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

-(void)createView{
    // 1. 创建商品图片
    self.proImageView = [[PDImageView alloc]init];
    self.proImageView.backgroundColor = [UIColor clearColor];
    self.proImageView.frame = CGRectMake(LCFloat(11), LCFloat(11), LCFloat(80), LCFloat(80));
    [self addSubview:self.proImageView];
    
    // 2. 创建商品名称
    self.proNameLabel = [[UILabel alloc]init];
    self.proNameLabel.backgroundColor = [UIColor clearColor];
    self.proNameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.proNameLabel];
    
    // 3.创建商品价格
    self.priceLabel = [[UILabel alloc]init];
    self.priceLabel.backgroundColor = [UIColor clearColor];
    self.priceLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.priceLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.priceLabel];
    
    //4.创建商品数量
    self.numberLabel = [[UILabel alloc]init];
    self.numberLabel.backgroundColor = [UIColor clearColor];
    self.numberLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.numberLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.numberLabel];
}

-(void)setTransferProductDetailModel:(PDOrderProductDetailModel *)transferProductDetailModel{
    _transferProductDetailModel = transferProductDetailModel;
    
    //
    
    
}





@end
