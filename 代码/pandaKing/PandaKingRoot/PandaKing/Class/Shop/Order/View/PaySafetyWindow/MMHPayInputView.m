//
//  MMHPayInputView.m
//  MamHao
//
//  Created by SmartMin on 15/6/12.
//  Copyright (c) 2015年 Mamhao. All rights reserved.
//

#import "MMHPayInputView.h"
#import "MMHCustomKeyboard.h"
@interface MMHPayInputView()
@property (nonatomic,strong)UIButton *confirmButton;            /**< 确认按钮*/
@property (nonatomic,strong)UIButton *cancelButton;             /**< 取消按钮*/
@property (nonatomic,strong)NSMutableArray *numberArr;          /**< 存放数字的数组*/
@property (nonatomic,strong)UILabel *subTtleLabel;
@property (nonatomic,strong)UIView *lineView;
@property (nonatomic,strong)UILabel *fixedResetLabel;           /**< 忘记密码*/
@property (nonatomic,strong)UIButton *findPwdButton;            /**< 寻找密码*/
@end

@implementation MMHPayInputView

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        self.backgroundColor = [UIColor clearColor];
        [self createKeyboardNote];                              // 注册键盘通知
        [self createCustomControl];                             // 1. 创建控件
    }
    return self;
}


-(void)createKeyboardNote{
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardDelete) name:keyboardDeleteButtonClick object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardConfirm) name:keyboardConfirmButtonClick object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardNumbers:) name:keyboardNumbersButtonClick object:nil];
}

#pragma mark 创建控件
-(void)createCustomControl{
    UIButton *confirmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:confirmButton];
    self.confirmButton = confirmButton;
    [self.confirmButton setImage:[UIImage imageNamed:@"payment_btn_04"] forState:UIControlStateNormal];
    [self.confirmButton setImage:[UIImage imageNamed:@"payment_btn_05"] forState:UIControlStateHighlighted];
    [self.confirmButton addTarget:self action:@selector(confirmButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self addSubview:cancelButton];
    self.cancelButton = cancelButton;
    [self.cancelButton setImage:[UIImage imageNamed:@"payment_btn_01"] forState:UIControlStateNormal];
    [self.cancelButton setImage:[UIImage imageNamed:@"payment_btn_02"] forState:UIControlStateHighlighted];
    [self.cancelButton addTarget:self action:@selector(cancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    
    // subTitle
    self.subTtleLabel = [[UILabel alloc]init];
    [self addSubview:self.subTtleLabel];
    
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = c3;
    [self addSubview:self.lineView];
    
    // findLabel
    self.fixedResetLabel = [[UILabel alloc]init];
    self.fixedResetLabel.text = @"忘记密码？";
    self.fixedResetLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.fixedResetLabel.textColor =c4;
    [self addSubview:self.fixedResetLabel];
    
    // 找回密码
    self.findPwdButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.findPwdButton setTitle:@"找回密码" forState:UIControlStateNormal];
    [self.findPwdButton setTitleColor:c2 forState:UIControlStateNormal];
    [self.findPwdButton addTarget:self action:@selector(findPwdButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.findPwdButton.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.findPwdButton];
}

#pragma mark arrayWithInit
-(NSMutableArray *)numberArr{
    if (_numberArr == nil){
        _numberArr = [NSMutableArray array];
    }
    return _numberArr;
}


#pragma mark - actionClick
#pragma mark 确认按钮
-(void)confirmButtonClick:(UIButton *)sender{
    UIButton *confirmButton = (UIButton *)sender;
    if ([self.payInputDelegate respondsToSelector:@selector(payinputView:confirmButton:)]){
        [self.payInputDelegate payinputView:self confirmButton:confirmButton];
    }
    NSMutableString *passcodeString = [NSMutableString string];
    for (int i = 0 ; i < self.numberArr.count; i ++){
        NSString *tempStr = [NSString stringWithFormat:@"%@",[self.numberArr objectAtIndex:i]];
        [passcodeString appendString:tempStr];
    }
    NSMutableDictionary *tempDic = [NSMutableDictionary dictionary];
    tempDic[payInputViewPassCodeKey] = passcodeString;
    [[NSNotificationCenter defaultCenter]postNotificationName:payInputViewConfirmButtonClick object:self userInfo:tempDic];
}

#pragma mark 取消按钮
-(void)cancelButtonClick:(UIButton *)sender{
    UIButton *cancelButton = (UIButton *)sender;
    if ([self.payInputDelegate respondsToSelector:@selector(payinputView:cancelButton:)]){
        [self.payInputDelegate payinputView:self cancelButton:cancelButton];
    }
    [[NSNotificationCenter defaultCenter]postNotificationName:payInputViewCancelButtonClick object:self];
}

#pragma mark 删除按钮
-(void)keyboardDelete{
    [self.numberArr removeLastObject];
    [self setNeedsDisplay];
}

#pragma mark 数字按钮
-(void)keyboardNumbers:(NSNotification *)note{
    if (self.numberArr.count >= kInputNumberCount){
        return;
    }
    NSDictionary *userInfoDic = note.userInfo;
    NSNumber *numberObj = userInfoDic[keyboardNumbersKey];
    [self.numberArr addObject:numberObj];
    [self setNeedsDisplay];
}

#pragma mark 确定按钮
-(void)keyboardConfirm{
    NSLog(@"确定");
}

#pragma mark 找回密码
-(void)findPwdButtonClick:(UIButton *)sender{
    self.findMyPwdBlock();
}

#pragma mark Layout
-(void)layoutSubviews{
    [super layoutSubviews];
    
    if (self.subTtleLabel){
        self.subTtleLabel.text = @"您正在使用妈妈好虚拟资产，为了保证您账户安全，请输入妈妈好支付密码";
        self.subTtleLabel.font = [UIFont fontWithCustomerSizeName:@"正文"];
        self.subTtleLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
        CGSize size = [self.subTtleLabel.text sizeWithCalcFont:self.subTtleLabel.font constrainedToSize:CGSizeMake(self.bounds.size.width - 2 * LCFloat(20), CGFLOAT_MAX)];
        self.subTtleLabel.numberOfLines = 0;
        self.subTtleLabel.frame= CGRectMake(LCFloat(20), (LCFloat(77) - size.height) / 2., self.bounds.size.width - 2 * LCFloat(20), size.height);
    }
    
    // lineView
    if (self.lineView){
        self.lineView.frame = CGRectMake(self.subTtleLabel.frame.origin.x, CGRectGetMaxY(self.subTtleLabel.frame) + self.subTtleLabel.frame.origin.y, self.subTtleLabel.bounds.size.width, .5f);
        [self addSubview:self.lineView];
    }
    
    UIImage *field = [UIImage imageNamed:@"password_in"];
    
    /** 取消按钮 */
    self.cancelButton.size_width = (self.bounds.size.width - 2 * LCFloat(20) - LCFloat(15)) / 2.;
    self.cancelButton.size_height = kScreenBounds.size.width * 0.128125;
    self.cancelButton.orgin_x = LCFloat(20);
    self.cancelButton.orgin_y = CGRectGetMaxY(self.lineView.frame) + 2 * LCFloat(15) + field.size.height;
    self.cancelButton.stringTag = @"cancelButton";
    
    /** 确定按钮 */
    self.confirmButton.orgin_y = self.cancelButton.orgin_y;
    self.confirmButton.size_width = self.cancelButton.size_width;
    self.confirmButton.size_height = self.cancelButton.size_height;
    self.confirmButton.orgin_x = (self.bounds.size.width - self.cancelButton.size_width - LCFloat(20));
    self.confirmButton.stringTag = @"confirmButton";
    
    // 忘记密码
    CGSize resetLabelSize = [self.fixedResetLabel.text sizeWithCalcFont:self.fixedResetLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]])];
    
    // 找回密码

    CGSize findPwdLabelSize = [@"找回密码" sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]])];
    
    CGFloat originX = (self.bounds.size.width - (resetLabelSize.width + findPwdLabelSize.width + 2 * LCFloat(5))) / 2.;
    
    self.fixedResetLabel.frame = CGRectMake(originX, CGRectGetMaxY(self.cancelButton.frame) + LCFloat(15), resetLabelSize.width, [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"小正文"]]);
    
    self.findPwdButton.frame = CGRectMake(CGRectGetMaxX(self.fixedResetLabel.frame), self.fixedResetLabel.frame.origin.y, findPwdLabelSize.width + 2 * LCFloat(10), self.fixedResetLabel.bounds.size.height);
}

#pragma mark 画图
-(void)drawRect:(CGRect)rect{
    // 画图
    UIImage *bg = [UIImage imageNamed:@"payment_bg_f0f0f0"];
    UIImage *field = [UIImage imageNamed:@"password_in"];
    
    [bg drawInRect:rect];
    
    CGFloat x = LCFloat(20);
    CGFloat y = CGRectGetMaxY(self.lineView.frame) + LCFloat(15);
    CGFloat w = self.bounds.size.width - 2 * LCFloat(20);
    CGFloat h = kScreenBounds.size.width * 0.121875;
    [field drawInRect:CGRectMake(x, y, w, h)];
    
    // 画点
    UIImage *pointImage = [UIImage imageNamed:@"password_pic_point"];
    CGFloat pointW = kScreenBounds.size.width * 0.05;
    CGFloat pointH = pointW;
    CGFloat pointY = (field.size.height - pointW)/2. + y;
    CGFloat pointX;
    CGFloat margin = kScreenBounds.size.width * 0.0484375;
    CGFloat padding = kScreenBounds.size.width * 0.045578125;
    for (int i = 0; i < self.numberArr.count; i++) {
        pointX = margin + padding + i * (pointW + 2 * padding);
        [pointImage drawInRect:CGRectMake(pointX, pointY, pointW, pointH)];
    }
    
    // ok按钮状态
    BOOL statue = NO;
    if (self.numberArr.count == kInputNumberCount) {
        statue = YES;
    } else {
        statue = NO;
    }
    self.confirmButton.enabled = statue;
}



@end
