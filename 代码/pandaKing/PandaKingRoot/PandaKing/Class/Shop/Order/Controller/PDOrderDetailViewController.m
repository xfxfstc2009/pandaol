//
//  PDOrderDetailViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/3.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDOrderDetailViewController.h"

@interface PDOrderDetailViewController()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)UITableView *orderTableView;
@property (nonatomic,strong)NSArray *orderArr;
@end


@implementation PDOrderDetailViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

-(void)viewDidLoad{
    [super viewDidLoad];
    
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.orderArr = @[@[@"商品详情",@"订单信息"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if(!self.orderTableView){
        self.orderTableView = [[UITableView alloc]initWithFrame:kScreenBounds style:UITableViewStylePlain];
        self.orderTableView.autoresizingMask = UIViewAutoresizingFlexibleHeight |UIViewAutoresizingFlexibleWidth;
        self.orderTableView.delegate = self;
        self.orderTableView.dataSource = self;
        self.orderTableView.backgroundColor = [UIColor clearColor];
        self.orderTableView.showsVerticalScrollIndicator = NO;
        self.orderTableView.scrollEnabled = YES;
        self.orderTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        [self.view addSubview:self.orderTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.orderArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.orderArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    UITableViewCell *cellWithRowOne =[tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if(!cellWithRowOne){
        cellWithRowOne = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return LCFloat(20);
}


@end
