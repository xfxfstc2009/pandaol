//
//  PDShopRootViewController.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

/// 夺宝列表
@interface PDShopRootViewController : AbstractViewController

+(instancetype)sharedController;

@end
