//
//  PDCouponView.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 优惠券view
#import <UIKit/UIKit.h>
#import "PDCouponSingleModel.h"
@interface PDCouponView : PDImageView

-(instancetype)initWithFrame:(CGRect)frame couponModel:(PDCouponSingleModel *)model;

@end
