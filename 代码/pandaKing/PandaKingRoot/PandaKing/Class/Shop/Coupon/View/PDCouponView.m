//
//  PDCouponView.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDCouponView.h"

@interface PDCouponView()
@property (nonatomic,strong)PDCouponSingleModel *couponSingleModel;         /**< 传递的优惠券model*/

@end

@implementation PDCouponView

-(instancetype)initWithFrame:(CGRect)frame couponModel:(PDCouponSingleModel *)model{
    self = [super initWithFrame:frame];
    if (self){
        _couponSingleModel = model;
        self.size_height = LCFloat(74);
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.backgroundColor = [UIColor clearColor];
    self.image = [UIImage imageNamed:@"icon_coupon_single"];
    
    // 2. 创建label
    UILabel *nameLabel = [[UILabel alloc]init];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = self.couponSingleModel.couponName;
    CGSize nameSize = [nameLabel.text sizeWithCalcFont:nameLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:nameLabel.font])];
    nameLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    nameLabel.frame = CGRectMake(LCFloat(11), LCFloat(13), nameSize.width, [NSString contentofHeightWithFont:nameLabel.font]);
    [self addSubview:nameLabel];
    
    // 3. 创建otherLabel
    UILabel *otherLabel = [[UILabel alloc]init];
    otherLabel.backgroundColor = [UIColor clearColor];
    otherLabel.font = [UIFont systemFontOfCustomeSize:8.];
    otherLabel.text = self.couponSingleModel.other;
    otherLabel.frame = CGRectMake(LCFloat(11), self.size_height - LCFloat(9) - [NSString contentofHeightWithFont:otherLabel.font], self.size_width - 2 * LCFloat(11), [NSString contentofHeightWithFont:otherLabel.font]);
    [self addSubview:otherLabel];
    otherLabel.textColor = c3;
    
    // 4. 创建label
    UILabel *yLabel = [[UILabel alloc]init];
    yLabel.backgroundColor = [UIColor clearColor];
    yLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    yLabel.text = @"￥";
    CGSize ySize = [yLabel.text sizeWithCalcFont:yLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:yLabel.font])];
    CGFloat origin_x = self.size_width - LCFloat(14) - ySize.width;
    CGFloat origin_y = LCFloat(11);
    yLabel.frame = CGRectMake(origin_x, origin_y, ySize.width, [NSString contentofHeightWithFont:yLabel.font]);
    [self addSubview:yLabel];
    
    // 5. 创建priceLabel
    UILabel *faceValueLabel = [[UILabel alloc]init];
    faceValueLabel.backgroundColor = [UIColor clearColor];
    faceValueLabel.text = self.couponSingleModel.faceValue;
    faceValueLabel.font = [UIFont HelveticaNeueBoldFontSize:24.];
    CGSize faceValueSize = [faceValueLabel.text sizeWithCalcFont:faceValueLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:faceValueLabel.font])];
    faceValueLabel.frame = CGRectMake(yLabel.orgin_x - LCFloat(4) - faceValueSize.width, LCFloat(11), faceValueSize.width, [NSString contentofHeightWithFont:faceValueLabel.font]);
    [self addSubview:faceValueLabel];
    
    // 4.1 重改羊角符frame
    yLabel.orgin_y = CGRectGetMaxY(faceValueLabel.frame) - [NSString contentofHeightWithFont:yLabel.font];
    
    self.size_height = LCFloat(13) + nameLabel.size_height + LCFloat(31) + otherLabel.size_height + LCFloat(9);
}

@end
