//
//  PDCouponSingleModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/8/17.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 优惠券信息
#import "FetchModel.h"

@interface PDCouponSingleModel : FetchModel

@property (nonatomic,copy)NSString *couponName;         /**< 优惠名称*/
@property (nonatomic,copy)NSString *faceValue;          /**< 面值*/
@property (nonatomic,copy)NSString *other;              /**< 其他信息*/
@end
