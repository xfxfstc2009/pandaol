//
//  PDShopRootViewController.m
//  PandaKing
//
//  Created by GiganticWhale on 16/8/29.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDShopRootViewController.h"
// 轮播相关
#import "PDCycleScrollView.h"
#import "PDSnatchProductBannerItems.h"

//活动相关
#import "PDProductCollectionViewCell.h"
#import "PDProductDetailViewController.h"
#import "PDSnatchProductList.h"
#import "PDShopSectionView.h"
// 标题
#import "PDTitleBar.h"

@interface PDShopRootViewController ()<SDCycleScrollViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, strong) PDTitleBar *titleBar;
@property (nonatomic, strong) PDSnatchProductBannerItems *items;
@property (nonatomic, strong) UICollectionView *mainView;
@property (nonatomic, strong) PDShopSectionView *sectionView;
@property (nonatomic, strong) NSMutableArray *bannerItemsArr;
@property (nonatomic, strong) NSMutableArray *allItemsArr;
@property (nonatomic, strong) NSMutableArray *lastedItemsArr;
@property (nonatomic, assign) int page1;
@property (nonatomic, assign) int page2;

//两个活动model
@property (nonatomic, strong) PDSnatchProductList *allList;
@property (nonatomic, strong) PDSnatchProductList *lastedList;

// 偏移量
@property (nonatomic, assign) CGFloat y;
@end

@implementation PDShopRootViewController

+ (instancetype)sharedController {
    static PDShopRootViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[PDShopRootViewController alloc] init];
    });
    return _sharedAccountModel;
}

- (NSMutableArray *)bannerItemsArr {
    if (!_bannerItemsArr) {
        _bannerItemsArr = [NSMutableArray array];
    }
    return _bannerItemsArr;
}

- (NSMutableArray *)allItemsArr {
    if (!_allItemsArr) {
        _allItemsArr = [NSMutableArray array];
    }
    return _allItemsArr;
}

- (NSMutableArray *)lastedItemsArr {
    if (!_lastedItemsArr) {
        _lastedItemsArr = [NSMutableArray array];
    }
    return _lastedItemsArr;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self basicSetting];
    [self pageSetting];
    [self titleBarSetting];
    [self fetchBanner];
    [self fetchAllProducts];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

- (void)basicSetting {
    _page1 = 1;
    [PDHUD showHUDProgress:@"正在获取活动列表，请稍后..." diary:0];
}

- (void)pageSetting {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.scrollDirection = UICollectionViewScrollDirectionVertical;
    layout.itemSize = [PDProductCollectionViewCell cellSize];
    layout.sectionInset = UIEdgeInsetsMake([PDProductCollectionViewCell space], [PDProductCollectionViewCell space], [PDProductCollectionViewCell space], [PDProductCollectionViewCell space]);
    [layout setHeaderReferenceSize:CGSizeMake(kScreenBounds.size.width, kBannerHeight + KTitleHeight)];
    self.mainView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:layout];
    self.mainView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.mainView.alwaysBounceVertical = YES;
    self.mainView.backgroundColor = BACKGROUND_VIEW_COLOR;
    self.mainView.delegate = self;
    self.mainView.dataSource = self;
    [self.mainView registerClass:[PDProductCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
    [self.mainView registerClass:[PDShopSectionView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"productSectionHeader"];
    [self.view addSubview:self.mainView];
    
    __weak typeof(self) weakSelf = self;
    [self.mainView appendingPullToRefreshHandler:^{
        if (weakSelf.allItemsArr.count) {
            [weakSelf.allItemsArr removeAllObjects];
        }
        weakSelf.page1 = 1;
        [weakSelf fetchAllProducts];
    }];
    [self.mainView appendingFiniteScrollingPullToRefreshHandler:^{
        if (weakSelf.allList.hasNextPage) {
            _page1++;
            [weakSelf fetchAllProducts];
        } else {
            [weakSelf.mainView stopFinishScrollingRefresh];
        }
    }];
}

- (void)titleBarSetting {
    // 自定义的导航栏
    self.titleBar = [[PDTitleBar alloc] init];
    self.titleBar.registScrollView = self.mainView;
    self.titleBar.title = @"夺宝";
    [self.view addSubview:self.titleBar];
}

#pragma mark - 网络请求

- (void)fetchBanner {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchBanner requestParams:nil responseObjectClass:[PDSnatchProductBannerItems class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        if (isSucceeded) {
            weakSelf.items = (PDSnatchProductBannerItems *)responseObject;
            [weakSelf.bannerItemsArr addObjectsFromArray:weakSelf.items.items];
            [weakSelf updateFrames];
        }
    }];
}

- (void)fetchNewIdWithId:(NSString *)ID {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchGetNewId requestParams:@{@"activityId":ID} responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (isSucceeded) {
            NSString *ID = responseObject[@"lotteryId"];
            PDProductDetailViewController *detailViewController = [[PDProductDetailViewController alloc] init];
            detailViewController.productId = ID;
            [weakSelf pushViewController:detailViewController animated:YES];
        }
    }];
}

- (void)updateFrames {
    NSMutableArray *imgArr = [NSMutableArray array];
    NSMutableArray *titleArr = [NSMutableArray array];
    for (PDSnatchProductBannerModel *item in self.bannerItemsArr) {
        [imgArr addObject:[PDCenterTool absoluteUrlWithRisqueUrl:item.pic]];
        [titleArr addObject:item.text];
    }
    
    self.sectionView.bannerScrollView.imageURLStringsGroup = [imgArr copy];
    self.sectionView.bannerScrollView.titlesGroup = [titleArr copy];
}

////////
/** 全部活动*/
- (void)fetchAllProducts {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchAllProduct requestParams:@{@"pageNum":@(_page1)} responseObjectClass:[PDSnatchProductList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        [PDHUD dismiss];
        if (isSucceeded) {
            PDSnatchProductList *list = (PDSnatchProductList *)responseObject;
            weakSelf.allList = list;
            
            //> 转化model中的时间列表
            for (PDSnatchProductItem *item in list.items) {
                item.openTime = item.currentTime + item.leftTime;
            }
            
            [weakSelf.allItemsArr addObjectsFromArray:list.items];
            [weakSelf.mainView reloadData];
        }
        
        [weakSelf.mainView stopFinishScrollingRefresh];
        [weakSelf.mainView stopPullToRefresh];
    }];
}

/** 最近活动*/
- (void)fetchLastedProducts {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchNewList requestParams:@{@"pageNum":@(_page2)} responseObjectClass:[PDSnatchProductList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        [PDHUD dismiss];
        if (isSucceeded) {
            PDSnatchProductList *list = (PDSnatchProductList *)responseObject;
            weakSelf.lastedList = list;
            
            for (PDSnatchProductItem *item in list.items) {
                item.openTime = item.currentTime + item.leftTime;
            }
            
            [weakSelf.lastedItemsArr addObjectsFromArray:list.items];
            [weakSelf.mainView reloadData];
        }
        
        [weakSelf.mainView stopFinishScrollingRefresh];
        [weakSelf.mainView stopPullToRefresh];
    }];
}

#pragma mark - 显示无数据页

- (void)showNoDataPage {
    __weak typeof(self) weakSelf = self;
    [self.mainView showPrompt:@"正在紧张的筹备夺宝活动中..." withImage:[UIImage imageNamed:@"icon_nodata_panda"] buttonTitle:@"点击刷新" clickBlock:^{
            weakSelf.page1 = 1;
            [weakSelf fetchAllProducts];
    }];
}

#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    PDSnatchProductBannerModel *item = self.bannerItemsArr[index];
    [self fetchNewIdWithId:item.actionEntityId];
}

#pragma mark - CollectionDelegate

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.allItemsArr.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    PDProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"productCell" forIndexPath:indexPath];
    
    if (self.allItemsArr.count > indexPath.row) {
        cell.needToShowMsec = YES;
        cell.model = self.allItemsArr[indexPath.row];
    }
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.allItemsArr.count) {
        PDProductDetailViewController *detailViewController = [[PDProductDetailViewController alloc] init];
        PDSnatchProductItem *item = self.allItemsArr[indexPath.row];
        detailViewController.productId = item.ID;
        [self pushViewController:detailViewController animated:YES];
    }
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        PDShopSectionView *sectionView = (PDShopSectionView *)[collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"productSectionHeader" forIndexPath:indexPath];
        self.sectionView = sectionView;
        self.sectionView.bannerScrollView.delegate = self;
        return sectionView;
    }
    return nil;
}

@end
