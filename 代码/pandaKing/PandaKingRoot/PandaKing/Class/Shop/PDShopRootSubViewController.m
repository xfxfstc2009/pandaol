//
//  PDShopRootSubViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/14.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDShopRootSubViewController.h"
#import "PDProductCollectionViewCell.h"
#import "PDProductDetailViewController.h"
#import "PDSnatchProductList.h"


@interface PDShopRootSubViewController()<UICollectionViewDataSource, UICollectionViewDelegate>{
    NSInteger currentPage;
}

@property (nonatomic,strong)UICollectionView *shopCollectionView;
@property (nonatomic,strong)NSMutableArray *collectionMutableArr;

@end

@implementation PDShopRootSubViewController


-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createCollectionView];
    [self interfaceManager];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    for (PDProductCollectionViewCell *cell in [self.shopCollectionView visibleCells]){
        [cell.dateLabel timerClean];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    for (PDProductCollectionViewCell *cell in [self.shopCollectionView visibleCells]){
        [cell.dateLabel timerStart];
    }
}

#pragma mrak - InterfaceManager
-(void)interfaceManager{
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestTogetProductWithHornal:YES];
}

#pragma mark - pageSetting
-(void)pageSetting{
    
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.collectionMutableArr = [NSMutableArray array];
    currentPage = 1;
}

#pragma mark - UICollectionView
-(void)createCollectionView{
    if(!self.shopCollectionView){
        UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
        self.shopCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        self.shopCollectionView.backgroundColor = [UIColor clearColor];
        self.shopCollectionView.showsVerticalScrollIndicator = NO;
        self.shopCollectionView.delegate = self;
        self.shopCollectionView.dataSource = self;
        self.shopCollectionView.showsHorizontalScrollIndicator = NO;
        self.shopCollectionView.scrollsToTop = YES;
        self.shopCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:self.shopCollectionView];
        
        [self.shopCollectionView registerClass:[PDProductCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
        [self.shopCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"productSectionHeader"];
        
        __weak typeof(self)weakSelf = self;
        [self.shopCollectionView appendingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestTogetProductWithHornal:YES];
        }];
        
        [self.shopCollectionView appendingFiniteScrollingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestTogetProductWithHornal:NO];
        }];
    }
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.collectionMutableArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PDProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"productCell" forIndexPath:indexPath];
    
    if (self.collectionMutableArr.count > indexPath.row) {
        cell.needToShowMsec = YES;
        cell.model = [self.collectionMutableArr objectAtIndex:indexPath.row];
    }
    
    cell.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
    cell.layer.borderWidth = .5f;
    
    __weak typeof(self)weakSelf = self;
    [cell timerFinishBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 1. 清除所有cell timer
        for (PDProductCollectionViewCell *cell in [strongSelf.shopCollectionView visibleCells]){
            [cell cleanTimeLabel];
        }
        // 2.重新请求
        [strongSelf sendRequestTogetProductWithHornal:YES];
    }];
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *sectionView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"productSectionHeader" forIndexPath:indexPath];
        return sectionView;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, LCFloat(10), 5, LCFloat(10));
}


#pragma mark - UICollectionViewDelegate
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [PDProductCollectionViewCell cellSize];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.collectionMutableArr.count) {
        PDProductDetailViewController *detailViewController = [[PDProductDetailViewController alloc] init];
        PDSnatchProductItem *item = [self.collectionMutableArr objectAtIndex:indexPath.row];
        detailViewController.productId = item.ID;
        [self pushViewController:detailViewController animated:YES];
    }
}

#pragma mark - InterfaceManager
-(void)sendRequestTogetProductWithHornal:(BOOL)hornal{
    __weak typeof(self) weakSelf = self;
    if (hornal){
        currentPage = 1;
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:snatchAllProduct requestParams:@{@"pageNum":@(currentPage)} responseObjectClass:[PDSnatchProductList class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf) {
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded) {
            if (hornal){
                [strongSelf.collectionMutableArr removeAllObjects];
            }
            
            strongSelf->currentPage ++ ;
            
            PDSnatchProductList *snatchProductsList = (PDSnatchProductList *)responseObject;
       
            //> 转化model中的时间列表
            for (PDSnatchProductItem *item in snatchProductsList.items) {
                item.openTime = item.currentTime + item.leftTime;
            }
            
            [strongSelf.collectionMutableArr addObjectsFromArray:snatchProductsList.items];
            
            if (strongSelf.collectionMutableArr.count){
                [strongSelf.shopCollectionView dismissPrompt];
            } else {
                [strongSelf.shopCollectionView showPrompt:@"当前没有夺宝内容哦~" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
            [strongSelf.shopCollectionView reloadData];
        }
    
        [strongSelf.shopCollectionView stopFinishScrollingRefresh];
        [strongSelf.shopCollectionView stopPullToRefresh];
    }];
}

@end
