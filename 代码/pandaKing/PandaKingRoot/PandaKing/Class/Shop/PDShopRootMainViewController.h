//
//  PDShopRootMainViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"

@interface PDShopRootMainViewController : AbstractViewController
+ (instancetype)sharedController;
@end
