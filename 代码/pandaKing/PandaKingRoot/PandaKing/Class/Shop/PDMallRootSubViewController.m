//
//  PDMallRootSubViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/15.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMallRootSubViewController.h"
#import "PDStoreListTableViewCell.h"
#import "PDStoreDetailViewController.h"
#import "PDStoreActivityModel.h"
#import "PDProductCollectionViewCell.h"

@interface PDMallRootSubViewController()<UICollectionViewDataSource, UICollectionViewDelegate>{
    NSInteger currentPage;
}
@property (nonatomic,strong)UICollectionView *mallCollectionView;
@property (nonatomic,strong)NSMutableArray *productsMutableArr;

@end

@implementation PDMallRootSubViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithinit];
    [self createCollectionView];
    [self sendRequestToGetStoreProductList];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"竹子商城";
}

#pragma mark - arrayWithInit
-(void)arrayWithinit{
    self.productsMutableArr = [NSMutableArray array];
    currentPage = 1;
}

#pragma mark - UICollectionView
-(void)createCollectionView{
    if(!self.mallCollectionView){
        UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
        self.mallCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        self.mallCollectionView.backgroundColor = [UIColor clearColor];
        self.mallCollectionView.showsVerticalScrollIndicator = NO;
        self.mallCollectionView.delegate = self;
        self.mallCollectionView.dataSource = self;
        self.mallCollectionView.showsHorizontalScrollIndicator = NO;
        self.mallCollectionView.scrollsToTop = YES;
        self.mallCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:self.mallCollectionView];
        
        [self.mallCollectionView registerClass:[PDProductCollectionViewCell class] forCellWithReuseIdentifier:@"productCell"];
        [self.mallCollectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"productSectionHeader"];
        
        __weak typeof(self)weakSelf = self;
        [self.mallCollectionView appendingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetStoreProductList];
        }];
        
        [self.mallCollectionView appendingFiniteScrollingPullToRefreshHandler:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToGetStoreProductList];
        }];
    }
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.productsMutableArr.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PDProductCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"productCell" forIndexPath:indexPath];
    
    if (self.productsMutableArr.count > indexPath.row) {
        cell.needToShowMsec = YES;
        cell.transferProductSingleModel = [self.productsMutableArr objectAtIndex:indexPath.row];
    }
    
    cell.layer.borderColor = [UIColor colorWithCustomerName:@"分割线"].CGColor;
    cell.layer.borderWidth = .5f;
    
    return cell;
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        UICollectionReusableView *sectionView = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"productSectionHeader" forIndexPath:indexPath];
        return sectionView;
    }
    return nil;
}

#pragma mark - UICollectionViewDelegate
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, LCFloat(10), 5, LCFloat(10));
}


#pragma mark - UICollectionViewDelegate
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [PDProductCollectionViewCell cellSize];
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.productsMutableArr.count) {
        PDStoreActivityItem *item = [self.productsMutableArr objectAtIndex:indexPath.row];
        PDStoreDetailViewController *detailViewController = [[PDStoreDetailViewController alloc] init];
        detailViewController.ID = item.ID;
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
}

#pragma mark - InterfaceManager
-(void)sendRequestToGetStoreProductList{
    __weak typeof(self)weakSelf = self;
    if ([self.mallCollectionView.isXiaLa isEqualToString:@"YES"]){
        currentPage = 1;
    }
    [[NetworkAdapter sharedAdapter] fetchWithPath:storeList requestParams:@{@"pageNum":@(currentPage)}  responseObjectClass:[PDStoreActivityModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDStoreActivityModel *storeActivityModel = (PDStoreActivityModel *)responseObject;
            if ([strongSelf.mallCollectionView.isXiaLa isEqualToString:@"YES"]){
                [strongSelf.productsMutableArr removeAllObjects];
            }
            strongSelf->currentPage ++;
            
            [strongSelf.productsMutableArr addObjectsFromArray:storeActivityModel.listPage.items];
            [weakSelf.mallCollectionView reloadData];
            
            if (strongSelf.productsMutableArr.count){
                [strongSelf.mallCollectionView dismissPrompt];
            } else {
                [strongSelf.mallCollectionView showPrompt:@"当前没有商品哦~" withImage:[UIImage imageNamed:@"icon_nodata_panda"] andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
        }
        [weakSelf.mallCollectionView stopFinishScrollingRefresh];
        [weakSelf.mallCollectionView stopPullToRefresh];
    }];
}
@end
