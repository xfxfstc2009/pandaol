//
//  PDShopRootMainViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/2/13.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDShopRootMainViewController.h"
#import <HTHorizontalSelectionList.h>
#import "PDShopRootViewController.h"
#import "PDShopRootMainHeaderView.h"
#import "PDShopRootSubViewController.h"
#import "PDMallRootSubViewController.h"
#import "CCZTrotingLabel.h"
#import "PDShopRootWinListModel.h"
#import "PDAmusementNoticeModel.h"
#import "PDWalletViewController.h"
#import "PDBackpackViewController.h"
#import "PDMessageViewController.h"
@interface PDShopRootMainViewController()<HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate,UIScrollViewDelegate,UINavigationControllerDelegate,UINavigationBarDelegate,UIGestureRecognizerDelegate>

@property (nonatomic,strong)HTHorizontalSelectionList *segmentRootList;
@property (nonatomic,strong)NSArray *segmentArr;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)PDShopRootSubViewController *shopRootViewController;
@property (nonatomic,strong)PDShopRootMainHeaderView *headerView;
@property (nonatomic,strong)PDMallRootSubViewController *storeViewController;
@property (nonatomic,strong)CCZTrotingLabel *gonggaoLabel;
@property (nonatomic,strong)UIPanGestureRecognizer *panGest;

@end

@implementation PDShopRootMainViewController

+ (instancetype)sharedController{
    static PDShopRootMainViewController *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[PDShopRootMainViewController alloc] init];
    });
    return _sharedAccountModel;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    [self.gonggaoLabel walk];
    [self fetchMessageState];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];                                 // 1. 页面设置
    [self arrayWithInit];                               // 2. 数据初始化
    [self createSegmentList];                           // 3. 创建segment
    [self createGonggao];                               // 4. 创建公告label
    [self createScrollView];                            // 5. 创建底部的scrollView
    [self createDuobaoViewController];                  // 6. 创建夺宝控制器
    [self createStoreViewController];                   // 7. 创建商品控制器
    [self sendRequestToGetGonggao];                     // 8. 获取公告数据源
    [self addGestureRecognizer];                        // 9. 添加手势
    
    [self setupRightItem];
}

- (void)setupRightItem {
    if (self == [self.navigationController.viewControllers firstObject]){
        __weak typeof(self)weakSelf = self;
        self.tempMsgButton = [weakSelf rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_panda_message_nor"] barHltImage:[UIImage imageNamed:@"icon_panda_message_nor"] action:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            PDMessageViewController *messageViewController = [[PDMessageViewController alloc]init];
            [messageViewController setHidesBottomBarWhenPushed:YES];
            [strongSelf.navigationController pushViewController:messageViewController animated:YES];
        }];
        [self.tempMsgButton sizeToFit];
    }
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"福利";
}

#pragma mark arrayWithInit
-(void)arrayWithInit{
    self.segmentArr = @[@"夺宝",@"商城"];
}

#pragma mark - createSegmentList
-(void)createSegmentList{
    if (!self.segmentRootList){
        self.segmentRootList = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake(0, 0,kScreenBounds.size.width , LCFloat(44))];
        self.segmentRootList.delegate = self;
        self.segmentRootList.dataSource = self;
        self.segmentRootList.backgroundColor = [UIColor whiteColor];
        [self.segmentRootList setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
        self.segmentRootList.selectionIndicatorColor = c26;
        self.segmentRootList.selectionIndicatorAnimationMode = HTHorizontalSelectionIndicatorAnimationModeHeavyBounce;
        self.segmentRootList.bottomTrimColor = [UIColor clearColor];
        [self.segmentRootList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
        self.segmentRootList.isNotScroll = YES;
        self.segmentRootList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleBottomBar;
        [self.view addSubview:self.segmentRootList];
    }
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
    return [self.segmentArr objectAtIndex:index];
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * index, 0) animated:YES];
}

#pragma mark - createA.d Label
-(void)createGonggao{
    self.gonggaoLabel = [[CCZTrotingLabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.segmentRootList.frame) + LCFloat(10), kScreenBounds.size.width, LCFloat(29))];
    self.gonggaoLabel.backgroundColor = [UIColor whiteColor];
    self.gonggaoLabel.repeatTextArr = YES;
    self.gonggaoLabel.pause = 1;
    self.gonggaoLabel.rate = RateNormal;
    self.gonggaoLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self.view addSubview:self.gonggaoLabel];
    
    UIView *tempView = [[UIView alloc]init];
    tempView.frame = CGRectMake(0, 0, 2 * LCFloat(11) + LCFloat(12), self.gonggaoLabel.size_height);
    tempView.backgroundColor = [UIColor clearColor];
    
    PDImageView *iconImgView = [[PDImageView alloc]init];
    iconImgView.backgroundColor = [UIColor clearColor];
    iconImgView.image = [UIImage imageNamed:@"icon_message_win"];
    iconImgView.frame = CGRectMake(LCFloat(11), 0, LCFloat(12), LCFloat(12));
    iconImgView.center_y = tempView.center_y;
    [tempView addSubview:iconImgView];
    self.gonggaoLabel.leftView = tempView;
}


-(void)createHeaderView{
    self.headerView = [[PDShopRootMainHeaderView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [PDShopRootMainHeaderView calculationCellHeight])];
    [self.view addSubview:self.headerView];
    
    
    UIView *lineView = [[UIView alloc]init];
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    lineView.frame = CGRectMake(0, self.headerView.size_height - .5f, kScreenBounds.size.width, .5f);
    [self.headerView addSubview:lineView];
    
    
    __weak typeof(self)weakSelf = self;
    [self.headerView actionClickWithLeft:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDWalletViewController *qianbaoViewController = [[PDWalletViewController alloc]init];
        [strongSelf.navigationController pushViewController:qianbaoViewController animated:YES];
    }];
    
    [self.headerView actionClickWithRight:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        PDBackpackViewController *qianbaoViewController = [[PDBackpackViewController alloc]init];
        [strongSelf.navigationController pushViewController:qianbaoViewController animated:YES];
    }];
    
}

#pragma mark - UIScrollView
-(void)createScrollView{
    self.mainScrollView = [[UIScrollView alloc]init];
    self.mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.gonggaoLabel.frame), kScreenBounds.size.width, kScreenBounds.size.height - CGRectGetMaxY(self.gonggaoLabel.frame) - 64);
    self.mainScrollView.delegate = self;
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 2 , self.mainScrollView.size_height);
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.scrollEnabled = YES;
    self.mainScrollView.showsVerticalScrollIndicator = NO;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.mainScrollView];
}

#pragma mark - 创建夺宝
-(void)createDuobaoViewController{
    self.shopRootViewController = [[PDShopRootSubViewController alloc]init];
    self.shopRootViewController.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.mainScrollView.size_height - 50);
    [self.mainScrollView addSubview:self.shopRootViewController.view];
    [self addChildViewController:self.shopRootViewController];
}

#pragma mark - 创建商城
-(void)createStoreViewController{
    self.storeViewController = [[PDMallRootSubViewController alloc]init];
    self.storeViewController.view.frame = CGRectMake(kScreenBounds.size.width, 0, kScreenBounds.size.width, self.mainScrollView.size_height);
    [self.mainScrollView addSubview:self.storeViewController.view];
    [self addChildViewController:self.storeViewController];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    NSInteger currentIndex = scrollView.contentOffset.x / kScreenBounds.size.width;
    [self.segmentRootList setSelectedButtonIndex:currentIndex animated:YES];
}

#pragma mark - sendRequestToGetInfo
-(void)sendRequestToGetGonggao{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:winList requestParams:nil responseObjectClass:[PDShopRootWinListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            PDShopRootWinListModel *rootListModel = (PDShopRootWinListModel *)responseObject;
            NSMutableArray *infoArr = [NSMutableArray array];
            for (int i = 0 ; i < rootListModel.items.count;i++){
                PDShopRootWinSingleModel *singleInfo = [rootListModel.items objectAtIndex:i];
                NSString *tempInfo = [NSString stringWithFormat:@"恭喜用户%@获得%@",singleInfo.winner,singleInfo.treasure];
                [infoArr addObject:tempInfo];
            }
            [strongSelf.gonggaoLabel addTexts:infoArr];
        }
    }];
}



#pragma mark - Other Manager
-(void)addGestureRecognizer{
    self.panGest = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(panGestureRecognizedWithMap:)];
    self.panGest.delegate = self;
    [self.view addGestureRecognizer:self.panGest];
}

- (void)panGestureRecognizedWithMap:(UIPanGestureRecognizer *)recognizer{
    [[RESideMenu shareInstance] panGestureRecognized:recognizer];
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (gestureRecognizer == self.panGest && [gestureRecognizer isKindOfClass:[UIPanGestureRecognizer class]]){
        CGPoint point = [touch locationInView:gestureRecognizer.view];
        if (point.x < 50.0 || point.x > self.view.frame.size.width - 50.0) {
            self.mainScrollView.scrollEnabled = NO;
            return YES;
        } else {
            self.mainScrollView.scrollEnabled = YES;
            return NO;
        }
    }
    
    return YES;
}

- (void)fetchMessageState {
    __weak typeof(self) weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithPath:messageState requestParams:nil responseObjectClass:[PDMessageState class] succeededBlock:^(BOOL isSucceeded, NSDictionary *responseObject, NSError *error) {
        if (!weakSelf) {
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded) {
            PDMessageState *state = (PDMessageState *)responseObject;
            // 重置
            [AccountModel sharedAccountModel].messageState = state;
            
            if (state.hasUnReadMsg) {
                [strongSelf.tempMsgButton setImage:[UIImage imageNamed:@"icon_panda_message_has_nor"] forState:UIControlStateNormal];
                [strongSelf.tempMsgButton sizeToFit];
            } else {
                [strongSelf.tempMsgButton setImage:[UIImage imageNamed:@"icon_panda_message_nor"] forState:UIControlStateNormal];
                [strongSelf.tempMsgButton sizeToFit];
            }
        }
    }];
}

@end
