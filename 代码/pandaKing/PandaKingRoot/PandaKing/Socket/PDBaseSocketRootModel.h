//
//  PDBaseSocketRootModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/10/1.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDMatchguessinfoGuessListSingleModel.h"
#import "PDChaosFightBeginGameRootModel.h"
#import "PDChaosFightGooutSingleModel.h"
#import "PDChaosFightTouzhuSingleModel.h"
#import "PDChaosFightTouzhuBierenRootModel.h"
#import "PDChaosFightKaijiangRootModel.h"
#import "PDChaosFightOtherTouzhuSingleModel.h"
#import "PDYuezhanCombatRootModel.h"
#import "PDJuediYuezhanSocketJoinGameModel.h"


typedef NS_ENUM(NSInteger,socketType) {
    socketType781 = 781,                        /**< 召唤师激活*/
    socketType780 = 780,                        /**< 裁判激活工单*/
    socketType801 = 801,                        /**< 获奖通知*/
    socketType900 = 900,                        /**< 获取当前的主播猜*/
    socketType901 = 901,                        /**< 主播猜竞猜*/
    socketType950 = 950,                        /**< 赛事猜*/
    // 打野大作战
    socketType971 = 971,                        /**< 进入房间*/
    socketType972 = 972,                        /**< 玩家进入*/
    socketType970 = 970,                        /**< 人数变化*/
    socketType973 = 973,                        /**< 退出*/
    socketType974 = 974,                        /**< 玩家退出*/
    socketType975 = 975,                        /**< 投注*/
    socketType976 = 976,                        /**< 别人投注*/
    socketType978 = 978,                        /**< 踢出*/
    socketType979 = 979,                        /**< 开奖*/
    
    // dota2 打野
    socketType980 = 980,                    /**< 总人数*/
    socketType981 = 981,
    socketType982 = 982,
    socketType983 = 983,
    socketType984 = 984,
    socketType985 = 985,
    socketType986 = 986,
    socketType987 = 987,
    socketType988 = 988,
    socketType989 = 989,
    
    socketType990 = 990,                    /**< 总人数*/
    socketType991 = 991,
    socketType992 = 992,
    socketType993 = 993,
    socketType994 = 994,
    socketType995 = 995,
    socketType996 = 996,
    socketType997 = 997,
    socketType998 = 998,
    socketType999 = 999,
    
    // 对战匹配
    socketType1100 = 1100,
    socketType1101 = 1101,
    socketType1102 = 1102,
    socketType1103 = 1103,
    socketType1104 = 1104,
    socketType1105 = 1105,
    
    socketType501 = 501,                        /**< 报错*/
    
    // 加入百人约战
    socketTypePubgCombatJoin = 1110,            /**< 加入约战*/
    socketTypePubgCombatConfigRoom = 1111,      /**< 配置房间*/
    socketTypePubgCombatStarted = 1112,         /**< 约战开始*/
    socketTypePubgCombatSettled = 1113,         /**< 约战结算*/
};

@interface PDBaseSocketRootModel : FetchModel

@property (nonatomic,copy)NSString *gmGameUserName;         /**< 裁判激活工单 - 游戏名字*/
@property (nonatomic,copy)NSString *orderId;                /**< 裁判激活工单 - 订单编号*/
@property (nonatomic,assign)socketType type;                   /**< 裁判激活工单 - 类型*/


// 召唤师激活通知  游戏信息
@property (nonatomic,copy)NSString *gameUserName;           /**< 召唤师激活通知 - 游戏名字*/
@property (nonatomic,assign)BOOL activate;                  /**< 召唤师激活通知 - 是否认证*/
@property (nonatomic,copy)NSString *msg;                    /**< 召唤师激活通知 - 消息内容*/
@property (nonatomic,assign)BOOL oldMemberBind;             /**< 召唤师是否绑定*/

// socket Main back

@property (nonatomic,copy)NSString *token;
@property (nonatomic,copy)NSString *status;
@property (nonatomic,copy)NSString *socketAddress;
@property (nonatomic,assign)NSInteger socketPort;
@property (nonatomic,assign)NSTimeInterval lastActiveTime;


// 赛事猜
@property (nonatomic,strong)PDMatchguessinfoGuessListSingleModel *items;

// 打野大作战
@property (nonatomic,strong)PDChaosFightBeginGameRootModel *jungleGuessInfo;
@property (nonatomic,assign)NSInteger totalMemberCount;                                 /**< 970*/
@property (nonatomic,strong)PDChaosFightGooutSingleModel *member;
@property (nonatomic,strong)PDChaosFightTouzhuSingleModel *guessItem;
@property (nonatomic,strong)PDChaosFightTouzhuBierenRootModel *aa;
@property (nonatomic,strong)PDChaosFightKaijiangRootModel *drawJungleLotteryResult;         /**< 开奖信息*/
@property (nonatomic,strong)PDChaosFightOtherTouzhuSingleModel *stakeData;

// 匹配对战
@property (nonatomic,strong)PDYuezhanCombatSingleModel *combat;
//
@property (nonatomic,strong)PDJuediYuezhanSocketJoinGameModel *pubgCombat;

@end
