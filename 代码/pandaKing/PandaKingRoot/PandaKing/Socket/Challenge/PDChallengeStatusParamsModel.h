//
//  PDChallengeStatusParamsModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDChallengeStatusParamsModel : FetchModel

@property (nonatomic,assign)NSInteger type;                 /**< 类型*/
@property (nonatomic,copy)NSString *uid;                    /**< 当前用户的编号*/

@end
