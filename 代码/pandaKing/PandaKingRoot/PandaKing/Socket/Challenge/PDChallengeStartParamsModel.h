//
//  PDChallengeStartModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 【出征】
#import "FetchModel.h"

@interface PDChallengeStartParamsModel : FetchModel

@property (nonatomic,assign)NSInteger type;             /**< 进行出征*/
@property (nonatomic,copy)NSString *lid;                /**< 队长id*/

@end
