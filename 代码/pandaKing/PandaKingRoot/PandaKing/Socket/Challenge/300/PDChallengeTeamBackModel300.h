//
//  PDChallengeTeamBackModel300.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDChallengeTeamBackModel300 : FetchModel

/*
 {type:300,fid:好友的id,fname:好友的昵称,:好友的头像,:卡片等级,zone:区的中文名称,cancel:请求持续时间，在持续时间内必须回复，未回复直接关闭，无需返回，从收到消息的时间开始计算}
 */

@property (nonatomic,assign)NSInteger type;             /**< 类型*/
@property (nonatomic,copy)NSString *fid;                /**< 好友的 id*/
@property (nonatomic,copy)NSString *fname;              /**< 好友的昵称*/
@property (nonatomic,copy)NSString *favatar;            /**< 好友的头像*/
@property (nonatomic,assign)PDCardLevel card;           /**< 卡片的等级*/
@property (nonatomic,copy)NSString *zone;               /**< 区的中文名称*/
@property (nonatomic,assign)NSInteger cancel;           /**< 请求的持续时间*/;

// 302
@property (nonatomic,copy)NSString *uid;                /**< 我的id*/
@property (nonatomic,copy)NSString *uname;              /**< 我的用户名*/
@property (nonatomic,copy)NSString *uavatar;            /**< 我的头像*/

@end
