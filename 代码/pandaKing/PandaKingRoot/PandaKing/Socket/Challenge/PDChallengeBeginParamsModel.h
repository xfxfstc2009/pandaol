//
//  PDChallengeBeginModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//
// 对战开始
#import "FetchModel.h"

@interface PDChallengeBeginParamsModel : FetchModel

@property (nonatomic,assign)NSInteger type;             /**< 类型*/
@property (nonatomic,copy)NSString *lid;                /**< 队长id*/

@end
