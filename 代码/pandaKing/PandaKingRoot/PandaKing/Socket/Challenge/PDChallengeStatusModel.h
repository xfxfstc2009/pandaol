//
//  PDChallengeStatusModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"
#import "PDChallengeStatusGroupModel.h"
@interface PDChallengeStatusModel : FetchModel

@property (nonatomic,assign)NSInteger type;         // 类型
@property (nonatomic,strong)NSArray<PDChallengeStatusGroupModel> *group;            /**< 组队信息*/

@end
