//
//  PDChallengeStatusGroupModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@protocol PDChallengeStatusGroupModel <NSObject>

@end

@interface PDChallengeStatusGroupModel : FetchModel

@property (nonatomic,copy)NSString *uid;                /**< 用户id */
@property (nonatomic,copy)NSString *uname;              /**< 用户名*/
@property (nonatomic,copy)NSString *uavatar;            /**< 用户头像*/
@property (nonatomic,assign)BOOL leader;                /**< 用户是否是队长 */

@end
