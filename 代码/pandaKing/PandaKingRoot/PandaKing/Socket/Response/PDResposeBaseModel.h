//
//  PDResposeBaseModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDResposeBaseModel : FetchModel

@property (nonatomic,assign)NSInteger type;               /**< 类型*/

@end
