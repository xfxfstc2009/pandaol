//
//  PDCloseTeamModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
// 【用户离开组队】

#import "FetchModel.h"

@interface PDCloseTeamModel : FetchModel
@property (nonatomic,assign)NSInteger type;             /**< 离队*/
@property (nonatomic,copy)NSString *fid;                /**< 队长的编号*/
@property (nonatomic,copy)NSString *uid;                /**< 当前用户的编号*/
@end
