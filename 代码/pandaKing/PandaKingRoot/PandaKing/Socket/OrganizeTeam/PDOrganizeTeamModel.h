//
//  PDOrganizeTeamModel.h
//  PandaKing
//
//  Created by GiganticWhale on 16/7/13.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "FetchModel.h"

@interface PDOrganizeTeamModel : FetchModel

@property (nonatomic,assign)NSInteger type;             /**< 200*/
@property (nonatomic,copy)NSString *uid;                /**< 用户id*/
@property (nonatomic,copy)NSString *uname;              /**< 用户昵称*/
@property (nonatomic,copy)NSString *uavatar;            /**< 用户头像*/
@property (nonatomic,copy)NSString *fid;                /**< 被邀请好友的id */
@property (nonatomic,copy)NSString *fname;              /**< 被邀请好友的昵称*/
@property (nonatomic,assign)PDCardLevel card;           /**< 卡片等级*/
@property (nonatomic,copy)NSString *zone;               /**< 区的中文名称*/

@end
